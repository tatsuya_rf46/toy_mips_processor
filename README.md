# Toy MIPS Processor
A mips-subset processor to calculate Fibonacci Function on fpga.  
We implemented pipelined processor that only supports a few instructions.  

## Implementation
The digital design can be seen in 'circuit_diagram/pipeline.pdf' or the diagram of 'design_3.bd' in vivado.

## How to use
In Vivado, open 'project/toy_mips_processor.xpr', and run simulation.  
This project only calculates 'fibonacci 15', not general-purpose.

### project/
Vivado Project Directory.  

### src/fibonacci.txt
Assembly File to Execute.  
The binary (src/inst.coe) is read in this project.

## Results
The result of simulation.  
"alu_result" shows '610', the value of fibonacci 15!  
![](src/screenshot.png)

The number of cycles that culculating 'fibonacci 15' takes.


| Implementation | | Cycles |
| ------ | ------ | ------ |
| Pipeline Processor | design_3.bd | 40448 |
