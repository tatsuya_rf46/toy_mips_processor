// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
P0s6hQN4sULS0iJYLkeO9ZaAigeRTsmBn8blUP52hjMcNyHrBinKbz7Do0E9QL2GaPe8Awbxtn7z
3isXAO8VK6CtSErjGy/M87qU44KdUl7nldPXegKuKkYO1canJNz6MsVfxS3tSrmepT3RoVZRUD98
jRpdL0kvRJ/NPqRicuBChA9IZqpqHDCcBqN0NVgvS0w08wsa6/9qHXUsLkhWJcCPPHJ4v7IjnPK3
ZG6DvH1pyjk7WfwTszVEAyKo40VCX5LDec16xOnMdTGbePXLUh60ZJKk2b01fppbqNYDO57BaJYX
lbMYR4nItMKgfftkWnpL0H65cwwdmwDVkyMoiqDHAc/bDF7lnMbl6OotO30dREOCtNGGdwB9paq4
VTtEI4YSZr13KdoDFcAjY6FqIRtPCjMMdsU/DNKQpFGqr7PJLuIJBBZopdt+sELS1XuwMhjEohEQ
xB8ibROrsuZws3qPcGUHurW65u5zBH013XyDu6xBbpiKhUSWVe8SNLshSIfYupEQIsCCpFThPEpz
OCqWycncVn+h1ghOrqao+F44v81GJoQ/aOYofGbThwDA1LNuRQmTHEKZ1Yb8LiYljNj5ZJhgO33Z
N0lW+nNA4JCHpRgEJ6p99Rp8EX3+htG3TBdQdn7KrIKWrJM9qnthDqyDxibxJ5wCzDL3zEAALtZG
xYCNYWqn3G56uWG9TvC4Iu526TT/60PR6S9iHmUxzE4+QwdYSSBVX9HOhPHb2J43oxegVHFGhxa5
B9sWl9jMKgUqDII1mhNWv/uiK26XTVKDq+1eWck16rapkDVRWOw83Mcva+tjDID1W6cP+6BRZ3UH
W2XYxFVWrlGUDMoLAUVTPJImf0MaiCxelJECUbGmn3ZY8VsPRO+pxiv2b4Yt0fTgwsi2wVNTNxHQ
EHkf966HpAs08FhCXeqWyDuDOor99atHCU9V37jYfofi0bj71Cx2F0xvrlHYsXVNwr4TWS5cIjoL
GV9IAFEKr5ybsMJGaWojsrsUn2FkinCM8/DldirUqMqmisEo6ngzrJH4/1eShliUXa1d5glzu9HB
6mIlBML/PpwMBazb2I4WEhz3+qB9BPwL8BaSM11+XKgFeEpqnv6A3ItehAMduDIxZpKuUv9Kan71
g/yVtMWl/aVGwShaJZzNeh7lbMt6UPFxYU7PHNLhne5z9YEaXQZLG418TOIghpOnbv/Mx0Yb0t5D
0gvvBAxyGLsR88txEb+LGzNI3oBnCF+vWsnDHRkuKrW2aLNijPy3Sw3oGp2NAKupkyxays0JYsi6
oPP+FOVZdq7hV9+7I1nSPrnutImNgrY1vi70vN8WDMyIQbn/Smq+4wdFFv7bjy/WkJPxx7ueddnW
Z/AkKDcI0LAm7R300BvDBlNHQge3EwCGma6fUSXYSYXxAGZXqCKmRb+kYdTzU/ZqafBNKJgOeHh6
Olje0vuV4OLBXKqYx+1As2i/hfJegLXLgZG9b/l2YJbraFOntpVRhjWi/Rypn3JVPrOUJInxZg6R
XJrW/yvch6K99g6bN8Tq2nEZWtAJtHx2djXM5jvr5XKXSrMSt3jRXQwwR3j2Jz3Of4L5xMShvwYo
qEh+hN47Ht9KBZZRP/HbO8QXvNjiI6WD5nGfDIWEJvPGdk0iPFfqx4+vbCyS2zEXFGOW8gYSvAm2
S6V2Jdmu9sy0msw2Iun/0vYCnOMT8wCAsPFD6ymKF18fdrs7Td2f2Q6mHKVHllCNGUGRClKWcS2Y
93VUP17pXs9nNbWtreQZhUZyadJU/7LP4jS/Jdp055sDln6YKQoWOqwI+5V5giZ4UovkFp6Rs9Z9
6NOCGWZtZgxz7i9vFQM4QFLtm2od7UFPN4yJt49mJl27ey7I51zMzfowkSMWCKkiR9HUTtD3NdDx
56PwjlJOOgV20lunjypGsru17krLzBU85yVQJYIhR60izWe8beAAwmXNiPlipzLm1pQxTbdVCuD6
83bMok2ErNTHXlNp60M/GUK1hc6umvWE0Em31Brg8mu26Pt3CEDY7FbYVWRhqdMuPd1zS03GAdZS
Vj6XMvQJxcoQJJTfHNjeDr1FtX/XlhN/9HEDBx1Rz3nbGHge6bN2+gykQjFsrFHpZPU5Ld2nWeV2
+SyI1Lp1Qy2sZZdHDIpJGmYdlHDuJTjRtlSG0dEDYtlBh0gpXOOtuOYJscpVqRB5yp/3EIEzRFsh
PrJ8at7EvgOTRbfCz8fXHFsh5fAHfN4JGQa1h9T2wepsN6umtQY/fN+68EYouJbuELe14BX1EkhO
3YA7EYYPgG+xhGKQ42MgGTfynxAORXf/Jrq2kf0o8yPTao9GYtQURq9bzuJ0aH7PkgW6Za704WQW
7tMHWcWmeu3pCji3UuuemYUrTr+1LUK9brY2uJGADA9mdbwuCf42505LjqmhQUsBQXR+mxoqY6E8
ntjrq8FAqK4TdZ5xz7ss7BPlcbAMHaqvAWIOhRv1OrbgctUyZ1uMtjZApcIwAGhwQ6tADu1yafNV
KKNzY6Qq0rBcu7g1lY+LALmlRKdjHLFebMJxGW1+zF2+vARijruv/X8bUu8pP1CHUC69T+vObFQn
o6GbAN9OJa1SCwjKpVDe3IPlJ/FO/p5BsBoo3kWQnL+oQHpG67/KqgDh75zkLUqQgzLOns+JUZ78
1pY4Utz9a/ypluXhGDlIrE850a5UTMO7PJ+vu/OBhPNPZh5d3px5gm3cEKGio5kMtQ+qtBnvA9Yp
1IBWVzbO3GkXVv6HdrprbFO4MBEPcDq/nljqMchWjvN0APq7l2FKXyVeqw6mnKBxZ3iQnk6+ysil
xyENCf/zKYq9vN8XXKNvMXk0ThVlJyPOjKbptdCAX82y7flevb6OaE0iADsfmpntGBvQiBf1gh+k
79W8WbT0qjpgvePLU42XVmajVtFrs3xOyO0KtCXB1yzsflqS4vNGDnx4pBnFEQMxKbT9rFlvxqrW
kALI4s5En3zGXl7Iq+bjyJnDjnHuJTVg5gIALuf1s5ESFC4k+LjBrQMy0eATGhjLQocXrzQeGhae
0WzZPnAg3lJkwpNH2uuEAS/nsqsdoaOgIXEbCxLCeM70lKcxFVu1lxXrftJ1aiHFVz+m1Q+O7TnE
I1VT5estsxl08Sl7GTaNbo2sMV4YTik+V0rxp18tsPJ/GzE7WYbn3mh942oAtYU+vTcLS1FmZo1o
D/3yD6kNhPDvNBc/i6WoKTCutt211ybofnUvPn6DfuujjF2St6qfiZSAtxNn5oxWHISvJ8s/vaTU
ZAokGjRuWuCqBPA3Se7tUauC42L92DMH1o0dz6HB4ru2ZXcW536hrnnCxd3yEoq7sgV1nfT4XoG1
yrD4sa7kvcLC5U4iO3H+fYVMeIqASVqr1ddlG5AI8x1XW/hLUYCzW5Uva7CmORm+ERyp4lDGY9Rh
zIHTS7HZ/GBeLgj7G2RejgxivRl0jejcfykWc1SExR8dG4eNCyCwj5UwxyUS7YDafDkEcWZTbQ9S
gj0WKANIJNbeU4vRN9z/DiLsmqtzxfsjKu9/rPEiCe/TAf/45iREHFHHaZow5TAE5LmDn1tvYPKg
4wonRRX+v6rI/KZ0/FrhL1K5PuEN573y2g41CeFvS8tzkMParGxQvosiSCjfr09kxJaA+RTCnC9M
2sGGULXlSIJ6WmCC2sd9H+pPmCCpzdjsy/bijqbMxZlpJFkqYgzBSTJ04PAUnt5qKUPHs2hbrjuu
2X6o5l5aZvUQ/j6PUmAx6bcDRalOb47kkXZEbiadmvnz259pLHJHkzu+E8UF8KLKuBENMJugzj08
kg/UMYE5XJ4bQQdteVxBoleeNbVccWsP118g0DSRyVL8gBRFz749eIAVwDxN+PWJcIl7UZOVElya
7d1DkXYv8dWX2RP6+Z20KYgcKlLywnt/xgaVb/Hdb/mhsxurfFd4QxTN7h8keZfETnM/AjsJtgu8
3iZp+iqEP4gxppHxuN2v62r7qNAVYWbvYs6i836zhSrnnkzpP7r+KXJRGqxENgVBexm4qAR+oBdx
VkG+bWm8h8lURa06ag+aL9d34PCN3bLwDWIMs3ngCsRIR19NcgIO2u4gcPW5IOeBxuMlyljyRHNv
n4m8pCzLuEv80xJ5o/vApnHkZQI57VJalj73N0AOx4Bb65vmW4myc6YGnDIsfLSyCGaUkxnEVgcx
8SteyJMxe9taq+9WteRJ0RFsy6QUcf08eoiIaTMcqYA7KGUS3KUoVcxfUUNmv+/CREoikZZTG9jK
UDFyT5+7DawhGi6hU3RgMTkhXnWjH8rtfR1alETtsIWJmnRgpGmlnXc6Fj4yx4gBHfs+M7yvmzdH
CrRyOgsS3moV4OLl/tB6uYmMUn8U8hPutk037rKEhZ0sYHfvppQKM9v+/K/bjVsA3XjsILYNWgMo
3fv212Hcgo+oJMZhJkbOeN/JSaoARv2Jrsonxvld2CI/4q18qiv36BEsf2JrkofPXWUeZjvFpHh3
IYv06/8MtRKjglRvrO4mEJ3hg+Jp7LRLiSgARjZJyyyU6gfzAyNVsMrI/FFg5xYxj4LCaxDbdkF4
oNCqDmzEeqPAyZ1+XOur+qFlrFodZo3FgNrkM0yMSx/VVycZAs+9VqG5DTgbOYMuc8ob2PHrkGET
paF1Q4l10jGYYPJXO/XAZDavW5HxK8UjECKLVa1+7Du0gjLQxmGRVCq2eD+BhJy5oEyLvOuocDMZ
ODIePke4dGBcsjRiX0/GzoXqvKf2I+9IBUMdu1ykaWh5fKgOoXvxJYaI3fzwjZXzMh10Uqp6aB8n
s8T1dMBSS9pnh1icakmEKOYmXS7FBrvRjCNACLztGBm20B1aO9ye5OtnHuFmoZPZmxCPVjJPPsVs
FM38uu8+IpIJrsiEWUQtKUHiSL8wQa/if6oK1RI42gYnh/m9YgFoyPKu+7rIV43JaQNiWpmk5dyx
xnCd58yO4YE7LhDTF7qvxheOM+1gkhwGjrmoKnGyG3a+c2hF1eLQz0f+4B1wg5L0sCCfAxDUHZul
+WVTuGCUoXyeH98nJ1OX8xz1m862eG2j+VjaywV2ymRrjGbFKI8m7id9GJ8CvDItWLpLGO7HUQ9g
DVhTi9v1fBI4uKXr0lrxmOMVkpE0x8XcMxmd8aMmX3NuqqT/p36lfbLiycUoAW08cMw8lE2R/x8f
IyBVW0IZEPiO5WBkLFP7ytuYMD7YKfb2F14yXl/04SlL9j4MKaxpThobrL1K87jWJkVB8kV2Uyif
vjAHRWFpGUuLZ+zkyshH9yphuC3zi4VROA1RiGzGjbtMNwgILfHGZUUbESJ19HxNdfh5zUzTybUP
9JhpU0s/D8A4VI6F+CJp6Lpa8+rq2aV7zg3wukP37rX8GBdXNWhUHrSs4NkxV3iazOE9VLLRojTY
pCI9A7OE+DiynZ9XYmmIw+rCRHfLwy4FIFNRezfmogD3nWm8ZQJkfm9Tw7JrEE7w6U60Kb0RU5z0
U0o+jS45i2/Er2gpFAcolmqGfHluTMUGW3H3GInNE16geQuNq66JSn+3cfHd0zeB0mtwaCiAktEt
ULY9v23RsHN1vbRoOWwWkUrOXQSC75KWxZR4nMEnxTKaNjP2eia21bV9y86q7IVzAL4NEOEOK5bT
gspDXiz9zy1BYuflmHnLgIcZyup4+2QJt+Iy+ZjvuKbpMWhofBvyP1sHfQZA/yZmyvk4l8pontql
r4I1Cl7ogK+6D8x0Jz0TnnlsIEXut0jKG3tV7v29s3tszDGgzCM2VUkSXWdoX0RTqTd6YPCQGIZ8
dK7xJs2l018+vCSWqH/G7n60pp0HCbgDg0OmFEsAttxEoHL7Ka7Glyhqgxs9AaDLv2iy7aF4POXM
AWwFzuVM7fhzGGOTXAlLTIciYO/OokYoqKqLZ44GS/uzxigSaDgPFflVc0fdF3lTvbVifIgoeW/W
hNhTzontnqNFrUUDYsBvFcgDhQrIfLG2zZoiTn00qDWLdV++3xphvYlySIhALzumLVs6Sd/4JdEQ
cj2M7JqsltEVC0zyHQIaXJHxC3j/tssLoT1Wo7BT+yagffJZO264CMOlsYIB3slquemNfkjNtcOe
5ZpBDiJcgiMrqnFgkCf8Q9D/xLdW76J2bQREILO53YqjjG/CpYWtEy8+ndSZKmIIZarhcTdPFKOY
vu1LdbS3EK2Zm7NDHDnPHH3EGe7+FRoZuqv31PhcVfM82ro3wsHUxie4DEIj00CvqtDIwWTGoNDu
cQXkYpKTZj6ROo6/QQBtUjsTTd//Xxh2RcNAPUxGAtqY2YIAhyqPm9qsUrf5QtJrutZsTVQi65l/
qqRPpfxJljeUPwMYrXIZTNA1+46DguG0INK61viNjyNdC7fXcmxfGR70lvhZEhaiJgeS20rK2TFv
WmxebX0E/N/vBHgvlbZhHaEypqzRD6wbhRdQFW92cpGi/fqEd8bA1gAsq2vcdb1vb2QeZ0MtJ9Im
Ef5lt0UhvEQj076+7ZImv4rEV/qSi1hvMiGrEvhh2JQQyHESfoJThPaftDMul54o0bmFJtYWUkvd
kpqdaUDPZquMDrn58zd/hrjbkpHYlxbg0bWV4z3e2Hs+lDEiBaXUjNWCSkemy3K1EdyvjMmfPAuo
Jmgz/HeNcOUX6/c2ORXkw+h0l+RGa9mqrO58WhDU6+J7lKPRAGUb6LH9WO5cimL05KtL2f+du9id
yYzFzDx30HP2Ci6gLMt5t4HaJ9A81OyVmosUJ/6Ch8mH3GG5PhSgGAK5qIyHxA7sKEJNevkIiKfU
ejryaPH/QPCwBdmUo+kf49BjGvtQwj/DEVHxpr8z112CrY14ARw4to0gStk+Yxi4EPsJ9rJNXLRG
pW59iFIUbQOfWgMfn3cb23MzRjpTyJqtZW3m4vXhF1d+4amAr8VRnSe0YGwNTPUpz1ghdwTSqAKF
7QfJTlxbXWZgbIPYAuAjAMW594Hto4yjByZ7AFteWcbpTEEp3SlhNPevdo291yUwz7i331fEFEax
kFpG5zWgYtA4Wt2aPR+r5eKgUiT1L937+h/ai3ZMlFtHEfERvqvpud5aC+iudlsGrLeYmz7SE8bA
r1DldZYB1v6fXhzjgiyz6n8iJHpUL8tAXTzf8wLy3vd9y18f3PNpU2rPPMWdfNaZDQhqJRKUibXY
49sY0h+bD6DoRq+32tB/2X7niJ2Uy36wgNhoXjLAigwP8Gfu7BNDOP6SR66JsOoMKk5BqUUWSTxf
j+4M7Q1P1Vd+WcQvXQ50jg/aiKeX43yASd+BAp9wkUoFKTRvWUC0yL95yMBMX1ROUVLvpECzd45A
dInwTAiAs3FcTiZSW9BY2b18FkgtPa0KXBD7+ECRTq20CE7qtzORfOwTK0DNtBlo4BQhMV8N3nZS
scSOwcPH0t0llmPKf/WCao7pJMtHL2Xln7XpBAZmEnQygBR+zZovX+HDXrPvA+wRxYcFkoXn1XpF
/P9y0VeHVVS/bibWi0kzOsVeWkhqgnncSrTYEkhtV60/+tSRFPRcNI0PcFzMwQVNcRvEXcwfZgKf
L1vDc84z1GirnsZjPVdKMh7KeQ8Am4SeBm/QwSgAkjw/XdlKwjp6e0YbSgTB3U57KimnUqU/RXDd
ij1+y8kvsByZjY3eTwi9LL2XTxZ9u5pVhilspTcOYTWS66RESSDLpgPH5pusMcwowwMjvNqDoVQ8
KrfwtoJqPdasW/T5QvN3k+IVXUY/+bWnar+17oY62eUGPaNW1hgL7iiloMVVQySFvfE6tttUFQrR
RlcNdM3aBgdKsWzdaAu6Di1UD9ExfC6oD/7AWDoCU80svQIROV0uSCtPw2NXoQi6i/vBHv2/MYox
lkp8AnQlztygK9WdXzs7OWurHEAO2hEH1QNvpdPV1xPSpdN+G4ow837gEnQKnqc82alb2A0Mj0NI
kYdNZUgf17aNEcin7/TSvDUWWOOj70nslSbB7gWlS7mTNvC72M+eeR+FmWBDpM4942NcP60RNyPB
N1zBqoBxMQjHwVCDxkPZHD+N1aLu4v9wWokttnGvTGft7CUQCE1146GmSsFBKOiGmhrab2n2rqbM
wQdHwdcwLj4aVAJcYySBD3foO2g7EaKe0Dj0lpksWozwaEVpnnxOucJ0y3b9L5c2a4auLWL/Xgcl
2tjPUE/fEA1/RuPECM21W2CMFcpHFKvQGgkBNmqkwMF6G0+VgJUx8EKZQSEtCpfh/cpC/kYAHJjm
dPdbfW9Mlkdxua0rXnOfR73l8u6aKtn22LC6ClaFiuGlPaziqQ2WVKZSUR7MgA/MT+Husqft1VUM
10RZXJk04Ivw9o6oEMjeBoDhtUifRCFkwV9n2jMRpa+35Rr9NWJDVBeI3UYnGHMbuIDv4yzdhRjY
qP5LR5f2dQxMRRXNt0StaUrRkHWtRhH2fWTeL2sC77N74nzKGT/RwWYMUM4+LQw8U38aL22IuPGR
JLt7qDF5aju33R6H4FaLY2V8nEnF9CM8TIjnUrBKMhNgXZIZJb0hmkvMGb2m5dh7HkuoYzDCv5rh
GuDhVS/Y3q/PpooCP8avbGtxz/Pt34np6eopxFxAuOHy6/ExsHGbj2x8K6rbI41bZAsV8aF7F3Uw
4zo5eo7vHrnS4WCRhsX5obHmFe2WKX0eNIJe9iva3VzQVRug7ZroeWQABSgfikn4bq/aLknUo85W
1/vFfKld8FLzo2gMXn5mnaWSZaBjNYNX0GvrHY2SvQVwK9r6HvGlmIcIeMUHqKl1q1pfsMeFp2Lg
LYeRY+nQ9a9cD38KyHSAz5BfPQgf+sW0QU/PUyNHXfgyDw4+SDAx3o3ki5ilBbplh0Sw2FS0KGXo
StH1kEDFX/FiPuwlH+EY9Ki99G8r74/X8PB0/lmlxLr0k1A1wZPoyq2uLGQXlIxMNDp5SO2SaqvR
vXysFv3EEpGnzSGdaaAVuqEJh5HB24T/7/6x5sA6ex8JCuxQ9+ubcXA0EkPChjj1OL8xxprnvluU
ETXZusKyC8ceHwghEL2Jix4Qr8ex7kPm7co7v4BwI2YLDk/2HhiB+77akLnN4hj2nfixeTCnz8hA
0MWQlkBLKBNERL2NF9R3DP+o6H3O5GoMKmDVz6lsk05/vVnfxmiZ4PYvjBuYQd5P6e76OYhf557z
CDSRLLd0UhqhIBa7np4Y0PWyLxS7cJ1/VjOlaPTKJ2BoLD3XCwogRWYGdW83Q4+RQkLIT/rFwzx9
YFUp3cNrt5uHW8It9wZpDvuQyYNi3FK/QFtwEN+vdhEYc+lujEx8XzMldG9s7ONWtDo7tVWv5Hiv
xf+T/hSJIagI1TG3yGaTye2ciicdjH87NsOhKs5jgs7QC4sRn7+O4VO+tk7qeXidxkbB0YA5Znrz
a8VI3+eAErQXkTAeSOqaznE72Jyhb4T8svXA4xgYrtCE7vmeMgewqQhUINGICB9M1iCJgv1K2Y1h
dHZYZsvkGjiztrm4bLR0xWCnKGe94It9ZR3Nnzru0IrmQDu2I9uWhAsuN9vKjanYARaJGbF/QG1h
uQ1RirtKJVjezLDNV2gBbOy5Ib20LyD1II5ShUwamUMU0UZ1EePd0J3qZPYdZ8++JrPDDeXbnSSX
ZR6iMw2kJvqe9wZC7GLvr+XrBOZg+0deum+P0gXDsXW5LSXSg/ZzDkBaYoUfPWLnesoB7DPGo9Bw
/ycUElxJj3BZeNEDrMN9MSDmAqtzJK1dqQ73kCE/nK5C/nL05YmbGdKIaPQwdFmrXPzNSfsgeDqe
NT1VvAoSc5st5orL2QYRp9PVvdKW2RwUDZUBFo0KdGnrf0MJsx2yuuVMaGD4YuCHOYD6JPhP26zM
eCWDBlmQjqnxbJmEepbRRxM2K3aMqirBSbusch8W5fuiV9jU1iVcNapnBarCbJHs+iE0Auksikpi
QcqhwPTALPngP/7BxQyAGGaswCiJ+5aD0Ihiiyo8oxJyr9JKBUEG4Eu/+i4YgJQfBE59fuRmtGmh
fKsH7H8m8dxKIxhQEqNNbfczUurJJ25YoAmpg+1QHL6Z+KFUfPyLmJ38TdtPfD/ON8L+FTRn3sl9
k2XQvxa0AA3bMXOb34tQLS/pAAoKo2dJnn594ii0CRUGBA24UVQ5NmCZDpq0wyAKD2a/eVyWUA/8
K7MZ0GCgGPQVrWK40rukHyvpdoPunxEXG1uuubCszIeQaV3QGt3r0zzosw1/LfuE8LNbVokqJY1S
IKd6i+/6Kz85ohq8x8pJ+qDMb/ehbhMsRPdCEp/UVekl+F4k4FXDPm/TiRYZ8K+VRu71Ns5nsq/D
defQ6a5snMLLldgXxtpP7l4ELROxEGyyqI/pZPvo3BRT2QP+kElAPUAcmyBQTPWzuA8M7X+yu6sK
V21+A6jwJc/cUHuLrG1Lx4nO5LG3DGmpnwDZbpuZqW7m2X4mtlHEIlJjIw0qQFoJPG1pGFtI2KwY
j54uDxZyw96Jyhq6LkDAdkpyZ54n9dvNRapMv3QgqHbRB/fGjf37Ud7Fg2+i63Pzhinl0xsC3ogy
e0x5g1e3fe/A0dYPGjcaHQtBnmOH8KuIvM6eRBqYDAFtwvpY1O7VqCbUhS0XrjM8VM0PXOwTO8fs
e+NGd9/eSSTFSxZaSZa+DrVbmm1VcLAkI7aWB8cZNoT8zAaWJsYG75E0TvQ6warOtMkGTgixhPE0
hjG8XnYVq0uHFYIjvrLxoF/VB+b3PToi10sN0C9JKaSnDsf1RoPzTKCTfrbeePkoyhTP0HNjLkR0
eF9/qTtEdwK6f3Xn1rkhbj1E05r6UV7CUVd+VT23szI6e4bOOrhqyUhjpmg5m8TRlj2iinnmYB1w
58l4JtDVe2LZMu38zokM3UfL4cxUDEDnRHmLqLpkxLehoqEvIgzCgA8739RiWEoCbY1qGR16dtL1
U5K8g1NqfN1qDMuc7fbItV2FbJoFGmum0iOYXehckwXc4q7Io75XQcseJzf2zlYn8zvpxQdS2zPJ
91F1YYFpSBkPxCcZ8hkZ8sv8DbHtcAaNytqfl0fqCbB9w16nFzvfBWgwL7Ebxhtq9EZ+hN0Mp74w
l+Ac4/Oo8B0VVXDafXQpNRj4kUjN6bg9HJHjY4k2ZuaSEAv+FLP28pUJlcrlLqAR5OuvrlstwDNS
7J/gGvJvZGDXkVtaBqaRDImrxLAgUdyIaDwYUoaGzeUGbQCTx4av2LS9UOMOJA0EBnyGMGjqC6gD
JlvrOBdiqhoatpTO06QrUyfB6Gfv8Skx0lTrOP2W9z25t/diNN42OCVSpVzfVGPC29I3ONQAA9oq
GtINI9jzqPj0caMIlaxwANG+pNiLlDFuJGOrx9MCYp8Qwy9OGNtBguBF3MK7isy5KsbWd/SiMll0
iRQIpz8aiYTfh0AJymSUTFQWgWHsqaorw/FnScLAq8uk1JlWkVTJGSX+3OCpvZsGdK+9CgETWxab
53Airpb0I0iPmWZNoZKK2KHzItumvASMHM9ekvvKowiKRfEc7QdSUFF6doxAPF0WJ0l8R1eiFrUX
s1CxfXweNfHx7N1VJX8WivHwmbPgMaJ4EMbxZD6JLvd88NUw4RTca4sHJAdfQ1KcLTCWqpYBgKDA
bZ8+FcrL5xxHxoqGvlrvPw55j7DI+y5ZL1NwUZnk/6Ano9Y4sF8/fDBHd1mLyxsx4WmG8YaIW1i2
eYaiK9kgujWgoH6DitnSxXW6BHEwXS9PFDyLGuaRNDVvFzO/tSHouiJBeq14e0Aak7SuPGdFC7Vx
M8ukj9ueKyS2TijGyELKIhGbCIS5+gIan9jpoOYAEQmIWJ8ZytP0EkdYPnbG3KZOTaE4h0lna0gi
A5GGa/s/NxEN7VbR6Y2NhRkQjsL/ce+Ig6BIyqp5UWBgjFEHzxkE33vvPdhfKko+OWMpBRHVylMN
KGGB5JATQxTxD/57nlDBa/ZFCSuzCgFwcTHdIxjY2h5OxB0QKO7vJi2sb5bJX5lBx2Bp8wgCwiR2
IWMmp3O3MIHsTY6OLj+kMofczikC207s5QhQk2f3gk7b0hOT5JDqnx5n0Df7IVlMXNjCot3CTzgu
8EN7EkuojnWGC2m98h1j+1G9ENuREuZuLQlnfQ5mtvpDxrqoGgcnuRXg5mwNyBhAEFp5NaOITsiO
4WnPg6dMBAH7jieKKd9iy+dRMta8q8zY40oRZq/akhZWYXt92vSZVYqmdpwzymT2tXCZ40EVixMn
2K1jJiAW3HetJjPYIP010R5N17pGoenM66KRA+gJ5f9nUjdK+2Cbcau2O0ohYW15mNJ7ByRs+Au4
8fduIGdJVbaj3odLkBEQa7vUV5C0CZAg2lgpRM5CBJCgTVJOw7KL6zZa8j91zXhHSQ5BcDPi0zhS
SAvf6fYqIeE96UzumcSmlEdQcBlQaST8Y4tmdjjZyyUwDJE0cKtLHxLVOYJlVADWqnBGNHNm+oxL
NGwdc9LMkTG1mU+bs6kSIr/ST2wX1T1LaRoq7vlU/pPG3IDJR1taZMNwaUnZTxDWKzg2rN7iN0HZ
26YTDlhxkE+zugTQWuVIL/kQdbaKOIW9yBuPi680zKIMh9qfSeXuLW/l7giYdCrSdtO50d65sE3i
VOlWK9X53pbsHm0yUr045SFjnH6qoH3D6wKwO7xCASH51P2CL1AUNNYV3n/0jJ07/jap8fZvkI+6
28nXZ6nLpiNLCzArN3Z87cE05+Gb7fE9g6+E5Qu1GvVVDxFM3ql9BdSP/81P7I3dQvS956q5H8nv
vRcadtFoqrx80pa7PAzrHVg=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
