// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9616)
`pragma protect data_block
QalXi80+GoYE00eUMkFP3fqnmg/7bgm8q3uk+OTRq7T3ZJRlWfJxZ/4Ohg6lNp5q4BWuYxbSKL2H
h6iSR5ZjFQQZa3BBxRB+gs4KN5oa8HKANW7f8NQUcjw/K+HNbYVhcOUvC8hNPVXKLVx31w6Lpr+p
l7ukbwDJwZiRwTaUdpUJx/RgxBln6oW3qz+P/JDVXhr5Fr0fqgSxu0VPv/l1DLBCE4AedR7ss7VZ
OWQRAzjvrH5pCdNzcXKpNZs5O3oWQp9krvCQDJ3q/RKVneuEaVbJ0onZGvm3NSTxt3sqjCMJ8YE3
UH2dFbIHCZ5PTngpRtEMYcaqB3QYjZCrmb46qZYXCOGEfcAAWsNyMVxwicH2vg4EmrmGva95nZxU
pnADwfxicuGHk3Amgmktbzwd+YEwhCAOmhTbRn8jXUIN9qkdvRBLm383zrI2PNtXwG93TF/KlHWj
bsE8okPp/eVs/yyKXx0YpqKh1Isj7lXEYr9PhRg4kvA4acDkAVOq9qej8NF+0K1HT75WSoX7PeUH
9W44k6AlURxz5Qkweev/HjeepMA2wGvK6FOCLVf9+m/ZuozEPtOVO61oTiOQjedTc3fbWJ93kD3h
uDPcgrWcMJf6HFOG6gZTZCYTNoMXh6Q63zwkYp6RPmdyeue+V+J77K6H6vlPIkGsLeuueyxu374l
c9uS1xxAK+wAXedEzTZE8PTaZiNtqQBtoKkA35OcRWDrukN5LQaAPrL3mgDKcS4aJX7nBBQp7qMU
cl1d7FZRLbqfGpBm5Nv2WQNUcRXlWxUWuTG+hLogTkXMcnNIVmve5U6vNe9pt6f8x75lBxmZzUai
7WWHlvacih+RuCrnTv+SwtDWtfbUvXHEjgcIbu6/JvT2PuVzFAp4LtFibetDWCZ0SGDtZ5W8QZth
IcUAIXtOvdNm2Skb3C22JyF/G+se6jQ6RienZ+3wbZLzllwXCzHH0Dvth8mQDmSi1lopXzB7dcyY
mpCZBVs2HZTRXrEsc5sk8nq+j6S+CEk0R1EGu9dtEb/j4VbNprYVhFDDbPzrGPruBLnW/I5KQRX5
bwfOyjxKHBc+56PjadzwjFrlkmPz7aw4l2AfUX1tEC677eKxV896E93LWy+OYJUyEezFbwj8olmP
woNMgaviYJ+MCGuog1OVxZ4tGHrx2XQTTV/aG3MIPdaaXC8OK0kzQQ86R3iN7VBq6Gui+fAukAJq
SwKSyqjQPlTVzaBD7aSXKkYAKClFhGqezVRCj83uWovro50bQpUyc+u1wgD7RA3DaC3XLvsZfELI
EAYaM13K+4l3j2MtPc5AzdgJQ2+3VfXBDBa+mZaOHq+2aCjrvHcooG0mWYU33vD9Y5PAy8JzGJKl
kjBDtw1aI6uEmEN4tbVC2WC34l8pJC8ABN5gBoFYCyhqu9uwcO+thVtlFat8pGC/481pxvVOWJ46
aU/xbvjkRjotXuDzWHPSIjhdkZnPTDM9sadqF9Sgi/+aiq50mqIqHJksoiJnl8EhZbGgPxRNwpYi
c7kD/6KUqpPN2YyTAnn/59Y9WGmKeJU4al8iJmFYn2cvEluRC/mdXLSF/VTB5aqqXQiZ+pVO0dTM
Az6YZoAx0cGZAXom9ee1L02QXppsTYz9Rm5ZA29ScH6NrdgbKBdwKAkJt3W21BvVniuY8RpAFzQ7
7VWyUXsFFxPPm3jOdgLnYEwgowqEVG/WSENyAHDLLXaBKWhi8UYwiQlRso+d/3oxz9Isiyikrr2j
70bXFz19PYWlYSiSsjId4ku2D6qSVYWjGO78jqzFVrEt7zSX9aR/J+PGU6ZiEtxnc0uxP1y3dM7+
yqixca8u1iwoL81Mr+wFxSTiKx8YZhILVInetnXMUL/oyWTwHloKq7CXa+GwI5d1oLO6kNRkN1ZB
jS8KZmBYcG17FphQjcznNlsVTOY1z/9Ew67bkPROXju/w7trwpsd7udPC7hwxhkUNimvq7gtG6Ok
1XBj7SILZk2tPKYTRhBDuJSXo4o1lMX8/KT55nHfJXIGEVcD/9s7tXK+vM8rMcyC803qE66dXvXq
V6q3XgnfopqM4n0HqRWKxKLCcqn56BX746WTe8HIDZD8Y3+/SPWqdDwcGln7ERWHJwHpTdgGYrgV
SUbIsVkQ3rm4S3ajrKXZnsXeLA6WUgcPUq1iqq9POWYQnkKAAOn5iHoprc8nWUDOVDSKBF5/REyc
Nh+e6HL/U8aiI4cgj0ZP9B6E7veR7T32guepPW/zcOo2/Jnyma+0VFVfafPTk4a/U5u0fbeALTsj
b4nf6kniAklBHhd0bCumhY+XuZB4VNBqzsODWL6Y+K9cnt3vC3ep9WGAV0HNksMz2mVuoGfph0bx
zrLDDXqkgbiSqBXFn6G2ycVdKzTEYVaOdhlE9eajJHVuMDvlqrtrX6HKmkTQYKZn/hrq6FbycbF7
eUu5cLVeLYAS39TIkZIAOGdsu5jruGTCuKh0bpK2nAI51o/9IX9cI9Suyba162GKSSzP0i5hoD1e
i/wKkVWRc1INrKVlOKUDPNM7fEbLEYhepBZyibfrJkqK4eT/sv9GoM6zh2e5J/MUq5I9IGnIX/CP
hX47E2+KQaxEuv9B5Pu+EFXtrrQnJ8OWHR0ivbROC8hBtTLAXTI9NbWo1lXVxcoUiY0k2V5BU1y2
vipyCP1C22glWU9KDOz5ga5KD9wbZh18gZ4+w7K7FR0heKJ4crHy+8hhKqVHsVqvlPwdMLMyCK9r
Z1whJw8IuKwLac+EP3VYIA9uyandOOotW+HtaoMUENV40U5s7S96R/TIwdhs6G4mEFGtuR7MEpRy
U/i7u8sCjJ7/MxMK0saqMAyy78oz7JNW9v6E0FYr8OGKmt+cZWqdDsR1ayOyRiLBp8A3znis8Fhz
1g3tJgjxKeXZOgjQFM3S6BK81ZeHA3juHjI68TvSJGmVDeYc0WV0jd3CHxICs3MC7p89QFfxPlBZ
oJ1pjmyQ3ZpRrhilEM5btBwgLa04Oh9saQGQkcyHCeX//Emt1RqaioIF5pAlwn2ITGQtYIiQA42K
veIuZAkypMs8LeSNmshWJJz7i8l5KZM6ixW3hRe67f68M/5ykd5ttPt84Tckdx6j/ptgNv0yuIyz
NzLv9Xch5xthw+HvMRdr8t0z0BPhcdBefWbS5tczmDk8ylAfx85gXgomNoSvIJtf2WIXlYKJXa7J
a23Dvzf9LBVOKYvYiJlA0B64ZS5EX9ObGoZzoyv+AX+4Kf7PFeZ3F6UdKQCJjbzNWuilBM4UhTlV
CnVpZ4RmA9FIriBm8D0q/kaucFip4A1Q65EBqcQfyCD1gAFCjR3W2VBFsq/vnX6BDMHLoW74ir0K
vqOh0dhl0foEsk/NN0ukBz3RLgRebrRxzPxkwLEKc3nXKwbji/Wn+H5vNj0bt+myZm0aKsG9CPya
qemdDamlcNIpygmlUMcr53lL6AyBh5AwpL1AAGkvLVPQQfq5F7+5k/XPcDe3KRcnE4XW3Iu12fVV
kuOtDkrlJsxJjMBTtTv4IeYbAS63g/P+m8FOBA1ZvA0CZ0eDI/WT+dLtHLWtK29LHEbpJRVMGOEr
tQIlycx7QEsNnmJbfK0QqLw6LsrbrpAs6jq2TAewImnjdHizUJboePpn/SNmizMQT0jwKfWiSE2j
NZqiNDubSbiZvLppz2sWokrbF1TAfc/xq17gzxFgjJks7JiQR2xqYMT1vrbWmUVqD8sAGbwiWTKc
8btzz6y0BoxubMSKC8ci1MJjkqayrYQFBj3F4+Y2e2HwiWusNFvJUM9oQXozrx5hKFeT2G7jnJmC
kgRMuA6Td1H0Y5BWtow++OzHCuHiQmzb0u4nM2JQq8EJbz6Wxh2jxkD5a2baiCiEQjFJnfAkuKRV
tyLlCrOS2r8QERMMrf1Wgouv2srozvqfTPIcye5T92jHilRcgasb8WSTHoc5LIddKn4X666qy5HB
XlfZVlX6YBwVtuLZ5jUZ4HDiBkBzk3FdGELQsvTJU0gNrqPWOi8k18K7MGBdIPSF60fFJtbh90/B
dqxBdpAWPge+iGB8q72mZit7cZTAMVgw5BsZTBGu+t6iWK1oG6ygmfdZfn9OLzJU/63RpF3mx78g
DTkvm5YVoOQMJM6JqSB9/29AjLLVsk5J9b20hHzOz5NhPQYvL5bOUKp/iCr2K1HLvKGR2aCnun/p
mYPamnyBYhtfEUUzV+7hhc/+v5Ps3vlyE2UuTt4XDgGjQdC8btrTepuk4UzCzQuX4uGblSGQe0DR
IGBV1hyd9dPkJqmOlVeddjrSwWyKwmSuIkyTK9uFu0jzhvQapOV81B/SrxRx6/dxh/qaAQnbsTVM
DZ6+1fWgWW1FBa207SqUA6VPIPzBlRpR1vDzH4XVaEwftORZjK8JP+bF6na4A3mV7TnRPgK0aEn/
a2UWVA9NcsLVV9bNTNW0ewVcXSPzRWQtFJT+QFoW9MWRlTwCOIEj24PAnhffdiTffI1FCiNAXr8u
xqaQe6SJPPWab6zwtG9QrjdAr/e2KPyqH5RHXTnm8dDv8npNt6sq3qL/1aMuC38H0jkcn0upKhqm
lFAKrlQhNEwl0395vhcAfX97I7qUOUAFHbSJWoowLHE17gZfNyx5B7X3nZXRlh/VO43ylF7zxAaq
wRMGczD4eXDHWSk59urnrdnZqgM5z2Sc1f8uqMMR23byNF3SmZZQ5SS0+IHrx0Bwrky0Id2/GxbR
QSKdQVr9NE52KghvT/kfgIvhRm4zDEOA136Xrf232ps2cn0esvTaGMAbD1Doejp2FHWIGXJDoOY5
LbwCogC2/L+SsQqv+cyG45T8VbqZdYwAVFS8Addmbi8ApP1GpkgwL5eqYuMxDhIFTLZcuyiU84BB
zTBDhbi2viLVVxeXk0Rvqk84cibTrB6s/85af7BfYdjSOSfvO6/evgfiDoYnlrXvhhD8rdc2K41d
1TJ+h/A2lNICk1lURGix+V9kQD3IKHuSd87i/MNcUFKiAiqDfjfNPUeLFfm6MkgX18fHQtkFvLKO
n7KmcYX+QLZZDaYyzk3RtA7xL/JniKg7EHGryVBZKLkxX1ywjn/We7iaZb546Mkpd/KrFvDq+sIk
UyVYGsEapjwkVRWDdobLW+CV5uXon0ZFzd4M9QZkd/Eoj9CIOOB6pN6/FEmXSIKO/DrBd4ji7Lg+
gIJdn2MVJMorogIzphiRmMXPeeQMlmyItKKKTzFzRWS6NsKI5m2ER4l9vyAt3DY4fQDiB6bjTp94
pc10afhtTzdff0ppxSuTTU+4nSBbAdow8F6eCYCBTriFn73Au2uRTexJ1KTOt04GK0xzMK7vsxk+
DOisrl4W4mpvM/OFRx/qcWEnwrgqGSdXSj0UgBTpe1XB4Lj290/2kuCfa7h7awzedvLnv8HFMn5u
E5Z6hM2t6zENsSkMKMBVIWt2v7L8oFTJxyaQBKUeyYIMioY4GDVt7YnyqsqTamHXoOEdke3saD5Z
RSevH/MBXdbJWPuCrdM6kBErpOA7Xtj6H5ed1M0b3JayOhTfYZz+aTdlG103TkT/ppn7Cd6Nib8z
Esj5JLHQO4SU6HWYTOWaquY7EyX8LSZ/wSFBjjERaXRGqslGAHHpOX2dGR5Uv+j9NVlz+TLvXK76
thNhnJZB2OjvMJXXuQRjRdNfreeQiVpT8dfxnC8O3XhGYBG1XO1G2pIDsZOVzIiQloTNF6nZt6SD
MYZ6rBONfo7sX2z4TcqzCJKrdKX1WTpMAsjZgrB8eSWzUpwuXrSOi+KUbUGhxEqptLevSasoxoJW
QlVXHnoXhi4gfWltRs39zWFzCG9lfV97Nd6N90z5wYlkzjnY8FoBmm4NBnWtGOhAG6kG+ua0nLnq
TFpankIdDRMvSEZKgRDoC3pM6/h2j/CdjPJEfT2p1dJ4mE48HU9wO7gQ0fuwvasc9f8lYm0niJO9
OGezfziA0ZE1x0vMdlTr+L+acg5kHZXfYkPaY18iYIea84YCARgm6q9ZhJ9OB3+orLvcPA8UQh+O
Wdq/ja0B9mg96nDTod+nm58DVNxRmfQr9yzjSKYCi6sv+H/Rb913dVebIs5mWRtcFmuOfGRM8Tn2
ZMQtMuHckP2GlcBGBbpEz1YugbiibNOJg2kfp8psUs/8keGiZeaa+yYhiO6EOSigJVBEpgFxH4Oq
8S9zuVuqruk982ZKQ5MQ6Xag/ctCjOKcU+ROEGKViyVGvI25CBGNFmkwRoj6L96k8ohXw9S2dmLT
fzVHSGY/ueS8SYOag6q4TUoBI3CDBtnluy0LMGtVjHJKzRg4UJ3y7tCs3+4P5wj7FbYYIdGDAvAw
E/i1GHuJELlcqQfkDlmYavjh4ktv1zYVflihIyKCLYRW1zs5U2f0fuHgIIhQrkRBPk8BVKtu8tNn
ZBAaUdpRMQ9IBKqCJj2GIF4kYiEZtexVt8R4GaacsbV6ithy38QY3V3VyHWDn4qTaIQnOHfpCNfk
HVLVDo98JVpFfInGeKLmcwLnDBfLxtQ4zZgXTjULHNGbqGhvGr3LsVPzZgzwO52+ESNKGYE47Zx5
4BoNBh6gBQjn+asvAT+9tCVWhxZXgMSL1jnph9KR7rbKmKLpweVAKeo+Xfi8Ubt4c3YMZseOjQT8
mcuOafOe9FClHJLamAk8SAtDvpxw6KquYfR1B8Mc5jL5crqJkfQVwaeNP4FF9B161KQbUo92Vidi
UQRkPRIObxpq/RCI4KEx05pwr8EEAcxWq47C6LfRsxgFiNsfXzlOaIodn0DtuTU7y/wlbcbCz2ma
Vw8uV4olPc/HfcYDCr/NJm7q2QR27fL/E5OGd3pFKGSESFu2w/LsAVFhSoctgEZdXMygphNbtbQ+
LDP9TUf8XKNDDy+0QcCq14a77n7hRIAiO4FWmLTau9Dpe4irw3cTARhAggu1F44UmvCyXUsnQrGo
RT13HSm+48yNo7mfvppp7XSLLDCY7lE+nlxKdF+P4UMt9AyQ4I92nAHPRfjnum8qinH4PJTdbRZf
qHouXz5LXRvcUoH5zwz5+UZ/jNVOVXPtdcmRmbF0SxFNHnpXI+6abH5xCmUeLcMkDdM/hbNxiGaY
lttvgAUA6lppMcb4b4bhGaS+9dhz39yTMB4Do7De4RBBACCBjeQmDbC5xvs0RPWje/hWmdFTA0v9
emsh2OHYp4VJNpOwdjf+tWP6XNJY3hXtKkgvWy2Sum2vH1nH96nXxGBM+ckfyUG1KrvdmStoaNrD
KPeFcNjK4Tg9cRjhvMtbBxZClN1oc+8p6X8V+EPOz3FSj8OmLxIIOwjDOLI7FcWszMkE7X9frTk1
H8vW9G/P/W50IRsuZ9cOh/maQ30DJM+MyJDE3lzyQ62UhVvjAJ0P76gVY41V3nQUEubYL2gJo+N8
nNErYrGm9f9ntu4D1XPxDLxDlhvqnBX0LzZA27ay/Q9/HRSDb8z/zWME/k6C42I1Iwr5RDndVfs6
mQ4YxpnoJgdQU6ZX5EXaaIm6P4hjT8dY7NzjMVDCsXNvmS9h7lXkW+ZzWVufIDjI35ViZeF/vN3o
PVHs+tfzCCKIUH29O3DBbB91AuT5aJi11rS2ER+7TjxqagT1L39/0BgNouxbqLugrrN9fqyBZLSH
vNSFID6pmlnaquQ36KGe9gPDFVCO8H8OTniLGjwPapSktreqlUPzD/EWLQ/1IYoRFJMy7muXjO3z
snld1DxTv1hc2BH61xMydrlK5w1hjBsWkOxq6gcsHBKVfBIygViVfVNoSX2MWr0wAzYEkYSUJHRh
Pw6Y35hUuEDr6g02iKHAmuzM0w8mBjK9fp10EIJ5Tg4rXpfCrv9Kzx/mYtoCXng7MuBvIHEZxxOf
4SUZGSWNp4YVsxrhtBswON5IaT5D946jFTshrI9ns9aEEBkAaCyFLg7Au76fqjQ6ci2Qx9cZD8rO
cwCTg8wffyss3gtiP+YbbwfUJ5n1dEIfw24hxsdLuJIHyqIua4piKvjZG0uAkR6ghrqKqgZJxIQ/
m0NbQRJaaXIBOwm8KcxdVuR8QGh7Qp6SXFcffyl8+7fXOhBZ9eff5m7R2cruN5xosRrtYudKdd0D
Qxi/7UkGMO1kCM/ktFlabgcf7Ghi88/U178pkhKRH093YnPMIxk+dm1mdtpuK/nRpDHXyjy75yJZ
SCf/xJdYXXOvIO+6X7mpKcGA8r6NAadqjBTl6HPu+kNsw5lwZJRRnq4/ZdgGktZy4L2lulPhKAGE
oOTVJBkwbgHVUDBkTNlWVBO0n56YiI478aWYafC8ilwIT7vg+lyZ8JRByWxeaZyLV4RmQIiMw2N0
DqBHstJEzKaTfhN6UqIIx4pF3wZl73wOeVfLWruesQL1UyU7KmtMbeGuhO6k2afIdCzo0WIKzZPz
cxLTq4IMYbjku550s7QnnRfvs40AoV0PDEV2wnBGvVh2M8CxZWG9nN/ShMtu9c4juLFXqLdNRxk2
30uIztfKuPuD9+1s1mkuGxgF+emiGaBzi75FmfduLRRVAROlbyRHw4Z2+ycM3qltO8Uuws1vRqyC
RrgXllaBf76T5yfm//TfFhb7vo2coJe4xsLFpG78VZ0sfUuKK8vnOfIges8Md2kwAQpvRzXSZMAt
zmsQntWenOlbx6g+JhPQdcgC4Mc9J9P+sAb9AF4b8OxNkD/tne0/PkwaH4tmifkCxnEQjxn8d+kS
ejTaxfuYlkEq4NwUcSJI4yMLkAL4L4tpUi3DXBGxDTM8LIR6h0gZ7W+RrjkzPWWP9QlUig7otbjn
pNsx1aK4Cpa1vwizv+Q1Iy9+Thyr1xHE08K8AELeZ54fVAOA1kAl1wt64j4z5N0pAoJHzxaPLIv0
JaEDArJ/OATscuJ9s7HEShMynCSq0g0ILAZ2ECJ5nWVBDNY2lv2vbSp77Lz1mbsdAkDrS820PQWn
ZvuqHmip3cDNHtlYA+Tpe8pBLDUH2DIywQBW9dT7XNZ/yJEdHE8U7BTRds8LJtGuxJrAlzIkLkEF
AFsHPOQiY4+Y0qA8Mo6gm5os3TasZXc2o+Y8oTOlJVUIfNolbbJ7MpalI7ED+D/bZsJHAUClbEzH
1TXa1q8V1cFkt8Tq3UXzrVZxOFmsOLODeyltNyS0WdNX00QibJyqEXJh1IhKW+wdthnopLXUUZQS
0MGSjO2qhxkbmCIqruRWQr5hN6tkmoafWJBX1REaq+3VTda/dEKJYYjVj2YRCQDkA56JmXwKIwDI
+KcgMRQV1CQLFCXrkWRsUpp//os+6hRU56fTDd/v7+2cwwloUJNgMso3JYLEY91XbDbvPyuanDsp
Sb3zNdrdcvRmQ6qo5MALZ+RplzEQx/8wBhTmPzr6e+bv7+F4UCFQjgCoqGuHR3Q3Dn6ULYEQk9yH
fF6MLQYofnQ8a2H71ID9Pzqrk+zKAJzLKFqY5NeOmsVbYWppZTiHhF3LCSKKZZCWcqzTmT1nWl6e
qnzXTk6s/kyYS4QtcDeIIkckFVW2uwFOitKCb5opRppr/HuvDjVtSov6gbyUYRBGf/yREDn1yHM4
EnfC2hDvqb5gV+lGNiZMV883LbpgQ0g19p9hF3GbnvWHj+hXI/oCvU8bBf6KN+CQoVue92LX8bNl
pJkgjJ7LTVgQ6IqjnLGgaMh68faUz5iwzCc/bAOOgIZyooOjBv0Dl34O9/zTCpQnMbgBRSe3AZTJ
F989YFT4KK5qidEKo1prlnHdU1A68L+dqpoU44wKVTnP819f1cepvzIAppX6PjSQLXKdvGbjXE1G
B7hUzzc20H/iTzgKt1u0Ta+clZGJJAv6rpXcgf2B22FDNDKVOpslOvA3+rhLCK/bVuxxzoVK9hA2
SvpyYo8nQqTeLvf3BaIAd0Vm6YC7wCR9X2+c8IMNC2dHYIcV/4qhxYkFfhsqH1sG0vWcTU4i+Trl
vg4zL8QhbXPZZmPcOAgXQFVbt4+muwtsb0oGPDIcKRsId62Yi/4my1oQ+wCUatk6jaaCepCKbYbX
SxBpjHF3X4BAZYwmHmlGpkxPDzEpIFxSTMjtdM4BLky23BO8XsmtcNhuDLc4+WcGs+wuM7yaTS94
2OyKssKmqVOz19Jbu5pwPWIjgH965GqHJMToa97CYStzRk7xzrpXyOgrbRcsE4W670ELYF1obGi2
+lQ8CIABGEGD/08C9/i/spAY6zWcETF3x8HHZhI3QBDughbpmx7E7DDsbiUvGAzKMEg4W4mslXHU
GybFMEOeS79iEZhbkDlKkmEJYfuSlRH85P7vdPsxL3TLSimz5p5I5s+bw9fzKhfOrdwxnSKdv6p1
BFKGaog6/FgotEEv2EiZjfyQjEznn2T0OhoGp+B32sZiCKw2WWk5bma5fK41KI3Nct3zjtr+U6uy
9vN3ngm40zhU9zkipGEElYE72kuHL1CAnkEMRwEX3TOODxrj2vrHC9OlQaxiBP3/jjKDCiaJH/1T
M8OcgNs3wNpkvu4UShC2N65PJiYaT+WiNjVviZg0j3l7qyJ5HLjmEQMT1PiQ1OuuuGEnk3b/Vlo4
vOEwlGIQRQJsHTjFT1dwARVBsEnEohJMuzpQXdD3iqD6cYuTBMo///5GnT5MBvDvMkYeyOjGx1Fo
QsCJ/7TjsuJY7hthZAlqSmDacR/Tu63Av+j80zmQOXx/wZtbr3F9ey9tRnyq0wU4FvfD9ZssfxW4
ae9SY72xD6GDcMbiZTyZSUDuYwPtvZd4yYti1i8PgYcdTdZmWrvwnqja1w5xA9CHtA18AQr96a7e
BV+5KJMwU6CCC/s5kg89m5BQufwp2lcBCC2+eQqR5OJhfAmkR3nrDt6LLILMgolQc/SWjqqaTYcM
o09QXWiaX8bk+McFzm0XF/4tsxkB+zs3Iuol3A/VYlDoesYasytAR5dje1J9FzQjA0UEkjjGcH8p
CmCTUBq/z6vvQGNim3KYylCywryvLYTaJbGQPdIpaoyoXGYystyYL24GhIv5VJ7xEptFzy6+mxh8
2SHxGmsyf/myxghP1dzWrzjKifzbPURqyDcg4CXe2j1Gqe1fvSYVCpp1yGEtW2qpafQcviu0YI56
yii5GX8QgC9pk3+bdQKQ3jft/k2+fWd+lm5kb0EJ5ThPNZRH84mail9WXUMl4w3+gRDphqTKgvIJ
KJubA42+ir45KgKh1YJIJm0x5Eqz0zTu6Y7WmW62Lc1SWdd5wmCxicfKIE5NzOHKhu8Njp+e2Q8h
nZhV3gmIwG2ZSwRKMciXNaoBxkY6Mh0INMROiEZhfOkzGfp+h7X2PLHB5+jvgkyqecKNo8vXthen
AdQPEY62n2RhA/VTqMEPv7Aw/T9CPvob5MgPPfqSa/K6Do/8SmHZHVZShCj3ikXmQnUbeg5qdcS8
CvZG68PDYOa4zUgNoFfjDwRAqsxuCBfCeiSpsnszXtvIHqhPT5aTe0y+A1PLeUkoLFRFAJ3/ywWd
Qk4qeHsSWOePflsZ6gVSivk30p+Dvs2rwt/uWLWKWtcBis9XfuE9ZbU6SNUvy8ZwbaKstcsuem5P
D3c5ph/wHSGYNVfx4RDcEHM5xWP63CvTAggCIIurA0PkAzA7sKrgFQdeFVrYRoMhl844pygcOfDw
t+ZXaUwaBqSDbglY1/61unEkBge7kwPFLZ7miYqOWlq1yV8phUVCX8q3UthAryM1oRERg/ZwCvIs
RS2thfRBZOuUTM8KQHvP1uv/yVdeNbDxo5eaSIZtD8a/Omg1WFdDR4o6aDNDuHAw36XlFrfXFF7K
Wt0CeqauDXeNiwuZR31lao1UIOYPrWgCESvUacDuo/cgpmAXlv4qRt+eVEy8fmICh3BL8C+nnIZ7
xJpHqIZfA/KFJh9gWWwxkcXhsXx4BnzM49xEtZvOhq6Xip97+yRgODBBfiPW7pGhyN2s3wQFSPCj
QUJrDD5HNDTp9rbqGIWGlA/ikiSPhR6Z5orLhQl7ZQy8btCjS/VGELmJE7Ny63OZ1uGdQhEcxJ/C
07fMxK4ZdwBI98HWrymHTplCRSVA0OfSGYH19FdiejRpAl0vGdRHo6/fOdh+QxxT4FNHpqyRND9o
B58iZWb5K7u96JOADYJPAMjF5cINxhKdcrP0aNe3C6rbOESJuKtTZEzu4CaPz3RkaGLoVYw9fdqV
85qvziNUSopZ8K8hffgq4wjoc8uTdqVCVce71HMG3iylDtpcsMv7NMA50z5qgzyICfM9WjoO74RC
rF9Tvdw/FgvpjwcwhYDhzSPFdI/5tPS6p+VA7JAlX6df8aGo9niksIBrU/N0KUnYETD/C2Gbp1FW
/xcyGh6y1qXtEW7yAcZs01FwZ2JDeciNGgqwIdaZrYxHNngqBAZvIDLK6C0py0YMIT13lLZ7tHOJ
lhK02j17og7PuXisCc7CSvyWpKlsDlNJ72V6ZB0Fxf1Vtv/DumWXX4SsQ2Q4ool9bsg/gIqmZCZc
UWOto1oON+fK3m7JnxjpCmghKKEdkXIY2ymH6zwGkIM2ckd8+RYXczBpj8VpaS22ebOASAahwCpp
WD6iSxFGLXZkt8eeMad53EdxSpXFePlPbNvJfGM1ErNVRFj0Bp6d8OFnCG+flddplKXNcsuy6Xu3
dXfG1/7Zp1DFt7GKxqaBwAW8V7jb30SwlbdBZfBuHDT5ncqWiFnNseoHOuv5bH+FOWD1JKVgtz4x
FDD5Mi/jIIBmRG35Vs88peEFy3kmiBFxR1i4vqBng9rh6MjW+7YAzIWlGIhVjWnSZbknVfHb/a5O
yqM3nRDvx79l+O7I41sXDeBSpFb01xZ6Sqe1KIyvA6QOYErid+hRIovi3q2hKPWOcV56sznZxw2f
N2/UWkyf08PTuEawbnhBNx5+udh/zkQLiPzKliKm/DiF0GdbjnjWtw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
