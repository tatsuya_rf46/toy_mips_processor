// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:03:51 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_4_1_sim_netlist.v
// Design      : design_3_c_shift_ram_4_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_4_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [2:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 3} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 3}" *) output [2:0]Q;

  wire CLK;
  wire [2:0]D;
  wire [2:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "3" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "000" *) (* C_DEFAULT_DATA = "000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "3" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [2:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [2:0]Q;

  wire CLK;
  wire [2:0]D;
  wire [2:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "3" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
OJG+snRQrl8bPVVJsBeO3lMkqs7RuZdrH06IOzPkd7XIW22m2avpetfGCxaTFF/bJJap5+R9OECB
TbjkjYdLe2z6eBetcFKoUkyj6EgAHYt4fszO+NeG8/j+PeRR+v5w0OXGps/gezW9a3SOYgb8h2ex
sScrrumd9MAkCw5LPokpLpUyLBPpB+r89I+cVHcNnU05zhYjZ7V/oC3aaZnWEoQ+38eVChF6Oqkx
fLANG1FFS1xRZfTmQcojUy9f1j1fehnKIOybpx+PoKiczZ+V/XAEME4kg9iZtFRCs10U+YV+o29N
GPNASVXjD1UOtfBoFowlyHk1O8btktuYxby0EA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
1t+ZlWiensqLQW+nnLr4TC4zZH3rSPihQFf8IZFzUlN+dRGpuTLcXVewT87hgHl7dlVi29ktYIlK
llnM1DS3dxFoDric0zu6ThUu5WvZzjE5Cuc41z+BgSo8NQr1I8nS06Q5NThmchXGmA941sP4U7rI
NMCqcci2IqMRdkuQ1TnfNIw1cre5Yi3e469q7PFy7gvvht8qtbwlQccCN16JqfLbNGaNlpGA2Y9Z
53v80cp91Wnq5ePOEXJeEz/Odj0PekFU3zoraTeQGCEwkzFxRMK59IS7iuJt+FDwzTps2/skUBvH
0wCw4OmaaR53ixM+mCdXsuJJiN+2eHfcK0gokw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 5472)
`pragma protect data_block
80lUV55MwfVlN41ILKctjBPIkX8XV9O//Jo88F3sxuJLtvZ1qR4JYtQSJqTmf22+COX0fT3//po3
x7JPLv7wOwDBhXxjv6X3Fbon73pAg7oZfoJEMCEjI2QefcICDQ3RcGoM+8WqiWPVMabnXORpo6I6
LkKTRL4R+7wUF9Tx03pH2fewjFe819ph7SCLtvh0N5l3SzeA9/iPTB8aN2ZTgEObtK2hFgVS7jqJ
eQMrA4hShADJfHZ+PyrrNctE199UG2NgrqFNryJFwMzCmQ3X5qgrzIcoOkTuW5nPGfdBS/ssMhEJ
GWjiFOeJ5ZOgqEV4cn0ArDV7kb8NXkSLKi3DCXiwmSetPk/StN12Scq5s5PnEPZt9wKxjSXnP/38
uBE3GAFsU3g+JhJ55Q6lUnfFGWvkxiv3qlZKNrHJj9jNnqvojubGAFWMH+Zy616sw+nyfmsOkBDP
do0V1WIdlmiqs0ty/oZBYlhAMfr5w/3U+9zTSKlCc5CWLfgSjBZITfSYhrh37ruDFZC5bIK9aQe5
2+1/a6fRIhIR4vpXH2pFhuZJDiPK32HVhAAx4XOmY2dmboIOYOMnkItbjdpAgrDG2IINewoCHwid
WUwUyJ4Tb6kerXhFSr/0aTZUp4XSud1GeZS12Rm+OpYeg/LUlU0Yoe9+BetNPpdKXxcK/iJS0f/m
rUlVKL5ciY/JZTPilvsBEgByzzw4N0cR5bK1vvCRrismxJSgZazIQzkLp/UXrUiZj6oaDtp47dv9
XWYS0/pf+cAKBdQE1IMfsC82L+BYmMAyznZNol/HB8sOONCM3mLkdCqSsqhQ2uE1jsUE2Dr16v9V
V5rvFbZAdyauQy4Xvw2sM4WpK3nMQa2EYV1tQmoZiCuHruOT+kBrnCK4o0kerdlP3CIcybDWOnje
KDnFnMH4hMLsru/jHdVIiRq2wApAx1OZyApWEsNkWOZiGsaKC4V6DdghVfowmijr9YcDr7g18f+x
xFiQvrfyDLpOl0roeb1qA38ddVmTchhXv8tmcvWwqMSLxSFDejApvYa/b63Nq31Egf2u/O8VVzX/
bHbec3MeJMQjjT1yQfhgI/mHNS6xIlPyDzdaMj2+fWdaEJb7/wWQOzdj0z1QiJV6Po1I9nFmf6ZF
SliqnijEqksQLexAJ3NogZdDztBLYrLFS0gjFpJciBk3RjREgBYXxa2pudKAxvhKu+F3MhBO3q3/
osOBxPtx6zmgiUmrlsvMYGqIxjXdrGY5wStcMzU+5nA2UAX+OykEVimyTUJdklNtDMjlmtI97yux
0ao/EStfzB9nsT9eAWohoMNfFkkwasA956uKI4wSXwNhqMx1PiBx0XiwIsu7BKzFjAxH3DssAKFK
qDHq5J7sBPkR/CaqUmQSO6Z/NhPzaOGAEJ+YRUW7NPX3hLoR+EDSAF2HMbjUIrA/ebY7Cny9xji+
E/jy2dkcDqk76YMwgHt/E5hipdHrjFTrUW8vaFdyS03gkJqcZML0WahX5rFpLm5KBIzHFt57pfFR
O358vSHNvwNsngVg/hsBFOUWm1YdchrLPp/zUeQs61eUb9Y+JaUTG2b4snYR6qLdacygBos16xZQ
KTHBoI72JIi6LKqEjNk92zTMrtfhMR+BIEqak0jOiI6xpae87/OlhLltavvh4F3YKeg6kro/SnSI
vh8WSo0oVpdanhRaA7z7hBLgkvPb4AWT0eAXysWr9Xlvh0n1U0616kzgbxcaK+UWxpvsCEKWH1JT
AWiZiB5qceyERBOy2IlWcY4dyXG5skC9mUBwpPDwaLHB4A5rtHS7ZjDMUqrSNJJCiH8vlC+nMKz5
F8W+BPCDYWVT9nam2GbYfIz7dRFVDLkhibDopZeit1AQicw4ke7Z434ItVARkIS1Ubrlj584+sqf
aOYlfyml7lh7bH8AAw7e3cgtd0MrKfaPxgFGFwZdzjJlRqdxOb4cS4OpSU4pLit4NKp+MglxoU2o
trv3xayy3fb41zSZnzWtA8js/rqUxwPzGMn68XVwnqOTyu889OzhzeLfjwIyrVAzo/32ADs96oHd
knpAaIOw8uWC0Odu8/jDE1mvLIVbymMfMP/g+7j2h0jp9gicOZYpN2vLkZvAOyKqOoxesT/ST8u+
Zm38/qm2hU7o+Adsueb9h3V3o/PDeDM7QdKN+O4MK5pBI0OVlugfb+I0esWM0kMNniEgZw6OfgDG
kBnGMFqUsljyIyOxMOHq7wCBwun+DgXF9ghnC27piK//RYAyUyFKEHFoDU1vlDk20TWcuJEIiliO
erAMbw9DQN3zw6gaEg4LZ2OovtEL9wFWYBs7zpHW7GgOnvK3jNFJ1XSkawkEpH6fhNZLiWkls1aC
O+XwHxg5eMTFtOGgna2iZKU6ucFjQnOIGL2kExr5VbEmTLBqEp0xZ2vMilALMP+nQeKB5b+VdOJC
Oc8gc1FD4aH+epKBJFhrfSNp+9XhJwSxDm3P2FUKNo2PV+6zMSVhQIc2SHLvUbTRMa5SLonv2n0l
Kjw8AKUioSCDcTr+EiTrVzqtcOe/avR+RLhqLcyMxSbRG6RrxJ5+VOnrworMfmRLKtLSyNqBP8m4
cpGNBwCCYlMRwX9fZaMPIehfgmulVQXeR8fzLE0UoIValAcr8q4iTEkPjEscY1NPDqHL8kp3Y8hZ
Kzsf6IwB1lzr+mZq6QKilx8+1z4bcILc9eAgeEdOEL9Leio9yp8FgqObSY/ODBJ+C5C7vVuGh2sa
meNWWvg5fY5fIiPe25fihAkIpMaaeNTPalYIrSlz/0VVlcG5Mbp6W8GR1tAEYOzTgCtx2Jw53vAt
Epsm+lJjuL2+e540wkIsS+RMbIHJm5sIOpbNRVTuc7s1KiMtKXuvbH+kTE8QvWtzj7rSkIHkh3oI
cZSIp0oPBStoIQ9wiqiICpqEh+RxXjHafUUYh6HSeZXBRXp5qyGSO94lHprRdejBWsoKokutYnym
z3CdgoqAPWcM7O1CSzO5mXHI6Tgp7fQC1EIOF7dHnQD8IvMjSKMnnwsVIgeBzqR7AWaBXqInGc1l
ttjpxTLFYz0HZn+TeIGbViHhvbNiIQZ/6cXa7vb7K+5RxGUBqjx7hVrBF7mXghNaz20cn73y1/QV
IzvYgaygHLwCQnJEPuYiKdz/3keTph/b0NGyw9m5odlryLMW1p1dmzwoS62Z6Es/3EAgL5fCsF2H
aKS2czJVmSqgOmIJ4alW91nZ9Z1e7sQNaLBAGdSRJhtxJsHhFCfsF2D08VeWxdbAQSnVol2wzyR5
HEnlsS9+pOArA0Ghpf9lQ1urdDTGGmjGUYxRBKXgBT1o/pWmhPCqFPjAmqlv6ajHu+LQKeEmb4RY
v2CCh1WxydgH0MMqoMOQjETJrSL/6UPcfrBJ8TqYsq0af7TOMVRUs7YUKuTg679m/mgHRVFiBTzx
j7+Di8RKhqp+1/vf42biwPaFYeuK/cDSSK4GVxgJ60Bbp18sdK0clbVf3It7t3Bg3NAl3MxRtFqF
X557LBieajScXF73I2WZBeqFxKX4nuI0EXlK+YdTTrvFsDADTTaT38ksRHYaYKvNbSERxr1m3lZh
KsedYopVuvWgAyjFj2lN2llt5PAYgLiLHCTIcPbEGvydJNbksE18FAhAGs/K9qxci6cF1x1vDzhU
xwJhz6xmFitMnoeOSVKAkcYUGfwePerBkdT94WSIIG3leI5R1U24VJUwPFcp8lFT2FLBi/vGQaLF
qvEaBvEO7shGTXq+dn4ce0KxgkH36FpYvueJ87NUovkxWiBoCOxt/Csg5JUHItSg2aP3tmxacmyv
rG0xPPjBq707A9YYw56BLe4hkl6XBCO+v9V9FxEGmeBD6klyrdcB/KSon5rYcVStRmkVyawF36Cz
95cd8i+CEOOGjyK4WUt0waS5fAaWYSSzgE9paV/YM2mE+Zd9hm1ybvxxetXfnSM+srYJI4XSX7/E
KPXWImg67GvYDYHNO48g+OfqlsP58eoNv49zwfD3bGUv4kWf/UPRKLlbepsmjy5MNyZfJ3GnKuhM
otZ0aX1AUjqNxxF1RuXy+gSSHCcFuYjfXOWSrcNt6JcRxHiVfn1i2tCpJ9LD3zt3sTUDpJ/lSxPE
wDXuXecWcffgy5w296NcXc7KCUTVk/iw/9dTj4qWKaEaj7/uPlevxrg/6ftkv+jdK939341lYvfI
P1cgoZgM6fVPrBVpcT9ovxDpgrmhGWkdqBtX+pdtas8TAyqVdDbgOg9Wb1QyO9IUTD0HsdHu3iSk
GsW5kBXdI7+hp8KY/d/otvMtz4pGZ8U+7LKvKX+lX3/b723uxzl2RxNCjs0l1mnZLu0y0UIu2mzN
cpa6qaxVzwQKA+pVext7bfiorphuy0erbg3ZUABIMguB0xYKMboHzL+sGpXYh0hoV7o10JRaH3vB
9JpMffZ6JrGorGHSUT0nrPX4eP046tk3K48CaTaYl2CQ/qUB3awCUq6vWT+S8RLV7QW365T8hvkg
ITI+GbZXKp/kVPpko0su7W57jiQbZhf4Xt2Cu4yUShWhTFu53uNDEqJQXIDPO42qMH6Pv9mwl7Su
1IdNRrbOIqCIQ9QcUl1AQMt5NvMMqZzovgA9k3a7Z8TaJ4i8z1jPY8PZzireKzdi6+0knaSntm+h
lCFWf2KR81pY65Hlu1bOLHS0LKwL29tL+QjFLRaCICroVoh9K0ltT8VQG27dgaEAQ1sa6UimHFCr
893T5xcjrGLNzi+F9d5nZEFnUrlROiM/rvdlmz1JrO0NLBRRDWGgy4VEXR/X4+AUis8Tj1BHhJHY
B2415rGjWTtae3jr1q/8cFRsMMVNaAWb409zSWDFEYj/DYUnnitW1WHC91TydcG9LwGjJfUppd/7
BIOGSDFRkrlnZTxaUVsgmb2sdncDCZShQHI1C1jkxIs1MuBKMr9e1ZUtAedbH4C7gcniTBR5Boag
TE63VwgPzVM5oxfwKumlmwFH1hEKH/lgcKtYsbvUV1ZWl7YbkW4lcnA9lvLF1Ye/1o/75ZHp/2D5
kbm3hArpEq7EcBt2guldmrZz85Px2pjuj5qLlSvPV6+GPdEVkbjh2dJsNlra208Fi2HAJgRQ78Ml
a1RRDbLhOI2uxN8Og3+Ck0wq/A+r5udHUZshbHlBC1PIMO2Ax60eckJsQSwlbqUOjWffMhH4MPfH
nGgK/w01TV1QR9JsoZ60BQrb/yi/5XbsflJqBFJlb6gUOP0OA2uOOfBrgbqrtbcO8fnZ4+TOLUdH
UnuggrBnG2DVnXllkyUZKD0S6mZYYOOqP8Ju/t3jcCZrsf83BVEAednRPMqovUiRnPh+NL74xd4z
+EjR2u+G/QULjFyvrgO1/ZhWK3M9fZBqs5St9f8a64jYTXgT6iuHM0XQ6XhyhDJDzIzAqVhHP0zJ
dh0xsFhx9Gu8KcvLMYy+vb0Chfk9vjN4y89/aLqRq1Py+LksXx5tan4ILS42DM/Lmtm3yFfMleSZ
lXl4mDmrum+zYlCnPuy3Q9osTjHlaqDsrYdIYF3zKbfpDmpRGdyzOjRUOqy/YvoImTbF9RgLUb5m
COMiPjmrOrDkHzMGZMChK5zHuW3roE/ksowU1x/JRXKYF3hBgDtziIOPs0CzYJ8STRhZiLB/AuU0
RhgWveIXBkZgqRCGBmq0DJDsMTxn+WSRaQDjuLvMCHqDyi/6jWOfDBR5H+8LS6WfRZRoc1rWILUR
kSuaKEuvk5N436C4Dra2o4aaEUp6oh65RqmsZtE/c56/RAFoYpswjIQ8IiGkLsSRPLYwUfpOIc9H
p0yl3rH5yx4xYjSyD99eyAl+FgiAAKcdq3a5fZuM/znIdS4+wvmHBLQtNllMgDc6ktEI/vqNXo9j
eGHhv+QgK5A+aq77C0cppCQfdXuzNMEDtWC3CRbkoUmh1YCFt/GrqVTes+1WghWkAtyGUdLoOJMC
3ka65SjFmCEqCva0iQk4Hh3A0wp899aZmkxCVyTK2O8GBduLSBsomBft96giOvMHypfg33AXgWxJ
clwbPW4hD539paZqK187zYe+7vIp3v0WaQHHD2Ax3ZRbsotExRWIx6eGarwgfS7riH5IGJK/hO/z
y3orbvzJ/dlbsCKfXuJl96EbPZxZRoKLTbnsudq8vcq/tLgh7y+sL+4knSmDmPdoiE7/wSL0Kukz
RBANYCAdT+0MJErY9Cy++O3e+vYt4i0XONzGjn9unoxPSzsEduPhHDnOSHy9FTyPMV+sBxj+8v84
+kLXAj1IG2xRCQXWsmt+n++JdprvD34WwgUT0nv5zvGQQwuG5ghogFHVNtwycvCjlTn7P8yXdJhm
9YlzmOQH89tD/HZuZJnconhylZ7hBFYsDyPeRF/xIo5wRDrmYS9EmY1qJRh/uQY2sGxG7PGsleOs
IP9uOvx7DcabKJ1Tl+p3f0nUeGzMQKCqFGRz4ZEWVxzkn6vwSLpAEmjv5ylVPbp7Tk9RPwRG8bc7
QJ/K6zhJWVGW/tDjtgQqABmu8/OCiHRAYxhTLssI2Sx/z/+DHAssQdZcdcarM+twQB8RFoUmT465
CQlP4zbyb39lCJlMAVlDuRIZWKsdJCGjmRITFTvsrQ241Tl9LC7BWkQyevTMhu5Rb7x73ppWPETf
vZZYEmxkGLLSe80RoEUlEXBw4TQJtSvNJxB7/AW/n/8oYig/UCeijyU0wDp/fS4xTzw/zgXHDxAe
fgPDiLVCWj+1cJNDQAWWSNEev9jxhnIyfD0vEvKqHnG4Z4zWYly1kWCLLp0cVbur0ft0UGgAjLpB
tcHKqJ4sqbIpxj+3gyV4kSNNDBDgX4qpWFUextNfk47eRBPcdnppWSU2Dg4xgJscqsaDrcuMEm6r
H4XMxenMEHKwAN9o0d1SI52NL4rNFrlFYsltUnsYpHk9T1vun0McvMjTTvLkEal/CZBQeyD+ntS/
GSKPNIRiGTwKqElt2hfK4KFGXY4+HtDKbJk3j7fp0AzjwJGEZ/YFD90UuYJP8IDWcVu59X4+RJev
1MZg3K+WKBnTc7i+HzXAsl9pvyfaURacL9BgO0K/fdOSIcas58AlGg+MfMumMoLEdjnbXQ1vlF6X
oX+yKejdNLVUbNrTX6k5z9On53b/lSNOCVYwuviQDpMddizqjRN4vuhtQIw9oS5OTYHRC3tw6ZQt
oVXkT2WbhBSN4zyAkr+GhVvki1jixOrxPE4ZYvvq9gjKWsSAFD8oPnMK55G9gMsKo29qbRcn170O
OuYkLYD9SAxFCfJ5IpCOmIAS17QN0gr2rDEzK8trgMVUO4fWneqA9vnXzwRrHTC3Yw9kuajl0RBm
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
