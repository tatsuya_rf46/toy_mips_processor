-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:03:51 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_4_1_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_4_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SICF7yO4l7nPmAEkWAtdfuquqiR93zDD0cfU6n7zu/bHv5KZXjUKE6TUMHlwuP1px47HBrnMy+BW
Y+ljCnylYYPWd7TTlyubzjcwqQ8tVMtIo0eCcVft4QpPhurC09gL036iTFoO9hIvdT4FzSA3PzxT
u9V8+wc0BJQKq5wZP7q0dAXT+/iuaUlUVN37dCHvBkdjpPcDjourfMdyeCGk/PckwHvpWLWhOWRn
MnPStiFzgxGMGIYBRcg3/mNnwvE1kK0IjVHvPxm7d61CDUrpZ9hRktL4R8jNH8O4wc2oR8hZfqtb
TRbM7YreXxAbzpras2Qg2nVe/2/YjhBFHxNZvQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
lDkLYL54VhiDGiHkwsjqITKt01FiNY2XyXsoP2MQDtiRLS6ftypcOxCfHrxmb0Y5DUx7j3ZJWxBr
goVu88q36h9eFPNMQvDxBh1erK/F3Mj32RJsg82WMfe6kKOqaEwJK1InGbH7tZaVMxIUlSD8s3bv
Un7RvFg9leLWZLrFlB8Y+ds7On6Q1y41/e3YVGw/6w/w3Vh4xS9qpLeYP8CCEG+90FHYzyLPoFZf
vEewmHiCdpaJqm4XmO4+kFGma6ueVsCSEkcH7qb1gLaoRQJr9EMZIKIAwGaUunMKKr+hmeayZaLO
drXiENefExe8HTza7TdHGDPt0BMgLNLYlo1Oaw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 15776)
`protect data_block
dq3UU2PZkbKeMCdM3q1rJSKJqt1HFz8Hi2FhKLlRxWcy+eDACS8xQ36x8sesqoPvprEv+tMkL1Qh
BgJMc1zpsM5Ejr4N2bpOogH4pPAUArqRLc9EExJt7znk0LTnTWMPmWuefS1Pf195cjU2fjIDPZ+Y
iicsHWXHqmUqehxZmSq7KWzJjmBQcnkVVtSEMY0cmHnoRymMXtxs3oFm5/k3KrRtxlABg+p4nQS1
Vj1afZoNZXpVa5Upjb5K7mPCC4WE9CZVdLyW+iECA1YKOVyOsjHX8EvPu+DbOuLBi5i8oSKMkx/E
ZLCA93H/DNxPkqRQ38Gz9/ObhJclMJeuL+mbFkfZE2mAabJVwfxNFPQ4zoWkIwoCyvVmqV8yLIIw
h7Th5pytx9YEosh9MsuFeTp+UpWAju4VmhXeJjyj0onxS38Gwf+QN7dcNlt2OZpkYzx/hBP1Dm6l
vn96PHqjAbQNUkp1p/UeTv0CMGhIM7B4MiM1JKCIFz38+dB2ORSZozxgt/2wpoAZOByLu9FOPh2A
xTLScpfDEK2IhYTSvwESTsduSNnsI2DoUwqXm6FjS+AUUKXm8c715qmOh498GVvM1OJDFobAf24F
KmITk7yfOk/YTHyLnoQ4RCsQvudpuERC6Kp39DuaXXVtv2yorqdIOFxgic6l3UKR8dRzxR/BCva4
oWK9TtEdZJRVYijhVnpra5juPKCQ46PkNLpf2NWHoi9jBl7hfgJCDFLrNK0HBAPAi54wh2cjsG0q
SB76/0lWCXvWiZhh2N1wXsQVFq2ljd0spbEepivqZA6J8c5mTGaHCJdmtmz9By5GvrpPybkAUBez
TK+Qep8qhnSTkDdDYyRcajxnlDKFtDCwx+EOZqphfWIIos/mYj/WNr2gyQac6+eXjQmFiskKFoDM
+AfcwgffduP2gWtjBihnyBNvKgYPHaiwenaIxSJ8/aR+RupdJzQgZbCxLBuVV8aQ/7rfp1I8wfs7
dqebJz3eMyJPY4M99pCRL/Eq9PJjFblY87MTew+l/AljbTbNc5t8z2zR4kTaQIDlXiZ4xbHuYs+d
QA1e+ieP+UZUDa93mJfs+1s2EbQ+97kqvlh2m1D4q41w3eLIKRmGSSs59txMbxmXUwKvdovPt7Xw
VT2FDXMIaq4oFkutLiA/LF+BiJp72HsidLtRvdorL1hjivVCE7M79HKc7sQPhnwETo1YyJf6KuKl
Y6UKTld5XHEuuzJJKwuQvKisvZAUlj6/jZMsD5mCpMMz6uiIgr1qguxxTcloA9mPRPSOeyqDYL0c
LsIQwZ4al2byhUgyVic7aNPoTyPxWIP0WpkKRDnQx54alSZSYJ50MZjPAGQoEkDRzvkiDiOv0GRZ
zDpz70qFzxG9PtzODP1xHJW187JPtrIE8gOdQ/GFPhNEGmmzaUOyb5Yh8ehJ8cR0VD9Pdp0QQIf1
vdUBRQTVAAK2OmdwgEG28LRHaA+JZf6+agdaiQUOEXPAGhupgpHR5Wf4XgL09LeoyrLeKWoESVPE
W8ItA0l7abxFd1mWR/eh3IsLNispCbnFIHYoJoGBCamop8Yc/IQ8DgSsRUzG4p1jdqQK7+d3vNbM
kcd9O8BMgP/Ck9FYVfuKHUvW62F9aklsQuociuPJSf+mWXkoaNxodj9K0Tae5srdFaHsv9jZWbTf
uXh05uBWMn/ftcFTpJfFzdMIvVkj33Pa317UcqWM/sGMFB42bUtGzHjqZNTcGVNGeGk2RGDpc7V5
N2iIH0zz/XdRsmYKSIMNeoHzTuG3ZLrFLUBIOEHYKB2Swrx1CYrWM5l8cX4h5Bq+tnkAdgX2CX4N
TqB02nFjNo/Vx98gX/bphF6Mp40hORrDTVvjWbxRupR7XKqzkKdrW9HLRnqYlWV39o1EiT66JNVy
W+l5mp+9xnbSn0Rf3U/95VqEx7mAu93YCQGu9oLsxAcQZVxps4VisjxYO4iQXpGU8rXfTyfFpL0T
NAt92I2KpMa4TBWu6CXiQnfQG06Gfu7EooiJLCH0ImcvLUIbq1VAe8L0L3dSJ9yfLTjmMDqhs7qq
dvt1N4JTeOjAZOIC6yCO6wWEjPLrGjKL0oOEEM+8BZ9qF8tUo68CAXjgIYu9r0Kg4TRK5lN7wJT3
4V5VJFSnuKHoWcySLV0MaHWJpEtK/M/A8Gtxi3xczM3hZdfnP2iTRpjuBlPNFBLpH7Myo+wOtMDL
vjc8/+GibCiDXxybYFXd1W3yB7ddaonBV3UVtofPb1LpudBI9LxJ/tNanwG9/3dQLr/uTAwIwGT/
tEavVkHCetDrH0BsYLseShyMe/QFriOYnVsaflHxBSzeDJKROBAnBEkU77RT3s3dcLu7/Fz9+hvF
baR6QNjU59R6wg33KQzoBwNl7F1r85srCze1HdY69Oy5QB6Yb85WBe/Ek3nPD9EKHbs3b9mGD/4M
sMForoStHl+q1AoNluqyFCVGdGl+IJaEfEEcL1t+qu9vrk0Vke5ZyRC2e/EfZCSNk7ZPVxfHjIN2
TCKQZ8+zoMP4m6IvTCstKjxOHpMV3SOMWH1Hdfre+qsXZKyJyOZovsZZ5eegLtLDbFLnnPWCZbls
i3EABbHnM2QuM6dtrR97tS2zugkh4SlXnSwlKiL7hvsrGc+NrNhcwwICsN+G1n17qp9IdFPUteRZ
qNB1xnlPKf+qXAHLFxLsh8ePGYgiLS69SYIWmCLsFMAbZpcDRnPnhNa/UDJ2leVryP+R59Nc4ZGE
qriPpyBc9w039g2sE1A+XF11yLs8a/UhXvJWgKhYhDj+7hKbFYrCLI7o6DLeihwOBrVOoerr85fd
qd6vpRmKRbcRVSdKifFvIawaUEU8pI40bz7pqE4HB1IrrUMfLkzVuT8bNwWLPSwj1IhJkLdGrpLb
fOL++MfbVH51Akfyu8/eEnPZ057iIN2Ok1sy82MQ/nSyl1nvgkMYWeqyX9OsXCVgp5dfoPlA4GCN
ZxGNC2oni40nD2TDU77FHGRRcUkRB52Q6L8HVngy6FY2tIh26e6E5dvPTPpvf2J5NixYN6agYRlm
L1oLCAGJVG33Ev9MLsgk1OKGIY/3QXveEG17EqQIgnVET6nnKsVSY0nwJR1M5WmpbtHmdl0l7V4v
xIL/5UDdVeNqdZJjQmqmQeX6NHwaJzoBW32Yq4q76VjY92Wj0/F+M0Xp/Fhu02W5ulvVI/iSAWOZ
vdpsGccUbJ61Zxa6No0eZ3/JNOY5QwhlhlgkhhzsGuPowo45R17hKgSnf4LJYUTACGw/RDxni6R8
NPJaNDmH+oFC8WuH5fT5pTBWjQUNtrmbVDriZG6DgJZyKri/lcbv+OViZGSDbUTr+B/kVIFZqo2h
ldgWxwDv/W7SUAqeb5SESHK9jucWc/jOD0J+Ykku5Ma7CreNXhblO4K4UI3wtvhrveBabqmeZTwk
OytNM9Cvy2ZLUfxTuXFmcPJV1lgJBfR0DMZNjN2eaInjBKFTQEJc5aICiZprubU9idRQazK9i8hg
cb4qCkbU+a9zWnDI47xwDS6Cf3B1Vkp1FtHjnomRRXixgLjIst+qtB6vYHv1y/F2WQRv+RFCNPVb
py+ALYop6nowrw9zmYyinQ4xLv6OfQH6paPknK++pKdmdo6+EUyoH5CZgomgx6+N/kqf2X6VgMvp
t9x0UPHtCND/OOcQkHa+5eA8e+k7PlolZpqkzq2Uoak/CUw7PhOqJcT/UHj0ES3AQrVqQ05N80Uk
fKVK9Y2VwuXuxMd943AdmjuV3yi1yZsoYCQ4BQ4GpHXJ2rStSgIjl5+4MTey9C3J6azFppyQ9T0Z
fg3g402YbPB5CtBI9u7xO9VJObCke/nE0I0FxP0XxbjW5LKfeVuaJHwsBH2D8vjYoBZp5+/+//Rt
cCJocC2zuGagG2VBmAEZKVZzD8VxVOq4h/OPmxP21qCM1Uypr0laG2Zv0TbUHVuQQkWJp4ehG9+O
RqHDXrUvN9Xpqsju/P6JZjvetEemzUprxSS2kHskqmKrB1FzYdxfo8QsO9C/WCLMmhJNEI7nZ7fZ
TqhLObn59pTc8aHktCQiofBgyOKW2ENjStEQ9Mgwh+3fRpk4bn5Mi3mhTf1zawoVB2cyVOt4353X
yylzyZfKsiu6rSzt33X0oJNugehyVtCXuj8McS9OWksNXzbay63eLE0mq79gAv/DAdQSamrFOm2i
aEolIEJMq1AuGXUzMCbiNSadQjZjwuWxSGwjqxTjC16c9UE1UwXpolwsnmRxDSpTZMWKmTGFxIGH
tlcFb5c87kQ5wBPWomeeefWP7KiTC3VK1zBFuE0vAtFyfTj4KURn12k4tQUFOoDnavOaLiPgbcGP
ryuRarf/fY+iB2FB5PHlBISc8aksZ8ijMRYqAHw5ONTImScHvftBjJyHuaePV6I8bIcnSRjXbwEX
BVbLtGc8jEKKLc/TCcZPHtjfGAC+GRw5pfJXkailslXHsI7RoNIMPbL6rYCuRxHmMpmMBFeqruMU
uGjBxvi7H6T2i0T14VbNLNze5J6mRaRT2sEMLcgSLUbp2uZ7G8R+BTv+xZdrbexWmY1bYN0rK/Np
eizwHORozcxSlvS2nNrNqEJJyO4IZXlckQsmUg1BQN8v6oYcIDavO9dYLzb3wdeaxiRvpatM+9yf
4Fhb9P46igMCjILUQ9uymGBeUurdw6GP85RYNSYlWwvW4qX3f0CY4AixIm/uzAVmzDxat8F8YYrw
kiopS727+WgYRx5BtVShl1dAPDbrKAXg4pPzDYHJutDdCrednDgt1tyfKaK5OWczkpCFybxTIHGJ
CkLCM4KDz4yUDMzz7TPdDRTsJFQA2aHdPSaj1YoZFd1NadnOCyR3m0IyJ8r9ML8BwsPj6ue9OUN7
9F4EPiKwhtXuqxAmO5uDuSbzXBzD59kgtqb5EnNlGGuGR088e1Clh5is/JWfOwi7C6kgdI01AyN0
dfgOdON8LMY0bCIc2auVUb9zwR3ZHrWgiKD2NA0VI9QtRfZ18dhdUgU2DBH6nxqO4IX6XT15/MwM
PYFs76eRXqfGisVg0NUbbm2mvbC7260UtdSYUGiiItKB6CsZ1lrGP4wyKzRd+kHD2751bjqwar0a
8fZ55ENOlU2E6YAY3GoT4NKUyfL0MAnTqHTApOp18cp3JIjs6POlPSefz/d7sWE7DcbTOzf/oBoe
l24OoEeAaGyq/wJPHu9SWmalePsGByPbwwZcrXnpmGknipBwOHuQ0QdM0y1eRNqpzGIOhjmF/NcK
RKZhg+tmzIm1x2gtjPzfMp/QgV5gsTLKEMjklB0qCcwHnPrk7K5Q2jKeiWQ5Uj8cE4+dydyA4r00
8dNETWnCONIhp7OwC/FCR/6ONVxiBQAOIJMdh4FkdBHH6WrTJxahyUZPnhpLHCsk/at2fX0JAPUV
xffsdmTs0JosXRrE3vcXToNIxtTIfApmguRHH611GLrhhdZkfvHUx399RxTPxxKfInHZXBkSNPRd
9NRTKQ6TnCFhr1R5kU9eKyCUiAzZnfP/qprTPQtJqJT/lsEHJnTDnJLTsApA50Wz+ZbzK9rybcM/
uRE2fmKlpRA9x7wxZA87b6goIKLT5POdkg0QkUAjdXl7XUa+4slbD0SC6M7cN4Gr3/Pk5Td+oWxx
SOivmeuS6G5TwQb8LWVifwcDgvr6/MzuG0FLUonaFHifm75HRkFpUQr6kiJTcJt3NiYuDFwXMQej
cFjSjBqNmUbS8DPCRO/2HErRfytx/l2Pa6r8Nx1hIowvREZvDpZontbkbfwSNCoeKKl9fKIII3HQ
APKEOpX4usGRA18VL39DvPBJ8HszDq0CHW3CajaZM+qMmJcKUAZE/XoJKJJ/G+HgWlZszkmD8c3p
c4Fsf2CkeUjQ5/sLh5zo9h9fGUr26rf27S7Voiwcr5njQ3zUI2fYO/HOFRRo4kUe8H5QYklbhjZA
n5JKXtMx5n3uRM52+uHSHPmdHYtuVq1Pa/82tnYhDf7UDgj1Fps6NQDkAsZRtre85b4tgzBDViEt
AFR5qgDoKreotEimsiXl5e/cs5TaaJR3WEMKsQBqi4XKtGJaP0h9bHKkz11aPi62S+wTINq7dOlJ
krSVTOp7UMTmdo33beUuGobKFtdR4RFirz5eQDdoI2k/7q46XG6KF6DmqlL+MgtJ7qoM5duhfcnl
iRY2JKLMo0+2mCef/j0sFlOvi08BlJI569WYoiVINZNqcProztnVLtJ724WkHuJiLwyAHQcf4Jaa
cVTMsUAeJTm7RYjkm06LSjq+AWQsWsi40bhmzX9/jLTRRNc0JKyK6CReXDUa0x7Ad8S3AbVk47oX
+orWBHOl0aeU4/kq72+91Yeck9qu3Dp7lXon3sYIpOAjQvEca1QevXjQ59NdnA64jwQTNDN6ISHK
skEMtz8jXFY1ejinwLaLk3US/sDwXVWZ7oy8mZygllX7kJsoAPUg2Aj9qqGe9yUlmZxVvqyE1Zbo
0XUq+yxukgdXiUGsUxdyI5B7ZCc6o3i59+Ck6MtEUN4xSLAsFPP0/g6y2OQqKGsIpCSXELS9/fnb
MEFEpBvCM2Y9tEyj/0VXZKRq7qtu1LJm7xVuJrowCzp706fMxChCHpO5k/CJ/bDlLnqxVdUjiLd+
HrjTPLANoeNF5VYsezDJljdibobmPy3OJ1FJMX5MB9eFyyDZVT8f4BA8PuWMa9VRiXzAZOuZT+xc
NFTVF172VOcChmQy1UtWek0MQRIsvBEszvP1w2JN8jA/9jX8byrWUey+tSs7gRLaVJgciB7vn2LX
fE3yRXRMXKerhqW/pD34yaYdrt85H+K2Q6NMYTs4i8iysxfEX34snSU+3LlWFjAkFxxQGTvEotx1
c1/4Ik9TXA7nvasfm1tTckE1+Yvx1SGc0RYqhS8RrFySHqYc/M392KyvLAnrbQWq308WWSswyO80
35ZdiS7neJh8uezDupKwhLn1YiwGNR2ZemReqY4rF37lUD0msYss0j65xMQjyaL0DBY/JG4GqG0A
YpSay46PXrc17xQOhCAbybuFXuHS45qV49KwLmTI+AzQrHS3C9hGWOHQFSqH4JAIRulllPuBwYIY
U7xzbN214CgkQHCQ+RRfxoAEgC9L3pNR3H/UFTp0UuHpYoWoczfMBBLprZQDGxsHG8Ee7gpC8to5
Nw9bVxYVjMSjoyUEzAWJiNkKQdaTwiQoZ9mP93Es1kO5VZwpcTG/4po85gQeq0ybiu4Z94c8T8vB
b8YBiV0uxoqxE79WygITHJVDeGlpbYlN2RuJkINW3JVrX697QApU3y0vA+VvMgWBI2oO0LPYSCqp
RikaGqx19PMgKnl1mFm66Pi08nVTBKxvWQWlEhtCFtK406EsHYqTSDqH1ZLgvApLSG+/mixvGre/
ZX3RfY91oR0SV/xUbdIfrcNSydaNHugIENVBoT+9fOIV0m9lQgDVIFkt7R6VmJvzCJNyj2/DDxUt
sj2i+WG80i20P006usZE1HcidCY3w7Nmv87ZA5zjgOEWaa20MRGwtgWKZuRBySJb/Rr4dfUuLLol
NN+0jbPq6PeA1gLKA/dL6wRymrhn5pglkTrdEKDHEdZZ5oTN4G8BLP+nYU6trzNayA/9evtiXcLY
30DfgyMQAZQ5HVxdD51IXm1IVmjBOXJXzovqpE/58IZc6XFA79M74gLW3EZOGjgQ9NUzxGAKLUFU
oY+RNsGM9oTUe3rNHSwdJl2D7UmV7KA3DmJekD+U/dshfjePHu/7EC9Vd9p9RuI/xeWBalHvPQcd
al8loQ+HFmQEVF7WMs8ApP1D06PRHeQzBo82yTl+2plZSHDA6BrdM4o8LXc03MPxaj2WFCyqCR7j
878lJ6q80a9otOs19issmuFg+2fdxmMFcGSi33JFobp7ZDNjbXPltmki6fuBNYq8d85m6Gxvp0jJ
uQNhKkEG2fo805r8ECBEZNitrfi60sftqYpTLEgtMXOZ5mL/piD2dLisykIeCrsUFTgtSPw2t9s1
hheeKckFE7B72Ts+iPNLvyY4OkSkyrg1Ocxiq8ifELYSvjGxm3iQPxCIRV+Z8byKCI7DpvZ9Scgi
/zntYOzstVsWE1ZM+ZpivoBsMoW/mG67dZAZmJqGOpQFwXtEgxc71a8mm3UcfPN5GIagfgdzVuXa
lmbxtvdeiol2Bz7ebzTKRYAhCmpF+3QsPklPHaG9wn4KHyOi/jTXcfsUWoubcMOXUmBO6dRg5Eer
QHcG290wPBa8K3JIlvv2gBkqUa1w9BDU0gDB5RWr6Ez8+WpS1b07G8u/UsRgKNAsgd4ETRzg1ZcW
bhbfYJyBlerkQzADZi7K3ZVrNN5yDGNxc1MSIPnvxHAbfjnSi/SI8D5v+bxP/sa5pZD97UETc/WM
mbnLgJzymYoBYJBXwLxBVwmd1tZLGIEeBrWtIWBrORFX+0bTyDpQ55/1ngmZ2HBoBQLlpD6oo96f
IAH9eVVNiHWPEWCmVm1Ju5oLWjNDvu7C5tq23fB8if/+DsmBqakLWySiCHWuJlOMK7j34HZxKhLp
eHFvi33PriqtRzEuvFMybA2ORTv8BlHx4Wv8fRcvbDo71QkJ0GHea+XywI1oVSXi6NcV90D/5o2c
DhnpbKlAatRxSsLfFujaHUyOGkz1X6rVgwUtr0KSxhgugMtFengr7YuZXsQ4zA4IjMhT4PL1TIbC
sOeD057zpEYrTUCAYbOVpKh9p7+RwsEvGng2JfQAH6oW5oDUsBI/5n2sKU9z430XcRA/Gu012t9C
8Tlerq4Jn/4YGM6rXd2f8Wh5doplu4yjpTZU6QYsPrjKMO1b4rm4GLi7skvx802RyO/27c3AFmC2
Oz9RWJZOl5VaqoGzNjCSfMxzaBg8ulQ5+ZsYcb95OZHAYRKvgbBV1z0OucLLRcS3zE6J7P9GJVQU
J+Y101lzHviAYJCM6cJEKGLcg6rpHlyC+fZ95QmAY2O/yXhUmzOaLKaBgXou+IHcqTjOrQikun2y
khFHv/YBmdxV1BsxtI5kDD3bKiXQDh9zBsPcHV1WsKQm/CeQGhf62UmWjn8yRDKvL7OXBJX/FM2b
ibR15aJMvmQdrDP6EW1rcbj/ZpAomJlgb16NJAyguxpIJ0o3cR7Vx5zldXN4YDMUHlh1AncRSBk+
P/QGsUun+FLApPaf4naLr5TVSHBDRI40a3snO+XbOgP/NXsRdD7Zfr0CM0xidWm5iIstMcrU4LVK
D1fQ7oF4x16r93AO9LhV1UP7ZEVDxoYe3PfI4SbjL5yPuADcGKE252QJg/HezeYDsBo0Bmm1Qs8Z
cNgf8gqhiCFFpDR7x1JUm41CWA7vHbgSMhLZAPbdDQytR3ca6E6sUwfYrnSnunYzdZQMxJZLweEP
pN2QkqQ1IQgzCuZviqfoCJh5ZSbRmWJueVKy1cchOyPL6/rRjAz2LtL5zyldiSjp9/dvtTNRaXWr
WNsQfyNaLMK0T40C4C4MhJcqwtaXqqjjb16Oy1iU5i+qTZXo97OHtxg87bcTuNhtIAykIOCHWbSc
jJAB5sH5xmr/GdcS/q+Ukv3/kRz3QCJ3XcQO5XoOkeVcC//zN6pHeLeOGD3V7rvTRDnhrUDWAQ7c
d73rXGr9qsj5/7TYMJOaoJ7X2K2hRMai0ulLHY9w1ruzss5isUxTSUcpuXUapmRKndcOskWQzgA4
nnZqBPZLd8oPlOPjwkm8O2y6/Vf+4SHpWVaa4XFOIvaQklu/PeDstCCmeP9fopkRPs6sZz/vPSaD
sahMxUsqPtMxka2ROZioyEta1u23hlZUKukJEJVT+errL3r7krHflvewo9w2AtAnwm35qXfTruEP
/ku35aW9bl/s50NinP9eXgJfSnBtHGjo3s6neqXtK5p8ticua8O4lTXzqtVflBU5HD3TMOjITLz4
ino3DvumDZK/jQWgBIQf8nOAeLmZKhYVnBCfjykcBkO2/81IgtQG7+kJhE3iKhFKJtfHXcSIhQIV
B1A02qcmRKAmdIr0O5SjyIMuUwSddPOyq03gYE2idBLQq7jDh3zBbx/R5ESdGHwk0bgCcTF155Kd
CZloVOvpLfRG18FTXMK5HHnylnGZ9zs7W42MoHCp5vfKb5nsvJpU5JHHFjOY/5WrW5nWOCv4Ew9L
oCM8o14t+bRqc4P4mlJXXVEL/y3m8NBvO3K2eyAN25LIYmzEjBNuGCw5cX/AqOBvLRgASb07+oSq
+Cig92nh1hxhCoq311cdkmDYfZEyDPth++ijO8gnoBTS18oFzaF11CN3k1e6XlzqfXczb5IfuIwW
1FE9nC45Z0fH5HGWoHbFjxC4rkfHFg2MJwXoytO+lxL50vfkKa6WLnLp5OhRtHT6AVfoqypEENJG
PliPhfms3Er7vcS23vUvJtsidAHy5d+pPOGrK4zM+Yu6hYdMi6pax9XGEWC6DJJya/3rk2mJxaq8
9MUaWWCNA2E6XszaM5igAFjypPH3JBMeB4fouJEZHSjrMfa1d7jdiSo34a9gcXi45p2rHKC/Bfif
gbph+Z1WRsgwPKoTd4adeRmM0aFDFTjlcBXPn4t2sMOZ8mvFefxphveVLvknwIyAytmZCqzNnVJt
gMCM8LvJhxB+7wuAI5Zc613dYjftrE8oEQHTEZvlxag/ENYHJ1bDxdqSUsoVxl6IutTOM1Aqwa8h
n7BaNx4LLvfZmyuxH9+OZQrn/vQGBbiXZz+bDAAU1IZzfDfP7NKvIJ0PpCk2G/H064CKCd2+Q1Lb
hGUypGXUd7lqLkAzKPodbz+pQp1NI9JqV1HUhpZYqxlCEjSrjsVLT46g6/U5AyEKDb+xX31ydZQh
7t6+7UnjG07RKfCWiSOml7FBpWw8yJZ2m8lVcZbFW4mjuxgy29zGg//0As9VAlMTjYIC6p7VqiOb
0Hz7m+F+48JhZUkpA+WzgX5ZJ5//mWwz2gMlbGzykwvX7hKqzQWzgXzkNCKMZtj+7obmaqjbJ74H
K5IczIoAmEIbaisTKfvStHIJ3Tr/phohO/11IxGKnpPsauDtk4/c3aCoMIWMl/0jAVIh/cP0dlh3
Ya23xmSrIBXWBdOW9QN2QhGOqOPLy0HfYQneeUuIXqhKMmCbb/+GtPGXjXE1l6YnQyuu5CrqlVKM
ZwWw/M02oUsXjActi/y2kYhyp3OxaxHUKqW3V4Bjm7Mu3X9GoSjbF90DdpfhSl+EukdF7RntaUq0
hIqfzSQ+9ODJwNuKAzcr6hYpHsvMAFJ45cse2Ley9+IGXKckuHa2wsnXcmQTGZpLlE7PeGBith4n
qflFflM8gvK7N03pOxaB0jbd53rghpv0YIpMRfc9Z1ITsfzs+xFAPacZy0zYKNbGnTbZoFKG3qaa
cXBuMk8dSwB/ezKyefa3hyFO42FTztPM4Qgr9lDq2Ll1zQQzb4T1NYF48UVeKhapiVdPd/Npyjjy
KgGNjILWJyReGHRneWaNQApEWVOmUlOcv2PbhHCU0GMSQa5cferdahiUNEIbiNxTSEntJnOhn3vs
RWJpCAlyHt4Gv127gEtignZTBjjvVjcLFKSor+JdqGonhnLdLbOvGjFlZqhWgpEeRr9R7HgFvEDY
wKcBSX3HELXsJhE/NcF7DpiETuGU4yGQfEsgPI8itqaUm1HjS3IwtmfftByCqs4wsiIgbRoZLVD2
5bknypzgyktR9JM2UGrujShF/u6fPRQHGByfdl2oDR8aV3T3KV24OYNx01hAK7TKTCNzemArHIpb
edFRskNjkqij5xywiVX8MsUW6InTRgpcmvHR8BTmm1EVhHmDVT9+wv+R3lzsoE5Wtl4dm38oBiD+
BIs6Hdf1L6TlQH9rlqwOxJrJq+e4FZixW3Ra1g/4k53N4Pk0kWQ0QAS6qi0Yt0scE8T/LVW4+UDa
G4QweaDnb5oknS1fGwR/6q5jS72k2ZeRJBgy2FuoqS4oMkkKneKiXwzqy8XbFKcLIw/g8QpRBTZg
vZg9j2NxHIbMuESrlxnD3G32KLNaLHNu+EZVjNrnY5yhRi4NdLM47qgvxZog/b/01OF9g5HB3CsI
aLh/w9tGkG5LLkBfDRrnTniykoVHV0Z7QU8jF04ONZnvUJBTHR1W7A9GPeHGqvErDZMAiOU/PYHR
GgY9AuOgf63I6xtL0JkZJ6jjvMQN1EOuPKphKt6ZPnz49hC588wPzQp2jf6mu3TCCc/Far6B0rmE
KciDwHjMXwDjKQ+E90kn1w1lOZkg6TM9ex6BPl5/EU3mSUH6Hqvyjt8csJgvWOyeU78Ja12mlrI7
jmmp42A8Jdml8MLA2E/vd6xd2P5HbhStuEwvWZqAH855atubNcF2ZAUSr+6oWT19CQCt8Kxw0/hF
BP8KHI35o2zRygiTucM2rzoVPCDHaiUJPUS1BbAl5nkcUGbnXKfCX+4csacCZrz9Ty27EO2JozcY
0CTPres7iTE+DWJi3mnHXDdOkhwhDeRQbZ6l3t048OjX0Hh6XN1mNE90U1/+IU76pLp8jfnLyDcV
LOYbtQ4xUS0/YzoP2TCQwtN20YIzRfrpeyfmp0kKKFri/Is7w3kU6ps62UjEQKf46ZQF4yP24D4A
FCyUQ02SCpHYMSMwa1JJvsekDX4UrgqZzaDRtCzYp0OIHLptiWyyJowVYWYRPy+tttPDyf/axQBG
FzRsR00emoC6HLtnHumprITdaw6OQpshdI5Yhdw1BH91U/5YIIMI6Dz1ztKXqaOBDuzTDDR6341f
fuOh1Pu8W3/0qG2MZcDLjdn8GCvXXXz2mX5MDWlRzj+8732f7qZO/Us1Nzza8snwDuFTxFBgNd8h
xE5ofcuIMEi46DLCqwrNXiESlb8ZnmQmOXXvn6wHo64FsmypymObB7FbnQA91De+b2qWtuVqWQAA
PYbHDn7Sirmk4GKC4dPEaOjqU5YyMq/i9IZt0RgID9gcGUm2jTLHetjU3M86xqyYITmxwovdU/mC
U/Q0z2HZ417KqosoI5wSl2tQZIYutYLKSFBzaij0gkFuj+n2F6SgD4UheCdx8fAgdw0oCFCuizfD
1Xb9LSuVJ2XQURPhoLFogsvijUAjw5vCTpNhcazSbGqYHtVHT/ebw8KLvPjst2xum0PrwCS13Wmm
fRghIYV0ivvINOJnZ4KGUSy1PJr0MGCPoWQGDZQ13WLS+4k+ABqFukpast30ruRbPCq3fWeiZe4y
JVQpOMon1uBYPEFBay/K9RGay/GU9NaIXi+wBpkw0uzOuSoGrz8pWGWTnZxZEWK2l9P56oPvxyJz
LCynKxJtV+kFzjQTE6JQzf0gLZ49BQHh6teQ0ed9wozlXAgp4oeK+D5F29ohIcy8Le13j6JhiauY
xoc7vXvcS+6q233CcgJqzPAk2UGCHFCmzCC1jrF+YmuSsR+cs2NZm48Wi/xLAUZZH1fTE7/lQ8C6
kwwlOC+6Jjvg+Sp31t/dBIiLwjAqaJ8kG6kpUFeMNqatyWPjAIWN9A8ECzP+8sOe7ShtOr+JrT70
eiTQoSLnVFGGy7Zv2rfQ6LlGYqTBaxquoVqrLuspYBxJWYu80M2BKBmw7GVLsYhe2N1D3XAQDSHQ
X7+54JtpR7f3bs3xGgq0Zb1qPP00e4sQk1Vi/KRmFonX8Asx+FmziD4SnLd9tYus6iM0xT3zXB6v
lMRQPXhbeaV4DXDDmZ7fj93x3X7j9Be0ip1pv+TScQ8u4xSH3vyq9U3VSU6JL512Ifx/qq+GSdge
gnlISbayAyKLGP/lWvZTS+CEhGzvFarudjQn8P16If49Vr3QuTN2PR9drr6WNBTnjAfFGusgXdzt
IsJEcKe4zTMoexSgcTKilL2W0X4aGK4Ek25IUTAwdZ4wRrnyA/t1/j9WKKqUn9srjRRvHpqkOb1o
MouhbDUngjqgZdJFca/REjAEFxVi5S9+D1rfJ625veKLZLtlPLt/LkB3Bhr7Aq0KlfKMrY22z6pf
aMpX6AsbYkKcigpZpw4mSH/dfq30Qe9AYIIpf5nnH1p0B+k+WadfP4T0AWCdU3tx1+mGHT7HUp5P
nh20nAGH5Ml1fF+iyEZ7hO0DOSyFQ4qlaYlZc0u/Q4QQ84SqCrolwbq9PmfyrsMsSdnyfGbncILd
M5tIDcEkjZwBby5dlZHygIkuezMMomwHE5c4lp6YrOn2GvKYMsNTe9xyTxbd1fxygNd5ueCEJ+D0
6sZ5rCjHaztxkQqWMEE4gh6+2wG2YKMpCh4n2DJGecGSJWFyDsjioe5KK1cMWKxx14pEmFQQnsI9
Z6LW5ZOTVMK7pZBbraVc9/6pzTgZ/o253abSykx+CGm3d7tWIVSLSwOMWZqSprak7KiiL5omvK7+
ygkYzV1QOAXEobhe4mJAVIAPp2PBUMnjJ9Vq1LTN3BQEE6BxI6MyEdDV9whguZyqI8aEeKqiiiu+
LV4lIQlF7LZBZoDTru0VctUMjFL+K7DsAa5UYWzi8Kgv9Nw2vx0T9cf38ix7KNOxXGgNz30yuy4D
p1AzGgwOEDyPT8QTV6abuQXUkrdkSgYY0HfkSJvhYLmUOWzNhek696Qg3ZZ5a6pPhKhNCAYLQwSj
mMkbAZiaod90ouIY19D3/uZ4vcRJd6abo9Bg3aIegXQPLMRjV1iRTWV3d095e2jWPxxD/Sps0meA
XzkFHMdQ/vQotXtTLrK00oIdMsD2rHM9a9pD+oRuqVJ9iL8jHk4yhc8RPZkFMO5N3WnX1wEJG152
eKuW8Gkig1owNQ9B24SoZK2f+HFqfaubm5+SrFyj7fhuwsAjtAcFe2MrfUs0PNEgA5HsaGlljAlS
qeDPjYAZAwsokgZlLcRU4ZUMjHz5w/lXZKNB+g1GXfHB0PVn5RZ/6T3UZTvDvXMuo3L47kPjIt1A
DEEtHI6CdWGgIIhbZEFXqmENBymaE3/moVQZxHO1SpynSwl+LRY2HDafKoXQ49CXy+nv0ouxtSJn
2vMYGO11RiMX5B+OzTWXwHtxP2SYJWCb1IrgTWBGy5uYMjfsL20fIk/t8WQTXbWqTq/nAKoKc4ho
1cxWuwGmq4sFTTj/YE4t90Rkk2ykttHYQhNSp6ONcp/fqWqFVKgZgXzblTvPWz/DUVQlJ9Zp9XO7
01K/yWAqWmUmQKrvdorLr7fF4/FHNg2Og/47vGhB5yBsYFh2IECnNBFPKBHrk/M6Vl7ptDFbKuP5
O8MfamqiI1n94DAjr1G662B/ZCdRL+xW64qJE1rp+pEYvG6ZnO79JOekNIsk9lNEZ956ht0ogBNY
Fdg38aYAvmyLf6hlP69UUgbRgn3U6zlXe2aoagy3aN8zCqD2zNNSKmuml/TtW3TcGJgxDOsaiSrt
OfDVtn056CDPzC5wYo6qVn2CJ8OvIZ+2QHQBEAtQRxAxPdpw+ws5B4sPO9ijXikkJNVQNUDkjvI3
AYzkpip4hOADpp0XE1sMZU5hnS3kAN/N0nC65t+stO54QZzna3yaK+gJHueHwWzKf0W8AK64XuG2
VJ2GAMnlTBv1cozNVtu7MAkEGIcCR6jVAWCefDI2cc+fkUqIgUMYAaNYzDYNwq/rH/SqWuaY2fm0
xwecmrmfmsj/emYnpAWdjzki7YXK3CHRS0UMPzqMt/5/i8pkpkBFCHHtywhpvzvW/i8kxgjBlBV0
YxyVAdpBSjrIOpOstOCJ83aVyxB8ZQYr6nGxyIRYCiwzLuXXlmWyjrhRyAKMcdJhR/x+rxb2d6FF
3/6faeQMNMnEN4gW/CeLh2TRIGOC1BHq/m5BFSkqkTThK886hi+0YN2cLqOK/FJaLmye0sQgSnGr
Ao1zOS4uudkEEQRUg7Cr4l5aC3NpXQLQ/OU5iNAhd2WU1d3ro/cvgmwgFzB+d9TRhlVATdAVDLgI
Rx67rTtzR1rpQrDdESr6glxJRmEQ3UpwNn3EmPAmYG3+FnUAbrZvHAcrxoO7xQt9IMzQzQMHYXsL
Aoz1aMUhSVvBYba73spvVfs6l5kHSGBHNPdZBGzt+MwFUZ9bjUKRMS51qyZ5v+5zZ/EoUo4OOM/C
l8K7dyc2m57CmJdw395EzXXVVT7Z/OXzRQXRc9f22TV8ZuCnlOqtScBjj6U6McTuDK2azwblH/DC
gYQwPkJ9TsAb0v0M2duibkvbMkcjnA5VJ3HMfJpRJy5941jvfIi6tlZbOJ9tsA2Y1TriCv6v9Zfm
G5ZHZny9A7wYG8NPdGZkCkPyaC1+mnURkrllfDF3BYWu5xBfARMYUhcR7c3QhdE4Q2ETmOlqP8j5
m8k4Uf1sIBbisagtRalJRIqoEbgzI9VqPjOfa5liPIhTjzIX/+03/xoUoHCX0VFrq7l2xRWG7c0n
hPE7YBK0AOeZn1/oWSSgnY1yoRbDnTfbbNT9hcdecRc8nbKYN+JeMgEHitjrYfU8UixEKJr4z/0M
dVnE547wggeDQZjfmoS4oJiocWFTsndITL/XfqK7G4c9qe4ZywAQHKd+qSQoGCDTbSb70jmRLS4h
od/cSatgc2jnKBPextJdq37/vzdnKpZqMMygPgpaIJECllXwu4jze3DMvy6hFjjH5X8sLPHg8gZU
25yyHvtjJyRUtFEZRhJTZgNPALh7pllVSey1kW7j1TVz9XhJweAViRPSzGmT7glq1QOzSc67xutR
+cUYREcYX5uRJizmG23eUlt2kQt6OvhCm9UQ7QgpeWdKaGSKbbca62lCGLR/MNNHCgao/hPJyzxX
QqNiMlP5zZ61c06jazsWVH+KA05Pg5AfsIjMgUI2s7yNNylj9zhBfMaOuv8L4yAhOaNbUGKNxVEk
SDCDLlhbNYu2WP+dWUcQU0LN5B/O4BCpPii1QFhWNcicCX1C8Qif+o5HQp/m9Ck1AZaE8Hn9PR8u
GgFxSUBDdt9C0fTHkQKqZTYQ9okHgfRD2kR99O8nMEMSMYQoyaMP5+l0T8dEl+mB40OZw66RAS0/
FBJinULQi/gLLWxmNAaj7gvC9tiOvUYl0c/Pz8TeTyCmjaO3i6gPwYrK0b6WAQErKcifLvw+Sf0i
zovB8/VH06MCilewF4XEM2qB6wXiB5MveHftQyPviehK6jgMQJqMsjRloG82lBuFKSn7V14VhiHb
H0SOniH8hSqHHARrErZTVONMQ68KOu04mRd+qZbuzidBFMPhkpIuVH9e3priW0lXtErq3re7tE1H
qBBODctLnwpJMia/aq3KmbSdG/RvE4Qy4wGynEuAyhGXqbWpv8YHV+XGooqVzJqw3rVSemGVrbnd
EeNy6K9CNUYpRPzf8f3VppM1LoiTg2E13W6P/W+tJo4G0bSVeuT69TEAd4Gwb+LdJmUYMnc3SHvK
/UZc3FYu2cp8Kg94Ycawny0xQZXs2XDbeAo06MrPRZfqzbZZG1OqTi5CTXYWu2Odtinh/xYomxpa
EzmGw2ht7+0NIPsQpGgeps2wOrjuWSNcpAtrbCySOiWOE613YRd5W2tP0EzJr0YM+9Y9MpSdiDKW
GEVZ7duSC8idH/J5EtrT62+SZ9LawOe5/464yeuWwJr6mubWEsI6ZPnBoJ5a47xZAN9t7Ys1xKjj
+ipNmGLrBfPsn7UdcDpXS/UrkGVBV1Wa8USh1ilL16RMqRWmnlcBIhDzOzEEquBZ+I/NRFuOzf7g
nmn/op80xDbb5yUci0IqM55M5L58IFci/UQeg4Wu23rlk0KZ0/bIybKWfDZNmsW1Ci4WP9OygduG
UvIYiR9u5Vc23DnAyZSh6+tOM+9XrQ7O5xnNqXuYcZ4BQFzT6pDSGcfYPDzWwTT/RAAXdNGTzm/N
RA9yddT5BJWHmwdQEgaX69k2utkhnHizgM6+OH7+wXJFVJsYPUmg678QGM4Ew339khCuBKMw0qDB
UstLyPMvUs0Q0QMyzeQupeKxaVxM3I/qKGUE2dMXRhOArA/HKPlOx8cVfxMFNBTkoRSR7XQQgHZj
eGVIg4TSRVfEFksa82puIKuIErkDDaY8seyvY02WXWXD23pSSxsK3nXRBJtRIP9N1gQLK3naWPFi
VS5HSSnhHIbhNUpp6/iB9aFmZQ8TsLFelwDBS7gyJM7yctoOF+sHv6ayma/dgd0sG2bjZC4e9wNB
fkejh0JOzSkSr1QCPloBQYa0/VpuJAHcg4AyzKXhKv78MIB6wVF3U7SEMaw22Zl6gK9grkCKt/Rr
GEU6egFWc0iL3bgmTSNGGbXrJWOZMBlAPW/5G/N618sRgV8WzM4dMNSvuj2S+vC0GHMHA+r3SLLb
cK5J1L3EEaeww6ttc2mGDfkgntvWusFnbOcErvpKQGWIoBGWwLR3FSzEGK1GaEC7GCBcY4FiGF61
fP8gFK+LuUU5IZjQJUhkKPbiOCj+1jhel8TwWlA6wRLvmB4vXWyS4s6dzYEh/rnJvcQyvf37uxGA
0/xEl5t5Dyn4xqlgFp8HxA6teTkdKD0TYp7MTx0ByE5ns4R/IwyJfxf3PfW7UgfHjA3diQL8ZH/c
S9ySRTjfLRSEq8qTo39W6sJMlRLDirCO38iBVXYZC0bohbr9fxRj1DgJ350Pqul1pHs7YgwO2kul
ai1FfhUZ5aSRZ5Q1vQzW1W13JriaHmvHgCQkTWmvfGvQO0OtDbegOIrQR1NdJWU2YjpNRsgMLXSL
hmjghEh3xo6bOSbPbLv+kxs/rOFXft2MSIspVJuFmpLUt9UJgzUveqRozcEKowENUl8Wpes9lcDN
FXf8We3gR/br/CpcEVDPwl4ki4snPLSt3g6AiZiHMsPDnIZOQIlz+IrgckYjDlgnSPA3hEJbSnBI
GZTxbHt/mna0wdFk0bFqGi0LI2osHE4e5FIAFyLCHytidw3LcbChdEq8YRUKd9nHj7sU7YCZgQWX
+UC9y+XTQclzVTilPMMUSKM2YmnTsWLpkhoM+WcWZQjmikZGj38cR4F0+Gc4VALdWCX9qKuVPeXe
F4IkUJyiS8mgeDZIv3v3W3YqFuKSgxDZSzfnQS7S9NRMNNYizCBk6tVSlv0e5H7JZDeM02aS0av3
EjU1wKCawWamNrsARX2WuDGwAZvgegjvoSsjZppmKhoUQfLxfzjgaQ1ls6Ao9Fkh8MAgn7PMjV3k
ehgX45Dq1Gx0uMmkxOnbr+WOEWZxe3l/Bb3Qm5bbrJP4iRc9dqfyGV5Hn+uTkT3xirbS902qdQMH
zz2Kqo+gJ4V8MWlsFyGvR0EaV4pF0UUIb/KMJIqJPsAPvV3jzgrCzOB7ezJ5Xo/FGL25+Bq2mOJF
kQUzOX83scNg6oxA033gnVlsDZTAX+VDblBazcIUQF9S7PKhlrodYF+3kuS/yPr1yjBbJ/cMjl+7
lBtVYgUvdRhXJZG/WB/4888uCPm8Dr5cu1Wd1qDF0OPHe2L9nGplV+R0GwKP0dd8fM0lFuayaMjp
ARd1ZEOlhF58LBVuV2xxZsJxXGYFKtUtpOliOKqOYiyYDJ8EJ5oC0eC8F+R2bKM5r+BsyRH8nEeR
kzCId9WpkIO0I6XkUdmsiNFlFTHahJdvaICuyfOEjp7r1JD0f8YQuAEX4q0LHUKcMF/ICqgp/ca8
tk/lBNpZDDo3XcJNAXbPoSnSVgEYwM+XpM8pY+ISlWhtifkvAmG5ZbfG9SmoVM3chZRkMl6k9wzU
e9Dt6Z1xkFjQbjJrDn1TsbgZEiQECAsHn+JXAsjpY7EPpLSUhcaQ1UpkpSt4tmUs1QBpBXfWvyAC
Pq4CX3daj05XboOTpQC/OHO5dLIPDBODVtbTvau1iFshVHC38ILT5Owf3KVj4vje9ZEym3iE2GWZ
gwl3oimwrFfJ1XjXnWsV5QPujuGNSi+T5c+82ttzcWkjtDJRLtoDdcjW1mepuJ+e44/0av2B16/k
6rLd4SBA7K8tUEgZAECW9GXSCMQlXHRnBhYNSCOl6v/sY8YlsTvY5nFfLg21KRCwGMZWkO9IbtGk
6hyukX8pyvPLi9IjjPwDICVJB6ezJct7BjDSsYFGoQeitDBLHtR9C2+qXSq2QyV2n+ojS9PfhGZz
N3VXOj33E8NRLsW2O+57HO+naP8JV2pVXoZ+zBo4gCZ6Ml08FJo0qL3eRfLpM8My0LKa4TG1pdq7
DyyWVohVgqVZkUJTNPsMAVfZb0L+KhGWxge6uqPvcCJSEe3RScjqcwnN0CW07nqBpJsFcUC5D8G+
CnE2c6AHdZ9JkUX25d9X2N3avqN2VBtV4EC18KDfPIkP/AvP0de55QJm4V6GUN+8jVnqXs+Sdpn5
rxaCvXJ0O7Z8ZJxrXzHCFyVVw9hb6FenCliPF+hCQa5p8if1b3P5Kdjb0XnpTWJP9WN2W1If68dk
oww8u93XdwQjHppgu14iPGgKcnM5mcEQhvl+v7EvrdOPPgDy1p84chrz3ktNHQ17rubwdnA/NVb9
vzh8VpAX82cj6/vNiRG114GZbGw0ep5vo6PQ+f2bAQvFI7YpabmZkRHniUoZmpMwuq1U1CM+UxL0
3ft8DXjh6SU48av01bBijrwxIK4ZhmwaUKOTWWxXR9D3Tk5WQBcREEKrflNPVe3SvTfgSoPHpc1I
akJt9NXeGl8peThg9D9viRFWzp+VYEW5cdjDg+ZPfQFkU2PaxS9Qcg3Bi2SbQU2DfQBlAAifMI2C
ae4ErAAPyXS2uJRMTwBDMlt9aaJr/EPI1WPspW/bsJAP7OrY0HAtIM/mHO02I/+fvIqrVdwrlf5+
mCno6BoB2W48ng0RxgrtNvCVA0xF8J4uKkRhK0PU3/TeYuU+RbU3HE02llVvDTF57Ub69f3zoe89
2Ts1zfDR5iWDQ1sy7Zp2OadhaSd3RbJnh2KqFyDQWjdXAk9/CmsawqaW52NyxO3T/IbGUc9lwUOf
tXeW6TqDiArQRDrd0I7Z20LTLXElam42R5lp/5XnZBXP2kXnBrNgzskK9186R9u7JGSEOAphKmN6
9W/u2a5fMOoZbO7QrSeTd6Sf4qNJ02n4098HnMMQIILV/s8hoMsILtxeYw9YiHZdo9Lb619Ou6YV
AyJFFPgH9fEphiIibUeaq16lNWQyC2U+n20uZ9lj0kkMeu2BQ32vo9aaWBEYVh3AJ3CMO6mKhrcx
z0hv73OJP9FD3W4RmMOFFt0isX9yPBV4zP1eEJVLSg8V/HvEVzFE8ncDRko=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 3;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 3;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(2 downto 0) => D(2 downto 0),
      Q(2 downto 0) => Q(2 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_c_shift_ram_4_1,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 3;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 3} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 3}";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(2 downto 0) => D(2 downto 0),
      Q(2 downto 0) => Q(2 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
