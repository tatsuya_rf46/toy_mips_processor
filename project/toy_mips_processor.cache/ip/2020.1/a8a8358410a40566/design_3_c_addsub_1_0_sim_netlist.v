// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:10 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_addsub_1_0_sim_netlist.v
// Design      : design_3_c_addsub_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_addsub_1_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    B,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) input [31:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire [31:0]B;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000000" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000000" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "0" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]B;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000000" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UO0fSmAGZhetrUJrxP7/ipTT//TI5UceiYrPeBtEAM2vtD5Ua0nkWu/nvW4hs8sh0k87l1TlZ0/A
gNvYyBTJKpZneKXo1I7IO+cAuyDrKEYY+B9sRcFxEa6YHNbF3meI4lASEYL9P6qxJL+5sa7tOIWq
8kAq/DAhGzQowPmSaKD2dSVYXo6L3g+ZLLhn71kO+3W0iMRy0pAnpomLQnESH+W+A6cTqDru9W/s
8Ir72x70ZmriyDxYMVl+MtLwa96pJUCsvaOwNlajHUhEihiTRDZXgTXUDwm1U82QEJ5cuj7lDR+I
qKNLDFL0w5iKQ5kvBxdVel1I0n6k8uhksM77fg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NZ3JwPQIUy5RIIO866mNWRc2iewl6oeL55ecS4QaV7n+OAVZ4uIncNNx04dJCpKSIF7J0LJ6X7zT
dt7VHpP0nRz7Cfvv9Injk8FBAes537+uDCJ+wl0MDXRYPYXfOYOaH5+27W9nvcTUuQqr5mG5dGmF
lejS5mTcxhYmA49Cur8ixOrvCUA2YjpE56jD44tks2ELl0EoQXQOnnyqArIUMRYJ3z0InpxRRQYR
NaRZdCw6Ec4eSNC32te1+cgsyyzjEBu4r5+T4pNESIpzWwzYYwEiqVatrJo+aVlw8F4e6IqZ1V+D
QQ93id0e27R4FSNaA2TqrL9lDWMZw4lQbhretg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 16368)
`pragma protect data_block
4CY97TKmN/p0ZjX7zS0lY1nUcRQh0fte79zxZPzcjIpj/3bJomQFNNdUXcasEQyfRtete7Ht34Uj
qlci6Bjsg0GAv2+JJZIcmAreikHp7BY24651BnLpt0l+c5B4QGYi5MejIhJ6hEiYaId30kv3XaP1
SkeWfW0ucMiD22rH2/gJk9cZ4G2xNe2zG1kgBRJ81DlLCm3whVYbUZs2RS4ALdqQ3IMdwowpJ7pX
N8A1gInQQx+cVHB5Jeif73SGlsdxi3V/ISPKiLWxBrPA67vpTjvOh3HWNp9JOGR50kyxEp3IJib2
yn/YFqEr5oT7ekOBx+EKF7IyZ5JFuTDiIzB/XbjczCIfJ/JVnjwSK0Icabclw8Av8XirUs1xxlpG
gbg/nRhBouw4yV0Q9zGQX7heoh4jZ3qNuxQJVstJGbC9/v4tWt9GtnlKMM+4HcJ6+rnV0p6buwrY
CGc0ihnWZhDr9GuvzpX3n3xbEcPJjv7aWEKf6y068KMHltd/364ykv9mWsXSj0vkMpvfz3R7oH4n
nl9dTdTZPJCP4EbzibxkVoyIAvBiSeWc2l6Pq/LOol4xz1g8ZgsrP/KrlRAn3XVEI1CNdkyM2qO2
vYQv936xDveM3obZVjCeTi4tkcSs9mW5yNzNjMFueg6e0lEk3Ma39aZxPPT66le2IBCcLFmfyys0
J87km19FPrYz5PPhzqU6TaZuMiwq5wYiTbRcOfR7JmOYoc/BepjjmS27c9fGx3PzhvGhQk6pysoe
so2Fk/8Fi3y3bHGceRTp/MKnGtpqBTHUCDzntl1c414AL1hN6SzswlTlCJ3woBLd3NQNuY2TaVgm
1Iw2Hfykrm83cKSP5BVwU7CwrM9EcteGYUG2ty+opyGfGEVABASWgqJPnjLRQoc2lPrwe61vNa+y
El83zBMFPvU8Vm8V4ZQ/iInN2BF3hCzdVbu1ijnLzV6JzDHkNBN5Bl+8J1IWLEYpRGOqPumqD1Fx
CMUvgVb+6mFLDCc++YWvzh0PUuSeS/e2yZBL/ibE7LgLFJ3LmuXaao8z69m0CGggBsFNPOJiLeCq
wAlLUvfzdoqFdw+GEsTqDt5DFdeILy5o0Z+FoYHN23PqOKmwGvKjO9UmEp8z8G7pQegSk0yzOGC9
eLjAewBviDKTjxghLbNcotthpfaRWXwJ6bwpd16xpa/hfZDeWcvzkgE0bt9ISmkVSpo7MRDemz2y
NUxpEDRvnGbGsSn06alAOuYoLdfXgh6HgsJpwqidjKe7J8YRN7pWUBZnUSNj3cQ3bM/vocjGgHzl
96CWROUOSGfTRt4ePtKhO6ZNw4vVhZ6ZS3hwSVl37XDMDKMhA/p++s2GJV/Skv368GTlOybNSZun
QYFZbsvvXUZ+6HVcLLoeNWIokkncl8keNFl8Nk7UV3HXvJrgCZKfZPBwAdmsjHoDh4LX1eYdOhFr
3D4tyKiNx9Losrt4JQlA/Cjt9beswK6faJUM7iwoo3Y82tKbmKR8/OYENz7IpZc4u7PV1SZR/rrX
sNsIaTmdjxXYEFNIAeT8eyq6w+cqqLmoILrC5/lqAvYHvinqEyUbWjDVNEDmUapQYsCTFLW03wGR
sGv/Um+P+5ADCMrlzx8ifZ3e8IkGvGMlYO9S5MEeyuRrdJNn0auZz0+A+HAaOYtFjMO+P7Z0SLvQ
+M4wlEbvK7YQagKzD+A4JnfAZDqovkCovaSphP7lNnPoU1xZcOMCpUKePE/RTBeiAdBsqkcdsvvk
hXhekJ71VVBeVOgXV1NxJDJ4JyClWJdOGTJxH1M/nfzIAwL8yo9Gl1FxzzmFSU8KkHmutPrSUwYX
BvlySbnX4vUn7Ul8VNYQyVWCDMliMVZpLDGNjfxqIdhPfal+oM2IucTCNMvc4ebjoIq4esxOaYqb
tbLQ5it5olNXIGEZ6n2YaUuuHG9TclCQVrfrFOgEz6oGB+f8thrHZVbDQJpZqYVhaeZ7oAoIjokf
kaLg43g0uxRutyUqmumZzRUybCdU71jPMollYpGMAruJer+XvLqKAQ5tJekGeFYSf8wwDC0+GMEc
a0tLMiC/K6OsWez8Rql6kfZXiCRn0KzJGK9migwH7lRdsKvtaRUVAo9XQ33gQz2xpWQm6DsjgWaG
BYewAeCAaFQO4G/TW2d8U7jvZeq4ROXej6xpPeiPtTEsxUH0127iKB8TDo0WnkGUXsnqFI3PJh6f
783c8U0NL0u2LBSAc83z3AHOL/DXamoYYfKgRjkGC1Kb507KwsfTK4h2jLAUMahCZ8qU7p78OQn1
swTlh0ssUCJbb2J/tKvqBBoiFuszJf+AzdYpl4Hl91fBYLet+Wmz8Qbdcg0vBNI9LCIgYdSwPun9
P+5SrR7SlECTK4+OqRcEFJnX9DhQWKOdHbHR7KsI8znobEIR1p2Gsa6aydbocAB/5IpQ8i30rojD
Ycqvs6Dh/kO+8DQhfSuyPRzTXREw2JLwyEP8HAc7Q77axjyYy2NNR8P6CMOMU6AHdQLUXr5NN/Nh
3Hf4jqo8XhQRBGb1F85FkCipuyqbznqH2orHrHZ6utdEnoir0reLveG3OftkYlSw8Z6KLl4dMxyu
XFmmz9JrY+oLynXDlio0ptIU94F/AKtMLhO/jAykV7tGq5LRX9larurP9KMa4zl/50ZZX7R5z6ji
Wa2ithkIUvaFWqAncDHGTnLjw1YS58W4gXcDR72fnPK5uvSN4ErWBYCEysW52e6ZG64ayTZwIr15
cb41tVKOGWVAD233wZXW3eV6BJmaD1Jnx0/Lt6uO5ZKgR8/ltTLrijq8IXIc8tX/e5pho24R1HgO
N2pGYgeZVoKQe0VdFIafQXO0TL/PwYLhGPU0jm8P0mvAFaG5crwCikLq/GIPBQcwSCv7uPQVNX4a
Hx1nBretRUChbM23+xxls/+pNcfM0oDVI3xklyt5RDtwAmN8l7k/9TTMV4VqeGUquHOjfyLWSKr4
4dnRbNZatnAvuy/bd4mWKayzNSHLPXRc+KOwHiw7pQk0f7hqoEam520OeKkjbRA8EifSS1bJ1RL2
fTDq0CdQi7/tvhJUBwNDOScSwF3HXUcWZfD33YLOVbYTINKFSdo0PcaODf3V3W9mENUPLjjirr/w
744xR/fTPRK8UeXkPVqnNSOaVUdTu81+9rNkldIAa34jggkoxDfFDGsvIGf7vGV4JV1ZTqMnxqNH
Y5+cZ8ZjwGnB/wEG0/6lFkNSUjuoQZ6AwsfyuBgJpvv4gU/vbt8cLwyUkCKoRmg2bWFwXaquwtiN
MdMLFKkjby8SM1DbCxIuUOrlZEaH6J8VjLKYYSF2njcULK3JA0W11hhHD7R3q8zkO+WF0Zm0CN6t
lJWbfME9YwvbW55DDeWQxiL40XZEGnWq8qamToiF4LyeImgo89Ti8IWMF1A4hSKx0QOxELHMM+bj
e5RdeDtTcr2rVJG3fTMEXWuVgVkuWvge0juh3iW3C6xUZeomgQQu/4eZkzLqSslmV3C/vZPx2DN4
dtKwtuyxruyZbRfc5JJqtEYrDLUCtS513GIzAbHG7RrjGI4T2xmbso9YruBkroohakR93v4T2Zkz
G612+EoKerJ9awVp7P79skRzr4Go6hrR4DGQgpMtU76PUvtjghXtM6tQX42rDKPSvvGX/rX6xGQc
QVPDr+fEZbzI8v++3Lpf2y2Un6YPSe2CT9Zvi1sCibD9ye06JCs/XI9h2P9txmmBUt5U9DiFQQUp
a2liiL/ZHQeYkFRT6F6HMcHyROZu+sYRqFeX0BZLtmgAqSl8Ku5CuEzxSXk4OEWONqXVNTpZIxZ8
LSL/wlyhGkjN1BLr6CFxQ6luzKpr2Fxuu3wDY+m9q+jSkZZwWbrFKQil+LVdpcVTIQr6vpsGG3/w
66YnyyVtQGLeqXoWFE46TZOtLl0B2XmqnKFdDVk+wCz93vZlw6iDIiqeHFLQMqPvjIO/VMTUCaNX
hcs0lBJYVk5tgk/0OFp/LEAT09/LoX0sCbW7fQGT9D5snl/lpukno2zaZIA7VWTJtdTjIYmklPbq
ma8LwTiRCTDrrQFkJ8RSp9TUnePmzFh0ghjph+vPVVaatG/KEMP3WzZuPYWRKkg6utjtBS7aoLV3
doWKs8mGVCo4RzptleVD95YUqIXrEbBf+G7mRb99K+tPEuPQnT9CVyyE8gUtiMNkc8W0urBLP343
BJCZzuiOIhsEmFLdu/l3Z8fUPM1zJQ/N+Yc4M9qxt6I2mVSc/OmFeNxRIE7zYaa0BfNMpMuZoZ0Z
SwGU9lZJWJ5IQaryO9kd2XRyKjnwPGxx7/rnhwNG/UIZkcDDk/QU2K7+1oIX+HZhVICvo6tWDdsV
JBWdPZzkjdWkwutLHiLT/V+3I5Ao8UvKlybA6rsJ96xx1F5iJ1DpkuiAquDA9HlGFlBp5yqm8GGG
RqlwMY8VC5ZSzpdRnlH1rfjgTudkxMeR6OvVl0RH/OEDoHFrikhQV1alHqoCQWywtgXBIKVV/Ugq
5vaPAcPhW+VQgKBgjpG8iKtZVz1hjNSN1G+Uq8AUpvlWCv3DAoIiR4i1GTmtiuKG5ffIE1ovPZxm
RuSGMuanlXEZvkJERx+QZfMThL2GcCVUg561fIUcUEBqBH/K6ZgscH55HYKqtdFv6rCxPNJCLb69
PC2HEsfim7Nm4GK/3pDKcrsAfIMbp0VWD1+yqcahYczeiUAKZc1CPaBF6q6sPO63wuVuycHW7rJb
LWRZp/EEXISEBKfUaWj4KZylueRL7kgJKRGXgCiUN4gbMiHNULXV4I2C9mDlV/ohRrosqKAr1w5U
izadoy7hPbLR/I/7jBJWq8f3fxWaWn1HZMDcYR7Y916Jap2th4GcOLvCtif/koQHVmGYmo4E5xIV
/cJ3uqRM1Kw/VedHFgqPzwrzxE5IqcTpRi3CN8MTYLVgy4CwMQDqlC9P1Eqp5Vw0tTobFAgHG6TH
B6o2IPk6iprHa9MPivIcl2Y5Lt6RTKYzsXdzxiojzLU6KzNYsKCZBp1og0UCdjXLOOFw65jhvXco
igwqCNrDeK3YdJV1rrrw+204WXWfXtcvpsY6d51PMtUMN/x419dCCN+OdolWE4HIpgcMYuZ5jU8U
N2CQQmdzNKUB5EDsabwh+qVZxOQ/udVvhrgJCf9KDyM8KmIf3vXZe97GF51XDfpyyubyQ/o5XJLS
uRj3liCX1JjN5dt9SAw3rgFhiyfGdrelQiQzuGldJyNk57DTtdHDwbm0jXx2hxEMtPxqe+FbHN/T
hngc7iT9vy9lIpvY05QM0RfHIyM2z1LVwVgo47TR7I9S2fH+Bu+m6WaCCxi+J9XfCl/wvk7weuda
c+7oAafPIzrI4g+Ay4Jnl8AQlyVnW7LbLc6OW4pyoFp56VlaWH7bhTTdeV8Plm2QwVbb9EWYPVFj
EfS5mb5N7evuFmsFH6e9CbYEnbLQ5Kfilnd53Z5OaGLVAKJO6IRx00UJOqA5Tg21s30pZP/pEf8O
0Yi5NlzvgaORIV4/zEqoOhQtlVyP2lkd+WQ5oDL1E6WQYrWrBvOpXh8WcPXZIyaiPD76cmNozN63
stSpvxuM3Ywh/tlASWO4nP29Fe1R4ONFwErqYyse17UgEy9qRCX5QA1YtJbRI3O+svbikCKmCLiN
sTp5D/FawwiMSjfsdY5Ksz7NYJpB55ttE0Ep03UllAKtO+PiJkkCJh1OqZ8XBEk4h8QOuw66F0jO
Y3s49kmY0wfd8NR5UuzH12sOBbuF16Fj1xmNPSCjmaC0psLcpRhgxJDJ7cluP7vsMl5JpUTYzgG7
ccTMm8GuyTEmbO9o42djqT7BCqGAFele/qmmELD9CslWj8mDxnI96yHK+k4yxgiyzIw93pEqtcXo
q+oP9IVnWm0fwKXZpalQUwPLeVAKd9kDnjD3xU6FuyeQxmxnLdzQQhwN2zf7l7149xoadkuUjvoJ
O1imStFwfoqx/kZyk79tf8S+hEu4INX4EmgOp3iSiEkXRChzUpgv8nLC1yHDU2CTwhUA7Ns+Q1vr
IoPWS5Glo/1/1d49ygTQBPZYdzl4spBSMRVo2c/RKVTpqgoCVMulhfCSlfNFEDv4b6frs6gY08aw
+0k8WrAKM1x++46DiaKK61qaj4GnUNvBB3LnZ7eqFXypmEpvgGDgbQJ5DwxlTiLdcE5YzkCcrNpF
GImZCskb4dEsJ2iqsRLEZWVVh75rFrrYfP62omFaBP7XJ4jYwTQheqQNeRUlIx3kVokIo05YrcE6
kklO38GvCLw0zQEu47fpt4zvETm55hvHxNYbIzDh3rbwVAFO9HsBk4UH+Qrlp8m1SWGJr6CKjDVR
d/uytQQEA5eL3U/RdLwI5wS6S3zbF2vuJ2BRBK9oTnhByiAzTpzoBPXpw6L9IexP7Yu9fHxsge0/
f4EraxyIn5pk429J9vZz3I84/suUF8A+SbR1+FwidZKub3r9RneeN0ymIu4jRe1dyk5Fta+gr7mX
Wq+TLHBOvkNYimMYB5quUr6Bn1G3AQWVQGc2kKxfy5e8hVmkqk6ZTNsPSvL7BNYmJPJBRUm2TqER
WGHlIeodOqZi3/Ji6mZxvFUvzmpa4/6AVHWyq/mwjlLlQoe39FwgiWrX1KQXzRySyBtnwVrXQU8h
YoBHjAhkVuFkHMz4ikMcAGv3aeSs2tyv1Ufv5bkZZPUL5sK1Sl2s85P3FKWPvDDTpiHboili495y
gJ2bmhOYk/oSadsyxOlYZBaI6FBdz4qiUJKQvB4mvvc7crD2Y2vQNENf0hHQdKVOwOc3a5bTZAG/
nAO14lZ1GNu5+ALriwJNyYhJZlN6fitHCxqzYzvdvrKdx4I+nFadODm1hjvPwGbIVNyxmAsbsMNV
WwqF+DNwcn31f9HQ+AoVPneEJyT2l+FVZ8HnB2j4hmopIY9/u54uJbHjb9U+IocMfV8HT2zkGNIi
Z56zVRQxisPyOmiFFbFGKzJnIM+BY1usNeEOehTM9SzYKYiEIXwRndlhepHYCdJCXlq1ndAAugO3
kT97n1TjEpHcd/Cw9pr/33oQIU7m/bL3ONMmwhOSlkKNfV7Dyn9T2wuRqHTtKpHjBUWMaL2wNhqM
xvBrBKxYz1yPHjiWXoaV3lCB5vUPZjulB5EbhClmhCLOW7KnM2sN4MkMZwsE4am0RkCXraB+H+ba
k5og2Tvu8tF6xwKkPMrVhBFvkrpGG20TLK+MV/akkTQZy25jTjHGFwQ3Jv5ln0eeU40K/W5Nqjqc
vE48rxg4R+m73rvYiFJqdqNkRm2TM/88YoILDDSmSQwpqZRNbp9D3p58uEuakvnZiKYFduqKRBJ+
epioGDhSgYAPBbyi5L4VDi3E8kuVDx5w9PfSZ7ou+KCm3D+9TwvHo0Y+0LAOdkA8eEnOhke7RNng
N7pqWq/8u1uwv/oixxopbDzfqyTQxC7arnt9y4wfBzLr1yMHLgXU/FNgQfcC7SS3XswEQweNwarh
6ahrbSrt9bPv2Xn68kLmJaPbatBKstz41ElYh+0KyJb8zIy+R6zqndA6YlzGrVhvWEANAf+O5hq1
8Gk+rdK2rlfbejyWKmyHMBBfDL4Gm0aKey+uz6WYnBAABWHCcA5I1sIB8sZuzQzaZj2vzvFjbmd8
xkbwZJus32KeGoNsfvJEaECt5EHxJonnQpC4JvmLa6hN1VRLAxzBgVeAr0yZbN4xwUNLz0hrIw3Q
pn/Ka1u33dJKw/ymkGHiDi33gD4CPNvmxAnpSHLkUy0imz3dsyuoaquYgi1OlPR2CFBh4w74LP5c
Lm4OlqslJK2E49J6EH1az35TkPNkutjriDWDhdEU3hv4ra2CtlryN+VD1Z+Y249+eVKi4j3nUeVC
jdcg4Olxqp6B02Y63sHTfYvzUBAduPxu3vvB82BGIMOgUEYgxQlAp3dzff44Zr9chEV14J/N2/+X
OI/PzFUKpj9NHgc2x3UGDxzRiCLZVCfSYxMdFeuwXnOiDz8oGWYTCkRebcKp2M0vFYA7QRdyFKA5
l51UdKAItksT3vQOG8azh/swfk3hktAzwuF+tfGFmHptsFis+rnnQO7ivokcnaCWD4E4RzdqZ4QL
YXlTbxcVpx0XyrS7eAkHoEeeJBfHl8q8b0F0zpcBaeoin86rGww1s6rytIxnWi4tRpK4fWnHIEdJ
ERsSSUqDuisLdbUMbAzc88B38wHyxmjkOabn3HZPZ7J49FZxY0f9EZeWC117ElKLsB4buMcxlFjb
xF3rde/ildphDZYcuVWC7pD0FEQrV5yLKbcKQj1O829LqF63iw8+u8//jA3aImePcmn0SpjmQH1F
GOq5F+ND0QpkGeBcJUob/mrGvvceX2v6+KJ3THWoHz7m/q1rP3Q7t3k2qIQNzTq9xdNUuDrllLLc
0XnJcpFj6bucmJcC0QoAtk4WkdQrh99eB47GNtgLrpGbfOhOxkYaaX1NFjcxyY+oAhGPr8NJkW3E
ckILEJ94gl0SuE+x6JWlYq9Z3ClDikUAOrM60C7NnFlWspEkxFylqyS5oQTQ3sY3ADrMwZTSM6Rp
pce+iDpqjagtxLQy110stcNXRLLNRUQKw37gqW2Xdw6XUJBtUOo7i8BP9J1pkrvoVdoJ4dsmbZKn
Xd5yuUlouhI7VPhLzqVDkTrmlDd12HQ8rS4VhXWPU/uolqmmLXLgJa3chS+D9O+OB0fOgVzYPqhB
RcVaT6qRrJ/F6PQPFH/UY8/1pYoAUo9//dwXsahWuCcgJR2T01hlzl92EQ4jFVe4BtupUVh6fxLx
z9Fw0HlR/okWBesbUBuHAXxjeMZRs8xKQGBYWCwDFGaPJpkUCg3ZwydNYhAL3II7keG8XUjwHDWg
Qq/9atE9XE7UVJLkBqEh/ufmKA6vPxLe3aTVBwZ53wogFEyOEP66UOqT972y1SuXDRRde6nERItD
ZIhqftxL4XMRSMOWZ8KO4TtMqlH4FbjCoFT8SjkB0s0DlV0DdHaVuU7YDYDhVwmdphXIC1sae5HW
Dg+gi3FB4wIU9DpDUbqRlEUd5s73j72cUGphfQy0GrNgTt4+2g2bx7uDIl9d1MzuFjBQTQ+oo21v
S2Rbmjw+1f6mXRpnJZXJXPTr6gThedIXQHw366ItIkODko6MTphQXuDCN+u/o7N09WrsOx8mcf75
YvjwSC1n6YM4wOaY3XXr8yOXtS3uGOYKSmXPmvYmV6OqUiIuSU5Ce8q/imd769iCozVdEs3sR9NZ
YLIfEN7Fiji5IHe8gip0gKW1750BSabYAhbQQaKZkmU8N86cstjpfOlo4cevgWaGviGIfyW5XX76
eEaRGCavQapHJMeH9yL/vuOs51MMGy/ulnF/D5JxsnUnYVgmmft7BbWiRW4zq3Y4orot7Xt1LXl9
h2nm/BWQLX1qcKu2eCTPhVTgGvee/6lH0hzwbjiBZEDrqKf7MA8WzHNoddisAP5FehPIVaFinZ3J
VHarTDO6IfQI00b1JwYUz/mM1fT3SRz28B9zhtkPUXSFkJ6h8RDCuOZHWuhCf1GOUpJVJuAB/Hi0
Zf5XQd8Pe+0fSQVckyDA0lZAuPgeLc4VNNeolEztXIOawXOLMWQPUoiT65yNfYiHP4miLEcmApFj
byEa00czY1lG+6wy4SG8DZPvjY7oC831tKOdsYg7zZsd/R64qMPxAuFtC3gsyQO/+PehQPhuDqe8
ku5ugNyGfLw0EgILl2IuIxvwI4GJBciNktOEBmnAcG/Of8DvCMpKbH6Nnc1SsC8xRPKyrDWnin91
7zKgn5tkz4XEap4NbYi0mCPqRiHnE+vtj39wcmVKIBc/B4BHm3J6uzGJhp8e/tXxcpn9i083TdeF
8H0BEo5v7KcRr4OjM6XBExmHXNdYBV6HnKECdFrTNEGKp167Br7vyX8ixlvnqYXQemI9MtW+cqiZ
Qh8D5u7s3rjebBdCUVxXdANY3Xeak2ato1L0Md592UM4NYiynacfwDR8zVbmfTr36oppMgELTALQ
B/hTsSZooqfn+Tqn6TfwIAHwkpfKPuhSdhyOCwucZNVopyUyRbWSmnP0nlXn9D2D5AvuT62F2lCr
NM5zp7WDh57SntpG99eNXjlkMDPgFkqwlKby2qg5zSrd4qrJwabsZ+bnxQ4tgLXqBUEZth0AAYMC
eL+GecpGIUsS85Fjtn3EYWUuZDEfAJ8tjagkHUQErm2RngZFEhAe8aofpr8M4l1Yc0ndDqy8PNes
mizD5e+wHG2ko1nDuOrm2V35WZeVQO8MfscCM5MKrIEn3XoX8xEAUmD+aGdhu3lELATWqIwfTugR
gXDAIEv+XcHqo1y5LlzhyQw7SinIJbYLAveNNURAwDcmTqxIfIr0ADx2EK/7ORCzF48u8c9tW2gM
8kmtGMzHN95JtnmJ7dvpxO9h6/0qUvIkMZo9vRcNBus4+gx7j3eR4OxL1WC4XFWlH3ylpBDSwm6u
DAP/BS6UJ/B6CSFnDqtoAPMfnQtAIwGfT8l6TahC+6hCHtyEjmsQ/aeCjWYr54Bq+FgXA268tlKh
VyiW4+JCWwnxEZwWdul++ujBOE1D6CIH3IEFCLIK29pInB/uWfEIaU1YLFOzKW/kYL7Wu/ZJ9v1G
h3+UCAv8SWL67280HpaIdxNQi1wgFgsnz3j922bgTWejN9b+D4g31R6TAqeOh31clDnZ3tsTT8Fh
8SsCZV1NvEWQPpo15++jWYk5nkqcXkUAqKJa9w0CteZS2+MF8yx3X21+dKNGX4fmvVNDLMjiXAI0
sma1WF9KjUHTv6EjKIjv1Oe+FOXlA+qcXPxr7ISMK/m0Cp6I7Jx8IdcAARPJtMl6T3xohnK8WQRJ
JNj1BNwItegz40MDfKJPHs3ocbYsP6HXoNrLi3oc+NdV1J/R0Ea8Li5/eLjr/NrcPKdCoIuIUOod
ZTCgxMHgvkrSYjs326ALOpsLlgNbPZPYq6LWtpVwR5lHZeqia+gNSB0CVJr5Hx+SEIEa7xefNtTu
F4xq9OlA7MF+NDC95qOXH90fN+7S5is2j5jCd6IJNzIFTNmcGayE+nNzECo4fP5WyHIRYCdoEiGc
Ogq0ouikJPZGLOcvESvD6KS8URG/5OrGk5Ou7YnCaYRLYWezShx3mLn83kH8zznss7qBjr9/BWGU
aK4dkx0eS1w/fursvfKoIdv4sEKt08XzquHbXCYB/ZxWTt78nFzAel9gfXwYqAhyiUxCxljvXcTT
gwH6PM2bzDExrcyfUV2FFF8LlkXTA7G2kL3NE9ZBtBQwfJHIRABZEMXofkt69FRq6VMEQQHoe+28
NbJAjEcHTmwhSm4/0t0T9T62J0vXRrFM74Npp/rWUbD4Q/YPkZ7rnMjgZYLMyuX2SCo3q9lurSpp
21rCUnoBZyL3zDZbU/gMWPGROQ6k1UZ/mlvbbi1HmNGJwWRJnqh8/3B+xQWGjql++g1gV8ARfxx4
NGZWcGtiWWWLVjYx0qCGpXahbScT82QMWfMn3j+8a4Nm/ov5NPOhlsI8f1i8/RdwJ1SZktZBH5he
Lfi7cglvMBoQo9KKpxbypeodebGD2ag3i8mWpImK0VCDDqhVpciMutX4zOORqdXhiRN0JPv2nw/G
3Sf6wBY0G4zBi0vVbGlmYtr4pAxJnNgK3ssBKteDl08JTD9mG+ox66kEY7LtA+kq/HH4PEgSKyCD
yJbshyqrTud+sZh9HZB2BjSbu4rA01IXTBBue6xPFlVAkvzoke4houHVuDlg9V94tcCgLeR/iWkv
hl34QYgqiON0l3P4G+eQdQp8e4Fd63tAarmRzfqAMPzxxjGaJw0g8s98oZYq27s7CaiFYWyAspI0
sAxHhdwN/NMX4imndi4IhuTwWlN2n0sOG3xN7M/sSM20csG00ReTL7jms46iOZKk0tW7H1vg1hop
tbgRIfs1LXAmMISgRQPuJnbg2VXYEkMdHvEec/7mNcouRXaOp+zBLQzc+j3C8/z72I8ovEEpM7CN
HAtURdhxQv7ZYea3mEuFtDGN2Ofofxw0FCDdSursIhL8pqWUvvd9Ate5mK2onqOzXdy2kFF3CWuI
w/eY2JpySTqy+T1xgdwFsZmAtsXHNC2bnLyTBDL8u1jfHhz4Z7AUyt19NZ3PQ32fNvBLdAcNrItw
Bxgz9lpGwVOLyWJxYroOSWycRZMGKMK0nxPCsd0z2ELE689HakvyMjjtJwcb+jE5IPR0saIF4l45
G+P9XX/uEwfJlxZXYF0+MVu/tWEKrGfOCrl7RheNHAz1eT/hFPc/wPs3wdLPo73y1DGCq0n3mzIh
CzM/kPF5LsZkJ79k8/sAZi9+ZHUj4Ryj8T6y6RKTwc3Q36WztQyj7Xd0jXd7RZvea8d393PuGftb
4b/Q/bsGhtsF1AD6XmFFCDfmcJV5ctGTQTpCvyNQUdbXtI9JX3ojFheGIlvlqBfmqEty3oP/9AGD
7BhXqTeHuXPZqTnd+2ndZoL7JtajifjdMd3j5/amdygSl3RScN2vzOscxyhiOouI6lKyPJviVMlY
RmE9vqie77q1WohUn9Zq9JaBkSoubhKuY4ar1Kn6coicCIhvc6/omAfOFZPULPjBiDN6KjzDIuRS
uKPFL/MpFnb7J+YXnTUm8V3XSYS5uf6YD6kbpuQ9BSnlhdtatwlpUqCgX7+8EResUJ6KuF7HbX/n
jjokG75ObBxoy0BkKIT9UcX4a79dpA6t2Y9bgSgvYnL2tT9NC/JbYWfqzH91O1SNPEI7u9rOtXSA
MHnTDEashSsr02CkCAMwVk2p/0BuH/kh5BEEYyM93w4iSusW5VI8efV/vinyuV6UAUGO0VUimTQa
FzRdpdEO102UUcQuR1Tva2n5rgxQjoypjJREJGM2Pkduu8vzDOnCVxPboNHN8Nh+/xR8kov4az4Q
Qi96QwrFyPKzL2k54SFrcaNHBNdWU34YlqGvH77nHTxmUxyTzkKib7bZPOI4hbYfxJIzsdYDfqar
gZn1ORbO8kMwC66WGKqNg7hsTmVe+LkSKKVqbc0Uq+UfqiMaSiPKcsqbMIb6cn+Z6QlJrgBS0lzQ
+53NqlRRlCCX80voYGcEBhjk52LXL/7qftwzMBvzGsg4/RInbdHl4i8qABBiyt1AiPipx9DLbBKQ
X8sKrxFDJCnCn/r3GayTfW/UN/fbLL/JyuyOpXGQ/CUaPhOLt4RqMGYahHklv5M0RAIMAeka9pbo
6soG2vKWGq4w7GdBc+JjO/CE3HXVAW3I4KuV0LVRXX3Qav9lZXhupa7851N4Y9d9gQNT8Pmvn4Ei
kzziJhNxwrNeO3p5Kbd6s/ubwaSRk/5uArNLoQ8Nlb0dbQBzin3RCJ48O9hNEHuWWE1KIbbUsPc0
RTk6GecWlkVM9MblGXFn0Jzk6foIrFnL58bDUyO7hxsmLeKnxh9g5gnoTXLfJ09FSvirNg5qptC1
Iy/IaO4BKyeSEV+90VNjX+2PZxYSKt90SNdi9mEdmX2HJok8KrQClAvsxamAm0XP2FFu1EiZRtqN
6elQ8XtYI01yhjo0Ks3FZ+0RKWJ1djdLMyJ9/M8UlTJE9Q+7NgFB9+76rHR36QVWW0j0VxuVywha
VAOmnEs8Iw9/4qVr7SqOX5c6URC7AjUVdafpgI225YRIzDE8TSz+Et/ZjDis5sd40D8Zt50exfMK
nabp6EpqFj4sjX6XVRBK9tflIGIgierDMn6vdc5Dkt6APeGXh8PYLXmJ85o8OcZ7vO49Jj5znFwN
nZSIPCKX7aNsG3SCb3MQfdu+7kDHV+WppfABPOkfdqgmZlpxZ/xmzvGMwgtviq+GVbejZFxOROBT
NwKeYg46rWn18zSlkMAN7a7v0W6gR+ziMUHMHAKzntqvype3RKEY/hqimdSX9QLLNil4l+2AQRgw
TMgSbTjFLHvxkaHUIGCVE15EWj/YrBSN+/clHb8YWiRCC+ygtW38n5Eiliw8+G1EcEXbJ6hemxHA
PzbNCXGlLRJBrFPOb4M81uoiMJB2pRa9t9WT64DfF0pceARNQHey9xS40HcQ512Q0vobO6+KpqG5
/gO7qIq5BdE1nvLl8WVRIqzgmRB0rBCL0v+CnPotpugG/UN+NXyl5GwATyG5k/IGA8SW//GodxdQ
TkUDvudkOyRQHeLoq8GRNJekCpfUnM4pHxG3Q6w7tNny8qNkd0lGrOLzukdhH4FEcG6YlrgMJn1+
omktX5u0CV0XLZcoYZK31d2gHKNngR5jAPpCFN6peDWMoInUHYvnXEMvJbAfTyBgHMlxCwggWNpC
wtyfMfxxeFDI9dn152AqtoGXQojCQyEThcvdsP1lLV8xl9+WZQ+L2XmUInmyzjZ1qqxHwzhsiBBL
/goKPA08XvBNoK15OdRYShaCJ9UxxApkLfsVhtWo/lu8cFvwJfHaj5rdvAwToV/erUWrkY1sSskP
9HwEjufUsvZVgNnEi0/8/VBKMWwI6B6gn01oaRDcoN78i4zs957l06h/nQbbYumgeZCx2f+oQzNN
24s+7lcjpFfasGicIN5uur63He+8+rzUDtCgTdja0mx9qJibXVAMmIahNG+TLrDrAvp29gwS3Sl8
qLIbbfFYRBY7QIerebcpJaqXd4kLsYuGMLEHGFgV69CG08qK3SD9cjyGNkyylQdFtDjRk582gOO2
hn9qKK/5dgqW+//hH48dgOJC97OUjlrNPKp+ddurA8veHncQ8OVSmr1+45IxgWXxNwdgKzm6AUTt
+QtYSpuVOB8OgZGnaxyp9Ry2dvsQO8csSgK6xjIRzwonriW9np1IAnPPob4DEm1/fLCLCUaNMX87
Et49cWTNKfy4r6n4dYCcHydCH3P7TDXBfVQYQXJlANG/J4e/Yc7TNg8esDZhR7Jado0aoN/Om1Qw
hQWluiGHCB8dN2c2FBqoVUnQE/Zrg5Kk3Fx1rOxFEv3xcgla2c3V8edzxrm3/StlcPdXr0H4oO3g
kxMiS/hPPK8eE/oD/vodiUeOs7BkbhTOWgUzOcS9VXFQwsDMYDMU+DB2v5BrxU86l0MeIY3cVfwM
oeM7Ejw5U9LzUY4PzbVOuiCf2LsAjNGcQ614LIz791ABfV6VUwyuXSERSdCd52NUf82+CeKSBYsT
4lsqX5Gv0WCVsxqiBvFmazZBCJxkitNs00SleeusLmEmFupcSjpJQ1dLJpkUd3eczfWI/zgpqEsM
qbrY7IGAMk5hW2x05fiKPwUwwmzx7xjkDQONJBQlfffC/WKlMH0imkbYTzsqR3j3bjguq4TZBjF5
FjUQt9UfWhyt5a32fUuAsPCYaVfuvtoT9NXvOrqkEXsuuy6/I/4grf9NSWpjULJpan0CPNas5WK8
XPmv/FAelp2jHA6OdiufCtcR4RthMeIC2qIewP+HdAjqJ5AwXGhI+DMS8d/T9w32BGUAWrlOlxiY
jBkDTLfRUEU7ZbneshcA9PTuGzmx/5w3r7mnBCBOdB6xjRUZcukWSmwhVEqtFk2rCuXaJIGqhznE
FNuhm5Xv1i+BvD1wpKVRiflsNDX2Q9HOqYbAun/N7yiVu4TihPvH5V1m5YFU4l6FY3jepqyqU2g1
5v/FG07eHUw7wRj/f+CpbrPwbgfXJ52ikAeYMNOSkf46FOYg7f78E57gtLB2cr6r/NtXO73D+826
zBKDW7mAqSzCL8fKyeJmSe3twp+tNiiuuVGEapSGUNZ4seo8KVs9PlfnaC2JUBjlVboV1VV2kRFo
7Jbehp8q1NVDass9rksU0KH3V0iflZprBIE/1EQnNdj0RojfcCTVgzWLTIyqz9ktiYrFvMSnyahF
plpYjnQ7lknvXsKvZlaUZNPV2TJTCUUlcLUSBAMZ316ozIyYzBuB4KFK0TgdUlO4WlsBk/Xg2NQf
qgqqrSODt8O3alWm0f4ye6YvbxbC7dD+TrzKofp2Xq0+tPD5P/wOmy0J6kBTWtgGuq1M4/VbH82+
JMPMi3xHNs6enZ9WLTHvSmnvIZxAh50qmJV11PI7Fd4gWpwULDIEUSCdx0PbeExmGUmTNJ2STbD6
KdPgB5vUIcOEIlwOsxKUCUpzAgYPlrrTLpy7ekd3tsdYAzeuQOORq0eiDszKbJHAJmUqhOT85cWi
4x/GnfPrD9rVSouECq3ZDANTmtzvYZJAexKh87BVOYmpr8yFle2pk2UFhp1xXtHQIN14aoe6yxq4
aw86vRY387rFbdgFXM1OzzUopIMbjZ41GqOssf8wJO9BVQyFvj7MKwstcelyfPPPjwApl+raE5fp
u/OFQMHZrJjYPatvl3xuwsAlI85D4FOfYoahcjZq6GJu3M5DdvhqrBOx0EghckeQA1YQ0cNgZERW
WLTno9bOCtokj7NpX5KxKFMPaIlp7BTFKp+G/e4wHzGtVIMAkFvMagWip17uwO5WSMdxCOs7sNvf
Ir5IzW2ssdbi5XclFWC89NnWEfgJoKxn52wBFhGNjfoq7vP0RcblEumKYFTZmNLx/cUz7WDE7Hp5
SfP4QhJy6QRclotBIFreDwNp8hmbXyBt8RD2VvqTrHz8wYz6vgEp6tGswA71BepSLSX+RkQpMebI
bgDGQpJMpUtaRH2awGa0ogA3f49xSfCtSQKI4cOhNKtLhByOTpTTT3OALKOjnIt2YaequiX5SsF1
yQYyS83i3wQoCrFFApx4Wepu4SQnHFCObrYocQGSOjMYKbIdPrVuGichH7NUlqsjTgnHAWwJ+fDA
mqrqxpssdI2illk+lO791wrX7xGuh6picXShscyMJ4yR2E2BQkDPCMLJRT2lR0RPTtP8nv4/gKVc
dQZSZEmETLGwKAWRscBCpkp8FSvrZRi25hka39vpCd9F8KMIzjYlADwvutAWxPqA+Eba1x0J6M4z
yoY6ZSuHFr+AR0s6k1rxNQL3NbJuJwtQ9Ngqh2e2CHxXxLDn7z/jzfSxr8ZEOxB6u2ox9BLg7W/x
kZnEPGaYiAT9lMp169nNTRlYGL6x56hhOgkQovBWsnUHrMvVlQCT1VA3v16f17FWX3buO7tSfvVW
BUYOtHGT3WESx8MWkAu9X9gFCvQvDQDNMPm4SeTuFpCW/kI861rsW1shA16ByB6IJT0byyu8zH/P
iWIbP3+gjxx8Kwj7yxOc89YVy4zOPIB62wsyf2ncE/MFmm17iFjldNKs9UA8t6yKkQqwIJkMWk93
icJ2PZiVCOUydDKDvp4v6qQWwdR79nqKjmKnKIJGIkchfwqr+j6VlYitrHDBiEC4LbBQycEqfU5q
CfkN39DyPj+o39A3/rJ6AyGP/+VNWW3u7mEklw5F5MhD73XoTYrQoG4DsBDV6zdDp2ELPLKIsl92
eedhrt5S0KoHspdP71nHw75gqGJW1Z2tzVydBXpgUiVZHaPvSEjITNqSbRBTIq5LWBD+bWZu+Lyo
RPlJ5zqlQNnSjxrBTF3ChXB25XHZS+FeeolCoqgJNp1rKe004B1mCwd17V0RfBXQMZ8pEJCdrNeg
doDu84AblvC646VMZoVx7kmZuu/QLet/WMRC56ogGC3O8h07hZS6Yn93WYutRdqlpPjEl0XmWE+I
dKcmg88EyjwRqmwMHeUEWBqCFNu6jlW9CrIHhGFWsExE+1zMi4tq2xZv1VFtVS/6gRHKRAVgqg6H
nwDBGZuB7bKwEtSDO9h8JAnDSACFuzcC4xvK7r6kpcDSNGdi0I/phn9vu50Awl4mvqmTiBsyHkX8
ea2UVwIzwkRHVNZyoDvqGiiQivX3i7acfhJjwc11YLtxinv2CYqF8XFq4ERvqeVLbvCtEcPH/v8T
IHKH7XoSv/IAlJiDO7gIrTrgWOl0k0iBa7Li+yM5qJV3wDkDHjb7umcJplMlbDgRzEmSNaxp8Uwc
8c3MbfkIDpftJeGZnaSNlqhqsAaYFibukF8aVVf4wI3OzcDnmG3PtKIeC3kgh4SFB+0yNNHpl8Kw
/BSqDqpNeKpARv9OXuXxhSbJGE2ajgyI51qAd9/SkgXpgwwwT07IZSEUqjzqzyPOgoIDOxtj9Zua
tPFggjVIBIzrv0Zx3tKm4hsmxrtw0z4tyQGeiPt7j9VMujb4V8rBne2AeAUihcH42B4GzJlMsiq2
QzT43s1aWUXOnpn7gLXSJ06aumcSJkSxU2T74uOVn+VTwufGsE979YBIQfoo8lH9ZHWGvqhUhP6q
xCOi4yXKgqfvEHDt0xy+tBlImdUNk1z8fCPVI7QT11TIpJFLc8bzJBMMoZVIdLRiJ28M1jf2orKW
QhcfseABsvE/iwyrtKRW/c8KeOhao0SqN1kPC9gcB1TMOD8z3yhuN1cDFqNJaFOuRUf2XepB0Pxb
pXgCkZDkT4KTtUk76auQmVYba66yHi7Ekg6kje+H1BG4Dz+FnpPUqlM9rteKSNjeWTy56+Y9jog1
homDQJJqOoAKl6SsNkwIVj/+f8HwsYbCo5hwfXWZQ0nLFrY7h2dHQp3fYF16bFIhJpqh0OqZCCb8
YmE5DEzWnFQ55oKRroJbOJo/JPfRLqQVV/GC/mEzuEyYxiwxITotMw7h9rYcZPiq5SSYUTT2FT0S
Gy3MFp3krS6Va8JNIhe08ujRYan74lwBx+oIkgtWPy0gdC+ErhKzNTwX6CoJeuzQoiC9xzBTuZ4Z
Xb1N2c9SLq0rNleVza6fMk83ZQInzzdIx7wOiFlJaVXowOm6Nmn35pHnjHIghzTAVcYgcE9nnCi6
wNf0i/eRBE3SxeoIOJ3LHz0iQXx+Tg/rYWxRwUtuQkxkqrj/h3fy3wBICvrD8NW+BY+k6wlUyXUq
fG6uAaHRwcXefqYf0kikMDaKSowv6f0X90RYkNiXha4EQGkvJDcJGhUNwOr/EFaA0Fi0VHi4PLZ5
lGE8Fqd2yMBCKPN0ff/lUSGjAXJoCm11Z2N1/jX2Ix+gqUDEKLvuXnsa3RkfvNzhrqByPaOccvz3
S7o4FmRi42zYK70eZpY9vFek5JQMKWvngIyslXRb1iRGDcnC+V3pPsrKp6/35WQgnVJ30WEg/wp1
5qCA/2GhNSiywnHpKp6y7Za2P7QASO+grcuBaUhxfBeRXjCJCVB8qk2OwR5mm9xW/7kTdW/Sd4co
ANl4UIUFAIoVn7V7SzB1bNKOV/oyGfCWMLFus9ymk0u9oJXUTFwqzXHmNccupCeinZv4HI08M9Ti
fM+QA4gZKkc1w8SdiQUEop40Fh15ruHHPEnFstfYt8McZRaENHFI0yqUV95pj+Ei6hZv9Q8ohUPd
bS8mjVgRnXzK2nCJuc8F4IjZUkeAw5KxIMDcbp2Zn4ITPAm+IxhR1KZleoGcMYcWQVd7dYkJGico
Oa4YVWw2fOJk75RIQJhIHQqgv1LPbbpoNP8Sb06iYsm3wArXii7i6kTGsqunHFXGRgbOmgUSXYja
S3QVkDqh5eY1Nn15OHhfd/ucnT0mVTgcgGgZAcsRZCsmwmNSpSvrLcnEB1FcLEI8tCbOC7gDCNYj
meBNl75VguqObOBudfApVOWMwUQyqJ9TsYN/GffMYPqdgvvwHIjknRmyTiOsvmj0K+rnGCyNRn9P
UTPVR9kvkayUELMkQd1JSD8zpWNvlKAj91CqWIpZQsEn7ergF5xbCnMqRnXq6/iROVdR+oBG2EJk
4d0K5C0tjD3c0J28joBARaiZ16DlIE/S0HBNUOM9KMwWbGc8QNGcce35ktmwyUpPsCgn7dnKVRh7
YKmMoKeU9NQZcGj5sBQzEMiWEKnmfhlJ6SUgsOBFRN0KHWvFkMNLzYWxnuSV2PWy/EPW9jiCEjrV
VQWi+mHDAu6bT2hCRl5jFHq4Lwjof02S1NY86mUz2UNQ3IsM1s9pyizS524BCFuR22kwPvAhwhVT
HLHkypfa0otdY6YmAYENEuAk+NLocSh1rFZhNlSklevBYEJnkGNLK7//cW8fvOWksmFozcZX0bkK
VBJnrGgZtXj5E7VbZDDZSL4NORJRdWyl/h/1haOvxltYA6CkPtu/VfGE5whG7T02WM2djAeuWRxe
EuFwX3kadf6+Y/SHqWytHvzgvR7904Qw3AwCgq9QNctXI34c66b3cK5reV8w+Aepv6XpfomKJI4Z
6hJlJba0ECn6WVtslfDzDMMFGt/3kbwL5eC3lbyvJlg11lC7uf1QblW73fhgplhrKEsf4PSHazv4
LZIHEEzmQKRd3xCXU5NNpat/PphSLg1nbleIS602THC2inlfXcGcGmwj3bGE4HzEfM98lDk2lFPw
YM2WYi+DX3NViLOP759lDuaMG6jYT8rGBqI85Ymlt0UxplpzsDI4Lu0UajQo4fvthBE+D8x6K8Y9
uioVThKXRBwZhBJ5FPYemXorvVJlQiroVEN3a+iBiME2aJE79/4FKp3pumXC9jV9sGmSmL58vDa3
5Ub4BFij3M+nOC1k6va4Lq6YSWMXYB/h9eJ5T9T53IsZt8IL/TaNqWJuEdcm0ZZg7IHwm3WT6eRI
Te3vLkRs/3k3Fp23yl3PQl7S6AW0CkD5pQ6Vff5nFTmvogua0r8XC9SkLDxWbFWH5bpL7/LkqE2R
14FLRoIG0mnVZdeNErgPN8Di8YvcqMUZ0vtj017cWezix/YirADvus2jX5+soHhDvwAgBBZwEe/9
dhipYIEJaHYpM4H3siiJlaA6T7uA2MaAuAwAttMLvL8oheS/YUeGfEzHU0M6uMaS5LyAClT+CWqn
2MSnC5YWVgro59OlTwbItazyUeEwloALPAxQWw6CBZO4TY9dRsrrrYtnUd8lgx1EcgEry7h8G0aj
kIKEd8Tbf1MkbVJY4Ao5eDdqM/jMkuPHGV1ROXtoQl1ESi4/RJC4ChgwouiyXCCZZ5GosQrq/Fo2
+0S1KoXd7s4SQKbDkXxLJbOGivdd0fytvewrpdDJ59ShWPFchclvuRa6FG1GJbYBR5Thfim0Q3Du
PaI+sKau3zRbrcGngV8nEFr2UkZfv0MIxn2doium6VKtObJ1nMbA/dUmK9Egw3MoRRlOlWfoY2ex
nEFa2Pn0LcqYjFTa73/RpNKneVh4DO729mKqWiu7wHz7v+N64s322Lnb3jCQ0eRBLHczo2TYBfj2
Ej+lDcby7YaPtSapbNzTBdlJdexsxeCOBDC+AerItSZsYApjr/sXSt1r5fYkX1EzKShGDslISMZ5
fKP8Jo64a+vkSl9ojCMQr4ycBWQfwkzZrmGn9T9hh0vcKwoWlM+htvMz7GPYo7IYJ2d/ZhmfRIkw
wWsyQXuVVAPD3azS0WolKknH+c2x97rOmDG7Tspx5oTKqwMIxdQYJbLZxXfEDoQbQhB7Cd8hhle+
8UgBmmKUwF0PkeFro0PcqZ2D4tUGTDEf/HVY2kEi7dt/IF2qrd5HZBqtkrL8X0jJr9yAKQ8SPeqL
7jGy2DHVG7D9j8cL7Wqfli77mNelJuYuQlEjGhtNsbNBKTMiQP5KtOj1AkfUICyJM5vX8P+WV4TS
Dq9VqRDQyhz4AUaIKnvkhs1ke0vNP80czISKGn0EWZvItjdfpnH181pEiliHCZNBDYM/nD/X/CNd
kAUBAw4JHh4q2X5KPrByzFxZQsrQubVGJEYvG+SN1B9LHNhZ0aC1S8R0YoG3xXWmuIp04FRBSQr/
CZo0xkEIdEcDbREYWYIrhXs+KILHTKu0rOZqSG2v8XuYQh3eAumLwh/nHZTb7GK7nRo2yp0wNlw7
WSBI1cdhUFyc/oyqWs/Ffiooerv891puvQOBAR1PFbMPYWdNyrAcGbp1aIPGJNOvUCOKDadbYymS
Qct84cTmCpI7TDTnI5/tmppni8fJcPk956D70RCLcr6267vzb/C5N2+yZUk99Uo/SiVmr8RUC0w5
9POOnLax12ZMWs62ZjcykdJp8HqpUgnrO6lpYqw4YjtaWBReWm1Fj5VjR3YAVzere80w5hLnsBME
wbs+eiYk6wQR
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
