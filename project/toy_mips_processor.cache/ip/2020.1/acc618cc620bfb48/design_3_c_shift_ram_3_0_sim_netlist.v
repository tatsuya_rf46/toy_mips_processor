// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4240)
`pragma protect data_block
Tem5+Ezkr/cEiXq3ers2HsCJLG7YQemAFDkoYNSer8oGOsk+kmUJXAGPRGAJgUH8zR3P9rg5O+Di
uWkqwiftMchFcEMFygX/oVXzmLQ1mBEcs1GC9nk73CZnfruYBZ9gxzSe8EiXe/9MRjsSC6kL8BYb
iUDH6DRa/BMNgMf1jIhv5/6cZUdzBmPZTD243RAxjaf203h7tXUlRFbD/CeGCVTAu40ZmjzEK/Nv
mmY09AnyGvJQBpjJk0GtIdwtiMoVW95qji9e6tTq45LhjT1LRWtrYhGUOZmoVv3RHhSnyKB8CG7b
uEex8Kbk3jcf2chQMMTfLOPFZNR1IHDILaiTOHsXAiJzNS3VQkcY/19yASv9IwxVhy7702iJ0EQM
xyLfi0oU1j5sRAFUug2vdEB5uwQ4gTuvMlxS+J9JlQHc3LWzMEcsOi8BdaMqSzmL/VzYOfnCwsKh
9jhye9w3MKx9rE1eaOsAaitqhU+lISGrY6KYChGlGgtv+OJeo1BMLmDzklUdF7L9hA6c4gQ/sK68
24zp+/yES+mE/yUABaiqv33TimiDxJ1AX4YRYpMuNOjp+WVMtTUYjBnaO/tXsxxdeItT8C71rPj3
Akv86mjvNkSOSfB8gS52oaqTEl+tYiY2vfLf4mtlUayWXVSh4klcy6ic3VM/PeY680VOcCqeiDgA
HkEhREMC2lphFGq/Yp8InueH4DiAQ2Bu1crKyMyQkdUijIDAKD5Dk99sVBry27u/nZwyt21sxPdw
DV2msudzTBmqJbeDfXbBHNHEWPJ9pamuXOGGZc1UcBFP8OVOWmO32An29x9P/JjNqO3SGGO8EWPc
wQzfUN//SKjNj3a9FRVidjCjYcLxJZ5H1z4uABuQSvGCdSJLY/ZT9IIDf03B87lVKkEYETe4WaZb
/znkiRmc84BXPDScNJxS2MebOB0mYm0IvBXgwl4/Cj8kpn6WFpLQ/p64SzQM9H/l/70tDlcb8e4f
fhof8FWXSChDQ5MR6c29Wn8KcLJKPvhjvajO9YJeaM+1iUD110PQ0sJOMuyj/hRrHQcuTDWPs55t
F4C5MMjAUnUFJSIiOrLFjWRTOvc4BvXIo8+KEXzNKWhkivg118sGfvYM9+DS9/3DHA1FO7HWEpUh
iRHC/yvnyxcoG70KWW95PFFPx8PAdBj8JD5lFcQnjKF7f2gFpmIsZplPRg08oW3s7NzSAR/qRei4
jDjNlibHxCsl1whPfN1wsB5kVxky7v113Kq2lE9V6mb4W1hmwhDo33MPy6wOJaSkqcX6saFX9y/j
knvMohjxeBNuEobqc5RXjlLJt8qGujzxX48ih2tAiQe7inKGZX71GiFoS+nUzPYWmIOY4tnkSYxV
qkqNbYWqJv7B6d5C8qbsC/xiP/6VVFZQGPE65JVhoPC2J5k5Izm6Y/tpUSH1t8BXKEwUxy90/06I
qz3cEb+5NduiDIioPowXhO4AB9BbkhYliHVAfPg233w3Yojws0eCPpckNfkxYI2vmQoqcPrT2xz2
i2d9CIXv+F6QoFfZw/K5rnRrluLtZYazZMnbF9KJHoWDyMnSJ7C5C2K/ST5qPIh0dWu5vmL9M9Jf
nGHzhQ3yujqTe2JnZ4ZlW4RKL4bxWwDkPMiS6aAi8rrOZksFtnEelaDQAMeKvuLhB0lW4FwJY1k9
qhYrx9vCrJTFEHWL1PHt6L4y6JRpPxFRCd73srXR4932CAipvva4Lx+d4qO+pyYm4FGer3tkaXic
JO6clyWL/ldfFPKlLjeWaVc7zbIjV577rc0onrDBqBueR6/qwkuz54/FbSsE9QQE+LNp142NFldH
218lUfqZBoaYAuJVYvNgy7GZd038n4L7Qi/8+NiQTZ3XqxVZ63H97/WTkcPdXOT7aVcpwu7nA+Ss
J86vEUmVm2KbfQ0FLbYqfEDeo8avSOCXlehUWLI3riFdVuwSUKDXQhYfcnKYk6+6Cy2TpNA71E4h
PrFJ9zkR8b8aYrEC4v3WUv8BZIN38CDqrq1UQWmy9jU6XKe29t0p2u4bFkhag3c3RQZbI0xuXVDs
mApm4GiQ9ytd+3+DLKnQB9qSj9aBRg7Ydx6Y0Ri6UFoqt1zMXrpDsWATGdZzArITtSZFLL9qqi0t
Jo/HbMSa/YBOMAoOQ1b8QJB984QLQ79WiztnsBKOj1VTJ56vIbo/iAm9aRiClUZ6ZhpFzhvpEpnX
//NFqGpeDn3gMg9ZsXIHJlaFN4sMzbT6m1C/zOBRBr3YFTfpn0oCCOzkPOEJF7qxGFgzbsi89Cp2
paB8eJpxUJFbGKXdsyrm9m76gVl6LimJUSIr6gdU6rMjqqmCllnIUHkJUjsJCgLHrunB7doxNYpg
7DgdUvMtqZ04uojeBIiDFxcQXbDGyncMOYj1ashaPaQOltRHSRYNvt2Rlv+3GSTCHQMqI8Lh/7dA
UXgsZDVN8OF8MD5RrNirgf+dAO6dV1Ehai6e8gC4ro7YUyUjVqjnINWHYTQ7zcIUylIcFAClxXdK
NHJQbbTktw4QdlKSLUQvqvdOtNYvWJD/LU77YiwtdU6UPfA4PmfgoXScj/OEzakAmUlzLuAa47CX
xz/ocE2sLpm4RSqWcr7GGFNTDnWshu+kgIpd4mkwZVyrWDlMrPxP/g8cj4HY3rebTKql9PLl1UKJ
y9MxYw8fnOpxKhykn58jDq9n/pLhrRzo/P+TgW1EK5lOCBdic2YmrJYYOgg67tpAeXMzmqjlamof
j0UvU2qyY9HDtX+vRw9EZ1tExOXC6VqOwxMOmY+Vym9DzEW1wEbjvwWoG4bhtE1kbjZ85tkX9Pra
anFw35qUHgvZtckhcxryBhONKI/5yqZZHYvFNgN3/wD6mtFG1Hee5Y6NxXcNTJ4NtIZ8SZteKzD3
FBURkm9oR35+R/3xlSSSpEUM3tXeaCGz9hvN46aTYzr15VTqL06B4iHCZnkrVzSLyP/mNFMd1C9R
ZhcsdXU/7g4Xuew4/J9JFc1eW2xabPC2Y5DqkYFZZEUAA/lkaQoMvTlc9MxY5I5wZj4tcyppGEIB
YyRNq+VLg9C/ft4MDQCvir0wH/JPw0JNaRN0jN93hcvf6UipIpEK4Xvxiu6vdVwPAVrV4951S1u+
0S8edjeYWwHASdT2l5VtoibBTakX2do1Hrirs1Nd4J/TvGljdq8G8Y0D56MtlOPuDngqgPNnq2v+
q8EekPcIlMmUXcKZPHeQbiMPvHSz85cC41Iup3yPONgtgTGCf3zuw94TIsw/sJ/ugYfPJGe5N1gx
Kr3gFycgWlvFEzdaE3w29YbvZfVZpzqASDHubYXTII1IsasqSnJdMtli89KR5jEszN+yEWjyWO+X
xqoimKyqr8HAt3aETvfUpy/rq1kd7CMIHtd6pUbWUrsjKXPKAzJ9dDD+qf9LFbbGbK/wVUpLG8e+
mxD2rNtu+zOFmhw3BMInRG0Md/QuNriC4r0KN9k7wLp9JHhpnKls2KdDt6mwF+0XXndttwHEWxH3
msbILJIBPGq/cSTRZTVG80ysFMELWCGhERl3779BfQTM8ksFWenHauwDTyLGQrtf0ltxN0+jcTFL
V5Wn14iseKXYz8JNpq8chkqrNv5DkjLhNbMfRJp9zDykUrVwHfNcTWsTttgUvPREHQTpIwoHdTUm
EtVbci16r4pTQi93zzpfdbiYdrUKcAHIiELURyTN4pz2GYwcdJmKQDwQqW7Aa2Pt6LzUCzTK9JJi
ymrA4MLg8HBE07nldPgvNzMj8ciVDKJ1+XQaCb8VzI01+wgSqLcc847IwiEEuV6YoKBTeLp8lPBm
4m2Nl7wFCExnupAa4tWqVmFlHr33g1Mfql6lubn4pQ8kYPToBHoh1Fxsje9oy/i57kXen6SZg0oO
ThmX5vBwYd6CeZmxANgqNdHqo2p8kIvhQAXxxD2m5gmWFN0BcEJkO+hrLg3TWK6hAMtjKpWBwJSR
eN5nEl/W5yZE7Ub+zaTpT9W6r73tnfEiUQOmwkzrhMqK2gGY/u06Eh0+dVdvGKHL8kzR3GRTSxkl
8lUakBtMbMhkPuB3M6vsLz5nY1Ad6T4xcAnD0rbtxyEfeoklaklXdmViR/0GcLT4al9SIMWZywac
f6W2NIL44BHbkdi8cxKiPd54Wlq7q/UteP8Bns4E36SobAbVN6Enz0f3oLoHK+0rgGlK42/JwzW4
g+gB92GQkoWvxSPcVlihTW/MB3oJMeJaCeHaygy7am1SuSr9Agz8oYNpT9aRBUJqEbWDla7fzBxI
wyOSAJGpjO01jPZ9zXF+bD6LxjiTFtZZ+P9x9O44rVdiUCxDsn3qo6xkFOeMgZ8k8VY6GA6WmcBh
9mzPX0doqCcC3i5mXKC+HUnsY3ZrLyLiJtNwL4HvgNyyNR0sDK+CK7w/1vHdKnPZU1vMw1QAbVR0
91dxgyjvY90resMpiZ36i88HsagdPFegPRV2BLyO7vhevVczP1si10iNsmB1Uz3A7t0NbR4kJrqV
RzDa1SSEbiH4Hs4IVZ6MKjQ8T3yI70BgoD8lBWVmE+yp5UOfRDdbeFAUKBwkrXIkod+OAuxiGn3N
KwJ8Tzjn6XGKiysScRMzfnAuUwAke6NRqjQinBsSVZJB2q6xpYcp4+0XXyf8Qsw9XosqUo/+vYi8
DNT/yNz6cJCgXGmB1OjCo34S+SI5PgCozXvzK/Urty72VZvFBl3dhllU4ZOWwbuRrGUi2CaKyUDK
UklfU7R2TCjMkjsOysw6zlp+hHZ/h4oP/Os/wo7doJ9SyJa61hhv2fXDTyoPHA7slmOrPpjdyptk
kvGg0DBmLNJpETNa7B9spWtxtDL3zBaUxrFZeXIbqNnAyG+7lqvm8pjPiG56lYgbB2EwoONXXBal
+X/lJmlLLoFTcd7DZuOm5uUZNna6OsUFWW/ygpFChX6lcvuybJi4EcVN4PXLjS/x8FFCq6TsYjxq
VpPvagXVp2zVTwcpmdJhu2bZxhiMUbeI4Ah4OEhcB4vMnw5nNlyR39+bzWouAPBEfx7p5AA3loN0
e3+7rUI4Np7pIDLH3cU77XB90sD/AGJH7hQzUlAPQKkYz3lFsidq8Y/sxr2QN4Q6BbpDrWpdysCt
iAdTCPPw+FnSJlpOLCx1piUbJq1qPi6xLepN3cegaG6Z7+olWJ2+RpPD5MDfy/nw6T5DHPxH+3PM
4BTMb6Xlm5ePEacGR9PnPVTNTUNIK1qvu6Qpoi1KiyIwIrI5i5qmW4Z2xvh94BpcGIQd1gOrM/YJ
SW30XOMfq8cwKKtwtuoU0J4mbiMokPu+GieiLSlmgPAx7pKyTvRNrbQGR4ZTsXq806LrXQc4PShL
EcDQL4QaCKEHl7dFUPxEneYIDNdWwrUtnOSfr4KP5ycQVYRvfPpVBwbCIfnSo8oaoWkJkEBtg57m
dRkksOxZt966UPcDLxReEv/AGk4zOMJLRgYeuw1RIpMbtN4miUORqnWm8kXzAE52G2EIm2I3q/mX
3INL85vBFCjfWNb46kbdfQ/GWD7BexSWM7Am90SifP2evBZJhwAtNdWdJFOtrF8nvDXYS1iweHLt
a4pWeJX2+DzGGrqV0Wt9wbfAZH1AUNfxV/ajpmJGKjvYsl0EL7iR4SNlz9P4RUpBF/v47iLbJUKP
SkteRgl1Y5+CKbUJoxwP8Z31O7FEWw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
