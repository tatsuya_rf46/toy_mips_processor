-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 14032)
`protect data_block
/jJf9lJ50/+zC6EonM803kDlKX4kc/RSQm3AN9Z7+Gr+XBSvDCjG+HjNNjREMVLChq45I1jF7Mtr
ysGIA+1or38/+/nOrqRWpUcVZR3rxV6X76pJDGSCTuhkyJxXM9nRbJw3+EkByULWn7HjIUlVrbxH
UHGn7gorYP7SXMobtjwHo8LZaDJtglIqm9F78kG2XUpNfKH4XgIBC2N+PnZqI8puQVAbGNikCqOI
/8z5UVU7YzFXymKACAX6/BukBCqsb6bG9y7mpKsJuUYwkPY9Ywh270Ra+O8ce/sRo1+IwMX/qjRj
suhHNLX9sVX8Ltxe4EjcKizP+yxEZKl1y1YmfRg0X7EqufUTlC2AlawzvczBodUUMPp0F3OLg+uh
gP2/cYwXrxLUu2yn43h/hYqr/nV+GyI5HXGkzmn1KA1bDGQhryMp0yOt7LTKztV+JpMH66gI3C2p
s2IkO9ID4QDee5tzoOBdPTa/IA4fn9Hh9Q2mNOR5oOHFwwW6vzzhwnMfTwLRtta/HWGh/ruWiMmf
U1Y83XeCyABsROCN1NTdvdwx4uDek7OeRGqdRTRKOm1UFuy8RSZM1a7lEDR9xdAdTNLn9AcvfkpS
sDDEfjbrFSTCXU/aDB0792KaKCuz1BRFLgiwE38ah9R6FtHenvu+vXT2LOHF/AYG67MrDmeqVXej
+/sVT7MwwDkAwndsGx8K2NFRyIXBbhnOXVdXWu3sytXB3hgvo1Yuvtj98KYwmneLaku2pIjFW46J
uS0uo4DDWEQ5YDUK2Hsdtk48RYsQjhQTXJJgqT8kIxJLaZCRKxVcmxj+WISdCMSSrjdbMUqx4kQ1
ZOY5lARiGN93UD6jah1sP1lgnIaJq+tr2icC9hZk6A5bXGujPWiBkf6QRm/5HwVaj+06EwSEYgn7
guFtTaH7Mp/MTHb1HEj2GewTjhOvx5rp/FjifCIOBE6vHM7LN3ftLFt4NGTXht0ONMJiWoKwbqf/
zEJhUQi80UCz4uvKblIkPc+u7G/k5BRyaqWyD53gbaPCjPR0lfv5Q6i5p+44sVaIXeO4hdRMCY9Y
O0dA8ygwARfxZthEu+/PfzV14IaG2DAlmTLjStYUXVsuIByX5uugPtqZJXaseAf9xYM8ExElNgIm
xjGmt3sjD3PW0IpUDjswlfR6tZvXqdT4+bYQwsQUrPoM0MKPuQicM3ch0GAMiIZzL4yHmhb5jk1Z
4zK+zGQ1r5VEmslK+zf5tOF9r0HImgPgm2LBZvhh9ukE4ay6mo8qbjcxMPeaV94Rn39PHAOyzt0E
p8y/xBFwvy8aRM3YqLhIhiMzOwmRhOWBtoOf+OiK00dsS/kiPXUm7TRW9xkPq5Nj2kOarKX4nrMG
KvxT4IaVQRw+0lziw9luwucJFVu0FKfZswvcc7ytdVhQbe7rXIZ55hTnnOwMVTdwa8a19dYhhhaf
F3zOwsXryWr6JzdkE3Sokd3Xyx4CK4p/nJdrAekoAcmQrNEVmOLc1S3e9KTnmFqzuPF7ppiBSbrm
3yFrhKTRiIjBu2KC+Fp6+U2RSx/MG7SBv/L9TTwUsX45tvkDmP27hcxMZOdlTstBmC/7XOhvkhpl
BewUMFIenT4HxrRGaIu5DyABGvOY75QyRDBq7W/8fW+WGM1IjQwzUpZ5L3HXsivKnsetQBei7KY2
x64Zt0q0y0qwHh568TtN/MYus3y205mWSNoHCdAaGptjL5DGE1gzxAQ0zbWwBDOZd4h28ti+keAB
EmH/FNACIu4gx02Ovn4uHUjgvZzcenzLv97MwlhaKgWo1PkX4W5qIM3v9HzbFgpYy4BEsts7h71j
fFNYRPs8nF6ydBakl/OPh6YTtP9NSCK1cl8iVN80FUbAuIeVBH53LJysHieUuRocy3wZn7HGBZHi
1g/jDHP57fSFdj+hsb51THJRUNe9AlYo9dAvVkP54tBAU4XEf9AsFeBwOj/zhXVe6n5HHe6o+9ju
Wx+EcXEuKfJFf3wSA7KH3bE/NuQipBN+/neWBADyozIjkFL7QZwHViFOjwagk56NKrf3e0HmKH8e
YVa9LLn8tb8shK9JUJ71eAwcH3eCp4oe5Q5t8iHUZcNjn0Q2fNEkHaGhXuSxJTZRgb5N8JaNEIgg
pnNnQa+CQTMKqJ4rk/jadK48Dpu8qtxHZX8SNMNH9zH6CsmH/MZjDU8AMO064rSYuqTE9RxO6/nW
j70V6wOLwZRrnCmPzEoYowYUnGUBOQPW2h86J7OD0el1beDJ0rsEGJmEU0Vee6u+7I/61+dX9Gwg
GsVbbRO2A+ISOUcj0C36xn/+40aq8OCWLyZTwGn489+xNvQFUbPL1u6YNmJ1dDycZdv0QB8FHveX
aaSAC6kPne7pjEZDdOn+9e1H/CR7PB0ZLRVnyBuXiugncyVsUCRqn9DBnu3US95MsrX5LBtj3mxx
A1ZDG6zvBgs7/d0p4wO0RrriDIEQ1rJqHE/c1I1s/ooeM0Tm2K90lxgiJuzW6XBi7+npnbWCVAwr
mN38zcFQwOd1A3tQZNST3P29yApSCLNZ9jN6v8kG4kvL+D42J5ToSGCLbFnepMQAsGOuKsoR44/A
vKD16q+wTizBlpNcVYl38o9z1PUnxSrlMY6PphM0JfIRgfmk7toKT8vBbxucAW72HkreV0XXADUZ
wkQzB1Pzuj6xwFfwB6mZ4ra+EiuS1r7Xcdy4pMEHAPfsNQFAOLRTrCQAc0mASSvNa1NYK7ExsdLM
I7+bLpzwT+3QdtJyAaqZwE1o6qBn36/xcUxCH3dhc4iiYi4thCkFadjtZux5IWYIv6+wT2/5KDZe
F9Ay5+CNnQuXwlfeRlQ/HNNB+ALR9oWCVfU/ubJMEByRR8Xmg2kjjAOaNiZsP5+JX/b5f5/pMpxY
Cu5LVfkcq186WaHEHSGv21gR42yqVvZkmWCM9eJgJ7scGXmcQlYzgp8OMX5lXC7RvYFgN87Eg9Fc
/km10pxm5YFvP4kym3BwtfO8M8EGFwf6RehHoseJWchHEwGED6rmaTGgdivRTcq45kN/v0bDiyUM
aBst6cW1Y/eSidCIdVHFll76P1zcR3dXtA4IPcGAVENc3SThkwvD8GrnpRiV+P+flAbxZvZG54tP
bdp16tFrnIVd0qpdEDZVgXDwJDqAARZQQnZjMH0R0JKCYHwzh481sumdEUH2KiaqFqIGLbP592xP
PKi4w6eDB6Ar4bl/mShUXI2HsrgppAXhHLWLqLJR89WaRLxtktwXVgwlBUcItFRL7VrPADa1TXcZ
KdQ0hdZ/yJVz5TmwvJM6vtGIFVfb95c1cX4WICL6T7wMoUFcFeN3d/gZQHxz6CKHe5sHU2JfzDCY
y2PaOSl2WvcAtSKdS9t4Z4DN48OBy8uqnegcNpyyiS8KW3OdUBNOu90KhY/oTEs9FAGDQcZATn4j
9EM47SAGcJf/4ke9ghpxvn6Sq+2/vd797gIcjG7PIvhkW+G/emyOe8L9jX81L2uPP0w1t908glku
6elY/gHG8zXqtpdlATPBH8iJf2XMQqwTcV2+60TJ3J/eqE7bI2gNa3dRzrcwfLmcGLTz85Ty4in1
UM3U41H2Y4u3pg5AV4nj+1nmyHwoAy8AnY068V4/gWdVPvaHauG/twlN//Ud8NbjZ6tNDlUrF/Sx
CdKekVLN3Z46rG5InOaLZMht3udBnMKoqjUckZacSlh3+zRcTkopxDZvfeFm748mBDu4VRdIPM5v
8yXVMp3CDb6QF93iFMbjWOhAnC7hP2RCWjRmfI7IDVinciC6yU8SKFAXZi4cVUZ0EAVpuidpC7KH
wXOFeX0JACke/AgIyh4kju0MJx7rh9jBkY8j9HFAS0UvyyC7HlpvPEOSUct7mAvPntrX/t43oxCR
/3qvwoqwW68pWMXH96LTeLBYcHzltppd9B/BLc8NZRyTUDiD7k5ogRc3jQsTiZ6ASbK/VCkbrwic
nKQ5sdugLh+sdv6sEct+210JRk3Sx/vSPj/2Gc10y4fac+JTWrpY5twNAwSg72BF5hJSehB9mmO9
uqji+q2JXIaD27qtTyW2GOhUHk1TZm0gRwGfNrSF3Xtkk4sr5/PeJ6h3WaHqNlYaaZgWr0YlWOOE
Yx8OaOutFb273sWVhgpi6J+x4Buge6S6MjSObwqMo0YDfGfL1m0MF26WMomhKpI8WwP4s3PrX45A
HEpk1QToPImTkid6AXBXETK2OgCfmmAeMQINycI0NAdviYcHjdgFPmwgNHfGMFntTePHe7SFUrPs
8lI27Fy6dDuICahNWhjKoeSmJR+yUOgmP3Z0TqmPLkBWdG2CU+SDkx0bzZcbo7CcaJQ286DhQhYs
sVdnPGxg1XECGNpsED+Zfkc5D/Ry7rxRK1PHYu70z7uGlnawRPT91gz7CiD5JXP4PVjaPhczj8Ex
E7Q598SlRL7uzK/dO7uNcrYl642LGBcNVnGDN4EexwD528IxBoZhs2sdNre8hvjuvmLIMlelN8Jk
N+rpa59YribnW7KoPgzfNkRIQmfwlARR9kaA/WWdG42HCNplfnwsfCcYv/6SByzhmZM2ZjIe0aMa
k//ZpsqdKwYWhfBYI9RMLnaBkE38N3LIzu5vsaNnRJqx1Sd75kK7DZOK1RLP/g2k1ly4zcJRJJ9+
4HFoBQElIBt1U1+6fe2N22L3KoXp+8EGKNv4twqtyxNVR+YPeOISkNxXi5YfkJdZnHP34/OINrcJ
wZpJjgW/2XE6tY8KxV4JAylPC1ZSeBcz0xUmBYajcB9/I8zP7rmWPKGfD4ocRUxuAqNVKOtE3eEK
J3UST6B2F8o4QezAjSXJYMtwIePLMeQxybrgrwhyjfiLGKlG+Sc53JcDAFQhKaARwJf6BXgRTs6I
G4gWi9Y+n/pqGaU8BSMpPCv2Umr1OLKVjTjtN/Eju4TQWe2boEQ2R++LGt3AJiGPTp2A5cbaS8EM
4xlm8TOyufH5EZkbVct8Sfx0ZWZVQWfZUyI/cABEqShmk2YxINNYbrM++kbKT4TTJKbkKJ9Xmo9e
/GS7PTxjMkId3wdnW6t996qYYTFqtBiYdouloO9sNmjy86qQudesbva81kVoDz5JnquO+cqkMG83
rQkyGrGBFPYX3jZWL5UrkuTlBxdBEBDbpFWuf9OK+IFTb0m71p8cTjpCieKCs8OEQWemgtDJM+Zq
9Ek/SSd7Oh4cu/PDQBn0t7RNk5cz0JhB09NIs3+o5Rdkh7sj0L7J0tEjRuBF8hVuEktj9ChBo4OT
/SoZ1XbMRtDcf53AqCNNh43gzX01tuYswTnBopZ6bygDducCmExME1oyZk+tyB2V3sok+fwozp0l
1ilZrj9R1BinYBR5XaKIpkcNg+puFcJPDcmyYwj4ZrPji0OTxIFffpJzGjUO9wTtaRZOKndKm6kP
ml2YIi38dymE9v0J/oaWlLwjfkzvL/YUD8JpB0sVbFSviQjrKP65oOE8qxxqeJaaAw39bDiudBKw
GbjxoVebshoYhjoE6JE9eQzAUbce0JtswZidwXg7UwM9AjSyxAvvGCO9oghgHxYlmOQrxz8X3/b7
RkW14i6IQayjbYfQdD5LiJYowNFglARazFU+r8g1ZE2ZceH4ZmUPhGIe+ZouTTZRMPKTQsklUuwY
r9dEQtbfz9jN1ub5xIzDKerE8N73z+8daj+x3pOgqgmfsQSnod82tri2lsL2VfA7I5yxG3luIWPZ
tkGRrnt2mHVgC2hu7rJsAJ+rIoUsdT3zvlpiK2pzUOxGPbfoHTgwG8N+ZomeCJQ0b8Mb/ahUi/Nz
VWpgQ1lpQKiJyZEm1ohPvvb3AdEZ08IsbN2qLtGg8g+d+5ZzGelq+Ex9Rxy41XAfxd1W6rzfuu9/
Kc3pljjAiaNVvyXBadkFUMnfAACJyaAKngQBdIovGDIcRzXGC6TLcktu4katXYLdBCE3Al/TJQjf
zd7lAD1qWeUpB0CRhF8kfLcTZxQtXBlrWBCMFl+C+Rl7g81Fak+jmgF9brVnyo5ZOAqC/KRMJMDd
FOblRNY8eVPdnLBbJmKRsITE7M9UtRRYwlLXjc9fGzbTeidiX4o8iBa1Aqh9i9JIg7sW3hbSuTn+
Bsy2dfyzQpzyITVOuLCPIAf2AYN/dc9IeNUVkFUqpuwNz7NzGZ6ksc6islsFdXwlJ+omcghOR6rO
rw+Qm0B+Peel0tEZxLkkJNZuJfKHCBwxtqt5q2hHOzsTcV5N0Bbk5SmJk3iqUx6oaNpjGa6Z8nEH
Jt6+uHoVKS0SjZbFtI7ZEW0C+q1Ew4I/PjWGTZsAalr2RyrMsPeYAgN3oWAUGSMuZxEr+93L6RhF
E4Nal0GDiDANkMye+5TPk7SxCdABHyc2nr0xgVExwLR2gWwHo8TgasLjME14SQvxs8865d2+GEc1
E86+K/vyRiwb0HBC3z9QcE13oAiQOtrui/HqaDzQwKcEmFeWzwNDzfRGJ09/qaPGBH5B1LvlU3WP
Godw12xAnOMNvdF+CMBkD8C9j3JRyyJ+Qf5E3IuJvkukNTSrrndTpwbq9Mpdzv3QGZ1SBK7EcmN6
ItMkfW7r39zKwZJnE0YALikLMKo+5AzntjC5/2wFujZiX1Cs+Cwxoqq0xNdYM++Be7hRJUMszBBl
Vf1Jf1/pRvyiBCOn5K2X5xmyP0bauR+5QU9IArKyLsT+iVkIFcC+ozZ/UlyN3FPd26hqRUhWqs+B
nB+ujUV87z/AcZhKd6Do+DvjtV4WjyLmpIA8sMlXlKGI2zrK2zp/1WO60k5M6EuIHeXpCfluORvf
IPx8Bz/sLfz1ihK/fDNH7QQQFgne22VYx16mmaDXH+gjxNRFTAd2Dyx0QEkzAn+6+6rL1DhCW84i
a407ntU/o4qhCCTRXXWM5DNuQTqzA+rs/Ry86/9oKfd5TSJkjRgs4Pal+JWszqSysa4q3yBIsCFf
tculvq8mDxbXyfHsa8S6lHfJKa2h40SV2idNjOj9Yhox95egm/cwzgIlf9TG5KStqlcmomd/revg
3C3RUTZ1vYtXrZBz7Qt8zJBMQAWx7psvq7msc/rPSGjQrWRU7P5zfpKGCNDIbFngV242ugb+3CyJ
PZMW/RnYWD0Kti1B16aDg694KwctyKiaoPskVvm1Fz7X7NxY/MtJDIanIEKkRUZgXAop3g1LCMPE
hcFrbJnN9zEe9ltpX4uJSRuOG/rtvtfZRyih1VusUQp4BZpx8pcZImU31/yVH4O9e5s0vpCM6K90
ozYIZERhQ0r9m2WjuT/XX4rFodBKyriXTLdnAWFsDYSJ/zC1LUTUkSxjnUg/HViOAI1GUvlW5jol
6NY6VHo7dJBRRcqFWtlz5dmpYf6QLEbBEzGUu9lVdew6DrmcszZe4egfxb4krAGbSkqfZ/X2+ekD
2Q8i/5kpCJKSGfPInqZeatWecciV2/6X0mOK1OTSC80Sp+QBckzcBvvfP90fvsxPdYMy2NJSFo+n
FfTraUhhJWTJR8T8nJ8EpperX9E9RKVPWmXAOMWcEmThJooT2ujSlA8fktK5KvnE4q8e1KF1+wz9
vxq6JaXAydge5KcmYGeVjmH7WgcdioZiaSWDX+yGhaFTRIColtIkGpwjBXJwEXbzAXxYDDwBFaXY
KXg3Fefka0Ohd+ghYTLKSUlPNNqVAjwPqENMkpX6aqKnotokPO5CMWQl4ccu8mLvE/4VJ0OlSYmV
ZLiyK2h1AE1b7bNKIMr1SZnCU+rH+MWHUXmZBsnFNm8Gh+aKtcKBMmcSWNMmx3AF9x/Y43onkN82
m/fxY+Grnp5VjAiKcDykUbmawSox2N0ldQHdvWGiFIUSktrNuKs49OXDQZ8Hpa2wAtVFg7CEdVv8
DD9lTKwtWZBcYnDkF5NHXmbpL61Nx381Sb3yfNqOoaPXHDN5xMnX5sEpzY0LzCRbhPa9EoOIGXCl
uRG1eMNbm/fdZ/HxkiDkLBd12+M/4eXOI1eITHPIvLFxWLgxEGzGr0og7ndw719RCaTitxVxu/Av
2c2Z384tYUVj1B4nhDeuW0MgpY4qnUeeIz4F89AkzImDbgeLVfKuxP4W3KD/4wcbgJkmKOegNISK
SL6u0HuRvhJCPY4rSPPAKS3lrfdb6OlEjVyVwVJTH3vbvzvJG0L+vmAduYo9mrtpO1AvF3mzafCY
cQxXmCiQXXMhDg6LViISwWS8rNAk/dgTMvqhcfxz0FU/D0sm9QtwBtpckAHhhxs8ng9hJ1jnDauK
oj5WTB/dzOWTcwoO7XjJIi//g/FoyPH9bU6PwQAtPP06Rce+oocQbHUHn4Zhpcg+0VjJVZ/Hq9SJ
Pke0fab56iaz/YevmQJmxbK4QOubaVyTaVuEL+suinb5+OUNNLHV+KnRByAPmJbSjkCNBMPin780
fj8/E+DjVj8+Qv+3mbcPaJexLyJ1C94Rq/G8jo01A/nZ2xJKuGtcJZYHIcgf7UnunXLrUp/NtQUA
t/9HS1GnPlvjjpaK5u6Z2BqV5VjoWHHUA1EVcrrS8PdxMrNVxTTwNCMHX8POW1rNWEJuMT+ks1df
5GeSs9QhrZW+cRAzaXdlh75Jp3xXdBwMP2rTl0lHIWzDxKGav0aOLctKAUxl58qWH3HE8xDGwLAV
vdqmavr6KxbXUhB0Oc3sgGtGAqFk5SU6GiPnYg6ixh1wSsZ0a3mTrywlTt5QScu8SdsaK6YBKCgz
KOKP6wOzXY+Jt7LuCB4yqe3y38VrukQkT37CUYnehq8GCgzC3LlEPEmFOp8CzJMuJhhwLAXvc4HS
iiEWkno9kx9gnkugXGndMx6fc0w9i5HjJvcb2PknoJGUYxZv434q3pb545SiQIaJJhg2S325l2QJ
XZZaV4j26n3EQY5HK0O+KyKA7eFX0FFLhGDRLJeqAZY7mToWu3MniewWfu3UjSzeoJw8IEzQVZhr
YBUmKpiB6eobgEODRypG2wvVQytcD78zTqvNbpJT6rxCpUOir5A9RrC3tlCqQuypkm/6GyO+54vj
M1za1ZJuf5rIfjh5UODdmUYoLIboLbfjn5EZYEc3Nobpu9Y7c6cdNsG1o7dnSwrFzwAFwneB4z7L
8MRFHKW/DCf/ZTQCSUiZazDbojOOQ/pagbivG8YQgbMUAvaJgHo9IwvtT3Jn62S3SC7rt13X5hw/
e4c50qDoWtCBba9XMUtx0nRP8De/KBlQHTqHrmrOIPHXn+qnvYAW/f0GbAttiJ2B5oqpq3DTAHKA
GvjKfBJ/Tfj+qHYHUOxJnsBBsqJvDg+Ll6WH0xaG6KJTk48c+Ljyey1L7OnCTTptkeufjYQs8mvR
0SkE7kI5Zj45bjAVsZMKnGBtjfShDlpU7bFYrnnMUoAjJcpgMvNoSaOiB/tBnAGEUEqsFXfHqyGy
dFeDcEOCBoA3amq/ImG/UNwMs+vJx4mg+Av7l0vCZ3O1TdziyAQAYFNBYZf6mVILAa1w0C+PzSqW
qQbJYLk7fVXha/SMOH7BBJh6FEZ8SOtR0uXSQ1RoD/L7twTniFzk/KEzBE/Z3zSrT8oAvwDeVfuR
4iRhEKUDXiDEb+VI1FAhA4giJYwR/oNqjZcYaNjjc6jTx/H6ZEbfQrUyHqjR/kckzYILcXQby6vN
hM68Bw6gW70bsY0BPQyeXqP6a9PZ+kVEz7Uyqm4N+wDiohIg3PRkbwEmH5bflESaAPPAobQpVM0t
6Wjq7fyugrnnNw4QPaHxaabHhtKmgSdbwTP2anPN/dMmN0z6CQ8t1vDP67VFnowiWye8qVv7sYkP
p9wPXSv/u0F4ol9Zw6sq9CWiY0Rf6AXvW6Ivhnkfyai3Dqn76zyiV5MHYkScF7Oc0X/QL6jlz874
S6MDjBZqzXF1wJ6A9WhTHew8rrelKWg/H/jghB4OLci2s0xiimx/5YDkYPDwf+fD6Z56Sa54qDUw
TP4qKBF1wly8KDtqd9R4zU7op0vejHYinFX50Ow2KGT3P2KREs9o7OpursEq8HcmvBHyqWIvMSkS
LGcL3wFTPRsviwbrCGfOjm4ECUMDTIfM+zpKIpaZDdSs5Cl/c+We9ig+7c0rCs9cDuMCEQyE/aoy
4I7yn+jPRsP1DxwoqhBNhdbQ8DKLKWPHhNTX9f5kW/jIPOMQMC0ULL4lpnM+23Jb2pgIlp9bHk9J
emgQKLuH9UuwfXbZkcIlDQ5oZPvPA8CfcZqUf6P1c/MfRl1hgOe6VIQEhTzBwe3yIvNOT1ahUMA5
96L5HTJMttiQHvXpMrzUEzfufQY5FqUCkc47hoyULGbSXCbu3Xnh0HSPiffk0ou9srFazQOyYbny
K0Y0IA2bhYzOSkhvjILndsY7LUO52ODzOY1TEtOLlvXAfgLB2BjwhKbwJGhxHgw7IglGxXFPW8vW
t6LpUd4duCBugBJnU+qZtzROTDD5Jaa7zcW9Y9lynahY0wgPM9KMwiE2+ycTmwGGRZFXeFX3yoX8
lhqmDOhnVR+A86e8Gr2/GnJdc1Z20wuCoj0aapCmz0dsStg8kPItnDx2YmWM9zcCwoL2iqncNDgs
jmM6iY3rMbXKjCLczY7XWe71KG+GDYngbPb3aqKWxclN8dW4/lHjqbQt6yOz6jKI1HRpk2DVcS4G
tSrNE91cKqdSg22RsAsuRrK5EFk3WZijgUrQ91sTYpNFB6DQ0xjGbk8KSZg7rjRWML9RvO066Q53
FYNh8pNhx6YCb5keMThPoNBhA/4mpGAcRDeALcfAVc6+pIfaRRjQXngJ7c84FF7iWEeC6OkGgTdE
TDy2OAiecxnBh586FwU5w+zNNYUXuzMGXEgYZUGhjKsiFJwsn8eZxBjW9AgCAjKwfhX/Relvkg3J
nMVkTmQGL87IzraaXXSK8ulb/Ty+fl0x87VM5CFUopiAvcop8PoPrGKrwY6Qy/GaX3h33TVcCD9y
FzeVZGsbw1R0rhClOUNSPPeFsGDDybMCAZKP6Tr8f+dC8GlvNPt1jxNehhW8wlV7sSv5hGS8y6HG
YIfpUGXTrq1bLi01o44LWD8/ZWcUnOAcj+CSvUnByPXDrmnOGGWZ67cJnoPCsKUK7y6kMuoYmc4Q
HAphWAHqRrIAqwJLHnjEL8dy4PiBaoSOc5feBiGrqE45GH1P/fr8yKg81NhXJ8X/jA8hymH1EMaV
SztuloT+V1zqunSlQPGgW9PnbbfBFS+J2pQNNEntBanOx/LsT9gckZeGYWaUZ52Nb8XcBWyZK3Ao
wsO1mtqJKKLErBmnPKiQ8KXC06HdiN6Wc6X0KepXUNPFGRyv9/8jxPt9r+QPyXXGGWSH8e+YAte2
WF1AHSCZlfAj/VM2OJaYwbmjUa67Ob79CIvrf7VUsgHF7rUTeJksUdzmMWQTU8hZel0fKtp4eTKr
GQ8C/jYBSnb41Mmemx5S9CgzRA0AvrG6MrUZ9bTJuNICdCrkyWB1yiwJE83rsgTQh5+qgqR6bpgl
ZIBQpdfDRmKIRfMPKv+lnCXiDjoWU4RVTRjEHxk6yCval4LYDzUiP+dpsyA4xTVBxtRzT8rMA8Qg
/Y/c0dIaNhpjlctao50PFI8qann2+pyWNC8zmRfVYiwt8fPCR0cUGastx4U5ixKO2mWSjj9BUjcU
aRnP1FnZLLrTC9XIVgofCtkx32eYWAeKEwSERlp9ZDGumivTowaD+TDl/ghc7o1b/fjDasuh1gla
K9j5GTuGcq7KjJh2j1H/FIUVaKVk7k1uXpi/dvVjDowZcpRvgUn8/x5ER0blJjk6JAzk3ay/DoaW
CQNp9Zy8lS4BEH45KkiVN2C6KxJCrP0YcsbXU0c7KfLVJUa1zNcD6q0SzDOnj0nEiArSkmJyrCrs
Rb39SlOkLeJj4TYnxxHAvLxnFjjW+xe27Xu8jctJFpmcWooXuCD8w+zzPqCHEdafvHN2Yw9M9HC0
hT8y7rYBgqiBPslZ04qYCe6EggrDW5GeYZXgGD4zRqppL9Zsa14ExQy3Fwwd+0wTIhE/2kGDgbEt
g4XCIZrg0IVdFarJZgVp+lEdoblwFhGqrcB6ipmhelpn3O4yD/GAUHo/0fkq+SRGcp4hcjUGDEHf
ypBl8iBmcFcZ/ZFTJUNt/6lk3UOozwkaWBcH5t/CBolj61ioR/wQFXFGpuSsZdDF71yB6p9z9UFg
c1XBe4mk03BrhrsOo3gzZEyFCivbPoW3xJkw7VcQs3ACkZkTv6UkEzL0wy7E/xq6g9zkZzUm39Dz
FaOGQKZ73JvdYxFp7bc9GspTH+6aE8FTt/na1zBWAc762i49nvwv8JO6HAaU+WN74MnFltOGHMfZ
HwPqrRlMB9eiLbJq704EltvDrYtHQlEaT7kzbMaOpyLcmXvkKB2qi8o1qM/PTECO/PRjYSYocA/i
nmzTGhFGQWa1k5lsBUsOrP57grNlbsxKgqyOpKZdMgMpbphJHMRd2co+Q5soBPec9pLvi4Anpbg9
cmvjNUs6ovz9R/O4mjfy9io5mquN1da2dDcb9ja8sO6yOoE5T27fR9Ero701KfdyOiyBjgzT+cZ8
pVGloS16c9hXtFtVPDT19Pv+24z0tERZQtYWBXMEDCRlsXVoEhg87nVjhY2dBr4CySOJzaOJhrFb
7ck3+/1UfpUPCUwFx3gwbcbKoG5hd51WrkcUE7QwloUn6CNf4iqjdrm7QedBgUlQSH+AHMmPjww2
9s3BYpQnNCEv5uE3QIf2ClJQ/xkqYtXDLZpKM+B8kws5f2JYHcBRQJVbBlT3f9Sm8HdRv+P5ZGLb
voVN+9yBAwXATfwTBjAAvaRwWR34PbL0us+Gwv8PMWvnrI8EL2ILewQlxN3nT00lR4nEbrXUsuoW
teyzHZgOgZ7iLGVXWYdzYmyJ5O8MviPpGNrEbZh0ZxTKdgxcZeel4Bbe/fNDX48krHinJ4HCgspK
Hlgwdn7qQ8R6D03XUHqFQVM9GKIYHBqkg0uiAo1TBBemw99GtrhuQ2JvcR2dq3MeFdDQeciHVrb8
RXJg90vYnkID+gKp3qRNu8OwtxKImkyEi4D4ucc6blq4/bPIHNhM7fPv0hbryePBF9A5aIMvBXhX
pqT+71QiCQbm3a3D2tqZNPd0f385sKa1Wj+3oS4yf1J+0EmjR8iGnXGtzB/sEpz2IKc3uNOvNykL
exyEetzn9SNLrKO4Qm25PCEpXoPEpeocppWfXPQpnEc3bUozbcTeoISmjfIVFbYCYhcm9FqhK4jp
ZyJgRXUpIg6whpUC55kGxzfcfw6MigwovExfY0DQm8t6Jv/4+jZUEjQFBBxKsxWwXpgiBcjQ7PFJ
mp3u2I91uMr8HLBRkjDTOikrGak2N6nAL+dp8IQCXbrytXPuF4Qg3axMBdLxm/MWflDwO9lkUzvt
heD0elUI8yzz1xk+RXzh9QdcwGhzzePRwhrX3r8fqPX1VGkx8QgjZye6J8RwsIuhvrFXYvu40uA0
fBYJ7GndHOFz2FpODnVrQO09K57bN+yYlAilA12Bha7OOzeIatKMQQilyhZ7I8bP+dNm06g3FSfp
D3f2Mf10Ry8AyNEhzt94z/Bptknt4+CGDpxoWOll0BiSMl4PrPv67/dudpzdbx1JZcG72mr7iNXd
5TVC7fO0ZnEAJ9r4wTOKc8wCyJb5+QQjGh0IeH10AYtVG3ZSAfjYC8CF8mTBXJJUPrVyG44GPPUh
PZIrlzc2HLse0Gq0Qt8pcf3h4br+w7+RrwM2HSF4imTQMkCZQ+as74A1dhPgv5ipr4nEMCgAyC9b
ksGzWik9asJMEIkrDfiHx9UjRh6QF9v1CXUYwrEYZrVv1g+LtfK+i7RsXo4ZKxzlca5HBJzVrhFz
v4lkja+Mfwnn6UeIzqrazZgw3I8Ey9myMuxLAOR8uFXNyBVXJSHQA5A6H7hBvbvdXmgiD5NSD1tv
T/uCe2qnUuppsrt1pf+hbBSNk2l0CktDoV/hP6rbGgFGkf1nahAXEy6HTT3DjvyiJz4InkMnkosU
l3rhnmJhvGh4zopL7U+dOGQ10Zx3YhdimhUY6yF2PEaQhbJdXx5cEPhKqBbmYJKgeZ0xbiZw8kJB
4GNJY2pj5Q5ZNg73ndEaHgCGUEB5qSbtxUy31VXap1+7mgqJLoqK0b5xzuul29f6a6oe7r6DznVE
Cv87AFXWxpKsHiykn+SfVHVgfAMCz97E267bArcVo1xHAw4NTHp81piEIoEVPzuL+qcddrDXx+Gt
a/yGqrGdid4S1k2E6pKfxXyLWdjhTorZbul6cJgMkEZCVHFBSvChh7sr0hiBJqwG2k0EnEncYidi
JGPHXRZmNd0pRcdoA6IGW9OdufeVnvefX5aZQH3cnAIsddZqleiIU3PBIBUom1x2JRwJ+kalHPdg
mVw1CTwENUn5XsQC9pKwX2euLnnHhuqjDahIoTmIw/Pm+FPmNkY6I/OQDHaq8OC/bIrqeV9KpbHY
w/SgLNbM1a0TzTd6xe3chuGlNQuNgzhD+7sSaAp31bqILE7Py/Ybf2xTcSt9UTib9++vVbWxVF+f
OFmKxie0nezXcAhNwT2PtrdB1AsxCSmcY2wo/nCVIKbaoKxxH4ZLfpaoV7gYQ6q/xQnmHzqhY1Ps
vdnigvwAOfZP4339G0+GTd9DsOAXV6BEKLyhDlTo4dO5l08s0V5au81KUXjUJeogwZCqHZl07YP6
P64xvvFiliZEZTOk7OildXvyTIqIyaz7+9SHB7jk8zSzuXsxp07U4qdpHAsgxiFM0qK8NcNAMpld
qxxGgfImfEHHlm2mG9dsas+IDOoBznd2Vl9uNzMW/VrFfjKYMYDr0g4uo/r965zL044KHdbBJDbT
qd77e8gFSeNAWhueotrr2kw7ez6dBQ6Bq5pGEfHyegIWcPzYzoejWfY+i+hmD26XtwCsmRaxTqfC
4mjimq82zT7O976Z2sXe/SS0879vYZCoD1MuCsU6FHD1OVJPTpYuozPwMVX6j3boEE9dWZudVOik
hXbgk46H7bAxoDECEQ1pAB+caddTkmCnUKaFH6gJiTR7FIi6/nBLuGPAs/od4wjorESeWcaBmWtW
xoKYy7FTVG4ur0/vySqr2Gh3Y/fPkYyZf7IFLIAI7NBoNr0xTWvTjPtLtoqYI+ng0Cy2F3qrtH2Z
nTOKDyqfiuJpxzSRe2T5rD4vCxddjqcGoa3qGz/VzqhiUwDHDqmV7ovgBafghoLNDlGTU280lxyY
w6lvvLT/tKoYqDTcYd6Fj3OGY0J8AdGmn1ZdBGCWmBNQTOhjUnG0Xnbi6+b9YD0t4Knib+FWxKOE
H1TSHe2dEJ0fTStVwAysNlxrzIz3dzHbj2oYb5qWsZ0a8kVTFPIbfGaQy6E04TmyxI/QCwhENgQZ
QlujbLbJ9XWQPzZvWWL7KtMrThYrMcCSl8UalHBJ1Wd/nRWgL96ZNH4gkQgSKHMlwR/2VRxqGqzb
9So7iefLHZi8VH59dfRQPO3CRFBaOxoKJVn+cmmffKZ1GXnb56xB91hXq4/4dbLjSCACHFpaAE64
rkKuBVtv4BYCvAwUBGvGyKjkoTUdVWJwEGTeCYk1OpVRvO5IaO9Tr69O+9WcJjM5kKH0LnPE6+xx
/zq36C6AdxJ/p9QNXEwB8JmSKw5aFskEi3ffuRjgSfQLpOFv0mtdDNvB4uMpTerm10drfKsgxfn/
XxUkHDUkJYYOyjhS5G9HpCd8NWw3Tf7RkV3dv32fMViGCa+UhGVc9WhvMjcfOyi+PZL5Z7VMsYof
5EHd8Z94VFZal+pMbNOwzE/uOJW4U+0gu5abbGb3U7kvaVsf19QyRp6a8l8QLIuYubEosku+0RGs
2YiiBl4V1NhknUnfu2O5bxO/DPgHOxWYFOgN7HPvwajwI2LQcKvtglp0mIjnlG8x+St6Ulzekn2D
TGqELMoE0uQ6rwH4eGQr2+2nsoJr/1vfwfQySJgI1YlrJk9f8iHw7dzQ8XOzDA7wxv3+lWWbuwST
DjCCj1BsP3AAlJjYpskTsjqF5FVKbDJQL9kZJ3jTPdLHIknbaI6RpoMjeBq5UU4vMV3QSsuD2hnE
ydpUuJ2SAkEGSlgsHJUG9Amirhc3EkUULIXhAM2TYYu3kTAGVdbM6GxIMot9ZAcmvmy3HwWSyR0i
ChvJ2ZdjemWGkuaPTKyWRshzwikZ7DtoG2yXmFoHoaq+ZPifD7h9Oo1oC1uG6JxhYvJ2CTbXUqxr
3BOiJdLQS0tUzcWhupwtN5jqE17ZJMtbM/+duNqP6p9FO7zbB6jXs0sOmDrfUDRbQFC6I6eKyLPs
IJdWqHV0+EZQdzkLCjm029gXwYxOotVucv5AJrzkRjnH3/tsoXr066RshjnqB/tOZ9bRWupN0Mrs
GoXcszPEykyFHDznJ+ePBbjQ2xkKCloS9KOreyVNggm9/kcbvMdm+2PcMWak606ZTzsvQjnzoaXX
805+560QbtgIMuSPHYu36s4CN2j3mIkOmk2vYkwOgTdtoicYkb8hMxhLV75nvZkthfvabpZr3t6O
Cr2EHEY5msE59ER8Oku3Y3OPqbaUECeUPVmzWbsYjOFwFYGH9/dMw2d1zYt0kaH+4QmDfzlI2CKt
ruqvhKPIB+OEwcOpuezhO7R5iGi4Zn0Dg9JFEw0crRrykVD10w8K3pUa5HrzHZyfPmNNfElClOyI
5UOFtcOUTvg/XMULXG+HM4hq1AhX2y870TlTRxr76Mu/QHUQt1AfwO3dIz8XDziw5EHcsD39+HZh
geMc6Ed78j0UxoJaOYs2fMSzHVOAmn806LHm4p+gZxhUyVUfpa8VjpfeXYWntGF+XwkbIbg06ZhA
J0ePRhVxGl0+sFXL9R/G9IAqmRc4ve4vgXfqUjE3J2g81B67x1XetSPCiuXCvorD6bSkbhSWVEbn
Jr0HbwrcJjPJBEPk3mNs+Q4kgC7tARQ/O8nYeKl9+9mRKg4umwwAozT8zhCdcp0SjwSEtFGH+45B
KVCZ8QAZYwUR7s/2unj1z40vBAEXx2s5vnECDNaD5Vg1ZZCvJhshKcUMzUPfez/nd6hxy7Em7Xu0
V6dDUHXTatezVehizKTIIuJGc8j2FFJyOuPOmtzPhcKjMF5u+iH69V/NWL1EC4XOW62AcneaZwn5
sUmHdeTtN3E3SHAR+TKFnrRaDhKxs2+E8Pp5hkWYGEMpB5NeV4xwFaj11KXzm0q9M4puX+xq6PPt
S4YaBjXEQkoH+cAAIG5E6RZrUzlDFyYFNDOOlpN6/GuSA9vnDokUR3UsdGaCVUJVFkbi8h5ZuyCl
cuiXHBvoi5dBCLgfFtzKO+gtfqSaM48RwZfX+DyWUkMgblrsgy0rpVr/6cujKB14AnGNcqVsm1NN
6zPUQkrAMdfMFvugrM12egd7H9859lypHZrmoZkGIcOecxsO/lDGezfhDJcRE03R6Cqmdcxbad2J
aGJoZXh7fxF4MC+EN3CrfHiUW4S0RmUXZO5O5GEc+4XcXMI7zAZMISZzsMeSyBVCW+99VKHcaogh
48v7kFTAdKR1j+ut1/3lzyciIzrgNzKw8WEGir1UnaC4ul6nPm5/BFy8utL2Fsg39YCNt0zXqb9P
PqrevW06zmqZGyUpY/qgr6JfUn564kmoOQ5jYgxQArFmGU+GKLEgdlKtVDB28o+WqsxNJK8GfDtc
NYaR5X+58u6lHTGT7phg+p53XMXC6XuQGeVaMVXZe+dMuCq80sCAmSU1ZLqe3PVKwRBU5wa2iuJa
O9FQG0cW4M5jnfmhK+PKcutf7IoAOQ2Iql4DADY2OU6BZboOq9iRaazkSWLSNmBdqlLEXcig8Jbf
VUAXCfAvh/2dHqM6kYond0kr4xBdutOjLg1hWhe60pBkuQQQCJAvyo5nHbxbMnrchJHP9pDudxZy
nI88nvacm0opF26qOXe0W2RNRRxNUBN5tJvrO+MFr5eOyU4gvnQjFD4jE+hxwnr9CD2urJLXbqmj
6V1T0gQ6LFRAGFEmM1989el4iYOe09BQqekll64QxqqvCy/ZpRK7GCBgpUH6blGr0tYpy+jaTYKC
IGJIJEnzk0882nzMBxyUd1nK8w1wRH1uygYzYd/93nWfQJyCM+Lc7mKPF1YuvjZmqvToLjfZm0G7
RXaZui+ihk7qhTM0OPsCn7vMnTfpk2HUdCh0186WHtmzRscCmvW4FIyIXSNVn+UavrXBooC7k/+p
YJTRctnR8Jvvy8lHdmpCoYfk0MIo2SX1UHG5wK9E+K7IuTSXzi18/SsA5o161ZHEuxAtRNVvEWxM
Ip6GVih0ERaaRvybdjfr4mxOj+GfcBcQk4glJNSWXJnwFJj4Fh0HR2VdJMYqSz8wuI2x2SKjnEY5
ulXoGzud75B/HB+CPnu8Fc+Lgod7S+pP0qwsQsK7swLmhRSdhKBYoJG9RCfhPZB61gLX+CAL0GOS
rmW5B3LdfMwKcjDixN9AOxHwwfzf1agrZ/dvbla6XELOW6s5etkQx95qFBikMKO1I1BE+TqvYB4y
Baz3VzkZObckmXxe7pXEsWz+UtW34+ggwo8fVYNiWcIYcphA5Fu9o8KCVkL9aA6PPbEz5BWTQjSv
6xZHOL18Ppg4oZy8X8SyP+dMDI25E8aXePlIWyJxfWspOjfukQ10WD+hNi7uMt/muztDDgnovoqx
kcD99Bj71kFViStBPWhbr053FKtY9nZzZeqPA8+Ah0cTWiurR/Q4U15YgIaFkiA5PEHYKosJUeYt
lB/315zTykBpJuokHbdQZ/L88LjOul+AJUprjvmK2Q8bcFlf1yStG1xj0qXJhwkpYBnN+UtaGLHL
zEvs9o12VqwPJg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
