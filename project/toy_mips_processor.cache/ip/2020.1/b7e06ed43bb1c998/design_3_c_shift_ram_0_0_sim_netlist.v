// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:04:03 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_0_0_sim_netlist.v
// Design      : design_3_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    CE,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6C9fFBVho6CD7Vsn3+N9ZO/hKyIFu45gGO/28RMJutVNmDyLrnwdRW0EBWdSUdXptOHy2x/Bd4G
uObaCT9v7zaBik4aNcPo5dOUXDyM2Y7tP+hJJcRH9LDQbp1GXlHDF+8JYpWX+gpdvi2I7gSl6x+V
kOmnISSxzVogVUf9aXQ7SpXxF1XMjcDDSydpCV756NeO+tC40Bn3c5POZzaHgZ/l8e4vIVi4NzZ6
lVLU+gYTx0Os+c1jJBtDi954Pk5yBo1uVXaa8uqcaj8yCb6vpx62zUooUbH8R6jf8PW+Fm7jSBNk
R6zjhWZdat3sC6wtG0RZ9G+bkX6I8wKIRdGOHg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rgN8zUGKiRqCzLjukWvt1HUv2EVBTnOqG4pl1MaV4arcX3OlCy9WyS9yYFnl8WmjOwKt92RsyWpq
2Gml9i5BtwBoJBjiAJrwpiHfXdTrzuD/JWYtOOfYx90y4gPs48bYWQkce68o1rVl5M1Lyr6NDCnc
9YgYNHK4jO5NtVpPkoBew/7n8DJ/c2nyLPOuitAyEJSRecu2XrK2BqcaenWDvCG6zz3jyEwNY9I6
VY295NRED5tuDxse48C/oZoBdDLKsPyQjfH0daIS5bA2bagYsRmc/sSIC3Q4Q40xIEj+I1T/YuBI
xcIFEbXlbXQU6h1kgZd15e0qoar/csxC6AkZyA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
UyaYPgCiUqRSZemtH/ojaupRmIY1FgBE7/lIpbwqlrz45lQB4l3YT5O3w8t/AnVEnIWQpHDDxoIZ
4Fe+uJFEhWlbAjC8HdXAAh/1ADsgsSfEpxbJJlluKG0D3tj8XflJ3zaSNSEP5IcY2fJBW2oCs/49
npGRZ41x81G7QA8+yCn3Y3LgOHI7c8C7NdAmvNdxPM4p1vw2kxCw4zoV26g1YjYVNgz7k+U/KWU4
0583S5XTPS3TFuqjbFqbhX4XwcsMxmOYssrriei69gF0mHtJpTGbCYAVHcddUKv+hvpstOsKUNej
UzpF/BjgxWar+iPtzMJL7zwG2Aq55r2pomfSstZrVgJi6YKzOaIrwsOB8R+EF7PmtNp4JLEVZYrl
VOinSKnjAjjxSWAcbXoKdOQRH3l0f5Y24XaEQQK8R4IGeb6R2p5LE+xzN5IIP7o6Upcy+WyN8HLM
V4nnRLZokWjB6rpL9VWxMevfyXQAxNr0jLtgJ8EhWLNGJWi3ni7/YlE2B6Q0B98dHpdC0t68tEis
kT1+aq3D7VJ61E2rU292BKACfrwJ76YmPYbUtJ1mx53Jp/KOrgeVc++Hdit3c18Qa2OEOK2VkaBH
cTb2YcQt4U8aT4JLhTqhpOkfejYFHi8QCWlwF2VY/rSsZ82SnewoJWKdJSOUVfTy1GiLXBDKeZLV
REn1I5ohHAd7C2On6cw0eOB+iPla/GtqzR6bSMHzDFDduDVvkCfd+om4pzkARGOHrUzH3hSQx+vj
ZRnxk6/C+YaRVfOLtrr2O4LSgyMb16+U60K9KR+4fG3vFc+OfEUV6UY2ZLvo68sRWgOq2ub/t5Xr
lHoLhkSV/lFCvmiJfe0laiGMNNbn5ltGhmDjuHzejluzVoQzN4nfoI3b6UIrB6Rrgp8J33VcW3DS
nmYSC/iZZKFH+ofBI5zIRcXG8o6fZyJXJTRlcbmQPnkMtSlMi6JvXQV20JPjM4iHEcIWeXqalZCf
LIMahQr+xCNPtQCpe1p1xWdCXiAUPqyNX0hqUPEPGhmZApJvDNLNWey+4Z4OqZhiqilB2btGwp+9
PhgDfDT1pBHlYQ7S8TwoB6OJJ75WGs31gYWHMHc9iWhs1zZanid1Y+FnbUzMFsTlhll5wbST8Otc
KtqOYa/ptHNkErEic5QRk10n8V+oV6hgE0IemMn3B+i1XSWxT773fG2uvOQOzbdbMYVlP2ARPVhl
zrgUDH22kZx4rtNK+Y+5mjqGZZ+BHHSrbVdTi1lE6lQIlJv8IbMTaUWDAxQT4NUBgQRr2pnNyQAM
6AyPB49e/wdn4A72TJVoLMxdRNgdnjW/41bjsKf3/o/pf6ch3brxpGx5q5FqBOMu7Ylr/DbxLnJv
xRsR8gfnWLOTuV9zp0M+ljg4d6uRQx4cUiv7Et7iQ6MLTXBnttm+3WJmQ0wQ10EDM1/w+6VkWZSc
6C8JN0FDJj/2zMMg/WQy0FCSGUmpFSzjMj2iOp68edORC+a+Lfqomiax2YQjmEANpEG5i9rStI63
KGuvY582bGjs70eYK+fee8DTy8HDJRQSaeGeW3jjv1HLxrSETMuNXzHrdXPfFbHTmE/M+12E0Nlt
cysy2KZo1ePfcnmpUvHuf8fu7ReHOiQIieNn2UC5sJmgbGRxAuDyjO+thB1MKVtUza4tlt1K5DNH
BlbbK8vrZlT4ufDWg8eIghGSdEhgNc4a0PHhKYaqNyEy8UGM1IRJgPts4wtKaTDf/uzMxcv/gkL0
qSatpZXC8eIri8y2sjZyNo1achHbOwUZhpdEwrlWh6b3i+TWgbyfIKk5SUTzCo7TePo1eVDIPDAZ
9+Q202NahR5us6ErtyXpPWUJUHYWVimqPjtCul8rkaQAHnczsy7nQw4cBsya/X/NvpjY8IBfX8ZV
VCaGYCQCOnVH41HA0d3qnQTWChd8xM/pz8DnytJq6ZS2vyFMyMY5ZCTTHXys20Y3L0FHjFOmLW5Q
t9oED22eTAwWIsajfPui+y2l2pLwB58PdMVeMBtaT/NckMgnM2U+DBeRBQaioN2p9RxiXHnKn7Mf
98MH5YcskJANrAW8vKpL0NLWJxZBJ5l13CUbhIMwheSx2rME4iInCjSdZQV3MXpFjBxjdidz4mrG
Z5InvA+j7bGd5qg1Su1V8D6SWET8lhtVPO7/dpU3UHXoozmGRdqGKFunWDA9BQKfJBxWOdVhO4L+
Di4ChERnwAvlLjdTRixmggVgSpQaRE+jaFjOLE95PAhtfUdWmMFAUE/eIJfA5kDhWP33mkKV2Af6
rhwJxkvMSru4V8WaUmGwcwZXWRXVCPpl+xtr0bP5nMSEcGzyw5c5QvFvJvNrhEHJsbYMT9XMxRzU
repHwm3Ql2Dz1eqH/1a7wAwWjfsbIvoK8DqFjWVRxxGWTmnSm85qZGngZ4toL21Qgnh5osCt0hc4
C4JOC9Dq4kwbiyLDWkMVyDfOdWY6VHpGcrxEPRk0xc1ctG874y6FHCo+nN3tSolx+DAkTHJq1eGd
A9+rIU+tcB2du92JFsuSUr3yrFE9VwIVzooyD6HRD0qy/Ernpe54YKazoeovdLI+vWDIczdmlUm1
0Dk9b6cAI36E7ynKC163OLuWhwCr36eIQ2IoTLn4X1kDu1lmyo0Yk7s0s4SCAxWozJzlr0U9qi56
813sCJoDg28+4VZymCxADQqwuSCLt2btG8XKCEUxNIDFfdFsILoLQ6icxGxUOWNeUhcqUKAPI0Of
gGIOWOsCFh+97/CjlLM4igVEpUJZ1WO3w5g09X/WepYZFN6/S/QFjx+YnQS5nVFGynl51Vzyg1fN
K9keSjwjJ0t5/7q7RMSrlcp9fEL54PJxmY6znzcWJlgffbcOs/0IK/HZ5+eL58AgRvkSEI55XMLD
m2fV5weJ84iZjKH3PL52Ww6zQp6EfcK0oxBle711tZdzGRUB+wvPvyM86GZC+bluzA8MVWwIkc5N
jSIRCOco8S0VuyaBHiHXFxTG66TmvT+aYilwHY2AurQ7THR6a135IlL6QWJxg2TDIe/i5b68GGTM
PsBBVfnJA/+LXCJahrCVN/MbiN8oBZwFeKim2i1jHUYezWDPb0BNpLjruk8OgP5hZ7mWgULGoWTl
B9YJSHzBBnm7yBTqud3v//XzaKkuvY1UdnpUZdLqHtUMHxiaoSCrCNTJmRQLAU0AFdmtNN56X8Y8
x98eXO1NET8ElawXrOq5moq388zlbaMD8wnLWiy2zFcSBlJCOrw593cNV9AUQjV5fXXvL0ga8Uwv
6U/wXhleLnZ8rmyADx9zdGLIdlDJlH+bIFivQ+NJ2GDNcvCWtVwudH6RxDkUtNC13l0o8Ot6aNJD
/wJa5R5+drcztpSFY59yJg1KMXS/yKwtwzyBjRHGcuE2PivOaERT/rZAcb/cZ3TkO5H1kSr94szs
7vEeQAIvRrW/MLgFF/xv/3c3PELaavnsTvkZoeVQgAZwbrykXu2LfoypRG0pb6HdWl/ge41+/O0I
rGIMffiVLG3m2l7ZeKv63svf03yfKmlYi1uanBRSkPFIE3eCBT04lalOKYCJfWtXwAlKRgddvU3Q
8SaTb+67waK4eBj0ap2Ui9maOjw6foDmqXM9jSrE0NhC/3eT0O97FqroPm4kxy5PH9EBaqS7pzRa
2gEyXsINHeK6WNRXmslc1pwHJGXl0lUWZsyD7ONP24gAsK3Dj1PvSDFMdsaxJ6BINKPOR06CtQ/T
Z1/T5WSokYBK1Qugu3Z4zdZojK/dBWCsx0rvN4Up78ldeKVFQfoqI1AxvI40tJRu3YoAzje+W0K5
558TIYhWRlpW2U8mjgNGz/ItVRs/pJjCvWBnTPOPSRW6bhP1I7D5c6wuuapMPS/WZXsQqHFOC7jw
JK1Xhc8b1+mPHyspjEr4EOsANlRxRYA9XAiPlhwtj1SNfCtXm29wNf+foKy2lEZoQRmiOZhHdGrU
mRaePa/yWA2F8yYyJ8fIJ5Dq+iTOFz11dzc+w7aoUZSUhPbPklrQGgq/Ik7ovcTS7xBjMEvqYsLp
J+6tBCPz/ac19qIXCdOoLMgx29MD3hoQ818NSycrqsIOJn7uQGaVfBK6LL/8lovmAJW3+QXNrtgk
+gvuabmXIB2OB7bXV0/vipH2civwqtJtKqulOcb4jueIqms5PZ0KbrjrKQxLQrWujiUzjtJ6/cBe
fU73kdXvLDdoeAe4e2pLtGMWUelSCFr9ahBpk3MMJtU8tgJCzTlueYy3M9oBeP5SPFDGq8qNzLhM
odQJ1JjmbYTcWSgWqibVbTq6ZV1+iqJS6mu82gxCuyxFGeheOK+ZZ8vilaMth3az7Wfh1Qd3laqp
HPRMH6Sv7MV+zdN4BqJv8csP8e7FQLPhdbg4NcUN3ApAvdeFV3LrqRgPJYaaspmwBJGuSy2mPSPR
i8CkQsvL9k6goL7QAMb2qKfPpi6+vhCKL7uFOU0GTT5QVYB+V+GTyiys/HbIJr8Zne/atXPmThpM
S7aAFnd9noMgc5U9ZyCJWupCGBlcgDb3qYpi9rvqXt1cBSQO1adYFrbJaEUzgQDPzy2YgtfptWjR
qFhZIQglFnuKMvWBTuXokcGZNHAg5Tok0BVqnv8Rdm1mAycab9eJ87IHLw7Y4bDMZYsAdtJgxXPD
GnpVdhwDUvQH3Gy+iZAFNwjC0O/xW7V1JlBQTuyxR5ih1UJen5ySEIRortmJZu5QYrXk3FXO9f8X
DsdZqdo3eV7MVD1zF54ZOuK3MK9RWd6lspWFXC5/DLTgFelGeQSW4TFj+snWGzv78sYj2ImTZEo6
qNESrZ3g4DCsWqfnBV2l4DYA+blTmcCWdc7cH5WSo3Yyt9o0pPEEHeX3ldbRhzgLlnyDriEbI7d/
BIRvQj9hLbF6BDMVemYll9HW9zkMpp98P6z9AtyhVJNrsqyJHSUdQvR8FRFn7Zwr0xDaqvSnzxI5
NtwM3JYM9oRnsilqj53iE+6pM8K97vtFiNGyzmoaL+srDm7/JH+9II8wG1ixleKE2TkV12zpd7bA
5SrcEV6rXd3Wfd6/TsI58Z++Ia6HuLX9AUB909vGrn1fFHrMtAm/1Kwxi7njmW/rpZGBlVHFl2jZ
uzwOX1UMdzgO+Ep3cuP5hlq57pCLIhUO0mrOTv3GS5GGFyVKocmA1AKXIg85/3iN8p+fS4qaW5iY
IPZa+ZgsoneP5BcyW2VjTI3VU0wS7iAowamVGAzOx5amV+zmz5en97ph8JUWT/tT/l0IZadCXozg
XXDaPlCxT9a9JRAogFbfXZGgOWTYA5cXPlpbfSQBChGKMLn1m1TW8QvEt3tISrpaZ3KHebcyUUFg
dzBvBy6FADmNi8iII6dcOb5EUIPEQaqlIWdx2Q1PXsT1lKrtQHeKExixjU/0hT32bxwt2+YXMK/p
QhxLVW26SyLq0TcvnjkvDebbd6OVK1oUPXQtMOYRiyZCwTv3mQqG+vm1HCyZdMIGDsyJuqO9HXTs
DxSYYN+6FMRo2G9pvGGJqNlCSXHG3y624dXWMO+ELBy0PizOWIAKLn9GO6X3R0QtCrmBKqDX34ww
Lurn/5uzMqSm3hpFMQeh/hxFCfAJJFzVSO2YneKfFpsEdK9aSJTERLTlvhAwdV+4vRogjKAqgT+/
XpmD/qp/7H8fI8ApMohz9IU0EknuceA/oGVAHaXYDZzi/Bp6mRA/9+RGdV7I9iqFwlroQmBxE0/I
U92L5+3MIZfcdrxVfhqJbhIpvdPWtaW10rewYmXvrokHcoImJOZZEyKtyqTZ0abs6Zfr3yV7OFIW
MwevXKKY87S0BuVTmAGi2I3c1O7AoRzN3c25gjWva0cAYTlsWd0BjDTd6PozzuSCKTpZQWN2qIFO
6860QfMGKl8rA0N3XRe1W8ccQ4RuUUwCNtqoKkZDupX6lSTL3miupFwnhbCD4zBwe5hwq00+jeDb
isr18Wa4YPiVFFWKfKMDuYUd1ry6OD3J4hbUcEjYZyzCjCHT0BDsCTn1lUqM8WF2BKLK7+W5Fx3r
ch1RCesyOyPlSlNAR79arj774b3q9t4Q+TbGEi2r0IY5VYa6KxUdkgQs712eUH6VcxZMpEuDZBq7
JDr0arBqby92XmCcdfJIuk53lEfzrS4aryzGG7VZS86pzf41gTMIP/B/VZNo06+QBq4VnCftVIvM
KaCC9HPcmeg4b4t+0wOeayOklzKQUqJ9vMjAXEAhJs+T8EEC54KXVS2el/hh7cHsonmYEdJT2S5z
bLLcHx1sElcFfQbMy3nT3cwEcwrNn3cdXYjN8lA9OkxnCepzTBYBJxMZ4+AmxO6Tp3SME9rPdzVV
cBOyzHLBMg2CIZ4vbFYf/IMUMKFoggS+S81DvT6loWJdjLXyuWZx06fAgWRWFuFCfxjGfpX2HZyz
XcI5Tl+uLL5h4y8RZ1Bxy8QNLtXuJ+r4Fs4i1b4J5xFvAn/FepzJP27A+pwXs4fp7BeT8YByDdqX
RQ/nfGidWJk4F2IeQSVd+AY57Cu6Ap5s3pzFwfH+mFOMeKJeKFQ+HUXywZCU+Ev9M71S0YNt1gYM
C6iki9hrEZ+d8thg3D0YiNktNhOv4MkYr/Fid2cUcOvCgGs82gMaailyWWXhnrTwglZojz6XzqFc
5YWkMqKTc79/Qrh2TVlsvmiU8acpERRcAwh0g4BzzlCE632D0mUWLTGSHqbDgT5ebzucmgj2P85p
JbxQr083aR0G5S1IcvK9MprHU7CZAR9ZLtOcqqm6rTfZFDhEgIVpYjm9ynjljDqmRzxGmrYtpSPE
Di0IBA/rztf2Dhky2fEvRDUttYgzptA2y5f3SB+axMyzerSNR/JXyfYGF199l1ZqiPYuHKy778Td
xP0xklB5KFXI4qK1tsyLFL+rnAyNk2+6UA4sHfWkLTq6FTVsJl7vLSfZ4x+iVfQGA8jo8fyyilVb
nkRQ7x7TTbJUltu9uxcResbVOnOt6mrg//PoPBMB/OGrW1QG7oBASpoZv7jV05ajSFbM4ztvXyZh
izeSQcxLyxCAs7Pt0A/iGlhwkPhpBrwazzzuTzeL7VmgXGEHk3UxtvKuaw+QL9LLghfSUzKJkoOL
PfBfQObN0lpJwFYCbK+mQ77kNXh/oa9Zw9Chdm8NvOls92y6kdszSqIy6isE83F7lCuDxOt5Ze4k
K2FT0m9ZSWbpXsuwGzS6DPq41ihMCM6t4+VK7u/9Vzw6YCcTTUpVIexLu5EzpOmkUETWP3VyHuE7
vjdkTLSppmVlFw8p3d5ZjATtTggwoUkqjAwd3V/4EARaOOiyGz5bdy2lygCm9PoCGu5YvvT2hidb
Ca7vOMwDv6ljaTHflO+s+Ogasvr8DkHVP+EHAnMnJLdCUH7avNK2BnSvAAnN139RCC1Vlj0oUD+j
3csKuvEfbjgvDN2p6VeWQzUBRCeCxZybP5WU7dwFn1+f4K58eo5geY9bh+nTBcb00FprmaISE0Wa
fRzEh62uypx8adjEQ59DxBV64LVr3FTavX85FIU0i/5AIuduYTBoGQI3vVhVHwo3fQ8Rk4iInTz+
ae4U+sASOUtA9ewrlBygZXoAAVaqAuljDrG524t/Crtn+sRrJ5ETxj4k8ITkqvfJMFekQwpmZsrP
yk2N/3mNvprzNSkdYDpwYHtnin+wgi7yO8asIyJmkayF3mPl9r646riQlR+VkggRvgXDdIWBpLvO
EQzFKA0ur4RpQmDGAIOUmnkeU1mOqXOwl2Cpj0V9a2PKxsxuNO07WM4fTh30UZL7ZSRQe5ZhFE8S
tTeE+lXzetj62uY4eY1kloc9whwmP698BVhZ4iLQGJfWYDciCqWtbqRBwlmw5RYnWXwVlbSOu6bi
vNiaMO4F1B1ud1LZAE975Fbp/ivTq4/GfLsu0PB3f92fdRqaNLaFTSKlDOzIzbbIiRXRJ7eeib4T
Q8B15Z//7uu7wYbc/4uopbUCQEF4LrSHnFbDNBPU64IJ8+D7EaS2F9aFX5hu4b7DHKQ83gxsJ36R
ZHMSwMJfyUqfMxs5P6wP9ykdNDqN4/bH88ExY762sVwktpnNR4vjBCClhIriE+VVliI9ENoBaHok
wBSOf/IDDMALxjjgbBfqXj5Fv7A3+3jvBSrTXqZ1t8r03yl3Wiq/GQW+5VmluED3zqbMYwC9Z5GF
JxHEMiCr51EfQOCzjwZ7Yh3E95/hIrDQG/57lpC7RjyllK9uLmVcLQcO9jojBhjp9JFyyGr/3Zyq
ckqU5gUBul1sDyo2FO8tbNtPhz+lv62+u9/p61gkGxFEXIxpcDXN1+aZkfUzSJEoQntkCdonGCL4
LXzbimPj+r8PIWy3V9kd1JPUB02KJ1ZmXfZo4YH0adrvJe8TH5lr49EhbJFeOZ2HE1k0MIp0OcG1
Qh9Zu6IcMK8gc32f33wYlVHsFah5AloVYa+lWwzFMBMmWj2aVGUvU2Os/lFUtIMDS2o7wchm8D30
Gp7cJaJqRitVNhG72M57b7oGSyimDBI2gS8Ov2F/impbAX+pcKryBhjS3IAARxMO/hyYjulYQN16
t2/Pin/4EUBf8WAnu5/a/UmqURF757PorWGaYKiH9i1tsoj2tVSGYbE3ICFAs3AmRTPGPFIh63Pl
X9KV6Sq/VaE34fKjxPAXDwnSMWpYlR4VXbg7EW6i/Dz+1VMVxQDsdFMNdL2EfmBmyErfy0LtJCZr
gENNVZRYhek0033uQ1bmgMfWs5lKhO3sVjWcAo2KRp5q1mwCX54o6kspxxnciOGHY0VxKHruDK/2
w3Nb70t7wEoQ5KgZhdBqwQd2/7Xc2bLWDvMQDEfwYZs7iCY8vOs6n8FQpROpoaFnAnp+Ch7vnGex
pcU4fhs30z+UORVb71y2ysivLSajpcX/hNTPa3o8zm20wg85x20JMdbXRzPUmxMS08UfjGJ6gg/L
4k5HqMnbgfCGEGbtzEWMK0OpT7yOL2fNHhyFhsKYa5bmVkv9dPyZZ8mtuD3Bh9K0WarTgMDhR90Q
ltpdqBBsctswy3euImYUCU5P73sYUJ54e6YSpKj7CyLtqTFs+t0hFX79jDhIve33axdHV1thpPJQ
1CbSmbwhHUoAT19CMMo+3DZyR2E7r3Qesl3g9YLaAQW+dWxfHIVjISbbZzXLu8eL78YqkB6FRORT
w6E52n8qxbSDuy/n5I10fAme6YiN5Gcw3KyKHMh3XaUZgRgqid7sZNQgm4luWQP5vP16YR2okYGE
fqaaNHLhh+2c0PLQGz1yfdlVUuvEApzAYIWJSnGkTVxwzcIjEtmz/2/oJhkmpj3ewCzBmxNxqZ74
o2X805QahjJZnQzA3s1y9cy8Nbg28sDAcoZaB/rrtFb9tv7rg3jKxJUeOcy9AHvhVL+76hgP6FaH
nxPQo20pdUy/GBYPx3naf+9nmhCRRN1kGQsxVeBVTH5yn3OOhjhMh0rthKTheHON23juxJxM8A7e
vpzEgFOgdqeZ4q/9ZQ1sfAWNohQzYg+Z29wVwP4G1XVUtKWwpmSv/1JphYMNpuzfTO9lO7ARnxLI
v0OfuJBev3Y5r0jUDgqU97HkYltvWhnImaZVOASfgLNNf9zpsOVPp9N47Ks4mYEeyiQJ3VQ51Q+t
wVVRQvhLKwSANBB50w4Y9YmgvUDsgissPMcbc/TdzsRDkkgU5i4lLA2FKtTEi7BHX6OkdWoOiIY/
x0FCWKbgSKjBHZP0knDJt+JM96BInBz0Lg4P+q6fUrWUuAaN84ojucamzU0WQrmETLq1jlxKuBGY
lnGQubjKhiAZWPmWx7JtXBeZnNz24Fxv13MBurvjxxzDyXTQQyhEbfk8IfgOyXw2JSP0gfLFzI3+
fW3nnrfo/LXereDEkKRio9cLLbJK9D9U2C9gROnmdXhLnyBNDl2O1qIkyRZcYWgLu7lGqh4CmDIC
Q4rgATuhjj5hrw4FbL8TbaSKE1nHBgZ2NqXKoCNFibsPhxplpvDUQr/lt2HcI6uP9ZvKZk/j96E5
Q5KOAUrfTZczcn+BK/oMqV2PtfpiMHX4ZJ/KvoueCsk0Pf4Ce5aReKjvFsG4O8hRRHsUhLU4DDXk
BIZ4eIlD0wOl0+QW/sRngUFrLEDROxgg48g7MUoQiO/AiuQWtrV+bZIlxqlrRK1P3Rq9Z8WDJJQK
ShWOHV+IO/ZgABnUF36d2wR1JQVzmZe81Z9hDY5TmwB2zQjVoVyr8FRKGKaYF2EZUGqwB3G3SgPR
nCqHWem+i1mWcd0Cmgock14TrxhvUlrKJh9wWFGENFKmg9bgMtyiTwJPZlas+4CoTG9NGYf7dq3o
9mQcPcwxzQWR2l1dqMO7vPtxOw4aca22uRK9MSAA3W/LlhKYSBDQlKBuSyvM597IzQLP79CE3TJ3
QjYTo9JBWEfpHwEWs7zRfeIKq0A34ZKdlwGzTJ/DqPcFtjxA9cZCp193V1f+zltOYxo2oLMlmtL/
XwqrBvi1AyxST4C10Wq3mH0IMSrlqScEsMqS9JBpYsOZPPvXIEuWcv9Y7WWVzPEL2fmeXwM8kqwE
8W4FlPGcIOoFRvLefZsfwLQzAnwJN0zMEB9AvhTbO+g/z4WSVVk6JPsPJNYJszyMDorp6SRpEOMh
0IV1NwU3p6+CK5cg0ClC/0PNDte8u4cfvBpiW/XxczgPu6B0Q3p+1zSz77WhOMPFhMftVyVKNKQl
7LORb2DzvdIeszlo0dQ8h+wcpT+Hz2oxK5ocduUFdv6Ha3B5/o7vUGBunshQi0X5NGKT3Sx57Zlq
riV1Ci9rVc+qxtFm5opmf0zxYQEa8H3C6Wqx8UAjB/A/MvBlKSCGFw7b4bxlO3kN5Wt0LKmQg58m
wpjjjm9EUBuSrAb0bBJM6JN9AW3brXixQlUrzMFbMGYdP4bNh3IuDm4AgTGvz+eDR367GTbAVvs1
lbv08LDPODEd8zQAzE23cggDkcenZvI61Ii9323deJozDIQQcZFe+pIwzabquKuyP19gFIrM7BTC
1icZMOBOrh0+CBFHbw+rjn6Mx4CTppi6pEwPvWC0Ih3WF7jrbJIW7DL+A/9NpfVdi6+oKgobQTkM
RkxIBdkiarUQUU+znClvCAngJQZo6UHY9i1ko9+q/jgHltmDSyrxeYiMN185jUXbvxv+0Ugf5T3B
Ue/SJfukj4Sj1pBng/FkP9l6FKNlSoFfalF7OGLTZ7LJb/ruOpfwFQTvsZNqqgwzDLwxVKXRSirU
nlxRVXRG4rrGpn0JQOQFiJgOd4VXjWBFqUGWU3sy8n3mUIsPOZE5PzvQC/qW5CQX3TnVMIRYLnsy
zOVnq50M66NlMBBYhMPi72gWHP2GDSYEQzJZi3CAhjlSMBRJMHI4gY/1ziPrPA4mI1VbeAmLHeCN
Hkdohiaqglf4ltLKWTuyCyIpAQr0WC6Lm37S+1mT5rwtbb8U/5uOORL5apNkmehTcJEftLf853CM
fmiDpqyKwX/ajpGlPQICoTBSZuROXx/mTXl703W/yMIHCzI90jVqzBFS73MjkCen4SJ9iTIr7U8Y
LNSZya74af3T76meaKorf2awwLp/FsdWystP1X838YkSd17NpNTkxxPvP4SSfn6RnMEcJFFrIPJ/
PiUXG7V9VPRG+fUzPWK4356q/ZtYXM858yB0rytxHpci0iqVsqHSUYpwl7caWq34sKlYbneo1mdD
m0YqyiYJcpDEgSH/MXR0/AeM2nUQ89X2U/5aqyoLAD0M0VLmC8qe8Kxz/lbBHnExaYd6ZIjUfpZn
iAZQ+13tSjhXYGFPzAGWHIis15CG2AuLE/WoLojWYNxbBPoFThiP4w1S+8FceNaE4J2vbo8y5iZL
a2E/PUPFcZy8E8C2dSurClmMBi/09QMwOQxNRyA30jKfSPK5HNk2OaWhn94xG/tw2lzVakb0oqIU
/uLqlogOc087A9jlGoJsN7O++j7+jZPKUIdrgi7sw4ig65yx5roy3W/AW6u47Xj8KxYRnjF/l/qR
Z7+mSUNAtgsGU9jLhoOlK/QaostWbR7ru1VvJddVGK7p46YgFuOBEoRDuEtmtHBzZOcHPVaZ9GjL
Xn4RvjZC34Btww9W1dm266Ktuys5o4J+wDSrIyAYt6tWha0TeExaBiW9Zv61PasQlXpgW1EgNsJg
2+PMJcxL2DK1r9MFWrM+hMSXf3Lp1H8s5+HplTsG6u3BkOOgcAMLGzXC1c8PGCB15ITzbjaYROUX
WjM8Jibix7+lUHpUlqxFD2zfJSp7MfrdnoupimejmnxOKD5FBXNqwJ5z9MW0s7mFspV4u3ThiNXQ
9kBfl58hGFQGJOY9UZcT8j1uOlCfVpea96Hib0zSL0zaQrGq/ZXNXOMeFkvc/yGEF2JgF09MjJrr
IYMJSEFpTRzTcKb/UibYDZBvHXxxsXWmRnxdbvVCojPFcd6NpF8qeTQntFDd0EW2CPR16N9f6fP8
idqO0Q87o2EpCn3Xbd5DwUfsJEGiQLa7lp6EABq0wU9cUhfUU0bZW45Se07/4FtIlMdKEV9UEyg2
OEJ0udVgZuuNf7W4nBsW/5H84VN3igFB9GWNw9bQF53WIx6GMwcfvoFkjY+/7mlSknQ/cqDINJMd
/dqL5G3kHV4eO5nsILX5YqorZN6+ZnXgAL2t8PUQQ1FfSHFMFE09NzXOirr4v9mYdzMA7wALuP7+
BjgE+N8t4d8JXY8PO96ioWRByXKFRMOOnUNHuPE298arOgT4CPaW28Iy0AR9oic3LmS2Ub8phcMA
gUlf9SZZgmg3VOYpZo18cxE=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
