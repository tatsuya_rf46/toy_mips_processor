// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_c_addsub_0_0_sim_netlist.v
// Design      : design_1_c_addsub_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_addsub_0_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire CLK;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "1" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000100" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "32" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "1" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000100" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire CLK;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "1" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000100" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "32" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
WNuVF9XwE30x+gxWUkWPKYcsGdj8lemWZNEjo1X/4HVFqgnB1rVXLEdruhTURSqrRTRuRAhlYOs5
aeMC1oqTM++23vv2EWQKOM4zTkkvkGI+VzkW0AjHzdxmQMm/adCbTgGn2B7f4P4LUQNelcgQnAlZ
gCNfAqD0zjRunt+puj78EDRE1ECRCccILIgqbScJZRoSVVnLiqeLOwaSIK7xAwzM4QtvGyydR6AP
6aLUabvOZvF1ZozsLiNY6fx3GX8eJrGjKjo6KZJRIzei2oC/TBfovdnRVlSjFRXORChLvOxaE2+Y
8/NEnd2pVeHSVjZKCTKZHewJgO9qdz7E2DvedQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
N+4FhRUpnIHuZXHgBpObp8aJz+bABwNV83P+KKeVGrAjmdF2nKJCF3RPf8BjUsafMJnpJI3J3r/f
j+bfJ6YCU1udPv3PuA0ZOgtrnrnOgEGGYOSD70bBHdABX4F+1C34k6GGeLvT64R+5jOzdknzDi4w
bg0EGkL3Okl0GsUfQasowxrq5JsGBdobtpltaSN7pLrvjZvGLgeoQk5ATQ6TZ7G+eA49vHbhpUOs
a6W8AZTXbZ/Wf43aH2oYIHGSRMxuJGuLqNZV+b0OlNa3M01MauKBJLr37rfg+0CCzb0FZxXe/QdO
pbPEd+Bm8u5a0+wSkKCXFfV8adR+yPtNnzmr3A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12416)
`pragma protect data_block
kbnCeZmXPlqibSJMiuX+XnDVymbwuj95x3mWw0x5XBjfC6x+CnPX5+DLRuKZO9ZDTrAlbDRJ3QkI
okZMAXAbS5UnrOArPaJyY4eBmCCKrO4+h+7nC++72Vp+ncLprSszkwsbzFVQg56Bi3rA3+KUbEM+
1qPL8TpdPmbydGxjehGcZg5ZiTZ2W/4bKuW2OPcR/rSM26KNRqZOULzPcjeS2sENpNzOwYNjTll6
zZGh1dMf/A84WffUZKnMQfdFvDaFr2xk47vBt2/sLMQkM81muiB5KTnqP6CZznfTGUHPm2L5ca4E
sRlPvtSFPU48U1+80F3MheghotXVu0XNzydjJ8WNkX2caWn2yW3VfNogqwaHDmQogV6JMdiyS4+i
hULVzbs8ODb1X4z/CUDyZZ+Fiw9MC1nXpJNCsOVc6tUrV/bFW/CRsjJn0ir+UVp2reZ7L2De9N3A
RaTNjue9qXdnHJ3QcwZxqKQfB1Lw4hH9cmfxDvrWHrITs3eU0c/wJrBv3a+GQRtq1JFLvrcRuCS3
H6C8y8QUIol1d7upjJrJ1x5QwjDD8eJicvSuPwgCAOu8h2XmUMcWgEB4z+2WuECkKSWeK7LqBhb2
JuJpEa1uQPwFdNl04IKlf/JXyffoTFP616Y9eDlGGYkFHun07At75Tq8Z0uDtKBzX8jNxPuCBW1g
Kesj0vkdDphSptOov/bduQT21gJcFwLnK9es7F6cbZG6FTtZ5wY6nuz2dMRaP93/asqCz6/Co9t7
lSPa/4dmpm70Bt8iE+nh36XoC6ZN/3MQLHwQvKU7XW5t+tt4+Tq86AQJbY2U6Ycqs3FNd12KVnLv
pTip0brNHrWtubeVWMC2pDINoztUm8g/xiYuMrHZsFz4HnZkt0cpLU2RXCzpgEVbILep3iM+5Awc
rJtLEKZUFkVnrvCSL5nAiVlHuGfecIHQ5k+jo+dHe93YeaZTk6Xxmz0h/yUvBuyGD4t0vaL/1FBt
3EWyisJT6NJ7KHHnMPHJE+650JY0ikFbv5i2ySsfiJbgCEKACXhM6+XRZPXBM31LYy8Rvs9riEnB
MYuLa8v+mOeR86K4Kk15Y26MNwXOmZ+WLfQlYwRUmnTY09Yd9QKUzOlo6NASjZd+AqXvDmikV1E0
6uXSWXrnXcNX2yPtQyGpQNzWv2Kf7Y7G2yzqAfF5VJJ9CCOT/+LkI9KSmgcNgpnD3E9R1ONE4Qt0
pAo1dS9pQL1GLQICbEQCkvzE8keuagr38rqOgY3/burminpOTVV1X9gsMD69loC6DKny842+Ossa
GY/lfiMAdShfPRWEJS4ht5leCs6GV80H0oQ9jBvIBGCcWMfPMMHjilFaZvxY/AOvGpk3cJKZkRi/
OD5UanF//dpSpoeT2pbP30svcUNqxBmsKqMPyiaT4V3Kb+2CDQbBeFutktt8JdR+ZUCVzy3SdzH1
GRaSjJpwwJ3AlNd/eBdrXygXDhGgu0AB4i7+K2MgjbBLkwTw+OVxovFqEJ6nICtRKlejMVEd4OBc
Xr5XITdoG7kD8/JzFs2OQpigNKLRVmin1ETOSA36OaL9jXresIzsb0HQYM1eHI46fSUjr+tqERRC
oYemp/V4hW5TcsTyUIJZtz3COBbCHHXXIVTUCWlGPj+NhZnNvDgYAoG88pRSV5NTwdo943kSqh4E
mei4S+Ikg4DchK9N8TZpXSqHDH16vt531BqihZWj1o3S9ZME/k4rIeWhbC243jVQRUfByTDPLNtB
96tdhmtHZb0ThJvNHu/C6ah/7lroclN2Pwa6xC95eocMXqMqe6cVHhetv0kPawgG+qeGFOd75iD2
Djz4BSKUd3phb43eqgfmEAl1OmpUXdUV1GMzuhMjHucQI/nwOTIeFaTH+gtM56hsKLeVP2ERt/VN
suoy7oX/e4JHXgrsfXTCWaDwUaQttA9GQ1HTMVZB64GFStVhwLpOweZKahQrm45wYq3JaI5Cyxk6
NpQR20cRey8ldysG8TtSAtEx3oz44USh9y3weBrcct4vE8hIvPs/p0WHFofb5A7loKIYtsBjlEfe
w9LGxYURGTgIHdtqKaKZI/aa9lzTUub1wzwNQ4+tK1eEr1X2iBIlLe6V/0b6IutkQZ8EBQdFqQfh
dzrm0uvr4RvHsAr3qpVKxngBNq5WhhqBiJSTXyzS/GddogKOaDqWMPiWl7swjn60Kq+dL/CUMfu1
xeAGUtrOBDhP7SIBTaOXcTTNOKridPDgbH+NnZ8hMeyRcZpd6Xbii+J9byGUMpGjU6/rSiYfWj1O
fbitxMkN93aR829nNEcu2VeaHR+iIbezq16XLvKTg1O/Qbj0kOjOwJIGCbWmC9wGyseo7hUkM4v3
cn/aLn6mnCzqvZktF6ZxwKiU6jMLsz7pxRgH6jhY+REvMXnilf/Pk/xiIhheXnakBmlI5uwnt5nw
gbR4MqpK56zPtG+ojk1K8k1s1Lh2MVkyhtYxryErFdc0S7rgFCbH5aJIeqk733W4JJy0toqXZAF/
+7F7zXlZYUSpVhFRdQApGwyHqMuo6HnevyFVfLKVMzhzYMTx+jRK3CvDJEB7e0ZlAEYcauesqanL
rjcefmEoGr3fiUH9w55FA+HhiV8Ne2WJ/dr+wrBzA01mDMT7j8HK/OJixeWy6+X6SA2XxpHpqpjd
sycMXDL1/Y876qGX8yur8JkKrY6r1+33B8vgpMNfp3/t8ZhToVkaCTqd0FJ4ZkQphAsL0jojwkXv
Vwzl0vA9TXnW6xVgkmwVDShFQEEIaYO7+sEC+RQsX39Zmw8Y8px6QxeH8yRR2aCAkW3P7BeJk7/j
caBApvjSmHP0yiyua+pVNJ1QnzCIuC2MrjCNggzMu2kur7iKrs3khmodA7YhW/kVxpiaGhXhjpcf
ycn+UCfvuSgeAgLRAQMZMGTkyFu1IDj49bNU7lbHq//14Xcyo+yiuO5SWHeUqj5xXl9tsPZYbqFV
2pjcNkbZ829j0bwV2mC3xXicZr3qMGl8nEOaYJIs4CvVlNZx1yQvisFAko9AG2htGELv/aNT7WFp
v4DjhJKltEiQ9Vy8VIU/Klux9Kvk/7Csb4ovASXJV0MPI1K7EFYHm4VLFSDBUhZTRP5cyIdlH55L
HdQUeFoXZt5uehRs4WmHxfnWhXqH86fX+Vgce+2IZMnL+FM7dgost78kiXozedwoBTdTCMSA7+PN
zuUwjBAhnxRTfltEcNthcEEcSBUHc90nDwhCdkoifbqoB3lAMB55lDg/UtG45pS0JWMnQBD9dFlv
+j8bZulRUemWglcBsCPt3KWf5tWxMy+7a/hwpqYrOoFEoEG4mOZ580ep+sYDwojetZi/48KsCmE+
hpvMeRBNmYN0cLCr9GPSg/5XpE9TUSrwBY0F6KyO/NR3FsjyQ9A288JJ8hYuhm78XaFhgAvYNz8d
/Rv+Z8siipvE5hKsfmOiRBU679u14g0FXY23cOeTZBQXgNvobPTxyEcy6cqpkPqQSbQxD7564pEA
opBAsx1untrxlb41KSj9RQvBB9VIWwG/PvTrFsD+0G2aKINROeAEGeH/kD3AEBU4VDBApO/QkcCr
+fIbi/GoEl9fEY8oxqX3Q7tQ1hZjVn19DZgvbg26HOdfuiM29scqUsi56gqp129ri/ifJt4YirFM
10hV78ovc5+l0EHS1BdbkiIpDHlhk8fEKpeoAJtQ4qcJrma841h6HWPT8pZWlPB2CoZs4PCSdzla
zD1HyhX0SE1tdSQnADr4WtVlqY5sZVaic3j7L/lLVlHcEMts5VXvGD9/IHTnKmAb45r8izJkVX0q
wrhyLE+RlTEA4W7dHHQLpzjDastWGk/HMB3Ws044pWr0axdYgsZ5XgltZ3H4VxtSnxbMlKNiUcxU
0zx5obOVuN/NzXwNz3Cx30zlceDpFwAdT/EF0Xj4K/egGUFGfFmAHia/AVYd84yp+VM5KJsmy/qr
j1klGK8Ye5ZktOxgM65wPXx5c/8LnTR0ndQcAZGdrc4N2HfR8ZeRhDjeLKKoYQdEIAMi55+14Aj+
fdMJHpBlvM8QOqQagsoqaT/sndh53jG4s2T/P4YtQnIo1Lcg7t/vVTCqhBH66Itxc7i5F8qzxash
U9dHos8tm/RirSUeUZVzlnBtzyVlpqJ6G44HU6N3KpU8BWU/DzBfdLPXdTh0pM/wXRyv59YaUkPN
vNNAW5rc9wuIBx7ipDMXE0knM45LcInXzkp4TODeRRH0/nRwW5F/3pPpXSaU2A1R3gbXMcQRQxhj
U9D+6hdPr5lwn3QrpvBvDCZ8jEs2muQkVClqBGfUMrqT5lo06xjhlPl2VnXcCIEF7KLMISpueviY
ySo9TZF2+8fJ2vkh05zjuL1Dbspt0iOfMKamRLMy9ruuBmhTSyT4pX4/+yy8mjkuNJJ5oYv/8fz2
xAxEikH3GWZeWQ+QgeUsyFWR23aOtVZ4fI0mJArI+JrTqgOKCpHhWIj2Y62rlATLYnDxUDhgYkYI
kMFL5BonpoNyRclMSHlnCg8m1OoXwM84eEyL3+DmzEL52Vc9ltqP8rftNMXjO1idY4UCMqrlKnVa
KYsxKbiOGgGqtYrQga4RWljTv0pMirZNyi7sPEyMOMvVQbq8eqRHwnqeDs+ZZ4qufd9vKXyc76XR
vycivyocsiHuzjOKZi1JXHxULqADR2UlIQoPNjKAtfjsR7iwtyPzRdBaEIo3G+Ebpv+CrUa5JP+8
KLQeuxl1QbTXvqk2KyWdTAkZpHdFm5sz4h+wGHoTShC01M8UEfNhAjMs/gXEtVb9vOi82fwlS8zi
CdeiY0h/vZzMuHBjZ8yoKiVsfwLj9sRALyRAqIvySmm6mqbui5zWT9mujAjkMz9rJ3LRzgGcj8WI
lrHvcG2JLDUVBoGjVRs/z/CVQHH96H9+l2TkzR+qYJYyPZXFLjSk4KjwUQtDLpMrYCRUafsuq+RG
0hPU5bCUM+T/ennqjOEQk+le4Pt7HLIXtgMwuSVryUbrEqTkzaRtb80WMPJ3Q494qFnsLSFHhR3E
jrHTtjZkDX3t+p1fnz/8xXjrzQeXcX8TqrcHOvi+peaZU1irPmw4GT8V701FzmBt/RWmqivnxv56
vvaNGPiAn1W+7lICg/LdeyJAoqygoumedF2QA2PHlI3pp5DJKLZpJNtTwz0NGj8IW/iurgoV1b/W
Oym2yjw8qhbmALwnSTPLBHeiWv9c67d6nJoA9wQaTI/ya5XHvPfkfT+Z1Y3sGekYxpCwsc75XETB
wXMiAso1NB4yRTv3U6ZzK1UIGUAM3nlkahdnh0/zjxysmzM6a87u3x7Usih+CXL++2VrcRVTn5Bw
zchtL1XK+TjDrJ7AmQGbVZudtrAuS23ZpYoOaKecJ9ia46jCTUgwjjuWMjll3RUNUzD37IMjX7A6
WYnNcapv5e2SMF/f2H/oZANHnrXiYat8EdIF1ULorQdvk3jqbqmIN02eXHy4iFlfkAw7u7B8Rnhs
XrejWiDMkkG9AOGdQypuN0Bg2uleSiIzS3g2rs2XSyLWjSWjDQMFkYgmF/4oXy8Cy4cNKH4Y3ZbR
8Imik9LxOW4Ya3uCZineBCnqNYRvXcFkueYPK9CY4AENHqiLYBjMyEwAxl0aExzZjv5WSsYzS26X
vgk07g0++q4X2Dy/MlhdAhVbI9kdKnEaJHcGgeTcqTZccX2gWvln62OEnf7kveW8/EeRAyeUAN8V
ci7drPs0HSIQ2HVafEPZPXo3gnDlgctbdQtS5aVCe7VIlh4dGNwyoqdYdZs/Yk1NWM5T48DYJvpW
bco7md4y6Y9vS/RlBiGJ9qoQGCWo4VzbX+mSZSyAOJDsRM2Py4ZYCnXmNkZmuubEGafvdL2l1TAJ
BQ6IY5rqF7fmjfW4V9EWDceRwhjpPgGOAew0ZoxS6YjWRbE0ZHbm9tfv9ecJ1oAaQeM+7ynYMHV+
pX/11X5AO03K1d0+uKLTWMo78+dMw7I8snWE4Uc31LQ2vVtiAuliygzdfKORiteQyljSu7NMM+Rj
8IVj/HlXrAOC0jj/3rFnmc2lKEjRw9Olcx530G+ltgDKO2gFFgQhwmSnIdHRtcgfie8zcIBcsR4/
doea4v8MhwuqX9+R7VHFf8gLuNjMv0Np8FNw+lxQx15RmCN26YsP0QEDXvvJDXx0JFImHEekhV5o
JHTxDXFC8IkWRYUs3NGAIiHDjtFGPFlP846qawYy56qx7b/lK8zYh8HqE1744FYyi7NHQiz3pyDq
tG9zoT6rFqUnD5903HHoSH7E7D5HwfhxAoMcM6jn4MARwHCNWhdis+hw/bJU/vuWn4H9fbUj6H4f
gpZcrJ41uQPeFFcpZHc1GNnx//1gXgmqMO4yVPruRsEmloDhAeKzK16uxcc9Hg4YM+mDGbEsaalv
ZRBEXYJeojpHbhy255AojnN5RnwPMsz1klrMvMke2NitxbDBxJKbTvFaZR/ZwUQVjztaxzpBxs8F
Lt+bRneINQB1TEF93rRaTHaGNHmCgKMOpGvVSl+VEULmjpzlBtHb6fPnT4bKesZunZetUUN01ce1
s8GlPo8ut+TeFmok9D3jxQkarcVLTkJ/anZEhtofa7k2RBjUcy8M5u9346T5U3wDvla4xU7obiKr
Rb9Bl/7qGk8IQZwcWTN1xajvdG9Y52AloI77NdnKfd2+fRKGyUn2s46iI+P3fZMcHh7ETeIyt+uQ
xdCBP7rW3tc+Xs2aUUdo1+1yIs9pAywZrlUsMdRomWkEbPrU+3zMBvbVTWaJ7mAEep2xBx56R0yR
OOJ5vIiN0juAhRsQHci1dkO82xkTR0CAnQpq3HwhjAv7jV8O6Uos6jusKlolIupx1gGU+c412LYF
xkNAn7z/v0GQvzihg4gPj0nEnHtMRqltcMFLy0ZdMsww0NrLgqlk6U/dSgf1GEmzxg4sX+trl6SN
4nabJfM6JybKlT37VtOXUwk1wW8qFt5qxFr57q1vqgWGm4NHOZrEAz7BQeVcDdZPEpY4W+qTB/l6
Ttg6Z6b9nkyLE+7zwifM3mne3u/1Sh+IVdWQVXSOhIUWpQ2YSwj2+0dN79/YA/beoJ7tyswvh6/Q
+ZXAMTmLzQsS7m3KIvC7x/BCaKTT7JWbBz1VoenCryb2mwPiib+vTpwoe9+Q/zpSPyjgNepIccgC
9/Lw+A8GZlut2+F0x7mbjcgpRJG1m37B69jRs5oL63uZGDqOaZ/kW1HKXHSS2bIO3cuAo1WmQWXE
g5J+raAS1etA1vA3IH24K71RsTrxvBqWK2zZYhZXCDd1CxmpsRdBuiYtgbHRBLRp65v8lo8kR799
TTy7wZwcHKNFA4mZ/4hCgUVeZq3TVEDxCGZ6tQKvl/+CML68OvnAuInkb00BipQAMjWQG3TXGVW5
2yXtynKIa8AQdOUrIpd3N1CufO9ALoMbmtPrUfFohcKeWtWTdmuBPVpVHHSbCjwP3DbqFdNnFoMd
upMP+Y6n99PM/rXcGixjHsCP9qSHfr/BbwIZcd80i5BT4qTXDEiW1ZOWvNbxB4/jfRh8l1qCllvu
a0qSxZMODnt+4zjusM++wXXxaOtB+HIn7JozFW3vYFIX5N+GX0x48u8xSsVrFLOAz3Fv0Ki/ks0r
/wLjs/WyY0fYiamxeRMvkklLm0N5a5QcIcrYoZmF/0yJm7iAqW4KToQp5pHGhcLkRUHNohAC5tY7
BztbJzHxC64CSAIEva73oJVwIgF/8nKmvXC/LEz1+ioVABhl4vuAQBopwhnaPhkbMznLBwmO3hhV
QhHQtfA5OxtnPWJ1n5WmunBF1VN2pJ425uPuP0Xej1mwPa+U+EgfDkXGYRteiU5PZ1J/6iZSunfr
epAjmBan/DW/1Rj7WJpnylcwHyNtrLw3DYUclgP/GKlxQ3Xmddvd0404lc6+Q+/EFCzCds/RJyuT
6CONVVUgXP3tF/wS8OxpvHFq4Y5eXFAgPg3CY1+nH4ix9EaVmYQPRSSPzhPPEPtctrSISnSKcf4b
2dXWMmPqgWdcPjkohiu8IK0YsNuUz8QuinrozlpH7hAHp9Hv3Qpjx92JekNbOp0q6BMsqj3xLDTP
HW24hpt5QFlacvs3xNvTjJGyW1TPDavQJDX66YbhtWWZIK+dzi2fRm2OGvzSxSZsM3af+mxYtX4x
eXcu1ONs7wlKdb8TC8WZbXZeoeky3IuBoBsPcv8q8P0+876EOwRMwBN5bXm9R2Ttlxqh5OkZfbD7
5kXKE1bGuM3YbIm54zijIACZ19Mju3Bj8kJtq9E5RvGCLu7+8OZyuJq6tMUe0LEXpenfW8iCO7yI
K1+m8SVhIEm7zpkQtFUW+VskwpA/5bz6jKNUBN/koHrUp8H7fR7HrUkWpeoJs4Ap5hN0rXscjcyF
Qlufi+x97okTmXXhkV6vME3XYJ0XceqB3K3X2i3O37PF8QSuLzuUoNkNOow/mpBztBGKOt3PzGb+
484S/6qUR+5jPXKjuyPsY4Le2DFcUlKnYW5LL5D0PWtbjrh2eudTHCLJsWdEpz//F/btHhthYAgu
gEmRIWzGkrmiiiKsP5zOfpjawJMXz+leg/y3mCQoEtvP4LvN/Emez+GkVBy19P6zRb8G2I/SIBy8
PikGiAA7BXPsEYpAmekjfByq6IrENMfCMwRZsDVOv+60XTIjIa+8seaWziEZrxX5tVR6q1sSlsZw
3gqmEs1rlNtxJsTAHrL40vEyigO+gsCiE8ExF66tt0gnj97wi6UV9f++0xCAM3/paW++cPmJuE+6
ItrLnSLN7V1sKFtxpKwyn14CrnzwoIrtxl82y7yigkN9WE3s98KjMKD0L0roBdiTIBx0QqURroNI
/VVEyD+V3/rzJYEvbE4Icv3HJ02aDQJso1fPwdQMkpG6FUEtKpCcXYh9eEtaNspp387qIeWQ7eAx
UdLYrwEZ5/dMlrMy/p3+j1lVKUqzde64276NPiWTMv9dAOwygw2H95L1VbMJfCdEuy3QHX6JhmBJ
VHNGj6hWgT5xwUyJkX1vEK9dqtaUr6rUXWfEZ/vcJSWtUYBoWBQVWge9sRL7AjWmdx3+ySqrTLRg
TEbrSyYBdzdgmotHtv2dv8z6C1h/sLqvXrsGD+X2A8pfQXcJO5J6ve3WHuKwt82LG4OhOKefl7+b
lB5QLe+krsvVHwJJFwLuEN5cUpbKIMS0eYvnl7DQqL0BAWhkMLwAvidsq8F99gZjyzgZuuqMRjpz
2yWQ5/SzJCAwzKkj+48nspObVRenC3dsAL4PspsM1fiXhs0QxFgQ3YGs2XisGnOMEQ29rjIV5wrm
woUsIks2KtqBTzsJIx0OXNeM3mBbRxIAHSEFnllXUFN7mcLFk0vXLB5y1ppAywSuZtJ0SMW81dLN
rz3NtRK+r/RpL88IeLvqYt65+q6BFL74duGjS4H4TOQqXB8wvdId4ReQbUzNq46qP+o+h85YG642
nOnW4oGYiQ4iTSTwpmWVplI7oFhy/bvyHMqij537pxCM+KAovzMPBR+5/CdSG38GJZIg7qkudBz+
PTxXqXpV0S/60KNTLDA2oPztxkUKm66Xvnbf0o81DmVB8FFNLsjm4cmxG2+rE3uQDb49vHqiAz4A
NUI1lxLNveDbHIIz5bfN1FELUrgkbNzM0nvDeB/K3UoE5Hupn/m+O21t29PUPtSdBsEXWNNTMBgq
7f8pqVh3ihLttPLAGgCwemv6fBPp7YUcyt50d13hTFubF91BQdLmNwa2fUy1kpokoKyo7VjEOlbx
Qq+DhRUUirpBmzKih7VqAHmQrlhiteehBXWpcYmaTpa6yJTpQgu8kAA+iqgPovv08DgqY02bLrWX
ICQHnITLfMAFsr6MvJt620MEncGHIDqVQ6c6ddudcYmt1UNteqKKj0WyOjZNV5yn/hqK3iItNZqC
bVifMKPf9Nd566OUoGFBOKv9e5fn/KozTMsSln5dX7t53xjxHzh08lKccOCkvYYqJASSA18C7owl
5plKYZCstgEqrbONbchm4Cufk7CjRK5T6IKU7/XGfeYhAwjr4Sdw+IJ2DXXNyocOJYQ5XXdRAOLY
hQn99CQKoS2jtIis8XuGxox3gzZ0VMRKHQuZuYJzbI46QmelQ0bngDT9lU2v4Kthb6+P3QpRi7/4
+QzGnprXO79d4wd67jjWSvwZ78tSDtwav+tt2jhJIyYwFsc2Uy+dT4EXuqNgR9VK9UOQjsZOBaA5
BHS/69LPbYqcsmRMBNve6vLae7kncnKwssw6O0xDcyKNoJDexNoQYYSYhV5OowTCeDVxF3dYUf5K
S07DAXcj4Dbn5O62N/jcTRxCwuFxrWEki8yas7t5KvhMqYUwiuxvB3GH2q/ecoidNFx7Lded506O
NPsc9mlS2/4muk4xcS8yhH6kNvXbhmEK+6PGSZyh1hDiLD44ngNCi55FZYxacC2aBq5TDvZuBmRj
Vlv1PYcKLH4cPW8D2PwilFLV2coNdNKilWHuJTkR2Fyi3MSk5pStlFLMhokajGznQRg+xkdC++WA
ywDNY/95KZfNqWfOPfMho+YlG2MnyEPWahvuqLTS12sSIvbO2X8FVTY93X7jQMQjYSnVtY9cmXxC
sVcsORFugzhascwcACKguegzPUdYkoUE63PSxbZCWJr0RU2QGrqHuEHzXsxhBiBkdETMp+ZkrbNg
e9misQLvYmOP84GLytiZqMkNzYthl3O5qZ0oBhyp6JpCdmoGmgyQzWRGKycxjpGxKRjcYoUVKn3u
yqcS7/x6u20zYRjK/06C4J7V552IX4E64bOey7pFHVDAOeJ6WK9cc/1zVXcaRZBpSipVKhPdfLj8
584PWL0BVBQpY53cJnCBCXlnvCiQ+h08jVYq+WY2Bv302g5ZRxIJVOdSi7hKdLNbVSqLz2gbg9nl
NrAF/8vQMVtBEUmPwcqR2a/LKAO9g0829Pp1BFPbi014vdyYXmV+0YK32u8LP+DQcdZMp8RoSqVJ
Rw9kUJvWwELhtrasrbc5SmhNIFERLpnjnYrdyDeyDLyA7YivLMRSA52U5CXN5MA9GRQ3BF3TZe2i
LT0yQs0DZ/oCeY+rX+74D997ShD6MW7iHiJpq2gUgCs3vIpaXK5vM2OYotqFFWeQgcGDa+TB7w0p
dnDd0Dk9o+0duI6R2gqEiVOSG9A2J5DkeZBSLUeYyHjCy0jH43Plx/djC/gh3JgN+U9ugziZRBeZ
2kl4TUigssToM8mCDxrqcub3s66TX5hV9uhhhn5D2+7jLi5QRg4E0ZT/xuc2aDHk4NkMtTRWWJ0T
+dAK9TsYG8dxxbhBkIClO+MT/q+BaJs/jRZkBmsnr3dsifU/ShuF/dTT2uc9ZwPwCZy9HqRCIzxN
CyOiRg4utrTBLThU3B58cMHZZl+HpM0gSEexb35GwNnXtB8kiS15bppwXGZXPiagU5QSvayln7AX
2X68imIVmJHnY/wTM/Pz1KihPsGFt2PPsoZkYY7GCWOi4oVBUBy33bqTu4F56V8OWCWAwKI0oOK8
tAIcbb4xujSfB2+1E7mUr1b5imuxKMe01GNDz7BlMcI2FrUbz8FN5qTBNpwu2zLUiYqrL6dhKCsu
3OBC5PlZOE8KjJvu/0jcZHP1Ksz9RipGUGGYJKUP8g4qdNqHfneBWq6xLKPHiI9x72xUxS07Tb5k
T0VhpjPM92j8cqucEWIgD8btlpwjwF3nIIiH6tFwfnCCaGomhVnMLGONqNzcIPfnw0Ws75Sn5Quz
eAFluWmlHx0KTplRVKHDFbdf0xHYI6HTabbqhXjK+XuXYLchpLS6KRddx2ypqSp8sFUBqSpq+tL2
ELzsVbeAErW28Q8tWimesN6bkJd/703umOM4n104d3fz+HkP4UhMDqBJB+vUEFkyR8W1/99qvPVr
p/Fa4lNseXjrmAlczan7Xg5yVk/cW0UjyIdqBrop0Ra9OUHnB1ZXOTofjI3Hy7c/OB8VGAu3dyn6
J69veZRuMuXbedtFKke3Fjc7EQbZwAHMVtLYWj2JQ9jq3pUSV83P1eMC7Sttj+1xJJ09G8PNujyG
C44g1qwMxWdUwHPGT2vRHf6+0aKtALVy8Pw7E2mlLEQFhyBOxRglCOE9LaRJwzXXvAKjHnLsLJAr
TWnfckJU7J2TbXNxAkd/BU/ImFu8gmRdFIJLYh1xGpVZW3WwDi0bTvkJkxseIhl7o1R3ikwsIO9T
vGyd9IM7ZK3UwWws9GLWviomKQzPvPMXbPZYm7BSoxj/0DJUxRJ+9qCb0t8lM5/XK5KLgL4Z0ZNJ
a9AVrrsQ4XoHwzbTEy6x4czOSZl2FXw8xTw8D24R9qtb3mIkzRIfTwMJWuejIhsa5QxxxD5vQHT3
XxTIWJotISFvqgrDuWFoXRWLXMHo92JSGvcJRyHew2GwaT3TWYNF4XfWna5HpKjt+Qs5ooATcipT
DoodF3vz/ew7lIR5qti2CpmY5Yj4sfNkKJvern87HbFpO4+xHVQmSZhuVmgI1xpfw8DK9ILc2uyG
T67PX4EB0pQfJ0axXU8AOQKBhJTM1+EHGolc+nF71ABm2NmySldTjoNirRFbEfbarZoXCPBK9DHi
aBx97eNCR4m9I39esqrn5tsAVC81ngtCkOgXUla432/Pnl05xkwKiwMPotzkjPmXVwjW2yif7Ww6
fEzF/S1L3ovAA3Q8Df3cY5hPJWSpwS4wIWRKSqSzR1iYrJTlGP7GuE08Pw512s3t4qS7lqzr1x2E
OlPlv8CFSz8HvKdpdy1mpvjF0GteBFXn5e40k+T3VTaOTn7s7LmN4/ZNQAsJmsgBHeGxzRPcjqbx
vYV8c9RsjTejv40fR35SCnquQErSoDn0QOuwcyPCst5NXP69q34pNbF2gdkIgKGYyzQ35Q5yJe2u
W1VLui8Z8ya6gNi420Zijswy0l4g/br9LNA8mx7f/2xTl7r9Pr5TqHdtyPqZyiI4lpEBL3aW+xSY
5BTcrGsFIzoTJmwfzzmX1P23AQ7VdciquXMhPgUJUQ/7dars0WJu+EmB7oM/AjRfi5OsMHxyNdv8
elClilKuPjO9TxyefAkCMBdkEyT9DXw44nJq8HpuWCRfz7SzXgh4EEvlF9TOqxOTgfXHmUoq1RZV
TzOVopuMu4qNXTP8mypjlXT78gsMsS8GIJmrCrmlqyCBip97BjT4F45kTZIdJU4SAuinEDNjy31H
1f3aCm6J7EzAfhDXgA7E/wbyHc4fy1uVPlFxtBvrbDx9lIsPTuPa+tH88P2bWfY3EYusJMKbO6vY
8FGn3Zt00IBfdVh2yXdjhB7BStWarZfXcqQMCQvGWc9rxRlILu74+U08OEPnA3gU+PA/34X8ky7J
XSHfdVQzDqKwabfGmY6Tak6uCgPPaY6pFqvvPpQY90qpey3STJhHS4Wl9V7Lz9ftwQ7XVZQG9YhV
rHeUX4EWGNQiVywh3cKWGcCSIZ5tvYBtfn2timEWU7uDdh2uAwhQtf9SE6eOHR/gRAOBBOv/5Cqd
I+6uca75C/9E6f6KFa0LcuItwAyXYjaxjHvzuh1pAGN1PS2hkHqByoM0/w9NrW1Ft7iQnttjoIPc
ix2GLHQrAD0fAYoGhRwTg9PtdoIKUCZCEKPKFgz21FEVHJyPfJZRWw+0Ci7dvWGcPSM/mAQftYUz
d6BQEPu41O/1vzSlZWuTTx8RjEFVolluqoDwIXnRYt1mgdB2QK46GWs9UcBcjiwNqP6vlyCI7RBP
nDVhMSKdyxTVFVFdhoBW3U02t5roVwsfdzhkLQo4kNZTJsQyW05oQ4VhcX41nYrmbUezgaSrGelA
RFaiVsOjLNU6Jf+rj4BUO69kQJGx3ZiLdXaaK4lers1OBa00hl69i5HTa5UxUXvM3b/+8hSCCaPg
anfK7DdqZSWnYJqX1JcgDPTG8JSG6v3Bty05p+mmm1uNetedAo59NmFE/j+ow6rC3NWeBHsBNwt9
1iBdnqGH2dz3jLF+A4P/Nw9nZkqK49W63GTH3m32JV7YzLfgrR7fL77tnaB3XziRpbkqdsCywko3
iURnoIuCeqfdfAhkiw8TiTmewKzABd2IDVM9C/tYBiRZ6fG7VP/O4HZAO7WhkfxbRnBxlNgXWEem
qds6OSqxzXvC0nSLtDgzmgLaDiGTAUPMgKmuLDRHlcqTiAC5291fnJ8Xv26umXcoVyi26YUiDwC4
E2X/RIqEAuHhzyBWHxoVD2DL7XtTt9RMwOyJCJFMK3I7hYVRaatWnNFi8vH4rfCk/CdpDZkxRQjL
RIOJ81jb5yHV+atQkZopILpFOePdUzxk0WVfJ267BGZyVkiWgJ+yqdOCVnMJLmOQ7nBVMlOfEMlb
UyqzXwmsbW+Vpn8+S1q7qoe10eex3vgyiXPFFWbuO3Lx3K2yhCaoZ9Lrxu4OnJWj22qwF3oHgqxJ
nZT66D2BCFN2HOFPFrEgeKNMJXD8ZimQ0BmwbBh+LAl6do+u6+tFxnPvTxcZ62N42hBcdBRqcKU/
trLswQwlZ2NxYOt/kdBSw6hCk2DX//UCle4U1cO9WHNQV7cZVOTEkw/mFMd2vQN6TWkdtJbNbtTo
7BmHdFpqQfSOblDVPLky5FRzaQN3anzS6fUX1DDge44sPLr2hlNJjgnlERslS0ADiXU4NHx6yeP0
/209+G/Clr2FlCLB5qzYusW5xnQjaXIbfaCR2bxzg4LBjq1yQvpEqfJ81Q7CFJ2bozrkujzY9/zp
jTuoBtshpxGx2jCo4vyUf71ZPjO+SMieghX6wYSLQ69czSLSMzR9ruUAH/WRbmteACvswgqRzV6i
xh/ugBazecnfx2FHR/Ykm712mnYLPcU8T/qj1xu7z9Z+RtfKKWbCyCxbXPfqRfD0zTz5sE9fO9Yw
C5cL1/G3YKSYpkLmoaofHpIgrvk5A9ej1fG/z2otKe538mfho9ahVzSf5MWdrVcyLuHShHAqPHFm
DlGx22CsgJ66oGf7CjNMEqsykf/2hYXa/0SLXxWlg4T4QiHnuytZzkSuWmp+KYO+fq+UusrXIOVH
7v6SadFSzY4aJZmBdtRHFDNdOBx0GVbK93wHMUdOt4B8rveE2FUOwmXQOnDKjb6I6iWffEDzY83o
7gQ72s7A172T8loObypju2a7U2813A6c7fN9P7eGqbFb32/d59tI8drmcwlWJX5bTKjJJAuT1gce
Bz2NsxbQcvDzJXWAB8NdJVqIcd4xrvLuw6ACBw/6cWfQs8Am7fHKtvXBPONRyGDC+gn9UZwKu8a7
8ov9HVWyCFdR6+/Pd4BCuGfT+mRSuvCillHpKQT0n57PiV3LqQJpOts2syeSLJP0cxWFEvLtObKu
bV6ki8ntGeHu/rxSF52Q+tSrmQtLscY1RtFf0pjZlpW5KjSZJhF7sfpywGp+BumKIYqyRjFn210s
WFmj+RmM9YVXOHDVfKoft1fHzO7FS+g/hYQKg8vFCK7ZhJKY2YGrI0g4OF2ZBqO1wLzxda0A6GmJ
JbB5dYeMJK9GSWfyhCiZYtp2L7boUbQNBV7fyxqct6OejHhjOlih3C4rz9Z33j9imz9ekghDT2oF
XEwjWCKoS19BepXTSjgfg7fWdBUcFBeD4GUxC8TyQLCKGRbhw+yLKZjBny9Y2gc1c7Jf23Mi64QU
+Nf57udvSwbyi5geJYIkTixzA058skrQs2x94uNigAnb0fCRXGv/4JWUWAXLm9XjD7lnBxG4pHrQ
4lmiu4MNIRgJuxADJ8PDUz/fqwwF43oUnfTu3fUzjQNh1zt8B2/AXQwUQKHJ8xEgXevayiiXhYF1
001vuXeDkxQKaKes8IkzUlUQglS1RBnhzA7E7mQmCwLLHSUv+S9jcaKFXkIUQ1tBO2GxbXSFG1wM
Co2On7E9ExBY7ZqbJ+Skyws3+LuNvrgCU3Aa8bAZEmnFxHMBdki++ASYo0baH8zjJbGeFsIGivYO
1CJSUYpnFY7vAGyiYqAuK7svHEAOL0B5RmWRdZgeSDg+oiBYIm4c1UUexH32xQzjrCpLz9Nc58hZ
tebz4ui0Ijep7io/7ahkBhO7lyqjT5Hys4YmlD8gpZAs2OZ/ylF/tir9ww8Ulgl1QGTwfhGStCF/
sTZqFE8gfZoohJW4SWTvxoqOAMLei4nrsUIO3rUV6ByV4QrDNj0mH5sPaRrvMuoCyDmiFjjzzceQ
4/dnqXbOXWQu7iHQ8mUIhrRO1wVND8lwLGs8FO9cegSCKXEZVchFVOwkyR5MQdut7Ue4SbRQkrJH
Q7dmnecLlIyhHv2dpiz91+BZ0JhgoYrHMeI5XYw5rHFDZyIu6cb75MoR54A4geozLWolG+nCmttG
xuQufvhkiwv6PHWA1fc9Z3Oa+PV+rlcX5yjbtVDiOAOZZg0lkxaI4dZD4XUqrmv2GZj6T7EUHL5J
Elk0Aykp8hqx+x9EQQr9PIjsQq0Mgjq568KV/i0/jmAEbuJekKTQWBMjMGdtfRzzULwktWpZae/o
BRM3lOXYiFuIL2CZfRI8macQMt7kve0trtlrAtj0O/SZqq7KlGCCgbYprsigrxKMI9a55Fm6ywh+
sBr8XLuU/oDYboiwwRyZv+wE+dWOJCGDuHwH88hM7s3DeL5Ml1TGRVb8IBUWm6A=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
