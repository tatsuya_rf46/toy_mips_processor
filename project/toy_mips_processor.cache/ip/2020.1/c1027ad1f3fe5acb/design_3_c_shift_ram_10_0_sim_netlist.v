// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:43 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_10_0_sim_netlist.v
// Design      : design_3_c_shift_ram_10_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nzu2b6+2pwFmlGio+ZB7ip57VZx++Axe5c/M4tXUaHoMXfu0Ohu/62s2rwnnPIytcpVXOBslp8m+
qmnxZqj2/kr4JFOLdjsU3kmGw4y09Iw3NWvVZQvNURXhEBeu8b18tO3yhg4GxK2+7OE89vkbg8Qn
hcIWzjnRy1AJtD3WyNO+KjXhXaXQgrGE55QmPnHRK0t8PgoIYfRm1YpSRTjIKWn9EIkUZQD7/F1p
13uwGN+/APTI48AiIskWPu9DnTL7EbxmlOJMHUXb6CI9vDTGrieWHvu6j/orETsjkqvS3AHf9Ou3
FOsXBbDUikL5aKt+bIOg7KwwIscAfP7rRV0IJA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FPsMJuR7NYf6t/1KkSRnSMiyzx4Vy1ffb3vIh1+BfkPUR2EZc4r6K/q4wEkI9VeaDw4BGVDIFn5s
uzN/aGbo8LjOE2DYSfH9uN97dsuMA0CCt0SHZl3y4Pu/GCjWMF1u35wNySBfaOzOa5n6LuI2Bp2r
8IozQes9+g8hWhoD/xSKbcX1xUKbrVgIRDewAnWNC/RH6HUABKxv9nxO4oOA1ydNGRyrQElgn6Ci
krySMYkpqM9ettb1bQcIY3669AGl58tSy69xLpH+TVwRftsAY9cug/8/HmdMWuzpn2hTWoWGeoTz
q7PJOUrSf9bBprkQtRBsaNJLbx3pPmw/FAk3ng==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 5008)
`pragma protect data_block
zW3c/kppPUijT3b4qBGxb05g5zLGygf+N0T7dzdXviwzISKrWyrEV6NwaCfIbIW5VVc2kMKYRdgh
pBaij8mrxwQ1EHTj3O32rJW3AMVK2Qfzk1IpSp9sfhh6D6Z72W4OGZDqXEzxo8TIzwLve+NveNPj
k7ievNgNc0bdg1YgpE6jRbULnmhngaQ4TGxvIbOxv8o1pItdeUReUtPn7Gp+Zpvkr2y9SqT20Wek
OdBzewHWwdjvE4SW2Kia5AKQhGQkqN65XO+jGzV3AWzjdKATh9CyHfD6WIRXWxn5g+uoRiC13aoP
bopMzEb0ohxQBztv1131c5plLcKMdmB7my8LDvHcdHm0LSwDg8c1i1OLrZD6wrl49cX5WXmhr6cD
kFOd/F3qoIOTss2xPBPnZJtygfQe67o0kfpid35o4Sf9Qjwm7wbK9cq74+bK2g0V5bI31NQqUC4a
iGhYOp7weztPru4Mne+c2HumihPRMA0UJDj0l2eXkzolF9UGqd+B3QHg0vnEwtlQ3HkrYsBQ0xV/
RnjeQ4gWPZdJoIzdzNWSETLJGTF5NTfeyws8JZ3vWnYgPW8G6rg+pCruHwEgUoJlX6kWLmDpFi63
GKyUa1RmQg+1lkgMRJPGbm46dlUcYC4EmuWIuolsJJBETZXylqTRuFuWoxHwG7MiGtuBKHdCInnA
IUVjGFqyZ8ahPKZzy5pfkntOtWhIsf6pCI1YG4FhftEiXa9ax0M59KopOXRZxaOwBH98CL2pQomT
V8prwkHTQOPfUVquWUP7gZuLN3J5dY0PZgvXCgFxxuvO4ThyJEbDFIwo9FvRC2KECkyNQ1/zfQs9
1dqYGzp6QCHqTqndvSoYCKFgk7DFDdnHV7ANOrj6QdptTmErvDWe4n9kaen1RX1k8YFDWuUocaYo
Y0VBqRdmSl+GSeispI9vNyzIbtIax+dcrW3PoxusLUkHf6oHht1RP+OkNN3dp+ZafLr5hjpE/1Xo
sGcLN3lQRjZ6emYqAdjDQsXXvhMSJOC1Yb6WJUWOOJkvCb2QVkpuFA22WWcmRfjp4lJ3cIXeo9Tj
klUjH46oCSQUGC5JdxE7KV561+JW8Y1mw/gELLh4Nd76M8H3Dq2shwtRVU2x4bhbfYPsV4m6j9fW
WOfCmjT+BZGuxmGb5wxxknVQDcYRHmNC0GfpQF9jlNjYrjvV7vmlNP5uFwUdOmqWzlCRnqIK8frI
yQmJobFySsFEFyGKTKZfSxmJPXS/nkctYpJ7p4A4IcY/+31xKHG9lScl5UhTawD0vCqxpDScmBRu
puIHUgLtNJN8cbgj2Kdh30Ccy15so66fyuR+Ptrfbq/o5i/MR9axPzF/Nx7TamXitc2BKHTPIyUo
Sck/GrkFmqtE9em4gISz1TdpVTCksI5c3L21vU0I0Qv8GiDsEHKFs4vN2Mi2QIYLQgoOdp28ITGC
zyLNKBJ2zxdgcy1CtBRzYRZYRDto8O2pIz7E3VessJbxs6AChcXowmnHk15CNkP/6dE786qBOydu
LyphnYbUD57xm7vZJMaqUuIL5R8r2wYH1qRWL8H0ACBx0aSXZ6oDm81JxJTgwtGZwtJIzFZTbieD
OZIAuM45EsapgkS4GpXSGs+KrWFc8xmLpaYFVxrTljBgbMWrLERPvUT2QdW809fgVnJFOlQAA4QH
5bFcCkbW8EztX56DgjnQqKMZT83xjlKBkIxi0sjnFrf8SvgXfA6xh0E63vPdtfjTG1NIwWGxwU0P
5LIQ8o7nObaI25PvFhn3EjBvdVbhvYjueg62aOb15c+99YoXThUqSderA76LoIxD5wvJQ+lKKpL+
2iRxEFD1Pg3JnADIsvFbhq89xKqpLJeqWWKK4VCbNL680sWqhLTpktBSHjcnX9ETiyc8lPksTZkR
tdJT45pl+I4HcqGTv73jXpWKP2MYE19S84HSiluFL7KFZuPzgxn4eZ9EWqzfT9HmnnCYkcEngldU
fAcRMtHeXndxEpiyZFpNI1DmACk/wBbXfKvZ3lbX96RZk600XckIVHa2kybm3HQFYn9b9VP/B71f
6La3nPvEkZfKirZfoLJi54LzZydxqr6tRPy6o8aulnTYpsDQKtPxIMKEwBEayCHx0HkLfF01bV+T
fAOBdfMHAlYtJlUMpwbgmXaOiq17h3E3VYcQhXEry2TGznA8tC6vR7jGe8lxAsf0KcyZsyQPhcgZ
y9AgBzpHidg1OJTOF2FWpX6XjA542++n6OVS8T22lIrFhJdtr7JajktU3jt/a96X7KrcJDVEEYFs
hiNvVEMTCIn9Dd8WHk4z8E+jpcDeCJKVe8sLXSHqAelozJYzGVKzlwbAK1A5IN0BFneAiraMkxWT
LwNXmtJt3MPJ6t66X6t9H9RZR1Fil0NlooDpY1FKtgZmw7QXmkZkYQ3UidBxvWzEqn9GpdfSzS2u
JSfZcOpXbk52apK+RPVutn7f17JDfuiNTN33P83rpASm8hOJZI4pnEPMGGJOnjopHEiVtABjHnXD
n0mIyi0XrNBX4oVaGyUv33sAtTavf8FOQ+5cf50u3s5Dh/Y89JxnN1Er3ILbcEVzMv8MbY5bA4ZN
MwBEFHMV/o1E7VISRHREsO4DhL9YR6TWc4MtOpv63WiMjxw/IGT+l8ssY0C9aBQFDJbsIlHawYa7
dUAmnt/MF3Jsnjr4ctvKKVbedXAxyKVhqHMMX/k7qzD5KjlDcRDTdGkOO2WC7++hhGjm3teTyXP9
WIYLPqAUCDmIbCjQY9OWK9rgEAUXoaVXYMuiUTqQ1ZL+z30oLsDLX7ihiGC9A4uyYyBgOmxH+87u
s+ASrCr524s2CBzxtmLp1fquDLk0aXcgg0/S1XRoojcLqjc++0FemDW8M2BwA8idOOtd6rPIzMvG
XLuZe5tyOOnGY2Rk5txbZGvmEf0Ha3TqJuBjY6htw0cdi2iUL0MBhS3NLEVbj0hNkKTkNcXzMK9/
XkkqMluCoK5XsJwpp/0NEpmDYG64vHqmoX0DUWkh2Yo31SNga/vVthsl1trICgrTDEeSHXFYIFnc
3+kzoLbA9WlCzO0+or03FFxDp7NCMxzYfGmfpUEyZS4xTtsAzh6N/37ATP8wqSWim7kAy+meuiRs
RKd1fHeG5VzlEgrC/LFX3h1I6B1ofgGe5OGfi+TUTQJX7d7sdDWSb+MftoazrqTskRxAnWfu+c/f
Mp2VBGj3yiwjRKiHYYszr53Xx0DbGCtQUppOQOt8ORflxRA/MXo0CK6R4UIzPrGwKIBKzArop/Bc
QbRbK+oP9G4pNo4O1CA+lrIzPGacAw9SIs6u42U5TfscrML6R3I0v9hrK1kv38BIVf8DscFTECBZ
S7tFfY07WjdT8Em3hCBKjEWOXwG90JZxGhiWJcPy4I3nIsFAcCy8OPXddI3U7Xs1Hh/qRAXpFe67
me1Bhzx7d55iVbe6fMxhcHmqJk4wClLn7bLPOw352aUj5dZ9L7rQ4+wH+9CjOUUtyHs8l1gPDf7n
eSIuacc2FuxtsMNUEBWjZXNCdEsBX8ht2ARISDI1HD/D5CHxnRhurCqRC/G6ehy5E0QU8Bmmqax/
PG8XKvQdna7uiiW70DV+BZaVtlbheO+7fR1EVCv1/xXr4I0W6LaoUTFXuCQjT8MuRsPjFbhnPR/v
I3tawttgA+Vqx4cidiCE6anS7NXt9sd9xlOH4Q3msrO76lXuopix72KlxP6hU39PJFkbHz+itVBP
m+dScGpZtGGMtv02s+edAxqJfkxebFLQWPw0kTsJFNWAv+xjwF+ffY/LshSvQfEyX4RBbMJhbb0o
BX+B0amctAfJAPYBY/9c9PdG5A0ZO7Y9d954lUnu+xjqZM0RoNvxkeQIJlrp3YQJozF7SXDI0ViK
glbnqO0odOShXnY+cWh5DA2mrKtJ1xQ0MU6aDXtkAyYm4W8eNviSi7wBIsPI1Du/xUCejIEJfAuz
bwzc6kk2d35U4ECQFZjpxm5P99m4zwR4IjKPsnaZ6/pbBXY5QOOkoITurOBzHgecnNfJCdkSgQKs
xr9/KuG9dJEUdH7FZkgKEUrGwhv32yjYL6oebSTMMqW5pe7pHkucosncoQBERS0+WDA/Z/Ts/LSI
f4nIlb7wRJangJLq0KimpLFCtgCMr0OSJaIH0xcFN1QF6spvUqo7vCyBfSgAdK1qNivkf+mM7H5Y
JHLW6T8j470puNfVs5TcX+Y9YGRBm0ub3PX0N0WgMtDp1PO5DQ3M727Wt49mySWO9L33999K1moE
GSX2GOgLlV0thhUlU2Fq9Tpmzx3AsHMIvzzZR4ygh1idHFz4DDmlYNckdgh7NHAZScknfZlWxT9X
xw8GcUKW2z/9jhSHKuA7nUY70/cQT3JYnqjM2G4rHcMDN8lITqgKbs/ALyEV5eoouOYls+fyIeZM
0BypOPpuFJjEPxIJdo5RAikIq6J2b+k00dOoIXPpZdpJ0DihktAQCAR1Dct4vlzQgQSkI1z8Rk/p
QO/aXmVFbGVETiK8YeULsDpQ51/z8Hm1Tqlb/JMWA7xYptC6u0uKL6aOJuGix9p+88rR3kY6X47Z
sVZ5IDnR477h3usKj5hbKwc1QMgvX4dlidvB2USnIPuKIb6lfEdrqR0U98z0vQdSbfeGUU1l0Qls
2ldRsjZUjCDC/sdD2t9ysy3FKSsdkTmldS4oYCEeT84WzRqGzV0B114O0W9M6FfQAWvmtcNR8+jk
rlVD8bFmY9oIuniveRUgkdxGDIeb1f1Xj3e3S+5rfRsPUIKC73sNToujDp7h6JzXBkdtu1fSQJ6s
sKJEsDI8AcRabH2znrO1YdmvThA6IUSibA4NZZAQrRZ32SnFqEKApP3v8DiRlS4EmDGyD15ZSm9Q
b3NCchmL5hKy1qWwOjD9NVdREX/xAaZ3ZaN4vvWCwhkxdS83trmT5t+HuMJUJy9+KEVsDC+NmnNb
Rb3Kp/4kVX4WRUKDXLSNz8r3M9ksi0zEDeeaOa21m3q3fKaba8MInND+vT8KTxXSjYzrfKqvMPSN
uURAfOK6R7iAy8OL9a+DjRpylYUwmCm6GtMj99u8KxU5FfTTrjUSARBHnOFKXRuIKXkatnj6mixR
GscAMxeHRpc2gZyvCLbU6JlsxL0w0iBbeDKX7AZZek8ebCWb4gmyWbm5IrYby95tCtCYnpEsy7wO
JiyzBhmie0ugv0+aO4a7nDtlqd4OACThA2j/UZJQB8xNZTYOvFhWfddrh55xHRvOqEV8xizVgJCY
SzjmFPNV+BG7GhouuZXgN5ejhFV+Udrw7Mz6Z6aIoii25749bhikK0hOtQ7CXWIvy4TwKOkdbPqR
+4RaBudq8d1UUWG+WB4fOJqkzDqReTO/0Ffby5mDTibx4z3pJR2Zh24NPLRYzxzS72gjatYmg92w
GfZgpYZbZUXuhX7GUTnh/9MscAasN51N1mR1RDtxCf4GiRLX3DvlHvcGHUgRdMpY4FtdQk/9D71J
bIra3qrlw/hdfaiTWPxJjdf2EaGthVZgqlu/nFtC4j12CioA2gG7Uwvd0SV/dFD3SNwAirQ6KfcD
/PBCOgN2q6Uihi6XYF+AfXv1oPWqE+UXBXo8VAdWxIGzN1ROdUWIGb31O/dFbaQeBslIOEe9Yjq/
glYmmAAt6r/vYLKLFNCnt1Ykg3MZ5m6NIyr3H5e++eX6A2PSgk2eHY+9DcJjj/V7GQeAzmodTuIv
YW+98pNFEzlhGCkdZliaqUsqkRlb0ERFGYwN0l8a8uYdfRd+AjbVH+IGwyjOeC7/afvLw2xKYGuQ
ueY6fz3oe/D+ty11/ahDL4pW3KjvttUlAYdxRoJBtyXR7OM2dOfGB3jmWAyU3PSa6+ZhS+BTZ3b6
YyvEHieY+4lbSvbHedNCwAYcncWru3ylN079eAK3exSSD+vuwhZPyUUVLM6Yl/V7atjNjsMP/4w5
lDxm64lKNmPf5RVEjVLQKEB4C7q1UIA3AvfuLpfsdLqzwKUtcmAptoImoQjtuGwu+V8Dpxs5PwpD
VM/bcRcUIBxk3t6QKbeg6yXJy9k4vM5Kf65x4uS25jNjhUV52/sWVIflh/XVTVgcyfkYltI0qRCf
Ma5f/B6M2Q2SeZ8F1Dp22zjYSkLpADZMb/deL4B8hPMt6IZfRKrBRR4s4A6D0VbfJle9h9TTs7lS
MTtK51jvMjiPLIMHxSjQ/lBoo0WqVsAKjYIMZ9ML649wPkAmuiMghZ8apoZmu2aZMcAX7KL3Iord
bQLtH7xKvZ/xpIkUEAhO07vANtFS90Dbcit/ieGWOV6Wamxg82BsTnR2IIBFPoY54b605OUhFZ7I
rDFXmm12Z/0xQFMHseFRL0F4t6mCRiULaaxoECM0OQ7WNJqaNCd9hhZfmKoTwDaaKnnfR8+dnzKq
xCn14r9Ng6NZ4Bb6WYLFOtIUGcgsXzC+RNX2MToLXazKwGPNPnrtb5KCqAuBvVetLCvS7Bu0xUa0
4HWqTa4A0VoD1kbl0pb9l8eloHLIhU8rjZkEP78WgU+29pMUXMWKdaayGzD3m2eSWEM0W+APPBQe
a+MMpxVIW+k6HRm0PSlPemMBbOb+vYByCEvPAlSV8c4ssrQsJ6rrwX0wZ59Sfgu1LnGGpPS0AoYg
vsyT/vnzpJ+7fMLGceamNc14d4SPg86/y3QQS3hwMC//8x9csw2RFfYWwqzdHHCYaw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
