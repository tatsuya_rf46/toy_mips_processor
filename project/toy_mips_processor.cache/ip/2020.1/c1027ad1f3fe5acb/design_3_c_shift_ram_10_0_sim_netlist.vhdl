-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:44 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_10_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_10_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QcE9JgSZJXlEdJa9l5/KM5UAOqKlcDvjOrzeoonFUtyvaM2j7QpunRFdD+nqKIU1Inva1SMB8nrF
gKiJvHkHyPRrcgMpXzTdYW3AoEFHbPojwVnbf/Su2642m4HcBlU2w6q7CBex29vK233dPM49gTvL
cCjsqfF5ORhuiFJh/8Y4Qm00d9ykN70WGas/TKecNJzkSyknK2KrDJaXz143djKBIW8o0Mw2Uk7X
u9QdFSxR2lHbs2ZcYl101/+tUkRm20+ZB2IFoubpRsfyA7Zr/1ruVO+cCC8bYU+935iCDHBrBH8z
gcfGMlWwQLEwQs0j0AIiXXCzbBIbg9jzFgAdRw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jEDgf3L4ROYKqe9m3y7//JBHbzABVZ2xw8rr+XLkcU6/1i/Q+CK1RrUSapYT3hns63S3dguuynOy
DYl6e2OTE/klNzp+twCnQthUEs4Pi41pIRPX9I6M+LVPP+pxJUfPeYAZhW+hSzxOFiasxpKmNxCl
+RmQ7gT/546CTxEn1w68kZXGG1W18g1AfLfg/nwd0jSr4BYTMeRE4jhnhSpINxIL+1iEWeH2e37C
laxtR9iW6eG2IkceRmemrmDcHn/Wgh0+jSmwlDtgbODRuuDkUu9xQ11TMt8SFzrXR3jPDxHZnvcX
n/Oy/YIB2p807TmxP3C0h1wHG/RyvKj+rBdBsw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 15056)
`protect data_block
aW+QOdfZjpxRsLUjvcgDNWB1sBnKxE8kKQEzn+b+GkLlbIHV+pFNLP3vCOOdqzBAGucuEMRe6A9v
KPsvbB321wIggBLWyhurHc50bNs+9CVpefWMMZ2/xtVnsouo5A724LqIAaIl/hZVU4vz4mLpTAto
oiyJJ3ZFc7Hkxv40XHRRtoOD58zyoa+vAnPyVi/mIjEfv8sKUJHqh6ErKVErTtBI2TwsCDPKsSLz
zRrBUB+AJQIVnn7BPhH3iL4yfJTOi1wGBgzeRlbPiTTtLong9C+DguOzqzgVQH73B6cdIjvN97PF
8ksEX8sSON+u9e4estK4ViiZcEE8ri/hidMZfTJCsGd6/+nJMvAjMXGw/Y0jOoiyoJ3TttgQUXTx
4JF8/r2DzYTjFADqSYVnP+3F3+3dKvGSif31baNojJTnlz7pWF6KqCd23WMVjBYV74IoR/Pj11uK
dpR5kf8NpVwtKkD3oP8l7JtJMxMcuXlDNTtKRf0LppnsjHg83xTlGz+mETqwHE7p6zYldKRqoXd+
IHCvr1i2vJxAYx4vvg8W8+e9lHU+75oYWdkUnvxLcSJkqqYjzNzBwSZl7nWW/4tqB/IUsAizfvRX
A812vRHI+fxSMJvzIyHBjVXaL6jvzw1DDA46gOtrpYgWZzwu+/0xZesqsOs+szJ4KQ/vysnh+Fug
MtFjA/ljOxLClb2LSeUg19WyqvYKPAOTnl1m2az3xK2VeVizNv8CHz9veto19PXjF+GivaTMhKL4
+F4sS0okiUQswRlpuF/ABq3DB6i0l5LImBbRjp7l55qyl+az8pfRgMWT+pbazfnLAO9JOCUE9hZ3
aISUg+3mlcFBgnXwRXcDBmV+BLXbc+wEj4QYsIYTUbpv2XFHQq+PVRV7auaZZkOJ0jrSYw1GzxaZ
kUfC/PK15iVx3YONg/6hvjheZS1VBv+OXAmu6SyYhAhajQY3LjFmO5nixDFx/yylOgv+Wz4L8/WF
F266RC05EfySdVxsI1ToZ/77w9y9K+yJ6nMjgOTRHKkaIICQh0bLVPsy0aruFVfoqb81zg0Gc+o3
3gvhCDK4ThA1gLlKauvJSSm9jh2V/Fp4zpEsq1PZPcaJl5BRykDSznMgIhXJpSxGS2WlKYTKVawQ
0GYKQ2qb7G15BQTCZBobI8sBOpKxmJzWaKMCA5DSOd4qAMtFfOLHRnwxkZiiADYXIFFjlB178lG6
VDdaQYsgxvCI8Oqz9CUL4J1UmX0fj8ywWepKBbgY9GQ0USV0c/uRr14NWuhE1ULte8fCnlcHm8/k
+NY+5q+IOVNBCTHh4GX3KvJk07UirrP30vVEWDz+yTSwcBoUZBppVgygE047bXeqauh2LZE6BGiM
WpuB7iBDFDzecNDHsf/vdrNew1JK5iuStf6B+decjj2ZfD49XcfcYuXV3bye4RZrcO1UcSYb5qWs
vWe7eTVj7v0Dcia1pr18P/kLmH/YS5fU4s+eDPI7D9JLBnvUF4XdRTrbmlyCdXoGbcOfIpdMb3tK
SMAvbDQev5Axv0cAs+BNAUJQs2wbkKpeMyDJWIaJrTyhfl86CoA8CSXQFkE+KwP4pYoiKxDTd3z8
HtT10hOoJGdS24DbSZBUJ3NQhQpqgZwNbwwDE74wxN2sy3Izn0CAaEGGhhP560Gej+rpFXHXo00q
T3IQdSM67HsqsKgLY2QEcTbDbaFX+0hUzH0/3eGTyBuTLOrK+WsaYyNN6eX5eXUsNI1PhbjKrxvc
T6TiwqfJ8FWMQzQc2twOR/ydr4s9cj4TxIVQhlbK1eYQuSQZr81fWfKoXQ0/0K6MtSW+0ogZH08f
/5ohnzH3/u+itXsZ20r5M6Cupg6XrQYjMG08XgX0fHLBJcMxxu6RWYvjsiZkrRxQzPwMj4TKhkNT
X4d/j6uGyjeWy9yxnqYqISEQdvCnKXTD34erhC7n3TOMTzIU1A9/oA4q1z7TYIqrT19+OEv8jsa/
GeSHLGqtSWFTh3nAmRhoUBozhhPIb75sMgAv/XafZct0Y/89DUc22CKIcDJ++Xx1Q0f/1gympzyj
Sznrwgok8w5AstyiKFnOhMGE9lw2L1jfNYDRNWr49DGY+UNHrE/2GejgUfLD0miGqAK93sbmsXmC
pTi7FNM5pX0TZtjPNKSQHC8W0WSYADe4QVIto3opSWqSD1Tct9Vpw6dgrGMhHobvWNn1YYXdXJT6
9yeyKIpnpEcUi/NtArqCPAZGhj37TemqbPiOdP94f+kqdzW8hy7Z7CRXH7203cZHG4iC6bFEa6qP
KCLWbsyJI5CTZEG6XXjzjC/R6QlmEsb4b0ppk8fcSBl7nIZ4uVtK4z9dWP/beQnjDsUJdQ7EKKnY
nhDoxMlGgJWEcfqgZJP7p4fwOmldXY2Iy4HTocLEEaHZ0eCDW4pbrlDuTvQsuqLZTkNQXDW8vhtZ
KSQXTp5Fgd92oMEYe+0OieLYpq1YPIYau2L4CnqtgJtzKqF+7/HpMldNJwJqqlfX7EZ2AYNUSr33
6pPRnutTsfomOEf9Gpntj4XlBpawfBtMxKQhicq4KHP0M6tAEjoba6TB0AN7J1X2c3nT4rsOJ7kJ
hEfqLyz/CUqziDOeH1/+pk0l0eSRvo6aSH5GMl4AOq9RWc5SMRHhQl8eHvPkrh8C7I3VNPj/gG+i
TzkxuMlMHgSaIXqusH3QSokPosDQNc0UYiu1QxCvMQT1k2GD9LKiQbcJabzqrJWh6phslSI+6URN
RG2Vyzy4TrPdSJrmHz6lUoaob8bdsx+tnATd1Ek/mW0dz+DMXD78z8czjO3Yd5mh/vfF4++2NIqt
YIRfYc+MMSuptRveE2AeUyohqtEm2v6R0Y4D5ee/m3bW4L3Jb4takmx9aL360YDlMERrr9ULx9FT
Zm+RXNr+NjsIZC2/J/yJarsHhNjiXACjeLuh+d9yu+f5587qrNyeWqt6PsHq8ykvhjOHax8VClQH
9kHf5vtzrxM3C9ASLDAV9tc5htYo9yj0Z5FgOdgyOauG9PBHBxTnUuR/WY138l/xUUStEEd0m8s9
KHB6/juprq8LrjkEptwfOFdeIffEwdqpemGpRPNzNcTtI7SxFm8RjJhk+C2tuqObe4WlYEzy823L
L9q/ks5DgmhXOdAAqS0GCW9gN/2xq68GErPb/eHkU8bcRxtRc+NrlUhgiyQZlVxejwe3m2oqRTe6
lCinKVSn71c/cf8an2Ckkq0uAKhNRkK0qMpZWevXQ9TAz48Cs1lpvrqR99wz98Qz7fkF/Z+Z5Oyd
DRsimn1anxgLR/npNgLas0XKbSJWF8HmxqglyMTjI8DV2IFLcSTFx01RaFyJXuFVDSunszj8opst
UgkWIy26ga2T+usi0R04EIquK+aiBq3QdkBACrZz6nDKHlD/XMutfLBkUZcPl5vJSGEoeRqJODhy
PQv+236b6ugCae0kECzvDpTPlisClrMsHvPaqjH6MzCnyMAnh2SiCfQSilLydptgnWEICFp/R8T6
ivqdC0L8ID0y64o8znbfUI7Y+OuvpqoUEnvZIVqIqC40UpBNuggMvtp7Ttf93ySuZP/aXyHDGnze
dS+tlQXbOeFf3WH1gP6TKQ0sLzwGPcUyqDNa/oMvMXA4Yggfx4vyBeED6+XLFBx8ATrCXTgubjI2
Nck9/PDybmZacIhq3dnr+vVe/STpjgOQKUZ3c1TYVocVgR4w806/Pokla/PDBXZtzcjFD+l2YZgj
fbdZxMs5jn8D3AUoB5WLI/rvFTDm9FAfjasdR0OdMR01oZlEKvXCD4OoSJnlbB6mRAhBGCPZS9Zx
c/ID2nsgugmq+CqaeGh2RmULOsdG4bZAsEDk6WqdXdgFSOUkztsefeqnt+QYAfNtMTqMa+lHBd7l
o/NMvPFFNma6fUT56ziKDTJk0QKlyHDgWqW7eE4eGrEJuayYp07/7D6GQCA2403Sbnr+Dnygdf3A
pRpmX8Cn70IX2OuD/Nh56UDU/kbwuIxg7pcT0MnAU2fORLDhBRWweElfmR40S/xIce7ef69AwGwU
9d3hHBS3ceBdAl/zgBJyTjb4ygXyYTYkTugHdglXVrMb8U+fbeXlzDZNPjolzrrVpeWteFoar+3u
cMoGzD4zBmPDMU59MKEvqY4xUaaiknJKKVW2V2Paj9xuNBJGh1FIiSwOJ/fHvgSAIoF/MJ6e2/QO
b0SrXn1xm2mTAhAhwNhTc4t5ZmTmjLqe/A3eM98C11/ZsJ2PRCCPAOWww6KFBYk5MoJCM69AnFKh
8N3fDvng8MTCcb27lbZNNVG+HZB3V3Ly7otdjuco9NTLKBCmR2e6YyULsVJqXeaW2KkcuwvhPcYs
XF/zfv63B5wKUAiy16zHWeMyXihbmFlOfb6pzLPwGkqmYzHe49KiG81QZSexm1BaBa+QctZCeEas
DDlA3yagkOlm8hbuSJvtacXKbQIt5a+rv4wS7GAsWjbMliKb/nCyWpBcRzqKtuSoPIeRR8q4FQ6H
FZMXnOQSEtLTSgywfjieKMTx5Rad1DLbiO8ZoNc8Y38Y0kIH54Q4HOYQyKDZQUaVPZR17IZ2ceWv
KIu4pRn5i8hkoLnGvYAcf+pyZucY+riNch9DL+UzwNc7BmG8utZHZtd7kwyDeNJB+Z4x1BH+3cOV
u3cqGZ0IMmBotrLVCRd31nwa8g3CsZweJuFBtDnJJ1X41VYEqUJ/oZ9gYrqkwKCdkbuYwmrGuhZ9
KkPZYBKNkBREVW8RMlP5942RUHUqlCQwl8A68UeD6kI6QDal40M5FgOaoIgX41yFW9QOdq0qrQd1
/DPWmV1DAt0hdHagWbFohoLIEHN2c77AW9aGG2dgIsZzhXjFwHQJbtRba9cOb2IFul03n87x4qjr
fvwjFtOuAY49YDhU2bjJY8UypVtnfD1+4r6CstEf1xFGmRjzAN9DTFWhkaWQ6qe6+nwVEShZW2QC
LuerwMppGZhd8mJLIYIiAXZlbaO4lS5jWmImnHgKOT6YKIEc7oHRhXfG7zZyFSCPNbAuC0qh7rYb
EADZh3pvs8SjfOmDO7l6onciw+RpAKJ2jSbli1fuINuwiRfdMJapJE11PX95ltj7dBvC5/oC2FkL
f2BlFKPY6ZdycYMhqrjwVbb3/YR5bF0lMfUTtX+0Xd2APWGu7gfUH1vdg775djws7jGiriOwDYDy
FWvxeZuR4S4UbwqZ/TxVE37JOxn6iWszfhXAekgyZqS8X/LdtONMxeNZg+NR+toK1O9ba7D8R7wB
U/e2GNESIl1qj+SuJP9CsrSpo2cPYy1X6AuuwxpXBO9p2zIqcGaTAVfvcg31uCtjIEHw2/X2H/2d
lKdfRg76Fo3u04ZybFsOyvVc+6ElzMg9neszIHAbNOr1p3a7oCX1LdzE1XiFlKn5watgQCfyeGSB
B0aUqWA3PrHN/evc0/dBZg64b3n9/eoKCoUCx58b79TZzmdRZxuglK/Y93GurnbZewNci6BDFjRr
S+3xfZBqSt3RQ7XCPC6yWGjiZd1Y4ZxL9l1h9FIMofP/QE3hDZ0jMDcaFmTlT0USksAWfOsIqSyy
kN+MfOC82FYL+75Z2tR6Fmsdodj+qj4bT8swcGDlORvPPuH/Mu3391l69ZiJGKW/A1WyYbLK6I9L
X3LirJ/hEp4WtDksYIvWJ+IgrskcPNuuBq5vejOQyXQaYBkgbQJ6BhWsF8pc51/2OqmikjQHK3E6
J38qiX7rQbiLDzVferCwNJXa2qIZypJwBfMI67MfUqg8NMcUe/1Y9vI06j+6j245/7gxFLdMKwY8
EA++RA29ax6QiY/xiF/VujW7tWPiYh1F5himw1zzdmM5AIgomvuFjW1+39uLnURsXtqqYusi/B2R
niPQG5XHz6FSfZTzigxwoTBm61azxnbpD1NE/6iQp+YqTq1B9zdWiN8Z78MKb9snM6++UhUatDnQ
SS3ugMeaQ5aIrjLlQDCDBlbNB8PFN4YNeGv3zMxjE6QLJDjDSafBTExa9SJbszs24Lh7TABCq+9A
loDxRNQORhcN6nBtpknGVgHlWGT6pVgFXXtG9cj8Mx2O1BIEK6XPCsjv4903pT0HaSqLEGV8JplM
3DL1cK4TKwGtCvuzGeN35xaWDgoD2BQe1tvs3c0URYaH450JwDXlZC+tX1PGT4N9A+fqbZyyZb4V
j8EH5//aUPN5WFHCkjQkQJutvpVKazoOK9LRBbNutPPPi10oebLqyLMvL3GxVo6FmwIt3v6OFWBk
zHPH0SxCq0fUDOStLpcKnJu7VyeQXWN/GGNeCZdGOayggDlsfJUrzrsqi1tbtbB42cq2kHxnUbte
SbXlxRs74DKcnuuQkzPDnTsLonOQVPeEq4fucLjh1D8opLJh1NO9Psvuk2Z1/Y7c7WS7MdSbt69e
+fGDpCuIng5jhOZNRnlFC5khog+my6a77pwkMo+KPti9A6ZI7VPbAq5Uz+NXJ1VChgGWGavhbDWv
rvvNFycfIOe2/luQQu3uddpoXgtaDaQtRJieABreSx3aSo9va1MbRvHT4RTjgoCxt++N9NKvZ+sx
YxqXd3BFehukVfpjwAh4PcazKuXrYggKH4gQliDL6Yc100VqTdus/LFUWHcI17JP1yVKulHT7vrs
QXpZaAyn99vmcHN2/Df/Gj8CASUb0tUkZ/GSwx+MFkQ7vz26Ug2y52/ERI5hkov5Q7WnzMMhyZm9
Uv8oaj7WnJeIv1MKh2X3WiaSPvr4LY24928I0GrVJmHdrDTIi81+RVQYJulbeaZ2/vORDsKq03XL
Y8De2QDHkuGlKeBS5+GVg0Cgcp/oZ4xXM7zshz8VkAL0lIWDHujaX0LnpQkNVPilKwOBcoQ4c0HN
ZD6lKy+Eafwkpiea/c1nStTcEnKjr2AtH7FiIxOA27+FwZia9+lDFkH/XGFl6j+Jh23roPwbOihm
h06uhfna0NwuvB9MWb6gSrBOb5aFBEGJ9ighnCpht6CoDCrlImyk6xNFuTMJv+KNYY0OLC0hfsVP
minhvB4ZGdp1pwHw7MmvgnoqQXRcIts0b2qssbl7lBPyvOLkJpvgz+D5juX1FmxM4MpsPzjNXDkd
DHctlbw2L801f7/CDr16ezycISDz8KHaoCEzynJiPpFrsgHjRJU/YO1WpYtJI0tv0cgNu2pFZpef
0T3Hnwej8APFurntmhijsdFMnXogmjBbXiH/tGkof9zjo76zJ6Et5nWtCxbPAnol9FnLJEGi/NuC
NKvh2/HAnOCDxLlPip98ZyyRdlqpzBvOTkOwu03AbEexdi3i06wNacSR5uZaB97+dlKmyARKyfyt
ly6b1Ea4LAWq0kF/xX74n053Q1A1qz4A+l926PhbWA23tlZdRkTHZe/AlsvpQygZaTnY0YeThUx4
qXdkylFYe3LA8HdhusFxC2fg+nRdjB/apmAKXQXdoHOm4g2MrAn1PwoLIU0/sEZI5/SBO+gdXI94
g0rE7eQd2wzuwbQ742ur8N7T0RY/XI4/QpODiExYA173Mrxc6SXaFtHpDt0JiTXRWtAgVkr0nuAg
boC/Gs9/63BuCa3BS/9Bku1jzX9w7sjwT4ANUwjouScHGZyiRulW1z+mY9AmW/XPB7kjZBUwG0iu
Tis+0tCvAlECbEYDg48w3tao9v+CULTD/V+pEaoBCCCEt90GGXdx1LtEWozdq7vPmSdWILkAndDE
axo4yqsJwoL+MyxXnkHySLPYaNZu44HWRjT/A2XWSnj7ph8jVwcPwaes1duNfg71TRvx7Xnh07cA
K3sSJkzkyCtISanrKiYJahtfBSVy2Ym9GIniomo0dmKDi4dfjTbteVfpEOs7MfU4ICKDX6BQRaVe
B1TnLKL7H+X2XSCUjxMbiXW3SlB7relV1PTCmG9pHHxdSTJQexrMLPiG3oLVgJac249URdgGMdZ+
BHNDOUKOBtqH82+bfE/LV+8Cs00XAys0c6lggNNoLSSoNTzMOTCHhFg5zWxdnqGOpgjkFjYjVSec
UzhVInfTvGkM7fYUydpA1ziJjFZDMs99MzCBalO7piGdUm36n2lXBDqLP56EuSIhya18VJqutXqD
w1KooMc/ZkBQrRARt1BJkQVnibURNQu17xCfvvgsIxJdizH4f/Uv354I0WTsS4J1RolHQFmTZF1u
VuFps/piJMJqVnj5swnVrIGSQ6aYLMGr0v2BAKvQDepUgjxV3l1sZOjo5DtQW0N9gTJ8pzRR5dHx
caEf5+Z19IJfTZeGT5Shs+y3qOElVnxfCkedHuAydh6dKXpwTJT2TV3Tks2zXSA9D/Z7U7IpKKft
RLBG6xmgPce8MGx8qCmXR94vVJeOys8AauQd6VcLZ9a4FalKnJ2ghPn+kg+zYYBv2EyJj72ocULt
8qc+XHC/Dc9VIuo0mvgTAIEnTnog2TJR2WA3ZnLjtsoCzmFsoEZLcWSxos0AXl6AjIOQtV7W+CRc
wNqE93RN/Ou4T3V/AfRVqtjZV12sI5Y6p+nMABct7mub72hk8BJSuQx2uUjgbRmLiiz83u/xSHLm
xHyjoJjoibg5RE9Ja1+tqXt6WV2qVswuj79+K+l8faICxNYyHoRMrG9uv+J9YexIgdqirJeoiP2t
wKBy5cEPhJ7xrgKkh0pmbWDwj/GQyzJzOheNYn2sHtNhu5yXYXbmoWlSaPhU7bEu6H01DBNG8Un4
fwo+jbNLVxJmamg4w3KJeKMGAkk18tmoJ3aeNHLxqvfaFCJzNlorac08+nmtUhvdsRwNU74YAeAy
8zrXnLgz0GRgXZqc5x/4pYB39wGZf/sx5W7LB0P0gA/CdriNctg7A8K3LPsKjl69c3qlw9u7bIjT
UMslGVw7KixAPkQyeGgD4IhpGWqLWCG2OSrmpOP8qaYm8Zdr6Icybq4zPoPZUAAo58oZ61/RtYGk
7qeg6BC6jJqB/Ykb2Ckw5LlPpNhOu3MJNa8waTIZF960x8QuNXWwr1eIUOWOe1Uv6AYrdkB0ErfL
5nFUDLKWxGEAsHD/3qyI/hPEgO9dB5vxQMEbegdEJNp38VqMD4ur3aK7wuzSxmaU7YxQdvsjKRBb
qPInNMMwsMywQV9iIMI8cbsk5HqILi1JLxDuZ0jdSdNk1qrQgiQiCi3X3TzssLx4Cj2siJt+s0ky
ZC2XayoOi7goxaxrBE1DNF34bXmZDODyNfSo1kMWzNKKZ5/Q0aGOvPW+9Lrm2meTbCilfoGgxtoU
ssjkNVx0rlHvBAffGsF1UMWM/AoSXxOn74G6uHRMeQ1IouGFWembtELcGG+sy3jqydGVcKU+XPtn
/nEVR7TgjgnqfMSA+y+w0nsc3YpTcw9I/SV9B+DlvVP2MB56E4hHvCxjaEY+4CzdFJ6srW8HS8j7
R0w99FMgfO2m5PPzbMeQqDClOnNlDWZNtaxqV83RwjZG6y+dIBv5Xku8DrZa9RvMfl0NGl4YpQG0
mZOxunL6PHTePf3EvUiU9vUcbPbXlNqhb5E13mcUfvQtEjKrrWk49echkpPGPKgA7Mgh3O1hXBiM
p1H/3lHfc/bn8LH+l51TR/tMmgwi3rO8rQm80VcvdnUiT85540ZDC/SzQY7nPPLSXQN9XflbHUXo
WJFIJWvo1ynmLy1OgHflMm8qp82x/3RlI0+EvBAx+KbJp2hypVxPxl5FYu9F10ZJz3T8rTAaIXNn
hHlzLxbzrrkBey0CeYrC40dKV2KaYja4MlwDYbRkP/a5tXs+iGW4W1Ns0rem4wzPPWgtQM+rmGMa
Yp49Z5Zn2LUMnWYRduw09i9eyJ6j7L9gDOcYLeRzD2G6cOIapXN/tUiHxXv9K/6GHN/O0w+G/oQX
mGD8yUVF9wuAvfrLdrsI9bJdw8xgysSzxOHvaW4GUhzRtIN8uuOMNfi6DnT2BZ88Bg+zubJXRh6I
XaJPvrjgUf0OjDwAVrGQu6ihR3YtKMoqSrWBo4ZhGv4ZiBeCbBDoRyrgdGo5niPh/PyDNfmmYjZH
mOrg07p4Wmth2bPI5C13SAf9Avo3A3peHLY/0z4ulVMO78Pn8kM6FC/ipNfU1SvH3rN1cZ1TnUsa
JPTkhEacYIsdFXr8Vj3ynDYgK3VNdqeVWZ8/CFU4w46l1OrNoZ4zrC4u+LnViOiFA8Hgc8TG7500
TTTxRSiN3RfB7K0VydLj02wUeyNupex9+KrUyHyAq8x5ddOFAx/HX8qNl9CJsEV3Vpg1xS8YDYAw
7ltJvr8poMcmgyEBFH/0BF/imvnpAc9PUF+Yr0MRRSY2M0zMmHGAC7t65bTaIXY2CyItYug/+2vA
zUGqYQhkJ7XROU9g6r6W+i+n3ANuiAioT11OquxnStZAbA/pYjVVQAFq/meUlAH/wO/ueJk50R5Z
1JT+XoxN8eDVME09MsYjcYyHYw0Xc3E6hPwxEgaz+jkW3vsVM1lIhNRt8jP6eg6guSUr4yG/jEkt
HJKsXcQNtP0XBkkJEvsSsEkadQNpoEL7n2iCabNIbV+z9zNUuaX+NCVjs+gUF45YYKWWE9oancuk
NNpP/IM052R7c27Gl4amGnQEG9RfCiriu5R1ZQhljyRTj717z3gukl+VgDqsuKS+uuANcHk2K25B
IH72Atlb4ofIBvbepD40iu3VD5WXK5t2YbiWWCN9tzUF7mW5eYgmlHFNvRu6YE2vX9blMQOOq5Rr
8CqM0Wyh3nnfskPMQ2+si/xc+I4FBo0FhKHHk/A0K7VGE+nBxV7mOtIVQSiNruRRVb0D1lkm+fr7
3siNzzD1KQwG8tPtqgADGkFKnOHraDB0eDFpYizuPc5n0GV6D9VTJwwqQSzrdLugKEpaTwh97pIk
y3xCpKgZt3gOYv0hlPTloKlGZAM2QABrmi6v5chbJ0rXtSX3XToVswE0O2Xw6DeDWWtT+biv+ALF
n4oSdaIF7At7Z8ep1NLwSNF9zxmWTvkxVmQOmtineOPIl9S7kPgusXYUUTCxMpntMeeNILTkVJ2J
djXm/SqVmeEr7p2nogwRGr0fqrXnU1eja6JFSqCwxVeMxy6eXl6Zg4PTUOes6c/2LdLiWJZhGodK
OMG3V2kdkd6Eoy2XWJRhkvE4PGVuH3Txa2tyH67iSEey8YPYzRvpKKtY7J601olo3/fGsOYl0QsB
Su9P5nS37n4PhKRpMhNp87ru5rzz0ceMGJ8KqpriDsvtAiZ9WltYu0Xz+igMUdIHrycSFtk2x8Ls
y8w8weNFkixvhEM9g0I0F4O9zVY+e8rggLh11Mv2FUHlTq5AEIY/lWEsgMS42M6Xz4dfgSiFYhR5
ebzRX4gfJHzrluHEfFSYHeENjbgEu/uTkMIe/m1qAh/6lSDtUofsVaW/GhPFmY/XjqcaxWUIDCgJ
cTv7rrGhHCQsLBoTBROe1lEyh7r18eXPc/cLgj6y+vWrOOJuxaWCaY1CZGvankFR8K4T0f9nixDO
ufpLV9bBgOSuuH11fRu8FgFwNXTtJp0D57Z7vMuIb7akUbH8VLdEnrC/J9qaV6n3alS9pIjqqv8w
hm1R3298h4lcQp0EQ1zkBcjI5ekvHLW57AupzlaZPVzTVFjK4jlUfQbt56aI1ui18aHQo4IxD8Pk
zWB/sdFUQNbRA7SBwxSVOS0McQnivtzCqVM20zxJ5aMKm1V57rWHeVl7eFc7IVAx77Dxy6yY+Yor
iCbsLjwdbAXddsoKJoxaQrL51g+xtc/9WaL9iaP3Jf0516AE+vKCJkrXCpfhAFktgCcR5tYBxcr+
Yej7bohTKzQpoRwPVyMfEnWbeBOQBPu1V4MJ+P2Zb5cumkRcUxO/rb0tXgdwc/NrsYGABvcTQnRo
i7FdLKUjKoXD3KzKyC99Ui4BJu+EUbd4SvF4+CT1/22ApcIXwahEtN/OXXc+L1SiqcI8wQqL2ctF
eqCG4WNQ65YVaCmQOrmwfBsu09w8RusOn0LvDEQb3PeX/mJdh+xq4JNe2/bPJwFs5srNi1AOP+IE
qQ30g/XPe1jkmEhqyA9dI/Sxe/kp4L2qsfaiWqxxzGG5wsHQXD4wu0sdYlo6mA9hq6488GGNrzJe
n5fe+cK3Rp6ogzFGfiAXtTPVvgH87EQTYuww/3BhktHSPqPazvgmDVN6eNL0xcurdvrNnaNh5Fms
WvzT+wcidhtZV4QRPjBlqOV1nzdEzTN5Cu0AMZIBe0hNDRsHBrB5UVY4CdK4axuO/DaGiUkjLaLC
6X42afZpy1LI3aoRvDoMXWU2jgcltR8oFR1v8rcWnsRdFAAHAAj50ReWqaRxAIYR81aqabRyJq10
/MTqk9NlKZ768BmNFJYOZY5LhNtEBS8SVE7pm3BBE/Ivl2TzJ/H5Wz8xBfpcf+fBl2qUdEwn8lmu
YXCry9u71l9PIQYI83W2EHvw9u7DWzk2imGuHFcySBPECgBvbq1UncIifp9OI/WKaIFI2obObIhk
4n24jhEIfcyDbhywtsZ4rcFMfWovkJ5f6G/jytwJg0uv0VCNxfuwydiHw6oq0Nnyp6LGQIhizNEr
9mqFTRgDwRaN+4I9WQljOrZ8PQUYziHBZft0159a6Z4jcEJ5XzEZQJzGa4INhJggGlSnZpa1aXji
LZsAt97w3zHTL1mtMxDtxcgc3be4Lmho/eYXcBpH1PAPRYRCmsp3sEhZ7I/YmdA6vfBnwgra8Ryl
P2sWbSo4iIbkvJRBoIEp31aMkBa/Uv0xfksqm2iFzTPdm9dj3gMsPqD40aGGdwry4XUgSa9d6JJ4
Qc/jqDbBoWmYKcf/UTcjqYZTLehIcHCEfZ4ruhhUxPHCZIaZ1ZHWDKp9C/WXVyZHRTZtNh33+nJN
Z2UUpZLAKfENH/KOwhcJLaAKcR59VaPtcEnCa7uGVBxlhdv2pD6xi3+aiA3f4GDZZksVoOg06yU0
xMnMNARj897uzkeLTIZFrz1GDIKjABJXhujZIQV379DuSDEYuKteKg1h6reBLtmzDmUzFWQsklMF
DNgUHve/0QU9fvCqs6ii7yWXGGCvH8r48ONC7xswYZZ5oCh/ujiK5Y8zyIdJP7HY9ZcQZAkNDpxr
wjt8HsELDIfhce40dlIWVP9xcE8va3DZ8aZCn5JzSBupWX4LlfaKTSYSNZAN84NmMKHrevgP5iYp
RRjLyOvLtZAdcl/k81jtsCqVnTNig3nHKjUdzOa+ZvVQmsD0MPke+b6IDad0K3wERwt8PZ8ONo/o
y3VbFtLyPmEl+myBvj23Ltg17JI3QQBt1QWBibQeTAI9BLN+MM3dcDi9HUOaXAsS/yDeMsZwUW5o
6Aygx+UCZsPKfT5KkKkI/L7zEH06eNpYbGGtUPAQ+bNkAcUGvFY7Nk9RorueIPoY1e2lCx7WtPg9
A8OobkKlj2rp14PKLMHIZK6V8ryfzfjzUVHhJG/0fDR3JiS6JsioMtfnCTCriS1WKttDeXtjGwg3
oNRZuFzQZaqV9bZ6i3vFRaJJBa1sQKVw+pURfV96Ofnqkm3HY5mXRn8F0SerYCNJmfplDJaODplw
yQZnGIE6EJpG6TfW3sZnSryuZzN+rADmtthEu6pcngpb1wM5r5IB5EO+umTxMM4I57hoRw1YVkgj
+UdEFS/tvQhgFKlCmdq6dsbLj80RQ9ruIPMzjPsm7HgSVlg9sIimxRHeW6I7I2Q/m5QcHRdesTX1
BuO0f/KrPBhPuDE3/6CTSs9CGr67l3mQUZnceIwKN18RQuTGr9LZU96Z6Swe8kCJz4JdQZ3LdlN3
WDSnjZlVa1vYSX4lEZsJGUuwrHMc2G0+EHGGx7lW4L+IztWcQ8AG2Mig5Wp40QwglyXEaVbZbYmj
GuVd9Mcq4/HIDD0wDPFFuoLg+i8vVJiOtDCWotp60mnpQAagQMUmwU19G0M+U792/iUb+4AdfOg/
ZZQ/El4IZfKVXAenba+5bkni11CKhUaW17fNJ447EVdx5SMVhYlGKt4EYHzifrVTTAfD2qu5amdU
j3+TPh/vnzorOSd2gx9mLbEJ43410x/rtwPyAOSloXxvCLvPXEItaSH8cIfPr9A9WoOxXPTvgmGH
IehL5uIefkEv/35C1AfXQVCvehr9fCNYInLtpYz+s4BSK4g8wd23h8fuGyLucqE98ppOklqbgkvx
/00KnY74hqVZWhGqPAqpWIvchEjyfgADoHaTuwxP6nSr4a4MQSZM+gJlQpxZrDaq5KsTM2W2ghMP
QtS+GYz73+dHgOzD7wufCZvS28to3Rk0/L39AcSeAg8Q4osWUdRG0UgHUhq8EeYdpVj40HTNhrmX
watTCAQGC5ewB/CaUbg8JX3e+vt+Q4TRKRSN6FyVS/col80N7FVxbcyFVSCJByspCfL8okxl1b54
h8PZe96+ISLenhPYD3jq9K/uQ1NAvJj/0xooWTDWi3eFKTqvdnL3P4rHBiwBSL12ngiQoWQnGDP2
0PrqgpcCdWqMmsuYBLMdx6fR/l+uUyndfg4yoe05lLS3t+NXdxPyzu6B/7kaPxCOwEWjUWscoN7t
WednrIQ4aum9kvOXnqKQrsbQ8WLG2opnLmT5+ZZImxbyq7bpjWfSeE80cy6NScQlM87ENEm9CkiO
8dUDsRMPch7YT8E19fXgjslXCCXDPMx5kJDpc4z5XRP51H3TGQbIrCwXlU5DyTvHBWrelnJXOQ41
rajYywLRXa4FgL1WaQXbNX2rfxvRpSoiEBcZCmYmZWk6ZmLAQrz2WZ4MqUiZcRysfzibxduRH1Ce
snDE0naTvNRGcmyRSKQQzO0Emz8UQnTRGiEIEnW7OKOBnOf4cMxl6+JwHDPyJazjlXPAKlCzMfbO
f3yJjumXMbyg3IFTeaeNfbk8ZXl/3I7PcsA60zs6uMI3GlgvxRCWGiLBRX5fu3YiGomvwikUbpGG
ayElHvFSrJOd2KrSOCVYxTixdPRmw9+CIp+5KwCB6VJWvU1dTqh+u78OEQ4TYBksA5P8uG3zOwnf
ndOYhbZZVuk/pDJAY4uRqccpTVc2pygno2hn7gv/to4zzORdXKtxFEJKJbZVyvEEePrwuzFEHNDJ
k5S1k2vq0HYP2wKIV4+6m3tV5QQU7AzvW+aWQVDaG0XRcXfaVpj2xLVEHBFJhzEikNiqGcRYtMuc
+3BCe3HYrr5yyTyyF2mp03JBaKWTS5KenlHmxPcF1MPo7FoVQYVCp0R/tFZgijSG15nwg4LqCuak
mjAx71wVBIpqrfxLOURvHZWNoq2PMpJUTrGpzL7MllxkLT501kmdUYcPa4qI4OgRZ026OFPMuuAI
hjd2AstWYJlTkvRSJtDfVBfQa86J4qz+E+2JCBdQd7YIpQ82GVLigIeWkkpEq2R6iJDl6U1cK0Ke
ZPiT9PPp0A2MY516GBy9qJqedSKIxStjj51JD3j0ulwCDBEcPnW2FgYP0plprmJWcVZW7SbpT9jy
YT8ZJZRIP3Qk/Xffk7RjZ3umyyuLGPi1lkp2Nb40f9yTqY9qMjr1Ar56Jq0CN4pbliHXFtS8Nk28
LOz5TfW3e/bjWDaNEJdkAzGQ+pj8cPmrYWQW+uKBqptM0NP9ffjDF+1k27fgrupMbpZ1ZPHl8oW/
9EOSukEHoFICRC4flyroPdsBVrl/AiH81npOt5scIXpKpNhScFNyYM/FQnjAVNHeXESewDaZQv/6
y/BoU07f43ZFbBQBsthJs+IfCGcf6+iCXKg7BihJ6TZ1P5fXVJrXnvTfrJhIa4xsB2hqSUb5J/mA
TfPZ0x4rTwr5UQFBarnZbagmMchG16nvDzUoXd8u0HE0CAPDlk/FwtnisIrYuhij3R1hJ/gvC5cg
RvWoFUmenJoe9WchAnj73D7xGPgnDnzcz92E5292GNHL8P6/xIXN8QbUiOBUav6bzTi5ViBs9rKZ
AJ4rHKyAt/N2qbqK+b22ICRWeIosxUy9IW8sHB/eolEpyF0ze30WO6au69ZidwSBDoKBBtzWNuT8
7rqjKJYXXy78NgLB6j4hJECx0pnf46gdNH1I4VSp6AUsinUvJIOpCv8x4fxzHCQ9uyv74RKIay2n
FDB8R1xyq+V4gNTtbnbAQ7yl1HxZ+QhTLKms3WS72rK8UMySnRTgBRQdWxs4KS5mq5cQvsrhxThC
KoO+ghnDOVB2NHZe1wWGJkwbc2PahIcIEndcwJaTAW9vEJrLBctzi0YK4PM7PKyM5IThuWnOt8zT
nhlsYtZI6OpzgOMp1SKPcQzOX6yBA0Y6wtl/pSbTxcfZFwSPyNmKQbDCfxXfF6SFE3EIzEidjCkq
nXdZu9M4x1gnTckWFl26+cU8XkdMnJCTwWxI1Mo4d7f54NWJ9z6axvQXidPF7d+4lktN2P0NTbcB
xetVCmflwGDG7GzVrsTr3UVxxm0VInJW8CMUsaJR2fYbgdk1fcPQ0KxfcSybLAFP9VoMk4hZv8UI
PToj87pT8a+Mw1VVzsjizJIl47WeYPrVA1CcGruVCJCR8rebnP5HdEgrzdN5pnbZLkPK/LSf8JTS
5xd3cVxTHM6jC1e6GSrf+E00l8WLyxj54bZPr4xk+K3kSfYoDzPdikf/R9Nlx+pbJPzFB63aaObQ
yHkLdqCLJa9+3RY0lxsO0CEvVOYFtrzRmhK0bKq4g/SxH0nwlAVVoRQHl5xUJECE4uq/Gp+u0vxn
5A8gc5mmtGDnsJ+BwDvD8jcEwNjKGYmPGeEjNqXtHY+eRnygQ+CQKMf1MnyVJbRuy28TT26jTGWo
Cd04+fSqWGygfQqSA3A0JQZvUqDie21O1nJT3K5GY67raNsozJUnI2eG7b5WnarJrmf5O4tMaTpi
5ADQXB2zzbZx8bN1Hw1weuapjl+fdnBp/sDY2ZC59RDAEq80wPN0xwztMj5m+ttHuoiC6xQTSwPI
rzCR1LAMx9pFje8PCks+uUIPNOO/9yqrkvrLu6ConYm0YUbapm5YpbeJATr7nzjslF8H87nLZc1z
5hEFB5Uxm2/nA3b2Kr/F68Sney2FCGpFugwsTSzQ2Uwz/s+vakoG0jb1cYgT8I0o9W8NyKMGRyZR
L6oMK8/DHUEDuNT4Tki5O9RaJgRavEw0LtOFFeNq2Wyik8Ekpu8tKhhLvGdnwU1yXWOWQSAELLpq
DooW/BZNSrCTtzoDcprwEe+ROUJLj+fNWRBiaLoggQ7kvxIypIrKiyLPDzaqQERglvWmEtgN8SRt
d5claMCpm+ux5XPpGlsoq9rDBxcVNiVdU5tB3LiGyjnAsAV7W/VVShx7gU+DK3TNdHGEQ8eCw2Da
T6NgMuFnfzrOffUoy75PvnVBY48v4FcZdH2e7AmZN9TlcqBv+QuugFSVmNRhYOYW+q1aP60zdof8
vs2B/E8TgGCMVO0pIeTVj/HJeCBN3qpMF/bBok9LVTnTxNrny+NmM5wY2615iIou/DhpcgVWZB4F
KWC30lj0JBNLMGMn2bK7fNUvZ8A6EEmbuYkT7f05Pshbm01aUO8zUVmGpBM6i2ic9u3XfEyfbw7+
xxtizvIqIhcS2nLc2hW4U0ElxK407Ksk0QnUfQwDXdcYQ0aPEW4oRdlUwiaXE+Frhsfymj4W4WVZ
qIphGCOtESOmzbOarh2Q4NaCrRlCFSsiLAxx8SHhc/UxJKNhASOWBgRHI84ZwJQ4fC83nu3oO+we
D2R2VD4wgCGjhw403Z7b06ojn4FvYVa+pgdeur38spyGHVB1G1Pmhwvo4nxJWWecwAEc8cWnfKei
y/0pydsOB8qSjl8/lXH1+6NyAw26zianCAkg0eUxsecwdNRi9X5Qd0tt8mGbJEsGxTvSyboLMOmQ
4HXMz0rF21lbafoW3UirYgsVo0FL4Foykk0QXHio/Y0cgMrWo2y+qYx/MveJGRp0AP+SgJfIJgpK
Xu5mmB9+jmmxqSPtmbFj1k6sfIDjwA+vRpyHiQp9vozXAG5OJnVupm10Ygw83kH9OAPRkz2UcPzF
Ac282Ae42P5k5TdBPEOADXzBJZF+5aV+UI83G4+QxdUiTH1qtrxzM9O7BJQ7T4cyJAf1nIbArsLP
xNijvcKRO6GvCP86eTVlgzYIlSGfhoKG2Wm6FUiNX2pIlHt7O6r5iYiFq353o8atLF+N7C60OPIX
0LXIzUiKX2RKsgG5iEqG7OBNLrepYUauW8h4ZRS5O6nDx9sLQ7VOD9fTllSAeD2fpQURGU/LxVLx
loFxIihh1pBeh6Ot8Q22iVAE8Y93ciOAMrOMZW/CNHucz728bWI0ekisYCZVNmgfPQhVXt2sfuaS
gO5Q2EuSvgaDYhqVoL/vSn1VtywNnyDBJqMJ3RuinrEF0Kl+pzERIkSDzWa71v22ppUfK4oMqW43
FHiVr9uawqdrh5ZNjL1A7A+a4q2F0d2SC/lXgUFrx35RrJFDTjMb//NrLdpc3qzIJHnrvLuVcEXS
H2aOdDN0nAiZk7hq0uzXP8ciJSWqhudAtb8lh8LK3Wy8X31utDOBOO4Xobc7MBbuI6FRX2haRxqG
Hn2gtPZBatF/H7rhvrr70Q/8rcV5tCoB7Biuv4U3Pc+acrTwApHpE/4P0C1t1gvOZmlNBxphq1Eq
bTzaqqzt/xYp0hEphIQRapt6PSNMrjNKVEs09d+4KacTDp39onz3U2iNfBBFUX/DnmhFmaTH05fD
YJub6XCMfPtAZZz9jr1DqPbsQpH+zVzZ6w5MpbwJWGoaCYVF95NFMGIsdaFfM8mErL0FQBwLgRkE
ucoze1wnGDTWcGJxEQNlUOyVOy7U1otWGlXKOSqNujcwXoKVTCSkkAmR50OzNJX4iOzNRRLLcVii
guB+E08L0TvxYQeAq3psTSU7yGqrVr4yMY95dBMj2AS3hcwJWHD+tsTjo7VlA2+wQofyZghchEMc
t3s0GWy2Sbdc6LwC0t0GLe9WpQ6Pg4QlrKTRMVpnoUXs2cNsc5TTsPd1WWEgy44M6NW06AByPiK8
NbjTlRZ7ylmZUJ+uUahdzHgfLp0VwXXj+M1/f2Y8Yq/0sHkcPv/RXhgVbdrL3iikI+SywdaLXenm
i4zpq1X1apOE/Uf7MtNacvsqMyRa5FZVmvu9C+ADKWU5vGhQ6J/YEynReFdTyX7vF4lxqX0BGMHb
IoJH7G0Ocs5FfTltx83sGkQn1LeD/t4IroTU4hx7KBEh3l+gqLv1+lDzj6AKPZGgRK4WHCHzLWst
i2zIYLtC/mxLr8qp+pFWmWz9kBZx85R3URv+14Y6u2jdHNGvyLucqzI58rV5Nnb+1AkfQK5Az5Di
LJD63u6m5zrLV+e9EzWSxIoG078IJqoIfGpZpgmkwvgQ72XXZZ8n81gY7ctouyuF6i4p4pR23epN
UTWbfSJadVz8xTQIo/XPeXHUXRHNJTdZ/QaImJWxj86EyxfZHwTQzlvCtZY3GPucGbkhkcFCAZkA
XwsHf8E8TNednjPDhF3+wBSgFuANqyLg2jmPTeIbghKiARsq+Cjzs8KcmQ45zLBIFGmp6ntZVe7C
eKgxaj1z1lgpRWME4/GXc1+3sHoADFjQgmphYz1dYRqicr319iwThKGn2n7LXNvyUCe8XpiXPxdW
OLTARt70ihyQXA2cYVNaMU/BzMmCcvLdwWUcKXbcHQ0neIx8/X9P1y/+2aKbXCETm9IIZ4hdlUmD
xUzdf4naa4J7m5DHSIjfq1miD1YN6XH2kB1PdBbb+KSEfLFnFPQ1JNnKjR/LXQfpFKBvkCMMW0yj
1O3wMUqCwj+Trqvu1jASu5+zwo45Bv1fmQ7Ht/f9s05eixLmfsuVdQTO9GLBgTj0lqpL/pdKrlr1
HF+U4uVYWygTFLFcK2buhYn29CuEflpjuh3pMuwCh2sTqiKI1hZytEeCldWsjxTgh3IeAlnxEzrt
NeOMVxUxA4Mwy4043XZzvtBEF+UNjnUSpFiYj+XUaQm4F2CDfZ+j7Y+HVAQMMieKSONlDEsQbfpS
XMpAjXZVITi9Ceofgwqvvsg7KPCpAR80CMSsxZX0RLI2sLP4TVYBGO982hWEbwWpn5glZxw7XyMi
qnSXKzE8S+wI9huGRa8j0ik2ykFVt/PhHTaxXmJnVY2Fo0py9G5ms9/tyF5PpsWJKYaAymJu0kHT
fYi3yQzq1DZWnpJtfrfWrq9L369qeXfPJ0NHgjSKq6iWEVNsfpO0Xd1hF50xS9GGX6hcUTUW/m6r
OA8oPHG18LA=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
