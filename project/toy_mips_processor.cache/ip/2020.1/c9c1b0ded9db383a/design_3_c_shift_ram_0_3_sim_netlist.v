// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4576)
`pragma protect data_block
gxLw9QrD4c8LSlUqjR9ZJf5D0BfKgQg76RMDk0DumgF04cLOUxlcH+JnhPQpeVSjaTQriu1UkU+u
BJ3KVx9S7lBCXS4WNlET24VMwkZmLt1lcJL9SNsAvHhe9VyM59Wwk+eENn65d8hCzQstMdyM/czK
+sJQ6F6SgpZD9VpBkSujeMuZ3Lck5vlO0Pn6QDzd/mzI82s0hmXzRRJJv0GOG11hI+E9LTKR7zMc
pIz8GXBgmrJtJd2vWCZCMY3cC9LC0I9j3FvMDporigdvWyRCwc6HX3uR4Ap3FLIbyeYvA7//OU3E
vh9uNYt0N7UhenrrxNAKf2UtoW4ykO9+sZu3iQfoQJFwmnq5axB3B+8a//XsCHEUdOIa2PWHh3Vn
H+rfWbD4TaVPZlSPtNEcOVjSbvThDTAvOsBCOUSbzxXckQqbuxYO/0RXCjqw2UHETO+BMm5KWIZr
cXVrqhzpLgIhM7xDRbIGUgZlyU2n6abX17pbS+3P9ODZnNmhTVhwfNxQCX/fXBWMd74fEJa7X5Xi
sa6c6VHgFX7n58KyMRax7oGBQTFkqIuQFumCkKuUGeVyAswmlSKuECBA+yyOBTK8uHSXqtI0PTAl
mUWvYs28lwIeOrHmnesPJDpax4NYbXzL5zu5mLAAMxBDqkC+Q0YccXmqVFchsI17go3P4UCyBv2E
BMN662t11UYlg+avmamY6cv+GKo194KhE+3vjauFy07zJydmI4RGTYZCcS8GtuF1t8MAwggpQIqY
uYLuN79hdiDbcP3tEoB21nxpyR5mfIrlDXh5PdOVodcB+vZCiqp0v38MP79hwuX2Aq+YHM92qHZv
tQEyP4Lq7WttM1g7YdYMt2UdHKBwnBxdrNOK9Lp1tKGPnu436cPyTrhmETBhd4zjQ2esURygfhYh
l27KCBnLwDFEkLNfSOgR9WFyZ4XEhMvH4A8roolq+nBMC/gjOoJnbMVR3RUDeKHkvsdO+UItWVup
5INIJQVund33jiDtx4h2MC9zNBOKlvz8r6iCEY0XqeI33yp9JnTT0Rt198uqWO0YE/r3WNxFEwUv
OBf2wepTygnmSkAwAQQAP8UDovvZL3ibbMEtCruBsYQY+xmV3ZK55Ue+C8vWtbSpclvpxG/vuFXN
a5uGDD8+k56EWF77Og47dalx9SiXlMzt48D0+UIby0eO6uY+Qk+PelqpVIQ5aGsXWmJ0i+fmqvp5
TQ+nAUKhXqGSAVdK442myCvcaxpK/12AdvZHEzXhl7PXdVQTBLLO/JPG1mZN6Kla2DPPk80qR+r+
dT7JJ2oSQX+GjA+aQ6GlusRaam6SWORDwHYpytXc3LZnsLLlQDQPXYp1zrn3+lrI9LEW5BubV/aT
zXoi6KIPe5etfHBZ82VlLsKHc75EMmOnGv/LsabQ/FzOCaJ5jsD6GMswevyLCQ1GBHqofbbUV4jT
vwXsAWe30ci6KJpzC4jTqTvWHvI6sx0sySvUtk+Y5alj26w1FtJWMk0rR+slrmN0rKFqxTBEfnTt
8xARNZCzx2XMoS/R7Xik7gWWqBjgkr1PHGVe3WKxSMUYe6af70UaRtnacKBM1o1KJk4hQtdNY80i
DdfhlLamf+kvTYdQgpk7TY7Bli/m9w+VGbfM/bhy3kK5dvfJZBBChbr3Hk0/pflrzBEbXHT/0cVl
N8XIoeBtL4OryZLiVs/+8wwjQV3oOK0x/Gx0WNoDyndnj3pDKKXzIdPbV+zolcQbRcvEX+FASnSP
zDvK/37Eo7g7cg4CtWMfbUoXbZlS+uYUK2SXIkvq6CT7pmOYsetsrgSgifiiDSYaGDQbhtExwQzg
0SvqVCcxDvEXpY+WF/D1Ha1QEl+XsacInYeyoyz84xXeaIh/awm/Y7Y3SgI7Fv+BM4ranaz/7xEw
Hx21151Tl0g2JY/AyheVoyqGkvh6UOR4a83ZzEkEcL98c212jLxRpZfNeiEGW7Wmd1CAVLlXO2Af
KiXk1VGDNG1VqSmWKckla8+k+lPt7T4eiOLISy8F7VatVQal0uoh1AqSCJG/feTTu3LoBtVVgq+3
V/3wul+5rwcSUMJXDqFDc6r72PuyrL7vsg/MEqosSnhCCnDju40CKZ2sw4AXBqAec1qk0F1SzcNd
sSjGkgSSMExRvU0H5FTUPJNFl/h70KMutz24crl0phB3j8zDMEcra/aJhChRtusoSrkuU3h1Nkqd
ThUxxZORYNdz0vIDl4upf0x5Z10O0UNculV9P1wL3t+pqVVHs0iomYOu5ysxeaHSRjkn/52lK1Ka
DFiDDzukFjMIHiOeD6sd0ndhvWQFOyjV1+Lk+Qe2TJGKfUSuofeOSO05SZrbpYbcpIa4YSpExl+K
9YCqZhLYMAirt8RQntZ+NacbRBbB5r7cqQfS52OGUXk9CFnxOXLX0X/LHgDEGCxyqR39GClFLhM3
SjTOqfPDOWhN85QJi/hG5FqBjo6elo3uU2FqcNRwwkMSgz4XlZ/1EqFzj8GHZlp89lMoJlDynFlF
vdTDA+rEG81YXY78B6Er6dv2HzCE8HBMV3dnabkfSJ9KNZsA1QwzF8x/kX/zbc6NxjuOFQU59ccR
jOHiYDcW1v9GF3XEwU7yNylcQKQ9fEz67NP0pr6LAb23AZ1pSOpu8uKgmVE2gljjC2KIqKBmnpJd
fQh/zg/3alvsJQXUTviUhShF37vwmdTouHw7i1CgabryVmvIGJMlhqU3bharp6lLTD3d+xLlQi//
LLXNT4EZJLQlBF5Nt/FdPdti7Mo8UjaR4oBcbA90Yp7sk9f54wvX2QWUYbsbL7LTasMLqb+w/wDL
UGKNFHwxmOzp0hvpaAulOV/j23L2YOMwEA2kvsLUBBlnZTXvEROxzDkxaDAAmLJ6sG8NlOKOEPXK
aeCj7yCESyS4kKAmQnlOzoK9czlDbn8ymdc7yeviMEdwgft+D8pMiG0KNSkn8wdww1d4fdNfzFnU
3MCRXP2QWkHUs7qoBk+zcQ0Q8ehJPs+t0s+GvJI6HuiwI326E8zFrf7EDmHA/gk/4pdn7GqZ0h0d
mr5Q47mkNlYyb4N0hBOiyi3AgO0GHjKnLzS41N15i6DPpfWnOdETBetU1Pokf3C9DnwXEqcV+li4
KedMshfMNv04SciOCT8/2/KmCwBGFknwDYkbE2Z/xIX+KLk0pvPyl6KP1EVm92SouaTRrwmHEgo4
VnYcX9FIBvp/LExlog5j/WQM7FFS3pa7fb0nsgusZXqSxE6s5/iK3I3/DhTPyXS7Thr4p4iGMipc
Xc98Unk4G1upjbcf6SjpQ1nJ/GcG/RhZ7KAP9kEETiXcNCewKDwXRnQBMKewVEAZGqrWmxYDv9OD
ytM1UvdnAf50AoFPzZtGzxRCTKG/Vq7KtBLuRvjQ4yTM+XItbmvunRYC/bWCXBqfYaXJ4Sn78+gB
CrmL9ZSfHtWgATCZZu8E29vUibKBX1Rx/vX6tQN1peq5ZQQM8ExCNMJ0GMqa3+036AuTixQxWT/F
74dNjhhbSpYSS6bLdvMio6Qx0zQgs07yptW6zVg2C/1pw2BH3wVAugdpAGgjk9QZjLA/LEGUb5er
P9u9VawlxDCCvkJsUaaM28SE+jZ1WMsV5rOWyvsAwMFviWyAG48FzMnIC+kxmTotLv1FqTuUXC5W
wGdkZ3BzkWyQApr6pizQfMEb1oqSwSzUe2V6943ND0krFTSQYAwwXZyE4CJhegUqXS+qjzNd++yJ
gm57EdJuggSTA/tVcAnSvFYGh9n3VqAVBB2G02JVQviEGKFJKlNxfF9ssryYjHvpYhWJcgOVDOIF
c9S18lPm3jySn/Y8dlEHA8WIkktGf9GZTbt68Mr8nLtf/zdThMXY/kU5ConRjZUHdwdf0FsLe9Iz
zTvJSLDWJ876eybYjRbqjc1HJW7WrHizI4aIausfb5unDOhrA5LnILfoPhfC0EIgSrhbE0U/LBEM
AXYfuzyqUtWIL5+uYbbigsNLpjiaxu+b5llDh5daM2D5ZfrCiD0uYly4ORrhipzOaDxlCg4N2GII
YYy3jNx07pLp31dlNJIkugnOCI6zjafFcdTzqFQxHeS4LNSTwc23RehmO8cyXcgTr0EYouuchnkV
y4A4iBJC7wMqApEpOGLVdvMr2e53vcWm3j/r9Ncn+4X7a9qgP8l3DbAcS12dsaigUaOcOY/UBaVx
Z6idFRsN5qx5+Iowrl4eY0cEswyMvBoUrO5FUOkf4f6jBvCp8azuLome7M745R4tWfE+sN+9MuMc
baluHOCZXA8+2vRdlX3duFwKYMt2YWyJtDtiYmbe3rx593REUTkvGlg4cZHt68NA1cNTb+gV16Wp
zwYjEOhXxdyqXTj6VeTjpbfgmT0Dyhd7xeQk4s/2tdydfyA62CTBVrPC1Ft3CP1Es16s7YkQ1t0F
2zUwfohnFOBpaB5FXmhJrziMO6v/UguCaMwrqGTbxeLYORgCx+/mCkgaZI40BJtbXcOHlTh/ETZu
PylpOSG7tNVzV31EIOSTVTJBlL767KY9VrmN/5YkY/mZyx9VO2HfXI1x5uq5hMymPO8G+Y/NxIsC
98837aRMPhmq3O+uPlYTBfpoc/V9oOuP9T16okH35vHj4cCqJ46GyFaPWBlgnrxepC6YEV5B4chH
krqjnOQ9aDZi0qvuG11BiGYFjyn2BUGmfU+OXDkYz2uBp7icXiEFqm34Bq4r6wEThjwO2LzpPvYv
+SaljjeuTuXFmuJAcomw/gn+hMZDg+cZjARjBEAzMJC2jf90/rSIL2GBiUGZ6/D5cuvWFb7+5Vwj
lFie9jdZZq29POsbTBMpGOWmI7SGd3nhAc8Cnb8e+qWylgSdMc0Gs1vrOi3YyJN+CNC7VDOGdmcR
NoEFg2TlIl7BVMMDQWU6q+R+wSfvKV/jb9gy2InsuM2Z1EWnDw1oyd3HSex6qjxnoLkIcj5sbGAL
AlU249jz57YwW2VWdAIgRfKCxkVakQpbble8IyFkmqC23AYOGyrrj9S4BCPgAdUNN7NOgPn/658L
Av52xpQFGQjN99WIfzPI6pWu/2ycx/537XbPO20IeQ3s1tX+hiQ+PebOefjvc7Sf4BocAFwLFw16
HrHKndUkLUfqSWJ+yq3eCC+jNa7OTzCJVCyQYlPATsxv5StHYpiTX52tau78Y+HiVHwRyWEeeiFQ
I1r+sMXp5GWhTscZ/biMSHr1PeIemj7sZLlBd9RiKRnwu3QCcmWLtoS8tmqcqWap9hHRyx5RRFJn
bMo9nXaaFQ/xOc926A6SKPiuHwVFCwfPCrcB3HNvnrk8VvGUW4qfGy6TXs4wu1AK3V0kv7scRyv8
ytr+zYsK/m9KdPpSrjjaOyTXr+11h606KJrElHvMGd0GzaDc/QdGTvUEU3OlpngFdJtnGObmf3r8
4Z8YQVFBA6qyAFlqQhxHKoM5xWXeDjO/OrHxAN5ldXd52k7QnQ8UwNYudYKJx5q62BE7WaytXDOH
aM6bYspoDTmDtybRbN7hgT61g97r9a45bKa2f15RG8dmK0360eqYhYpN2x68gnnMTxINDdP5yWkg
bEOFDJpxC+xGwct2Ln5IEg0yRqhnxOh8sik72ATJezNdLAwG1O/gxpaKn+1Ri9wCghXxC+bJieZD
0d+R832KSfVZaz03zqEcAjfJmmanl+KzaVt/MUb3HYTIC6QR0VcrcAVomUA/xShWXXD5E3CDPQTE
kM+ZNV3e7SjASYG75quMfK4LmuK1b+TGJT2TA1+C6FAWrT5zq9TasXWrJ8zhw13BszezUwab9H9l
0Si8tptBJw9+K6I+VVOHLeMCYJSwI42rWaMyJEbhQTk+1aN2n2KFkzpGsBQGKesZc0NRLo0BcI/v
k9TmFG2LGmSFib4P56lGKixfmYUZ10H/5TEh2erK8tS1bfyNHsJqibQFsfOknIfyaAGQwlSvGuVy
ccBKvDMus6U9xxZsrbCU/hm0R2VC7LZllGWPDHu61u0xnkGgTcHRuplrmS+4eUq0cKTbaOCQqS5u
gmcgYD0r6EKWOZXXRkO/TFJgBTm6cdQ12OZGds33SnwBDSTVTrjECSJzHz3jMMzOSE77M7eWVUPt
Qp/PfJOYMpmmJkNAQULZOg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
