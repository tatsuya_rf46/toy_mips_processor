-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 14400)
`protect data_block
hfv/cF2HFPuO919hCSBInDznaRZUyaDGPAoHfpEnEu9YWsm0Z8iZhAXd3/kkaHZAlNe8mYV0k06S
JGDm8ecdisUoov2ReVDZniCjvBC2+mqOYpy1G/FKrKcRJIE4Lo2inPgTUv1WmP7rVtS5IIb4x42c
r+JmOTsZ93iue94Dw+fPL1iMyuKBxf2/k82ZWsirl/XG/xkQ0iUJCUyRq3RobhqfkrGav5IP4h/c
84ge+NgiWCrjL652rkgfx90J4nH+xlYlQ7qNoJ5baxOxgkaUtvnmegX5GXUHoAIzPMSv8PuBGv7Z
3ZwrkfYnKzZRPzjh7B13cPaSTsn0REn0DiyHeN2EmptJNag4j+Hl3DziDOhKMo7cBZQJGu5cn/lO
tjXu9AjsQrsmSkauCs/BHf8NiNsF7Wjf3A5Se0qWDOe8kZUOHmQLL/8dpakeMMX4/qNDAHj9qyjW
l73c3LIgwyCPdDLkFtmy3t/t9OakW8tsOuVQR4YaVpyo9sR8wsNqTBEucr+lzLujJbiVPeunSZnc
OdC7wNrvwEFS9wtIAdiSX1k1wreN0yTSLMh0VDRo/slLZ0NB/QUcbQzYBSVRLDgjBe+bPRlM0zbi
xB4DbYz9mjf0RHDPZzouMEMaLnt4cPucPRWrQbR5E4eK/TDYY3TLAK+pgyycRtXCwlATWOgyUuTk
KYIlxMiZq3iEKCLHIETEkJKWsjEjxZGQDlixMuBiBJtWNKZfJILiikh/pPjjKEk4sJmHmMKT/PB9
AniEV3MuU4CB3gRnji0idvsoPOlz++oZMC6yvFDkfXBNkOaA8KDbLmghlkl/P+i7KDYPDhA/fB2J
ZW/AxakwV2yYM4Fhctg3RoPEMOxTYE6dTlRpq/lbUU84lwQ5Spyeh+EdGvpQUC5LlgYWS7PhJi+S
1lJHOX7iN8CihC+mVwMIZS9xE88x7SaqhCjzY74L62uJl5CuRU3flAU96O5RKLH7155ILwxJbUop
ZBYIJ8SV5UZR6gJq7SBgjo6xlDLusr2+dHdhWS497FA9AA/TbQExr3CdTOAS48Yf6am4cqLes4UA
PUGB1SpUizQZLg6OG4nuhsRDH1groWhZL5GDB4SFaWIKSkWORwA95HWRPTugAwnxDTj0mVHtqiNh
vojI1shIQ0pZ/SzuTX+CL6MpZOiT8G8plZIg6hLnli/4a1IUgciAQNLYBpWzDbg879Aa3AKfXswT
tR+cRiEEt5Dnnbcyt3CByHetx5mqEN9RR8Vrj9n8rIbTOaeVJEEsZw6BDh6YJc/X5gy7O7X2y4UY
yKt2lFhZNMz1zcVcbumlXyZ49cnKkt63VE3O7XGdfytIc6/5zxPjjz77FXnfQL+y970QOCEka6pd
1l5ZKclkK5diaOiYEbkCOVe15O5S+fbwSFMcmg56ccov7fdXhdqp/k9IXsbbLSTV8Pb1lG2LnQS6
IwNC2j+NZsMdiMivt5ZmP+KJO7mmxPMRkfW4jyfaJYdOyj7kYrpVXtmbo/jH9dKFt41/w3xT8tcW
CuZxPO9fl4U9WdBrX9S9lpl8Tcb9BDBeAXK8ajI5BerhLr9EMpmRDkIADOevWuTgs6Pw8mNVyHiB
LhfYfMsX0wGoLUWx5iE93TinB/Gl/nvQOCI+9dZOqhlwH6fPFsoah1llukfmKUaItQ8sQE4TvDLH
ftqhM9YhLraSHKlVyIu7YYRw1zCksUApm6Zm7dqIutvhPaSjYwkN9Sa45iNXofAH3/g6TZYFyhsS
3DPz5Q+91bEHETJXqJ9jFodG/EHc3c35JfKyqqnqh6UtF3g7tHhW16lEY+muJcS/5+svBfQA2I9k
6PYJdQMO9D8oJlF7Das03QFpHKALGoAR+yx3YxcCCZwfQnL3jOpT4OSkL3zYl28XzBh3WKmBXS5x
GUg98H36ClAWg0FOqwAt6L69HWFn7t+CQPtovnz3lvtld5LMTG+SbRbdUy3abhdnkKep0FTnWohp
QO9WQRuz9HVHnAJYwwloJYzjeEfRbKnsiBIb02wbRxlx5PXUtTtpcNc8SEgoTOdZUj6A6+WRaCL5
z/sLN4V0gazBcvDXCzKWYww4ephQdIaKYLCzsMCfBQtzf1SD+1T6LglrUzWnhKLbOUAZDK0PQJNA
7KUwM+rrwrZHeN6LChp+iIa0WsHGIvso/CG/5oFnterZmrkVHtYquLQpz2h24lXHToVTS26hsa8H
SxD3D8n4ac8sbzu4Mpq0CB/tCMFDjwrlcVczSRHAQTRDQN9ZUxu7/YJlYmfFi0TVTY6Qcp27rEMR
/a553BNXLEqQZF9t/hX0+EH/I08ETVmnLgNFiz8ofagbxUk45cLUCk4uN9uQawA7F+HSFvmeVKuJ
AcwkoGaKQGbdYwgfhJsr/zHYV0rnwT19XCmH38W3qI7U+ZwCXRXIsQ3YC9AXI8p85lKWpWRGXHsG
uJSeTRDc14DQJqLA1cEXOinSfUs6UWBjgb1UL1PNz+XBdazZJv+Z0TIJdG3r0JjUMNeqCbRa948H
6oIarYVynnhzzkEo6plnnuAIE+03NoeKCKeE+LTG9mzC7XZtflce7tRpVyIMPfF0Z8XjTZKz3t9c
bcYsWIIB/SiTaD1IgmsapN9xoasxTO1vBgCCWHUzWh9dGk297mqQJVTgV5AAm0ytHDPkmOIpL9ab
iJYk7P1L0DEnq82mV82eVx2yEL5DcF9NwRjIXsL6QjO5fjW40+WWVKzSa8137/XhfZcShviqZYJH
exdxr6yf449Q+jqcf5psRDbOtxNP+GZER5M8VlRzh4xunBSMvOKGFguEmaORg+IW6y7OBUc4VKzB
UTA/Bdoet83QQ/zBT5u1jLxif0WaWD90nSGNb4pB2eyR4VO5+QBtTOckOeXqdHemSeSEAxz9kgZL
+6iDsUNQjTwKKic/c6N3l9yTEXrJhU/dh2+seJhRHq7YiaPmnaXO/NUdRLUk2djzaJdAPb2nERI4
WGwOf6y6D1yjAUUD/5zS38IR3BxrF681zZPsR+I1fEKN2UBxWaqihq/74GdTUy7Xe0S5fKh9BFy4
AW4Sms8lIQQRRdxGB6j3MLlKU+HoId5pGlRJOcxOo5qCS8y6uSaELn1460JvIRimqk8Bn1I86Se5
cBnQxf2x5brD5fp3q1Vk7Yma2zQ2F3/azXKF7ItDA97uaspAffiGkQt2jfBWStjT+u2tLHadqbIn
RzxkUo8eXzXBZAFyyO5cDQGoXnkoZYKJJLf6LkzbeGy9RCl62z5ZXXxjPzxYW3gKHYRAiNng0LtW
ibt9q/FeFoyZgCDHgjJAeYb77ZxTXrYLKKgY1iK2hOjAETUzd/7MdkMF8tUPeRLqHvgnIJjf8xVh
bh2ZlbA78gIfnZp9xwRCxfoBUB677ZwtMupQzYpV0QuAcb3mfIXxODJOnRIMl4pwfaT3fhjFW5w9
ek2QFvddUqVcHBK6TNSBfNtaPblivmnf6A19P/2Dx3ZlGaVOV5cOJ60NxSwSMIb85Nt27NGGUtSe
7peVbWpk+VkGyqpuKGw7Pxw86mouh1xuEdW4YFckeglZ/n4bkPG3l+PLLy4MIbkjBWW83O4lDYBN
PwcrK44wqgKiaZ++xkMKDjPsygUxihjTl0wG0fEAdVn1furprBqyrvlql4JSHHe9jnB1k2RSPbJv
DI6x4IIgHJ7TC7mO7eUbsSy6EEgk6qXNle9S8YKhVWwrM+C4e/TpI9SeAnKjdwlmjk6ite6f8jVY
bYEOORPjKerwlHBqThkDd7nIsnRA6E0qC/Qw0TNCAY4CqabEFjufMKlo39mk0JFPTER9qmFvc+DB
rwNVymYNrkJrSwoyBiSQHhZ17vxsrM4cB7y6aGMy/TDrha/IetHwLMswdFhFbFstE6Gla9LSNb/u
3kgUEMf1XhPqXwRwqGJUV84vInjIqze2tOopZcwW/yu19GXnQjuV8SEej5QNPZqohSSBeEyFwkIV
mLE9MSKnGRDUpxyakmbuT368GB8iGvvTAM3AXIaB0n9epj1I6dWCoEaH1ur8CodOASH9vwQFUe5/
VfWMVSq+ug6LAEDdn0lUDuW0+FQo5T9EVig15cB02e1afj+sb/NHcfyIzfBmBpVyyxurD0p5X1HH
jn5GNMNuy+pRYygRVTCL13joIdK1v0U3DflnOhBkOrnx5GW7XHBHTVFpBuQhpUIGzByBeedSmuxi
QzhYmWynr5sDN7oa4LdTiz2rXlfbyYuEal3oj2+d/msRapoqHe3qSzdNXaf0mRw6FvcCYZvUhziM
YdUmEXTUFQnau8Ft2LAcv+5T+EVEuqEvzCXYFrPioxudd2nJuoqZwYa5iyiKuqCCUsU1mZWrkDCX
p02pQcA6lW02RHTFvq7qUZ5cEM1i40GJC9D9ro6UvLmu5l6nJWlACpWnYA0dfGvBWDHFl3sukliS
DPzRbDCck/rFxjCrS0EZ89uVMoGdWiEiJGYzabArwBm63Anu9LOcEmQNFgfzXeBLcxs6x79kxHuc
kQ55V9bjdl2rtfiegKrE5pGgO+AUOh9g0Hod9nnc1SdNxTfDdUgnhkLGU4eDZel4pRkTk4v9O9Do
zyj0pvkNY5A+ZlqhhODwZkl80GEY/ygs90II/PJ7ae/OvkRkma+noZBYsUsFsL5wbMlLWoHcTwd4
yqedYmLWTyLJ5sKjx3uL0biNsb73POzDZNPaUCgv9WHcIBMStTG8yBpBUlqqvOFQQRJaH5+gbqMg
gxoXGJJrcKjFFbIMAxTl2+XYCv51/S9QqLlG6xDcIsolSWeAk3XLp2RbE3zRGz28ffwhre3apEl9
dRz9myGXv7NCcj8zYZqVN0oVXK49va7yCcBk4qpy1rEliNJ1gTI1KHbvSmXdyH1TlOI2VXdmHc78
IcBe1HeW6W0tkLEG60LksLh1SZf8+jcprz5T5o8AzyO2EbA4dtj3HfKFb+XTPaDjnce3sxXuH/qg
El9w+2Ly8a39lK75SbkeSzT4iIEKtLvUOvKJHdXol9xKcIlx3oyeW20zXbBRbvHY8T6Xj0rY1x0O
JphpuoR/xegj4N9M+tNR/kFQ69yrFJGm8NjnDkPIaxPxLOlxYSLZ4qKpNR63aZOupGStiYNRR3h/
UFpqaIrsSCppMbIzIzJdZYsiMKb9+pQphPrOvMsQK34+UZMaJizIWRHWuJGgGfCBztpoMUHIu7p6
iV9yeSVrRrcKKdOU3BiMdiYdl+Sz0eR4VYJdnDV2csmNSmxe8R10RIDNgqRpkWeoOlocWQnkWLbo
QgCLkVUK2tSmfP2xukUA7QbNXxmcYjT/NfTa3u8Az0gKTKVbW2uybX+XCtSJZRLbdiY8JYZqE9yp
fgejwpVafS8E24JP7SqF1ta/xIRyATZRXQWX9n65j8sA+WsnvqjagsQj8slTzBldkd0Kzr4+TQ2Y
L/Dmwh0c4XFlvUr3w2S1Ejrf/U+0+RwG21Q0YRPbWBMirswH3D1B8coJ9qeWg8/IfbcMzzgLRL9m
nCBuykUOK77o8vIXccmMFsjtjWCD4iahPchM6agJwb9C5D1/weMnZixpqi7FsRmrq1io66aFMmFD
dfs7G+OlMK0Cgo0b95PUFhgtT0QzwZHBvfWVEtUOl9uGuEgjs3hC190e901b8x+DVsG51Xl7WSCP
NErF6yiZ5Wqyt7Mkg11HtLf3UYXvgjA8rIuidjzT48o+zuAt72EaysEG7Ljg1z7kOtal4gjjVCR/
JZr5Nidv7eFYZsFjlzLpZHsOtxqHo1ACnuK7IP0KMoqWor9oPoGRg0PZTDQVOoWOaN2X2xa3YoKb
dvK67ghweGqu1tAt8Kh98PVAkYZhDDShO+Cv3GwwCdjlZhNfCp46Rebrk3Himg3SCNSgbS+jW++T
G9F8NTU/gk7wc8sD3wm9gCB1yGEySwap/UEaEdsv11VlhiPvAclGWV9Tg5ju4QG+sSa4y0eOOixh
GDTwB88kfM8RltJYhj6ubN1TBkn5a0/XUJnGDhPNgl5sBLf6EEc4Z88x3sZt8gILKivBlHEw6nto
uTc82qKUTXywOorao8D2d5pqpXaObU/dTdR8RIIdwSMdcK59OuYdXxGn57XJ6kEDTcs72lbr1W8J
W5EsJZv1s3itgaBmcGmnR/MYNolcDtKJod3ET4FXw4FMs40LP+4ZNBgrLhBCgnkq3EezDoFZpHZZ
LFYyL0le5GjAV9xT8+Cn4QZ07z9mKYd8c4tLSwuK5ikxcu3a01JqQdvWBlh9tgJu/y3z2thWVXc/
boasZJjSO5R3ZalUP9M5KQalc7K2w6lH5qZpZobz5Q2j2LSJF1syaqULiEnEhVs0pvN4qI8Cfltz
mRr/P6TmEp2C3rdGtJ7oebAcTBYuouK/x/vafQx6tzQis4FxkS9ZqQGUzGCrU6gen3OmLIAXWevc
IUjgM9ap5FDhl6hQwLnX3hwzvCn4Kl9euNKfHWufOp2Q62HUoJqz80thGoxCK9IXtWEUTxQpZcef
24MKBX0rfRKtRKzQBCrLVnP/jDTdZDtbIVLQe7iZy0vzaKWkgeMlZfh+NTNyfihNOLAVgblfcyGf
QlU99SVD8ukrAoqOrCSW2po5fuGpaB6HvJE58T/wsRr7GgseFQ5SmD2o4IO3DVbZ5Rbi2XnpHqaf
yNf3xyT4E+1FI9NITfnmOsZM4/qSwwUYylkIeOKSoIDUXGllY9A81WOj6UAN6hXKMZcLfNkT/M17
rshhoKck31DddOoQzBKUHxhemFmYxYJtn6blsJqBiSB/3V+tM13IicrV6TKfiIlvb8Xuxm0FK4tK
dQyjxHKyYIJEgBfNUn48LZ+bMba7KhNYhmrZOGqP9lFYSer43rpVA+4Jvmc0f7zE3+NV8d3s8AJE
70E7DMoMwqYyrnZjNXFy6gK3QZOv2beWB4OZrx38sIMpDYaz1eXtbxspE40VseqKiMg2OO4GGipI
XQw7cAkUMeoRQR9UJyIH84/mPY9qXK/bo6FqJZA16otkbda23/4i1ay6MKPOb5lkCm7wVe4o+gmP
CJ7s+Hn6Y1LBYrEroTtGEz2H4aMUvTmfFn1ODIJ+YkUlEBE+yJSk/VwtLsyWGIG2T8AP05edN9a3
KgNIQ6Y2i5ROrFj55CPcw4W2XdI36HPJfrGbYKhIuws4/2p7PJWTw1IYVWcLqhAKdJPCJijps422
NUb2M8K/D3nwT7Yk4dDRkKC3j2ETyqEvqbo1URAGvKlxN9v+zK4Mc+3ZW3GFmO+u9u6g32eQL7Zd
1InseBPCgdnM4uMHu+tY0papi6nLwkesW2of93GJfA6tI3k3NHlavkHxCtryPsxDqHxMf3a21i/R
xNaaPFgc1muip25+A8O/2x2ZDdj3PZhcDtDpjVoGSbUF1HBtoHky1qqQGh5Y9dcWf8UIDLtg4wqx
iY5Lef5w8HWZwIK2HAoBLs43L0XWK8xexWZKYKlq+eHtoIzFcBqeJbRshYc+JeV1Lv6HgKbhWRNc
bQl21MPThNm+WhgsXANskecpoZgZR+bHmsI5J9MlQqzF1/83N5/dzjBQdyJd/29gQjLFz0wqTVq4
eb9a2eufeSf8V5RTsCVDx+3bv0d/lIEODuWYRzTxnd5A2tvKd3RmbGVgjCzr8tp3UjiKshD4/xKo
BElvrAKp4z72PWGTZGflbe77fIpENCItMF8oQF7W50jxcQDo2PPaWj+cleVKOsLzV5ZRen1jONgu
P/E3O68RKH15Aue6b+exQWF/HZADYcCipdIdsWqXgktvmpfuywfXbEo7By9ylhmjCopLoZpnPnZ6
5wLLJIXw9z1o6JC9JHtfG3VldQ3R8gEkH+vaCUUZmAKg+84cvmC5lk8f1DDkB+MMfidHHIkZWHE2
iT7nfz24O7yo8ScWj2EmEdHMKDq26JH7fU6twigsAfWTs70xSGkqyPxA1vCN2MpI8cTId/4F2Htf
TXe0q4kHnc4szdVd1J0/W/x1Bxk4ftghFs348690r8p2CyBrHrOyhwso2+g8SBBNBA/Y1RO7S2Qp
FSWTlfwL/UeRZFlgx6oripTBpjnHWSmr+pQNoFtRBrSkOCmRLXcoBuPPBzPYGBx4i5snJDrt5h45
zPBbEGDY6n4Adqzpwk3MZPi8fXl+1moxFmUWdM6/28RXW1qSzX2jTBnRoSE+tMVEtsV78MuMs4xM
d5XDojnTOYbQY6Nvyb0DQW9pZfbcKX3yCe6i3AG2NLcHp8HvYzRNc6w8s43Xuo1g6Woie2nKYj57
Hhxb9lpinIzU8gaCs1xkmI4H0tFCVkO6regajRw7bjY1H6IzplmG5TGHv+evUSVZ8IX4siektM6q
HypLm5EmAPnB/+Mgf/BR/+2i58J2XLvDc0JuzYqfxYdWjFE36ixAkZ9Zt2dWfQPLlKUPbqtdmuxc
56c9hHn85f5iFcglI/hzpUtKtMt0yKt07+/iSsH1RFJsRR8LKSosZY5hObb03jwW/QgjJKba7Vj4
suUA+OvZULn9DAyky7YZ4LZ/+7Vyi+ma6owzN3EtgC1mdemqYEOuLMU2YGqvbqM7ZysoSLuZ2kTS
ZcQdaEN6+Mq69Tn2SrmCc5ZiqezrTfEe81fQbJBxKcJf25neHrFV5xWNO8WSpvKF8ZRZNYpVBErG
roKB9EWfOKKq8lPwqYB8SvecKTzuyeiIka/ChnPmMGfFQmuHtErRPrICV9TUFrxmXtl6sBovFq6B
9ih4baO6GEEr7WUdhKkHD9idpg9qMYPkzLcHKgoovWnUqdHNPxea8ZnrxDhsBDyDQP33tB+DH1Fk
tZl9lyJ0cPn8bozIDiN5IkMG61iHu4PvYoAU1xS4LDfz5A3k54AyQbnXYysnBk4crW61UzodGOda
/txipaklSVJo8K79ITeucIBVL7ilNOXoMTS7w9r+8j03OqCCnMn+wco/KwFXEKgUR9KsHeonZ08d
oVCAALgy7bJpkM6r2a1HM36T6E0EJ8CvPmD3tHpstIrmO+KjrdGNcW7yOdq2CeQrpsHOdhRPiof2
qsMccUKBJcMyDk2xOOWhmZ0VLnXMJP0bKDZ4HB82+VgxowXmXEWvWaevpZSb4lm0Mb/axcw8ckbL
T93spTR3bO05DGOJcTLbi5h5tBGj9H8p7ebSplNAMQUqg1HkPRexPQf20ue0Lo8KC3d1QNTuK1Ku
596ELvo1bZECgV7YvweX/y0RV5gM+OtYPwGjK8639G/HhcWDai/uank6i4L6HCjseW788uB+iFMP
u+UWSn3C59LeXA3OGD1YZbDzjCawfAVqzxMWSamIc/DbuRCLB4vPKL+SfQ5vaZ2RhkpSRByMvFqI
lhxyap3NycaJ6DEBHvGHNKR5sAJzBNg6TlsMf5UNoMkRaLBnK0GHc1n4JnCRypsEN4lkbWTGHeJ3
uBNcYHq3LI3zZEegDJcTPt0RvecUSw1/pD/2NQ31AWYk9SOdQjD8eUHDQnS45XrUeTwT4+ZYpQ4L
kkcurouSevCpcmQ6HHaOiQtnLylMeAdaXTLkFvRejnQP0HvrO9D8QlJBslMx1bRC1fulbbKJ4GqI
qaXgX7WNaLBwK+yJp+J71mVDt0Wo4Vrb6mPdMqr8kNvd1eTkM1qcIYmWPe3LaBTp/Qn+ztAvCjUR
DKwnBTppYg7yjgBxshEHdFzdJ5TGO7MT49KIsRSUbtPWSqjQDLqCnAFA3BOryX3quvJV7+NrjsCc
KPw8GQQehmZJlGwLPeeVAj2o4oZwO5kVzzsmsKdSWypbXEckE6e/p9pFGtAPE9KgCTzZaZuJXAGt
XoAp3jI9QKr4XpKqnItcIQ27/MEL+IJutQQ1xeUAauLgb6Lir9BpT2R/qtcADq+gmM6+X2OpVtKz
UCILe2vOixdbmlEd/Qhf9si3MGovinPf7yOQm4HJ3dCjKEq/ivULtjd2qlhzuFGKnPcwjpiV9RTK
uceh9Wur0Owb/WBHAXq/CRipYIqSHuYSuhswdInVL7ekGIw2A3yywMNxV+XzpiFRTrXTkBvT+shF
H+KqxmH9xlY1ebmhzOOGtgOFQQTZnZHYsxbECxK3UMiYut5yZEV1BsqoTd7qVVw01PdGtIQE/GaB
xPwbQ4mDgweg36CFln1V3p/Uhxhs6Y6+Gems+p0tHCY9pIol1bSYbivZtU2hR2iC7axFK22hI6nH
bqmqEVUpDjjz3wq94hzFIP99p21f3Lk73cT/9PqJn406ssbP5oUwwQ00qza3ZQxoM9vRuTPv01+I
6/hLJKlklzD6k55lzdrDFTib29zPfPISPfVUa3MohCHM/EM5UtuXsZAJvmP7POSo/MjW40GbyU5D
M/8AhaSODX/ppjE4RlTHDDR78vrtmEopb2MtA9ZKSAvT1vN+wMEvdgY9JkyDTUf08L/RkdDbw3B9
zIr9MwBMG/cQpuUGLd5A1VvHC25TWzSXDlDvT8utzWFEIdAbskoT/f3sOA8hpQ1J/VnqxYwtpv/+
KBo1QvSNWXZDh943co5Tx346UY2UdIL4f7VKCmsZfiI3Pq3R5dfFxk1hSvlJTbJL186b2LNEM9lk
AuO5e9MpcBr+NhS2JQwpowsSjxh5YsaaJgFKJP2OtIHDBXK4y0zK5TNrEkdinr8C4IIKE+OxOQxU
/6v7C1xluyxiI2KMbszRYAXPuZEeFrCc3E1IQxtikvQku9jdqy3XRXE8NVgljuN2WImTHoDIFEkb
yQQNhAS0HTkFtZ0ATai7TvlJKNy7G7dF7ZcsM3ezDMfzwFr6ysXrqLj3iedAzFQb71vKRzZ5gN5Q
AeXjwwArfQ8aDSR9hJxX6ANJSsGl7EDBuGgcV8OWZWV03cYDWSd3tU5oXH3j94XeRLwYMp7qkM/y
5uwjJmFE5nFSnSaKRublAXS99xUYQCJyGgFNdbjkIGgaEotZKEtKDLHTTXqlkCQxsbMrSEo8PgWC
KQwVkrTixbtiD8qnSn+FZdYOAAxD+HnirHrnxngbCHoN1/5Xe2ZrLrAVJRuF588s5Fh6AZA+gMHT
qAtwroEh4H09cVsC8ZRKwzBlDs5/G/nQP1q+QCeftqf0LBRUcLXdBigkeILDwEnurATFvtPZagBh
aC0/XpHBxtbtFQOIlflfXG9bDunL+6fZnetCUUOcIS037sO6OiDKju0NrfTRaXVusVA6h/B5ozXb
baVyKWk1pv3PM2X+YSaK/hleGwIZTCOw8i8AJ6QCqm/sZ2dZGLOAbE3W46mAF0ehY2kSIaui4TiR
kPoz+Nms9jprSXpJ5huF8tRSoDl6D0UBFTA0C+aC94FHigi9zTK9/1JVzP8JMV1fU5w/1OyMzOOa
iwQbaqJf9kfvyIHc8KHZ3abX1godVTXwur0HG2L0cwNd32cq3dmvffNA0Qp4+cy4GFI9IyXIHH2g
zp34lfn1pwFoBpMvQ3w7r1uLcUb+KVtpqqZ+NNn8iYCCjJpri3seS53n4C26WaKh003iFTq2QHgt
x+SDxF0+P2JbENiVJQtCjV1OEDPwOBTomvdiJ3xYA59FZ8oJVj25mdFr7JMBayNiHM63h6HNapRb
Gn5feV5ugoBM6WNIOvIJhxabA3XZOHfh7rinVMUkbAIFJgMqoijpSqvjsX/9erZ4AHPedpMHgcEv
UP425N1CvnTbynk+XG9APKW01tRcJFok/rh71/INEMKFA8bE2vhPp3ASsFpcwymtjEhmrGjW9TQu
hR+Jy5pMo7d3LHhPUMhIKKkvudw48xarjMrNKFhkzhOf+M7isgdYoMgQfuW7DkA14LqPJTqCFS8U
E71pvkSSMS9aZ/lPVZB17Zyp6i17qQuRuX678GhKMVVeMdyoHAMV3AOhdx9vPx7yOPfPFq8+58XP
41IBrdd5BvDjWPV5ldWaHuS9MmE0s6b0mG5wPmeIv3wiVsidKRhc96SvGEiuukKSFp1SkHis3Ylz
l72lHGOwqPf9szJWJ929+aaW7pbqMJNgYIJa1B6QWoM9qvPhY6hXFYK5O4tYBWBOPBHR0sKI0jRB
+wslBHZHlWBfLKINiE+objdtS/32JOPjN4gvkFYjvyixbazU2Pl21VEB7xbm7syje10S1++tdwna
jMIwMRkGpwRARsh3HM8lCgvwcqYIL7bD4om8oq/MyXz0iU3hxZIfZ3TZwDWLcLlWBDtO1doTzaNn
i03incyeQ7MKKx+7gkd/Dp20LE4sh/f2TRNNWERzgKTz6x8btG9CCGh+g8HW9jr4zq6+hG7lTFjY
ItP2KPfzyahaZAYn5V8ZLITwbcrWx4weMzPM7wwYZQQydQOZh1CdqEgFldm6eWz945CsTqkYIpjZ
hPsDVRWRkryDYJJD8nOpX1d5cCoRnvOiiY5pJHxmpDKlPTBFvY3HgxChi1Hgf2/psTnSij6/LCIn
OGWun3vILsQBS/RyjtUPvAhtvN1Lvg2U+XA/v1Z5yZYm1wlJozuhwGs4HpLf+UvBtqB4T5zDebTK
TP7rkbVJWUWM9fadUzAkz4gNr1VEjX2rbzn+L+R+wI18oGp6YPFp9fxJgl2Dy+UV1/hw34l7f69B
Lx9hUvy/jVFVbwi3dVDolPkwPt0B+sakXkr9tfyjDF1fO/huVcjL3UmUKZ/Y+Fyx18h7/nQTl6hY
I+E8pxdvn6s5Cuz11b7ugkaiBB7jmTpDFS20zAZegJA3GKmfxAWxV8ACGJ/Fhfua4eogYLlc/o9R
HoUagpodORNrf9XEjmf5Z7Cbpbrgesd9Us/0spwRuI5i9kjeAOs+hFXFffHiLsjCg11ZIVgEoRJQ
/KhlxKpqRvDEGOm+YjHlpOwmoYnD1AJad/Q5k7KyPezVrbt6VSMgGW7otTXuCdLRgLXdSv/kC0al
5myAKRHz4WC8PG4qmPkTZMKHMMUZ0KPY7GiS5n/OudbC5+5gg3mfZLZJ+MQ4DKl+Iu0RB7eExqYX
YOnikgCREpV9AZtDFqFIXXgG8vO3nPEcG3lnJtL0wcKbXRUWQEFYoS+ax8+lUwPXySinx07KmReW
/cal9pVwJqV6K8l6dcPojhKMPmpEwFplB7kZbWJnxutAce/LxuF1W8cgEjR97FOkyyt702NmUdv1
gtAhNXQl4tsO71UJQtKxMVFtJBsunVCKnPEwIEQw1Y3Fk8IoqoT6bJgVbKa9GY6NT5H2sNzOs0lW
v+cS4vIm6DMwuDShSf3QK6jRXpurhtalCz1KOOtptboaT5HSbTzbM4yXd0cOuXUe7q9BjMYLgLOE
piyWqfd7VgGwXMZv7UU7YBdP+TyDMv53ihmUP7wmTTFvatUquvalDjpJk+dMV6Hw+dIANwXNIEkf
P2eowrBIGJGdOS2J4D41W830dLMsw5FfcKoFHuIAbvaBwl5iXMdPwmjncG0Gy3hKtNQGQwljmM1B
O61B6oM1QeU4srSAgFtetqjin4xtMhEakPmcJjSe5ub3VW8VNKhsjG2xrBpGdr/qJPUnvve+s16Z
plEVVGTbl0XSPAkJxBGPWPIBLoRmm280q49o1SVQk6X3GNiox7+5adbbhrFcFUM0Esb1LIikD61x
LnmG1B+UrrGaE5uzkMxCtY5sTt/R/TiYehTJQWHNdWM2rMKEFXhadB1pi4pEoQHbrWcYXijG3YfB
WQ7RT9QzF2GkQxP8GtoIvzN7cGj5vc6kPJ9OA7NGpppY0M6nGvgqpUdUxMLvF/Uz/J1UcxrWAUoZ
h1OPPNJm4ou3DU6RJFLalyVmZnIM7/0vgAAYbny3I+cQCchy3x1A0O1lynpsqpVrwUsqno87EAjD
mTTn6OpeidQ+CIWiCO9QZ2bZXYmuQV35lvQLgwFHFu68L1to3KUuKKQPUrtshM6sGgqXYc2ddTXg
OqO69lJj0v6lriF1uRQCq20/UJDpXw4dEgvnOWDc9w//19vV2sv3Ir3lP6EoQhuiNYxACWIyQvNb
uOX0nb3nbvNw+VwqEGjvGenfHThNvPOEQPTCaJVAUnIczM5tMWn7vnmNAzlffCimCAy3Q6W3GDyl
JuL+SpCI0TeldCrLw2FwGHuB9ACn5qjYLdqG+UHvMTYTZ169zuPz4zA0N0JkQpbQp4MXARY2y2VS
sFRyaOaURxHtTCk8cF08gjyciIr+AD7rUH8cIYGHc4n/tqKv0tcwWTawPdHy1bjnzOB90ALJK7NH
aHB5KpnEsjAKkJ7I5PzTSWgR3z2Dj16LaqgnL5Ggj2LkDpuuuzJfxEosyTo9FxQBm4JjEg6mcAku
ai8ndPsLTM38IWmkifawm4SiYdAq0cdW/FsHMNMYa0+sbtXNP5s2rmJOsShtQWisQe+bvF4w2UMf
pM0EtcGO/1bdfnTEs15L52oV1BkrI+becuMwCErfph4J28sF182t4PFaLXmolnMjDJoHpbLiBnjF
mr1BM87SjM5gCO1Dxd3artXQcRTE3wkcgQEElChcDqiJJ0kA5O7U3zSdxk/tLeDd0ZMx4eLga2CU
Q+X28t8PKRL/IiNCnBuFaaETWTqNWGiQ/qXVJOFUpQYx0iAhTHUo8Pi7qv4IFkbnSfDwagL5w6fU
NOs85E0IMoleMPzZWr0I44H9XITHor8eoN5SBlfz9/U2oCchiWgt2BW1hrNR2TVmoMQH7M7z79ON
IIS+waRd5VsI1RvCpMKvS8li78h55PfrCMZY+LjiO3uU59uCGkqkocLhSk4gVpRFzZbxfvrKZ+Ty
WpcLPgZj94qz85yk+bwlfpTpZGeCxL3RYlBzqdeqG1wRsFR5B2BjM/4UI60uvu3LzMrfzMLx5kQy
Ub+yeYayU8jd9MRJV0xR6Q1WRE8/ug3c72CMyn432Ue2RN///BGGoG8gepnZ9woTIDsh0FuYauk7
tyZRlfClsIB6pWaxp9sb/McfI7ZSYkX8ixNiTYUn41SSYjUjST4jG9HUd28OdZFMygZkISh7AQos
qeho3P4bA9BuemA6VVzTivH//XnVHL2wuz3pT9HeGqQkrvDPJAdc1iHZFm2qh6ohYTk83UPjEHr/
MgPvA+ogi2y9hfOFvLBPlbuspEqn656Lojdi3W3eYhbEe3l4x3kV2ESDIty9ZcMLO+b3cWU5tJpG
4bytuPQ/TaKpTGfpMhn2nv78kPmu+8ose72TWhCQB9al9eHwijwLpg+UsDlJ7dKfEzQAuvBwOtCl
iF7tkZepxaIHqFTrM0cNsreqSsCWQo/UVRkC/WVxJYHRTMXqCEreqO8zYoOqQCd2E3TuwH0CEAC/
T3O9ogk2K4wTZI3/lMDksTBpIyIfbLLLcx1eh/6/nyzO/oVCiKAVeqKQyTLdYUM17BRelwJEzVqH
waTMTEz8wHw3arVupVydkb4t/gSFHX6RU432RewqWkKbBmhxHeiERraqYE4S/hJUTXEQunkNBBS8
XnTqg4lgiSFKVLfT8HKJ4VESDw33T0LbezT8QD8eieOpZHMJLLvjCxBjH7jzXZePE9KNC2HV0RgT
XxCVH057/8cWpvDjKtzxNJ9aeZXJ3nfuXB+gwvD0Yoh6dAhsxok7586CoZWOl5FwicxXCL2tA9sl
mPDjUUYERNgf7cTXhDX03cOmiy+7N9CW657RGteuaxreHGzHgP/mZI6OVogvHvniKT3I7ZK9DQKB
35nVi5gBAsh19Qxg1X0cWToCUbnZ3i3A6D+PqOEGHTMZGe6/oOFkyR05lJ6QLibBlgb5uj2TTG6Z
/r72FUNbplyZJQwAp0Cakiyt9CCu0LFIRGkODigLkOBF1jjxe3BfRtVZ9GBfgYxaleROfZ8AR8my
YWw3j0K6zDhU8JFiZ8kxywFrjPnQKdVrHQsg24Hew+XJ5eUAikE7dl0CIUA1WQesz/r6C6AZ+n3S
ZbfCu4OQ4cVEMXdN2DdZcKaoVbPyFCHk6CCYMzwb9PCAPkyXGePGYPWaXjE+5/xJrzcJRiEw9r5J
4BvhFjHkKqUOjlKgKlPtI67vtONO/eMR3S1GLxWlaXVmkxxfzHPy7AR9cZ5LHvJjc/v0Btg1x1tY
mQdMpHf0NYB/MrjOXsx+Xyn1/3RZpPuYfHIZFIoAX8/L0hvN/k815znY69vVeeDam0LBq+8d8sDH
gDPOlSuw07e6hc8ewhyDpTgjlZBTB5C5a9/AgMtam5eHBixcAB1pB8ZBOck13jpRSGaKy2emN/pm
e81A+eqdMGPDhBXjrxbXTj2TskOeBlQODt139f31gh0T48+jpJRaw0/xD26x+YhTzyCzbROwFZ4l
PPO9QHaPmFiV720nhahMeJurlUagr6GAxqOwRPudVfkeGsHYUqPj8U58E9wDxyv917OBTfqU734H
aZOWoQ3ZvNmJyJGw/tCBxEylcj21nkNzierW5HYv31jk+OpdOwTHJSiHQbCxwnq69+fGcx5wTEs4
Wh8sSyxJeKqKhdAGxp+3CU+I0DXuyTOrDvWuxDg2D2fw26+uqg2WFL7sN2vf9nmPL19YUqvA9c3w
0k8P5Cw00P5dG06Nwvx/+tMWdAY1Ie/2kqYzZaph+NaZ+7UT5awCm8QOCFRYTpQNzqoz5WLxqXdJ
3RFHIkQIZWekuZD4ndjzCZxUsxkLqfSRWF+Ouv4uJyF9sP1TgXxN3TE1bZug7LI3sSxGWGts7wtW
oak8nwhHk+X5SMwU/a1dRZL3HoEgkHmb56ba7owlarIU661p5VkeQHHAxg/JyilwpF5NFBRFLMma
s5/GS1U5a+ANtR0nf5Rz9+IGs73M9fjZoei4vYvVJygr+tZK5DNCPsAPX2h3fgeD4oNi/VgVndTN
xdytwsRNyeJ823ZLcE2y7rPLm+aG+2HocurzAN21u7xAz6Jt31Eyaxm4YJqdzFRyZuTkfiFywu4d
6iSkIlCCQWmtz5O4X08UEpy7CcYyfhLomDukOrCJn+Zb0MRB3xQVLMGNJgOIVTAdzrdOxC/YbbRz
chqV0PA5J+TZws5UAzIw8I9cERVjmgHWTZm5HfDMtBoWFTXFyEIMwKnmjDhEpjjAIOXEHYNtOVuk
FarOTlk2by7PY+jNGEmYYFmR3gBIEewcRe+UmPJm0ZNtIAUzW5uPgZKwgRWDxEFxx4cujyPoC2Mk
Pryv4Ykz7i+OiWw1QoF21vYkuEIQjwwrl6VPDYr141+VcnXDPutca9MJPZuP5RS6MctEW47u4lit
gPtMNa7PGz/Zbr+Cz4lH3Mz7pkMFYzMoRWhKj6KZuqjvsdUTah0iqrlsNP0DTBfiXi9ptbihgtE4
IiztPwpEs4NldnPHVa9h6ry8Qb6qSYLd5LDAlddXFx7/56t3600CPzxLqcI3b2ONYFtlPXCN0wTg
Px65wqPCDjFLVidJK/7CAZQ7lw9272SocaaWUaRxQP7L84gQ4M7OOdmjz+qIC25Qlt5eBgioFx9t
glUKs24CIGvwi6hElHImtyeLeBozX6xPxoDHF/qP+OjnYZ75bPPEtAP75xwTIgjWIU7/NRu3ph7x
/Ch98ybQTn2r1fOFbLh2RIIv1wwanltiJf14wrNVffKLXdpBg7/4eWhssnoxQX2EgMDW+CUeKnNG
vOs1bbXP9I1u/SYRFgTg6vr63mQOO6lph/Ys3lFW5OGT84r5SCYvUxsiRKBvgPIa7rIy8XtHnwKt
P2ebXVCouJ5wozuZSCTKNodd8mdwwbi0NLfzpeGr/Bmq4/3o++BUnxdUnxRMUHwFk772A4/wdimZ
8cycur+k7TEkkrRye6CHlOkz6qYUapkX4H/676uyt9YmTfGl+/1Kop6ZR8SVPakyBUy2lzp/wGJu
jlwN1nk/fSA9byyzUESWLOcKQACX6F1fd0gQJQhQ4vRA5YVvEgRhyL9lHgN7G5jFvODCXGENXHgy
5gYYoULkU5QeHp/nQl2ucC+YXAIwYB8ZJ8U41r+rY/3tOq2Dl7LyWUsG9EZM2HnHJHINhtPSW6sU
zfuO8jo6SqiLSuSZxLIcbEblmsKwe8p/kNI4EyAiJHAM3Kfu2Yed7IYgJuWONgNKrSAcQDb+OD/v
XYZsOXDDYgjxKT9ODRnUMTrZc1ZjRNyWfVD8e/DrV9/uNXLzzbogr9JvF2M+g7SZMf5pTHxrkPyl
e077ntkP6v2uuGpqzoxBaOmKJtl6L+Uk4DuHkE2KVvlkj69Nr9BXCvrgssz2lg/7WW3hNw3H4Amu
7uIb9KPj1dKorywz11/8h0clvjIwq7KUettBtUqEp3hcrRP/q8i7t1Q0eDw+7C9crIngk2QF0Lyi
e5ol9x/hAUqN7afa0Gh5Fjvj5ms982IagajmifLLH2zqvLAa11YrNE1uQ1BBkQfkZBscBc7jX3D5
3sV1/bL+GiAr8RW6Uh1HtK8GSqSO5eZup8QyD9xgZ1KetWa0NFcqMicOYAQNNACQiJKyIqFz1Ydu
/D3WdSN+4u6g08zzVrTS9UmupP0qCgLPXmco2DU3UjVpoxydSrIkCDLZ/fLM6/80Yff5J7fWTVcT
CwAmYxa961LCk6oYK/mpBjagRvGxNVD/XAibIRi5trwx5c8HciZdYosXC5kPYmT+Y/i3o8O9ZqvV
D/67X/6Q/N15+qPSDb0qM0DBXljQsn0/gHGJbFl2H+9/jI0ZFicmqjW9gfZf7DxNoRYLG8yK2rcy
EQzoYho7emvHRQ580saKu2vGwlVCjuBMT/fjYrdVI+znJGNzfzcwI45Ywwh7Y80ESQ0rAg0utXNq
4/aVc5dsmnutrslHv4x1UZ5yuGLpfmAYt3kTuHsp56WCK/0xHpO2voeB6+8DYdw1QtYAQDfZ3iQ3
NDSlxm2VRW4+cwQVyPmogKQhN8uahrCgeTQZwc1bwAbf1asbEL+SxIQ4umTOXOAClI9KKIkSx+En
40Q/lCXhmViIIPgqT+FoRzomnDukt8bfF2SAARnVJhEENqTFIUoU1RbK5jko5GUOMpttG2t+F39S
f34fAF62AhaNVOB+fkbCYFWHtoJ49IKm7f/dBMBuxjKUxoGAdjPYFX9B4L0EUhB/QVCeZkY5syE5
ko7AAjoKfUGiC8iqhioElRYpFZ0NT7Uf0QjBKEhj0iIlFuWrXnCzVBqoGNgy8hLIIQf9NLMr8cNS
icIBZF9VL7UbpzBj/HDWSSVrQtL/XfpueXUBiJH/QkkPShlfrsWO/D+zWPe7dV+29jnO6js5Ahbx
DsGl5zQl7XkisVYt6Vm4PmPHi3wsJvRR7k7OPXWaU18IejTRxaYlELlo5A2gsYebXuYYzf7JgGLE
4HadarsW4hpqjG08inM4y1ZDLuI187iZetIEpaa3z8KzyRfpom5bPBuiUaJqTVC8lpWMsnpSvw4j
JKi4/sCUEEC7Py4booMlOt25fcwbxIEKLtQvCHbDExrRs3fW
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
