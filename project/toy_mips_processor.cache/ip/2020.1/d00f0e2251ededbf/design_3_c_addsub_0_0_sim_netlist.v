// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:20 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_addsub_0_0_sim_netlist.v
// Design      : design_3_c_addsub_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_addsub_0_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "1" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000100" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "1" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000100" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "0" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "1" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000100" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Q3x0rY5yGWC8HuhEpWIlg3gNoUlEi4xNkebt6wApgwOBKgj37i8XgF0YbJQkKXgwJ3fD+XVy+9aO
F7sMKIUxZX2IUP48bDOTryk6dRvF6kdAxz5tujZasKCr8pugubV8T7AcC8LE7FLLm80djrTiYfTC
2ytNLxLUt/YH40J2SsAyFAgLKaZ4r3N4vV8E1SPi6LAH8p9JXSV2DBrvQ7x4WvQGojIUfgZapdHc
tQWd5tDQ0PA40OtyZestcuUEr7iGY3jYeL2ETQlYPwjEK3ZM4Up7FaruGzzMb4xb0ztSdeGn6jeA
7cnObyQUIECeRkSS6gCiXqcRVHfEwhBLXjnBRA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
msoKKHRA6P64g9sXciGzCBtO+Ir+U8FxEO6qMA6sPitXEdwVcFNg8R/RcLjT0nkP5444PhKBviCr
VO8Mam+to2zMAppAZkUQJyCE030IgUnmLhvG+FqvkaLvdU6JtbpMSt+EXe/UXF+RQqh7eIcH1Xxr
Hp+cgB+VXjnTTY6DycmbfQsYd8uUPy+r/obChs66lNOFyo4IvXf2IyZ5hlIv7V3iY4dvyPh0lo/X
o1jSykqFztvpeVdPyFeAYtaOnH41pGK2sSzsfZY7t1BFSSNjKWIvv3GDikSSpZJTEQ/ghHxF0cW6
En93vQnZngjqkGQsFa4e+jsj7p3l3bB+DMLGYA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 6640)
`pragma protect data_block
cBpNjRiKm4T3TdHTJI4ZKDLlbZ2moncJl1lDEZXZaDLoShFcRZDZXRkKNk8U7tZAHI1yufxnAg9L
fiU2oaLRFkxKdb75DclktA9YCvkpRORYL4oxzrhs/CMf7d2wT+RP09QORJG1SFrsBtyWm92FHgEM
J8zqXCplQK6WWrfeCiNWu2+Ss0m+6UBHEY2fZkV2KgiW832MEJSXmmfVPBwqrVtc7YuucPiLcklZ
j0I+8GpnCvwdlcuxRx5PI6L5OydYfbEvonW4sqi0Ilnxtr6YGetw4oszf1edaNAQdyH3JKICnaD1
blY5gDAZBDsrJQ5hn9QvzaMz32wgZ3M6nPy46TCdjqGdke6bKk3Uexbw57UADmih86VB12jW1lIE
gc8R+IWiIikiPBWcGPak0oorSglrSqml4C4AWq5MnlyT47TMj9QAhA7BhaNy1h4eybMkOaBCe2Xm
Vh0LLibQY1PX+JsHZQDXjhIzQhlkGO6adw6bt5siVn7ZhTKH7uiaagJ0i8QCSKfRy/fxHWmjKqEa
44SuBhyWQl+djzhxeYjueYFWSxBRRGJiF6kQQlDFFpWxr11+zMashVWCyl4fZlKaPSRrT4+WCMIL
oYlLjSI3ddOwPtUmfiW/H9MGS/ftOtyFRiIF6OdIJMfUZD2QSVhiSvb5K4edDJENxmZDvYXRkYie
0OMmC29L5NCG5FPbrRysZ4I6GYUmz2yvPsMomKBP1Ro1E30Eh/70j9aGSK9Pp7GGawWNmprZ3uMu
euQb1eF0q9azpBeWuRqnEGsmn8MSFvAUrnXTE9b3EKXdvpVgrE0jUtrhZglI5gq8G0D0c6rrRIxl
rmkxWcEIlQ6rknTWhoRs7MuUJ+oU0fCHMwmK2d7FBOu7eE/QDRWP7x4Nd6MiIBcwC7B5igRCJZUx
y+XQnfjyC3sF1Y42Ky3BVseVxuKp3DTpGTwKmDugkSswetT2+I2lu1M9NRD8c9ljtaeDjqMIU/+Y
cLj6/H2icx3IsP7EZHazVaffMbI0XfGzMJvAmfii/QJryh+NEJpmQ/IETvqHQqmDzhw4dXQs1w6Y
2MtQLHVbDkevuhrJSYhMQbGMwLQZ505uylOSmBsgvUGV8RizzYdEDuHgGbaiS96T/ILo4kik/Gm5
TMMvO1Uy4VOJSykK/Pou44uDpQKhlQ+3edskGkWCZ0chChhRowTt0UBMmuKp0IhuiDStiIYU2JWK
bZGkp5x/QV24fW2ZglF3PMtoZ1EZwPd9lHeE60qbvLcfLC6Em3xKIgcAuk0fGn8SHT/6x/veGf1+
PT92t++O0g2BfypIaRLivtzlsGvKd5xUQoTnTf2sEAxtfja4RDaNjr8xebEbAL50Asl7VJcmGzJH
eq1pS6I09lFvaC94hlnS6kYPps3dhOj5Om0ZTqW7qgEze3Sow0ljwF42zuE5f2WSWfWuRCOK2Iam
mYmBAczxFOVq7dZiMefp1E1oRUAXtewkQYpaAhrtUW6084Ttl+lkYz/Jai/8W8VNSAx1lT6/oKgb
pHO916riy0h9BZn73upUU+Jd12pkwHATFXXnTcKtJgHDDv8jlRP5YJVJw0nlLklVgWd13lGUlXxp
zd+RO9FnPDVScoaXoamWaTkIDGmx8KslK+1g4UUr7EFzfaM946tLLiBtCilqAHINwn2v/lDaiFDs
nmI8QHR48+aeTmtIEu/uCpFGg6ZLZOHb4CkXSveZjfxM3yX3W7eHi4Ua/3DfJtgThM7gm0lii4ka
EWYjahLAS3IHbtYYMvFBkbUgFdxLhZe4hTAp/3H5TZxLjjzIok1/dt0tskZnYMzFYgelb58LK0aj
XTpz74PGYpDy4gwAQlQ+7gP62+BuOost4nHUjmY8MDiOatd0igIcFzUUcYhDoIVZgV4Ss8k86Z5H
3SbsBaTe5Vz0X3su42uhhBPcNYXCI12KEMsixvvEsfryZ2r3ArvlM4zdkUEDELDQMipfJoBDQMtS
S2cBP2GFovukeRLLJUc+KRA5rH/Ir361Eyd0KaW70fq47T4Sv3ArfOSgNO6bpAOuJPdnl06iVOjL
W7Pz3FXqPdfxB0Lm3KZF3yi5v1A/aeGDHthv/f9g6pyKYyMRKixeW9wcARQj/PyzLLsu/a3RDBsF
Z6urBoaCvGzLg2lUeM8xcnYJM82+MN+l1rkJ6HR7N9dxNDe1iZ6WgSEASDAOwEFs6LI1LHMJ7+CM
+KvXsoxFANwLB26OpHe3cu65+lq5a3nghqKTSYb2BDyva9/dvL8zgDnxXYOj+f57rtqRujaxLiEo
rTnRy0hfUOm9ynL2DJ+iDM3xLMktrdGVGvQoo+fRJuXq5Qjc6taX6wSl9UIkFJtafkGYVx0XEvEV
ye+2dricJhSNYBze9vjFYvEZhrvlPVBbp/3R0C7DqezxT7h+tv1nh04R0vM6xBzxrKAf2KU06Vy5
HBF6Gz5edxpdkyfSxHjTUE6AtLldQkm/5tAT17pjfAFy4Hj8eAyEvgfp1LQZFIdwtYhXiZGMYGJr
Bguk2faHRTAFCK98GEIquZYjQ4q4B3N5X4EAcOVtCFWdqxIQC4kWYhireOimMJ3CAr8CN5bqgY2x
sHzuNfwqe/p7FgAvqNzoMlY/qqX5GAky4LrSQJKPbhb841CjNG0wVfokci9+4dMsJim0viTM/6Go
/LYKY0PS/2ymcbXbXNPGLGas2BtqxFIxL5cU2+I+jTIqg2qE+Uyt6ZK3A0f2g2AukXGnIotpQYXa
t/R3AZWNFkv9ogxHtmtvCm/UfOgM84gjXgwemjZql/aAA5noS+k++n1IeTF6f/1JrJYbur/w7H/Z
ga8r5NpHf1QOBhF7ZhR1SPtwrco6V2a1sZzzkiBph8iQRj4ej6MV4wVltluw5GVPNzt/EFJsvg+K
43i/2P7nU1N87eECfDcNeZ7LUKbonMlGzD9Xdrup9QaZc+NH3rQGl9FXQtNpWS5oOZGvxyeKXVDO
iELphSYjb2eRc7ehTnWoKbr4Fx4Pby31CYEB9l28Y+98NtcC2EqCPJYxT94wr2d1js/ncwaHpLrw
jDaJWBv2s6HO08NC3tBjUWmlpgEB59wv5jNLIDJIVA6Ymwh7iB2COG/fEjGoRD3PHAhjEXva7Ahv
9D9wFcix0SRBOexBEFc3R71Ehatw5DF0jw7a0W1UGR9lPxlybGrLxzgAvs4j3fRXIr82NeZdQefb
NzMI3HYQRFoJUU+wlVACD0ROnKmIWSg9kqVwfBqWpcOeN83PDlN+PDlyCOCb3AGjnmDg4sdkje6D
9d1DNJ9ZmmuQTDRj6N5EJtNaMZkDulykoRBq+PNBij7WcXBRiU9cTls4GXIyx6dtqE0+x88WIUHX
4TanWX73KoUFsSDblVlgJ1lBjMpkbE6bdqFKsAUQ1RFvCZCVuEe78E/vtkzVeQkUsZcDvN4u9oyz
DRQmoaN5ksZ5K88WU+yR/39PjPVJHjl/pcLKmY4P511G/16zclCZFn0iHGMFEODqWyrWOXon5tNF
wK6DxZZG2QQXLToabDBQiX39XT9vaMrYIwGWkBquuJu2QAtiMR8ypNl7Bgi1qVIWbE5ntUj4GqWn
egT6Yv2hEFmUyOc7HWzso37xpvmGdQZeNLBbly7c1Kj4KGjF7Hq9A+cO+B98UgApRyyJXzd6DJc7
8IyM+XuzDgJUgbhdOkpoSm9frcd3uC/iTpBVERlXYM7B8ZX8CrspSPBpjFx3LGhbKtJcw3kr/IQK
34pIZ/jl2Z6vK3NuyhfLRibHGBTBUVLwa5r6af+nmj0CldkTsYQYt6zgJpKVQ9SMNW8NLDoJKkyc
I5RT194J/QoSRY+YBFq8vdaekOCFrd5+ugR5+o1sOgmfgJTkSttY3M7tko7eAA6aDYfV8hia9c+I
Augsx8WPzlLIsLDgdL8qzLoL/52cE0VQc7ZlCyj1zqo6goo/zRV9ZANSNejJBJce/jWCfartG8d2
u7PUW8tnifH3TG7HcOmU7YNH80JZjg7BRNZO1YU/YZXaQ82cShFrxdS0O1BK676YRWhaZm1gAOjB
CuZC0v6S545eAssOEWvJ6oWgk2bClbZOW2gLl4TQMXliB1Wnr7p6h9vruwMBF2OpART8d+gkQSVf
Q5e8tEPVdBp0gRbMWq3bO1FJNoItYoy/PxHeX82QLr4J77K5Bzd406lNZtcRxyw9nSYtitpCV1s9
I9ChcgYvVp01/oc10VBRDrlnM84n5PJuePpv+sVnuPDnIROZ63tOn9MC14rkYqwUKHsy1GngSxa8
no5vJNVGR1nhu0Z5/kNKWQ9dkQ02NYzvCj7Ac9WSgHeI5hAhNYmTSpNO/E5C5uVMDkNZSmVM8P9U
nIhSfZv9iIfvwBUvhl0MbrKvJBxzu8ML6ueNSWBSBsAcFu1gjkQ1WSa6a+ysybTiNLcvL2VvNMhV
Jd0Fh6MJ8rAIjsBbdcX95tN25dRw9vm2xl/nvViUSqyYeD+Dcd4w6PMNPZ/pD5Yf47Ltnwc1aYFj
lzxBWcfk45aNUjC5SJ2eYyn67gTSYiHFLBHBBZnUZ3THH/kA/6FYdjOJ4Wz7kW3Lef0w6nTHBimr
431ll6x+DlmzENAqudulJN9KQrOrjWpeFj9FaWHxs+TAko39AW+U4n7RcLgps1BWKCBi8AcnWVV4
Tiicaf95rIExlAdHB/ZI+F+BHBbhYMgDcoxWaRHt8ukK3xHh+K78RL17+pnhhOURhn+UtID8JZJs
6/yZx3V9fcNU09W1mQ/mfxeNjf7ELlIuzCcW9fg1QdevQUsWzGYZxoaceCKclJvhoh38AqF7MpD4
gaSP/cutAAFQEybH5d4P9FFENKqhzkJSk/v4i3DT+dsKn5pPGEO4RgKQxXQTIErknoGjRm6WcnIe
o/35p731hzjBPJ3kSCvpwwuRdWtsWUcOCpy5j/VQkOWfLBAsZMg7MARY7rlm7xuyNCxXAPCtSYgv
hxJS1PtOW8SmyDLPq7gdWdriwyIklKIUUkDeyvJpcZru5wmOCEU9VHDUOPqakAPRGciwm5tTFwew
Zfxku1XH0i177SwDJsmXk37h3vULKiMXrAhBzQ6rvLsodfBK8fbhO6QAtnrPA5qqPCnSBgQF/7Bb
LKNicOF1vLezN8Oyxfg7JoXnq4ZsxdshHWxVFZhKe9icSDKjgnaQqar8AlNKEa+MZuXqX6IoRSPP
n+OaCweDpw1AZwrNIMHgCAoqdfRELuJ2C784lT5REyzezc89y8XH/4ZoeBwto46EVf8uEHJFSrUA
UpxyD/Avn2ZKogdP3YuLGFgYGik5aO+CjUyTHEQemr04zAjv1Ic2hoXsoWaN0tjmSSWMXYvFWDks
/jDGa9bBaPDxCqg71rCnkTM2xiXV+UKuw3sv6kAYc/KRbeBng+t7uGS6aMoF/mUQuwl7qrYJm17i
nH8OHNKMWL6x6DPT6taHPl8z+9Mqls+YVe0c5i+cmwBLEN+LQs5K+4Y2J+2EXgiPI5elGjFTf+A1
BegnSyPkaz0aSCQCh6uk1U5zoeK8ABfqfRaRSA2vBK5p4KMwz39dj4+BFvDOiZtVhZOLucSrRzvr
kFZYRc1dcqn1s+8aZxD2uazKS6Vhvsd4eP7tpTr1hRtxe05isNexP+FlXSbwt36XbUOXgB/Fs3k8
zQWXkTrTp6Np4TX+GCRos3+OYKLgNavQUmyIIEzHU9bQQC/guSjKxBEEYMAhnOfy7qDwVAgmUdal
/YAC1GIDtZLsHqQvkJKSN20xwaYQExNwskwSK9Vdq9PkQdSeMMGoKYV7UVJtg7b89tKH0GMR7ukx
Yc1YED3o5EEWdj9umASBUoWdAdU95OXnrfQ50NhnQ2yLLvsy2sSz+ZveWWqaZqnXQzaGjNyOFmjf
cSjDzXkiDvCtCzZy+nJQYYMfuJWF6dswVXX3FDdHSSiHbq2VB5ooChk1UZ80SttmyZ/CSmIrM2Vk
5mQIKClAAGHdUhIrCPfSViZ4OVG2T9RsBRNDAh7yBw5A3QdMETBZziBWUaED9fhtFqOi5W+59ix/
94mheN8h2d+/OoM+uzNmwmxMf2zMCGN5f1jxapABUh684PEKTLEzLE9S4QRhoUR5FaY8Q6UjZGPM
RCp1wNk7n/Z1KBIZEhMDylWDQ9321/W54vvLOcy3sg2bI/dSREkAOdjj+GpvKrvWsJ0Gckz2qmJY
6nFoKlzZgmy92BfOUAFrI+w1bh8UJzUDmVyvXE02K2NboGvhg+uAw/3F21LTr+9t5bNOJyOn3Wjb
FVmn2LNZm+JvO+7AmuIWqiioCVCIXsgGMDg0e9Cx0JjvzDHpeE4HZjCuLgOYZg9RjezV307kfidO
H90lVW5cTqagSMfggPO0XsU8ZRx9pzq/X/YmIqWxIdXu7iSCmWJol6lkpkDEheMa8KVgPBAyOyUz
lXDHOXDnHT6EEEXtlqA5sGiBPZSRSsPHNblRfUvsS3CKzfWIuCW34LQ1b0aJ/Qeq/1WVAN0+JRWq
ykCm4TPAl4cdkohifAH9TCOuN8kebdf6ehA/O8bhJ6fzRGYcg+gZrlSntp75dDqMe/FV1wQux8N7
U5P3LX+KvLXOIbueYUk1zcvbgpKje/MYzk7ow6OZJSyVxk1HsnlLxj209vOOEi8jPb07+qRLkRLG
RoB3pX0rFcj4Q4oOhSBsmeG9a3RbJ8sRiDwhTdcvwClpbwmnoIfijiCM80D6qkesOcldZeFGbq9y
ohsNVlidC7MQrnD8uBKJm0Lek6IZVlbXpACNNmzPpSzQs4yie80k18rQAMafqE5rsrW6zTyz2cTW
PCsatRrydsVNjLpqS/0lD4+wz0Tj8vUEiV2oZgf8IaSF31wgBagEl3uN/IUaINhiHqTceXKL3qoJ
fM6iI/dZCzOMR9yxOM9n0+Vr8Xcjxh9A5z1vHtHvqPdc96ld7rRMsDlK3on6TFHiaQZ91Q+W1Dul
q90Ng8WXhjVTclouEb2GbZQy5EnozbR+QJOmUiEp+OwJS/t8luVAJDatZt2R842qvQrP3BrosvFv
vZ39lcURsTUASoaosna9M9Fc9bDh8ICH5T2OF9OHTTdAHUP6kEdBWJvOy1h8y0EML8ufcDZWRLng
nGP/lSUvKg6+TPJmmXjXxtu3erXWxRI/k0u+/jRpe7BXyF1ASSnQGo2nXLwlAO9RoU+Ugpky0TKa
r0pLGD4NMwOGFdXevCxDww7dKJojqGxUOuO7Oxr0Lxw1QP0Scy6xDOx++KoYJX2f0zR/znxXa6Oa
RWnakySojEktucwZCUxW4yfFZPAHNNAXWhIueU1sUXxyIrffbvW+CRVeCijUjzAHTxZIWv5hwho1
B9S4/T8gVcgMMzhhVISVfz8WbJG63VpBAkFbWixTM5jCq9vWkOtLFZDfm9kW/HIIo+FxEz3ybbcI
WEdzurz2WnbXuzUk5wm3qdD7A2f48uxGalnolZw+CAy+l6VzGyivMaN9sMAhmf318aN3JADJdhen
IOwPlLt50mFnLNejgAsM8dIrNGehSk6qQmo843OyTrONJf+Hze7TGpBza1ciGH3FxAfYIp1Yp3Ds
2o3mCVNiE69ovFate/i3Kc5NIvUHMtxNW2slyA/tUPfpGuTcSGWMvFH+oxe4PfuqzLqCYP4wHDNi
Hi4ulz8/cPk6IIkUYrWLIPbpOZKsmx13hgNPHl2I4KFwkp22LgunycUtMV7gb5ba+XlWXMdPoywC
aKMWuBwzm7a/gkVqxnsSxV8nH7ZkOJYJxrxuo1Nc9whl2sWnqK+NSmlw2NJiWsC7efwABYrP2hj8
W7/Dg1BsS6K4PfRYLw8dtayWjN051eIwLw5uRgFTj2/AsPtPnE6mUJATMoJs0leRyni04vA9km1i
YUVhvcIgaEX59Rhh8q+MfGblLooKOUgq9AYI9Xc7tNAUPFZastHrMZfeOLyhXqzNcfnernI/34JY
rP/QnSzuK86IkSzgKngsounF9s3pAxjqWG6J4W3HBXIjcmYEUdcJ5ax7IL0HSGe0JUN3hlHGhDAG
FHAnYEuGeBugDS+EtNjCgBBKiclmSSemzS5TbSwF766WsprXscP2FQKztfQcbIle6srSv4O4dsKg
eteTQmYleaDQv5cRPch4VYlBsof1tICV4X3F0Z1GWlaxdT/lBQSVcnmuTVctlCjMu3hzyR7S17vb
CH0DbBJ0hDvFGHjrttlR6/vs/fmB4lx0p7IKhJk3UE2Cp5KE/ZMJO3wpak8J/vM1BxfDbdDugyE1
p0gE/EkoHWemzcMXSJ3nTaYpWgDOuKRq/v9FfmYkzXiPzdS57LrZCcQXefuwDhoedpGLQ7ntpR+j
ekSsmgqBxkqKWQD2W8P3JEknruDSZW610ry77JyTqd47+f/bB7sGxVnFEtzNdSd3TXXpkxuz2zvF
i89+XPLeYSaEWnLQwRo85Gx8lTcjPrSDZS7f+41bqZ5NMZjAxxS6YUzPVeIZWjDeLqIKRDK3B2vJ
vYRN+kDD6XaPTTQnnv5BlfEUdsf8D0GR087adBP3F0r58GApYqisFtfk8HT0wnXtUEA+6g5f/GUs
cDBQNSXXjy97C9/6nWWH39M7kbo0DX6ZEMesEB08HnKb0TTP8oEKD0UJanJ/BsXfwB72AdChCej4
FHabZlQ0rzvAO2ec3q25CQHxKMNOEGU3nCG9TgVxk1bD+O8Nt+tKvwoFxncsUqjFNQRDu13N/HZo
98I9NN4hHozcqEJCAWZ926WyYDsCwuKeTC4xYDRArkLRThNz13FsXspg+G6MXlFWeiv9FabTFSt+
OiRVY3ANV0OfxUMVYjMKAtOLWBF8VGZeqmdN+FMSF6+rUWLCtBKzcDGoJmCrCwF9NFERVKhl6ury
rUJYxZtvEduNE5kD9+9zHhwhZnHbdd0IW48ACA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
