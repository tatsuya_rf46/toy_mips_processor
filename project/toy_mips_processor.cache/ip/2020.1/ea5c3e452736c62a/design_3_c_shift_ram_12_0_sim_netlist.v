// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:59:51 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_12_0_sim_netlist.v
// Design      : design_3_c_shift_ram_12_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mEL3Zs2sBWwq70h+LUnZNQceciepEbrWHUq4x8NxuQ+R/Xo0qtAnNBdTgK0YVw4cfjk3HOeBPbTc
Ucr1RFYKuLGiegSPB4xT8O/tg00dD1T4MXFADWllCy5Kck+jfn5iyiGYNVhwwTzYVWefQlsgrrvu
d3da33N6Pa/p1O9VzL0QTdZsE2uhr+qG6Ztz4QO2+/9yrcx2BkKkMHwfR43FfFvDk8m7EMPYnSwV
FTIdjGaP96U197zOch3ex68ttlCjoYusJUqDKpGfAqxv63ogq5k02yXYGEXVVTSOfUW3+XSVkLsn
RIO6md+xL8AYcmsGNsayUUkb0mHYHteqHioJNw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NUxscx4eYr+gVYGdkcesDcqbOiD+CSwPwnxLDU5WRjj1FNLP+bzBs+17nuDkEHWISuMz4nvIeWSo
bL2oqzak4iG2OpiAZNtsA3pa/aDbVHguoNqozSWdM5jZ4qrzTkl8qvlfpvI0bEcbpjdGi1RjGN8k
gQkrNfdm7V2qUC2Zex0FaHSM2iNAzOoe9M2YejPTqngZn6iQ+t/LUytGpn9up1z4W7xX8E/CWahP
YoUigcVROQdHjscmxZH5WnjSoUczc4tT7/Q+fPLYcmccgiIHxZZzKrFPmOnI15nkyMVFzpwIcgzO
zhF7AxpCslLjG0GbE7kbcoOGOtOUMUr0F/38TQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4928)
`pragma protect data_block
W6LRB1VlBOkaf1ik7+GY0PsYB1G1ievBpLMDI6unX/ac4rZx2Nc+nEfYGjGL5e824nqaarXrIy30
CwwX8IHBOhCnXHd2kEBhtwGK/fzlUzo4fVWWHMbrf8VFujQZ03SAq+sfCljo1LhmsxBbO8xhZdk+
kgy6c6cH1dL+4t9IK8AY8c1yw+pjLSjEdu+ON8TqZCudfNtXPWQTJkGW6PblM8BaUHQ1IfoUy1wR
goPplB+ipDt1rgB2JrfStncLHcMHF/4xMETwkc5QpNpIrQTC0QdNfj4ZWKM50YTNdX7jHetw+0ly
vTAVenPnirSI4OJ2AjNuQEOAg5XcCtS/nG6Fn4V+6HjG29kGIR11ccg1GUAL1m89SnfCsRRix2Fv
YAG+czhquz88X6ELp8A/ocuX59viMhgHvdCM+EZMl1iYKYvIvWWJuKeuZT2t9PvUSCQ9uItrmdVW
Oq3OBSQCeHXPnlMBQUmIIh9FgaG4762tvLK4ae1iRO9RFtoJdCigiGy3je81zgFOohLkpS2N4ouk
eXPKbOV7lG6LALWd7rZEZve6iIgeQCOLMh6XfCypV81KQsKbvFSOhOKEBOiL7FYwE9kUygtujp4R
S4lOKSnXwCvYTow4Ka/+8xfDNk/g1WhwOwoWNYGqd+pb0uWB+m1pfB4XK0U4+jy3zT0SVsFyM4a8
8BwsYb68YN1HFRJAF2KfJV3OBuhSALwEV+6U52eSp/2Dwl3TPuzV6PSjUDp+hYvTJ8PSrBR3Bsyg
IuZP3WZexi0qiy9pe13sDFVIbLsFR6vs8srsr3I8q+JS+uqlKnP7m5Q3piRKs7kTHWtXIjH4FBXG
TrhH4MpzCinGjnaotMLs88A0bfWFoY13hOEHl6Hk0C0OtHFqKiLqn+Ks7vDz2Db888ErvWSlQXwi
DclBqazOIv10xWPS/8zYzzubLQtGX9mClEJ2fMAP+g8ViNorJN2lcYSxXxj1dbTUKiRK13bs6OM7
hujKGTDY9OajjcHw2MWjYMIZ1fn0DjytiW3CGo0ARhWULkO+SSZ8aHgL4urzbUJ6SeS/tPsf2VHl
b34dUHXYHB6Z1zkyyCumZ8QayNuhl6QJADsjUP8Pr6XFsd40LGiFDRqh+fBhrDleGls0sHVYEiky
0xkDgPBt74P4J4Z+zkIa5CJK4m69XcTtQq4HIZtnwQ1HvVPzDV/y3HxwsBG/m9f02hGUacVr1baD
aGqtCDIVvk/tMfNHBcvcm/oykVILBw9+VBP/y7F11ZVRFd0xztwHQLHtVlgn615rJwzbDlbZDA78
9nIvZr5+ZVf4Tb8SM7s9y8Q9HwQwsPFYacG4OXT88+W1RGrF9VWHozvXiUdszDnuAM+p/v12Rqg+
am4zxZYHyXRNdRylFwXyxKgbui18W/gyq7BFWH2gCkt56xdvI0FrtmPgC65+3JN5FLCYgiRpVo/W
t6Ty+/ZSdd2+aBOGvZPYYf05IsKEGDVlII30EL9n6jofvUZ78feHqMUlneKQvS9c6OIyKixTAWb5
yE4Gn84+3LnOYVA50N7ACWXjvcMWvsa3ssebttQbB/xx6mWkyXrEfbhJZIuuxa7hDzeGSZbxblRS
OFGz7yOG4lF0zJB0aqbRmgYNh8oHNGkk8SRwKTlXJLiv8uUFeucz9xLjzAFU4hZ0e8IBPmWHVw40
kgpJDYlFvhgTPQrrumuF/PBjGgvV8CDjpm/MDn6AYk+or95BLynGQ5aD66+U0c8x1HFbD4hfgkJQ
OBGwBKD1TIRZ+6oLkGg7WuDhs3uqtJuExexZEq/gnmjw1osT6i9ll0+wk3hdtFJdxtnH73IO6wu2
npGEYIit3mnQjO/R+cikqMXunuHGAtka+tBJFXMJ/qU7us9DCxneD0VkSVKckYZReBknT71YKE6e
r5QtaFADtyyKp5QShwiCH4/clMzSotRyYurVQnZunhPS5/PilKFuLhXPxSCsve7vNu1+EFg7j5xw
hH6Wtjs9TqH0iBOVzMQyTQhuXn4G3Qs9cZwO5P8IvEgypyAun2AwDR6TB7Myr1J6E7cRhjwMhVpq
aOLz98kssIAa15RkwJ8CpEDpmYK0CEAgkYxbG9HEcU/1NNAT6+Pcx4Iu6pt8fuD/Ru8KUpXvm3iQ
dxq4iU1H5qfIYLZKL32+zKza4VUAUCBacQ1KKmjDdH7Mj7PoRJ9Yl5c5th0pWMUCKufOPuqEFRlo
VUcSqh+HL5IcCzuohK3J3TjW/sppYMeQzifA/SP0tOF3ODd6+nEL7IJ4l+/WGNZ4U5alJQ2ju18j
EBOS2UHIitRlgj/xxwhC7LM8S0X+8ZuxeCKfoVeSmbuaKjiY2WiYtt0Ku2XiLVwHJJFU5j6TNOKt
utVp5RT/zAlLigy/osjkWX27UrvFlJwOfpbFdJghL9HdsMjdCGhP/hF1UFaqjxOa/jNwExZ4Kd/A
NWJ5cl5qsx0nk4zS7zQrfZhaL90umtgOqTjGTffKbxYoB0iSJJvInpsJfwuhN5jGwJ+5UxCyY4a9
gdwGnU+iLGRErVydJ0XlD0rKcljpS13+lQwOZGYPl31PBhXEx3AW775C35s4+evjuj4e+pjpyyLx
kT0YhbQwqTPdPnkx7Ke1w/ag5dAXk4HCc5VXYhR8rzCkrvTAHvFn6FY7sUf9TLTtgZjdbNGaN5u1
eW4rBzDsxlqxvGrxW74TPNmaw9Xv8kko08/pRmJUczPM1lfGVvqwHrNx0w9n6Rdt5m7MbTv8dWfV
rFtT9Br+oUUr0GNvK+nXM1ZWCrAHhZCg1yYK5RYj8h49mG683Kz3p+HaiCglhCoS0vWedKJWi/C4
8XDtCR+VrQBypQmbQxy4SiXXTppWBJi7RUb74eCqB32oRKxWL3BweVL8cJksftrYYqfOlDzNv/Bw
pl2KVFngWZrCXwO/8GcWekhN19a7TOdCznloi6FHSHjyHlH3tU6yX36SlPaaItyP4Tpguq/ZL+jP
jC4VuEcBklxOHb8J1P/wFHRv0eOl+o37Iuwxb4o1uuJ5Vuv5OHSg3nwaXEN46ha7RBKiIgvehYwg
CMB/VcrrrqolqNBNqRigDJKjUep9rsvj9u2AmO3cTsg16iZbOsLJB8q8JG6dKD0MI/tYQwHMGNJw
NwtwKmtOdDSy2SMohDMhGpc5MLfiqgWT0sdpmgLV9eHmd54bqHJBbKmTdStUGzEiFmJ4wMImUh8e
Ga04qrOUyiOCpsOGXI4Mci/iMn+MUnp39KpYaSrGdeEUGjnzlwkDFcy7S12DVaz3XxDZ2WoHa7S8
Aejl50E3129wjp+mZxQYxyBrXZAFkmo6oM5a26yxn3lHr7FjKp7/w2YujoUr+admGIJLIRn9CTQi
1Zcun+n64NF7ELOH0Izm+LY+b+1IZMTCHGZeQQSDsZAFM5riXw+8h/z0YpHSq52N4DiAy7aW4l87
mjQlJT6TL1hgE4s6hhlAvR0qPwh4yGEdcFnf3IxhEqOv62/BXoMSwG6XA2lnaFvZc3NWL60OZQlM
Nu69XWwRp/LBwu1OSOtUmR3xR4r0TfLsG/R04lbubXejPrpLPrQvUQW9xWbBrqmlB93x6aElDALr
jcrTdfRU+lDCK/JWrCXm+2u5Io+t1PBwuw8Hza9NGVZRH1XL4cEZIlxyooplrNSRq6nUTYMzTqO0
UjZwrsVAt7jW63NpHrgmtllWFKnrGqirBsHtonc3MXv5CQLqlTZeHVyhNG7NwcTrsTPYfh5coljQ
5112m9ns8VMDfNVVNVYvERSNw4Rse+7PFKYzp05Sdo5RU4BY9390FffuOCa58vwD5coejImr16Q+
NzuuqPu+meiKn3UAS37Ng0jMgYRIzcXRtRrxZjezK4AcxMLMOxuYp9kpxMIUBJemNZSqmAJu/2E9
XlLNewnkE4l/v4KeEryHpwQjb34TBUpca5FjYHeP4cfC7I3K8Dz3pYX26o6TMKAdz6/Fb36NMQ82
1HvaKdbZEI2fCWxpY3yBBAL72nVMaBCVEnI7+fTM+QNj1g4mJ86x0eGRnxTAqV7CQHz0XmRlnEqi
PcVbhUyeeNiBgmeDci5aYpE7KWcZ8tHk0JNPBgdwYYo3/TfjQ8vk7sdbureTbzg9jSdM7TFMYctE
wWA1J4wBMNEuw5hOr87ophkZz3n6FByTSej/RcVjtFNeAdPL1zcXnBqfL00NpsCuLNupj7CuXPUn
ZeFlj2j+I0FTb+j/pwonwbv5+A4cTnwURgitz5JX/8OlCvuJurv0bpEDPh5caBF4Z0AIiJnRIXG4
a4XEtDXcDnPqAs0ITLxsE2vTv1xmSgKLaou21BojLN9plHRISDUAgheWqH1o056APCAHneoLJtqx
4B2hnFt8XJRT+tBBrGKr8f0viiDnofUy6D9sii+K50xcQ2gYgg/CevcUIz6IIXkK8sVb3iH7zBTU
vBvTYA99Us59vzPoKliP/eD/v9O666q8ZUGUbIsw2RHQfgf9SED48jmrF3O04EPzMAAGcw4dag1C
VrVgIYLU5iEGk/ed1ZNIpshH9Z97qDigkO6BlDSw7reuVqX1bF4EUhneQm+1xQeAxddxbJIJEixm
X99hSlXZoVETN33HFlX1I9iTofHSwIWJbQhiJPhij7SL+DAI869bMA2wNEMCRoXyPgjEsbS4lc0U
DvjPNSumdZGb6qNdHNN2zNvSFFXZh/GaMUoTzr2hp0MHahaKtNdcYQBy3lzIlc1ChXTG5Ba8yFZe
4w3Vblt/+X3umYU9vdWEePuJ2lM2ND3G6XjDLGimOwkw8QdHtqAaShULhn8zeJ4oiGurPU3zLMyB
bJZ8dUcca65dYftsrOk2BQ3zet9KR3NL1W8ceWiUOQRI+xeKiTpqNifdfw3CVQZQPkwYt9fTFUau
OXHZH3dx2dbiqyL86BWioi2OZX0w3nUdDU84cV29pHpBx+fFUpgwXjasQw5/jeQmzthHNBlNY0uF
qbYY2aRuiRfSTnuXtguzmpex46mA3Y85nJS+jqNzHvxrtGJ/6bdZxeLeFpNB4+QRr+89nJP+yA4n
IMKCiNsi00jRbBXOaWMIvy5ErlQ5gZgSLOdti+y7ZP7LyWUP8dzoju/9VZtMzhHWb1Ff4qXpIiYH
/lFDD5IA+lktI/joi3W7LIa9TfT/3J4InYh4D+jBsu1/9Ue3eslKwRjwxyRC+8A0ppC8LdzEmMff
t/qP6aKg63pT339p/jLdPTZGsn/31AM2wgUplpuVDacDr/aSXIDekhFEXzXotKdZWvqkH0bGwORx
oCvtNAvgrjfEWv2hup1GJ2OlV7o/5zr6CvAhDGWquicX2bxYQh0+RvtdAl0Z39K+PlH735nEl61T
yOUnS8t1h+XDm/H1dFfg/wmTKNOpzhj6etKE1ham83tPGL9Soj5ngDcSe5qpoojYFYQh8Hoxo/jb
Ukf/Sj6bal22jXTSx66RdcOU12HsGipwMaacuZrlTq5k/v4Iijv5gItnRT2jPkrorVNpjCLrU+yQ
Qvbg0nU9adaoYJjmzZi1i+zl/hfBgqX7WaA7sRWLALA2s6bKDmA59IsvOOToPy+a7GDo8i3NqnVh
aMtEI0CfZXjIHO+hkWwn5Kd85KtioH+4otd/7zzoVYsjUrgUMVtdBfwSNneqR7BJ2IBd1agF1krs
DSspOT1Y+207b/gi9FlYXQvYDhJlUqIaKEVRrk/4NDEV4/dFeg4mOWKUVaZ5qB1Sh3QGb199Ke97
11H1bv2ogf6S0XJaeEIdQiiIFtPhmbabsRLgvfoXL8/rIatev49AgrOBwlfXm7GMr1ZyN/hNkHVU
f2c22nFjK7cAF7u0c7nCvH90eZH1Cqeb2DIzvlVUWiNfC6FBQiJhJlZwkIoHa63I0BYXpKufEQOc
VI3x3M3hCe6QRLXvKq23dhpdSC8mMnKMGCH1dmehO2OObyd3WzP1B7NVk0tJDvV449cGn5FdpsuS
elaT7i3RxWiM8tXxnYMGKYF0eJkYsJsDn3T+T6FKqLodeyp+MFyqG0FWhjTLz3Aw7swJoMii+0WC
VBg7vtJrInIduPVEgG8LLMlhuxgyZJYQ34MY/z7/9504QHxHiUHGW1+LNG+V/0iRKttQcncTluyF
SR6rmgdJL/ck/mKXapHjlt7FRXCkTDMWVSzE0rQPHVVJpNcExgajkotTXeladRoDPOdFNxoKA00K
VJpEredgHgEqrTZV75WcJvQEznfe9Gqnsd60/zTXZ+fwNPvI8Fl7mrWPG/wBEaOPpugrjy0DOpkA
81bRTj7/RcKtDtm8Bu3TEUVesq73j3h3/ifM45LahFzO1Pq0cnED5en9sYDunIkz/mj93AXpd5NX
BPqBuUMq5Ai1n9adQRas105C9nPTRQ87Id6ufRVd7re4dMcu49yt3ZySAlYmUcM9/alitbPZ3cxv
iwWJAa0tiFFRDnITacitzA4c/VXx3waMQFDGIpLzOwkayojT7k+riRUUe3mPAhCzONMwQyG2lYZP
lPg49w7wphDQ1SQ0y+o1DfnaNa8kDsQidJDBmUj5bNdClR9yp1mWU5WWsyV8UEpfUa6V12a1D/dk
u+RLgV60UdhhkOu++ZUoxckfDE5D6CUht7w=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
