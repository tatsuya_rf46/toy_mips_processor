-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:59:51 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_12_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_12_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
kUUqDeYk9Y2B9N1jVaHGhWR6OsL2SRZ8juKeIBX8zdZZ/KkYEwKz4m/SILJ3ZSybpuj7b7vYoRQy
wJVpgXiFvkz2J+9mWCdjy7duqjnJdZp9hPW71erfNae3uGorx4fZW26mCum1FvjuBLMhwUkve+7l
CPqnytB88l2VXCQbUoNrUALI/dStvUEep+sX8BqTO2NhsSiAMuJ+U1f9PIW3+WUgdJJ8k/UUUdqN
NOAGM6gSt81E38Fu+7DzPNszo4bEBf7n+Rdww6Si1eeFBhw21R/26EFgah4wVkNf/A2zmjiGnJZ+
vIqnu/nkpDDyK7jcvaJOgoQ6Yy+wQp3nFOjQJQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nSBRS4IuSoqugUyTfcBnhp03EBh9iQhkWX9JL6RZrYWbkSO6fPP7iW9qDrTIoFRno9sYx/KMYhoY
IPDC35Va8+vtcex2FGP0e+TUxUwXEAK7AeCs18B+uZkVcSmg/zpYLlajxQywjH9yTFCputLlH39h
DZUI3QtQarYRwf2W71jsvG0kduFSNefyqi8n+/L/tG2jaBskfVZhaWJB6E5BiKOlHU3d52fzfoXy
DTHhJlvwN+vmm9P5DxE5idQ9NSCUlCtOQYHnLvOM9HRhksw6qmVANRjb8CGYIty5gzHGvJNqhbU6
4UpzshdNu6wOyZou1YzalBCfomeHa5H/Il8HbA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 15008)
`protect data_block
69gkrPp8ttPYqmtz6ENn45JVVKT+yjVdH17+r7C/QDx0Z/l8QH7xfXjj85sa3aAZjeW4q3NyxkQ8
G33cEStQWBFEb/+hlGOtjJTROHToKbMPU81iZp0JI9/ElfbhQAVXIwQXn+rUNiyTOFCYn0v0GgnG
VbqglS+JrFRnBbPEop+dZtsgFbcTbf4ZY/boIhgIVmECOIs/BjsqQYVTeaftR+kixhpz5JXb/MDn
qo7kqw/bU5T+PWa6UTQ7nsFZDYCLIKl1TFGlNwozxYrFcoihnswLBILH3eaH/LuUgjeRcTQcDAEx
ZPYUaSauLN3q2PpB5hvOFYoy17kH9wX34YW5bFTk+RR02q3DbnkZy4E/ByFDHbVvxIvAV897zVNo
umgvVKXgGz+w5MISxIDj+levYPzVErUgniuLBIx3dvpQfwUeQHZf6zrdRrNINABHNvX2QT5Nfh1X
RmXchGz/zu9ZPhJNjeOnzcFOJ9BLnvqFcyzyWXpQbWw6P0uNMb2CUwlvIUb+nKqjDU/tPn5dz3uv
IlMcwhhJ3AYTf0jQ/d0nt6VENOnRD0p38ar69QFLbJZ1mIt7jMWFtQXIJRDI6WeNBw9OyXCBK9DO
Hq041fs/FhYAcy9+OGnqIykK9+5ExcJhupix90Ch+1N+FS+5EKuoDHrWF3OIz+1OGD/dXEzHhq1y
pJZwiEi3VhDU5yyYnQeR2LqEtf+G7JsZlYMJdDq1V8gDTZobNxneWiRjrJAY81yRsSRhWWlrsOo7
iKA7Qe9BBitDE4m2pspvms/jLqiQk1gHrQocB7oWwMOPJygPkQ8iAlQ1+6SEt8HYWxYY9vJAYsUy
/DrXG5eNjm3Wyl1F1aH145RYR6EsVsYE2QWQjaB1a6S8TbrGIF+Z8F3gV+8yu7hNVuphNGx4oxye
ClnlJ9puINNG5K+mc3bbiFHJ1jUhH4u4cp1qXG6eoLtF5yfqVwOvh2HXqV60G2nx55SqCXW0WHs/
XKwqD/c5++tcTzPpMp8gZsSViBjyKL/dlkr/H7Ob0bckmurrtVp7phwQbDN4bwx+x9uDCIGJT8fI
jFe02wu9mMivrVxLL+wCsxKzIF1L4Mnb9QyAHRwrIKPPEKPRCJnyjEjsrqZhzCQlLr6YWKjZ9Ktg
syX/PPSNji50L9H2dfnI9TuELioaCgZtHNzreeyRl47ik8EAGEN9VqTY21xWZiH8ZdMW0mwSXD62
W4mfUxYxSjkrisPtUvsZZsjYxscDOwsTTuqZOKOcdbqYZDtcD9XF6DSB6IiRrfqdNridL81oRL0o
KLSn0LTIpoaTaQwZUtGw7J9L6cNK+ySyFXM/7++PozGqv6ZrTDcvQTLC7T4vVThj+glhEUbAqZ1+
tNUZPPPPfL09XzLxBk2ovaF1+2eWVUTuwwoVysqwA1sN1Z/m/8AWUETjOqruw473n04oGEy4gfOB
6YixxnGa2e2Wfejq5XS1hfNu6Q3/LIt6BcHnuzX1zTZQwHtD9KvJ3EnijTziA+nzS8nM8yA//PcH
LiagyZevNJX+vgXgUZUuFTKVK4CZVwlXc4tcwSz06rDB8cx5ur4qJQIYW2cSijrS6VB1aw9OdXFU
YWlQus0u7xWw8iSjLJrN8jtWIvOIB/QCMqDa3YZTazIILjoAtjQOwZ6Cz/cX6SD2cHODWzEBF3Yg
i/nFRi4zZIHp9duGzN+kn5CWpgYCZY1Y1vCLODGrLTMgK+TGutC2rrz911HHFDcslE3xF32GIT32
dVQ3vwOMMy2NLCLiXI1/7TTe6kVadxL5rENaIQtll9gGkkAcXLnL8gGNOEe/fSFOPIR9rthccx66
PxxXbVwOuE02v6uTm+uJRdSfvqhkXHMXn9REdNk/bR1CMi7hABj/M0db2Xm6iQEZE3TSYmn7B8zZ
has8O6l1pbg1nY+u3s9csl5F3x1rFW5ac+W5V4YTc+3H9pWLIKGr1RnH/uPk3f+7x5Oo1KBAvf1k
RuYw+F1c9cEOwn3Z/wEfupEvvHlQPn8tHPutYKsJsfq7SluUsI0Wl69cWFAW6hG7gyH/mi7pBzja
UygYO2Nwhhx3xo0vHqEuk/B/jEyyla7P0WlapHzzqIDbd4ozoeCMWyP372rANw2C5L1H0KN9UYfa
OU4J/WMvX/3bVmsStqt7lpwWsd9taYCilj6vHM8XysuGwESNgIwSL+bF30NhxZ3n1GF48rpkqqrv
gFefMtlo3Av/L1zt7OYjhQwNgagLzSPY9O3MpwOmcAx/hZC8C1mBn9mR5RcxxhMSjj7MG9Updplq
8p4o6rsEx1V4jc6ooUZSXXrO7Nrkr0j8NVbEFmQah1hF/TEZyWuL1Tu8ARnYCJVEe2h4+WcEp+2X
G8HH9PKOGxwxXDAib5Zr1vIEknxDIaRWt2cNeHvpZA6cMVEeeSy9ntJztEAfGKELN0/FZZpL3iu6
6rWhCfx3HRcuTPk9pmofmcBzjbUykulymAh7rE+9irYokDMkY/I4iAG6sMpqahQKZGQ4mPGSXD4R
xETY1HrmjGtc0P14ViQ97hhN4tjlIfZh4QLRMpX1uV4Cg1rT4vYUf5wAoapIdE2F1fvXQuPdSNDB
pNET54Y/05yrMKCVDBEF8D6zG5M7MIEAX/u+dGkbOds+ypgm3ZcKrhHnzAKk42NVVyUUQwDhCR65
4LvzeTfxSzogzQUmjrIlFYefK4RGsuGWwOyqjJWlSVxmYgsrbH3f/HjzxxGSqvSCelJ+ygtlfHR1
sIjri4DiVnl3Ll86QOCFVYo1tEHk9L05dIRSdVm7G/Ux0/zLW+J8Wv7z4X7deKJnx3RtCTHWpslz
DcUP7cInbdw6sGxRWPk2qWYmlfdD8dqaZd3yOxU319iiocNJGBffq9Z0FGwl5XgHLUg6K9m0ToG5
PkwMb2rjak8HhyNh0PgYR4txgy4PpCsg7BK+NpD2liHcwjt/3fLDbnBFbNcx4HouE3hFF6u1Lmlt
4/7anGrSp6WXuoortbekxjwpNUp8hj+jLnMgqdrrLuS+kvN/Cx5RFAxkksnYwVMkz7NdKluMXrG9
83T/gFxSpaCzYiU5wP0O/0BaJg/yDzHuomCNRZJbx7N0mdhThECyaNhxwQqaVGMFXndDOzqlgeBl
N+HqXDh1bDhvxLKsUsFM4MV2l/WNDu+U8BDyeVwZYM+ioMebNBfs0dwaXbXhCLm5IltYPWI7iob9
28mpyKbH5YsG++RvYrSisekkj/CBBexzH3ITZ3st/HCx0F4E5KMg6CiuN1hjX3i6vQJp2jAs7FEd
KDBff5v9lWulxCGDXtGUi2mkOJHXYASQ63tApGi88oZBVl1d0YKtL6DnBKnJnYFC7GTLW++FnVcO
Zqtl9xcXvm5SiK/j+ItCs+4LtlYWGN0oRiPwsn0psjaVWgm09gfqZyTlkET/kNjRRRBq+DAp6qcc
2gEZkbNggaVOls+BI1cyBIXWsguOwDI42vTI8BbQi8ZUADLB09qoLWqY5UpS8x6H8vfMKf0e4n0W
l1hDJ+8VAK2X4Z9ZljE1FWEWl931o6rgFZ6yj6gsTFLVQ7ObibHahSjKkIPhV3FzZS+rq4nXYArG
33oYmk6RO/hOCCVYTV9hRhuiohSqG6bNYgS4hmXDvNT+GY6rLScWEMUZdnzHqAxVGlnfETDel/IT
f5qa3+40EUlMAR3eNItrZBktg+r/WX1a0klC2j1L0MXvMHQAe5og9T1+XQfnL1C6dgm03xTkfc+3
Jw/T5X4xNKDwRP5ME7f4+GcVaCctrM6wFdg2TknZw2yXjarw9aIvk8oXXQHpyc0mFjgpTdUfJgK4
GKNdAGKnBHViojtmC4xTlqQlBPVuYHTXvtx+Zp5tDuQAAnjsWepSqlvlExn2EsHjGIpQn6RMYwZj
e8tUdGrbrhI4AmLZ0hYbR5HeJTGvapLkv3NG+OwRRRZzfNZNmiyDjjlhgI89Zsxv7/EdJdZvajGN
pGDMcI16cGsUraNyjcX3xI7uBsvzdp8Sljl/OSwK8F0fo7KTnBOl18Om2ziXaRTLnPXlFLbvQzu4
dBupUKp7VzPBoyH7guLMg3SXJfrd9A5ee9CiB3pDiSwUPvgnapqMEjHtoztamNiODWdvvcfoKZ+N
DDEPR5rt+b5+aBszX4vlF6q/tlkbjGIT+R0/GcHA6MvZxCh0EnRXiYunGmzxJT9cQOlWTj7SBy7Z
d6HCAqAFmx2NHaNq/XPvmAdPDybGTaCCOgxuOUtzJuBs9UbpLYZThM1B73m92bNOq9uYNFxdnYxB
59pdFx/h9Fhne01t4Al6zAs6YQLJ1jUxiasYhgFafS9mpyofgD51p/iTouLQSJRaI3zl5sivYqlq
IPGMDI6LWe9qIPED9okJATLbhL2+qm5Ffwotf3jffMid3O8FgU6IQBmUf7Dv4Pm3WMKzZmobgvC8
KkGQXx3LvCWUBaUNJ2S2+JkpqGBhnVYMlg7lX01EED5c6ZkX2YeZrmI4vASpp0FQFc7Ks7uNOypV
mOVt1Yu5WHKcGcsb6aZLQxZ0kHAghzOmHUE/SPdOm5pTgDDCLyxF/J8K/yQ5jH/klhs/lfHDj6gY
UtYnjHZha4l5gLl2cSECNc9JsZCinr9jWHG5KGxLT472pbZ5wCph4qX5Jco9IqjoAlP65b463fkK
YDaBgQwG6m5b46ng1gqrr3xk+YgrXS18J3PdcizkkOaUa2rSoWqI1c16NHGO1XDlSc360zM+Lvwo
iYs8GRV1yQ5jFB2hyu1ySthTWPygrgqrOM5uM7GhLtL7BeYodIKkgZ+2UbOWU/odOKtPDXV99AnM
CcEr7N/vWQx4sx5J9TTDUBq5FS8ILiICuaISmko1fTXD8ePs7TPEb6Y7sA3DQtrBXrQsyMFV4Qhy
rxvdGaEzTKzfklj48wxEpareArYWQc9Sx7QnynmzkiERDZmfelZjhpI1DSKfgantwy1aTqFKGhCF
MbHVns2r/5bTKnXfV39mSNC5cQFr/eCslr9EbPg+hO+Q7mc3P61fojugowuWIw6/vjUzjmnZMh8+
vlgIBsCmwELpPR2VkTi/gqfGuyqpO0dc1Shf6Tm72U6RIRE6ykVGiNmLUzuysy7EFprJOkHNp9dr
kV5cz/r+cAy+4at0lAREjSXJRg/TrAILgWj46ggqWFBoo5wiHZ18YnCEHaiRBQzHSaol/0Rikd3X
rjlaaL2wmAM80O1Sk/de8wsLjK0deiLVwi+MdOvGbcGY2z/K7dvq96v9Xf0FNwVpb/d8F+xGEsrl
nscH87/TKqjakJe7MNNpPSq6pLDHHxoknxCvGhxBZqQqt+kDZlxRHxNda/nxeLpwsQ+r5aMjhKwQ
lBCGsnZhgyM20vw33wdDK3tuqZehjtNaDWjc83oHqC69+Z3rtQ+f24cKWqoex9bl1qjJymLrZOO4
lKJ2rHyNB25gvGwhfCCRCEA+SWwROxLhXzJtDYfkaTAmLJIbcImjzVb0lazJsVBIm4kuuWk2O3fB
oB74THOBTlzMhKoxA17ot85wGdhi2Mayrm2aawSGtilNaQcWSyD5/LxlOlAU8dZDu1zRSQAIt/41
FidF3hCuQ2MmvYIuskWGKAJTJLkqjt7HevoF8Io49j8QvplL1OSBGAz8xK/M6P2g+9PfdKjT3NHn
7IlMH/QUWeFLfPt8JTARE50pQdC5DzlBTzNawdx2RDTJR8lRiqHvbE2zvK5FxaUtKabYGA1Try+J
cpD33IlcNgNSjId4Wbgji9xBxXW4A7g2leAnONZG5ty16K8k+Qh/rJQOans+OmnkzuijHRRL0YhN
pHxmACEFUFhah/znjJV/kQosfzBMKh1eIth9Kbe37geaeH/zGfja7hO7yKKAuE/4Rg20eKEfDwqG
eYvq5KqIEWAb/2kThMGL6P+cF5s8UMJGJ8M2I9U4hbBbJQXGJomvjUzYEa+rQBow2T7bUVIq/zHX
7LwM9XmM3AC+GSU7tIdqupBZ+c1hp3WNuo1CGfXIEDPhFAVA0F7bsHcwKdunddL6f0OfYQqhRjLe
ikC7nCB1Q2qzQf4YjKW6oOWlmeaKaFyslVUs3SN2d/AhQ54L3H3NY+T1p9F0d6WYCDQGjeAvOSCG
L7F1ZLzt7FcD16xxRFZb7AUegCHWDyHM3L897FomJuO5OvN12NpsvPwuNKJ1VffWWvgqEgEB4NtX
2/MPGG8qijsUofUOI8/M71oX8enWF8VF/wcpALUedEjJu0aZyfCKrL+Y74v4dekt5/jn4W59MHMV
5GNEiRPyIWjp5f97ti2aBK28hYsMOznLa22cXFHEZ2OGFCZruMK8dUF784NS8jOnR8Jrha8PMH7C
JCORGpiqsiuF3wcUL66nAFKPfxs5fh5Eo7+hQopF9n2yb2Gp+qn2uyFuE4dqQQLPBy8QVtCtd4mu
4hxSfYmXTYm0jW599htZcyheYzQWSvhauuA68shlrpqw/P8TFcbuQ008Huh6YTP7GrTVApTN7ZMn
eA+h7HtDXnJIbiQ4ih7UVoqh1HggYcn5XqPBxhhuGhrNpi6GPt1Vv7zVrmaOSHm0w/Dr4qyNxGoK
VbCv0kRrcvXq2khwa3D05UI/EVXZS2JvOc7UzZAd81M5j+smLeS7fFjfvHQSfuE0ukV9KsO+WqPV
GvCdu2w6yLRXShN53Jo7LApWq1YWu1d8sYum5+yxu6z8hwwWXvO5/tIAqGkUQJJ4bjdu5zHqbBdT
AsivntL0ccxuYcwJS+mjF/G3hsoO9AeOYOkkjz+1cxYiJS4xGMc+pOugUm3BJYmi4QqhEV89aMQZ
MO7yzdEE5b9fjUV7q+v62h7qSgwkRF+W5/B34bEdcyFA3E60lQeTQFUgL4Gbw0gDvNQPzKrfzTf/
zZLKot0SETuR75jYpcjmzSgehPOWsHn7m081FEQ7+sSJqGAtyMbNAyfCWdjRvM/KFKxFmo+J8tL1
I8KDd7gZBr/ba74P9R6HrrII1wt2hWFbhBV+dwB9rsWIbBsnaZuzdW3qehCaoy/6DtQB2rLzqAI1
ZGAbv6Vs12i1fWlj1ixGuCUitRIW2j/QGxvpJoSv9iHd9hKCuEO+4VE+6tblNI8C9SSXqGcRtKOz
z3UHFHjVp+Wq1ADB8BQcCH7apQ3CSV1mEQ/71FLVHxr4vv+IagWIj8GkvFyensUDzOpK7lM6iex6
hrnu0UwQo/hDYPBb1/CPwCmJHa7eD0WiPEaGIrwmRKb/NISlsYyUMlmVsKYvBzNs2A1YBZJ3zfpi
CEC7p/tGWDyjvP2RbxxTPyub1uV7ceG9gAschoGqtEOTwBlSBOPDGeFM/H0AQkTocv5Mo5/AOJGN
dSmTWxnhCvF0AY9JsM6AvLbLkYXS6zEMoKcylI7nXxDkEclh06ocsG1izO6SD35VOrjF3oZe0sRR
puEJBx8lpuJ6zwXZlmDRob/WaA3FHy3vJFZyrY4BKbukDRm85CutHdRr1DPaJOY/GoN686wNAlRJ
RvEKQBWAuZVdF3xpiKiCspUIt45pknAFikZjj4XN6djVunBQRnpmLgH3yJYZA4euWXtO4Bm9TNTR
qeNXYdNN6pyGfgCmh5paV+KuVTmP/Mz507PKBQYTY9QgAuQfNFIp1/ASxoS0Dg3og5wFYUvxvluW
2MZB9CFSNj2NTbmuqB7UKuJC/ahzYE+XiriattnjCr9TZ4DJQLPe85Jf8DxovtYh19DF5crqCHKE
mzwKij58mMcRZL15/ZbV44neKTx2FHbJYapScHaZq4LkVdtaq0c9dFVqcwuLXj8frV+tcqYjJEqI
DLNqTE3kIEopSru0zegC1xpUEnzc8Q8ZkC7eyXvXZJf0+XQpGSN/iS7kbDCcEh4Dcr80Gzmu3Nge
wBEqmPEH0MleRp3nbpyt5L66NlR2DgjOIplwbL89fFuC7mOoSh5BKfTZLwbahMjrJuVevD0Aar2l
IQpy3gtnfcNSE2rJY4gq4MSGfbJ5GaG26HsHr5hJwrlnODJZ78vwe54BYtd57YaGYYwNmO8xMo7s
s0MWd8z8uGRIEgTI0JsTqicioLKqnbuvwmJQy/gDCgW+dYWfrTk22mVT8Lg7IXbcFGhsfOldLc1J
tN5feCY/6f+SgQCiuND2647gsmYRAR6Cc7zkmihl0uLsVaPOejVzIFXIqy/y/8Q0ImCUqqxn6LGy
2c8Dqwvab+wd1dIiPSNRjW/jQbIswkSkfw5/0rzdonlud4gmEyOSUTTsshvWl4ROv5gKHwYK9Ure
LzGonSkYAvMZM7gPrEoecsSxDwATtQ7CLJi92YgISWp0WoFPvpthTy/4HKA2+fpbtrgGS7W33b5/
L9bN6R63k1eAJ7eTJ4X08d6pmEtrjz+wedl076C20ZmGxgLopYycGdg6JS3hr2QMdCUod2UNsuck
IxZPqzbtj6lHO/dqZSqoR/LOhFkB/P042xBKUUO+F6YzuX1qaGmC9OUYg2EPf1zfhM5A0OnTP+QC
SW9zHTNtdNH55H6BZ095ucnUPgAx6x1Ql49fQM2MjhINWeidO6klnjc2FJIek6ysWFT+1zeLYZfX
F7GVSc/UzDcfAi1EMqlCkJPLK45Hb2Npisq+Te+vXe93s5KyMROaZvj2t7yEBFXlYmrLRzIfTYY6
vh1beKMpJbmZ2azFSshMXWgyD03pWQ9So59cAVYFDomO/uym0svlKl9WO7mJeUMB2rWqStVqEy4H
zUMp65X6VahLwQdOGDc+fb7SRgGYt9DAvbIk39lXYoydVD/9HxmsEpJ2ZI3cQxjM3I2JKT5SirpO
5inGcjkh6bzPI2rpqdlIgOLuWjBtzh/40koVCituKJQpkQPUdFURKrkhLBV8cZ9GWR6abR3UWZQ9
AzEUEN7Ge8uekN3WLxQEs+ihW3fjq0NZQnh5uBUoUNTAEsV/rWhQFyL8afcchIkqli3XtHOdAbjE
zIG5n3OvxQRKZ+X/+DYMcvXRB6kZiP2sr+6iwWiDNNMyg+8iimvj/kEGCoIvNAmngdWup/mmXiIh
9hPV4D16xj1c/DG4KNj0R1KbmWC+rHbLP1z45aPAF1rf94VktCcBqZRzbowy45I2Nj88zWSO+Sdt
xuktF/lBW/uhISjAQpbG0Q50zsN96QDWU5wnNypIouWj7UkKd8fcKCZGMsWTfkrFZsTFmJCFCfei
7joTvDGbm5C0vyJgWrBi8e1ZIbFJ5m9xuYZWynb0h0dzbdidmm/OSW9e1juBA/KgFcLvMElbt/OB
aaw8aPLNtNPMUCDNd4u4lwLwKrDT8g/y8i0uVCV5oF4h307k565A8ulOjIlkSIOMyLnL1jFNpKiM
/h10DZ5uwx8ihf3/VuuDwPQe+pw5C2+p8giuLpzczHFpBAeEyrK2fWRGrLXjWPIL5lE312x2oBWX
S/Bu35vKZHvwXJ7s/whjRTfBVUSsVI/dvgVxg7fyqUMgDQqNYLAP7fMK5RSTAZiHAERtpZdBd1ig
noDXbaAEpEoHR9M1po4jBIRJgoKRfKlw5JO8ZGXR188gyj/icZlnYvCpEi2wYn5j/bOr3Jflj0l2
wFkYo2JzpOr4N9rxrIan90rhNDCLJMHw3IQJ/cqjl5UUFoGxbb3pHn7Ujk4G1Wdg0/E/nYu3YMG7
jcL71Fy0+XR7taqaUL9z1Zake4H7USm54X9XOFlq++hZrEy1lhUdAXnHe3N4p/RkbHmngRu4eKON
quOpiN6jw84QT+91zqXc6TJSopCwaWx7klJSGljYgsx730zZ2iv2R5MjaFNP1XLjXqu0VqbT5Cqm
1ZmshfHAf9LktPGFv04OknzM+GDa6BBvcpdn0oyGtT3i5uMNs2bH2l9PrPwQ2/nfK7D8eplhPkTS
cPgAxJzvW7kcElxoxW1+ci5MJba/4JA004lYxC8bC5/vS9yXvEtZUMT3Rk0vR4T2xMMrsdL8x6QX
+0De2H6K/Pxi63s3TjHiJBS/stuJcFJxqXXIxbmNBc/85xVmvz9cuBSz2tKqM9+I+rlwOWyptMDX
UBxzmbs4LXTv1vKZzbO4mI/ZCHYy9FAVqPgH6+ct7rVmWMxmDqShTkBwZBlmR1fPTK8Z8L1Dg4LG
gxTFJpyZBCtoEripOmqPC9//Y74zGvpmLnmYjZFy5tUd2OY/7ChxF3lS9yw2uVLkpzIa/XXex+1q
oxCeI8CbNmhwInamAfyoK9uYELHHtveayAmd+GIcV8/uDUA3HrG6Re4lH6a/D9LyGdgoHmVb5M10
UBbwO5fGXcGYlU8FYidFyUcVRkG0zRxxrCjPjH5Kky14dn6lEX8Gf4B6Opn9Q5Xve7wmAy9opn5q
WIuFxlb/8L5AdxeagwXSLFg5vpdgMR8H4NcitmM27IrrUiWLvJDRSQxGQh+oL2/iBYjEEu6EZd8b
0YoYWQrpoQWLbS0Rd1VzEpX3p/uVkcCdiaEXgpTdIbQqnQoKrKueuWuj0dntw2OZbSv+dods5lN8
ZPCWINontnDaeyGHLQZc82HSgSk8KNsmDKmS7YZ2cORxN26kXiQ76MNamHCyJab5um8icU4/pgdI
/cHsCjGqEvIry7dmlZnqRDwRPD6D6hhCfOat+dWlZZ35AAKca6p8velZZWGUE4zhAwpbC1KnJxOg
2ZxcWIMr6mqUmTQVjNePeeG1BlGpsJjXT88yAgURDvISpdAzQ8pdXQtn5attL+r/RSfs2cbAVpJ5
KnnciYrc/2s3M+dDgS6pzGtyZZJORMSlmA7C6EffCDq1NRlSTVtIxYzstM+qIS2qj1j7pJQNZWHS
gk59wVcbRw2zlgW8OC2eO82F2Qu33EVydX3E0wWSKorjLF1SfOjr8NoeXaOdmolTjWvMHTRudrqT
F+HYQq7t/Qy/buMT4vT/VcQpU29s3h0lACXwHQH1uzEa4Tn4Tj1dHUj0SxQOrW0WZIEQ128SlY/e
KUDS+62iQeMrIm9pQKuiUA4jlxCkW0z87YgdhAmJuhSjOw5ccOFRS07k7E/zJx4V/vJ6Tb+WrBF9
dSNIa8VlZ1HUaEXT31NW8RPVEhtGw98RewfymE/4pzaHvWCxTY2IiD8QQ0DcfpSGaSVW9NvzctkQ
0/JAUuRhb22jh+QKiUFkRTx9HlpoHRtVshj1jktTMvE98Sb957udnH/E0py+UlI1GZeNn11yUZWh
xuri5pIDxiIEyGWr7TcP06U6gDsFBg6X3aOrM8s40+olieUBA9ko++y7YOIsEc61x3olEWO668WW
rJZyyB9Ki0gEMVsgw66MVLNbZ1hSFGpZ/XIr3PhhHhBP4t8/pvJ7uweZWYeJbmEwzT24eqWpi2Xc
fLMONA53xmJL40Z7QRlqw32Eowk2uxV+4AbbiLnWZ9lf/RayQ3fqHhyc8zDvXpZRE/jB2iT0THBD
mRgazsajhI/ZLjxSoO5MZ1OVaUjp0hBVusAWCyggDG3R9Ob/snh1wUaKE67HMgmy9tg5DVawGPRC
FNs0y92aPlTD4UpAA4GjVyp2wEAyX4pZrcO6tsVHKFSjDvjMIDSfZHfc2BIuSDUCC9R0FMjFaf3H
XqzZlvDEgv+I1fLgD8Z752awjVzkat+kXk2fWBjUnmOuVdIfxm2EHbddKQucmG97hBSU19r5qqpY
7bP+fAOnaL7CCkom2swneMeohRGXFm4sOd6RN6aYtxg3HuSD2lVE+7sIHKtqXtYj7GiwrCwORY15
trYfERwJ4Zt5WenP8GkIp67XoALDmP947nszoIoCe74zvWtPTjb4WGV1wXNM5RM17UxI69CDVG5f
XpRTO/rzfE6AaP9oWHwsjkeMoZ1hfuu4dzNOwqgkEWDjtKBclc1dlR0kPAwyptxdjvwoOyR9QvE7
20TugvM7BHlzNWEDn0wpulEUfp2Liv1Mm/8T8Zq/VUn5UVtlSfbttzzNWqhtCM6zfBVfANttdJpn
psWeTMIK86+6Pn8e7ke+9CBlULSiu6bfkCvZrRD91b5gejFsp9rt60Z33FXFurojBdAZZcZ9tAG8
E1t0EPIE/n/G1VOZVjZjd9F82m4Zi0NLzeT8/U6Etjg8CjPZSwlrpEyjgZO97BiI+B8Rm6qIN4BN
T6IeWdjnRN1c2DsIXQzBRDmv2+R6mKb3OOJAm8e/9w0O4/e/+0DOiHfUI1ZTW3xbamVSplgj6ZzM
W8G3EfAp9IkrIxIW3WRL3lrep+CFCZsrmoI5BwF8KBS3EIKBzG0CRfuQ89P5A11gBP1FPTKgDALI
q/hBUMB803vdZLHnna3kxBO8ScGb1zwbeqSp78Os2puiZzjxxr+/3Go6swuuhMf5oCYiocydqOhF
2cH6D6ey/b0GuJslaYq4+2/8vl6RAsk/X5Kq9phgtg9UtjC27488aQkovIdQ4m27WAGxIERVNsW/
ofCMyeKpv7I0HSJ+c8RmLIQtXCpGOAsRh+SL1/FBiy4pP9jw0gQibP0dUXKALStGRjlSgAlE/rwl
YWiIvcwQpBaN+pMdwqJ+23KSUFoLmuMxwRtQ0S2vsMtOLPd3e61IFrCxtwdUKUtLQKRBi8lWL0Xy
OdmzAwZpmih+2Inotzp4kV7pn4kD9Hk2V7gSTtBZn10dSPaPwESAS6RM+wUug6ld+KZ9OQV+aI30
8gzG+F4ISzi/gBSLfv6XM6pGh9U7h66UZUXbHC4L9FuEym841iBiD7AGkraDaYVpNdOFS4+SqKi6
BQtugGOTz8MdkuYXRrbabMhl/iOzL0etnp30QnFBbOBw7VPKecs2jXxutnl9sUIGm0gqq+Xhue63
KRdzkan/DrR1KriHu73MuoL+IeDM9QBvqnSuaYBTFIiVvl/oZivmYEKK97HxAaIgTmkJ7ZZm5Y4L
aic3VrM7gad6FYMqPSQZa9C3EKh/F1yjg1ddq+P429HBIzz/M9FWZz4rrpoWhQ820PdhMPUIbed2
spiCgptoXJ2fIHAHfkZMpob1Gfan8hjALSafgplGNQzRDZVhSsNqMwu948gcdJ6Nl6UYohl8LQt6
kWrKtuZInXjsJe46U/H2/x9K52n0MNPd45wji7/scOjnOMSl6upsPQWlaG+WZCasLnpelPV6Kzwb
wFs3y8FevmP0aAdHt8ZHOKY5TewnaCdpDN8gil77xbJJ6N/8wHUTMjJuOCo6X5d1nBEOJl6Aw36d
gmmRAROiourL73PBrKdSejslVc6qPRRLD51Hu77o5XiYhp5IEs/x2JNammTCbuZCw41O89SJID9g
ZSY9BmslTy00OOJDBEPToIpNQv3ev/e8ajNMqt9pZKnZZIAGKlAiKrDBdXl0a4GEfoyyMk/VP0Oj
NKD2AClmrusTzDhBzat8CFYOV9lGSXXK0Hn9R8Dd2U+eeeuAaO32/xm7XzgKJS7ZjVwlWCYiWw9H
Oqq4MDQ9nUWwBWTHZZl/n7vvJm54bZBlaET+McmmuX+ScT94ApRDc6U8pbiVUd3SBHKfxo+eNttV
wR7YjpfAKbav/C88tq+pAPc2qYxCTDuDDSjd677d+hfb4l82sj/Ca+ep46fXf48zQBncgMxUr0qs
oXk+XPHw2NgDwN9Z/loM2JWptmI0l+X1drgGTl2JqJMN0iUbO9SSCwNLgCuBByqzJCGg1iabtyPv
022QyOSgfntUxPtSy+vsZHp7VPgPH3SrZ5LguxiDzidBnc/HmSVL3xYfBv287Z9eK+aQNzRIxK/V
kK3M5EEQKXnzQQmnX/8AdxzWt5wwmmIufSo0MSDtE0/+i/mzxdfTABByi02XoY7L9DERLyua6V+L
AxU7CJMEuStU8MPAjLC7PaTQI8fyPOqqub9g746rLWKTCdTDhEpcldBY3DDwA4ngkL5ooAhdTnFL
1xcLLzkVTecglKcR7djlUduMah6fOmvsQSFoZgHP/QbaYP/YKLlzpjjQ+h2RArQwaL7f3tIES4Gn
H23kf3Gb1i+zA2273TbLjcs83ZWLWCmmaFrUuq7X0a0udIYtwdaHpwRuAQkcH0M0BOU0yFcKBl0i
/mZh5XBe5VHPiuNcHfPbq9qBJf08slLBV6aHzVAYIw9zZ7g3p6RcFleEIIJzy4/MMM1rREbuXnP5
VLtoO76ikkPi7a+XPxnVW7QdfzPdC8zAP308IUg4j57udc+K2KLTGrr8wtPBr48onFmvFNllb1+k
2asoI5gFf1qtPqlU8TFVP9IkoMRy5GjRXY5E52r8GA1CUxeplww4hRWRZbY9yZD8ve6dzMHOV+2Y
XBrHJroucRJM/saM54XZZBNhvSIHKvug4rUCDjvOLmySgCmV0rmqfY//ItmRK0RMtQXHlcWtwvIP
juYcfZrk9NR4ZNqrSpuE7zjQrNXmUkKZEScJ+axZ/Np6gjFW1LDK/jFGusycA6vLRoDgC9NXqS/G
pA79PtdXTIskDDtvtsbqhNFrFSHqmqKww3OPqn+p6lCmtrc0HXi8prx+WRnL+fd5kpGM11sLJeAI
f0Nmiw/UP0NODYEVkq8ril9dnwm0H+hcqMHV6pvoFA5sqWXYI5Kkjyjc5nM0/Qj54W0Xv7mKazGr
4SfjUFxXqOBVhND0nB/2u6nk5Cwe+1iMEf0A8DYnAAOGWC2h987IyuroyILORhRsCrYuwAXFE+F3
Qu2p+jfo7aklU1mcRH68X9pq5KQD8P/2yzmthsUon9xXb84TVcjA/vCiNx2+oPXO1tgA8hm3+BFY
kcmBBNeaL7vKfqls4dlvbYRNHPUc3IOwjBfUr62pspBG1U1OL/ACDBmy1hebj0ULbDf+P39rC8Ba
kRHinFHUOxTTjpZBS8bylIHqf3hn+XsnYim+B/hWn87RYjo291nrUdmhnCaiomyQOxwrON69umhA
alQ5KszhLwpQkdyR+wv7pz7CwOznF7AtAQ2sx6fmKUhtlR3bX5KLG5q11VcPjpFotKcquj/6c/aP
jVMNGZe6Myk7A0J8UyFygpPwa0WWPGW+mNHnvZtI0UbjT2yhZ7nYbamcDsycCiFuedkpwGy+RtJi
rwZcvaV8TJnquMultP0wI46716J/ck1zNkhUgm/JYF4QWGvJxkYqIpin8/X8Q/nhgwpkXq6eIm0D
RPD6HdvAiquPeijKHFea+9xMODpz7T8PiTWoMynPjF2bB8MmNu8CNw91yAu7V+lNtQkuhtPn5xyH
HStZmgXxVPu7VOa5tgoxu2985HE/5HzyZDWd68RqfN7i/35+HnAl5OQrVNAAxhxNaCpIjD2YvGzw
HS2n15TmIZVNGOvK8WLJibC08ceknvu8z7J1YmBETynizfSTHjSAetmXBPRIf3xWC7PzUomDrhMl
CiP0RpvLwsC4VfgBbUcQ1/2PTEHX04XhKIhkuS0B57lcL7JFWHlIRKpscrJ1SZmjUJbeJHLFgM3+
WmHj4oZZTGFf1GIvgrXLXOOmikyjyXIFPrTT18qgmQ+J0ANQIJv4QuT+lEpNw4nRvKI7hQHCnD1j
7cEE6hxHmKrhYl2zNrYYWfBrOupMR+FIf6WRWAxWXwp0XrPBl7Rp15Fbmx2Rlz2+95XebS7PfCKw
nWZc9Z+SiouL5BGAjociTL17tCOcuZaqss6dO24kSMtdt8DBaiILn6ctEYLofcgrQOW4cMgmCW76
iAloDwMwyQ4uK4Q8z8stHvlmu8gVRHIXmBa21gl/ogEUSH+Wu3736LYbabDdmx0WXVUNeebvj0lt
5WLUT7XJrng+cSyCU8wq9UdFDmAdLKM/XBfFQ9WRc3iYYwMDiJ9Ux0Oc0s3SRRLnSV0pWtv29Zcd
eVp3j/GyAWgRv8OVrWUb2YbiB177stAhdftJCwX4EXUkvFq2XdGgEIdPWbA6n7W9++PbO+1osASE
RPZqHVE1kyu4mOPv3hdYSZovwS7L9ymR3oC7fNwQWS9C7WnFrIez0hM1k+Kp1Skge/9fTz2dho5m
tSEt0SXhpitU4Xryv/O7bXyrHuLQRAXqSQHtSIR950oe/qn0tj6ls6Mw0t9kDFyD8QxrOk6xLzWZ
ZgR0yxcNhZWWqTmZE4jsqpYF49QzaBdnkqB0tLjMd+wA5/1BSQ4/soEjEDUctNeWG0XbZg1UvEy1
fon+XPAjW47SwMJ/OFimBzzrooC46q3AIwLxFcB+T66VZ9ML8txnH/J5iqpFXQSbduAncH8SjO6H
BJc9YS8EHMtqLTIWJ3MYa449nzLM4dQ2ifX/4Rr8brGzlGqH/Bq14S0P0STeuAdhepgDWtGKQu0P
CU16YAL3z0VcEeIZfFeerRI7iDNiTx21Q1GFXi1JJQL0G1sxV+L3WEHlmadbxjyo4xBc50TWmJvI
iRtYghocrcEvjaigGkbJYBJGyA2PWeb7PIPyeL1LEYyQnHMG7nABMVeH4/zdLKkfPwm+z0fcG4dy
Ewy3atwyjL/tvGFo39BDfh8GxluTYCskTLHp4yCCrSdWIWlM4VTCFiQLbnkzQMHh5ZFsQjHVN4lD
eT5maMqNCcF8WPS5AQx03HnCI7qfuiiDmzkSraantYB8aspHfeTu4fxe48KvUSZILYu0ecvwA3p3
EjHoVGzd+UIUdzWO7hhSqvKkKjEkAwHdSb85izkwxBasE7LJE2qY7A/kBCaKlMbGy3b9gtwJhBI8
UXPxSrjR00BTGDET5g1roYBJn+KiM1SMOOAu50y9BgS/+T69HmuO6DqDUEWnMMK7NLVMzhyqa2Zu
3I753EoDT9jDi0f7XezSlS5+MymYqtxju6SDAp3kqKjwacsWGaxfxFwQq77KPWHolgcd6J0cobnq
JMeDm/sCHUJw6BHs53XXlUgu3U/eftL2Ph0WbUgLk5VZtA1ZHBR6ijxkiofNf1if6jMfODHkRz9G
EVq1Puw1Sxs4O2DteTm/7+EHTGLwMgfr5bxorpkuBLF6LU/4LWbdSvwDuBCiyyQJi0jNgddyyNEM
gFRe9XIvNa820kbH9CV0M5j9sILHl+qOcyo2ArkUN0/I39TPEazvKS6MThCsVyXAJBsqAn1FHvE8
Xv5fG/05IOv7VrsTLjfTX/e1V5dqzzTrT5h0C2E3GqlswBHBsNwcfxMYFH9NJySpnUWfM3+W1/Ho
Vq4pKwbFwNZtNlHcL3H7eLn1wftvjBtIDYSVHiVwdc6LAo/3DIsIguLpYYf5VzVg8Yixae9hRA68
F5cNvK7b4LRhxMPGcgVBb9TexActOh+z6YB1QACwgbWpUEU3WpqEBfsxAAtlzwXawOOiZAo/64Pm
SQu3WHFo2Wbi0LvjM1zgp9kwi6tPOKlrIRGiYUZfp0Cn3CIqEe6xKSa/wDXUqZTyeVTnY0euApfP
liuxjx5LMCarSSJRv625Ui+ZFUSsz0Jrp1e0O1O6YVophamIqvO530ItSQbzYE8CSqVadbCtsEK7
74gAS9WSYFJKWVa76sscH6atNOFIP8EKsOrxbJ5wEFgKhPG9I9hkr3WjkY7aPmjAnyeIU16Mq+aL
yRIxB7uKl5EodgpsOC1tmRSgEJgbYZssF8VmqkMSNdW8oWq7Z5gsrq/QvqA5UYaRvsRYFG1A4Kj4
CRW6ttCCvxnZT3mH+YqbnXtqGPXMriqVWcoyyZJ/zBlxpRNZdjYIxFzvlj5q/mQNw1etwzY2uOzS
hcwraHrG85OttQ/IbHqHduFqlV/9OcWkl0eu86iy+ACbuxI/lFOH8cjh2CxR5cdjkVIiGIY6RDvH
SJVACUnXHxgk55c7LvdlpCcmgc470ikTDUCqeg7YXPulXutGHeKE/aRZsY33CISaosOjLc5NViD+
5eUCvBeuILoqnhrKpvYYlJ5L8pOoFl9/XLESkCgFJGiMgkvpGuGLwynmxvyhzF6JyOifJZQ8kege
a3bTL0v8F4Zx0mLSaqc3E43B/GipClaPUqpM4St3K0ULgC+z3nFQaxvpJztpLRLF8xKP7q1kO9id
Vcnc49p4WBJc2qjGQyphvJ2QBpQL1mJL92jiLcnTjIJ/fyTkFcIrr7wdNqtzq52oXdtgesfil3m+
DO0PWMbKF1KqzGFeIzA/+JrTPfWIu3+aH9Fq0+msZUoiqYeSYk834XWmRG/xefyStQ9ZXQg7xLK3
0vb56EekbnBC8v/kcnbziExbnovEXAmprx9ceo58KdghWVo+4wzycqnMVzCUfgwIGlhloP/GUKce
v93EQ4qPHu7J0OR//iXkx1zoSTdKu6EmIBFBk9x1Q2OIHEswgw+3W7u/rTAPBG1524LpCkg9YJay
fgiwn5RFbYr5aO2lY7eo7FFSadZWHXdxi3ycrV/orKTM2j2iE5JzyRtHQ0ccPdhWjgL5czjBspHY
X05My4SWNghrxQXdaR9NoSg4PqJb0NjoTPtfwhfNf/XzUgk35EKtxuBUPGc2QLo1KxKxJ5arvRe7
SkrzhWXTI17uXnSCNJDspo7re0O4JMJ1zwCp1y/gS80bACtydM3Ce33R8syV3U3t1to5tImj1UJ/
88O3AwCNa3Jq2bTyapuOqfbk3LZkX+y2RLKF5kfhug8joVYYjYzooH6wc47QIYNO4wjxXXehB4W2
aIR3gls1E87Nc77YwYQLFPNRqAM9PH7gwZ2ZojtQA36+/Y3vgvTEJzdrl4WGh7I840yZr5BXVVsm
6sSLZ8hdqLJVBFT0ved3b18NUkkDV6kGXdByyDrORvDYnYsQ7tau1Qib5f+b7k4+Ab0/qGpWreoi
lKU/1LOsHIrkj1bm88eE/rq9Fsnv8gZT9UGUy6TWwKlRR1XIEBlSlEGX/uP3ff1R0YhBjT4Z6+v3
twFjlpTZeuRg8W0j9htTooi5eYMx+VXHRBUcl/IAZ3czfrh03vgwqNh5U4MEs2t6N6T3lky35DaJ
rMEoCYyzrUZRVh8SngvN+Qno4Z0cwsCiukbCD27dKGGGLgplRoLpKGuMvu7RO+etln8E0RI4fiNr
L6L5nwyNiRB4T/mfB0V0aJFNsFdwsilDxewZXWGaMHjEoBHE9nP7cvjELhQuD9TYAO/AF2IZH8r7
fB1FV/qMOY1npHfhSsaL15okx0zb16tu5f+1LT232MTIx34GKG1dX/eHPNDROftGvRV7v96cJQSY
1R4t90ZVL7VFyn6dKM/zXn1slyckSyX/+4oRVGDLezajZhYMiH50FcwUnSn9415fswaysOvdmEKl
RtLS/rdzdvtWuTo8tbcPu+A0OMvY06uO+v4Jf9Yh+fypVzxOWHgpWPitLnBYnj6XXwr61J2NYWw3
46fTd9cudgCHSe5MBSR3ut92y4gdSU/5BysukU+J6i+SXgAQp9Wi9t8gDemMxWYyBqGyBhZTrts5
5uoqPSzdMjsZuGfA205UwvjXK9JyQ/EIvkhGZd8HmhPGeZnSDVvJzPnfFJOsvLM3IRR5Qua+0tkH
TWWu4fqn1z93ZgNStxIaQHUhUmJV29S1m7nOR9f85Rzo2NVGPb8itlBZ1xxRtbg0gQD1vRmAclGv
HSddhe+dk+PJQpMdaEXxDBId2wA29Py/sYNKZWpvBeDqKpqbBjKpB02/l9e0XAjDLOBORr390re7
cwmCZrEkSIqhWybq1/9QuRIS7E3ovrdyAKlP7YyvSpg+PD9aC6hUtL3PTtX5M/kaGUSbWzDCZyT5
2BcoWFnCfZ+hZh+qXc0CcP+/aeEsL7dAsDZ0V/sXcpsKbBf5Q0t4kl24ALnIzSwpokeFr4yJ2DIR
S9IlR/UlhYV7S/oZLnLyGiv8kzBorHtswE+rs7nwZ44aa8b43lQJD7m0FMct2KFghZGvJh3cn6pI
kdVgxLpw9xwduebT5ZGSGq5tuqtb3ZxjiQBh/0GmCSueJ9G1Akr9oDED5O0kgpUpPmzskOnk0H6F
rmWt3D4lW3vMQZ7pheR8QLB+MdNSNpPKK63yrl+Dkaj+/Oi/Qhdl8Va3mPvOfOxh9qymwea7ba0C
+Tk5ajO++dmzYMOLs9CoIn0QjudxJ+A/unaFOnD4xSTo9N83QjilicHUxBPT3EPetrp38yQQ3m21
sBtcW4XKtSqBuI+5wZInvqa6efDx+QDTYesEtGJR1IZsYQADX4CGauBNsfw9z1BqrAhHTYU8NuJo
2Wm8nQ3HYmAxglRAoh8PH+FSTKpGT7gqVnQluFh3OsL3hpZnzlRwlodlLsPGolOY45brZ4UIaJSE
chXAB1mqCOV4xHmogZ6t6KI=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
