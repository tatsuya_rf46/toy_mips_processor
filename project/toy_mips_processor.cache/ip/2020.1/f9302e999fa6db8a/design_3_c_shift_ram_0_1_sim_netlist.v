// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_c_shift_ram_0_1_sim_netlist.v
// Design      : design_3_c_shift_ram_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    CE,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}, PortType data, PortType.PROP_SRC false" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "1" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZEvbP3BmpbsEzNzKwXv9FfIdjnyEYErgGV5uZ6/x45t8rDORSuraYS7IefdTdRyF6pufcW9y7Oyv
5XQSBclEYkwRVvIi492OO+8OaLrEq/x2zHIRfWeH3fS81IUwKQxvshTdd7sDOCTKL+L6PIekfIM6
NpTWW4Ne+bmEjS8+gslJvV2MZekDCwimPb8Rn/PXP5arWs3Tea0x60gLUTH5GaLojGiUGzck5IR0
AGvhQgc3wO5lJB3oOtQ6NQvMrAMSkAM34REA2QN0uBXZN5n0V2SLqf58Q4XE9r+aX2Yy7wEx56aC
s6cD/9H8A8HmTMWgUkN+GzbRkjG1CUvKMvu3vA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RkokOkTBcySD/H9LYkaaYC9NrMv0V7F9uD4DLqQXWc/hgKA9ZRWXyhoNH8YjahdxW1lYiWKbPWgw
1tV4fXMlwNi5BtmqUfztHHNKYA236ShmrkKo7cbeEoPiTYVISyslj+RZO1R2ngC7g3PTC5XMiWCh
wDzOTIx7UmycavTRmS1iHCTeHzwuRYXJLOSHmuYDNBRdWP9JmX9KnPIv9Q9WQ0Z1u8ZeLHcHspuu
k+893AiQMEGuX2lJm6Ziz3s3FgxfUf4kxi12+fYt3yv7rHE9n4y7lZxuAFk2jN2Cv4J+GYMch8qa
9gQnbvcgt4aSYXZpcRFn066+wsV16Es7S77rpA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9936)
`pragma protect data_block
rfFVf6UndOcnfyNl3EUehVJyFk4/AbsMs4H9NbsT7zQHdDjwOgceTvosrjavTECboWdnJIHnGX2u
Ny/O9XuF8ul0EnlKkcLW7ZXdhMIyXWM1KSUsqNeVY5tyot7xvFmwiNpJzg5f1NbqsMQHPdV79caz
DKzntmUjQ1dRMAxiExukRdjhpPxulblK5m18yxNLQGI5zVD/WbYRHfV+l1ObgMFdPihV0Ddrmi2B
vV7uIMwc+GUWSZfNg1OcLNGfaexs1PkQFIPGOcsX0p3n39vmRdg41iob4NPn49jnGaNoouYm3PC2
UpU/TkJbauTSb1yFGAzmOqp+ADA8lyPJGcT4IR0R3q8PHmN2SGW1MOY3T/Z8iFbqxDIRFRmfc/AN
g+X7KhZ6XocYW4t0Xa4vDFB2Z6kVGdbMaPqWdQ3lCaSNptYPv/mF/fcBRe8yH7MMP0cIvavVieCz
6T2UGHDOZot1bEgMv4R0QBE3Mm/tIO7wzSyf5MZwWUkcAwIEmPiybnunebIUfRUR2Z5BR+oxala/
s3f1pDXSSusSEKgYn3zXtP+Qh0JxpiKsmSZcadnHAmhnxZ95EgZ/w4P2JbSMqbmVEH2LZHe92rLS
3uZManDiBj7MQufX4227rkHkcHkj24Qe81cGa/zIA/igCqqOFZRiJBBR4KNR5jjqOgdDuKY3xZ4G
PExhqvQ1ALyvfjAedjHFRv89R1ZQHV/Yq8o2JS1Bi0zaA3wmNVF7iMgfDjLtftfckwO0MsJD5doO
wJ28jfdT8H4y+yOH+cV6PaBI+u4Pwc3i5yXt1H1vLG/8ntYREOXnMbqH5VM2MLmxPuJuvt/fGbT4
vcF7+4G3/M11QNUJY/zCe4wL7XmipiXOZJamOc/i+92Vtk/jANGuTD/xDlO/HcZH9ATUF0wbmxIo
NiCRAt1l67Z6wPfyVsfQeqIccK8RX0km32YGWdfFHdB6nvOj0esBZrvAjhMhFGb50vESadfnAhe2
xmUZVeYUCGT3pSSwBoT0y9Zxxz4MOH5ONCJS3Ndq1NTNriMDst4V2dld9jTQUHtzjUbRU78fo6Rz
4ci6ZLE3DFu04rj3NY1nQwTnCWRHufJwBTxV2YZtA5GS3hizFjQr8GKNKqP5BGpgAwlzhkscGQib
6RU9roTsfloSMHX77AH31F+Nc/pFXDcBq7G1Ye9kgWPYE5fptBqYGoK/1xQYefYWy14EnkC5e3UQ
vYdzAkhUuj/5Fa1JDBVeazwWE5+1gEUgcbGZWd4qXu6DT2i1rFVLfkxCPlIwGLh9d57X6LDIgsH0
DYYipttvT0JSnEqzQIE2dqrZOPaJnlXSQquXYe/cExDFl9ATwbeUOXjzKsJ+8GgJldO3n1w4foGk
StoV4TYkp8CVRB8KgjNEacdwIG5vZEoCtBIuG0bs7BuYDyEzawyEG2g00kFnvhkI+3CxX2STpGV0
asmCGouXLER+TFIUI+XwIjQgXoGUp4ESNbUKn/gpnWO2/OZIpuf+39YVyyOFPSvxAaiAArw2bipk
ImVkDAXH0JGsDgGaZNu+YeboRz8gPJ22ubJGTUMoGUxQaAsl9woX/SF65TrbQ9KV64My92qSdBRl
NCCRiCKouRI3gB4fnhCr1dw8LkF8G8+wOTJJfuH4EAeQspqsN5nwPzmI/C8btcug2zxnwgevDH3J
gL051Heo7o+1fBXAlmmyTReko/SXG+/DQt7el69zH83DIw1JhRSpY176BC6hFgr7z9ZzibYD44Hw
yM6FhrAffe+bXQBh33ap6bLw79DBPqZXRSnXOXa/ZdGoLVZl8faXW/V8VTvfvnV+hGnVMaySMocJ
dKaTASe0qHmonZ9aFaPcFHv8hCc6skp838yG3j7WfJ0qUbVMvdMuzcPcO03II1+JD2PBTMuYwXMj
XU1HJrSicrgjXh943HIUwGRndWzRnHtyOP7aITCHIsEmidPV6tMGQrOtooxyRrklEyWADEAgD3cp
UTDd1jCxuRjqUB5svWfpcqRpVVwCAR/WKoZIUj4+U3J5EZUU4vyjdgUl9eWYK0Jx6kC/Ix5rjxck
EWcmFxcoH0EmA9ZXjSENkrxls89kb9YbtI/Zy2WwJ91CyQlPNfOS4vVy/jvUWpKgNnehxVxcdgL9
SggYG0v/G9ZZNg5yWITs/T3Z3w5zaynhQvb8krZUZWWrsW62icMpHvuSd+PXI7jd86Xralb6bHfx
xEcfgS9mHvoGvRx6OHA8tV+66pUNYA7oSBRln+3KPXCQFp1i/0rmG6T28UfcHWc8F3sRPRUv4zBd
hc3yXvhhmDMbhrpHk7JJBHBNcM2lve/J6+6NxvlMpN+CYwAXRlnrG+uDLv//XS9wFt4n9ngKVqHJ
Klyv/eK5G/eH92UJq3QBmZKwSZuq9fiBj3MqkKNMjZpQ2pDaqhDqMZxi1SOpXqlxwhzbNQfTuZ5c
yBo6tD5wfIIjRtvyOJ/BCovHwXthj9SR4aInyBmG+iYkJNCaFJBB9TpSdCHwKvO0tEoPxed551Cs
d0609IUwlwRexkW7eITPK6laI37hy0VfPgyHheytR+XiGq5+v7OH6cc/LpRS/RcIY7YZC/U0TPIT
DC1li/BQgKwW7mt+izxKEmbNa2aCZhOvjFShQql5VLl82zoRoM6x5X7IxxxTP2rkC8FUd2IexIfk
Vq+LejpeOpUY3HIR6/jfnr+pOAhgJFXVx7Ff8dz3/Hopp6shDct0+IWJjbZqieAHp5wsqJn2REnQ
kzjweWj0mpmCM/XhXGYxN+HhHtzMkgBINQNZ/S2PHfBSLhnR5FIy41cZDkXu0BnGs54wok2Ppf7P
4qzglP2sFRGU8HZC809mZn8Y+7C7SaVr7XsfxqsXyUI3BW7OcJ4iaHr7/nMFXb/P1FVjqeVRKQ/C
o5n+H412z7Itd/ewFuXTS1veSq+kXFyDuUskm2EBkvWjPGiThty3Ub+Uu4f7ZZ5QCEXJOsRZ5szp
01Ii2xHaQZU4W2XeJzxlWUn+VoYOy614yDFghFsASQYwieAyQrn4enShPJxBLv4vlj+4fiF7eV4z
298eiuCPTL3N+BIvede6wLsQfI4vRVAqxSBCrqpJ2Ir37nbA7RoFOjRApIVeQ5cVGHod2JflaMfM
Lvqqy+7LME+zDz4Z2kAxZmDr1lmxgB1eng0aa+rbZUmjOJrWYTBKQDCon/3EdJnlGyCecNZSxs0/
49TB2/LGuLSQqVqj/N42ltBkRGtqUEoZ41c3H4Ojejjbz72Acc87RVQYF+Awtb25QToCwpV7KhME
Sve8pja5cvvyYyQ35EBekaDqdf+mQ1waq9Bffksj+sJRigm7ks1Sacxxv+zKrVED+4ukcjquZkAE
WVzZwNgOxBPVxhu6ueRJ3lZsQWGQVuRAm8yHGJIjN4gFQXqI+3RVwO+cCU5zxf9LCCyHMz5EtDHQ
yRdJrcbkoc9Rx9XcL0t3YMd0r/RoxYigd4z4mF0QUqAkcJGXAZT7XxAo/rgn6lQpEaI9irS6CbSe
NtnK7+SIBk2dQ6l0kBR5mnl2lkkWqIEDPvUDSaT03/ifVe0au/9Cvjka7z1CT/wFALghqo6V3A9Q
Ds10QECtNxVVwGkST8Rp//Yp5oyf8IovDqa6hPsrZJZl93xoSBB2yrNM2ULf3rOcQTh4/AH26wx8
fS6cl563KxsOcPM75whHXLjTGSfG47cLyBuHZxltGUciH9mZR49/N/tCNkQp2JnL35vPD4l+fTVW
2S8phYfqjhVOHJbfoEo9HznFZeQIAJ56GsQqrANC0oXOJCIdgft0XKGpEtf9w0/UXPD7CaGsVHgA
L3XOyf+XHsfMV937sjI/nOWuM7CCAxrxwa6r0jb0TH3rPwos33Ixx1MYaspPa9RJyARw3KMuTAp9
Tjvl+hwT/adgupps+R6hK2ShkTHyc5Smr0zyehpqcgkGpvMqyfRuxJI4mkTEwaYnFFcJdZEh0Ake
IT4voWHRFpejjDI6IqreulYXeC5b1/f9yL+ZuSnGSX/2L93WgFncxpry5YPCQoDnbQFEsFVmnSA5
FgOHlDi4O5mN627jk+xA9Djc6TziqyuEe5W7fmtNSBzoaprmdF34YCjEvZ/p36RIWImDUD09VopB
APvimIfjxr0Fqx41GEy3aQRnsZMBGiG1y5am0IO8x4HQnEuJUfH2m8BAsw3BquOOmE6KLQIxPfdT
2ARw5SFkK0aXsrybRd1zQyp+9QoSlkxxED+QHg6tJmU3dCkKL47/2CIFEvv7wmX4AxrztEPyutJt
WX/qjD+psa4wTFk7BCpiyzz4sE8kyw6PXj/GdF8n2u15kTBr3SR4axUbpT1ATXFHeNA0zNAE6E81
tn8c9Kv0u81A/fY9FcF1BrnJet7UUQ038/trRwwXxAjwRmC+3I/rceC/O5WeNspHdCwBpCNjKOup
Uhd/AQCnJJKBqWqIFowJRMFOK070qs0Zpk9RGCVXFqt+/wwg2JMI8S+tl+lbyf8o7G9n8jssowfb
Dk8AtEJ3wSH4HehurSz73nlOTQpWI4l6F139I4YfnD8LCLOBRY+sTgyHG78CNFMidN9gvlRG8+bj
NLhMR/kuSBzOjLUORgwgXfajjiT4GA770aO3lTBLXVO9tTLqIT5xsO9aRSyIsFzIWrIuJC961BB3
r6m9PKA89zFuCg76uJdctfu1iDNCQHXhhlHYGO6Vt2MGpS8rXpv/GvaC/pniX8yJGRgggFGe+W8d
1JHTg7Cyy81wzHyxYyXED3T4XYxWLaBiEGkKIbZVZtq7a0CaoP2kB4gh2rq3vcV8Q+meLTQXBCHG
bul+pXIjeT2c4pUEy7xKSvSRVFWrAU4a1A7SzZ6StUAq3bvnd2sV0iv9VxotngWAaQ+gExN92PRc
eLWgV48l+H1iQlHn8XJY7xLki4Pkav15W9OawMuBskj4uXgfxj7Wq8uF0WDh96ZGLNDyUB5Gl63u
/pAJ/p2fwzM0fAVwD6yxpjAbzpAbHJ8z949+NU7EtQ+3PzPTypHce1cVWlC+AtYc1qKQpjVx+SB5
qbnWFebP2iSIX739zyXt3PlxBtJkA+pjkwvLVrIqjG90VpBFA5aEWXvCwBQBT3BebcoxyLINaCw5
9BwhdKRqyMl1EPnmr5gz9amAqeRo7EMSWOZF90BKriP9eSHYL/FXiJhUKypMDIYApjeMGN4n8fM5
FWELolISJXg3XLQjMbxdc989prsa8BuqpcRP0cU7bwdqdzc9sXVe9lroMETC8bfetsZyngC2hn0r
DlGIMMBiWH7cS3OtzFJi3anKVvGcZ46TEm6W4XniG1v7lw0OjhywSgjdmo6pjiqLwTtdk9tBVwXW
/WSUipaREBpByMPySxLp5sAZYW0yAZlnxlntYU7Wvn+Zb/HBx91Nd2SAqI79K+lqNEize0ndYwHb
CgyyTbD+mKVjUEiVLprZuFIm1zuiwYML7ktcr/hI2FmfUBsSPGuq/riEUsPlMeeCHVzOoZSoObuz
SGY6z/fxkBsYIkjTd1ixTImeYdYSkqQCQ41gd2IXEgmx/stWncVoovihPzRUMb/L3RZEFJhQbTgo
eIRCBDlcoIkpgePE+X4ZFQDhYqxxi8tL3/20VfOYMrEUehVTGozmG9atYiRnGZOrvtABoipVwnoi
y7QYo49rvstEumFc+G0wPOIoUxrKrdyTI2M8VqCs88YvvHTaWpdkKFG+B2kxJhrbbB6cf3W6E+EB
ddZmP8iwi/pQRP6rf6MohL7c2rPmYeCBn0Nl2yj+bh8cvv9whzzjoyyhIyQSLw3BPwfzsPK8bXwp
6P72xxHye2zC0LbfVUiFDVV4gfMpw2fmUDCtJSvG8XfP5661/hj7jetQDeXDRWuwTL2v7lH2IwGI
aWKzvinfqlPjBUOWHEy+ZlFVEIwecCBHojfEBtGY7/pks9C/XEUq7mWq60FaXXjGwUOFTJEtx/ui
wTCHO3C5fiCrUP03KD5UhkgevRpWmIsqArRZVTCFItcic9S5eQB3b4nDFAHNXrxu1MG6fqCiM5Pu
jJ/wsSAVnDUBLHgZ152kbx6fhaa61BhF63HWEIDNUaiNS+GgjRLhVrfll+lHvIsxLvL/RK2XKJkM
kB9vCy0oEYpW5SvolgfZXieJy3Jj4iQyrX9KXBgZ44jND3xo0p7YvEcuhw/minogNk4VB208siSH
xcP+/FmazafnRTBFIOAC0AmN0zgBSfg/2Tgn1V2umEzXWpUbU+qe2XyWi7GocwbikLhU+LCiIefO
WJwHqPgUAIU+0eP8EM2DzYchAF+HI4G+jyK85l2zjPgx3KuP4m33EHsf2R+m6kHhyzmro07Vnmiy
CH4sWeQR7CCWtAPBubAw+Fz+EMqpVkQ9EpEwemfGnxYmCCEy55Shx+9dq184RH+ZIqs6mvjbFjlb
bKVWv4SBCa1Pi3F2Tjj8d8HGtA+P1Oqc+umXFp5ZV/cdczw7463UkYdptZgK+JHzwm4gTy+8fSgx
1wMD0sbU25Y0dGNOtmJFq55a9K8M0GXDW4N8K6rOL7HxQJYbLpsWV5+cVfGHoiiN6/fLXJgyd309
acI66Ts2Ig9ZY3M0n1aKqdVqZuQEHP9YKztT6libuE9dlGWsmMO/agw4nfZYQiDYxjyppNoCKIzU
+hz+32RXeWeTyxDU2woqJzP3krUwL7otVkFzQT3Wwm6dOlybhQRD9eMHcKGvBTnHwJthe435mnHn
PCIqnyY/nkQPKQzy103fqWxri0io1DeOyLWyhNgCKLllN3w/Avfofk1f5JDLs1wMH7hL8UUADPnE
2uE3/fyupxGz0eUkTAJ4axhKrRwszt/Ex5dTj0TYdBr8dCVRMljgmIBbg3kXx4cD8zjIdIKnmAYL
3jqGExLj+vl5v/WRJA7XQugfxYNTvdO1cxYHnAufQeV3h9wxGjkx5ucv7ZVsh/2C3ZaDUc9vSiYt
shsBmb0rDZJeQrnNoYO8dePyiOpO6D2Kqce4ozqOa3I/PoTsZdEAiBqzPMolUGwAYNoKpuCuHaTp
DSadOt9zT2y1NVNBmbwO23py43jWO195jAqAFQ7c38HTXcsOCxPeZFZddScAnSz8vqvtY2E+uTu3
EXQE/uY2Baj1Yk+pMvDYNfFuxczYYjif2TWnLluRtHS+B6zhXWfvgRbJI9WgHKT/q0BGau/MKE09
pCAgRB8TVoicpGE6QtbD9BEs8UJIXieCkTiWiBV8uHrlk12X8j5kYzjtA2VCCZ1MDsTO3X0DbYD2
xWYbrLGix6lMTEXCdPuow/hllY3y4ymXg4c0vGlsHHt4krHMT7ULJGxSglWmmku2GqdlPeR7UpsD
nzQHtxqoyNhhXLfqiFE2L9/oO6Tn43UBQu4xg65YoVgQTLVaRAa91wMs/cCPX872h3QENuisqUdb
xB/RfDfZcBWeqYcWu8H33anO/VycetjqPMxbTUNbWuzXOeyilD0TzrpesHLXUHIVWNrGDyEwZkxf
L78xfMzve2vlrmtW26NIa9v1pB3b6r9zHv4Z/g3xS1LPuIa04llCdAZ3e26sxaOK43NUkYkmUX+N
PMa3Derlp8q2TpiZg7duWAkMhCWFHShuRlGoiUq45CxG9aq8sG/pYHCdjzRKHYVKqG7Vxt+wj/Pl
Eq5/fHEkHEDT1jF1kOCm+ycuCQTBtRhGXDtuxz39svGJXuDSCFaSQVXA+rtFSsLWiKR7rNvmPqt8
0SD3czaxfPi9LqIGUP8Vq72s3Tjx06c9QzE3fhPluaU/8mdejEOv88sBhuoIRj2Cy4YoX8gSRr5S
SIRWjGdfhU6tSQUgXR0RIuel9E6UjHTOMCh8YMyjYR/V28Jg+OFb0Hdj0JesUfW5gTX5Eirc/A0G
YuVsUGHjEI4qgx7VSZgNHUDgXVX3HlXrIMoldGAaz46plC0u9tb9frbLY0x+7UrXEryS3Njh5EPY
SU90XZBMtabk56UsD+jt8eKd+ozprkEShiK6Hx1ZdCgzrBWadSSRvxhQiG2SrP29ttnm4ZyZ4ebV
2KZG2CuTWm6ejAhGTeWyf5TGFQy6q2NVyjGhZ8RW4EdjxxHcRJy6AoDSYdbtd7GG8mU0byZCK39I
ugjIu7mvivv8MQf84HxRLxqsdfVdFZMPQhWzUoNVHnGQd0g3erFUcNe0Zu/aah6ZSwHOLmxiTKlX
AfdlGkp3FVY2RjJjD4FMR1jrLunAGSsPaf//v7N/DIC2NAOCrmFmZ0HVgXlD4arirX0tUGLMpmkm
V7E0ZMAKVtlox/27E418AGBUSaDJ6rN08TNctUqRk9aCMq4c9msQe1rL4VovaURla2gpY+SnJ8Cu
REFISzeHX+A2gaO90BYs46Lff2P+pBEV8Y34l/rwewm0knNCrHkHmyV0sdaM2rOO/aXrluQK32o2
Ia+m6neyafHDee4rPJtGhSq+Ai5laDbu11Qcb/DHUOpTdtfLAuAGNZKDScJYfCggOLhHeAM2k2Hj
4cjTcMdWCdtq4bYlOnQWwmSOWdO5v/js811F9vqzlrKgFwRA/1ejXaSTDMFT8IeGirdpogX0gYAf
HqrXGQ1e1eIzmmXByafxhtF88jtrJt3oF3ap4qM/DTcwDrrEqZs9oOcw0ldNmr8tUoI9rACz5ZNj
oj56XQU3f90GD/atzupI/13N4oHwILfSEvHGUlEWw/5nd3O7KAiRX28hEkjMolza35vc+RTM7iJ4
HbywlAC2s3blwZxgoFWyIAv0Ckpb2LNK/WRomfKmVxgXd94IZc51XN0j+dEY2vi5kKSbq2oRwwXh
ZMAclNazV9Th0DiRldY/GBO/VS8XNGpML7ZHRGR6BeLtZbLoAPS2wrFtP0TsQh8tQ7ccRlxLLeGZ
YsohicEjDIXnc1wkwKc2B9ofgDLnqzRcHePYIXMmSbEzNAV+ZzG73XqmgSzp9+CXSZR2XYu9CbNv
YoDHohH0iIFBVIAKB3+8VEkw0Ri7ABLIsBYULh7KoDhMqurfTwskzJ30/sYIvPQt6YiJxSdWktmv
TjIFa8r0UokAjCWtyrciorg8pM/TyOXZFoQFbZskgNhuUmPC5+gqL0DbsL2R0HiYJQljYsdXQH62
ha69yFIC8trHKqzBa0fRyoFB1SLNn6C0SbDSkuXbUMtgliQIFLHekFiy0dpYvge+Se0EFNvTuCD2
jBIZUI1WjBWN/v62hLgvgX1/TbVPf8EIy+aJ9pTapmB/vEkpcheAolPJBlJjQUWkugmSaVDNFjeG
xGtVG+HJ+cWxX5IxkrGI07iPNRSjSYsOZ8CsX4j5SlAttzAkOrCd3CDZzKtBYWEfU87qCwbA33Th
DvYVUacPfYA/pubmk8UrnWz2j62iCSAdCKN1o9q1gbnhay10FCGj5O25/WpUkecZK7UN/go5fdXr
+hrpfCsmW78A84YnZol6592YoNtXeGUzYkGYV3Axdvs5e3W6hLBxwvfBE3d0CXu/z8JQUVtWIxTC
wLrkEyyDlRlIFM0Bl2tx/7oYlBCGxmmu/2XyqVsFP0Ukm2Ih1DN5gYZslAtu75cMFdAn9iinivkC
4citfMo+o4/hYqzdcCEkHDy9fBkChpU8VKuD/pYP0pK0udg0GUbWtUpm72hhU/Ie5ESyowjNP1SM
crym764vfEQqTYbr7dXAz8iOaOZBwCEzhwjH4z3HGMd9Ok2Qsi5pMOhUmvDY45YEnthMB0/5+Rfq
rI7vSfFTNbMuzYO7D6qFmnmVvHKDHFXPlGZ89w+5ZhnXi9ny8jy4LjOx5PBOlO6qQYo516obZr/y
AkFZh4m65RwWqu7FPs8/TKWXdEKdnQG6OeHs7dSb/RH1JgpvEe83hDjY+baHI7eblVtXEHeBhQaw
KJ7HUgyDm/X062x36V2rYMtUGimRlK7sT2zzdThLsjIbSMYUzUhMkdegUmori1XDqLl06xH2kl4V
MtJMp+VmQaYpeIA9WlBr5e5mZRVo6IftQdP33Urj4gRzTBC0MIQ+8yyDCfMhFnQDDX/POvY/9f8E
utkTyEuR2146qAu0pafNbQGr+ncP6vpiMWAgrjQT+XLo7+B7Bs+yEsGIpWsIqdCsByq/oVuv48a/
elYiUXQEEIiiah782ZjEtW34UGrgK+457UlhYy6HpxSFJwoOoO/QpATGr2k11j+MIOB02N5IkYxz
cewIO5vhOxXJZur6UcGs6nH17r8IvRpWjpRG8JBbT6tZKz0X/ydda+gowIH4nhr4bLAPOzFx0kyU
rH/gLahnrHFgrTNptVip0ikmSdy1FEjjunWxqaENtdJlXnCuoceXmE/tV+mZcx9C7tYaDL9M3Tc8
jUnbKtI/NjLuQSzzxSTtSn3+lg9RElqKS2gg/xwt69hf3GQ3yUxjcxmyMKp2bRSx1YwfASVFqeUl
t2V+iCRYkxNcvbM5NETw7uVnmvL03OknA5kRhQu9yGIb2TFrGg32IA11dFAYQKNUU3OBkfpnTTPR
GuAl6VzmLhvXsqhdzAk4VqRt+S8YkEidhgltvx3Y3HOscj8Wr9QINPOeVwOS+vK2RaVrLp+VRvG0
b92gQREXqT8EIQIJJsL55d6HqvX3TwXv4z7g6CGR8LuW1wMt69wCzGaN4IxzaxupH22vp5FqRPJD
1pwK1mrpQBHoMsMI21GeFJAE0F7VbV+o0sS7FSBDvno+TLffNzRwk72C/S1ejMMqxsWtzrRPt10d
yaijcn4FBf1Y6l9mSg87GHVrcRRHnH6hR6BjmgQirOCqlsoyEoI5XDf3h0hlK/pD76dwq5xAiZae
+fVSV6Gh0Tdop94Wih62So7bDeX6HVeC7G4xMyM2O2JJ3k8ZW6BDomuIbwWcp+Rwl8rZywwcBaCB
JaxOt2/BYboc7xoYfKDZj+k8kJPOT47mGaoZ/hGI1wQEfKIav1S7mCotIkANv29uc6C2zQ4Vr7Ve
epZWF89DBRqn/5mISLAgyzwFIjzE94fWScKp8qYte9HPYCMgAqTARUv+ENETzitlUgAmLYTSIf4H
pbFVi8fUOEMTFNRzEpJFfi+qtYEjQbJ8goT0eDF2uEP/wdFsDpRyBKIWJmPTAVZMjbhwHIeis/1t
pgBhTbie1MdwVOXDfcxzsW+Nt6XpkVX3TSFHxrrL36HD/uCfJsjuiYd+De2QJsO8+194pFTQ4UgG
UN8Y0BfjdADII4esOON9NfP+KjPI/gYz7R0MPkGuNBMY+VG9k7ngaGXWUB6xxw3PNnIYOPzFUTCP
slY6A4S9PPTp/PCU6umsZenZ0W2Sd7crewqvU1SozfldT+nKW5upAqpWtFJefMQkD86cOv5aj7Sj
DIO4FxCdt6g1iPg16e2gU5IgH5R7XIkdmsBs93/kiFfugdzW1KKVWg9l8p3ELk1uZu31tmkS1wwt
MqwjRYZtJF9qGu+XeUnFHd/RJ4Eck9nLsswDen8fSnb1hUcViOxSz/ehF0aTAWaOtrLvsZSMIJB6
t3OKwMXdGJPZvzcIJaOImMlV5Txw8rExh+x7zoat8jUudlDS8MGH3RtfcCpfsxSyMlworvMeYnlP
KCrc8a/DG4cnEg1W9N0C4unoWtVS+LqqHoy8EuRJ0x1VpA1SHhqSANi3aiLfyQ7S3u/cdqTKi5bZ
CTpnl9ZcUXGQG1bnlF3jKNrej2WdaLols7/JU8K/xs8MHfIowJaqY6w1vNtFABje5ar9rohbQ0st
m2eU9PdNW9InoDXh5dKnRnh0/JdHfbhJssze/6JVqCJeJ1u+PvhEalstWJD3E3QE5TSQAsFxXiA3
nX+z5weRIUhj6Ar8gtd0ws+jMmz8JKYYtPlx2ZGcsS1ZsBQX9vN/wlbatryjSbrcA+Sb3GQkuwmn
WCEO6S/oBxsh7QzJ9C0k00Idao3490ihR7AZZDnpMLU8fTczTrHziZ9g3T3gkOX3n1+j5GF3U0KC
34aBA4096Yfku0qCi1YFwJtdxCFVYTaiMsdIOXxeOEJADsf4UCxJbtu3Y2E+4zMa0jmhr1YmIFi3
83yE59VZJo9y90cu2YDeYQWqAhveJzwfvHfudkxp1IU4RzafhdFj/hfIPvoO53dfzKEw1zprftA8
LghPR2NdhRNY/sNRIKRUK6v/fsMT2n6x5bP8elmzwqZ6enwNRGrRpKs7Whknn2mVdrKiFSuXOp6u
P3Ce1zASRTPmvpq+Y6R7efsgXTIAprwNQ53NIU8XzarmXcm6ogI6hhVakEahYTRbQ3+9h5fWD3zE
rsa7BYhfBJHpvBV+0iJoY7c8iuVPjVpioXFxDgUtBKMYm7oZnb3mp4I3k0sq77nMeHlLdVoETyZ7
Y6mxBDndJajGZreKciHHYFuLPNDMFU33H77fZz1D4b/B7kcyii7Npni62w9vY8dEZvP13w/QtE7/
9HZFRDWJtI7xHuvBfubwVNrh2HyTOyyK0sa9gsAx0ofCyRVm/0qFvdaw/OGnHKSkg3iwK4E2L1Zu
Qjhn3M7V8Pz4JbZRW0kk71wNw8stdxuDb85wgIY5bdf9yuNOEFzJaVVNBoUEsSgGWTXPrrVYUdbC
AKgxKSz4RZ9mnMDAOoGhqEuJdKnGIZRmVn7XeS6NSoimiXXSi2BLIu5YCb8qY9637odcL9EWfCNC
NbD+P5Hfs707+68bNTas75cq6/Z3H/TL6KkrqOav1SMlUN0JfnY3Y4ZAYo/Mny410ZS9nUxEJuwV
no0rxQIKCBu3uKCXDVRSV8zLiPOG0gINE8LxhLAncbmT3cRiPv9xYgXDurHUplqRYqCoM6Crs6jA
s6hP52nmNEHzRZwO4c7ch4gy9DgEP0KbH1aam3f/39YYfLoK531tDnxscg7jzat+ot/WfHzRcaq5
8JPbHrXoiV3xMy89x+x6poXUSC8rvEZTkXbX6u5oxZ7+rDcTO9N8gGtNnSWsLZVcbPG0Grku/7cD
HjoOzfQ0/iqpkuf9+SLYAh3ndSCFbnokZUnloACFatxnfUEb8pX2LLpc6qSzbd1vhWXwhdkL+KMZ
YDTDpaYCoFv4TpMrs24AHOAFoH7FsEDS0TQNZey7ivE68Y21lJloSEhwiokf0AcOMoa0rhhHsLvd
eFt9ndol7H0cQY+EcyAA46E4GOP0zWAkq23z+YkBIZ8zFNxUj4hTNd7EkgM7RGzSxC1ReuNDpRd6
hnYKiOmRJ7KHNa6elDAgCKQFOB+rylFFUzzFTP/Ch++RhC5fEA8g8Gk/t/j4VUiVyYSQqEPcHOjo
uBGE83Ex7Pfj4XsGqdehu+I8wHccwJeiD+9dKpUA0lcK7D2o95xz+YAgauM9ExT1NNB9p5lrhYIu
4/axptIB4buoFmCbtUidaiAO
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
