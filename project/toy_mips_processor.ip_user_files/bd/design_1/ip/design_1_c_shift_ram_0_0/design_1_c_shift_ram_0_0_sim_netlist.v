// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_1/ip/design_1_c_shift_ram_0_0/design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_1_c_shift_ram_0_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_shift_ram_0_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_1_c_shift_ram_0_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_shift_ram_0_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
hy4XJIo/FjDs7UhW7e2LRLzuxxIRu8dtL5HYpgygEGk3h4sXZxuLyLbcCcC7/FWQti5nJFltUgta
Mr/keNqd6J30a1PfX/mT1454ctLc/fZl3mrcErTcfQ9vg6gDgNdvrUNCJa1tmxPwH43n1G4sQHBv
Od8WXfUj4rkJnZk7Skskf2vgsdA2jE85w0gC0di4JmQrn/0s685V+wjLwsZkKWdtIC8JG9v6pRhD
fYRacFrepHkBWA/hIgE/ugB/smVzgIexzilZpHlV0/XzosMMIfhlSuxS0MpwbZ3DSoqy+2K1mTwQ
coHHv26cuoF1Rn2ajGQnZUrZJFv0TqMeeZv4nQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
6YeRzDUQX//KJkD4G19P0kvgyKZeMJLAKkIxtU1bPWnfoqOO2qCPvAoj/Gf9mbFX1hKtlr7kURek
GCTCea9rzwQWsq7ppF/QrVX8ugqznMlRb1yT4NmGkIx2wr0WRvEgwx/edTAskOIOqItqrqdqxpy+
gMo/1JtZTsCB1GbUsbT0/t/CE2qfGONAuVIOdE3i6BBz5wrYhbhMckR2P3y4caIw8qg0u+fVZ9YT
zK/8vreLmFD9t6LC2xgR8JGW9joaUpk57Rrls8WlDUjSqzHmlzDJjj7Vkl9tEgWsNJKH9DH7y/R8
L3/ftBkquKU19ArQba1hWDwCvwu+QDwx/L8NbA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9600)
`pragma protect data_block
FvRJ0Mr7CnNLV4/trYNxuyrcQJkqUeaOYMcXSknu1F1N7hbdQgNuHrgbkCKEB0HBHbP0k3430Nn3
t+oFn99Oh7p0/ChGtnDyEO8JzjA+WurXGVgcSzHm4FwlsCj02Of0rEpWKM8c9Mkc9JXY0yFgnIhG
3O/GHpH/tKBnDDjYT7O58K8iVYa0XxIe6ae1pkLBThI+CwusJOu0HZHNGmWQfPm9+OCYXNGdD9dl
kbkJ11rdPX023zJrAZHzF2+yObRa0YBco6BTsTaYwXq76Zoa+GnFgzpRkLj4XMFIsFhzF2RSAu1d
Z3TTgv6zIamsukx+MQu8cve+htIPljmJVQNHxRAN0mmjSWIdyEkG/wWmEy7mtv4d30/QhK+ejjLt
+e8rAHu4QggKhwCoS55VK6rMtAd+jMgG9uul8G80Ck6o34AklrvoTs+euonz+dpkQs73WV8uuF7t
jtyRFz85VBRRQ9xpkPiOgCHCGWbIJ+bTsy0sVTXqBayrIiyHXt/rgwk+qad95Dl2tKVPJiEVReeQ
Mt0z/ObNKLflcyqUS9/FF80kYPSKmg/0pyzXgx5b0Xg45PWLCQ+EX7Si5q7FjdUIu/YqUw6MOUfZ
pY3U+6IKSOYqQ7lTVb5UoQ/DYjm3X5HRWAotRupNcOLn1UGUwr2j4HgAZkEPG7AiRSSxvGuPEnIJ
jdS60qmiUltNrXEeKCYoS15AhWEFQmErUX6fEo1MaLeIFZMSltMAmW1eF5fkAwhX3yfkLZbhYLCJ
YsXIjvadQjTP+6usG7eSXtaY5F7bVv2pMFbhd4fVMC5eg+kzNm37OtjR9+ZFy1gmawVCZ6O16MmQ
PLN9nq6upNY0Nons6lINaYrjZAjBhCHJ5f7b9/9tVG+CTjjlczVGjVxA6SQoFyRDsIPYAGnOTL5s
JQw+97Y7oRih9+SGoPwHR28bacIBg8GNMBxjt7qIioYblb7nnmXyFGYqW0jbrBbORpqKRz8/I3aO
4HNfl2XVYY9Yb2yeJMdEeWH7KlBPVEgVk3VNhw2U3B3rCRttv78LaLQoKfJHr0uxOzJV9tyS+9tY
GLlK2Qytsqva84gSD/nl2VpoeTrop7BKzySCfCiC7nQ+gITjo7tJoljo8aRxLfYhDJPLC6xuGvvK
dCeB3CYalad3/zuuZk/384lCIkACJTpefD9yi8XQ5VcbkzkrhBENfnmanSpM8R2EQk3HW5MNCLAV
mknAddNthKMsaTGb3ghvWWLoYqme3oBayPsosi5ndVIOwOB6N8loG0LxjIJwawcFmkTvSNUWwbu8
IGkl9w518EkAW1n8ybgqjaUQpwOzJjJzyrPTra17uKM+oInE48kXq0Xjkwry5qYMsUvrir0TRHqM
gyGDwGlSyD9FRTRx74WavmqyfvgBMeGClNaKyCLsJOrA7dKb6/8/djjX8FHNehmuvNTBmpxdGkwy
FSyzyDCK09JUPYC8sAHnmZG8BBtccKAaj6ikeBIPuDi+UTrMVcXp035zTR0m0nn0pmz5A0Kfp4qj
Tks2gD8+gF9n2ia05wkb1YfzaKGiV/dvt+spDH1XSa/iMNKbc026ewkDNUbzV+PO5/hGR2IfOE5Z
OvzQz0A8vUqjMF8WlakcZvKgmYamJvCgDDMICvWV4+/eEiLwKvT1Wrty4aSblPo6uF0HZiUwAG+C
pnWRCxhaRgrDH5y/JOFctU8KctoONaAUdwX7cs6VjLS9lQpXsQXZYmhZwkPDXrcD79LOqwuJusVu
mRXYmpCyHWRyuHHLfBUG+QgwISFt4XW3N3cl1S3E+Dhoj9d/wKPDZsBym0Ha/peU1koejbfddA48
WK49VpA+Y8FKYJZ4BKEFcq5opJXaY6wUToLC7jtfUChIWaGNs61deHhCsSf2mEjjyWIRah+657xE
e60OpnMs8h0TMdZMBuUXXn7DreUG2vlONNcX1M2JW8nmGaZlQoYSUnz+vWmPPDNLZ8qjb4hmvzrh
Ju1mSzF+CwlH6R1w2rTmpy6e/K6tvjf7qAtImXC18YrC9ru4LNYt6R/93bcC1S1Jg0fWy8Z+zO40
VPu8rALpAJeQ2HIcLEnlEifPqEEfXGCpCN+thswSOjLpFdD1HEeBB6Udd6+0ym5qUHyxTdDRqkqD
oSKWBSx3PYu0MRSL0iJByUy79OP5mg4TGE0+lNcmcZFi+OyGY9UVlbrUoZBdRaDKx1tu59A03SC0
xDfrjokL+405MeSDBb8lzJZUxlcu4PuT9d0l6nDlTRoAsICB9HXPPAOVdhHAaq3xTlZIggKgowBg
vTM4NR/Uh5jKbSRoqsfTn1OoZnvf9QKnkn1bKxi0oEiq7QLphWuwlf2KRKPBd4rcKDrQ6SW2aD/d
icF87gs9LEEVRlok2DidBo89Dt1zzSdeZ72lFWuJjbQqwLOA+qdB/tmdWPpocZtPf71qdFQKJLFr
2JEkEZ0qhNCbQNVp/Z0XVk8jaTOLz9ZZI7cyZ2nW3k9dPvssw3T/T2EgImBIaQcpH0f8NftLwGo6
WGa0rSrlZYVDcnPCgRxDG1w3tToJNiq4jTu1HQTYpoa8SpXD4DN46HcvAKUoSOwcbhtYb1M8D183
VVx52dAlMPbN69k4mQ8I9Kufwe01Ooq98dCr4IFOHVi8uqY2nFlS5R/eGVepePGujzhsC27J7U9M
MaDbH/P01yohtihxoyjPcPaetRFNkpqVr75S+GWr+akHEAn7HSEFY3rNDq6DY+EgY4k3jpOE33LI
4Cif+03BVOOXO+E9yIQHy1N40RARPtvdZU9fiBwNQ5KoCPO6pSTzI8xIp+M4pne5PXyFmx0KZEAg
wbB6lMn/CgR+KnUT+Mvvg97g+gNVqnAarfkssYFVHh+zz+lowLkL4Qjp94IhJ6YiEZsLBdzyU1ta
VFYX/k1wAbh8e+DBQCio09Vnu4pM59WO9kjHYyyB/7g4dPYdZNAmEofP9xnzPPF7sy3bvy090Z95
L/Mdu1DnyR7YrZBohPdfkTDWek4F87M4fZice1peYFFWpFt1iQYmXsXWzODs19p9nMvyGFsyfHVF
FXLFuVI9zMv4Rf6E0g58+Qc7gzdVUSycgtqvWzNPjp4M9r2dlrmAwRlvelf6CwwkJPy0u1Qnd5xD
QFa0nprTlleqt74wJGsst2kuIWA2XNEN3/QD++pXzZxi+4TJXN7MzMFQPcF94qkZL8v0CeSg2hSv
mNX+yMBY08p1ibRt8vqUi8K5XbJdKU6nw/KDDB0Vi9w7WkkokAPzd/waeJAdcN5C58RaPbnzGKi9
OfnVEEf7dYAqxo2JZU/1MGc+gQd7k9UPuDzX6Go8N1IP4C8z+RSeKfZTIaOcNRJy+7wgPF10HvnH
ozBqqw8pdMSCd9p8O4+WzFlftsYN5+njrycDOmIFlRL6NZaWpIBnENCCok81UreDQAodupO+dDsz
fUEH1KsvfepgFtA2QuL/KudbnJNRLwJouHPq4l2JXY9jtyUXYs/I2r6Y2L/XXa3O/ICWuznJRlCM
kVx+IlE+P6sqINa8AlT2IteoFVv/Io6RBHDhiisWpmgYIHns9C+8svEMsKHBoqqGiNZeE/z3+R5P
GBlxt2PeZvGgfEKFQOa3IyqqVQEV+8xVFl7RcBplGY6abBfiiWlM2ek46FOuXYAdnXHM8UbsJEvG
wTlz85ag0kGUnp0/H+FYVVIOMtzY5TtnwF2ZHEsf9wdaTfk3opWnVmTSBs0GX1P4NYlGpXQHk9qh
mO3Mor1xKsdBK19S+P+Eow5y+SJ2WevJqw4haKQ9fi/deFi/AA2VWgzd2m6I23shr8nCqlTDHx1q
I3VcPe2xzrMfs7OV2fWZ3XCB4Rg7FFfddh0fh/siN4Cn+ekpGF7clIWgot4ZdoVOMOZxRK23/p7c
Ys6PX2Vw8LJDOliutJtisht/TaWxI8fOiVjI2XhVd5u17L0kS60RQ5KPmG8EkqbZCQ1mH+Z7Kfe2
LiNMNWBliSCS5PxZ1W7ImW7hYrPaccS72K66VwGRZYVWQIb71Znlb2RhZ+ge69Uy/IUgxb86Syja
PV3ThsS6Jxom08soV5ZgagDFFTiLKiwZJvgxMHKGhxm+w5X+RdFH5FSVFwn39DMNadeEVKNN1N2K
rjrQL7MqsFN8UeDYFEeE1NO5l9iJ8793iB1MR0uDNdm/ahPSDBVV0jczVjLvQ1DOT0NsIwaKUoC8
aSqId/ecibfV2KbGGYL+Y+pEQFJRzg50EFZVcLGbJWFzSlJagvhVt18ejoJhF1seuC9gpKHsbCY5
bKsqxbnYulgOja25Bwa6gFO3DUlAcW8HO8f4OegQxCUX/yT50kXg6IkMKVmsKpJTPQ746pC+3Xmc
otPBqHCg9QROrqs3Jy5EJUWbb1/TY/amXINDK/C1+mreeqzIoY2lVB+1mWZeB6NPf6UjAmGTXBi4
uh6TPpaCAJzIt9b9KxK1870wQh/hgeuGw9P7vfeoOWocmNBx+pLqMIZdzzb8temejWw/H3rsc2lw
pkIvzs4KFEypSrFXaapsnpRRoe8mNcKCRW4kDJR3KD0P80cjqAnSA4ESubWE3ARoGmnXMP7qNpTA
4XQYuYNWgydvBUuYq5vfuNR7nKO6FV5V1XToNGwAiSbhGvglDNbBUt5utHn0tEpp0XaFr6yzGt+2
NUMCd2O3WipaCyiQZlrTCF3MNLp/vmyMHboJCc4+iQ5zUHBhq+GF12pIymssNqAz0Sn/5uEVkdI6
OGJqSWTlrdu8b/7CPsN/SozTcq13++Y1OYOXf4ty+yStXePRhQvEFcrR2J3SmzcVFxyf3QYX3ffR
s6WXaMTOAjkRdcuZfcv1HKpQ9ldO4NkITCJG0DRkRlEjUcDQkL96ZOI+9sc8gWfAjCGdn52QAWb+
xDwbiSBnt6d8tXl0xMezdliK8pWMr7KaPRvRD+yduU3/EG77gZSLYsdSFGql3ccRhfd9eO+Ja0d7
dEJFLCj6TEJE1UK/BBsIiRnjmNlJtXUkQRQ9MnrQd472WXyXoA4062HlkVZaRBGt/BfU36h8W/+0
DP0s+goM8n7uDqcWmqt7LdMwGx5MfLm70ztoe1h088Op5z4mt1pndMA7lbWNEgsMISFxZA4hQwIA
TTYq153sjuJwEiJiMXG0phDvBix8IQJ3kkkRxETWb32hHHyg5DmyaQDeSoMlw6/WaO+zoS15ti5B
dHaZ+gZY8D9M06xyDUsQf874F99tDmGcFHdITA1vB67HAO8sap+BWfWLUWAUhSp5WzwBkdu3mYVZ
wdyc66J8jfzTPpEmvCqIxVFx+dgs6tb2gY3zCTMYIT8v7tXZO35jox8kMnKXrXbZSAr5JqCcVWWJ
H7oUgIBKiKF9zlK9UnTE896HFCE/TriBls2qLviowX0UxKBKR6PwW1dQ45UA6VScO/9kjyBfkG7/
F5hK3Lobt0mzhkBxpkXuVnz6g13IbopsVyeR+oTwqUMnDQHKuYALUqDRR1Xsl46yYdH71jYOBfHP
BpXeIhSYRSsptAdXMMTij4eY0G1yBiuxmFdmKtSDlcPCEX6fbpIpsWJrvXD/ZXjOrMbqtZ2zgR95
ABKddgDscKtU3fcqePIw382cpJv7wnywYBKSDlI/YlgX4VPtTTgxVuWqLNJH+0CGhYTbja0YqGL2
O8edQjoBCsmjRhe4hM7jpB2yEEU62g2Yrvk0l6aYYSX/tAKqIKZYdT7aw7J1Em/xVTO8pUJowxl5
0FcA49jb1kVzF9vad+xgTYqmPuNbrib/dI2VeqD54zy6drKPAqlSOPaIN4HGiUE6UlolOqNFZ+0w
gBaKA7l9BHmi4K/GnqfRx3a23N48N9lKucGn6YMiD+6y67YB/fEW2Rouuzb0/cWdHCYBkYFpY8iJ
1m0/umePbIjkM1OnaQyqcr+JmGR4x/rHHIVgUihiwoajdN0nzYA0CYbVJgbOxe7ekFdNCZWXl3OE
uCZsUA+K/UbgGWvjgmuDkyw5vV3P7teBhmxqsWEgdT2oH5L5WBBym/lJH2OmU+QKF7lX/l/84nU4
YOvUa9KNOoW8RkMhzMwumd2dTPQwJ1GffUVrT/Q3xMbxZIUlkBATcfPE3DPCNZ5W29JdUkVro2xU
ZaPznxdPafVZMWGfqsF+iRNEgFZwfB2OVHv+q2E9KY+dsQxDKQAyF5yVIwI7iH/26noCFhQhtG0+
x9HepyvW6zNP0eXbopdqd2Mz2Ps/IP46uWoofP84VjUqKRq490oGq+I2JFO8lZASez5RcOTHS0BY
uLWUZq245XYGLSX5lLVfPsqg3AxcqCgTH8m0OzSCjDt5Hgnh6U1eDc+Cmk3VYN3kZlAljtC+qkN5
CmK4Snm8CYQLdEXtTgxS2k0yk0jzj/2lnN5YXNlhCNDlr3v6OZO1p0FNWLtATpOt2k2QvASN/tLf
CoTV3fJU5H3rWsVr/RfeVMNQAo0PT8tF5KdZmBTUzoAyrJM6QHJqCoX3qBoIXdlBK/TUZFjEBORx
4ruUWoDe0KAVQQu8WZiYqfbD+jZ7kJg2MmrtlC4dT3KL5jruar6cw6I6TIMV2JfVrgbdXaNy0vJs
dP+T+C6zUmrQAZdSEXuBTdLZWOqHp1JhCFlPJmTK508gIkESLLohjv72Fqz2W6lfukGd4D/fb3Ed
zPONmUjmA8ZMqiNLOR+6nwwSFfGZRDequWLQFl2mDqaimkfRuQpMP7lf6eRDXXsc2sz7krZ+ZiNC
MenwuxYEw0j+ESiKPo2QDFA25hhTfJLn0YKvUhBenJdO6wibU4v51taQrPTAv2+rDBPQmDU9RhvD
spy6gErqvX1xJws8VeefrgKobcZxMLKga/MoAkI7Ob1e3nk7JIj9JvNVdhPWmGLm4LUyajU4Nb1v
vnQyQuGiuGQBw06zehzBuPsg/TEpMDftbRmeA7MePO5/7BuJwrqQAgQxna9z5jE6ZODjaTg7MY3q
BBpeqbFoIJG8zG2Xlua5F3v4DdhKajrz7i0+Zy9pfkVDp5SCX7WEJ/9RWnErtgd3s3Ap/TMhYsbi
fDZojP2Kk4eGP10YiwdxkTVk8KxkYEeR7MXj7RavYYyarVUikCFud5kNIiThGFMtXvLRBNObAPvq
82YQ/H4df7a4AIwxCHMisFBlb/s+01BpeMQ3MmD1DL8nx9PVtM4ZVWOHASj9c+IhGeixtFrt10DH
pIXiilHUixwvgtUafJUTTX1mtW/4VY11Ii7Uogql7o2VMHTmjs6nXjjzjh6FdUG9KNnGAzU2cyPH
HAwA4/rc3YiCJ66QS5++l3ai+h5GUfdkgmV6sVwPwljEyaJatBgMrAZlEFgahP1dieD98pgx9wMQ
QZCGr9CwK4VAJqiBfMj1iF05cDRU3cFoit9LBH0KPuim/OIUo+C6UPBgoEKln9N3/WT2no2tkHaS
klRPUWE0fpp7sqg3rJ9F6KTtlu9E/O6BJxMlm5EBS28SaRHlRp1tryAGPHFsgH1Hn9zK9ikyK8g2
REUTxSvKnHEtubPNpXEsvozZor7YNKNFLtj5HQzHWSEeB6dge7adskIVEKUcHXTHvyw+EtFuBBpi
yC3p0Vcp+IJTDjt/UceWG1TCRVFiJKZDqvCEYqiTdI+i6xwr4e/vOtxkQ/86BtDBxhNcjK+4EZQI
bcIwAZkTVa8zXqrBdwxfUWsf3PJcbaTQBUQNP4DxE2v0owdzWXL80BJmGwCLSJQuuU4oiaDldsEf
xK2rfgSyj6P5e5ktrL1/0XNd/bolfc35UwDWj5K1uOMeV/Ah+TB5Spg6a8BQfDvGOEt8MK8R5BAa
sjBwy/Pje8CdEVgsOjBgtDACdJFuchlftt7p7uy+eNqyb81OnPCkT7g2VMtvKTPSkA3xlFjKJpEY
JkcLhTTZBxu5BoQDdyd6I3NwKlHyVT5JpVXFljVkLnrAEbY1hOIq6kt+Iu4W1W0CSwrVxvsldGNn
6guVgeU0WIhfgs34c8oGbnLcGqdws0a/ZWyWWB4DxphZodr3sWPmYWlQnGT8j2VkunhKRnF6JGNd
JWeM3vry8M32MslXbGeCoRAUgcbCXgwOmPaEZ1nLoSWEBPrhoSVoCEKIh/HL/ztOXTXsbS1M/RnU
Mb7mAqjqGiW5c06Af0c89QhcfDGFNHzjxkLJkYu+czDvCJzMJ+w7cuMFlVIMbvEzMPX2dJg5Q7az
pY+DqYfDS0FXfoosNMpCPLAXwf3w8rFychTBwh8elDZuDSQ/bQ8kayiXo0DPHALR0o3QEolGbLzD
3vpkNNwyht0SIsO0kSNMo4AFUgOfWkJkE2RX2yEoQXdj68L6dOhSG9FlZZUi6hudX/oOAf8Svifw
HgaLyT9lKYWz8szmp8kDrUlu1pKT3PIQKs1uYZnnskSSel1R940qLlcw8EGiIv0XmlD+BbYjhNVA
o6zM//1+gIu0snFFQ7Qlt2TJe8Ap9GIP7qqWuQ4pkLD9Jd6bM1dOAqnAuGDA9EN5yMwk1DjyIwyf
xpc4gRVoNw7GEppxmS32oJiNefv301ugWhYLCcbU9ThM9H7IaDqluZP2+8bH4xWMVKqtUVa5ybw5
wj/jEgcHYv4/BAy4Is1ykyOh/jVKjERPdSEmum/LK1VMG65nJkLs6O2xty/SGTU/ZyC9NWnblhBl
WT9oSQuNxxY9aoHJ7v4jzSILPPEKhZs1UG1C9sEaJz1iOeqV2puRvIIJX/v+WP7C3AR7KkU5wqcS
XbWgwdqOU6vazxvQA8xV54KTXRrD4yoqinAXFONw07+xwpKbUHIfPCMsHqILSEFZ+5EuIRlgqpo1
Z9D4RP6+KU78AWqdARAra/BgRVTVjTK1sPAMwXsJT447On1RK8AT/iQAlmFih3xHlnTCUtdvYYkp
7UjIEOfhYhOnyUSNUyiVvN5cg+6CcyEo3U+AXWCH2NN1nJ0WqPVCiVNeOb7rMeHprR1+I+H2NaCe
AtY7VLMicmrvCVOnY1ABHlvVKxZyUoUGTsgNELRp+wzW6CQjwT+uqcUzulbkvT7cXDEIQ3qwe+Ay
zuCvwTc05CfqjSXHnVBX+RU9Lk20uj8R/nAMFiCQqnb8IqgIiZm02YT/UdsHNoyqSRkQ4VRrmN1m
9QbmX0cH7g1tpzs+ej15fkLYmKnL8k6RzmusCSQg9jFuvL01nRwgNynrVK2lbMlY8Gdx/hkO/qPZ
VGr3jsQUbXCE836so+v/wJUNup1IFHKTuNxZC6Vts6SVo/YkvCDBzTOzhK5T2aNni0nGcO7nftx0
9+hk7BxFvyo8g7hIA9uvUBOrZcn1W32qf0x2tRtcMx6p5e8HJCvCkmeEECT6LK7LpOHNdd3gLx+S
9PbFMvZP/6ium3eEGhG+xDZYe6NUya0qZrqUCxLRP+9yq+X1pR5z5xYuxT0roHfp68E/LT9TZgFo
1Ne7yKzLhxb/MGk5EdytO9yluEEQSxm/nmA5wFLIzt0EGv8T/A3GXCBKzSQfMl1DFfR81aeFtFpZ
6iXxibdMOcCRxG2AQB8Mz4sLgin6L7IbnXPtsJDwge4yiQIdJsmqu1fsKqslFKk2wCjsTZeKSYtu
NylV2+K4GZZWIWReYSLyAMh876muHKQAsTy5ycQ91iSt52623pLBJnS3AAV+HhsSTqwJKGM1r1M0
z1YkkqWUGXJUZVyN1lsPMDMZ6kLO/Ua0UOiJBJk6wc1VsSidL3xexGB53ZtXnoNWM3FLLG1MnaGE
36eTgx2WaXRz4P/WRACkcl1CV4YtMcRA1zaZuyfY3Rn5oBY3t5ieUXFWvhKMCJBNB1Z/4nS0vurS
dUcqmTBzaCu1x4UpE1JHveQ+WX8mzepxy/JUiTt1mWqI4DeN5a+ANYw0Z7fNJ35v61KtzIqFtJDu
gZunSy3J99Xp1uJNi3uLpy/IovzURav710ODp1LP9Vkj4UgQLLJzLiW0h9eE8wUKVGURkk70V6RA
M9LgHZhGMdMIULmVusGth5BHL0bfD6X4OTTP8xTmRIkJQkqNQMApDUabBFFP4CjOtWtR6WHJYme5
uixMeRAQSIR+gqgySehc2mrQuTwTatnR00nLDa8RSusf59727KZeXFeJ5wBsXynBxe4wPmU0uqWT
yLRCyXmp+OfIIpsyHhjP8Owb6K1QRZ56v8iqf0btykUn2M0UG3fwSCm/3P+Ad2DMxu5QIm7eglRM
mHbBBsvgZdfRdZztD/lZDfj07eR0cpIwQHJEONJUQWJgf/umu7H0u21DZ1xkKmjFTqqTjYfQmdkv
5V52/T0Ox2mVa+2wfmfCo6RcPJfCLw/5pFPQ3h0Yphz8q49pfbRtyZw6+tW1FPItDCUyn5LGuoXu
14Rg5OTfdliF9LrvZfPmSRTHKgMOuoeKJyJCzKyl2GDNfHJy6aKSzWbecooEn7VknSwam+6qibun
UZO6zFFeebDsfJf4jyyBVHRcCulGMTH6lSA1/YQv/Kw01xP0OPCYEI3ho36xNrZKnG6XsxqhS1Sk
qUDqzlTaiAopBMVsDEMrkX2H+tZJ+zBsKsQWOj1zXJhIiUD+H62scFfmAAilR2yawc44g4q3Ivjf
wUrMl8UHYlETtjyjnTUZHnOPFXvI0+7lPE1gmfhuj6UaM9ZYIpZ8skEJRWSuMrB+kdltCZv7ebNu
xM6AnMvcPUqZcS6g64Df39Alhrim0p+sY9Lj4aHc3ewa25huv+LGlmY2vXHgz8tTPYxwBtntP3xh
zM2Gm1++d0KogaqyxTENUhedtNuaZTBqm+evoJlL1Y8HtBQhPTZ5sCw69gXHGOgQj8D1Ursy8ehF
JX6ir7jCnrCDr+w8MBs/YEJuq12198cD3mtVte+kC5hkyo4YuGbfXFGtqL3PaW3s+mR2Zw3mGFiy
dULevYOS2BrYzX365DkCSYue7Fu62iApKyMIazZoMxb9S7HUUcW3QzUeEilasI6z0kymNm5jEeyV
+uqXMoPV/y5LeETpd2ZQ054pVV7lwCsFf1uNdDcJ1Ru1zBiFy8GRhvyntXjRyHDLlmNRJw1NSQF6
DLjnRGgabOOGQhbGcZsHBt6ajUU6ITKgrbjPxg1si7Ag43r52ksYOXC3eMcyY9HGMid5bWuo/M4r
H1ygMIo2M0odRR3wQYhD2JXKwv0jeFgDi9MZ40g8leBRT23M0HaFk/VkA85KLa8g1hKHCTOi3+eQ
/5lzA+6XuSmGHM5DC6qLk9RFEkvAZ+so/hQi/jHI3j0Ites570ihgxTZL5v/zRUOD6TQkt00XW36
5DgbmfIyTEBY2WI7nSSUhj0NVci6vl9wBbOO4yj7zfEyvS4H3c52BqbvVHAIgJq0u2Tuez8JgCVE
VHzsX+aL9NzPN/C6q9nEmuijZW8HKAMUAv0bGAjDZGqDWnahWHtY1pLFZk4p+n2+RQ4jOdwKJFmk
lumNlNwbTtele9Bw8dnFhD1VE0Un0YidlLotwkJh+flFgQYMrXV0PHaVxWtIGbk0q9uNFUBi0vDr
SOPBH0pYhP1L0J3L5PAPlYC+5rlXr7Ena+cVek795OwgF6okBs43u/xR17VFDeWo4U70lxDtUDah
NgL9HdIqLwXAE+8rD/8H5dLOk4UgHhtSVm+OBJ/QmRJPvIUIzvQjd6t9dNVrEpZWhon7mNCLa+iE
inK9vWWKlSHe15mgpR+jqwnV/p3GI0JmobYZ611GHzFbhL+ddgvNA7AzPf+FUVH+pP14Kyn8jjJJ
YzIEwXps6wMir5hC8SKmQqe11ghOkxnC2fKCGFbS3af0nAuU/VexKiD+U8RDcCdsUw4+qhFTgJS8
6x44dgqTTqZ0IfHwM1Mv87fWppMOai72yHXrq2cxRxKVxM4qsjcQNmEfZKh28nRm+NqmPmafrvs5
WaW2gp3VnxzSXAqPc9mYL+8oicpHBEhvkhPuDkjA5wZGbUls5xkQEm80kaEoJd3ip/hvdLmBsSYt
8xVJ4/goW5xuBSAgUPjBH2MkxBRqizadUxR/J4OGbOf0J/5HQEnNWccN4bujOUAIp7B2hXPBYihT
Tiaa/US/sqBRv9FgdhRvNugjV4U4MzfiIeMSC/qzCCxiFoaDpi6Z11TqMvipIkYtbcNByQ0implm
YX/c6136VMWlhCmeF7xsQ/ltR6Tz30+DrM/lukJPlnPQ9v5VGdLElxa67xHc/F+cJB3JqZI1lbbi
2lvR8ROwxdirtFlSOhWr9zMQyNDD8TuBm9kj/Q82KfgPLNNKkwrsvQLyrN3XUlYe0uvccAdLrIik
AFBxPAxpY1KjjkFiTXHyyTGRuK0iS9RiShy0T0t7WEv8QTXRHDxCxvlIzimRvrCc3RDC7lGf5jE1
chRh4YeEKUnI7gljdEW/aBmAEOC01sb0JXbZcz231MQCuVtxVeCx4mDB//ZEflJRMw8FkjIW+69f
nNPZa/3vrEZcfGKW3g1T2W2TzhnTj9VF9xdsjmuZRz2JuZ8vyo+zTYUPIlVqEX6hZgWPCx8zrIGQ
22ilb7rzWuiapeVs8Vow/XwF1J+sWj+8NITl3drE6qn59uGEB9ahHpvqB2tNXj6dJH5rVQg5zh+V
BPcICwx3qSg+x2rsV2UFtjjvI9e3xubX9i2UzgRUSed/0ygfwkQydV5y0Jxxqtzh6h8n/gq9fuxW
g1OO6kGqnykr46raN2t6D2XBjNqhtp7ue6FY2p9S00L3R7UP6IAf5V1YgSLrL0ANN88XoQjsRqwN
O15b1LrLJTeB5ojWXXmrl2iAB8LZDsMr/D2k+lj/GFDm6yQdCEguvNmFtdLcVQD90yvLS/CQtlNK
0mbAc84IE1wmua1Nx/TiuBHzohCrTR22uf1E9rfN/tXTO6eCB/I4Xgcq0B2WhMVPJNETNSFwnW7D
SU/Q3BqhErgZFUJLFXPOnKOP8BVpEPGC
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
