//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
//Date        : Mon Sep  7 20:58:35 2020
//Host        : nowi74 running 64-bit Ubuntu 20.04.1 LTS
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=35,numReposBlks=35,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=13,numPkgbdBlks=0,bdsource=USER,da_board_cnt=3,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   ();

  wire [31:0]alu_0_alu_result;
  wire alu_0_zero;
  wire [31:0]blk_mem_gen_0_douta;
  wire [31:0]c_addsub_0_S;
  wire [31:0]c_addsub_1_S;
  wire [31:0]c_shift_ram_0_Q;
  wire [2:0]control_unit_0_ALUControl;
  wire control_unit_0_ALUSrc;
  wire control_unit_0_Branch;
  wire control_unit_0_Jump;
  wire control_unit_0_LeaveLink;
  wire control_unit_0_MemWrite;
  wire control_unit_0_MemtoReg;
  wire control_unit_0_RegDst;
  wire control_unit_0_RegWrite;
  wire control_unit_0_RegtoPC;
  wire control_unit_0_ToggleEqual;
  wire [31:0]dist_mem_gen_0_dpo;
  wire [31:0]dist_mem_gen_1_dpo;
  wire [31:0]dist_mem_gen_3_spo;
  wire [27:0]logical_shift_l2_26t_0_out;
  wire [31:0]logical_shift_l2_32_0_out;
  wire [31:0]multiplexer_32_0_y;
  wire [31:0]multiplexer_32_1_y;
  wire [31:0]multiplexer_32_2_y;
  wire [31:0]multiplexer_32_3_y;
  wire [31:0]multiplexer_32_4_y;
  wire [31:0]multiplexer_32_5_y;
  wire [4:0]multiplexer_5_0_y;
  wire [4:0]multiplexer_5_1_y;
  wire [31:0]sign_extend_16to32_0_out;
  wire sim_clk_gen_0_clk;
  wire [0:0]util_vector_logic_0_Res;
  wire [0:0]util_vector_logic_1_Res;
  wire [31:0]xlconcat_0_dout;
  wire [4:0]xlconstant_1_dout;
  wire [4:0]xlslice_0_Dout;
  wire [4:0]xlslice_1_Dout;
  wire [5:0]xlslice_2_Dout;
  wire [5:0]xlslice_3_Dout;
  wire [4:0]xlslice_4_Dout;
  wire [4:0]xlslice_5_Dout;
  wire [15:0]xlslice_6_Dout;
  wire [25:0]xlslice_7_Dout;
  wire [3:0]xlslice_8_Dout;
  wire [15:0]xlslice_9_Dout;

  design_1_alu_0_0 alu_0
       (.alu_control(control_unit_0_ALUControl),
        .alu_result(alu_0_alu_result),
        .srcA(dist_mem_gen_0_dpo),
        .srcB(multiplexer_32_5_y),
        .zero(alu_0_zero));
  design_1_c_addsub_0_0 c_addsub_0
       (.A(c_shift_ram_0_Q),
        .S(c_addsub_0_S));
  design_1_c_addsub_1_0 c_addsub_1
       (.A(logical_shift_l2_32_0_out),
        .B(c_addsub_0_S),
        .S(c_addsub_1_S));
  design_1_c_shift_ram_0_0 c_shift_ram_0
       (.CLK(sim_clk_gen_0_clk),
        .D(multiplexer_32_1_y),
        .Q(c_shift_ram_0_Q));
  design_1_control_unit_0_1 control_unit_0
       (.ALUControl(control_unit_0_ALUControl),
        .ALUSrc(control_unit_0_ALUSrc),
        .Branch(control_unit_0_Branch),
        .Funct(xlslice_3_Dout),
        .Jump(control_unit_0_Jump),
        .LeaveLink(control_unit_0_LeaveLink),
        .MemWrite(control_unit_0_MemWrite),
        .MemtoReg(control_unit_0_MemtoReg),
        .Op(xlslice_2_Dout),
        .RegDst(control_unit_0_RegDst),
        .RegWrite(control_unit_0_RegWrite),
        .RegtoPC(control_unit_0_RegtoPC),
        .ToggleEqual(control_unit_0_ToggleEqual));
  design_1_dist_mem_gen_0_1 dist_mem_gen_0
       (.a(multiplexer_5_1_y),
        .clk(sim_clk_gen_0_clk),
        .d(multiplexer_32_4_y),
        .dpo(dist_mem_gen_0_dpo),
        .dpra(xlslice_0_Dout),
        .we(control_unit_0_RegWrite));
  design_1_dist_mem_gen_1_0 dist_mem_gen_1
       (.a(multiplexer_5_1_y),
        .clk(sim_clk_gen_0_clk),
        .d(multiplexer_32_4_y),
        .dpo(dist_mem_gen_1_dpo),
        .dpra(xlslice_1_Dout),
        .we(control_unit_0_RegWrite));
  design_1_dist_mem_gen_2_0 dist_mem_gen_2
       (.a(c_shift_ram_0_Q[6:0]),
        .spo(blk_mem_gen_0_douta));
  design_1_dist_mem_gen_3_0 dist_mem_gen_3
       (.a(xlslice_9_Dout),
        .clk(sim_clk_gen_0_clk),
        .d(dist_mem_gen_1_dpo),
        .spo(dist_mem_gen_3_spo),
        .we(control_unit_0_MemWrite));
  design_1_logical_shift_l2_26t_0_0 logical_shift_l2_26t_0
       (.in(xlslice_7_Dout),
        .out(logical_shift_l2_26t_0_out));
  design_1_logical_shift_l2_32_0_0 logical_shift_l2_32_0
       (.in(sign_extend_16to32_0_out),
        .out(logical_shift_l2_32_0_out));
  design_1_multiplexer_32_0_0 multiplexer_32_0
       (.inp1(alu_0_alu_result),
        .inp2(dist_mem_gen_3_spo),
        .selector(control_unit_0_MemtoReg),
        .y(multiplexer_32_0_y));
  design_1_multiplexer_32_1_0 multiplexer_32_1
       (.inp1(multiplexer_32_3_y),
        .inp2(multiplexer_32_2_y),
        .selector(control_unit_0_Jump),
        .y(multiplexer_32_1_y));
  design_1_multiplexer_32_2_0 multiplexer_32_2
       (.inp1(xlconcat_0_dout),
        .inp2(dist_mem_gen_0_dpo),
        .selector(control_unit_0_RegtoPC),
        .y(multiplexer_32_2_y));
  design_1_multiplexer_32_3_0 multiplexer_32_3
       (.inp1(c_addsub_0_S),
        .inp2(c_addsub_1_S),
        .selector(util_vector_logic_0_Res),
        .y(multiplexer_32_3_y));
  design_1_multiplexer_32_4_1 multiplexer_32_4
       (.inp1(multiplexer_32_0_y),
        .inp2(c_addsub_0_S),
        .selector(control_unit_0_LeaveLink),
        .y(multiplexer_32_4_y));
  design_1_multiplexer_32_5_1 multiplexer_32_5
       (.inp1(dist_mem_gen_1_dpo),
        .inp2(sign_extend_16to32_0_out),
        .selector(control_unit_0_ALUSrc),
        .y(multiplexer_32_5_y));
  design_1_multiplexer_5_0_0 multiplexer_5_0
       (.inp1(xlslice_4_Dout),
        .inp2(xlslice_5_Dout),
        .selector(control_unit_0_RegDst),
        .y(multiplexer_5_0_y));
  design_1_multiplexer_5_1_2 multiplexer_5_1
       (.inp1(multiplexer_5_0_y),
        .inp2(xlconstant_1_dout),
        .selector(control_unit_0_LeaveLink),
        .y(multiplexer_5_1_y));
  design_1_sign_extend_16to32_0_0 sign_extend_16to32_0
       (.in(xlslice_6_Dout),
        .out(sign_extend_16to32_0_out));
  design_1_sim_clk_gen_0_0 sim_clk_gen_0
       (.clk(sim_clk_gen_0_clk));
  design_1_util_vector_logic_0_0 util_vector_logic_0
       (.Op1(control_unit_0_Branch),
        .Op2(util_vector_logic_1_Res),
        .Res(util_vector_logic_0_Res));
  design_1_util_vector_logic_1_0 util_vector_logic_1
       (.Op1(control_unit_0_ToggleEqual),
        .Op2(alu_0_zero),
        .Res(util_vector_logic_1_Res));
  design_1_xlconcat_0_0 xlconcat_0
       (.In0(logical_shift_l2_26t_0_out),
        .In1(xlslice_8_Dout),
        .dout(xlconcat_0_dout));
  design_1_xlconstant_1_0 xlconstant_1
       (.dout(xlconstant_1_dout));
  design_1_xlslice_0_0 xlslice_0
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_0_Dout));
  design_1_xlslice_1_0 xlslice_1
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_1_Dout));
  design_1_xlslice_2_0 xlslice_2
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_2_Dout));
  design_1_xlslice_3_0 xlslice_3
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_3_Dout));
  design_1_xlslice_4_0 xlslice_4
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_4_Dout));
  design_1_xlslice_4_1 xlslice_5
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_5_Dout));
  design_1_xlslice_6_0 xlslice_6
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_6_Dout));
  design_1_xlslice_6_1 xlslice_7
       (.Din(blk_mem_gen_0_douta),
        .Dout(xlslice_7_Dout));
  design_1_xlslice_8_0 xlslice_8
       (.Din(c_addsub_0_S),
        .Dout(xlslice_8_Dout));
  design_1_xlslice_9_0 xlslice_9
       (.Din(alu_0_alu_result),
        .Dout(xlslice_9_Dout));
endmodule
