//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
//Date        : Tue Sep 22 00:10:41 2020
//Host        : tsuts running 64-bit Ubuntu 20.04.1 LTS
//Command     : generate_target design_2.bd
//Design      : design_2
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_2,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_2,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=38,numReposBlks=38,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=12,numPkgbdBlks=0,bdsource=USER,da_board_cnt=2,da_clkrst_cnt=1,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_2.hwdef" *) 
module design_2
   ();

  wire [31:0]alu_0_alu_result;
  wire alu_0_zero;
  wire [31:0]blk_mem_gen_0_douta;
  wire [31:0]c_shift_ram_0_Q;
  wire [31:0]c_shift_ram_1_Q;
  wire [31:0]c_shift_ram_3_Q;
  wire [31:0]c_shift_ram_4_Q;
  wire [31:0]c_shift_ram_5_Q;
  wire [31:0]c_shift_ram_6_Q;
  wire clk_wiz_0_clk_out1;
  wire [31:0]dist_mem_gen_0_dpo;
  wire [31:0]dist_mem_gen_1_dpo;
  wire [27:0]logical_shift_l2_26t_0_shifted;
  wire [31:0]logical_shift_l2_32_0_out;
  wire [2:0]multi_control_unit_0_ALUControl;
  wire multi_control_unit_0_ALUSrcA;
  wire [1:0]multi_control_unit_0_ALUSrcB;
  wire multi_control_unit_0_Branch;
  wire multi_control_unit_0_IRWrite;
  wire multi_control_unit_0_IorD;
  wire multi_control_unit_0_MemWrite;
  wire [1:0]multi_control_unit_0_MemtoReg;
  wire [1:0]multi_control_unit_0_PCSrc;
  wire multi_control_unit_0_PCWrite;
  wire [1:0]multi_control_unit_0_RegDst;
  wire multi_control_unit_0_RegWrite;
  wire multi_control_unit_0_ToggleEqual;
  wire [31:0]multiplexer_32_0_y;
  wire [31:0]multiplexer_32_1_y;
  wire [31:0]multiplexer_32_2_y;
  wire [31:0]multiplexer_4p_32_0_y;
  wire [31:0]multiplexer_4p_32_1_y;
  wire [4:0]multiplexer_5_0_y;
  wire [31:0]sign_extend_16to32_0_out32;
  wire [3:0]sign_extend_1to4_0_out;
  wire sim_clk_gen_0_sync_rst;
  wire [0:0]util_vector_logic_0_Res;
  wire [0:0]util_vector_logic_1_Res;
  wire [0:0]util_vector_logic_2_Res;
  wire [31:0]xlconcat_0_dout;
  wire [31:0]xlconstant_0_dout;
  wire [0:0]xlconstant_1_dout;
  wire [4:0]xlconstant_2_dout;
  wire [4:0]xlslice_0_Dout;
  wire [15:0]xlslice_1_Dout;
  wire [4:0]xlslice_2_Dout;
  wire [5:0]xlslice_3_Dout;
  wire [4:0]xlslice_4_Dout;
  wire [5:0]xlslice_5_Dout;
  wire [25:0]xlslice_6_Dout;
  wire [3:0]xlslice_7_Dout;
  wire [4:0]xlslice_8_Dout;

  design_2_alu_0_1 alu_0
       (.alu_control(multi_control_unit_0_ALUControl),
        .alu_result(alu_0_alu_result),
        .srcA(multiplexer_32_1_y),
        .srcB(multiplexer_4p_32_0_y),
        .zero(alu_0_zero));
  design_2_blk_mem_gen_0_0 blk_mem_gen_0
       (.addra(multiplexer_32_0_y[13:0]),
        .clka(clk_wiz_0_clk_out1),
        .dina(c_shift_ram_6_Q),
        .douta(blk_mem_gen_0_douta),
        .ena(xlconstant_1_dout),
        .wea(sign_extend_1to4_0_out));
  design_2_c_shift_ram_0_0 c_shift_ram_0
       (.CE(util_vector_logic_2_Res),
        .CLK(clk_wiz_0_clk_out1),
        .D(multiplexer_4p_32_1_y),
        .Q(c_shift_ram_0_Q));
  design_2_c_shift_ram_1_0 c_shift_ram_1
       (.CE(multi_control_unit_0_IRWrite),
        .CLK(clk_wiz_0_clk_out1),
        .D(blk_mem_gen_0_douta),
        .Q(c_shift_ram_1_Q));
  design_2_c_shift_ram_3_0 c_shift_ram_3
       (.CLK(clk_wiz_0_clk_out1),
        .D(alu_0_alu_result),
        .Q(c_shift_ram_3_Q));
  design_2_c_shift_ram_1_1 c_shift_ram_4
       (.CLK(clk_wiz_0_clk_out1),
        .D(blk_mem_gen_0_douta),
        .Q(c_shift_ram_4_Q));
  design_2_c_shift_ram_5_0 c_shift_ram_5
       (.CLK(clk_wiz_0_clk_out1),
        .D(dist_mem_gen_0_dpo),
        .Q(c_shift_ram_5_Q));
  design_2_c_shift_ram_5_1 c_shift_ram_6
       (.CLK(clk_wiz_0_clk_out1),
        .D(dist_mem_gen_1_dpo),
        .Q(c_shift_ram_6_Q));
  design_2_dist_mem_gen_0_0 dist_mem_gen_0
       (.a(multiplexer_5_0_y),
        .clk(clk_wiz_0_clk_out1),
        .d(multiplexer_32_2_y),
        .dpo(dist_mem_gen_0_dpo),
        .dpra(xlslice_0_Dout),
        .we(multi_control_unit_0_RegWrite));
  design_2_dist_mem_gen_0_1 dist_mem_gen_1
       (.a(multiplexer_5_0_y),
        .clk(clk_wiz_0_clk_out1),
        .d(multiplexer_32_2_y),
        .dpo(dist_mem_gen_1_dpo),
        .dpra(xlslice_8_Dout),
        .we(multi_control_unit_0_RegWrite));
  design_2_logical_shift_l2_26t_0_1 logical_shift_l2_26t_0
       (.shifted(logical_shift_l2_26t_0_shifted),
        .source(xlslice_6_Dout));
  design_2_logical_shift_l2_32_0_0 logical_shift_l2_32_0
       (.shifted(logical_shift_l2_32_0_out),
        .source(sign_extend_16to32_0_out32));
  design_2_multi_control_unit_0_0 multi_control_unit_0
       (.ALUControl(multi_control_unit_0_ALUControl),
        .ALUSrcA(multi_control_unit_0_ALUSrcA),
        .ALUSrcB(multi_control_unit_0_ALUSrcB),
        .Branch(multi_control_unit_0_Branch),
        .Funct(xlslice_5_Dout),
        .IRWrite(multi_control_unit_0_IRWrite),
        .IorD(multi_control_unit_0_IorD),
        .MemWrite(multi_control_unit_0_MemWrite),
        .MemtoReg(multi_control_unit_0_MemtoReg),
        .Op(xlslice_3_Dout),
        .PCSrc(multi_control_unit_0_PCSrc),
        .PCWrite(multi_control_unit_0_PCWrite),
        .RegDst(multi_control_unit_0_RegDst),
        .RegWrite(multi_control_unit_0_RegWrite),
        .ToggleEqual(multi_control_unit_0_ToggleEqual),
        .clk(clk_wiz_0_clk_out1),
        .rstn(sim_clk_gen_0_sync_rst));
  design_2_multiplexer_32_0_0 multiplexer_32_0
       (.inp1(c_shift_ram_0_Q),
        .inp2(c_shift_ram_3_Q),
        .selector(multi_control_unit_0_IorD),
        .y(multiplexer_32_0_y));
  design_2_multiplexer_32_1_0 multiplexer_32_1
       (.inp1(c_shift_ram_0_Q),
        .inp2(c_shift_ram_5_Q),
        .selector(multi_control_unit_0_ALUSrcA),
        .y(multiplexer_32_1_y));
  design_2_multiplexer_4p_32_0_0 multiplexer_4p_32_0
       (.in1(c_shift_ram_6_Q),
        .in2(xlconstant_0_dout),
        .in3(sign_extend_16to32_0_out32),
        .in4(logical_shift_l2_32_0_out),
        .selector(multi_control_unit_0_ALUSrcB),
        .y(multiplexer_4p_32_0_y));
  design_2_multiplexer_4p_32_1_0 multiplexer_4p_32_1
       (.in1(alu_0_alu_result),
        .in2(c_shift_ram_3_Q),
        .in3(xlconcat_0_dout),
        .in4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .selector(multi_control_unit_0_PCSrc),
        .y(multiplexer_4p_32_1_y));
  design_2_multiplexer_4p_32_3_0 multiplexer_4p_32_3
       (.in1(c_shift_ram_3_Q),
        .in2(c_shift_ram_4_Q),
        .in3(c_shift_ram_0_Q),
        .in4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .selector(multi_control_unit_0_MemtoReg),
        .y(multiplexer_32_2_y));
  design_2_multiplexer_4p_5_0_0 multiplexer_4p_5_0
       (.in1(xlslice_2_Dout),
        .in2(xlslice_4_Dout),
        .in3(xlconstant_2_dout),
        .in4({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .selector(multi_control_unit_0_RegDst),
        .y(multiplexer_5_0_y));
  design_2_sign_extend_16to32_0_0 sign_extend_16to32_0
       (.in16(xlslice_1_Dout),
        .out32(sign_extend_16to32_0_out32));
  design_2_sign_extend_1to4_0_0 sign_extend_1to4_0
       (.in1(multi_control_unit_0_MemWrite),
        .out4(sign_extend_1to4_0_out));
  design_2_sim_clk_gen_0_0 sim_clk_gen_0
       (.clk(clk_wiz_0_clk_out1),
        .sync_rst(sim_clk_gen_0_sync_rst));
  design_2_util_vector_logic_0_0 util_vector_logic_0
       (.Op1(multi_control_unit_0_Branch),
        .Op2(util_vector_logic_1_Res),
        .Res(util_vector_logic_0_Res));
  design_2_util_vector_logic_1_0 util_vector_logic_1
       (.Op1(multi_control_unit_0_ToggleEqual),
        .Op2(alu_0_zero),
        .Res(util_vector_logic_1_Res));
  design_2_util_vector_logic_2_0 util_vector_logic_2
       (.Op1(multi_control_unit_0_PCWrite),
        .Op2(util_vector_logic_0_Res),
        .Res(util_vector_logic_2_Res));
  design_2_xlconcat_0_0 xlconcat_0
       (.In0(logical_shift_l2_26t_0_shifted),
        .In1(xlslice_7_Dout),
        .dout(xlconcat_0_dout));
  design_2_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_2_xlconstant_1_0 xlconstant_1
       (.dout(xlconstant_1_dout));
  design_2_xlconstant_2_2 xlconstant_2
       (.dout(xlconstant_2_dout));
  design_2_xlslice_0_0 xlslice_0
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_0_Dout));
  design_2_xlslice_1_0 xlslice_1
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_1_Dout));
  design_2_xlslice_0_1 xlslice_2
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_2_Dout));
  design_2_xlslice_3_1 xlslice_3
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_3_Dout));
  design_2_xlslice_3_0 xlslice_4
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_4_Dout));
  design_2_xlslice_5_0 xlslice_5
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_5_Dout));
  design_2_xlslice_6_0 xlslice_6
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_6_Dout));
  design_2_xlslice_7_0 xlslice_7
       (.Din(c_shift_ram_0_Q),
        .Dout(xlslice_7_Dout));
  design_2_xlslice_8_0 xlslice_8
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_8_Dout));
endmodule
