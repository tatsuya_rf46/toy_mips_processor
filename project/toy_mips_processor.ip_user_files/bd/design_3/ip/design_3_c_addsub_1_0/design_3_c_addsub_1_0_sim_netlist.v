// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:11 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_addsub_1_0/design_3_c_addsub_1_0_sim_netlist.v
// Design      : design_3_c_addsub_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_addsub_1_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_addsub_1_0
   (A,
    B,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) input [31:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire [31:0]B;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000000" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_addsub_1_0_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000000" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "0" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_addsub_v12_0_14" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_addsub_1_0_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]B;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000000" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_addsub_1_0_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T+60/HQt15kQb894SepKfn/MzONMnVMqW2jbt54YmTfyARbkfXHLakCDIOxQUN7TEzr1hbEMWFi3
/47YQA8q4Tr8LPpjtyzT12yuKhTqKrB0CLrICUOMT0+aHNR+tvlNuZxyM9InF8RleZRFcT9UQn4Y
smsBhQMAhMzKKY2+9aXyjP2zHL9pPMwNCjIP8WvugPCQN6ClKoIyl+9ey1Eyj4F4Zqa5xGZ9O31J
a7qD9uvrU3Fow65Lj+3PxpXc0iWepkDXIup+e1Xtr8eOJ6OEAgaUHFd+yAYe+oLcv8jaZaWuAUT+
qYpn5ULwtul68fd0eKfkUx1obzzPCUXRLcRMYQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EMcBTJHZv110S6tfMowsXVngFJ3FS6cg7uYXuTDoWkaWsdkHV4oDO9Ql2XzWKCw+neDgG4Ea8ef2
J7+SnocGThWmTcmHzz5VfiY7m2/m7AROtexRU2hlSo+C4WrDeIIvFOOVD/zWnjkrwQa3W/NnoI5J
f+Wu+6I1Xo0MngG8T5tPr9rExzr9eCCszGmBUeShSRrzAABNliiMHObGgI3nvPJ+xfbRiWfgp2dc
yCN6Kn5YaMLba0rtpC90iT80O4xof1DIVv/ncvEESx8FOaxnSdxANwOkViYfka7rHNt6agMW4sdt
nip9GC8FxDyYSrnGExS5Xxl3eBiJxBBbNb2+Vw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 16432)
`pragma protect data_block
wNYUDVZVRZ06sL5QdBeiOs/4zwfv309wZQ1nL76UoU5uTRVdXSzg7XOutYImcUuxdLrLtXasbB+s
PSRriPkQE9Hd0wNZle8XJ9GqS3E+XXA6p8uCTIQ+K22xnBkskDBn4ZJ7A2k5GXyv2MV98AdfGwaV
kobj0lPrxbMZsICqGEkHFPXXL6tjSHPldutNANRj8ugX/ftHagba/n7O8ucQFAthAX7Z17mmCII1
tlBNnO31misxZoz/Frc/Gk79H2107vFpe8HE8144ac8Fq6hPmvv6clmr3PK6SYhzYR61xkA534ag
MjN6se/uhbIcCGH3oebAWgyx18GNAxJvZrC5hKLJ+F5ttS0cuvlQ6R1DJ0IcKVkfhMuCsT5Tnwo/
lWP2MVNMa1+L5irTmsmhvgA58tklU3sectGE6ImIJDNyjOoH7Vvr/TF2Isnq0B4Ji0LueBipAP+K
WrX2Ea6EN9Z5Hg9lBF+087pMXf0NOm2zT45mBiUF2hl1ciDawBgd8wGbax7lSfgxfGQHqkS2bjNR
j9K/aMa8uK0aETUSVAvTuVW9KNF1fzuyvYLy9hAXOdfHdWZ1rbUhtBRrTLHMXlMp5MIs7Z7ONu/d
A1uYPNTrCIYlvItj9xXD0ueGRyoFNm0J56A4JYQBZexCDiejdJfVUjMqAqBoCt7zGQcvqpnsDDCm
iz2ZaBBUR6BDBLxVMTvcjooLQRiKMRyXXQahY08PfyWrrs4gg/Q/gRy/s7Vsq0RZB2KfQZDuzB7P
vBtjFUGyRCdHwaUj63BGOmJDuh3+So4T0TN/AmXVP3J4MEcOmhab9jNI3zcWZCjBMMiAz/BjeW/n
oe8JwFdZvHopsS/rKFyueFiAq5DQj0ASfFx6uJXrt1wk1s0AF06o7sQTl+E+wu4gn/5UDRsp5Y2b
ymKYyreMZHd1VJRjziVUMQHpEVKnblv/GkOG7SHSfeJ+W30jJDEzpjRzT0rfQwJznwRHaSV5GsOR
NAuroYYEnySjooShIQ6SR1VM+hnRyDPy9SYzo8yyE2kVys04c7lNG/nbi8AIGAngoUHfSQ7pcLOd
l3ebDPvReTaB8tRAlIhFerbdIRqHuql/S7eHxSjLLPdqifSmsnYBey0DnuBgQ/FWdZHknPrT0w5p
hn/DlrOgMlZNKPPooxQ2cuhjmdVzuEz6U3/xEHmD8ZO8zjoEYz3s1rgGsvY15/re3AjpdusS2h/M
veUo8j+ezxuWWAGDleGrJFrH/AEAMklQgbBOb2+lQRvJCwSZfTpmGp2JGdb6FA555HB5E0Fhr5xb
vzEZ9ytqLr06+A0jWeR9K7T1UrjiME5SaDViyXyyVHG2Mu0Y8I9Y7UIItwAe4XaXdruitk3HD/8z
FwcfJzXy4641jdEGgmGYCKDuDaeRMNgaiLXF7KevTD2/nbsZDPG9VLyOYMENNuEc+gc8ijZxOdW6
jtYwldVEfiGOhXvC1oafF7wLzAB6CBvVPXT9xl7ej8qpAHro7QNgxIBltVSvtI1rwK13wf7TE79x
FwHaWHg+kbBzYtdwhRe21L31DO+VcytC4sWsEH8iAuV+3+CddC1oBuO4Ud0STRjH+5BX5GZg1EKJ
yU+l7IiYAOL2DS9eywEtHJXff38K5Cp6EdgS14Ostjnj8pl0KHI5dzwdeLPiKI4ICEJEhd6FKti9
bjbwFdNtrHw8gMUAH6zYZaarUtvQ7CqSFxnqiWKgkLFva3l6OBl/BAE4/n6aafbK2zn+8fas5xex
f4qzzGtNuF/mrzG9MVKTTC/3LcjuJu1WTLGXbJFQRjnIfs2KgTjZ7P1TAEdvSpum3PCDv6UXQhsF
LecoOJaK58wA0dTtxWriWSrZQpFmTbOfBDL1Smk6+gGpU0zX3d+A57ZXhxi/9qELJbC0f/cB7QMw
4HxuFkdvVXPTDXWBrf0ado6MLnIB3KVhxPo8XVSxAl1UbVUOCRnZ8wrD54OixystWUDp5lNgVn9A
c20WG3kGeG9V+LW+cZhn39mD3n+gvmifF0KB5uJun8+VVW09lquoEdAlBkhmjCdDwL7IWOkUeA8J
5cNeiZQtT2B+8wFfYmAWQotA2hLCu6n0jXFuhv0hhOz0S6SBzNaFs/KvZpvOWAzu4ycdMGagCe9J
8ZXQgDPAOMWL+TLqI4LLMSvUFc7YDjo8Sn38Q84/0MWIn3qrYjlge3lokycfbofinvgBy2Md7bWj
7N1Uxr9xQh1JxF+t5uCrtF27jFPNrxjZid6ovz8MnmYCHpmbQTpHutuF5jFjf/ReupZl3I7wWLUj
eQWY7ArE1mPtLY0Z4cQiUcM8ZH4dqiD0n9e7m9KR2c5U6UMiljrujKWxF0osRTAU5iPA8+X+NQub
1nVTynlHEcxR1HD3V2zy/WJleS20b8+D7uhNpx41r+srH23gTpgAsRA1QO0CvtmzXj2V/i6iriPz
JeFa+DJ+3XqHuMEhdlp/FmhJ4fxwXw0acY0K6lp5YFgt8CZuI/ZlKqVpiDFFe+7Dhl7emh5URhmv
ER/yi970PKxybsZtWs2vpj0QMgBba6qhqWbqbN4Oe3y+zWri0raNZ+NXdYGIN7Cir0CPx+AZtJg3
3UbFrDHSaIkfL/vrfHtb2+Ftdjny9jBj3TA6ZBjAbn/SduxEGCJYZNN9xSFKrJhT/3crv5uvBSEt
GxiDPZJ0pYesAQRgPWiXaoGDzFT/yqY/BupAWGzunrAeebWq58bU+H8lUmZVP2JzYFTbkLhmYcDH
cTf+qeCZuF2/ftZdWI7bfO0oMsd2aN3JYIMpTjjnNB48kpl4XuBMXcY3fN7GpNZWtrQS3WXSj2oc
kW3USZ9UJZmxd0qV9HTwMYVF5Y1pWGfcQM3c7u7PCfM69NaV4HhsIR1fpzRDuRJHLOJYvt85LgPv
0l4f4jnuSkPydMCfJloqG7sFEeLpXTp9mHxo8TpbwxcyqTkD81//lUko+FaRUP0VwoX22l/NnNlK
WRvFJdrcATODaE9XjNetjGle76G4lHrHIqd3d/GczGgVhOp7xgKc6G58QpxtBdvPlgarDrECMkxY
2g4ve/4EjFBbSeBZpVhIkYlQ+lv2xEq94yP9Mh8+/9ETqyJF1HKVeISXl1hzaDwVRfuvZHQWtWQG
0R9ErMgi3nVqOTqbxHyVW73I/Yup4DfgnVMp4/LBYi4L3g3twipviK8FklFRh0spk9Bj+RqmKojf
x8yroKdMDiwoio0x74h1u3L70/FDQOltisGy5iBiodjsNzkk+NRmQPCEtrQFQP7eeEW3gv7Dl0cG
mPI0M+zAouD4b1vxhYpTxtS7UtrJtiDDomdC2ROejre5vZaOvcYeh3YdsaF4ZP/Vee0CcFL+JGph
anFTQgAqPeA8X+gR9a6PadvAzKVUf3+AWFNOYk+n471e1gupgLIr0h7ijlMCou6Fenb6RGTmS5x/
N9LDMAIjTpo+trlbG2nwfmfhjqZuHptc5+4E+jPTYQwZLdWgxWUrZ8ooS7oTo0omLbuHfzwd6koM
82FYMcdi1k5fOXL2XKr+w0vTCCFNMR3RZVfByzT/r1lrQFh6fX35MnP6P0q8hKX+sTVBBKWaikHt
YSXIxXL9glCeHLJCmcbGCZt3SoqxOXZOUH2fN4fVrHfJkQ2xY7UjNvLeFl+Rtpp5shwIk/yMg+8M
NLAwUgBPgJd54gVjWvqeawOaNvpEfwv/TVcMmafj/I6Fq4l5/WM3uzZtcTw1YZghhCKY5Xcyvuff
8yzk/aMc5IQ0zSzvwGSw6Zj8cS5uJhmyfGJt0+FvUGAUTnh3xdixAXCQBhCw0jl+ZXzYDvgY5nnn
7W1eNLHVNboONIc7abiatA/CQfkBI/XvLew7n1SIPz0X4HPqbXIwu60HZPw8YNGDaSzyDYtO/sAv
+L7OEsXVZuWghG42Av+QZk2eLXsgaFxKviStIkwU7trpH81oKKlVEJK1KGVNLzvjPrBbKyCqSiOT
/mjCYBPA3s5VU0CDkheoL2KswT8zOT2M+En/VAMqWmQdH9sNI6VZacYzGU8ENr/FIikENaCMPORd
D6QA1tlAIPGN1QVMONvc3TDaSpX6yCbjN85WZrMp9fJQ3lw+EPow1nbEpmY4oXslh1ZqsRqWnuRM
ppujdK11MCKH3KwvpV29J81YkwC3uvYRBrNBfR6GgbrWV3qF/E4z9mptEkdjVSl4AqQoSMDEABCi
32+/V4E5jeBGvvWq/GxEfPVuaoPSpUoOYxRcVFhlUC0vDfvM+xWyFOIyr1qFLJQy54wy+kmi/FoZ
4fFc4eDWC7MtOf5/HJ5HGh9UhXz3RlqdJRPS0v9MyuLNfgmxVzkgEA4KS7yXFtXwSLku3oa8hDf+
wldqQDRoxx6GgptDi19bpsG2bWQ6PnxnL2wBtvp5TX1XM+Xo4ipzfPMxJ+t0JJu5+u6Qa4MTa3gl
CQXT5mSOw3LHlWiUDciJ5EaHj2sCfnkAUCBym+dPuvu52bzb34Jfxs4KauEYuWtAqpArttLYvNqC
rq8EjTtwp/25CrEgeODTAwXoZ0fUdwOcN6F2wMuu5h1Uh7MOqHqQvjG5RWElOyQMTRh4swON4IxQ
VNaO+ZNklF9XuIk9U0jQW6CIs01dg2xCdwz2Emge406+RR73vk6yO4Tlk40ORNsMRFCYiSFUMhQc
WBBRfhR/heHlTkdNQQV44Fi1u28rCIyxFSkowXAJBwT/DurDDj6/BL50D535ogcXFkciuVM2vMYn
iY0k63AdfixlaZz8i2Kw8dQdUa905rJ3irlBWQCL8hsuNeKYIjgQP2so+7FBHRIqrm73/KiMrzHf
KI7TL2VpLLU3YM/juKQws/v9ER9+hgcBQeRO7IVjh3teGZoJrQ6g/a8BT/ZFFMgpBGF8z2snEPZU
ootzq2vdL/rJNnlHQ4LIt4xOFa4eYkqm5Fk9WcoA5POXgfmB718mV1qKzf8iOd2wiZV8r6mOEw8J
HF9dprwzatW1d9OKI2jc4VQuvHCc/h5sN+09iJlO4QwYyhEFIIM8XYb/0wx0ehyQkkRiXnWcAl+G
1anhtvU5r8UprlMFXPXr9FOvJdBRZTghFqR0tjqFOR8J1PZip780SWXBpW6lL+LDDdsEEjH4Kb5Z
Zo5rGcYZajBxw/9gAwB57Ixb2U0GmJSZ4rvz9MYiZDyBCW5IIppDTU0GFrtaqtRaMt8ZgpQ87WED
JCS4h3cbIqZLJSHfyoG/iPt+rBRtjQlvLx5ACKqSBCQCIvfLDr/UW8epHbHwmAn7Bjl9626/X+mc
fof/eW8QQY3pAyHRqQsF0vhASNSW2E00SEgewISKRzgEWvpZ35dXPh9ree42xbU7XsGXVjQmXxJh
xuSxI0pPnSxiimwpiarWSazvkBe62tcW20AM0mja528AWiiUgumsm4rpACFyTOdovnUbVWw5aqAp
62dzR2/DjXW3mpjM+OnR1skuHqecwmOsZaQutOSYl/CNynVP43vJhZsvm4sKWp5wP+a0XnGmod4t
FqiKGqAMzsSqjGeuh4qYOxwyxiJSSdw8pUjwHC0yCKUVXIXfN7q43dgYD/ktw4aotde1xSPfwRik
Z5XlybhXlsyPjIQBgoeSsYEnYuek+hOq6iJDn+QJe8b8P+oJWlPAIbkFhwr0TxhnASQvx3R4n0wD
8fLBZ40a/4tZdD8IyuOAJgRgmMGhgmjNG1IKFADzHm/WC7GJwfInRQ1HNEqqZBpfUVFvQe4Pfsnw
VUbb7c8RWodXNo48pmzqUbWajeSZrwp2s5fxXHFGo5lD6k6aze/3DNJmMP9Xx0rDRoB+3d2P6bbA
m4w2bIW82XK/JU40LojEYmnnRKguysAJHybTEaeFInrkEGPt4WLVldEdJvZ8nrcGrbUPv2vNTYZx
Ks5CJDMI5abiaOJBzmh8ufGP4BrYbDk3zOF3QALLFRQgjwhRpqjAA16NXh3eER/Y27H63yxHonq3
2ZKvUOG4dmaug4XQjnNbNgvSPFNrAcRL602emOwfhfZjxdZxjk6LhSMM+QP7q9J7mQGaX4RlI+W3
zxXxP20FPcUj2h7rLEqLV+iAqVRmLX5iyCFwE3VWbjvcHcxMHgA+FgUQQydFS/CLPSonSSWfw3PI
6zIayRfNOpcsJtVBrjNtw3/UmmFIYpaOAOL12Kth8a9YTdAKOzsrDNa3SyruaKhwHx3+NL01L2F6
jeXCybKabYDi8BJA2Iea9CSE8Di49j3FEfIRD5fOcy2T9zJrrzk348+v97upgqOR7/h3S/z9fbui
ig6jxaJvsg0Fa1rj5ayr1jePoHl6mcyHZHic0AXMV98Q6GLGku4lbAv4FY8zpqS5ATcnoPexlE4m
o/SAyhYuazoH0NytLDcuGsCLjzTkJKsYuaHbJVQ9wJ/44p88TW6elv3HGcw9kE9c8fTrg44dcJQD
BCQ8tDjdFqFbjjOodWWBJMvOK9K/EF8+3xzy918jocJzpPBw5kcTfNi0lAJHdVd6RtOPSCkMPrVt
AFVnrG6kUC3IbD52s3xARfxITgtsHen8dTRcMnj2rsDq9q0Mcd5Mk5yNWnnLHPAAL5nnDkQro98V
aOLQPEHnUvb0Qq8v1Gmt8Y8CEaAI+wAieQzujazUcDZSzP2mJRo/dYlvgQXTWhImo6Scapcs76Nw
EzQSTyPvlYvvE59GQXuWOQG8HebMUNF5e7OdmxLcObj0jeQbGQnEfhr1Dguw1WOquPnKIDLdcGmU
XHCvYYmPO7zuMdFJyNMfGcxcNlAJf+wpCo8pk5qBtzfyIvi2bmWelYDHZOqjQuG5ZpXlCwOnawKT
+di61yPt9wTRqsI58sW/P8vgZ9DN1qztbo/IN7Lf/Xl6vcL6Xt0g6fYn5uUCZ9BRkKZO7gCyg6FM
65b/3PkIwr124tLeufRkkeWearVWUmVWnwPKH0MDLYex66vNez7nuOmU71F24BpVJHlYPC58MsRj
JmhU2B9PAFlRa7AjvxJ3uVodVhGa+ne0cSodJq+80YIdpLb+biQGtK6UXrcVdh3EJ8fp94eTMxu3
mdJjfBULKusdEZoO4RC9/5eEfso8qJ07r8rkLDhk/Yl520CjQrHzOP+td0KUHhoi8AVpSk9sv8Mx
KgAY8NcyeI5M81zjmHfuO1Ja6QMv1rEktIYk7bbnXlX7KR5gtgSbDXus7O2ulqF47GrW9KHZ/VJg
TcdZ4aKvaAn2TQoL2TJBmtnZWFQTID8z9vJEcBtseKDZVHxwrdplfxo6csfhRPBTFWhbbWLVtnn+
rDyCwhyBv92Qi3RvUSyCf0DOIfcOTzFExUCla/DvoX2Cca4/VcdeIKh1MwYDr41nxPzspy0gMbi5
twomPgTU4mS78AnDF10VpeOiEWj6uPLTjDj7eWhZThNo0+1i7ay4xLic9XFD2qlOT6ZBXOHBrjqI
mqf4akKelvgG8wT+Hw7CvKeHWDH06oYJ4pOUXH4nFJq4Cgt9lZDIo8nntc9NCCMIwdJfaYlu5q4Y
sFmntCfCTvmVCSw7r3jTy8Hjl3hgKM6L7PvsQzxvpoLaui7MmgdM6qmH6Q7FkKJ6beYtfE0dONXE
SZrMTA6Y0eP8SxzeHyC8konx1Z8669q/5SQgZ9cGCoFItucl095ncdYcWqXUVppj/yWXJdrugmrt
Z56RYU+h3crK/kWWuoDClDA23un84rVCbnRjxoyPpO0RC8efNac4iiKuvXiTcjI4AyNoUQ11/Ysl
8CrUGjPpTevIgg3sTunjyBDHEbPo8qAxRm22IZ8gYIMVUjE97I13fWyJkQIcIlXsKaPsr6KtztYl
To+Wp9Fat1q7CWzYS6x+WSiLeQJAtfSsZapOCd7IN38bLxSkYSxmI1IVlZaL7AdgB/dFO9/9JpyY
TIUaffg7L30xAdvGB4KmOX4lVrzG67/MOtR8BwrPN63W1QMzvOpKdCS4A2g9M1gTGbQ/Vvkn9VYZ
ad6s+lwAD/lC7FfId95e5EBBTtPVgTi1CWTti2F76YvYbdt366SDw5VQV8VS3LroX12IhlRAxv6e
Oh4y+Tk2HzC+x1+PvVinrBM9mLK9M8w1shUBG0guPDyP3pPv2CvNsHBVyBPb0ce2aGz1E+T4WP/t
sbpjlRP7KuNPAOMrL1UK6EHd98FFi1Ljan4E0WvTmjymjWVJEDYCAudOfN2HVGJsRPmNdPGxXaiM
5o+iEcyzT3auh0FxehAJZ1cAeVGp8UZZfbw07h4tt49crCjc3yBKv0eYzuiX5SCiMF2sGDzHg9DU
/NBD0LX00+tRaHkZS0xNQW+mbkL4TPlbDfgvTRF+ASFwzLZg4EsV+6ozS7qwE71N7Qy1JEDwxcko
/jWg6vWLbTps/iITln6n10mrmkTT97ln6SoHuEuXKs9cmWDC9oN6x0a9Do/6aJEQoO1K+89v+/s+
nAE87NHMtsB+mEhtDJny6sdgrOv9YTZ/MiBeCas/knLS87ySmLmlBqHwqK922O69PRl1XXj2NKyM
bErfWPct+sU+GrI0jFEJj7x1aWpVJGMKB2Mw8pI/vQI5OWcr32C8AHsY5Cqw2gU5fid0FLiJ0f2b
yf8vxIfhJhXBX3nb4IHA8Jcv52iE53U2aCfc+BDMreMjIV/oqxC4rNHVqCPmqha8fAOpL9Bu9ym9
+b8XUFuDnijxc9MHrHHxAtGn0T0NzlSkqhmBLOObtS+mnW9xjzJAVYZotsV3gOYQPGi77EgGPENc
f+qllljTZ3l/JuV7JQ1hFVs9wmI+WypjthvY0GMDQGEpZzEseuBWKBMEt3zLOtzlDDj+063zS98y
uOC7Srj2z6kls7w20uhg/E8VD6EzNzZywf2HfjjU74+SuayPP+iTNy2LAgDZUvVWVOhMp1C3BSg6
uUJIeZ9yXDz30WLeM/k5/o1Rz0t7DbcR976orwcz1RWSJB/j7CUmA5f29Km1JtgnqanF5jutHh0D
XnEyTX6e8y9QK0Yp+D4vOG6/ttY68a5nKpIhBYL7hmmr4rbGjiTLhan2TSKpxlJkPqAtPXbqIt89
R+Pnm5+DVrqFyPVexJz4b3qSfSDM5bSWAeUDGMJ9AFkfTwJUil3/Fe/DsKPaOdXeejGprNnio5Fn
EvE1F4Kr8A5q75WszHqOPmaC5su9hAC4S8jbTHeM5hjyEymGgOiGyuWq13MP0DFeKXbGjQtISmg6
mFurOj9IgK3QjSzen6gv9iLVjGIaaGdhD/56hHerKyDnjFjL7GCrC9erf5FmrLqTYwMpgQCMfmmE
gzIfSZDu45tMKe3LBKZcMdcHq6eHRGGM+/X/mHDQfr0Cfsc+ANRLr/LKxX5Zf4G2p8x64ppj7NUx
pprsl50b67rbuPcpRCDK1dRMVKNIaAtvrUGJIyd7fcNeFbB42mS+/w7mfJGn0C4IKMHiElWd8WZQ
LtT+RGvyMf1tHK9BT8KerjAV28mz3jhEMmZS4F8yEFP5cuo1Is0YoVn/xGDZrKdFd2J/GSIudwbK
lbL6wPHG5k8wRhaJrY8N5gq5W8KeCXNfe8j4T5AA0xEXPIimQPrdjYCgcAaHT3ZuageHmmsEr9B7
Qsv3x/M4Dke8hl5I6fPi1e7tcWc4th8G1gnDO6ES/pMk6aqkTv7QA55VQlKiwiXTGebBNi+3CabC
oQdBVrR/7LC52iKAMfrzf0uzXpNKnOEEAQKfopznZChxOstAPaMHqpUNyzydhBEclTRJ/exvC43O
uFnkGCG/bSmE+gIBMiKOLFrrv5uD3aWpjts6vTE7PULee7HH1i4fiJoWttxAU3T/8U4VljMtcKc5
Yiw1mjzKlaODNNMMKiUHh1bwWixDswBE0N1jpGb9j52dUuCg5DEi+Uwmy/yS5LHg81GkfYdUxKkJ
yGw6DrIs10+UOPkNjs17LPwNWwTz3pR/OEoVy1hdv6R8UTFuDavSDSvW8/0tmRrlVUEdNXs8JpOQ
v+NsjLUUaWj3yfLQKBDtvCpNJvaBN6TiRvL4UDG4zgKQIPZwsb9tBjZW7fY2xmNhItN0oxDXDdEp
b0n/DsPcUUmBwlekGMja6v8kJmdUBXm3hBSCGcWPx3KUQeP03QKQ3mFbtk+whLPVHfqyOYrPsEZp
t/qOZhCrZY11B4Pk9yvG1yhKHeWp0ZjwZDnu6MlEsBi3+2jv4U3ZpwO8EhCiUKvAM2/Q8152/u6W
c4hUFJg7tW8C46XfhScB7FtzU7RazuMHJnze1H65/qPA9EuP5po3aOFt0WGJmOQc+WnxhjvUIVNg
8qIq1JsBVFs6qt2edqrDadc5vMyVhrqEhxNPE2zmuu+F1YyjOPDB+zUJL8aDGP08FPhHgF1MRFUr
N/pu5wK2kWcUcbbxdHc60VZaApwoeRUnQynO1gl9C84oNIzyAVUmpzK7+bnqSe7FbjQhLb5iTxcn
K2mzOq4bgwbMn06G74DDiBtFN52q1PvGjiSdeItQcjd382Dlh2BMth0X3Bp5dAXWJ+Tgme9n0yhY
H0eSkXC9AMHtX9yMU5IR/627Jdo+dAf4C0YwSjnExE3oMaTrscOFBZfsC4kz/ROVJ9TTgs5hclxF
aHCux38LqU94RNhVQ9u0ZPCcAMtFq5LQlksMwV7XraXTlAMrv0L4OdvR0E/Nhtz2f45U1G0iMzY+
89rzFsAqol9nwH4UfgwbwMsfUSIALC8yowjfoQfO2QVnDSoPL+GlKXwHY0DBCLGc3LxDzoGjCFQm
8kSWaFk0XH72/l7F9mFMxgPhMH41Fp6btv6G5qZlULh0kpiSaMig+ur7oD0aptZtnbgHD+IOfkAq
a4aGQniRwIA9wER318zIhKXAAmZcG7L5k2zUAoc4xVAwvpq2IwESAvaxGhpSTDGrnReNkaDM6HTJ
nPY+uDUL7asueINxU/iwXwbwbgDbdagYQoCr2iTElWhpvZz4F2bT6kMuS8+6WxliVqD+wAVGDSS3
8vjoDwKOEHSCvN7A8USI2zCmizfLETD55kTs72MKrCb7NR+LPcVcN1usWqX78jjXRri+cutOx4G8
bPwWnU342CIQYYnWKTUBNNOQUrkifkbfMlh/0FFIZkdRKl8Wb5BAsJcVf9R81bmiyeB4RTQZtAju
8TyVaUKxcZCBtxOG+7Zd3hwkjk/kz6VgpeWXMaMva+tc2c5VWyq/T6+64w39oUYtI+Z2+b6VMd9S
pqsl7b1iJ7h853bjAlRpzjg50jYQ5zcFhrPvUlvNIRDj9WpMXBAE0Y/aOPlFiIsjvz7nyYxBEKaw
/3T8q/JxJmpw2HpFx55H8X39+ifByi0Oxyi/efwtbx2eZoCR0420b/51t9xRM0/UPzIhJqx+nefd
OoTqzrLNoaGMNh3s9eALPXJ9KKGiOK0MzNO03HfwkhA7jtyof0g/n24kg71m6JAJ9vBi+2mzUDqx
8xsf7redBlouJy3t7w8IdQpYtPwS0e+TRosMu/i+CXN2iZpd8K2sf9El2u+SIERygLeC2r76P+ve
UqiZVWGwEZWMKRI0fbx4aAenl1g/hHf1xaaGlhirpEJ6McI2f2aI1VNVEfrgAke9Qbi7XSxJkB30
5HOxSdnKtGu47RsU+dvNV1mo26ocyDk6hunE/7h5Iqee6bmXUQfAqGALShI/kKzu5CqLny09kjto
msdas1pPJRaujoKSCUo00yFFkbsAIkJywkWyK9M/FjunuCwAWxv8GvN6aDkKB5QB0uyqTWeu2g/i
BcOdl5k/CLiAd1KVDVhQwQdyOYxP7qdb8Dq867ODgL53E77Wj5XW5VObnljPXwh9RYu5h8p7lvKB
BaBM++MqVsJTFggSVzPB4GegBddDYEMkaMbLey9v+kA8BLSUci9lxmngXLcM95DPjAUo9EoHqs8w
KmGz46GcLCDwHMQ9IDHC9R0ISXlR0l8CCG8eQ2ylaXc7mOhNhgtTw5D/vKOszPSHhrjYdzGmWPt9
wqrYHr1XbPaJ8RMz0rj37gHvTvfpjbd6V2hlgPdMHldDzf4bUcyzCzt0Fby0XXVeM/M60jHkKU7N
7hUmEOJRK253EGW7SK1/BRbsA1CwQ2nJt61ETOpoN48DbIsuKlZY106LQlQGZJ5e79jvvYYCx7Ud
0xyGrlNZhTeZaOsJeNbGvvj64J+RPIfvON6QPllwZwhIRvmCbAcTCkjraR3Z6K84/AzX1PYo1S5t
+IuTFRLQ0B8omBgi4/3fmKG/49SlMMOGXJgl8nCdCAAdUE/40EjUZDLUllwoW03vATz8ed+gEI+U
4a2YE9s1YSxSEqREtTKUO7r/jHKqBD8psOifzB8qMuKsh4jC76EMJYPyDzs6qMZHdK/Op/qX+C9i
JrIPmhn/mGUdG7yiXryQ6JwQvohH15ptXMeRH4OKtHTNXRVtV8p5hYmoNOyOFQ7gpUqo6w4YlDps
KVWJn7RyAU0HDUTFNBQMBaLj3hHyt9cOkUgHvWpe/Mdr23Dfl6lGptStUfdZolsRmdK0m5/py2b4
DAJ2BhC7Ag8m8LsUP+3Qubg5raSmGRyZSXYv6UcleRIpDtgPH+dQPUSBf8r6AaDLN/26udJCgwCT
Er9TjsFwwZ1qQc0Mje8Clnp3GNnNzIjTthJKey63kyyYtiA7046YYFUfQN2KH6gwpdWgig8A637P
JIIDIPS5njdmIO2n2+NBbuUy79P9jHFAkgIX4JId1q1qPIYgu5f1M4MBnhQdDbpdnT1VvelUFd9J
isIwGtbolwnaLtFa0ZsreRpBSKUSMtlukYjj9csY+yL3XHCggJqCv3RDltkwwpPJVp1TTEYy7qfe
N2/uU6UPAMvFd6/3wDKDDDGAKMjUqOTh7YPN850bMxqzAkSDArWZlQf5y/paxAvb+N3sPG3JHcEn
8QkgS2PDjAnoK5EemDsljfr2lugHR/kS+mY2X0JMOMXSvWlBwINPOOX4nf9xlWqRItZ5jp/o7PWk
dulo4xn6GSAdvYXj3e2ilAAlJuEVlnUUs+FE9cgU8T7LNbG4P0UE+ekR+nKEV3k0pjheuye233Ko
oopE0slSbgrKq1EpuNX7kNmf5S36Rn8I8gIg0UjlwKOHs97u/CnKkzf9K7TbKM6oZ3UGsOkrzMI5
47RedHlBjDmvNfCaKgGJDk1f7gDfApwVgNYsLJkj+mH3xui98E5dsOuXvWwfyx9ikZNPFz0QM42C
sa4FubOj6KfnVge2Z2h4AetEv2eF+IKsAu9wT+xbODRc9Jvy7VVtYdDSOoQdEr84sIxe1J6oMYJ3
7a8p2okFyh2iwWicFDhsSldD+w6jRY3i9qGgJyGSZWiVB4vpTUQs8VLXSmGLwx9FeQW2PZBOZ1y2
JkxMgkuJ07WF+XSrqzqoS+Ufulr2ALgPQ8iNdMcYeimi+QIeiXkK81ffXrRPj/6Ru8RymfwNDWlu
F4BM73bSdLgVH2gk3V+U7NusaOtMkCk9IbpLkKyI/8FMDspdgzlGJEgbMhauNwQTEBXF6IZGq1GU
OOEOA5OyEx1+wlhmziFUSKLUi/wtAxQunWBvS8wRxHwugzAx8oAdY2WxjDZcltp9PdRq/keykwbZ
jImXQsY5stJCIkneADapFW6BzNUtZVtLXBpt49YVQBvhMIhM8KBIU1NjwKxQcBFxAYIYk7B0wced
g/yUkkDIhN5UBwBrnMBALlm14ndNfQBvuIOXf3ftuR4KJ6Bniq0ygpMbqyfCnbujBRc/BtaofO4L
8TUuQc4OHIeT0YyaO+DJFCHsaKH4ZT6e2E1nDZFJRFiHi7MXdZmNtw/QmXJIHK9abSVAf0uBpLUf
4hSPeztErrZp+GCu2IyMGvHDPwZgrIlD5sWrnA65LEMiKYd4b8ju7iEFYj7i+U5TvYiTbzgAMuWN
QZ3qui9+b3J2M/3q7tD/BXy8hfFuueNEKX5X4oLuMmKiYTunUyUM9+6UrCbXq+3ns28WpzFbNohC
jcr9Z157vse3TJS6J6IinT/NJA+UbRCQeaj8QeMKdSGFjUvxdEg0lf2e0nWWmrwZtXaVLPhv4on5
S1xDStWYnku07YMGh2h5KEJ50O/6Mw9DJDaC7nkwX7JDdSOvYj3wYa8SeYSAdqV+n/1Dd1PpHHoV
MdQ6zOGSVNprHjBLZFh7bLZXZ71gPfwHjKLDAP/wo6Blrf+lECRY6Id8knBPePvHsFDdxPfUajl1
BHJgLHf9rnvZ/D5RI/D25qW+hnESVIhO+x/a0CwK0e2atkz+cizySFclcv7AO5ESYDEGDy2Hs150
Q7GgB9F9tyANckRnUmihUr+idFGf0N9vvMhPDKCrKgH5Roqk/IE1KLDMSVUT/+wrUaQauObt2ma5
y2d9JHlPHCn3qOO+tbGtyC2Lqq//pYVI1y8ghe0QjEFt4uGjKFRmTKQ3xWdpI4oDkeFTtnpVmFh2
yERPUTaljYj69Hv6dRqnSOAvWnKXob8d3xxrYby/jQKFJuu8me6bZGCxrZVQfHqwnndPfh+vjGvZ
UpIS54sHVnzwzYDQlwmu/4nRSnY3ZYAgqGR2NSMJe9Pm3iSvh6DiZN2owiyg2d2FSPwcZM2jDOa0
njz6ab+U7K3AYm2vdb2RCln4Qmke5UARDOdlSqQXl1pCzYwb4oolfLaNk4wAvcsmj/gWuN8ecLIX
bqgPfSzwq2Gf6aOoT0WIZGh5JlllZDf6CUs+YzxCC2JLwDRH3ZpkA/BVxf+skN3hh0EC/8lUpUF0
UULd35fDypH1kUfNXcmI2dCMq1WJagGz1w4YH50owFItR1xAveNOrIoGtIReA6LXY/nbBBwehQye
KxzLIboPxcHh/Mg6TdDMPTk8NVHgXTRlBEYXkyx5InEBwk/mAgVYh1GpNLQQ6y21WRDw4mqLL2i6
MucJn2xl5zFkcK9ZPSCo7fH296Qf/dKHzCi1iX8faw5PhD5EkA5aG9wAzUNi31pXHwIWEvLsp4Jq
KgOc0hyS6mOCvFizSbUe9Mv3+qCNOX7zOppvEjHUWLXi200Sg1BopkenwvJZmv8Cg2ynlaUJy+Nu
u/SvbR1Th30Jr5RMZm3tHt+vKNgjutf57FCwJEgw4E8W55vcio5omaEqXcQivSkTk/W1vBmju2iP
gH/dImnLjQrnZ1AurEhTDdfpwGuLGe0qACzfEPI10PgitT2eSBcm+Tw2WSgS0Vuu7IZCODoHSRtL
6zdHPi+0bnNCCCJhTR/5dy9tPuI3M3uXqyFqV7j/Ef5/j7PC8iKsE6t5RXKWfVIUX6EDTMsBS5ng
spg8jQli6dqgafLju81kbU3IpC/lH0469tctyyLQnz4S+pB5IWUzpMWQ49LZQmvtDqee3QmjdJgW
Zq30dYca7sagRJki6zIUtJbewby7aHJenRI7b3xqzjHZXT7ZaMaRzSNG2pPQCTdgzsgyJijoSO+h
MxeYSwG0sdTUx9lzyVFDl3kAYMwBapVZH+mGw3/9dlNucIl30NVWw0lcv8yQ1XnTVZZZp37tyyFl
ruKMkzf7MN2Ukxkn4Vlt7wqvUUotsV2ye4EeCdhRargSTpC2qho6FOxsL6tHcBc3uoozDOyCPkGD
oLE662BrKXbt+kFGH1+TCJGHSUaQjihiG9/NU7ehJLHH3ivJwivt/ocp9YdBZThPkgoK9R/hdXXK
7ikaiMtfKkNSTaahaLfVamvQj+2GYd2MfWhyvfYK8j8/IpAq3uf9ebZVdRJ++O6he9lKQksPOOma
uJrVb5xrI9X3fuPt9kLK7qWIq8JtXXhlxGcUw7ylqRtVl60eEqWqmcfbQpdFoX4Xx7/AXYtYXMJx
zfGxU+rT1F7ZP1xKuAI2hxtjkPW1qUXgUdIXcV5v0HiRxEHWBNTFP4uZFMiwol8M1HSnbWOPl2Hx
zHdetUX0m6tDx071aZa4mjHF3Ca24xbAF/G6gDGJQBAI9KCeBvxbMbYko6t2GI85AM5u38byGDjF
yqtZT2zC1sxERroQ3nNKF/RA4zipPNH/uo9yhJ3WdO7raojWLnD3MIOg7F99aPUDobv6DJzZZQXq
qavs6EcCaUTcMoH1IeyROXC/DlcoR3LK+4+cn0LKWVPs1QN0+IWCxiL2KxWXaDnxpAf8Aw8UYh/H
EDDw9TLewChmDNOOSGvi5Gca9hNzmPoloEx+j53OU/6WmRxBU3AcxK/+mJAAK0ujZOh2bK+sHzgL
I87wq/dyZJuJUAZi4pyVUsveR9ETVRyHzsZARlMXfj/Q5bdCif5lGu9NDGWd4Rjv62XR/5qAzMZm
rRlJeteY0OQFHExlqdMFOGEnpy5dt7e4KNx1tP7Zq5beHucGnGW6xNtz3QyRPJywabdmLwsS1FD9
MJc/OaIVM4iq/nOwVh2wrhade1s/K7Z16XlJIRII9dc7PVp6tx7YYa5cJSrRqgebjoXap2sSBCul
uWuI7+hZs3WOh/FpO4vBjaOzlvKAUk/vi8f7hXHHf3gdFxIDXd8yW7Luwp7XW8XjzeCBcY5uJVMz
5y+jgFcMaNq5laluB/+fQdrhgYltxH3G0bypxXQotAyTcKT1uCmUP4qdfdOQCDCWga0uCJeC2gl2
67Nq0zmGeqnRvLHXBKzq4xjjTdosSnJ0OfQhU/E0Ch7fuLgmsnw9+G77QXito7soyHtfd+g95haD
mPto3yamtv9fsacN+rk0b5btvKBC9s1pqoy7m7DqZJAst2UWNHd6R5IblTSmSZB8QIlEo058wXng
2KCDjBgrf4md8rKoGZ7LAsNLjesplzP3boYwvLE6IuNpJUWewVDbw5eIIagP+mJkKkLbURB55ZIG
JsSJ5jQMXvVovPeVtZm/Lr/wSL+qRwVfGLz83LX/aEzNtHv0sjlaFuG2EaTRKShNcPRDVr8n0w/X
UDrQLQrgqJOetGTwv0SpNNfWbapKqPHxPcuLlu8rQL4aQt7aLocpWGGjUc8mbbhZJfAgCQ4tBsv7
57QTZ1a8Pw6mv+9KbdE1ohJPMmcVHz2egO8uqBHWXS70Pjgb7+bUEzHAjeelfPIV9uGEPKr2TYJU
sJtcVf0LnGbtxMDqg3IM9tt+spY+G+lpNTtei2ba34WFSp57myjPyLNDRo3H7lVNGsRkZHPJXyBz
87uFbKNZpqmznlhr5TbrzOBhgrbqeFRF3VqJkKVda4nN5dQsMGACrXTSIWx5F1rlS1zn45q6sKaA
IfLEiIfYcUJEzUsVHkh3lTfS/VxF4VQvQzwLB9kbtRf1wNiYby3XNiAM8jvgBWAIIKEsbYhjFcf3
1tPsjqTaavLytD7pSFFmijcEwttZq55zsbDAzLWMsM8uJjw5avHC+jP7qzLyBUWVcwp90i40kL2f
ugxei232YBeW5GMHkCqXucSnl6XXfgjtvi+5yGSsJQ0WLJOR7Ka025ePHnwCeX9r3Gvr+8udg9vw
GiF90PofyMLGpMaVcaF4aGEC/1YNM6y+ebIqBJhnzM4MiMSlk7v2OmtbA5Gs3aL5pYPHD2h4HtcO
nbp20RJQMIxR9Ow2OOtS35GCXUOklipZp/IBjYUaWwFBIO4TkDJZCsDY0m7V7daatydvuvn3VejS
3RMP3avOSbBFoozTVdaq9o/EKCvDKuCXUGmc35oYeV/8HvuUqMKuBEDrg3frGjQTVh8wtskoeROt
WV+TQ8w4zVvyHthVEEVDFYW1RuStL/2FdmyMj3XB7eAQsWqBpaFr7Kphs2V8ld+T7f2W33FMzVi+
tcCd7ooVtUZcGZBiTGaBb/zFc66WV61gdKB5+rv8ahjGlBLBg07qRMjqJcDdKRJlbZC0PgP4hAMt
nAWmthO4EPGg8QWhs2tlwUM4HfrKxeC4349XwM4loCqLlquHyjW+U854kGzbSELiLPTqnjF4clNB
3MvUBNX0Gi5x1F5OZAUtvHL9jhfuxxsQplSS6AC+dtg4t0Cgsk5c2sldpDcQUd4ySnvzi+gtWZMP
6pfGdJPqYkBFBzZkmZq0NGnQTUBkx/V4VhECv1vf7A2Q6efP5i/Eq52GaQgE5ct+6bJaKxINQsj0
2zIrermtQOi88nkuZZ0m/eG3LDR6Wg1cNlJGIhISmQmodv6LxVISdn38GY+qIQ9dD2fktNHBN/Py
l4dXWtSgoK6BcxfmndIpFDYBxlsZO6qHSCFXD3q8BeAnkXTU+N8RszawXuKR2zhBeRL8myTTB/io
I32I+tsEpyBV3wRxGYlDdj82oH3tuLE72Kd7JKUHPCfWVPNhTzLoYAfq3Nc0B/758yhis+aJxBVg
WxvrJpmiM8CQ3OH7VCuCHFbh14+KsIX/P93TZ8oOl6fqaOJXF2hFebVlTWVru7hGERmhr40B3RgY
7Y2RioJB5PwTNigm0gu7lauhxl43lg3kNL8a5U9i1oODqq3us/dBXzHoanZplWTdQCnZE17ohp89
8sXXVwIUjeb+VpreF0J/gDkE7HBdN7k+g3/Th5OFV526Mmxmu5NqNAAMCcIStU0fTMnIPxFHFiqn
ZhC3q+VnaeWA+IDbNlNKMKfL1+Qgm0Y2o016hbrr4fPOHku10Zz/CovDsSzga7wl7fvvQGhlaNb6
CJ6mR1dP2I3Evia0q1+H8Ohl9halQakpDwKEGUI/GrOMIvp21F7/T+miOp93eQn204ZvxvZYfsG6
nSxZuBdrIke82a57NKJJd+iXKpWPm8kBxF5AHEGlvVoGAfvQVALVCak26S4I6KlyrP3leZJPh9jB
MIXc/RwyEk7GK7rjCaTONjfukhYbJrT8FSgzZNr1jiSEMP53vFbB4Ad3oVv46gRKrvlgnN6qajY5
X/aotU9NV4fCtFl7EhSxqNQ+FPIjvhSWWyLT1mdii+ljH4qP3glwEaUy+fuvmZT/PAPp4vdxJF6D
nyNwm8L+hRAVLdnb4/S924NjXbeDAb7yh8wJ95VwsgZSmhuwUj2RcWgbsm/VMw+Lyozp6NdLHfoK
3yzaoDrfK9vtKoQZty8frERRaz8gMzBsFzZTm1UgE4fAzST2sf/bXzoJ0gXHhutURQtmWKP5byEH
mnL+imJ6wjTl0MNO5Zo4e3MjKl+BxcH4VwddFJs71t/F1Q4gW+cYpU8g3TsLJKAun/aDGP52pNre
r+jxv6+JOWqMnC+2jTW64x78vf0EUZTM8yWgX4OlmyvZsDFaMmZ2jc9flwi06ZvUyAdrBiGL09PV
siiDZlm8jO7Z75qaWE09ipH2sU+SOgQZdWU1be+h+F/tW/137y7Tu1wodSlCSkmI+tq/PiF8K6ih
wq8a8P2xUbV5B0KfmYvcwb8o6i7UY2kL8IGvG4ViOi9okla1XpfKi12n6fVaBl68yI2Ci1DoHGA9
rI2oSVQvNo2EaM4vylLVaLPXbf6znlofhMbhgwaCpynJdEugMLDdhSRhuSVhs8UW5IIby+WsOi16
VjXPGKOL0ppxFEeyIz+iZnunLPxPo57RPCsU3UyN45tZq7rIuBTqrlsoBg0qEc+y7shWF4x6/ebo
r3artIPdj3QMfoXDnhajEq7QCX4dEYo48TMXE2SUJdPdtHn1itgy4r5qrH9FY1zCz6OHlax7bW9+
P6syyuzH1x89pNVuxnd33b6zPUNZGDIjgaQupF3mWkreKNNXkHJZzZlnzAjDo2jhFGyLZQ+NRNEc
Je8ZlLwIV1m0NgKmPaWwqNCwSTyVyRFMEQklwahGSvtl1I2TJr/OEkUyhSKjQtoqc4ACGInq8MyP
y7cN4fHX4AbKyJN8OBQl/YG18fZUlW5zdr8KD3TlbMvxGDrrbhlwOG+nbfI6VN+fH/Z+MFmZsFNK
oMkkNf1Yn8ueYCRIkMTccXmsuMwXNI/NILgHwXT+Bq3/9OqxP2Z3+5mfsdYV1XMZBb2ntVzhjhhM
GkeNBWfSUhHNTQxUwPUVXg6j8h47yR0z5+RiZTYpSaIqchXgM1ZQnfEZ/DITr79cqlcDSi7t5ZUL
lICfS+nGuMsUSg8NB8j6+DdUU/2we4oxcJMkRv965i8UjuZOmm5phtWPdgahTTyyhTf+Z906gBDy
T3iO/tIyFXnVlNsXp1EKywTDq1JK5YByByNDolFkFF/SF8c4x/vNH5yGQgOBd7ow46wWXjndJ4S9
gSFAtffTKCornphh8Z/4s15nQG5PZR8w7Cz9fi26pzg5HIS708jY12eA7+nUnlFS+E3CRpOqWw22
VYrETeYvhRfQNjtlbbjz/w2hKCmKJy7cy34lyxtc2iyjW8KbSvkBeDnJAKxV399TXpJcD26VfS3h
4PSAmT41O9Ffq5CLr3Sk3YWcCoh9u0PjaLo8fbrBmdWMtY5InLCD9H9fgldRvG++zcFadZuJFf8r
hl2Wq5rpLdU5cqqIJkaDp+cn26dIKOhQChEGGce/tts20aFpbCf2hXMkOItvKItz0pnqXHPgPquM
1AT+RIa9YgreT5lSj875Ol02kpcsaMcr8xx/ZTUhGLvAb+GWRtDhO3Rh4QPfq1a2AUrzvVa0hMfp
32CVB1DMc8YukZ61motmNWVAMEnmH2eCYOEBPNmLAv7VHVbdz44k3q38KjfFFHaLXBreGu2CHVG5
19hXOyRBUrQyFLeSn9h/I+Rqb+kFZDgZduTGgoqUjX04keni0JTaofwgViAv9nX342yuVJwcAdVR
lBsXLrieOH4/BYrBCYO6EVqFFX092WrilZyLl9AoAaEoxkqH/oQXD/1xLRJxP4fBYsbWqr8sZegR
x90u2KWrDROZQGgL/iakT9nwvAU7pkpFqDukU+my8gWr0m87qfSIRaCU2HT5x1GWSd7c4XYuNAR0
g907L8Nn8Q+IZKouUXHBySOuz7MaCJ5WeVZaWCUYts1ah4HdUbRy922A4JdU3M6f2kQr9wVopIgN
z9ibN3+SzycLA0tEC9dJ6cA8Abgn0JppM1LyVs4TLC1MkvOVTfk57MG+9u8YrurQHmilEGH6Wmd1
7hd1OmjmrTfTVJDqTOOtItHl8fYqpn1T7zN0F2ASKWwxh1T19pKORQzYd2l2l7vuwxLUvKwj03ra
Z8W/ucDLbVrp0nFk7uMVifMPZjG/bQopz53sR118VDEqe1o4XI7lOEwl0zO46Q3f7iY6BZxvdfCA
xcpugjKRx0w6kMKmrKRZ7qr5UNBaKpWZsfSFYpJ9VAuPuiwFi5CI6HxXQgcOxlBizg4eKpobtvQk
gbfdq2vk6U4McvYMA40BgWfI49ILXeIwd8M7UJWno8Pye4kBEdCKRyQhX1Y6IRUvd303YabuXe7a
DFAAzA0af5lDVzqVZ0TlpwykUqoLTxsf5+Mu8793DxRU1T9xUCRWY8Irdxk8nVviywW6uXVwoQkR
q0qbY77l+dGyW/MhU74llvfYAlYxkEW35nQZKWVJMR1PCBtLkp4z+MMgsIn7kQXOZHH0ksoaYDbT
Yt8QGT2C8dXZ48gUa7VUwII2ooF62EwAKGP9QjkfCz8Qnlo7VGBiHDIUYWZ+SWLZs9R7tjDfN4J3
s4TdCoor6vCMO+vm6CKuNYeURObjeze0rsLxmwPDUYMLamukFjBHyKzq2I91wLSw7n+Rg0s2zUtX
Vpomm4P6j6kiPz67Q6x1M5bXdW62ZWNeOcPhIB618FfIrrRVi4p3VOW2jIzPisylDPsDUt8Ykr/P
VKD9HLWRHw6SVIwZhPNKo5XPPs93lIdutO/IkUUEZpwtRYBqPEUsVpmBZNsv4KFZdJkdOQfcjSqf
g/TYAkITM1u1+iSghPXPCUocDe/CZACk7NHsHxRcqng2ewOywZP/XT6SBcXH4q+EDAPK5SAAF1d8
WsmXJuxXlwbUKid06dtk4T5ehIRFzaXGkdOBC/MqihgYD8C8VFDbzxhF9ZTsPzmckyDLXIN7n6/B
zRwSga1TUTfgtqVJnEBDaPqusvYuOAGcwaGWI/OakMBAih1ULO58JpO1cdJg3zEZ3CRkM+zqkIV5
38rYEj6/k3ecQ928izDDaqtS+u2+M5wS3Co+JHuS/jFtJ/pDnUbFIRMCMZdrxKkH3ec8FpLUH+TH
ycv1PYa8tElQ7+791z+BUj9t/y9ysV0Pv+6dqAo4GVUGBjjs+37KY8ig4sAd4Jf8H1hDwn0JFDEp
KoGrNGmoYVXkga73POjlEA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
