// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:04:04 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_0_0/design_3_c_shift_ram_0_0_sim_netlist.v
// Design      : design_3_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_0_0
   (D,
    CLK,
    CE,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_0_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gmeLeZKwb+CzZTAF2F1l4LBEwSW7O7L+u8YVntB5FXi+VV+Nns2pTOqXUFv8YMJLuNr3aIkzRXkV
w13OMmWRUt7RilZ5phc5fZk352tuHv+zdquT4qNFioePQDXb/QCWd5DQ6YEIFwa/dqJo/j5ONDoC
6KPc4J1ZPx1NV7oESfx/JY2vknmNzyK9iJXArTCjIqnEMePhvutTUUsdxhdBIW/3xuCaUeSnKT46
OITE4lGhpVQMt36z+7AIeUe9e2bFnMGdY/4zLyBRtpU6NhgKRmvQmphapCK5Vv/QdDe5wK/k9uB3
eH5GGFSdyDu7a/C39JSLeEcOnRvLXGCFEDcYbg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lTAsy6phPdjC6Mr46f5wZ/kBINX5uYP5jbbSEJwgajISZz/o8izlgeIvF2ldf/EYbvFlTYuISq6o
zNxm0VYRyT4nND58ftOmq1+odIws5f8DK3ANwggzEebaUMRhRknxdtqucuBD+VaeQDH/FXm/OQ0T
L99toYwmI3g+GY7MwJysiIzj5Kbe+YXS3rjY8+s7sMutQY7F0lIIAJp3acPj63OqvJs1EcTtirHE
yN5oQxODRVpONnE+KaMrJZATHLEal+D4AsxSgABgLXsVNr313tgnd8bRI+7gVr8Rx/adD3gJpXLm
8XLIO6gjOgSHu0Z06FQxdc6wLTfOWh4lXbOrRg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9600)
`pragma protect data_block
ZI4VduM4wjNGqmPlsyuxg6/mklwSDj//jcSxoGOwEVJJmXTOMwzoCf506NM25LmhvOQSDzqWgi7w
kTnhnEbVepba2aF77Pky/CLMIUFUiGSS1PgC82lJjDNYcRqFS9ExcvzbB13n6fLSPlP7quliPx8s
S8nBP32SX2aJ1SdKSkP+X4d+OWTMUi8DyvESz9rL8djeMD1p1p2cSdQXmBuFrr/0wBue9QznWjMC
eqIxeOhcNULYo1jTrbHRO2Ljw8MG8JBT9UukW2LcsqYvpkjzE0wTsu0SSx1GZI6efZYGwzP4p2T0
unKzvzUXmVE55Ks2Hcxx8gkXtx7822qvjmHsCae2Tv7G+IQ8UYMRcPouek5FUp236vup2UwKfWc3
icPQ0FUbs/KBqWl9QagF89CIQIfSMa8FKPpUGk1TjornRMQqmBI7ddvZrirLHUQgCZE79vakaFvg
AyyWz4gXBZb1I0BQ+UkkFgcmfVFueihffJtrsFYsV9FKklma+KtQ8z/7QAkn716NfeyFyLtF4OOZ
Upswn9MHfvthZtRIRqLZG8nN8xjv30QyLzyFWHf4jGwF6sRIgiD6Yb2vSOfAPlzdU91ugicWJLjz
CpapocBSYSC/EKpqVwJc/BuSLcbLAjkZsA77ZA/cUM4qlSi1lzORB0zHXVKjW9O9y0rjrFSe7n3m
WiTOe6gpexQMAEDliT+ftrmfTDj+tF0+LI37Bjgdu00rjU8tG6Y5wwOeGah/0fbR/tAswAj9yPW+
2/zxYcKmXYTkMuRSqBs45nsvYLYU60x6skNEA0OjpDiFSG/AAQE2wXMUtf3y7c9luquC8d1+T2JN
/s0t4rWaY2l6NhLmYPYtuV4JgnqzEJ4BAEDeY25APq95egLDXilZ9sDOGCr3c12Za62+qGaJm+Jo
E2YzhGLjILEOvun4GUG+OnfLV/RTGps0ssMIq29apV4oywS1PRYbOO2kqnHM9m+X2i/yG7+VYOT4
QOhiNamWiF+v520m/EZYuHA1IMuzdweCzo+gqffnXRmze07lyIP9U0044f6QbjNj9hLqUTLlNi0n
7AwNiw4bKgN1wXhAbzUrlbESB8Q9deaaakkeiLN0d64wxETde7ngXlBsB+W2GViIv3gEzraufaYP
f/NBERrSc6vqRVQyBXcMO1aCidUeuN+j73HIuGX5D2Se4Fj0kWRWfRGQj9MKLyjmmlyLon25197c
pduqJdaph/tGCFNh8HYAO0QORP42UxrEsQFan+n9YauBrh1NEpcMFSPL4j0MPLKoWoMmNHgUW0Rr
lNp7cChDHwNS1PUSpHKRu6oUhRE5d/nSLrraZeLBvIFCNpIVVZfE7F4FCxM0Cyzc23nggQWxjVd1
1JKWdb2OpPajpptK/3hRGsxgOWVXlx3rd9Deuv97BY6tBY5hAS9PA/37heFyo4vxE/Lh73GtqHqT
1/eVcqUnXApH9ay71o68OO+JONKWEPjRunB9exYqpRRThFzNb8T8IHniUm7ZhlfiPhZUyl3zhNoS
YERJkSTcq4WMYY+qv45dBIseMVx3LKwwfVDN8qmPe/Fj9GQ+W/lKv4RRo4Yzmvl68Lzr9lbk67G2
Ej5c72znklwlYKPTrjMqzUz7t8SZb4sXhXE3CmIeDHxrQAkWz3iVfSCtog9MqceWcAh1qHDoXdQp
m938QXzNyZlnu8+iUFouVbDG7of1lDUBmz3wjaTwLPUNJbNnYbxjDxqe10uOmOtkZM5964mQsWfu
wa77+4ZjT4BWkPK9rwiuup0TsMiqsrnPaGqBbA2g+U3WHZIGTAJF2O1tHvISyFgkwZfXgssF1t+3
HX40262tLT/6EdXonMihzCVPRTplfGP9ShqjvZ635Y1MYsw6ddjOD2/6zE1r35XRQAnuOUCEF9DN
DQTDax6BxVfl5R9l5YgHC89VDDu2YvN5OISgjGEfVCuJJsM8ZbqTVOnge46iy1RK7Hq+VI7bVzu+
wgdQpJ68i+2CYSe03GBKBAgtIpDndk+VXPXbyl5njw71gbtRxTeNXf+iVx43DuysuDonsfVwX9b3
M5DVPzplkxaCA7V78CFWZO4HzceVcdYxlOS1AQE+V/yMlBLBsBr3n/4tM8D9EX3jJbc3RiwzfxD7
/togR8BI4aEtkPnz3rncjAY7wocw+6Z9g0DBUgWEN3wQNNNqsTL99iLcYxgfcjDgOiOBu60pI0OX
e6chyyjpMOMiq8aoZXVdnpuI0YsnI4OPTIkmo1hG7SO3zHY7mSEegTUT90OLIAyirO3UG/qX2VoI
wduZwqoNNVfP5jaF80eJvhsTT1VG3JFEp0dhHynZVRa4Tiugs3od2lCBENCIB43GcYuGCdA8xQI7
+ndJi/ZfliYeSNChK/7BQe8ITDNsN87zntYPwrcRMJUEkmkHSK4EnnTMsNcg4zHm3L2BRccqiT8I
tKh1WX9db3e69Tr2+f6L3V4PvQdXjqofpspE4GF/RKsXhmIsCGhtB5tixlE9zk16g/80ErMKtHjS
JxQ1gWqDnsI2YG/Se2rEYA4PujpP6qmPDevpGq3JY5f3tVbigJrwyx07Yd3J+NIiIHDvgNNbh820
1ovaYmwCFwrnPpy02FXFg/O2y8LRlxlByRS2YAdH9lTAsUdqAGlgLDAnzxakGlpqsYCQxEMjJC49
GeKio6Kv+GcG0adrJYuSkDhH8iyIZIgnjXatEJCM0fc/nSOonnjdjcr/b5+M6I909VsbaN0xqxuy
2KuZ/DzHQIS7cX87BrYnT2EsonIno2iAxihGu7mvZAc3CvqoNHjj+uzWgXPKxTMtv1e+HAbg0a6n
N4GNWdsO47nQVg7JVsnIgNo0KvfT8SCNOmjncZQwz10rWvndi1sKSQELnDfdGXk2fvoVaw3eR64Q
jGMDLVhlk/3EV8CjfY4nXxHl07WBXHHLBfeExp1aWG37t2W3nsyu+JiPOWJq4r4BqmlHNMYustG2
qOQyg14h4tQR6eipB0m/8fxB7u/Pjd/ZUD0B8CaCu7rVUvegd7OsF6PWa0SxbEDMeYZnR/bKZygK
S3mWAoVWEzY4f/ol0eIHyHqAbE4sS9LDP9HyH1qBrKYLsm1ZQI13vHvdyR88WuyYGUM6BTzNYzLS
ocBZG73t1lsNgRAz5dZu78Jrgnh684rattX3q5kfvAp7rJ6F9AaQS46H9CS1Jv0IIcSpE7/9KEzQ
z5i0JKFXHUNm8HUnArzdSUiTG7kw3i0ZYyjCHL6g0Jx7ecesKQ2tE3eU+FThMX+l8XzT2szyz67h
EqOZwdy/pmsI+GTu+vX0gUMOMMlIyoacS+ZiKjY5SzlcnkYPmmodxWfpIDuuSRSmvD8NeL82a153
FfNh2JAfk2ahIk26ttwMOObGW/EpnoGuvcg/WcQOsYRJGcaYuq+FL0DjizcQsvkV8DJfR1NgXETh
FwAV9YmMfsI/4ojec5foE0wXorvDCQJtNPCOJ5ITHtsKp1ivPFEMu/T5wVwW1/HNkorBYl+IP22S
Y4XWn1FVaHkjl6/SmFLkNncruTQN+aUwmimKGqiko9QYstKJ6qI5nuaBVnrFx6FlJPS8P7PZHlQw
zKh+JC+/Bal+Apm6/ZDHN5pOrErPLBJuRtEaiD9oWRN9r5sposguZ1Gr2e4ofmhVPXMHTEx7XD7r
7J6hXKKKNOj/qnkJdz+XsTC/tDV6hk1DZvTb96tOWDoK2CQ0rDf97mfmOFApGCp+ZQObabGW3rI1
zbfZ9g4Y3a7qE3t4MSq2Y9ZQ+Y6WPdmovkvlhiOKAg0/fB1NnzetKDTPAE647I7NoMzpxZjerN2Q
Lvn9pQwv3mV+m+0DLnhff62WLgEu86uBIobNZ6saIp2qQK/nYpnVH3ul5ToSCXucBLgtW5+SIYUn
cu3bEClJB79caljUOBpGBkil4Ohl1QuJRg4LmegLK5LKgZ1W6ISfQvwtFY3VJ4orMDA/L1hHny3x
z1k0xSGXsIYc/Ud+cXnVSPeabQz5Igx0y+Sbgg6Alfdwgklzzw4GkoGFWnQnbRVR4DNoHgYGzO/S
6PgLjeaINQqmeLPlrGHUKX8UqQgi8yRaDelpBdsxWv8zm65zomgiytR2eSHNOVsbsit/p9r/S0PE
HNPlsb4PBdQIYmlRM18KFLIcCtaxrc8uUlfxnKpQ7Vb4HeJQyHR3NKM8bi4vZNqMOxMFj0hl4iyB
qvMyuli5iXGB/GtXRZeUYY+75r+oSIG7iCZ2nj5yahVi1+dssCdeA/rVDTEy1QvvtTLhZsfJXCIj
PB/GdoZHNTvHV1DdksxcfuKxjv9wlwYXblZeZ7AR3Nz1t97gMUZiJpiCnQ2gPSYByP964Brym2SB
URVFaYv11u/vm2Wanu710Ob7w+VtqO1vtigVfk9Rg0uFjkBKMwajgTujdAKTyN56M7FBxBEXWcNy
CuQFM9vEdz+Mi1FuLCKuIEhG4SRw0F60gzFjfxpSJicB21Qp7ikfosxststMrjFi8unvrg6elKhn
B1LRtgaH/GOW8q7D2Ol3C3qaGhpMyUYOYTyLSj6Q+Ziy276AKCEMsQ/GlQddHor1fUh12H9O8cC/
baauPEklZivdEPn3CKOniI9Ue8ReRtqI62pFjnfA6897BcuL5KwlGP9P+qOV46oQvlvK2IA5QbS5
77I3rBTuZ0EL6kUVYtD17j8mepgp5lwop1K6UozAHBjxpNp1PUy4Rwzwl7RV7kNlnfPlW+Tb/Axb
YrU3c7fRzRV59srplTWJPzYljG/mLbu6q6w40SyW02tMFmFpvxssW0YF2ZF/Q2asQeremZ+9tqqs
QeWG6uKuV24/CUwWS2zInA0+p6qQraAOQ4vOJOzE+5SNldIclen0BMaFlRTLOPUkc2U0xlbQz+Gu
ttzuJQ1wxHDFeqmi/OvZdaNz4+tGvanzGtHdIVBOpGDSPH+s+wquFnUSqvuaP8WHJuWGaqOfdm7p
6MhNeUdqfsVNCmrhXfOdJzxltqcZWdEyFS8t6El1oAwjjXE1vBzY/Hjo0jaxSlQ9ARoixKcn6PYJ
hFPy6l6B9PA9rRNYVGq2LAQvi7aaoJ1PMfmHpqeuWI8lhEx8jr4kXa8a4UJNY30tXjKIv6pPqi83
jxBfSKj8slhhphOhnrlbbkzmTCjUeAB0X+XB3gcRmJo2qm6TZWIhht8OOx4TJfSPay4kaWgNBzpb
6zRC5BYeEQjZsEwyshAvXiRxI6eeNgB+V37ClZIUMcsKsE/OflK75defrVrHufzUcGYKC2TYPwLR
lQM7caRZD1rmMIkA0oxtoP5/AgiMABs/oISxg/PplMWCNVcijugARAMSpZ0yshckYzv6IzBSwgP3
efxu3a2Hdz3jz2ZN/RrF4x7SLQlkWvu1/W5R4uZnWcfo+3yPVKlywJXAq4h4UhNd+7YPgnUFllU0
4vvWrBSRyECaL1hYWEgLr+fTXChVyVLRCN3LkRS0O1rq7IX3Yd0tqvB8og+NJUzief1uRtjb7Cl9
Cnza6ZmvwLKDMqqSxAzqAcf6WKmWYPise6h9BT1qnfhJZPyZ6GpkhOS/1dh9fRmDHqF4+CqU6Ulo
q9EAYdSqx0ppKjspWmVdSnWZE04kIjUqr1OzpqqH8fomX4bitcHPr2pr2qUUfSJlAmUEhvKqfPd+
iJsWPc92XQ3HzSMyBUGqO541QkLduhK/GvV3oelngfnyzhdix/Jr5t3jMH/XPCtqGlRLZiPlcsgq
9Ss6fTUsZRfZHpWqxy2OUflMtXJR8n1pyP0iuFZDPhRxw7umLxtOILcy2lzkJPOzNWGDjMygcUgQ
9YhRHV9xbJgEPnxZfwbZE7h84rzdKt3CBmrcQZY0tlqksCgA8yRCzffAc1hZNctzTLHn3gpZ5j7l
dBYA5TTp97rnnOh2AcmZZushdI4vgB9C91FxQuZtNTAYEXCND9OID6vRUKLkYBV6cHJahpj5vQ1y
L2iG4Wy3kkfHAemkEOyhjTtcvxVnGoA9UOCktjypb7ZG0lPgWirpJ2R3rCxG6E+VmP8M2ZQ+ZoJX
qT+ibXE9mGsRQd3uQoL00YX2OHchSs6m3JUAIvQZ6bn7RfmmCkRLHOCWHpJHOQbp+LyJRDqdMpxV
oT5xXikOh2Y/yjsPLbD4FWEtP8IFaLYGRFb/an1ee4+Z4/RA8Nwr4AswlQOkD+l+s0kTkIs60XFX
oSGWzj98t5MRUKhZEP3ENJfIuqYA9zjTfBbZd/QgOx9l6ExLBLcy4OziIhbn5NLOXSsKgZP/cEkr
UiulvXUDiYiWqEi84aTAJirPAZaIG+7eg4MkuU3o0YrhiEJZXvE3QkfqFr2oJOv3zdhMlCCuVeTa
bZmORmwvPYqeDq4kTkBBk42MA8LmIv9QToGVQ9g8BdNJI2o184v2lK2FBPJoX2Ws02zV+sGAPhC+
9KnLoMTta3eix8NHptZotAHRNNWQYs7F81eL44TNONFJscls8Vf87g8r6BDXVfS1UHBTpwCsYJHl
aj4pr7pJ4nS7+1Liq8JixA27NWyvkXHLK0hxZaN6drCr+ML9STvPGQSzVLz71TEmLiBD0KGU6xES
LkdSWXo1pok4wmQ1AE4JUegNqfQTibpt0h7UsaqcFhv6uzm8oJy722HmNODMr8cBATXS7ZZ2QT4h
QjvXviW8sZ/3e0G/T51m+6RygI6gyDzK4PCYl2fT8VaRot3kKAxabF/nEKCFrXCKSOXOOMZfoJe/
96ImXEtveccaDGHReQYEe9w1R+zYh9ehYrDydnrEStJy67MjSzBBBbAybmgntehx4bSwfrgCD3Vn
yqzKkcgnXJ5FujzCqxPt0gLNPaox37IN5nCMsUzAW3xSE5GyM8QlpHCUIiCD7jij/MaCw0wBz8K/
BoA4ChvQTC99xEbjsKDd6KKRQo9yNSEx98IHE7h7DEJUnZlkoLzUrtK6/23MAq1zK5Zdt6We7e9u
xC5luAJ5IhTLFe3Sq0TLD5yKbOWHt7lXckq1b7emFNLHoeHGecucKOTJ2HZPn1QvPTJayCmaxa7w
ORs6L4DeHuxFCf6E2kNHLDMZmsGMToO0pVujpBA5zCneZ7Mr0UtqJ1mh4j4WHwbt93Gnh1of+g6V
VNPmC6NMECwPzUDbGZ7VUk4jJE3fiT8tCfr78r/z5DPimFamWPXnAJI79OTz0x10aN38oIgzJSoY
XxRp6LF+k3DQfiCdpKOwwDLjQMuWkn+vj0FCsxUcqn/YxtOUqiLGhQmU+3yAxZh8GCXld/VU+j7c
Ipcdz1HteCm8ndSMuGmaW7hf6bUDoj/nCTsXiMNB8u0CjJpWl+3nR1jTmZb43f4BrIJ2Scsa1BU3
Dmp88Q5MHvpgyoFf5Gojshk8z6AHl4thtJ+bPc0yk8rRpoSMfTmBXY2BrxbkXPgUcSzOuQcgqgaN
fuOq5Bewe8m0g20GYN088tO50mSJ7h/82963vdty5dkMWk5Vh7t9raQIKIuGDqDQ/4TdEws/VbSt
nBZhJRAL1Ex9S/CdoqopGbmuXx1GFyUSZPtwg/Mlbsh5wRUd9g5QHAcmhLJxel4YF8m3ch8CUseG
r4UlvoisLBhnxO00n2nuR3Cw6aV70sh0tdWlsyOmrOTmsooJbqtGDHKPjr+WDVFQQluGMSlU0jo0
aqO7cj86MLMPtwlRT+vLNL9UiMyNWH4fir/WFU4V2QX0WigDt8llO2I1KfVmZtERj87UGKQM0Ek2
usi0eqqKzGi5gvDAS0HVfPdsk84G0iUfOMqmRnj2jO03SltqJHjbBH71dtswyCcy2du4Nwx91SYi
diS/a06geaLKxDVu9R+hc6jfy+S08jRzHOUz0jShimU4RVsSTCpuUsLCShZ8vHpX805vrB/j5ZV0
lFQ9LBnh9uJHfXA9rgL0LBp/Gt6R6oRFT3Tyxejvn9iBMoF9aFPRG9kQarTMBmxeSbCYH+Z91MeA
oMkSI87ooWYP5iY8msUi6AMZ2k/AH5DBKMsiJi47Sn3dS+in31ZliuROmxYL7uVwNlobk/dgPPXY
9HJ55l3CJ6bL5bR3X2YLy8w7GbApUMJUsbq/ZXqgFoyO2qddIRJLWzT81e9gkYgwWf/vLsW4/TX/
G5h0FrS+zZQ4wBAkWuQDN4hnc42WLqVyyFKwuw6PAEBGAfutM+HjF7+nIKZW18BITM5GkXy+dpLq
yIjjb6oNitjKFzEz/Y8x+j8jEoyGi6XVD08+X43/xyYM5W+ab38d76KN2cJ7zoGcXHs1yXGesHPW
3UlITfHgd8lsHtoBgyqB1PDkYWCjnH8pv8ns66ZaUAoU/Ecp45uraGbYz7gItkPpt4T+CZ31X6FQ
o+IkPlGWVNtxqW5FgfNBAbghHMkV7LYJLouDWKWM6XOLU95X6a6BLmvnTJi5ssfHuT8ht2tW4It5
jlO5VjP6vrAeXh0JFigKl+OdRByIjXI8cwXK3x5sOcftGVs5Vq09jUJ0hmi0Pz9pxxxbemMXZm8x
79PVGcARFy/dAxdNLVYOGdsw6GnIDLZGv9FGrEFeWBVwnTm5apUzl1T6u+Oe27t1shVR+ikMMOi0
v9iAsfXU9BlTyRNzRGjCY0x2/v+YSUDm9/umu/l4hWrXt3FguhpDXBormn+kfrgUgJtJpK0jl2Ql
AYRpuFMyDXzdxdr/GyppHtdFLdUMs2xqn13I+RJCKt5xoaksSrc/e4zGLEaoPnyluwKvDNebR1U8
culM0cg3dlNSey1rFsfe4qV8pLPI5cXhyqA/oJ2ixjTygr2xF/b6T9KWKy5SHFVvbDwcfxcid91q
3Kia1dv2Ga2XIkugkuj3X/uiE+nFPDzcHPJHb2gDnYbcVZvY+mpODCB/OzRHwN/Oh3mR0OHUtL+q
5bmGYZaqiEDD/696ft3yri9S0YBsQ/gmYI3RkcvaFElFL4DrSQq9iXw68LZwlhv6/SXU+7xudfSH
isQhYwZzkQPmTEDYN0A3oHeUtHFGUBGMjxCV/cumUTsXyBIqjAKRXlGO/Z3MSQxFezDdLKqvseRn
ay+0FmJHDkCMVD1iO1uQ8g8znzHeqLTwNkldBNhJQP4/TGgDAEZVrmtWDzI6+GPvLbBOSExFCNQF
APtCYWgJ3Nj9DICLOXayGwjqlcfw93PSlAOyiG7uiMYh/sPDzY6cbW48b2m1ZDP3AVdoP/7KhWxs
haSy/nLGr2bv6KQakIxiEIiHFU9dGaQxTXRjVabIMipWFNnnxyqQk9dHmsGb8/UkIAXvkjgZTALo
ofmkRxPJtWxHQsnKZNEx1zU+rL9IwRvt9gJcLlxvljruZCPyrgROo3PJdhbF2FN15qwcD4KDk8hz
50H4rknAJCOVhoWPOZfuoyzY+9+JRlFmtVXjaI7HwP4zQ3KhMwK9Hnqi3ZZMBXBuuVLyh/kL874+
grWuTHoxKtdfbTbrsU55HHBlGvjayLEfeGPNxLuE9gs2zNrsEvz4GHlaLx5OHpj0SEAV4pV1w2eh
8mlHcb1uiMuWJRAgaDqs1jWLb1qKRJxt/A5IXl9TarSitgV1qTz/JDnO7TLJUpAY+xYL8BpYc2iP
vnaALBcjrs4j336jJrcEKBCt8m+8BC6N95dfbUZzI7q3H1yRp+iMq62R1qvSXkxl4SL8cv/0QyHc
nVxGmoA+Guz4lhZxs+As1xR4NfoW9T27yYazeTYFe9NUoouNrVjjxzt27mOL+KnPU290angTx2/1
nHnVqvx5A07D4XSEzyOCVT5A1X0mfqSWeGVtesc4bkUHEQA7srnzM9ZPpgnayebgNMXjv1p/PxXJ
53uK42bAQ9u07QMl4ln67peOVtaFYZmdGMIajakZMLqahunLpIEK9nEMXz/lXcZsY+S24coLj6tv
/CgNxP+Eybw8P2EGYLUBNUchIcGUDAPClaSkhlNKkr1pVf64mHwaNPGpzEr1wvyMWqp8Pxmi5wjb
Byfnu4WKjawNXuuG7YZTBvq5HCA8QuK7ujifrpDokLsYtbhpml/UA1Yun28yM9hTmX59BLNPNopw
0GrhnR5rljBMKyiR9m2vE4reAiQobcp6zrPaF9aZ0zJ0/lCMz1pix3m9rbU5a46ttnM2KDXr30yz
AbxL8nGb1ux6OsFCefu26r9uEzmxYXbTdFCkWSgvEGJLYyJrLrPi8JTDDM2XY4HfyxvvivOiZMjv
HNKSlg6n/chHO3E+xFAMRpj4Rlql7fgaSicbiqEWNMIv+mhEiv3Q5HyGRMHlVfkH4gLXnQXFuNwV
+Zos+uKa7HffN/nkrEIqKeHqHG82uqEU2EW8Aq+5QGgRDh4jgXHfuGJ3MYgcU5/T2CbBX3mo9oh4
vZbJNDv0y6Chs+BvKH7eq2oabPbvEGYzZyPkAQOnEmRaaBKccMjDU0w+PukTkXixVmRefrSgZqH3
E/knwEl72AM/BU2IkqStf4Oapqtxhdp8HzsGs7ke51NFklA/2f6V6UQjHXbkl7d301SQv+ASkC9N
c4WnP9Y9eu5PW1bcQ2zLzPOIw0KShg86c+JqbIPpjOSnqdDzHRnDJbOBJFklE2ITElSaKzyfDL8G
fmlEb/l0aaVvc2XEH+/dZnWjrNCrzDl9u9I8vvjHBv7zf6r3pAVWGB7a7kvBFeIGJ9mBTSACfwoS
Dbrcr2dmqW+saAJ3xwZ/dr48jOB4X5FA5bVFdF2CAPejnPvijxRHohIF4r72yqvY3BbRnaSaByoC
ABSvPplgqucSxtxJ/G+ZtHS9WdF4cwmyrU/90kIgoY761rotujeo58nB8noxNnq7AM3eevedz3Ja
1zP6pf3skBvEjlsxJz4XzaCjIov4FZ4bJXEJLxbNDLBLUFEAP4wUEoW1oeApTOoEONTZQ89IHFf+
VLgh6u4G0HFc5LMXZkg7tFzSZNzwdDkDSg5Ke/mNc+F/O58RiKAiiTRG7XHL2xD75smFMFikbDw+
XYAIDyXgWQjv8aKS+nnnd3YARm+uI5zkBpeZYeWA9fKRyypz3EU0zml6h7hIkenbfjh9UQbAxRaf
1jBt9J6/o2/K1dYI/W+AHQRNEUC4P5pEkOAkrU1CjgU8o85KqMA735BaE8bF0OfqPiEtmHpwf4Cs
397BPz7pUqSnWBQhyziqEyCBbbD6tqhgRCij2HDwMlv2f5duWBAfTf1naWOnBE8Y5MJ7a8V67efs
kP5UCFLNY/zaAWUyXnI8anGlI5wmGSpzFBRWMTCru9CYVnrRD5ou0tQ/jGef0IlJiSJdLV2WMg19
IftBdQY+FWuntDUSBrbGs8J3Cp+QOn+2lYw5GKOGLGDF77xgIY7vM1PCavdkbAXyTErO1ESEbLX3
eNZCnyf5PzARUuOvLKNUE8ShwsiJNZR3j6E+zoghmTI3GiiD/tb0CYDdpwMtxBfk7KqJbNDWrqQQ
mRsiIabXfLvNA8T1pep2sHDPz31iorPeTuUtMWFeBxwF54IlTI/BrC7hpjUwKIFWW+7ko7cX2eV0
nHECjupr5dgaOjjBcViq8C/9ZRtzlMeaF3tPERlZS+DFa0WNqROpBJyuCemEPJ78itzHtjSfGHzm
ldW37N7VaJLzDa0Dc4eMlxZ1/UClXMX0TDlac5IXg+uX+yv3H3Z8R5P4PrasHHvPZhCJ7hmArjeh
sVoqcRkBG9qZfkyPREukW3W0GvROsIfZCQKk+VSOnO0pjBwYRrfWKmyewBD/HolOkNwDExn8CydC
H+39vY6b6Y9bEdzsS0Plu5vRCwjPZxB4s85u4+JNql4lsPAlVzgljRM/enHoaL1rB9L3XM3OPkyd
lpfSOIYXNIQPVOrc8F/qKq2Uicagcapxg2RqvTT1k98/s0wTQ9zxcf9x6kwjh0N1awoOBrJV8Zlf
at76rz1QI1chZIjmP9pwrhFeSoLF04zcYLQqP+GPRk6aoFwzUDVPUf/G/PvbxBZNKsc6jGcJeMs0
eFQmmnhsw6nYVDQ68IaiozNscTqngVgHien3s47jDoTG5XGfPjJulj1XiugyAap5eIiWr72FPsWu
Y57wZ0AC9xXHS6+ZiUYuiYzrf65/dFxS9em9Gg2Zub2aCxoLTZIbmf+Trfcc4h+BeprDL09jiP9A
300FKdm4c3GwQqUm+JIePaRVcjt6/SYjUvQOVzj+Tsu0xPvw5sRqLqXKjLWnFhKXuZXWXjTt6+Zd
bDlD10FW1zMlI9ierpWxVfbFg7JtWL/cawYuEBVUTwXf5LAT/nQeMWQJ4MtgeksQ1dCnCyqy4Biw
C9xifjZZ8riATQr86XprTQEUBKjmayFCFqZckEENtEDgIESjj199ou41R6896BIovzK7q1PN8Q2C
sxGnOLlQrl21n0izNiv9YZ/lYLbvVTOOAX0BNSVNinXmhtdQVB3sDcBTd+W+InNILbALc6O90umS
vAFOEBJYr9E7j5zIhpfy4+eQ2oubdcZIUi16Hp/wCQb4SaH8A3bQi8LruYVXl5ZtttVUy8u/WUbI
bxFOYj5kfobSZZL3mod3ULLoqFopuJvFrCPCzUrkneveWGA5GWzad8GdqLG55g4+5am33o4emB++
AXke4QIGoRo4zlc8NmnmhQ2nfnlezT4N+nIfpFJQuSHWTUmbEKRKJqJsDNXrfHsu/swSqMh49S41
oGNSlvdQQ+xCRlN/zSD88eEGWaZ8HLZGW+4WALGIT1yq6tAWSpanJOVRFOiczayFUnVbV+R3w83j
1Khe7scX/XmbC375PhtsTDc9N/d+zbnsS6H7bS0XlpM4x2uIf6mY1PWUiV/MOxDlvu4tN6XKzjb1
LBYD2Sta/DW/SjIVngXWWImx03UQidaX1Ram2gnv/TiSFjRScPG4XQQ0WxrSLEy0UMFw5cpGHWpN
e75qb9ZDWCSSMfEzBAo13T+uT6+L9j6o
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
