// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_0_2 -prefix
//               design_3_c_shift_ram_0_2_ design_3_c_shift_ram_0_1_sim_netlist.v
// Design      : design_3_c_shift_ram_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_0_2
   (D,
    CLK,
    CE,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}, PortType data, PortType.PROP_SRC false" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_2_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "1" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_0_2_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_2_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZEvbP3BmpbsEzNzKwXv9FfIdjnyEYErgGV5uZ6/x45t8rDORSuraYS7IefdTdRyF6pufcW9y7Oyv
5XQSBclEYkwRVvIi492OO+8OaLrEq/x2zHIRfWeH3fS81IUwKQxvshTdd7sDOCTKL+L6PIekfIM6
NpTWW4Ne+bmEjS8+gslJvV2MZekDCwimPb8Rn/PXP5arWs3Tea0x60gLUTH5GaLojGiUGzck5IR0
AGvhQgc3wO5lJB3oOtQ6NQvMrAMSkAM34REA2QN0uBXZN5n0V2SLqf58Q4XE9r+aX2Yy7wEx56aC
s6cD/9H8A8HmTMWgUkN+GzbRkjG1CUvKMvu3vA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RkokOkTBcySD/H9LYkaaYC9NrMv0V7F9uD4DLqQXWc/hgKA9ZRWXyhoNH8YjahdxW1lYiWKbPWgw
1tV4fXMlwNi5BtmqUfztHHNKYA236ShmrkKo7cbeEoPiTYVISyslj+RZO1R2ngC7g3PTC5XMiWCh
wDzOTIx7UmycavTRmS1iHCTeHzwuRYXJLOSHmuYDNBRdWP9JmX9KnPIv9Q9WQ0Z1u8ZeLHcHspuu
k+893AiQMEGuX2lJm6Ziz3s3FgxfUf4kxi12+fYt3yv7rHE9n4y7lZxuAFk2jN2Cv4J+GYMch8qa
9gQnbvcgt4aSYXZpcRFn066+wsV16Es7S77rpA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9856)
`pragma protect data_block
JN/W11HxtMYzFmrWNpSR7Q6NzTKiW8VcBKjB5NmRFuKE0xoab2MrLlz5CstNALdzV/3+drCz5tSW
R/60SaunEwBefdgEbKnSDwdb7iVJQuylAE7cDxQVWQaEeK+dKp8cq5Lt3+7lERIv8ohl0lDcnRsT
r0uklOORw9mT8KyuLoyy4kw/dafu+ICDM5y+1CLiK/Amj+nIfqDrxjIkJL3QJFGkMNsEk3mYeaf9
5emyLNVe4H4Q2fjndrRRi+CkMF2kaVSgtrsighdyxCCkbK2Wdlt2yiUxRl//ptQjkUYCaCH+UGdM
C/S4YSa9NajA/MFcCSHAseUUxLidlRWtIvfYvObNiAxGWPfYDJ6kkx3ChH3Ecuy4RRoy3rJ0k3zn
J0jL1H2fBqFrnT96Nf4/K2PXqlpzT15T3D+2lcmYnfBSbkEw4KRaQgRIC4cgMX2CazXABWEcQAoU
g+JDZZYHzVWj3ddz8UKDl/C16mga3rGRd9woOTC1muH8s5kxvzX+hYUvvXlzxEsjg4JKWSdheigs
RDHPI8mbxGyXY5N/QQaR/G88iwysvTf72tG/B8AWgDsGtQqy5Gfaj2ES2Tl4gcuB/DulgI1vzShm
CklL8aUtlhfUeLn7asMdnDZaZnsMzTtCMpZ+8hG9t1IYhUIlMQzHZZsJuH7NAblC/bQSTtwxZlEK
cecKqwpc2AWDcMuv5t3J+a0mAC4cdmmn/wmClHZU5ZIaRKeWKgWo+IkJNewVZioeHKCIptp6d7T6
ejngprrw/evdV7pIcdLFKkH3Jv8ZNGqs7GskBwE49gvUCysOS9jvO4cuCWb1Za/N/AdWQDoKRpae
9HnA3auxH9r7Aak3zv5RZmG52VDGQGw9Mu4VyWRIUW+nBhEr8WcREwpLRnH5cl3CFDQNBXpjJI/u
XxSQd9CztbRQwKj9NRh8tEAOwQrTtyzPdLbYDcDrSkqS+gdSpReQvIhU1kCTuWSt1D/GGLMUZdKL
qBDAMFkv2xPGzr8YZ9plMjtkAxmeDXfVyR1QVCfyj9/BrVqUCShc/bxmTpwTLAINqPRd1hDMc8e4
ir1VnD5Emwas9vNpKHOZt82BcXkrmQH8J2BjdOF53zvGGqLqQh2lPbqSha0Cjc0Sfuo2YB41Oiyv
JnbdkhLmIE63C2P97LHDWIg7CZJRGZ/RGjUAa7CA1LUaotk4N86REFZj7t9Uw2VGrU0jsagg6eEm
n7s24UrSaRlfFVOynttpfjJLoQINAtnF8++KwvjAxcQPQYaEQr47gehkrDeB03SB/B1YZtCaUfm0
Bha6+0G7vUcT27LzRc3d2CTGINLcDy0Sb5OadFcrXD3yMg05MW8sP4VS8VN372JDLqG6jXDxqyTC
ehoN0cs7qC2GX/3w/NTyc76isLbxjTyc37bTvTyXP6UyYtkK277vvHMmsP+zLNypu0dVM+vcNmrU
GTeoPUhEoZRX8TeCVib6SQP46YknVw9GTkvvlNw9L41epq2cZYwmF3n7DZ/sTvXKP7dOtxWl0ohS
SZQzaQ3Eqbn7IY4oMxvuOHcnnAlH9U3xEnkVuea6npOJitfvXFeCgvzg4MvnzDRCNdNyn7PLxxY1
wrI3PbF8jA8c1PhuQfIbPMDpQlCB2r/FZKiTOQVvQoznfVhJwqg8ZAigTZfq41Ik1vg6sf0dmKXz
5INn51dRZQvFgO6T2vz59Nm3Am75hV/Jfu4TD+1u43CazvE6zfYOKmJIMAR2YtO8Dng2TdNJeIrL
gnwkdRHC4KMAoc3KIH+4uv0rJ7vxpeHEMonCb4qK3RPj3VtwnODFppv0fSD3fHNesHOGWreQnwci
zEQ3dascW2NW3jjqiU/2yyB1gWs1HSR1z9zy3D0MpAnMVNSofk0uGbexDh9rRxW6odj6lJzKKIHm
fcUdLxIbrCtcd8yxkaXMQLAhNPtAOXAGBQPNuZkR09e9SbMrJnmASs3jSPqQTGJWFyqGDLZGAI1B
xftNDpzOeb8shJ5k55lH0sWi2qAVZJhTFPdAuYlSDsFbTw0d0TrDf2WuqayOjuXBTSHpDiWRiRsL
QfWv/exJcVUpCiiTIqmPm9qxnAcVe5oHOstLWCXG2f0cPuk6/hbbuFPeJL4ehFAAcBhCcdb0Swqu
6ljnC795ekVzFv600rhP1O+OC7OzD1x2tz3gESOtZ27JVDE1aq451TatRiKz+bKCUHTeu3k2/fuI
6VKceIevJP74VFO+Q6W3jXNPL4m4OyPmgEEKzysq3FnMdI/AJpoUYnbk9UJ8FsIz877YYLtLTTvL
1sY0S23x0xr+ROSfx1TM/kAS2X1Ov0xS4RUdKd28X1gjX10bXNjOhC6KW+kWMiXgtIuMAvYqmZU+
pqtaHroCBgVPrnRO4eY4ZyDxhTJ3wSUjiB8hnwi3kI0boC49aDHbAnpRilor36g/XfhyuziiAcd/
47xvNfsYnnI5gwrKvV4Pj4Wlgu8mw0xJD+9FnebdcHi6LlE7Lqe3dFUNI/CLHNrGsHQhxrfXCRtz
TgzKUsiQ8A+9rDQVk8SPgjFHs5rsuaHb6vn7A14l93YgieLdTYRcmDuq32u/x5FZE60cjYUkGNdU
vBuMTr/Rs+/21Z7oinWMeRa/Toow+Mg26DgsbIznRa0p8u07bqLj6l+lNCH6SrKjdIaWKrEwTj8T
idVYZWVEdJW78/RCbSRy4VfraWTED0LArI4LVtveQ4gcg1AJSfGa6wrS7AX6bpaRfCPDVimHeNLh
I45JFd3v8uUkF2Eg4jZfumdRGbrdGMhq494oJL2FX/HB698Vd+5P66Cpj87V1uAbXyhqdXiDBsBU
gWSWQvdv6+miYARnZBR9PXk+4QB0x4fcZzFiNfQsX9XN5Hx9J4PONkzKuaAHRf6I5r02yDpeZBIG
2nbYnbPzzeY1ypCBlVqDPaStxbvzyiRBEoNwb9ESLKShToK69Ri1HuFDEbYHagtKlUU3v+IKnocI
sZESqrHIK+R98sEhaenDX0hk3AwaFgtxXZ4rhNwD4LlfTqhxwp/SGT1trSXLsyurN9OLequyNhzq
Mr7kzR3kQgvnFEUydyxv0FhIYZqCQ6CjLuZN0Kc5E9abyIJUQKU0bvPe9Q5ODMEWbXsU3ywjMdxt
Ue6PfdEdyJmaKOr4k9u2wwszR3kLXE9c7qfn+O58UyDdECnBwE6jMRIdBpP3vFQIS5onCJ4eLJsl
iUxXFN4eRMmp3oalQto4MUavMMuwMUqSvbCn00SQz2vENir00nkd3QdoiM9zQbp3MIpXa+cFnIvZ
+qweDmBPaE1Y5c2vkf3PEi1Z3pNh1V//l3YBR+iPT39rXzr83E1T0Fzkn22Asiq2YC1OMpmpCY7b
oZbYlBUpqqycNzqBm3GRrQTQRTW/tGF4Cjx6+QdV2MTO/4BU0k+kSoxzDQENWzX9scgwItXwTpbH
YqISEOE984UwMM3PdLvA6mRkmIvHlSWi150+2qXQJhMKuUo7fomcW4q2JeCbjtl5sVttdh+pfCfS
Vm23Jsfnitb4kX5CXRnVfl+x6W9JOKSAArye8QOZDoxbyCRNwyrYnHX3kAW0DiYa2/wUTmKqTXBx
3j2Zs12HszZS3KAd7O3y8LXQJjScJtXIvOBJrfGumodt0e2ydgeCqJYaV7pJwgrxYJ+uyILjEzy1
8rSOl54X0RjkWLb3Q92KHOPKIf8MqkUof1/HX+SeQxgjMULQAQd4kju7tZcG+MMGtkb9N2VYz7hM
Ei1dBsGwIFfgMaHvZjj249PQKBJN0lY/7DV4DMswhZDahqC5xXHdoalCk+RNK2Hg5n9QRrp2qY1q
iefBAvyOTgEYus0kGRrHm/6YMCGBamPQpSePbObWOFNCRtbLzZc5DqvaAZWUJmxQz1z50CoiGqbD
xEYETv3JPVEvbHKhab6pW/v+a5guuPlJ3KYS3C1U1pM+yclOFNeXjCQ5To4LuQNC92C1oX+RCy0G
4MriKnuY6mAPW7h0VnbCGTmsIvrhw9KCNEodeOINZKEeIJRQ9/c88XCoZXgEMNg2kUAiGBJq3yno
UEQfilllq/70RPC0n9REqDzPEimK7IhPAPvm7smgqxg85pcTvbnaOCOP67aM9YFnQbJwi/s97/Do
DYQG7VWHyEgNeeGIRtotfY4YtO7Tn3GDuVbDgtwdvVpJ+YsYTuWwzIDqhsNTN+j+MgFAooiGoE+G
kK8CjOc3H3oUT2VCA4tH7/JYsASnvN6pt8Sxew6kQHWRD+edazeezT9sWXi3+uhVTvUIlJw1gy9a
mDfbsEVcPkVWzHBaJ2LsArmK/8bsyDXFo4iNVa2Gq7c/rglEifk/UKKm/4ehtAjNRYcWqSbD9D9+
tzQQ60eGNFkdCO4l27IztJ+Eo+ZaSfXH5ZofOPUN0YV7IVQBAwAf6Fn7gi2YY9R6XBC+5hd2EeOn
8jhy7GVToflrB5S9reaZ3AFbiyC8j8Syd5x6gQ07PYO9XeXsH4c9t42kVrJhkoCTCKrxxmv3WNsY
Zr7gO37qajCK26B0pVOzXe/mep2ykKFYSgcQS7vAT8NkPptFQxbOIggWMSuPJJSgzzcBsLh9Kx3s
lTOCAFbuVGsdEemuS17/yxzcQSXS46I+N83EXPZHE+wgs8HhFV6coNBOkZNVaiqZk+TzJDWdvS/i
W70Jd75bFZJH2LtYjcxtfp+XEASKErWdLmeNNlugZ8iR+GESJN2bNT7yleEAJtVb1jOt+R4G3Vlr
B+Vr1e1boKzVJIBwxH9NExvN5E9oyHiyI9HrzvZtez0z+fisHLt+PxlEevb8qTE1oe2qNEn9//Or
Y6u2z8c+eErgZ9RsopHjlCpjJbKY0sRn7MhifoTm7A1ppZNdozuPDXsoARepq9mfKG3n1S6UM5DU
V+tw2fxPO6WWThsiK54UFH84y2xkHtN73wDm7KEEuL4Vf5ox7xLlTQFwdfQODz5s6QHOwQi7ZHOV
YtT4pBBP7K6+G2cQ/54sXAkjFCGEezfTf3n/k6RQk3F9/55zzUUWoZA2FZTu+gyytBhq4dDEugI0
+gzWuYxXa49S4pzMaCDPvKEeEhSDeYMyhamZA4nHX6shUNZj752QMAsem7VgnJLNzaPojYlpmc1d
WLxXkHqax6dXsw6lo8d7XtIGGVEtY3zUj9jdWPaAKa7RMRQbqCDzwwAIvbEbaN9hcrXHTImrGKF1
tfS66BrqsmySv2x7y5CHtj4Aau0MogroHUjNNcjFPSobntWDOLCFihP42AHZ4M4zWawHDt4nZ8Ku
nVDfM3oXPD/Mef0j0wX1PVyJEt3AD6oKb6D9uQ1Hsf5uqMPSq28+8UrdQPfXjYlXy+Po/thNZCE+
5kvdW3beptQGvSBstrL6zcaV7yR9/+SG2dmnx/KXhFGFsA5Yi/ZxpIyltXKWdAd9zrWFpLu/7eG/
UK1JJxJHHFCI0tIWzhgAgPRmq/EznpxmJusZJiuzwAf0S28F/gwz2Vca10t1gv7iU8hLosIFhAHa
nv0uWkIB7EKynafaqAvx0mt7PlYP5tD+99C2svc531sQjUFzBpp+2K3BK/n1t12V/r3rZGdJUX45
Ye5rA7inY4u4d8scmATLAmZ8YHM1diYTuNkbYifBbSSnSqcavQPcuVMtmc0bSNKk2EJEt5SICjhJ
x8tZG/L6OEAbSuEIKiRFHst3nCgWeqf0fFMYBwv7Zw0obtpUzMt9fk+5e6VAPJ0RuFd6UyCuwT+z
LUTLxVbrVKEbDwbY+1dldm0hoK86KX93S0yd0IFzzKeJDT3sRsAC/3z2vCZlP6VJlBqtd7DHGrDm
UG3RrmHcX/QWfjaAcWoYKSR2i/gPAxC4nAmkDglDf4kBin4kD3+8wRBM9VZujbItHoD6bS4aDCwW
NRGhIuCQ++wgqfALyksWNupZyomPOq/5FEStmKa+U0mS+/hmp5+OUHrZCBPBEkYzPr5SCAsGigVZ
r+tGdqCnkTa9MNQdMolPMI19BBO9JI35pJ3kNHFG2/jJBc8ZjgRf2u2/MIIqj5x9ihx7s1BiSne3
TnpvhAWVt/AdsQ9EmvpM2C8QhETdZ+USwWFlD2hC7ZvAdTO4uIpBBaxnQdnleVbNlqZkuhR4upmU
U3aJGJvKNIteCP/n/a54xh8aSxF4yTZekLWTc/c30/wA2MC9OL5uLin4ypqVkxZKWRZgLiLzPcsv
MulX+us5LHe6/qaDRyhmV2lvpfBLsVwxcv8afqj6xIoywUUrBlpNvEfILoDf3/xnTj8zIO9Afks2
c9GnpV/qmwa9dFgeJceYpF+JUShdERlgIkFJShecsTV1DAymvDniuZQoW/wFqNo/FxTcIl271d1j
e+hn1UUfakm+/MCptT19Mhgy76Y1fnhz+esRXFWQxioO88XBTRWOq7wl1bxbCHi5pLcNGoY2jTDq
WLWKfaBHsL22p21SzYLOUe1irc/6VtrTir6NlnDrI5i4uB/lWayACRwW3FS+41xAVhEL+57NqfKH
lCcCszJKdjOnIKFPA0/MURQSnz7VxhFxUC/vb8+F3Sm7Yu33QlH1K4Bidcpu3Zjp19gg0EkI2Ivr
uR9enBlaNRzjTLB7zFrCXLd08BRt7h+JlA0dXkfqC8SRvhFxfEcSZGRdAPDV0xGETkS6LL1pAIW/
Ls0q3mOt2xyU6g2CBz+Ur2f/sUAyHqYh3KAcA8zD7NtwqLEeD2aMa5/isAExYyjMHTujbGkg1OfR
1Kb//aEDIHivDGSFXyn1fxmCWq2p/Y4gzK0fWvihhb0Dob3eVEDjQZ4J5NCpwZ5/UNiGul5C8cIv
OogHiwlWfA+4hxyeEGaIT9Ii9IeeV1gcD+wbPSPK2sMVv16cG4JNkiDLN8wJT6WpAVwSeDlXzr27
G7NSHKzQ1Kd4YpA4/g0opYSRs/TxPNnHxtbfxkQoi8y9442+P5fglAJcA80cG+mBao4DlHtq+yvF
7yVJavSPNHN4gMgKqqg8e/SJFG2nFua3qyIrfIKyTYecMd21+GIdcHLvZz4osGX08z67bAVbyizs
XeESPlXiD0clV0JEMJ3R+w3aH9wqLDoCyl/ZseeFXvWE5yZMuZ5zWKLYlUk5xZDOHBVfJzBlF9ur
HOxbiticqysDsidSkWeY5g/CSKC1ynRhcs3tM+JPGVNaRoYjoXueWy9LcBCMia8QoYpbjniscgpF
FaV6C+aPQ3osKApsMO0roHZbQ+H7aKH8GE40wk84zBMEA+KV/dOs2XH8AD7wHJfosKmFKRsKbpM2
tdqPHWNMQ79SIUB0kVRn9K/87/jK+XXaR0iazpaEwIMt9/TmDIZztt7xOrBOrANVSFO3qE4bbNr7
0nFJ4l3KXfpyN708LqiYMzSjosvR440iJX0o4Mn4HMSsIA86mLW5OWZqN6XYM3BBNVt9iCDMO0/W
H+fJ8OPxypzoXqwH5MthmeNix473G+8gBI5c+gqKC33YYfr8b084WjHiYU1EWXXujFxYi4gv5DG9
E5qAPifKT4xOIa6BwpRe71EurK55IFQ47etQN4wU3H68lnaVN5cQAaRlghSgQ4xM4VrkSJhkB8hU
S/uAPBwLSRxOEDZZJfQIEtJycqxiMenjwf8luQp2TNfx+H/K6eGEFQWyNGE2G4dQY62ErNmnxn3M
Ftr+AmzVpyMnKbKTkLFU5Ld799d35HmC8ZZWKOx4qJzS/yHkvn65oQZsomlDI4pTi+mL4a8sYU/V
go8lxPkYz3CMr7pGY4dm+ct72Dr0DULc8FDvDbR8HhnJSwoFJ9RFIoF/h3qjUlTZm63QSBNwP3A1
fTdR7Aoe7eTk2nBIl70qCd/StXCGXbvPwUzb/9BYN6ccYnFBbWOWZDQb/pI+AqM9vrs13v/4fKb/
9uOtkrcsnwC/Iz+WFVg0eyJBEV+DzN1FoYYYOO1vS5deKw4zJlDu1UWWhuEpFmpxSkrFpwzeMTYd
JeV6+7B39U5AT8SSYQuQNYGaWERU+kA+2G0i/m93IRujQHO0rdlV1YB1p+JXDb7UoFVFH7/c9Mt+
IflYSBqnHdOjJYZ/iDBERsk1lRNy6mc5zapR4DECV1wIQRxI6B9xVn5pDG0aSWQniOt6heTj9cfi
1vfNzyamtwetLJdUX9v2PcoP9oMcb0R4i559tXs21aKneuBaLeJDGjQFblp+G4l50U/aH5WzSsrQ
obD0JDLtffAJBeCwPi/tVRZ5RwP5tT21Bqzuo3POPwskVzmVOZrlS7rSgWVdwQRDn0geoas6lDKB
wuO5+iE5CoxTt8w3t8xMqTgXjoZq3EyF3W/L9mEcoxA8dgZmMs580imyi6pC3yx11DthXmoPCuxq
LDg0fQr/IrukNJncd+bjl+TK/o9h6QPCgYYGfP7gpHTTAKnwsE7QGdoIpmquaAOnJqrz9vmpF2Dn
1cPB9x77DcrGJXxEzDZkOiTcA4faLPfZyRKBJob/yl948buLXeYNJEtXf6GC+8z9NakiLLF6YAlh
TSmOoPa2ViGDXxTHK90bf3qP/uF8iRzNmgXeXIJqHIFq5CVpBr2r/hmjB3QNw1q8WZGnGHj0oRbL
t2QmYy5hR9exL4USTQMSAgqEPfQGJkCJcF3tisN6i1AEze4de0a+47E83eRYpXGiK80U9MaQUfzm
w9z7WQLRTgG4xavee+C937WLcV2WadkySYwjDAxykwuXmKgdXGkzUv32jlKs3hbRfzxe+vP5eKIo
AtniK0BcW2mkzTN4ItTqI8DKhXy6Q1nKre5CbxUUKmigwQ5fujM/xz+w86Dkdedxi729creZvujg
50BCX8IC8Xbz/Swo7hYF3FTYwl6jx+7YV34bkPeF262dvFwje9I/8ViOGva3wEvWQWWXxcB5VIlK
6UCU3mZiJrUADjqJKW6ij/tKsCmR6CBXmLqjlKXvLWcbZKQNL4I2m5EP6CRsKJPi7Don9G733IN+
g/89EOkBYq0p9ZCM7AABLF0R0xLEXgNXnz1184OQlafDEop0zhTGMCEqlgI2XKIUE1TF2xoCLr9h
tQrlc4mN0kxmS0tFPfD9Ebl8W9ty4zHdb4/vo8eTz1Wbq0mFfoWG5A+AT7R43BGI7K87/+pE8ll1
9i/ky9sZhwE37TDJs8L95ZWL1N0d0L9iScgG+J8gyLrBdxPGDCdhpyG0ho7p0GNLr7MtIgpwZ5Sk
/ANTsj6QFNm8AbhH5byEwHDKTD5ayNyR7rCCKb5T/G9B1jLQywa6aTS25pRmuzvar3l12q84MpQq
NdHoWphQSGHBYnXqZaF5r7DvVidijYRcwynxrZlBkQZil7P6ET2v2+fogWCuQ66ZtlubfbPK3ZaP
QNVXllM0ythPRLL9Mfrif0h3sd5Iins+lQdP6MsntU43kD1wvamumMsjFBuhmiTb1a4C5F7Ln0OL
AIfg961KoiruiHoUZgHpNxa9iFPeveVUdS7kTsm7AGu/J9zzhKxZ+2ljpYsmcBy4Lv2xrQupJdsC
dkkVDGFpkI4HaOKHNaDOR3+LXeuLKBXgY6LES1UH9DqhhGODbztjWLL+lUtNCL37a4faXZtx9Beh
EOuSXiKzHCuiXH8DTIXQ68nY4c9inP9fz3egtBq1YRaCnGLmFZzNFOZ4dgascbUk4Rx+JUyyCMED
iwfgBAcrXiqZUVCTzJVPz37HkoyxMWX6R302hHbiFxsgCENCq+cSUkNlszQQN3/x9eZDKlI9pYt9
BupVqrCe6LGhuZTL4Xw8re7dtu48E5+2jSPYDwO+V94Cv5CZaC1KcTg4YXaHyYpNw51ZRt43EBEc
1nyudyISAitBF7zFFjaujI4hua98CC+4z3pbrlB5naY90CEqQCDbN7gpthnEYDsT8H0Cv+qHj3Nq
+d4P4ixao1y9fMC6Ml7FAICsGYhM2b4ytvBTNrXBzWdHhLuxXs2VvcCJgxEFvPKOxdueW18gktOn
qwyUs9NGVr/zC5DT1vtXlv4sxrj71fU0SzUTHTEFO7dVdwbi4lNBpQC0aKR13jCT0cEIP8JQ2Jm7
7XwSmoSlJK9fxVLeQKlBSfJ7JkAXyPMS9QHC2Q3yjS9EjccLi8E++8WhAi2iaqJAG1/aYTtssl//
xhZQ6ZWNbeHtZF8bWfuIuikK91Cl/UXBOOxRJVLUb8LmCsnc9ejw/M3hSzUKfaJ3Yjr2F9m98F4v
qQ4pnf5YSD0+sPsskhcQhsZx0PjTzn9kPQpnNlmy5VS92kGcJJl863ihIsUj9t96Pb4BX/4/PKc+
RrZtc65PVzu8h5wrqlNKhZL8uKbnbV1xyTxi2lxurJsPdgM4+y46A6EUmM2OVqzwD5Eply31CCP0
pSQ8aj3KEcMWdyUuY1QZ7+BLROiwXrARN9SE4uPjtG6Qg5WHz5sN0KAdBqQhRUWN+Lbr8nH72L16
STrOJV3dvI1ErZVofoZzEYU7htQEkbBxW86EYgckE11hr5m9vjxfPMMFd315Cr36cwzhFdwf0Zoa
ivOQQQhJ6lD4Og61oWqTp+BvWUYzYZj41NW5CEuBHSSUuAJ/jMlbMLi7u4yTre3BlBH+dD/tVjdZ
9wGHb+hIQtQUhk/tvPbYn1AAQ8RIoQk4vYDZs4Wo+G1XYASWmkIYCn7pS2HQMpj1/Df0bKSnuY7d
YlAqzUYQwLrUOs9hAGGQnI6nJvz0Bweehx+z0LA5oZHXyPzwH5z3nWpNBOaTG00IOkDLQCctM3Zl
msdyxbXUc4CWXYMLYal9FqE9P24LvY6Ujtsn2ZUEjnQAVchzrcRwZS0cX3eBNikg6wuikTNT2jNE
slezbQuFe/UOMAwjykgruNsO7d+xKUpyHOWlmgLJjUgfyBIUa6re88iQRN2VuHGZrzIq8fWCBp8c
hAyN0yNmir9VA5yC58py77etsnW68tufHIDj66QtTivuOUMbAEDNVQlLIrWoxgqHhWKgmAiN1iiA
Oec9o/NqAqC3IITzivtd+rYxvAvLniRkuFnFrnw/UQRhCtc2r/7+eUHJ3ipSuzqDZfKqOLcck51P
kEJ4jQJSGF9ST8DKRhCHoI12mI76mGdUUXLO+sZ2QatvNoPLo1+S563QZDF7i99cMbPtIIR+ltwG
TcT8qaNWx+hIcSeuD5Pq87tg2C94vOCN1iNkxJ430yUg0tz8vUY2fN88yxTg4Gckqu+qU2LU15fJ
MlmR5uWuQ0Uqd8xSn2uyYef3yaEHiV4GQNKVHyNE08rhEUej2CFKZgppLi5gNgK2FDnrqBJFViOd
0l75RckFMYfv2cyarRBS2xVzk6+613ux9c7NepSMaFZ7bFMYwRqmdX2xJgbq0VgDd7E5YCmY4iy0
saHcFpz/hH8igmLMXLIhXfmZtUHMDr4PEDK7CceT6pTXee6sToZb1fw248StHd4j+g8GtrbcPCxV
nYusow4sAGUv4+KR6MaarKM+KLnbapHZ6ZeLgBXzu6cfMHSPmwNvNuEIV14bR8cMvkN3Hqc4w7Z7
82CRkfyB2BSjwS6Gyj3xZIohfSTZh2uwtBJBshD1GjgRXaDFsq//MPji2UQximcr/KGJHryD/wVG
xsaEShxTHixwUdkuloVBxKIaPGNN8ZQRU7DeCz16xYK9h1xzLrC4Gm9AO1AYqAaz7Cr3TSGCZEg6
0u/5YLAsTGHjxDy+vc5ZkVVQONzGe4awWPd00uz00ZgKT+E6CYNXMarPKLs3jg5K38jk7+Sfb5O1
GB0V0mFhknhxW3prPOnpPUY4FKRHFtOH6XQMYvGhzy/V2KgbD1N37lhnt0RC1PbI9teVsLGcaSvQ
7o1wtr10EyghoUqJVlfNKuvJTj/SsaW988RWG4/0nlZzoRdq1tmRQlx7Ib44ZZInjwfN0pHWqAcP
bk6EjzswKnjDNm+PMAKXV54c5hZDe0XKYjaDLfuscBCVg9nTBT0ogEkNCXCS0xKjaSSoJBh7mv6P
RpRIfhJaG3MSJUs6OVHHz1Ws6wBXKWnvdvfK1t1QhIG1VAhrRdrkk5E24qUYNrcF9lwEWaKi+S3x
w/ECZ6F1RQ5TNH/W7ZyT6rcps1aL7g5x80A3ucIVgzvfY7XuujMHq7FTsuEGyAiNtWO0IeSJSth8
0H1iYfJoLGO0Qr9PvgrWzNZ03y1/Y0IePGETx53d8+Ji14/tkpNvZztHfP27alqNZz5WuhSc1PD4
FoISwiKEKFL3ZIL6beD/z/HO9V5HLTV2MlcT1POwa2Xyx6sqyP55ZF6+oGvrL6FEde1FeuXJ4+RZ
GhzhJjygEvRzytR5NSODJcKo/2s5kxVrH+RZJvoPA1jzMC6Rq8pGvmopZ8A3OxOLaOGHz76xnM42
VKcthm2bPyhzsoC+d81qZ0ONZ3mNqNdxqO9gerIB9ImusMs4/0QseOtWdk7XrXnJlQH3YyiZRz9R
4+P8lhbeNFPazBCR2XiWFOltK2bQgzG4kG1qnEGZl4g0f6ZtWaLlU0/4qFvXSaS+N/uUHY8BdgTm
H+F4LiZQpnY1PNIgpg7rsNJUhPPh2kKzD2BwT5VGizzh7rNFgFw+8R1VYVSj5iytZa+GuYoTIVvO
Kc5OBL6kcU4MO0F1aasx9cLf/vXsrglm8gd97DaGT3s9qFtMv+zC26TY9YADG1uHRhZ8S18cQDqo
lqtY6mWFTiFQsYlylsyq4n+eHQrsfbg1bX7pOjWWD+YHJivHbfCWbhGwcRDThBj6P0aGQfHPNfYJ
uWbx82ogLye3F8l0tMfTPsoM1dMBLFZqzjSRn7N3uRmtrzySf6DHLwsK+89Ebe1dTMX5g/zXVI4m
MfGIBaScv6dAC/W2etMGY1YAIvB99SL+Oj2u2FPmX+Zrf+1PntBmZOSWYRQFsLOTHJ32Nzz7/FuN
CtcizwFE8CT6rUHwsWuuBqphINzq3PKRfEDmxfH7KuNkRHXEe2eb8tSCBReEkxozslt1SXhsTP2h
12S+e5MSycYKmGfL5b3ouE7JMYIs3BcfnNMJWhVk7uh1WO4RxLZn+x2oiZmqc8TBTrlLvphiq/gr
n1NrGsFAAZ2Ghe5yKWc4Uwq88bWaztCeCcqwwu0xabU8OJ+EzojvtxLQlGMaFEjtQNn+7Brody8C
44gtrXuWbw66HAAqHH4+of7RvEabSPkVw8PQ2n00JzwVjLimuPRHTzCb2vXrWl26kGC8rKHyyvAR
0vyPLHCKHJ/tBYs1LS1sFJbKJt+AwlYfZDJ0VHt/ykyrS1LlIS67dQZcHfVkvgl69YrhXw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
