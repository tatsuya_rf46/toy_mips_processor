-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_0_4 -prefix
--               design_3_c_shift_ram_0_4_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
EN+4gOYyLoDc/r1UcUquTnFtDtzX3rmbkXeKLtmJBk+HjFq+GfkMPkRKJxUsd/vkpNpGfkqhmT6U
UjB8rPA1M7OBTnwm/VTq56vD2MCXHfywhmKQirISJ3uOPuAgATz5/o9tGETzkGxJcSl1dEF468U0
icU82hlZpD5LPah/aMnKrVngNfLAWwrIWtr0aamavM2kzFPWn6DhAFh3fvBRVmTVuDbQA7++lxCQ
cty5Go05TYDxxDnWEJQBNGsLdBlYVCHlvExjnNH1sQxt2V0Y7L0gj6PtdhQ0NyfBR0aT0055SnU4
afWs9RUP8VbXnU5zmwL+rFN8bZc5RG05I3an/PqaMhwnl2KdQmy+11X6Nwf3SLS7+LwavWEbeGRj
RomXBmGULtRs8Lmb6k13yN0LZO3Ls4HpERCzfDWhfbbehNTVUdw4RlOVvPuNEQpdKNfgBwN2t1gL
IAERDVU6uBe248LvwrvqvoALF/EyhIJugya2/RJqOG3MftMGdZ4GrxQLoAG13QdbAH0ENWwW3KWy
Uc5TcGu2RIFYzO1IoZJggMHO2FCTxMcMKuQEmjFGcXIgipxyHwzkdHa5UMJO2p9dSU1NrBWYFNrN
8W9L3x3heyw5QnnmJG6QbY2Vf4K6dpYTOvHAjcNUEOc/yHaVXnZwUHSzCzue3CgRafsI+1W2XD6v
TB371D/4/0p6+qT3LIOgSObX8+LqbLbVGrLc785uWyXZbXSAYJ2xJGUDCdRGZxtimlQuS8IFViBH
pJ+L2oPo50dQQn4+Cl303b2eA/OAF6BXA1FFhCCWcUiOMM0UTooNoE1+41MvGDQXxhXYxAnum5Go
GFOZU6slgjMX9kFlxmYwHMY9tQXSvieTMdQgXFim26PSmscgorl19fzEryKV9m9KgYuncZh+yAQo
nkUtnfy0qZd4NemKyAZPBmHsI7jEquLE0xuuuxcxGI5OUVSzLfItyrj2TuTXX7n1IdR3PLYsenLx
CPxMWQShKrRg0rM+AkKIfU4YW+B7de/zH55B3cDjahM/pExkLNQmCqrjdZBzx38txlzzRfSXsx5u
EXbWRYDIafYvxVJksjOGUAXvVMZ5gQZsIA5pKgTT3/iPVM8f5BuehYIxp0mdpqJt7g/k+9ZcU8Si
izNydCWWOjYrGUApr18fVWTKl+BZf07z5JuTWzDSfihjopR5+ZAJ1q1eE7w5TszsUs3WsaSUa/cg
dZ3uFaUdmgslizEQy2NHrC31WxmycNg0+6yXnRVSVlNdqb26iJ9PELZhgB0ilw/X24fLNoR83PZC
lE7oYoapQSAnI++7es7+dDuqMgBnYnq/Ds2cKeszoe0TBf3MaL7VO+LgQn4KmJatR0asXmZrs0S9
uRIGQSFTpzAzi5PPoqyb/qFjFPMTA0uImF30arNSchu6qJ0TvofpkVGROh3DHPaVKp8FkKdO64lK
jMsLaWn8VSPKQ6vcH1C+MIAqSC28O5W9YeqUaaF7aKXQZoLYrNeMJa6C/K4+NHg7fReW5ytTWKbb
60jutWgcT5sG7beNah5byVs2MyN0fpo+sZNIwBDu3zi0+BC1a7zUmrqq08RyDQufuSYfOPRdDBP+
Js7rcUxCIr1Vgtha44j9BAAf5iJIker69rYflVDoygbfYUJdYpiznq3k1qrFqz607s1wC/0iKYR+
T2rlJKQ4ugighHFrPaxH9et5+NRMt/xUlFOruEKCkhInqdea8NZajVH3NyTMrv9dM0E8az4vlcTf
WReh+xzyufO5Uws9yXiGAh9wAk34hEVv2u7nYJK90M80dPirKAb9VUGUP+lNww1rmO6WNMIlAONw
opAT88CT7xrUkFU1fvDFp8Ii7pOTUQRXYn7A19Wtc0jrtwDb5TyAzRoqel9BAFx71tggKGaXS+GR
qcVSoDdrMPVZHim8iNcE+y0EpmCwU2jxk94KQQsLZhD+iwrC72mfSfFddFwGxEhY15g0RnOCBu/l
ctDN3wURCkh4iZu3oNChGRVgU6/iTwT0ZdsDuUZK8qfX98aCprKcgVwjh6Mw5UyRG8nyWZTSdOXq
tVPE2iU0d3zx14uO6rL31aHJWxDcxHi+VRNSq6ooHgY4iz1ZSwM4+g3u7lXmCUKE2uD9pdsswTl9
QZU8v5lyZf1WZZpqtjcy8Odjo6raaRJaO0E2vpwosSt8PbC1n9VUDnfawqkSWN9rbb1xnOLxLasA
0ZH1XtAatd/BXMpMAE+EI7mJOPzt45t50DZ9+2EIu/iKQu+hbqXDc0yffA6Mp36qAfr6Pq1cPGWq
4Ef4ANpd0k3Ify012NhKHeE2vOxiTtqz4Jtn/6zKOVpsZzn+xdSc5P13/dBd4dTKWQzKroqF5ET7
N7XjZR4rj7ip/moU4rM5RGnsE8XLO4YBaBjUzgR0kqNr5caCvR651fHPSyAlh6dbkXA6nXHMhWhY
RKNgh2X5DJr/e92Y3NY1kpXnGQsSilC1Ajq0AEjCl6TWSczhvBxcH8cYzPl1TaHwPPSAj3uMeWxM
eRhgE+1rh1P+1BRChAeXNqHT+HN7bGRVVCfMSwaLmFpjDTybT/dCCXg9/jjXiBv2xGEWDMtDbylb
0S+qm7kmLCFXkRyFTeaKkR+FuwS3c0DVqOR+WKgNgqQI/NTbguFjga7kYy8BsAU8TKKl1yB5Gfes
RwJsiFhDUscqUuS4Ug7yk7ccv4cay3gHSS9wynXlR717fJAzuynjLFLmY739vYeukukBLvV3abtM
WonLdRfAATRimmDeRm9C+8EWQ2+543fsmNgcZq7hgMaoA6+u5JKDGI3IzLPcZBCKNslNDcz+TsDo
3mDGisTQzaMXMlW9I5wPh2SsbAOs5/tAIdn0WbR6+WTAyY2LSRNudhL55JBRe5ex+QcME1ELXUN7
UcxGYNbbqEG/cATl2NPUBB2Sjd1Wuy+w3yDRgR2EwV00Y9+U46CcXxKsdrzVyDu2PJXvwXZdLW6+
EHcJazQIw6jMS92hLc6XXj1vrDLGPyeWRH5WXmJyb8HgXw7TAypL1Eum/kgGdzlVmpB4dk/Mjh6J
tyd6OgFak2eKhqP6RRcpLiW/Gec/q78jGKN1agy/A4ELIf/YFEkSjy5qzA9Ws1vkTTK6uzS1kT+M
ZBQdT0/8Ukr8IiDFqqKTK2Fg5cnYWEZyZmPg9CS4PuPllT9UZjpCqsSov2KV3fClTKn1IhfkF84Z
LOLJLSfT/glSBR311y9PplTDRInIDQ1a4KIlunrHU0yXy3ZXzI5HimVmqSIhH8URWiUDvKA+h/wD
K9xVVm6OvRhFjkBnS3YR4UUq3YpZkRICI99TrVsUCqczoMCGqnBApjzjnzKoZsfYLKw2p1JauC5N
OWLvtXfr3BYri8noWcA2UvsCDECPgeapq39AyuSgaUi2YyjxLxq8vMgRRGVXscDCZrGAa3FPqbt7
CeEYMEm8lzx/JXTPlWcX/qsUc25DWltqfT9UTAUZCkhK+S8MEBUaG/7rJ9Ic1RhDWX0xrX4nEsDQ
bGxTM2s4CAzoV3ayvR2vWqU96vIYQpY56jbgSTWrP8V24eUHrXcBizr7k1Q/V9Ora0F0KAMXaYTk
JAT/msUK8BrrEgaRJr84rPyWvAramM0NRR0bH8lt9LVCV/gmb3vEPl29HVg6NnqpGNFUspigHnFN
tjA/iyy1GEFgQrfnZR8dVGZJVIWzEFM2VFXyxZ4DazS2Se5ol2ZGg5YqoMeD9I0eJ7ceJviiN+1T
g/RTF4DnlUq1kE6IXsK1YKdHPya0U5dwVFmQuO5Dzs8eH3KrW0ZRpQCrNOmhRca8+9OPwTmtsm8Q
R7mmREB95aHKeL8CPBgEVPXss8rM37BVVa+HEqQse+D0lxwy6raFqTYjV1X269D/GZ0UxuJrCsVT
mI9t8aJxPi4RkdhY34rb6FVYXAVEOClFptvpU3WLlRz5TXiait+T70MJ4l1a07vmdQMiRQkH2UCu
sl4oaytn0ilODmsIIZtKbny2D6bbWw1rpo98cJXRpYnrS8+mRukMN/sLLuxNu7REsJwB9759E/nd
kgCjGct9+q0gXJeh66DILD5LfmNWlzsegS0RFmy2LVser10WPPCcW9cDAA+Hy2xbhBi0nIjI0EZn
NXFDVjfUDwOr5bagIrnuYwnaDv46WhkFPvC9/s7UWKPyTSrHJWE7cL6Gtz+VSFWHUYhfE+HZE/rn
sQQyIjThqChIunMxZuBPxokk8mr7nkNpFgiD6CCVh9hAQz9Hfdi6hIbAhTRklky20hCTlwwASggJ
EwOctx0qP2uihR2srAbogbhWT2mjZ+gMLz2I2Q0euL7jW0CVVLKteKqJA20ohb0kfwcWD9KYwXjj
BRuoT5IONchQxFbZHhERFF48PS8ombC7XfhDL+vqZxsDtsS6TpnIQsuKTGGTPbMLY4K2aqEqAhuA
03loxIFvytfYkXUhUM8uDfHsqVMdCR+9weybrMrFWZmcO4fVtKBPcGCC1/XKGEatVUGjCf9rBehS
tV/g4ruh4xKtSiwHsj6M+0DMrPjlk6XZIku6KoBWOhdSCaQy6ut4+rZW/2hYbejtdZcwfPDT2Omh
NtfIMtx1VHKMciQRJhvAvDJPfpt6tRH/9sj+vvIngSxQPSSJZkFeWIjdGjjydWuKOnhphWAwDkno
0jl0eR0PjUy4X0Z0aj7hHC9PF6z7TUqhawAXp0gIoyx6CsLl+1uhnBwCu3WmY1zIqZEX8ws45jjf
GNpYYBVPmy4GAVRFai6OOT0oKTsGagCYVp1oSYY/jEtYPJFmjvQkv3Fxx6CFFZgOS6zmKkDmjZyE
1DvKT2VBS5gTQx0MMI9a+Bknb856yTv+nnc2GMkzrsTN6SFJMsGKe0CDhgGqCVzJ9WJpldrGFFBd
GiUeRE1HWioFUqT66jNH1qBLnIc3Ys0Zf1a7olCTbmjwo2GUkfSEi8FVMxORjvYvGT/C7meC/rQQ
1EjKrYkVQtp2D5uZVIthS18450bwOwUfC06+mAjYtZNpLwBJ8uU53KuAcPKB21DyutZ5nllCTCxy
z/K0q++EB0mD7GX0tvfeKFAkrp9AlbXy66OlU4J+jIkG0YVEDtubsJUHvgvy1fev+iaYsPcsD7VY
dlI5ET64nNKFqme6WfeVdNJ+LlGwGV+PeJa/+BMDqV1lY/AHlDskQjf81KTdhW3zUcEs0LuldNAE
kRNu7AeyXXwDg5nAiHEEYRW4Jlt2W3Vbal8X7SQW+kb9tHmqcph3Aif6aopsTc6Nyg6lltCAlZYC
Y9PamAUF0CBLK1RJj8ypvl7qEQ6DaQ92OXOPgVNeeoHavIqRybIcfGFOS3tjJzIfchB5O1mp6ZIR
uuhZ2lHLJsv8q5DrfJrbr1fFw1ZTuwR6i0CicTNUTsBBT/riPLwDJeQojDBoeDp2Kodrt24Pgmni
jRoS/EpQ7ob97Ovz3sUALxQngkTadVR6c6Z/iG+6pg/Etc/tU6fzYxKfvgzkKB6j/G2fQEe5oohM
Eom1zG3iBzdTKv9HWV4D5EQtIk5LhsOni7xUAnHCUz+PsYhvHGhpexCCeN8jSqSN3YeEX5rnnD4V
MYkXmXBY5l+nqkJTwhhTRIguZ8uavVDMxCH32y3WolWvYd9XBzZay2X8MvZaNBFw5ZKazdDJC+3E
chmN98v3CKGAE6jrKsUkQka4KW+Btj5k9Mr6gANEh/kWHgiT/Ta76VJnAaAuWxFkEQUXpriFm6HB
xarfdXvIF+m1fDzbzXY5aC0CjyiIfGIIw5ewub5ER+wEcaCnVyKVB/Y1v48iNN1JU5H7TG8yjfYb
Jf18Hu2kUuTpCl7sx1tdGKtcsZvPFN2Pmx5N3vNRio1Gp+6B549on3FcwTOqXIcuixlVxyvAQ9TH
77XKpvRLbK12VSSJoYgRoQ6sZf4WjXy7rieA5OYhEPfZI4cPfeWx4fjcjlWs7Ck7nQVxVQGHKv4T
pfT3Z//WUC5PBFr8ISzYSv2mQYpZI2YrVGS2lnPmNJpZQzAcTVPpX1R53AxKm56JoFfeUjdaZG7n
BBJU3ZC7Uu+Bgyjr6gDxx1JGHCXEIhZek1vj+YU4aP8CePcHusvVR07YVXDpFBWFuoPPfCX3g+xs
0+BEdsCCfOs7k//2n9mezW6fSWEykQO3UisDlA+Q5FnsXENEHOIkHy/Fo8XE49cjaO19oqz4Lcm9
IgkkXHwIIseBnQ9AYu6uyOz+7KBqUGnKV15ojVleZO0phlwx+9JG+uNBhZ3uqLe2jv29zr6bBqOr
DCZgD3mQb2c6v478im5gzbzuhYhaPe2jtvLJgEdd5OU1VUnsQNWNrinNziTW3xT7CTstHSY03GS3
tDT0MV0LKUJs9Aydgy2HWjMZE6KvE6qazuAgVwaly7VHSjZAO2CKmdpbcbSCcd+e2gVqCFwgRsGC
6WgvlcisN9kUuxuTj/XlkubKFSTx7dTOTCxclx2b92U5J4BXYgu56goUCPTKlsq9a9GzRckZGR2R
pEpKhCNqaTjErBGkab37jPo7acQEfodLvmyqCFFFMWRPRJ3zJBnNy/hCP2JEAeo4C89CrpuWP/5f
Y7AnDXdzsaDnHRBL6+b+MKU2m/X+boF6zJGs5P8b050cNqS3NlEApuGnyCA8J+UVX4Z1saeaH3bn
2tQ+mG/wzGvzu4HhvlYC1ZVN4r1Vd4xeo4yVK//EII8c4Gvki1PDHzad0hqZHDw4xLzkzlWfVfSj
ExrIXWcR9xcNx2M7oUXnXwTAde0VAEsp2NYm8X/dSWY3Fw/X2FWXQxBW2dsChh4mMIXoShBftg7H
OypFB0AAfXUNhBA/d5PB5HjEMemoQ/HTyeYoPaze3Qn9LQF4UeMpk6tkmu3qchy7R3BzLz3b5Qnu
CiXuWwujbSSDF9CDndOXqZtBTb81VPc1QrbYeoKvpCPC22kvV6eZkh9ZIa+ds3gdjVed/BDg6WXV
ahYEjKTHly6A5fJeAXXvxKuQxtyL+YK3PEV6AipuaFSGgaIotX2rj5cZ4BZ8LCkJdvX5BUToLsE2
MbNj5cq7Wf3U7AmQatr3CSV/32kgOdyVHAskPbnaH3WMK4xIU4Dur7qpbwYV67sxI6QCWHIzCm08
UzMVhOKjUp7p5geNJsG8foWmFV1GKlgisKJ0mrttMQgCG4oRsvWxTPbLMjjhet3sJq6Bh13JL34t
PFJRjzE7UTuuCmoP3NZDCgwd//IkuIAtTCKteYjPUb4VFegX2eIYIE197T3JWX6dkaPjyfVOJhUO
CFBE9uTkIwvwu1KFxCzNmckYKATBjIgI71tzmaQ3OoJcXNVjC5D7yIzJ4vRnRQtGPUIlPYH5XTaj
QHwMJWbr6suFtlbMKpUO75cVH2jZqdsuJX1axSWNexpzXd/Nb/jpe+KaaMxMCibJXFm6nDuYvB+n
MlOl3L3dPN9rUPddbpHlWXsA5Dh+r3KmazbZ9rr9wnHkcGPxlYK9Dp//YyVe0hX9d6UrQqiqF45a
JwJzKWMdiFNm9y9YafO5Ag3N2ZhJAblQkYM6AwA8ebpR3FkGYPvPqus3SqbTpnoKOiu7+0abTnHv
9sUTab1oKNJ9bb8srHaw4nLlaI+K/JyN5A9Z8qhvJuYq9ZiHGdKnb3f6ERWnAeQgorvWmFFdU9pM
COTzAqEZan2+K36QD4ndN8fu0TudoBPygOhEQQtFeTovYnssDVyxn63rFm6yYcBsxAN1YnvI8JKE
+8lU3VM549+3eS0KD1S+0UV29qlyMaJ90QST0o2mEjX8gWo/2Y8goMnWFVDd7yNUFD7AyMnvFUTi
nim4IzxN0zMh4BS4d0igCWQK3Adx4vfBf8VlifWRjaf0xDhscqbXu2FKQioaib0HZXisrRb6bxFw
eN6sh/fxvZF1PkFVr6YonsxyyLqGIV+rs0oezSOPUXPp7nRb/BgIEl501KnMMTSbqfBM9zgQUFgI
c+svzI5iN00/XquQx/0jj3Rmntn6iPs+YNEt3zgarN756K9wXCpxRZRaZvFJRNNFFLvqn5wZtdxk
TexXtjIbm9xy4R30ARypOvuxRBjtVph2bJpkCHWt9rwiT9buDtQHHqN+mSCXqh2nciPNgWw2K1/z
j3xo077JzsJDpiryqJH8cMSpb4PFk99UMoHS4F+r95UjDHo5bSwBCE+R/33j1s+1pr9vjibb0Oxm
Fo+f71qCMRtm/8/96i2l/npR9sCCUKNwe/e4wqsgN/2Fn79dI9SE6UCY1uWst4b6C0rt6Oc4aj2g
djCjzGqahqoF7CWArJPJ4pYCJo9dn5yUs/cnWwmBovuUc1fLKxg8RaWob85NnBuoI8xBSvpghmqm
rl9BjU/c+SYuoBSS6jNJD3DOoFZdLw6dFVIK/mofIQc3s3+hGXn4pzAznpfOI0si7znbKj16UwGG
Op4KYQUV/ri1Z5xKWj1RgI4G7BKfkj2dt6ZanuW5mcuxX01fOkZrjEtt0ReJj9tNsuJL4HNvY6R7
6NZ0Ov85j+jdP4ws2nbUupydc1pUr9ZNNfZDd0Hhselk01eBMLqBXmxPkx0iTR38a9dnY4yqPqyG
aLwU1t5pDuwlFRDu3gzUucUdH1ayjoWRHq1g8A3pxlYuNm1n6vW0paOm/qzmfxREtIFn+ck/W/GK
FU8E7PRv+oZcFIq1ddBQNQxTEBBiqS2pJ2AdHwN9n+tcpU13W+yvnRDf6jF9EY+NBcSdMDFdAupe
fotXAoVsPgSeKOeZShUApP6L1skfKse2wuEBtHZVsh7WY/7ZBEtmyGeDhJ2yzTp8ZmQLr8y9VQaq
SX+YPVtWjW5MIsfNMJ1ChtpXz0a5RbEERpyNrACFQUvinY7AbXJAn84BoRIn2dc2RxEtAltMkMvF
PogK/dzMpKn8ICJBr55iBoj1LnAc4m0uD6tA9r9tP8wJvWgq2+jSZyxqxR5ysjBqhB/cZIIJqCPK
mm7MWA6FnuZoD3u6Jy0uRkn8OJZhAPSFwtOIclEDPrO7mxyvlDl34Jfdf+VhTwrH+MhpwZ41NU3z
miSja5BEo7z8Wxj6R8XZmbnSj/SbXKRN/wOVrSrfFRS1bDcQy7Mnxa03RKW97N/HYcCox9fxQ6Vd
az/KPBMzlzWUzb/Awaig44X8l6Wd9J2OWA4erD9s0rGwkhrkT+90hTH0A7AnrXJF6pEcgTlMei5p
oJIBi+FmB/mTW00i6+ee2GzegAgTZjLoOaz9Dh0e/4M+uQ8zX57p3/CXmyouCqnnjeommib1mK8F
3WQ4rg+STCmW2s31I2hBWOt3ZTZYlkCRpprMsjfa2M3LTqaodpVGkDRYNXzXE0+BWhcEsDmzhXX1
8Hxa8jiaGHl9JcHKhQkTJ6g/K14wgdFN3B+GETk38ugV/R5kkMa+n/akJAALS0A9VVFVke+zgUSB
kJ8J/g/6sa7gB1xsR03FqOzxQmoqUiv8uljygzaE55ZekBvvNNfY7cp1P4IxyBlztTrugmnnAicP
+UGB06lC4AnZHLqNmOYrHDqS21z/NhoGT5rb/P5VkMouJEq//GiN2lVdLCIMAkkBc9hXLD4MSbLT
0HIZvA4ZADpO3P+txuNGVD+DdXI8lhkThbPkmRmzjkQBpUyyrGsMBtM+SujzwS32e/SuIZmtoyG4
pkjWTC7WLq3AToE743EvuxMJ6F9d4f3x+xlzXt4ck+GDAF2k3BIK00CAa2NkM74v2ZOnA54zF4uX
WJ3X28kTgGCBxx/MRcJjFm/rhgG0lPJDHQ285uE8ua847NFTog9JxmJoxudpnfjedIKDJ+JImhBQ
Y7zM058+s0pc3ODXysDbZRqjJqFILm6u8kXQUGKiZj3DgHHzsxY/86pveqtf1ULudAlhb1r8IC+5
RiHsqTj7K6V7wBxZTBl4iTW7bqoYXHKX2A9NgQgsxFUgtLMh6cgoXHZfOtWonL3fMiQIbGsuYTH7
aO9nPZ7av1ZcDdY4BXmFQ3TwoLFT2uWTw2k/nzjwwultZbJpdv0j7SLpHfIO5fwdhm0qBMLIX8c+
s1HYOaFt6nJi1tK60hxECNpEv2fSl/1g9ilS+ihbL7uWWZLjLKP6vWDnf33l7JPt51696blUhCQI
X/r9zkKUEQxHraka/+eZp6iPMavxdEN+3x9vTudG8zywKjlWrY2dipgqwD8ncOwR9sGcEYd9eGfM
QuI/d+4uFHe1fm6Nk8kVYNyJBqAQf+W8hz/qqubzQZhHhAGK4lOl2b+3cBnVpEZQw88YVz6ZEXXy
0kY182UnEIcntxbBI4NptPg/zLEu644pQpBvi3XanI5ouliKEDBG63ki1/wPj8ipH5S0g2QiEyss
4fwlG9GZIHR1OlAc51bdrUPcI59ePhcHknPl4pDsHgHIDwGmpkhWYbeUyB3UeMwYH52/AhC3vBR6
rcERRcnFGQc5TNR2UOb2A8GvdyyQr/s7Ggwc0Avz2G4akxGuI2h9fSlePs2VhTt39v9VK+/MD1wX
0WbIG9HOUnUmbs1wEWHGteodQERzfppoEWHk7gBBf+/ABNQp5tJHTWJMadANNAgS0IQVLjQy1E1O
4YHCefpXVcJqpFuGd0+3QNFs+excEZ69i3dh3ZyzSJRi7OKkZmyq6p3IojYX72DFbDvTwHibjCpq
724GMjQy/6Uzy7ntjCQnbxptoYFXSAF8djUkoi7rApG9jCXSMGyTZQ5UcMibukYt1385ZUGdc8fy
k7I7V5G5pNw1K0jJCaEYHawKhMD9XUrexBCiHYjtbDFxRRnrMIw5eCmCpdufH5dugZYg1S89vBZt
Osz3PNPODFc+ck0mua59OruAjWpKXj6GO7iNXp3NFl6WH3XN97ALOjFYtai93ffhmGdELBE4it0c
GnLtEDgyXBTdP2SRYS5VA131eXcD3F7WVNKH/0oXV47FNx0OEJ3+imhhl0ISh+j8zkPK2lednVbo
+vBDMK9/vG5dse0RRu9xHMKSDnid/uvgtdlTZGGYQVCZlFrxmnS6scz8CLZ3mYowy0lPTkHPWEAa
kly7GLXu/D9ApoStMMFABx/IVR6JHx8X8poKzJ+YEhD0A1ONEw1A4sSMOLYkeiT04yElEA3bnwPe
9WOSP5t3/Ps2reObXgtnJyCcZHgxKEh4hL3RGEzxJlE3eTS2Shp9/PT0jY4d5RYhhxp11likHML8
pb6YHz9vbaKPfPVEwxQtO0bsFyB+h+0yP4YQQUP7c9hq4caho9ZYPSpRnyvkGd+nhNIc7txZ+vif
qyOMV1VMyONWVrSwJtGOuD6pGrxUopiR/nYRcGSYKUPuzOR/MYu2/v86UxDg0Ater7oGkBu3YcfX
cM9Ql+rJ8ySoQpHfc+AjO6qtWNAxaYX5HI2B79/1lF4EFDrRjrUzzbbkGqwQd/LnpVlLyw05OnfC
yvVMk5KLltu5IHlWmiiP4THa+nzozK5FbGnl/fdi0EIs+kJS8+UVzakMde+XQPfRKDIQjlvhfkz+
dLQYJiJxVXOoceTrldn1wcBoOAqe9V6+IevMBRyOyZEQ3Dk3ysdhZHdXAu2G4Cm/VCFIyQjOv/gd
MpiSLelDsZiuOFLNOAnl50K9QdTd6/THWO18Fzz0AeMR0zuxObWXFMeStnjCmbzrFp5P4Iy2APDY
+TEkuNPvnJxvvbj91xX/iReTIjOHeNXUINJM18/HyJIglVf1YjFmtWcNikBgpHDMIItH5WPOZtE4
C+5VXGWxn0oWmilUpZoR4OSbgt47b8+oQn77daxKCw8zsdNN6nuErEXTASxDkXBClAaj18kL9VpT
S3D0PqBY0akJ7CTa1MOHo/d3HJl7IK8skQkdYs+p0opFO7ptwWqirtw8tH3JTPnaByqkUiHaYO3z
mZlKhlwg3NFGZsuxG2WcqpInfnB17cTCp3rY3ORVdubo0QwpmK0AHPC2azq326ef/4cPYtUBCB52
viDKm/ba+FSUKbkAg4nibFM8z2ZBY35O1VAH3nUWaqCG8P9b3enpf/lovs+Ismc8P4YmAhyoP3uX
rYMga2/6lj01AyqGvVJrteKXARDL8WUsDrwOsMQZlCbzy+FBFuu9Lef2xP0G1XUwnwVZupo+0aIn
osiKJErD3zQ9y71Ta/kv/d3zncLiW+cP7ZoYcvj8q5/LziXeSPhzlLqMLg/C0Hpw0QHfwSybMZi1
dhiJnyLRPE2fbqEeBhzvPF2zJsYq5gJv3T/s703Q1QS1bCgkFolfQFMNmE3/WvRBUVOKFBpa4kyd
c07kNnzgcyaVbb/HkIP26z1+cwXzJiX55lERCWs/3y5AiTLZeUCcrNteYHrz+F18NipEVGryrSfv
OOKeygD/+zemtMzQmabOHwDG0H/SNoK6UXFPRQLDNX5Tyyi568SD8rKQxUBFZKaD/u/Fd0r4X/x6
jjH1tUgJFlS7FiD9c1WnfeYaYZnvUagvd0HZLwSr0hActHtd4wHLOTNNLa9irq1KEBzgqK91l40q
GfZPLAml86yPiIC0AuLSA/JfV1PdMgbczGvrEsO45mohYE1D/y4mo5Wk6bfAP+zE/hmeeOUHJC9+
Ft7CK+XK67CRrlhq7+dARYQPY6uvdRNbMoJ+RLNptTT+9Gbx74Pg+s/ItZRqHyTQUPxkiQcL3895
LdFpCEh8cWumozOK1vgJoynlZWDbaObhETQKXhQkGdV8Y5lT6sQQp4TSFi1HPqMPj7A+Isal6ChG
gd0TvUjHhXPdvzH190AYZSz5gzSIxvkml7ixU6s89GZE3AQfppXef9dsWcF1IlPrrMOLXwVJJ7v8
R/ZX8UoLPPlQx6ECuXLvOG+UQk9/8hsTPUbYHLF14YGQuWPDKeDAa68FEMscT9g0WBuq7JDWA5oB
V1mG2mjvqzIWZwfneB+WbgO4iGe6/sGkl7dwp61UZOH3j0vdAUl1PBbyjUILBHLERkG5H0epqX+i
FOK2PXITnrtjIjuchd3lYCYgRtMrQyB/7NHRIABwNzGdzne2H14RN65OPW5GbYGoW3jL9HeCIuEZ
0VUeWi/jI7uuujmqFzKE8U3JmK86a4UVQegmcmrCF72lMaogimtFn4NPrdCJrSXmD8CWVY9lzcCz
t7J48zle3CnG9p02SDkzHcfXarH12tchbzU/k8F1JvQWLMLqpB2L1a4SI03d2oTAB+I8bJpEu3Hm
4Y7iN5frd1YONUbBZRu4P0iX4fLuBxY0zVLfQ13A78hnvz2BUAADawaKJj2GgyeySO0NEqYR9d6k
lPpOz5ackPFOANKjOcbvQ6jsI7ivaQInP18Eofb6Gqfe1Y63ZPNHaZQVJTrP6SrPpavagwh5Grym
dxqxfd/lKv8Iwh8dIpiSvxabnJZIfnpaPuVEisLRyoBFJxMN//TAd12sFja3mjUfygW7UHn6kDfU
Wz4miSqM8/zTGkVpiv9hcHlBWrEbc2CZc/m7R7uaKvc3xRMxIVDRMNFgkrnxdcySaUt0ES6BOpWf
pDMUoRsdqMMapk0lz5IgArEiyIKpobSt973WBCUlKDCTGU32yyWfL0u7hjpEJsp2m7X0vzVsqVZs
Pfe2QPIg1FszMWaopFLPemSkecAdvZn+vSDK5pvx5vkgcHZEmsEWmrcRcOy7wPkbTYy3tl0OSofX
K8VIxFi0VeE2EEd6TYqu2z+GADOhG9ZTiBrrycaSwU490YTNjOT+Z0+rFWaXNotYej34SZL+OtO3
4Jo70KLqDI8udXsqPt9jrOSGOuujHcoeBrAfYlRazOXYL9Edx0MZG3QaQROWg4h5Ke3tPFReiaCY
Ej0DQGeu36JlJONlTXUPoUPXKlEYUVzq5ClyE2UExFjTgnyBxLwWigKdAf4nWQmJIEMFsk38Q/QY
jC2F3jo8mTAxdtefdTXjyJdkAGmCgluwAOCVzUaZgba59mQi6Cp2/+g5TslpUtl6RJXstDSLrG8v
/Rt0X6zzWvErJl+muHqB9TF2WL59jgaySD540K+ymXAguev3EzdLRCz+X71etfe2uFFBEewhhStf
7YNH2pQLmnPMHkv5H691NxQGFxI0NakNHAj5ilbJ6a/ZRGaCT5hNXL3g+8j6tr5ko7Kn0t5/IxCX
YtSmeoaxl9fsaWv58yE4s+DFSdfTBQCEona4wGamQ8ldL1SMq/9iNil+rR4M/QKxNGBdw03Uesvn
NtcONY5pbqalgmSbSs8cn78yk+P91qAT+ZzfwIDha8wyxZ3NGm1Cdg8SEi2G1akyKftVVMVJcuN7
N63zELOo1loSCSK9et/XZhsJO93TfQhOorEhrHb66QuntnSgGCunet3SR8ic8ClLA2XveMAK9Bow
yEq9reyrtQs6S7pDQNK4Z6RXOF0WYWTeBdd2obNA99kT2nON2SEwmKKdWHq2DZzB6rZh8baIE482
tA8EYhDlqTrO+RueXGDatZOy0x9HTGhrIj3UzadhgWAVaINSVQOuoVpxcX8B+Xaz+McH3ISEdwvQ
qi0ZDnVfnKKfo8Hz6W2tk4scJWqTSi6iNXb1z9ckDUV32BI/F3l9fcLgPOwjAJk2z1DVFligINsV
qD7cm5iJDgtWml8D9caD6vSl5NX7cV3orO2Z6EYDsLGkI44rj/9bPeW4Ix4ElxRd//cJ0dO5wB1u
0tdpMYwsgelb7g5UsbC5SvZRrjU+WFNlAdUddd0crwU4QpZZSU8+/cFhN27zMICFNkDxPsMLnL9w
v7WQICjWPrnwhPUB8PoOMw7rex1QBFpc74SvbUDFghteSYZGr4SX32zsLYIftdpy4Jd4/+bKw4lO
IXNtZIfLUbOYskap3kBczZl/XexAvPf4tAXC0wqWMPBUabRUTGpRVSL+nO+ibutmdvcdtJBxaKzP
+bdAYvOVSa6AuX6McpXNGaNZQVnQD50ybLyg6Bwnb6c8xMgtdVdaNj01O57gA1eMokASrAWi7sTx
mDEY4VCfVX7Bm73PLRKxwggPAGrbSNKDMs2nAaE6CvvINtndYiQH5qHSg3ezbXclhCmx7gINroNK
hKQlrcL5U7y/1zaHKNCCqkVs8E1TQwkAozkra+EMgUMSHe6/pbF8hilXNGsPcLHjRyNspFwC2zvD
iBPd/L22uDQkDfCNoPAW7K6pVCjMIwcfCAkke6M/r5Ea/NgK/okitVNNGcxEVDyCrZBtG6apcvoe
gB0RegybYtmqIrPwfSU0gz8wwvJiwtOI5Zh+o90YFqwpIUQTjK0of6VeAb3sCsckcccB14M1HoCy
NvhvFjyw6j0khUXImo8UJ6C5TjmPIfretVeVVN7190dp+FoxFAOulv5NIFPaNXbenSKN+6GBbwik
fZulto5sQhozawEVkF4yoUP1vkCrxgTaK2G+vfmzYa/nbIh2/4hc5cPEtWqpjGtN6xp+LSd7MZhB
WfrUCYFHB7A/YjiOq/6RN7ULInHSyPDsu7DQ5tjGCxFqPWXQ+2Rf+vPgREnEScbgKayg84gdPg7T
7Eo4OrkkC31zRB/tE4/vr96Jgqg9g1t/pCK+Ae6xSm6kPg+OmHwVXvvN6waKxcl3Usd7JVHSx++K
z5We93OwEVcq9mrPGHsXkX46jRJh5wgNQy8/gkCD2Vg34S9Y9RFS6riWWu2m6Wsflx4Uv4c3GBv3
tnD9AVDG5xP0W5jZ34z1O5G9Ya+fIF787FALeEpr/8LJT6WyibRfq2zx7Z4zvRBrwOQzZA0NkZVx
hN1IhD9fGLUjQs3t3j4T61clRs1eRF6BOIGQUIVp2ILwPkZSVzlH97CmLVfThMfGT+x2Sq2QsZ0D
+Ai79W+ABHJVPS2HOLXAF+VbCTpDRkJGRgmtjheQpWt6Tm2Q2ZNeG3e1E/JYYWQOJaCPylJt9Zs7
2pj/KN0nKkG+eJIdenkPCG4eoraAmWzdGEfudSwC1BGmiAl79Hxo2M9ihIdB+Zx9R6LDjuDA8Gcs
TkhaxrWrQgl2wnzG8ONVZo6QSIX0caRywd15Jvlos1PSNp0p05gQRspXKE86Zt9LumrqSIv/GRpH
N0yg6Cf2Nh2Gtdrq/hdZwgLCW8oIQk4X6L9tB+89wJLXq6bSOsUVP1nR8kYVIad7qwhZPzItSVUw
LO9coD6Vy1+1HSFpMHuWJ0JzmULeH83R8kafRgrlsZdRSIekNTkJcrIbDMTJop1tRmN9AayvFjM8
hGVQz/reHjfojbUm6UGnPDolyNq3IOgZA5oP48v0reANqJDPC5Qibawi71KHizSgMmAZhxi72GHK
NOJGY20GWa2QF8P4kzD0b1z1xxqMQt5C4h1bKvajniWqH1rAnFa7K5hytPkCvhqh2WYjI/9GWRG2
VpIuCVylW/Yng1wlUENFyEotaHdko44FcCKcSr5q8hO004IkYzs9QsYMZFMK/mdj7UfUvFEgcYl8
HAAq0gPlQVizYDApqigx4LCN/5juI6wvkXv0wgPMwTCnL42EDW++wxmkJU1+CqmdN3gbj+gZtJIj
ZBUwH17cuL9QdOI5Gyi34gXbCekCKa/4jZpF9Ghs7pcan8Eg1jnmrRZYYicmwhvhbx22b7vWENl1
gwheHh3tSH1EXwcB9GUfPxwQk9Xvn7LiQrFm3pUDIdhxWAm/bm6EJ+QWjbXAnvmEwQQyiVo9/oE6
zq+3Js8mT8oylRwVbfAVS9CVPHWOj5jRBOkOe+dGy6ZB5/xG9qNdZYdjpnJbKlvBBdHx1Z/Y3ytl
FWEfF/pYRvu7yemUZhVdLhOEhOaenSsZ+SVkSPo9Yf7oKHBMSmVOvpaCHf+KqnKAvPBpT+xmIKgo
JCQu3j0xdWLKEh4qWtLLIDUvYu9ZpGA/OFL0z0zKasHzFrrlsOh37BWlhQYuxV46i7oOindkQpvV
jRE8qxUiS/Edh4xGRwxzFHHeWfFF2JUiEEqB5YFo42DnBJCSFrhcxaSbAJX2hIchX+cPe/S7JNH1
biVm1NrDVD+s0xxxVcrBt1mdcPqJOQ2xglfqCPElql7jiELhawl7hxO24IGoQm877W7myIgGAPKK
hbBCJnustsNqXRAVT6g/cveBkwEiiw5fXFiiMw3xQQb1TSIItoYkqGOUOhpOYq4j3zK9MuNL/GCo
7JMYjcKybPiSBrQyYRJNiWuisyq5qjYnAQl2i9qZ2nV+/uSL3BnPWjzguHjngCc8DVm8lWc25VTq
3bnry7W8jSyy3gLthuaXJXss1ZWuAXAekJIkHoRbFg+NH4nF0xg2jrzi8qiOYflzyGQK7h+/2Crd
sX99N0OUbUD68uLlN0VW2GV3VcXAgMJviFAQB+twcJQIcMFNQJmqQKsJHzT9fT5dWMs6f0MQs79l
LISEECMQ4kuUOEVwk3+jaSFkMUGX9SdnJFVXZxSzToI2s3c+ydq3uaqqFmB6Go5RZVBnvjAefR+U
qcsRlCBBYQYLDqE2zN+tTKvqYASabaCSMoAta/3rjXbpCXimvFcs8INFG21G2Bd9a/MDUM6NG9WD
dE13SBhJgT2JQBaiZ9UM16DXOfQYQBIEWZDTLzEjpVK0qqtD2xEbbCn2/VL/bdiznXIg+rHizOrS
yVvhcDOW8e01UE4mm5I2egkL1x4hLp2w6R7wE30e5Pi7i2k34J14iWmsv/QpdKCWJaAk0M2q4605
XFAJYsqfmGR79ekzqWw9gvPBXv5b+p9tGkuU29At1UOQKpJyxpvXTijctgI1XKKhkSCJWrc/2iWQ
Ng==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_0_4 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_0_4 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_0_4 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_0_4 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_0_4 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_0_4;

architecture STRUCTURE of design_3_c_shift_ram_0_4 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
