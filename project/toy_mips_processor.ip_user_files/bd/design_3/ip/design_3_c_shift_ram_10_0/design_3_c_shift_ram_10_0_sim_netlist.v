// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:44 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_10_0/design_3_c_shift_ram_10_0_sim_netlist.v
// Design      : design_3_c_shift_ram_10_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_10_0
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qV7n3sZfSo7z5vLnG3dzZdm06mBv2xr0Db2VQaTJrXsaG/5WX27ODQXEpSY6jE1J2ckl49hUWhgq
ovgvRQjyi/p3hmIoqSmFUAhQuP5Jz9Is9ceYbVeNH2RrrAlmM+hQjyLbogYfo4/Zkf0oy5QOqfTt
TR9os2IXeCqbzE27t+dqFyxWKZxp3GeWtI60j++tayVQXZQVVo86duybyxZiGW0WJzw+91ALcdb9
c4pNhco210yX5ANViN4xqhVI4ErFHxV7qLXckZrcjAcdPdyeBFlolf5wMb9rSzD4K5LkspVRHv3j
+dcGAyL1xwcfXnLFQx8MAIGjQzDPotaCC+o3vg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
6eM3N/2Cu8vc6amWssxfVVvo2txzEL6mC9L3U0x3dL09XpqTfYQLonGojkI/4rZTS10fPwYdx1GG
43+uTSPj/6VyMLgMNxYOtYa0xZfaP9AUm40PoNJ+gx4c1w9oG2Z+mJsz95Bik/u3BJiII9tHE6WK
NT/vKhQEy53ZMNntAHFX095As+ZvXmJ4iOSLa6HVx3Ae16hZjazFkl6Uc6sW/TQgA35Fzld2ltNp
RMYGJ+t1xQSIRBXsfDuPQKYrhLbQpg7mPOKcoJHMY81taRFUVvnwxIAgIKlMVcjCIK0u0GeOZDPl
Isgt91M2DqDgimI2qBdhiinH1AX3pu6sBTTMoQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 5072)
`pragma protect data_block
qp6/nuQVtFM1xwDoXskfGYlIrGOaQei6tSDB/mSComgO4/GO9/r7PPFl3mzi27i6dY+tDn/CFdHv
tZ62XpTNa5tYCiJo/1dWsQpOazWx5nwRIKCgMHWkrd724NYb3uQ7uoxf4woTvbEP64zusMUWxbpX
sN1ivqIbUgMsM0EImLrKZSqCTZEBunmvj9nvcAM4vtvsL031cm2Y1gJfEPleLWHPs7nLVUnhgchf
RS42CdzHZ1iIXV2AjvdWjsodCLKlqZbTiqdFQdeA/Bga7CJX4AozCsfQ6BSxMybmEZQHCxCwbHGC
WDWMmXzV2X8EvNl8lQQsvKAa+v1HbS/VZalzqZ9YlG/oXEwxNPBxmxIFHCq914PB6NT7T9CRB2ju
CDGmNsYRGtYZVbS2g4xOH6dpVcNxMWer5wWmYQC1ovbXt63pPxIyhhOgQO1lzozSTlfjiG55YLQX
TgjvMlXqSIy+kZ5Ov6exNn7krnbG2x1GGaFCpvLJf6JTCnaTOgswVUeV5ikv6lNwywgtXGva3qyR
hNrCL4+wBMrzdT/4FadMXFa0nzei2nanrIH9AQIUe1LJXUEG0bvhOFlyIuK3w6tBwOnJcSGRPRlz
bwOs9Cyx0U2t9AbQ92maFcuR2ZgWsaKftaBEVDqFH0gfALC+oYtXpceh89kc7d9EX8GetITuZfNt
HygOKLRudCNw4gsrEaCLJrU70019QgxnbAoATSUO1eqsZcfPJraKWu3rN5tkC3zY8mOVrCrFyY7i
bbax2jPu7xpNPOsJJSShDecJQik2VuWXOku61R07IRnK76spPuZeAdcdIrPyxC4XY+dlzZBX9vZB
H8fdhfpPPmot9qEpPjhn0d+anuOWWUXFaTmb0ETNHqYIxTrVKkeTeITi0I84Ovu9+EVsLhC9Scjo
N/3s4JPrUTvR6x700sZlhRsQRMPIgj0kCzeFeFSLhCo4ivBWLB2NDOFWTxtIrPCltRqDcwxipuel
TwzT8A8l5nFjEC32yHsp0VJz/ycd6W2ulH/k2cSHw3QmqvB289NgOvuoAXGrhFVwaTxxLwml3VBB
9Kyj35Yw1qDHYg+qvmxzle12iyHLinEOKIfo3uw4KfSILfr52OybV59NuexznZLJSoUVan5cknEn
YntQouiYrr/wfR60QlNK04zH0wNZpRszR44j0+bhvQCUNk0iGfpbXN8SLMREIejzQFpG/GIh5mld
79UT6TSiONZeyWToWQ5B+PH1KWUSlO65g2nPk65R7IHCvkCqed8+QuD3bzjwcxY2Muj3LlmtjPpr
5ZP+W0TNtfTHklLK1HMvINzqgqa2KqvYyOhkk1uaAsxZcgDHOGd9hoLwebtiLTKqQgos9ATgMjWi
Drl0+g5SYK9LohYUqQ/pg4/Rrl1ePvoI6C47mjkdQu2O2bUDo/q7eymceI5nzoc9WqS9xIDFBSFM
dkAKiK43+j3vWuGgTu8wqc4VgOBAsFrnKY7lnTRUGJK2tjEx3DKlX5rPXo8vIf25bvU9VoFtbMdn
xwO/RMYtoc6pLM3OCkdekuwPGVbDhp+Ppl+OIGwVgm/lsZ9NjNa8tl1N3mabT9JMvhUPvId50I37
u0TjMhXfenUPS0MxeI2d9RY2IzlQyJL8EEuQoUmgV8aV/pyrVMV6LZ7W7Z6SX2Z7UM2ccdqgYt/b
NFa/icgXWiT3EpkGcf/lThb3+xMNGN2ZYjCdj6LBHh74eMLyy2A9DPvPABcQWLsVEnNRMXG+IgPh
Abp+e1GfTort8NB8L4W8IpNsXp5aug7xk1ehaV/YR0MK0iY9Hac2sjs+1cKM/8tZQVze0EmxvNNR
R8AlY0AsL0XTl/KCu/YnigcKffsruvv8MihjXdzRI7mxgWAGFx+eg6tAB+MsggWsu6YV7FJRy5+N
tlhVZ6pKsXjEB5JUW15lUPWogyu6o5hs2A6EPM+tw6EDAjnrOXSjEu/K+v7XAcMcO5b2pybVHor/
Ymrfdiy4HGCvOJDDiwEuoAKVCD6uSmXYq+K++3hIgQKMjgN1aNxaQCiVDq3VSjzdXpu/C4xXslqJ
gDOq74hpUeJwwHu3A9e/e7W13n6/tAhTmqQLYwFEX9NqqEEjhmX1DhMYGCIHfarkdXI9AFtcAmH5
I66N4LnUyf2dTN4VPqXpIDDyUrJaOiD2AQ3gWObnI8Y0N2QQ3f8IoAo5OqVYhkP/HnMz+K/K+7z1
jw/8qN8MjDaMyn1yPbZgJJHKsIp1I6+msF+Oyee7TBu3q6a8HKGSLHcMsgoPnrHC/MNAHqPNuGB/
HUt742iyBT6FxXBcxghfLrrRV+5m4EhrPWLm5owQtk44rWCwBhO0cQxaDkPpzCC7rZiMTflmdXUf
nLpfypcr4a/vi4DAE612ogOjWZCFIK3cmxtXWzrnaEOqNagfCocJX6Qlsa4xICKqMtwUN2auwqPx
50/dZWzG3vV+unyHZXoPAoUMi4UWaT4RAE1za9OriRu+QKrAX6P5vH4l/t3XWlHgZX+ZgUB0vobW
egXFfbqYymUnyuhlP3iflpsoY8Mjpa5u92x/c3DCr+dekDUdsYux6NV6il0g5fScxA6biParSxqB
GAy/zO+K+Z7vQ/jT9oH0q374veqPcn2T3l8qL8i/3iw0tMELqh1u2RhFnHazRzPakKSG77/OXojB
BGEZr3DcWFZZNkcMedB3o31YfPotoEDi3LvCHuwUzjx9TZ7kBBPz+meAqLptvOu6O6c5NDyVMS57
CT4utkPsXL5qob/hvX1T4Ci7z5bfIjTHhLrHPQ2KrfR0bM7/eXubhPlzd25eVKw+Pf6ClOSRLyBn
YE3EZV9QH9SpuSpNgg9toGhxaViGvJCrtMQ6chFB6HKkNqMOHvkeKo79ifZuytvEhOlyVAj+AW6I
cQLz+6AOl3+I5hAytFoeZNOhGq+f1mQaCfGlHITNdkCN+zMys6MUZag42MEtNlZIWo9ryYRluiRc
z1UrCEzin51CVGUFGTSGVGIU1AFWmz/JP8noqROY1mGrq7Ye9BA+Imt+oVypuMhgu9/0ExovpstM
Bz7FHiMhFbLyK26kEP+Yfs79AFkgrKuG1WQk6u+6GahV1SEvsK3f3eXT19YjAFNi6dlEbBh0hJGU
OmhwAVJ4nrDsI/ovppKsx58jzKP03WC+SeSrYMtfKLjp48v2z3SOhFGtCD7ntcGehbsx1HM2E83z
P6dJjGUAD/e2mgWYh1FRvprIIHE4iyRJ7TT0RLAt9ci0Cvn8SFE6xe1/1CJfiGCpabli/iPDajZ6
ngHke/kDQlMm2Kih4gboDztmXQQ1/XbcOelK+tdamPYc7rBax1e21MBUc4rcCi71kqPunE8j/ban
CBiOkVy3TGnZTHe+N5N+QLZCuvr0r6io4UiU71bMtCzgQzGvICHiQxiJCCVXm4gJbDg47jXQQhL/
9tXfiJ+0Nxl4FB5ONlW0RDEMHNEVSsoeRTfDYfZn7EqEdmaH/hlPFMpWdfiVzMigsfGKD/iAu5wb
3hx9fXHRdctSvxdoOPqeAi+szqTQ0aBCR+rxxFugF6ub8KdglBmSDA4O+0UrJmEJucQQIWZN5gsJ
gvmHsCqZ6qmuHU0lB204aqcdJa1sUccVkIf193ZDvm4gW7pUqb1HiTsuaPkLQtgmPxkxFmrDMJGs
kmCx7mY5Y5oBI3REwmT+oki1+Hw8TyGs1E5eAis6dWjsiJCja5smM55gza6Sat83cRtQV28wX2Kc
Y1nyA8k9XAes49E2yUjts1mO4s+ro39cGf6SGZOm+HzcR69eGbIAogzR4YoKBrlJyZ+5Qgvy4b/q
EGiVFsQLr38GXVO2yxJ+iN4Xi9F4eSeqQj1Tc1SGuoj9p5Pe/JOq1FglD7Z7q29wMm29Mice6ZCu
Fv6ddGAnsaR+5lxqvKhaXXYEb5suiIVfe455/ZZQHsbTx1LpFY7dd7cSR9kxRFrV8Ikssf5kqfwO
Qo1pQ5oy7y4arqxqr4W5fFNAZeI3U+YZvskmJ07c/oUufI/PljLm2cvmCk5iYSRCXA5L6cVcSToB
uRJvl7rsK1mH3QuF1a+Q6rYkiX3Crh/opYnNRLPWdsLodc0oiacKYcgD/3vhXJFueYOuSBoRZEUQ
GZrJjz9BVMw2oPRlALjf0785PcVY8VKHA+lEV960X0ksACNJ0MuDF/zT4P9cFGfDmEELugtut9qS
f4/CXtdxETa/vdjM+NbdZy7fqyAUs9pQy3tpawrWJb3h1UACwcOdA8HizJVK0fX0/MvkwVf6H5Tx
y0ZdyuRnPZPK1olAuUAqJr+5OF5QmIQqLxB7GtupXG/os0NWl2ut4R+5+deJEiGTLoqF8Uj1KN2I
BTb7ByZsF51DkEP4pf8EW0TcJGxtlcl0B98kRaTW45js3sIG79Hh3cpGMyhXIZzBLmri7ixXxwX1
V9mtyqZM0kwtENYaAmI/xhs6hZ/gXjvJpYF9Fgx4yBPwfhwqOAwK/lErTe358RoUqWgqL5GH4HPg
yVWDXk9qH49r63MRmQhPg0FaL43eURQrIkCLy4GT6OcjCX4EalJrW+sJWJ3JRX21NeJvN+AKh62C
w0NLMEJ410JdJwgQHMr9d/y37JQUOLaRCiXa3grBlFL/jK/HDaBeMx0ebCy8+MUfTGpy+NtT2zLR
5WDhHg3UI6+Enb7NNzFkjcA6sItzgys8gxsBFfxT9QfHGU0o+7K5n+j/zjv6mZgWclhNq9Hoo51I
DU9sTMMqFmbFFGiv7fsbDzCL8YY+4JgKu5Jfp483Yc0zOcwtXB0LnldlOdwMua/Al0Rs7DRSA59o
4KuE685dFBzORJhehvyJkzs6w29RfN6SW5BiXKosZ6F9pXgzPAEuE+kCpsBQklLk7gpJKYANzVeD
O6iLgElXMUZjyRxAWRpIVcGm2yEto7YdUpGS7XyFVOoYi8ZpWUtN6u59cvtmpasDXRzBcV6jX91g
pdo5Ic/jjJ43c+pVFDldW7nq0mixuTVs47DnqyKz4RHlTo3Yl8oPnhace4wbKNtaI6d63j6W2ynK
xaOjuuFmrG0eMoOEzRz4jJK+HQ2f6qjzINNfccCu2cl692Otw/nelSjeap/7zqp5CG0aG1ALOA/l
a4kVO6ECl63ayRkzjeZxfQea6zoAbTYZj6a6hhm1Hm28Y1mJfp3J8w9535dk6Vhg/CrJVLbA3ceQ
bA9+fmzF2nZHMG5TCeH69AnfFZqyFxdVLH1i2E42eFvYiWYr7epWPEgQM9ldib0zgBNagSfTccSe
cZv+n5egZ0kL8kQPeGCK2/oSQz5ImWVES5SwR4wBCqhUwuarc3QkbTM6EBOU9RJJM7pOfUrog9VB
eQ7uVqCGPYhcZ+q2ylfDGa+Md59ZyKzUWzOU9fN0le/pHzATjLQ6eyNVVV51cSXbPvRl40Vc6Tii
wmPPHlgpMyQVsua5yKstUJQB0z0fTMRiAB0hLafgMzhX6gPNFD/KcgPNmYUUy1HPv0DsoTJs1wOB
UK6W9xI5zbG61yQbfDPx7BsMq+0jTc3LICEIEtS+umVmTmaUbkiawhZlk51+1F35FnGs3jZVSnRA
cbaIQeTXex7oeanps41o2oRtrndJKSmstdbwc+AmzRE2yVAU3QP/6Trn/gHHtvYWNOGF1aYw9awy
ycuLp9XFBSsD9arqk1WRdoM/HWAMU59hFuhMxncYMSQpfi0PhOKyZV42xrBJ9UNfhchVzvpodKZQ
rqfw8rkt1uB98fYqTz1kyqrfuyTLjqhhvLrqJBeZpMnnuuvdxCF7zvWiEJQs76vYbnOGcggCoC12
WIVjE8nMp4TXt1qDT11axgvi8jeZmcjVqiR+0hGQoaE2vM50LCkfP+0KwHlNnJmuVY1eAP3kRjB9
gIX7+MU8JHaUyVgTTdN7vut26IyYPod2OpPbsHYCHicbmEToASP35Pw9Lf3ctgDxEUC1I8hVvmJW
pI/Jv1P6KPwaaM3UzJpD5aVdbWkTDQF/ppGNCHi/1aDe62xnhIE3bMGSmcBGnV5cZ18E0GF4s5Ic
2te0dfOKgC5846EA2uc2EsN7H2SmY/fZKfLIsCvimr0CkckaxRBTO7JYUb4TbcHDdA2lH+SOC1cl
/MhnkzOrz/oWohul7UHOc1nBt7wRG6VYBCJA3Mv6+lOVKyvKWSAO6FxZIqHII1wQqOFlpRc6ZkJl
68rlZq+Q02cgUqdH7/o5T1smMpGOqOCtDVYYcG3GtcBxnckytPwYMMNAwYLsB6g3jVXsyH+W3evC
z8rlicjq16pntPMGCipP7q47Vh0GLNCmVILy1XFbXGJjcycuFAixv5nL3kiO5tyjxmAep5Q7x66Q
G8PiS7nkfxShvHT2GX18w6UiwpKK8NR1pBMMSaKvq9h3hTLY6wASi1bT39LpurqkOSdfLvJ8sgwC
DeTb1mn6M8GDflzFfqereoGEzMikDDjRLkS8Uvxp0fpoi+5rXZGrabcuCdHf3AM3+fJqwZmsW14U
sZ6ZVPFMwlmOdegGWxKjo9M2cSGTb7h21a3gSg+A/zfuc9bfrGXpOq6A9oUv/z51uw3TByaubTvp
YOmhvT5nOwdRkcN7pvJPN09Z5zV2QztDtUUTiUoOneywNUf4FhbDJJp9wC9c/hM8qxPHv3wGQYGH
y/dXhwESv65YY+t1Mw5Q3MuQjzskTqMDKcam5mIEm9cnqVIXrulIodvg6M5WC0UTiupe8P6me2TK
6Toteq/pfftb3aJHmFSeu6v3pqqjBE7Wm2rYS1PaKmXl6Sn0uGFj82XGBgxfv7SKj7dWDBly9hk=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
