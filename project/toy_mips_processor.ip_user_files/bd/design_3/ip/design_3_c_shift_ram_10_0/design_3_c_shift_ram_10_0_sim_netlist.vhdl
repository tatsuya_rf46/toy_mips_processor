-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:45 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_10_0/design_3_c_shift_ram_10_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_10_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
BsPsQscvTJol4ZN8fskIvstAvL/WP7oPSB6KhxFliBcbeFW90OeJs1g/RVnLv5HQR3l4V0ZloAg5
LdtuKbcYFm2rSWqXygsrry91Jbi6PtrAtc/B+t34Twaxm6DaXs5imYO5JjDMyqipxFtMxNq5X8CC
UwNX5YQFmMFHpl8rRD5co8BXwkE2NYKZzHwOuZoT+e+U0KhfowMtQb1+9+fTutCCKt4E8vpU9N8e
bLIGOPc/4N700Bajx+be2BVFMYnzFZBVEetGGmup/fQd1ra76wWJsIqOcNn4rVB1W/7maDWu2stN
uyKyB5FvXpplquW/gaxL3isx0nCfH9gLBCJqsA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
xvV/JsaF43l16ZtX3AFVw5UY1ul5FGI1ckYXmhm+zXK2AkYFjSBVbcYO5Z6v5qJ+Yr/+GtQta28W
gUqzRKuM2rck6n6+cSaHxpNxRGSWhPGbrZTKg2Xr8MC324tpjqFiat0uChIxRkF5DUKthw+/lxnu
McEgaO2IAASSQkjrWf7Qmi/aUGPQ2eTw2zF3fGnUG5Q2knU6mme7VrB77t9tTPALWOB3VLK1uIa2
NmDjnEeErdHrLIWCvCXtjhOS8mcFggGNwyyle67snUfvPRGuZMv5yunVNTsG2akTTSej743wr/jM
vRltlm1a71prkzlJqaCUizUqr4rzGlwKJfmupQ==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 14368)
`protect data_block
z8KqvG+MUVkC77L3bNESRIMQ78FDaBi/BWH1Zr8tPcKiibcCzqIkIKClMiCGahQG0w+/WUU36Z4Q
xo9ckwWap4XGyOTIWgCZG00W3f2+eC89qtFXAyJlCnlHqgAyKe9jKhvfxY6cN1JGtIgCwIuA5wmT
2ndX9NkAV6Y76oErOzVON9h4UO6OXz2yCHGrrDaWLVZaQjb2zdTNL8SvKbYx0pc/n44xI8NXvqC7
lPyY5+LLDP8wsv44CKq2CuDZyP+ebuF82s33p6FRBYidDuST0jDR4PUA/CwxikgMrPDum5WNrUvX
Jj8pK15tKaURR0/112OPcjCxEtZdEX9N0iGEGEtnuqEZFfYB+GiOBRGjZOy8/o9mqSXkCzohRO5V
9nnf8YuwDN+zVd+WBBtAO5VbtslbJay0z/AHcCMevIRrTUWoq1SZAXDxydpDFalPZWdqaE/ljG0c
OYMTMOuu1a0MlQwKskayr8dPMjusJW3mzTJd/KGCihn7kJrxXURFMUDLjr9vraXtW0m52/WM9e2J
AKTecFR15wn+9cKF0g35AFgchewbMrajPe3vGyDSW+jiDkaSTxufBM74Hrf018zadPKK3CbV9O9F
vrD8J1b+uuHfEEA/bjrLHkSQX+QndGmBsE7kf1bgDuwEG4/9rBh8Ge7Ok/6KtDNqiGOUAo9f5oJu
cvTURQXq1CZ43EGluiL2vARyCiqNzSLgcqvWt4YRiDEv3WKMPvhOLb+bRMz3pwvxvjv08Re/VSjU
ECMqlXVzgiY7cnRR1Ov7Zb8IDUf7LC5AamR+BUH908smPva5rviOdVAgaK7jHGRPS7+Q/gTU4m1S
uEpqKonTr1YGggayso+8MVB647Q3Tj475NCyhiNJXIo5zB8kQeQBAcSnCDUganmM1P51xE6EuK4t
qGaLHr5m+4fsWyaPeH7UmAU/xql6BtejqLzGc4Ywvk4JZ9hCqsALJecD+wuf6Nb+n2WkjLd2/JZw
49cCx0rXXnlpzKbixDgXCBRsxjP+RmCcJr0SWnYom3sZcBy25/S8TgLzCZVi1ltdb2CNq3T/ZNGb
14pGMif9eUez/KDY3JPhOWSS6ikAf8G3sPlqiO+0nJR3aldsh5WXvJZ2XDnJ1R/Vm5XJfpru7Jnq
cxRoynj3DKg5E7b3Ax0xCENmgrjFi/eu1v8uxjwxO2x7H8abhdTJfyZeRjan28Ra9V5DTTNeSm3n
VO7KP6eS57L7nDz/4MXEEkdLLArkV/E+i/CYn6iQ0PDVr7VJTpd/dNxlkWqOlyRf4GH9UW3spdvF
uGqDH8hYp9iZPSK35ZwymlZlngnWpmwsWIC+OOFtKOWd1sVYVksTLU8Qy8XYf+ItGoNxrsqxDB+B
RZYmwI5fOU9yrFtF5CH6b2w/kt1WG/vCWbJZ6Spf6Xe/9rw3C6Kqc4VVh/B8A2ViNGFfbL8crkKO
x0OANGFEJ3otxa72yFBrOlcfKzILGsLER0QAmOECpZCkatefqkvBUBSxAAKJJzD+bW9gyRMsKPGH
8sDMuNgzzno3vu7T9FK28H1YD5ENadio4zqoLPxJ7SZTAttR8aj8L943VPMYVidyhuC/zEeckkny
AhlQ99kv008GsAiTvEjtBFcjA33osn1sxLUyMFgvlv2SuC74Qrqs/KwcpmifkG5MKiAyAHr+REHQ
Gg184/GhoQ9AAMROpgVM9n53PPgUrvkSZJpkCBP0VBghRUbN0pYIy7xSsUcWe1WX/6JFPWux1Jfh
IDlrAyRPy00s3Ha+CvQ+DmvVodIwybzwnhTzdufUNztU4ckqh9wIH73149SWrHjD3+f2+4yzuLeX
J4QDINfV6aNB4zpL09QXLRmKs1ZiUbFzrGYFxz8MtWDUNHQTZ/JRxsJ45dYkCCNMdz/+yXdBF3sl
UONoD2k+P3+DrEO4b+prqbsQak8Fn/29QZlr6evHakiVyOgfY+axJYdaZ+3oGAj5h0kNj0Cunp/a
YtyoNDq7zGlrqwUwR+GV1q/hgi20/z8khCSYfjfEyDP/wHhY2P9WiiRnM/2bdvmrv+KlN8X5/5qI
22KfUY8AROMnjbqFlN+kwNC7q/IVM4W6l96g5rux28bS8T0Kipo6tQaA/il9HZQgBo9YSRBCQyxP
7AjwkHnWltbVKJnLH83v7xPHBm6DOIrBUJ3lDd2UWfSZfgHSaKYUgk5F9qbQmAw6tjnL72I99WCt
eCYLXrogJDfYlrSjY5yxrcGT/NJOqt952Ge+7BpibkjTaXmjz38mKa8PzC+VjkVkrge3kqUG8/tf
CpyjWuG38cFlNDCzkLHjs1PwaVwDUGe+qbJsScE3841p4dQQ9DxKY6elOg6OJP86ZoNkPtSZ0pDe
9gDmwyf6KD+ZuAERNztzepPDLbiXA+X1BE41pDoxAJ0VJ82UZIVSHTuI0kAZLN2WGeXpZpLlcdrc
3Gdku9VCcIm7OqkBNM+0Xrgu7Wo+qHYXg2sjVGbndR9kA+kInquIgxPVLLZlNWFtXyR+7zqLJzgv
CpRJz8MZh3Rfj/zcEDs3rk7gyekWFD5HNTgEzI1k10NVYKKhK1fi4svKlTFTDOFkD7HbLZIh33DZ
UgfUfpKF+aKpJezW0pi7zzjFSMd7/IQtcgll6KP0iIb1sIzWURvc+GWoBxPJgPcWq0z7fJKF+1Me
t4auUQsiwI/2OSab8E1abCc5BCOsmFc7sjo0fjAVi7pXVajLDzwymbZFeWXTeNsfCkWKieGU3EZK
2uZo4TgIeZDhGlTmMx3p7f5zm34DAwCYPUJLsKURIGspVXc/YYCKUIsGcIZ+8khFVMB/OzCedGm3
NLet4XvWgPlTBx++n+0n+Qku7xKK6EktMa1MPA0F891gaG1f1b6aniAKlft2qeaWFBrjYoIEa2jt
C3etrVn2aLjPjgSChSz/UOW3pCjbfObWSNcVPptc+QmCf1oKVRZlq3ps/slOSNlXPkv8EQuLqTw3
VCUlbVdFWK4gaCsSn4UEz/p75MjOq6EmNP5UH87iga9rIvQERXVvm9V+tGfeutHS8MLe3z1z7zNw
lFqN1/52x4/6Bf0F2dFyOwpWFm6EitfWDlmTVsjleBXSbXelbpy3qqXnFlF4lbocGYrVuGaUiVZW
zC1Ao5Lc5MCBz68g7xOQwN2V/CIa5XBbBH8AxrteuFplfOVUv24AlTO+iXqe+6q1HiYrFdtvzT54
G+sIQATPaz86lP5x165i7x14yQgqUWgxcpzsQyo14DFAtjt+y10wtfsPow/qYLpxMOXUawbQKiMJ
p1YB6zdQounW8uphNQEjKewgSOCRhf6pvhR0OAIv9OEy7WkNFUjWOi1m1NIBcsmqrl17lUM/lv9W
I75Ap/3yt6mcOhf1EdPqXThBODKqu05606oRmKW22dBqEM+z2Rq3X0mXe/R/NxDsHXZQBK30H+Ze
v7w8K5R0UuOO7F1WYdQATtRcvI8n8VeiksGI9Wia3dGVZdJ7d8gj7U4J4FprC7ZGjK1PLwR+IoU/
73TOOPbKq0Nyxzn75g/WFCtbQwikVXednixgpvKB8wCpJwyP3ARn6tSs2yBi1mxTzLQp3AS2V7qT
5unV/D1DOgq7ys33zdwKX+f9b6rkeY2U2pdu4C4RZOHGQomnMVEXXO7+wIBNXmYp9WZ5TgvC7Vrc
zf1ZtaJSMPMvLOGeBt5zcVR0lHYir0FaZZnBwMcpih99jjf8C5zRczP6TMBgDZ2WQfO66JMoiwal
T9g1CLQ3cyoZUMMKrX/lTAN5gQdCw2GMTFcnnxATIey1bI0GS+ZewSJ5f2hy9fAr9vVCFRMIQXsw
mEnWHdIF6SOi++OMuXjVf1Z1uwzXOCGpzjsYe2z5+0BxkTUZIiLOa4BKletbjzjqWHhpaApSSJwx
uj32BzLI8T2AgphQpDnJvEDXvIHkBPTOOcNDtSylOeKnKvc5FpAnhv/K6mLCjjIwrtl0ECVndnIO
yFpQ96M1ZcOj4mkXE+Vb0c82n6PhLJtdiNsudd06tzaOX/jxO0pmwp0N8vbNAD5b+pWUHAXn3Al2
04o+ppFHbdKutFuMraA0wPs5g7o0boVQ6/j4B9OQ+zrNjQkH0DCaBlW0/0zIDWBiXpjk6jT1MSfh
YicZZsGlMFo8Xs2Jge6jcnNWnnbHVTOFzJVKQmFtYH1XXCistfRqmWgi/R/iFijJA+loni4BmUyi
J1FcOMMAMZwzf6ysZ+9gr0C0uq3JOSDNIl7YYOaZXq1XFCYSV/pb+lTbnjFAX7XulbGfYZ7iVrNM
KYX54QA9NzjFkX1W6Nlbhfec7GPYxFii1e5wVwNkTf0RudPuHdydgYUxnST1V7ku12st6HlE/p67
DRcKRWWT5rgOEsvpAe4ADMwnxbyhmRRVm1UtgJ5o5vXK6bYAHjMrBvVS/Je1Z9dxZPRgqOj6JKIC
JNEQCEqLHU5ZD9F8gxZ8cECbua+qeWheHyaTz0r97nxtvCnxzslrzsghkzjC24swoknvFsySSOJC
KppJN61FkSKs8Ga+j465NMlvnx2w22xfE78sNVsIPuX5zgnLAL9KQ0nbLlMdV0sYV7sgmmuswepZ
PVH7R7QZ8s9xpVpqYIRcYzhRNkCw6t+p+7IjmyNdOjjE84IvN4tpEqLJ+ZIhz+1R3GyNrOq1PKkg
TE3Z3BDuwKd6lK3s18I/cHQv549AKMfTHfVlufALEo3k70ye+MlygvSCB5wdNRKzaOWgwMzNEk0S
boEIj32zQzCmreiitQOWovVrpZNnZY5OkpILIJsw5nTX2GMaIvgLzGa11jU3DtTv4h26i2JOd7G4
kjLL/ouG9RqqGDGznFfLeFrTcPeQ4/Kwt60gMZjsFckNibv8ivgs43yhdFqqL6Kvk1LgkVbyM6hU
dWnNn7Q7QGxW8hp7u8WQAJkBO7t4i6p+A7RJB5rQaylCDyIvvqpA2BrWkIR8Ab4x37WTTFZnRrWa
Mrnwj2PzB5zd/UAJL+8VGhtJlbUBtQvxG8arIKe5AMRHOlgPCbtBr0FyKKLl7xs567fJj+lz21zm
C5fe5UeJI89tZCq8Op2bIUcp+DUzBqMYRXrSW0gqZF06OCR/fN020SGn7ik1kt0KYkrC9yHO40mF
cDRLHVuuNthwbRm79zYdNLUjMstAj9LsZ5E97ZqqonECiPKF2GNtN5oFI8EQGcyukQGTVRAQNlUL
oANCFxG/0vmJfCLrn0Zxddv6wXHGopYje//ke2Ucncttev+hZX359RA10Dpaw02Eql7fzVru6yLS
umcEHe8WEDpF/T6ca7AiDAxalsF+TTjIbmmmDCendjBaE/CtrGxgmTlz7VpNuyazl+tDwk7rBrxh
nxWvDupWoWwmOsN9agPHgHukpjQiIcYO6xy8Wg3zI+tvLxYVMGqjxBf6SV30fcfCo6n36fUEEpgM
qJKo0eEfn0atyDCZf0Zhi3I/jys/tw/cmQiNwCR5SPSTz/7oIE5qvIIjpLSZhMVRfKbxpqQAxEiB
76v+ayIsK9Fr1Scj1o5DE+VAaGzeMaTIcj9rd/9eCjjesXigHcZ6dvuBxIeAaq/7TOuxY15tmmqj
+amdI4PIlOqETyqkVlZrCtPr8MUiKnRj0LbNFR6NA0g2XBAmS4VmXaht+zPt4Qcqdb6EA+8iAIrJ
PaoSP++LBwtYbalb/I6FjYm4gRE4/MdjrfAExadQ5/olUOcGnkMjWHp184hIm2l/PSUAWwBZ83kI
P2cifeV7ozU6mi97Dt/7oi+e8QeBvwMWHKJou39/5o9a5ytOGkhdOqp5m+xdMV/fT3+bN580nExY
9Lcambw8j1KSSJd3+Ge99B9OASbA+mm3b0zgl0gEifDPE7LOGfaIX+Mj6nQmqhEnF3o+5NHdeYw5
c8Dabg6ReerMtabCXjpoGmbeAVp1cRAWw8nRnSm7Vssa3m5LJK5OPlgFt6ozPBm1k3dQ3vNslZcG
QpHy4P1f/M7O7OjS+bQVa3EbQVTqEV7wM6BN4wD2Mnrj+U3Iv+yKxoc49pMWrsPeWPc9TxkVIXz/
HBH3Q79ENyHOuIgdj+6adsTfx8dTDCzb5ncctRj1l4ctnUGNd/m/c7taQSpafbgXmWEIMhb0fdu2
6TjFaIDEhB+Dg0rNB6gkr9L+uIunx+XcEGrKYxx9wcEMlIn8vQ6cxWjGqcNuZ27vXZOPwaR2Z4x1
dhBmaLc2ioVtHp378bpsmikhEzzrWykwgopbMiljGfw8dmDy/p+chWJ15dlwM/RzJeF0759r4Wcd
0OKGPpp5ITP40nrMTaoIKTDgwX9VpfUhlutZYV4ixLK6uSPmKzqeGtntGyYahyqBKtJF6SsKWgYW
gDITSu2n747sywD2OEbIJQAkCa6RBuBlUtnfNUZVA8BfCk+H7xMVViUHVQqzJVml7oTbbgX/XxEQ
WVGFVU0VW6PmanUmuUAkWhBDpYpfOFVbzbq42KhkfMbxsrjAsneymNuwLyT3KiP5iCUJyU96aRT3
2+oA2al2hFPYhpvzNArDxbGCImc+b8XAmOrtw3LZLrkzVswU5M/EnCo7Y8+VHhOQO7ppbNhHnqsO
gLUL+1UyGG0QY6YR+FBE0z7a4ckuo4Pr38DgGbQxf32mhjJoEsimMvPZ18Ef1Bnc0LFIN16I7man
klNQmkwcF9eonPI3BPwuOjTWUdPpShKxrsYXhpj65dFD5NzNbhLvWc3JhlgadRH9h269QMP3zMNf
nEeVWh98O9qDUgg2YXx4SYdw1N1GdYCqBP1gLsWtcpjLPdmC2JBfn2u6vVyKncKnRamFwZvoaCnI
Tu6Q642IWojKDnuQTSZfM+gfwGSDe32zKxLjT4eSMpt9l2+my4wzex/FKLW8HOEoya25Fd1BbR+X
dGqBUVfo4QkbI2JlFhjKnzS4cnzCAkrvI/Qaj+Tye5Xzs6/3csOpjGzIt/CFEltAvFinLDN80dH2
xo0OWusUEPQd71+kdMh9RUWKjB0c6hOCSRb7HIkycIkSL75s4H3kp7WL2hD1WsgeP+hraSocaK3H
SsnavNIW/K27OuMRnf4/rrXIaw0BNpC3bIRpNmAm1m8tUy27dRp43i/BFVJQmkekWNEtCgFwTLw1
fD9yECvJHUUY9DHxBJfIBYiRA7XTsIwXHMSLU0IXfAtHalqpXRejqR4isI3hcHlBnU/wLhv2Cf5p
6N49r0mhYp//XkfGiG+ghKssjc7Dor/4OmvPGd1c+4npQDpVc7LYb/rlJbjv9kycb5bBbgOHeUqm
NBovUAWEYFZPkVdb6Jg0/Mztnhew//BMZIcn8yXNHAo8rbVLJbL8w134HMpzKEl0iFBNemyikqNa
MLflx3kQosO5nBQcvubsZ29EHRIkvAQCWjtYv+qlCtZQzqZE6yN0BKHHFR+RED0H3IpmwN090b3r
olhkWQQShNfNNpBlARfTOlMWrjQYLzOI+oAQPwlOSmMaQB0o0CnVm0qsHy60l2p1FasdTY9HxzZS
3TmqBA5Ci4ne93C9TQil3BpmQb41Iyr0+rtv9iRxewhgDINeO406r3LoHboxqa4EYq17ZWs/6BYl
K1d1sk9d+mMRgoOSsi5ka870oSrjIdHlqFTccfW++EqGd3Xc5Pf/OIn/gfE92AWkGg5Lv88mZuVh
qvFldLxF/fiN0xE8Z0TwtV1d8HvPabacbfiFiNpTVKKfJoMxwC73ylHw9P5C4dXj1oBHf7gETPtn
LqBlv+LEjqWsFZNsF9lCEDP9HzyXBGKSlTGF9QXlUagJamHMm73T6Y8LV5jVQzrXde8t5KdivmTG
UIdS3d1FXuLtdG1hcQi0TkYCV//GdQ7HnyzbXnA4VfpdYlpqTWIeGYSamSZ8d1lgli23C39iriUa
u6lSOsqA2+89q+v1Q3uBtWmj25FDVsZde+cVyfX+h48E3JsLzHdhleofo1lpJL4n+kqCXAn+KT1g
7WGKJ3UfOv1hu6S4QjzcvXavOJZq2eFQDwyM5ojW4fDf1gtfs3s9K2G1m8K07Mqb5stc6eH5mmS8
UBPXSpjfsSFNHtHCWZ/SnfcURNATyAzmHYMRY83U6IM6mjqFJxEoZuHSTCws5lh5HBKUOpJ8oCo0
5nzQF8FOzT4jgq+ZUlVkoOZxGp3ifl9j7Wk7Ofgl1k7bu9+qlS/q4SOG0/dyl0IJBet9S/ptiBU0
y/UZSX6B/w+d6q4Jp60h/ae969M50H/m+XX1JqodFeH0iMICycmnN4Ma5nF1NrnvWnuMfrye6Plm
tFCThv3pIkruw9GIFlaXqYKXidaAkFMwdPqnsYJa0aFfing9IlmwPeLOYNz+mIq1zatfP9ss55nw
QRO+YJl5z4PtpjXP4NBjfUZEft+GmHlT6BQvM22LJcgXK4YCiMB1j9PuJ4uJItpXYUj8qVqrssJG
U95eJ6WLtWd4fVfQ757F45VqpsTIZG4fQFlRb5vRkEtkv/ZF8zBj4TLTKcQlp8tZlTBD1w6jCUGI
hi383iB9I75EMRXsIuNN18aR7wBSb9CyAY5XNbpTPUf7HiBnDFES1mQPcmiL+5l/MobA+x39jbO9
XHfz2Jw/uSqqEpImrHwCcqM7F0/f8n019E54I+u506551n0+W7zweQ8gSnvGCbcff0noNhZo8CH9
bfP13P0BgKjapqpXCSoovRh3Rswq7rQns2CTW3Rd1KLgMtAxM5RxhUX/6x1iE3hshdogtiUu9opb
FTYOHw8yO6S4O0S0T49zEscRqwW2KFEo/HQXgOdSinDoLF8fZQRasO80d16tYrTUsoGQEEYFKVkU
RPShfdZ5bKxl/KoGKWznaSkq0NA+1rmtptIL7FXCzjwP953yzYRCAmdk71HsRMfv6Ry0rlaVJSzb
s93VVRmWSiEt78yBJ99U/Dud24uQ7sThdZK3HzGMKlyNeBqx24IuD8uUFphLlshyT6GnhSs26AIn
Jt1/iMWqIF6wM3EpcjC6xc3tXvYW4syvaVbZSRoVIOrqZUfJ59n4PV1nKcvoFQuhotrp39/OSLRW
XqWUuTlZc6g8IKbx9T7sTpFnIrwgV8Pz5ew25Gt/+YFBAUsZZ9jvYPVX4KWbFjCN9JkTGPGpHDvC
dlG9hRn9+ai/wdV0O9gPu/fLZ3KdtmWdTYTLZCH6hkUqadot1xfVvCzKsq0Xp1pQEWX8eLGxVvOT
lgi82jg0CenCDec39sMeMhTK1iL6rCy21xinv+04Tpizr/m+kjaIafkLvX/4hZyde0CfCI3GoilU
cmnmlD6JvB6PObY3lHo7lriaUzAvWdBlj21QjVV4vDRoG9gn1iFZRmBZiHz1GDV0t3Q8bLRhbPVQ
6Vi9VwdgGGiy2UfDzX/rL9sYoCEaY1Hnr68gh4/fvDnjH5zpMmVo13BKAFuceiRbZxpUQXT4r29S
PUX6aGDzEjk7he9IC/pYzi56vxUmKHr4rhcOOb9EQMKUtdXtpKcKbQir5J0z1fCVpUAwO0bqa45z
5dlLSr+HO8R0BYmaSbldjwoAjdEEVTbDpVBpCq81DVVCI4GdsMAiqWPpKbHdvRG1UuH7CJSTlWNm
Pd1ubrDqe9+Cr8K2pxejZ7iAwcrW9xH0jAFphT49KvRfgB4dPPJofG0AJPf3afjsMOrFi0dIqW08
+Y+6529ji3+mm0WrOYxCxdTPQ/FsG8qdBEGrhmcsi2o0l7pyoHYKqtZ3C9sLuP/SaDlfPb4FNd4R
zvE7v0tdCA1VFQijXtHtFgeAOqfXSscFE9VkyY/XtQR2LMMvX84Mnv0BXqNCo/TSf3xhmyIBeYMM
hGhXkbISouCppdviutwfKLPrgeOcGnM4yvpxFiyAlpijUc9FvNitBwj+3mPUNlxumb+pSx4AFega
9bQeFHB4crS06hV10MBAg6VRn87+qE0WR85x2gh2D6JEM62PtSULONyqqSzRXSr3J5iV66Dc42s4
r2M5b5vxdoi8VLCeR3iA8KnSX0wtB7Z+q2cs/CT3paWBKx6pX8Yu99QOy3ahXZ6ts+UevCTOvbqe
MDdSvcHrVRVzOOiBvTwxr4fqVk60nE2PkCL2f6JaYQG6CkuH5iwJHVGDu0s8DFpnObKIbchS4+XW
RZn2ucYhD+NNIwbpi5MFdvSxGonQQYTcHhJvRBKCyXG+3MLVG8e6SWkY7vyaPuMsm/yNOdvqHVQH
aGCI/+MIbT2XmBzXBaY+0NDcE2sPrlAHZ3Dyy3du9VorGbYwl0NcfAVo8mdlYv1LYT9GJpT4bolz
1RtYgPqku+z7ShXXavEbCrY0FyWYoLlrLPo45E0KgZafSXI7rQaNGZ7tsbjwcVDuSuhy+WOSRGzc
JjwpkMWgpQ7H/3I8Iyr4KtiNy19wQD+ft6TU9wdinAo7uMNHY3yn5ZOmJog7vT1v6vZmR3j7QS/G
bpGVFiPH7dOYq642gUzTrNZdVMsFRfHfDETdJWHBi/7KMcQCxFmWHUmS8iKCRrwYnLUnj6vp8yD5
9cDaFpssjLggiXqe6IolKMKySH5kFeCk7a2We1o9y4ZqRrcfWW8Pxl/899aggih14YJL+vTuWfxc
1J4tD0O+5mCcthdb6vaNlJi9OLjq06Rk5Pom36hYJP+lCVPnoZvKcrLMmKdN0m8YtGuymM666eFH
5dbbHDBjMJ7f2duNByMhCdEe6iu76ND/KdpC+GraftQVY+M6VwbNKATpCj4CDa7ZU1aq3AlPh4Lf
SpD3VDt8P9m7VTo8Cd126ZQZ6zBR8JObfkXNG+qfPtxcjyiljajFQ+a0ZjZ0GH2+EvA0UjKzuEnQ
0+ZY+uxuPfluaWkbbWMpb6+vsCguQuDO+SjYvtYctqyW7mvnVZL8bWH3G8FkWHfQ60D9wxzdD2/Z
mYDbxymsYiiLRSouOdVRDtceoTukmv9B2SSVwnc4kp2+ur5SKL2Zi3asfMU6qktYLzORdAim5kEi
rnJmy2agQpTHLZR7AtAeECvTjtb9nOn/0o6o2NQ11Mnr/GS/rmgYZWhcLETsgu906gLTZD0CWLYQ
4g/L3I7TJuVxLLDx0kB/Vjr0JwMTdX6TJHRCGgU2jamMlRk8iTCsPOVTc34Z/MTUQmZMBK29YZeW
8/WxukTSNyGvVxMx/bL5aVgbhabsQ2MeQZAnQjHGXahT3wqDAF66AS9Pm9PvLgG03le1IPHqV5kH
pOQXLi7/b7lUOD+4FspXi226yNMryNndA7DaHVzfY44cjQlHxCIkZ7xy+UyuNB0S7hk5WAupZiNT
zFCgF+/yJ//5BQ6AGbFOtLN0TSGRSSGEqWUM4NBSDXZ4Yqmae4qAi4/IIrdBYESlOELH8L9SDh5M
GhvKnb/87s7VffHOCyXL5VKUgXnzoG47G/is+I2BX31zRJonQq/ncQ56ZerQFdNW0tVnyFAYtHfT
nVewFtHCJ1tEiPcU1zstIc0xZJLyJOlKTVAqy3FOZerbrzmwE8l0qV8ZDzQy1uIQiqaZq5JCc7hu
oKVpYAWOSHv9+ZKIhErp6K3r6ay/t6+p8mW6N6Sk1PRROcvGu4/ksz89IIHJnuWCFJFwELYX+17k
jXbCPksSG5C3F2nJN695hahHDoJVloUA+3om8jsPBR/tIZA4/7UvIPluukRpX/6M4e9MJBCEk72u
Jk8wVUyrjvJV58ZRWDVSUYfVPU8J5nZ4JAS0GcX66UYLYDcVP0q5QU+ERKvHRddCplZZL9yXBAkQ
Wnv9yrt65uwhqids9hLWWUCXmjU3la3YDNNRomzo7XrCipemInOTQONvoJB/R1REdt6Dogc69LKE
VI3n7sV0uaBb/z7VYYG6TwDOI37nNJliapqH5OO4mdh10Ri0ibbO+zPmjjYDuNWbKsoOnX4TyI+s
8ymwTGfYa5VVnjWC3QhpvJdSn4VoeboMgDvsj0Uxh9WVQaKJKMQ+l+5wBT9dKUJKY+2HpD0lkY4P
8xuGobkcb/VFP6Pqw16xkGdNwonddnEf3U1hBxTymnSEKml1gaX8fgHmTXkCe8kKXa801AYLfAUC
rr0oxDX+XZLSyug5MyMa9l8A9rOjdATYVasZPXE2xvYiS2Rijg4yBTUYyDemPmveGZlxHrfUN/9x
JL5CPkV6Np4re89qysghcOf6A8k5lvjyy9fFgptklRrylu/FybtzxA2mTGRITd1ET3PUuS1pZMBH
KHqkyzM91HBStNaBopK8H1co0Yt6ZGrxXdXB3MVftMnAy4U2ddu0KHDOqJIrrbIheCPEWeAhezRH
Ls9pNe2u7LRbGT6ppNk/fYok6LE6OKacDC21QHAJFmyknikYJ1AAy7gUc9eW1o6HVKSapAo/VdZ6
vCtHpqppA9ZXYtvCaQ5+CspznZUf4jMX5Q/xXvGv3UBIJdlxxadtOZ69Bj90n2b0VQDQlCLHkoZW
m31/s9GXBTasdgYQs+885i9mmqd+dJKeZbDtTlgUD7IzNj9W1y5kcZDp/pVAmwotNQXa4J4eknXv
m5GVUCuZz6Z9Lk3sJo/CmOcDQaJ5FdCargdRbo/qN2DwHPi2zrXXBuumJTtDk5KkcVO/winFQPl6
NCZHwiSmgoJn3YMOoNJJ3Da6a2A8dyar8a+OyIMIX8MjqKXd18BCM9ClyXnNJZqerMCWj13ZIdK6
ErlXFh9nERFkmVuAvCn8TS5lKndQbsdZXQFmlqWKkepsllKvhPYGw6FA50Qyom9cl5dXRXPz6xb4
/MSepJdbh99jUorV32yMFmSWjJVvoTXXzRGyNk7PKuDUN9z45TVTS146jnIjGLQBr9ttgJUEcNDl
ao6lBdEgc69d9XRauUgHn65eXIbQfAVNWIXXWFt2lhCI05N0uco0TTUhImDscl/etDKtrubvCx9e
dFoqMpkkqRw1BNZT04cjIblT1G/SL2peYuBS4A4fygveooY7rrwbd75XfQcI4PHXq+HEGS/nxEZt
O59CYojfCYBcaPjUieOKo36CXR2WeWwstqKWgnB2os/pmwE7Mh41bGa7F+/nIH8p0JQRgMJrXuE7
F/pitHeH3a+za1GmNFe4ID+wl0Cu/FRazY9RZCbB7HGQ/wQrVU9J17f4NI0+vGK/iLRcYeeuhouG
0nOvmR0ge5nw6Kzc0B5GeivUHPLMaW1oUHP15FS/bOPUwCygGzdNSPWGCQNXRWdHpd6AnEU29U1X
xeouXe43ZP+Gg6tWh7AEkd3mHVeAiJag1gKn5VDCfV0svm42nTp1dsECeb5KOyhQsyTkL1P66N94
U4+/wA5HFRmOagufxvPhS7JBRvJhQus3/CLHHfjSjUu22lsyHkBUQOMpqnr9+WFQysc0SnJeg2bh
uYdJMEIuyYSlE8zlV6aFlJRbniEOXSJX+JgIAJig29SkmB3rLNfAwGUXt+iVtqZNUqJUpeTFGjAd
73shUNtyMV/C1FGPI35TU7Ttt1T0HlqIykpjwbZVMC/rAg1KxoGSpOAU+eqTOe0z1ZhgZQ82cz4i
cyTPpqj5rFCbP4KAGIBwApELiH+YhvtGHn7m9+spTw6VB6OodKQqXi4l4c+jLAoK/1tblUe0jFkl
+1Z9aSODxkiVFsKpw8XPaG6YWybM+roVrxGx0m8VpOGyKCDCeqVo1/mQl+RL+Wz59NQ2pdhQ7E/b
TzHIqnJz7klP8Ud7CZo3odAdIAPmVltK/rXmGStueg4YxX2BxbTYClDpQ6erSse2AmWgbGtXYvIk
bI1yUM7t8BKdQrqqs7nfPKFEKhmZjXR31wYCAbUK+3jt8vEWAZ0CZBgh3JDoh0PW6je422C59JI3
Qo5uW9FY9eB3Xmu9jZNF25QOecjEJC4sfJi8cnl6wUOv3kZ/VkMs/aHDsO6kucgt/E8Sp0f1uieL
CcKyL0xzVvcI9HQ+CM56g8yJnE51Ydu6X+pfANgW0iM7U6K92QjD9NfeFRdFph9uzIjfK7ahf6IY
jto/NPDfaP+DhdIaNQcQY5fp5Cutez1pHylSRI3WFolCkUg/xsKPjlfqeC7xchL/VC+tMQAin4IO
yQQH6iyImwx8/T9xKLdwkSOt9Yglo1MOZIb9CiYmDwNFoxZ64TqcaWbLNTeuBdiGXCIZ+RD1+6Bj
USZ+IsBLDBzeFrqnABPiKud5CL4PiFICPtlpVs92JAHvKVPYds5T0zR6rS14jSh6YDfmE88oKsbE
yBFFehtLTE066TwM16ySIxHb37CKv/A8HH69shdS1l/Dk3syv7hoM2pcwfi2dpiZz9VMKFCPdCyR
fPPXI6ZF6tFBrHNpdgAUMkR0laKbhC5sHn/FTB1dgjRJKQf/qeTiI2de9IOnIqFw04r6Cy7rNSlN
m6sK0+sNjOlXTCK7lvPyUJSKjgwEGUcV3KfZsy4knjLIgyUKXO8sbLD0aeP2wy6mv6djl383844L
aRIV4P+3yl1v/VM9e7js3RhARUp5pCKYW+PKNdlGGkvoFtsxDW71bmKqrOIj7x2pMMHYGJz+jIlf
LSmbMvQRo6ivg6Je8AXzpVG9RJ+Bs/5yWWK/l6cFyPHZIiLdYs1JFkeFfKpepR4mvRbDTrGcAKrH
QkHKVCrLY5SCbTOzdNwaQT7xxh8GK2tm3plDkPVneb3JtVLs9FdixJCiYDsZHvxEqUatPr3GGsWh
BUKQjkN3yXSoK1zQb82IqfZc986o6GM73QeZuMB1iLoJ6TCWCIojVXBfTeCEL4QOAdAIUk+PLmbk
oPMWT0d5blYU+j9HZUdJpviOmd6G5noFnUcFHUvGwMvSzQ5/557t8k3twrswBrQkJiMTs0I0QbH1
XUUO+zhp35tHNqz1wJ4knPzgtZjMMq6+Abo5NNlNKQsCOhJKsD/ZGHOzCcyMyL8Gsi/7r7QK7yz1
riibc6qKag1IzWr6P1XY1+oHEi2NleKGNkmHqPcgJi5yiGJA5Xd2Lwy0M2e521w7L6e9eHJsTpYx
nQ1cC7UV2rREQMKhiBRa2Rta267+tDHAdukpvhEDbWguGyq3IsBlu1KJUopURWtv7WTBov9af5JR
GxUaYcDFu7oYlZrkvOaadfW3DrUHUmTgv22l+Y8KTC8z1AbQK7EBW+WPjiHxx2J9P4Z3cQ2QFK0s
cj4G0K/pqkgUFf/t22sg8nIqGze6NyhnplIBfLkvgaUXE3rgOHKneIo1czv2JCepbYo6TeYb7wte
bKYyRtDkHSTGVwykHTjg0JoW993ppwUpdOuomusvybYacYHFYPJkdmyqdjKhbRZrBOyf+x17mWpZ
U27pYMsih69zDFrNn02lhomtfxZSVq1d2NLHvuX6aXBpOY4l6BAjRnkO7/a6X/GGSlJmgqOp7v3d
+fAF0dni+Clx+PRsyKhKhfUIUnx1eOIfmwLGovlUXK8DzCg/F9UpcnsxjiXBQCWLAkg8QWOKCjx3
2ppReeWz6031JxrxpFjmCMdy3JONCM3f21N4pAq57FS3Q+IvaFTwBp+MmdqVgivQofG23y5G4ltq
rAIm/FepgGsZ9bOcvVYffOTs/5xU6YvWQ6vk6PGZgWNaKwu/+574jfo7ssuD/pHUtNKgNNI9APRr
/4kO/riShXcZJ+3GuICsQGqGn1FEqE+RCQAi5ecHCbZ80AH/aNyslnCb9UrTIhOaJf4WAMYw4o1d
XSJFNSNzDIATT6rg5pGGFgB6d7f4gRNjMm6O/NdJCLYaREFECmKE9EDGz9flPA6+mVIb84FidNBW
e1EWYeU8pW8YNTD+ZhYGHmmg72wg2iqz7ttOmKOrzAmy5pkMPjJA7dfK5eT/zop4NiHhGXc5NjLu
TjDcGMLF+c1S7nk6NDoHPFqviN3+6plJvscmG/Xxye4I5huwQCdgdYCGRFBH7nmqjWoIffdf79kH
iAs2C59Pxefg0ii+co0tgTJKVp0JwgAH9/GgMCl6y31KWUo1HhvR4UQvj2YoHINIfGiCnDphs6V/
JB/tykPmoSIVVvUd/xrPjjtnXNMPYROR3gpqNxN7ynkHa+KbP9azNQOR4TVZXyGFOVeEUfOSNOrI
mLkE7NWHUNoiCDuSuPg4Xn/qta0IBOebj/RZJSQIsNel1S2LJJu6VjNgyMHqUbCHhIK27/RdPtXa
vsK5AA4CTXNlZrOsqUAc/8gdO6Y2gDOVHdGK5SKVImC9vZe2vGuXdGVTfKt4S8VAsSSe/4mowEil
Lk5WKY/rubz/sU/8+BiSIwHHUVo9i5TIac1r86/c3EjoqZJPyBnwPiYjmGb95fbwB+dnPw3clnXP
Aby1gEZwWrhaaCKO7fBzsMBiyd9S+gFXjUtAa8mMzs9WNoS67ycsII7wHNKtW7DYhIDBrOG1iLdC
h9skqY0WmK2L17CLb8jLO6sBw/fDpEqTAd6Yzyw+bKrmbiCvMzu0Ni9jwkuup1TwdS9roKBhcYPV
GiaPsO2DViaYjHdNGZ0NE3zxFr2VeEad3DpCtggv5XnfZhK7Qy/hkRRpgC3enNCkX2sgxP67cKBX
P5Q0vLo2KMUJlREF+Jteae4weWkjc6yEAZ2tq+HcKqKZY5ehOpZeeg0wK7odSBPhRAP/9izkc7ih
HXdJzDGQ9z7vWkOltH39ODJZnimVA2/VuT5xqQ0riIEvfmDqLmRDNmoTq2JGzRz9FPmP0Zu0Q1lI
v6WUAL5Yw8DM1xIJF/KFt7In6a0Thj1DaoGdK7neOB2CjnXNouZii0fziaBDRfGIR2yWNDlVCtoR
6h5WSoWrG0g1tcfHhQKA/qUFDVRc3K0f0Ldohl0QLvOLhBBo0ohD0LRpfYkaq0LTWRqLSy4Sybru
9J12Xm9TX3RlMqJYIAYMmnwFliAVfbzyhCZtQFwX7Vspk1WlN/xCufjJE3oqKITLy9mIjBbcvJ92
jO9gQ62+IAg85/Dcp2yihB+ViHtteU2jqEUJvNnOyg7aCIhbykbbxE1Wpu1KQDRLo9ItEK71cPNP
3IYVPIyjsplYgp6vdls9iS1RAoSyUaJf7RYrsWWLhHC9LiqVo7dz2rLCyPm2ivJ3aEBVdo+Yj++3
Il/c0wgkEuaVbSatZRGYQpJJ7MHTmNfavyqPV7be+7vefSxW10HQpgf50MdM+XPQqzICUP3nvvoA
VcBdpjo7/FA5LnTPXQYA4gcyWIICbvNBTyrm1veHk+ScLKW5snhmUEtMdAEeL/EFTby1lEj/KAFa
s0/RkEY4/vnKlqRPRy1CWbOo+jmerLs30XeNEDkx6QaCKS5Svk2BGLwKhLG39l/aUftNcUJO2bRb
T3YfNqKoM2WeIXU9h7Ssen4B+6xs7fCsZVfezRV3oJL1Zhp/6CJeZYfpPSGKdCbKaqjyYQRlEqLz
dX8uvmqrze16sYzjhxaYhVrpVUQxf5fDm8IXeQth2LMHTU7euYzc/WlEVslIeITPcFEKFmYDYswJ
iS6WAIx5VgrQxjiBmlU+5S1FW10WDQMJiXmwOMsQD778nKKFoNrVyQgN54lrpEj5QWEJRSSQ81JH
IlJ7v52z0bvhmHuppub96k40fYs14FqxmOp9wRT+yWeJgSZodrZXkDA4pdthtPLk9iSiF0uadRSZ
LWG2+MFy7jnqCbzL5P800DLSXhyxpLlIhb0etx8QqVPYV6aiWkGHISSRyWKnEz+phSXMa6FvOqUL
7cJsiCTx7XC20ZTcnKgVwYVHAjq/b53oPtfz8g5Q/czneg7YBxrHUdZpnyHlfs+3n8EJuMwOtW0R
kyY5UH5qc4NhrAKs8mKq8SVVErS335UlyBv9AKZuSVSWkoP20xWfnvkZ0LWQhenCuH4HfpNlD6g8
H4HBtCt+XRycij6kY9OI4BQ0maZfKQLkjLv3UMPtTPPEZmNyBPn1OrzyGwR2VBY4FeMEF1OGgXrz
5nQo5fiS8Q4SzLm1jHuF2a4g0I6ew9M4+nllB494VML+N4+3my6VrKengVEW+p5H2IAhcH8QHzif
lc5IUQpmYJm4+j/D9sDvIJ92Q41X4DwO/dP2CClV7rL7mBmivocRfgXSW1yluWIpJC2JLRQCDGa3
Xkztokt1kxbpXaK6kNDBFzKq+UfSsoRvfWlJT1tyOqqk3fW/97AZDkxjoc/z+/wyDWlPiJt3c379
udM/JB2cTxYL0leZg15ynx3yRudCwJQK+5YTdu+6pWZHhO+oNiNCY+K+ClGnQZQe7MJu1nF0y/iI
bPectXfh01mPto8W0AlDJ5Bj2vA7rHxh0W6N/MTXpctPsnHbY20xgQJfSVLb8T0+ljtYeTjVLHqE
OZFgr29j9oGj76mM67PeWMPE0Zs+DydT98rJ5nHoaBlZm3gmAKjCooI4l2b0+Ni8UfdTkGSYQXaU
7NrcQnW7i6d+LSSVP3Yejvh2M4MYwCQ9GA6cr+hwtnYQqghwByyiyxtolOrGDRZXjKi+bl1FTiIX
tTgBn1s3VF7Qu37jbaBkcp2BQEqFKnI5zgRiZh/rgsRJm0pYnMOT+cvW0mcCssDu6zUkBotrw0SS
qqB1iXoQfERskZBgQjpcsvBpezIYgk/LrHgLquEOYEx6s+H4y97odlBj4X1LUQPiw+TPEoPxgKU6
jtrQcW8ZjlLOuZUyyiEC99pSG/F6cEIG664iRleEJ6ZV+DmFW9YDFqmfJLhPE/Odlndwmkmk+m+J
5edGt5OV5z9DahDrWoDm7v+xbJErKsjPWJozBbB6n4IDGT0ygzdMVfYIFd0WpGeXiK2IMO4KiZt9
bEV4nWqZP0TxCfaIKmKTEo74fI3/GMys9T+JmsGDgEzkPXIQhsPPg9fH/1xNYCTa+KpSTInkYQ3H
h234vSj1R8BccKb3L7p5kF8G7SDLOLVdGj8sSYCnbPeUTuHxxPTCqVLrWnf+DpBFUhBMexH86o2/
WX7I4A99cHRYnI0VRSe7dRRfecl0qaHogzQSAzdHuVOe5P5juOxn5Ujtm1z+Dxe2b0Bi/UmVSv5L
Hpe+tQ90auLVk3w/SQMq0enbUFSs/JXqplfkgFpMepbAlR9yLt5KDJ9rbni2d5IwL2KOpYHP1VpC
KsialuLj/i7SIp11+a/n9d3fLK/2fK56Nu4P1KfuJyVGDZP6KlJcOAtssUGsBF7uVhNMhZNxTh/X
fHm6Rp30mWq9c9a3muel0cWkHuDcBZYciQCCQ+Qc4w22WQuR6f9j7IyaxbljnYcP/7NQpo0FFrEo
WvzxcD4Mf4za3AOfGiZi3+gMJoR0xGL0Py96/dWeL74iYNmTUsqfkJBRZXlTnsg1L2z6P598H+SZ
2IBFA2PsIbQXaE1EDQyoh//PUi2lCnnrJbdRtHyR0qFJM6nE/6+xsH6KXug1ecQoXASZPotSMTU/
XGBwrA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_10_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_10_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_10_0 : entity is "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_10_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_10_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_10_0;

architecture STRUCTURE of design_3_c_shift_ram_10_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_3_c_shift_ram_10_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
