// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:26 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_10_1/design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_10_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_10_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_10_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_10_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z5SQIKpv6sLbtDhBWpyIEqSxo7a/KnnfhERows3dCShKK85LadD7EgamHUw/+6ySYat5YUkauBUU
yeMTWYzPQSpTZk/07OlZU2L42lzQNftkrBRhz68j81vVkLT1blabe+oXvL7nij+bDZ8at+5hBAvH
9+cVWjPWaPy2yLMf+UwvfuKiDBcmaF7XcivClWDZAnF31h+ezlaL5v7lZE57OEJ8EuAfhH2021PG
dcr3PqABAye8RBiPZopAcrvg2/o+eKv8iRZtbZdM625T2+jIf3X9kTBksSolOrjz2s5inxGflqGs
qqdDC9S2AWAb+bq1dH/YA/GfZy8U4TQhs2/b4A==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HTEqoD7oFN7YxVdx0ekFDtqVM2IMCfbLVZlYAR2C6S6qVn/L6X8HX7Q60MJziA7NgQoohGUYrq/v
MOu0/3nLUCStKGi8CXwIyEaW+Mtx5JZ9M8RqdnTCO2scN2Ue9mfUiX5RoZiyeJoYQn5mQ2jEtkRX
0vz3cluK68sx3/RIzBiCgFUVVP0UEAwpTia96n9O8wxCUKcoTQy/lbna98bVAAy4tWsukdq/KsAF
qXC1Frq8DJXhBG6WIMHtD4EYIW/EbRx4pPLa54ppw6XB4/hBlSQg+qCDrhg9J2+S49V9nSMslAV8
yZXsuH3Ya9lUoT5l5nlvqOs7lppHPkN5gCbGmQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9696)
`pragma protect data_block
w8E78UrTp/23yup4M5DoDGBgFRrt/a6h8wzF2w7Hg4zFLxKhq0jEPBXgJyeLlgcpf6JsfDWK8H4G
rYDNPqUr62DoLrVN0LbqfgM36FIOwEtWe1WDk8wSZaIMRP+7ldt9Xil9Fiicp0J0dLWddnkAKe0b
30pPXIk3CCvyw42e9r03/aAOHSvjwlecFoHj9z+1y2doZnFOOmj9b4/Pfs3Wus6ywPBoAT8v/E+M
+Nopi9wL43zQqGGU9ct+DgrfBlute10WiChu5iRGO0cuc+HyILLwggLjhlnOpugNMzvdDK0/K7cs
8m2Cpl76PXdikupaUZjgQWglBdngqVB0PY7YWNbBshRql3kPHfsEGQs88voHYW8JhlHvqVWskrIW
ZU40R6FjKuzSx1gWIK+m9Ih6/0M4uDWg0YwCIVhcQ5mbGgva3MdZoOyIG8Ng8eVQ+g6dwA7rLn6v
7QQbJz+MqczX70HWL4Q4ejb/gVgzkMdyYCUq98Rv8GtAYn4DAcETNWK57jo8d06lJUhrkNYir0CQ
7pUVTEMOGE/zJAGJ60wUBmI0Qg21UD5ADYiJ5gqbngNVCOynEBGxgggDGbzQIW3nzTeXw5v3hW0S
CVrAN2GRsXDpsaSxd7vFhSQqPsbCvABm8I8GUiBUtbUTf6cNuuUZcuho9LyimwuiX0EM9LAVE1YM
C/nPL00skWGBwZH3e3ICdgFWpJ4UJzR2qvc62pAydgC6CQYTIHJhdwZ162WER/hU/A12lpZpslfF
a7PgI/e/K/M84f58uJ94i8/x2UpbmJ0Y5xkovshKs1eX2rEgciHsIssBXM5KM7J5KBxS2TIertK3
pATtV2aNPGFYo7QX1LwHQTLwDMtpo6TNJqDa00rmWAtF3ftFibLJNmV8i/DiWWfisPcLKdAAU1P/
SKIWlOkLP8MJnqwYywoLyg6AbOsICKK2l+wAQA4uuzK1D1KAKeTL48EoJX1Y+xkr/VXDQ49EfSK5
p0yV9KoYhR6eYafGpBbkLv9jnQF7Dx8WJ0GdS8lhPrLUMrraVDNhhGhV7GKNQSFwsV7pvz16jVRB
07zAAAfc0ldZe/BiIgYytjbEQbd8SBJHXSjGFPRalrqudWh7ueMBj01olK859lP5ITq/9U/XWRQX
MI0/bLm/egi8S8osSQ0NVKqIvJ0R/UnLo/hjnNsLRfbiUUEfJsR4uGlZ6Z0OAtZoXSFe0Nz49FmA
7Xe/6YshJlH63mqcCscuWPfchgrqMp4WrcVGAnrUc6VL9yM5ZgoVR6KxvPAxRP2Zz+V/R4y8swfC
/GhS2Oae5TgIiyQBhvWQQJbGI7+uaUTuozozvhAQKJavD7Rdz6mGOD42HsINgCzGXLNGcVx/fxh+
33N+rQ90L59gVT6nUlorYlnW3UipfYeBO5mD5h1q+jaj7zjT88a4cgJP/sQwYlAmgjS1isx5P/0H
VBNFK9Ljc4T95jZiDD+TFqDEFJvAg+qNQLsspV3SMfhKqlV04UEL2+GQonuGJg8+fyfPQND4j13V
1T+ySFUmTZ5djX7oQOj/WydCyvjkqkb39v9nJGhz4XhTsNoQu+8vTmxZGhahjaeO7bR3EAWoCJ6N
wUkrSs03r8qo4VuntOarcvQiFPioZK4PZ6eWWkwQEOEmrcG6Lh15j6btITkRS7Of+hnTT2ApkPKE
IqRWb00O6rrPkcVPPPg8oN/36n/rIyqwcNjHNFfcHHbbOdHUOjvRuOFZjS6khKUN5HQVPhbuiJMX
gh/4GBacfKu2E2BGaugLA6zzyAALQDMCcfP7INvoMCG/jkzc15UyA//Pf1zoVXOPmb4GZoOBxdnn
l8evPUOuc8Sj4oC89q5qjykIcnFgPvVHJ3MIJEAcnlgi6MlN4nobdJA044UB4+t6r98NIfYs01et
3gBgdBGwLAyWKjYQeTo8jOOINbuWjPVxWA7dvA9jGA1ko66Acee3AyKyRNimNE/88PlvNTNnq6ts
kxbxjp3KIUh5z6qFwNXOAvC4UfB2KUTwbVat05YABgcukHJTjh4wJ8LTSmQtbnXkJTR/XWOnJjO4
bi7HsF3U8bl/hAIiVnS3dIHrq5Q0KQ5ClFw+LrVDO9Y5KFRZKq+l1xtUKKah7cZWEJvvREeEmIxp
CUCeL+Ef3xfp1TnJYKhddpmXc+lEQn/Dxlfmt0uXUJvb+jNcJt7xv38ntSLK1izpyCVTOefPh6iE
ztPsW5LR/iCVKr/ojSCOegY2C4KURmNG7l9/cwwOxaZuFseHQUFmSGspW+22ow32PjeCy4Tte0gL
wJw+2gJ8RlFKwh+zX/eSjwf3UwqSfVDf6pPG8tGJlg7ku518hmYdHrscEMkim+bQKJdkmh6s7B1f
C5TPAsL5Y554eeVH6ddCFmegWkAaNO+KgLagqfVK4C/7zo5dQJizwKJUgkZAZ5C0FiTF4EcbksGi
7UoIXzSVojlQ6ks+UJKSfYn9QVPQOq98qXedLqxhKc2kViQIhugZGQ7re+hrktFEN8miLgfNkQcZ
1yPKL85bIzWtJkPFI0qUujezYhTf/NxU4sW79EEZRTGLSOu8S5WlchjLY2NbrFLa8hxI46wLGzfB
sZMMzz3jN7kuGG2+77+huPJpv/Hu4+U/l88WzQl2DG4KsztcRdcDLe50SFG9c2m5qsBJpj010VT8
mhPmois5XD9zlMMvLMcVa4z0pzMYNeikRz75zcEimYtTevvbbMUbQi/Tn26OMdsQPZ4gye0+n0Ce
yhWmXq55BypUHfc5NkR8SG1OZ1oiYXkduQq2SCxbntG1dwsxmEgGyhJOOZ3gowVPOcDSYc/YbkiQ
898TC0lUe3yb1uOunwGeZ8i3wJ/XOVX2gPE5TOYUhOmDSykViLDMPisS9BwlL4+gI3q45XAgxWQ5
wlISjuwq6pwMYKbWmVbjqCGugiqKHKPmsjB46jmk81SY8cNTsrzO3mBHeDoNplXBd1WGtOlXqw1m
jHWcGbX8jA4qFSKuEmsKktltiBpRw3zxrwtBS6O2+a0MP/uYx3VZ4suH5N7P67X/T9RgnwOWLFcu
qFEPMt8b+9THTX6QAYR4PB34oDP73eEpmPqjcvUr/e4nPgpZVrzBwgj/Se7/kpp1QDLAffTpA0vn
xIwysA8Xe/P6ctjjR173ZifegHDH6hCmIelG9k2IBxYreRPfXVcfxYyTsifmmWX9B2hMcQ2RAZT+
0utkR1dGaHChc4JAtzEu+Ud5CdaKK58pkNI22vm1fiaCVOJ6cytfaq2KP8liXQH+LEUG+pCl8n/b
UNCpwEbEmj/b4W1N0HPN0AL8AfLu+NEIN5T3QP0xuMT87Jmq4Yh44N9PN/wrx4d+yWgZuoUGdjgL
sj/hx/rG7Vn6WBEoIE0BSK27nQ0/LoWPTOMJVE80X6yDJaPIen9vgYVgY9Jc7cUNGnIWlzDovtPu
o2Hxo1IPjNxwPClFkBOi/b2dI1j8HQKFIPIN4A1l56XSziXG7+Rd7d4EQjSNcGsY9km3K4pkMY/4
qb8/+A4z6PX77V1GKPPn4Cs/EWDp4L94QJL63dwWtE+/vy2Ck2u3pSNl9KST9Hu4eOmJQdni8Z6n
PJyizKqN6hIiJRoK4/NSWCqLwLbysgYGdi8jgsoXtZHxwi3a+UohbnYP5NptCGz4H0Hjlhq5/S8T
EcYUshUWq2RwWS4y9uTkLN/7ZIJ09VwaxDjK27E/B4mmTLOsW/XuokAO1sucZaD0eo8tQPUzPNcL
sz6CfhetoN5qw90OVmqqvDBHw4l+tWGjuWiqrEaiSIue7bfurySbpDwkrd4eBUgcDKUGpBQ4Gp3P
nBHdVbctw0QBsihNtIUU4Oa+lCuOqZCJ9iy7RucnBHPfwx0W5Yb96Z4ZB00rEGcTKg855EIeK3Rw
8gy0bcS6lLMJrj2bVyZ95b+E5i4iXlT/Kpw02pR+WByXqHdWO06wmX1WpeTlK4Obr+7pFuzsaEyf
vdjPZiUDUw1CpecKBcWR+J/qxmTTwnIh9pZbxOgA2wBzaDJQneBI4znMDFgezD092rPeNx9jFa90
trt6QMaHlBdVeh/RYOVwyTTu/JSZuLovtOGpt4YHb1zTvfY3X9cneCWVHEs5xwiR8o/niAix1Eng
6/gT4cmqrtF2oVD5WV+9T5yY2aR2JhvOZPdx5QbYOFjyiv2jUoEbTT78REYfmzU2g1PtSMDYYyTU
SxwMm2+PR6TV3N7iAstRlUHDrSVyCEY7k5p9mzXWU5ZArb8nIJ74E3HO0h9WyGcvaJt3dOY90Z4Y
IJkbgUnRLqydqSQfHJgYhR4WFjv06OIjedqTGAC7gLRN95e3jpsKKnyxZoe2bzIfTpuqjHEt8L3w
zr0m6fxNy5Jzf/F0zOxG78ooeNv/USC0/HFZBjj+mHGXT82KitrUJZKCshegLYsGP/W7Pu3i4BOW
hCJqx3mvJqCUadUJPM6cG9v5uvH84hcexY593ooPzxUKgbpmoRs5gZlApzKabdTSfrCgHQFhhawB
qfdwaOfjy+kJD4A/3kGBTd3oBvzhmWB5DMHH3QaAmnLZ+I5rTe2UzssrWxN4A0hh+D2yZIYGyO6v
1K/7zCPyRKCMkGMc4bBrnauNtvMMmhUMmvkbpvR11si1mt3KY2tG7MFor/MWGm9uNAtBhEnVm8cN
mpxRhEOQENA/0Pn9LldXZP+oPaJQDcu+qCCS6Xuv6a8FojS5/JUJDgi7j0V9AwXJH4QECWF2oeSx
8ZvYT/hQVoomk0D04r4Rq0Kd/5OhRW+40MJ+rMfM63fDgPJ4KlJz350kZEE/Q3v9BjzhcYgOgn71
GTVEiIl5p8Ffsj6GstE7zMGm0PCwdGcmx5xWuxRkbTUe85TLgUBK5t3I39kXi62j5hbXyfBYYCYT
mj3cNY2LfpaRadNXIIe0ft0N/RhH/cLAuVh1jiClEzb3SnJbNmTcuin6NuYDACpBaVgnNlnyr4WY
f6+jIy2FUWiUAX0wKtz7iBzvVL3E7qfeqk0B+YtOpiZ8aIxytdqCtqe16MxyCOCRQwF8bplfPXx1
nY+9Bg8eJemq/YudNSwPvVMY8BKp6Kt/6HY6eedu2U2b5RQ7VHIuzEfA+ZFsLymSa/tRCJc30yXM
h6ojL7EdbaHtFBeMgYAKFEj3wsQfcKYKdt0BFGGUi/ck2TfevcYTea7nq3nvABD8ZGyBV6gTMEBE
zjUugEAJzMXqlnrXeQZKjHjotjnrDWYMr51cY4dvD9lMPpcJ4RJXBli/pQ+FcCr98KBzgR7FxjS6
xGT5Oj72armVHJI2XPmvuGI7Bq4DH1XpP5xwsgHyq5DVQLVH4jj72QzKoQZOAITkhoUBd5jB2rSA
eU98jtqDheRMwK1oeNd7NLYHuOZ6RftrKWI71BxexPS3njp5pjI4Pq7LJcxsoiVYvN9KRBN+IbBm
hHzaM7rT+82nDz0YXZL4d1aKTbW69d4/OO8Dw5JTPDCH2WoCROM2Wz9S4GSvFSiqz8pSo9zt9y2I
FR+ir3KYq9oYgHTzLW7j53e47Hzz7z50gTvCH7A7KFnlPtlF6z6SBGqGlMJlq4K8KcqT67X8O9lX
1ndJ5kpXXRLmKIZD4v9vy48HxVEvBED4h+MqT/bI9GBMRjmnlxro3JjPe+am7WiubeddFYAPSijG
2CdZNffe+qvUgfMTXI8aA3RO2+ydewZg8UYMzlXce2iYO4HNgZ1BpaDOb21wod6vfU8Jj6GW/Df6
Ti77gNkwsVuiktOzkHOL2aVM1EdTVc8EtyYwpKzedO0GqX4T0wQBgIKKheqO3W61OJC87SLpZoNc
ts7Yb9l/Tj9gis1UIw8Qtz5/w7d9DpIEEVAcVEUhY1atrTWLtS5iE3NlXSXtnAD4b6w3pCGWu2IZ
2Y3GNwbLb2qIYuN8Yn2ggMAzuoVUM/dXzBGgaKl2yZsE3wwc3ssDNRkHci5AO80EJZ+e2UC56PfB
F/qggjgX41Pw50Pf51o5e1bCEyJnnVAnVMxOnsN69Wu2aQANU8EE74Y/Az0fO3Nm/edgughWasK2
WLrFcJhZe3kTAKP6Tk6IFN8x9jGukszJm7cUfVwi4RM+SSUlUDQekpCvAv1YU8k07E2YhVFp5M/2
vqdN5Jl2QyK7/acuYa4ojRGx2G32d4ieM8scSuLs5gkUTHqFBJfaw3Br2t3IwFLl+TVsZzHAvypm
qHXOLpT0I1IcGe+UjZl5JWBoM3QshRb7iwEU0URWJFJaDcSYfk7upMp5oLUDXLeCtbYhytY0NtCt
zPdm1BvrbiSyzet8/mHFxnn8MIfeny3gHSdg9Cj8iv2OvuP8H7bw8G0SaAg3btIkAWqHglVjMkYM
2hHLUXc52UN5a4fGZRsL5NzL/DUafzZpvyRTc5IjYXi9Kgez869oYQnYBuIvOOW93rzVcYwCNYu8
VZYP1x5PyIFT2bhNJMgJP4hLmMoICyA972w0yd+En1AxcFs9qjkg/W8dkheQXqfIpvd+/wMrw1Gc
1h8t+bjLoSl36OKHygSJcBfcOEer+dtqnpmxV1U5c1zjwgOs1QRU6hCWgeoHZ3E2tDT3UGDbQZ1/
pEyutP75xZXRgQxlwG8gaLTBQgBHYq3Xy+exd+6NDBxWWagu6BNumUS2HPv9i7BmE/ucVwHWoaiW
/CFdP8pt8zxKOOb8ysH1ozRrO2FGg0OdfhhqAiqAt0uHFVqRwv6aFX127C2lDXyCXrzSCu666H1k
1XQcZOPoZhSZshn0VQV795hc8AnKMw+BSaCgl+j8DphuybmD4MYuOyjv2J052J07+fGlLGVuzPv3
mz9I0avNl52wk7chjSPpu2QSfHO5YmAmnnrCYaAUGUzjHaXzDp375Cb6zZE6nh61+692EMVkpiZK
NS/DX8c89tSfZUGa3ZBYSPLv0HtMt73etjx64R67fxsywNf8wjFTo2qTAY/PFr3UpzkcMrhhNt/T
G7z6Qk06k6bYnJeIwxsUYwlouH7uQxxnqQKN8ESyug3zIcueo/CqrruI4lTJgcl/Ii7yR5Qsz/nc
P/cjDeDVwUHY+LxsBnYO3ixKGi8GAYQcZmUX7loTbRVD4dwQ6c+bTkiQXvH9i3uJq4bCIdyAgHX/
S84yER+Ye65eoiBnNEBPWz5Zn0nFQTPjCK0o284KN7uxFauFWIisnzn+YwdzUEoh8ES+DZI9+W+T
GIudA3HSnbh+oJ5KqbIOXZDmUNp9yB9RPekEna3ElyZEJ9z8ng6FoqE6E8PLgf73hWef6mJspUhz
m4CBgxLrRJsAoc+vvIA0QV+akEe0efcOc0iw2glp2m6AE7eGAtcU7BT2YF4Y/5Tx0/XAbwQYoCSL
GSeHc538f92bn0gaGRcAqjfTwDrhO6TBk+IHd9d4b+hdFezvQJD/BZ8B43SmqklliO40oJXbA7bD
5gULMFo4fHQRwKx/xsTyXMOPBfpDSubZtmRVkgHCcXDnXDwWBKuvZPJfu8Art0wSfqLDcR2ZWtdm
f4oqvculFxwIlkEId9YLXrXOUYe+xFN7koYX69Icd47KAhr/SYV6ZoDfrCJlYpaPZHmpEmI5YnEK
aG9+WnKhHzgMRuaPncqKaa0pOgEfCQUVzEhxuqsqcMfgFVYU0rMziK76Tf6XePmeP65dO5z0bTIf
p4EGEKv6Drl6ZJ/dgOQSALsxrcNVbnapTHmKAB9pDA6yvNvGxcdMJ9WVhiaM78+19Gl4gmySG/Ex
G+1FZs161a7zn5qn6wqjyZTYn6ER3cvLXUiA/XosvaHukhZ9vg9Bc1MzHY+jVjWr8a+BZe3d3NJd
Z6mG9z4RG6YN5Am+V8BQVNUbnxxAQmbkvRR3G8l4TTd1CpmjeLTB4ZmNXT/0HT6zCnKv4Hk3jpQA
Y8trHO+C/JrAbsDfrq8kiZC/gf5p+bGzp+7/wjT/oCQ2x9QTN8Y1G8sro0qEv3x+tNEQvTU8WIxn
kPnvKZdtKrUOpfeU2/lrJC38HbaKFqHoegSE3WX2ZTfmPcadlcMwWFX5Hq8Jvz8w9rG68XZs8Ngq
hVdcwdVWeFbCnNCpDuD9NcCMZQfslPE/Vn94iX/lGwEJcuAISKWNhXLzfSj+iwg9Cz5+givXkrZQ
RLIsfwhpRx9UJwrIWXDGy8C5gnMwudoIboZhLhUKD5lr8XWao+mUSLcO5dlLEaDtZk2HRwDYQg92
3CHY7jqfezDkZYJRA5mXfwlBY5usfqtvw3cm3IB7tkeb1rso/HFVJWGBHt/ZHaLikShNxuEtQQB1
S57+i1sfgInEb0v9n2N8Y1/FBsORYHaCqFVNyw4IR4OxcbCg4fPS1s8eI09BLV8RTruPF47wlmT3
gmWGsCemS1QugSZC9N7tSN1gWmzvmw+eyKd5nHDyLF75GMU5BZZCSS28yYboVbGXq23H8NxEDdCc
k+YOghAHOE1zZsXipeyZlUMOAGVZZJ9KDNbMPHvTaRKUGZH9N/5KGxCFRSa9Ea6dC8YZCSKfcKHm
44HmNKDc2L/nXITJZQCeboBj2ZishWNt+8dmoJy4jksIAWqjO7HlIYfgMiSCUcnJzW15DPMTM/h7
XEuWmFxbd07tHi6Bv3k8iJUyXQnYHf1oXgGg2iTVTOkysFnqADbLtk+rNeFjZpQ/05M6svzGXXlp
z+EdqjKA+k6ncytDbOWTRbXd5jXOfjWAcd0QiWJocTZ2xxaF4WN3f+bxb32hRt3++DI5Yu2oQy+3
cTCED7ASbpT6f/FcEsboNUXOr2UJoOw/Np2VRyf2s1A8kfIXLG8h4T1P1ZAwqN+fF4bhoYKBwzrk
PfrnkieSOVs/PTv3G29EQZsAdUGqxwWV5iHdi+PlFyL/2VSnszcQcdFm+9SvDUZ8Nwm4utmGAeh/
UiCMyyRAkFRymBciggYgy2jwmJX8fOnqXyD08TdUSry8nH4IepGteh2bnldD9NKw9wpfkqVUQJ4M
aoema3xSjkdIdg7sQN/P6VHQyX0st4AWnVf3eF2MWrxiOrN6wOA1hhuKa5xZKMmeXpk/rmvHmleE
nuYY41XqZPRYj7jsLzojn5GODbSpc+uil11yjHYRrx9FmiZfb0O74NmwYazAYXu/bOHeIIn3Fklp
8Z/sdwfX/MjdZTZIa00vYxEAGdRYBUFm9kOQpZ966Fkm1OD45ZpstnrWt6YpJAgEt3vAtl/VlzJZ
/19JqoFMRRw2NDujJwtoNbVMVDNI2ajzhgqLZeFB7w6OUEpsbwxhaA9fE/T1LN0KM1Q2Ytr9IKEQ
eFrMUpqSGOiMte+vjm/1tOSwukWUADNJSe77cergyp6TjtFwBcB9ARc32kGMryoI4WyvUzAq1aZA
uKYsGN1Ib4D/Y/iSlYCe0xkOdNABJB4JWk6wyak7jSUk2vvSvVeympgv/Fs2FU3KR+27Vjxa3OMN
Kman5SSWPa/rkUH4nRJdUHobGjgkGv2KNSkxFQKvXafSLqL0GZZawVyIDQzI6e7gZc43s0ve+U76
mCAGwcvNJokKi2yv2N+o6o+SSvMxrjFMaJpHC/qSy8wugOmu37cWrWNtfduq+yyVIhNZ1cA9Fn1/
h8vC9kCkhtI+baTHCKqdzQMWme6myx79EHvawUqaSQ0YbvLvBvWNvFOhNg6AIy/xCkBiTrrnIVat
EhihEqFEnWaiaQ9zv51BH1ChURUvVGEuRGN/LQ0ZVPiVWKYkGh+WKcEV6Ynh82mvXqWqePWOGEu9
wIJLISZXXl3489CeRJvJa++Mdc6CMORLq5TzkUy3X+tlpRpy2xI3GgpUesIYRqLZSeGji18Vvmpx
qrg7kbIYfwMrFeroakFBPLG6Dfg0fysAoHa4Nsa1LU72FaWhi/sQZ311PlAp3bZT1tqejLp0ElOg
950uk/4+/nOqimKa5k0o5j4JtBtkh5uyhVvUeMZEN1i+xW1aBwiK201uiovMYydyHQIT/WT4H6GC
2XXXWnPcOfdfid8J07TvUk2GVMhedIZ59OvmMRZ00hPPxDKVZyvxL6q9AC/9Ovp2Kzo2aeKJo/S5
g83gn5/OnMA1M6pft64L2LjAFqfF3DWXCw5kpWprEn38EHwEfDwVh4ttX5rBFpI1bnFhHinWpWJk
Y3KfYkfXUF/3JPj8u9Yf6L0/89pr0P2i+2H/VTTg81UKRdotrqQQeRYNoEbVZ2C120xj7WrIIoUT
Dt5MFoZPxslztLzfPdxoTMPP5//PfDj+3Y635W5wgKtmDVkNV+/tEn9Ehl1ikvUD4ne7lK2tcj2u
JllIvaljbER6E6tejWxZ7eT62i89YpyvQ6VQ4B7dhdbxpCwlIqrovCJgphiqk6CIrt+YqWZRoLlz
Nn36LkHNVuTjWTQV0laTU+FVTZeIEeE1iDzoVFCHOsRh78xByyFQEvW5C4awNxl38l/PNfiVYtaX
ifTXXlcSX0f4ELyWSrpcWL2uulOLupVE8Z3BOcZHvoxXcrhrFvaDsG2tPDX54muzaHoJUZ+N2D1R
WC9lMUhbAIj04yMzli+G+ANAHKtHcgCULteN3pOExDR3bH+0BjyQFlfCJw5YT4+4HDa5JdH/pZL3
DSTSVtp+sIPuW63rqa78+NIGeRyuYDlmpTN1QzcFT5UnqYJLz9UxShi/xW0zSaYx1LzAwdMRMqAO
NW3XouCygNkSETeqRYoKLqU2muQRg7pBMsNnkg1g0uZpOp0GmfzaIzzKntp6b/ELtwfsxnJxNCTs
MS6MKvYGwDW6ZDA7PKr8S4Ha8/e1sqGI81Rq0NNLBgg92C4YnIuSOM98AY5RYHqmPt9khEU0dGcU
bLD0hiFDmxT8J+8t4sCcrhdkhildRM7KazB1sj7i3BPwCQsXR8ftDknJEpcBibkDK/ZpeDbme0Ts
yBf4eAWRF/yGVwf5B1Tec2Q95zdhXVfCObhugJEbbwO4J57rwZBQmVOilF9QYAWcCYhKJ+eeUeE9
aGUdTa5s8zxupbBO/Hs57BWYcuznhCsoyTfP91gimIEJgSJeE8/aUxLs1hKCdxCA1f4CLfXta1Nu
j1l9loJHRU9yjDn2fnI/dMzVlmTBVHdxrXWchYD690V8ucA+OcGC+zRsemKzs6BlTcwmMtcWiw5I
hbY9obakF6o4NmriTLy9ms7oxHNDENVQzH9L/tsi3z+AbVl+0MP1BSrEH4tcfoJu15oNs8pdXI1w
MUUGMXOyuj1eeIdQpekCPuHtiHF8cBvsd9oLx6TEeTY7z03vfJ2mih+3bgGe1oTnvwn9rOATpCoc
25Vrkvp1F18x6xSe9wvdYGxwpkRuKBewJ4dkXm+FxbmGnxgRmKwrm8Fw3CgIeuuBM51TSbs9+ONK
krRPQj0W/QxClazjKg+XNeykYE4+D9tnfkKiQzm+FPrSvm1wX8hmN5nanEOmKhMfIrpbxAlWq/p0
wYFg0qHZq8+1LBJcIDEMgKPU+l+n4wYKp9Q2wvbDgHAj5jupD57CTL7NJbdbATt0ytENHbE2eFx+
Wtc93024/g3KKR262MSNM675e4gB1/qmXNk/A+4Shr6Cx2CDH2cNc5Bih+Hx0pJrFef3vdYCYhke
DkUElyBLWZcdk+t6lXVK/6NfoVjEhwmF09Zpgi3dG+8A2p6aJcO++VLMW/mQwqXsq+0+lL8bLE2h
N1z4xnbdMza49pL5KQZOUOqwJbjHFu/Z0gmOc8H+Vdvp5y9pyMfjVSH1jA3PumpluprsyPnPcK+G
L456pTfybDPcYwRseIIL7FmmlQSMpw3ArZhERm4rBzDXYjpy8tZGfiRRM2u2zlkSx0003pXfF4Ps
hSNCFwFv984NJsG0Gk660NMfuFg8NusdC7/2kiFkeB3c3Hg9mcx7+6x3HyTeTfWnFkwj7+ZFZk4H
xGIJhTrnLNVEwgXg1mmiaIvvRvp9WwZIMwPxZ2o/X+vMh9A7wLjEwGOCHb6EPQ2mzj4a1c0SkPkV
TzW9Zdep4ZUNi5yvFvqpgYKgfcwlOM/gGQY0x5SZcf9pdslEpob90O5Eb7s32kIEj67eO+djbK1J
vkou1I0YQODWlB+LgPAcM0EH7NWJ2FLa8paumJHSxkm5xGasy286x1rmsD15GFkeh4RewKsrCSLf
eRsPjy4mUM4ifsGVg8UsJTQCTdnE9hGxR9V2/An0UpKX45R5euSKO8qcjqPt28OCWhr46B2c19Eb
s329bKxY+NjQtXvE9m5jEr//hrOy8H4PhRb0FNuUVlaM4NEXljMl8z9HZ7oTcV2zcv2qLglHAnlp
gs5zkVGB3oI2BWZozH3XmhN4xMjM6Re5lxtKaWcTCtQAVWPMA9aggScBL+IdQ+pgW/4Xk1MOq/z3
A417g3io2ltn10d6A7dw7eo3YHkPfUGxhzex2G1YochtJK4GvSD5TOXPXeKXUzMbyqHRpXuVrhT4
7AuM6IcEbiqiVyAkxQf02VhouYiVEfaJ8APS8Rzd4ZVCzMjP2mUriWAVAUoH4WHtVd26W5Oe0WbZ
/WXCXw+1WvbWxdd/QNTu1mPG3rNV0PLa/9VCFkG/Z/2FObfh6f9Mrk+VfiF0ekS0dXKqcQCjoQTH
jtGWymNEfTXsGTiVTFoyqpqRHnYXZt4SLQeZv47vfDlSsctW7PMlspBmsKGgUMWYRIxQw6ayTlVF
LeVg8ri2GGM3lAEUnmjaH3NIy3PsqarNjpFw6fhSSm+mIUT/np4TF5TaHtWR7SRngU0ow3+Z7Mtw
5NgjiOSTjAv3BwdfmbQEop/B8goSr/0PGFje/KuNG0zmYnVDJ8+CUS9UHikgx5rWwt8KsWCpbW9g
1QdO87kN37IwtWAf+x71rXiNmIkGBIZeoVkWpcDlXvauLt9EEnzb9JnTCzFI1jaGE6Z8cxiiXQkP
dR3CcGm9mjKolk8gK7hwVMCg7hWG4NPnkcYiikB8jUTk6f2BO1lZfy7Yz08CYl0H+Mam/WnG/gkD
tNy0DUo55kA93clwEdre7DEKrJbF6WgrNQlz/kZKj5jc0EN0TDzE0rYUmGg0ifJV77emRRpDRdK5
AQHcSYU1
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
