-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:44 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_11_0 -prefix
--               design_3_c_shift_ram_11_0_ design_3_c_shift_ram_10_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_10_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QcE9JgSZJXlEdJa9l5/KM5UAOqKlcDvjOrzeoonFUtyvaM2j7QpunRFdD+nqKIU1Inva1SMB8nrF
gKiJvHkHyPRrcgMpXzTdYW3AoEFHbPojwVnbf/Su2642m4HcBlU2w6q7CBex29vK233dPM49gTvL
cCjsqfF5ORhuiFJh/8Y4Qm00d9ykN70WGas/TKecNJzkSyknK2KrDJaXz143djKBIW8o0Mw2Uk7X
u9QdFSxR2lHbs2ZcYl101/+tUkRm20+ZB2IFoubpRsfyA7Zr/1ruVO+cCC8bYU+935iCDHBrBH8z
gcfGMlWwQLEwQs0j0AIiXXCzbBIbg9jzFgAdRw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jEDgf3L4ROYKqe9m3y7//JBHbzABVZ2xw8rr+XLkcU6/1i/Q+CK1RrUSapYT3hns63S3dguuynOy
DYl6e2OTE/klNzp+twCnQthUEs4Pi41pIRPX9I6M+LVPP+pxJUfPeYAZhW+hSzxOFiasxpKmNxCl
+RmQ7gT/546CTxEn1w68kZXGG1W18g1AfLfg/nwd0jSr4BYTMeRE4jhnhSpINxIL+1iEWeH2e37C
laxtR9iW6eG2IkceRmemrmDcHn/Wgh0+jSmwlDtgbODRuuDkUu9xQ11TMt8SFzrXR3jPDxHZnvcX
n/Oy/YIB2p807TmxP3C0h1wHG/RyvKj+rBdBsw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13904)
`protect data_block
jrzHwmqzeP256HE93DeDAKu2xnx+6RTv3gd3Ymsy6so9FH8nI2H4+3scPQbI9JAabHpyKeJ6xg0b
qDcwZhQBpWbFdQlxZncKr0ktnVHPqxd+xClVy9IjgwywRcAj3TJUwtWxR+2bSOWGFp2XG3gaFFjI
nMKEnNqg0FDb2i3wx7hu00YElwyYutgSbnRpUlXBl3CdcsNRmCXxd1PCZlG7zK20Gj/+kkdSjMG7
eVwIoRFAP9D98FuodmM6hSnWDUPP9P7dTrqusMmCt5bx/RTBtswYy6/S+ajASEgqqrpqSh5c1KTl
OoXbRiFKbS6X4hKWuuPOKbBoiZTbchyUEzWchX4oepXAQZXAYdWXw84klYKnIQKC9gmdom8O2P5P
fdfZAKgW3EZ9fqqsJVe5bROxguj8mZBN/I6eAxCb97vWe+bKbewNdsRoyvUKOl0S2TQZlGR2DiuL
bWHUbJrELlgVOwLNmGuePOqEpDW9ljF/IQqAA3OxuDROZAQmp1quDNL39Gwelb0zp6USc6Ybu4p0
YVoqPeWijwz2KM4azh5znOYX6botPz5WT5G/nEaT3zLgw661jSy6kF9ctTsIZ5PZ5xJRr5y30KNq
4TCPIPw0iSzX37Rda3kZ9B4U0l2qj+UTmtINM4bxbTfKe482tcYutS7Y06k4NHsaGl627OyDDeNk
5oP/bhLpUb40LJ4x7BNrc82Ttyf2e/Sng/geBgDvhZH8NEz5WFOGPhQf6akVDnLsvz1lwXOcHF8K
UieWuYvEjxWHwF+TlSBxIdKMNrMlR9avAJK0g+pwmSccGVIeFWbiJdQ2WjTC08CNejRAuKSY96Gf
fM+uKD9hwPlEz2c1LFvWC8x2hA0pS6k1Q435neO1mybCaSFFMs0jq1x5RsRpUSEf+VO73ZH6QyCw
RV72McJWYUqqvaKbxhI5zZMTc3UfAM1+B297lb8+aAvhJryoJrks/8hdrEW5PRj4KcTLdXlrce9+
NPWT8pDzhuVXutqLA4Uppx9PrC1yq7sW53yJUjFo6RXQsOJy41LNS4e/Agfh5ZbUmTXJSMbZs+Yq
uuqjtSVLrPzBdW6ZJTTdYKaQ5bQIj1LasykPTwRMxgp/yzZIdpHtAs2L/KrlTJ+rAcJ7aJXWDGqT
RVpmWaoQsb97CavLvS2BpLIVjpKDCxbKwcqzzqkZC664nRNmrHzlqIaw3TXkj5YkdjJf3+SW0KKL
jSoXTZ19bA3LfB1IC+2WU88EV4cySL4RJ1Wqc3HEoG7F8Uusg8cdumCwIUEOJqOla2ReJJxLauFq
CdEcr67DKyhbCz+Gy/+uO/2arJoWKYVv4EXcxCW1OQryUjxk6LH648RJ1FKHzh4B92UuescshhFQ
djYW6yFNu5cogZsBpn4o1ioUAVmNI02jbCpJY3V69lNtGZU7mlFN4930N3BvviJ9nJD+TsiUVMY0
tQLNfjU9MoPFZODC3udunCIzZ/Wcil0QULH8if2k4QCnoT/KkNlLKth4oe7WYCVoEpRR+3YT4/EC
KdAJjvdmRbjECYMhZYHT/K1ypDra9pFKDF8oOA4jrLQgk/1XOHFplRUJAz7V67OF/z6U+2x+667d
Hxc9NtCHZCRAdfrT/kq9I5ICK1aWG/503tAdUdzo9iH9YzXPZK1GVNTiHNkyIaBiz6wP6MSiv6AU
9LP+YZjOXdsoWOTI3d/zvctPvQY4sWH+lkEVqOGtOl4wHhy/Pz0CndoOFYFfW5LEOKQ48R4KNz4M
N176E9DSCUsI7CEaspgnWRgBVFxFh6gkXQGT1JrDhj82rXgsi84OpxhS5JgLl8go7pctkavNkPep
UGKLvL8IlI8gXUmgvAYEPW5C3VF2Ncm400dawrp0d7fU0vtsHzBUH3WGV5Q9pcV5RERD+4mXvsPS
+fH6swXVBJ91BmtbhOWqQsndjTtkKMzg01y3CTc0C93BBFRfxPjA1flIEYker+I2og3Pc1/t54fG
BmZ4QF2xxrEGlInteEzvWO/VRWCGacUoCF0POL3bUW6f8XmeVQefhXPgd8fTqNpo28NtPt8kmDZ2
w9sMW8LIIIIt+b+D5VLZ5cfzeagq/bbIiWk94MONXDG9bH9gYHQZvA9nmNQ5VTsGwCYiW5XCLFD8
9FFCBV5gyMWLuD6BiUgeit0v0/1b2twjMfLINpjws/G7fkYqFIx0Xa7G3qsjTst177lm1J54uxeY
XD7zSw6MCLIKFFP7LdKl0c/mkEKj4v8HqK2TRZaUyuSkyQVE9AR85jmuhtBqmduGVg1XBskSFtIn
fvKVJDqkGxkuC1HFA52rU/2Co4C9McGpI9ZxG3dU1brjKic7t1WsNgoBYgc/DYSXJQvN5emC1W9n
tPgNDXqRgG2ez4RRSWBLcbuL7AbY9sDmEJS7n0C0ZBh5dZT98f1kLXEobii79JRDpJg2Jk9/3OUR
2p7jJ2rgVm5NlyNRCX03hXdK3DXq45GDsB74sN5JA8k2u6BkihDbMPxm4MXGLTqCgVW2xpPoyWjt
cYYZ5JQHSZ2bH7lLBoGv3qVVjRYb97W6PzyXYfySiNkW0+WzQb0YYxc4dB+qLoAzF2xNIp88y3Fp
zUm69Lzty6W5KlEqJ6+HghEyFaQ7E67u1cTOXHZHTvAka8f7wUQVuYLxpZPLqT2iearuyyYBlxc9
wnNWvaCzGuZHzs0JJ4ajkzEDNUUHZRJ58B+sN59E4gcrPzkzdgAd5yydKtT4EFfd2UMCaSuPPOBl
nut7x39C0h1fjmYiV9/qg8Udgmh6jOlR+OTe49EXUTRIPdj8MMcXJO5F6bx/nUbqVHgZJfCH9eHO
npw/2kU+owmFNRbS2oerBuc77G25/h1cq0bdsgsf6BXGXtkmFUAI5ysnk8oAz0nPSLMTjVpB9dYR
utz/FU7aCD8ChByJGg65g5H4nfBo7mJpLqGZ/X2wj+lTKLN6e/+egyKsuTitmr9P/hZKZlr3NqO8
JPCkMU3Dtsshsm7KUMWseLUxqqvmE7jlcBxHQyQpELff2m3Y0y0on6kXnus4STT7U9r1eQtq2PGe
8HuI0gJH2WJrRgnvjKrcBQI6nH3OlNACcTEglP9A4rJJqkVeg1bdwecZrd/Z2yGqLxJFJa9Wo+us
M2o/xrF+jAGg+sVKqVz0dWR1aCJ45ix14UR5L+U1F23ei45EB9NSVWUVtNcKzKAgODu3ZWDpcuSL
wkE/JtBP9x/Wp/ZSF/4fSzo7iSfxnzpwqORzyKci8bkUjVUgtB5AfuP3w4trw7c/uUTCDuoM1vBD
xRt/AS2+pJXjZvuUChOMIPvo0N9IhD65n6YjkVRKo1XFYH7ZAwYdmpjt08r5zt2Oa93F9seqtzBC
/ZuI0z2XimWa61yQQ2/Ry85YO/5+ZXCp5wYRcp3wQ+//ASL12uOFx6R0TX9lWmkzqI1EsM19HOaq
S4HJJ9Wt0yaZr2rqbEUFE2xCMxpvcsbQs7hKuU5I6Vpm4iGBax+Kurn53ZMFr0DbnwlUyALEu/Wt
aqyUE0ZQ+u3ScL45Eu5UuzqwvqODCxcTFUPN5jt4FO46nYY1ROY5gAZvIDFIOMWs3bYd+mKHDfct
2iF7fZsV2b6egQ8IKKol2PEXoxQ28ZJ04d7sFcZ8ke1JALNP8TbQopqG4FoD/+KhqFPCCz06TVUV
RZxzqZ+eVyN6VcX6dYMSBoBbzv9aa7jeEr0YukDqtFUJAUvtfy9kIksRwHKjS7xRbaWrzHNuDO3A
T+0bAuG4lQt6CSQtt1DdjigrVo9jbw9Dz/suKjv/Iwu4yE0h/MoQ518rLwG+I1ZckeUxM0Llw5TX
06cMc7tHoN6aX8gOzChUCT4J6i5PiwBg/dDPjd0GAMtasD9g2bvop/tP6y8m8JmMe4egqWznHQL8
QS/AeB5NevMgA8hjH7Vi406QorrclDGQuMWn5ZXEptcNLI3vFLfDwrgvGEn2hdgl7PEigbD7Qjss
PduA0Ffucs6k4mDHEqEF3wnjAtQHQUCj2rL0Xqmgcv+kTLzjEMWXFep+h3BKRNiQMmUpgoDkJFX6
VfoEw7v2QJ/iahhieTkWcbBfzpYg7T7aXuZKoeqeyTlJycEGQGJVfvecgsOWaE/gU45zeElTmz85
99J0GMJvW6P243KjAzPMpheOV/+6ZnoFVbMzV9yJ5ZZKRmagTm/EpLGUNO8tU9cTA+xuwuxBd/K5
07AT5R6uj3/WtN0wonoxgv7/Yjg88C/pwsdPLnCJgkhKgxcI2hQYxc0zN2icM4GaL+h+PeOwnf27
S2RoVJZKgQ5lxOgPX7SXdOtIjk99LiZlkOm6qNOlmpNRX4y/cSCHtHq6QRDvFtIkLRAomLLC9eCX
u1zmkxME2sMQmFP3Wb/fA8PtQHybHosRkcM7yL9SawRsZ7nW5Xjqm3c2PKQgeppv+T4xQlnypLlA
UqKfnNj8wCWLiDKvX1U51YVPtnOiU+WNb6shN0EVqSEvoFHwke6+2pixkGiR3H7hDkzLUEqv10GT
08qCjAc5VZA64CGJjozb5dUzYkb9Q6yh4DfYq+hP4TMXWREX1A44W7PRYUh5FDytV/q6Rsmxb/W1
LmmTAcim7GL3OGQsvautu9ytVM5fUT9KwrgeXJsqRHz0IP6oou1Tsn2JN2vWMLz/0rS7dWu9U3fH
EUrEdlh7KQZx0PDM1YkJ7R5yz7X50uP2XT8p5br1tYzVyII5hfa+lJEIAK0/2V+GCLll3tphSi7V
nIGmQgYaMT2bT2e4IC8GsstyYHS4BBrwB+rchYSbeMQIF9E1fW+q4Ec3sILx77gjKbeeo3vWKT5v
ZX3Kq5NMSm6l0od7MFTMo9L3w3HsYK+vPjxpks+xVsUAMYG/Q8fSP50Cy57Km1pgXEdSr1fS2AcM
Kh88TzwS8QSPjAh9gt/J2CPYm51/RSdnuXTV66teqctWkpeII7t2PEahDnHDYxCYUerlCB2ii1mH
QM4gPj5grCXvvnCRO/MkgBGnVDfeDecIOG/tbDloO6knzS8pm0w4OpcqaBKfd3HamrNOn5KZt6bA
F5RDV2/Ih9/8DVFFYJANpnNLzT8Wgk9OngD6fZqd3fnHPyROgUm97uKef4J0ymysG6RsYtPIkoKZ
0ua8UNuOsrSatm0s6HVRi4sojXjKs/7wEa04NYrPJ9EfJC2kU4lEXVURq5vryCTFlemdyZbBnRdt
pJwLJx9SIyPr1WaeA5rbOfK5XJAYwL4MzljwzXWtkZl8wouz6SN2YL2LFIrp7Njf2oLSp5WamxYx
a7rBT5COzFmiSs/C57DDflJl233BSrvLMjBD8STE4/FamEmWKOlyIwgGS6Uphggt97kQuTEA89lO
f1PZYkiZ8spbsaYu9aD4DVPjEGFSyG7isvlqQjDryOr/Cor1ILVgI+GGolX+9aIe6stTo/L/h6MN
dxygw0TNbEDJnZ+JVAQxYXIs+FXWP7uzYb2jU1NAXBnwyoiilQvMldIF+2athg+1ZF8D3SElcETD
U1RNaEEDklzHbL0Hk/lPxMKX+rlVHgHuysjV+PAVj5qJ9fKMGV1c88VbHn8Lw/WLv54X2MezycU9
8tCbbZ0kErc2p0eku/H/vU9bSxFf+1s5em7dQRkjQQEPkUqhTXaElns6gw3eY5DuT/QirQWRGCf1
kjeDRZNOTG/lte++UAk/3Q89w+fSr76hgnQFeo1F1aDGZCkVjP2lOye6ASYW8GHQQYRmzEkDH3oy
prEWbYhhySM3cPp9VKUAQ/BTeRBeNTMxTvY5D4RTec31n91tvEaaIaM/+7Bj5WPI806HyPtDUpw5
g6gxH2V8AAGLAzd8B0dT5fvdn4vowxs7qYlniRTTWIk4HXDvCDYQj4IMU3TW3NJ1UjsPbzwo7/xu
H+3IwlHCQdu21+mrp8vepTFiFJpOcP724zsoAhZ8RYq4rRVzT7So4q2dwVWNloXT40PzYQxBbWHH
doFgnSeBVaQhBGGYWhDRO8WD8ZfkbWQa9jn1seIH1x7FZQ82vwxY016ZcyHGRTBg8TbmafKxW1E5
YeFw8lMt9CfN/wZ2KfGWuUk9opAJQgCAv/tJQpIEeCiGuyJlFxjIGY0hT+lUolIgyZ+G4SuhP7wD
kytPjjhuc8D/juH4VuRD8XPn+VLiaohLzjTd9WT8kTtekhPO6VvqUNxp+h0iUfwz7sSvP0ycXUEq
GYTxZKzeMCsWY2ol/dLMUJr/aPl1wrDmckECxY8+Lqj9GYi5LBg0GxTrvb1bw10mm8ZveczW2exM
q98wW2eqA4qDlt1JECqVxXjkSaBj0tKp7R3MYXzMY5Ce7m/+5hfN7wQnmh1kZ0Dry+gXElwmMCMi
Xv1Zn5xyCpCAfo0vY+bLQVfP6yCAcfpYRXuF1d0dNqzlzpnTxqI3oi3wA8wHjMZiTbZnlSbRiopS
J6EPEbZXf42CAAmgjFFi94taj4wJGsspr5tA3z4gUMCMvOBrJBS1uxL274ZDldaNR2Cq9Wh8LnG4
SKk2N5lVqnAxhwXeFYfAuTmCsRZRkjPioRx2u0cNStm2RQPvMnn+y+vzxXKjdJ/C/DDkBzQ1u27t
Wjj9eRt+TJb+XktvOuNrtI+4chiRWrlFT5HZjyJ2/qlPOPiV7k1giYtf6mk4oCxU04RQz8PFd5h2
azfxFhdEmkaPN9ZCMbnxTA5k74vZ/4a68V9AE15f2kT3Vq0dr4XXU1fm7mDrI/zLdIZt+8EApEtm
CJnUuLRLqe+KYJJRW+37byD5MM+Hav9crSBKI4TfrphV5OnjGd+KYSywbbp99/PhTvI3dhUstsJH
z8wGqK9J2G+YADGEIDCTtnz2+MnT4x0K0Pr26NCVCNjKeWDeelk7Tk3kcBlq5HEdVh5SfKsS6g/w
11g51o0W+O1NvnC6fpHFvpfzO7PU5IR/hsFvSftp4367rqK5BnlDRVSFBo/DfdQgEfJOBuqgoL0L
q3MyG2s1YhvX6JgMllEUuuedtz2m042NBTPdOARz+DesL+9EfgxbMa8olCHfGA6nKnTXxqdev2xe
eZLDDzkBrWaGsNuD/J2dbbBDVSZNvnTO7zMior4vNZ7OgP3BH6s0GCuR/FIb5l7pacSEjaTqnCFx
KnxoUtegeRVSLb3IKDP/8991W/GL1bTKOTi9R+bDQl9k2qghSMSweNVO+HqTiJyHhE3FpRsEcsMi
2qJ/D23koRUOA0wMb4uZpnwjaDMYLRmqAinC1IxJ078OGHmtEf5IGYxPMQB34T2UVQt0/wUg33A8
qXcemC76RGgdjdJ9v37rg/couXi4S4vbNfkQcWRCNtXTl4n+QwHUP1aBfp8jqUU329Y6JheA/lpY
BWcbIxlFyiKCbNRL7o0fb3Av1oiSToVL2PqGVHNk3Ifx5FfaXdAVB6AqGTu+Dv5yhPuUOoPiW7dZ
2dOB3CPM9z62QnYglFylbYXr5ryUM5AN0CbWo/wbiMZ0pDRCrqJooGn2jMT3bzGdIvwvBsQWyfSB
aBYzKooDN/Y1HHlnfBsHnid7hlcYcvLMuIsI7+dsd+WE+d/S4+opw7KrRda/oBxwE7Es1KkfWcRR
LKL6ZkppdiPBEkCJPmr/GFXvqkzMVoQH2xEjSxg2ycrhudMlTT5MYxiGUnUhccAJhTKRG7ML4lnw
kAsmfSL9BZsoP0j5Fxs/WhSyR3ukuZpDpqNnW/CG/PRx6AaLccbNIbZsvV8W7CtWVtRsAvUDCSlY
vL4dlW9I2mpOwSuzMasMSMlTg7UrcPImlDWxL4vBoqja6M4gundfc7mENjT/vyrLSTAm8zTrPcA6
ujzghMjvVsMRctQymQaBuBC9dtGU3sH8llWHyieVhPiZBbn4YMMzWkDBuHQ95Hag8r5v5o9Vnv/S
hOksKhWdky9Y8MNPwz6Xhk4JrKrn90sMCsI7xPi+mYNN+WNRJMkckJ7KDB0Tlzvluqom9cKVTCfo
g8nQUMMiDuPKKZy0MDywueCo3l+43nqaESrzseACRN0qHGAEPrpsP1CBBYghremY0CCdL7xD70KF
sEylw59wmRnPHocZyZKHaSKxhYmRrMh2D1tk7Pwn8rf9MNCx93P0H/luVATQ1aF5EOf20rUO8Ws1
/qiwd3rYoLjq6w5xZCVMFD069vxlLSd48elH2+kR+GPf9wn+Lz/gyx/X+MPJAXLyUHElO6wRZWaJ
C9CD/o3+cYOR9oDJ7FkRo14NzZ3otub/HJEGAE9GTDg3N7L3T1+i0zPqTmE8JceOmMaj5i7nUTCG
IEJCFTL7sjGwAtH3GYciNsvWxYbzORZLN+qD28XGz40SsRKSKj+inomWD+s3PIlMkbwmTxGIUlYt
4YfMWKUs4vuSTZte/ZuVcIehyChFxPLsiQz6JqoOZz3ybAuqK9wvDFlzQWqmAjuW5oWmpnTaqnIA
HMVDtf4yRLU5Ui5SmXcMX2/1cVfja9BtnmsSXcNh2UUoYdo4+D0ffT/0xciyrietrl5fyX3gZtv+
8/ek1pmocIqVdJdBfaPwWcBv7dRuRvlEUJrVIlRSalZuaIkMitEd2/FYirNbXbFt7o4mJoAxcJTN
w3zChF4WkWSSQCKF2Ff+gIIVWIBii4W+NcdpZ0m3fRbCiFiDtkrUlYNOL/tE5DZsRwS6e2S5Sbma
xYHskMltv1zp5tndstRrAn3Ru+ozT1xKXX64Ln2KNhRjkmJXi3Lt40zd6VRQGvbUh9IiNiw7KHor
YJjGEQZZFYbL+dfXIqojuumSvcVYgaCpGlkhq+Y4DCp46RF2t/EvkryAm5pXWKyqc7DLp3zbY+sg
NmhZmoeLsmkkVpPQHPl2KUCb+MpzWh7077aVY0KVHnLBEWt43+7xzy1G2HQVl2xolqyclFnEAGdd
BDGTJ3dEGJzir8O4qJsokOHOZHps40b0z1NVDIZ4J9aqztd5FjlydXJNe8hYcoqIOz8YNEL/laY2
28PRVBHobbHHLiAS+gsEfSadzwoej3ea6fb06w5SYxbkOOJp2/LPiKgrX4U2cQ/zyrc1OWYaMI8D
s2mZrVCdnIxPQoDGgQyQ4v3c+9P4IUn079bRBEVYvjZJaqQEbPEbUqgCWlZl1QNzuwoqGhGfBCnn
PXpAr+N83xCBBJMXJ2Zel4WgpWaIvrfrhsjpF081y5q4Nf5+PFVjOZpM+XvGhdhUVPcG4mYKKt/F
ncsrlFk2gXRcZpyVmmzNptuEr7tfcnK0I58B9CnFHdATSb/UKo+vlnf1o/AY2jOYrKxCAs3ouB5S
PhWcImRky+mQjITGlcBNY8eA5DOWfjABFqctwpJjFneUesRX0WHjN7mgt4qsIX5r3dW6PthUB9va
M+WXVpfc7Qio3OEq0pVHMbLfwp3XO9SZ5M5jG18vK313FU6WL3vCnsdhI2q9LjEdMrqi/MgPadZM
N0KNaiZ34LTZV9wdNMyBWv9TAeB2BrfG0QpQ37qiz0WAR7tVm1FusW5Q/NQPEcduFDVpjKj+q4ES
8qqXVVMqedaA/GclwazLLGC742GsNKzA9hL7hUL8i7OwOKLj+Y7PfD4uYTp3WxZ/8vflHBQJjLo/
mSo0rlz9j+QxV+FJd9QKguDdDEHl8f6hLIPkIn3cjjbNuQDPGKHpncaI431pvu1PeQZvrwjdM5B5
Y6ioX4JlN/X5YF9HdPHc+PBYEflHF4h3YUWmvVjmYWjhJkfED6Rw/y0BibLpkrPSou6YgXpkT8dI
fDe2TERXpJgZ5NbFMNEzzDMSxp9vmuQMSnmO3oqbrmn/UrctNsLlg6fM+VOtzUuiySwv+va2pePU
7EU/OPYw75XGrvU8PZjx8OcG230vLgyOcpGcqmTLw7uEeSH4QFULZh89tqEi/slp5m6bSGL4nWma
FipQ07Klwda99oEs3mG9/1lwCqvlUVXaXxbjKnDIS17PmBL51hE0O2vTjvMokQSZ3946sfjbZ8R6
qRyFax1FYYEZtwLxfvWi2uXtnSRBzHNgLLlWUOyndxPNlYmnjT2I9SZ8eK63sIfXxor/F2Je+qyl
PBUGjHeK5E3WjD3bL2nqh8/CqAwknXgUzpEckHdo8cIz3hDqFWaidBV4Wu3YwlSGTYXkzOak3hA9
RVGKEUMpHbxDdrGodx5LiHb9KJB7AhA7ONoeeN1vsZ8dmXZoy2v79E0UTcLk9lXoqwkASr/wS9DM
F23rjWQMtkjXGO6i+JaImxljMOJqXgXyr1nmQAnUxibjboKsu+kEfa/hnh4G/iFLyFy2r7DrTzok
AkbkROzALGg1tup1Tzb4kgk9RFzxnYsRvmiGbdhlB8GL1w4GWyk9ekQHtS9Eimg6VxRTE9LduWsB
YY7bpfMXVodPhjzPmtXwhn69zGQECB8IkLeygTh8oFhkb4Or85VzSyfEwdhBwHznWmvjmp4R8w4T
g310oDwP/mtAFkder1M4Aj0iBddkDt1PZgLu+KMiq4tNzxP3rYj9okuXfYYLomINO6rjvnH3FvGY
jLtKzqz28t4JKU24v/5s0SXo97e1JLRH55PNpM3550AZ77XE0rDWar9jEl4pNZxx6/DWAz+TlulE
3gWHJLRkkbZQ5QxZ80tvaGGky6W7l5Pzz1e8Nsk2U1Y3cTT1Z1X9PqqHX5ewrAuI+qfS7QijwAsi
HEQJlIRpN5Mcu80KEXD8lNVmeuhBV6gZJ58qAM0ZFUSDXDCI71yEBWrJ+fzOAzOahjOtOxjTdZRQ
tcCqiOWej3sfbquQcWKsW8WFe0PZ+ysTtuhlO4aWR0TNGWRnVXuJpyviqaqI3fUa87ZhOhW09Tws
0jbSkB8c3IK1Pzsks2R1Bj2HXqWwk6NG0s8ScWO9gWTQzMv7IRhN/wz+5hVuv8QMUoU621Ba19GZ
4vGyo+g7cCJU9qykrUPLznJl2O2I+EBVvxeGl4pvgJdGNNLOOd3FRYrnCL5Ma9cGi6ZQwyzim9pV
O9c6QH6tQz9EgQ2BJaH68RiqhKcDJde50JRZeQ1/2wst7OBULXOyd2ocHeUfnlfA+LshjKRVIaGB
Q6SOnWxFzb0cQhHXxzcr8qRNzuqYh9ksKc47/Izqz4BXoqUo9f7n0tfjwHKAx9NRY9CtRIIAvmF9
xoe7bu5czuMxtiZNagYjrvnSJbgBXNt2YX8ELxvX2ke2fIbiVuupi6N8VoSI5TrtkYqwh3E9cTsK
BkqeYjtcDTy+kdDOJ9UFM23fux0CTosD9yC2rnmaBiDx9tKzLyJ3MgbkGB1U9gDnJl4X2AJNtA6D
h3YvDzDpUvRjrvBizm08IHsv+3vMxUi2VlVNit2dXMc4FUwvhUjQ1eIHG8k19JZnFq0w0nlYTuwx
Xhi7X45LOch8Hgnv7jffybkmxJN0y5CQjEx0mnNVD/L3orZirZrTGBOR96w2aATRzpyRPU4IWRKg
zca76/ewYjKnQbN+F3ozLQuNJv/6vObmUJIXXd+awuyHwwMw/FyMXM5jUhhyaxPUbq9xcm7bdU5L
lGB25KT3ZDbQD7PreXkvrQe0IRR7INP6u8BtFd2xhlMHcs8GAQtCqJ1O6zVIk0fQqdDk7ASIC7wJ
LOqcGE98HQj5EIgE1/v4kBSK74PC7/a7scMG6bjm8UWk38+xYcrledvE46AdaR+p0rv7abyCd7DS
X3hCKTCzl8ZWTGHjO2s7BZMamQXjbXkaY9tqIhLfMmd6TCCqZLZvAq2q1iB514+KzZLrejdHE+Ck
H5F/8YaG+y7kHMnX/Y9MJpAKyv3dH3yYheH66kaGCN5WcTK4rSMqCt5aUG6SF8NlSrfVZO09rt/t
ELGNHjk8gdYnaGdZLu68QZATCljyTW87uO5Ie0bGPgbY5t+2fbpDpYKFDuc2gRhcb8JXgl/iAYCB
Pb2iI/EhZm5ADsDoI/mvNi2dzmgw2x9YRzOh+OLduKZED1fqUioOcS/oTvy6iM5nva5LzKsj4kx+
31XuQnS16LPjRDfNQ8hx358H8KqEZ9Vyeano9x22WSfQr9UkxalPy91SnpKp8oO3ACgMk+OXPPM2
roehzgI/E6pC8vdJ3iP4X3zHpa1Hk8/q1ujdwgFwWfSk6ZvCiXuCd7WCC4oRV00J+v6icHRyAxpa
8sV9A7c8G7cwKEZDabUtmRfTVkvKdHwoNUGZ2GqqHH2ggh9f8zGsr1atc8RfrIQq+ZNdz4hv6ZB0
c0z84Gkam8EzzuAiH1JpCy3wfa14UlPpL4GbTUlpSjN7cVqBbT/RaNAQO7+BnmmcDTBPxqFWw5we
DWWc06eCZiXBONG/bGDFRUaANfJiHN/RnnLyS4BJnjfdiwaSR3SUjQv1knSwbHjTLFB08e7JBkLL
QPxYWxqF3nVYEtzw2lzzTMcU9vwED8tqtD7JA94YT4Pl6vIiXH7ErL/EqidJqD0iMyInDvFrMJts
QZhfNM1JGqBoTo6V6Js2zrjxR2dcOZqPWFiOjNsXg5xGEOUlFwDogUAZiYl/EYB9glwpqf7m3Jmw
GFbwmPmfn6rebXYRRhNNkMh/ipexLkp/xqvry20tjvY7OtTpDOJiA+wso4/EF8mpl8Sc0Dbm13TO
jDEEJtZKnror7/4z20CctESmNgYr8zuIjpPf0AkblVomt9Hzccly6Ed7wmflEnm1OBh4/pBGJ+LJ
1vCw+Su8JmKfSSE6jYEkc7C7i66znSz+4myMB7CNhBLHkjm4eEYmKfJl+ekplvCfXTGBsVsa6oUQ
O9GJkBKgGYTC59zIW547kQaIPA8SLWWTvFVW59tEDrNcoVXLyJmvbHKbnpcmgM0pcIL4JO/xMhhc
SZGNJOuT/v+tUlx9cM8eEGTRejeHUGI2TfpGnMW4bJB5Hqtg6+foipb54Z4hjSHI1GUeqBEZcn4u
Y3VeyG3jZEMshZo2qAcDcbeneYfHLaolAxdCeUJUEOQyD7RwBmNCcFwQpeOOx9WSGCDaIPSTa4Zt
Ugu/hEFYCu4bDxyCHJsZ21+2qsyiIXHM7QsA8awxM+nZmw0cefOyYYtj+e08e6v4C7n2wuI7lZS9
DHO6zAnSLwbIsdUpoav0eL621QbCKn/pzWB1WqAXNqDPTl4NjENFkeZaDi64Kjrp+b1vBg8c6hnG
QoQpZToRu6gCqsuLxr3HKXhZgUzS/cu3zkBmZsMJivwbl88HKoNrte8w6glRLCObg5ifNFu6Jp77
zoYCKjWTvA/DkTJnuIS9Oi2BK+3vQFzFwer/OubAd+BTiQDVwwEQnzbPTDI/50X/jS1agwpRo6xN
oC3v5LeBHK7OXgBHiq0RzLuRy44e0Mh2zRUHh+UTfXAjsHifqu6kZ/yIXbw7g84a+nqEMv2M/4Fo
IgPw7a/O/0l2FBue3QngoSdEkODXnFVWsI8HJcV+LFCkXsBYsrTy9L3z3QcA3MoAB3z1vJGaVDva
zMT6UTFp+MrySE8k7D57XZduu7lWv+9nEBedvYEa5AJXpJ1HxL3FUN5m/N7KWJzVdnJ8W75phCAi
2FTijF8khKYi6LXR4Yqm+DTpW9062mHsYnfy9Vx7VosLg29A3njtNvDnR98p1Bo2TJITm9q0Zv4h
bKb+iUdYZlmkcK/C2BL+y0z21zVJPZfC4iEfsnZ085GvVusLEkYgrgFPNOQQRP7oNZO1DstS3Vf6
kecWCsqcKKWIVZYjYiCrvxEPYNOimgJhqrdY0lvTZgd2WeuuINjJUzcshVGVTwJIkS/Ono8LFUIP
ORJnFMB955OvgLNHRJIyzhdfJ/bnnB+VAf+smQq7N0YmaIpKzODE4MrjScyVTpddps3+pW3G+oF9
5ONLXIUxRmqWSsvX2fJCqp6lO0EbgKgokhulSW8K8hsI2+ra5IrGOHJXQMptQsUtEKBcWzwhn8ma
x0QWOdm8aYZbvej96AgEuNDQ93dsu6S4Yy3chOYUfUOJQ8mgaoHZvVbFObec1SxKeS935GKjtnEa
4VFmKjV+ZgUJJTGBqYvXHKCD8pZ7E7AL+5x0hl/1qARN56biOhf7rq43YLQSKznm3bexB6uEI6Bn
OcMwU6f8W4geMgkAhqbi08VRgHPe8kT+dNOWpMap31jjoIuAQ07Xl3XfVNa2FKtxHTx+mymkGo+p
UZgm52y6oYB4064zYKJZ5Zg/qwVLrYiLMBmU+Q60xWW/1D4JDocP7MeBvP4glJ1j/qCco7ojoKph
/fJf7tDIqmZ12dZKxG3Km+y6fshpmmQflPcDhDTwk1mxgMCoxzTjUmTK8remvuOOh6BmRbE4Ax7r
KmmU+b8QrEFzQBpG7EncKdEMA/TaXxe0Jwvr2tu5M9eDJG++h2qY2q1zRm3jOqKHya1ZqGE0fmv1
h1TAzL//idHM5YRNhjdSh1srrBEHn05acnFzyyFV7NziB638esSKETozD0G591O83Lu1OWfW6Nsu
N1zTT4YzQSpcw37Z8PlXt1MWQe+s8gvTZNEmHHI8leOCgeQzAwHEnnh/mNf70b6fDIYjMJazgttF
FhRxIB8aRRSN/ohrI/lrHCTlzRhmVF7hMYt8LgrRkZHXpDvaHQkLqpVxV1tC95EFXlz6nu7YRNJl
9HHK16U0YaVuHJtWAOvcbb1HhFyDYsrslVWqNqLsGtIoCivx4tYGp2ag8coWWBB2FN743sJfH92I
+aM+zq4I7j2TTQ54EyMh/1zk5AJ1xBZkegNvDMinLrdmAF/eeDvkdJdWaYuKH3Lzz0eS0Dru8CL/
EUVq/OFIvUWNqtU/3VKhzCbpdC5n436VqGMmjbG2HG9U2sMu6WyW8D2qFFvxY/n1v+djRlA9PZA9
UwQt3rsnyPZpQgHjfERgTCtfezed9K4ud+DARPHslTQurXLOFySEwl2ZL0Ow6ynSpRS2sqbMAVmN
uKpz2cjPIfNY64UB3b6Ftp8sYoMujqfd53LQsqj7jCL/0N62TEeLrzJY49G/nA6LZPYwhCCHbdlw
Y388aEbI/9BEZgZ4kLG7HKiHmWDrrImOGZnaGy32nnxfkRN4KQ+BVqO8FIUliRLErq4Xyb3apAW0
Taenr3wcL2f1zm13/9XETLi0hnOHPP0eul5hm7g1NpqwmFKCoZzTIi6ulBIJUt2Crnj1Qh7/HiK/
Qzz8SyrRa30qVLlqQvrvR7SB/A1b5twuMuFJnpReYKwQbmbMKtZfGtUfQBT6+AsIGx9b54jWCYYy
TYgsw0KeVf8vFNn2EFiO3b0AuIooO29dsYFuWvrUByObeN6zXw5k+hOWeTdMhya68a4cVcYKLTVu
42ZfZaMQ5IA7VcYGcwkhPh6WSb/hLFrYr+HeTbtb5TLiRoRUKpY3Yy2jD2TsQ60JHzZiJuwMN8Ds
beEAoFzIEDaJIwprZbaGcsCw9sKzD8YOfy80Sv+SG/ViAnXhcyFDKBO3twCg8Ypv8gmezQYDCqVH
0ZfphyKNYzQ2vmxgPA3zxOWHHwVJf7+EyiIMtSGyscX8Owv0MzEUearVtBYVpx6wF6YwUlKn/r+N
bL0MHFSwKb8fNwo+gzLu7tkcdvr9YfqP7OmvJb0I9nmQrNLm3SghpzjXdALk+hP3WHPqRv1WNu2f
uI6WcpBv65oNOFODx9s6tTW6gAr2h9kOMN3cUvn0jnKM1UHC0I/mvPUmSQeAz1/cBO4abmept0QQ
aslqSZIKme4IZoPnRaIp6xA/5pNwRKkCHD8EZk5JOm/TwMnz2hKIugCBwsGd/BmCKxBg+/JOpPT9
LwIViMIuUZmhwOXv/zkv4qWyMjq/2wNo7Jv9ZrOyB3j3b9lQ0MqAaoWk1rbxVzYT7DixYoWt1UDs
qix0knA0g1AdpTrROsWqHINBUGeIJGaxaSbQ/og/yh3lWJWjyfQkc71+yFJnRzkQfTFM55puNTRo
NwSdsk8E7xxN8Tnlom9sb00/B0idxMr87xtJXUH9fI5UPe1tqnCFZzzxAfwt0ZNYEIXT3znyfIcv
emR9NSuklCFvn+AdTloKQQCVoHRfWHsor9wndoagOEBbNp1CwxLvCIvlNOnIhYpTIPX2ip7UNG5o
lfkYbQ7H+33gEs3czRLzELC7H9V+JvRwq+7t2qU/tS3mYPNL7bSDRf+ouaQEPBfa/PDxLU20wHP/
9cHffR2KgmcscISXsCAcmmEvdWDnEXJ+vdkMm3Mgz2rOy0p0gfZa+3sh/3eYwQqv8zF32kUEFnsi
NfsXpROnknFKeBSln3NDhmRloMPp23onq2gFzUITtxd5O/NZg8ix7fmHpBDxRVS60wxX3rowXxT1
DFyyZT7I/c/+BX0nHCMq64Ryu6SDgmF1EOXp6/7RepSMPfhsaxYmZAE6qvA6DQZUFIYVF+A1MS1g
JubjYCGO1RWdFoXDK3omKQ7aypOepB13dn3XJkF7ViBTZ3NmE/qpIwCCI6WtFa5zV6ivoSNgyMVu
JKx/BcjrGRZrVmdee6qrezT1xqsBEScyB5EOavbBqSo67rs8VbDecN+i29BO8r2/oPX5gH90qhbP
Eo93TIPf0SBs6HNk5/BjYwesca+bfvvmuzx3tt9KQUNQkE5s/SLKy6EnwYK3HqEx1v8KUAh5gQfL
nk2WpqE41aUXkMrPRjzTjIaQBctJxO/BdswqFOFimr94wCMu+IUqNkvL452f8yjrt4btbgurnHa1
cfQp2h5fUwTCC+sw3273Ng05aW5NpQxeX+cJmS9pOwewQg0w5rJlFj9FCQBl7aEEzeSZILWS2LPl
9VB1rb5ZiqZGmUioS4rs2eCnGGbtuShRYKJRp1Wh3jnrGlNP0EDnUs1X2jDqy+LyJIZOEMClywdM
KAJBYSPmUvcZJ1bvgcVTagl0YWMnudOpavBFrovOD5T1uS81R6KVUUF2nTU65W0xhXOg1kCCojwP
+kR6Bn3xt7xqAVJg1y9yGYB1u6wwH1+VoShw6JQLFznDRRhn4huOWY3Jl124nCdQ2t///2TS3Pq8
JAk48IgtqckprkzFyPJ+6efplYpUJGv4eMrEZG7N0djXBXWRJ/lU7e9etPTmbE62VMQyr9LYTPWU
0mG9m3fG6V4ceiCSvaJfeB+V/jDu+IH/vyyX4FjNnzPFoESMQmcbon1W4gbpxLrfQ8ClTIFFXXuK
D+wbuoVOKuQz4RYRksEhrfISqQdcZOG0PJY2/+BfOt94km6JHb2Z/Ooj9BzCejIYtCk0w/MJj9aS
+6L3MJXLq8mF1ICWNUScVxicwrh48uL+06RVe5j7w+KsgdF6RALLeXm3W98+b8imR/dBxObFbiMx
5QrjOd677EKCBa1w0kvgWdXDZI+10ky6Oy8cYb7LtJDYA7JBCSJVLTd8iW5X0jGkm7353p99ypWE
ivjsE/RpUvcMiR/PS9HtTPcDaEoDyNqGdNU2xoP654spy6Uvgkr7gMMRgGw/w3QknuKCuIeBnafe
CrLdamg14vD0qhD/BrsudmgN/XrxfQmwutJ8XnljYJsDNwPPOe+GORDEcMoWXwQkIqoEssrLjP0J
LDTagX6UMXkw7bL+pV1PphiOD9fokJ3SL6Evrz+4oah1pJEfC1oiCvrKKZ6/oTz1mIDohK0UWw8r
xpc/5iGYaX1ytxZpGYS3bPDRqybFInx77h9ZZXzIgxZpkAfGnf6Opsaw24s3DmegPemY/KtJbjlk
z4obmta5/Q4LiMcWCyPQwmIqk4XjXPBHnJFv+wb3v1Wt8wGhNaNRvBB7g7mztTI4eG3DnyPnhS5a
7Zo7ubf8Zl/q521av1U0X4g5kccutwOrGZW3p2bVTzlvqr1EzgW2sJ+X5k2A3GFjPVxZD+2du5RS
k8vx4tAHOC6ooLlj32YTgFl0Kf+cUBIlaWSkEHyNuZY44dHevGhqpzi6nPeyVRyKj0edQ12ud8TT
Nr0RQSQCkXE6isoi0Lr/criDVD22HC6r8a3mRlrgIY22PwRffrdljGrHdt22KZ1nKW7Du+UqaaQn
VmPP5ixzD6E2+uLZEdoLYTVTHLGPkDezcuuhAJEKRCn8U0l3cyV7b67/P+VrAB/+49dDYbDiURjr
NvU1xgzwHepiijn6Pdv9PBshEe1eIYmYCcAuhLd4PbmmbnO+gn7IKi5RGOyzJrqoiID0MyBcVUyb
yYJZwV1x1RmpKYywo9tu4CnL6eSgXRK79ptweIbHJLBp0CY61ny96rDE1TxUu19xrGUZJljEKW9H
BR03qBHFw+Wvf4bTlLd1HLpQwWIVC0l/KwQyzNR1LM4tJ1gD7e5HA2nD03IOFuFtDgVdwaHeWJVm
hJSCSFVg/mvwW2CyrygxS/soqhBzuZMJygzJ6jAvwOmmRvr42gHh5u8YyFxVir9bERiAcG/U0m+/
jJpjtnDNeQ9AMzXClt/tGWAgYRw4IftSig/BUJ+2daAIBkcJXFr5Pv+QdxWYToIwHqJHKpYdniFI
nPlB67RjXQHM+P2UxOs99zaHQaMoUioil4rkCLVhMxWi5ujpR83fO9UeL4C7PCe0Z8AYpA703J3i
1Fr35iR2MvSG9rCUSH91w49rmjJjmxMjt8eZuUpaC2AcMbJLRm7Y1GOoztWkbtCM2OKu5K6TO4ux
MtkAYond/xDHCahz48Uz8WXDQELcY3jOhYCvoka6xvoID28p1+kDMmgsAusH03T2L/HLd+PCz43h
TiOiZLxdr/WGzNd+vcLukstbYtzdJ6GK9ZP0KzLiEq6PLNTy/ITxRKK0zHc5AjOpbwbiP+8=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_11_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_11_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_11_0 : entity is "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_11_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_11_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_11_0;

architecture STRUCTURE of design_3_c_shift_ram_11_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
