-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:44 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_11_1 -prefix
--               design_3_c_shift_ram_11_1_ design_3_c_shift_ram_10_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_10_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QcE9JgSZJXlEdJa9l5/KM5UAOqKlcDvjOrzeoonFUtyvaM2j7QpunRFdD+nqKIU1Inva1SMB8nrF
gKiJvHkHyPRrcgMpXzTdYW3AoEFHbPojwVnbf/Su2642m4HcBlU2w6q7CBex29vK233dPM49gTvL
cCjsqfF5ORhuiFJh/8Y4Qm00d9ykN70WGas/TKecNJzkSyknK2KrDJaXz143djKBIW8o0Mw2Uk7X
u9QdFSxR2lHbs2ZcYl101/+tUkRm20+ZB2IFoubpRsfyA7Zr/1ruVO+cCC8bYU+935iCDHBrBH8z
gcfGMlWwQLEwQs0j0AIiXXCzbBIbg9jzFgAdRw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jEDgf3L4ROYKqe9m3y7//JBHbzABVZ2xw8rr+XLkcU6/1i/Q+CK1RrUSapYT3hns63S3dguuynOy
DYl6e2OTE/klNzp+twCnQthUEs4Pi41pIRPX9I6M+LVPP+pxJUfPeYAZhW+hSzxOFiasxpKmNxCl
+RmQ7gT/546CTxEn1w68kZXGG1W18g1AfLfg/nwd0jSr4BYTMeRE4jhnhSpINxIL+1iEWeH2e37C
laxtR9iW6eG2IkceRmemrmDcHn/Wgh0+jSmwlDtgbODRuuDkUu9xQ11TMt8SFzrXR3jPDxHZnvcX
n/Oy/YIB2p807TmxP3C0h1wHG/RyvKj+rBdBsw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13904)
`protect data_block
uNFgICRrgUZ5Zv6osOgJeC8E8kE7+J0t8BmIgagdVwhNXljB7hp0GazSAgj7E73ht4VpteU73FTK
0806NrOn24vwuS/bHu91x+EGP1fzkQwj7clDiUnJrufxSdUNqk+W5S38LjJxE1lJTB0BEKFj/bVl
kkv68cQieiH+PIInWJFTeIB8xesVgQrrX/mjiOMJXN20R6ZWY7hVwwBojIANhGSiQb1IO8zI5JcE
s4d89oDhf5Fm1d4nyZPmw+ofe8ppqiihIZNKUnc5c4t5tsh8stezrX/RfJWjwck/TUR6KfAscutX
FgcE1dHUb+u9mOawj1OqERkS8swiVG1B6PbMWf9iWe7aGPkwwBONHfrdAgahjF5EVXCjFIn3F9V7
CvvFi01AtchvabD+cUhWyf/Tiii8aCzlq+a7yLJ8Wli/bdVaNm7h7dOMQQv+15FG0QuAQ1GGx/Hn
f37u2GdiDpmMsBnThJb0d82/B5oDbi0tz0vuM5B8AAjzl6CwKUxgUj/+KFA2646IDz5cEXl5ua0y
yt+dvNIvpVkTChZ7WgDAmTRiC7Bp9snIQLCtVXKlSz2U4pU7aqS1kArYhn0wcjyyVkvw0XqKqZn7
QFyMVsG3iQoQFWaMF2AcOoCFNiVR4qsB+/FzFOlWdZvabjzu6jUcOMx5UogSOjR3bk8Erg4K8do6
iqJXrReAg547Z3cDrE/zGneShJ+2+rBzd8dNhvIaNDcSMalSIJVRUigjcgDHNsiMtNeyu6FQNV03
TV5A+QQbUkjVFnx3UtqRm9/K670RDquMSrFALN9wJNDyQOqOS4LKF3NVK5Taj1n3Te4/5g31c7AE
rWTMmQNoygKKQAiJ5E22QtS3NXZs92NyFFIgHDcpQ4rbCiJIMy96ApHiAwpNBqynBEEWNDZ0Y6HH
mzF9UYzjGTliYPnkupMOwZKzXTKgVwPLYE1ULbdK6gDv7um7yQM2lC3JbuZc6OPklqr37/tJOD72
dFXd1EzaWEPw6is2rRK3bUMR/ng1o9mqHusV1gbCqqTTmNcyBuEYGgyKj/QbNvnZ+0ioVDIyg4BK
S+ybTmY5bSWUczOnDsbpjNZrv1m3WeWAYBR4Yjkc6cNEzcUpMK9j+hQg5MK2zprJBJyZfF4jsHE0
+MbD6FpPFOF9peIbkzyQASm1PBjgQo8/xFMvllaETsNL6f6sTAAbY+e9Cb9kcR8qGNqK4GmvxVBD
L5fQ4wOZoPHyf0vSl9aPu6/BPtu3OwPP26PQkv4zwLHx54fxbPNdorrVYM0VXWA/iwjBwogQKqFu
cLLGNTtgi8oymRR4eYUsrZ1j0aXf081Rg+4vsjLRmWXX3NlZ+yZvD35XqDbjoOARheXCs2Pdzp9L
aXCj291sAjn3ZiLIN4c9TfUFkSWdRIeDW3f4HD3nPOeq+GoJhoM/+9G4YxW78Ua3ez+aTk/WS0Kw
GXlpUiySx2mpkHSIHngQGgqxNbilsrk+Unr5nLHCaVGWiO3sj49OcnL1OErVeFE2+vhCCBit3seN
pO/iuzHxwNpicsahHjORNcZKU7Eoa+1USOrgPIiFJttzfFoj1ZB2CfUwpvk7dZSN3CQRr/iq0YQB
MblNcv8BMgCSzW4NMCXsS9Qw1Siurfy1QDqB8dZBjAVtoWsaXATDyqBKsq3BVex1uzMa88zUAlXh
L/GnimbSoUczaPocXgDN11XRD/j9QPEKi/Dou0uH2cT4xWHiuO/cQUUyVPU6KLHge9L/KRo0fzgl
rCRP/qdOoK+FZ1lQC/DvlH8q6A+yf3Dj3y4kgWvy2bqy3OT4N1/w5gCF/jULkuzRMJJCxdwLliaJ
aoD4xeUJlJ0CqfFt8cObRyk8nnV6XOeWEQQ+CmvTl9nfEnlwFd2kI83T7Xdr7uXWoHi7dem+ORMG
cSqn5mP25ame8nNYQJQIXVYpuJzz5mN5DtTMpo1IyDYFiSUNFksQ8etEIqOSmfsBINV7AXTk8tYG
998/cC2CgarCSB3EyyEWkLBvnr20V1LrqTq5fPQ8kyRLy9inXddlNgkl4sNzhEn2mViLCoaZwXtl
gxwps/tUf65rRLBrW6iXXDKNGJr2kfCj0VF0in6AnQO9I4YL9jNXjm+QVN01NGtFvZHwYkH68SuC
KcpsjGDXLqZbonM7ddpfi96THR7IaAQ5qUM7a2tg23VONDcID1Dg0QO4rUCtnYEPslZwwmYaC7Us
yZ33uNIsuAtMDbXbjsKvVgNU7gPapDp5h1DjnrF6i1yltVYwjfnH5wP1Gw6XRZiCV8O/A6gzEjlW
zWe19y/DDOXuYDeXIh7LJCLtD6So/Oh/m1PYfWfXhmCUVnzw3UaqKp/Shpl8hBwTJRLsFYS/q9R7
3YgBCP+kgz/JpCB961fFHoAsDx0vw3eQncQi+n/sQ2fVMBC5rU4tcVmIewF1/pn7COfvXXcauFMH
5HBLdM8IOF3Qgh/11Gl/YMHrjMQQpyfDHFQUN1DzaLkeJ16iWa7YW/wNx23kh50drVJhG4gFNntN
bhJkWTU8RUHadFZC7UQ8xnt+CiqE0qJ+HOpFBkfuWAZdvorOWigph+SKSTIp7ija59cRzScl/ii2
xrTvuYDdpsiTNq/Ioy7Cps9q+w8u2zKRgvSQW/GEF5eyEZJJ+W2G5QftGZ4U46E1d3nqDwBBMPE2
BJgORHi0IZHfdDNbLg/3B36NHaz4SbrBjXhyIR5kAzjy20ovSnGTNvVpzjX7g0cub2fy9JO/ETbB
YmDnXUy0nEc0rIPyQ5Lad2llVTLHMlaQUYliF6uxXi3lmca1KWb6iFOEYo6JE+rSWbuskN5OUtGh
if3lBcv8IMKEEoj5quTfaKO8At4sbaCTCFrc4ZYsDUSDstt0PeL+kLjcoqIu4UUqr2H39wnu76ol
MEgU531kMySsU8R30UYFg+JQ3Lhw4Jpk6SsqfT7Tzn5qbyEi0tEveeeTdIaFUClny5m0O9Tz8RMB
Rhznxy+8f6yF61xeBCCCcqH0LEQBgFQVbzST9OT9547JyZdaGHbSQuYMilO90G+xV65h/DBRe7Fm
UKyWt7hzx6bnu1Z/lUpBKqoxCSojLWRuVo31prFBkXJhSFaJqTXLLxjp0L2ySaJEpmCnaRZbnUog
8aBZM0CFjO8ALLlO2ETvYC4rvToc93Lk2bhQCBi1yECMy/vxjXLSk4/plNUduLqc5AeiTmvOn8N8
8sZsx2t5Ts7zc5eBonVjXok6r8LcMco9sNPoQESN2AYxHbdNhdsnvf/O75zILYDKXE9ESyah+czJ
l2GGiIkRqOj7I2HcVW0TUBJHYcfkKVzJDE1ZtHaBqGtKqc3VMZqh60frlfQWM3VPHTECxCq/yTZK
dHNp9ioHMI3+De2gbu4JWHuBQEy7tif2W3ooAxanhUiBy29snJ9evSLaWEY0Ja4wpL4rQkmIiaPQ
fbRx23PgsQWSsHPT54Oa7uR9wOWKbf2vWK4u/D8t4Cyg9SXXQlNBFOgrYBImM8zkkGG8LXtexSJm
RFMHr9aUeXMmaTWszYG4UgbHOh1NW7K7Pc/xDd/lLfHikHD3wFVzuq6OMaZOZwjwNUNjP0Jh/VOd
4ozhU5bkRUbZVeCFfTEeiE2xQA7PCKahS3Nt9hkMs5NcWNx//C/ZxyQd6AVuv5shwViR4NgBPqwj
9hQPL7OBd7ltdK+7Ndo8YpEXq4K3W6f//raYGHLx5A+gLfgWLj1o0pwzuqRkx3Qoao25jys4s2j7
D1nInYSpr5QR/JDJ+wxcC7BuQe3WCg9Ifzq7QvoK/JklYgMZHCdR5/VCWLxMgwwu3eYABbeSjAAB
dp3MsQNuccEFTvnWzawK/CunmhJWjTyb+0FeR0mRk4jOgAJvaZMSwovihStJ1TqYJbLEcrQXKu5D
GGY/ahxdaaB1qeMdCGuFcC4c+kFhqLUCGHJnUidss6KIDflrqhCTXgq5nwkmGaix3WIOROdomeBs
i65UCTlB28NbXsC0jmnjvcHY40XoLhGUFGWE3BHjLwTck5K+a0yDTTCXtWuYSHXfswlSRYKhWby3
qZW9BrUgKMebTiBvreXECOr/LxW4dqWxWfgl23iSTYOxnib3DdyOqJoV6iSXIl7NJTtIxfm0CkX5
e33yzVoeqUGLINTTVsEYDhR+YS4tMSv/c/mlJWNffibZvXXsL0QzT9Eh2GsrncT9g3/CodO1IAot
NNoxbze4AyFCZlvKf/wL0vF1FPNAMKaPlGM8geNWgWRBYGS3DejxyXwYccOojQ4LYjkYW27UxwB2
/J+Ge0QHIbhbVIG2Wn7Hazkw7iC1m5AFM3EEgLv4TukooGlMEk6inuk3gGGv8A+40xBcOUvEXO0Y
Y7MSgIFo93Fcd86FN3SCEX1vuuSpbjHVsdy+17SyxddDQGOMxST7aTmg7xk1VRN85s6RGU9WlegV
WmSU/z9eQnAXqjkIc+6isAcOPZAF6o4B1cMJF0r+/jTioMH8sy66o44TZTvc7HBYaeLc0nzEWXz3
LfgrA4KvUOLQcc0y5eYXd52WwM/YJwt/MXiiyBcnjoWlDa5diD6zixkVrCH7n/EZiTHYxxrefET3
BoyhYUJ/S5I3B0rV5kczfB6tgJBdq9aEVVkk6p0GyB2r1cL4eE9EMtsZ/NsMT6bi5o5JnOYgVU1Q
+9P23eaWsKRqpNTB9qhX7pSRn7jpJ9n+BzekZdmQHv3YOeznKPlVzNiln3p8u+G3xienOCVNRHXw
WsxWc7a9rFEGiyZ3A+2eQz14dEKmPLwS7k/vxYQ4zPf/B6iNU7034Q/qNB7I1M3hdFr/a1TnEyvC
c/uu5TsdfCIzcCppGEUXLodp5C8LqQdiqKn1uB5MlRbl3E0dRnh30C1Jf5BhEtr/VNDrDpksCMNe
wHk+sFeARbZ7z2jzu9nfA1ztt3ag9tT2rCVaDtKi4uuv0lgH4xBnGPScU7r2E2+pbZTTl0m6qfOO
73vR3g6ZgsJ/VSZ5szcMYA0+9KKJNUGammM/UkWlIMoHp3Uy8T7zjeDzvSOMhhlK/WLdZbRg35bd
ffnDbXpPq0g2az9AKhE3piQf/y7spodDrBH7EjWfljbHCtuSUTAhZVrAOD3sUYl984vlDXvzbrI2
LXm2+nEALoJ8V8quQict12j19LAQYwd8yxRPE64HhknUfgu8fuX3qSJ7DrmKoNmbudyLnUTNURJs
0ka1J9jztIrXSAa4Buc8NkCymc00iCoVf/kctOMy9wcm21LsvfEAwdlbbMlv+TtaZvS8u1o/uTum
l4fq12Zr1DsDffJEb7LuTqtNqyfN121OvMfy5Ut4oAPq2LW+mPs1SltZKrg+xAODMkTBDQRkWt9h
qxzWHN89kt7YEGstxvSpsRoc9mErZdG0XytGAaB2YFqe+ZTAlB8Egv7EDk93dnNVLwSlHt8S8NuU
4u4sAeQ2uXWxZik4RHDhf6n6/1fVVY/T/pKTffi6kta4OaolSjYehkYPP7oh9oZoiWfTOVH/MSR6
ueYHr99mDG280W2Zhd7f71QbDkCxAUGhawNNNCcFo9fK236ZGbbvfHlQWDEkXw+5kEk1WHDdoafl
Mr/1GVRjk2CkIH43F0Lrnx8tluk8g412+AhQdMimSil4YraAEhM6mvbiJynA3jAH5WDc22XXaqV7
0QkVqLBMn33v8f1Bilg2/7HpXNwJsa8L46w5WaVER8qkQOBquoiYAO3j7HXNdjXZcPlnuWEfswps
wH+fFaXQM0vVZDnJov3p1n4eU5/KmBPF+aOL1xrUTQKxO4hb5Wb3wrbxRDrfpxbEjDoes/SY403p
rVZ/l7JT1FJWI93YYzeYUXGRQ0Cz3poj0CEmSmAs6EUFKvsBOTmnwwh9D9Za3C0Nz7OOynIxsQ2W
OfloSJobk/x6btn1FXV7f3deIMnlIWIRtrqC9xqr1G7iG1GJzLbVmSMENp5z6CdQscs0pX+BRG3b
Qu3sbrFsdMY4TnQ1urljYt1Ysq6tSa82dP6lpiX1oGxfPbDVSXutHAUPOBJN2aFSGTX356npS2oh
A4Hh3Ok/R4JHD5OXrBHsd/1hdNwboy3NQQiqjSUFWxoEism5TVqRCTnn3BxtMY/GzpngcEKBSliN
MQgX0o04Q4qWKcVCoJHIkvY7702f7PTF+wRIJkx8y4FYYEq+wwK2+M62XJigAuPrEtQ17VHFhHdm
OB0qY07auwRLwtBbVUnjIPL2iuhcDx9N+/giyiVclBFFUfrPGUpCXgzwGq8xDz+m7GHnOfHP76ng
vwJx62zphNQp3smXsnr+8ElyZ9R0w2eJDAOJfKMbIC+QelICaE6GPevWOFAbpAltcCxPTE07HOYC
gvSIiUGTLj2aLOjYMHJoQHN8QCskeB5fmSDsVCy7gNRU2iXsp+Jip7yHeXMzNANAGMM/Nw8IwPIU
oe6ggI3Csn52W+VLnes2xFfdCDKAk5uSRaIQ8aKVI4JP+b6ccdtIAQrhMwqDoDeUKmjRzNoxnJsc
wV9gK9SJIqzUecXjq+xdQMU1EPKHuq1QH+YII+Q9FxZewTxWzI932N98aCRj3RBK8bwAiPofIiRi
hxBkOz1KesQGv5YosnlTwQKVNAtVLqv+vJtD6IwlBKYwoEz/a8ti568Iifuv6iMoJi9sVxW2XH67
WlPM6+W2ifbOLcCUxEctUqSQkbP1QnZHwglE/57WsXBxoAioB9ViIxhknxpBUJFL88Q1YD/3DzaM
QhFLTJ4KZyh+Xkq/Eloboly66ONVqtMAIkVD8cjxrmHsiiyX7RewD8HMFMS+c5OpoEvMD/MhbT8E
gu7Yteeagwsao2TCQOL7g7woz/dtwnR78BUR0gmQwyW7JZPhMWi5Dvxa5wIcv/4GLc7IPtXVnBxm
tOINhrmFUaWty2xnQwYwI7dc7Dje6QqpE3LpscG+aINQA3kpwqYIcZIP7eDx6F2Wy9Wb0wKbAoAp
4XAQbI0W7mMHlQYLULMPrCsjWN2YvB6qKAck2T2W9D+aHPGOUQL17Y90mnZPQQ5C5Cq26slT53Zk
tjkbywceABJ2/LfjgZHr/pXwY2d4voYD0TwBZZwzY+9c9B3nhg2Ia2Zdd9xmMYAHhuI+gEV6IIln
B0jH6C9BwcdM+riwsnRGseEyI68oST6MSkZzw5QgL+KMxRcrSJp3peDb+QB/ssR+Ii4h3pe8nsN1
4ivInQPodpJXFMxgDSGH6c1sxWxAA6i0RdBvHOSfEDf0xHELgtiHVOnZkCGUHhCfpnecAKV/jjr3
vJYVFxKxtt8ednK0BcpetcER+xYTuSSG4dXvc1uY8HYxrWQndOOCLzNGVeJURoR2qxWGm6mdFe4+
yy+BBtdwcpp9cMDclntN3flubHV7W6Xg5Eskq4CqwAojP/GS4+oC8RPP93lBo9lPWASM3JhrlvFB
Nk/ZIRUCu30KXJCZn3vVS8OSA6puie+pXAbGOgxEPRKv6vL4q5x8M0nuOpzdpkWxwpjIIriSHM5C
3pcCQutKrvxsVpOzptMZeGGdxYTUWHnDQGk1WCZmSfc3MjfbflSHMY1cp5zFnIKZSt59CbMVyX6j
plvCWxVOSDhUPiG6THQVYplLYHtXPwCTNM2dWFzvvZvWdNOrcIn73DmvgRedEhimKzEEQykC3uGx
IlSD0R91EJ37TXZTxeqUMr+7lH27klCtzjhDleEud1KNty//r+6JHHpdTnuXtdaco2659tCSP2Ht
Nqou4GM138ctFALSyIy8TvfqREGEHOOaQ3Jljhona/7zLBwprKddBE3BEIoBryDxV1QaoJ9mbnrC
Aw2OByrBTw1O8ctkZv2Mck2Qdf+WSLlED3RRoyFnDK6oh6tTTeGg5nxRp2RcIMa+CXqc1kJCFFxp
piBJrf45xDiIdsUdL+sPi0mU8rqdNEsrlS+849ZldWDl1KpMVCk/buo62s6hHgHq1DHO2sUE6naa
p+1KwGuo01GQdjYhXmWtyMsE43XkCkTq78zjv7V5vJwOKS4RqZSpnVjJgnSAEPpM78+XuDAdyXeI
lnTCCJKzJZ5qTPuv88UYnTZDNUnA3W6x8ilXdYL4Gpti63o4LXa3DdFuXELFTQ4sJHPKQEP1oCKZ
IQ0Pqcu4yKEySL3f9V5mMsMvWnr04Kl3+PtvpgYKYsSyRt/vaU2BafO212G8Sbz9/BeZusp0K4Hz
fB2X4WTR5EQfoIkQ5od0OArOeYYjGWc0fnBv+lnzV5k2KgTEcQkT2JBfwyUa85QGrfunaPYFu+yA
2NiJXjNqq7YmzXbRzAROdQiH3pcmynkRjFuny2CKttEFXHC2ayrTeAV2ho+vqQcGkUr5ZZ+xWA2R
PzHAJ02rcqWmXuOOQwMATnnpsWkrL/hGgFhSnv9asNrnyI7WARF037yTY2X6G31QTwYRLoH5XEFh
YgjIkHx06qJo9kXPIq1hXa8GM2Q9zJGoRJ+VhZq3DbQ8Q5t/i34pihneaggXf3+paNd7dGTPzEBT
eR4lhdMjTx68RWyc0CMGRgnwGMiSzsS44lKGc3UWwHGGBx2ySYhqS0GXhXlnRRzV2kC3GRzUnQhq
E+d4TGwjcpQQIjqqAJPuDRUO7Pi4bLjPMO8RVz49M69w2gkW31CQe68MOUP8bRwuODIMMQq5enah
S/xcQUtREpRSGeh9zIoCIde9Sfy3FKsKDUmQpNqZPL+AVUpe/mV/JlJnppiAComMz6cHcjoLTre7
H/nXGi2jMvW+EYTW0CTRFVnq5f1f1XLlW0RH+Sw5ZHayC12tDEJcALQEQUTniChpdfslM2spF488
UbsSByZI1hRS74b4XmnfBsXJZ5hQlD04hG/8NDXNdJNZj9b8pjEGALu7aEckVu0VpGfRYYUPHgLS
ynDmPYH34Cx28NuupRgAsCQq6xM1NSK9+aRaJjzwzumCwnmbc3YVbqbzERe7k5gzkFvqI3uygPj/
gnxFPEEe9rXbTJAwdaLuB41iYaVpNj0SPxYgsi9/fHlUu27QGX7hV5BZILt/HvymzCeFTKm2nWZm
eW1GHJCKo3DRRKpwbCucxsifkM0GOkjeVnmMR+JEtiHxVrbuFP7WhF43RNjkkuFSaxW64QgB4BOd
ITw9rriKyFtrgrn4tBC5M/Lxh/5YccC2LX8Tbw7yGKILxU7SaNJ2yzddp9mqvnk/zCNVL+S25Igo
g2/N/UEPljhbRbgZShakNH2lLaYm6zN/H34d91Z4mZQKvlF7KaYaCLOJ7ck8rC43pj+gQVONhCjJ
4CZqQ9A7KRuvDhPZHy8o9JI4MQeRKiEFU1DfqnQ2a1tPG9HmgX86pWw8qngDgJK/fwLOUATBNHFU
ddVP7vPBVD1aBvPVf6HwfFLuTEc1LZD2tsa88ta/PCwwEQqVhjz0MddzEQLDEuRaZPcgGBeNNF9A
qvKv5bWybTvpSBPrJ1uFvR/8JzKTtLPHBDnj/ZKVaM9TPC1HU+5E3IByhSGQXNpVsXHr0+qoDRC5
qfqOYxGFmxhSChMSX5mvdtoQG4+bEY9xYlgP4a6P1KcyyAR1rvsE4WBV+QciU37wFPJdKh0kLXlM
C0hk8l128GlxXcYO5gj+aWAkzhwk4c3IWvrLa+3n3p3P+lJwY5k8AnUrjL0JiqjBo0yb2LlTi1si
bAXzBe6gahhXBBJPAXD9fbJGQZ991540N8BaoAPUF9gozN9axI2OD9YXZN3OWFAUknK9pPAF7Dzy
5Q7evldPXA7PSfU5mxZuW2OxdjJvUza983Vva0P+ny5NjAMsTP2vJ80EBHFcLr8lRjLQ2hDQe57F
UUCzbHVJ7d94cCQ9f3cFj/8EZixEIgcp9MbXtQ1EoLJNtycJpLWIVqz/rl8tsAFkkOnBrO38weG3
HKPIhwSipZJYZxbim5aFiZ2OjmWr5MhFWAVStkYVDAnqOEJImacHqpFnxhbSS2ssHP50UQxuRrlz
MqAkURN7rR1Ku+gbpnyksos7JnxSKXT8fEDbE2Q+gWRzD4zniJxgebqOQWXkE5z4Da629qAqRuYd
aCIb4aODexx9wW0pAAUjmsX/FL7fZM5lhVCWuXNTbNqkS8Ylj3XR1XaVP90kJtlpV2f0nWlC73tr
HUc77egSJMl1D3Q6jljsZGiwj3OggldCxQQtpC+QY0ihgCd683a8jV1CyiC3SufLGfD6ZIubr5vg
MXybX4gWQERvAQlNdbn5EKkudosm0l97ZXZ6chvvkMJPmvEZZ9LauB4SKGbmtPddoWehr2W+IEwK
YHG0rSQ+/FGxGM2V1Srsn6soyvITRyvt5/IMs5Bu+dMGd1/cT2SyShjkFk7qd9P/W0vv6+QSFn3D
cRxEBk0uXIC7pg36gNUempjWLT0Xiyo0e+VD3lJQPp9wAkcDMklVeVBt6eRCz5hRx1wfA9gs+j9x
cJjYnRC9d5ZFX4owxBNc9iS77cSqryd/PdVzF9m3iBrQ7lDr9i07HC0FLZPlbcn2di0QlzA8v9L7
AMSDnapWe4CgiHCLZGwBjtpW5p2My0fkTOLKLBgB+XGPE4IE8wY3ewDSVphlEa6aZ5BVa2hna8u8
es4RbPqtptiFz4j9z0/iWyGZ9SQlKZxm8a33Kj22LKenJL1xWRSC5fnDHwlFq/Tq3HM+MmoVYukf
h4XBhKonWlYDe877AE9jwkqTANWaMeiC0QfpsheSx+Geb53+F49XhqhWjJtRxBDz2NLTZ92Dw53V
BS9xNW0WLSLJ11FiVeRXWfbUKfDSCqfl4Nj1Sh/RU6uKh0zcGBIoov2Cp/LHNDSb6KunJm/MnGGq
FLvi4L8rsswoTx6Mi+tTZ2sl9v2mRu1BH3myH/incU9BoaZlvmLt3K0r4qqG4zJWOy+n/vXxFrRC
ad0t7X551tkantUh26tVbIdi4fb+Ty7PzzGBxyELVOLC8QdRNABfXeQkpnTaFrBF7QegsdTSDesx
DkZVXpZarxWks2lphpc1fV7spsRoWUeF+55x5/8UBheBpZnq2zcpgsqAoEvyR/hu0AayowZLY9AM
OxH7f4jjlrudopA8/S1kkTjuwHlZHxAcy1T/raYLJgpEsL7MDxbs3OiNmJMC8oFkZ//q0IVRt806
7DLpnItLKY1YieWYpNrudu4GCEOHJngu490DGZ3iOrqSv4dO9NGH/WGgrs9hmdKSJBvAfqws19wb
XXQv0xmpC1+R6T7tmVuZVlv4i0n/siNi6rF739tZNgBBR/e4Y+imzGjBr2f4bafE5wxo+aRjuZRv
Ka3URPsVAsJLKnWxNFWujrDD1as3wiyinQRQQ4v4DNBvaz/ajCbehAQRh03hDTuk2N1TvnC3IagT
K1uUWLTmKofDuOHs5XeABh+zZ73rN3BhXw/01nNU5SmgxGe8e/qnVRrjdpsIuiMMi94YaGM+vo9j
hqPlEGMlu1LCHsvaleCtK/IYRo64aaAVtNo58PiekBFFpiGD+fPdiZlsUS2Vfkfeev+Y4IjcVtvR
Cc/4ZeyMV1AAfwW0wra3kuK76RteSZBpTEbO4s8aN2CsWbmYWlqE+r7I6fxnFLwzjFMq8oUzmY/T
5Y9imY8c4O0szN5iiPddmxKp9RWWl06aMktdmsqVx4K5Q/EkVOqTtvREGChn2tVYj70fkU42QQNU
ltMx33t+dIwA3y8aU2WRx0Gj1t2B0XOCwIQSNyhdtRPZ9nckz9CMaFPBhilYv97OMcofSBAoNp2T
mh1Qtu8glQGF5efNyvH7R5DL1RcRQi3U5ojfY2o7rysTSkhkvKzTwpQB7VirEtO3P0+P1QFPSuBc
nSRXg1rOUE+oNC8DSnF8dFCi+kgau8xo5Dl3seNMMEmAnrNrKwdGfukeLhmZnP+ZKFVu1SLQgq76
T5Qoss+ptn1dsaBTvozUlCp7VEX1+p4L0PcIHzzxzaQz/IKD5kqi3GF6eE8onmdNXD6TnfFuB9i6
iVi2kdeUckbCYM4aPlCuJ9K5Z/bslNTY2EG/N2c+NHmQG4w3JxJuBdpgekdLNW1ZIl5h+l2ykKYb
giYUlxz7vSaGeJ8smK9ENPkYIvaKVJtidgYmiwe6GZl/LokmLv8vL31QLnE5hit0P2UWCaIIRoDE
W5JO9hYDM3UijZPpSKUKKaE+1uQYlVULfYuEZ5eeddKSas2hcl8BeZoc7IAV/A/0AUHlKEvwsBrb
FsLA7rUbcQDO7QdR25G77FeBe4Gk+CUTgknISsyZ5ApOrZgg3ruM8TLSVwfLvpPwyFXzq2ErFNMm
K7wxOoSIz530srjjzQOEciGGjFZnx6wzCHABKPZ6MtV8FQdmsFrJxx7hJJkU3kMtwX53OLQ5AOjH
vLzhvraDcvmpOUwGR+zlQ32spkjzZrIsl97Aww5mfNdVSfVdF1cg9ZyeWfXUVoMCt2ssymF3cdPy
IBu/wFaAQs23eajxsgoNrzW+xKQVovb4rJYxsmrk1JgWinBXb9w5brOQNJ+tmVitE4+SnpL0+nr0
lObFq8eL+Cx/Yl8dtkLg6OBkesRWJIwV0UqdFmubD2m/jt8va0/06TZOo9HKrxgfn0OxTe5LW3Yp
Nht7OAD3eDmLVqfBllU4tWB1BYqPekYQYNEfO0/cOfIghewr+ABFXdagTk844kqjboNvMvnV6vr6
15a73zfwteAcv8BYO98o+iaG5mfKFn6oQht8o9gKvczkQrsAn6lQQsbRp/tkelvACq7p+TTyZ1UI
iEmSeBQsol7tzC/suhQmVyVjjqi+fdxsLPlbHQ5TS7B+4T+dYsnrJYvea+Q/XWgMLXtBsPIpU4EM
PRCNFJuqpOU5exMeYXQmMsvVQlEmEjaa4LE0NYnYxeIbgNPPCv0IR3AFrJ+nzxWG3cUAQrO9xiq8
eMXNIjIEaAaVbKbl62jCXqWHnGzUSzET9pzlWcng1FCvYI7T+Fk30J2JBz3tcIIGLFDTuagNuy6P
n/Lhyha3NQkmbk5NvfavWzOf8omonSwtXRFA5cphnEpNHhEV8WJqymcnztWW3huAF5E+890ucQsb
747VXPEtpJgydlexlEyT7efBMwbReVzDbl04UQrCh8knchfD3qOP2K1R+Zr0VtkYLz0amBKF0bzQ
hjPkOna13vGuz64NUVNQ68RwZBRmz+YZTyHVwy5RkC6mvC0HZAkNMmhENj8+LycnwvavQn7yeNlt
UbWocwdpCZp5xUJZw3OMkUkDSBgAu3rZX5/JNF8d2TUEIlDShwiVFQphJuuw8j8ZPH4xGi392cQy
bjvIEQdA383QO0z6fq0v0v4385kAvHnKTlwzi/rSVnJm9PhbVOZ4WMznq0C1ZGbO8A5LJY7MC8cc
tJr+NveqY6oY2hOGUs8626R0E1SxypFqd594+6HZP3uVzC/LDq+yVyR7lUa25ZwFdUMVV1gMRokP
y9ZJztFh5BdMOYgbpb6C7VWfnYqazah2yunlJS5XMGHuH4DRGfQj7nl9aKS+Je3Ebo866v5BIKDx
FB7Jq/OkqiuGvTWUCOy7rgguY+m1aOsM1MSl7cCeUAAblKj8hnVHbUOlX44Mfkn5DbbzY5kqMJ4N
8UTUxp2hqFHZbP0IZpCudY7/Q/DcTCdBu6MgfqjldOhlDWHY1nUtQec/jflhi+qH7vPg5I18XHfw
hhyzo6Xx53iGuO2r+hD+qNIJNcMwt01lpyYxdA2tmdlJIM6In7HqdE7jIdbjNJaE1hqB9iLFbUDH
FSHrxRoab8L3qj6cD6QBV7XK/FaRj0srVWXBBrlPONAdu8ixIFcvv2zLUtryHKus9nny+gmv9Xhq
upF1IAbgoFDkzpZSh9X+sui+SObOEAIgWXqnR/FJ/IUnlJ+hHX0Ld+mM6h8hFjF6JxfD7kFRS/y7
NuDz167nzfiihyXcBo0kZqNawRfoIRAIoyt+OxYlCE45FJls7gVrfu1lZgTvyLCGWQAGB6qoz7P4
FaA/bMQsMJDuxzUOJp80rNfYgxNpsmf6biegaTtKe2REfZ4jnnOgQa8+lDRasj1vpwYLYut4GqoQ
eu816hr+dpARWmwaxUGQv49TmIKD3j6e9yjTjGQutVmxV14wzwMzmUfBH2snSIjB7jDmDf0Qcu07
tMGKOXuWhJS5vFvExcJ9NZdCAlKS/nqhgBpc4Q7v3Um8949DnGsMMmGqr88VCHq+sBQx1W0ctmKr
PgGmHQU47KExEpDawGtRxtE7k3HYctm9HmW2M2kAw2MmI/hYvKOwRmXtn/8OnvKbAmpUv6Fr3JWw
o2r4QIFJNlcFhfGvWTsxeH7t/oNUsYWn1wLTIDpvQL6G476st0vvQUbGnjdSGNGrE6w5Nlpw6Wjh
Cmp5yfrA+vuurweWD08gZ3PL89h48ZpGVF2X6Fm6nBUb3cBI4hDzYidEc5vDY8mmDCdpKIIIGKh7
nah6aEkWvJWys/TY5/QU1Bwa99Q1MkXHDpENynLhLwo2lCX5/8N4RC4DFwImjbUGo2QiMun7ploz
dlgkizVEHYe47sqEkh6jarw7EvCW+shmfBKl4j5ggCNniTAwTAtC82HBu5v2KVf/d3UL548SES0w
eGmbT+dk4CJ34xRvrERcCQo8Q4Pnm+1wU6iXpfJwspd71xVe5SLVYBXxaQXGppmXOhMaqpuh4UOD
5thV4JbIWWH9X67C4QdA0hmeha1NnYQgnC8Q7vsa0BqdR6na+IjLUuU13rLtpJp+HZigaqqJBwms
OgxalJh+8/Go+zERbNsDc4ES738ble5gsKK7adxIkXu/aHJMHWE0etl1z8PuxuBRUglq/wRcmt9Q
hMQJltvTeJuFOE1vIDwDK/jQ5BN11Uq38PX5NLJ+dKwQOMaWSHRG8qX8y3rCCVRz78mT8JsEvzpc
jhzUUfMDJ6EWlggPYdafIoaCsWTsoUBwc6cXvM7v2fE7JGXIJXWvnx9reVvyPd1Ye1BOA4D/qKh4
M1we67xIGimzaleXB9Wa8GKmiyu2i4Yt1Os14xBqskdMPIZdwdayTV19uGl7ATjLifEfSPNL8Nsl
WixqiqktK3VJ+ne6rdSRWpx6nLDCMvLaT+6oxm/E7fBKGa1R/KwkEWplDwJFxWSmX0rjarMJLDEP
KNIxzLAltufwSZGBhRIN/ubwgldTYPUC0UohUFbnBwijaafTOh/YnlsNeYnlQmgvXROt1dcGgQHW
4pUV4oJFDqUyfoJtp6nDoJyAjWtGKrFwVfvU//VjK5q2Jd/BBiq4LQAsHp/EeipQ1PuAj4hQhRLn
9U7Y5ixfE1SeJZRDnRuO9HbLzmM03OtX5UYhGJZYxJ3DfzbKiRQP45+Hz/t5gGfDyZRuHjxzSdDN
+Fbmw+/Ve453LcQYgKPd3gHGe4yqom9uMO1ECBooTkNw9hGuexxgIB3hoMO77p9u1B346V2YcOFy
JQl0M/X/HUARyGeTxY4GfXb/SJImONzcG0rTPkbEf2zO+CSpVXbRGet0rUuCpLXLXNZKPaU6S108
r99Ye5S19nzXMmL+01A6mHGiWWmIRE57tKupz30gNQ5fMr8AWz+BG9DbAbIBqRqmUR1Gzfdl36CH
/VtoY1SgmJdLeEXQXA/rxPMFenNHEtyxrJ9YVBaK0CJQ9GwXV0NTDy2oiE8l+7ongrYg0GLrnZI5
uZyllqSb3umMMH/u6P33SjpN6QDMbjdFq+LGIY9BdHwKQrH2aqx+E892QkINeBw7icxSkawu4yWS
CU3OMcdNe/YK3JQpQveu0E9js/ujtmCQ+q7B+A9MlqINNQERM3xjM+PO/RtfycTOCbEaklkR9UNp
n0No6uUFKBQVLr1HvRfKQLQ+Zqm+i8qUaG0d7hxj5PfuC9xOO6qSI34hL2Kb2u0HYJ1uJEQihtVB
1x61raPuRuSCGfkJONsOhUooEKHwpsB5WVH73YGRrN/Rt/T9Ym/oiP8UE6d+XzDTlQb5/w9hvz0F
Dc6trj86aS6vyfrvhp3WgSySKE8xutGu+zNu5SQQUaM+oLJG4hDLOcopLkdrcJMlomRuQJWToJge
SHG93uMf5IpoRL5gUC7tJY+Lfd4KrLPikAGGKHJbDzmXKcji7jsQ2ApN3zGdAO/dPQ0vfPRjC9AR
vQ0/eBu9Ez+NpkfsVkgciFP+4T00cDbW8ht5Ys9xytSFFSk6vvhixjbjW5Fxab+Mv927N3sVjYlp
shf7x5yC8o5EWTiKZGD7tFhOhMX9qTsQrSBZ8iIfYSomvUVpjdBtllUgCxhUEOInifx6VAZwrNzy
Mvw5XpnyCz4RsA1oL1iP1zWltyGgvsLHzkzGUKmnnWCIpjMDC9GK4jSSl8zfTSkiYDx80TUGANJ+
G/ZG0+hl7iZcgqExAEkfBTlhYkyGqmpvAZo5F05EV9gW7Y+A+RKW3q7k1aMtqsRxyJpmE3zQkSQp
hOhd66RQ5it9ufzwkn2YWL0bNL854EG52mBGOQ88TKhHq0kxoY8k8zIkgx6oRylfdtyWrH5F2cKI
aZN9fwWVU3VGhNun9Eok8rP7auNIAzi+3zRkpQC0tFz3Bty4DvTLcDV1+qMbIGHfJzNLK1kBTsqR
QTxA41syuiorfZrCo3jSA1uNiCD05t4nZODauF4GysgML8GLirigmWl6sygnKxERDKVjlTtE5znA
N3g1cI/sY50qMTZJFlUuZdYW1XgjxKwluZWGYsevThkMIPovNtEsxFeycRfI+rCBGrHfPA7YtT0c
vu3CvJd5IZq3Z2hLGvg3lJO2x7WzZf5FNM9o/zcFcIByszkVVdTe0lMJl2shRnaTOPS4hgPdzBlO
oKnqFOcyrL7lL0srGT6ETkB62pamJdjrc1MCYNImeFUk1EiFvaMmJPDVuj/FCpPeKouK+TVt6SIQ
cFVEAh04NyJAaByiMYqyCR2WXvdAeJN/vfy/yn6bL3jQ7Goz/tq2/kefvMukw52mfJheDtPUEW01
8Iu5HEXsZVP6MJEilToUtfs57bXDqXej0Lyv81pINCqBDVIVs9obsqxg3Hy09aOt4+3Lto2UV2st
4Irknn1PGZGJmqCDTNJQTTB8R38xTz+RPQvQX2ounyrxQI01Z1SBelH9z0n/vC0m8dkMyVEBYdD1
tKGOJP7ozmrrVirE5WX+BcChsYOgrei9f5rUvf+8ldPizg/swjbBZVCo934+PNAyz4M894ptxbT8
9OwDLSZCHQkDEN2BjAkQFoAoDqsEeWQfXbpWojePS2BJtUC6tK5Mv4H345qkuhzfVDsKtQr+s0V6
ljs8j2GHMAHWmWzbkZ5td/OoPPdJWAMRWMbxxoTOIq3rxRmrJTSbwGPuQNFRvIIoCb9cv1I+3V64
bjFRTJfhG+ATydfmTXoby2Mh4BPLWzjqWyYqR6Kru+vvV+GgJ4sWHtAEtzIkl0m5vOaj11+ewM/c
FZ9hqKTbkTReWemBc6AiRt0pWn/x3o84yDanN/tJYlsXlVlByoWv+EnwYpPlCZ3PgDzP0T0cAdBq
YvSC7d4n8pzuTnOjPYIsZPKKlLwjACLOlTxM0xtLuSEHW2iefYChdWj1lfTxPEX9nd4hM1kSGvYi
/xP5AbwsTQAZUJggCMejVEshFqSAnGOKvmfPLklo/Ey8ap3WihIqSAwkE9l6JZClylsy/9hWmeQP
HXY7EJ3Aj96XwY0BzEVJzoJ615db30GWTiQGrcR4t1++y/MVNltrlsdc2eUMR1Im9UHK0Tjq2TaM
LjAyMr4AV/atu5SbDlJYjaQWSrhXBuMeojL3g51tcGIKHLj4q4Iohr/WNreqiURjY48em5H/q5pj
YjOXrUemjaiV3VEnacWZjeKbrJzJF/MtiGrOCcH6FPib8I/UsWDFukhakTq4C6ph3ZEj5gjKDdDR
rz9u99bLjwbdZ5bzjgKtwscNzivdMf/pZgg8gfu4Rqhv4AsOxAkYOmSdg0Sx8yijfHy8U63e6TmF
6tKzuWz+M5KckeIWkb5KGSk1gb5DwQcaqA1/LTySszA3X4t5ifAvyftGrFHz7XPF4KaQsMy8ToJV
w4Eabds7/7WlrBfKWtk6GGwtq1WauHONSHZPpLye/fILQPkd6aeUJeXtLT9g7QnwRPWBVzgYKW/e
RD85hNBDiCAGg8mQvkPPPySg4j7umg+178S5WC5kN1cTgHfZ1d0UrYSpGRjzCEOTf3qtQ8+Wb1dk
MbmAIftKD98RodxYGhU682PKj5Tq7cXk0fOnWBbdHMgS2E9c9mtxNvrUCRdwuQcXbfdjOoJwSp2c
9GSu44nzUddYq3/AZ72exN45M6NIoy1S8/us8umaKSwAgiJtktQP1Yzo7iKWB8hSohDz4mgDM1YE
gZca0oyyUdedLd2J6BBU3+mu8F5TSC3COytMmbhb5Z7YGHDncEhKW0OJvOi7tojbBn4Z9Ao6SDLa
D/MzDhOwbWDfA0LusZm/uqr4ky2ddwePZTL3V4JGljZ/74AbfkkIJAnhB1SzE+UPAif7kyvyRiNs
1dKaBpy2cUIj5r0EvpFlpe0eny1PyY3vjN25yjUSh4N9m3UqBYqxhhCJ9f7DgXDs4pLw0wdhMOiv
Aa5FCCvT3HNTkHo3gVWrDajDI7VMREiVMykw3A1g3UATr2B7PV7S/E7LrHKEnE/mq/xPI7gzSsul
+eVfDyovg1TdL586SjSuwupXonnGlcK38wjCFpCEKCxMBFKrpmPjlHBiYG5I+DxAuqVbWPQ=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_11_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_11_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_11_1 : entity is "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_11_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_11_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_11_1;

architecture STRUCTURE of design_3_c_shift_ram_11_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
