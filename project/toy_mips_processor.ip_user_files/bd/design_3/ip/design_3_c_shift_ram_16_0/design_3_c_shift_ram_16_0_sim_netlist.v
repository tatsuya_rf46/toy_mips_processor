// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_16_0 -prefix
//               design_3_c_shift_ram_16_0_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_16_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
Bb5oXqZ4vlgzK0T48AJDvSaZ7Z6H2sjTPKCPyqSLc0eTzqeB496wojL4PAW0veYsBzDRDsZggVM3
DF74kn9h8+JsGBRWgI2Oj50tA1K9k2PFom4Dbt4kjjIXn20iemv/1w4xAlywWDxHWhVKIilV+d+1
cR+vTJ3zaMFTtf6x15PRiHVtHr4AFdv434aEfsN94QlAiJ6We0jGyIjoeUMX9yt6zjTeSE/GkQ76
HH+ipGPd8gzgyW3FLZXYvsOvUFAt4PGsaDqgL1maLRZ/eskk0uJQEDeWoKfzpyQ+wTfIuaod+2gQ
bXcibCHxf9NT5BjNvP3CLUX14BCcI2iVeU8AOODvbrZ34VrPVJ5l1IQcs2Pajv3DaJtKUzbJUnzw
PwNT1jT0YIzSLMqbK5ZMidMd9//NltlMWpYXkWRaqRv6cQITnbtO2kuiVHK/ozqtHuPNJ1DSKPJS
HM759mqjP7uTqoy7ELTtw/rdr4yChZwvxWqgeQ/z9GaQEEykBO1gV057LZS/Eta0iPu1MCRrwE3o
JWT1jydxND4PKi8fuaiVlC3p2ZsHlTPZ4eJX8Mx4b7SNt4hlyfKwN9UP5809eSRQf7xu/Qyc7dtZ
EY/944zRNDWDwtLgfhqg/1jwgg0oqaXkRKFp9cnreZf8w7xpTYa2lx2tXKZ0pze90TWDu3VZtuVt
q3PudAJRe/4e0lqZXNn7KaVZxuLrTAwQNt3eKKOMgiMUvnFV1TS+AS+z03s1eJazCT37lOMwepB2
gVcUR7FI+fo9RSuHbLdF/CHB9JqhSSPRRuzdSC5kqsIh57YmNvP0scinE9nNJgesdFaiATmr4rkH
7BR/zHNb2rlONt63dDvsa6Zw52YpaEF+BIxwSA/M73H+JAZ4vvN6Jw0I7cckhQHSfHn0+uWDkl18
VbMY5hrACoiEn+AsACOGwpViRUvIExHO6JLIdPeEJ5oL2CzvHAUImHxhtr37rR8PPX+wnpQYIZfO
G00lp8GAXRgI66SlF/OHnm+V32qQYzn3SzUMs9WxBR8bEwmpibHwsioqy38SBtmzCz8cKVWo0A/L
cU1SE/8lIszXZvool6AUqDbpUHNt7XO+EHemq3UwUUXalRg9IslvmEwnaKzxIbs15zdq7SqowCIK
i7aluFFlAbnMr6GNMyQSt1Yb39ly3B3nfoPm2m62kmL0Lti4//LsEEKrQIwVnuED+mwwR1GfQb2z
ViaSaEaIAjUSbRmLC0evEeowIRwBh5BgxbLLr224EZXS8yOOPFBG5jKvCtbGCz0gAycB0zb/ebl2
JiX0bW89IBzZyyf910w/0TuZptx1zZDhmica54jVUZ39Kk4tJ5InCoPJhghXYrte9uid/hsP99nB
TnyRDshxoe3WthfKbgjx0y6ZxTFzx3s7rz1gIw2NVpsBhQCU5qhs+BdmBTLJNLXHLRRtI2jtZQ5G
w2ckZ/832zrEq/kbnHTcyrz1+2jvkhQitNtwZ5FgzmvKTrq1zvjkusmo18WYh0KG0pNy/axjU0rN
/f/NsB3Vx0W7C/38Y6+NxbSe3Ioa1lgofWi6pc/Awc9MrIea/r1IhmKXQtVkQRNLBOuGxnki1m7z
JMAHeI96pg/We7oI3+8H28p7Wh5xiCczRGQZhWs47Lw2ecdxhHdWXtPDcQjWJznB6L30qYnEWs4E
88vCnKuG9QwmQT+Fv/V1VLB3Lz6YyScTau/3bqwE0BAnm0Tw5l6aWaazAqLrn9iaivAm1268LTQX
CFo14s3AyLOzN0/71yc3K2ZEujHasAAZQPhOXK1YKM9whDARSYp2Y+sZVf7ea0FZB2IRQpufxfbI
4GmBDgNx0oDchYT6WNjmyMGgA9PMGaXgDcMrndVRQwxMa+VeZHmYQ9hNCAQoSzRk0WQvOjI9W9Pj
iNThrgrOvckGY3GVA+Yg2pVezhqNo6zW9QyZk4Ofr+MPCWBdpzDMw4U/urT3ck0X3c+6uMqbsj3p
VnYW0srhFgwop1e9cDxJFuIQKh6aaOwYfsLQqFrI412MW+KH9kHWDqZCW9x090CjIFL8LU7az8cq
9AN7Qw7vq2K31MB0EL5N2dU2RFsP8dePJ2KfiL28yvccqPW/CKghFQVh7aX6FlGPHhtk7RagfTTA
9MJJ2EdiGiROe8YKgq9R98SNGpmVHzTR8os8Z6ZV5XANi5RyN8jfp735GA8ON9srwAE9LrZ1F4sZ
vSb7h5NN7iqte1JYY1BAFfxmxnTzDAx5z4JnkKV4PSdxLhCvZuKtcoduWOO/QISs4Yi4xjT49Gp8
Cr7mND7EQNMGu8LgREbXfty5O0OHLYlDIKKMhtFAyMOQtuNvNIMbz2NE/zQk+Zj2W+A+FyZFtVc3
GK6Zmp7MehDdrffegnId0nZtxdSSDcchW34uhne762dQXjd+2ET1hRNX/aSunIJ+/pRoLdnyfjLn
HwP9E1Ux24mhT5qt7kN/TVl1ss83sI1YT+UqMuDwDYIW3Q2aBix1hxMlvFmKNkkqX+ccmnJXA1D4
2ABwMxc4ndQoYnuFBsyQ4znHjJTVizIkTt6LmXFJcWCDo8vocSDam8Fcq6y5Yufwb97qdUqVcgUJ
orCQDQsBWD9ibzKuug1fkOyFDBGMPyUip8Hq0+LY2uD9vGHEGHu25sGy+itlOulczOVjkNA/RNbR
dc1oDk+xS2U4D1jOa43tVqtq7bMy4nTNnA/xU5b2/Er6eTQvVoH1Thd3G3ngUEy0HcL8QGrAseUP
BsFhY9UAJf0Gked3lFVNf57lHGPJE0CB7FREWQQHf37qQuvIasfE6Anfd7beOShpJTKMmpgO4A0Y
+yfO7v/GjShf+IJ24l4/mQPnkyAClTkzc2OK/61bE8rb5E58MfmpR2LWNA947H36rh5kg2JFuBKu
nWHdwLhH+52s+2NAHVnk1+2w/HTBkJ0ecum6ufl8QYl9T0ZIib1U6AnDpjyrrN642ouoHnS64xGd
2o8hclE8uq6mBFw6Y0H1PFH4iZVnKZa0UIAtVI+2H75dpVEb3O1GRGtcrsmUzTquzaNCyM0N2t3Z
5GUrEQA/5RDEj8ecIg8eJZDxkVr/t2VScxlHr4+3uvE8DD8tsS95b+OWNFXv5B/PAk1deorMneVs
mdRdXK25eY0EGSwgWP8Ohbd4++zUnbRbi7y+r4775mk9Vjh5C80rgrm6PvWdkB+shUygS4gCuR6B
wNeE89z+RUjaPu9Kyhs0rdOZvLzpYKzokeHPV9u+wppwcrGzTycKw3zAhL3feOfBvTmI1ovGKfnn
TGwQ2r+AwYb55yuVuT/3MIzbJLDZ0SUX7ZCoN/9kFDMblrJpKOcJvhxm8gSu9DsvPiklQniLKU5F
FjLxS38cZt2/AFB0pT90iTEQLjdkr6cQsPN1iI7g6zW9On1mRJGVIQ3BCwL0RpWbaN7boH6ECW/o
g7pzcpoV7Z6QjrWi14qEKLDo2DUmCgYXfb4U5/a2sl0/2Re77jLzRAJ7hq0LHdHwkd4c5bq4MEQb
prFLP1Nk4oAkbnm3/oy83FmQSK9f6IKGyy84osv1atEXwHCnUUqC+eVeUJlPn4p/nJ5zP+eivWwH
CzGXQGOsnYsqhHR2iDpvhZGyZ0S24vB21/OdekS9spTfa1oabitGACDipdy47q3yAkC8keX0igCn
qhMMM5AksgluJd9ZQIJYMVzNsCVv8Bi7VvvbE82oqB/bKd48uusTD2VJwhhkCh408FwX6PmERi2o
pIeiFYC3Ktzvrhz6s1oEFcj3Jv4aNnaUCetw3Pl63wKedT8nRteoqCluhTWqmq4Ot4d6Hs+bdsDm
/tjD7nRcMSV75ORCjG9vpgHKRkOoK8jlUoUZK1XDmBj8R8BYaBA/4lc3Z8PEebMGuYpTFdC1JAc0
Wx9r7UYovghrYot+Ed+t71y1f95iAk/SFtFcjtrdLHFGbwVaZ2VBOGEx1VB4pKrAM2MGXXEgx4Mz
VIEFuEgsw4GvjMpqng9+O+EPT4wMORas/4+F5XRKWQsnHzOsBgeLnRmckg24vV0EB8w8xymtbJOo
RqCRTpjG5SdFQvOBuRAno8CmN2b4fDHFIo/7SK2GfN8QIuIfAphZE3bpVLEw4UWL+MzBBiw2HqqH
IDe6YFlepdLUePOyyMjfTxFUy5glUb6UjKWNsP+dohl6xcs9m+kTUALSuTbaRjmdCcS1sTLnEqjT
j3ctQkKtkiPQGvASD3Rfgqzbzj+Lf1SR7mpWYOn94yykQawyYNT8zWDDu7uiA0bNnv64ZrnefnWR
7Y1MVsMONgkowkKkvILfjigMzuv2EdcJz1g+mcvhVqi3YXi8T7hCAClQmcUDDPg1inXA1338V6St
hmPrAamRcuOe5U7nnOBCmsB+LpAhzVgddyDPPXaEOzpj+jbUDXAcFxXRGGgyFaLdNr6Q7946SKn8
WKyNVyV8efd5hjT6HhMY0B3XGIAu4+Cf8mA2X19qN02ux/5lO6tOcOjQGN67GWpg0BfdlUCiFk9V
qzTJapYOS7DQmQRLf0woIt+EEmaLVRUEeApCCQNdX60glOCQrf82O35SCsbTe5+XLaoTxyUbOPHF
ROmb+H6pBm26nlhRtTjnmXzrTdCzYidr4IYPErJ9Z0zUwUtny00lfb/vB9OqeQsx3QZ5N6xxy/4z
I0yCNgGw6wXdNhRWpYFyZD5DUQlQ0OZ986ObAuS32K/yzo3fLkxgaVO2H/pFGcY3lbZYeXhGOdS5
9i2RgzXglhQbOIpsbFKog99ojkpIk+n8oIQepxqzq3LyZXhpCCFYokN+mAXNUDBl57AzrbjlzUe7
PqAU/g/kgE2BHwWN4+b6KYLgsTvBw3+Zb4EolZ65s88gHuHEDe4gegzMChCFpbi4c5D2l2DDA/oL
RVyEFphAZzxiU4EyVKokbJiqjFq2icrbeeUKu3u++etE05t3qKk4kiPd9pnX/iz19BUwV65HfgWj
VvHt06g57kDxWfm5Tdm5Ix2ZRmcVg3PteCU1gf6ED/U3e3XYEmaYH5QiQyTqwsMc00pBTn5cSf7L
6sxaxyo5QN1UGCjYlRKJWsk8g9palmHETMj6SNVMQfofRNvh07EYvVyLN/AUCNgvsPopQXr0tSpB
GnIzVZZOecoT8so8QFXw+LPfZtlgHnLb/8KvUfrBb1+0xIM86aK5AMQwt7UEET3e6h/WhsvN3wrs
tVyoI7X09YbJwcZeQG/yBJTC1N9HFO0wv8br2YR/lWqnC8iWzmZYKAfy6ofObbpbRECKgMuopW+y
95af9Tb3Jc3iKxFL9RmTgTtk0IOfN1zZOX2bvFMtodx9cefqAdfTwlI9I4cfuA7YTonkC2F56gk6
dKSuUPkSalkFHRb5g6ExaRvlxKLj/zLZmvlIP3dVt9D16cBKwmK3Lr7j8VyT3U5COAtniXYg9SDh
wBxdZvD01hyKKK4Vitql6GnyUjuJzC+hOvFfp8IIMKDPhreA5q6ika7Br+P38Dy2B1S8Kp/h0joy
bGt5OEzYNyIeDLT/PrQY2hwM3/oV17clMTFMKeI6y45jYn0j45gfCB8YTjj1klMZU+JuRf570PI=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
