// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_18_0 -prefix
//               design_3_c_shift_ram_18_0_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_18_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_18_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_18_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_18_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
YmpnNGiKyWt0Vqy2zmdo1egTzTkwuKyoAfP01ncO6b/BjAouT9OdWbJHT0/UF3GvCVZWhjY2vLM9
FulOFwHO5etooEpLbXNFXD1QFi4cNU6KzSVK6sId+ImaDYaCLMkkBxyK2CM0ogDEZ81wLcnqG6qJ
4O4dilfxxc5R5+Wh+3igUxdRs6IBUNbS0ZJgiBYH0xdHpljnKtm7tytW8nZOjp3tsBzkx20mHUCU
V3EbA54MLBGqX4gwNx+/GJRHOr8cBoK6A9OF2jRKztWltSx8eYZOEm8hwOP9fx1FFx9g6wlYCu3P
H6lshOi/uRNAoORu2SKUFguQqeBK2e54SJHf9WOBsbcp8Cj4ri/xvs8M0Js1ha8u0wh36SPBu6ZA
zd4Vs9cEFzTl+kEI4fYc1oAZYJxQJJ2bxe7LqVzeyzCSx8xOz5MHWYWQ641gThLEOPUMYOyWnNeN
OyETRMzqut2hOz5aM3we7L//0uEs7qjJIHGNb2wMF4aea2tMRbzVNauMNxPduOYARabgcVzJjW5+
/8IcayFfK9iVMr1PdhtBme6+VQvv+zfnEKJnXdXQNfEVuOufYhCIxSXwMKtOVYfv3vxxEfUNftmE
G8+CLUvsWrr0MLKpB6rrx/H0ZsQGEQqeEep9xcEOnK9Lf2r1BUt1E9sH1IW1YkOKI3EjUoqgZMjY
nYPUBvgkU5cnZCE2LKan102xnBEW152EX0moQSP5AkUYszXfB+7+s9M5RmeSv0b5xKxIKcHVtsVg
rAqlkLTq8NtmgQT1kscErmfB9z5xI6RC3BMWLiLrTmDsub4U5PYrSXCBG90XTShbd+HCZ4Muxt2q
zoaZXybF5j4hP7PFEePZWgIGNiOZmZWYpNCYe2QtKYpNtb6bBMCwu60P0mfrMX3dOsveLenZh2Qa
n7m+ZYGXOZhyXCB2ztiWRdTvLXbBRk0PcfLikVemituZ69IsyCP1CJM47ivYeTSig9GpLpIbFLOj
+BUZyqUIQCRtykcO40d6QNXTuW3+4hRF8Gxd7XJgPmoAWlzDFf3PORFcnIla/yCX5wtoJEsyIXxn
14IqW1G+aRgu1eiiRMjMXUR2tTpy36J+b+xhcyHAKSqSF4a12INxZ8YRqJ+KI45NviXhJRZP9JjY
sm+xJ01BQ4ZNUy6veFE1vG7Ramz2LkI/DwJ6ESVgXR0RIp3O9ypdN89AA4ebzKLC4I9yUNCYAmdB
9y/lyRHiBuq2Xu5Cu8SQgJ8IRyKV5ekNimz/R+RILpKvvy4Xb/1xXuojGdSdmQCQHFRdIeLV4+o7
mXPAeOX/sKmb+iL9h8jsqLBm1U9kbrbC1//F1cUSBqbk+XM4XeBgdw/NQAB0Qb3SpwBHsNBzOiSU
0+L+PRKYzMMjR1OQ/46qvTSj/fGE+JX2QUM1yYEvIE1OaD4yuxkObTXW/IRB2eQPYgRP3wXRACwV
vhpxA1TZ3ZXcEPVMrQDdRILjM53BX8lMm2MMSrT/v+jTT0NKerY8M4w6urD6qcxgMbi8B9mhKKHC
yzO4VHgjt/cfN8W2JZsuOj8xHN0s1ip5KUlYS/RLls8QxcF089b8UIbgHcvpuGcKC1H1h1jvuZAi
Z1mEiFhvz3+zdKJ43oMxcuue83Qnbo8IgsgXp2pTFeAcshumasZsIqpjHO0bF3NkldEGzC+r16Zc
TQzx341vq2/4teYhy3efvjCnaaib0q42c1dPB7H7ZvcY5EI0DxozXpYV22fbHh+2r/8qBUbj8eZn
Wn6RxsCUuAo7oQqJEf13hZqE6MsZQttDrWZCJ9F0Z13CUHGAbh19Nay7kZuHT57R2y7t6BU2ghOU
NPWg7e5LZ4JAF3JV/hD4adyvilrX2REfmJRoRpVmhhXTp1UeqT2FKvfXk6S//11gTnuNqI2d7kZ1
95zwY2QzLv+IgjgKgBWZZ7vML1X8EgVKAQpt93xSsfoxRipkjAer588S/ZdQikQpRYlQ+XoT72tt
fDyYJxxN7Z1TMQ6UN/vRm/xmuO6C+gwe/7ygnu45uzQc1pP+38EoPucTrLzP47iBNnZ9wgfdid3o
Sg5rk/cnt+MoJrqqJsNvmVv7sQx2PVs93mggXlZLM3WSQ9AT1EKpUZuez4aWJvmz+WYmbaVFQ/Go
kBZhCh+bAq2ApgGjJghqAuap6Y4PyRX4DzGByBNfJImrn1X39BUtxTh989Vh4DF14S54Wl/GCGBP
E9J1sdRvWjxZYv93Gp2Na7EM7HZRgBOaM8yogLftOHMukLmYV+gP4oBOPhmnBU7KTd/Y6uhQBl8p
AncQn0GRNl41ImtGQqvNkRKK8g5O1hLCKN0HNxKwxUNK2c/PcC9dqMuM3M/RnWvWUF6xJoU+z/80
glKE1TSbemGPhRnFlZQs/Rve7euqXRtnrE0ilT3/laTdvBd/Qb2FR9oUVguBfGfQI1VGfLCY72+g
rvD6Wv6p9oEqA6uWXFWXUFrRbTxtX1I4CgRoR9++mygfOLKUUFOFQ1omBf3IMA8bTPx64kg2gVyy
xNvrQW6XEQSKKyXRddcjqn9dFO9U9liJSyOn5jAsCYa/2vhEkHQuPe8dYLBcOp90esSAH1QhFcah
KnOPiQ534DVlWwzD4X6BiluE4cJE93Rfr8lowiBwyg98WjKgbKjdWBQXPYUQMIAqOzIrn/J04EQU
4cw4Sb0eZEMW2q56QG2wyiYc7nLiDwuJcZzNbh+ctnYkl1yjufoRSjjHTb3XIw2clJKD/NIsOPO7
B/+5RHaVqFCQSfPNjOr/p+TpLLHqkIM/fIpmzR4wbLJd3PGb/upkbMQSDspMhbaKQr3ADHFDVeVN
+z5eccUt4/e4PLPhsZJVOwWZAeoyDvunGYc9azlcrU09uqK1iG3xM8nmtRsmq2HWBFu0tH1unEQl
IrIDIdZ/KV9pmdxzb3iD/urh9eylq076TyyEzT6NOzZwqmptJebr4kfl5Pb4hc1xUyHlfOfA2YiF
OtrK3hDGx/9aU9VOX/En4XxOcAgGhxv4VwHkSEgx4qveUbZ+W3rxulW//O8e/A4OShRsZPzY3WYa
I/OPpjS0K6/rG3jQH5PUd3mshcjTxxN4D/1RZrbz08CPjjNd2G47s6hsZUdPG2hxTFVbpBIHjvVC
PmaClV2qp4WGMNAb+1MOSBAwnsPur12pe15grV4QZCEW+9+vx4bRXBPOtIbFooUWhqffgHu98gzV
8mHDpB0morHY5vl4OGfH9jQ9eds7msX9Vtj5C13wLI/VVEr+19jS5l1svLkZqay2srEyiAYH+N59
Mfsq9FZmiD3BbOb8MDv+Ie+NW6MmVj/PKG5hQyIajP8agEyQgTPJSLG1VXNILu9dXAUk7ba2Xio1
cWkt44I+23INkBDJhssUQR6Lr3Lud4lJOS+jlqwapEeBv48DOETBy5OwLOac1erg0hN0d4IphimD
ZRunfF0ip+QwiKb4dvTfN5rJj+nxIJtCJm/txz9tIVXD6So+M/GzHxQbjUbXg0IcStVaUToQ8IN8
37+RA+KUnSvTlnqdoPLWDp6Nr6xYfDKKo+jdL4TRBmfJIWKJoySxip//UhIxMuZ9YD0mwPq0+EkP
Ovfo6eQMvwe/xoujkKIjPjqxdtRfiJujnfhKfhf2NxVUF6M+DY00wvIqZTIrUlqHs6boZ+mV1j51
Xs22vYOLq2XELFBM1xO8dHm/Nrp0Et8cOI5hv5qTuVxyx3yVUqLnpUFWoywVjHH6YiEyGW8E9B1r
IzGD8/nz66jH04skRB+ZkKByUOKuJCaERmyhs8m7k2TQyQop4ZNAAY1uMIcm3Uw49WUkiR5Zv73l
SF626jjloT7TQcjBaDZlKHhj4QqwFiQ0miHd58DbK2kd0B8CIpWBnhqjMdZD2xdEhHzO5M+V3ZQl
UlJ9LSyNlu8PpaBmw8XgAkuUxeTcOnMggnnm1obgVfMRyp+izX08LBSLxTY4YuqgxaYhxJCHEbPw
UXHtXJU3/KX3jC9v+NZ6EIjBoiqs+Fk+F4/YLpVNDq8AlSSxwGwjuGGRA2VmsuFqI7UO+t6OpTG2
Im0YJ3st41uwNzG3YlmiPj6Uu2aZwPjBJoh4NfCE8To2kEBXy52MWZr82vpVVa8aGYk2ozOwLikF
J/maa4cj9iviTeF6RgcR1VnP/kj+ckwnpgYrUWLBCSLNI3tsqYnkOl2RJ4dsgxDWMFGW8WxFA2ol
r3nilVlhFTcQFMnhqHJWanOEdl6x5gw9N1z3v+Cv2f712q/8XAljxk2J0BRQziK5LsI5sadeour7
r2nYxiIDizw0UYlcw69DF5hqZwI5jm3B/+q6SkLy74q4rn2dUAtOKVKQoWiXNkQDQYgdszmJ3fro
lUyfC3UM7tUlNMFYSDu26mPua5fsU4BbS2SveqY/oEyDhXyKGux0+hMDn2A78ZrDfW6rnJLGVQLZ
30Hg2QHQ4eh22FDZ+1L9+1XbDwtCKw1ZukapO1IBzEVCBFQq/+rPxVIh0nKEwzOIbs++UU5/UfZ7
z79Bi3XZkodXTU0edbII/YlNGhAaG7jesuWOc/biGRm1HXzmRTj4YaORiHpZIfFlbUChhn0igIjR
s2D7imcV7qcxwT52JE+TuBhXWSgKNd1i65QPKU4Ikx9K1mbRtR4kyfcUx4gxnSwUpGquHdOQhVzr
Kacq7RSef28ZE0m5fLjcPxjxuLY0CGMs7uZu+XjJGrqGNsMGk4wGi3sLVXfw/gBkHtNRNw7gqG6H
FLxNFjo4jcGn8uTYGmIoqWqocTbJcGzvzI3jxRRX+G1QZapAflH7m4EpqHZA6AFAw+mg6keWT/Mh
dPidEn4GsIRi2LWLNxT8KGMheEoKxUA5IJmiDjCh3O1IFbjRYJDta39V/9nwc5BpHNypH7Bpz683
o3TqneXw3UkF8AB662zIT1/wI/muWw5sa0BlYccu1gK8gVYtx7vkCBNPMpcCA2xcRIvBS206FDD8
ZNiClJ2iNtw2WxNvdgeCyfyo36QwcGl4Do+F55hk/fMatKJNWJ8WxSdesX9XmeOqwb0d4TdMbKNS
tanuTERwuoS0FaNUrX4U6opxaxptAZ64FPqcyZZKYQoaVPTpmNpDx+aAmjtn3Pry0p45PphhGzLr
1U2EastWU4p01Rfx+Q0rDHMS2oNpvhaYoCUZf/bEihXOgYEsvxXZA9BCR3xxXYERe//wytE+kB/y
TmNcDcE1Sjm6lQVMtNwSZkHC6MMiLMBYQVKXiQQko4E3JvPcctsBfTX+I5Q9uuXLrMqHH0ts7bZK
mOfhXQgPaIleqyR9CBpT4wSNRYkKQEJX4Ac2YYsiBiBNo0djFFiyMv3hNMQXB+4GTdtPE0jg3W/N
n+EpMVkokQG/0gTUigO090SwUTMh8voMdm5zVPmR0eBMWWIsLq1F17Um2XDouDCq1bg6gyd6XiE0
ItlMm9TbcQjBHwwgB9kMc9Y7aqcSSrB2j9hKYM9E+UnyASABa7xeEQNxpamrlUG4CvascEoSjgn7
h9Bcdydtt0AqGVxf9s5YYR6CPYbf/aPE0StEBLN+RBHMDd2W4fW2GxRMB+/nP1oCJKjhbeaFPIzh
fUUftd03HRYKgA+NZ1mKhumnzEGoEmfGm9cSWuL7rhI/RlUT8A5JwpsebccQ8Y/jbsrStgCVH3sg
kT0siMIjoCEHJtB5LKsXxi0udZST2J6w13mN8fn3cnMLSTiGZcb9OkCzm/8rhlBjWYvBzMCT1TNu
2tYyZiROihVke9vGpx3sloLDOgh6mG02KebvYscSA0fvJYTZdIthKmH6Dq2L1CalarxLCTShfB4n
62cFzl9gSDoAoDBTcn9l8EYfS/LSAnTLXyvRdOTZBgm5lf4WV35+jUQUNsWRWDAgffdqelJX5ook
c9FHhoVUL5/DZ3Y+IwEYSXQJdABpqfUCYQASvk9ZRqd22rDL8flUoOySsF/MaKnQJT7f4WowZX3k
ouXu2qQPgDqB4mWSvKOjoIcT63wcNeFFOYvQxemAe2mkvC87JwGZJPr4aFkUOdHuCKSlUiZDCTR9
ayz8Bq35c/6GO4CTKFbZvZHEGmxGuvdt0pG9+k3+SBgNP6yUROnmtjBEBI6DHdlI5Pw+r8BI9aRo
J8u6UEfFvyx7DN6bZHHYncPb3DJlyybIKvav8t1KLwAZH0izhUTW+b7zLCsgOBO64t4t9NUAKKbX
qfhvZ9fsQSQPIIvBdGNiMbQpfurqumbyx3JwtF7s+iNel0X+75BK9gRO12A4fljkloBbdfbmFqHL
jLyib8stcN43bsAwBHr6KpKU93qm8AuS/yoCcnnwhwBYgcpZOMHlOKFNhWfS55lS9ZwzGCKMXjHY
zD3lqlY2U+vPZ2DCW2lbspOzny0uDK3X0MnyHmT2H50sr+FP3njnTkh9Tlf50he4LsHOAwPXB0x6
YPZyg3DCHp9TCdnPfBag2VrQeE81ixiIGRcbeMjy6K/2eocPUudPSrauKvoCodHyY/FvK/i3U1/+
aGQDCMdnrvDsjgZUkqT9CAQhDBpL4QBV2UVpR+w4WENRmz1YSyssL+tQyPtKkElwSHNYxf21ObED
ftVtz70hfHvxu6c1Xj8eBu/D+HLiX8KHAnLyiSZ0P6xKQeBZwWt6qNmrE8t2rKNJ+40KQoanDxV7
hqOYMqU1OSQq2/MF3MfRIrqx8tCuPiH7niQWez/QLBPIbbHxxQxt49G+BheiiLFOW7omVlXgVRIs
bE6nZ7y1g/fE9pPiBjKzNcTXnrRSchjAVF0r+ymQtDc+fHRDFNGkdHF3gMRN0AOmVde0F3zB5KNf
uzy/Z1pl4fxtqF7FhFp2sfmu8luC7J9xKQJh9QCTTjleX1qpUmTDAGvkKs01JjV7eLSm0GYBYZzz
EcMVc1qnKIMTrP/vvuOPE51omM5Ab84SbiW8LkY4ywfRZQJ7KNhRN1LkSop8OIRkxNLyYvbqVQv0
zbr3ikeADZseGRLWEb++R6qGcnKEGlm4Dvsem+u6JWxLTS2lwxMkMVpR3WT3BFDfRWhEqHCf2DoK
2BszmMaT7+xj3LG/HPfmFadxMtiVDvEhCljdeVcUILByAB3i3QrFA0SEkHViK6//SlkK/zJyo/Nz
A5qpKx+/AfTG0aDuxwaGVlaw+VXibExk5/eJlhNauk0qMJwSOlvXONqp69Ms9jQ3uvsSIdfvzQFg
GjEHUghAEus4MIw8IJ7h6lSp00kljo0BgTLu6Nm1+vzO+I8gVu1kdKoauQ7cMr7G+B5Fz3Depja3
7jJzFhcRAv7HJIn1WcPY09tGiJATtaEN7njZvBxQf8/OzuiXqQ7+fW1GSR3PujxM72KvcTvXfuYz
scg5JvzpfLvVA9f6PLrVNIv4bOonm9PVJkF5Rn1TEDd0MZUvE43cArj9cthKDffMFAFaEEoE9BLn
mKyelVErv9EfcVQRwvjJlDPfVNbXD+Iy9mG0dhh3NKIt0xTqQNJI/ZxLbfinyd3fTEFxD5dJ+AR+
U9AUfl/dmFHvTsk8XjgfKoMbnd/acRnxovumHmfHtglDYJL4N9gtVF3e6peK/1yb08cfZIWGrfSS
ahEh8wgEn1GtJ32YJB5CdLp0RKVhNJ12eeH3CP+R11nY2ANQaKFLCUxHZd8s+wUZPDzD0/mU2ZVU
uxuGGSJvX1x5ymcLSTEdAYxbZH3L/3wJ2ewcP77ZK1CAoTxCiLgpAY7/H7Egi8/2qaRWU7MS4l7t
J+qqbvVYByafNdNALAnX7Sb46JTUQcyqEDjhbB8JCL6f4kincX9bD7WBg/1mEvL/Tk7ZnitcS8on
96I/FnnhRySneLHjwzp+tdoPFv7AMg5GhimgeLKDj41ZWLILX5jQcYdVFTGTOnMkPbU9rtXafpCr
uVVQp3PZtKzHD1UzyDtZlng1aDBsRHvCuYmfX4sdYg6xKE0M55LnrG4QR73oW1zKdi5S5b+Yke7W
xqXKDFZ6Zo+U87htu1sayrVl1y78U/czNJbSP6KLc1cHmkNIAJ97CMdSnos2HzJYHk+h74B3xj62
Si0Aso5gbDrPdQEa1Bok0dX5aA6YRT1WR4Zl0FRULVQMDaXO4nUc89qm2vlnncXyD/0SEM+uQ07j
ZYwXHUhQWojI7wx+5WKC/edo9BHE+UTnCd+3VjE7CHyXx5XnCqXl5/kvVkIOlzgHtZ6MO9ksglOf
sWG9w1sKR7DftfdL7dcaoxOBWe5ycuMCSonOiM3xX+jx7sgb+39pZ7FZfwrN6c/AUHFO7LgG1KGD
uoaoyWpoYVz+W6ZEurHgymM3MkGh8tRXXlSHObUgxHOtMnbB+nabaknRzoi88S1/Nz9qjw6zQ2dI
Pi6NGwZfcRLbkFgLd1RjtgVeYNoq2rQcHIoMZ6ZiQxRQ6g5JrCHsMBVNePOpZKAHofis6topatc6
Lno+EcQ4yDDGR/g1uQgBfz0HGiXW38KLx0e97JnsfoSY6h73VgaG10HhO85N81L2bYDIyNt7kBRk
V7DbiMgQ3jaNTadNsbrrKbJBuabsMu2kPElkY9DD7bDQszzr2WJkrGRRRq5NA35mwUn/UYUESqxt
mRMhKKLLw1n4sIrrm7TgmyAHvsiwwsmd2D2g9F4ccIYJJmVseHfBe7VDL9wawanvZTIpvJfxXE3z
upasO4BdB+coshFAjcQExO1v2YNDLqxrkzKAuiaX+VWy4Td6gv53T4LSR6aw/3buWp4JmjUNgzhS
nCAs/K31ki8CRkYxtA8slHCXu/bcCEt+5PHfyGmGjiD8gqa6ZSdojrQ3+L4isjFYzYEZtMK6xYsm
w+rb47o4fjCtXl8Y3xhlP+FXhgHAfn+vZOQZpCyPITHu10/7TF+CNtNblwNwm5q5+t8BEt4TfvJ7
MWKYmZno1FW4wZLV9MncyisgJ7qXLg29k8WtIUhdNJaKeroY7tHD96URhudVepfsTUapexwUiigw
SKWLU3KZvuzSufly9M4QLaw0kOPwMxK1fTZgMXVbSG+/9aQBW78wnkQpnuxS/4aEDg3wZHBsXCmO
EuPIpavB2pT8UCP8kvgZ/3rC1Kt+e+kxAV8v/kMVZOTbl5ot7xANe/s6JY8id4dyI3OcSaSJyZn5
rRjU7n2uPTy+KTiWGLIS0OCdnEEYbKpJUDskbQgXcmgyhNJjFd351VJ7J6X7MPXE173u7JPrMEcI
AzJd6TUT8Fp/CFMIQMmMxfmWVcrYjb5jwMeZc0kqOoRJWOeJpATart4JRm3PZDG5uqIaZ0Ihz6IZ
ir81D8ypXMr3iZWhTbVTeXjY69KxzTwaTe5lHtNHotyovN9cslh1/k0SXJVvuNdyL1HIG2nm3UF1
RSRMbbT30JlegIECgxUtZlTyy/5skdEia/sAJREMk0OdQB3u70+mMHY2uofn9k3qzSVktGroVmB2
GFxr3rA4V36xpbk/79SzjKlehR1WH19Z38eiqNOQkWgxRgthtCyjiBSi1a7f4pm7DT1J2yERoopa
0vr0D7KGo6rGBI+f05p8+0paRpKG6/hLuW6paztCe2XxModDtaFXCGo0aW9gWMTCSK8D7X+RrkKZ
Cd/0qXyeZbsHkgErhpvUd/4c0wA6ecEakvB2dY3LveJuNcvTEE/2+jujAZH3W0cUXplSKwO1JPzv
5G1eIdamFQ4ZrCeZdmbikkFBZDwSFqwMWy1NSVPJ1n7Zr4/tVEd0VBiJ8Ufq3VBucyOpkZsX8bGY
UNlUV84ycv+7zuwQ+BLDPbXGSQpK7J6RQI2PoIHQ2Jjm+ljt5jswJ87ELu0dKXCDh/uUkXOZ0jF5
hiDQjQulBXhANK6la7nFZvlOjJXvnVK6+VZGIGL7kZoNCRQ6Tli6DmWEG+EDp2Kph1T7ZxbT2icq
jd+sxVTXNYsMFukUSCn9gcHrFfTi7tZ6nix+dm2v+j+pv0vWzPm5f7dgTuZuVoL5hnzTUQ9ml5+5
z8VDx/V9iHbPdg4/W1iB1AiopSriGmlzl1N7x5yTxiHk21aR75IBPjLYAN0qnfABP2K3mpJR2dqO
TpSxtfBQXxakMu/w5SgGZl15pIwTNQtLesIu3mMw9Yrezl4WZJamoBV8SG3zbcjqVf0fXS13EeSa
YwULpbcUbpvbwK4660FPwcJtsNZMbm282AKGK1zrjubjyKrtYm7iSeu5uNJslb+CvMT58ZLzOabu
8Qf6oa0awG5BwrpmnvLmaCYunot5NbyZe3EEOtZkOJEoXKmnEtcI96WCLIOtGrxEG54skvEVZu1w
eabbIdnDxNvQYMAtjCIW+1jYTPe6di7upTyoP/FFBgavyUpbNGq3IjAsv17eDaZhfYqLsJMsR3xo
5VLgbyt2QEnkYpcpSI/OOiu2ef2u5QV/GBsKnzqe+f/ys1M1Im5IOoyaSxoYPEPAW0W86LSMqRjV
1WYhROGDK1FxYT3CB+ubuKWFOaOZbbLjOZ6Bclf9Ki+l3x0PAEEvsvmxicc3wqiczKVvBa62eVbe
9yzTbGpjGp8H993YEtmBVPVmyvvMuDApSprVoV8Q0+xNL1jYLKLNcqGae0shLMMR6DbigKPsxO4I
N4hi7+O0lC0Vfp322fF2FSjRb9GF+raIRca+WxfWWSwAF05e4GGqqYlO9aTQhNMngUUVsOu8UncO
tGWfZtL2Op1eNO1Vu1WZEwxzD4AdfegGumEI0AG/IvlsWKVccLue9haGyr5Upvy64zYKmIuT+UWY
7cNNJMtRCAAwh7Rt/QlU/pML8Zu3Tvg+lF7Ws2rYOb1owi7jlVTnmv7sNNQcRBBkbQSwSCTaAY5e
BzqR6mCSMib9iYVSfUX7AdeVYsXIJlpPshCiHUUibSDFyZ8FMhc9YFB26gsp3Aj5DVIVIiRRUGGC
rjhM8pkYweBijLIJZoR5qmji8MqMTa/LaCLnIxP/d1veAKm95R+TaW0Ti16RZthcNsqcTeK/CUmW
hKssWeMhoXzi37DJI6lIeB8jc2kEudbNEuST1YYZPQK41XfySe5RV2fhP8781Lyk3FrbPNHDbPy9
Fd8YyevVoUBpUJC+2kC14AT8DH3Ix0/lj32JT+q+UvjOI18kC0YlOdIgTgPKq/wRAlcG0yVUvXVE
k3tbQHqMCumCSbUkTJyh63d5czfDuTWpP3A8ImOH/lLC1/5vA9lXd+IbCaifrK5zTclYu5o09EYS
w/VsR6BnOL+pMkgJlfhour17d8vEWALTwAJBlHUgknEGTme9tY0Ptzdz+62oinbtQk7+BtXcdYA2
Y3rLv8AU9poBrVlVSGMhfq0OkFlXm3gESXA5w0uereB6dns/leLrg6Lz5lZZ6AqUqMnYK/S5ykeZ
zkYamqy2+ruqeRcVlujYc1ERgIHYr4Ls6hl66cJ3I0P7+YgDsIY9LT6GqqOgv+twctIh2vjVcl1W
O583fp4+6SW9JJz1woRuFevA/zcqkN6+lxM2rGMExMw+hIs1cnnZLpCKJqomtBbtaYjcPjVe6o11
WbZ/x1PIGY1GHOgx80cMS/lL20rDf74PjOx+HsEtkBEyZEYVAreMNtQX6hCAaoZz/Tx/ohsEuvXW
rZa0+0zrNQUoCCIcyaRGn1iJvrCQp03rlYTnjNrottvqXx7THpBBQdDC8nPODTd2D9z3hkN0syIg
/3AlPOrNuZO6cUiFpF9pBI+5jIc+p2IYIIYHA1BeMf5UuglNUNixP2xU6MMyGig2chd1b5Vm0MUz
lu/1Ru1HkcQEyKANo7CARrB7b0/QYKwQ9c5we3trpe0kaHeN7uxRJTrHF7mfvCQX38IdAGMAvnPK
4UtHO54nGoIRBuPX0qG2AOwYR5NKa9Q8t9h5AO+QvHJSJDClyqixlomSlGnfjzk/hsk5mJMukNUi
XX8mE8fOQp7ssKYhoyupM7+GPphbWV4/ZCixbPrgo11fQxbFaJGbDWA33u1u/3mBlCZsBNUb79wf
Q4jIRq0Z0LwQOKt9SdhuRCPXMapQTp8p4DMTY4GQ9GeOMN1RmYuvSvGSPUzNZI9i9/Xx5yvsyMoR
nXa7S58ic2WAeHbQ038yK4BTYK/ZennHWEkvAhQAcdrj4hSXp7qKLRE8OaT57CZVSDBbpekjka7n
o9bNwYZQxPg7cV5tP8dY3kxXXT+oyzRge6xpZ51nLGBraesRY7CxyTWq6Z8QvO97BxcYlz2ET8p8
6YXetLig6GiGc82/WoGHAGyQjeHRH57ST2b7qZlmMVdRBB7RcMmcrEJa5dQRSBPEnHPoPINPa7OO
AduQ671R8niH3h3t/QwMaeWiHZbjFEGEtssl58a3DkkcRp4lOSIcEHcTl30IQOQjoMumDT2vBfg/
Tl2trnxPY5au5nA5xh774mCZB4HuoE05Hqn8Cxaa1rRdjLYR+LyKlASPMWesoLUQMcq2/Ttf9nxo
g6kKOW5I3s90GAKW/fP4XiSn9rUdBB0AwFQCM7gVGFz35qdDIsyRxhkfg1zdLSckpGT2FzdF+Kkq
MorNCmXj9AC0aCW1niG7lcyGe9OZxeibkiasRUzCnzUuzI2fUiZugQn+CKXzt0Rp5nAkYZ4ioe9E
84rql3dN8HVxbtyR58CepHHVeBETqk1WvrvB33aIpfoJtu/IAzjO50eNikwanuYydl+qgmTDDJz4
pDNMegOiaKRDO8SNVu+EVCsbrEwbLoEEgB1haJuB13/v3RHRYu81TMrp5EPrRH7jY2I6
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
