// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_20_1 -prefix
//               design_3_c_shift_ram_20_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_20_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_20_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
ZyaSonyL8GICagzjozSzyoh9HarcYCf8yaoK5sZwbaiNwCZkbhU1zzZ9NEvXjvESBjuPhfKSX1oo
2s5EdIgDmJAGqZCTRgXUi/zfOjN2pB14hFz7QzoZ1WNJVF9pXzEna4tyHHhC0VHg6wC9aTRbfxEJ
rJbQqIVmy31CjuKUvdI6uuZ8KbW1IslUKsmJ+8UY9+6FaudrIadJ0s5+59fPSqvMxv6yCUK4fYOY
BNuZ3schWoJWrGj+6E4ApVnssJhNOtSLhLmdUM0ltSR0J6AM7IBVujBnSc3c5HTnG0cGJB2WGyLo
HL1ASAU0/x26An5q37vrENgNoC4J1oqg6ihZoF/+PesJFRs5nXfuBDqoG8/qSnNigXYW4Yy0Rcf3
fdTranBmovZPp/aC0QKTWFQXTLT7hMIfAhzNXYxVUaHBziv3HKw9LNXbn0CDLQravLX3SEXaw4wt
MvO/dweNlu+waAvV/+rxHD9oKkjFEeVSsdy5NWG2tRWyt1nmDVfPcijZrhLW00sKkectgbNWIV3P
XNNRGshlkBBVwrDHRXDgVH37CZXy3F//p+MLFaLyVav1y0WrF9hBDhz71X+TkptJYeNux444xdzB
X/FBbCx+Ga1mjPca+XJBPCp9+Vddr7obSRclvnj64q8ltZX5V/FgmMzMnO8sf3ene3YP9h2D0aPQ
WIrpIsQYtwD44uefkHIRTqZ6UUWLlEMRugUuRXEJ1fIIULBbPPoLHQvx/oQBt2ImPaGvf3IAaxQ8
Hvf1lRDuvAhn1NSAtPGZlny5xkZolcwlzVh+CZe7zjh8foC2mMUtdzc4U8VLN3DmcEnpdGw0miLs
tsEdud6kDwpPxjr73Df0HoAfX8bFTzaBS6oGwFMMFINcGfKuta8g39arDmnnUic1ieKk3Ts9MYSY
D9Ab5wc+BHjm2DPMLnXbR36+gP+6Fj5kA8sRzkD4D9kRBy7gWFlxTMqUffs4gr/3kWLULHQNaxjV
qXioCxsjjsLF00vPmTAZwCBY471291bIMsx53G9By1g95d/LzpgrH+KxoFIjDMyCsHQHlCmLpZEa
6YjVkFpykytIwMCqZGPFw1qDXdRLf0OpMbYyDIg6RYIKfxAEQcJDhsv7iBROl3yF4/4iDl7pxmY+
n0Xn7Fszl2xysMWdI0uVErk2Cq9o2BR5KRSOTkrPvPRF5I6zPdR/zYDQBi+Dw6S2lbyEqp8hq92K
KTu6bWAbiPyPLgPD5GtGA/H1DThM8CtI6CBi62uzCevrnhB/4qWAZNfGRIaSjWZR0Fy5XompRMEb
5mykRKlfoppdEDKG2oXSFWa7zKWdMCpAIVEvIfqOGkteqnIm1PxcE0yw5XvrK0jZgQcRpz2x7z0V
fmCdS9redX2ahAxA2/6TmcXMZeEJnKLIrNFbJ6k1U+nqyMph3v6o5+EDL6Ze4MJrNXZVJV0GJNZv
yuZZ0mHQ3MVpHRFOtbQY4X1EQRMADNRZ9YP8t3ADCRTOhrTecrtLT+CHsA6CZQV7n8Kxo2vlRzoH
GQDj/UZLg98Q8wa7CSicq42zAvWr4acdYEow4fsu1qgnRwrYNRbIX1WFKNAiTO/lhPFFsq6c6F7y
0fJQWLUN6m8uj6saL9Qv0cR7+rXQK2D3pHDa9FN7TmSS1iYwtDHACsS4JnsE+KrDidZvYifeCmPI
V4RRN9gav4zHPzKhOr6DmIgva56irBceyH4DS5TDxjETFStEq9+za/oGjksmGWIAQjhvbfjymz99
cX7vpHmb+J+6ZZr5BEkPv6nr/WFmmW1eVFkDSGx38L3Rn2jJbqlwe/aWcJg4xtL87NkrRn1rGGJ2
eOk0JjdivudGon+a8IC6T8tncEduxvTy1cXTHSneKuw0kjVe6dLIspDfy1oKLzvpGW/Z7OPSaB3U
RIzCsBxAMTImne6igO9MiRppBch6P8lpVtyqsYqEqZsZUuxflywTjhuMB1fvq322ePllZQknONaI
RJff4QiHd8B1/yXYIUbBMxzaTaAjTHCmVznBAiZHN5tlR6eahyeI/LoBR0ZMryCx1I2L39ejBx5N
Ry7ahPfzNT1ctAyFklZlhJQDf19WcpoGy+jYzgSdY7P34T76BJBSAXB1/Hd0kNCAqZSwj4Gpw9se
fA3hVUgxk6OkwY24b+0fAdiItAxEDmyZQo9lAjsz5IeOfj628UMAzqj0Ezylxgpnlk7Ai2jtgouE
o+a7m7JCbltjgGZnehE9uRFBPcWvlO+C+RdSSPkiXxpkceW4COHHEWq1QxhopRa4NXJXb8qwLboX
Rc9BY4Ad+DwDr5X/XRJzKha6jpPOAeATiWDwQ2SPI7cBTAxgQ77TRXuXV+1CN7e3JrKehF9dWx+8
wXUp7T2vr5hSTonhcshKGJg8LeSW0M7YFnEnpy4FhR/UmaGjYz1MGsbEMIDO1+J5fPCRzrJjv+VG
9JxhzlcqUKn1BjKRUKf7Ahd0PUy+22TwcQjcWVVdZh7Z8pEYX0PB81R8hEROTnjJ40vUdiN4eRG/
hVzHyywytjHpZ8u6vX9bI88uOsMrh0x0TDbcQ0vV7SGkExuD/lvWb7tz04n7cZm/w+L+CPG+YvcY
yBE2k6zb6ol6wSwF3FP38t6ESHk+NADoWGfkH3HxxbqvKFlrlg+IwImm0yOo27KtGCIdw1vSsOWL
+nBr1fKc8dYw30fingHEeMgODIxnTYX0Eq2VGJjgKdSqx/2KOnynH0ih4amzNP76ROH53GRMkvG1
1tOBPR8xV8YkClwwdJJEwqGCtR7nQepvWbxmNJm3LHT1pdlT/G9nPXdChXD3KFrBVQ4zELV5S8fn
tz0IRHCk6EBiSwKJuDZad1VDW1IDZ9y/CPZvoRJCRBcLsMvGNMdIon9Ucmmd955lM7y/hpzVdz6o
qSFec3po+SJsW7OglN1RDahQwYcWGEdoZf6QOfw3ANmedKXjNes31+I/Emk+hcCrkQA4cU7B3+m5
tB5+K3nILjDYU8zLuTpgzAcVR5pw+ZhuQFRMGmBpDyRzdVI3oFo08XSk+SZUmuyH90LzQYwXdFto
I5BIz+HVxt95T3n/hpcaJ3epCaN2J2Bw1+YrIrXWmuxKWK8o3yjY6WGWUmnKI2Z1fSffZDUdk695
K0ktNBXuorci5HZKZNl72xsEYl5s0RmmdicbIYIQBe5TuatH4cBnehbFT/QNy0zj63Hu40X1z0uV
e4zZx9uulD9+XT5h8OX3rKJhxcasSIErG4hUCjZ8ilQ8dAfTHNBqzLUjXIv2Uymcso8XB3tVWYxW
TXUEPPMqJDxrdy/tj8NbvjyTO+wAiu52a7NINPLzY8rJwfSqiY5HSaoTxIsJKsRzdLM8wvK8Z7l2
dle5Wertsy51+qIbwLDPvPmuuKlLjdcFKIfcXEsJpLEYiCyXjaiU8JqM3FmsHSRAc0ESFImhptXr
vJCJEyD+JpY3BJ1o8XhiV6TSIyTlQLe+nC/qNZlv1OI3Ux9XobuS1zkldzllh7IcCzx69+kuw20X
r41cZ+umu4iFJn6CV7zHOT432cWP7JXjRx+dPdM1x607f8cmWR4j49fJxkH2JlY0NjKPFMNFSQgE
ZybmKuw+ALUy6MyeYyp9vum6a7R9O2uxqvtL7SSyym/aUevDXz+GTEVJq8QQLOMbOVbhOpLRsz1X
y0YU6NkF3x9emkZ9gtK1CWJWGDrO9XF0TgPNRZR8uYHGVyAmVxxETX3AgkkVrRSBcV4SUGAKRteB
DKM44B96lgaXh7Fy8Ulj4QPmI4ry5+sRr5Nd9BYhd2T7zV0ctiaWa2AJ1F4gxliYR/hvPk8DW55t
hXtu8wHK90DjSgGcs3k9xUCPzVEe/WMQUT8xKja85cspHDSLPOiS+IG0rXWH4wYSjftbyfQyT0Vx
e1QaCLbTHtYGvh4EHRTpaFTAx3TPkn5tTrk5qowoZJBvsHElo8oytaI2UUabVbhUFXXjWaLzmk53
uRCVs1TSQmFb4QGf/cSLSWPkG2SKlfjiB4dlhTWXSHMDGLl9ZeNGLDrZ5k1saitNqvb651a7HZx3
VqZUtSFLG6MF8r41L932/V95h/HSvdEYuxeto4Bf8eruh4EZMfDYI+J1SkXmnKuqc1d49Jtmkrsh
eILYBNnyvf/IzRvfwwpk8h5j74CJIAGOm9H/y8HH9KBSalwwU++NR9Se1/I7HmsnKTXZk7XkgO0r
RNdZlZcY5ptX9WbUGDJ8sv7ZMmQz6m1mZBQt1qZ1IhJXc41EOw4OAoDFDGI3YuVa6wAVSrdqc/aE
NKBCriiUgyGYGdHNS21zIudyhOOik6mWkA9GgFlCOYWcnlsbXpHpjCC2qGX1fjMr7ZtyiwjBFgWT
q8zqnlaaIgXwx6gcb4bLAWD/RjWxo7pt7iGQfJiCtpKOypIxUtE5EI/hf6swx560VYgrkniCV8g0
J0egsc+LMGfXfNN6SrPioBWHEJQIwzxzFeUfwaONSsTrH+loUWQgHkLEzvKOq4wIDd9AKAHOr2Cs
BtBQ5FlCpRu+nMIQeY8MpxJq9mYG7aUkFWl6FhzUS6z+DbusL8ikoGQRn+GWdV+tf/5XVtMiNQu4
IzCkFjmgZgUFHqSp2vrJKHygrb20MA3HbbAGpumfOc/M5dtSV5LwQhUGIsZFr6CSVMpVYLNBbKt8
TVXJ3/W2IskK7rwFKCsR1s6ejObI2bKTvmjIuvIsS7Yy0UFC3NV+K0+TfuPZyjChQ8CCk7G6geJA
yrqPY1mTPF5WIEq6fQkEKVc+uMVuTgxO6Lc2++v+a2+gFmujUaLWI4xwhQHS8U1nqAN+SqLVjAiF
nRCDrXzb49fR6x+63wFmyjvX8jfIQrGfqBY3fy9j25oUvbdn6ipZFqJBj1cTaIPGIa8qyPM+X+/C
MBtylNolTXqLFzXFxX+vwkBWg7bHfS2SYOeVS1p8KE8bvJh9p2tCgJIiT98AGeO751zmOSa6JO0M
xk9ZFjvD0A8dh9gpt7OXHMOkhnqJxwMgaNFmU/nvCpgj2d6rmUF7bhXx85u8sM+RrD7D6rPnCHXb
XG9+97nQTGcOFPjgY51jYGgQ/6yZF9kp6MxdtwCrBDRvEapLmdU0mpWHOzC96Dr6iPWmpkAfMk+/
Fc03Vu8DU+U8z9/wNlsi23hVLppFEvLWB0xUUNC5wd3jAoqpYuOaPrmQwIbq+svezisKIg0trnur
bcRIDvVCAmKdKjJzCP9qQ1wiqKyR4qqvoghzJH364MSIRWp06abzwKD2Qro1rertb52tPeLtxdPS
eWf7CB9kaQTxuM7NUL2Ps7gwKzwaz15EnU4hAPOjKR0kpgnt7B5xmHLZEo2AAcEoikmjBu7sjpqk
GftPF1JO8Rh+7AgTodBjFbz+KC/rA9ClErItv1ATzGJ+59HkWOhhZcGPS+mMd8vqnvQEg3R8fxtj
1RpC9khlEj3ax9TbNu983WkowDrH2nEiQuY+mq+n2zJ/nK2gmJN7KNQEKTnJrQgX+6u4Mp/MlVCk
+xRA9v7KK1VKCGPzBiImE1dicv6zzjOMbwZTUO03xfc3ISeZaoULMi8drBFuLSSjC/aIOCGgpt6X
eWJFe9sm7OqzVC5S2pWqtlPrztkbu6TF3puDAFd2k5534WkIipAocmuqhF1wrkJaE6mjvScwy5In
oWJgd1vReZDtDR4QpZr37YCo3Ti3WAhmuG1MrhroKfhTn/n121kiw07mYBh0ELA1J+auvrBGcw/f
7oJ6xUFjY2qONphJb+LS1gMgd2MpV0wF+eyYNIa7dyBE9Eb2QQhI0ypZEmfGmGFhJWmpXRRczyz9
jRqSAe2uw/FZlM0BeBqmXzfh9XtqzglRABgk5MXAVqiMkP5Z/t9nFezdakyNcaiIAt+GoMT30WHD
q/ruAJrgArlIhMur1WlfpW8txW7xWk0dc6M/ZzIxSsvpY5vGZIyKHFzZlTKV9Rdipe1y7Cj2GkrF
Va1dF/7j4aEcU17p6LMj3Hi7n8aJbuJYh/bPrGkhNcr7oCYPnk80YsJ95CGnqtbtdlNY4mkg1Llq
JqHE3quem279xzgtUQG0T/llipfFh7YHVQkU7+2hzXrOUUMTcpBlmaVy45J5uAje6htfeV1SxCuj
Y2PGw7GlpdY/s9D5wZluIHIArTfjGVqMCxRwhBMzNdoS+mLy7a4S+gUp86aOXUiaXhpQHHhnWb0E
XGDrrW/TbdYSS1H6QNwhw8UbwnmwK+x2Nprn6w42TQxUcTjX8jauZbkevEhmW4EyQ71cc0j/nMsw
NJbW+u4Wszpq6/PcGQoT2BeWV2RrtCjTkDZrO1d8V8fze2IHORtJugt7LiPTAostGDgCuvnszGy/
PG41JWyf8+7up4Rj4UY0oUayaqf6/GWaRmmkQoJx9X4w/PCTu3w7FrMU35WLC7VlJFfqEs7ce8qK
iKVC+CFUiEwYcdeC2yt0sh72Z7Euoao1LFxR4QpJnTDs97mk6cHtktwypW8sJP8imd1Y1lPFvXoQ
8PALkNvicNoTeKGSQTTijFkTxJCc4R8gmd8UAifmF1iUizcgcVquN0JEuxdUDEKj+q/2EpZt/2lv
0CeoBTqaAkvVducVKV+6vqqo3nipu9inoRZE06bw4sNu8Av8LAadL/vyK8YPqbxZzX5/lPSHRaFf
QjxPhVemAdAn4SLWf/eQADTfyuP0B/PT8mFFZKqUKVb97O8+vk88vEAFpiK+GXrAXTV6+ZgeyHQA
yPUOouvlED2WownOcgW8qJ/pg8nkaNxZsZ9awGaVBvsix6IdBrxSpmApIHFF3T/goIP6UkPcS5HG
ab5Fr9Ru59IaMZN0VrsZdDHcWJy7866g5+g+6ntjcA3uel44tzZIUjXzuHAjP+4YupvNsDIza40p
tHK1RNXYI92ehg2uhP8jJzHhlH7HIZg1jNsyQLvqBduheDRGV1ytet4gHDAIMQ/X1erNDSb/x7rh
NB3WokMPe9BY3ER6Miv9YpkDX8nMsE3zIR+xr8MGb3KP5C7P3yq8mbl6aAri/Oks33TdNQJCLE9i
zRcidHm2kTYnnMdd7d0ixSbniFdYyiQtH7wzR4I34e6rKxJyD5EZ4w1Pjp4zNWFLmruxdWIA8I/H
mvWPAUS3DtEx0NPQaXDdkz+Kjt8HfKMG1gR3GkpAlrCpDqgI7CAh6ZM5LIEnrpfc/nbUOjdHz1f7
Ibu2h9zvzgZ7FwhUgbMAsL1aM7qhFw4ovV3TCwQ3S3VTu4BSGxVG6DF/TtWX/QAIk4lKuVwsOKGb
HiSyNEDMzgfexSlPdtZnx9XBAi8AsTPihu6t+rvJE7XDf79DHNfdB2rXAcQBqIQ3pl226NQymede
mXAYs/hzlnuFzBYMqK9My7w+8nkmrJYbFRVYKVA/xa0ZMvlmX0RodbJGjuUosLcReqXCDMnNkNZq
Mz/BGwPF6/2+cKaQWQ/idxD6AVq05dpEbbgnyk4Yxve1QcsZkoD9QE/SLXiF6ZeE38yJG00UEjVr
FM8XH7juLk2t8K16QZ50hYlvlsH3d/s5tgJdVFMf/6yVJsam0+gwMB7zvUkP9/QA+E6dQhR7fMXh
k+gpdu9p7aLjm4i1kpieTNlKP0IRPglBQgKg7PwAJFSSpoVzWu/Y3nSnhA/Vs4C+dA0r89TaxzAO
+38oezH/nl1AxcbzmIi70YBU6UOllGGSPs54RJPt5KKYMRc1AeTOxADMlT7lr1WY0LiwZSP5pHgr
MVqBQHFgW+tBZLELY+5wGPKT4cFUUziiKtNGQjXVy1GToj+8+yecP0mfzyd4eckR26hHQKxS8AcG
9AhP51BcA2HKgZczmJWrCb9HpGxFupYAhjelrmtuoqUwY6fJV2YgFuywZE6V77kovKfKR4nhKqXX
Vx5LYWkpKG+DGT4JNYrfzp9hLKY1yfoHpTfu4HjeWkd73gti6yCDR3aQNyY7CNA4Biheb9umjEvp
+gNcdBshnyk7ZQ/GryYw9GXxk/WV6V06WsvwEEkQ1MDUBDK9pstxpeUCELm2FZfVqECFXQtvVIxR
dBeOeuAhTuLbNNzttsHcpqU1TJUhWfnRf49rvB+TiGM2lgYtrGDkzGSxiyTatx5UCxd3ciA9VU5z
uEgc75MQXNkM+2kT3+IGNd+SgPlzw3xxD8atv0Ipmx8buZALipMuQ9fgoD4uSp11JGUIQ7djixT8
79Ym/5LP6V7vRKGFo0DThN1q2hnqRJPd6lkWufIQD5KIQJVIJe4/0Gm4rUPcu9HPYM7LhPr4vtCu
AZj3jwyAXNUd+LvIEeyqaUmb1f6OE2anOICIiXz+Cft/e3WGqzta3d48ypF0SkHJDJxoLt9pnGb5
9QmxaJE6uxlbE2eSYSxA5qkcBiSbyG/T7KM16sDYArJfcccY2gXRz8pENYREHT18utHzOWy8Iybm
rGzkf1MR7mnOHiFDiUXcHtajOnGRn5PqEgPkHzH/32ijMj1Two4zSDH3HKOOpOd8nSda/qbLUgQ4
E1fOwdr+fIC/Idh7ou/SJRqVqu/NmEAB2y1CPDJPtgIFZfbl8TXXIUJl6JVJparoyCEjB+OCX7p7
dafAWvnJKoU63TdPZLw3SPC633GudDegHKyRxe3aqenXgnCF9O4fnIvteOIQbvJtDYl8ezuEToIw
i+LMEhEf2qe61CB8QdjIhMlTOirjMQRq8pj1n7S7BHAqYdSrNuKAIwa8I/jnWJbvi3NDcn4KpbeE
XqoqW8Emb8IwO9B1WWm8zHhpqfiUi/PG8bQeSfri5+qNTyQejChVEKInH3Umw/FizI+rt+VKOUaR
5nOjmrvTGFb9XOP31VbiujewFKFJKL7N8F8YBxnsaRlDqYDpBK2hVs53mf8OIMI+N4t0nFIpkSQ+
Ju3y1B+4oeorQyG2Aurd24b4eqJ3YMmCSrkSDyaaf9EsVU8QXVi2p10N+Qb9H6r2tCJAPkhxVyAK
coIoS66NzV5wAk+NZqFm58BJvsQsfENIbhU0fgVx6dB46h6kuLdyvOWtkzRshKIXoYWuLGCpjCcw
U5yOkqiwUmsEjkgIzIXnOckuhWFg9u2oB4n3XjWIWRWpo+ii3qadocKjRcq4RPg9KcapIMXUasgK
FEKFaiuO7wHfReh8ZAYi30vnL/zduuwtWQTT56z1uF0nj1zy7Yo00fNXTTXuVzwa4LRPhJyEVE3X
r7zfdch41N6LoluQDkd3ucZpx67a4ycSIW3ZrKlYrRbzg1kcuxcCTD6f6mIRAHDrq+UkAaTSD4ne
JnkFQhy8KgPb8NQei8J0pg51TmVxo7F0pH5OGr4KOxNx7Qrg7rNKCXvkMudzNZ7gI2Bw/RWvRxS+
WlcbyqfI75xziDPcYDokugycpn+P6IyjefbnEYxVQEQiVdmepOWJPbYMY3/4XQlCAaTKMdnj24Xd
CBOCDh5c/Jgao+R75Z1lkY/gBI/dJTbVeR9pz/eTaRzcUKVwq/2jrAapOgple5DKfiEsxL+RHvs1
tT+7W30pLqXWqsMmlvTC3ZbKkb8h+xm72s9tEFqBJVQrfkZ+z4/qwGl53JJN371pP408aRd9t9C/
tC6Zp/PDA8k3bB4y3tT8e0VP7CrEFZDIdDBYYOlcuPhNX6+IiKRbfg9/olbb22VLUadmgJEtWgj4
zQ/S/cSNEPgF4bKK/APiK2Ie/VkdyBKFASaTTiCzLeDWyv2YQkOMtLls+oWgkR1P1QCEcEmsIhL4
KpIdwDGnkOkoX/UwNpxNmWt5Elr8K22YBp+TkSHD3HE+lnh0VU0E3CHnMjjec5M7ys8vO/LiGuKV
EuaszAfhEFPTyV4WlfhZ1gD5kEad5PugdRscAZ/9MVxhhZhXfoK4Y7poQQE53oSn3tgP88FdmvDn
A7BqmCmgg79rxwEa3/79lk6hC8d9N3lMOVoX4Ix2D16MUQclN88GVtvpBCLEFaI/evfmD/x7jUdq
X1OsQdLvvVkQ6ltpjPXEPG7n1TXNujLk/3r8Ezx6YYW4X4CKyKpvzSYuVAUj5a2xkdhzyVmdfvfd
Gi4JEytHEMJqiNVxPjR5ZNZqQXNQjLqy11TOg3T59ZnM0l0KkwyCwg1LVS5BCZGxRV/cXGvsHsq4
jk15yGT3V2hBRZ1PYz+6f2IiyOGUjLe3KqW6nfJE8LR55kTHPg5L9N2qETmKAZmGyOo7tmmAe+KP
B4d6dnIL/Dd4w3LcWIvdGqg0hZwskwpSN22ARmLxHEZruslzHc9d50x7i4jFjjlyVXzA07KhUY8I
FllRPW2NIs350NzQPZbi5yLZSSYEA8v4AlGuN3onSq7CYSGt1+jqRqqxMnV0zl+BVjCxyYdqaMzm
GzqF3R/M05IzE7vNyfTBpNNXFxsC8mXXbt1LnwCXOzAcqmfO0QDGkIVXWCRZAQvmZeUgsERwfZik
YrGa++keBDLLN7Tbs+66vUJ3ZCamrRMJXeeikCKRW4FHEmN002UNxo9AlDsF+XDUmcEmKvzmyeqO
ihwipVYFYlMKSSnoQIqs37nauzTdBIcfaDFnLy351DSLlqgqTE6tw7bMPR6Q4mUve/cgKTN++rT0
alPkQFoPnm36xv7g+f5RNoxStYNr55XNE8nch83i/Wp/5ceXn62nxT7kmVyySUXEUzwubbFWTfdA
+NsOtVkCcML4eVgmFsr2U6I5fDt3kcyrunFw5o36VUCdUXQ5gbJAyk2VHSY+SzoDwRirj/s+mlO+
U8F4LVitXpB7pzatz6It47Lh+c0KWhGn5S8tJnRYd5eygG1aDXO2IPOhdO/kYZjdGKB62WCwd00S
mMuj+Xv5XRH4gTvq8G5Uj5agFRiTupXBL6TrEyoYidbkd5wfVS85TRohDbH6hgpcjfY8v9hdwKm5
0YDgfCzodQdCKwAkz4NElZ3Vt2CTqVoj7b3WM2v22nbGWB4TQi7xTpiegZgG8gug5owM3f0C8EyK
MXSZwzUUqXs1Iy59bfBb26W87wrJuV5WKH8P/g99ipQl+mzU74Y/gGmxVD1UeO1CT2WTwBM3a5vq
9JFitPGeRn4swprfYMAWB0EIRjS1SfkX3f8kQYCmzmumM4+Dk9ZHNTf81cWti0ITHpjK5b4D2R2r
5okMwqnEMU84HWNOWBsKlPKmq1DWCQFFskgf9Br6KpBWQvWEpj0dfkM4QPGTta7xllHOw3VpyU+x
H8XyVJaLHuhkXHtSKsnRTkAV2IwhC/OkbA537EtcGb35QvwO+PB5ClEr1/rcFgHpXb3wazMFp/te
S3GUHSs924HLqleRy7J0y6qsjgnV6fmDSlFY8xZyk7Eo7tW3gcGdQNPIS3/PAnDqUsY57GmHVPV6
C/IJ6wFJVhnfuIWwSC9d6k2jeqnLPNd/7MwxgL0nXXKszN2ZSZZdB6cy1iJ9vySBXcf64RLI1lqS
2WFJtIlsiOZcVpsIfyXGNYS6AefP/QA2XIU4EpUCLXdJqCRDuxo3A0hZ/uMk2sFM198I5RCmUngZ
RQ//AC/e0NZ7k4oP2G/qzS6dVHP9R/LhZy+Qjc8KIgqmNyA96ytryX4jLSCnd73/rKdw7/Gi1MiG
qzqf4Swyc1bAr7XkYuvk4Hgv0369STLCDrzjlQxwNChnMz0r4WjxwF1EVQOJc+UjZCinzvg+/2lR
8+E8t/FzKe6bk5/G3rLvsORANmdl2KxlB2Q2GH3874dIrxK1lc8aC7sTjIVglXzrqZ+edT+b7fwf
i/EimxM6pwF2JKEDtnrLyf6ljT+4BzwrSx7rfKRAuf3LeG0M7IClD13RarLt+wILgu7HmGc1Z97j
i7ktOvDNBpt0nwWm4saw4gw583qmXOUrExccdPQZVFCHHhnWdO3Ma61PRw1nwcf/HuQetZ5cBfpG
5OE6tkW3bHSoIXHtARu5JO9DUccB66LuqsIe2Hbi9/4+rceqVvAGwsKm9+DNk18n79toSffA4rpl
WQKEFHOZD1BFg4khJLkRhYjRDUnJWp1PfuPGF/U7G5OeD3Bm0B2nAFfVyJBZHWgBBjzJtXfhViID
rS+lDJicr4dOSVrQYvBdvnRnpJGqbp9WWf/kHJoInyV3Gp39p41EfRc9FyoyGNhSJAciMW3+j1DS
0Yc15anV+Ew9/6kUGXU6GIJTMFf0sjHRTBmeS6izxoVlFBrDMS1othva/PVweINhzpxikQroEoBg
VPxkcByue+BjEorrLROOi1/JEeU/h+n2FPJ42NcZkfBI7grNnX/5IBPVb/A/RkwincSYioxi8iCv
BEJKKBOunVSTok8uarnvOW9rtFEmmKm9ONqEz1aZjIxUBH3V5o+Nh65nFSLlEaiWftRPfxgQQkwy
vXokiG67Mb0ZCcsqe2kWlpv33ZSW259PbWzNky8/cAZKnrNIHfhQFpYoOj4+riVeM4VFij1V8EW9
12Z0DXQhS797EgYUYmN2jDOArgB3MK1kocx37GJFy68oTiIvQwQbcK8RxqjO7DxpVLMpODhh9mk0
9BZX9QBcpOCiDRhKyyG5T6PoOmIVaMy7yBELby6Oy4xyTsdCnGX3KX14W+c+udOWs//B3xRy9lPi
/QwPx6pR0381AFPZW8YNOexDtOa5dTrRgFYuqftZdfwe4ed3UJO11fVEW5cjtOfpZlF/IjlzL6KG
j3DXNc+hitqVHorTkgEVFw5PMxIao9FhE5kSKdGeTJhuEL4gf7oPgy6mbuU6wr8B2RFR
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
