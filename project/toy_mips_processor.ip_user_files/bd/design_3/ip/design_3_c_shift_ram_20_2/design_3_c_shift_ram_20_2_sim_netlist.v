// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_20_2 -prefix
//               design_3_c_shift_ram_20_2_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_20_2
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_2_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_20_2_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_2_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
Riio+MihtPQ+ndfMSpD2EDTTCackNBqMjs54gsWAKtcWuG3g6XJW9MI8GSakiHc1lOA7TZnq0BE9
V2cKiDLAA3cg5KpxvvsMHOMPDfThZQF1HzmjejRk4+qC6x7Fy1vET8Y472JgoEHymc7i47bCnfX/
d5noMwIpkDxBPnb5Z1uzKL6yM5rw1No+YV/YEj4miAi/aFSN3fqOFO6nRkcNUaJtUh+GLWEC+kaq
CJp9Jl6Xd6Hs/MaVHBgQ1j5VO1JpBrt2n+goPSS/VBrSLPr4VM4BnF//M7n+XbmLwnxPwvJxEQuW
UfWmA+vIS/XZKz64mQ1oHDmNsNg6PgzJ6zL9tDWfgR54VPZF5ysTSKfXIN1atLG0py9KL1zHJvBk
D0ioaVngqUeMKdIFu8FVGnxRlKaCB9Yux9EF8slLoaUS2tVlTI4UU+W6LWPFddRuTQ8bSMP6TSEP
BdRQ10FishCHHmvPQr3uaLhf2yEnla/zy9VER3R3AOhA1SzCEESUEpxFVvY41wXS2dTX1os6mKm7
SI8oK4rg9Qyn5m28MWAeI3q3lSdDhipenBOm6tYNX5QDqFOEbLOgCvxWdAH+t61D9j2IUPdpL/2m
h+pKCA4XDAhA/SnEVthNG+AoDnSVFYA6PTxqci/uQRRfHvorAVfHl8rqSw++n6ZRzb7BAGigfejm
WvfO8LKbm7r3ZC5c4L7NGkv+q3Kcs/ODcRFszlmN4mA0XqHKR583J8SA5sLRfMBiL7/murayb+1c
wxjuzXRZttKO/c9n7m0XkBJ5MHJ5aS0Cor5ucVy4q9Q/eIZx5tmptuLWnvCFX8McBThTU0b5pFUp
px28hCJWqPZWuh2d26RkNCJfabbQ2vozQUamC2FposixvaDup3kdlkefRIR8A7K21aJMqH9qnebc
7dUHKG3cCGVMDrsG4oOP/y56L+I8JMu6Po37wi9JQRAqI/mNE3fjz9ny5tzGANA+aeU+KpGinqyT
vxoQJRslSuJacQDDV0t3DaIUlH7NXGZTqpL2bog3iqiZaOw8ESuQXMhSrnq3aE9kBGtcNW2pORov
3rDHXnLqusO/2s7atXaMTYvcIW24RbzJaRW4vLkcrTE6bfl2nxzlfGMRvXHDxJN2BD1oVmD8fT4j
fVi0rbVzPgQnwXCrln6UXw9xl5wblqHGka0wTuFyMEwN/tNhs2zqJ4C/cw3CTdfpM0xyOJYSRXQH
pGi3eie3yi+KXdQoDAnhLkMem7+tMjCJeP2C4PcvdNiZP0cbH43GqNZAu3mAm4XJ5N8u+Af3BlyA
k7cwgBG8+kqzkPRhSteQtBwnsgQXOsazFFIOITJKsk8S50hgvO10GuWn+cnsXmB9CLZyuXUvcTjM
2JbQDMVznEOtqShAzbGjoE/MmjdhYcggyGoUTz8B2xqMdvgSX2NYj8lkYqo3T13eGusNTwNFi+YI
JFKDY9EMFD9cBenRr2/9VHPqxDQnQo7IyTJENoQ2uzkwi0dL7agAijrnoUDLeGT3dJFPUjLdFX4q
PyqnMwZ5DPWvSz3Ru7k9mJ8ulqUn4QVdRMBVhgPlWCvDXVc9MEpPh06zMN5SpML7+MZq9hn2Hg3C
xSKMg7UkaR/QzC/tZi8jEnHTXaw7DAt6E1zr4+xn+Jx5W1nlqktiznM3ty1BHmtVZPMHo4m7ardP
CJ3RGnQq3BsNcqNlX42JoIidRXIG0JJYkRqibpNx9RdpSRPFAhIqRDBOWHA9FJmWXfK2AU92FfVN
PIwBcmLQ9l/M1b/fNXTMlDWzmT4jLxmzGVp5QI3FRYalXNfPAp10S+wOdBUJijVpeV2GYurk9P8P
oU/EpWxw42mz3mnzI/3FDVZrfRio1A9IxwP27u1v0GMaWxvjN6+lurr1NktwoB7iX0lDEa1+fCb6
L1/MG0nAR8XvRYqWPH3fKFdp4K403LCVMXzySlcFf9fDRY6oifWGOXcs/1/mH5uqHNrLVjYSNocS
vWdCyN+qZuqQJ9S5mr1EWSsHZzixAISj08fqjW0YBUkQ96P4e19u2fdB9P4s1JIiXKtcf/sK4BAc
qLLqi9kGFL08puWrC/G1lP8vN+dy5p0xnbPJxaGbPPLCarRQNkxfxtPSU+QWpicsYYVONtP7WX4I
cDnJyPvk+n+IbKy3mWp0vUhrxvlGT67YIwZLdBtkLYUUyWBuRC6SAi8axDUOYC45Oa5H0j/qJLjC
m2PPJAbwzL38n/D3fNmJp7mU+y9uf0EkzbMvNatq4Aq8XDpA/K8q4ues7A8thBYxskx37+Q3UiwY
c6j4cJyfFr26JhUAg3s/CIJYbxU1avbNpXZnJv43CP3DRPHHY8QChd12H2Xd2Js+Tj+sv/Bh1dgk
+uiB3qxtdmtmosHeOu5Gt449dvHbJVAPwZDjhJy71PN/5mzSsT5QOG+d2hOqG0abep4NM73Su5KX
MCHEHKmw9hWRAAJzeZ61F2+Q2iFhHc5AhwEBCco5ATjNw/C1JYH7ymnkrSDSre7bxDsfLC++hR+R
ylWDWan5/wOSW1PlYCH5AAZ6q2meaj5H+8X1g5dxJw2d1V09ScXeMNndOPv5D4ASB6xb21Hu1yEX
YrTCnZEsIQ47rfKdRNo2d3iqvr6eYkXTvwuQwpGmGED3oPKsxf9CQBYz7cTL476VMUMwtzHiQa3r
P36fTg3k7t9TTatW29GhkkmTgPPZwocv4F4gOTMUq7sB1Ox3sFCxeUNrFqLELOIH/5GaQeC+uiGt
WPIkQailctDcMIUVqP/hjoJC2ZwxK1z+AyN7UKsm3BJWqvs4o1yZoIOY5zLQMVz9PRZ0b1SfQhPG
G0S8YRA3pMbZvW3apR9yjtVAE7rbSw6FrzbPSnGRv//6aoJQ03WrLIOG3XDgjsKgvv5eR5Kzc5LC
8N4OL4Ib2Ffu4/taN5ME3CSTb8s11oqXImQ2hPLolW2cjSUGQ7fy6kjHjmSt2+nf8lk7odMXIIRB
e7vDxYlC4jBMrSjU5H6AfHFlaP5neb/P753Om9vZ+zG2AqCj3OInNACUU0qbo9d64jnQ0aRrviJ0
cYAT5ZFaOzoBdjZl2KL9jL8bXkwHkjTXbdVGqOsc/Hg4upH413X0DIlyEMqC8WMixYph5EzO12rT
Y0SXwyPMu+rQt+jUNOSFgEvyUWDJulE1XUaH6TKioxTAKyoqNWCn4cfOmrHH8J4eGm05iwGo3PiQ
yueHm8IcYtK4j5EmCel39HmSn2WZR5WsnHB2SuPApSGUqXW6a5qmAVVyumNZpUlEkXowuNfeJ67T
2eRVO3JBDxzsOTf0zXbZm2vEZuwHpSvs1P9pQXAxgR2Ap8EpeLiktITFKns5ckwOJMrkfU1u1C3D
ajVjgmMQ+sH5MMtJSxx+zFi/GkikalYzdod97+UN19TDODeW+froUe9MTjReaXZ2Kn7WxZ2XeP6w
/ruKDxq11UVC/PBW5B86vQUL8188Cw9RA4xObUjw2DDYqo74aD4M9EdSUTYh2EU1vtrFuFZ+KUNU
4Wa68U9AKgWsnZDJgp14G3qA7mdkYqJWMXEu0mty21r03X3j7+TeNOwAsAkRBwQOfgvxdtykTKTB
QQ06eJBAIHhEw41tNlj97yH+xe4YzN9Rd6AGrfAGKNIgaYF7tKmM8du8lDPPqHgTbW5W1l860q+N
z0sx6Xfy6GsiNQG3Ulun7kwnSaG1xa/JTE0txT4RdBKafsgyzhJwsV6v0w5eep/THL+ePDd3BBW9
OWo1u82N49NMnPCoCQF2eU6fHpRlx7pTezP7YfVtZkoWSZeMExaBYoOs9gxUposXJPgHIkT4zhMN
MYqR0Yf10NT59Ms4brRfisKQFs99Xe5P14b1eMmVlchd7yilcY2XHOV8LTGsemTE1YGk4B2+y3F2
o85k0X2jdVqGyQp7Kcb8nJEno/C/wgAtZwx26Cn5fymf7QxoHV2BGGdIBcrqWK6Xw018pmnwyJ2i
46QTA+m9CvTdeiq7QK0O5Rb9Vbil/mI7iAvs4vsMJ27Jjk0u2lVq/G02vyamgEXQ1zTouAHljgFT
1xMSMQHmfs484cA+iMAcGvD+8bULvU5RNBE3geW5X9qFm4/PZkdPUCHbNfG1gjlN1HFDIlZgNth6
IKih93iOMGGgxOB7mrpa4w3Yhv9/qTP50IkRYNqxBDHUc+sIJmUbgvKQ5Ud7jyUw9Mqnpd0kT4tO
kSFLdafeNLmIzWVGEBkR9Yzlq2CMKQLYVI9pUwJDv3klxWBaksZGdNX9lsMg6lxXY6l1h3EmXQCX
v2jFSJu/OjjO8mnDI8GYFDVFJVeAj8os3ROqEIov0VkWPDVeJ82laqWOY2nRgJa5Lb9oxXvZwtvB
6i45HKdMB1nREvU2jWqwjH3Oqcc+kNHaDTxIESa0jf3S1axHqdUsJqt4LOMUMZlNtHaMGBkNYipY
ynyiZZVnjSW/GOGjn5uLYrcP0Kt61N5BD+j8A6BMyMsUDEN3Xkchv7jL6gVpDvSIKyQcuAJSTAVk
/KsB9TFIiWccxKShXZHc6Ow7C3rZd32oUL5ca07QcR34LoeX/vwp28pXyY4d4NYSA2W9mx2+y1Cv
lzBEwd6ecx6YhCry/zYfFYj3TQR0tJmGtpei8kzU1pIOAmPO8NMjqZzV0PVArTBnw+YbSn2cMyFo
sGWGwe1B+/D/jJl9iNlv6duwewADij14CAZiKzQcMrXkMHSqSoLnOZ3fHccPWwLRDnNIObRAYcHr
xqLytXC8y8epTXF4KSV7mHqj2yQJz62A2ffHcLd+kRmaqC8SLKHa1TMj/39j0B7Nup0FpcU4PSDB
6P6J1LL+acD57b5KrNuI4CDFLYhTQz/WKPd8dqIklemORYyoVp1FCyFw66B4kdV2Rs2LZnKzvhaB
JhDnM3uWhApqP/iFCfCzJKXEJyOtlyXODvezNJllpop0Ea1G0y66THV+YTRdfPJGBVOuqcmyPN0T
UjfEzygud1zjYzFX0cbchCJPf6bmCB5MRnECS4y6lumb4nT3bY9lZEVfNC7JCtfBAob5jRb13byK
OSEUlgc3X4hL837ZOL+dB2KAtimjGHMXyGV7Q8k1RPXmYda/lUCLF6BYjRknoH+MVTu1L5TNQ2Gh
lo+EksF9iZtceNGX2soi5CPfn4PxzOjcOO/S9LIkMaHfA926KJ1D105qcb1CqKfOj4txhFuCt+s7
JkWl5zjE53n63wjxdrV5dIUTp+ROh5B5B67ONMzsJcZKX8Cstzzw4JG70+gSvvFAhuLuXE8A12C9
OVO029MV2UJh1oVl0RpibK4H3x2VHM+9EpoxMl5A8/RBbQO6J4C6kr9l+nqG8r23oZitsGgy1fJX
jhGupzmx/v/RDPVqwt61R3r14NXzNVevM3FX7nMMssWj+wRU2xlM1RJoH0ncp16WzHkapmvV8dnt
porHRjgaGqz6LcgZCvBqS6b6WU0cAoDi5vw0Vbqmp8IO1hr7rU2t82rpndEwyvLR701wzZmEAngU
WcJ3IBduKoMJlYtDr4PCtKjmJ4TB6WVoUUdho0cY241YO8RU2gIHpktN355XoLCknkC9idhtGCH9
zykdiF7XbUZUZeNOHrxAJKComIRoCALkClD+ZPFOL0LhBHjJxx6Yds19IVb9VIhpOI+wjTXG/W60
NYXoaTd9u4Ue8i4y8vxLTmm3MCVYBS6Jjjt1R4f4yhsoE0KMiHnDm/dl7CUgqMmRcUQwWG5xM8T8
Zu36YqSdnc9ENvJXQYemy7wrA8NfjMl0eqppiR5yYGCUEp4qFEcgC9fdYq9w+652+/bfmIoubO7j
2URD+xqE8U0lb7wnG5u4tqjdvC1t0i3HJufpLuc8830YRiiWdx5BBAwyMeG6Bj1nR1yMvo+o8UR0
sJYdx8yvq2r9aY/XlYQBKhNEWb4voNvVg/y2j/gGB9J6Qn9nmMLZFA+eD30pbxXg7nPEp04HskSW
z+a2MhYMx4IyENkcn7d14cEiVQGdF3kJf4joqggkVz0FybzNPCnXxi1GuUczWkJ9mUV99vxspa+e
6wcMxgoFYMqnicgNPOW/4KAde4lLvxXvSg08ZPG5emOm9vbmm/vnh56JDiYGh93eIsefnyooS7lC
qfG1HoPdmBGB7ebXlUDS1VJme3Y3S5aQjspqXaSDlraVhbDBL95H6SmWBTPpR56ZsWrYYTSWVFdL
t2lWCE94RqQFmVnihCA7GJW5l1jKR9RgZI7QiLz2YDVuS5h+5BzdSEJh4ZjDGU+CF9MKzq1Tyy0H
s5uKqcUBOsVOVcSWWaoF7bEtWnMbHY2+5jRbeOXNch3mCTip+rN3NQ0IA6x8Ab3fvP0qX3QKkA5y
5rI1CdNuxf7sFZbEUF/mo/0lilWYERL4R+1MdN+RKGCyNmT/lZAVFdStSCtQ+FBadjRXCdHhiMFX
33iIodPQuRMDenUpFzH5hG4WitCKwLyeWUDMnyt4nL4paINJ6gimhqkzUNDb6euj2SW7fopGTETu
nXmfJR0/8+pAdEtlb6Y1VuA3rsmdtP/NDMLRge5bA/Fa7sZvohYgqV//EApdOfQEKZFbRNW/WsFF
u4OXsE7DBMmJMeNYU6p7KuOYCg5naqiepUiIGIpFRdAwSG+NEp6UHqKhk4sq7Z8hX8KTXFE4Yg6H
xszDkIZ/+1SksnvZqPJ5EAcyo0TpP6Bb/sMaZBivnZ6HwVaXBZwRZCBufvpR7n6HJH47489j6t7y
BmqqfpHL38/LCMvlWwLBfrE43q/tdW6nqfBG74tZl7Te5oN/inltgrCnzJPZ6fTMD+rF+Oo6iyo1
Xx0q1A0elemP9qDFCo49tGd0FLds64xEhV3sdE0cqmmLuf5djmDNaX0Mtl6R0NJPck+/ByhkQdXk
Fo7Yb95+GIyYjdBVZL7iUMqWDd8rumkzt2Dq4Hpdy8AuelrimD+dhdg8+eTVzz5Kdef1grPv9jbo
EomSvRZX2tU2tWBY0HZH+NIK5Dghuvo0DOPnXXaktuXDObqTyxnJWS06xs91RSrPSiIKQv1+jk1a
mUgnwEV/pEx5KpVVF4glR1JqpdGnRVb5NpmANp7wQaZEzcet3nkBtPgCSx6I6m+5HmhF1BnfllUm
edIBHwwMRCkxXVdvkOmKIubb3JwGqE9lY6GqkcEni9Xd1/Nj/Ym6qeFnQLA5j0MpTe8BXVJTqRhY
MG/vv22a4BZhycclX61zzX7ryX62e263cMFfHW9Jhr8Ky6cPY3KlF7LisC+wfdvU8EXzgvwUDIZc
yrdY/FHkLWZM16QrIuDVk/gWjF0GpbfXGXynux8E9u6xHQaDarkDOJRjgNeaEgdCiY7tknKYG3ZS
pPYmcRPvhlHiez7p13oZHim51Xv6MwxD7knZBvhuQs99BIhucpjI4a3ojf2lTJWRhJTedhXeFvqF
vCweYLo6sgo6nny1QWLNg5jJaQtuV4QF6v2Qapk0rCtj+CkTN0v5g8RH8cSYMmrKGK2AGN7e8Dsb
DCWhlfOZAw3kaRS5O9FS5NCwYIMRJcQ4Hj4GW1oHqtD631W8SukMQweIy0AgvGgXi9LOd87dXXGY
3ck+b6qAMkYfGYJyC9sKItmNwLuMa6mbMBMSHmRGaJbLx6DjeWfT9RbAFwE4nuXD8hNsThfr33BE
+BF6V91S1lj2ebsJ+BJz4loVUxXZv6lyh1bc4WalBvvDhUGeDhJB6jfTqKc8J644Fo/uWIOmMTzB
eGco61AdKwDwP3jtuegxY7UuH23FzG57LN8O5wVnYDi+JpgHaonNfF64dtHrhz5bwV0p1MEfk7YU
lxSlUtdxY6iUh2scFrzyVw75jrKeUyQnLTfKVcqEDrk3qdP6hesyhck8BJ9otFjtYZ9CEFRyMOIp
vr7tWsznxPjBOgP4YjdD3ABF7D3ce/z9SaJOlX0sMlBPy/1SgQY197lOhTHngNn2fZoJZsfMA3fv
IbTzM/u97hto5kjKEzbioboc+pPTMKmPKkm7bFSsrthAOo94ldcS6tNi3RfWnCrAZOo/DATUECVL
wUoZ/aUXB6s/8Rm8uxUnPpM+W5KG+xlFSYY0fvYI7OfZ3AMqMBUxNQqLcN1OwsuqG2QVQNRvLniA
X2L0wKLtBty7guK8+gT1KMynsaCuANw05J4rU4WnlAq3KcOG1m5HMymjQ5wPiPf+4vYruWEAMksg
i0bLet8RT3lWIsFa4Opdn3USIDPSy8oFSUXXZ+U3f3bfQiLvT1Cbawf9YVOrFQlSPV97ZkqSkRJS
YnVkUOHvGDkcOUDIAKc8TS+e3yqpUxrz3ZbbaL0+wt937+S9pAT28MljK6zgFsz4q7ZJ6RtGc3Hc
LXWC6CL91bRShoBoQIv8XJYkxTYp52vuE5D2WOTCtb3sgD/c5iqxOF3gsKN/lEKOnmpIKB2giq2l
o/TtRy0I5ehX4nc+oMnAAXgnmZk7fr99LI9VidW9fA+pToHuW3X6/iLkyjnOswpD5xiYzKNXX4uz
ijDWXvEKqwCBV7idBqgH0EkyDKakAHRCl1wEGtjSDRh3ujpJrRQTgoFbk05SX/N1Mf1qNrr5ytns
fpj2IQCfhzwQGe3siwu+mvWzzIRHrgQCpLU1u46N5plVGPi2y4CtUZDLnAfbcQEN0Byb1RxDGvd8
Q8Rm+50P95MRYwk9ttn5Ei4ZvIGaHI9bnadiH+P6kcVla7xHoJ2+nK43N3JcAuryse5oLvtOHqzb
Y68/CVbFdoSBszZggNh5bHI1y2iZmsSoorl2fttcNKA5c1e3G99JZoN/vQr2gMgaZdfnGSnT9ZU7
53ErD7Ilb6KwFsdYSFpYOtfr7GDrOMmXFHnG3yD2Y5if+Asx1yI+oaDO5Pyil5kEhVIrvOFzxqNX
AnzzW8wlYv+gecGcQCaUGApE2lPpwiyPlKCnBV6c8yD5vo3XP84H68tnd084z1XbC1Ibk1UHHnOY
6PZZ4PXanhjh1f7kpWCIAkdE2z6z/wxpkqYl8yCst6v77uFeKyfgG81aNSYEOUXQPtmEUT4iilW4
AlMVC7ohH4zz7HpIVIPhlbG8vR1mHYEIZM4YV1nWajDC+qdluFqsxfu8PzLHnnPg2/NtbyPHG5Gc
fKt6stzLoHb7/az0cJBj8gHKmo5T13SLfJfPdiC4JeYcK97yNAiTDsvF+YYvMUR1Hqo7IdbD9DAX
8TbKWAgHLViAAoXoWXO0kNZfTI7wnrPCCSOkz+hd071Y10hmyaM7g5s0xKYZkX3uLIdyLHYUwOAC
VjdVtXmEK9y6E4nX9o6y99MfplOhgq0VbbdluD36K+X21kY8m/QlVWT+f61NEO5bv/qNh7wR3uZo
BYMmgMe8W3bhzNXW8lh76IQmaK/5InBjxLbB/w/pk6nJp2uEXTsbOxrOEvta/c8l9HWDjLyqrjmO
KrJowABbDEfvRukFkRq8uAjDWR+5+cPfzyyWWSGXNkFxyYUgP/MGtvEiwbvPcjezJpNXHpTVsxKJ
Yk5o4FKcs0HkZ5WIyAp15KsJDsZGvtGTjHzoLs2tntSNh1JfhU4j+W388DVTTflhJZ1w0yKdLVZ5
qQbriIY5qnB8p6k42kilFvmW+gJcTMCc9L4qxsUmxLoHixdyc3MssLAXvTBRJ86L0G8NNVGxZ1Pm
rIjtje1L/YQF3mfHO9zoghX6TrObIkaoUAgiKz5LUPOCj6l0uMd7i0nsbjkwY68VBIZIPhVlsMd7
Ryuv4KFUTMZqOStjyAEEpUekNWIzocB6P49M4Tmqz6XXkNwj5Y8PwenY5iSPnr8FttVn1swgX5Wm
sG6wKzpbRYl8mowZg31C4Wgs2HEtIc01rR/q6JXKr7+blZQ8N0l+ag8v9KvzceTZKG4p3t47pgP/
J7Py5uchTem808DyvwGDu1rq1r+tox8SbQ7J5AxourIfMjpTElxJkx09g6YJGJNnXJjqCc72GK7K
P2qbfwu//5pA9ufV2J/Ge5k10G1uDk7zAReFfNf1xfNnnYTiXDStNy+MdR8xDRSO53RUL5dffblm
qI1+0LHjIIHa7U/sYgvBjJo6RRVwnLNKkb7N2B5lWE2DHS+q5nMbysrEQOggHVMm9i2iE0SFdZ10
R6OGEfSMDQ+aFPNU+iGdMjLl2SGQyiVbr6Qa94dESQNT/vx9dwgOenLpngFCY6b4SR1QglsaKbXC
Yd+AxbeVUjzh2jWZloWVQQ3cohkVm5SyId3VXf5LNNXpU8rrDp1TLIOOSY3GY8svPYfYU2eyZcmR
BW27dT1HhkplDQepu8VLyN36H4zl24ttk1+Q2+mtVMRfKdYdWoIDOYhKXay9Mjqhg8v6lnnEMvCA
+C6bMCy2qQvnsy9aI+l4wLqeuOXVROCfXzXel14R3m80hcslJBIZeQg0drHNvpd3HZcmmmpeieWm
CpLsUplrOfzaDlZM97TLrrB5CRZzgwC6VY/SdzOz9qlgLa3DHOklAYPA8b12GQWNbX853dqAYw7x
bCC4op7aLf4xQwIEJN+ZAg+s2oGhCLHNWSRh0oaaQTBJs5rPwMp0kEY4Q9CyYmMCIwm6m8uDISac
xSRtrcsP4ub1AgeVE1yXX5VHoqRXhtrffa34x+EiGBf2cftS4NEeOm1yFoxWcrY6Y23IEiKji61M
3DaR6Cb3DN768ngdDD3LrqvOols6JjOTeV5e8650SARvYmyYv1S4PL164+yaPaHMTpoxct5xbeaY
JggXzy5sHyZBvuWpz/FOhsEHrNKANRtFw1NdYXFjTdo3zR0utdT6uNS3nwpjU3bv4elyjFnjhE8c
sDeJ0sCpY1WqIwl1RVy5dWiQjdoSgptbak/hd8HMfUbbiYxISAntYsfSLzEPteVtXnnX+jXZTHc+
8sWibbf5T2rTcsZ7D19IJkjnqugHLt71GWgLyKGU/0E6QG9qIbchHr1unZAT5EOJ+XRQjgYyYejd
jCCFQtvsQvl+fKIpUNiLl3vOSGqelswyXuZFPRUNgt+BYtaWAgIwdnnIaGWrxQ4c1dw5aLpQZHiy
zDx5JlOuLzW/Zq/ejhJxm6o7Q0uNz5gCt8lDXetQmINt10Tbzaez43fXf2+HAbOu0fsaNtPbdPaY
9spVM5URsYg+Uc2K7Fz6NSb092+bI3+jXwImxLQME1/A/yaeHW+Tt3AstprjMvOkQyJaKYngxq2E
2A0kJdw4L8T/oN6Shb9Pv3qmGv4ZX4qCur80pjGj1HoRVDugF+oRQVnsjHdTlxNH1kffeyxmJwQp
IuCcNHZ3jiNOCgzzKUh5wk6ZQfJz3yG2SKnIoLk86VWdL2PJMjvrlszJDi12+2fEFXgqUFWnRhab
Q9nrcy6DllIGPaBESkABg7RHB5v2h9b9mG1jAwBO1DtOPjvBO4YX5B14NWE8KdaJYydCUtmZkwJA
Htx/vOJ/69hpu7HhTZAJk+X4v4Syw6/jhCo0zdVQsDvF6ZY8vIKFS3VFJoqfNC5pCUpLbG6cTZ7h
Ys0cVdcovhcd/Ca4EQ3prlX1xjSjTjjTWWlubSufs2Mm9n1TLN/czKEuucPK0JSydCLmjNtFo+C6
YH2QunwAnVrdlLel7npzGEugAkp5HL007r0JF+Yv/ONwSSnBeqBfuF2RLipjLKtBzosZwou3TDzf
N9/7rxAa05GZ/8UwXfXtTIZR1rBSJzB1+1iherAwrnYaIK8FxLSFRiI4+pPCe+DvVghTuEwJJjV+
G+2NNMvxDVHoi3S9GS5TB4CtLltPeQjvCrqJ884Vhkkid/5bi4LxUpF+SPYooCGsMe749MfecrnB
M9Ocu45Z9Xgi24eG/xMYlslf72eeFT8eVTc9FlissDJf1PbS6PjP19h45TplYvwuMqJv2iuFugpk
YLc6Jj6aY6S7O63VT5Ac54yNw2Y7c14nLkOtFa8Dp9UfgfFn+CMOdqtFUWELxCcUupJU6SeVBSPI
zRIHGn/TmwlplPIWbn3ScMXsIq7rou7WpHBm+8d5Jv2yxkA2S53kh9/9tyxYrPy3eJ1RPWhZu/Rx
RYN5bNtOs3T9GVhEyR8Fk9zEs/E5eT/w3Q09i9sTLko5tq5K3/f9vR6SP+4oSVBn0WSDW5Mtk8s1
VTRkKTrlwCtb+NgqmgPXs1BO8ENvxx9WERBqMd4uft5ugDr/gcFe4ft30Ns1WcuGfffgpfHkVXqx
oSXaYvlOU5Q7s5MwAO7Vcxj3EUvMnh7wtg4yJA0CZQjEfVfMBSzOJetMHca226SItwPdDNzPKCjM
lsQ233Il8RB04EpYg+1BUGwgc1CCC8rqsQbKCvrWw8qVGbLF+zAZ85ZETbLAEjLKP42zd3QaE9Kz
LcYJ/uqsl3o0NZhexfMbs5mh921yhzRY1b9BLLFLa78f+DylLFBcDwx4jOTssnVe+cW8KvW5ecZ9
HgEWD8PurN1/odrzgFD5FfR5Sgk0kkoI8XC9pCVV6oZKVy+mZi5o6Xq97yI9bNvPTEi0ok5sPerV
p9JdreAsU6LuWcYlvjEDwk511MlzmCB7jLT6Zy3yyaFVMTgcmA0l6aCkxhTWkOERJS6TWxW7Fyrf
ADLHSz0mS6G4zJZwewRkHtgTiiIoCIr6jWjcUSBTsXHPj72YyNNlsTveb2Kxz2Rp5iukkDy1UJ54
RA2PgcHZC/qcP/HNEGdA2qLGFQCGzm0sENt3+98ed6nrbBvZ2IiB/zHDr3e2qwun/GbB
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
