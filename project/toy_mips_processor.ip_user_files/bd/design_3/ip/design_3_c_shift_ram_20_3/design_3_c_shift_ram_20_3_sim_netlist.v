// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_20_3 -prefix
//               design_3_c_shift_ram_20_3_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_20_3
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_3_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_20_3_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_3_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
KbdE6w194BZNV1Tzb7a0Ypm563I5V1jGLwIlyRq2IUjJ2JY34255G+xTSqXdnRTNxwx1FwLYwGaN
NweEW4sAYoJnKU4jRAE6VjrOPezvPPIwUA9C7oE2fHc5YS/PHdD+cvsQDLvmEtGzrJBiArdzVU+P
eatojHK/rCFJu1gm3UEFC7lEc1dVSL10LKQ07Fh6BinqFPNwkwDzKx0xzyw9WHflKSq7qqAvp9ZY
SCxOHxJAkKcvM12yYJ7qLdfh2hTbkWTdf9+UEsBswARsSuojGH+EyfYMzqNmRihyIXtLgd0E2iTe
W9DoHZQ8rO4zTOZ2qFO1o7ahJlCw/4sGoeo/XWE9JCnTkgv/xp2ZMDBRHloGtBvjyWSJvVTS/uSx
Xa7WN0Fi07qIps0xHLHEairvv/Gnr5OfqcqFw2eLNAQcql8jTdq+Ggow5NHpiqv0IQQmscgGAsVV
gyvNnHH5o6T2N0Fw+x8tr1WGpR0WF9MfOZjhLYv0WrYKa12YGcZW5jhc7UCL5r2Nj7YqIz+ZBy2+
NmWmqgxOMvyA3CJf+774lx4N0RozM2IplcbxDzlcnJydpvEOl7ms8nXUkiYRfz91b5XI4iL1Eh3/
YDBbXce/CL5vPvx7bxhjX3Uj+I4l22Tv5aEYiLw3mpGfA06m4DVthP7d7KG/bjqmWmGC9dOfKtDY
pBOXXtkhEFcpmDvPViLg1Ka4aId8Z+QGoCWwK0o+jRbkCdZbpH996MKXtEG2yc4SmrjeyA68Ro+Z
bqdzAXjU8KsWNWm0ysmg9WXMn4DPHVYU6cvk70Mv6fKTpgfjZwG5MFTOwxzkNdvXdFNie0v1wamZ
KZkaorkYa2tRLIayMzNmfA4XweNnM9SlFGHKsPp96fvDYiwp9e287X+7V+l+D7s4Q96yPz/m/aTR
fTr7o9n9+V1js6TEnXRmhCv/UO9tH09AG/08H22N5VTmAK1qFQ3ncioi9ObbRIG0vMjuru7ud7R8
RmOTdfbcYNmE11tCAEUE4yeUCnvY0zpGWe1XCzWUB6vaF82nCLwf6IYgWPyeRvvpSbBJ7dI7s7TU
xYmg5kPQfqaeAwn7BLkVOzdqomJ83N3o+FopwrhPgC6pr6nkC+j4tH+kR1OAd1Dq4gZ+cTvScrSe
zpF9JqxnvOjknak4RodWBpdJpyWr5gJ5q19NNIVSAxx23/ahuUSfgv75E3qWXonMvuRCKTSuqFhh
2PFlXp2WBiR+GXNVpKJZMIsK8QPIka9cEtP4/nY7qeFFD2zzwhZaZTNitdgsHTWnSRnBNmwke3/G
xTbQBaKIjQNCitUMBV3pbD8qpPcWRrWg0thHu1dxU7OhehpLprhjo4vLJAEtiAnuItSwBtAF4uCg
SZw2p050sAawOpYEZfn2cEFkk+lSuaV+fDms1pOp/Msly04Hz1MvYFH0pUCAmlmjubfNhsDrbQwJ
lr9C8MpJxJuwaEN8zMiltT3GC53jlE+i4GTiHYvN0AQdH2BegJ+yHOEvj6caDkGFNqBXXKr4G1e3
aU90KnCU64GVL2ydGZ/SZ3ys8NSMSUT6HNWlCZYSgzhIMexNRbsZ25pkZ192tEVMOzHmNyyZG3sB
Qs1DGOopObBaiEYE9S5olqkE3Z6NlECzYwtHJ1fHREGWu4vaB0D9v2HIFKXlYd7KL3/zgeakkFtA
np9PSnR3E109VZiE8+e6v9ScNjyO2T/nyzUB3CQ3bsI5K25j/HbGp00FEAassiKr+CKIbrEmdE2d
Za87egO2CJle9yCDAy6apPDgpjE3FPuULXwpJU9aRKbqPUGmKVnqXI81Oob8oDziD08EqcQ+Pmfd
a6mBbz/VFQquq6y7TrNFJm2dKppv8FoTX03mrrEVR6fgaftAhOgCzEolt+40OAdnkZjHINUdrZ9b
J5l75TPXLuxuA0TsuR0kyv0YeEqPl83iEq7bhc02xBuSo+Qg5CktCie2ksPxv96HVMCt8FLnyGfe
6diWqmoSf9xeWNm20ZX6OyCrSdNYCmEHuttJpC7vm2/UnuYYJVTOj2C6eD0N0PGlN8D1AzNgJ/1V
ddHl3/WLVOZQ6/ynamFSBP8JUsLxQDff/y8WmQWz/rLvGy086VmX03yvvpy5Xh1kHp1bgb8RQ6Tq
JYrRWyA8TbTQvp2zHgcKHvxrkd/f9ZQPfIyoFuj3JXPN2ElUchxgvVe28LsNybC+0zzbJFHpXF9Q
qmQHuAV1g8oAxTfxaI1jZPg8sdCa53BShcT2W+w6lR7lKr+kjfVzQAOPuyXESRNbn0h3bpInMr1G
X6zPaPHTlcygUBXDuK7MgZQjOn/KBGJiJ8mAwYtP1M9P1R8McHHYA74aDriMOEvTPU7INAykYxZb
ROsZHvFbAsEVoPJODkCzNPXRA5KThUX5i3wfdg51gzk67Tst0O2DfKP8G+opvWEErhoXnqY8C2HU
VR2xgL0ALX/iZ8UAjfbf2QhcaTtfFPdMdjE5wV+BdBdrDEo3qfODMPjSqJNmASkl2QAExstWYiMP
WLKWPC2sInXyqdlGkF4UoP7n+aWSyCaf5ckvOS+OFEal/1jEKFJMYCVLR9suQ724FuGpPTaZe4Yn
VrpkkijAKtOTzBbtPN07jqpEz3VYPQGqDBhkTuVXlvNH6h4VoHggNqThu/4B8ZZNNYWFpOVgzW6k
XCmNZQytZ1eM2ljbZAIw3kNJCmI/uladalYtdD3i3oYjPtHX1pSVMj5OSI/oW8Mf9gmKggUxQdz+
Bofzc+NkVkSGDfN5h9qHjOkR4FzZie5xnYqYDBPPh7Aq8b++wkEY/aDImvwKIhjvLIybTy3EwkYK
NCDZJ46CJj9DhsIV+aYJjdzKHLizpDPzC27Av0XzXxLJ9/qELXEYYtPJT3KZPhqLq+7iclqVDVDE
zAdwkI8VvvlY2HI0UQo0mhcuav+aFVAZneFY1506KLpP4E4E8QxY8qbPGKeF3wSGm+/p2WMWkF5v
bgY9zC4N+krUBQdcWphCn1Yq5h+Hyie3Z6iej8sFGxLdr0XEa2eRBH9JBbxosI+ELEVvZj9kOGMC
CViDEzT+2tXpvMQBYJTVN4cCDmqdXTfKHoUVZiyfaCs0KiRDa3ay+5zOWVfBmlNZmYLYFEuH7mtc
WzQyzu65ZFpZayoRZ5D8WmwdaCAK+HKAzO2XgsfkMib0NEhN4CFpf8nNh/n9PE6jE6/u4MzxTQ9G
jwHoqp4scZMoen/sOSbFtjfFarYmAOvVcjH9cCA0tcc3HlABBe1BzdczBr815N11Vnups9jydkzL
kSkYi5hcSTPs0qT8I8OxU6hod362xpf8MznY01xhzTZ8W2lcNF1aR46xkoS0GNC9n4YCZ1x/rC2g
ZlktTzuP39qdTnPv0WSaSrctbOz1lIU0tsDuts0T75gQVd/hxEgQLuUeOGWpJPXYTSWLXbmVXlOt
F12fjI+MYpVFFsibSpla2/dPvOvRFwANX9SVr+pVQPJbJHGPDaYwp4qOow6fDRnJW4PlXDx5yZCw
WAxwAXx7Dd2mhuIeYWSmSTlaWA8JoOQhfQY0lWJ5vSA2Ddxdw8fFjeIXp7XsSpb/4ClofFDIERCW
a9pBuqGFsQRINtWTDHUM5Yko8etTuE80beYZgS58XQPAbrLRrzqeE7n1tPfIgDa1jtRNanplP3++
ik9p2eDRXxdKdtSg36ZQVelNkYy+pRzKpVX5iHrepZCZjBb7gUmWawk3PAR9RytbkDfe8KjsxigO
SoKBWhwSahU+pK54CU+oF6fFpaC8nuxWkpA9NW+AGKWiSfvcaxNFUhign/+ud7P5G1kww/vEzhve
FaZdlR219jXsfSzZqgliz66yZZwTrf7qg6t2Nr2svT37poe0v95HiQF5L2a3SkPQnwQ/Mub/SEcd
2YNcW2sulnL3jsz8bX3BzdS5b/aub+HDytntQa1gm0XiDzt5Utrwbxim9qyAazSY8AnFqJl2TSM/
ychxnxmee2lrD5rPdYgMR1IBmdhD2x2KziW7AWeyo1OH1WIq6mJMgu+ZpPzHOqhrUNEdnZbxXIu0
RMXXwiM0juRBxfb937tjWQa7Bj0bTfVhzEOreIVJMtN83fLO0wejq22VBkHabAMPM1pEb9nsnAtw
KB+Io7rJwSMzKIqvt+IQ3hT/1pcV2V2T9teblD/s4iIGoqeBpHhVMzFm5YN0hgku/2RGTVutjpqa
CP4AJxYuaMy9lxzDaJ2E+A6ru4nn+GeJ7plhVtieUe7H+1n/JZy9mC70L8y2bBJOfTZ1IjFZvAAK
CCli0g/fXvay1+5hJwfNtOQ2ztIUph/2W0GVr3YzCVAE4eQ/5L+F8/teIgkZhYipWNoczf5czUR3
tCupSgiNS1CQ80zBnadXa2bnKM2ARJ2hGXXAnkCSjgisvIV2aaf2nOS9sHCBrvSj54V2+JCqfwI3
KeJT/Bdyn03YxhMeUpffeQDZxWTjf3q8/2c2/Cli3ykCW0AJXFqDJrkqCCXiKBvFzgS3JAKjsLlB
ky/kMYALAIBgUF8EsWf40Ljz0to11voMDl03eapAaNcVo08bGaVQXqeRjCrVCVj4xxyTdtGq6seL
NmEJvOH9nft1o7z+99/6lerxrOCAALn5f53E//w3etGJ0RTsoPo+VPsfzZ6/EOYKCou1f5Sfnraj
cd8cY/RAXrGehiBDfpLfKwDyi2xk+G3Q/634b1fQ80EfOMJZPVxB1GFjH7/rJRuVe0m6JM3XBjzb
mWOlvkUeIAfnQdVm9GzV5x2P/EVFtfH1w09AvB09pKphU6KOHEBRbuJTfoZEG1qXaI5YC7hQEYV7
SaHS+mehuPb5GsDIJXdD9hRGHN7BeW3kARaGx17cupe1nJBmHnC2S5q6LWXYOjSB3zYjwU+q/iyj
bAWXrEvojzzK3/nvNOHu6siSNBvrUA/ncFxGpTphhERieva/yEvGSRPLJKe/98OTPMr5n32uY3ZS
rT0pEnwSi6+4hd1Kjf1OYYT39A8aZZygCkriXubp826cklPtki5nzD2IFQOqh/O/T/WjBBUoA52k
IXL3hEtbnrErUMWyzw93IzIUtaneqhfEfGdz1P4348WXAYTP0H4jBKZPVH6hg+ILCdTKny2joru/
fPMPn8L/48t8jxMCYu/staN0WfBMr2rpboKGzeatowmALMPe08+O/p7XeCk0h81VImQH3YAj7Wk+
Bj1Pe8hJhx4pbOn3xvPzXC3h+1YTRZiHk39Dz7mZSZaVaFrXvQ0NhblCIE9oTJewisBaWEq/6F8/
zkf2sRamTiMvBamNkdN7q9GDMoOiA1iBxwwZESDGTGhufankDKVPUNKBthON1N89tZxfqoAgITly
3e6I3lc5Zn7SIZbwoPKEjKahXVZdSM1x4iQWY71LF6/SBC5smtZEp2eMWJCHKzxPoG4PnykzBTuC
2zrEX2PNTDg5RjFkDVTNbRmTR1A9sppK2zcuF3E1YAYvnuROz0PW58aU1JQl6pbohNY94buUu/0r
fYW0Jyn3VTuIAIj7TmCVaNCHf652X0YikYKKDIF0pG5H90Cel26bNjdSb0fYgEmlfz6rMReNCwcT
UgbuH3EQe4Xl6UIQtUACJpaasyvfp5g4X1eDtH3Kt9vHCYv2eDnahjceo5Uq6Ct2JGNjOebCRutf
Lz4YagL3q4spYXnwEwDFx/avBQV/UaN6fH8PcnEqpqP7oFThYsObr2QD7MpOgAPHkpS7lSA628LD
YdNA75z2deLmyVdV/y7qXxasR/zaVHarWa8gGlEv+9fE8kwEye8SvAJ72ThHXNW6HrfGfDecpDb4
WrQT5lwXS3ktX0LepJ2ZxNzS1KN0taJfFW4cB3IlxK+IbyAKHKIba0vKqmlrI2PBjMVKy8YrcTvd
8ao+LQ5MRjTO9p5l1GEsh971gYiPSqsYYr3o5ytddRTX1YLpaf19CADpg/8a5ct/BJfX0yNCozUb
Zxtn/x004XAFWNrtXpE96blc9Ezfja5yeLCgV++4NF6pF6KMeh4ZXCEn6vzwSjUr3iNvGYlXVRxr
1a3GqY7f4pg89h8Kmz7QLSWg5qKzMCEs1DJob/jh1XpVLb0E5amAow2fjdmuMEPLAQsu2keNUlmr
nMxERBdHT3J2bjV8I6k91sOJba7x8SPTdUA5OHyDK1RKX8f6BHg3KmU13BlbT36+gu3PuPEHfb+B
fbvY1pcqgqbyWZrVgtdRddrm9M+hcFYTQcgRnBIPCpD1fQpzHteXrOcJsQozW/ZlDFS2aUknEuIX
trW+4VrpyIaroQa0bMZ/diA/BOU064Lsa/RG/rq+7C+2PA9aeDRqgilzisapeKtduplZhETVwiKw
B/zDq8dJS2KWq3e16FXb5CW+l77lSsfvIIaSp6fBzmCRljsJdIH+myIWdRiKhln2emGaxW8U5+q0
lMuWlCloTwUCvuGAjZjeZm84LB+qLjuhaMggqMBBl402s+yS/qAHhtqD13xMqfInIrD9JeQoUyMT
9S2TUdOiigkSdhjhm4onqYcX1csxTNBsskhUb6HQpwhTnGEEP2AhkRMFbem2FtKpcp/7Owwlrv1X
rWpAWMdmFC9N3Y6+k9SYAljKNR7aawJPXi5eHcEEj77jhdblysbygAARRySc809vfIzxKFOUeMuc
tXWB/B8x1+0eTWVOWKCFRFTOrh4ptfB3iW8yZF2mcstyQ9j+3RjjMNHF3Zj+ly3poblFW3ZuFnRS
HGg5ZXTvXGlLhVuPoIDEpa8kSNUPgWAebuxs4hVHg4RxnXnc8iwiOiypkQDXI6MCcFWZE5nIsW4d
m4gNXHaV6198Ih9RMxCMGfdIzfdoDObiVlAsgCag86ekoNoLOL25sP+308g/e9jHYU+7dE0MgBbz
4r5RweDsmJaT8yPu04m5WTUQkD7yuXNcf0tFHWjLQrjc02NujiYv1E0+ruznU41pAGEsvgNDTl/E
0wIsff1eFI0UOL+BObq+mVDVCdHOLo+d73KCfoonNwQ65lL2xIm4NWNIue4ClAElxze/ngs4cQfW
cIcsrZQJtKil4/YMKg2NIYxrv1h3lqv2YCFTajXTh3vC9fL3d+gfiXltssDbknEcv3ax47V7i/4Q
l888KKPLynl2q64agwfFHtObZUqbeQLj61Rg1gLaMnzMNPC2BJOQJGOICDx6JLX8n47NvvFxXd8u
jb3Jx4wCTkL5gITbDTwU9zWM9l1zbDk0J7lViQbd+vWSPYQRrHuok4m/shEmCw7Gf7cM9BM+ai4u
iz6KspaY01ezRRofY44V/5siNd+UqRewW2Ht9OaY1xFU3r0YMYjCvBY318tmNcb+4j8OJgcRhuRE
Wn8vOnN9t9yz6Ug+HtTPqEqRvrZs/ZJj8uiq+l2UURPwW8Y58+ZNF7K6xVX2BLUoco5SVXoAVprx
bY4OCgtVr7rYHuzDVOWNMQf9CjZwfZjyL0SMigXYtCg12v0oD6wlEgHAfzRxoCPJzUzA1ejy1jrS
8ttaUeAu3IM1t817xcn6yh9ipqBsp0QgSlzA+2lcKMVVHES0za0XxiNYmZZ2NlJAKQcbT/30RznS
itY2a+gVBkbGLu5uFEEYSE1uSKz7x2pJImXMTBTob1aCFcou8LK83U3R5ATt2+3efJ+OWI3zSOvo
1TFOzGa1ZxOZjVL0mSly6pI3ZAHIWmMmsDtjEeUHhStws9ehaGwSu60C4vQSU4Kb6J98Ec0qnc8U
DJbq9Jy7iZNshHXmGxHuihRq6QR2QIllGz66BbghX1yfpumBKBf2RCayw1IjlPgRyQH4EmW2Nw8w
mXqCMA2/ACdL9ch7OTRH77hDvCBweQcWBvkIqY8MdYY4/UQYK99onDIxtPMTDng9TP2nnP7rhlfJ
CBEtaXUFym/kmw5YSEUCqGAU/USsOgNJA91BJQqmjunWaSSjwmsiXVSCJAPymFq7eAdkLeocROnD
B0pZ/2/TU2/D21nR9o5wIyRWUBFfnKwhNK94xPhdCIX1GkgauQFo83CsrXsjbVAe5dMTSBun20To
RGglFLBakEDlXD+OufGqApw5d8fNk7RIK98d7I5abdf470w0F1TiJiuuJD9Cdrk0jXgUCC5yNMiu
jUrUBeLAbEgNsz/b4xrOfpuejQ0V1aWkKHMpabeADmqJlYNQ+NotSGsnwlotBGqJjBzXhgfxV7qd
kd7JV6DCVn7zdVqykMpcYLWG3BsnmL+bX4ECNv+rRjGpSClj2b9n070aXENxuz7BWHJ/mhZH3hfG
urwrjGSEefBPBAMNM6gIUsAPKuHwS7g2cOw4n2IWQFKncdPq0hIkjS/+OTRvNNvOMhL/6B5ZGJhh
bp7YgPmbJjpV2+JBq5/jsk+mCFszQQLHzjuuBZFR8U5xAGLXW8SoCFlrpQJJqokOrKuU6tjT9ZRp
Hlrk1kSodt038A8kMh54Bp+2WeQwbSBNFEcd4k2BW8SgJRaVMdAaAHJJ/ZoKLKVJ0yrBKp0v0DG5
JZX9pfp83RmPK+YRaUNnvbOyesdyt9SUxUhMAiBq1XtvcnB594GlMpb+ofTVw6gjTcS58Zhc9A1T
0+VBz+MLLllE38Z4gkj2YvN8ygOGv481l3cXFdoU0/unU8M4YP4JSEJDCG0EfkrrQaCfiDt1qIO+
pegMIv7XdZSVT3iuTio01lV4xQwmW1iY7hYVlMlwIFwnhzUyF9vTGcVHAUk2dzZaEtbs8XuAqZ47
cMYG8wOuJCp1jCNYe+50ncjdvOfa0WA1RdtAzgdrtcZT4p9jyhxwi3FlB71nqrneZp9j0DmdExw6
c+Ft/scUX44IpCnfRyQg/87OBxNCUxH7mK1Oxpirwx6iDnwpkMfFEr/2XBuTKxAoCc0H/aq/lBeI
NJXn+iAcWFd4fZkuvnY0xWoS3QLdAeyj97hBq5/efl+ii83Gbdf4txj5zbBriR8sJ9OxTNCr4CqG
E/GhyAdOAmAjkkzC+61V6FPvjjdJBXzpB3U1IDAvcp2CURK5T2jAKJDbRByhMTucrRtdcWgU6Uot
XSSPDV4FzO6e2ax67Eu3bDiqfjCMYML+rudHOQEgpZ17gpY6r2l0mev/aKKISf4fL9D8y5eF7rBw
1IQUJfP7f66yW+Zvpjtk6A5XlU4c1eij/slCU8EPsCV43X2CZHSgNbvX62Wqu/6vKvww97HYO+TJ
QmJHZd1GBKug1s25+eNnBF6qw3cf4sNyc1B4oYUaQaXBaonfGCpicXYu3w46PAx70uMD84C67S1F
kLxR4Uj8I1jPToFlFcowlcA2h9ujWANCKhQkZcWp9NucyDpr2g2lz4Q7C2wGH6asGGoNo3ywuZv/
pQmhyBWgfSPwWSpETK1fgamBI6bwvZj+xeUBzy9zIpM/D6ClWbyoRaErKtXMmorcaC9ARyyA38WQ
QElpxtJ7L5zv5pi4p1SIS0J+Zm2cBoLFUxW3bK6JepxmNUs6HHthcPUWHLKkigovET4EVQHJGURN
OU3bOxNfwSfrJywXTobQTDvyI4Gj7VpXe+hftCGtcS5tGi//SY5UxzlF4Rj9GUFcFTiW/t6JBu9k
mNmhx/G3EIxPLxvAXPCcjzOFDjhW9JArL1TX/cpA/4yKkotGLj2gWm7CK6k+DcGdehvirN4peiPA
cx2JZDGIH1bnrmA6MabBewNPPNpogVeDJfhG21A1L+S9DV1ZC371LvIiyynXACU5O4W330CXRbDB
qbCvanoXJdDmTXPFr1Cu5l9qrqOxnUjWFSeVvLEJXyrYXyDRfATohBnMe4TnIa6ss0kOpCZ3rq80
JefFstWB7EsLN6nlJJXC5v0zwqAzMabaJisZdvLyhCZK8HM1OwEfauY1VDkOwfj6iE2xtEeh8PZv
CnmyP/J6hs1g5EzOMSAPFOE2ndC0JMYTPymFn5bz6gEPC6fahFlL+juX1XgYmgduwt6HjT5QYMWc
38J5GFNCxFqiPzXdNMcvwf6Dq7F6nzIJ0JYlN1FjS67H3PQ5nI2rvU6jeDFjcDIE0Moy8StGQbvj
JVnxrfstpHw5O0pvxuqcz5EYKeKcLLUk+dSK208l2l0dOC2JfGq7I8vvbUAVFdJvYDsohXFgtTIH
6k3KsK+L5cwff9p/sOcMydudkDyNFo6EsmMO2yX3eUSot2X9lHSjniAa15oHv6UEtxMMZcTuwyNR
q3pSGqxFgiUGsDZ+djpztjcIeV0WSwLfYzs+zgkTrX7TtgdAA07jBGRHoBVK96WPwB44I6bdg0fB
Q7Uh/reyReIhbplWL+O2kihmdJqwpSEtpLKj3ogAw0kC31ZHWg8AsbPeHQz1CUOJxHruQwIGenf4
m3wJ/gvaasd1fLLGdFJzQfdlbRLy7ewdcElxBErK+t0hpO6eL5WAcNo1m55qtQe1BYLOfaGffB8E
GRXnrVs7lTb/TDrxOtwO2MbgzXJsfFuEByuKn62MDHQOmDEMPHLlc5GDirJEJCdsYLyE1bB2U2W6
jRA3R44PtOFkebBU3L6yYy7dCMAWH9RhQHB5R5xtLueWxksL3vqVQ9Two/AViKGTbkj/x+bOzcto
RjzOV9vRe+rHznXFrI61ZwcLiyaJjI1bsGwuZabEiiIVTsFRV1LMOXie+4x2aI9qm6syoIu3U9GD
4a92GoZTPIB6/qymgbuBtH67oZO78daOtia2T51RnAGDWcy4iU5NuPMyvzbYsBAoUtFZThKeIGkP
x7YTRJCjf6S9CQlgwsIDDkGpu+9mZ/iiFIHxvJ5rMM5HhnSCx84xoMf1CSSy2QXeKl251UbBIAjq
A4fego29+D0EJZOCt9rAR3hQ8VDJAp+KFG+iQsllQpDWyQhaeo7tJKDyQrXJXVbK1o6qA/bpKaVI
EEKpUFqaHITtIJbSKhzhN5IAKfqAHO1BAJkUDz8b2AFYPG0CJZ6DF9m1J55sm2TCvLS/tCrbPudB
ZtVv+iwINQsTAnFEZmlC5eKQLKCsxrIZdcT6LHs+xZMHgFTT95NQbY8zw/rEe4Wz0CZI1edGq2NP
rgQErzb45WoO4qngcJkfOc7prQaGyuYKwSEjgsbohrKDRg3LC6vDB+0pj/bIIKwL9oPkCnacBt1z
bOzymgEiWZZXQgLcKg2XIA+oNKzApIWQB2xlgbxa8RqzCHZbKufU1fMMVTVGA0M820V5Wr1Fv75d
pE2x3aQBd3ddAhHs6cQtlKRTr0yBoeyrN2oZKQ6NakGbvI2R7haO5FAbNKErRbEV3YOdLs6Ddhjn
ifazhTYeq/xMYYWJbAmEMh5qSjdlBwvBQdSHESQIu1ViLZWrBSYNHQwowTNJ8Vv0ObxAun1HoXpi
p6R7hwt8PENX6jGIBQ2muJTCq0s54rRL69qv5Mrhgfh38NqRHTNSlkJGM1Q/CH3qu5+UtYj36zzg
YPzQIqIxI829HFKoQdap38NeY/i6RJlmgzDJazfDdpG9IHbLNXTbRfiflYdJFaXJwS5YX8awxdKy
yGeUa4m7JxUXEnHld5znQv+vyWriQowtN68njGtHJ9MF6DlVA7jeTsUjYsdpAOzQm3WSfM4rP0rL
GxGrbvdb3ICLU5J0rIxRY78DNobM4K6HTVPrZVIVXdrTpO2vlfTQzTSUMHwogW9taHvoLZudPofE
Tsl3kfgZkci7k20BUe1ebIAE+ujRzcrCXRNYgK2eF25MeI/PS51QNssCu7Og0C1WUHo1+yT96MBk
pcNw86N2NWLkGm8qRkDS5PARu+236fySRmGzw8E8Tpy776S5h1nnXcT4JEQRptUB5/BKaVQ3ILq3
nWmYIkW4EmQjCKJ/RCcj77pRIZu4VmaNzVg9d7UOXm+ntcRczDKdJJz2CsVri9C8/z7rfYNSk+aj
lRo+iQTSwfsJ4qu4UdEo9RLqK1C15xKomulZtsi1GXDOt1WjVZfrnl8O959zx7lIjpRvCcilX9zR
27XvsZG9BmLH76JvqqMxtJPqTrKgiI7/GUSXxqq6TnNd5ELxAsObS89q86kZUiBP76GBV289WNwn
Fk8f5g0VfMpOsb92F6Ma9Pvn9uFyggjFWEr4X3wuKLnW4ZcUeL1M5408je04+Km6BJmHE8GTuUAK
exdmY3TcoY9rTvH83H0qilwleSjNl+OxaMZZpQJCWIfD6gPTRZulVFg51yLOS65IEPjKOePb2lqk
YzCvtC/cKRUFOmFqHowf5E+o5WGxpVdRIuvXFHuRm3KHFWXPATqsKxnNTV0uX/b+sP3RClm3Q63B
Qt5u9Mh9tPgoG1Xtod57eH41UUVSgCFqlFrqhebq+NdtoEqKsKag4KWIqcxPcDpZJAVLUmKLDKNU
pj04dIK+ndkbm0l8pa+6RMiECgJwjpWAYgoW9Cbo2WcJ7ovM5n5+P8UkoUaL2LX6cD2m4/s7vWLX
+djI2UfbqibPbTl/x7svT7E4TrqhXfIm+GN5ZvoRIdQcd+F/Bj0JbNXBSdrCr5c/hrqPPKwxrgpZ
Xj6O39iJLVOfx92ax+S04HR6Ypb7fV64KgYnB2zGgE6zw6s/HertCZJBQy+pFpUYqPiG70L5GAaU
y0uwkTRgjsO8XbmImAf0ZwWe8BtDPIu8gWszI0Tcj+YoMYVveGdiZHjaqU8Amk6PiCCR+YqpcHsw
iZ8DNHvYYWR3tGaEF0qNW9QJ8Nsn+ViNqprJrb8chwsm5Rj2NJdXYBUGU9HT6IjYtulpfKLEdVrG
V2TYSayQLUGcJMpWWNyLef0vBEdClu+vmauR7xACCwlxsB1w2UlaUrIK8U2iQRv6ebNg41/AP6zA
Dpw9qctslceF0IPqiSu4Pyk=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
