// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_22_1 -prefix
//               design_3_c_shift_ram_22_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_22_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
LavFXNUnwTlyyXL1UnJ68+4Tl0D2d27toVISWGvrB/uNQOVKPBT5Pi2VX1I8vbtN4lGXOSHeu6Cg
fedjuZeQHgMrzU4oPMhEM4WhdEpV5wfzrJ82vVqdjg2v12wR+TGqgFjpEf27HpMaJYakcxZ1n2kE
GLHkCIofoHMKVyP6ZEq1QVO4tDaw8hEycryKkec3OhpzCUOF9H/LFNqWqoeLzxxVXty9xZ5ifdMs
amrtvLiRkMsYWHIAJsuazzEO7jv2xKj7X397hK6wJ9FQuueZOFqUwEZe+1CKDQafgKALrB7hVq3J
g8rzis7egPO7laaN23/w1TZf3SUnsaPqbL5JSP6naTdsFgQh5kfulpr3bcnC0NpYvSEh5nSUcYsT
Bj/Gz2URIH43lWfiIazrWJw9IfDNcx5GYqYbH/+bzGwmHnxdeIJHIOfXZQJKlDR/Dq6JQLM6C/IV
tqfyV6eCOjXhL061aSTBBaBgAlsMoKPDlURk29rqDPARXueLyZ+DsYmD/MX7Zkk0UhYbEiaHxbpR
ORqYi9tdB2FvU1RueO6EBhoLUX6Agyz/Le/3M1QolAPBOcmRPluPbv15ehOyLd/xxY9YctqLStMg
jBQ6nkxLQRnDq7Pxc4Fb1TV/EnhR++xh+aPGyqDVsuwV58Qpn/ovRW9wHWOEl6UlXL8+BtNhJ/iy
qJLciYQXpcKypEOyNnuN565pOakoPelNgXCglnjnkmhsKinxDkRkMsTbsidRqP8xKuXg2M0vkkoE
dbz+Z16VbTY9eipo7Mjcazjq1fp6oeTPpx7ZMLIvWCB5vgZZKS6PQDBhVcYDIyingtYH0taUSjXe
jCLgmEhSd/m6RCdwuwQdu+RwcAXYjQRg75LXPe3lmPt2fnEf6Kpki42bV2WJPay1xDZV4X47tBEc
CDGb7BXg6+ddnblmsAyi5c3gK4tp79QUiJBh1pFn2gwKx2KTyQtHQ99NBPmegQoER9P2gKUR9M6s
0fPKG3/rJ9UGVvU0AvgwfRUuwgxXbG9RqWpbO1WiMRt2f5KCtgbl24+BxX1QZiEytt1YJjpxoxXA
Q/APUWtx1s2/WqosznYIR8NVJ1SpaDSoJfnCEUJV7qM4oW8CoFHTXaUa9oPwKbWsXOiQC7IF38jN
CgHWZmNTtCqy58LpSbSn394909uM6wJxJNCGIz1vdbdqLeh8uk/TXo5XH5PnuWCJ3FtGNf0uScSa
pki0xHpRhxGevzEtpdN5GwqnamxK12o5wutVEjAwNHywzOJK+oGLIadJwYPna9jjduinxGz6Xwaq
70OIuuEjyChr71Z7kzw2HCfPZorQomWPTzHcdoMVx7B8ug4w428BeZaBH3tGKnYWbuLhM6/kmZmN
3lbIMVFtPN9j+H2E3HWrrmtFSmQdvb9sX+qYlvqItqhmyimgtnvD0pNej/V3Ews/VESofwThL0OR
wVzmzwfWKP+9vVzLtnCuxf61WyzErluIH1LmgyTLoCw2c0LUE61BplWm/TD/XtQRynlV79Q5BoSq
Sbk4TVN6kucIeom7V8DQY6/KQ+ZYPsof1/rnsWqV4Uuy6QAo2+LzQmSH5yxJDLzF4teg1k9KyDiX
DNAFVCpTlsfiH7JZexXkHFEwBfCX3/LTdTKHUr7cuO8Ox0J/6Xcl5YsHHkb0c4vaV65gGcJ8o8na
gtdRZCJDXRVUiV8xTGmnEpf8pqco+IfLpIRCO+Z6v8r+iAwoKk8SHwKmBubzdc+p/GEhtUGTgqcy
LXwA3gVq6uoYFetivgaIGb6MnOIXgmyzmczz5Rr7n9JE+BPXpJnYYXtiis+uny3QrbTUc+Q5xTpA
Kv3QiiOazxUMypsQo6XsYw5gu8WRi1z9kAOCebm9pIlfzubiL+D3EPMT5hMvWiK5G8WqyGwhNaIK
JdZn98EuhpbdaaoL+ZNzcRx8HYa6Q58kf2X06mDH5HkLgagCktqQ0sMfwK25cGswgyUpe8vi+mHW
hvoiZ8Uz+03puaYQToPPTq3mC0UnEFDG/I5Ol1SHcqLWLu837bZ8PpnA3LxEw2WZJqOaLjbVhi6z
v95KIoFaRT3Icdk/rvBcnfCJtUXaRzHSMaz8/f+0LyjMaLXdwERqp1Z4OVYutauqEih0pnye1zm8
U6RSls+zM1Gp5yew9b3EuVEucujVowS7xijjpbihTThNt63azVmf3z2cQqyVp1ivDuoN7AFPVlL5
ldTNgZ5OSqa/v6sH02qMwgGK9P8xbbhi0Mmb6/6wOuPD0Rg8TmkvukY7bVIrHziC2aChm7fWhFSG
WwEI0F7KCk+m4Pg/73LGFQBbsmcEneXux8pY+hGpBV9RU1Isi1WfTPuAlwa6hEAOSZgyy49eK8pj
/ldu0RLLi/2XY9E3aWRPX+GeRyW2g46pSD1t9xXzke+mYY3Mwt9bzYXYfDIt4zradWZQCOzOlfCs
MNLq+Pf6pa8REQ8QaOGQZ8j7VMAdTYVdTCcKs2dZmjU/MSpZnxFmGZ9l9/P4QYrv29H52+EDXxZQ
eIg28MMqYsnGjP9ZF2yGwItnkAftBIVIsO202nIhhhlTbEEKYYNA3bWjrozkQONEfoW1cZgpFA1T
bnlp4PCF77BIY8JQ71YfbNDSdRwwTlUqwvMMJ59OXrI8/Oe+aAjwIz6yDMJIvEywZqPZDrQDPQyY
CACf4oniL4PtEUcSizBDjvahINEtNMZN7SXgx3P0kCzu3MH+WXZLhDwTfQdOj+q7kjvvJnRdyBEr
td/vxzwNBu7cHbPLf3+XpWcmor1B86+cSMb7TwTilCiAQie4Cae+XqrhWdGbmra+qT73BVVNqLoE
UjokbYn/oWFdDWii8bSJKgqLv+BhLv2e70UBzeIfJiund1E3sCsLfte3Be5ldLZvwvgIC9tN7GHe
uiVFA7tKvQjV8nu2OSyaKyoKTSqhsAaMh3QeXETcrAebUbwOkdoURdKnHbDZWwJVY2JH76f5Iu9p
oyWnLWUDSWeKrYFew9kfJlzhjEY1HLQ2UjX8hPHhPO7U5+d+KqZAuetL2TFWiOrGYV27kreDozQS
PSN9CEhjXIOoxVFpV5jymSJEAO0aGNyeetzv1LXOPqJSwV5Sjqx2Wi9svDVXvstDZbdbSznM3yId
vcevA3ZLqyvT9PEPhqGHXFvFUV8G0csydQCIRvLyd0D8y6XKZB9nJgcpnHm71eZPsMOo4FtIEDuI
SWPSWb3zE7GT7KErwArP6AFqL4QTTbivGq+Z37up+boLjJJt1Gu3T+LERUOZtPqbYR9mnKg3mxq7
sR2sL5TuSZFM6z88cTbx7ZuFr2Qb9ogF0U9/MRmIDdNujsfVIdpS3wNKqd1z4cl/Ay0t3JF3qIYs
VOb7a4xzsL5Sy4cG93vYUzLa9/fhQyEq02FENhULRrdDwI6cCLp2BG/J/ep+ncLXPBxDGAcie3l3
UEf8GlhKZrAvmQmkV4KRzyfGi9oZL97Ck6/kOjes8wyLNrf5PAPZvcRKllgnCTGARL3x71dbGFeE
QcI+9GaPQpy4RBF6k6kqEdEkQQ3p/sxj0NDJbYNrc/KT/qesX6eVP6QRY0l6fN1gMwMQacwdrwf9
tloQzxTgV84BfuubLQjLEFTIYrynLkz6InlUFPkImU2A5UiTwrCsfJ3Mie+dLNZy8w8quTCFKz6W
SwMNsQIIUbzY85cxz5dXFDCEyvMGFvvU00gRFWLEZcP59lFzzKYJdOmRIjOaWWaf3uPSbadaN6Xd
u56yk1I+1sP7y2wxuKOrMTYClIGO4WiC3g8ewM4hHboErkpovpfDQ0/3LWWXivMd24XGBsKAFLcn
QY+IJ7itQznGesD5UrJmL7xpnM4eoEggNPM0uiXs9igArmHSwylxxrOAi2A7q6ZyrfcXkHDL4PxS
Mh6re+/8R4QeWFs8eQmdO2YqUZt5kUOTVdqMAvB3vg3SjP/L0iOtjSnm+AjrDSszKhDDjjU8YAbe
cc94t7cHUJtINz/kYyYqtCs0rs2pPedkOF5ghx0rFhmoov55yq9gUjJyhjSRt5cLurEUMN6YBfmk
kUWFX+9NuXyfp9zEHBw9LshKlXnCZYGbZXRIlBzLMAVshdkmZJ6z96iuRQjOZ9SuU/P9SowbP0Za
+k+aXJjeOVovkSflOxTNVWPkurJsW7c36a9qerYyjMx841gsLD3bJLBSrWVXws6Or+wgabNknCVL
jGxo9rJ9cDoy5SQsuOarMCDdsL3UcTgl8UnmD4hQ8lvHZmOarHGHKpPXkd5b7ZuLMMOU7V3GF8vn
qqsRiBLGzqi8rPOrS46BUIUmQvPgNpiXeqfeCRZVY1a9rHNFJ3JOjDeK9ZI/be0K1J4OLmALaiWE
tQLqpFi0hQjErWYzcCCVkubOk1g4HQShuboJEPDfKK+TuqKTjgVF4h92RpoSWWXRnjppKJtfaaC4
vZ22dQy0t9YfluL0L4gi+1quP38fM52tZ8joJGXpNSuwGSSv1YrDajpaK5dgtMTX5eYJOy5pPiOP
gaWf2uIs+YBgJyFc2Tvai13kZZzYsgZbG0nBL+uV9xMkgrayp8yxKdnXzTplO1HIbyF1yXNkVaR/
8ynB9SdX0pg8M/IhFJHsaQujpCtyxbMSUu1MggcSaL3HBzLmLXv9QIEiDpWbE82HVSXXPKGt03LV
QVyRqBO5O3yGHccLNuFNZcCDSOb4snXIfRUb2hQhR83qM9XvfOqYMzAFc3LuFLeCM7SonrBaMTRY
99LSsTvz5MhZxV0MwvDIsTQybDnCI/inw9itDdJzF99qoReRWB5mAjNr3eIXjQD9MiQzIXYJoh35
4RnUhPriDoiF+CqgHa9aY5ciDmPnYqID13tF1XTGJfv87GYAhTqgHWDwnUVuUzWL72dswefvaMFH
WVInN0rRf9Qqn0RP/KBYHPC2c0/nuUkNoa4Q239C4yND1dNE/yjunkRhAhatzPIwLQOIt1nR7Te3
NqNYjX5Gpj9KSuYqpHlgKyMvECeLLpUoK6yywnsS5UhXwHEBJVdIRpg/FEfNCq/g/G8yS2NEGrFH
rB7U+MvNnd71tVXnxu90V7K1rH3xQv7+xLbLkxPmVKjHCzT1a/swAO/FnmXZneWnuYYjvYg4+Ig8
RAzmiEo/F3DkmzXX4eKyZ3pKrJ0mTwT8Q8GW2A1maAI3kt/sh2ocRz8ezfa8OpvprgqlrHQ0qtNh
3eppr3dLvIKuT8TZdVX9rkriwB4AmEeXGG8asJpRuE3AB36d1CkhFkXGMNbuaxHH2YFDVLDOR3cm
C2ox7IWsJdT3N2EZELmVSqWZG2c8l3bIqs+C2IaHjqCFYa3wckVYYjizlcPd7G4naZT3IeL/mynr
I7B1RGvaFnWIKGBY4nbkHIGzhtvC7Ka+tdakDG2/ZzKwDLbaLUkAxzXxLQVCH1+y6ZTv2iP/i9rr
n/yYSSVyDhm0MAQNPQckuirD6RSuTMSAp2D2xx4O7zJOlOteqwgbpIOrcNwTJMovGpQu6AT92/14
HELi8eTd0haVLFjVMfbdbvj4J9c/uJ26B7uGEcKcQguID22I8rB5EDMbK9AQqyG4/ygfEMV0gbA=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
