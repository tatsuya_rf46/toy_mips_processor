// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_22_2 -prefix
//               design_3_c_shift_ram_22_2_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_22_2
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
fIdvud3bFtO4LoR/W4L6QxCf2bJ0+yrPORSEzO9xvyBNiONNTwpQKXBBUOZ89aVVqUSRaqem5R4u
hKKEuQprK2Q7a3uuOvDFJ/gXOnxD4LrkLnZO20rFJ781DCmK2XggDFde8pJT+CUV8JWoe0+Zp4qY
ReWozLAlxpnjSPKw9GSV1J826NwjeYFbNGmfj4IodTOSwbwHkq0dcZ+8E6rEq/+e7xKYyRMFVqJO
vynuJcBuNEV985dbbtt9SYrCUQkk28VB1qw61JAIjMEhoPPk+PejGBmy7WHf4WjeUu3JalHBomPm
ZOyUx+z0U0Yt9e8lcLro7Gm32jxhh/eYwhGBzamxM9Sh2HsRNgZM6F1UK6fny4I47JAh+9nSUNmU
Okn63o5AQNfXKYEQzZBNMFPN28fJLz7Q43wN6fuq4EleuVvji3U/kAPDkTKrbpmBZCKebOs3GU5D
WRpYmI49aqRaruge/hacZUwlIvYR5Ur/3g0RtZ/LCe7Sww4k04h7fBk5t/sn6ECZk4Tdrr9qFiGN
x3KsoSyYb5vBrf1XMJFitk4hNhn/WChclVhCkYcFOVT21T6iNQtHVFp+AfSm2EpSz6OfZm0lPRyr
b3AmJPznRr98ut41YpILoyL99FurWHHXv2C0mcRLIQz0m2CRgjh6WwZIxxYqkS3CL/3j1iR/I07L
qPZDpUi0GTQUneAeL81BWaERtj3wT+gzrBwQ7Mz7j+9JVJt7grMCBx1jRj6KvC2o/81pnmvaD5et
BKCcq6VDQ9+gpkwHpCmKHKzYrNWDuLAEd5rGgXpOgQc1lnK3LJTP64SDFXf/J2z6/Avtobq4NC4t
sfFBl7jPgF1dnzMtos9uq6BauMDnBOr9TUY2EPXDo5IfpF3d/hN8pZSG3tDX4dcEYBdfLc3uT0ro
cCNo90W2nepEqJNvRvpKL25pBAL7rxZLNq+OJ6DoikPH00l7Q3I8PGmEjueqHn1xL5FclIgUrw7l
5xDr8+K8hm55yb5abtmd09VZUu+TrYJjg7CzLteDPC8BMllHHR0BeJHoJYwGbW25V3Zsl9ytpMPa
99GYb3kbPXuuKfc9skLZk7pfKewg7L3iFmc3hHhrPCbi24Hk8UULFJJdFagDDELRg900OnJXip3f
oo4kMXJWZflOBxjXjAiF0YIFb98Xa8o3ML7dh2Pu0hg9EWcLX8gXwR+6nWrVnJ0lcrHpt3uIl4Cs
egPcNQdyNjpUbjXnbrGAjAvpqD2nHDDDIwQH1geHEQb+oHMZim7sBwzwPIpT5bwdxSzLuxGDP2wp
JAG/rbMvOZ/l5hPzxdJo3z2JDVaUyRwBJ4A6Ob+MGM8iiLF+VvX6NxwMZp2GFQh0UOTXLDEGces3
DPGugZLvQDA9+/pBIlP5PhvQ3OGRbV+asbLv2ZRISAbvK7w73jMBM+/6serQNhiFfSU+xr+mZK11
EafHDgski3082ea4W1ABAjplX6/BBW0z4utJ53MhCo7NPColcEjE8LJNG0zI02htefmQQFyLG4z5
d1ePHGnYi3d2bOvoYrwuSIGK8+gChr9fVgzQtvhty2X+KFp7O3s3UpTZ7j+0RyXuP6L9cpL6zoZb
JeJO6BtlvJJKk4KxsTSbPE1YQ33GmE24Tr19xMmj2JIBeVA68G6zJ3pGTEgHrSsvQL5EdPTKg49y
Vf+CautVfj3NFIKDXNUgJK1GUtkPI8GfdNAtXQl+JGEKsR6dt5jWQofETJdgiTV+y5cFRVhQGcs9
iRjHhDw8Oi3imV3rQl2BXop2yRfqb4G9/zIDbKiornMyqQ/qLtt4rQUADQehnqzJFdJipqqi6B8T
T2XwK5OYMXAh5Bq0gElGTvDWrJ8wDxScbXYbpWPFhCDNDZ3Ko2hqbhNAbUuO+icTndL+mG0SALwk
HNHov5OVRBgYUe62XU/EwaFhP5A0qrkWFqgVizWqx+ONGt14PEBtYIxnzJr1LIyKflSpRAHFtfkt
Lw9mdqHP78kjO7ehoCRBeWLdGE3xF/nJEFHPwe7tmXbyqeNDS7a2mQBYJ9zDYzOEh1z/6kIVdjRh
WmS+Lmbc4Kj6XriHzhsTwc6ObiLvxZD/zdMWysX5BTh/atgx13QmuOTSZZPj/Ceqyp6aV4vq9QIu
Vtl0mb1Ht4imiplNKY4RAnO83Doilihwsvz0VZTz4URAYwit+E+SKlxkV6zRlKxv1r5BYhiPt1jp
QlKtLzt6ArFNEqpzqTNrQZyBC9gIF3FI4LTyjTzAhMqBfm1Xp+xiJbIeT6LZjzFeStJH/YqAu4kf
x53mce6oy/MabAD1Bemt3fRSg91eFmYHzGgCs+klRbYq9wp7eZLZMSdgrIR0fsKIoOalqKkIxrrn
CaFR5I7WuTxseJvL/AwILqGqr5qij3Pdogit5TT5d6covI8QWsK5v3KWwOjqxcxo8Geqmh14dqVs
q30iJ8Ht7tCSWLIKrLk39AlLc6evntiJYAA4l1mdHJXPr0wZ8NFOBF0DnvrqVYsO8ZlAlqF4oUmg
GK01PNUtONFW9YFOfXdXISZQ0/7+yS+Q21eM8HtYmdM16PW5pMOG68fJrKZeceB7pxhVHZUYhkhM
5Vu+qXyIocmstrmK9jUkQOTq45tToQ83SI3dpf0tuZ7i0uzw9CLDHabMbK4HmhnusXCYgIpMJWtq
NZon3yf9qOeZGlUDBNs/sjCRKuWfAas6hn2ZZ2mVaNfUXdU5eqvEb8c+bl8NFofAz5LX+R3zmj27
F4rKjAn63MMi7zaqP6VB92cP3vhbS2g3pro28ZE/LWA763fgn8qkNeIX3LTSyMgSzSQSJ1T/Q7L3
9SAf3VobRUPb24dfY1Vbh5p6OO/bSiTOIU+lXSNcUAVenNbqqIo8AbxNpEFvu5X7S3iBxcT8TdbZ
KvuMxQ1dISMYo+pyaa4QT7GkSEtWp7gbdRq7+VgHul7Trp3WBv6GUwJ7LMd6184RTnOo517/BRyv
E8238xiu6gZN6mQF/1dWUTYiGjIYMV9hYCjrwaLcVPbjg+nQTmAq4J1/xMXad/MUSxWCuHwxfUjY
o9sCnVmqfEJeuSEXBribD0JVGCcPInmXAYpqItcfIYRWNEIYAIkR+TZacZ1QsMN+iZiYDaBbvsuw
W/3LOS8ChC18t0Ca6pAf7qMs0JuD2MjDoIZvOvYXcSj0e2axy0dTTH+br4OG639hJkJpGWOgnwlw
TbGWmMRB4cCasugoChnwdAv12y4S3OcJHb4g6t/gDOdp6ZfhOSvmf+kHLy8Zes4x5mGrv571Ug+4
/H3g2l6Nc9d+m4mrsfovrSdn2DG1tvnQEADLQdPjR4ZAaHNDNt0ic+orcAWty7epRrloGRIJZAY2
ITb6oDEl9qsZn4e9XyTAR+H/QfJCvr6N1ri+DchVdzIfUUzCyRJuSbP9TeUA/8kMjTmjXwBW3XhZ
tc2oGJRTkxRa188Qsh4ASo0t0mbZKf+LciWwVHi6OQrMVmbr/lQENSmmFAUcKgSpFmK4os5B4uP9
JBUg2ia6ssDlROBcdBYZpfUqsYas8qsBNfcAbo3Vf5YmWDtSlafQZ16oCAiLB+PTrUa9nnk4JFhu
hgKIFZLMLoMJe48REVudOTY975fFIymF8JNDDIj58kjAyKQeXAmJvIjmEeGDEhPfRDp274cCkUPX
vSJaAOEsZ9e6OdUqqybfaXNiekrgyHGy06y0txMTf4Z7LpqUGEWfCKOLvG3XobzdTxCoUx/9NnyK
kh8XIWlLTkoB1x4IxixMWtyl1+XhrTpoxTn6+hKRI6lr3+Jb8ejbNjfOkOhxO5T9/D3vxpuYUzvN
QDJoqsIl98EYkD04H/uFOylsFZfUNvPo5umREn40LZJGd1apnM7eSpkOGEe0+x7hBce1kUbZuPqY
49n0r6ovXIuXbx+53BiyRze+9vbl3GVe6Ep1kt/6mr7DAu9xYspIilKs5QaRyDgtxsncuZBrvoiy
rmYoUOw/H2QOgfOEhX/cPb/QJjloi8tlP+HuSoIbg8OK466j6ROXWa2Rfe8zvrLkUKitNe/f0IsI
q7BKhTx8j9XG51Df492kooZ/DE1cBxfCPjXC7hdHZwMp5ggUTK+hWXa6PhNMvl8x1qzTT/Umegw6
RPgPCuahFExxgkvn1xmAOoR8Zh+W2InAJaZHitiMw+Mvrmy3gsHB0OUYiXY5bCaoCD4EH3bG+dll
xH0u2NoFx/pcaei9iLr6xaYM4VeRXV+aJr7ZkIQM6S1ZjCCAzw3Xeo3k1Jwp2c9/HPiYFM+GCqCR
CjgQNMDbqDrSxbruLskOk41uI5RMoTNN65BlmGy5aM4HOJbPMpzz6nXD4e3uBpYyGj5j1myACpVF
uj5Vs7So8ix04t94t445ptK9avIJZfDOzfdq9evq4Q9JPqq8S3yG3YaEHgN5Bz3aHGVltJFy6cb8
YvMVsaBfP9zJuSLT1yetKXWYvr7oYRbrQ1T3vTnKylb0wvitHIHH7uf09d59fPlxS3UEsLSavI3V
Itkz6goeozcKtipNv5GZvdszM8js7YieWB0OmeDDER4KnGmkZ874/N1IKpuVxn/PczKUoI4WTADp
zpoHRap8IijSiNZaRf5mSDr51I+XOUx69QXWuChN4Mp2dzPtwo3GsHOY7/QBr85zVMc4UomsXfgk
uZOJzTR0+jgt2p04ZAoGRhomUVROHZG4mOj0ln4EJoOlhaMbKswZJCW8TZcZKsnUM2JJ2UEuxtd7
5a0UuYvg8ghSojlQbe1oxOzF7en4aHMvDFKUiua2zmi/ZYm5wgZaBl5IGzKi5NKI5MLmwH3FfarH
phF/Eg5MfG5X0TZue0w/PqRrFfxNn3L9LDj4tb+DE95os5mY/S8Eu2RRSmOi4WvLxyyfZ/CGDq5i
4kKtUPZqR+/mTSx2jHs3Th1DbMxTHWkzlPy08uOgNWHEzBfcqgTTBBPJeCmecneYrccYHbwm/FgL
ohQcFaEo58ua2WVwV8D43W0yJspRTkMET/cnghtWDgMV71cXdZLDnDDALpgQpzWD7oG4H/WF7KxL
zsOIG3Pxkiu1MaVDUxspqo9zdNUPGSGGJBBUO+YVVE3d4EJAvsmAdgNTry9fW5lVfgVuP7kw/dGz
yAGTPXH+LRSiJSrF/nrpeJ6G861WBB/AvXzsJgZuuXBqozA/FJPzCc58iM+xQYT+tL2bG9LVATvS
+2CsTWfhygNiDWafxKT2Hxpq3mKRwpKUWxzfVW7ziopJ4lVXnfLtMHtxBMSET9lbCLBIArKwC524
+SbPMQ9UzUgAH2FJ1XxS+eRN1IIMQi1848zq/+CO2rBbVMjSfP9JbcZFZqcbZ5tN8v9PfnmCrAQk
SDNJpjhlLfztwF2OZkK6rui85W4d7GvR92MH6NpQaAvPIG2m8djhAUh2msmyqjHOIL3HLdvAfnVb
E3ztf9U3UBnhkplhWV98qYGKKE+aU0kydxp7zIi5B9JvBgaw1f+En//YIKZQS+gVIZGWY8xupdhW
ETuIMKO3otEjjvTAyoKUUL0U44R1BarGAknjGB4bOZdPDvfGassE/OkXDeG4KVrl09hY6s/JdQU=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
