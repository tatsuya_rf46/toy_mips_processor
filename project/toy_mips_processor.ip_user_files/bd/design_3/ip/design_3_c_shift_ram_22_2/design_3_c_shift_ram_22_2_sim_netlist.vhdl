-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_22_2 -prefix
--               design_3_c_shift_ram_22_2_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
VxYHhRIOBDzGPbaApnsEg5ZJe6HMLSfADvFkIlxKa4MA1Ce2FFKvMPcWpVlg1YDBQyDCo3RiZfa/
GTWysg1Arex90CvUAWqDOFF5piZZCSK23Fxkxo9b4DQ0fzmdLhizsvXqQGC+9YPY5gAXhQm46y9D
N4h3gUNbUSAUM6RqTLSCQwbjw+4D/fJHLSs4HDb8p/0s2xEpT1T+3dq+mPWfThGvgKqgJ+4GOzZV
Fu78kZZsIwFXaLHm0YN5a4StAKLI0NIj4oCxIu9R9KNh7HN5LmfrwL7Prr5akjUmJhkNpi6obxli
toYxZ++ows2rXyB6vvbf8NCh07lacWVEA0eI1GyEYPBTDPh4d9ykuwm2RnKPBl+F9Kl7LhU4c0Lw
0YtzdTR0O8oRhdPobXXGAaZGI98rU7CM7ptzLdAme2VJFFUfcIti3GgZZ/qsx3Zc2Lo69Jn87UEL
F+Pbmy6SHnnundARsDufwqiW/Xod+fEQacJn+YyzMmeJ/R+8FNOmcKg35qNj+VkbThx6PcNZlZbu
ppHDR9tz8aJQVFHSnOAIdFD6K1nazFIai+7hgGHulTVreYL5rEPE8KUcJK7rotK/EK9EG/W/ixkU
KBnW87BDGuAfXjlZwUnYX0FlA+i2Wycf4jubeuk00ro4AiW87PqITXvEUnCfqK28LwEorvCi44dh
k/HbG97vqIxNDWFAtas7PIQfNcTlJesBPOHfimb1MurNtmzVYh6b4qWBTykBPjoJAxAQaz7khmJk
VlFjYACsxsd9FmOnoOdwyIUNUaqeFkxiu7JPWqwot7H2BvmmCh6cEwMVq7qFdJctPyYsLDCKgJBc
2mOmR7x85bmqCpaJ2oi4X+7s33k7Ut2N9c+YlXpV7KU2a21Mhz91yNfMbVF9V7fhX7psBkODRWKQ
Px7AGH4VELUkSqZB11v5SJlaOoz2Fi5xZmk0n10Yiy0BkI65F0a8kE6z39RbKdS8eINHfDhOr4Nc
JjhR+fczIMX2ma1GNnjYu6kdGXmyhDOpvGlVFoovwq9JdP92Z9P3Ih62pNZZVJ1ZsC3n0X5mUkWL
GZD9DH9Ye9fFUdjFLX29pnZTZ/9wzsxbWs7TRAi3uDSrmWkozuE5SbW1VwJWQX3gWH2GVYG7jD+d
bNuFJHDvsopk/B3x4FiyLkm4EZjH2BTJclvuBAHR+/QLEzrAP3aA3m/SdmTiDrhG0Df+wbcuQqK5
OhEwXy8aypd6bFDRhk106Pe2B7QsYkteiDBsCVnVUzDCvjOc1+N60tn08ATCjmblR6EtorPOdWmZ
lwhyqggjobr2lfmlfRDLMBmeQL1iNc2wMlBow/EtpvmRHsCTTjYcdsh+biDhH1QfU6FWV1TcJCnL
ucOPRnPM3JQ0BOruLU4GyOtfmVJfaD0NviunXWG5QSiROhlmM+ia1Hnd12HcDLnPprdA/v0xPy4G
3J7SNlDi5Ec7/sUeStCeGlYhP54/mTNcWoQoXx8rfQlAugmX0HNRL5NpPt6m8hw0rTrqG2Gslxi/
+6ltjYUfMctPxC+2hmIIbwyxydM53uBquLUy4XUJiIEgdjdZWTaCMJVzFEMTYh8umAZDMo0+0HEX
BwV8I62EyZV3DEA7duqUxMPoLDXET+poToPewHOHiP4hkfIBVUPmtxhtIPRvJs7Q+cjWiR08/olO
CuHczs0oTBUaXhq1uKo0JChaz3T5NH2mRDiHeq420wsHuiHNIKHSGvNWk+v+Lh7yNmfsamiCGWwx
Mcivly6UEIAPZ36ETiAXdJEauPUB6kgpR6F5D36tjsiWVo9MMRA14TpvKCB54mkdreAvCaN000ct
+vGx6oeDmBqS3K3USYHtbl8nxtkQz8ML4O6ItQfP+CxlOTfUznhxkNKGt3q3BD0WP7xZ8u69U0cw
4CXv/qFaRczPVCbPggsH25FFPCMllIihU+cngmmStzF3OIaDdRnH2K9fVR4OMKl/pTkTdeQP6HVx
Hmb5eynMn/3UfPokEMtMfg7KRb1j20yNGx+JsYgLP54PAkLKrFKxAvHXYejmmCStvGeFYnS7O6uJ
P6tKbumKJJVogVgW8+POmeYsl8jC5cZrJEWge8C1u8noFaLKm50hn3UgwAWmW/IbAOw71jEVL8HQ
7q92jMkaCzTX9q0JYGXpo6M+M6VP0nYug69xo0zOmgUERFZ2zIH8ow8P0fkM7z9POzAdSg0lQ30R
RvnvgeMQtMMg1TaujQ4MVhZ0Q1dfinEdBljHGn5ApbDwnXi4xj3T5Eh1Y0ioFIGSMJsb0BVb0ScY
wpM6zV6PeMFPWckGZeiOZ1GuSj9JqxxKAHR05C6RZ42es84+NpFhG3ze953OFR1T84qxbDsi7bSg
5zKukUqqpYeEmMBRDnO03V9MwbLFyp8kDv2xxasmFAi4WIBcG3utJrK4mPpBJzOVEwLx3PZ0Oy+Y
800AymWVd6k3yaY9ZyEE9g4QStiviiZ0d2X5/3pY7lbjwMRtHDj/EhQBYMymQGiZEwp0q1Svcd3k
oerRS64CQ6p/RlctYy2O5zAx+PY/57O9QI7Wvc4ISF4FdKzVBzc8G4LvyUIudcD3JI/oyMlf5WDC
kM5xp0akP9wvjMPSPoEgKx6KzDzJu4pLw4ItEUKCnCBNKAmJ6dXa+fjh++UUdSWmoH81Eypb90zz
xNFPqFQ6v+e2rnf0fmgNeR64CeJR9SoHg0kNrYrFS8/5sJdF5bVNDGiVdJuvtQ5TRTxF196hXOZ0
LKNdSdkchSCmKKquN/FGh3KWrZIbcDhr0wNQk5MJP4foa0/fkVbMb9T3C/vry5rOVOLuEUQ3K2cz
x+59wpvwIoe5QayCoWQuUJsfi53RJKzgNhtAfbBPVpjTdGgRzlGztklB/A61MImeO+qzYqfLvOyf
KnbkzpzCS7jmC79pM/m6Omc8pXbsxv6cSl+s1aZ5LWcgLw768cb6Qed8/Ba7j+xD7da+wDFywvZL
GUVUNfHJhAhu1tnOiRbriHvfoWLRrqmLp2J4Y/Ao0LS+GjeVJ/Xa7Z5Zw4Jlh5DPuCJ8fynC3iZk
sHCfQ9krdcpDe/eqYlphWQFVR0buosCXNfPEtDbHAOThGPFYwJOLJbR9xpQ8ajZblhyo/4j8Tm9B
2L4QgSB1tM0lU1eF3QVgSCr6iwWN5C7oXI5X5r0elkKEWs7OjFImMRd98+V+9bfmqR3mT71/EY61
rt3+6I1pDECwin4AbPiIL3BazMtrkmO6BSxX+pkooEGoEFCdG4ZZHFrNkqmrhpyKl+boCxzzOFC6
PvtTXJsipdEMi9R//a3gx8wnm135rJB3VaQ/zLpgoZB4qcvs3nQGaIST+NtYe8TIb+PmE5SicMzz
tzEawF9gKyme8ylX4PHziVyU2Mti9HQ6lOwM69GiePmsq+NQaG9mZ3DlVnHa/c9eV/6L7XJeLnQV
iP/uzLYmN9lw6PjPZF/Y6vvLGYkWA8SKq/RKoIafCFRWbr+ApLVx5XllzfXoizFwhqrzgbai6orA
74zbMjyKOEHekocRxx1DznICS/LRD5M+pU0QKyBPI2nv9IlNREWgBP1PwrUor2tMVbsHxXdcHIuV
N4NjZEu3+eZsLHqPNCv4seNEaaa5Y0EjwYAdLDZiCuUwtiFb6//iO+bGvIvmkWyie5svj8AfWvRE
ItYibo7xPmnz8eug7IqlCtY9CVLxMsk7JlIdPpvk45nfxAmHZmzyWTQI86eTwFyoRGXka260Df33
I4oou8v6sxlqPImzWxR41Li+etqrL17GGkVuf/x5C28xaj53ThEGP3+nEod86Zx83/jpWO5LvZZ3
r2js9Wn1oJQhIsI02mJ9KYdKDCzGNYNxwlk0umtd1qrKer2+skO+iqBLy/GeLQD6BWa32IdR4WZ8
D9dK/1m1GdIeSm2wPbHfS1FG+u9lsMiqQ5Sx4b5H8QN/tVa8knJYOsWTbHTW0XImIDynV73qZyQm
ctK1UyTukUlcauKhpwlkwkjUCLHGs2vD410PwSP9+xC8k8WH0nMBiLGtKsatA2B6W7sccTaPOQop
waqCY0IX2K6zWMXIZvSEGct+Ck/gzK4vtl1JtNOgJa6YpOl5Qvn41njN4Z5qNDccklnKAW6jdaPf
bvIpARbRqCLq0eicoos54jnbuuFdPwaY1Im91yPsz1Ea5GAS0s1WUj062iP26E0AKzg2qowUirsV
FXLFhfz+5+Ckn5Py/YdWgvnICNagMiB23UGa0972ABlWF6PZ9OxGiURM/zsIjONeOFKLFqkbdzy0
b6zqkVqTgBtZd/RGOCxaeqk3iQlgaUzfxSEbYhgyWYxwqXuagh/ON9k0sI4Yk67Ufx6PPqPxq9nG
oDS0Vg8pfrMPoHstlu45tedZD2vNRtn3lTJESTKhjiz19rhAn1EcPhe0X+gh3xFYqFZ6b6fqilaZ
gI5q2mYR5kQm1kDHa0QrpDFg+J3kcqJGXS70/r+RZAhD9ERe1cuJOoiaIyPOlGLsj5rsmETRuQMc
gREmHqnipGnWbEfU6BAuieZNnh9sI2suyiRTQPd3+CU4J4CGgMJ74xZkGTwqQURgnduVxD92cXdH
jaUxnN4L+xLdSNi4Hm4n234b5vrpSL4eNVLEc4hojhYD18UPvRuhHwT8y6X6NJRGrbQCyrAucHjt
47/TQ7EOH1j3cZtoR/UmePXstG0lzWGpAMVT+sxIyslsQA/GpnpkBMxqON/dYXxSwW/6WtvMKKdG
KdVri7NIuj7DjyphecqAJKd+2lsOW+X5VV1XgN/TR6s3l/NYUh0ScFUdWH11KEJ3DmNtyZCeAhLb
d7vn+iWtee/5+OP1wqEO7W+KcXYBreBcMcMsVUv/NMxM+9Wj/S5fsq46dqbP/LRwR3lXU8aPT7Yy
9iJILeHmDZSJ4A6Vp0Ry8IEgd4Q8GvHZ9OQ9B/Y4KdIYZHFe93lRBgkXA3vClFh3gG5pwwTdMga5
XRLr0Hvn0mxjvvW+rz6wHZYw//qmly/MMTa2onwFjBur4uZ8VmDpZrwn0UBq8BFAQh+g7BRXNbis
GqnQWoLKbn7eUCkOdggqKlVSeAhTC2AbNFJXY+AqzBczxpplmR25zMdqvrOOdAlFqywOonek50nj
Ap1e2IFWk0J1vxmzTWo36T8NTQdpKvfdXoIDh6jX0T64H++TSZ5Zf20W+K3c4G6ymKAzv0mME581
qa2GzTXLvAX3SBlZekEY8oC9jB4BVOvTcyB5NSp7eWoC1rCc5B4eVbEKmtkdg+SX76gpztYPNr0s
V5l04kmG8N+y9FLtbliMQ1QKQj5qm7/CGeWVh9zzoPJ5EsqqWSFRJOj7WjyXRRYXCKviD6orSs22
DwH4M7d/7xh3R+790dxDE7iG0MHmnFhWeLYFEFig+Ez660zewOS5jzO9VJ17G2rM6s4HpSejQggm
NFC/WG1Bd4092bCzgt1J5Lv1zQtf+Z58iA0D8Mxpv9YeOL9T/ArOkZ0mQB43Gk402NPnbRt6wXLs
awCV2+i2gjJfR4kcHasBJoNQjO5beGVjUMVjiaCwPE+asz+ud40o5K3HiiNqnWmA6vFDWqErWtlW
LkO83VuAfG4W4tUuubVVh1rXJI56TOsWjkuPwp9Ko2PqhpQgMCsrLppvouHEiIMdu6lr08XjoLuA
DQ8IfOWkHkEBeQ5Lq+F7+SDyHgNM7FBxMWpSu7BuVyEzYgN1toHZliSzwqlN45D8r0iuHK8/ad4e
/7Id0m5OHtYR+43vHAXfmco2VLN7Qbcxek79Al8f4/20m5VTjAhoxaEL4NmnbnlZuayTveTKh0Ar
pxk5wi4LYFnNC1Iasuk7MhcCwiUXh0hxqUlq6d8xagEeBxaIzYGDr5OCMaJeMzeW+A3+sOq2FcPk
QJAvfdHwYvJGckNoKHY0Nx8bb7VzNTLwh0fz4z17Gpq0ewH5ml3m+bnb3pZmQeXK+/2hdmyWpbXw
qukrVIqqr7Nm8nCsEz04nPs8NC2UjmGXRWP8JjaRI8fROh1Fm3cgRHN7LyOnFJ9/iVs6PALGZd6u
XPsxIkg//6uqdOrHZf/hMKIqGTi+8Q8SL5dzpBtD6eR3HUSApSvhPoo/ad929XUas1ltxxRasd5I
zlf8EGRwwJVk/VoNoOGoBqJYGA6hULtQvlp6jJRrgV6fTCutv1IR48MO9BUJaLxJ15L5MJrdqcKq
VUAONZl8uKmTllrYFXlpWfNNQsXmGu5kEYz6MeCa+aLNVMOeMCJSGtqYnA/+qGRsW1E5F7C4oSpg
MAbpfXq34SG5Z6cOgPxmAvNx742anELYup4TMI1WSv27htn2QUWdWGh3FKJe5EAlqxWWkOw2sB75
ENPYfg46a1xQJMugjLFc80BorAA22/aXF8crBSPMXP+jHInEToCZ9fyiudTxU4yDCGLhn7VEkI0W
ZMW9AQgWVcWj8LPshoL0U6SQJxEYfeB4WD7jCRN52fFuxS/J/7ndPxXOZTnQ3vI29RzIG47+nGFA
N1uLwarRVXpnJ5G7K0iJFGdqr7QyJaz39fS2BLwE2bK/ZaWmUBJiLYEGDZNfQx9o62xGbQrM99Fx
+lyTWABQcgKC1v+Di5KsUKRMNiRrWqw8XIQY1DoGGRuJor93TJe63POahiFUhCYJk4xaseI2OSSF
FM9PI9y+rudH++Dt6RP0M6HQFzetuyAERAECTeZPOoT5whfKp8xtZ8WoATe2XZrCc5DUWEbuOiQU
ZZTj2Z8X+fDJIPwZoF1KgymfxzB1QIBUGD9/iC3csg3+kjD/VqTaK9/q4bqaeFseoZhcSXDpdCE3
DngMCS4/fILuZUqbevugy59atL1y2MgRN97nExWGBngzIn01QQTQIknwZQK+7jZAYUPkQ0CPAUnc
EDFOYef9+06n2KUit8qLxp4N/KIFrYBXZDtRpTrzB+r3WjPvMa+W6eoZ+NWlrxCMYovk+ClQtpdV
K9JcFzbeYEjmrMS6p2Ik0y+m9J7CXtoO1wqhEIS2viHIeMfXk7xgY+WfEbWZGq+RkV0VHG3KxOLK
+MEMJAddEB94t/A1dnD8s0abYnlN6iEpULE1311vuNu+CsOu5n0Y6b7HrZQuEDlvX0MBCri2Sa0L
Rg7fU8GD/X3b/9N5MvS41xO+x3CnOB9HSbVhEWcwCXx4F66Ix/l4UOFFbGG/yIDHITyKNOh40J3V
xeueRG2UCSleCznLUzW//9LSwY9NTixels/bbaE6vOMoa8+IMJlylsA1BraANyK1uHOrM21mGse1
8X7EkyN2lx4pGKuh5dJBmxFr4C9djxo+tz9WNjkmsLgSFUXKfkO2CohwfkR3WFNsKgBjFlZ/bIkX
SKj6/58mjzsOmhhUTmIx1/MbfAjQU2JKTKQ4su/odU5k2og6E9AqYD+V4f8hOIhXkWYCxf/E5Gc/
MQpAcDCU7DjMvOUK/4fpTAvHeZrd0u/s/vNQwd+5rb+O71tXJ/AfE8m97HvEf0nQ6tZwe1hXIb9U
Oagh/chFLE65CdMdyS6z1NH7DD6CAwQLcTZypdXzRFZAz87DkGiVvRn63sXBRPAiNyTbug5ialxv
ke+rxde4Z0o4GfP+SFFFq6rG3UKGv5fIffs00xV7goJE1JX2J0j1CJHfefo5I6Tj/TKNXVB4m98L
Ei6E9jWXbxMK7W9p/M06yR9erh75uKV9sv2mRtawVkD3J7Bvk7NJxtV0h3AEo7WjmMbSnynmrutH
sTP4bMhUxo/cRDdlAyVq3AfbDfmqdT/Xb+FYgvDbcBv2hEj31ZQZK9k5bIlroiQnXCh5xw/kYf+J
LnoOIqsO+E/PqGJW91YWwkG1yHwcLhHhTXbs3CTcoXJH7d1qS0aGA//f9LlJPsFDs2nGKdy8be9y
+SEbtNydzi++ep0LsNYl6NopCT05ObTqPc+u0QY92jICsTvfsMP1pJjIMdIIaQwdhim0G5AX9wzt
V2jmcQvacayEiAnlKPYg7FhlRColV/dRsIV9GG6V/wfSnfUDP8J/ZSGd7qyPC6cLR/NRCWebOw6P
l4pdQqZ5UXcr+YDpxUOxh39d6+B6EiqMRShQyAyXtu3dwMhOqjD1QBygqh4vNj3Z/hfSocUPWhIw
LaywYLbhM5KKwjCvbC1y9VK0DbwvdcFehdc3liumWOAvxSQv1JId19gYd1Nb/TOEwSf167og0Wp6
K6EkIdmCN8igLswiCB4hD2kg4rEZ7Flw3EQHTL/qZMXpRngnf9wziY19MxKA1MaBrajaMkomr/N+
t0EnfDFlwDqlVZgaTqGjvBmjsNSemS6leLpgKh6hksy3oJX8AYbSHpuLJ8C5zQhY+s0WGtBE2T9Y
ekW6yqjSkcRmVv4s86fRyI6hT/6dmCST2rqwq0Bmy+7ACFphGqy2KKGvYv54X/mZNwlcQw6uwoZx
xUyzDjUPlFlkXfUv64gq2ycbEnPfNXPLJhmoLsueSLs2iyMYBh8dJU5+NPiIUWWI/oC59wXpYD8S
jCtvtVLlgjTrXtysp5QC7pOS40udECYXxn7LOgT7YVM40r8BDLUaMnrn+kIPrG6vJHPdSY+a6iFu
bS+9sz186zfbLkUkNlMXNMcFep4O81Ke+xGJW3lhIQC001MWhdAPMOeUC/lyOnFXPh2cPEK06qoE
69mTBBAgNgk5HkT8yOC3fbcQ1Bf/GOntzEuqCxKAYwGSn0lkqcZUnFoHh8fKPM7sLWxdxXxzSpDz
p0SadWk8l4XWQaTlnbm8GDHpKKh5Lt48jKuaab9UwyT4QRidnrM+5wivsievWPGBAFS04kyY13YC
fFuddhnJ6d9WXBMrwk+ErMIAX5y3hqm1SKhKoVY2C9fnYyptxEKNmvNridr7L/TS6zDZXUXPzyQ/
qcqzbRA5hqXKwqhn0GHWDCNK5TDpTofVEdYK9DER/KVpb3j5MqVHZL1ri+SXZHaTjKJmmT+lVaYR
Tey3+FdIp7T9jgKxMWPOh1TOignWMfG7i5DgzstPDVIUKGX1uJshhDydkKWO3Y4QsZ/KlxwkwcBd
QaYaLbtui4XGWCpLdGuFLJUBUdRCnK6txVQA+Z0gry5DGUP/55nVcSjyJHOwQYtUjqRXbW7BmmbE
V0104VH5XIstbtXNBUHMEh3dENyoQr3BXI9e4+7U8eHg1cP8rxUtizTQzkCcmZiYTz58SUCjnLS2
LF3DA4Brmsa0nVqCmMKun9u2R+89YByXHwQt4cCh3j2RYwiRmS2Zl8Q5+0399W8svUBJp1JnnXCj
c3Fr03EsxirXM/vwut5K6MfLeNSw4irhgymGzUNXFiAnZn22PLqI/dWjBOvWwwy8fnFB6ouc3Juh
4JwjwVJTnL9GwhXyNzrV+lKBb48bmLXtOln6u89TOaRf1fI4MLMCqnwxetBS1yIqSKynH0HEvUYL
xvd3+kl2EssDii2ppTlulNDWjCgoIEFVsxx5bWwOv0/TG5cE3T44+m8p4Vn3ZVF56Tf+epGhOzgx
N2x5y/ocouS+4NS4mNseQnZnlMeTJMAAOERluBgQHce7pl9V64HtvHJlauv4M6J2O15WaUO6uIX+
96IcAZZbYd9zFZTCNk2vLD/4vg2+3Xh1Pz2PDyKAXWTTm7OCtY2h73F0MrVtLvera69vIdLVXCGe
kI0zj0AzzxOFiR4gLiCICkEDjq6GDH3mbzCfW5P8cGY0Z8b9Cu1zccsK9XrPvs+O42wZuhlnh540
s6TpoIoGMg/fGwBhb98FwOJ47hvk4WzPHRWatp2Pxw+eEZySyI3TyHY49l4AC3GkyBnvZRRPgu3O
G2A/VSyY5pMjJF2EZRxNGCio5s8Zo+bSYsWP4CsezJymhzef3UA1CjFrV++6o3kPhnRlywUVKbmd
zEulofy4nkwKzXdKA9ZnOiydh9leUfe64kHgFBsPSlm5g8f9JmDexHi13Fdb+KgokVF9cif0WgFr
0ldlK/e1aw6FUmFH7+rKF6SqtZZzZT5EQAyal61Qfcma6nQL8PvQFTbplUKeBi3JqdmI8rwfxn4f
/QSzLIR9huydEe4i5163uU3CLDpCQ7HddcjOXUCImpf4TjaOLGX2ueK3kJ5b3liL69uf0B7ti3Be
9J5ew8D4RTZjvdxwGgTwaEs5tb/vn49A+KqLT855onVE3Cim+b3uU2WfIvZvzfrXTIkkqw+aIv2q
bSu8TiK1fIaBA4OiF1Exm9lc7DtSKTxya1Q/wP1v/BtW1RoiRBPlhW5KbKSpKjOE5wxPfhySxwnG
VMpSmY08mAmVe9ZV5YweHBq204eQz20Kw+17mRgYgEYz/J2BZoBrmbomiCMED8SfgDnRZ2uUaoea
IoQZXsOTUzJHqyHCec6dsAhcKdG5RKyBrT53H8nMMWjEBdY3YPNEtxrpVppszvlelyc4Np4+C9S5
/McUwd4rRMmaqco/poZVUYtI+EeDVqEAzF602P4zEXwK+3rn4caXeMPXsKeKFYJDNFl3+9M69ArN
iJh42iZsCGwnM4gVBsHOSVZc6XiYDmDfRebpQOu1OsQKhdhe9x0hJJqr1Daka28AzMNrHO0x2vnU
mj+gE0hxOvzHEYdDwFt2Bodf/4X6DwpGnGjsfHBp5D2opIii1XSZ0sPU1QZEP0DhjMhHKCqYqpO/
cFpbX/mnol/mrUrh+iIbanMNBjEBqMlXKen0Q0Dk6xPGjKojiQP1+7IqoQPWEwKDNWqhaRw1qqCc
VYiIRU+NoFiyRodfxtPQZJhrzKGM7on+SzPZhbI4yqAfYScIC0Wmje1jncsMMCs8CTIt4XAIwIvd
4JgP1KIWqX5vTt8zEjn0VonySopu9gaGVC84YfX4y4lsu7DjMPlaRvEKLDmFUDvLCQ+zUFFVSizF
8PQTiylcL5miJBeQ4oZj5siGl16raF1d5wGwcu/5tnueO2IE9pF177XMl74gvPBh9uyNDrnxpRh4
uATyk9ST2JHYF8ZDLFSkl8RNp9lkGV+KvCnB0TBGLZsUDLUxbvYFNgiaqYjKRdann6DZe3HVAjea
jOsS9BXv1NgkR/yR81rU1BOfsEJsKlDRn61T+dD7NrPPAvCBlh9SHtpzfDMZXK2GZWrf9nNZMEQo
RJmiyrRmR7w2+OeT2Whuf52iVI1bGMWu53mYre7j2ILfNh+VFgkTSTSRLb5/LvdwXWrEKFP/UB2n
k1P5Vu81L2z32XdDx8cMS+62NG5x45QkM1AqgAQRwF4ruCxWxnvSEF+yS0GECpclFY1aqioG6cx6
4ktQNwi7QEAdNXPszpU70P48QNB9nqWeJP22IqSCP3e4sGVfPc3nR5XZGphEuvVND4Y1Mcrb+LOF
aK+qXMPGGC4TBEBbqnE1RH/h/4B/Z/FC4s90qQNblXkVHWWktKTRMhMvAL3J0+Im7SbMuq+a7c2M
g9GbrY835+g3Ku7hLvjKL+fO2hX0alhtKRAq6D5/yq+ArU2h6XaM0Zs2GakJQAFXwBHWszY2BRkA
9mVsG4EnmJaPpB2yXN7Sb4R5IEHo/DgLAdGMsDirQPP3781muFEmZzmiLG66obQhdJGVNCLboHEl
lGdVZXccVK0htNebUVGh9dfTvmS8xFQVAUUf7si+3RCTDY8zj5+rQZR6YJrQOT0L6DyaECr+t1k1
VckrhjlF+Vj6M98EoGd8qp0+4TprnTVRn5Nbq6pYDFx+dB71J/O4v2/fNCPC9Waya9w/BEZHvL+y
ncbqcBlHyVd+nAT0gkAHmSdOK0aHpnPY8+3xkA0V0/evYOFQ13LOkn2/KVR7DScJStt0oKhtXOtF
qEBNmpuPupjgcovuffohVpX8lkB6UZ2WF4tc2AxnmUdnyLspSOvrlkNb/5JbPfiQWacozPBtNMsW
VcYJhNxDgO7l4clWglXqWc27zTNXM1q6npBqNIFkwNa1gUwKZPFoFAMtY7tTM+99fn9NrMzlQUb1
MYr88TTgK/K4QeQdq6CPEoWTMibsMcdu8bLFufnVMCoWjqqJDsV1xu+Ux9Nstqr8v9rASKr8WzaB
gph1lnqFOuQ0bCeX9m4pPvwPJkmu7cHqubs+KG7h/H78QBlSZqC8TlcRkMeD+KNsNiWBceAkY5Hc
A42jUgkZdU5CvHVFCn1oVR9X8pzSXpj4smxXQEpHJxsi2mqLrBKE1HGiwjwfeEN0lQjADSyV6q8z
kJxqk0mXbeUwWXklrg5yoFwBh6VLu2mpK/fPEpzBbR6GY8f/FOYlpcmnvIHQOBiAydlXVSFqflRO
ysWsctE1LDnJAOqUJuFsKWPq4mTjblFUqz5XlaC549ZtdCp8RxPZNQ44cySD8BHjkLN9KaJ6tVSY
szFTya620aL1Nf5ClNatO5qWgpi7mnZpNzv261IZ8HSzP+tnxw6hhnSGDIHuMI79fO04qMI5s4Gy
DXsE1uJfDhr1gArUeZq6nO0U39fWdiYBFqpdiOq6jlVUv6VJUJBCZRYClqcbO70CRNengoJRKctW
xB9daDfOanvi2RUPwuKqWYJZ6x4I9GxFZN4xq6rAWNzemYSjPsytbLyoVmlKKpn9DHAZrjel8GIZ
t0Sc71/kSes4I8QnMgnz5vSoAKpuIv9kYZAOk0uhVPhzvCUyIbsYlMuczZBt8UfK/3KcvN0FfKiW
0gf7QwAHInoC2c8I/cOHuCS5It43S8NImshYFVNU+VASZvcQFPoQ+7Vt2dolIdatdFmfpiyZDqSF
ThOa/5VRQ0tl+nUEl349jnQbUNTlyfnGkMHSQoKmahuNEwPMFMmun1Ex3MnStWUzPX13Q3UbJCkg
Llc1k1WVW1VgoY0jdVMNf9+DFaYMUpml945QD3pYPhJn9DjCLBVUmyO/+tfT+ajGeoQ6CtuAkqim
b0Pnp/JcTLntm+ynb6aeqAGw7Ma2pTQmtf8RF7OewqUXO7+L8G6hOwLMBJHCw3+gxJ91/G+C+8aR
hxJX5Ua+4f21epfC2F6NkFVvW8cbm6j1RCv2aNwPtDWrxsVqVEsajhGi54vp/5G4GdA+Hbfmzqqf
IzvjZkebII2IwDLG6sDO2UPS+bPXcX9Kv8wGieTb/2E1Lp1+QT5o+dQ87N/HcT4GYsZlk5TRshca
pHVxRekrJg27woLNfl8q7RgeBcu/iybZuAfamuM5Ii5ocLABiSXYPxIthnME+Tmrw7FndUE4eheN
AccZ2QcvQF6Fo+7B/NX496A84nbVWpSyTTSIS1jT6O0OqhGKPJDsnjQ3/98ctgVFQix3p3Cyu38m
nVeqYjbJvULQX2YOTbYtNtBabaPMRYdNTQQY3apwG3sBWlZjtb7IxTneArB5TdAsucZJC65TQDkE
qd7n71vSHOtIFU/crSE9oLBsmiJmeHalbxtUN2tnVISDZNVSPyOz1hEO0009SqeUt1rFbYJKHAXM
03DpEfEeLAs245tsL7wFoJgHORZ1WnZPCcPpXhohlurQGi/hQai2KYxphz0sBuKVT+FH2bzXC82d
5JOXyTKhg8S/LB4jy+YVTuyJYJgeQigyJc1DE3dj/f89or/WaoQxJv8/4iy0LkmmZE32nnKTgpJc
vklpw2Z4fbD+SwBdrpcm2g9fqOGxsSIYlgB9XKetj5m9D+A/lVe7851PhpvgwKsTYPo6gXiSM3+N
z5EmCpoW90SNuxvjsAfxqydtpXMzNOQMzHQbSg6E6FjqqUZ9bTcg2VOcJjhv+U9jvbOSdD4ZE5KI
/JtTUycYZ7bWBQS1kGrf4DflqxiE/QDYUNxQsbmYYnXivcOnCfTs6EDr5Ux8C6gwO/MaD8A+PIcc
NWjRhaR2GprQGcx6o7FkH/bHgSFQVr51pW+c+GrLkG3OLSerwsR3qft1sYCT18HHHYxRVBwZ/ior
EA18+Hh/dQSZj1wfrFmtFFZYyTlInxuFzcdu19U6X9ngqTtty963ibsKA06rd0VwLW6YwzNwSUtL
ZcK4O0p46mKMd11Utxec51lLBYLb5U8DfaMtiMKIHc3W8EyX3/h6Ucp4AT2cwdvoJmbrP5++bvMr
VxjubFUzBHoiltkzpUz/1jVL8PfpW/SHNlQh0vyPTfHGF7t1Sr71KsZso1KoV418ozJC4/QuTfMm
hGaxaDamfr8PNDcsDOr/fH8GwxNAYSw48L+2DElKFpItWfosFgBJpvnmk5ZxSZ4QQyD8R1ACjAfo
fcXACSpXQQLGsk49h76r2k09+ioOrTPtPe024J2AX/V4QebZWvh2PWAPS0uGweFzRn5uFSblpclo
ylndqbflUBbJIHh3c8ojrJ8tw6i4/eZ5F6PHLvq3cJPUUGtvpcqgEYrSOu9leYhgI3rLjNn+eBPt
MlPCNW3KdvLGw+4X8e3wx1md6ezKCvb7A4bBI8zqvfa3E7njjXIKp2L5o3pMaXkDJELCmw9nH5V4
FCIE1ZCSrPAf25EKnPCRmc4kjfgLmCWy9z+UdD5E/b60b+Job1MCj1/wTx9YvJ9r/9UZhYkh7/UW
rIP/9p/C3jyf2U/yGcSAqg/Ewuo8Ww0trxYJp3DnctUNpGDB4lRcVV6aCSHK74touw3+G94nSEA/
4ZpjUtiB7ln1W5jdL/lowz2T++THeUNiGQWzYhXhYEQ72SnnImBJrFbXjxWHItBX6RDns7ClVQhl
oRLhDt8FvJ0ISQvvtb/Lj4yx5cog1KeUN1P66pQAvjsQ9xgOVFcHzViFs3R0qB4IWe2FRHiW7qG1
pcRRswQSx9L68xUC0hGmFIZSBvpNybtLvAIhSERNtlt0YEctfw0vHnw/Tgg/ZFTpSI9n0NtLMG9F
VWX6P/V/1kB+A4ubz1WWl5SwN3wq659AiOlVoOiyNc1nwD0p8C+afatA9vdthbdJGyBX15BTF/57
FvAL/AiqP13DFWFB4nGhrR78Th55hgMcoTQNlx3EzcBIYvbxcuiCCZh20PB6TNHqDpMXgPQO5oBB
mRJNxqo0ILbEh3J/vErxwZmRbGJnl5ywY/bGmqauAb43wEiEJ7XSzrTgj0xzTniMl9zlAYmAh8R/
KBfjbpAk+7HMzAgAHo1tHFS5jIvjv3cXUOifjf6z01dD6CJYwxifVm9ob8sx8KYLd9fF1nP4NSqM
UuK3GQC5Wt9vXAFrX2T1/mLj7JcPdwV5vF6bMEBQz7mY7ZpKi0LcPuRZzDBwjc+C5dPrROfev/Iq
dRPCP9AspXxVJLzaVo69eIIqzix+n6AAk+s8dafaGNnXZrnThyuGdP8pK/6QY0+7/go9z4Jinlng
ACXcna7uqPQh2ar730ECok/iu7LjVlQb6C5X3TWaN3Vb3vUpyt9U0Ugnq92LgkbrxCWYnSqa5DeE
FHDAmOC8HdZ4kugQBf2eqx3ueEfqiNHRuq3PHYMN/8YCMbKTSE96xUdlYqE7U/YB+ZaFdiLxprfO
BvF6D9K0SWmR239BUjhzIWCMptExp4yqjdlz+4s1TXcC9CT+00+hJzTCynkbdTxW/Qll7EisvIoj
Cv7magnTAXIsU/rJVA+kVIxgfgO73w1dm16G1LTcu9Hf+Kl9t40e8dTXB5gBb37F6jJ2XPIp0ufO
nhkX486SZtLXI5qcOsDMheDt4aMuRHybNe3RTsNEzF5yGRPzhqPnV0ukOLCJ92Yz1TCR0aIYSLKq
mtsyvQvxCVFhouU1GHxmp8aLCAHWnuzfl0/4cDWu9uKEJcXtVyBRS2NDmQ2pp6eGbzC7BqJ3ui/V
Ux7ftuOaPcdre4GWyPtH6HXyHdJWgAavpFQLqStp70hDxBLGcJefccoobpndV/5XZwxw3ijJjR3A
uDqmKySoO5rEqqCPTaBVH1RlaxxkNZuTzfxc9e4O9FTWMXX8vz7l9T1FKaHi3T64H0DwKMwIArm9
8at8QYagt53UV2L64eolipvR4wMj6szMpO2i6AfDKvcAVgksMhrQ7LvZ3f08VMGS7C2ZluSmgAuU
GCnyaVGGmBHwjSvdjEGGEpYnCZ3BqeFC9HVEbDuCVFdkdyCvVdRGVGhQB5dWPRqplNJHr5WlkHDv
ZY7dgJfF9fa5rA/Ylrs738GNjjE0lkfoh3E0++k63F1zh6m2cMzXmvL+i4220YYnjUabxRRG9xoe
UAYACI/2zhLIwf0LbSy4c3RBfNZWL8SmHmlDpbQDnqsu2eRCvfXm9skXZAWN3bgNMfL9ewU1kRM9
rA0oYGdZ6Nb2VzqFfXou2eFbuv/36W1g8LYgJ8bSMXlJ8j3kKlUX5I1ggrVljKtWM/N99+1KWojn
1hv9XfZKHnmF606sFXrfuE6X1kg0Iu3AtkG2+cRohSY/lPggV+gd8FvJteu/XuycLZTR8lxqoECD
Nu30tRtKFwO6wcQd82iG62BUIA8k7jJwTlxEwOAdRyI5foEpQjzDq/vXm15bvidlTH3n2mhi1pku
zMP7bWf7totux0OAQF0zo13p0yj+MCTVspqlAsl+KWC3pdi0PKGzjTr/oCfKTTS8G15RdKC0mh3G
1roE6UviPBXvk1zzXiQCHin6eRbwtfMlCb9UFgovir2kv5q1P+Z2F4ZcVMW0gRy/i14I79wWVBYB
gtzTXp8TKVUpC2X+EwNtGHNhrmsCnqai8oPX3athZeWKyGvbfg2kqx34RkB6paFb5xbSEpOZ3rYy
Kqgic/sWFBdwsRc8j1U1JS7VmKbjfEAdBvkxtL8InIiq2AJQwqZo0SxHz/sBVOexEqFsvL9Ou+T5
MTZp05NjFIdMqIokxcJHsymVgj0TCA2ILSt7jKwrQYe2clJIFRsLD8FRVwxVYBUH+9Y7umLRJk0F
6CncCVCcskqMPZJkThk2nWpaCTcNDwfnEoRSHoIrKFDaDB2mw1rG5xajkgMw/6Ecdzu6d7pmxt7E
QrgU1Ih5Yg69eduqNHwFxI+tiBCsfjjjwmFqMKpGz/pIjwW8xVckBPogPkUB9TEDcd0SPLD2RhJG
u+DLqY+vHUFnoVjm+a22uaFIEJqWou2Pm6LX7vSg9/7lNG0bcjv7/z36rV+tTYWFr1hlYPTol1nP
Q8qzaP2hxZOeiaV8ak5DwMZdI9LhjFrA71nOdbc2hvS06ldhjB8MlHUO/ZDemJ0h5fB2df0zg/1m
qyYr3TE+Q9gqXtqeBVzZuk0cCRQnPpJBLrUmoylN5Jlw16H/MLwr0q4f/IiwEQeLxg4m1QSZX5aY
/0QFMLzcEPXMYAiKgW15TvDlWwRFhU3UxtQVHYhRltDz5V2nevWOXxuwwEmRqqjxMgf0CSy96sP7
jn6mwjsrU7PeMuKg0555K6/FNxlP/kK0gDiiEfDT3YAVBcTcxvTiWmgWt2IvO+YRH6wVQurfsQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_22_2 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_22_2 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_22_2 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_22_2 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_22_2 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_22_2;

architecture STRUCTURE of design_3_c_shift_ram_22_2 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_22_2_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
