-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:49 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_3_0/design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
mG6KvOwsu7yFXvq0KOqXez5z4pJ+kwju9VhxC5t0BXCJdN5MKGjopgyI4c9R/HtIsicNzvKR9XEz
SEFWrNltTeg4Tp9rBBOhqmp9qAoJy5vh+PezectXs7WiZxg5w+bbxC85bjff568mmwzsloLjeyv4
xJeYHTJltxOm9B2V44d9Fo2fjSdYdGEBkeI4DZHXwAz+p+Lzaap7Q0WjLWXYwEgniK+7Ux10WwOf
wWodeSXXJ7UbbUBTzPEbacN4N0H+YKSMLdGBE6kAxUG1O4SpljjLCVX+9q2IvVzmjQhkzwavLq9a
go8oLaOuC64etuppL1r66qvzxhy7/mEWSZnvAA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
U4BZBGmu5WOCFREbvxX409PYOHTueMbNpE1/01hDwdmivuqLijUMy1dPAeMbe9PR9s0v8db6NzP7
AgieK9l2CW5waoODSocQMrYMZ+cdhDoHXwI/6ratcx4S2tmM8xe61Z1LG+fRPTJFIEPLtX68D25Z
kpbbc0GQ3y5x+9NqxBmUXlqulWw7h8Kf2DIHhzOYFREgArrSg9/LPwVNkqBhNeX0KEoTqs/1u/JG
t1d7UFhyCpFZZ4Hmh1gI2njft8JHqn7v13WeXXZ3naLJeadzCZ9/VOJa9xsW4RWIXuI88UyifWNF
bAf9sP0c2WWn5QCjpJItYAV8e+PXLG54MK33SA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13280)
`protect data_block
Y7VOtzHVE6DWI1LXhUGJ/iGlpCNKZocUlE/x9BJvrbsYoVCq5cL2HNABQfVnJa7ZlCho+M1v+eln
go9za5W9wwJe96zxIFR/NLQ3jo+myK7TuesnCCjYmdqzvZhhpvT24/njAGAsYz5jxSiJhhd9hB00
ARHXIykwHQWg1luTJq5drzbTWzDB4KDIRnjhWR12Qv5kJdfcygWXgWv/mOpHwrBcTPf663bDTgCp
qWjdQ+TLqEUi9CNpTErL9dKLMYOpgV2/IENdxhDCnREnplUc/UlsjKlCCiC5PwNXt/B09iQ7ecax
q2D1c6hU6pDTbnDYIYdYxlRfvdb69uzI/NdH7mxvk9OWldqnd5B9cUskGwtI/iVL9Z2pLjEm06v3
nQvaWaJbb8dyR2BoTspDXpcgCjm7T0g7rSy+TaKeBNBWl7tUqVPIbRH1EblyN7eIPNvd43Fv3GPC
fXQUcAVG5fNliHrPbRd0XmCIP3d2hd5jgVRJobINM84almH8lUYpveXAZKGG4VxmGfE/18EnoM2U
CQCUJF+Fwqod6bwuNjbGMsTki2gX9Hm3xd5V7cvKPH8xD1IBVElTghNm4ICluYAMtSLFIoqBcDng
/+Bz4h5puo0oRws4PBfvnTTHlU4KRyFepX6Wc/8tEIeR3XaEZKWSjgiyGq5gGnXuPHJZMwBuGHqJ
5xMFdAFX7ulLmewevLhRvFJ8HoeKEWjr53+g33Man9I+fquOM15r0sIDRtEqOFsN4/kPq2dLfVlG
Xet8c0Vxr8HZxLgd4D8Tx2vawux4mWl+iAhciFQQoHR0Dxakb6L27Ll+//aw9Wan1Kuo20vUo1Z2
qRPlmJjNBHn/x+jpTzgx0RDcVV1Bi08gyO042HwmdtmqxaCpAntJ9i9mRGAb+OztL8arnsTZA19v
fEr0+AqY7eij2CPFFDK9POfZhRTsP1BG3cdL1Ut8A1E44Bcsve/27yae9ZIp5tc2gio/F9y10VTk
k6adwR4yz70xBGL4XPhjOdoJxj9FaeKIWWTJ3NrNqj7kJAMdb4H55lC8Ek9o0VGGPS1wdl7/PbK3
iJ9RO1/tcg3cTLC/ZWjbi7yCgTv3jaGlskBmuMHcf3mP9KciADrgU6ulVbeIHlZpJgBQWfspPbW6
O+1v6EY9GUTZGRq0vSGY5jpQnQJNxAb9wt/QXtcaSoT1WiOQi10Nsoe1+kEENL0RzWAYT4lGm52N
IwL78y/vQ3A2niXXAsWqZfaLp/folTD4oKBinevtvq6gFrlrGlAyum23pqCIybXa4pRIrwhbuXPS
WMqN6mA0NdlmQlkzVIVXEzlb9he3AWUex7rkyTBojQwjEyf4izMlZkrnsyoW3w08TeOZqKKU6Klp
OWRNMsqknanFXlSbNXOH5RK5TAUuv5/k3NKwqgkqCZmaHLeeiByicnkBmoE5vLw79UYot/e6Pq65
HUVYIDjm01o+Xen2LAsqfd/z2QiFLghcfSXEyxzmctaFwWWu+CrR5OfwV/0lXq9Kgn+ppFTKqfoL
6vmLEHus93qln4kWocjdNMibSMKmAPmlcUk/TYvMIO34N3jsjD6onEklwoRXHNv2n9MGh7wUUmVG
5mtn7jMp9WKNrSyALX9p5DhMpdml9TX4bpaF60VE52Lb5KaJXzmwZUgmhM35MQk4MJxWT/y9fEmM
gvvBhsytRB0ODf3PaazAtVXeFfaaIoRn3Hzz7HAn1zqAMwVFNHj5ZZUFpy2eTQh59zsk+FB0UDBK
J1eCJVYHwsJfpWK2qiqFdATmiSUXkBqVA0ErevFn3e++bxQG4Tg2xY5t+Ho/YSV+BZgCa03+8aMu
OVlHRQS8RLplNO5yh1mv7bD5b6fAOF6iLXEkwPiBS2zWvKiDXoXQppohChMUajxC4aZNFrHw06Oc
7oo+5VKD7arPAMug2y05YAFSw4vsoE+qIEkh33X9d3WFGDlQRSbWaVwu1ISboP6t/82nwi0AOL4b
PIIrGXX7MbFGnM8RqEzqoxmOKRoyHpx14Ig9I3ZzJBOwzT+6OgYp5Wl5BRrUVseweT9JK9k3Vt/D
9dWInepgM1leSp3a66BcI0FZVuOfDuaSq9divsMJdqmygq/WfVj32pc9aqA0ZahLlbySzDqhGFje
/zXjPrU9W+/lnixV7MZSDsSWQykM1BVZ4mnx8kmgt9HMn1Zybh8nOufbTLVGsqOdm4/K0kln6tSk
RljQySiODePTATu3YD85LsR+eotYU5YCVAfVPqpZIQXbEsrux1eRuqdZ2SHtE9vfKszfvOobJsKF
4etqhFqKQSKnGtf+ytcCXirD3SdU8QT3gJqqRnKQbIaw75Wrq+s0Ct7WzmCnEo5b+XVo7xy3uIFW
pTY+UUGb5Z6NdjdGS6CWGjMk9/tnksiV1fRHbGDPGxCcPR72tBI2beoraY8EN4eaFL4sYGC1rvje
KTqgY7qbW8hpqFLHtaEkWYSoR9VeLRcQNPNqbA2b8jIBKxlXZ4CLlhJXq743uV/9ylOScR3jU7jq
bPu4+Ve+Nr5/ije2FEOH7pCXG6JThLhMaibw5xx6EaIfp0oVtzBoMxTAyBnrhDMirslal5xae2Ax
dU/JZcd43vWjB909lcVWqvPkSCwT2q8Uivy9+oaiUIUw6f5imCe0GDVJZD12CLfkpWFA+kBYT8VR
Aq59Q3HaRzx6/wcjHilfgZQZ6mYpK/RScAo36h1iPxSzQxjY/ZdzgiWKCfWrgnxc+dcPpz9XiCjP
+c2feuz4UBjZ7DDOz3jn3DmYPd+9yFrXIbQJKZmTVv8k0gvBRW/V7YoNL3whB8qXAuAgElendwqO
g1Yx2Fv2n/dnN490dJmF81Rwxv1cn6rLUqeYmketzmec2kqb2vgEHf6hdSgsqv36wXqGa7dTfTFt
KfDC3tvPjexYXLkXIVyADjpj01QSQ8y3/6c/7zL/jbDdxiVtudUuXugCn7kOVBVyQL1848Dp7sTL
KneFkvq944OIRV9OltqyvqPDhkArBkUKilNEv8ACtdYN26b5ufvA82HuU6KOrri6UnE75HsaIr2t
UOa1n/88owjd165vwkWT+y/1rO4ZjcQPv7OpF20kiSczoWFPqSJsTcZ0SzSdipWhe5cEAJJ25qVe
7cH7pZS9LmaZaQFyhjoJGbaPA8JG+3WBU4Yz2Lom2C0SYBtj+HMTjtTIkgqNF2z3ZkSL8ps4Wd3f
lCT3MPSq4mbGGaDe1cCaJbNr67DXb0XEtgEdMsqzviSGxpF68Jy1Wt6eZM058xWSVW2tR6l38bYF
eKNTCkBTkmfwWgdmAsn0zVPeQBFjCZ9YRNWUEFnsaytC9hwXLwJTNElao/bh85+dXSC06IdshyQ5
awpSX44l5eV5Kjaqpx8eYkcOdwOJBkuvDssWl5yV8eU/nBTByDWGL7UQ7MgbbNXyqzRinE9a4xPg
o6GdiaQe0wNkSQEMUfk1sms7bLXxwED+OVvn79EDJeCeooINFGkboeP3jjVLKRiufjvnH3mac3kw
UJNhZNA+rQB0GaJ3nlv2RXFkUE1ue+HvT8UPphTEiYkr/+YpBiYBENp/c3DrmSk0hRQGGS8uUyBe
l76JWbjuIxoeN1JKYxZOi8Ei778ndH0VXJdfm3GM9NSFG6og7o83/EtdZQ2XMm7MqChBpz1auj5/
yuIaQDPzBxEuREQs9hsjTSA9iXes0Sx1La60RAHC4Qt0olV+/iR4ICJCSdv4UiC3L16w+RcL/xEw
cr0i0nV5MSrRnRMWHYQ/++Y5q3iXGS8Ci2EwWUnyr/X6tJhEAB49dWOrwo+j2keR1+IDMO0GOAAY
X/LRRuauWXlyCt0jRdpcepKoi8Nd6dufVi2K67Y4KXfvsWsGY2X8XFYpD8jVjTeOg1yVmd8zw+Jz
NW/0mcOQhkfcrnNYMCyRG7UPdBQXcW5fk1mXlX9e9T010cu9MYY2HKQ+gD52pnOu5Ajg3CHvxQxK
SZFL58Mi6n9Q0oxslc85H6Vgo2Pj33Eiwk0kZ/VMZ4DIR/+CvBNDe8LRtXbVbL8PHcvIJGAlu0RW
w4dQFRz1YtCUdK0HGseTA07zFg4ybCL2V1w/MxjPBCUXNoPsNGGbfsOXhM7GeAT+ZNn0r20WWIkS
mv+pQo3nz7mzPEtNjUW5IE2BW2cqU9edpbLg7/dmbESKMAy+P8JiigkvfsQK80ZNM5yT4vBimc/m
Lw3gKGBXO4ajUcXuEnR2FPdCWYwUJ/S9HYf4ecLnIMeskK12JmgexeBN3YbYb6JAM49yDFgXuQKF
Q9lm1dusZ57SiSRoWhb1g6ro+NqMcDH/mqHCHRaq+Vt5Rv9rJqyQCrhbHLoJGTmusPrdf28ehncX
/5hVSBZi5Q8oqwUm7WswyOH4TobLn2FuiljBq/SrPMDnLl1LIURiV3Vg0gVO/JU8isQfg3VYUdQX
UoCgRlYVxO143qWAQtBd756OyggoGdENjgSYyMZ+Z+1X13/0E8J5+KYfPljxLQ2ugG+e56KZu+MQ
xQML2mQxhWH7OSNbrpoav4SwBW7kRMF8kw5FoBPkK2YQMpWuBl5ehUsp6yX/wFiro9FhHGm5wSqD
tPn6qX95TZPsmGzYb4rj6PDhpYFOuxzEvzj25dgMQCbSapvOzVZz2hUaXvEkZaVnwqWhRXpeuKlh
d8A7jvItbwagrtBgGDrjiGUMpoI6dK28q2yse+Vphu/d+6GxUG6GgC4RVZ6WALeUOb9flr6bc644
dJaIMpbVXCgWsbmNDdc8JtkM+pxZxp2rQ2qHQM67s3Nr9Reyc+rnHW/km8iMPVZzG38rstu9qnnp
fIPnW4jO7dXiQoMbuxTI3LZvQg1vJvNdX7iwo4sUlCRD2zgZyt+qozHTqylrbMnUe1eAXxEPZJOC
9gcxOxe52pPyI2S71JG+7sT93JjmkVK4kr1+yMuziCkaOIBJyDreQPHOx8/LQKJxKFaZ9rlGzgIf
SYvQZBav9ibx2C+fy5j6X2Sk1ZzriM/iyzyslMi2DLD22BWNrOoGM6NPGq5Ei6e6uvVwJIdzOL6Z
b4CozgWLqfg/gExFdA8abzvgDvt9PeOnj9iHpFXS7ZRY2Rl6bChbp/jtgrXJI5EDd2DQ7gEABeJ+
0VEBHjvLHcpHfDjtVG+WQb9YutmphVP4dYvvZ/iF0tkPPIhGi2k/tOlZGXA6y/MW2ZdUIcubsTA5
ORE7HZ3cGE/0ycl2LZVsdKyCrnny9PUq0qkuRAlUuyZlxlyzZ6Y4ssSQKDDlVxF8onLreR3oVGRd
t1U/2YQgKN9zT7gl29JAt28aVD0JQa+Y+80sOVAsO6i4+ngEL48GDV7Pb2vSNRSaU/R8z7WS/VdW
6X3DXRrm8s+ub4vjyMzXE/J7/EqGNGCihD7ZT++fPQ4meOn4YE4kMgMhFTP7+8yfA6G1Mw/Dw2ar
quWvpkQeOhJ6MUMOfSuitWZkY25J8hvXDoMl9kKRAnlxL7FG6yZWxYfDdlE4KXU1ncNYVprseP2r
U7Z0pkc0Ndw9gGl38oSI26xBN4LwROJ2opUlf8smACXIHVdVEq0Nn3JG6Kns/fuo4fkIR72VI0Bu
vGyUaGqFkW9xq7Zqtl5LEYarua0OdgN8Nk8lieiTs6PoCbJddoZJX3sRqPUl0SNAO0D0L7DRAqKN
e71AwDt1H4Q6wYsDauw2EfWoXn2WqvrofaDVnvgobVeo5ylglAS1iyijFHcmneA1yuIzEnScxgEj
4afjsmIHPDOIq2zWsm9aBlodkfbAjHUoN6FNJN19/EtIBh4RGL7U+2nuOxYZ4go1JMBSfPuacNnx
RJOa+mFQkHZc6QXSv0gBlWrAoJtJqTKvuVhwpfAYNIuNB/HknQO2G1Ayg/ECmp5vIz4ZxozJjJ21
7C0JvUt/9TvPDl/XOehDE+IAGUJ94z5ngCD45c4K3gt3v/lSuTKXP/TSIZKbt+Xm6BMmv6eTURec
Os7AWIDP4tVK2OJ9oTiEn0kYw2oml/a2H+jKbEFT9XHogX3M6FrVXBpifF2vG9/SD/7Sf0DOEoUm
CGpU0wCzToPm+dMKBrkr1l4LA5YCH5GwMFg3fu0rDMk4Wp10Lv2ecdQd1GfGR7B7+LMmaXXZ9Hl2
a1p9lK86mQxUJGXnc1xFjxvPsqR0vis8KvsDtN+eOSbn+KAKVB8qgp7HBlTg8LXhaFBGjvQjvK5e
LADFFKgjTiaBeD+6s2ul/7jLrD/TYFxSbMUvZHjJKZMa1HU2uvXtGRSKQZwHS7TjdlxxwsIRXgxI
U1MNZiZ7q5DeWHCZg1jXYDL5jjo1kObotxblzmOi6LdPzg/JVIjjK/nSYAt+F+ni82wxI4sLoCti
y1sVlVE7TdugSN9EvKMibIo2WtuRROCj58eAklI/OHvuWS/YSFCqpaNCOxfm+US6b/ciOw4DQCHB
/cGh4LCFmUifFIwJvsp/ayzDtgbJHLhroUB1ffIIJSgrV/8tKvUocQJOeH7i9fZXEEl0NT7wO6o+
WB95bUlanHnpybLMHdGItpxQAebSNFwBPyBNGhqd0J3AItTRBCLz25WNvyTnmaIM8osOF27LXudl
81nEMtlWJueDOS+hiDbY40wPoWAw8f0PVZngRdlAqdUN5Y0Aza6G9RnH0cvrqykZRV272T4wxVWX
i5Es47Cy2c/a9PllOAEW83d6H2R1E3vYPDb9BWnPXEQRSWlszGkR2Y8qJSkTtF4hVCvP1IyNWW0I
7+ZUZkHsuWrG5cfqLMuDZtOIEFQn8Uvlokc8QhXgz9PESC77Kx69CsDUQ+5D5ZOzXwxISPaNX/NM
wljbA+MwZRM7uo6ZNQV4U4wtsY1rcbIYeChFbKOoevdBhEAGPEKjRK5AYz7OWFKMiSxpmsMk3v1x
fMS4RS/687zI+r+QHw1xe/qEZGPdEzW/xqn3wnbV5YIfutKW3RYYV2uYiC5kD6GAHaBtXaUGFRhx
dkZ+Qz6EzcVSsoD1kLMOL8eIPc72kYSGwXVKHVUSGjBYdcoHf8QmWYocW2VSRyqqJpgIFqOudxFd
zaIBVhZFmcQ/F3SSZfBNXiSX7hj+FkLhrKnvjYEPMt+LfDhKvn4hOJOoIRLDxogJa7kKvSfi3ayy
VZj/0ay3nhF1JtweZc2WmBZo6n8OILmuk3z6rQ+Gq/wc/StxMO8PtvJfeHlP59nZtACGx6s865hb
SIAf7h1LE/5mzlebHQrovUhsEqOe61P2aqjh6UocZHb6rQq+stLjwSDuqHxPwLnWtZ6SgWYhY8dH
/FZL6oClb9LvzPJdWAjkiMkj2YV9dcEqbvT9Cyp+daN2eoxQ0xZ48RsXqg9HFAPyB45tb9VKWk/A
MswigolaQnrxlLePPIxOG6pHiMnOfSz+oSh1dsm1U5nSyx5OL/7M+TBrq45xS51MREcUGp+Gber/
XRBVVsBj2mmyrPjbi4dGoPHKsovSnGcj8pXW2wgoOd/ON5SVqfWBG/4rLfYE54pfo/xGkDqy4jt4
Cwa/z7DHqgu4nUBgaLiC9HjwUzBeQ5o8O9GwqY1Ho6YkrLeSqXkYrqKi3uyCScUUMsa9iAAY7vqJ
II61xTABa/JlhNg+TCsLkuA+LnVtbKbfcaXAmISH9Lecqz53hizIbqIvlkZ2rj3s6z8TFydW37ic
jhlW8JoTaKMT+AUM4cNaYpw34tp5IggSFlyl330Ozk2slZYQS91rigAn6KHU+O1y4du5TKtU0UYG
zAoqHWFfVVs8OCDFsl2Q4SElrSi1i0DIpU6b+7s+9bifwU5hxpkzy0vxB0yjUn0FSbuw2J7hN+Xx
iUeG0H4NauCE/Wime6VI17wW94k53JjczwUNmLB2gVejWtk4O7vHNOeXgxl74bevrcQiG5twAo9A
tz4mFRBfqa+FHkIAkb0jrcK/lMexKyjo+3Z5TFVGrKiTfAw6GLryj71wmngEpzabbRzmIEuOR44m
zYJqJoWdSqVwG209RKH5ucExulTeDLMK1ymxngTYVLKYO6fMGZecoms2du+atSruA3uoDmrlxTLZ
M0M7aNwk/KhKv0gt9KSCgAKA7YIgRc9RiQWPYYDjIewdwW/cMXPzRWq2+IdDaJpzoZ6UQaQ+qfpE
+lwIk2KxqT0KXXk2sVsaW28V9QrHRbybcPyCvWHRKX7vy7NMgWso+MlbcSW+ozVhJrZ/ZXTM5SPc
dq8vbNsCv28DWSYfbTkOgz1zPLS3Ghly+bIHeT8f59cfPaBrK5r2BYFQyxtLUPcSkap3xbAGJNZZ
BeyLi9cW8/ZT+0PFRKNf8K/Ou7IX+dAEmDnay+MAFNSyZBcJCd9vESvacOKQ/pVOy4CRJaPse7R/
DCpGw+6VFGxdov4b7yxhLiQCr/cJx2yq5KrZVWbX52ELwSFAqNp/vY1OB6SCmigfZ3c679T+pz+Z
Yj2xfEoDs6fsqiJnA+zAJXjO7i8zU5ORxTsywnal/2aYUxoFDgywrRLNOCg2Ql8jClHYvMH8bgZ5
ZU2BEa0ahIgB1DeAhjEa8xgeAyyg7U8DpBZx8MrZQphz2P1L25gGRVx0OIzuqMgMBYq+awllBlKS
Pfx1QqydiNueUIxz2CY9QV1buW+sIkQX+rpQAazjI/deA7WuigvbW9erOe1r4PEagVwD98c+DUzk
hqyzmzx9c0TMu1UU1gtIsJslaJqW9J+QC1E45U7azvx/7IoCvHldP8Rap/e8pmkIOxwKC24d4n+Y
eR2dr7ylPmrC3niyM751iqLWZPNiHBm6L4olKi312xmzyn6DAYHMoiNuzWHhKToZJF6Pb0/xNs/u
IwUMwSaNW7fbGFMUlQDfTXKCLl8PryBiKzKMdV20E6a6QqADdsc6DizWZSdrrC5/6XRWazRi7qrq
6PhRXvB/39+yGuPylTuk15mAcez/LCfXREva4cwrdbHi5tncTWGuPAEXjfIHeOgtbnC9vA2woEzq
HrqkVV2fhAR2UMVWShnrH1xZTdD/nY+BqouF9RKF7qjKmhQQVkpF0oTIto6FpUh/8nKx7UJ4kMlj
nhixesTCr9zf85cV++PRVjK3FoGo6rzmqFShyC5VybvNBlzbB74U+9R0c2ZiL5ZumtozKWagFW2a
vJCA/qYJiiy7rMWNGcJohbARLudUL3mcthNH10fl02n4MUdTdWlJnTzl/z2R7ZOnFIcrMqPnn9r2
ExzLGPTeISljRQ45xPBFF6Kbu0s2y2iaYv30TFpcK23wsfQxXhWnkfePI6qhMXl4COoOCPNdUiiw
XhEzvd4DUIeI6PI+4YNPPf6r7WykRX16LkyJVPC2iC54faW0SoqKVHrnEVf5G0vSqaVLdvDInKlw
Br6YUVaM7PlYhL3RLXsaQQQF6bwu5+NKQXHN87eRX6mxkcKA9qxZ8m7ifI1bq/3RV9OoXru2oBNi
fduGHg6wj2ou1lEeypcI9/yBBhwOF9C7ChUgkCcKjJ17vWY0JYO5zGb5TDEpX6DYJukLn/DRANj6
UtuW0L23yC7Q7JR3jOnuH70UCHJrFNgIa4NhWkvjzne/UdHMeMReZVRq/C2cU2K5BeMmj/GPse4e
iOdJ++Xu+y4h+YJwjOrvqAw3q3LmkJnlDsPf/zhZ0pl46WImVYI4OoTRjZLlWYQrUjCl4ULhFJzW
EBNkObwhPV6/nZhGL1HqSjUSxZvX/JXmL+1dwghld/8skqad/6JQi2McfW9WeMW7QlmxxyF6Jx5l
n23sH447SUAFsBVn+aaWdtPbAvJ84gRWPNWI3mx/ensKKVRoAN1CxSs+s+OVyP/VO4jO9m2u9dgq
D54xHqukUcYieLhKgFSp3A68RUAUKGweBSEAhMmbcCRvp8yURcY11HuIERh7CTEi4gXJdSw2WBC/
1lyOBogT2XqHpMLKSAVBaVL5V7cNIna3xmbp+TaShBuRuoNmha+98up5qL35hLCDoT8m+GuidXmw
9c4Q24RJtCRTEElKWD+sNYXSJ7fxUYf5alqI4HcsZeUD9iXycimac1wzWghw0NKXjTl54FzW3dOp
RWHqs3xlAqjGSl7VswPQFKL+0MNBAA7xeo3/nfInOhN5HOTNNfJRVMZaCUgCzm6kcENjpVhT7yDG
r2OP9pldyrDl3e91q+9y/MXmXcPvHJFurdkngwu6O2KuW2MeK+C94LegVPDVwMpN2xh/pWOyTx8+
gZnWsz5NcqjRmeBT+2ik6anIKCJj3kSL9YFktGm6lriOm9+bU+7Q50NFiI7zIpEC6OU9QHSgiiM/
kRQuujE4WaHP5EnKhHOVmJcsjRl0zCvQvY2tek4ZpABe472tQr2be/l2K9/b3X5gG5XrkojtMwFY
ZL4ICZGqoUwo6cNr3I1/qQpOQgkZCK1FkCL5gxzEutHyLFYqjvL3EUHa4npZcJIP3xhZYrGZbH/j
vV20tc+airPmra3GC9HKW0na5EyKmjsotfkX/bXQGf9HsFOvYdfGM/TN/gRE68Crsdx2/rpoze2E
yJU1BNIH7eCeez+OhIWJVukU9E9+m9ITD5F7tdVALYfZZpKyE9kdjfaXIaPlcuBdW08wzFIsL2lL
Hr7no+82LCby8Ndz+ihm0TgD1o8IAPGk1bt/hNhsvm0RCaG7crcwVjKGjdNA2+DkBU4gI6to4FHe
+DZ5Cq0GNt/aostxUjzi1Ik+0aU49UbwEhZQ0zU9Ah0XUDc1zqdXW6qCkM37QTP0eDuiJ4Ybaoau
WbqqSolfwfJP10EdXXOXYeWznIeBjeSMZ36OR3uxmqKE1riVvXd9ao4mBgmoUWiRDXPj2/fM7rzX
70yG5OMDhkKpOcVOaNUVDU6azgCc4zjSKQj06sVhiNoodH9mg439kD6TKSE0hvPQuIUjXPruRkNL
VVepFCjyN0ekF4+U9bx7aOz395bvk+lb907OINu2CKn7lDhEi7Rm6tSgtDQUPIBvWwnuESEQguXV
tHa13U7AJt/eRGjq2MbhS3+Io7mncU8+eUK2/uW4cQ9m77xsOaZbfbntpIf8IklvKEZFXFgZxTQC
Jb+Tnhvckl6mUs/ocb2hFYYMsFe9baEYyyXiR2oeW8OAZKjWSF8mUZZdukSqCgxXu8ixWvalRnsP
4IjCkdjDHfoMsgsR9Ht7MnBf+6Af/pssqqTL8sR4UB9wk4HiSs7yHv8z7rCSj5mSkqeopvTTam1o
O3c8qW8e5eA6S9d+kfG5XiO/JUj7OS7vfKxrcoJQtQY/1tJgg0ON0C8K2bHQ2rEmvJ7Liepf4Zul
zY54cb7P5kGDeAbERxEESxpVUkN95iujgtLwH1innp0RX1g83dRvPwNJz0rpjkRca9z6vLkGZdvh
bmnp8lpMaZJu3HVvgwroBkN9J2AhQxyO1lAe/3zIPyZ08kJ5v3tp4D0rMRXD5JohFsROEvXzZRzo
zdxU3GDrlIdUjrq1RsdvBBak+9k3dXsUWhtQTwUEuxjHsTlna2Yj+uhuacL+djBUv+8vQhkl6UOT
7RMYqnbsH5lp998ZRnPTiZFtPSHkuCaRThTPKAR9Hrn4qzFMSctm5Z3/VZ4l5mSZIbdbkAlEKRdJ
TD0knqY52+TURnTxWRTyG3usUt1lrbomdDgcTkpNvlwBSCKIAonWZLT6HJWMBdEqWhVHY9VQJqc0
pwtBPTUGt9zVcy6P5JsmeYQ8BYS8d2XSj/SqVkd7RFwa3tawnJSGkhlLw9YghrorrUfuCj7/6Mzb
NL/5skzc5XXmjBpGQFXtn/i+6axiLCskNOnttp2/1qa2SDttv9Czn+aaz7Z2UW0N5mOB6sa94OlT
ypAZMMHUULuatWl/mqVvlvGLV56oEAXAhBc3udzfrnVi3WQKBqdqM9Msn38qAwvsK8ka5yv4/Jwb
JrkqbeLvU10E2f8LvWmK/XfloU0RW/OHsc0Y0hLQTQXYVCDzv4Ty1yGEKbLmVwjWtLmT6AOWxmmF
MuAeXgx6wLFoqKCodttEdgYLjwpDzZepJRgxYvoh+ars7QVrBORf4rSyOW/4YnkANkEzgfxmveXK
oRC+Tg5Y7LMAlVSs7/cKTux24+fg+wfNU8Ai+kSayg5PzqpAJvU8T811U3aZa4WRHFJ1WTbRFZ1C
hljgTIfJd7/+F35FsLg9wtgHXvqyLVlXCL/et458pNLU4K8C/WDp6vmfzF4Dg5yBycrVxZfH7h9c
5V73swAHafHVyeGhxY/LHvMvEjVnVvm3eeFO5DVmg7XwBrOrQzffLP21M037NPdLBrDZ/OW9MWtj
fbLOLdwCx1VIOUkuy2Av7PvxkColXHAIeyWXUwf3Q02gXSMepzlqsYQYRgwdtYvN27vzwP664cYW
YQ1FuwnawsUQePSefDUPAwYiWYN2jbd0pqFjqIGBSDcc33drtY0QbTI6cDFXqSMvnYNtKWwyK6Ya
2AzzngVBItlstrtCAOYd2P74X7egSAt/Z+J3ZcHN3Ydfs+WDU8PwFP89rckJLryfK5wx2MD+lLFo
5K23qWyomsEpvVB7009dfTtOIUhnUwiXigabDBRyzr1HOQc3bvRS0urJeereQrdiVdxSnzK9tv/+
JzLzwRuOYK5eUb3cv/kkry1u/PDNUF1MHdpjFdKsgCEBwYbWzBI9UXZFh3apL3gx+rtDFs2GR8Io
L3sP4FLGq8kO8GPHdYakR4lHALO8FZQ4GY+RuYEg9BXJ4UppMm7GLPncvw4EqI7k9OOg7xrLp8Js
DaJSrf1V/ahMQvu0+dH/QMWKWQQtP/J0IIrsr0D2xPQKHbYN/w42xwrzFH/Doo3E4aXntosrFXei
bvgQFQ2b3t4BCutJkZEvHUyi7taAt4xAwFPYwELhby/Yyy5n4wwrOEf4g4CmySgWX8GkZB8qaiVo
nguhgGRinFLUmAXLLJ49M8h8LFMAq7wbB8w9BVl6rTcAdLLDMpladTglI79w1llpWbuAOfIchzPb
AZilbRwpSw7LkS7gEyfcP+41pQWO+tyPl3775fIJJBqbSOYzHIybCD9WR86onZAWc1R28n2sjRvF
TzOgGPDWb94J622yKTHsPQbunx/JtxO7hrEN/7bQsFbonFlE7P6ORYinfPFeAyQFuRm01rQgchkr
MuLqlJhjEf1MbbUQlaCRsUJojouRwPt1HLTU7FO+7dbgsQrAOY61qE/qKhN7kHuh4+mIpBpFdsjk
HsI7MYY90Giddl0AlNKKgG5dpBBTFvBRnTy3qBS6wT2EdPX5uD6PQMSq5LdKClbgZtroVBvm65v5
tVI/o9GDE9ZSC+ujw3XrEsihzJFAowCGA/wbqd1KUo1SbyaRPtmMCBuB64KWRZ7VhDGylrJO9alf
VzGsKnWLNY2B8Ea+UFnc+s54Abc3lDeponts3tw3Di8R9yFtPqgZnrjMfNhhxhda3zuEOj9heEdB
D1awQlTps8ED3RcJ5LkJ3P43W0Bir0VXW26nXdIZN50IaXSAoAMr0QRtDQLs1/C/IcnEb9dnRwqw
3UQK1Z+D2X3lmgJmfTZrfk21PCYlwfx2XssVzKKRV/75esRaWLhaMjoQNdn9o2m41Ga9GwL6Hjjy
pymPmIF0dgnYFRcUVoYVqJ+FI/fOxgPm61xl514Q+F8HESPOQLCh3bXY2rPVhsxakP1EpZvm+vd7
9UDahDoSxibX4+RQ9tH1nNV4wZYCfRRwdEP1z/uNeydpkQZ7+qsQCZRcRoY0OmgD/KmfJnQrfQFu
T5iGGDtU4E1999wMV9Duhz21KtNKPuNR86S9PvXjo5V7rwVbmPHx4mZnX9deJo/zfmuf+VrfHyDf
7bqpENvdA7jUC4yhpTR99laND2Sa4ZY54xAYsfFcwoT1x6RTXzePDACdIYTnr9wr8FxXHx3y8ZZn
3/iotidgaC6OFUM5iyP4euGomKeaCgYtanBweq3aa4YBKf6yz7OyUIvmi8wlfQLxyOVMXUCaWla0
k9Ua/0uwGpq6Fa6ZmJ5RPJudvtYBQE7ALz9RfYMGEerfJbNKldqtTkXl9sevEoxDwX0cE61ERi0R
iHv1gPRZ8Ez+lnqFM5VVf02nYYZGCZ0zGpKVS6UGSBfff4lH2RefP79ufyvd8TdElTrautmAoySZ
pO7GOcMciVcKcgh25mjtQ1KsdVHYm/cNLwaxWItm4n/Vy7VVPw0BXUdXmHhz/w0ssEWROUC6vAai
dOeAhCpu2hakCYEklcMjF5R+rTUFN0JA/2GLj8ra2LNQfT1nrF8cS4KE+6h1E+JZSukdLkDNmvqh
313PlFeT7kv/lJ6QvtJtVqJuJheXFFWe+79Z7EI3SC8+4Mq+vWig5uNLXwgkL5BVOf4CzTCizX53
rDp+Okh4IOBPXn7JwHZKUsAWLEjgX9k2ZtC1uDtAN9HgbN/KtDwDMXVvvdpGNLshZGYBISb4gY8V
dvtAcRLWvijv17TeuS3nR1VjV6gKoP5eJe22ZGjO1t94mwnXhqv4VqeSxisD/zbWFCbC4U7Ckqii
KCWtSCUe1MOxVy16VymPSCB4PIMaKFRDQm+HUc+XUcbbXL7eRIuDt61GGJksqHET8RIOPEHYpBMC
fxyN1/p+j0HZhcisUo2F6MUkZuWPpHipiaXLxIhyFQ2Xq6pBmZ6JJNt3TMUvLIExfc48bQHvotFS
9M6rGo/GmWZK4cL6d5uYbjdO8T9XlZoTF7OAWtLd6c+9rIrFOL2g91Wwa/8jVK1gIuryqCkL24Aq
s3Ee5MEaRRMCCR86hzQh0EABI1U9LFKXS3o96ROAk0PRsicCFpoI7UuY+nAQubMbGgnoUrVzD3lW
xEK2Ph7CJzyBkdHcoVvsNZ2dqPC10+yKSSXHe//sd0HRZd4xsgXJcwSP61pE7oD2y73a/rm8SovF
WFZHgRIrC3b4WfDEt19kq2GOiOqHrHIh1mo7giWKJg5CEpJyk6zNk0o+xs8QqtH6Da7fKKDvDiKv
E677dNEvRnbtPkqJ+KsiNiih6w4ipoqhCl0+AyK1IUXGxkV2qGA2iG4G00ESiHpmEzXRmMtjg+QY
zT0MYJzy4XL1mVrFL1MCC3z4+Cqu39tGAZZ0oBxqcSD7Bw+pG1HXBXXKjHvhRK6vsQhB5UGa2o8n
RTAgggjPErYE7ZlmKDCEKP0h8wou1fPDWuDv6eeLtqxsjRMtoS3452hJioNUIx8P32cSTmi2hlD/
47xoS+uflQXdyraZxNKhjAqUS5+xYnW6Gl+y99yBM5HDneYse8VAyxcPBmFNqROT/lirNJZoDlqk
hVE8K+e0gvpFPiUo+UCXuSC2GRBuEtI8i4opKfN+L/eUj+ThJeOtcp3JOWHsyZmJoReUoWOMGsA+
cl+aburBVogJ1FZ5u2StCt+Qe7RktKVK1ZjoywMUnyhx23hItduc4QLuTZ5mWRiWiNqGl2sOJbMu
36XOKS6Tv1Bh6qtbGaf2h7ci5VtjyfuwMravXuOeh9VMqxlyuC+xixeMv6sVtj44LGY+KqjV5Xkr
y2gwHb6uqcEjSge4koQhRoIR0wjDSL7cgBa7wzACylrkhLi8LuYhMSp/YRRHNyzrtwLN/ZQXpXvB
5xF6Lji8d2rOGUL5ULDfqpo+2vRUqVvhb9TAkmha2U9+0MLSJaG9VJ5TUm4cdkA6YeekTEBKXSYp
CggRDMsn/sYhDd45g3jfPZxEKJcGVWVjHZ9TEr485DEGoHIyUUnhQs17YtY/LhADRDLjh4yEKDPS
rpyd8aFB/W0IOX9NFzVlrw3FeynabqlRRUYYn5tXcY0gxEV+fhOGl4VrUjWYw/sfz2PBRF4YL3a0
ta9uQvow6fTZFTfJWcdfJxbAiF++Ld1D+BRBpVZVzg/lGt7o9jjRbo8KlmtXgN89qgWRwwyXeqGr
LB1H0/hT/Db+4dOgMuA25ev8dVQ5PrqfUCiwuDWl4XFJvmxRPbNhh8jNoiIGS4HIzP+q6XqUL3ru
UtCERYOoDt5StoVccPbO2e5R/banaSrlzU2JzPa9dgq9qbIu6wqOWq+Y/jaVA9jFLUNtEhmfUdK/
JnptUz1KbSMvRXY7CM/zVw3vu4aAe2gCbyVHzbl9+bh5llH0Eqsw+LVWtXL7sS1vP7Ip1J1BVNKV
GZh4j+XWLL9a+1WvjPAbuem7QgsgAhV9dNk2vfUxo4Q9trqV04Ml74l4jprSiLygvZ34B4U7pWws
sigHxL/eXDVvaRe0FWK3VxSWwkJ40WTRAagW3ITQwKLLReTxFVYR2Cm10RRXj6Az7KVHE6RyP3C+
M5/k7zjTQ9unN4PHNWQHfTcfPbW4I6GsHNGObWvmDXBNnRxKUNqdb3QkujDGdi8r9z8PlIgWDyiS
sAnWi9hSx5ghMyO4TKIRd593BoU7C6bV/4sGXksikKnn/CWvXbSQDFt12JByG0FYZluOEFGAK83o
sgF4NmZfo4jkEaKYe7v6pv/ErTrwmBMpV4Wj9jUW2H/97uQbM2fF9iXfUUe2/xotvLJ+XGaxQTjG
B3zD/0qBCYmI6qRvTAGpR5B71+cCGNKelUbpyoinxm+/Mv03l0Rt6i94Gt18Xt+WJW16Y2LOF7hp
64hIirLo78jAkEHTmHruaRyAo20Njg399pogNTqzJV1jspba4l3Rv5X3mjaPgdISJgr+WulSXhQA
ezOcCF2k3fkPVdlJiWyzX67/crFJkQiT6x7DtbwYRBP6YEWB/wUnYs+7sKERCAe0QcWOt1RB4YPm
kO+xpzhIqRsrc76F0rqn67eSF4xF4jDO9Ffy07Y26AlEZNfXsEg1peenITt9RLH4IYJVTVHg7af7
SaDVOiJYFtf4ZwN+eGqO93UcXP3vb4VCMtFsmcTTwo6S8b1twqXizHSuaCFNbwBxXpXRGHQj5/Ym
8nQl7OkcW2bCcL+2Go5jJbAC6r1YUOjM7QkjsDsAyBpVE8jidPJAUAlOEEZ/RjcAd7djcmfxHcdN
qzXwsbkPjkpzoaTMAnYUSkhRxq+nC/lYeRkvsvoN8XjJUzoOuT3cyPBwhj2QafYDLaQJZLpf7JOQ
EkfyMzeFYA5szxikLyetL9ECvk8N1hwy8PHbOCmnQ5UPBto4iE3Gryg5a0jSyajtl+J42rWHknb1
1hnYzgeY3uhSQ+u8D4Nst2aqZGlvUNa+NOnCvyWmaX/4FckUPgMqzt389I7aHySDkT2sTFgh8f8D
MRgNnP9WfrdTEW/k3jodPmnromhXJKLSB+w8KEyuO1PVOMccWN3++BHiYG/BzGjjJSA1Sdp5XsTT
BgtGDcdF2s3j7yFdlPg6eHGaC+QaRAC7PCV195c52WpcFjCR5Pv3VmODmbRY3HBvcqODvRbKiTU5
weBiP4oh2P9b02IzyOd8JV75wlHHtFa8gSE5C2l86LEABsCXHZmN3HGJ5YCQXLM1iq2jMS3iMazK
BUOffdgD+ghZxufnQcfeBU/uGgWgEyrzXAo17KObF0ueBFRYxpdX42UM0VxnF7iNEwMBeOxhvYFd
f6DeXRVwfOrmSDfpYlSIPT7ni2c3ifWcNIanpDp2Q6uyv4VchKMIxm8DaUlTCeTMJnoAHKWucQpV
qWPvquPmOn6llRnVUA16gqZ0Bca43oNN9OqJozbWQ9xdD8pk0Lot/2yWR6cYh2Tys0I6mmofsrCj
Jw+rCOtXvhFPXYwZ0L6auvnGV9/xyssq2ityJz2MmWc1TBi04qI0PV6iWjXAZOPICMsVxVqjBhLN
Zv2EvheUjEBdEbPRd63NG2XH+2NtzXXnwTkmv1QrJ0ApV9kAMBtnsemj38rTk3Ayt3nW7R6yznVe
Af0Z3MPCM3xfGYzulCgXhHooi6WjUjzaiiC+OrM/fElnwNJuKbUoEgbZ1/sJDCyYk5jY1NdI5xE=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_3_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_3_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_3_0 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_3_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_3_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_3_0;

architecture STRUCTURE of design_3_c_shift_ram_3_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
