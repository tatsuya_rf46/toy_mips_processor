// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_3_1 -prefix
//               design_3_c_shift_ram_3_1_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_3_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
FPlrbreDOgfq4Z0yb9XkA1iQstkrck1BdS9lHTtuqDaBKi3IcyF9I/qB0XqgOsvmw/fEz+jweErc
b1cPS3ruwBP6wuOxpEv+T0lGe7JQMfD8WEegEDeTKPlsgl9GQX03abqQ1EDegy9LQTYYhQec8LTC
yv01Aw8rIK9WajYBX7izNwmoSna3b2Pqz+vVn/I65wQyRKKK9cZFDJrHsoXzTrcrwTcnhTItNWLW
PBvlSflO/F0Ncdk+srGAW/gLmf4tZTYrUPLvxfbg/rNk3H4mikWyGIo31ZohjSGaYYcpMCViay1e
PNGdRivkuaWjJrqr4ZpRs/6wSFwMXZwLwZlMF3Bz0k1+NAXELGOByYsGTKVppegHu6OZIaZnOSkG
DYDh/f7Me7cmZ9aYONK3UAY9N8/14B5JedJFbeDWu68Feh5y3YYZlX71E60TeiyPQJ9NVihWF0cf
SwA3QxG76AUXlgTAgBq9/LxgxB9bi7ogFeDj4Vo8Uz9V+YWvQIRsRNddqrkdgDfpm8hdkx64lv0T
hccTID/V7kdvDRMvQ0LmfcteF9LwF0XsxOgmNc4Y04obpfhL8fH7UqSkaei/dyDX6ZBPhJCHybyJ
LTLti1dTv2epZTHRBQtbMftRt/qYn4hxBbGkTpU3XfkBr0POoPRshBvflciXZaIccWY6ufpEW6eQ
EOqC0H9SYP+lRdOIUNDF8DR5y6lAoV8vCTnQA4cwtdn306e/jkU+K9yXNqzxW1CW/L1PMyFxkOgh
O52bs/DgzbCjUrIrw080JP7vc3GlU3QNJf/YC2YIl35oCbc0RPVz611vlzz+0rHJzmDmbVbCJVXU
UcYv+Wlr7+neLtC352m4hto5RbIPY//TfxX8jujXj7YoXa4ZQ2AzXhzGeMQ4hicQVwGOFJpPfBmK
zmyMrEs7D0+/7btq5/nRemP4bgGsaUZ9yFPJSdrf914LqDXE6qrhnWloO6ccv9Dj4dfqZwL8SyLP
FBPzOLXiOgf246k8foISCpcPpDC8wrQIkFrKXw99F+zH6F1hwN/0a7VOOde05OXVNclHWQTD79HM
d2WiANlixF8uFO7mwk0jRla3d0PEuKL96MHnqwdOc+sZQIGG7yToS2ONOWHcM39qeEqrftQW2xiw
HtcbKwRopUeH3uqOo26A/sStKeGWDjEoNIRySlm1O5u3uxt33i46+Loh2g8vvpZlwWe0Hu2qWQI3
G6Em2xWryybzsPn11DCwTNI3qfw4QYTXzcNkRzA3Oe4pFXydp+rYNMCLTBZ6tG1PD2D7sK9lcRNc
plMtqswgHsNcrTfSgu1i8U4a/ozj5kmmwbutjGoZb8RLZKhf1H2WAZA1AkH7uflaGdR8qhq4Zeyq
zw8smXRoTk6LbT8NADx7TWeL9uFxsVQmSW6qXfQhcwbG13ObXxMguhIUKLeKAuaBBSxpRM3sf5L/
RtcJGeyDLHqdneR1tCpROIAVMo6Crl0kqCCkLiU3Z/2DBqQ/CEC2Y3XD7PO9DSji0dZmMeD7fR71
W7O8tYQEFoFtMrwm2IPjgh3HQHd29HCsC9n5BWpTBFQf3rLGPYN1m8qXJEg2noYxpOVqLnRV74yv
wS62/sfHuadBQytwxNBsHE+wRRe2Nkas7cWnIUPGeovxIm3rX6uYva1HGIZclXk7R84qzOPQ2QRg
oZJFHIU4qRrvS4rsYaGEg7ukkextZN/tWycOqPBRZsVxTYAP6EcVMJUnVE6keSfFx6sQt9qONkMz
42K5BmfZei5QAa9nNK/o1VVIjZHr/ibzyXlqnZzajAccYDPKlKt2BpiBEohWPz0+sTSYxqJRrKHK
d6td3VcGLBAFp1MSWheBT6UpNwEObGsdxDuec5mHkGRr4F4k/yX0f9h2Mct62ojs5UABURKu2nk0
9SbnRq232T3Nz6nNes9RGvQbXDkdPXijuSCj465KpfjC/n3iNJ5hG8U15q4ta85ep9kwIgXu4K4a
2721zaQ70rFD9McIQVOEzkNDC92lBx2kHxTlaS52wkTQ8t1wZmHohmrzSSkQdMA/OhmS8tOB7nmC
23Nfx33NewBk8Ngw7HH/4h1C6QxtctchiLh/JtevwCYnN0cRlH4yY0bEUpxeBUZw2N/QZqQXeCBH
Y+rgWNe6BBcUf4/cTd7ovjtgl3FuIxw0AkQbRKiA6+VJLMxtXU3hjzVcRZ0NhtyBDzFRDDNaWing
SJoTJSGnJCabcHo/L+ZVD7Ta5GNfvpAHDtAYjf0BCfTpsKKWuAWThF2hNbHLfy1CGD5nmZ7ghIqN
EI1GMkFw50AI38rqcE1RRSnRMXj0iKcdKDM2GRHR+fLBOgjhO76hIQ1Id9lZ4h+hksJPypHM/IF3
P+3v1+5YzhbHDCvCJ4ULbvN2P3ZM2COyjxivKOFQWnuugoWv7IZP5AsHGM42iyTAjC7w27jd5ff+
Umicyax6AgDYXWCqDfJ1vuj76/Wo/Ob8EruLoHKUOaD5q+bktbRPP8jBKQxz3/PooSbykfNPBSG5
6PuE6XC8MjJOpvbPrmWUkI9U0+zX5iSyvZZOuW0jPiX1BJX90WK2LCiH/UE+I1qNLt0Hc+r9GaJq
yse/pOFOH3qigSFBRxQDD17vSLeg+55EMxemAn+8TbZLYyvowB6zjIRQM0TlkzpzSz0Vh5zI32cZ
sFvYfSQ7G4xZxrILBMCxDUvdrHAbB7dLrTUWDwOJ9QtZC4lSFPGMW3ACJN2IF+3Eo/jLZ0ai4Ch6
tFKHF2z+loMWCF/tlTkDDyASKw4CTF+XKai30lYXyKevQ56OXTyjqjMPzASUhXjf8k7m+tKSq4Pr
60j3WeeEPn/jIYvNnZo96bP96gC1PECLfAF0dsJOz1T61rnkec00tyZwqzHymIAdOOCyFBiG0Z1l
7wfj/Z6xkYbMi/sl2lNYhBD92uyco9xvkttmIKYnVQVT0dUhlFeFqP0FkWrAEH6f4kL49YjDey5+
Spl1w4x6PJMQSeK080ehUUaoHv3X8w8aiXDs2vMgK7cyNlk17cREq0IptyR3Cy1AitBjC/2OI9bo
gvLQO6Mqhu0XwNaDQhrX0LQQFhcLocTV6VH/AJwTDZbv9/+B6doyr+SSm/g4KFlvfnU2ceOBP5lj
KXphL8L04/hN8Vymz07U4euB5qC40SYUUUCeb3OgQ7FOaPvctTPX1ZEsNbyFuc1Pt0xxbHwK6w0H
RNzguekD6v4Ru+/2ckpCqnaecVGsJQDfOQIMzKIZgeF2ZXaZLsKijXE+/+yVIaxj80cJSLKCY6Mc
I8EVPTSzNjYdBiFIhcNv7tbaVKHjcO9IOg68knUY3/bPyFLYl7FSx8kly4TGaKy91XJlXnh8NPGn
ejP4IV5GdoT2QkFTDRq2zh57vWtr2rp4SRR+Ap5TOwcQ+Kt/KKQGD47YDTYWrQ9rO//qvWwl9kRz
D+hg66PAQ3nQkKCRi6POus1s5DKDecIHYVzFsaOwdVxJcdog8IF5IpnwkYh9LHuvrxwvsQp1iI82
MxR4ZhDLWFrtMlq833He2q6MFNYErWes0pUAgJCvEUIlM3ch6u+SjEHjPj4p6qukyf5EHTG3g/fp
Pp/4/SYBOGedHuF+6ntb5pfImNUy+ifdGLQvwzePRoUguVF3LtyktneGOq0Q9FHpkNtT9gZO40Se
u8ur0a5BV4udmspODZEneLi6403j17ZNIXWAWROBqYLRcom1mW+8ebO+U4MmKGpB3Uj/9Jfukehd
mBAO2/cfHzKIRpPqUYqojHA7rVZexAQkEKrC2hS3ofMASJYj1kJwKsKgwXxnDsgJjZw64EWldCFH
p1zDtD42rzItunezYIkYOkkZTy7ERBarfRQ3C+8ixs5uaJOc+SzwhcVcwjBel/OlAWGPaHNqLSKB
a4htch2I4bVrNIz0fL+x9lzMThGaRB1//KoLG7DzkaVQ4H4XGXGtmTc01K58NWCCpoTa2x08zUDo
t6BD5LCccuMESxdDlGu+A/MNzL8YSi8TAFxjGzVxzX45yWbFz0WDA6tMum12MIbb3VquLjJeRLBd
jKWL4W/ngMu9Y87a3kLpsI8IV8A4lq58nRim0/ZUfxaM7Hds7hhuPl65mmO9Mu8c4iaAEuTpXPN7
LWnpe+FxpEr07e/qpuYv+mi9fahKZY4wOUjOTfwIWPdOjLxzopi2DHAQbUJy0Z7jDLxkeUO12upW
bzQv+9yhYK/C3PaPNwf4ryXs6Z3AD3usPY8p0XwNeyPyDLpK72SmhyGTvxvug0E1epMjKf2cNquU
9UfphNv0n7vpySPLY6xQiIR/5bSjpYGZiXGp2b3xHCwDUhFBnfqJzMkOGJ61qhjLhJ/eEPWuCerQ
jRqvOBL2/x6yIbeDby94D/fgtfEGw0VzpY4lrWI/EdfNhqct2xUT3VqhGqZXmxy+uPzv9Z9g1HjB
i3ZNV15QaAanErktysVq2t4gmWbt8Ob8Bcgcjds7gjOyQ3CM9jkcA5Dht6E2SoGXIV74KnalIk2t
kT9gjzLYdg8rOFXKfyQoxkA7vdSkCI+jn4cmyqnLPwkVsIHIaeSRXaQWUr3S1AYuBssqddnccGmU
PBuNuMzAqMaipTuKtJnnaoxHDlZ/pPIcRCklZC/jV4ZIPqWOUp8CSfpU9lnxExgirNaN3o3jbNxs
eeeF0x/sbFHm8jWaVPOpSq1ujvaSeIyJiWBfBKYQvyI5SS2Vpym9VfqKKrRh8D6xjRfjh5kutiRd
QlOsC4BNfIme49RivdQtRo4+rwP+xQg3p0s10SAH/964v8OswwlHNTEXL93b3vS9C7gK3dVn2utp
R+/+AdbOT1NbPwQgQo1P9mUc+udJRmhvibprXh6+Z8hAuHVlqG27ohp7kpCjtjIyW0X8A4k5pYgM
l4zS9TGsWvPDLULlmwsoBlNKTP/RMpyYAXZhPNK53YcVjPl1HPH5Hn7nosO538fp2pzDqLSw5LlO
Lo/AT2KpHKDcfQDnDBR1/uWo28XzbwGQuXQkE8qAUGqs7gBzfFxhInVRRqE5QXap28u9Lj91gciY
/eIOkHs97CI7wxhed9JKmL94NhMQxM2zofg4td8vdyAfzxbcnuztMqJ8Dzi16A8j8wIZlf5hdyBg
kocnkD1+RRnThRy3YqLNepCpo+LsWKgVTmSYoHqVG8ZihAyltPg738MJ/9w8CZMFuNQ9cYVEDvaw
RDi23jtoOW/FAljN9SHjsLWo8tY6QKpXoWjaHR4MmY+LwxDKRObR/zIA6q8Y700j1fX4r7kAsDsa
IXqy8/bx9l4U/Xvt7WB3Jq0KufimL2lKeCyDsiClWbezs++6IIz1GM81HufLIVJAmS6Z627yJe6y
+yCH14i8fXttE2w7Dkfoyp0mdD5bE7ujZTGWA4zfetbWKqmr9KPmsT0puqSv+Xe0SVnwgWOogUWa
n3lxuAnmsqGhRkDCGZ0VVCKMA/QcqWICQDnlij8+2K73anL2U9aaf5WuADY+Xwy4vGDdIlQMEdud
NW2ngo8emEp2GGdCd6YP1AXYPljhELMd3DSIXJEJXhGzX1PaHZ5n7i6cjr+ApOd1AHNPFQI+RkQy
n+C9SY5Jgci+GttR0Y88gKLRgmOjEfoc3gdG1JSLhuYrB5c6LOy4dqOgaD4tkBNtLua2w8IViBiW
VRAd4UKJFDuXDM6Ar10cuD8+qZ0qVaz8jAB7YS+tuHlImduiwlpndy+SfeQDsrK+ge/E0Hk0072O
9gmJE3dgLmqQ2IWpFIXUvP3wTXU82vHl8HXUw6eA8iLfLJtmyD/3eH2rAyGnW3xsPa93kmDwd4jU
ZgXcC+ovOE7V+FK735qF+hWruzsD0eFGGsJcrSw7yGJ9gQL1G2+kWTI5QX9eTQGVPccviOzZ5pwP
Q1wAED/xtmYg/+7+Kg91hcAEVRihkkzrRs0jvQgSQD2uxG6veEICB6CVe8QHvT2wjQnCSUZvtxa+
ZDqEVgHVv7QzsdhkBzbIQllnZT9kLxs3lAobPIzUjjBVpQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
