// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_3_2 -prefix
//               design_3_c_shift_ram_3_2_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_3_2
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
kHnZsMsIMNI1dVH06IUdFnubmWK33ONn3pIvXPKhZagvWp2sLlB38RakXLsXXGDAU4aQ+uW1E/4e
tA5VrB+3lPeLz9hopCWqWydu7vZrNbkMlx3IRN2LF96sAtu+Kfzc9lSoC+Nybljdgk5ljt1SiXGe
3mCTAn+6PEBoqWnRdD0E8cgLNVORsI6BZajNtVdbx8C7SoptJ1JB6lHt9m4DMJkRQrshy4a7O4Ey
z+7v7iDBNphrTbjrW/zQHTzht+RqHhkDJYf6lmwT6F8H2EnYo1DaCLhwClYVYnweWBdR3SsUU9XJ
NCImJrBNGoM57omahKxsCe/lS1kXZm62wv8Zyj+f+fM7IXt2DfP8TB5fwzvRe+0/sMEFh7rHKr3j
mJlyPAwv43fmqq8O2skktyo/kLXZ2T6F8Axs7IfEG2TSNxlpjickHfkQFaItlVD3Mc8DVo9FdUjk
oNn1pqKTrGwFbmKfd07mts7qtoegrfkJigP6v1MSgEyiI2byDTALV/NhgbwTtT0Yw26/VoHr6Qh7
hR9SxVr6/y3yMBDSZvAQhJKPaBaqPemRrTkgOyHJ5rPnwyJFP7UQU5tBUS1u2DQNwc517D75Iup+
MBkrfn38f+7/fin6Z90XFUqka1sptt4deO5oXOHMxMe4c3BC113HFD8zv06V4SfhE1mvDtgcATZn
yl7iI/23ZB6RYuFMxZ2TtEJ6ZU4/y2XqhFg3+GYehsd6UTz7eQH5TpQogojgK88xERwUXtumhJsR
k/oRNo7E0fJaOoDt7nwYqUpnvQH7PqirLuTAGwFH2sLyQpuZinOJ7VZpyiQBKDJW4GoH3VkyRNFn
/RXggzDQxWkYF8uraa7CV69Xve0rgHk9BPS1bewnLNEoc2EOQF+Ht9sE6KmR6s+w9Ij3ULgnJWSr
3vMPekWU4+iStqkTaKgZbNRSzgydsYTXH0bw8kuyzEeBZDzu+X0ceWwi1zmWW/seZu185kB3aIRq
RiZbCe692uobd6sccFMTXgoykgPIj7XxJJHqmxX77kQ574QdXA6KFIkn78GXTJWzYqlo7UQRbVe7
duBQFs+HAJweQCFhroXJ3BgpB9hF0WDde+SsTzEJ0izNQi3Hfa8nuZqNK6ZSYZ5fsnY4l2rO/3t3
jJMDyPVrZVyZmCvalYu1h2MPXYbZ05p4gG5PoPyXYpidLG0o7MWVi894Le854HK2ijU7uk72AC/n
8K9SQeOfH1vuWhkyu5sEPyMhzOqx9WS2p1j6YSw35o9gpqMrVwLgmU+x1s8pnNB8gEp9mrCjqu5I
r63306Fg7jjry4uAXiUoxSN+0YFRePOFvUilo1Cy+VJhJ1KtOIzoJhrXMdrQRYrPGZ3mavpIg/1Q
7e2CbX6WO5lcc6ZRGQIEihgYVn4vFyh6OskdpeYnERg9fvYexRucT7g9Yufwgql7OxAIFFy5hOno
0s3L3BHoYlCo0cEG9rfvkOP7paY/ULzFLnCKsFVHrosBEh0kbbHdaQKW3DVGMbUKpknpDa3LuFbO
t1mHog55NqxJvpWFl3OQdm8QgV3VFiZUOq1KB9PHuhoaaZ1GFyhpOeI6a6DinfW+vf1hmpmnenqS
Atjd56/+fK6D0im43rcyds9kJ7Mfw+GjZxhN3LqQiCT0OZ2elwmBF3tF8Qlh85sYgV5UQkR2+DtC
JTdWqYKsMiMqy+iYK3seM+mu1Gp2rBJrmX+36BGgsmFBtltp4zmfU9n9Yq96ePAlnQUa3PqkNw2q
d6UXCyTSRGNI0oYOSOe8snEjymwLZuTc2vwrJs7b+dhBJgCyfBW8eRu/x8/9fS/YN5/mElnpaM/r
rIVpJmrOoaL6ymaLU6SWCUewMYEXzqX1HcqSHmcdSxeZ21bBkP4QoyVVGxP8I6uVYc6aMW00/kLY
Gl60vkFJZkhraWQZyMLhDZojhhOzdm3vYNBSgWmv/ufFtN6tZ5JGEQSdsck0ZyUcjL35uUaxRLkW
pjOD/7Xdgn7F6C/nND8s3ygHN4J6ry49a4YCYkp53p9S/N7M0MHJ8IYfGxcx4q7P+9HXbaxlUuSW
/FPsnoJSka5ZLdrHv0MuYszUqt4IcnGscH4eGu2NQ8l3+XeYI7OdU+f1CGt0bGDnTMd5Ffgy9bPo
2IqIt4CU+KSH/Q9H543OAEM0x+4W+YJGQXcCCHBxsYrLF+NAoqZr02c/lWZvBryqIW03TqKoj95D
oDTG5qfMjtQa8kgFygl4A4oXiotWqqOazB42izSgMin6yV+UeqV01t/fPNb4xSfuWp8P8Ye1NLZU
E+FDFPhUVJzFBfa71T+NfHiNyuby3wutdIEJKEbT34A1Nm6soVuYvW6AjdTXQX3j9F9ReSQjBwRo
84YdCPTQZ+WVEcDEDLouu+G6CgUWE4euqwMvH0JAXBxzqrBFrTMH8bXZgHsBrmfsGTGMfnJyc7RR
gZJdMQwD2LDT7tJaav68TslR51Q23/gELuuvDkVTeWJ7XbxFGypc8XhkzHscEVVHfnjzM6PSQiee
lZmta4TZiDslGFkttSqomeeKb2eUt6pOIdY70EICT0gPoS46c1hzaLyX9LVF7qbonE7+wbOsXWc/
O+sA4s4Z2d+H04vxqHMyNHd49klWKRfQt3kQi9se2VuT5nx9hIsOjzko+09Zq0L4vtd+ilXF5WWb
IRFWk4L5O0pNNxvoOEnVz7uJuHgvz57Jdpl1HIduaqnqrks+xoa99oA5wVhhxhfZ0+V3jWib5/K+
5cJ+0xdIIoSswlhYZf2XjKNDuJWisJWiTtuoQSFgDQKtNKLkIixI1BylzDtRruM926ivBJSwoq59
m2495nlUFh3bLnO9OVcPmVzhiuDXKRcWgSuFRoTme5e/ShBL6dhSC9gPCTh6hqk5qF+PknrXA3gv
THwCZ4+l+eR1tRuB3nABMadox7RnQtgZd9JNpDUZNy9hRGa5NtuTaQ9xjStAj33XpyQYFchCM7M8
flymZHAStGnAEeCQXZCKzZjCIUP9geFp9FdgkNttSwjYUBDaDEnpLTxitUFinP92nXS8WsTLmMf+
HnN2P8eK/khiD4L3jyEdpt7jm1+aXs8yyoDQ42mhbkrSMhY6ItXoRSMaJHXkV3D172/gzWJUcnDP
DUjuhZ3Qi1GntoRBzkfBeYoYulMmtNWse+7tCd0H0ZiVpN5UyKBi1IglAKh9iNmd208VJntS/GRn
Yye6ZqiWYwUtKyXWbfIYX/gnOXBOu565j6MKn6iSrfctUNQhVmAyatgCPw6uqB2KTlZwcsnR7VYn
0MQLYZniAdZP/QDG8sRieCG1Y2qWz9P/oiOE9kAN+PiaIv3dd5as/b5o5CuiPmkZJMZyoJb2nxKT
IuohNE2SYZpgF1t3d5vxwcSqUMvXDjxdU21aHBbu2ZuRgdd8xO/X4f+Qe1SUK/XeLNiNyot13Tc8
gJmxKu0dBCgZ/vsKHJf5KVmAtB/h/ezhUVmQtuzmsRA3cF8S2fgv6yqU1iNoADIAOSgUZZFwW18I
C/CIL4QO1W6skhJQDOygnnvl44lqRasUW3KEqyH3QqpeKD+Ohr4qdU6FjP0+ta22kTMIvZD0jRGV
7DWF27MegbOZh63Y1ilROJVP95NffBKeMOdANLNiZTvwkFv7tCoC4K+9xHRM2f7QTbA4O+mGycsK
JqLXNcnkZ4gQCxsqKrJPjyfavVEtYT0PjIcSZWjF6/oNIEgChlpM7nxc27lAHDeaxXFZUWMLZzsN
MJlcb86D/JxV8gcNvSKnSGYz+t4xrZNlSNJw9ntLxz8FWPMx4Q2F69qMOFpTHel6qiT/3V2SINYq
5M7zjI/QTnxl/PBYFZJ1ZDLX3KOqjDcjT20QQgtZeWcEDPwVVR/PVkcJjWiKk6yiDV55XkDVDIEu
7Ij/sorRAWggBkQUcXfsSyqsFpzgosoDRyChyQ1wAliebLU+3px6Ab9Npn42XlnC2IYCH72I4q0c
rtnwfSUjgl3FHkOjmFPyKOfsnRarO0o+PLvXMKWrikvuRuhgZ1UrsL4/6bdhVZX0Py9zP6RzAJOC
OCO0L0vX8kW6OgEerMBuNHejIS1Uvc0AbVaRMech755MpBgbEesiMzDIZ9NnPOLAkdud8LGZLWtH
VMVAH4gqnnPn/Q+O/Szj4pM7ROabLY/xLIoJFaMcqBGdhk/xTW1VipbjVdnYXrkhEtW02bMe3DUh
6BgGv0v6GL+rXLI8xdstka9U+jW2xPFfr0xRDtrwCScQk0EAjmh1RUX5DQzGydCSXSgwhIBL8kSr
oRnA1K/wo3readjqjmQG0VXZuDeglA+WBRa7tyRwmud6d9KAxSi5kf6sobgMKzAeGOCkupJ5cjJn
GvyE7NU8BqH+X0YjgAsttitzzE2lh9SaWtVsfCzbY4mNUchoCB5GEJuQJcChq382GBl+h7n/+eLa
gyzef/otzwTim7i0g/w1DtR1cz6EJI2I9Z8ttGfayBeaQGtYYeS1qn/naUo6jfEMdVgVv6KHn7nr
rc4E1Qir7IVMnPdRSsR6SfLjsfnXLKzL9iGMxKOOIJzPcONX5aUEcSbGqf6lhVymKOrLvBaSUOcT
NK29BfAxwVftGgS4acijYERnUFiCu2P+b2BR54xFGmDZspB90amNVaEwXPLzOdSXIn2bhQsuyrm2
OzlaTC+kWzxkGS+UwnXmisAW73QfQu/zd1CXJiKF2j2vN1lT8nYeiWKMf18QVFlNpOSiXDuEYuc5
HWKx2WU2lfoxR6Yij6kJGMnscYFUisROX0FrO5TZdsiN7MfpO5zdx3IPhrzlytmun2w/kXNlrE0S
//TV2gYVmpw4JIrPJTHH+cNMHXpYX3vbxTQFmznRci7uf602nsH8C4LUobOa+8ga8bwvmVJ8B6xq
V75A99d/ePtNAo9bJpzXwwOeRRO1/hvRyP5fdtqM5cMyqJWSeAzCxs0s5S+JDNVJE1LFl+g7yZOo
UwK2Sq9Ijo2qaMGw6wIeCQCHUqXzZ6HZPoAf0vRG+RoKKDMVkz7+5WfPNxWUcdCJHz95MKdhDC6F
O+Uz9+TCkxfzmJjl+MkQ7DqjGb3gJoNWvX4mMpBjSzmeD8Ml/5QwHe6sqk0JNBUIhaPd6zctQ0co
qw0PD/7T6BonnKsjLSUylwHFpfumFLKWKL9F5iuT3N8w8V7JEKM6pWsAAHnO5j0/AwZF9zy6sClx
sIRBnMBipdOSNK0J3HeuStRPRv0Rt54QGop0AXNheOIgNRW8NI5bQn9Bv5X9z83Z8yPHB0dO9aiw
/xLC1irKD3iJpK64sbylNaxZNKFjzu2l2RUtlxkcuS913QSCPsL2P/8WaTjfqTpcj5Dhheaagrhv
VELalztvafKn2/VeMtgiG+296vGoroe4n4xk658FhgsxB7hFrLn0FxU8bkN4maNd1ZZrcNu0mddd
J3CqUTz61tXDW6raNIGShzvlk840Gaqjg7AZgQWbGXEb7h94+56nas/XJ7b0iq6BqKDhTmCt4N+P
yPxUhNNF11L+j5rbuMdVzdQEwvJraJ7S2v+pJLvR6/CPqjuENt7R8usrritOH5O0pHL0Yqfh+a8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
