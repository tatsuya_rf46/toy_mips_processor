// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_4_0 -prefix
//               design_3_c_shift_ram_4_0_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_4_0
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
IJm8mnNd1zwt4FNOUW/lr5TWR4pGk+TaoX4IC/02OCQfITGpBd3YhG4Zi+BegUqropGWrMEdptTR
6g2HF1E7MljdXV+juAjc5MQfkCfiy1+jr+70Om2bCpmkfVwpQ3eJgu39L8v6dGOHnD7JemdX9NeV
X/XTscizVqJIBLOS3JF2X/HOxuOSq0s83zYKsFJmsv2I78dTpgQKV8COm3CeterOk+AI3J3pvPCd
87Y5xFQcPlo2eL5pokR19h59J8oDbvNZMeJ68AX3yXyGHuAT7CADrHbknu046Xu2OwLNm/tecHOI
RHaV9EICjgTCGFrw2cadjJ1ja5WdomG3XbuJLJhmGHULvaWeZ44FpSbFmzWyfgQZROCLG0yeTCwG
mvvzLJM3Hs0Idw/Y21t+GF+XIWqFfNGx/swOVe0TETUAdDKA6IdiQl2abkeJnfYPK0YuDW5nY0ZM
qV3v1EcYklkYVNJ0d+auROIjzPzGSGLD4XM6Sprg+D+ZDv1lUyN9rTc8fpHESRYhwUEeyV26OZr0
S8Df37e8qmLFMdS2NW5vhQP3qvYGSmSWuJAbjLh/AsabrMqVGqIjCJtO21yCdLNv3DKiJiKJXdQ+
TpBscAQWAoSGlnZ6O3CmjmXni2CIMTBV6SeYseW686BZb72sR1sBJw3wbxa2uXjKFpVgLB6jZ4Up
FJM/YPGlhEch4YFFD6XpRoGpaazjNzww5hcb4/F2JRrR/kwz3/1zkkQl1RiM6Jg/rvqv+xut7mDe
3nhhaJ+FkAD2csGs6SWkjBH5yb4263NnOZ5K6E+eHPYlonb358pd3lNfSWoXPDl0OY3YJajInqyP
oXRJeUY3ILdeSFE63EshOQxLqVOmTGnHQ9zV30zlYW49b3Q0tfJndiP9zSwolYph6dn8W5yOUPEt
6w8CtJFSxtlX/ZXSMDwcfUagcKIQi/4371tLHTsmOwbUR/b2Mw7NPK2vxlDURmpJNCET/eEg5Edk
dUkK5PNlq/TnPyj1WxNqzdy08d41cQKvtMKA/9Wr4ZPf+MVhhQJ4g8aEvPT1DBeg0pdD70jwhhVn
rBAxrkwdUgRmgj3OYSj8gqpmu7fb1GaCVG+QwnR1B1MgrdexaECNZ3zT30dXeB1cQkn+fr2lYhgU
AgCmFQYm4ebSVge8Chv9TfoDljfbfByOogv5upBViLAELo3x79kJKYiHlJJ+tjuP6wOt3XVhGFCw
KT0yUz3kYcR0Rqg/HKoWbqMNKj/HW04pEatmZLkbzQUwT8u8sfYdc7CzedZapDnapOZ6lepTysrS
/9B3lJ8daPqJfw8Dk+pfYV//1qwv1H80K40v7zrUfhQ23QLjR8SHBa+OMnpeWuumJ7k4+zD9yLwM
Vbls7gFxG1U+x6XDpPbL90iVZB4cMI/t3aBjP/omygFwEVBW2UyhU4Fx9ZJO7ugnZaeqlJ+K473p
Dgy+Dxdm7RhedRCg+bTglRmxGWiZ1WVF3Ir0hcWojUw9M+y0dYo8dc9qNgkKsJS3p6M2KKQ743iV
4NFGwaaU7SMC92bGc0yJKi1uI89NOdA7Jt7dYFgyODNi8ooaVnYYWq4YaSjhV+3IohTHXvnWlfAV
9LOxl0WZ4Pq/e65nOvGol1XkhlGewGZquqJwJO9woKyVafrBPKorH8957AhyhyKfGOGsN1PASAHn
kkytHz1rB51lFx/mrRUk4gCHjbLCxDd9qfDPQOGxr6KV0jFONDC3kRxTTMVJhh2R+lHOIXVUpf/V
rf6MejHWY+vta/lpkE18AKk55YakYulgixwhn1948pOisLwH0XAW1USbigXAljXigty7ZQ386Gf3
qyBgTbJPmV5qcqVumD6NnyrBDa+YriUKlg9hhstbwm7ofl/jw5QyRdgLMcrErjZjFUYz8NGFnE2f
k9YiDBV/jQs5IYodEmWuic8LBxWqMuPNepBiQhSsooqxkkP3EtipRebn/SSqxUU0I+ZeXOR2y5Bw
hYcqvGQ6iLf8UsBb+UaPIQ7PDov0eYgCP4YdYyyrd3xDPWE6khZaAMQO9p1r9jsZ0NntxnDIqKQj
tcLW2yIU+/wFZqvBgV61iliROcPbBVCtVNr5lw47E6ealeW2ec8Of0PYEyzx+e7F5RDvNAJgMjdQ
ZFisi8xEE48VszSfpkgwK6pPeobjc55ku9Hdptm47mpXOIheIm+NdtMNsTcoYnNN3KFPI0Bsxa8T
wdOMt7KsCBrOZI92+oUJtYi7SRpPfjV6aJFhQYy6Ms4jIplifCEX6Rhflh3ABU7kV7mKywr2EiNd
oo9Jey+wC5cZMq/tSJuA2uhr6Z5dawtcjmgeswLAKbv3XUnSgMFQFLUOpDLgeLZelYpy3zsvaN/8
c1T+MUyvLG8wIrKuS777eg3hK0FCXPCJ/EYcfDvBsStIjuTOK/LFNUUx/WgKkW1XEuB6Cqs36ido
5QVsfd3CNg7yeQWoqFJ+1uMMKMMTYbiYLGPjjNUVB6Kr4QGmT1t7MXvz/5qukJsbljwfzqxypg/U
mtnsANq3pFmp/GuL5CCOECZJxRZ5ExWA71oFTeuAGSHPZQyQPVQ/Oq6F5EdLpT/7eJ6ZSvp09fX7
SIwUcdDhMuf4tbPfFfXIkHF+p4cN9qSVSm4raUdRsnBcKaQm2psJ7Gyv2Xqt03kBzxblRUQjrH7F
rmj+uHV6Gc0DnJ+0ktA4zax+s/xvGqf/1CSotNnH/zOppoMSLOxvjJ7EfWCbqOS+DTQvHStsyehn
/KInYrZS/b05Rav5OduQAlhT0uf5Zhb6/NXAMrhjEQJCGP23ucqzjJo6ueNVTaWWIzyDCZk8H+Bk
lfxQKoSPrqaEkR1FasNM8/GEKopxRxnY0OQmk3rX0GQ3Oc9X6fcf5K+jRRPcTPW+qppckHZo/THX
qCoHLNEtL/rCk9T4QsN/mkJNL3Bc04SaJFOm2c4PtxacU0j9TqIP0VP/EndkuyyS2oGK/AVgDkto
f7r6Kv2tOYnmCCzhlN9dvdipwa4zQd79swfSzPmui+kp+hpRzR5GghIYRDyzqOPOfGoQrl1z7BP0
Pr9moCnu45d0YrluhQ4aC8tJHRiRtB+5veJruVyQNUZBJt1tTyJmIxydwX6nym6b6MrFqxLwG5ZI
j3il+Sai/oGPVuAmupUNwabn3MVGm22wSJxUz2zGtE5tZY6S08bJ+/wi1R+L/APm8JFi2nhtaL5g
IaNDp59tc9iJkcRdB0nBJa51T9I1MBrKUJAq/5Lw63PPKVHcI8eZZOIERqPXpbtfRaNeYPBDcm+a
9IBy6uEAkJKRPPir178kTNxsN17S/f0lT1PuJay/T8VMyP4+279AtV9ItgPM8C0HYjTWZD4j7TIf
3KNOuX+fapbVXP7M8Z5ccEnyaKroIzI4ASnJ30kioX1dlaO25ZkoH8HrbxY4yjL/HeO/Wb5PcIMt
ITBW9aNrRAEJp1iFvk2xYOBjvS+amsylnAgiXGX2d0GyBJzaruF5XhWCN7CnhF/hJhzJ9XT+u0mS
NI1eJvh8KGG3rRcu0lc0ZAu3n1/x0nGpEL3aeSloPMzAf3a0l3zD5zGAp2U02TAdiY7y+zP7A5Fy
G+19/MczgOsCv9yAdBcCcVwb3bbWTiRYykWCu3oRJiJokj9/ECBjp+XrksBEvKE1WjZ9o1jQOCyp
NOYhBi7lUbc4OwDa3W41ZNQReQ3sd4q9dJJIloVNqjKhxOgDBJY690NTLDn1tGjUfgCAwHuq5iN4
/xKH3SNvyiqNV/mslRnt2BHYQCHI6dR9ieHmihdihuxMomwHZ29pUJLpV+DMxC8mGU3d6xAx0PAV
UCXFbobfDgN+wIZZj9PNSwjEF2TTkPqxKSdLA6i+lqPv5P8QMPT28nc2uDN75qQc3eOblOS+U5Ej
LTzWySHzkeT9Fdp7YW5qLuXEVi2CP0aQMd8Z5oj0LY/S+TVLtJ7VCC8rLaIeqxqfuQEL3bz8r7hr
/FHcsWDGz6BO/nMJG/30TGhpe1o/sF+kO3th0umTtUG/ZqrD3SyQ51ubgOVPmnlsJXfUmxAwrzJS
nNfl9yc6GEGRjTK5mx4jkzY8tvW5bx0aXzS4wFwhQ1GotIQp9zrGr9PUez0Ve1gtb7YQjvLwl/SB
PDoFlHAQIyaS9eueEB0PxjsLp+F9oGFbmCxhfe7+m7xzm8aWA4QNEY4B/Ouzsoc3D8B6iE2QkFF+
bbZWRtss3Lt4pcrZBg1IvX3lbleCsgJbKeeddg9DtgSQBWgKkZFMR0vtHabFYT0tac+YsIf99X8o
YhVp9a6lvh4DXztB5v30vdCxN9r/uIPhvR4/+NV2ITkJfdcmsO/UZwXTAPdwhEORGMS0kPnbld6h
CwHDWfPyuJmmMtko13XmVm+P/UCFQ51cJoHZ30Dr704dpcwzH5CnSMF0vywP4w2heFlNbyEFWhXJ
rAXt+RSfB8tz7Em1SfYfBMi0ZwS97tlZjjkkDX2sZxYsCQYd3nNtZCJ71oepXixLb8Vi3y8BssLd
W4ZiYsf2Qr4M0AllZBcNSSOPSK57OIIgOjzglTjm1gi98Aa892sVtPlqQA78SWfHimOvniT4tROM
yWCpZGXk3+k0QcSySAR1gT4rGLWx7u0FFj/t1sX/MfNgdhpJdAw6sH1LeJ1T3Bycqqhja/q9tnrX
A5p2B7wv11Uk5vPs+/3JKi76B+gecEjnb/XUKCHK7c+Xa/lqQIpze4yQvG2dG8wOcIwXQYM/XpXm
qedS+uVg01zADWsMOh8xGzycL4KyN9vMhFmUhS8M4/mCkd309yEZtZc7LSVk9p3ZvpO9npR21Tm3
QK6p0qRMzz/XmCicA4zPpe12FaDj4+Q6YRZ093PPJyAngk2X/vKCjd1aBI0xabiVkePUYkX3wYVw
5yOCIVoyVEBqfcVGlGoESWuGUiK+pUMJQsioowhgm4whsAaCeaDIuwe/E/E5o97yE8e9Y0UiTYUQ
yrSFsUsT4pNX1f51hQ1IWsvbuYkEp2qfxFIr8IERMEYHglkMeSntmyNp7NJkAc11gkPe0u4hFOvK
wwhna10OkUo+FKLp4W0lpg9u6D/SVTqCiFP+0UbCtbddX64T79lODSzObCVzfZCaUqg4BSoAi+dz
Swwy0AwHLHZ5y2xQCDYIOq5TpYcH0IgUPNggg+EAH0Fvb4R1RPKVfS9a+hadFj/N9jEYOUP+EBOi
u2pSZJhz07usH0pvEGbM2Bmi1nURL6uiXZ8ArKke/LgZWWt5bBDXtNAUG3ixylvSC2LUlieyGTbn
GWcW4AVle6OX5TQFQsEkWlUTYXxH/v3ou278P1/HCRvxp5MeytHu2NxQ1k8gC+4lqCqZ9Ok4vSY8
kFie1plvrDmuovSuLgoxVx+IBx6939SMycVWdyqyK4nbHBHjf4tKg//EPeHc7ng5+yOMeMFKyOac
D6sAaRJQSJJLE+LtuvTp8Hlojn7T1l0WJDCWG7iEweNJcYUUawLcwCmCCRV6X1vvOBx4a6I0LgV8
fVrkgkmm2O1Axcb8C2WOGoa18oDZexEBjZqRk06N4D3v/X6ax4OAjyoxGfdnYz7aKUXaxdTfUxVJ
Uk6CItW0B7fVLVacu+HAGWehrVtM2JOzj9tsdYAUSxjBrVl8+jFAFDs6GRzWymDBscSHCEooNjnV
zSQG3yj1T+qQhEtajfhlP9fhHRpo8PCp5AjOwXdMog8VRRfu1B1NXxqvtzJLJCaAFuotjqQO93rh
I3vb5WeUdYLTR4+puEfIW3pgEZqqu+kSKNvCL9gzYnhG7prVBetElVjk45Kf7pK2EdR0EvCASAUd
QL2XnZW+YD6qRLLMyI4z8EqcKdQJ4clBLCi/ut+8zsAt4JGATfcm6LpV312WZ2MYzvx+OUvI/bZI
bkn/7kQWSs06zIS1RLwRnW+E+HBFcI4R72gZSi97aj/vMn8FzZaWsuTPC+737pAEDDN5huNPf2/b
0/lZW0NQIre0uZzf3Y+z5QynZBGrz5N230XS9vaKDwOxJg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
