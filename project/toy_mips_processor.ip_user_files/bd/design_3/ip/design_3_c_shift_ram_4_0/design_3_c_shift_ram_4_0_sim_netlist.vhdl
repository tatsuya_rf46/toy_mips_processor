-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_4_0 -prefix
--               design_3_c_shift_ram_4_0_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
2vM9y9b+jPoucXIH/kbiDaco0KZxTfpoFvuyPu5y9hfsE+hW1bTxksBR0jxoxvqZIFhaAtQIHIob
NFltqDATJmIPJEvPvPqStSxfXLAuvBhQNgm+Kl6UpG7EvNAiqAIXjxM6vToCyiMqnuB49zuPYJoo
uAHKGEZCUxkjTLKa/N/6lkFe72DYdRlAVs9Nm/JjF+I4vOFdJQwW4CM479P5Fr/DSK5CjjzselTQ
uxdN18UvgdMnufkx4B4d4/f0D/UKSU4mVnZmAvbAEvKupoXloCeOFL8WDUMX/+9h2lm3hZKcikyj
ysf+GUH5RN/wcd6oK/IAj96gEUp/jT7kjNj+sgE9aCx5dgoPzZpzRQF3glj6miOfC666gOAYt4CF
p8STjkvIyeKFGzk0g15EUJ09bXgnrart5dsacZ9cBrM5lD7jah+77Mi/lUA5F4148F4yAhY2kEhp
ncgymu8j9z10N8XD5iCQpLaaFinSQOZVuCp7YzAZ2v0EgPGOFEUyPkKTTnlWObpGrtS2STLeCPpZ
8+DhlG6brPaISViqgyBEEabGRnzR5lyjf2H1B/rkIuz7z6eG+efS/MiZy9KIH4v+sSFSWTmTm6LI
eP/6D5puBZagNJFAOEStw5muwrRm7y07y6feJV5eFypE0+Zyx9em12eI4HOK7X2XEJsXISd/OY/c
JpKYwsisDwGNCFV/UDE7NJIrAto2ogjf5dR62ygJIJiBnzgK60MDmzOEHD4roZVsjGkQgzc2Xg1B
kKkd6q1jXwZ6eaLkdnCPwsPotZRqYeKq98mBCuyahq466TGFkvw7IeQABJRF9pK2v4Qvbq32njND
9ntNeEswCn7+/kJlcBaBL3RLh/Cj0AGWk0QuRJYHCT4CWhLIsntS9h+ZVSaK6+SoiFPja+c+ckWr
9cmcjDLPzaj46xfa5rgWvYbwIkiusGMbPVNqPzK8iq8vI4B+GhbSaoJGDLeANjjYp+/bQ4nwFhyu
RxC+ZBW2Obu1f4vmTZFnUKkuhr8BsjNYlB/msyAN8yvVLxCUiROz0bGxbEypgy6ICCflI0y7EzCX
Q0054br0Zpak73xosT+JwAw/Xe1yHrWEQP0I+KABAFg5Ut3drfso6kolekiDwWRFMVrmA2yB7hAn
nrUkuynqiHGIL3D5WqOTtd8phEeEAfiF0YTiULUcKclhRAV3TVaBP9YDunBC9mGtmq40ZyFbC92g
fG9R553d8OhaMYITFWooB3Xj/ULMA2sWuB1ZkBid9arigLfyN1fDkNWBjq3vZKNIOEeA9rM2XU6m
1UGJ1hsT2TwTwM1Hlg7oDG8srwjqBxGkOHOuQa3EdrA1CKBBNdaaSG9kiCUgHxwxUwyUc6TlGIsT
UdiL44T214TqVGN+p0ompfSi6zzbdYUBZxbIRNmLQ4VfYw6/uvq+IRawsazTRV2bocDaaUvJZu4w
JogJ4p5P/hrYXUXvv/dkm0MwFZyjY7DdbTg9xqUQyOGJb7/GP5RL6oRkhG77QjGGVk0wVtfIzaEj
LqYZlw6ppZvByZIRYHhEPftXOEh7lnlKVh2xv30tNkF5/cuXho81VNvvjvfoF9+7pRjmRxVyEyKj
TUtsj3c7tQGswk8EP6KIB3prLWO0oVgoCuXEF/uPfOJP8OPmxa3tSuLiZ/2oKCijNmUVaAp3DCMt
01J2vY+g2FseyhMFbpq84SIO/wbVV3JuoAGCPUhC3L0hZTf+B3TIbK8NgBer2WecnggnYSNXVVn0
NW2/Kr4SARXxRY/zfSgv4PGtTFJaba6tIQLIG0NTMhwwT/w+rQcxwUw/7p3vJ9lZVlhInsRv7krG
vw/Plwd0an2ySVGYghvrA9TWr/t9eEZ9UzK9hgiqfbeIHQSOXySbVeCgaGC8CqqM3a9a22GRpCEz
oWYWg5eL0w1a0ud3owXppOT49FQ1QfGnn1gXgPTmjpYozhrExpLkvfCJVUVoTJi4cusvhiQtpwMi
c5ajLVu2llY1yl2rCildiGsIIkk90rJAonEXWIq3OD6Es3gdA1h0lbCcKDoH9BGskFcZfbww7Pdt
2+wtT/3YzBlhRDDjv8WIH/GGsZQpmIP0hkTxjsYnHVEXLaA0k5kN+7+fDA8GyPR8iWdrwrC6z7gC
xriEWmuSnBNnITUWFxUo7GDN/8i3SfHnmD0rHoy/kkXFiPi47DF4C5ghsamM35FSv1nEkm5oraGF
85viBZD7KAjn4a6HsX719IC6A5RCx31wvKgG8VGK8hlacKZRVXJANv5MZ8vJdnTUdmbykGdorfQF
k+TQJEu2NvJIdVEPh6WWC9h2jsJGkMSLqls8UDijZpzN7zonjyD4JZ6jIDmWJzC369SgtWvWFm/c
RsWQrn5L4fqDRlgWnh4SZcqeAch+keFxpPmDW9PJ6a9hg8qWEtlW3ozlOtepy57ve46t+27jQxDi
/fM+vyU78UozpWS15Dwb1+rASn9jvuzTQGszzSs1Y9YizLLZMgdtjFxf8jPP/2ynYV9LtZrSdgvA
DF0vYGcBeDCkrWGBLNWBNkDN2i46nH3ts+X0Y4Vvw+0aEroOjERmfezXdwJg8MyFkscOz84G6Tng
qSZwFIbgETxBQlczW1EhjLOpwkesP8XtClOLYh1leC7ZPAcx2Q5JnuYmpvyD5GqzI3v6te0/NoVz
SlynHXF3PwyOtm+YdLQKuH9p7U8rYCMQgKdPwoc8vV7qPD7Y7BG/sIqAUhfT7d4xeq8afwsUmB5S
+Bh+SLf//YtQ9m4KG2ilZDLWylG8M8Mn5liogFvcHWFsSprCtrBoPe0hBE6i+MNaDKnUQO1P0cGw
63OKTmvH9nT8VZOzV1HZX8qqz9BdpOEIY3Tho7Aem8r5HA2YKFeJnvDFMbPPBHzgvd+zY5tK8/Dg
bl0c6s/WLODNv58a8WrxQjvSB/OPbnF50v0hADSzqGI44LtEU3teKeR0UyBxZwbL4iBKYqg63/0C
9uKUYX/4BV5Tdlam50XLsa1ncRlOXBeZzVWLn2x6v8yBcqUBefzYQJwDUyeRwIMwQzTff+e37rB3
VOskiLy3q+bA5PtnnLPAReM0PtmBkWOKVgMrSDpHUmzRU8OJcr29+6gnRAEifyJEcqFJT4elp2Xh
p9z9n/J1MJ3T2zz3qNdaOxiC/o+UtQMLf8VmvoDpjUP426jkwQSy3xDnTkEton+pvfVQ5Xrb+3uk
UoXG89C6BtQ5yDJ0rc1E36xh6azsKxA7eDow5m3yYE0WVId5wX00rQUaMVbKCpSDHLXcle95qbvi
0VdOP3yvFXoJx+8jX9xT+7/YWB2SEXPJqu1OgpNXA+E1apm+y2g1U73LEpTm+moIydqtvGky+Xhf
8raaUW9r+8EvnF0kZEIHlG6prOdCjMqTJ6WoN5oICQ3WNV8S/oNX2J2x60yrvTFKBtQE5e7v5AF+
5Uzf4Xw2ygBQvgprS2jF2OqbdQVinaLye7FwoXEEj5CjSbVZtvHNU/nbMcR22fD2wfoC8XxQtGiY
57FQb6hR2s7g+7CxTeg+TLlwMZYIuvAjjb5l1sUrUODjIEICsRzDDep7t/hXCKs9GiLZ33MuF6wD
Li3xLit+/6TIx77/C4NvAuEjmR7nQ1e6LERD3i/mr8EFJEeA1sUi3sdcCal22ATLGh1yu4MaaQy9
6jovoKhTGGKlpswYZmNcYuxfBZKfh2OmJBh3khm1O0FUNfPhAJhPZM1pDI2D+1y5aKDZWylouo6p
dnlD2WqMFwLV7EOCqZNN00gQ0SapoK+k52ZLt9lva8rCST6BP+5ynyQSkv+ErOkzvW9+Ue1AFtTA
yZm+o5jO/bUagoTxBqijplKm6+YeDRvgo8ClSDpYzAlQ3dcDi2izG7s/gQvXsYu4QNzzK2RCIGYH
dfjTarQvfI/xoP2eoVBzt0cUBhsf02+iDaP29wLqjuUdG63wE5G6jEchKKjl5TCMBcE6AEoyG4ez
iwG5d72GCSQhrVCh+4Ih8NwSz8vyZShMFSSB4RynUN+8XtCNNQm7aoSG6vK5krSDmYyirDouoacx
Y/5l+aVrxPhEC2gRsKXThpDsT7sblbDpv46Sr2Qkwn5rShv6S9FRNtESNW463AnNR5IfqDY0B3w/
wPlV5D2nHDmPPogdVhRrfI14GfZbTghjFpGGUkPwCI5iPA9mEsSVsHuPH574T12komPb7YR98ICx
/qcUPYv1pcLtyrsbdkOcQXjtQ5wnjytBl+2VSuQ5qVYXfihKH5g34P2A+2dB2b5AqFpn9XcXrbRo
h/Z6vwd2GGQav4vKjvdy/iQOfxE506JaClQxXGNrwHYU0uV6737VVR9foLqvxMChJwse1Xy7zDWb
2EMezUJFyPH3N4sju01JmTbr615JMLIBQDdMmR/s09cvOVO0ZHQlOJ0tyBu9asOg6naRDB7X0kGX
mCeVtLRHfKCsCRhObe3FidQnKDlSP3xha1J8J2j39dt37rhm5rzOAiORM33MHB8ewz28o5KcLFmQ
pqF9l0W4TeXqlzVMpchV5xonlBBxTMOrhQ2Ms49VC/F1cAJW35A3O4XfW0Dy+s2QvCttdCWo5jdb
UWREwCN7MasuF3ltYI4kezWm1hSr86QyCrPd1u5yOK1m5F6/oTUIf9kZ3UAzqAVVn4pSVXOYy6x/
ayuVjYhWEthXia/XpJlZHPAuZrRu9ItToJS5VbnHXS70nkpGD7zBjxqwptrlWTEuGQg/gg9FCz9J
REkqRqVmCm3S0v9Ij6+iNXESvFa94DUgopSu8HyZWNk1saPj8tNzsPUaU1kXDG38TBXpFtmHr2c5
FxO/bsTjNN0AYsJ2/dQ8MSHDVcBVkKGTnqZgJgpuDqbHt8u5tgRchPrlkvv1pTrF3JOVd3f7/afR
5kqIXlddxDTYeHfHl3noxTEQDCzFhXnNy6GKFq2YQXyzlqNH0ux0Zh3jSV/2Fx2nTkKwVxOJp11c
xAVIIRqTGMVpW5Hzz2LBGlRVxZ4ec0Acgx+ZCEOFCwmjTopOCSz4pPVFKTXyp+rxPvuBC/VqI0jN
YUJJLv9H1ewh+F8ExjQyRCuBW6aEDpLxIsxpfh/HBxg1iwe6GGfP6AnoQiXDRk0CbgKqk2KwKMqR
SZPB50kZddZUZwLJBH4XGxTrbWT8wtpKwrA/TTN0C4p4AiyKfF287xivYx4eor1uwZw21oZyP5EX
tARtBAZ9XRMEq0I2jTZCB2Z2lDF+AB59JfnDlaKDhhjDBzzXNUlyaUJVP1rXCk7me1hWSZ9AJ/Ji
Vba6wzTfGi4X0BKdmXQN2uekOOytXngN+5kwz/jkOhIHSuMB2vr04zWtpx1PXoxgbFs4kyfBd12w
hLYDhxS9On9VFiXWXHEDGW5Vh7yUMkvOaNnKpv8DOt+y2C3fUG4P3+U8/2x06iNhmWshX5TEROx8
BoB5p8WqONscmO4JCrDhUPNLIDMqMdPmDwPNlb7M6RJ+a01jd/yNLSMuTBNQXYvxt1GBaZodDknj
edDCtkAmoXh8ulCbKkCBPbZy3SDa8TCM7wmgoGTwmGHltdH6ZGOBRA6n/euy7nh6kU5j+eiO80pO
Vrj4v703I2xzoDP3+pgZXqHqjHO+ueO7RP69VBnj7Rr+bqOnnu73Gh4KTu4cJ55cMJh6xrRNSyTZ
crCGemuZ+o5ByDkjfKd3pPC2Q2G0W4rPI3NZEjXlTlyNzWY2sFeIO4zgSRWEcstvUfsWY11hIWGs
RK0/HvRzOtsCurjtQicFXc6WlJrGMGmjisGapDZweIR46wxvGHJKjAFgWLDNze0aUJHEb/RXUHKx
Q3YK0sQhjxza94pubC9NRUl8uGDjGOx0y3gRM4XZSaKlDXNPlMdpGzmWDr24rdFavLBfdtZ7V67X
GUOVWYQtgJeYuHOgg/FFNjv6W761vnvd7aCFAa3I9T2VUkPkeU3+wYy4OmhtZcBfCQz9vv9ojDDY
IYlEDQQIeqUCvIgUXo7MdRiH8rnQ1PXxy/3ApwGCaQZ6Sqp+wP75rv39KXZHxfJP+GeaqIXGW274
7qyVKFrVYrWN1A+G0s1COER4IenvhazJtj6TykU054saZO14WG5BQkjoPGuUDLsvpwyi7fgYv8uZ
X163kN26jsiytPgoNpPZtKvzzZvwyZ1yX/UFmSF9/O84Nuyl/lWGkHHloo67dE0Db902FeqWqlyD
TQefZesm7y8FZXUGXBvmPJbjpmWdK43QF4BDHTpUIGaGfalQH5f+ufRIx6xZDlTd0BOzOW0oOkbC
9CzgXLMiETalKjDW/nqxtYHkPuWqQ/8BIAWaYDAmG1Yx2K4GL01vm/wBprTkvCp+SWuUAESasJil
mzors6b556wXnrr0th9iE6OdRryXCT59vMiRlFnW8jtf69oTyF/Od0kUa4dcVJqp9fTvWYuPxRCK
nDRTMaTANopTzayosiaQfiodwLJO4ErL1oeadMDTLQF/h8tcFCS9/N08Nw0XPzjc/jADqJeD1V3h
WbCgjgDzZwYrZSr8iKdJHdhr7mvUuWHUrF9LPX5zyFPYOwKnRmWWZqQN9XQTV6Yeg13bKsJ60Fgn
PuBcI4i/2HMk0uWzEWpTgXM3Wc6E34s0YXcR4C5hRydPuCEjo47uQxpyv2FiXbXTBa8cbqR0uR0x
XsQ/Sz2TtY387Kg+YlmEzZOmV6ry6lmUT/p+U5AeweIpNXbctIgOxXzsKIBCG3VngYz9pDuIZdKw
3yfejNCNStgsov4PxjXGCvUfThLWONmDMhycUyTfvTSYKS/jc8R8KZfoY08E8npwvSBE3d4ZCW/R
O7fZIw03Sa/TIVvPB1rV0a9RyFa14dKn3maWU5ZTKSYzmhfC+36/Z6aFdUv/favItw6L0GRWCd+d
M2vtZ7dM6hiTMsTn8zvh8vkXSSTyvIPGBMMYnJ79hChr9xe7s3zon+cGwdYCTojL1nVhe5Z8TI7v
UOyoiw1rK/OFbc6FgG4L8MOEOSdoIczEskRE4RkR0KBkzrcWQcqw+yQ/YuFrCzYScOeQXGQmfEGy
gfMmToquLuGeyBX+gTENQQa/V9XjHcT+DhDdkwinEK1lANSBSw0cjDY8DXhTyD4fW+5o6wDP/BH6
ypesGbcRPe/2bh5mYUYYGusn7kb6E7tqnn9ED80XnkNJHhWVJ/SiYHr3DL5zm5MbHp5+8q5OrEjs
6PA/twjMuaZTZ9nrsaLYLfWGmDg/x/zSDuAEnMjSa+gIL3T7SVFSEMhySq1EYLD49ba8gTnmab6F
dZhoVnGYpdnclhP4s9ElxVGeX3BExSWdH9pbXQp5HPzWbCtLF41mmg5dHeMwPiM+/8vIouT4ndZO
oW57C6JZFoseSbq8hYOmfSKO523JugCiPTCmf6L+carP37WWnbHqez+Eko28cV3MEKXb4R11UWYN
kP18aDDUsd2kKSwj91IDbD9fX8MMH+wthUIjqJdhxXc9BsukZ6VvLlqNdl+9wY5Hculbxma/9rsa
HE+WM7Pg+HZdIECX5p9xzDAZ6C9uj3I3GqELvA4u3lfcerlSBP5ChhWjaY/B2HiuW1PDCYyzcDVw
f3JgJQ7+8l06tIxgdRgCNsLqn87XAjBWmiQ3t3HOE/ml+YB78xTBExPxQwo6/ibr962PE8qfKhPe
e1UL7nI5ywYzs7c4S3nFWf83o70akagmOPMGA5zUMy4SyHu9ECw9NXy8TnH/d0tBWHFobuUF3DUh
1WxCqIKLQSTw2VzF3FEmpoNn3ykGvrQ8+MkhzbTAOM7fPkacGALKX7XuQFlUAz06rQEi5CQr4Q04
qlXTUPQcU5FR2r3VeRU0WBCeUs+6E/VpKtbMNfJ35teOXKnqDLv1/Swn7G/+0HmFkSaiOMXyCh+m
TFU1D7DHHmoweTZrraLmz6UbcENYEhA3omvj5OB8NA6js4A1paVb4ijmuQ/ZJwAFOCc5XPW5VVee
h9B0ZI6YJWB47i6yfiA5sqO787Cr0yayuzRDo/1zIXoNM4UeIn8IFcs5VavzH8U1qUwTQBzsckpJ
5iBBSihNbLKySCYDCuBAoo4r9aPncKlk59TuujPbMRV3Jnz0LDwYlyNiTmEj1RKD54gd3y1hgGLA
itl95MF1Jd7unIxGKG591jDcdV8p8jHvV/q8mpKXZj0B2qg01GQveApD2q9GreMkJMZmGyieNqFd
eKGzPoDcVIHR1+4Qa4OqsqlwfnKxoPrUPmSD9Bik80wAvkPpRSluMgPbfwK/RaQ1kg3v2VC5Q1Ip
2NcOXbhUwcCooFeCsMiT6HHZuHNNqIg28kOJyi9P05is8dFaLG18ClLuNWeLBXqvuLuk/3eCnZxJ
CmSPt8tQE143KoG0hyqgbFJhLJkSMqEqc/7eYA7VEC3EglMJmnwicByTzlBmUhqfNiViuIk8DC8q
kf4lA0PI8Z5dXTu7rFCy2dOatZ4ArMcOfglv3kO9T7j4K/Ej8vZKPXUhPirLfVRwTFKsBbANATdp
ZGztXArPtDwxHrQ0/KwnoLz70gADIU7pynFbtzogAPGv6rtaNNVcKTrjaSo0zXkR457Nes+oDLz3
JbvUJfBQKp+UBw6xJqjg0WnhAULiLDqk5RUTxXxniDstvE/z4wsOHR72ROB79ALrgrI4J0qpFUeG
9G96XvVsBMry2cu9l5/aVBZK2skO0yM8Y2ZeWWitGsxL38hRU9GCXBsmKvfqR6prrt7eGSPAeFsS
KI+Fgrj8CIifEZhOGpMJ+1nurIIr0V7B4ZyeHSNpKXIsfHme984bGg3pui4KsmNKN815A14UCyFl
wFMMKg4CZn++R19CqrWaCgpmb4jBXfE06lFKvWgiKBfdPeo+4dWwxQ1B5Zwretda539sGK0hniEU
/lK+O1KuIomaUrv5eocHvThkN4CsDdOgfdGX158dzNNrtkiea+oVj9Qrept8qSADDJUF19t7Mjk5
+uFPCsLG0CCF+6nkfRl4BbWPxTdPzoPyn3zQpYm3YkOGAtcM4VCUlqkK9P6J2j8Ny4ag4TLTOo+b
9zE8M/CtcdgzuKpJ3YRcumkcwqlzMrkjjUzLdg0fy1MESgUFP6DwtCfVdAzFHGR4oiOuol7QNlX9
0MDdmTxwRx7vCMq0x9HAZL1+08VaWpzPezAD2CCiOp1QfC4tZOTCejIhDkV7915yo2LseV/dgq+t
Wat7ioDyOJe43ZV0lnyjNtzuPovi9PDpdm/ic0++DxUkv2YDxocAVyTt+3ADfLYnmIZFaLCB2U7v
4XOKTI4TDzIdK3h5o0qtAQ5yuafAmzNRqtc95Tr04bMROnAhmlW0HrUmXMzy9pz9KmW5dKpVwzmg
Cc7zmgTtc4anhcwMsyJg4ChrvuRFeu9901c3NLLG+YHTW3t8JyXCD2omZYzH/g55RyFCOGkdCr3B
HuBOfO0qkJ5UDLC855/gtF4gdiHjAH5Vz4EamwIC+ME+5ovAXNNscIUArIZopznc8CMisS64fPMN
i2opPxnkmlHazCB39CcJeuln3OU61wteN4OPtoIw9CLXJuEPmowdue/ye3aJzQ7DVaEm61gnqqLG
FluH50oyqd9DedT30BzzPSlfNEsZjjoi6VD0Ebp4eaS0sjwp5UFIp82Vi7zSg/aiw753e825gI8H
KldHUWoknebSJXk5SqHtSqoQz4+f+R+rvDHf3HvnIGrkOmqnpvKBatyqFt4q8chKLyQVdkeFMTP2
hOnZc/BnYETYgamzKTz2u46nausOOyRoWFNIuPxsF98tjl9xwIQ9mkazi8611StXX5ScH9nKDiNl
9YhmbAhOKOEv8uAqH8dBdpnCcJOb9pAVVwk8bcuDwHWUb3OSiMyZxp9WN2eNPrr81W03wUJDxp6O
MHRaj8Knl/tLkt616HJREPpSqRwGxwEV+7O/mBEfA0VmmzLY0AcWtDA4S51fYO0qaNpedXcNXacB
dFwBQKHxGbsaIa/MxlgDQreSncYi6aWEEsEOXJ8usw9V/K4oDf/YaWW8jwLB9e2LV+Qvr2nx9Jy7
YfdsdeW96FuQLM5yMAkHCnd9NoTRGgAsDHKV+GeJxk+wCZcAV6d6S8yW9a3hbIBykTk+aOSKXgy6
cP+jjTuheTC08lUbv3cP0PIBtEoFnXfbvEs9GGfv2r86g7elhj+B3/0g2qJpitj90hHTHUBsGh01
QbYHaxM7K1xBkau848VL90OGmSUSEgfnmdmeeUArfL7Sw+Z1AXxyG58/bt4TTs9O1xuLNhkBvJOU
FhS5qJeWvPM1FJV4hNDP5bc7tsGy3MSqhb5LVExAT0jS+SFIr2XxlAj8fAV9DXz+ufdm5LUNs33B
8D2RPtvxGKQ1QszTNYq8OwRg4J/TkUYfwuRKg4fClyse6zJjZ/RphL9AuWr2KL1Oa2/bCJhNEJib
lBwSpuqTDc6BLWyo1JiotZ1R/ZD0wxQXv98pzzUSeAentyA2Xp9asevFBXax77S7atsc2OlMvIMj
qC3wbtz5QZv+B020/z+YECvIdbPX/bvtCNNZnTbcRVy3wU0MHOEUlhXLNwGWIeh1wRSWhJidtNz8
Qq+G3KYKydenLAPha1la2BN3bf0Q9ol1O8DnNQyYoc7H+vJSlUjak4kcv48TrU+ANUWHnCfEc7Hg
oOzWOnww7mrCzUpxOh2F4VqfB9JpJnqtg/33HE0jozvQNHOoag/jUetcqNIIe9wkpFDmCP1w7TDp
URWzMsMuBJeX4dh38UQXXJWrfTOBZtP4KZk820vUKZMFhpAB47dWcyFTjBLsGZsjLRBUMfKVa38u
Eozwf4/Iw8lk03YhvLyexudX6aJ+zrJHPuKJ1VrtB2mkcLe9xAui0d2rmxyGcqopuU582XFxsv3e
HZm1H1TMQHG7dWTspKbOtbMrv1z8ltaEMS2wrnyURC/UePIRNJyEpb/YGnX6ZsB+MH+UZ3GAdwCU
I15p925DFkJQQcSbP9o1Oxvk2DLX2WiSNlALWUeVRQRE2W0dC4u8FSqmunruGmEgGJuuC4rEHi6i
alI1pkA69XNV9BXJiANuUqGpeijWCAR6fQH6lkBoHqZEBzOKjcL7SpsJ10kV2ZGsGxjKx4uFPZG0
HZEXmU5LHfGJqcRr+zm1SMAtEFVKcKwfOdD45Rl1AwRClBj39Dx5Iqk8qOQU2Ak94EfHe5NLucEr
BAMGSh8CkpCVLeILC2++S8lNMoUD3gopCA1aTcSpY9A7iMdeX8RnSqEuoVAFikfbHeYkoliQcuGK
IcdMKxWFAX+yKb5UcxSBvF8mMSmDW7AI7+SWiI8up/4u+WMiUl7UNlHbsbmCI+CQYEwWn8GjQ01r
MEPcFqZHcZmhFGtrV4JgMPVNpeCG80o4ViMCEyxytdJmeH1DRkcNsRRC8RsWFya1lZ6d1YeYDnCj
KyZ1CmbOiYViafjq1l1V9Xc69HOXYwNlp0JfO10rRZAkkkerlnDdUIaBiFfdDZHmMAQ0xyUH5mij
Zm6NyRd+2HYcxYFLDCJ/v5XMDBYIW8L0JAmc73aQgg0vSiw71qeZQxExNfbeZhgl2kUZ6YSE3nSF
aHD6A9VrNPbcTvT3Uu3ClRLp4gIaQcq4mZx5l+FNO1w+rWUGvu4DeEvfazv0agTdUBObZaOg+gTa
i4F+v3+mXNLA3Jd5EkxARV37ov35CTTvC18PV5GfQxDqoaC9JDvBUM5hWZG7GRkAEgx7oJ3TkHOM
nADLRZV8XBw071Uq0zCFJJ52lrp2v8dqO0uuPB0VCf2UP3CFuQzssfcbQbpWFmkXJyCYETuuITVa
hGCF/WbfoBNo/H4tdyUdU5QitXzUQvtfP/6gld76c0sh05YZjIVYURM/9hY4TrcaaoqZ6Q3r1Y87
/QG7flQKvHlqRjhcSgSqI8PcoHeO0kDQ2Ue8FILrihxU9fvoTFwTAoLwhWeq92BWjfDu6270FAtr
e0Twgtvg+yt/TDwYLNpO9qpU984PwE5nwcWFUP2m0x10IWkF1Fegy7eMTsKxDcWERwy83BEmSCMu
QFm5lid9wrW2O0AZ55zAFK1J762q5Dwx+xikRp+Yu2NlnEMq7s98MlMLaoI+tkvOLF9vQSyJyyQj
DV4TGYnZrixN0dvUcEIWgXQ3VJg14UkhXtuzPRyBc3fDo03fvmPuxx648IvrhXOTikheBgodav98
HiZVAzdNb86hFN4fEHcQVy+R1nGQZRJtY59Olvho454Ani4piu22og7i975EiEIBEiGcqnZHE7AX
kxUnspANoC0qW6ai4+tZ/cjrRi5FiQDAL/O2+iERHA1LfRLZ2qb1RiNA3jCXA/rU8Z8WLQ5Trq/z
xNM6SxtgU1KBPMV/jkXZQHam/hHTyf1T85SDZ/Qu8foJpX0z8v/2XBi17/7tf75BHKBSysPQJZYC
xDBQ/Xy8GSDMq8JY3UQt8J7n1JQp/QuqqrGR+HI8PRqxIDXfs5c6J05oHFrf/jyN6s/WRN+W7NKg
NdOM9gYSLzqEvH3RcuVssHpe+ou4fHw5loxr+6rXpHhjQJ02ePbXyBT+ObOHRlOs0PshV0xLH6lM
Gp5Ot5mcv9/51zWlNZNLrV3A+u7MaYvrfCQBphNWMJaZMQjFg2RpIr0DjFwaG8wdLXsLmdQFWnKa
x0zf8DQGIFYJdAc7+cmkVeFspA66H09tveA7ZxebisrjtVtFmKDZdSL0zpDzqF63bW3XV0+jBC11
4pGL11kTRhnaD7ita58+wt3ATmxXQ3mGoUuoPmoCiW+ymBBzZ+XVt3eDQDNqG8gPkSYSS+YmXhTz
/3duguvDe8ZROQ2QOB7gMgHE12/foCIN5QbZfzZoAFKL2sJSoZ1uasvkvYaVNbFIrjQaYt5wEtdp
RrrKV9YUfOvUA/E/QZCacOhcTAvpr/DOxfA2vWZKXXFczO+XmyBNZOmkA3Js4s0n6tzzmmzf+wSV
AgXH8a47ayrXX0ieoynU+W3ajE1rQ5j731oJ0Ok+Fd6KnIDijKoQGvblhfwzd4SkxfDCRZwvs9Nq
Q0zMzIcv9WQlChq1dMyZ+RmyZKwNgPKVJkjYHWE6SmylAw76kjjili5IciI9mcbFnDeO87tQ6/OA
7MH3EcrySU+9TdxPWgdrMN9Fsckx7JQFsLVs6LrS4SsHpzTueItDtlMQjDI2erf4clNjXyMfhjXQ
Ns42cWGQSpArYXZ2MV8AqXN26kxTxzoB6+c3Fio2plSAK/VAsV9fSWiquo6FQqKPtHFDBEzc7JS7
aemRmj2zPiSw+0no9DtR9vLwz5YuuvSMevUz/D/fzqcsVEuS+W1bH3uUcdCBEB9JC+phY7+YmMLY
5w0BB0UoASj9hd47Ny9vcpTmEGdGjbwaUAAA37d70+9lQWeAfVRjxGTb4Z7Vu9/3dBUbjoH0Nx4u
sREF912bmEhF4uzh9W45Rh9aOB8N1t/Ozmy18wnf0cQSTrh/eKjJ0atsjWxrYNbjqQymQk9EnmEy
gFS9J9Q2cHGfGmJ9jAPcVCYB0LBDAqjVY6mX6EQNrq08w8J6Le5muDZvGw9L/3RDi7M1b29DB63d
+fQAPX6gmKcf6Ekyu5tyFFsP8rJmXnMuyJU0ZBl/5vY4zBimAtij9QskyUC6wM9m8R/vTXBtaAfx
MFzF6RGW0D/CnqmrkanZPMad965ULLzXsI+bzqipjfQC24pIwDNXxc3RjlfGj/FEKKBmMtBBoR4u
3kJF6j8pqn2wOLQ0Fjp/SdhcG1E/IupX3raktwxKf0rIVdTJCJwgqfhfi4OzbuaTG9mdyrjhyuET
u9FlPf0hFpyDM/ZHq/gMAEg+LcQ79Tf+iO4nomCGa7q0Hihs+pO1s8DwTHD8zbs2Yy+P6OXrcN9C
8bMXfeomYC4NhhN6vMueOcfH363FaSTK8dssCiivRKzaFObxcPdAYjuYVSwCS/weB6mA6Jakrlos
FMkmdh+1jHXiYnYmEgVMrAx3xH4zYW/ZDDuAQ9+uEBDVua5YFmM9J2e2ghhDdx7HUrGMzcnj0+DS
iJW6eaJJvbU2ShuYNCzQb2vbxPLxL3kt/OWlpXuvp5xSDz0c6P9tKcgNUCvyIj48u/LVceeOuP9b
oWxMY1J/x1K9aqkYnoap01ul7Yf+N9p4stgc0OY20qL79LYiF6qpMIPHDWeAPRnL1/EP4czz99KI
KHm9XEdENFx303XoVCuUCQ4wKyEy5kfsEhVM/7RJ8547Z8hfVnAQKULphHXZgieTQw+WwLQQCJV6
uNOiAKTddlMDZOQ4drWpBMiyRDVNOLB5NAgtbpU2teGKVYAnb9T/109bQbt6qf20bq3qOSGlI+RZ
HimJxLu4WqTrA6E2klM8oh1ShM52ChPK2WdfztanHuPMnGgYM2UU+MM7YDQM3wiuAd51rp7WpnNx
q7+2wOtGRBtnVSPUy5VihEtQE3xpcJaJaCA2+BIrcUAaXkYvVispNX7vaEvxQPIUN4ZM56ktJxy1
Vo2QlafFOr1A7jBu018Z+vIsb6gNcI018QOhvJqCCX/Y+VmiKnX3OsKzJR7ujtrKxM/Q2TcLa6/l
56uoqy4nMTsqnXRyYhaNtXA+KMrg79AuTpDev5hMpxdPz0Uc4o/KVCIw/jeO7LyJ8ixPBsYDnCbG
EG66KrPu7us27NkxtdHcQS1GpKY1mY3yyp0ZIwnaof3EnbRqUUiLsnXqgTOPtyqRNftiCiYKQCpd
nbpjPN19JrfymHx6Z8tjLdWyBagf6YCy1iLLnveH/9KhfKN/VCYuw5rLlyntugxjOF59KOQyCRW+
khQuZq+4LP3fUkJsYzRyuW9JKnxZA7lljJJFqUl4dsxNPxZelMcXb+VsSjPNskgY908zJuRLfPaW
MBpZw7bDV+Pm3YZdwc50UahneqZw7zI8FufYjOhbne7OWLcfZ8jZJrZlSE0lIzYh1w9yQlORE9EV
HEBtRsdeMuDwysp3UI0WIvWzaStOF5WlD7L3jH6IrG0YVtF3/AH3t9XSlKWmr0kZKY63/cMQFDy3
XXshQsM3xVoHl1wII6CGYdspLI20oZBC2fnj6rFsFyHvKP3naCLbzTYqEXm/IAfWEkvcTcd0Bjsf
Hpitb/QaUS0Onozovy80eGReYtDOsM0Czx664nRKFyRCqmplWRtSXPXd/kWIGFk+xNwXIRCktVNv
+QPZH78Ax7P7Q0r7E6cjtrlTY3FZCeN/vE6083WZhwpAdmWKeGWWpTTrBmSJX2EuZyp7deuADahS
LECn0uFQIHxoW8YS+qnUgCaxKzi+w+H4TPDaZBoSmfAB7NaZJQVG3zVBu0llrP5q2i7kJtyYkGRh
Tl8XUNUlNuFZdDltNjWyb+cB9ib/cI0qcxj2so7SDFZed7Wsoi9CSNkBeyaKpDN1XuG22VnLuCGV
ALHv7zl881oYJt+C50HwA4Kz3/x7W2U1+QIGJLJurM9V017OQOsWHWQyMRCdsPp1ne1hVdGz02uB
77dPYTPmBrhvHuFj4QPbu4DzfBJ7OR4XG+D13rJEDLJ+BjTvkm9aI0IPraz+haXYeVjdz/tdvRah
uQp3fxBF3N/PkeAI89iYmpRUM1Bzic2KIWkAMlIuZfVWdFo/O/DFJAeDecwfO85GxS7gBLtbeHme
egeHEhsYdAsRuF6gtCd0STG2/Qae/cq0+V1shBoYh1UMEqowW0e575yx3hfyRvLpBTvXyG03GKpJ
tWtjbRmONKGPP8ZbZdZ+IEqgRUQBRN5ybVcJ2+2bydGAkcmdn00XrpsE0Py9c4nJwo9EV7vewkCS
U9RLqqJ7WBqT8u+3UXyHOXr+ml6Ihz1st7+VjHwa9wlUlpJCSnPHg16tBKJwJMYt9Pp6MN3jg6xj
ZDe2H66zgRHCbBFB7Y1zcdOr/0zvJKH01riNylYPaw9Ya0NTvar+YAHozlp1VC1zcJ8a/JzDGP00
NxeleqxBPFhce2/INf1Rr9uxDyskpgznwG7OUnuzpi17ETX0fGBfeSV5mjIfrpOHNp9+NLv7p5Ur
dHrBayIo/p15tuBlvW6DDevWBwQwRYPtJJkYRgGauhGMtV40HY8REEgP6Sp/WtM9DX0Ovbm6m+Wm
DKJppia/YhTwQ1WTz1lZfXM8f72u4/Wzf7sotmgVbahkkQ+LDl9yccrUEVy1NE44mZ1OV4DgEz4j
8vUTyjsgbvUFqLeWbdeP2UmTn5m3PD71sneFr6kV2M5W/yxCabOlUFYz9SnKaeCmt850Fo1+jkcN
Ej4eAsNplwPrGYRYOa69Dz5aT1XyHOJQOVVsFl29f6N+/fZE25jqfHyJlv/LCdlNCML2aMe5dNAg
l2EiWmu6C7a6YSK6T+3mGtVZ6X7tIr/yRCujHGphkfWj8PTcm6Eh2L71OX+DfsbRUiEq23TnQ7sW
SxwPX8cAnCQex8ztvgnqUg8yc0zNJHgfzI8vm5GbjxNzTOSYq/iv438Q1pnV2xvp5C7QSaGL0DyR
DISg5isP6wmgBCefK3A16IymIQPDycB53T9eoOMqqkqm3C8kM4mK6msvEdtnBd5tl9H4tEyaFEMH
R1GOHKFaZVQg5xl6eEHieTy6B5gWowQKeL7loUjbazT1iD02G4PqHdwjC/0dWqOT6BPsnrNgELkB
qTtKlkjz5By5B2M9qcuX6UOAXAYVdu59obCzGEAXmQL96exD8zzjdhUXhVt/wT8LHI6eRURpHHkL
sZgtXV7zQfvBVeDDbihypZz2CXmk2EX954TfqkqDqxoPZjChLNDjbKULfnmHrpNNQq4Qn5oB/i3Q
MMDgK0pai8oPWs8tjOC34xW5WJRgIXGbngrZ+G2QB3xBDzGOMFC3P7KriNRczavs8JgV3hW0t5dP
APP6TkLT4UnMGcRJi6R8UQBowRtUpsNO/XdxB8tZxJ0Y72B7dTnOmC6+IpV6fZszg2e4FozciX1L
HdpF1cfoZpMpcy+mqfEmBJum4Jeras9/O+uO//gPlomWEWVmCiawfZ7JISVEL7aIcGjEzQYb/LnX
+pjytwxYYA2ZvULRCuxMWiloo3INlf96u++Bm9VfqnpQ2WWykHdlxewyqGWcSSY+gLDasfQ3HqbD
t80dBhfeRInpiT0mjbO2RsnO7bqxT8AS/T6uUzM1cBi2zO+bxmKaKTr1p6BD90SAklnCzHITkvlL
2/yW4oCYXfKBrGkbFXqWBj2gRbVWCnnivEEguNC8zm6j3xVvSoADoc/iTiJbIvnGep+rd/wJ4F1o
fvx1GapTagdh9aa348h9CiGpBjvgSM97jQqvCBBmV0sxMZ41i6zgyQ30EFAztq2yn0PzHoTSjTAu
iLABlQpXc+cRpPrT3EQRwWpYJA3b6KjTWQc7atOAPHrpq/M3wOnn/gysDS1RULy0COwyT8YgCQmY
wqV4T5Yq83jUUY6JqB/uRAttHcK3/suUJWqqAD/5R8BowIBVXepPiKldzgjqYnWhCQIpU/2GFeu5
+Yx/5q6e0xMyD9/mT9ecvyaBHIncsFHYB9G0eZgkbmDuEQ0BLWOzaPJER+hTZDOKZx/kXZ/UapXF
P+m+o3HziA8MWgFGoUWkrWablFGXR7hplDwHL7HcbcoR0zPQkenS4xMGT9AK62/l01qlp2G2+k4U
HIhr5G8+HcdzpOC2BXOr0bHSjyMACOGIDecoPRiasPOPJXR4gDOg7fehFGT2mW5MD0Kp5i7SotMD
bA==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_4_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_4_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_4_0 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_4_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_4_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_4_0;

architecture STRUCTURE of design_3_c_shift_ram_4_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_4_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
