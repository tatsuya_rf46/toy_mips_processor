// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:03:52 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_4_1/design_3_c_shift_ram_4_1_sim_netlist.v
// Design      : design_3_c_shift_ram_4_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_4_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_4_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [2:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 3} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 3}" *) output [2:0]Q;

  wire CLK;
  wire [2:0]D;
  wire [2:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "3" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "000" *) (* C_DEFAULT_DATA = "000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "3" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [2:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [2:0]Q;

  wire CLK;
  wire [2:0]D;
  wire [2:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "3" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qCpvOMPGjtUUVvyEjH18WQnLoEEVufipMBeB3ODoxnHCZmHnJ6pHkAttIulews3Eu+AogSjjjrPy
HteoFVVfYkay/qnHXCseJHsvzGMffDcnV9Z+bf+DpkEIjktuptmSJW68nd2OFRHSqbuyKvDi3Rzr
mg/2Z6FIXQlvD/ikjqv6xcrQXl1HnU7XnkJJIhW6DVYVulhAGxNwmYY/W3pqTwZ9metKIM6CSs0i
faIlUVL0Mjec/AYOON0NEibFQYYB5Xxx+I/1v8uyADlgEDATNhNgilnegviF8lXSge+OTJbGCS7S
wZ5KAcExpPOtPWep4RaQMbK5TtnIJhuxVFwdzQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cw2cwf8xo23dsFDc77hxjDILs4s/8h45CTe5z4wU9sT4KbgNdihsRYyYR0aUP26sPbHufLp1dNV0
BtaljE9QcEMZFyaejyf2jAbftbvDx+1u0++nK2v86utHAuNdMpLY/KEZmlfCM+ECHOEi5LdPc2lR
44X+T1VHZHNbneFjyK6nL2rEf5HLK5i7UEp+IHRQp12mvhOJVX0wHHYRPGZYrbAIBnRd7wEKs0Jx
vBiUMtgV1GK6bfndYroyJ4KqVtydVzi70UMGQVK8KEUYejXo62k7GhIQumjsXnIBXbaQb+1/8stb
ej52jnPYapkZqOCCRB9sqF4v4r+oYpT/XeD/1A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 5536)
`pragma protect data_block
Wd6TOOCzvaMlXIxP5Hu9sSRAK/pmH6S9aRg/IjNpDOAapN2f3AnmBlipBTUyF4oxzpQDNjM7fPv2
shVdqyE1DKyWKX/chLIucmGR7AZDbtwV1CRszsFIZfWWSoj7GsiOIsX5K6cc92muLSNfhQ+kkMZr
0wiXQsmKPSJ9KcFjO3ak0Py1KxxpdLkwVEEM8PDwLJ0F+BJGb+LrIjy/vQcAFnnHymDy1h+7Ey3g
+uUNrFrQxImQyWOS3BO0s2xUOdCLp9+ia34TMjCP2BjIKALlWoCYPxLucgBsr1QrHg+20/cJ9Pqm
zscroBBEV8DBNPGvn7LkH5BWnH/oBRB7v7liqmsuVe9mepAdzpXE9nTsdb4EbYrINQmr+8jRhO8B
PbInRFCFR2iVkwWNxcHx2fK04UaZpL+WWvKEYdqABDhU9C8q4ezbEFlY1Fuq8ZoHcKtaYm5/aumy
f6M1l2AcP1ooTPS4cLUWfnKMTykerBsphkynUBcfj6mS8kCk0BOgLFAiWOlwfsPz93WaEJhHTkTw
TICT3EG+opVugc1GeRPjcaM7JgWlVsvToXE7A36Z/lfbo+rQN/CW1OQfcGCwIALDXkb8BhLzUJZ6
5ChLBa6JkqURPftUY1G1256dZUXX3u9OUuQ/+QYYX+nOG5GMpk8MrumRtWnx4vln4Wd9RL/h2Hzy
ZZB6Nh5cztoyojKLIM4VQjjYohdsGPPMu2E0R47dlwqasJcP6tiRgoG7UAOlX/CBkXJG/QwZr7sf
mO6b+TQ+jpjEBrtQqLT8vzQclS5B2qqSpFX/8otTyzrTYPR7c4PCp5btA5vbsesht1udSJljNr6i
PZsm+LrvJ9LuJJ2AUJJ+WXb6WQDk4ZYKJYMVWyjcbuiqhmGGuYPxr7McKmSgecpG1lKLBmbMC1Lw
0EI/Ivly9dslPzj9PYTf2ewKrZ04uDbPETQGiL7zyfnml8ULUWb9pGhwHVn0CVrsz1cIjnQN6koy
sS/bY4ZdyVjBJ2b38x4R47KOKabcWa5xOagTGpwl3FqKI7yxinB2BlbDPRBKuDO0f+L9Jlf2X+3d
HQVrIcZgjILSM4hwHG1UGCUyxo9R12bWueskScKdBJhzgumRaz1YTKruhchDAH8EPJMcX+VYVzcd
Hv061WW0jOwYfKxKfIVtDOMNAw4wVsM0O8vI4MAmwSw3IWe4YfEojt+dQWEvWOf1ApK7g00yzKHe
n86dIlB0svRWMXEfH+bBS+qBEkQmd9NDJW4s4/62jKuXdeOH0eHp91F24M+IVD9YJSC6gj6jUuma
xOTWF5+AdgmleR1Qyek/W8ELPd78u68RIVTH8vEdAnpq1hYbK+er+6XX/T47e0dM1RvFL6awAHd2
AXC2qJDxxrEznDZ/dptm5GkIRsCpSkIE08+RTcr9gyiF8nQiTVXYwuGgQnKaSXx4SsEX8WlMJctU
Hn/R+Ix3MjaBif07b7Vs2ocUlw6su9sNhS5IriZN0rFHTM4Bpna16t9+DT20B1EkhnOTTgpWUCRE
uabZTrW0kp8zbP3vpMt7/Qxp/NDFqZUcxDq2epd2B/Q1dZw/FAdv6PeG2DGOCtXTEg4dMONmI3vH
67PpZNW9W9YmbILs7lhTYSwicy8W0w9kow3EncqVZJ4GXgYSpw+eFrhOaaw7D4ezZjg34+wZ9Hm4
n42fkxAzh2GhKhj1m8opmI1LtZ0Teh4ceIdr5Mpcj5/cp4aJLZlCpFqL1rqzBvWSClQmcvCwpR93
w7zerfH8cVCT/ivPvO1q/fL+ADsb7A8tkaT0Tj4SFmZOwrlXBABCIZwYac46Z2gDqqw/rO47gVqT
k2mLUa5FcGzYmuwPyy7O/h5x9QoGulVLorCW1WI4NSmR8EkFPuPC4RYYFULCOPfAOVIgYVylCdUl
ibccHTdv1BdCtJA4wk2GFAsOL341dq3m1aTW1AUls7oBecatM2t1NXTx0ZmUjd7LNDbN7OaFMv9J
xdhJD/24N+FWTqcON07YyQUZE0fggvH6C2hG6IC6STP6KmaVNsTViVcyyeEJIj38/3pzVPqNkO8X
35thqOurNSghcMGJXVDvB1so7oxDEQvNVEo9XEVbNHvGGTLNWPXgqvIqfTtye0ahsye0tOs5MKFm
HIY8cikl2XTGlTmjW+QDsvkwIYhmJQqZjqioOlvV6YnOVf9KaCbvMQM7xka6cXDsvAsVh9xLU50C
yGj4uKw/6SCPTmBTRmjsqNGS2AHYuHa/Vp/v+nZ/geX5RfJEsNLP/98SC93npJwCGEgXfBDf4TII
pQvbkQzL6qfXlphXOt8fIUoldieA/GVJYbdjzVHntv3FWTRr7ECsuVZxnpFV58HX99iK6c4lNnEt
fZzGWG8FH56VabCrM+TEufEwwbyU4rI3HvZyPB3nJ3qQnMGgDYf2/PJEAdkB+fMQx1rSZFJZ0YWd
HwxThnDZDPxxkuRWIL8209Na3cWOQpFAfrXXOibN0KpePd2CHq8B0P0s1WbVmSqzjSKAUK0Mi3lm
Dt+maHfV7SmxoSX4rUfY7tlpsQb/9zA0KxzGCYQjxVBB/Dkjfcxx9U3uG2lRkqYldj54Sk8b6Ius
XGmLNfI5wfqsbpHlrkLuFbglTfQzRzjmsDA9em5IEO83oWTb24LtIdK/1ziUrQk39oqMJMTJsu05
AeXQVujNiEGrdA+YxhRNNpL7jZ/NRMsJIC/fdeFMh7zPfbUcm1Dei5g+nVFIImLrCkWIH+Ce9TW1
hXP9LBW2iuZQXVhjGxbw5hAPeLPlhQM0VdSccHpwyW0k2EvRtf86DucuodUKo7ZLi9PAbJSh28kj
7Tg7mSdAnQzayOJcJl+Ru/vU644JPsFSgDng7g1EhaX/h5K68gyseL3XX/53R3Vr5You5lgKy6vR
6GVDTEnbNvc7rR86CjzmrO81pDWiAXozoUrxXo0PGqHmWUnuvd5Ozw202th0deoXYLnjijeSjMVo
gsjGdgVzP8mNvuO96TcILpdlcSiArTrYRl/GNPRF5W0W+mjo6pMo0NUqAIWmxJVvTQ8fSZqJZ91s
I2lnds+/AQRylGTHgpF3bYT6W/n1GAxzfWblAIUjHMCzqOL9XThYfujZTTvsQfOjuQ+3a20BSRzw
CQD1xLD4NbxOqFybYYfJdV0/3R6LEXj01bOklOL/95SRX6Ra0kwkIP7yHlGxxyyANrb999fZNxMs
FOuHqh4IYwP8no8qmhHThSn52ypHfNQneAWWRDeHiYtVm6BXP/AHEFRbG/VMJY3kaQ7l5/Rw4MiA
sNmKFFQ0gNHX4+sdwEeZ4Hzl5kEq25gnb5JXfcUd+mjzRid/Z+Q25GL5TBGNE0nn/6Tw0Z3ZODCV
XVcl1jA39OOkZ0HJs/Y9JDv4vSoWlvptnovlfEkAq/tRpPpXLN3lNHxay96hDlMYwW1UDdsnERZK
EX103ybtiGlE7Znmt65G+ZujmGn0gx8olGMTdBvF3ODK+qLAdgOApa9Z9/5oSPZ9GZQvHAuMNy2k
O8toMeFpRq67o+d+hI9aav6eq4jCGwP3dHB19rRbUM5z6i+y41+fB+9YpJTKVIQRhSEqvIoqgXlH
wS3eIVSovGBfnwNA5FdNrBc0LR3tidrEtaNNkGHsMxIC+AYHvfuBuxbNCReB8FUX6KBjjiTM7Hu7
qkAqdl8jT817dbWG1Fa187/Nbre0og93ZqLBfaOPaZg8Mm+djsJvQbFzrM5eMwNnaRp+SsOI0X23
xfzA4slWdDiLQFRNA2z4I4RqvGpmPF40QUQgxi1VVWYlfdaJwv6KAe3/nGUQjEj9Z2pxusDiJONG
sFt1jKBNZs7CTWBUEgZpsEphlT7Nl/AWK5u6Si//qd7TnSCCFbJxa1k0/Vj25enkiMORB6dodbL/
nu/jUTnokDrNZOCaZNY9iN25T+QAxq9oQetGjYHQ2W9sgDEEHZRm3TkhfzJvCNNq8AdMORQEaIqS
bBOzbI/t34TYkTcUGdQ8lKxXQKYdR2q7P0++GNP2drGGinjENaVn+Md6hQYST8UflDY9aHIXps2w
F6KlINvz9Vt8BA1A2Sbylm2WtUUpWjP/T9h7EpvwdvZxCDukiSwywz/mVe9x8rk5Qx9asds+gyC3
lV/LA3WR2F9GmnL4KwjF6Pj9EXPoPXqDxJAQY6v+QaVbHgD/88dASjYpHRhCTvo0haDRfLJ38lip
wJPtkmbRHKg25T3Vtc+a30qG9xYJyCvjKcJG0p9XobAT08ttqdB+BQPwH78EVzxlcvZjzf5Q0VVh
Uu9qKJpoMesjex85nHQdv2WoPiFG9kVVDmEt1km5sqDEUmd0Fl0aU7wXIUfJw5E9Fb8AvvybeaTw
SSdNQ1RrM4HQB3laFnvA9YNm9RfV2iqCQM84BiQ5z6lw7tRKWRSaXz/mK0xlgPzXADYeaX7TWOwI
MucuB5No2ZAz/AHt9YyMBjYy1F2J+k81e5LLJJ+XtSXZGFbgtjf5z3IqfQk/qnms1gl3ePSbKBrv
thb4hTghv9ERx4+eZ75p+kpUcE0ZiChDcKm0BFYI4I3KIEokS1qbZf7ZM1NYt4psXseFxkcplKHS
4jkrYIooDR6vzwYli/nNuupo11ZOf00nX6Xv0Zw+inaxRh3C3ivaiuSFTXnOz949qvWkPJJ6dZDX
4sQ89BMVwo2FPKoS8IJeNOj4s0OG6CoRIGHmwwwCk1xbDN9BGLrijmRrVBGgJ9UxZ+t+t8kkG2fl
hjnEWYM5ItEv65LKGz1LdGxrDCZtZyi+LZfn/bzLAjO4IqP4D6tmmMzvR9UMHEhneIIDoXcFYPP8
DSNrBu5bQX1v7q3m8OjcpYh2RJ02KxW+qsI4g1dWyrf6DmYFTkuTdtWVufArCuNeRArEzVVbTv1L
ixGlKC8lvj/uygVtuh4fcsg44AeEz+EYFFVKUJ/cMDZI6nsFYSdEECR0zr6tElleNYWPsQOqzXHs
awPi7EGEaZ+FQau+irAR6/cA6WVCV1LlSJ47nuVhSivKin9QsDa4/8YxS9KeCMCsia+dfhqmua7g
q1oVqwy8et1v/jKAMTS+R1Wopi5LvsIW3pXCgeNyjy+kI+4jHz560OvKXz1CiCDxXE8Pa1C1sVkn
kr1FzN93t7ATdPix4H9RpBekBq5LaQ5cFkDnl28dt5wlqEIWAfCyj2J1CdC84CxTFvORbBF37+Mw
/TjiFi0mwt/0l1Fw0yZ51V6LTsGdH/I4qfoeZCFykBhpPP3d0e40JgbQ+aCyV5gAOEHV21LDPq1r
ByNOo3/rXxuSlCnu7FhTBgllwJmfsR5Wn3VBETFm4gDg84LonyS0qAdvb6cfIibh1g0o0N8eu3hp
tOFdYejs2Rcj3tLfOuqJik/l+zKbVWo+Lymp/mhqMFLude9HWsTqEnYk9MgwbzjztbrQ2/7VyXUV
3PFefGe4kjG1Zt7qGiPiONxksAq+N6PjRKSOkFZbUXQL+FfimgplRNBMOU9QbRm3NIsc2Z+oFy+Z
GtMNcIRfx2tkNr1ewNnsqH+XZThcFSlPQ8KyU+A218pTMa3Woz3fdlLGCUSz0wTFinpuOsBAIi64
v5teOLPIBXfJKSz7kxeHtyWXmZgLz9hVDzIFC0ViigQ8RSj3gjGM2XuDRFO3vv0+AbQIrcvvv87K
JKqkWTpHROfdEwxAv23epN7428JYwluozD3urUJKD+fNTWoBxcs9/nkQUb/TmDwoQyTWb+FkmiP+
MX1LNoJpgH8hEdOmW1BeEfSXnrVcGoyanmKbAuMafkwhyK17mKdA57OpQjYfACwcMSWu5wo3grZ2
JP6FHE5WaTHAa1Ci+4OXBPdH8eEw02YFKljmaxiUotEBPSj58EIxhB49huxHo1JmssrDIURdQqUG
M3tY3jvkirLFrPnO8wm5dFqPggkcXmHZHMNkBWnZT66ByvTResyRBgJ4EZm3CdbsRTvUnT+G28Hw
YNnyJCsnS2XFHIf8Jid+flH05TW26TIwlCztMadX1xCoQ/QHZ/AwTXp6Nx0EHhhVEfAq0xQ5KOSd
sGNG6yMdhzI1mkL7HH7SF/Exb0Tv9/n5xlkuB1La0LLoxQZld1ZBjfXDDPsdCJ7Vdg1TAHhJyTGF
A8URA70iDg9FoUTY+4wY3h/Xm+SjjVlKE+9tGDBD8i1ogwFk3Kj3G0+daZ5CHv5w3oIVPQWrRNhQ
vY9Vyq6U2R3dA2wsZKS/Yo+GtSoIgsqP/auDZy0q0WrtFdVw8iGpHhcoqj5jRpopqU6p0NJVqNYu
4U72g7l9jR2EuJ8APeMDcXMkTOSEWJKUTsSR+Q4y/MXpSv8O0rRfsVCRhKcKPABfc+FePZmMsQqJ
5phh4uhEQ/qyhelMK1lv/66KZP3n0buprCOegsisFiLEDI7km+7Sh19tW7B8OeMAukZ074PSVkp7
yZYB9uFEwIxkit1DYIuo8bQwl2dSSmECoUSrCYchtZYfEq/DcmeEe8pSY8Tyy5MCG/n5AgDaONJM
tcjNOVJy3dzTC25DBfq3YImDY50Fxhxx0cPVxSLZzUEwdSycQX7ihLOugk7t4/cwa/uulmJR7Fs4
rOjtKdsp6l6B3yZ7uNExo6dT+hCGUfdD1GNpbdU/s1zhyva1+lqDAXU3pxLqeyf16+eR4eYtPmCY
QxpfArJ2UrKlkKqNUVUBgWr2sKE7nSdz9nfxstUc15XhrpG4KY8cRwjWLV5IWf2sQAc2FPWSWoS0
DCSh9lf1dJKmQB7ELPEkvjyFCDf1EsFzGKdIbwJA8Ubyc7j03oUmTifqTxhbdHpaeb0hEn3YFD0F
2r/4wYHmn3umwubg/ltgq699t6cLJhWqDYHKnOJh2214qH8nndsQ0iEIoAi5B0Ftjp8BaGcsSwGH
rilhyE0mLVfJ543EHlne7MNesRTwcOj7IAezcSKLVDjGaDZNs9/iqS8E8HdFjzywbMwPdUJMXWqs
L5aNpThiIRIEFdlfJMQPssyCT/iwEtFbFSy7xPgGrbm2wTjkMHAATY9B/4mg+X0fABI6smBsZeR5
1Ii0Ei3tLfxRXr+OKoAU4euzz4lP23dxe77/aKQuAYKFgbMv2uKqfI0X3QkHTAqV0X+To305/Wpp
qulrAyl537jJnBOdDTZS8VayI2mPrmBARi0u1LCk88jHofmW7XB+aWex4IPzaxAZyWRs3oMulJHq
VwEUe4N5Z+yE8+AUpF2SztZP9wWc1Du5d9Oq1XZi+Vn3SzukeHc6Gfu5tvwacASRSOzw/E19QdpS
BeordbZspRxbtYr+mToZwYqQcIKU9tO5n1qa4AXQhWJ367wI7AqDMdggZvHLMEf4vuFTrTzt/nA9
thUgQGFbgrFDY5FXjkW8CimW79ZZJSKIQbefzwrFhmV0sRJa7f1QBpiRooF+gjrfXQxU9CEbu/27
3SL40l2gaA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
