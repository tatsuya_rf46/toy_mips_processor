// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_4_2 -prefix
//               design_3_c_shift_ram_4_2_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_4_2
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
pgHN8KGt5EHR7KuIIoQ6BHSIqK1lILHSvskYRxyFlVvv4+S+7mpEfejXbJk+A1dWU/e9qhK2684B
kaLLjIWpy9rAapFBEydjaAu1cjKXsmhxwy6wNW+ZmJU8zWE5vlDbyTutKfbxQyJ+T2VwYbpJcZPz
qg7S8WAJmrJPXc1IF0JUJQt/XbQ6EYwWAJrBLe/0rMLDtX02xoRbxZH9ozGL67W87iwVqQwtzHEc
IX7KPS3NW76ALP+K+KGq1tcykyn4IFAbfrP6lzCiETQlc4gQrnLJjViUbMzP2kw8ixiwUZ++gdgx
43/vZiSxEdXQfY+bqkvivn2maXXmz472kfJInceoIuuaXc2MjMO5Sg3YmbjXIduEw4Zu3GC3Mbqn
9vKH83gv1Ikwk4bhDQc3Y3H8UiX9NVknSJBf0G0xSvKEPjh7LbY5+phIEMqLD5k/52nDVKkVgjqh
5TUk9z1HQWPrQN7ewDac+SruWVWQ9nkcMfLfrj2MnD9QGVuDY39GcpOJ0sGI0T+ykBFniy/xH4Va
0+8Ns4rw5THBzV8qw2KMPN3B01p/DxBR7sqWZPn8p//Ddn9UBxhz6ZM4CkWwptl81CMSxbbMXYcG
6NNTyEu5HzPRZfNVs/Nzj+u/0zgCJp/Kqd+D4ec6IYvb5GKSrdy62zUS+QWX0my4b4j1qLu+hG/j
TePz0Hq2FxTpNeGC0ArPYKMwJnbYK1KSFfo/wVpDVi8qwJTrOqgcIp+xkFHawlIsPGFThqAH2Our
1KFbBcAyJjG5agD9EzzIXYWz/Da2tMoy/GrewBz5lQxB4V41dzqgUjtWcWACjng6WqubaeLinrEp
7eq27CbLcoVeqt3EA7BFxD3sHpLPE1DnLl0AO3sCpB6UNbDxFP1HLQpdMBjRmLez/pJZ5k4BYbCM
DC8ALZRxNurMfOUftjRjGuJ3Y7iORJ3wt9pEDJOuhkVxrF1TtxdnikcaouNRarXGTW+Up8mpzCyk
rrQPbOPCqZcVDn3aL6cSvEwzrYGAqCmCgAWsIn52yMzMugjxoQYFBAl7nxVpXYKBO7CXu/lDWuPn
BfnP6mKOrZlvfoYlUBJngXbyoqKtbmcpCe7I8Jim40VDDwNMRU3OVRLA3EBrhBByn2RU4TtKIXwh
O4LM6GwNmWClx9zdshQON6F4x3gmyTwNuHCHZKN9fQ0scVNDZrOuJLk/Sxo86zyYo1VGbBX/4KH/
1UCduZNShSpmhdsM4nr/CQW9ECGKMnxv+oE20yGXqZUPvZ6Mp2RTygRzNICOXaL6+J8hSUzpE6kg
77PHdwyhIRjfiMvGx9vSzESWKgAb6o+OB2ybGE+HK+7opWoWHASUiMobt3OXOy5FYi47xkwMzxaW
yFG5vECDhTOJgfF+w9JqW+qVV9UaRvBXiiBBKSG+NRSlclXIApuuGPByyGY4ao9k56+DAGsXKELi
lypU4KM0mK2XyklDWLdWlwMGe+BecSgCX1/JpHXKbAfcb9kZyjkm+hkHDD14H7UsuyWr4QMM7d10
fNEmD5rArPa/cGzCzMSrMNf6Y1r/oiPqefp4ytOYsW4wqPGOFRPn9+np/eL0G97EDypc5bRB4+vD
8ow/GTqTKuLR/ZyBhzetLGA05v+8qwCkpz26TvwYcurb4lNtMC5b25DXPAif6XphFP2pVpENwj9y
fM6F/4u77xqazyL4tuXYHqR6UvyZA8CaEGtJhalcF/LvJEtn7CQAFfcb3hp8L5IUpEM7+aIW+eyO
kbGadGr69w6oORcS/UF7pTSmr/trTE//ChifQAV+CiTZ4Mb7mD0TAb4QOz8gk2v6Ce1rY5eP2nMQ
f2lwONLMcZsaWGH/WdRyLFXbjEJTY7B1QEUoa8V792Wjo7eLzryy1pIG4zJKw3MkVbVnmPLzQ0NU
oqGhvWcvdqxF/KppDy1LWd5HnrK3ROqWRDAdjeAH4A69Qs4kJRDLEDM3uTHQl24zXNv84MqtxhWM
3AS6uRO606G0/+/+5d6ZoCujxlR/5vAt6+bWGwcPk1gDVmeX9jWDAC9ABSuy7xRX3MjmkR3RSN6L
UxvVMmx+gsmWP6YU6Gcnw73UMg/7BaXGcjkOMt/Fuc3Sh+GbQg6vh08szpDkFdn27fl4/FWT+WO5
ak4GQjBzIKcrw+EY1VeIc02ujfR8uBFFi0NFCZS+QBLCXk5uChqmy1ZHit+E64Ou9LWFeFeOKVBS
5hukOuIdSVFkf5W5E2zLRWEj5wbbQ24gj+pJfTEmEryXkMOYsHdkfj7tCkHebwNoArGKpSRfzIr8
bjb8ukQdfvJPvYyd7Ev1RT5ENDPo2Yt31tnT1hx0vLz1ddJBhFkymE7JRfR24w1qrlsQ2VQqWaF9
kouyAEf82uuPincpL5YL3Q+5MnAe/PaNzkF0ALTSe7zaWAnCtqA8QRRcVBRRhVSn2jp6faHr5Dtf
20yHlnTqtSTT0ZoGp3RV7C+sqLh54WWOIMWRcSZYNpdfaaXgomq5N2aty+wCzCFs6fPqgeK0mXZ3
NFExyeg0QiIQIbp8iCgdxYVEJT8bOFPu6O3G6hMmHiDO2b4Z2h8EXX3zjvHrPNpN8yaK1jcfhU6a
Ae1dzwLwX83ir/1lZvK379jRBERgPJ+T1zhak+2aGl+2DqfGn/E9f4Wrpuro4Ul7URZ9WaLLrf6E
CRS0BPJfat509krspeaZ0ahgDzRKMaps3+sAjijDpoLiyOHh5BQwQ0SMQX5vQOARQpQ7hX/hQ1YL
0N5Rk1BZHyCUZv8fo5l8j83Dn2PkVTjZgj5dRGE/kntlNtJR6eRoMyOXOM2rb3lb/DUT0fWDvLvb
DmZhDKoY91iu3Aeg3adxuBk2qQ6ptTpVAduQ9vZ2wUM0naKAK+kpXo2ue24uNoKBqACpckMQDHHA
5FpNcMpC7x6crBoEbwbdgzS4twuHuU0K4MNKkbvKXLQO8r2dhJIFz7Y6OIQUYPGsyLjD+iiKHLUl
tQ8Yl03MWw3rQfToK6HNSUzyFBto/q0OIs8AwVO/Y++bLrjWo2FRPsnfrnmgpkxjWetVMEgooHeY
gwuVlOpSki0BErNn3jQLOX6NDUbU+idv3nn/cW1/haHjDGHDpJWUq9XBk1eDjG+4hveL36fOBRKt
HBduZHMU76xdz8X9T9ne0DyeACbHURRUrGw7Gxu2EHdy5fAG4OLeg4ZEuTOORR7TwdeRonDe7nuQ
UJYuhJFkFVYGDlr4JD1zToUyDCiQTz+vnmr5oJHlUQIZMPC9vtBANcJrSQFo64GpcoS6mqNJ4H7w
dRawXzHwDf7t0ly0R7oQav6ohz881nugdTDshADOVAenArbmCiMFRg+H6oNkVI6ivD5uO2c+PW6b
5WwG7hM5vgyoD8IbfTxBGIEZ+LyjZijaEMLsmyaLi2q5CI0a+/y3bq+aB1+E45Srhbnw+FA2TjIu
rnPInK2/u3UY3rN4wVNvmN/kewGiMGO67kvJVGitlZ8IHPxF+Dz9Ftg4iyILo1Sv46TLLeBObEYw
XzIRETD++zU0fpbr8d/kIAcauEQDHef82x3412n9NwjRbX4WCj77GtHF6PfAI8DRgUmED8i0XASG
j87hRVmxDGMGIT9N1KTHzdwOsc/uZb2Y1l7SFlAEwuLS6Tb1VMGRfkNq+w1x74T03ZXZHiNmRLLQ
NilbgLJJDaNrTWP7lghWyaMFOPVemIhwAI+Vgt3EroqpUQwihWB1gs7zs1UXRmaukrIcEzA6hIqp
dMr8qU3Gt3cONiS216NVJ54jEeMD5XtwPSIJPYZ50CCjUKxcAks/pad7i40kvNia6PULiaIAhhn7
WsBFmr8ipVnnuTaeNPP5Xt9witicdUSDLKV0mDZkEnkIOElAXPs0hIkRK5sQuoiZl9JBkaJvyi2E
HH/ZMytwUJL7FmqL37ygdP4tBIpD6VcLXU1yM30po6LrsDg7P01JrcPb32uXW8T8MqIkZxnDY1g1
rW6stOeGFrbWVOIRmCKse60zXz59WEwa1708H3hyL7eV9qYjFBv5cZe1aqLF2i36+Z91qExviTZn
j97AY1/sdiZqxNRwO/I4Qybga/11/UG0p6fcCkpCQlAxBvt9ZVdbaIVYLsA/vin0cy7yXczvPki6
dKxobA59R0dMZKfZhiu50oJS8Z9vWol9DEsNoIXgYaKMo8aeBAfN6n0FubMntKrnLug+6lEQAH8W
CFAZgbIWYsyE+rgqCBKjFTFTsZTkT++Hy8ylYpC9d/73N8jC3zggMUvruok4A7eaubAGzs4SLa7a
82TfO8O2ERmI8KVbdQDp8CTq+E+LUYF9Ii/ut02LmjSAu+hbfcSximaxxFIY8qd9nfyf5BldIi7v
fzCpt+Yj6CjxS/hOHSz/s/uxbEDXR0IY4W3JprE6lRCM1yg59o1mBjmMjBQgqrMXhVijYYtN9jRJ
NhXiolKikaHExziWz0ap7ifGBfA870I7c8GQ84iPg8faxGIhIe1FAC9SF/CoqDTh/SAwFVrvx+8K
yBacGqTAyA4vvkKF91mJVqRTb17V+3Boh+ge2BhmfHKQ61jBcGvdgNCBgBnwYnQ77QC9dRb5uPID
2cOF/7EWvW6Ak80VBBq3WHOkPfNeItb65UugryDSL8yFnFEyXl/Pir96i1BvRpuKg13vy+mGH5KI
Wx48KHMbpHF8wEUVPp7zlPaoc9pCPzpB/1Yjp5yMBBo1y1O6w5ut1sjzCAna1XOVLGTR5MPAQcag
80DtVs1be0ppkSzQ1LeUmY/VkXf3+EhcgzHUUpQ8Zl7itHXrONoT1ofbHTZrcmRYd6lbRYpzPvpN
g071sXOTkopcvyObtPQMXTpLxn2UVOma6c1CUoIRP/WcZjcLd4RLE+CgCoP1NNMb7hD1fN8faMvK
KoVs3bQ0TUYgMYe+ZnvZcR3qeMCiTVcxIoC5fuInb2hCJTkAyavNvxL8w7JT/LFhmgJxOUDhQ4Wp
sZzhqZFvk+n+vfIljnvKI/L8+8Cr2zpxh/b4/nI5GQsS07h+3LepQpmQm/yE0LnF88UvmO7IXHAd
IuP2e/Wfwgs9SOykLc/ESaXYWrPx7EigcF5as/Qb6R+gdy8XLUrnRJ09AuB/5Sl6FNaY2YtGlfRi
4Kv5+9YkkAE0Lx9h42kK7xN+bdecxwQwUML67clYxZ4pKP57ZMjoKstGURbmshgKaIW3uq6Ol2mo
6jBn000sjgj9cpb4Um/KInjd4eEhGaeVKQRG7Hcu+RGslkNtNzmnQJ5F1u6BI2pmZ37Q1FETSxD4
5Apl1G6OvT+PTEEqyC0w7cenLXmYHeRms/A+feBuOr7yuMzbJSuUiOgMl9tegBDOpQyFkd9fqQBj
m0NgGLIh0zm13GAY/IWBtNfW6urjnYhn1Dgz/CJUdXFoqG+LdQFAPuyhoIyTlyHyTAL7tKfx8Mek
jn1Z73MlcKeo8C/XDDF6P6obhkEuCF/i46g8fMpfnUV5jTvIfDIDz44jkzoZiXu3AZgIi68XlPeX
PJ3KhRQGie1rjl4RCSNT7Vix9HX67e2y1pFzqefHID9p4jwcSSB4V1Gbgm0X0uGWpmaV4o2xDIwp
OPCY63Rz/Z1Y6aZJk8yM/OOPLoqarZnP9FlMXsOzbBbn80Gpt1vf9856HFFlN5he0g6UvSPg/Jnc
7KcMPuuUFwXPMzzPsYiYpKDNhDHE5gpLdK3QuJmGGWdRbIZkWbjVxvhGLZqU6sFdnYfk7Vkiq7GP
LmL2v/NONyLZnFFA1uWdAcj9fquDKC7xtz3SZHtDbaOq0AwGuwOyTJEXqAkRQKZuP3XlGoGJ8Lay
B0mmvc0/Ugz75ck9rxQV/LmGY0XX6/bReposqoZOtbrZVjuOAz093s0AMF2LvG8aANPZyTxI20G2
kCIpXaBpi6F9vJvCYNJlUMKVLpfCujDJHYkIDYX+vt7qHMufX5s7o8hK8kI4ixag8wDGxRRyqZ2k
AqbahWFaBKKQqJhWngNlB567jUB33Sex4k1XEL5drBWP+A==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
