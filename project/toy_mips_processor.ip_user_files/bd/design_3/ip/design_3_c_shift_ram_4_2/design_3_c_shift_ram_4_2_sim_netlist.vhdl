-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_4_2 -prefix
--               design_3_c_shift_ram_4_2_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
jWNy9xGeaefRgtm/caGHr/e2/LXGE2d120utHm5jhLxzhpwGtfNkGpvCU0NBXLcuAYWmpwYVp6QU
oAqvrbWHxgg4/eqM2riCijqYTjgHICfoU/mCybzkZMaVPfEKbu8IRitFFEmvqvc8oU/cBo2tGKXb
/uc9q9yyTWNQ5qy1lbdFUA3+zMAYH4PaLgFmPP5yFGSZ2s1T0aKLQ6RC78Pqjo9yb6yTE0Zu/KlG
BsjYJIsGZ0GsDPcumug0SZ6hkpSrd1RRGnnv3VhFSoZbjhIRslUuPKbR+oLicUaFROiH95HWhB/2
NItkuwKgE4dS6Llc97W6aO3zX83U2YawJVQ0VUNYnIbZ7MzikWcp+AUGOmaufKO/KocshnuIn0ka
UcrFvM2naFEfKSfd251w1TZp1/YAXXRMUE5E1FYXCqtHwFDaHMc3O9a0rsAE8y8VURImIDg4sFwy
bhUtFimXhtYDY7Nx8i/80YiJDvkMeHb8DwuFVD0rLAXt5+NqylphpGvllOhCOTuKT9BX4NNyaGhv
2cMxqvdL/8wOhuzTEg+kFBlmUVChgBOSkQct7EHW3BhBLH35z1zKdn27/nw9/9roBhHiX1kTFu2Y
XLaPCxNfhssMacsL2WS69hlMO4TDpT6M5REoNfXPZw8r+pOI1Phhpvys23so9IQqyubC9Y7+Ugsl
Iu1NA2qZhRFh5P/ttoHnUXfCKNW2glSTRO+cR3pPaTurUQDarUaLvqc0vm64GKlfPlUoslgF8XMx
y0af31MuvuFvunQQVPtu0hbQ1qO39a2rZOo7USi0i0HMChSpXWHza4HhjExl4wzdcwtGm4a2j/Q4
1+KQd+5PHGpoNVflaTCshPfJ52im+kp8rdofhLBcboNhUTAo5PP/b3g4PgBgYVWN9SlPKdDexUeV
yXZPMyQumgmXW/9mFDwIO1G7lhooo9IP8jieNnfXc7ApCe5UdR0MN0NTVH+qcG5MG5hAHrkUNJE6
FKyO4Kf6PWIM2NKTAcMepMD498T/CyOeY/+PlVGafJDBzDSzdkaDHC0sDE6pVKMHETNNxRbxjfDt
uTVb7AJp5OFLOc+t7KrO8ygYBo0z6saGOIfB7o4eTbPkN9G5MR+fBHump1i6ggcOs9sAjzwebWei
UgXfENueE09w8c2y67ac0R4ZPeJ41h+gLUxJb5iK2VsI8uQLtKEizCidAA5DRXbYApHl+ESdsqkz
zfgGYXz373YTR+YTJUiiczHcROlmLurR4S99aXUVXsVDCmUrWDEKXzPB8/bakvHnhKOQAhL0qnsq
tPKC07clkTCb9hT6xmCkpdGOEueVl4Wd85cPiqHaLn9iaH0joH74erKG35KGFrwBavawufu0AKLh
gRZCGI7wblLOzQMqIlhTtc5hId+ulkPAuwVQkpYKuo0NofTA9Olodt/Jl6wT/d1qwzz7zswiP2jJ
wHENNNZ0t9McOSilhU186/hUXF3WqjrmW8j3Jbqtm+EAgYD4FBCGmkiztkYlO+X+XJ68JeLIFysl
L5TRCfEsCFqO8JGMDr7ebpZ8gLXrNQp2YsITavFbwRncTGXzBfY5rWq7rfzdUpfjrSkQQ23ErGZy
FI+tZYOttDyR0d9Ug5WtgrAkTujzSS8wLqJs6jYOQJa2MStGGiJx1ksqrCkot9gw7IZVpXsXru2z
jbpGQ5kdPZcZaHINPrBZ36D5L+tqMvbwSEXKoaz6/1cQl47SU5JsdRa8fIwK1ZqlOXYxfS4Ek4mX
QJAvL7dXeVERjs8Y6F0hpGyEODhzU2GAgqC/IZfYVkWkCLUUyIxYSHxj6bZBz0Q53NRgANWRZ2eQ
jH0A3ykdFCdIZZuNsvZegnMXI5ahKkDx3sbU/aGYnEn5YPRN88H0wlqzmd27YXAlMUVqIVQ464Bt
TsLJU0u/hez+jk6VMMGh5f29+QbOG0gBlDL+Th4IlBVT1g3wIl01TogBJ8M0J4K8WQOvmTNErDPy
ZEO45YQCY/r/JriFFE+mEyNiicUQKzOH0994ElTl1HIkolSL6TCtQ4c3vyae1rE6RFRvw2affjlv
Q2VPM7WpMqNUnN7TE3IIhCdNdXmr5/UqTdyrFmlMhFP3TvrtWpZcMkYpGF0B2WUpeULhckykD+p4
6umDXG/r9QXh7wmjzb+JEHOYwBCUXiyRAdxSNRKeyxrn4jHcs+WqAAGA+FwZfqvnq8Ws9BNSFgsn
zygchf6oya7VHpdgcddRfMirwptYf+jJr7zn+18pUXOEqYJxJkc3+5/2tHFh7cbzsFgdvF2dUcPj
9nvlwG48w8s4Og+703hS48ehGj0iAeRaUei7WtcNUvPJLHncmknsXd80zXvSGi+Rsnc4ZA5io2Df
L8w+MJ3L8zXdjtJRcz/26yUsP/IWQxoztkbI0VBo4Jv1OgLj5yFfMIoVUOGNxU3QlkesMj9gwXcV
QnV2NKE8g9+ykXbVpt2I0Cc8N/OORBY9X2atchqMKphE9hn3AK5Lx6jxKREQSFs8mRmjyr9P/3KE
oVSFGXoVqW7X2Z2xqNyg+xWJDVWl13zPERBN9/2bowIBQ2JOjBMfzjENTQJpo+Y+X8r7tRYh1xrJ
+ZgebDjVJvrSLKH7cdj1TPNb6FXesv/hIuXGP4h6PnZNFSjybT5S12IfAnZfhfSc7RAZJGXsbxKH
jM5W5NWNrINCHD+22Zu/gUFFpwjuUpt0kOgKuhKQtwx+VOeRMzQpUYsj9HSz2FO0bzJXHz5u4fpt
xIPrwmZOXNcL/9G9QMvfxAM+Ec+/n8vfxtWEksCdgwdcAQ5/ogLZ47p1pwd96ONHLv8vlFusmmZA
kXCfIDyxIbaQh9bjaeAepJt4ntaThqKfHrHga0Le1lFWK19yS4PruHeJQf+gEJRkYQNMszIEj2bL
IXMBMalX8NhxkXJhHlcjjjQtu+ddgwgCU59FUrKpeb9oNhRr06h1RXfKpiQkkjZIomSJV+Yubn5V
R+g2qKhhn4FuJDpzXbsGd1POq8OF9QcGzmN8Pxk/n2W7Uby6RVsbeyyb2N3KXGnR4Cn/I1LIhQ2K
CkQBMH4RQiZcAKN7X0JG61GferX8hXRoBQHys48SaHSzMXCddjdIHaLk5ZeyDwxuAOEK1HWm/zPo
c7W2tTCaKkVhCF3olmLvlJpCrZDiVCyVfXhkBgytiZ43vMde/76E9clUVN/AkOp6u0qZNUwgGlDQ
Rh83ae7/ZIAd+s3qqPKWwiHLnAun9uK7mNUHrNFAIzhKqWoy9FdhFprm3X2wlFqETdQsomZ/VNsP
ys9o705Hgw7D4kEMqGRNXWbguP/lyYtu6uNmkm4OEOr/ZA3oRPtQ36Q5JU0o5kVXPaDnm0g/SvFp
XLDgrJj/BQqzcdEOcJoY7NPJqoHBRIp2uBCP6d/78AXYNWC8LzG9n7lh8qXOWRR9wqmRp3uZAG5M
QmeuCU09+RyRZKewXLBV6NyYbkD8QHduIfCnYB2r9tHoiV0NLWdWqdmMiK4Um96J8MNv7sj2UOrW
4+jj57dOmZFxf2mkOSEFAU7JTofWiBF3hnVcchI6qq3kkVcqTJhVHdOco9dPPL6EsWsD/DxywouX
uBXbLy95Qh/NG4gRWAnGApLVYIklN/0BeqUGVkfLv+mruJroyaIgEnss2PVkdmPAJoU3rAPf2Gii
mYIagYlRHU5pS+NFqLHvMHcpvDsTUIl3a07yYVXEdy2EVvkLTep/oewvbKIsbablnCX6xgzhrJUH
924MPfiaLBGLla2lyX34AkjXsY99KUKSiciwOsX+ZTu775w9VwosYHvrztkJW5u4+rUbpRygzs28
osCxk6Gk9RjzTsBWle2mQnfy3Yn1a2Xn06uhd/2+pghna39ZA1FCEhMTtJJMY7WuBP0kZPhX9pAF
4D75p3R975fvJtJ4vyZqw28p9NI+JKTiabwcZzLGKbgBtexWNC2hyMPlswD6z5rmklYXS1Dq1N5c
xTO9IzJZxxru1tPKe5zd/gTm72tjGWk7GI1liSy0+X+boYm/tbPd8UXSGcB5DvviduLMB3WVLlKI
Vtbx8kTvZ0mvUoFmPxwEeMwMVWRgmGLWt6L9y2P6bmEGtUJ9p9YD0TEBo+OEhngu3cdalawZgvfG
ESAAwcXODjjd1ne5GzmVplHjIcD3mceYvwz/GywJ7NXshKBw35dLZpFGuymHohM/Mtf8Qf8JbfqC
or6InuzRR18TFDBWOD6JLdsSzkbVTwb21yIE7UspCrgh7iRQNlDAdAAdmvWfnJPASryVDXjd/+DP
sRVqmFqVsk5bNt/Vv/SnqZEgvncfzRs7Tng059o4N48BLdAo9iMkwcAMSkayWoPum8CHh6ePxIPt
gsCkTCjLlHrW3dVK69Q8AkQ3UP+ghCQauoXS59Ctnsn40EjskRw2a6hjU+VyJ1nkxrX7ssH8Z5rg
fXolPVYKFOlDktiInjHFvv913T+U0uftFDkvcBJzaoEjdvHhXGPGLYLcEAYPgfH/g20oUFWx7vtY
zLkqZl+IBlwZWtbfXMKN4bURN2hUZePbjcrL2RgY+ItyEZT5M8KRD2nZxk+eBdO74R36meiLW6JJ
nSzzUrS80RhY2cSgX1EzYL8ivhAGmekhXSCfDkfF2aee3hcvjVJetII8TM1X1s4yZKncFEZblBNJ
5frPjrt/hAJoNecpQ2ztG3skLADfhpM6CX+uTmHBGmbCMezdf/VnjxcF1k1Zodc08sK8t77vCkgx
JpV/rtRfJ7ykSOPi/I7r+7m3YaCv2YAZavcdCepc9nxfNSosp34ISvh7GUEHYLX49n/O22dc3xNb
/LF41ET7UxwzD1bcGiGPBaLApLKb2mH9GHqYuAgqlsMyswaE+5y89A/iCzUJ346IcMqzDS6/lRNN
/GuIrC8LxjfunvBQE8SqDpvT91PeUar+oBzu57QeLODONQWN/HhWuz3u38kcLSlDpvkWey6qFdnV
6Pk5Av9NV/Q/KPIHzlle4XIFnpWQBzKkh6JIfSXosgtq3fxu5HGtwwW5u7/xBg07tz7oGdWdH/LK
LmTsOi93oludDy6AZ/CIdQeniTBYUQo6yKdovkMx2SmcQCmhQGzcC2WoAWj5KJcCWcgD6WmOdgrk
OyCIzl+Sm5kR98VNZ9GJxTGZN5b+Vs6mw4L+37hNDUG0ed76mEZEzdhdARNOY/S4Hl3MeOaKKsGR
4f5m/rVD7IGfyoanSUqb6udJhiETiwnrLTSyn1aPPTHpfvxP8qhRdASie0V8LnAQ+i3a+tBa3gVj
ZTgga5+a1gikmwPJUIxn+HSh3pIf5j3azIsRNGBzXT3duOPrvUYXWrpe2jygg52IO7Ruy9SI4Gre
Z8VPCDv4ggYMgn0hWxJnn2oe87JV6+GNyuTVjKdoPGb33BmaMXZ7B1d41E/9qS4TCVATBPRkhzoB
0agiBjKMDP8zvSNvi62VUfHSO3YQWT/rT1XgTNaviRt/RaVgrNPwSc0AVVgKDjBq69t6Cm1zXl4E
FGx4jiTWXAzfQLz1+vcDkj9VZKpggzwTDvTEffDqGm7UUYaT1EZsbL1XrzK2zh4OFnIqMbppHg7k
yYVX923iXln1x7vub63f9WkmHP4azXugEFzuZLcyjVNUQYBvRncaeJzfYom7qNVME28JcPKgeRc5
0sEEA1rTc8TTCXCLjDpDj1DQIMulqiu5iB1LDbo1p0AIrN17baTWtNsZDiX1YCG1+nQNVTt1Sh9F
KEGWYKGyA6sG3XOI71D3mx5N513K9lRNgPJ9rQVV00WakVQjNJjTXTbA+hwe8Vsiwrs7H5OSLG+Q
W6+86kjfrSZINt7Ia1E4dzwGbTkhYU1lQ2bslF5jNJJC9PXlqtEsl31ePuR0I2EVL0zoGb6IrNzC
ShNfU4bYwlnwg9cAHi6dWaxlgIVqvmsA2nt1hgCT2ersGTVZZkpsk/PqU55/g8e45/9yVSY18JTR
FbyrMDboxtoTpe9jYLMMhU0FtQSi7gRI49szNJW4KqZUw8W982hSjuL/FYuiHMX0WhxOMWRuMw/n
lWgO3KeZup4/ILpQ6/UthpuC18kUBjjHgy3wwvmWvtSOykFUP7fA0raJJfQSi+GtJP9vDo28pKAC
wUMLJJNjr9tBJd7qkgOPeS+R2YpJ4BhU3QprDPzjs1avZbbntG0swizVfB+dnsv9szC8LNCSRCF/
b1ogyKXCPQEBPpEFHKfqgx6JhEPwTerFxa/FT+V1esvnLo1ZMvSXbywQsxie7SAto5naxLcHMz/c
T+iglFmJFsP6taEZlDp7E5pklsFjvGysMCU4tzfBDIWPDCfZMbUHN6575DJPl728DzqhADgQkOqP
b0ZrZ9Lwl/MHeKHKrkiRSVyROFk7TR5zhW/GfAMQFk4MufJDLaLkgkrYTZMTEJtTT2R6qI29w4VZ
6JQlvfC4jYRfHISQKvHkdNHZs9qpRy7BG6Vk10u+3t05vf7PAm2xxYja3SyITgf15ojqFxG3+ytA
Kw1DeLaalYw1nOLkVtj50hStl5wnTZzfLJmmmekGbFBUY78fGT1sAcqkopO/H8HOMLVtRRoIRIgT
z8dG1zjO3S9OrJNJ6mqK8WULhGnmfRG8V7tDsvfyDIksZkj3rn0vWQ47wy+5FfOzOprDuEWPxyLj
+8Sv5162hwszI9y/dMWmwf9ulucFIeOMrqHCduvA61ccc86I9I6UOKiJeCRN1zT71yBxHnJ4bGhK
sUC3eB+bo1BGxGVK6++bU8tdRhu65nXgoPjavueIgUfjlBhtqvz0svCPXzYoJNFbqVzxwrF4sabN
G1NeKL6/Qknlh8oYzzJg3ltsNeEJTW16NszvY4LIGH3sArznMnwlI5OH5xwC6ABuQFQ9LXPdppdv
7B8b1x/r+YApxxJyAmReJEo7BBwWtC14Q0MDLpCYFq9ntwagcQSV9t/h5ho+UIE2Th1Ab5FXLf+/
fg3oDj0dxEz06rY5vB/kqjQVcn8v1bjOi5Glv6/CNZh69KS2vQWKTFoMN/TCSkNQYv5DAATSu+Lv
pWlnz8HlRuTcgPAeTIZ4suRcOimOgrPBXw01enBSaxXOLY4TNy6ZswdTv/i2ZFyAC3dMhewJX28f
RBmFzrXvYrtRZGgJUNOoyPwgY9jytUENvrWqXdp3KSGfmDu3Rtl20CVz9eHTI1Iaysn1D0d3iLd8
JlZKB4E59Cn3Ly8r6VPy6O8TegbviOXDv/yLEPpIrt/9mxtQmyQOHYvduy/yUn+R3rN1/dW9diRt
9K9sjj1j79fiitif72IBDBoRjfgK4T9LAC1ybOZzLxH42Hn/0VVUTimveBh0WPaUola+DR/VMHlG
RFf7Y7MlQ5Wa3P9IEswKxAJcygJ+0cVHvLKdz3VI6lPMitaioYhy5eUlwGCKOx+XC025zMibu9zH
vUZkM7TXjZCigyCfboHd25sjCCky+/VHCSR2MP6PqDpNj0x+EAwY29hGr9Ygf4SdHA6Phl8x3/bE
nD8uFFrnRa1WRFAR9YRnms5Lg5ILlcqMKdegT+EsnY68Tc5yuLEdH0pJ7Vu9x7+B6vRVHwrEf0Si
QIfKfN9Mrs9PVScymHsaF9WNoqvmU1xNkMPTS3yAb31CU2jJzEkI+tjEIqY4tL3MavK1QERCrte1
Ct7HOqPHAxY23+MO8JlzmwNtB5ENtmC7SrIiBX7jtWwRedz73FBtkD3W5xfQV3LCtpDZW34W+aXn
H35NMEjFESOrQDWMlYrWDYHvtScEs87FjtfdfOaaYkOMDNcgZXSgDNvfn4Fl1//HMHN/tc+6eEKv
pcnNKvRzVZ6emI3ShYmJ4bRxln8SJ4HYx3xA+FgGiTZTlbJ+ye9HFPQQdV6ojAtnhwFpI+7Z+iSZ
3pJpZqLVpiPHqe7WWN3ZTHFhki11bA3OKVRUXsA9QpoTymOnKq1z60iIwwM4Bz1yCiNItKVI71sh
3Hd3VvDaS2v5yn5TUdwKEeQ5a5SzqK+JS73SBST2VGlaiyECy9VDVQ2MxbE7k1QAxcx8S3nBtD4y
jLHpwYhicWMADZ6obNjuYeJpXRuEyT/Vx413dEJ8FKMGGsWCcVROi8gE8F+5Fx5XeTwWJ5a2DxtN
JKHaHdBxySMqF8s8FvfzhPM7WKrD9O3UntKgf+DTJWc7VThQwO0ZdT+1U55qJhzvs1opeEBCI6Jv
q/aUT5KFNVewp7G1zyAg/CRyRPXyX1MImB2CA7/DLzhDLoc0LxfL5cp+6fWuvkZT6lCLa4PdF5Pj
uFWGonx4law9VxQ72S8bTJTuILdvUUetfQMAKChZanrqMjR7hf9dcjHiFJnXm/fSb3SZNds3sGvT
KAarzurrfIrcTTIYvhuxF9xKR75SVnDuPUnlTe9MrQdjZUBYB2nJXw6FtPFht1v71F3NTJqTsLix
TY8ifMZw9Mo7XEzgFpFlexZvL1t3LodCBHS6lQioLd//FsguNzq0a3XzxsJNh16xHkteFcIlOZYG
E6FMOxGQjH+UpKDcjOKBxvXH7MJ7T7BU/O4NlKPiH0f9AMGdxnZ5yMqAUCv/XFXh/R6kj8VT1O9I
q2YbSwLj7Z++IWHzkXnUj831eqNNNba/bLfy4SvA5fkKiDUzPgUaa15HL4LZTYneNihByNZbezJ9
az5fpYHFAPF4yNkODxdJyWhg7NsBBAPnxw9Bevu+jfFSf/QksN7X1qyuAb1/teKfh9HneT5g7UOx
c1SfuYUvGSgQuS/QdW57s7cTlm41ZdEwDO9YTeAbgmU1+gZyOL1NFICr4N3kjJnGAIvUx/giRcPa
efZUDPHasMYvu1iteVtlQ08odxufeLdXbxgyD5WMDxxUge4UCBrGBTCUp3vHuvWQiUr01S4wyiy0
UAWEXM0bAqGjRS3J0niW4LpmV4UJB91w2xpbA4OBznG0muJrbew2osmJ9NaDyUKqZKll96fzmju1
DOMvszoGm++5JZ8zlQ4IJV+/45im160UdIGcKDenwfxSlCo3PaDU4qbanm0lO0PTYooZBxE5d0KG
FlIvQZ5K709ueZEtACQYHsR4FmZVfsL6FMi+DvA17roz4/iPDWZz1kyvME1Vh7dhVwKGe+m9K8T3
VwAFtWRnRFdWKYQL1KG73ejYm2nS/n/PCFif7X3so0smAEB3esioWHrniavTNPBp7Hu1Qhq/av+r
l6mxGqlsdflCLd6kMXk8dVToGMVUPcMmldT/9nOa+1jfc7nN8oFUvMpmpWfQ1eAbbWS8aMhvW/AQ
38fuBjxVguDSAAR/1gV+Fe5GasJk6yqXJJGVUdjQ8SZ94zXhR4dj984ZaAS+EdfoCchrPbLyOXMf
VH8YrkVbxCHgb1ZBIHHUijfQeb/lupG7wXHWIXvELjAavDof7OXKlZ2D+ReQbg1D1ONnFgyA59b+
IWzkH2MJoOvEtLRHx3KqNJHnWSBlF83Cxa1Gg6UyNIb0nFtEl9fSyWbAO3m+0/tV+ddL2I9Rf5C2
gEmy8YW3PkqKVRkAIih4VxIz+s6Gwh0m74iKZ4YiT4AcwU//4Rk0qH1c57JfpPr+5jFoTXAdz209
y/hUDNPblxYiF1vBbD0IzNQD0kzFrvgRDfavzZzfS40IexpcO9WVTAUPVUxrwu4p9ZFDu3RuoheT
bA1+QgA5cxd7R0Dpc5ObV0Jy2komf9rAT1xpPGLDdKu7XOp7yT+g4J0ksNVnfJs4K+mL5Ur/+bI9
wFdehbhkz0TbRS1ziIPM8aeXL6RwjYwwbXimavwKWALlPHGbAqJnNxt3HYDTgyr1tn/hPn9oE048
38U6p8ypWhUtVQ3+FXBz5FWUM6aFixG97pzGcynRiEKD0EnnLUBcGnm3Hv8qjw3W7LLoL6itv9pV
2P9dfBbSWSvaO+Jj1abXtY0rcGvikhp5PicgzSCPH4S6jCFbWXLOO8jLwxDvYvgnxlPE2UMxOpxL
xm6cM0+woQBPvvWC4DDwa73NHUbo29KI95BAdWaBLEelBTMthsO6Mo8gvSZyO0AHbH/pA2E1qRdX
fHP6fyA02aybWlYS+fCPDKk2NalLUBijTmfYVY02MSvFETou6rMStn8HOPSTpc8K7rTb6DFIuGUf
Xyw+ys0AKU1ewxcoFJQcgvvIYhXBjKHBJcmBEWu+phBM07lrgCUQ4Smvx8lOY6CpDR+0PtwcoZ/I
xFO5OEbRYk8d6VCCbz5Ycb2pAm3MCITcCqDratc1dtx7cYqGf+2x4sbQCTCLX3kwc9u6HsbS3CPB
tvqUqT8yzmy0kPSp5BP09mve7LaCakTqhsTsolKg5b5aimvFkI+EayOtUheKN6ksJ1yOaY97kPDu
uBX2VjQwMCniVSu0hEmFGtqBy3TaIVfoG2wP7GGIxqybnwRE6f+c94wQ/ZYljXGEgGbXnYUdyvAB
n4nimI6MPFH8B1tQFQhfSR93fxrtaBi8OOQDQtT0icThFicF4Blb0JH7rF+rOVpF6UBTt2rWYSF1
IDHOa6EgYo7MM3QsRSGnHaTwhSVF7VL3JX5RrfgBPB/uLFj4erb5UK2/nDULNYXoPlbZknQCjeIu
IPbfSjqZaCHD9kDgyAKexM8gsdxgB+Tt8NEdoG7EN7qHU8EV4GvWMtAZTHXMXh6mmjhe1yCRhAW7
wUH+u8xL6/xxyflLTHWZZQDa7dO3vV+XUCOIsTY+SE67/HItGuOS24wZUGD9gFX/QRpQhXHOtSXU
gKTJcwV55yjQXwGgCiGX829BuKZVRdwONUq4fyWD5uZLJaDCByVIh9s1WN6lnusSMfQN6khjALfA
m4hpIhrMYF/G3KXrW0vKex5kneamK+A8s+KxYhCMvGsutjaNr3qCPI+C6JZ6bVoxmq+k8Xa+5X14
yX7QsNqABza1fJ6V7s68EqSWMfxWGmwsIxiHE9jiU2FdEjUSBasmLS/8wuiFKHwleoc3HwFOHvsS
kDDiicEm1lX7dbhfTXITbuWdo3I65B/8+qAssH5UtDGExaCk0ss675BfzaM+JaHoEjYUwCi8eDuM
kOlM6We60OIqy5BQI8AFzTOlm7fifaoPUiVoPE+0++8f+XUyNSxkZSmh1Cqd7qW2us9rc67HVW5O
hUt+RXgyYY/eNaga9UPiTouHv4d39O/fXWJXY40vX9a0Vpswa5wtX3sWX5uDVFbUfSXpVoUhI/S5
xRzx26vpelbbVThGh81OOPhiZxoX+XQBOCR748Qh62X1ak+qWY9WtuU3XtYCs2GlQPZNetshOkXC
0aFq5SgqfVXfsWD6CQwpNOkLz3MfkUCR9QGNVIejRBAeZqzIivIfMQPKDxtQw5cNG+oPZUrBjcD1
0gxNStNWlOu078nziEXL8hGvAfaiWyNdtJAFSyi1FAtTUffTKlsqfPDuGdIACitrjCQH6BuAPJgT
B2G/tuogpPc7th0iqbizoTRxe39zXMpW0R1yXy+O8DNB2bBo/Zea2m1o+q9jAlGHIh+EzO6byBgC
7V1Zo6YyIgKNorjlzYVDLDNn9Uewhs3n4r/xHdxvtjHRxHjphU0NJ/JUJmZ3TCFG03X6w0wz6DA/
F/MvXsHntSjHfTyWVgm8wXR9bQ411+pkE6obglp2sXzHVJS3qNybzwp0ou2KGctiXTU7d5sk6i1q
K3Gagt9UTbloRsOy9JpQEna5Tp4AkKKL53wqsFSrWC8u/CC8QuO1GEWh0Iblf8CMWDOkseH83fm7
fvicMOnWZ1ieoiI9UcI6PQQ49Q09p29hGcxKjmFr1vu0YLEUphQcA61vvKL65sH0PuAV67lf04d4
1l7l3uzXywSDIazuxJmgSHgJ2MQxaouaT48rmq2ryzPv8jZ0lVfpM1MgPLTTr0qN6X/TCLU7MWWK
IgNEaGWKms4C/Jv17Iy14hkxdjKD6Urt4E/XyfKdSZ7mH6HPAnOPwdXpFuFpu0Ju6dxTIDbD8zIJ
5PMAh15IcIwHpObwzK1kzwgsbmqLGcAliEqwgUpzlGL/h5ZlMWn6M4FYNjukUM46bpL4KexRSS6L
Mh7dnjvH+GiQ9CkTWcEbvEg1dG7/kfloMnMTNK/9Lq7HTKrGqn2PTm1mcK9f9JnX+HDdLpG4dlpm
CeXroGizhqNTO4ySmz5PbLxIhxxAtj92p28RwSWg1k5KQNuSAdV8CB5k3fBRo3TL9U/+PGOu9HVs
/4aSkqFO1gqpSBlDSEGParYGZwLVV8TV1uVcwmjbA9Gi5qgpl+CvTdVAZcKvaNScQteiy1osTqGj
81ZxViFnlbfzfKXf1tQVUKPyH9wxOBZQ5mwPepJbUSm/9NKX8iztKnd8m6bTbDMMA4gmyufmey42
DJRA/3hJzu/fFZU4HIxE1AUdL31ZYZjyROU3uM/nF9zcMCwAsbAdC7fo9BXRUZCJaMLc+5WZ9KU+
ukx18HXMPSqe7NyVMYfujHbqvVS5jd7/sl9wFOQbviCZKVN3D1hXxAql4xmNmDAaomdYzHoXVVYt
9XakQ2Ch7fNwsJWJnSHL8LjFotcXjbolY1675DfcjesyJpdkPYhvF9DI3dhR17s30EKXGBPBObTp
3K4rl5rKe3Tep2sUff92X8eijZkfgNsTX8JAV86Ch7eofJotV58kn+BweCtU86bWd+TSLqb4a3xu
z51cKY45tHlDQSZKkPhm1icY4lZUQXPBRat1u0NQEy8f8rEaFpxqAGlvVtwrixsVndVZz8qLp1sJ
OHF1JP22y3l2gWC7YCy9GA016lAnhu6903xY5ysIeMPj7Ac/SlqzB/5/FVwjm6m4UGQ8uin6yihI
C/9K9utC95tK2LdqltKpjP0/mHWkwoGdEthCHwMW8aFs/iIzf3U4OWt20XMof6eeIXKm8Ewp9yEE
CRg3Fk6bbKptITAHfNgMHqXVcbfIfZMdYNDn68TW4NO0d756Z/e3E2ov0SfYGN2bWB1HEzLaJ7eK
42zNIoPUDh+4pNH/npUEYvEsUC06M2RLHCqL61+BMrilhtEWasblaCMk2rarfcNziRqVYI8hb0hx
JOHMcnjn0WJKLxxSpzueFjAirsvV9mZ3pXDOcmllfKo/oDGO4w+zuWzIMw/V8+F38czAeTuZJw3e
t7IfRBNsz3xYKL+9sJCfYdwdiQFHBNr5jJmTWjGOheev0QIegyT5D26Vf9I7PlMOwdrx+MnmSN3C
yAeOwyZEeZtyVIpgEqR52QSjNHdDQyOqlRVaYqh96jT9dh1zP+imGdgH+1oKFfOQ83ytF188zHjg
v7S86vWpBCBOrKBDcyjJeLPi2tWjFbSbOUg3ylxNUQG+d0mOSRy42PN/fJl7Dn6+DLD1DasfGSa5
BU7VJ8U/TgxI0V1B8bZn++kr+AU+gvRxSKg7tkji3dHKEZvLn9vqohy9D+dQFAnc8n/kLO2LcicC
h9DhzKsWmN1INpMN/0WPhgIFcqqkRQGvw7miWbRiXuuB5rdJ/WytQ1TkvoP9cI7SqX8uLwrfhx3X
Au0Kemfnoib223h7qGLISRIz1W/wJu47Xgu7sd0MmR0SWhXap8lFEeHLgvuXPk8KGt8rOq68CtaV
0z69YKaXhNH3C2yA0m3QF0JEMsrxqBvJSuOvJ9/O8clHGzzMWJiCMAmzHhb1l7LD83Jxz+uI8xXI
Mi6caEPdLNRnUSDYcTmO544OTg72Z6N4UDb3yBJ9POpq4G1tiBlBQr/Gn7VL9XJCy/3x95+pAI/N
nYokH+GA0sXIvuP1rxjyirWltmgfrohFT3Iy9Nsd4MLnOfUe/YJ1FQhiJ3Ne4brtEXMXQwdYWlP+
HLnFeZnLhnmKu47Whfl7+K+PyXa/slaoW+it3Y2rIEgWnI9zDY2xXMf/+YphKM0Ik5PNO9uPTzG9
98jS/W45mAD5iO1yqabtYNyxgCRQ951/y7YkUlyC1EE/kD+T91/TF/GXwVqNxHtFysZS0kkav2ud
HeL8vyaPDUhu7Dx9SX1skMF1rP+AZwukrmgo61YHGA0AHrwWWpIDL6MpLozJsGR7LqWZ2bzaSb4Q
R+fErkKaRRHg6XuN8Fn14CjNeA9A+1CSfLTVzyrf989B1KeKeoXG37w5jnFWCEpSW2RJ9HA2qbTi
c0JV80SLMkU7WBtrVVazt5kIaxIEC9SCFc/qrYSiwmP+uwQZtPqowH0y7FoQUF2e1LUIyNBLVe0E
6/RCIwpfXC11EiElRd91OJOYWB6pdZHVpw50iBFS9YzR3uxsUdd8AifmkJeUhM7HnK09S7yCMCxO
VxRArQyf/j6fvQDe1wpxiNvor0kO/c6ViDAQ8totpE6EZOxzB9WrRcDMHMdnJuirNCycu8qEwE0I
IB9kpRoqy5g+CgQgbOh4QkHoXWXRdajhv6fjFLQV0QBmpy/cyK3OlEilxboPSjHpr9p0YTcYyf7W
w6WhjYwa+buIyHV3ff7u6TmvCDxHRAbCYNffPQlOO+VI/kER4KFKBACyte48y43mT/0mSXNUSl8k
rwNRCX9j8u8bIQZ6xBPmUV4i65dZyNYe0+7dNDK+RaOfs+FrcMBh9Z2u7V80iv+MrskECmz+k48l
x+QpkUDAQZVgXJ/013TFyzmwHaEeN8dfH5HQpylxi3MyLOLusQQFv6fvDOa11SzbuYXPM7yabs9Z
/q+AG/PwBbq4FK8FF8urJ3FSDcWGyFcisMv1XDcH0Ivs3H28ELkWhDFLyvz745UNfOsGWj3IVAdO
TWTRh/zfI5933jI6P6mTX63YyGjaRcvKZtomPj7KtYmTOlYDuiY+lbSMMVi1lxGcldDwQi1g+7qF
Vr9iV9wqGPkMuWaw/rn3ke/Rf7ZLapNffnkNUhNZux29nNGSoyWjYrBO2+KQ5+az8kyoQsQRMbSl
y8jbuxDWkT+XayYURv64L9tx3OKW9yRuWvWiiwUPLiU4fPGkHWF1/27KwP0qhSd770+YKDzgKZQG
5ZpgTjnTjga+jdLgGBhFtKIkhqFsn7OYEhe3l6uR2EaGo6REGDf5AwcdM1cL8UPqX5c8vuIpq3Ir
33pGL1S1Iriy1z31FYbdZhINnT2/fhCGWDeD8Mslbddf2dYFvW+0x02mbwrcHuHJdfacpkYnxCfd
b7EdRPgDwMXNr/vxgfayVDL4TpdyjruALlAU06L69IQCTHH5zoHR2bgNVLAHhcxEEqUk2HxA2xk0
BX9rcHe1TrpEvxdZNXKwLrEru+Qgs8CTf1DEf92Xd2DZ8xV5ymBaxLXKgalZ+YgqHGS7howWLQAd
3ACSG1jqgWOksSsVTUIBnVH7WmHGzdZaSCgCyELgXtIf7fTz/v79O4827XGPmaJBi7X9DC5L+JFa
e1T5f584zT6TgvZ1nZfELahFoGB3flF8lCqihyA5yHiEr3Az4NJsaGsAhKzitgnTAkX4n7rj8r1g
VfP8EL9bHR4h2opnQ6TpZu08nYHHPch0FtwgzcF3KEM5N/qOCbgSkHLCXRw0U43hJl2a98EtubBq
npjqsuInxs0I9Ee3op2XM5VyLF0mg1EIN1xZp4OU9i3EzJPJt/2wm6QKW5FnA74M2FVDGXWdjXHW
PbyGo5vJKl4q2TaVWQhBh7ii8YrLmIaWmyLhHB9q9vTtemYjFuH+V4L/musserCM6BMhF29lxfkr
hmD+8rIbCBNbK+G/fT/3YOgEjVdinxRZyqrJ6A7bvORKp0S2OV1PFca+JhiRz0vtqyoI1xxd6hWZ
TI6pZx5mfOfUkN9fQL7NovJg34/AztHhALnIB18oJ7YYv8huK8VFndNg/S+l/g1/4YcRW6HZQsOu
iHnka/tNDYNYAbgPVDc1SF6RROl0gZd1qov8n757GtxuBhoE4E0S5BE8YjPXwMoR567eqktKe6/p
LGZb5Dj6bJ6y4AyhIB3249nOPKB0w/QDDYwUiOuvZumTXxFlvkhbR/AxcgY6zbFSrsG+henynuql
AQJikZn73kUTR/BdJHV5E0hYWv7w6bJLWoRqXHlf0cBalbrwij6R9utKY8LcxyMJOjhIbtIkhWCq
dxUE8HRODYfWFidCuvqCHzIsx7aUwnZR0sCQBahqBQ4mOGoDygCfHFfOpzTp4/mvq9pi5uTsLN6W
JCwSE6DLTAZG1xovyP3nN8478CZAVC7cF4C02XjHpoOb12XrS9bFz8VBQh0Cfo5ZiS18tjjv5fGZ
3ZV5r7qEJe5cdKFPxiznirS+BGhtBNW601cZRp97/zBfZuvv5BDTy7ZbN/xbFjLR9XXEfrskHKns
4Z2ilwkvY9cyTCaEg99KM6khWJ5MoJDojVwiocaG2qak6NInqAxC40KEeeu9R3RFvtGxpCIPPrM1
VwbxhD5f3+EB+I0a7m5OV7B/L/KSkoNZsqch8cDAAvSNndfsHwO1x6Oakw3JZN23RfQ4lfFfgdbC
G1z9C1kQkeC2apqgszU4SsKPHjZZ6TH3QwfAbmFYH0aJnDlUF6F2SELsfXEOczNyt33LeGw39rJt
/wxAhYvw0FFoBqVal2SlHBGoSfA8+46C2Q/+yozmZgQpqA+8t4BT8BD66Q4krzEBapROi/CeA1Q8
gygMXtMeOHdpg+DMo6K2OegPWmPCs/9wO1qEjZaL1CymWp0J5Zn83o76SiO39dF39Ro3fQEMJ8aY
S/3G3gtDL8QOuzL/6aoIu//QWNyhagZ6x+OdC42monpfCnMOWiW4oX5jpERFdTozMtZiTuhr9s+h
sK0oTsICcui14GqUXt5V0kO5gye9e6dPni69V9IstJ5x2pHD1Amymm6/JGaVHBzk+FVcqaKaJYIV
/YxdeK6GMdB4KW4RTo8unV6Q8393H/Pn5mcRs7N0XzWQ9fFuYkUfnZ4RrfvVveQmknMN8K2Uqxcb
wOjSmwuFBuabVdr+bWvd1b4/6j1vYW20vneW6/t8v05Ty0L/bCjoQYqSQiz/AknpH2oLw9Njtx0q
HRIH7G8weUZpk4iKKC3EZa7WML+8ehl+hxckAT632ADrRoCF0putgZUqD0mxS9Jg8SrCZdvClarS
LSeKh/wMYkWkeKYl4CpUfySrHR5xWdH3hwSi71j1PgjxtAhaZkFiYMki5NmIJIu61yi1VoMnR06D
bZ4LTOSFEwNjt8U/ZYOYkXapAnvLTqFryZIEtQQcZMP0vfSPBSzcTkFIln//bAnBuuDmchu03P18
fgcauhsm7xKPE4dP8rMFWVc3hhVRb2wv24mNA6RbzpLDwi3i9WhCR+AOJFfr33wmLhqJTnJ1CwSE
SijuUXl1eUoQ2uqESHcB+3juuhmkarGcmaoqJlCyUlk5tQs2gCFsT+OWQugb/PxVG8VjQCE6wl+M
HSnSRQzNpkLA9SdsDNMlNy5cOnLuVQtcdvdGj73Asa+4yVWQBaAAR+90V1pJeTOY2sIZF1d01d6q
I/K+0HVc+PolQRQ4XFpXAiK24vT7XLDfYfTrQdUIrGgVJokjMTYg7QQJ2Wg/WkmQSIrGnIKuPPOG
cDFL5rFGctNrl0ZNbb/BakxbomySWISnauSGOpBukKidHncFGfD3qVKG0laSK/smst+OcGvpkY00
zNvvJU2zTHJEoqRFDEpOxXCGmEGn/+JF2o1lvY3Jsmqh+g7SPqXpztJpNK7rwx6waThFVS1IwUyE
wwe+/jpzrr9Vt0JXw8pWVheuV+WRx6DAuBvJR9JILLofn59Xzz43rZhG7Uxr5Xe10X57hSd3e1zl
gQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_4_2 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_4_2 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_4_2 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_4_2 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_4_2 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_4_2;

architecture STRUCTURE of design_3_c_shift_ram_4_2 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_4_2_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
