-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_7_0 -prefix
--               design_3_c_shift_ram_7_0_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
+k52ux1iDRrwzqO94muyW/i9tNULFn+VasUMOmjjDEgX7w/B4LnKnNMfqpxLRDmUGSXXkKOSCYaC
4ZDinY1UJOdpYoF708fUvHqWKM0BgOU112vL/jwYq9PMQKqhrT7P8uqVE1WuM5nt0YramheNOEWQ
tFVKnYc24EQOQwP9VCXnOysol+DlfHfZ3dZPmSxXtMwUejOjasr0rElk3o2aubquMeV5AS2yf5hh
VEOGb69FyffhvSsShrytbznkmd6BgM2b8ytWaSK5Zf8knnvTqaq+kQdr0ap018Ai3L8T0MAH6zft
WXNe36Ea2RaosQPWf/YUfw9YD5tnoB+APnY8DwsEOoxVKasPls3g2HSr9VMPKmGLbomDINRU/ZLp
SRJcuIJ/Y7042ZLdBp8UK54c0lAMddRFFpEtTkoSSgKcf30EK6mBEnXhDso/MvO24ablPD6q71++
aIcWI1YXfK53gZezBppKaR9tGMhNEqFE3J3xtmlIxBatC1U+ms9kvlAq2Qo02aGzpMyEuxEBcW52
mpYPsQIICr66LlIUT6KXnm+ROMOJp6LOKvWqtyvMheP4t4LXelJzIz+da+o4hgZ9KHD6OcscLzBn
REylS6Fzf8FXvsQFeQzAZEp1JudD43N6Ltv5CGeU3rBXOTg7TEeyYjZcWwizZsRbZmpj1EOPRI7q
Z22YOmTPqHx+ZZ4ffyxiDsHjEJDf22SIOJtKr61SSns2JG5EdZGlsqyIq4gxIwyjAvuVH1Xg9+oc
vwDmMt0LipKDQDKhPBuTXsXiLCwKPQscX5AOkg323KOh1PZxhUXOcnlakt053w5Ac/QMR4UaEdvA
/PequGhI+WH6HPuITsI3ZCTqSIwROeXhf6vAy3jiJNN/b0Mja8xaZpk65mbe3o4sDd0jakaP3cMD
5YBYlYdpQy1pWg7ERiirTeulaM3hq7pu98JAC01AVeo+3JcZZP5N//ZOpMXUxvrR+yZ1vHyOqm6S
Pt0DhgIsM5prh75rcbtC8Isj8jkiOuRfj9MV18Pt2p+IdTid0PD0COZkK/+GaRVwhn0BUeXUJ7ES
NjvJyBJoLIojebxfS7j0thrIuwJAUmsgpzBLaMMfnqPCAFHYrZxRSSJUex/wz09KRZ1T5ewOrtQo
re9lE9P0gM7QrDUAFmASKPTETPxS+GZAWywKbDp4no/NLn+Ts7b7PQnwdU9ZltWWgK1bUuzWonv1
EvR8EeCnI9PwIxhaKEWZ74POPmKWslpVoeZtGXZM2g/258oyM25Y9m+HGqxX3DCh0ko0dTCtogYU
9pBBP2FxkqTes+ens2Cu4iyr3WPNxwyQRH3YxqhnuHvpr4I4KRn26lCU+UuCODeikHFbna1cIX7P
I4PD2rqueMFwITG4/NnH2QshHLTg09Q+s9Vr0mr/qR5IChEelEgjLr4itFrrr8fKJSojIx6AGL0u
JSKQvOxQRMz41TT9PGaUL19UHwg/Nnsi4M0cqjpq5uDWLY6Liyvzmh32c2VTQEg6t5eUsktcVI9M
ozarsoUn2/GqtGhlWNuSz28gGxIhHDmfqUjpQma5Z+4Ux2Wo7FGR68bncWUOO9A4uoL1+YFuL2nI
uB2tF7hX2JC9Q13a05Nk0rVE/MkFr08f69oM+5XEp/vBXTkLhKWsTrHc/LziBJMwqiy28+W1s8IY
9d1chlmDbq0Sa95zToJ4YW1vk4I7GLcDAveRvAzKy/JiOhtJX7aKnA+7V+otO/iaUaJ34DRlXp1V
35ry4jyayGHRybjEorfSEXuk1vY8McFlXBXZqqBf1hvJoY2OCQ4ZwTWwuPzHcZnJZIWQCKkekibn
8SBNlJaNABv39JhSESEB/ReU8RzcOj+c93q+uWLegmrxPtaX/nV1WZi74TvGyWlrd86CKD0iPYhJ
alO/INV1eDTtXsfmlD7HDCc3KUO26qSSaGTNe+eHnCxfLR9N5/+DJbzeFmtC7fB3iT/v9qOTDhyK
NWi7BPNlnAK1QTeD3q2zyYLEsQhDtpo5pV0+sGN+V0CKlJ1oFfDJqh7T9by0xWWKrxT3lyAEwtwI
Pw/5DypH1e+eGnOiXeaP15QhyM73wk7Hxj+XD0V5mGMJ99jLc2IL1JV7/TGaUS9h8FKIzVh2pWtn
jd+vgVQnwW5sP5pnbhO3riZgDUmi0RnxhHdtp2Mry/MBI17pmtO1Ow4u1XYtmYpPkqt4D8wSs7zs
JEX8TkMtDTzGdlXnX40Z2eFcem/Ak5BJeREHigKQtGGgmv4ssGCu1KzWm5FQcSScw3lp6jkrAMD3
SkBfnT3j1iVKeBN8UIlnUCZ6giB+lx/3crrChVgJDri3zZj2hYXwtrvVcoviJE+2bGBpgC7FC5ZD
CTTOnoIxeTFLQGH1La/5hJ4f/mUVLI8+ORL/MxCDn4GWQ0S2FcBI3gUf0yQgytioPLXR5Q+Pj1it
CLxsLxh9poYW8r6CY2l3e7THI4Fjyz/bvBwgP9J6ynCVhqhw6N26QjVQTmUZEfIaRVBUOsx4uxMv
s4vNHCF5mz7n+TcxB4r/B5F+EaZAS2pkDFwHSyQV8PChCkj9SmqMc72Btkemp2pa2N+RXHgBYeTB
OhBE7ZIgag7hR4cMM8ResF5iCynMj+RDP1Ucy3jGNfOjLYKk8UZCLRHGatrura6dYFki+KfRthqU
vE8XS5bdC9FBedWPKFfAt6BDLBUZEfvLUOTWmcxzGb8dF0mLqLiuRXxu7EOXyoRFa7WX7vPdmb79
2znFDeK+QYSCPhoXiMwO+8/2Y9h3Lm8m8XmILa3LiHMkO4b/OqRq8eKW+7oDpSX0oU5sZB+OdBHQ
zImUumibwV2q+aeuy2/g0lnCf+nLzbK/gLBshvKLmO/Mu2FN/QILa1+Nq2JWiOr/qrGBmYtE9YAA
asBXQxuBH16J8dZwKJVIs+mMNJbnGq+D3ybuUBC/1HUbGD23THQZNNaBM/xNkOgvGNhiAvN92z8G
Y8qrGpT+adHPDu0G2Et6soNvBBK/xui6lFIsrJZCg70VuoLziQROxIgYpXztu0Ql6PLjVKPXFRqo
4ektfQ2CEdn87OCGxko+H+FNCi9OMZM1gCHJzG1xwCjQOPAzCnhPROY73jUtQaBMHCtCFvfbwqPj
h+f8hyDEPZ502gQydjF/vkSYbWOPU+RNKZoYFS3nqmufoNbUgEyhJcbnv8aM8r77rGk9dJGDZCiG
oQ8SF+f+o3QT13kFjhvyRv5QBJHVcIFBAkB5Cn8IMuLpEjYm0hIXpGRMextLBUjObBCU35xZgCfg
bwdpmf9Uswzu8saB/u83aImwcl09V23hexvCKn2QfrSgh8aH0fJeFHYJoWR6jJ/8g4bByIdfZi0X
Nk/rggpKhk+6m5xAvlYYQaGIwi5Hc9vGCOrzvxSz1V2EfxQmtMCOjjICSDbUX9zGPLaNT8x41KGj
1S02RHatzUQ/Wygoav7H1Ll8tFUIszJMz+buz52EI4xrQ1Eu73xQ40ot7nyDbSgSe/pUoJmA5eOR
24ZC1RhcnQwZP2RemWe8f7vaUdkKcmag7+CHb/m44KpOzoxZ2U5LkRx/Uk4KCuI91FlxSF6fUdTp
Dh30T48s7GqSyxcuDbS2PVoztsH62poARSygC+V717HD/Yy3KiHm/3cRlFVNkj9yQkQgr9yoR5P7
zoYuIGW8/MxbRHnRau/VHLZH9K4ycEfsW/mWvTO+uYHq4Am8ZC87BFG4/dVIl9WmH/VDHAGLkO/a
uiDqvg0j2EnUdfW2OixmxMcD3cNj/2b+vaplgIwRDj7abSTTpdajwJu46j5nXAWcwJdOxWsjqXF4
tYhCL1a96rfItlsQm2iWRDo6y/RzlCaYh3MbAeIbxtmTiwOiZRfFp0jIuJpaPyk4Xlw6U0JiZt4j
/z8OR7m/CHcH6Esd4EhEd+F4StuJfkp/iYaTmESuqtO7uXWEJqv913PdcU6l/l9s3IkCUciN7fig
IZVjX+1lWJkecSDpg1FybKW6Wzr++Z6IPm0o0aEnhEMDdmhQw+DRQxyO262Kjj4pQsKCmwIMPGqk
1+Wnmuz8FFa0e5OH9y5okImY0CNqj07Iiam8Tf4Vic95Pv+oXeQ3tNGc2MbvHQwa3h2ohPLvjnJg
S9XUSWlFElDKDZlXZQP/M4b8Mp79wnw0WVlkqF7BFALsBUkDn0etth+BE9sgpHva2ucG1wRVhxQK
lP5QsfICbf/RZaAio8ynFfPQZAwCzh4mgQlSEBtSKdnhYtKRTwGiFPusDkVMlppUjZlFnCwOURBo
clgV94kwmUtvxtYu98zLsIMLKcOL86sOwOogV5dNmJ4KsMsXueyEOiZBKTA1Fp1EScvyIQlq8fYv
TAJtBf6ZDf+LaC7+FZ1DKgdX2uNfG9goLBCVpI2hov8a/9nRZtuVMCsD1Zs9DL66CwHENNRR/yH3
n1m+pRVnzevRo5Sy5MJtCPEqKO5QgbEd/fgEvX3RQAVQfkTWbd6VJEnf7StphJCo95lYAQ3Q6vTa
NZq/11gu5RAQ0i+XFvfLasM1ecIOPR5Kwg1hfXrd+P/67H+CEL5WCIM/g15RvDiAP/JVLKoms5jR
JC77TCdjEKBXDl6s/9nN0LpNTLhL12nPyD++116I3CYNGpALMle7QT713t8ZbzVHK+FwANOfCc35
MOaWUiAzY84wCQoaF+Xa1uc7nXs7gkScbxbcOTHKsavndlGf4xJdWUYeHrnTny622Sl/zrGpvq2g
+fswgfKRZy86bTN0PV5VZM7ayrszM60XW8g1CK4ibujMTdrEJlkdzyFmfWzxqlqA+hImsIk/IAKG
0HH+f55V7OGe5s5jV94rNO8nm0VrPAOjPbmqfdVSBVED1xgt42PYFTIbPZHHqf9iJ1jHsmqThSnq
BH53UCkcOJTS5Z38XQ6AVkY0sA1CkcmYO0tlzclj3emAOIryykUbPmk0r5zcNdUAdMIu6yrrN0cd
GDJ4NwA4SzqOuziJNtXTeK3RBd7SxyNNtnYziS84gl9u/Y1kqw6aii5EhudUdObuU8WUT6UrM9lb
rFim3LutMfzw2pqsHMnM9EXAVZ9lG/ZIjE0q6OJ/stm+4Lh0U6bCsQoNMv+fvR3RmLcHhTNhIyEk
dZFIRV6d5sQQeJybmG4rrdt7Tw2PBk8u8A+IzA+6J7TtpBqSyGGhjRKjRHQQfe8GhL1t7njtyyYg
f9WYughGZC1MdIOCX4EBGbhyQSaq5ns6ij5caXGguscKBVx6Qe/rxTYr0mJE3i0BJpTKoXwbn5/e
JQTTu0hvonrUcmLlTFp8mqHuUN15jDBLd+x4fb6hd7RpRw2z9WtCcbBlMFMIpWMep90v4Zqlc422
vcRWUdeYLjLOovr9OCCXuNpbsAIS95bUutg08reSlJSy8JETMapgdnfmuCVtFwJ+0ZJNmE0+f5co
6K+nRyWn7yKgleYT91BdVDTLlKzBkCQCLsi0hKnKYxr57Kc/c0SVY+/tr0x2QnWov8n25jcE2hzT
W9hTNTML2bP4YGVsetISdw3uTYUNDwezJsbSVKcCwaI4kw+N+TwISv8XbLXLD3G70gcQfzIH1KYO
+TeXtINI3TKZ7pJRKRSM1FxYEYxfi8aXddnM6KBPRhvfv6hh5gXqOmFt/8fBRMwWwRZuG/0bCE0l
t87GURCSGnJYhQy2wSneIxrG4AHT0RG5uqVatL9zsFQgM3uPJGF/28w1t31jk0u4/qRO+rGpG8ps
y1ZuuAvcOPXAoGHwGPhGMEJcdeKO22F1lkBTDcRfScnA3OGTiqeliLEE/A9JmeUEC8SbvDYa5JvK
GXm2+Mud/gDmHZHzQzU6LFoX70P6w5dzn/k65DDFDP+ZmKMAmIZj2OODFRyyemBQiftAfpz+jk87
31Lo9lGmVIzNZntocMJGAOUhjrdk/Vay1BdGgF9PPFAzU8aTFA8PCcPqb8YsgY92RcBHtvpzxgE9
oHgAEXrJCeoAXb1he4qlpYuSidrMAGw2flJbMYFka6E64rl9t99GvaCus6gJ/TE6GsoRWoPQE3ao
ZkdbQSI4WaHTkhxJlDHG8gfb6c2GjAYl16bWfqd4Pb4mTnPTgkoJJGMiEanKsAG6VRfmC6FJ2X6a
PuJx9/9TFU1XOr62ZLVrK9a4w8m215I5U3QS/hWNwYpIUSw3CyGO9MBa8MXf1qmqAKo2Fo2ffvIt
3dZWpe2Ur6Hlg+hRgPTm/DF+FIztGXuf1tbmUAqyL6kayzGBtCWSps8XLRB94ITCJEnK9SQrS3z/
aCkKEpKwjDx3bB6lyKm+mUrK74NthtmDR17rxOC5bhe91AsUEhuqDb0sPT9LWK/MD0HYMNhMYo1r
8nYsPGof7CMS4QoUgfTnI8V2DleJeHAG9VKVKiNpk4qOam5R1g61C0CAsrqmnyB67h0GRjG9Ylvv
SxsX7+kWgKWpbzX9f1pBmxyn87ugbIJJrdL/Rf1mmbGIg6ZGB+oAyUtkqtDCiAVV6eQL4Sgp+23S
mdTua97Fc4l0y+W+6YaYmK48mBL4BfPIt5UywIwfSRTTEOFXOnFsALW0/8shWX//x9qxQ7+18Sjs
nBgXB2NE+O+MYFZYazAQxmU7VT3OopCKMVh+GCRn2W+VWdssSpYlRMbU0vzEV/XO+1BU1q59JQ9+
EnLjfA+yoG5+Btb0Qw8IhLSWnxEJRIrDk24hXNcPSHQpSRNH/rsnTCx1kfvEfAg8AhxPI6pOlD4T
gxPx9gqxqvjOJUtb+2F1KQtEopvwpwIA7arECC8t2YVDChyGgRW28E5CahEfyyACOuiYr+aCwZRP
j6H8Fg2bVVnLIQTxNjWEKnVnGgs46zdAQ4KJjPP8+Qda5ZTmJIty5398K2bF1zMgxv68bHag+HlO
Uez0ArGk7H49PzWlGzrmcsJ7d/5cu2/pW2DZkpNyHZ+w4b1382mMLRg7pec4bGsyeeecLjnBb8j+
ovddEzfEl654qu8vJ7YbowT2g7nO6lJaZK8lbl7MwsY4j+hHaHZRpIt6NCG8IbuS3K4Qgow8EE0T
8TWZBu0BghJIdsz9/Q/9SkxY5h0ZclieV9p28C3T0bPH+ffl2jUrPQubTqsq5T3tWGepjVRiqo3a
hNl9XBVgudX0oIkbC5um/siFgbE+T2TBway+LO88fuO/mi1JEBAdVOBh23oMBpWCLXDEc+3RauSv
W0wBRnAJDb03ih8WF19DKj2B1arms5jlCBGSEJdt4hOz0H1HouI49+m7oDvEEv68MfLpUyfP+y+Y
5L5Y4KFdm0aFTeVpwnTAKwYvJ1yaLnByV9aLmUZgR4ZB/SMkhmXVYpIuOCNxCm4ZYp9yjx6W2zvr
TwTT0VxuSNlja1jnzEr5ZBMXWQlpEkWdHo6592tbEgIlhgMnb4hc9pekNrCtPM+Ad7I37vfqKn1I
qJ3mOx6ULw3Gtldl8wstavZHwiCAmeN8we41/fXPevwxPqnTHKfLEmzjS++/Om9vZth7p4TRecxT
svDgVkhkGwQoADRYbBuxQgktkajUby+GbesryApGj3xYa51owy6fzac3H9QYpVbynrOkVH5VHadQ
Hboq4JrdjXA0dvmVb9YxbLrFTY6OrFY/aQwfJAcK/OKXhQF/DfP1SEYbLMDobfFYEJu2OxNcHGCP
qC61Kh8lrxMuANbA1yRn6+SPq3F0whLYeOVRICM9VudiGFCqSdGETc0vXPlVWFPB8dtO7/le4f5r
hIiq/CROualHqZunbjx1wtRNxqeSRH+C+yIOkHnDkrYMfTT929kYhsDwm+TtcR7Zj9kZB8WDN4Ev
hqbtJaKYqU/gCKGzHPavNjgLwbuq+cEm4bhFzE2hUUavopIf5muHnxL7bx7aWXAUeXSSKAhuxueg
6XB+52EVICi3sRQw+3KOIA913YbBPWkbqXHHPfSybwdmUgD2ticA4hwk0wWw6JVkf5qS/5SB9apa
Mq8a8wSRBqGgiqs8lDuuB5jnKVtGKp5IHPVfMUPXlrcjiods0yIPWNAJb5Hav/q9emhOdvWTnVWY
YjfbEGIIiqEzQX6cL4E0JMysZ+okyCYu6FM6mG6sJ/77a/SpFg6f3ypNQkTHUL45QAaPiMjnSIvL
OS2WeMzr3KeoFhRMALnM278KZlA1/tsHt5Qze9H+7EnTEXxmCfMx44QYVE6Dz38aSfst/PvO221+
0RVMALy2aGU8Yr7KQMhqbTKt3C+pWJmg5xZPI++ppO+6KxLEuqYOSBjO4Qi5oENm7XBD1ryiDLwl
Ov1mDcQ8d/mj0Uw5wlWahYJA0wjZJqbLnnNqMM31XM3/5VDT+wzaHCVLUdz1Kh3zq7YpcQAi6eto
+A59ed8X81FJdQrEsfgNlk34zB1Y0kzPaV7OAami8CMn8jpwywLHkdmCWWqq8qTwu0pXv4ff3vq/
vByE5k/JX2pHu3+0Ag6YEp43Ot03LudWLAIkgg4Zclwwjo7n4HXkpCZ22o1veRMhm1Lfv3RUxa73
KXnlv6ZFaB22DJxIMcEy1CPYbnmlSR+tiXOddpspYV6ocVOh9uXYNEMsx3ThtwofU6fe5uoiejWi
MZTMDUn+FTSiKJAozJbOb4Ef4fmgZA6kX2h4aMUNcx6LybChHaCjPzQyZhtp0fUyPFtDksAKc8Xx
0Em2tAJY5cM67YCi6vD4WKNawt+uiLhW3hPElL/08dvcdCA+TPASiAaEVyEJATPz6ghqw72u7Bj5
Nb9MV2i82EWv83BFOKQkax8gIeXGnSyMPAjwrJP7IiIQuhF4UTu4MG6MT5wD3pieBD99Ui+tvUOu
i2VHblgmMnE6tLLJmySMcEQ820CuMLSldQTPGVwNlFNgmmIljUaYWZ5DXZFti9jmL6VwPWMPg2hk
6FrLEGPBaeX4EpIxtEF6h5VCpomO5/shbDtyx7N7XeMh+jspd/3suuFU63jCKMNNtJw/qARt6C2X
99BzbdCO0iAoQcP4oxmPj3/a/abmzl6RbafWim/JnkD1SN8UgF/I5XWHhBnL2y6MveToQaM0ovOU
1UjnNL1J3n2SWtgAtPzcBxAYNN1BHQq9QB/WxueCTgAU3hZThGsQsiC7e9RkGH2o32wfBWwsyYjJ
MiPECsjttWlX1kD4p/qjNjIOp1u3zzUOUdu6d/fZc2R8Y5Fmsg4Zy5uq/Dm+d+RocYi70TDetwFa
a1bh+ND6U3sLuF3Lpagp8wlCy/IWyb4BHOSMPUCau/XlfxBqM12U3D4VIgW7IPaI63htPIknpq/y
y5xPmkJ/ZrUE6YPjS4r60cfTK+bNP5Bp/TBwBXiqJnsaJiTYs9+L857dNT5LYGW8WmwXd0GxfPAo
6k/SkA5TEDrzppBjnXDS3O42QYTC3heqjZQ3/zIreA4JWS4qrZxRDFnz6tZdM6M5DqsA1UVjfte4
lQ0ju2gCFW+H5tP682049BPt89fcxt4HfaE2Vlg74tpN2XELoPN1u5iH+oZzIDJE+CmMEx26KCNT
DVewu/Dr54CezL6Ye4Ha00nzXEev/rzlFzE8dU/EWOS2c7eetgyWNy+PqWyviXJ7XRF43ITiNDdI
2czztSPeVzhE0JIPx7TSh2ktv3t2xiggQ9nIxDzxGzXVx/bkocQMn/swFuwf2rGc+fFcUNHIHkE5
f801E8+zG+yTYhPEg82rEx+APtuh8rhUX8ehig5bm8ayGNaePZIDHIi92rvZI7ftoVsGAPyTTLq9
419EveHJzeD0NZ3TyKttdhQHVa5KgE4iNFq5yDu2x2xf5Pf1QzkU7mkCK1asFVXVJDdeaqtEGxkI
O8U/mUfJ1wEg0SEojkphmH6Q3jlLUNKUOltAzIwOglss5DfGlG0yJf6V8TzyU11wtaTTub8HkIuF
6TWqFkoTsOhKKmVt4e4jw+/7WnJZ8n7cWn2/sR8X8R4zowJ9wtNTl75mDICWvOHQEiA0QOOkjtnX
F6bKoHrYRRaG4i4O1080bCiHz+U5VBxk9exvuKEuyGAK8WpkFbSzofqZ52roEWfroEJhRNwKE5UW
HVxQVmQ71yUV9ZCDv2VBCKtoUFH5sfbI6yps5rcHgH36rqV1OIjU3E0VMdagqiSkT4ZST61722tf
pO9p/fdw8yfgtvTG5jZhN7RXFLDlRA/6N8GMXMAMIjuVWsKEEcUptw/H9KiUhv5d69l9+LTW/M7W
mt5LJamLCDlgoXtiwt3YNqnPc268U/Py2qxc3gw+3YK+uYBmNaRtn7t61rDPHUJtKLkuaP+NvbSn
HceqSlMZ/gTiIPPpUcIPO5o/O+qUIzjycCoZONUhZUb4xM6HPln3zdTnzcmMw3YhtNJeRAMQ7FW4
qHDnBtiOppZyWKDhNSWT1vQNgbgJYr02oXk//O+IJOb4/Vg297u0vIJa9kU2iSz+gre1Gebzg68o
cSHywM29VoAmt8EcxbLiwyn2b7Wb8ln7ytIiFHbqWIu9MqX0zPcC3X3Yd10tr/aA+NWYEyylufj5
jK+p8sCo0eKFjvspcRpBViFZqGxUwGwEv7fktnfT3oqHUkrXy6ceBfgVBuPid490kan+g2ExqK2K
1mNGNA8hBqspIM9aeMKftDqI93/wFP533VDr2aGS41bdcRVOMMZ3CazZM0scndc6maraCdsq5CXC
eZZJ6qQslovgyKcau7dyTqR9de2tCUg/UJFlYpUOWS1Ibk0Cye5rUH/sDTWcmcnNl0jeVH0ABmf+
7TFuuU4q5WD8rV2EvtcmIFayqZ0uTAPkkNswkeb+cCqT1hMQYY1d9EtKOrGCCubncrf7b9hZ9FVG
YCPrg9VcYV9txlNNLGUTSYP6+YU6Tpwx3kvhY0o/k6/pZjPMWAO164GSlj3Fa+hUEtTUssjEX6Xl
uTANipfehDWKt90PhIe8WK4FR40PjwadXldtHZBey3r76LqrQtmO5cQpi8k5yYD/CI4sG6AivANy
2oMCtawXH6wXMRXDOs4TX8J3h+lpODsdIIaUNCV0GryLySkApQN3rnGWRfDv0D5gfIAkfGB9B5aR
u9xL9YdMJ6HBerkT4SMmojW+1Ne34V1+iJknZQUxCA/7lL4PG7UdItDStqJLU6t8BaGnbeXNForH
Q2MxZHydkBXDDo/CMgu1zpDuDM3Iy+Sy62aIFD/kSPGOWN5Rw4PGQk3Jdn3mFj4XX9yS+l34+JEg
XqrLmqXN22irWuNLYb1NEHTkaiP85j8+b4DWaOGornRhRuJzM2DiBRoLS3l0w7BP5xi/IrYsswpU
NfVRAZByPJjRIWDqUQadZhtB/PeT+BEvrV+SH8oS2DN8JO4XHdKWGv4SCl3SZrwBiUmc9qPRW3k0
kuF68LiKbuCiYJyoQGZMlSEE2AZQTclECRzQffes/K3mm+DF4bmLfOcfpKc6RC30wWFVlf4qvDeW
0Z6zzxo36xihjQ0IYDrl1AZ2SJVeJ0DNwUhUWI82r7RwamugfaKxscNXeX6NNwMMul4PBeaoTFzZ
v0uRA6EnhQpcZrhUsZkHyiYF3iREAMkQNoYY8zsFdMQm69Gb/c8Y1YHXgV+ehG0f0MUZiNHoZl8K
lZ0zDt1i2RY6gPG8LWeX2TW/FdWqsg3x+HpF3RYPsW9LaagGXSo+V0lbG0u5X9J9TK26A8T9bFfh
XBxU1XGi1i6S3LjKJ5Z4r6yEXw2cQiaQc35a+HZM6zcnbwXJdPwK3kyY9qLdcQqcbB7nhsHKu0d+
J6y90XVah0zXqgc9Ag9zuAs/WXuExpIFQ6niQF5zgJVERAHlZ8VVVjmBwdCFL+Juch4DnCN5RABx
78y0NxtpghkSnB10ZxRzIAj7hVafStubSQhMRO1L6oY1pFqo4gmEz78K4ilBpO5iCOOuZVNDjD+s
sM+ni9El9a9egAypiKhDSpbMJ5lSjvzONAywdzl3DGAqWkBdE5SpciziQkzvvwDCqeP+kz1Ysymc
4XnDjHCZsVBUWnWL1qSSZFtunBKwkwFNhAM+DfoCwFNLrXWJDkR2kBpzf5sB1kKIKclKvm4+Dmvc
ueLE8WsoUGIwtmIR8lHO6FIVAziBcN69mzLe5tflY7GQO3y/9HMJKmXUvjJOnd3lJ91odhyYLXJ1
8hCEibSxmFSjgEseev9Za9xglnPq0Ru+2wNJ3OMoUg281fmu8JpZ/7v4XQ5fjLMq+cu3EUqoqrTg
ES5mz9JCzfrgwBG1Xx19FdoJf++lGSqZvJmrlZ+ih+6DcXLlu1IolxGDeK7ybfug6thw7rolJw3R
upiHfMvfXq1+QWAp5gpvUHN9WvfM0MkEvFRPzY6J3g9JzIgnKuuP+qkvctdhpAasYgaiAcULDnu2
9uVXrc9mNIHM72obUHwem/T1GFhg6J3pObcG9C9nusCL5szQKn4kE/7rc8lHNcAjzykETM6JDrEW
AjhBm4D7N6oYrn/DmuB+p9KBh/54xR2sJw9k7TM3QCn2XH+StJykHDjWck/Hc4CXRY1/vLH+BpXX
LfqCpIVZ2gH+Z1LK9zUVuNP51En8Pskj7WsXJ8dqYOh4MjVXzr9EoWYvYHCOoXVKjoN6+guwS9J/
VvDADUWMUo80aXD86gUbujLsaiaSFb6/KlLHpx5VWNDTUoJJyfwjqTHI5odbcGSb6+m3FRfAkoMY
W0HFbK89GIPRZrcT1db14hYP35s0om8CvR4IVHanPjOSyVj1KkDVSL34/Zr0kdTZFQYWYAGRVJTv
zLHhrcxv10qWO4BBfiL4L5tHbhep2nkzz9RkQSqqUZkmDtRBqHuO5iaAwMaH6ytqh78lFR8Y8h42
kIaYLcKyXMgqZyRhpwBd15FLM0qsCMNY8JkETjyMl3xKmKThm3YvTrMIYlZz88wdluJ6HH9+VMEd
Pp5NiRc0/aqCmlfHk0PwStKLycRYzoTKIj5cP3jqlV2paQE6zhjvOlNlHhrVr3jTRY0SYmZkDYlk
rUTh7Zr/Or/J0hJx+aCEsbZbGpee4kxUqwmuI+7jTXukoXT01AUA8joEROqH0JyLug20/IDXq9JG
OS89KydZb0K6T9xaun6Mpc/g2gv8HHgyniHZsJbheF27hMheJIYhiQ9+eEH088q8QgK5PfoHdTyH
2xkXwiIJWosAYW12XvBb5X3l/kz6lclKWqftyD0skvMU/W+Nd07PSUFi7eUiJkzrGMQq57lIpc+K
sCbsJ4VnwcixwsXcbAyFpnkdUgQFtAPMz2SnXBFczkwTyD8SA13W6SpiwcQBRt8r/FkYy6RnXZrj
qFcH4HFhC8F/GH/B0LMUJKsmCBGQJ6uSy5HFCe3pbJ2G4aoeN2bXj5JwRVbHPW87wJMApBn0OMSl
sFnOhUyCwpMJsoFifK4zLVQLNi+cGwbaNqH7wPLULtfmOkMjZCb1fty//j7Pb8LAvvCV5jhzdvbJ
Fa0kbwyp34SZiNYBTDTuk2H7RLrHcdbkJbWLVRgZbF7gRsYxj7X4YIdX8eip3rCAvrGupaVTN8fZ
CRPqv80I4f2bwBgTuJyk4p8cp4dxKk6Fkad+ld8KMvaqG7GTq7s4CGFYaoz7uGB+BmZGeWYoXxos
u/rGopPecV42RxG5yjOvWzIwqQI7SA+xUEg6ZQVQbzKGNcnGNMYdI8njforIKyiOOZ7ebjCmDqyh
QmP+c7JWjyvgG44ybPrGfBQrj4Tgqkuk6QTTZazrn0sXb2mQJWNhOY4JSM0SA04Noznrf8UqUo7b
kXWVyYBI17+QqREMKvlleeU+iTIQYEMtC1eh4ySWZQNnptMMLkYqp/MQLvYa8AdmKfQIelsCEtul
mLqfUvQdNQkKQMvnC/qOr3G0Sp6QecbO0MKV+ofZUCVcALIpUkwmz1agL5NiVbiROh2ICoYuq2Z7
kr9rAlyGnZnBDpjLl6Ws2SvYIxz37mq09MVh2wB/DoII6P4IVJD68Wx36eC7Fjx+4u/d7I7ZxNh3
OAPxFop+QmMGS5FGcoheayjLDSNiLuhRAAaUwsZmhm2lfoTJ7KPKdOaEXqx1LbyzvCz/0flHZHZL
aL475rVHof/CRdLUW9lKaR0bVnulC9tkyYePbik8Vwr/CAjw4860xyWtvab/cMTKG2Dmgn6LvnK6
fcUpuA9hVQkA8I6OBwWU6aOpi4KqlTEQWbt6JILHYUTtijcOAoVRSDTkMf8k71c/iJNAnipok5LM
OXUUKRct919F4kU8tg4KeV4PMo+btr6sAP1enRTV+qMnS3VJwMURh/pEwk2hJjLtgWOhkqdizKUX
EMUVzPNA0Ew+Mgh53e1vBBTuAQVEQZW8gGbLU4dl8Cu5CLgiNDabbEZYdT3S2MS8erJABvEj/dsc
NLZHqrzNDLXP4Y92t+7Pd1ne4wmGKY6/4BO5t3wBSaTDb/2H9LImzpoRrcyNeR0IvQm2gS5V0XZU
xEA7dJP9NV5lM7vf91mhz1Bnu25hQl0soMAmBh3Bkt+6oEogiesr0dxj+3/T7q2moEwzy5YXLDWn
+kS0HNsFcDjGNenw9y+I1U2x/afKu4WnuwqdCnpmQGANe1SvmWM+sjwKkhbSRDlYXPyFgWDQZkDV
alrLzjH1yDJhMLLuhmVodYuuftZRvTphjv+T96WGjB6fLM+WMkiP1OLESatsurv2/QOnittRyU+j
Dtp1oCz6giTKlPmzbVbNEWCM84GrkHn+1RV6oy6J8fXXPdbo302yDAKIlvmiZSCRIfI+volw1nk8
3YqcyKaALxBD3Ka6LUdJqpaY56lFg/0SnxGF9CT2q9biyJJOCok9ROll1jRYt/bS3Rqu1a5su6r5
8dk1LS4NR+FvgLbLEtdU8oQWMHb6Jwkn4y78KsJaB0hp/NVQVDyDXMIsOh7bUGCgEUVJUp48E/0l
PSq5gIGejdMLiCS5SWygthkRLXKCw3kx3Xgw5qpkMCy8W/TU4+wX00nSLvYvD5YMA7AS2l4sMA71
hRqhMaRWBk18lk4ZbcpmmMu18kKRQrbpBmFeP1+h3igoT+mv8YTjblMCuUy9BiqT3exjDT96pGxi
eHZ9nQNWoz/8me8O5pN0KnW8jDYp6vx6b4dW9VUuX01Bn7tvkRmji2r2JAX0CgESnDPyVsE+Slix
vMJ14wa/SdoPoCzkZAB/12L99PGnLdfLOFnTjxlIFpHTPH3HBnPhJAzj+C3RD9+69NKLY0Ybn7Hb
rOpANsN2koFrgpH7fwIVsCYS2eKX4BuKr1YItyiMWRasW9Kj1eEPa/jLHAj8dF2VzSCIyZNTRKkL
iW3+CWC3EFj3Oysx4Jzv5zbc+RhYJV4xUV2xlzYjeZZECogekilR/7oLVPLll8WiLiTzMtT7zwlv
FvNVtkB3vFbFFUIq4VyCRFALw6l4U4WRGrRIjPvYOe7FKpo0hazNfFEFdGjl9sXKCg1+Raz1XE8D
YHxHQTfxXuc4L48rVyScqg8hiNj1HlUXGH8lSKPjNDW3cTPCrDYEmQKF9zSzWzoHxuicca6yvMO7
mjGDl7UEquZ8RsXn4vM7Dlv9rMxAG8zINRggqylsJGSEnJltqdjPYz7+Q65fJdRoxe5UgVZ7bX/z
t1OjoyykRCovE8aWfvh5bnVptPB7ZpZZ8gutqEekvAn4xZomIoObgPo0RnFrrUhiKJnQQbDlsmXx
r4GqRAPqhp31JMpce5U0aUoSk8YFXvzSlKE0oNm0/DaM24EWG3Zdf/m8fBFJdoW1QJwuPoPjTtVK
2/acx94R2GDswjfRwTRgs1/lPv0ttLD7lNmqaqqo/21Ng72M2p2cNYg84cvNvrWEPR0CZQBclr6k
LIyA9kYW1jQN7ufGez6Q1gzJo60/bITe6qjBbpfMwXRbAkgR6w8O7csO8VGSW3uPCINypfenmmpM
wqiAQN+UKDall20LFaWHIM45+1N2BnKDIzJkT/DeCO62JiqAFZ+wuCN8gEyRCKlsmqpadojNgiBh
XYwH8vtpSwDoABhZ5/ks2NWXpi31xL/4vKiGCVluW+nBxnLN5hV2xxTJXNj37rzlKQIuX3fqB8qk
mJfPiodLAy/y4oqfxknA3u023T3Vs/bdTIDCcktLZiO6TI3IYv6ol5fDrHAe+wBrR2iohkK9Na9Q
eli9IuSN3ZER1ymSRPgkmDrE1t85bYt+ujI93wt7wZ8GQpoR7AYVOuO+HK+FcUVONCJ1tClqNvAk
hWHxWrV32IvLeLdjYkNeLKjxd3WEWactvqS2GoW8KGHxIxi8n9MWYtW5oNClnr79cslqTpvmYnv0
MTzg8I7KekJNZkw6kn+4lAJHKl4UrogTYF0ZAAmP4FuMiV/xgIePSkDyr/Sv0FdJ6ZeIrdRSOx3+
XrzvKa140GZla51it+JcVWsTbycsEQs3lYs07ewESBGEb5ikoIOoCBYgy0razmhr+34s21JcEcEz
7CUMayN9McKp4qdmHC68ZDWTBHSDctD5xBCI8SgiDqYOQ8JqkZVocPfrFtzxo49HxaXdis4nXm+p
bGssSYqnb8Dd8VAivUp0UZiZ1PHPJAaeqdjcBOTHlIOVlJeB1OiejZC0UeZizWev23QK9zXDTNoX
qO/qIqKBzS8bOGoylobLA4ov74NKNOGkqVbLwI/qu03HpVBj5m5+bx1eR3k28+AUQ+wPzB/I1BP/
pAB5AIGA+uO1jJNerJq4nlad/M4A8+cSn6tCbEq7WxEX2EfdLrqtGd9SNjwrG1OqJbUZVsBJjJEg
XEWT2KmrfuUq5wiqV5hSi3An8M4wLKrM9ZZq+4APN3r3E75zOME5ME+/siUyynj+Sc8gBRBb/WoE
ubbar6zcfCeZ6fmRQ9Yf/MNjV2WtxGg0ckaJn4AFwdnzaJhdE6VdroxGcw1YtZ7AbwxRTUSZElS7
bldtoiRRioLa6X7CWqWyfu1Ot3wwXb/lzScnjNBZg8w794tJLUcA7HmFX5V3021iOTHsyChz7m8h
auVm2h+D8yrYuHS9GEZU7vzhGoqSK5iuqjspZm0kumGCyswuvH2vDE+3ZdFNvufhEyYJN4JbYA4J
jwtpExBYYeZtNFp+DOCmctfqs7ZxVKl0HV+g99N1vBzKwUrKy3BjbxCyqgC6JV7ZwHa5G7x5NLKI
jeUMRQmEbEuXGqUtXCgN0azNbmO0QaqfLyRksILbTMY+aMPlMbryJewYUT1H7H010B37momFFECZ
WY16iNmgKGozVfJ4QmbA05wkbBGcBI3NblhjQEykR9/+9pHCsKD0hpnUdeJaCXgjSE2hfXAp4Jgw
XVviirdOfBdBGMublU/4YozsUMh7K6kSs0ENvpMzxS8S2xTAT/seGmFx4yTTqgHZ4Sqe49KWqLPE
pcMUtKRsMz3JTDsvUwinWjsYyfUU1KIkKLuaThY03VgyASGmMrd/p/VXA6QkAbAOM5XJH5ivJEKL
gi5pktRm4NC5vx/cBRJ0fY+q9JNQBb4vOZMbN7lgk1/So1eDPWaA+n37G3JjaI9vJwatVV4bwupr
c1/ktXlYbapbb0hWR7x9gfjvkvzz6M1+Tco5nLBQhkNyJQM682EdOO7rO4Lm+yNlTM7fdjmbGvZO
TtKwG4YuFRxD54WXEhHzfbCQC5EDBdIVrzxRiWCQq5gy6nwR/poBXNFaV2blkhixmFiNarFNzTym
cAh6Ar5rqg7WJ3yhLZ8D4UELcfoPGjgA7DuKHWflGl/2qWSq8nfI2hG89rxomAGZv4xqPmAW5Ni1
Yg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_7_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_7_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_7_0 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_7_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_7_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_7_0;

architecture STRUCTURE of design_3_c_shift_ram_7_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
