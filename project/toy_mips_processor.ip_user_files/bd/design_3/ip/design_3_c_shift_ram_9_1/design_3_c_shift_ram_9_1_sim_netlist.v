// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_9_1 -prefix
//               design_3_c_shift_ram_9_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_9_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_9_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_9_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_9_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
DhIogHrF5EwEdYtk8vOhrP+2ul4pyYeK5BH2cXfxMfghkZbeRBHXNosiVby7IxYAoyGB3Vf3XJJy
BPQSZu9gNm70CLpYNuM2zpFvhkigzdNF+DVLzY4pYmGX+0uIyiK0qSgDuQ5k+FOpm+Drvc61mqek
K723iUlJC+BLFqGfVO/d+EmG0l6hKSl96PbzaZo6xm8gHiPzx5bqrcHYGF0jrIToMaSHaue4s+Xc
QCgY/7pdt3lC/W/iYa9ifnXlDHBxYQsNUVJARYNDjYfzYPJKEyhJD1oPG+vJEdKej03xJr0L2Y+C
fi0cDb/zVpMCHT4J1hVJVN+jMif1exvMTtvGE67epedi7fwc2ZRc5LvN7OLNOgiMpCw2Osn0YrYp
oR8+bxznlczRKi8tIuaTuov9EeaIm0jtS0aSS2yNcclcZCnVmUki7gsLdz7DvkUryQiAtRErm0VV
V0vvJK8TfZQGDuF4z4SFOJ/1JJXceHLqdyvf7SzS8iXjaZeqCc7vozD7PiHlaXMMrB1b8Q5yHxAY
jQhlLAhqfLtsd8iU5DmRMrh3C1umvDg5NiC+9QzNKBTehNBNGQO4dOzUtTuZoH8Xg2dudmWwb+RH
JzkvEnNm+UCTLoRvJQ9EsYUgq+1zza9PRGEsbOSbZUZE7hq2/hqgkOIyE1FNByW5g+TTZamxXg6y
XM+0GhPS1SDJFr7wtZdw3+v7L2Szi6fWHgUeC4sMTmM4elE2RbreZcnLNnTxNZECeInGUfWsu83l
IlNqSpd21Hw5y3ngrPI+IfMjdRQVV8PTkp35cIn3RmJGGT3PV3CAPB+RAFUf3tlbxWDGJPXOTyc1
ADfP3KE8pc9VN1bRAVOdr77OCdAMZ4Q8wq3e9hKGBXbqmFYgXugjAFUxTd2dWU4o+vnsmBFDZFZJ
hhAKQQVtk/vLGHzH2+UZsCQMwxlFvxG5ol0V4G13hzjvpBo+5lOagNiVdffBxuy4AVi0Eqxh2uDy
nTUg25E2xv8+Pom3y1YF2V50HRcXSHr4wnEctcABscwGYibb7Jw75dDaEkxXjIq8b+CF841U0CGh
pvKqQlQAbGYLToAb/331RIkE/27lmkWDTN/5MF/LtDpKRwFTRrJWYZIU8EhKEpVplhn9Fu6An/kO
7ZWmqvGH1EMRcFYkK7nZLM662NT4u+FvBKP5Cuuys7zfysxnvDPKBdzTYky6xuoPy/Aih1lw2kNv
HWyzSVoWLGKb0KFlTukX9q7hCRYZpfipXdQzIgzY5U6ZCsxs87V+b/I8oFB8DStho8oYLZsGUf9+
KhPtsQ020nVHCEN2bUa4twcIDBD70sh/rM5yjTwh2cKYADlGMBPkFdv/dRAbnndf1d72hslS+D5N
dfIinX0f/QY/NqjDocMZdtCYrz2BNsvwpy7uhGamE57snwnxMxQC9wqvO3kUFPtgG1374GyurxU/
+kW+srcVwypfm8nBaLJQFqKkHweTh9IYMhIVBhzYJLvy7UDOnHeBiz7YOYdQtcOhfxx5asffBvry
8Ejgtv1bp7ocUxKVwv0TCH0YZnFLLLF/heTok+wTQo0lzbvRZ75V2YpW1XQYmRXDhiSL0JfdoP94
1qsNT2dcdJ4sR7UXLfjwKnF0yKHugLdFHqeFSy1fjMym/TcC+ZpoHxv7gwieXFr/zL4nY1UEpm0Q
MOXUsjnqm1TYI8mjLE5dy2DWQPy8Uo+e+EpgceSe9t5jO+6/ssXkIa+LTFcPpCCroVQiivNU9noc
7HnGrGQ12PPPuXTxeCzwZUvq86xDv8OnlQ0eRMJqjKr4ko6EneSgTGOWZG46Nh6UAVmXY4S96Ano
6kc1XbKGexEvH/EI6E35cEqFb48YqBAkg/IIfuwNHalj0g0Va2930dN30PsRkT3joR/LzNXiL/fP
cfjz8hPW3wn6d+/0Nk6lKTMSPDH45IRYd8K66ILQRb0sx9JlwYnFd/6j+XYd5xycAQPTld5WONW/
FJ8R28uwZwQb1Vjips0MBWUjyDjvyMcbG85ctJ5CW93RE0eiOyulsw5Pn71LCs7A/1CKQnXLWF/L
HANjkGO5vELm5KIO6mW8YVhkaucddYGUIHjDQr3LmY6P25SIiPE6bV29WvAporVrBIpoyj+fNoVQ
Yy4ZzedIU8aG45V1KVMZoAhkAI97jHZhdItUQvK6gfBAyQaqdklrhQ6b7FMk9nnTkdn8/8qHQ0o7
oSQhQkG3pbop6d9uelBhlRltMfle7I7amnCpoWZ0of86s2WqfpH4aEdI/0XpK+hnPyJA3tpMHRNz
UFLdDoenY8vtzXkwIjPHgzLWjLeVPCa2gJYORlpy2G/FMqxVOZzLGNOX5zodrYb+itJGInUZqQJ4
4FaF8axdjmfskgzj2YiR8UCeoay8A76IFKtIkfH7j0n8nIGqdrf6KLJGL4Oip1s+NUYnrgmLfF1H
7Lq8wIt9tuOtT0MPAvrpYEtFPcPMxIZQcs4ydJ/XaeUhhNSV/0EDtHFLehhtFF5Y59ln+q5NnE62
IAPZv5wKF3s1oOtdYEKwxEAhWDfvMRD+LfEuM9tj6eKcwta4eNC7vbduyXyA/nGluZCzZ5mjZqEL
+kkJpvHcix1of5JL1h2pwpzAZzdiO2WPGR9NPf+OB8DxK6XPLna7nxLgDQzlTQINIFKu99jyUSm/
UBYklRHKyH0tmRSusnjvFDn2XK7aPUqDF065pThDRuyls4x2ouj52bePs2rK26rNoppi4XoIdIN9
smOHdimTaPgH4zON8mameaY6zPqpGQuF0e4ThxY3S9+Gq2VHRIbtcRX0d6T3qC7eaP66pDDpf64L
TRhSJP4VgzByyoQZBYIGkmvkbsceOEF/GUCPoL1PO8RG9JPdQj78yM3wMMeF7p5HvDHamv15sOcN
ILzQJs/tGPxc3AAND4VI+Fe/Ls9wz/3Xl6/5phlFXHJw4o/NtXmBogZxn2XLXDXFM1p/AWu4eXq+
1VYDG94yKfNPLb7kTDOA/bo6JiRosogvGY/5/EGPkf/B9geSmG1lW+WwVcqD6uwBMAJhFNajfjU5
Bk2fPHWEJS/vu25dtcBvU33xay2Fnq7hzb1vVdBjUBDNpkGQQNgBHMUBrepCOQSSO/54Wl5lZt62
Lgb47UsnNAryhfs/gO2y62cb0bMU1uc2XRWnoCnSpUtCm3VNVNINZhQJDlkvBMQHXy/bPEVNLWvC
lHUzIGa/nPkrTZxqNF5P4nV9bi6jo8q+RUXmOb3e4UOh6j8h+Ih5i9eozhKs8mZGhc914AtfrYYr
dbBEs4Csv6GMmghdRYcODdcbNvqIqkB0afW5w6f5geMH2Cs1wIYvb6bqLg5TDyRlP5xBZqWABVo1
+gQls1d5DoB+vOdDR9/UwNXxuyFlJAt/5T/BV6RPYwU6Bd/d/fqaaq7K6XrRrFJ0XzXAFWlzSfYc
tdMFB72HbjuKjrfxfCWyX8R+gBW3gZlR01tW5RsYKSDClVIxJ22+SsK5fKvnYs3WNi8K3/tR7ALc
zPMv7pBmv3HOglgzFlEE/Oz/szmk8rsr889LuEEIip9MNRGw+PiAvUmuyNCBepXtK/q0MhZBQ6CW
YlsKTZtbaRidnMLIbb/QH2eNa1tQVnH5yhIcczk0wJ69SS/FRNgHqzT2+//kuHPsWk1sONt/c7Ii
EXNtSukfQ0+ppggiKC7b8lRjuqrRZ+hWmCEfE3XIrTjp1ETxlzdVBLjX1HPsHokWUnGAm42swazt
PuDdaq4NqjMdZQuLI/zAp8g6qLMJCu47lOwyaXVsvI0/MHmzIWvLSp3UdVEUC2ksnsRPBWVnFQeu
2m45tEFndNYVUostlVDzvbDEVflrWHlzhSCOVrI/Xk092+2pE05NhSThDiT1vblhsJMYOsC33BcA
hGuvJupSET9TqaUpRYVdh5JBMcdtIRCy2+TDRuViKTcsi2N5a1XLkVLeienG9xWfCAOuSNAeD5z8
RwgKejRwryZ121KzsKJhLuficfueOi2fjG3gjWWbQfk7GEtoZOHEnsm3zwEU8r36Fk66u+pjoKCL
F3tQscUYzI7xs3xE1FWBp4LvtgmUUURAcLp2TSlxFrE6er2HMlqFpQfA+NcS/1NMtvFVH2wHRast
A8NPIlYGR4A3kxZ5K3zoF894Uofl/EHjEJdz21gzpaV1iBZI3Ybn7DDmFQuiqqrb4yzry2zOkQl5
x/ID9vDRdfgf9ccwVlAibuEgoIYgw7tnMMx+3YFcwroNt2uwh473D4wx/dEowHEQt/g+EOlttLLC
F69lXlzW2XNDOMwpfVJ1NUcdBom9V9Zt/mzTqdHterG+EdG7fIO2FNtdovSpsYcE4mUGAaYSibyx
FjxduQmj0OB6eZvWrMFI9fRJb1Kn6wHHXG2tgos0d0sq9x0CavW7y6j6VlCMfmE6/o9sNsJuAuoO
uET7i0KDlL8xqsmkaHqppcISDYabt9BXDdu5SLatexu7EcJ+ZZ77vCpg4MfwnM+O7CBXL2Qqgolc
fynhuBgB180W49hCJvVJJJw8gpt8XgKCM61gNMMhPqy/hTiu2zoSHqRnx+810cG2RxOkbo6sapFr
OXadT8em258TmjYx3PC/p2UDwQDr7Y7NplyNLr6XlvJPM80dgDgTsAFuFrR2kfsVjAJCmH2XgFwg
TEm0RPOa3yGeDnKBgXGe3fLwLz/mUyn+0xWign2bcTxw9wIV5tnjlghW5KUafW/6whocyJYJ426+
FRTZo/Jn28zZ1CVAZU736MGSEPYkeWcef3IqcZsoqopHb8WeNR5jyhKBWe8fCjvZ1TLrqECqFHJZ
rwp32vtWwnwNEyte14HDLfCJmVOcE3wVjcqgr2xBwrJA4ohN+r6iR4iRR01ZnzmY4/JldHtHSRcI
4+eiUrSO9LXlZQXE4Qfqf7t6Lht1taXf0jV/ERPGPOh0OQFlri61vcrzyAZfxUjv2Jj/k1YfPPfI
VzZOcXIk/qSbuxgtBFu3TM0eDCwhrYr5FUzXK8/c7XNH45gXTGacnDElcDS6K7Ko+P56wUoLxDH5
PuwQJg3ahEC66Ttnp+4npgCUAQzFB4i1NfqlE/rv/PdnTRUcSm+Im5wZP1EWt5AT8DDv9LMUDwAQ
uox1Ya9YGMQQcxoWbbi7G1Fjxnz0Hr8MBD3gqk5jACbYSIV0KE6DP1mErCzaqaXt5ppfjc45GGfo
fIX26g1wdtK9MfaSdW/R2HS6n2CZFWKs1hY9tl4VcDJTa6qHRAzn+LAN5plWIwu5ANLpGUdmHijq
SYJDkc//OS6xYLYxpl35rjFTr/0LMr2gLRqaXbB0G5Sd6SyRkl0fnbBF9n3FEjyldy+95MW7dD+o
ZxqKf5g4LTHw96RxWt6cJT4Y+4nfFifUJ8OEQeKTelGyfT3SFvERw/CEItw37iUlon4jc8PKr89a
98fKI48AiSAu8SRl/hHrLqMsvbPlnq0gCKIBognOUc9qEZayQQeIYIniiiXVjiY4Q+AJ0G9c9LlM
Y1p3c0M66sm42YA+MDjfuGx7mBsSNaVJMKN6Y0Q9bb+3xpFiTBUFj3ds6EgrfXExrBE7fQ38Cpo9
sFexKZrRHuP1lNp/nQ7rDCC6CfZRxhzeNrYm0yeItel21U5YwwfdqwiJ/ufRkQXI9jxmxfAa+ZY1
hfHVXoALkhwnU5bQ1WNuQwXA+inTuYrFZK0O3/e9wiF7SP4V1JGXa/y0OOBIBwcPxi40kaCpDZQG
g/cuSM+07RnO1Ru0684227jzK8N9AbPev65xMLmS4fOpWMXxHhsV0qTH+ZhA24DexKvEwPGLMNZU
fGuwmLT80X84BHKiKvQNkwnl9X2T3K/UVZb3buffa7c68y1OuOQIyT7gcykjfza0AI1bJdaViRPD
BpWLezAiqK01k+WxecIBM4asX+C4vhcdcdV1k5OAarQB7Be+8Sruz3lvCxfjqFCcqg6fbOU5dQ+2
pvtQwXLuOeKI10Pf1YERWZdnMxEGce7kec4UfZFbsu+qsk1p3LQRmrNpWPrOK/iQs84M0Ua+VOvK
PEv8xdeJUhEre9NkQKQFa8Dq6/6yjVRINWaQVGryAI/XpDn/IsDJrQgG14ZDHSbKWp89l/AUKVzk
Y/VANk7Zexo1vQwtTAI/NawqoOjpiH+lMl0Ls8w+32iOqo0VcrueyaSWokn0YoyUNnZTolZTgvNx
mYW9YsFwLb3whde/spJwWgPC/XIei+9Ijdr7A8hPqcSUvIBaQu0zSgi7GbCXAxXH5vUiJWIMb2Jg
ECULPGqHFBUhMlhybg9Zjj0KkfeUTGr/x9cp6KpHw7kVmmPeDBns2HXV4H856zOwr4O+fejIkSgl
FUfC6t2TniK5i6jlcpgXOAHoCDR8OOW4md6eeJd3mY2VWQYj0SgOYp8x0T1wKJjHMcXKlQ30cUuh
VTaq/z2zNRRpBkWa69mf/hzrPspVniRNi5vpLcGGoFH3nJhL/0w69uh5TXICkor6V7A09tpuX55H
A37jcmEEwZNIHLuBhdcYYzsLy+gSUtKyP1R8o5M2g3YfRDiQll56fCTh5KjiyIlv8arUeqCV3tKW
5/hZzPw8+I70M+ZIyVvfgu/y58JiNRiRY73r2WYPvC3nyYrJbyjTI0LQOkHtwjSb6IsuqyEj+mxq
EjEuVsN/kyXTaortdSFB6SM0i8rws0jDBXwbNhilVbS6aELKVVeclXrGlvphz+0BQFvCyLI0eNPh
7G14Xo7jIZKPf/qx03wacvjgpMesCDyCewbrQfLii5p6kZSUtQhnptzYRlC/Ino1/hi8S6kMtFLN
G4VeCHacRBnY7PZyrapmAbUiSQ8UHmNj5LoWqJcJLorGEGxUSYKSCfYxZjkxQQsUEpdVUQCu6hEN
8RjCgXy155RTW9AMX/b6ux+rvrn/v2vKSbe4qRp8gpk40iBfLGUUB/+Q30jwqGQSaBvkfc1L2Jon
5BGC9WalKkMt6gIG0gWyd5wRB6DUThyGZ/cgQTLbqC+UdhhXHmEeR+ksopVX2lZ211DpdFkejgu7
b/Cdz0JCbaFT6fiArUyQfemjUUx91YaLDCe9qR/Oz3Pn+RyYrSXgKIITiqWXttC9UL8QHw8svyLY
k182nR1jwMsm7oYkaLSsYn5QRuaAT6mCQD5A09MkPBpc190y3ZPzJK6PVxk+E3KShLhMy4OxHTZS
jg6tF1hN7o6pTiB6RRst4VebGA4GB+jOPcoH1vxXZMRmLd2DfWZrKp22wVsuSPadnnMRHNqJFa2X
15iTXrbzzz3ckAnHQyb9AOsnL/rLvCtSjHC7aIfctdN1tw8DT58ysdGg95p+GCDsQnCqodhIJliZ
a0X5EFhA7Pqj4RbiqyspUnaX8e9645XE2RJwSgTuSvWIXES8fuL2tzIXRVk1NiYJyR3qNK3gmx8l
pOzBeMqXH77NGBxTCZTZ8Vx4fMzGDpucxzHsy9/Hn2zq8E2tj/JeAsLzzeHFzevp9fi+CONi73UO
3/RHbiJeGqiNNDMuRoOcDw0spi9h+leXFSlyi6ErDg+qKPhxzGH15fDEQ0r2tgE+5GEnF//5kOnW
EgkVl8LQcanbZ/uOchXjBqPAffZMCspQh3KpixvsTFKqvIwrag/i3AQ8XhRTH/ytZKckLvtRufAR
vY2PcOjon2jJR6PWgZVfbWta7D6K14zb8+acmN7ZudRoVBvyDjVwW/XLe7AdBzDzjwXc6d2duGnz
m2OmlwqdpTZv8UJZrxjc0QbdTfEfeSmb4qiKIvrU5wYKErVkkQU0S3aERe9p59nnSy/dJ92x7owP
hdmet1TKiVm0ZjaRwqzQHzcX+QECTTBznuyJkSBBsA56mpM7XyJf/11yEh/yuiD/FVQJng1AeG8W
vVhabSahg7SAi9kILhnpT8dc7cuTC9xPJIy3ryck01El1kKY1MKb6jZGh9/bQGfbqyl/Hq/w/lkW
WGT17arMeTUsifhZAmGnFL6znue79yS1bKV7EMMScrKiJ9SXOL+NSbu1Y3YXFZROR58QYQRZQilB
awl5/JkmE10drW2R+EWFutqKCoa3LNEMzLk6H3/DnJ5rq9d0XzmDIpTUKqRMhOtCfzeqni+T9ASV
7moiYRFPO9WDk9Jl005TMFqFXyS12zLshid1dcH3ctQKPUzjPrh7Pa+uC1jitbXNVIUZIpK/2BGl
JVzG7nWt2xzKT7Wb355jIL52jf87R+1KarR/NSCjGfedphtPVXnkVWaTJD9t8WJf4bR/U/pWeDsN
zih2n23XywAPYKnRhfvfqH3r8FWcM8PTDGG7iC6yeH4YqWbKiWRdNlHjjF3r/yhFollNzwraatZl
/sc9e4Lur89eW4Pfa0Fhy/1RLWwaGTFrsoWJEKdzDNWnQ9rk3TrHigGRPM5y1uO/s/o4Te44YE98
ngucWyOFNAZgNXLOmRvfhfhohzQsV2Z9RjwtZ6Snmea5NG6yhZ0ulDkG+iUMk0UBgK5BTnhW7Zw3
1LS15Soqr7VGCRVQ8bUhoa3/aAmVeJk3f01hA9gM0MVAIdGZB963jmXFBFB3P26XfPakk2HNblV5
Ad/iBWlLQx0h0oF/tVIc16fBPY259bM2P0FpxPyNi3+a5xCgo8D0efSFZH7umPyuOV0fFwqaNFtT
7jn7htLqnQSfwSKm28mJfEG+7zdsS/W+WghNWQ5x4wqEr8wnJq9BvgwgpkIoO5Yqo2Zm46voW4DO
aVbazzvnGjnN6tSqJIiwtkpZ+s1iU2oWb04sfkQ+RDDlds9Sffh1zUMhN0r3O4PGG53ZExcYxAgK
SFUVug6lSi1y7xEp8yr4ukt2io17IYgTku38vzrC6hAiIth79L3+e2gNX79KPC2Z7XespltZmqZ4
Roz3+P5IAh8XbwILHUS7nYL1YR2W8SbkvZlzp7cTJymTYb20olj6xVty11Y8YMn605SZdxOY9oxA
5D+yjI/LUxAuWRePuesZnW7UKOSAh+j4FONsXoByStvDgMqo+ICEZeU6K3lUgQC1baPAph0lzB3A
dJJLnUrJp3Psw3jA77OVTVj4sdektdm2XE9yP9xNUZIghn+LvJRI0hmbweVcOgh8buCFk66wPvHM
uuYmIIdX1mhCKZfM3D3g3LlokY1+Bx0I+mEt3di077A4YdrUFnCXQzp8qaB68AMiMpVw9WZ1wxfg
IDvsvI8z78glnRHRfuj8IC6CT/oQ4JlxpoGdTam4k4MWFS1XeyhWNpf61CXlhBm4pJ+XCTFX029C
HwJ4ZwGpgqrYyc0tnwA7/OwPfbq+CmySU3zX3COgJBVqxDuwh+4LKJlC3OxoVcrwxOO5Ku43NN6e
ChRLI90n0rkGbBxTdKLPxUamjZNthq0GCaACIIpwl5TdN1O8LriiiRz/IeyDd9twfI/A98LSVfph
83rpUdH1IYdwCCoX26/QMxDQMQesTawpgRiJ+Xkjum8arVgRkKb8fTzQta4eILmCsnhUdzpTtz6S
2G3GFzjjIymn2qxda57cXnEs7FN373TImilDWEp+P+JVSX9HZglo3WHFU3rjoCsqaxu7ab2iAvu1
z+1Wl45kVs0+blRr0GR62fIIVTLBPeR/Gh3DvSZpKtZk76R9T+7McPqpZu2kDvpyx0rYzuhDmrpR
Xlo1UiieoWTpnfkArTX+H+SKlid0mtAyWnxxb/yruy9i6C14oyvdMRtz/NmkNfxQnRm6hFLky0sG
681dGL7F+0Th5T+Vw24435foYWwpb8yvkau+siJd7FZ5m8b420AeB9lu/67e9X3y/PsI0FqKsaBS
hHIYdmvtNG+tir9PWYxPXNL31o6eOeNU1cdHCOgevMgT6m1U/yULUsDmkwIqLBIykIQchXKLnO2M
c/GbPvjnZKQkMUJ3PoNXMmYGhTAOVKr/Mj168wHj8AvheTU+x5Q5yqvHSW7z51IkWrBLhlzd09sj
I3jskNc8NSlP4TMjvi/6syx0+sEmyVzd7F4U1hRNdI6ob97yUoGzTuCCSnTCZKypRRtKHp2epFDG
4Tliz9PcDFTKLSugF3POov7qaKx+S9u3wKCB+GmdEfzgPiPMGuKRlaYD328nYtlz1vqpf4jk5dtA
RoWx5Unma1WAt8S1l2UMoWSXMVRo2wpp5Bc/9T9IpPA1W8fh4HV+j8qw5aQFXc/Heb7X8EAChknT
v5ab/kEc8d94KN/xheh+tushjHsKDJc17kGUNWHAHlhjDCsSQ6vpUIVmGoVJCDsg6Gg1bAQMz61n
fbGcUgOW4Y0U+TQVQnP9UfP54s8hqW4GiaAULTdyPNrGu6tG+lm+EcKb0ZvT6/aBr5JkmIHgDsHb
9WR2peedtzkO+xORd7wAi48qPds57FrTVj6feTL6B4MHZheEtd7zYUH/ZIsmY1jGwRW8gu+sJe1Z
YrWDwpxtTb1Ewufhcvk3NymS04d0UqCrQbI+/KFsK4dKMttDsDAiL/iaOAo+uwfvlMCHnWAH2uXl
77wGrQAC+305eSddkG0LBvkSukOhi+OULDIN7dtBtRtfWoaY6OIhNGVx4xUaF2fNti/VFCB+PEqB
L2uLyZL0++9/gegDmHN1a715TJLuH9m0yfx1W6lbtQRTPzkaswfqu0QfnRyymPe9tthplorD+wdt
LFZ4yqyVKh8dl0fEF1Y9T6JyhayseTBcDDUNRSjOsGdmBWof0Q/lCR3EmuaYXHSYkaHFtcc/5OlI
7kgi4/hLONc5/w6CHQyYnGjhnoYTzfeK6pC8lZJ4mp59fuDaMDzdBoDCyq2FQ/XITCicCbs1YeXM
7TyBZoJKzwttuoXKghc2gNLz+gN+Uj8T3BmHYdAg3IHNpMnOTSMGqOliNLU+G9bO9LresSjxPrQl
U4bqo3PVAaPw7GSvTSMU/bRFRokJ1guaxLGdN/I1vyBUzNnSXghSHC1Y/jJGCqj0FjRswhy9FJ81
Yd+c6z9RfDDLjcaqcXMSzUzcE3PPzwllCZNHeEbSz02HmnxsVYjpQYmebM7+305baVCxtvlaUeWc
Rp5SZfqKRLnpOgns1yP7bZza3yaMyo8Tb4xm46APmvBBaHsatJsnlLVxEI2wt9BDZ0M8UPDH7VH/
R0MH8VutNCQ0wHUjavtiBMTxNnV4OMCR3G/MnGZozwPJcI7aUqvTAzByh7MxSJbtlbG3RDn7BC8K
VPZyUbNlns/Q+LWWH/UQum00VhuAnBzX/4x1I5ULShD9IBMvT+cSSgz1LEG34fb13Ff8YeUETyO3
Ta23X/TS39ge/EQzVHVkaSC//sorXWu1s1xgcE2Md9iuibXllnCjczbO02IE44/A9yIeIfQMWa4a
mig/Hm3lRuiTCge203VQDwi5CIWYOEBwKljkqMGTB//cAq6NxFjeedn9/bHedSgw6xgmPa71aRHP
AcX62esSltjGK8hLnNxP6lVK5SD1YMpjhfBFjwLMxSTVVA3iXkD0F37mjgANbOVL2dtiHPZv+yHq
s2WDMlvPDZDQMtrAYVOJWfvQzy/h/EWU8AGaNJZJ/jUve00dFACdG0eAEALQj0cN14ShoHQjGpxc
+3Aki4bXvqeHi/EZpTyOBTndHGQUPw+0pJlfoDZV8eHBbpXcM5sYYMNyi5Bx0ZAvAT0NMpU4k9kk
l/2B3aLm2ulZERKCSz4DYa0HMEnBh0juERW9IFd5z8nWRhJmsB4q14H1w+4kkVFjSF0WSiQ/HwGI
xIwRfTXEA1Y9DpvtqViCydxSJz8aAbEyvVlLX7fRAjR7mdWJIpDGPy99xgBY/JN9t5C/E9WqWYcV
D+ZOg5jXiO1XeiwIh0s4WQ659jtJz3s4uimsDovmJBZJypgZD8Gi0wyT49aHO+W2+NjXQom9PmyC
UNpknq09oeC3rTvcXCBmMG1tYK/ssucSH94DW26mGkRa4AM+tyqoogypJ4FoSfbCGKSxi46npwzP
POFvtpXJE0LZ47tYXWRJQAn9mCax6cgzCDUq0/z5tBYVUfSVgilMZl04gVTzuSgMH4ffm5B+n7Ru
10/RGUOtQdMhdwQfqd2VunnGRmy/qVuoGplDHDn8HFzS7xtVNgCkRij3yshFE4UamOJ0HU2NfuO/
8PxQQAGan7GMYgHH5zmW6mZq0cZz2U/GmjQKTqXuMs3NB0sgaJ5PT1fW+Vm1scH2sBHWEOP5kdoc
zjOKv0P90BXq7yTMfaKzi6mmo7l1qm6kGsvyIIlPJURKYo0JA3EudHf8+FZjquoVmdSQIK/VfLkf
6VGAKrWQ3li22sBU4CLWZppjIv2w7hjV2ywPPxOgj+OOfyvRssMCWn1ned30CVG2HqgunvfixHtc
YozbqSWWYLMFO8yuteLZcwHn1AMgeCg2ItfDjBj4Kou4+GMHO1N72g/VneAQ7dZLFLjp2D0P/01S
+tOQ7dXnJ2SAMTbsq18FG8bgjapD1jjUDqiKQDVYMtZChGT8bJFrYOsL/76mzU6xmNuBCCKL7qaz
Vn/nLq4kJAG55fRC0GZR5j+e67pTpAUa7Jcrh4zbGufEaPRVCOiEtdFxoYlw0xitH/WOl2vjXW45
CEWZf5wfHDZ5DxeoSqpww4g6xYnKCuIK9Cc6XXTOkahnUgFII2hLCtK+MRkqLnJmiY4AWZfzwn/i
izKCEWI3avAO8AhVbJAI6xzu1xoCkXWNKNIH/bSeQFyfxRsZTufNEe63S4P5AG/EULvO
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
