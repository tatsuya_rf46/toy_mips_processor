-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:04:34 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_multiplexer_4p_32_0_3/design_3_multiplexer_4p_32_0_3_sim_netlist.vhdl
-- Design      : design_3_multiplexer_4p_32_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_multiplexer_4p_32_0_3_multiplexer_4p_32 is
  port (
    y : out STD_LOGIC_VECTOR ( 31 downto 0 );
    in2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in4 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    selector : in STD_LOGIC_VECTOR ( 1 downto 0 );
    in3 : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_3_multiplexer_4p_32_0_3_multiplexer_4p_32 : entity is "multiplexer_4p_32";
end design_3_multiplexer_4p_32_0_3_multiplexer_4p_32;

architecture STRUCTURE of design_3_multiplexer_4p_32_0_3_multiplexer_4p_32 is
begin
\y[0]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(0),
      I1 => in1(0),
      I2 => in4(0),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(0),
      O => y(0)
    );
\y[10]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(10),
      I1 => in1(10),
      I2 => in4(10),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(10),
      O => y(10)
    );
\y[11]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(11),
      I1 => in1(11),
      I2 => in4(11),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(11),
      O => y(11)
    );
\y[12]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(12),
      I1 => in1(12),
      I2 => in4(12),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(12),
      O => y(12)
    );
\y[13]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(13),
      I1 => in1(13),
      I2 => in4(13),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(13),
      O => y(13)
    );
\y[14]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(14),
      I1 => in1(14),
      I2 => in4(14),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(14),
      O => y(14)
    );
\y[15]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(15),
      I1 => in1(15),
      I2 => in4(15),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(15),
      O => y(15)
    );
\y[16]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(16),
      I1 => in1(16),
      I2 => in4(16),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(16),
      O => y(16)
    );
\y[17]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(17),
      I1 => in1(17),
      I2 => in4(17),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(17),
      O => y(17)
    );
\y[18]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(18),
      I1 => in1(18),
      I2 => in4(18),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(18),
      O => y(18)
    );
\y[19]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(19),
      I1 => in1(19),
      I2 => in4(19),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(19),
      O => y(19)
    );
\y[1]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(1),
      I1 => in1(1),
      I2 => in4(1),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(1),
      O => y(1)
    );
\y[20]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(20),
      I1 => in1(20),
      I2 => in4(20),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(20),
      O => y(20)
    );
\y[21]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(21),
      I1 => in1(21),
      I2 => in4(21),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(21),
      O => y(21)
    );
\y[22]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(22),
      I1 => in1(22),
      I2 => in4(22),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(22),
      O => y(22)
    );
\y[23]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(23),
      I1 => in1(23),
      I2 => in4(23),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(23),
      O => y(23)
    );
\y[24]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(24),
      I1 => in1(24),
      I2 => in4(24),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(24),
      O => y(24)
    );
\y[25]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(25),
      I1 => in1(25),
      I2 => in4(25),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(25),
      O => y(25)
    );
\y[26]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(26),
      I1 => in1(26),
      I2 => in4(26),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(26),
      O => y(26)
    );
\y[27]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(27),
      I1 => in1(27),
      I2 => in4(27),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(27),
      O => y(27)
    );
\y[28]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(28),
      I1 => in1(28),
      I2 => in4(28),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(28),
      O => y(28)
    );
\y[29]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(29),
      I1 => in1(29),
      I2 => in4(29),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(29),
      O => y(29)
    );
\y[2]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(2),
      I1 => in1(2),
      I2 => in4(2),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(2),
      O => y(2)
    );
\y[30]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(30),
      I1 => in1(30),
      I2 => in4(30),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(30),
      O => y(30)
    );
\y[31]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(31),
      I1 => in1(31),
      I2 => in4(31),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(31),
      O => y(31)
    );
\y[3]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(3),
      I1 => in1(3),
      I2 => in4(3),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(3),
      O => y(3)
    );
\y[4]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(4),
      I1 => in1(4),
      I2 => in4(4),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(4),
      O => y(4)
    );
\y[5]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(5),
      I1 => in1(5),
      I2 => in4(5),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(5),
      O => y(5)
    );
\y[6]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(6),
      I1 => in1(6),
      I2 => in4(6),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(6),
      O => y(6)
    );
\y[7]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(7),
      I1 => in1(7),
      I2 => in4(7),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(7),
      O => y(7)
    );
\y[8]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(8),
      I1 => in1(8),
      I2 => in4(8),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(8),
      O => y(8)
    );
\y[9]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFAACCF000AACC"
    )
        port map (
      I0 => in2(9),
      I1 => in1(9),
      I2 => in4(9),
      I3 => selector(0),
      I4 => selector(1),
      I5 => in3(9),
      O => y(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_multiplexer_4p_32_0_3 is
  port (
    in1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    in4 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    selector : in STD_LOGIC_VECTOR ( 1 downto 0 );
    y : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_multiplexer_4p_32_0_3 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_multiplexer_4p_32_0_3 : entity is "design_3_multiplexer_4p_32_0_3,multiplexer_4p_32,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_3_multiplexer_4p_32_0_3 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_3_multiplexer_4p_32_0_3 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_3_multiplexer_4p_32_0_3 : entity is "multiplexer_4p_32,Vivado 2020.1";
end design_3_multiplexer_4p_32_0_3;

architecture STRUCTURE of design_3_multiplexer_4p_32_0_3 is
begin
inst: entity work.design_3_multiplexer_4p_32_0_3_multiplexer_4p_32
     port map (
      in1(31 downto 0) => in1(31 downto 0),
      in2(31 downto 0) => in2(31 downto 0),
      in3(31 downto 0) => in3(31 downto 0),
      in4(31 downto 0) => in4(31 downto 0),
      selector(1 downto 0) => selector(1 downto 0),
      y(31 downto 0) => y(31 downto 0)
    );
end STRUCTURE;
