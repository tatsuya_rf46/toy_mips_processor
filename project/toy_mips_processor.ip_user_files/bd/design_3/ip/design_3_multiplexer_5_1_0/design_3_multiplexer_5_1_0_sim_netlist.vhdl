-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:01:41 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_multiplexer_5_1_0/design_3_multiplexer_5_1_0_sim_netlist.vhdl
-- Design      : design_3_multiplexer_5_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_multiplexer_5_1_0_multiplexer_5 is
  port (
    y : out STD_LOGIC_VECTOR ( 4 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    selector : in STD_LOGIC;
    inp1 : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_3_multiplexer_5_1_0_multiplexer_5 : entity is "multiplexer_5";
end design_3_multiplexer_5_1_0_multiplexer_5;

architecture STRUCTURE of design_3_multiplexer_5_1_0_multiplexer_5 is
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \y[0]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \y[1]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \y[2]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \y[3]_INST_0\ : label is "soft_lutpair1";
begin
\y[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => inp2(0),
      I1 => selector,
      I2 => inp1(0),
      O => y(0)
    );
\y[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => inp2(1),
      I1 => selector,
      I2 => inp1(1),
      O => y(1)
    );
\y[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => inp2(2),
      I1 => selector,
      I2 => inp1(2),
      O => y(2)
    );
\y[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => inp2(3),
      I1 => selector,
      I2 => inp1(3),
      O => y(3)
    );
\y[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => inp2(4),
      I1 => selector,
      I2 => inp1(4),
      O => y(4)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_multiplexer_5_1_0 is
  port (
    inp1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    selector : in STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_multiplexer_5_1_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_multiplexer_5_1_0 : entity is "design_3_multiplexer_5_1_0,multiplexer_5,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_3_multiplexer_5_1_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_3_multiplexer_5_1_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_3_multiplexer_5_1_0 : entity is "multiplexer_5,Vivado 2020.1";
end design_3_multiplexer_5_1_0;

architecture STRUCTURE of design_3_multiplexer_5_1_0 is
begin
inst: entity work.design_3_multiplexer_5_1_0_multiplexer_5
     port map (
      inp1(4 downto 0) => inp1(4 downto 0),
      inp2(4 downto 0) => inp2(4 downto 0),
      selector => selector,
      y(4 downto 0) => y(4 downto 0)
    );
end STRUCTURE;
