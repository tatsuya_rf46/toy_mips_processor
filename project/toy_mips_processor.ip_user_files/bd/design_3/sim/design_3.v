//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
//Date        : Thu Sep 24 20:31:39 2020
//Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
//Command     : generate_target design_3.bd
//Design      : design_3
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_3,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_3,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=77,numReposBlks=77,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=22,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_3.hwdef" *) 
module design_3
   ();

  wire [31:0]alu_0_alu_result;
  wire [31:0]c_addsub_0_S;
  wire [31:0]c_addsub_1_S;
  wire [31:0]c_shift_ram_0_Q;
  wire [31:0]c_shift_ram_10_Q;
  wire [4:0]c_shift_ram_11_Q;
  wire [4:0]c_shift_ram_12_Q;
  wire [4:0]c_shift_ram_13_Q;
  wire [31:0]c_shift_ram_14_Q;
  wire [0:0]c_shift_ram_15_Q;
  wire [0:0]c_shift_ram_16_Q;
  wire [0:0]c_shift_ram_17_Q;
  wire [31:0]c_shift_ram_18_Q;
  wire [31:0]c_shift_ram_19_Q;
  wire [31:0]c_shift_ram_1_Q;
  wire [4:0]c_shift_ram_20_Q;
  wire [0:0]c_shift_ram_21_Q;
  wire [0:0]c_shift_ram_22_Q;
  wire [0:0]c_shift_ram_23_Q;
  wire [0:0]c_shift_ram_24_Q;
  wire [0:0]c_shift_ram_25_Q;
  wire [31:0]c_shift_ram_26_Q;
  wire [31:0]c_shift_ram_27_Q;
  wire [4:0]c_shift_ram_28_Q;
  wire [31:0]c_shift_ram_29_Q;
  wire [31:0]c_shift_ram_2_Q;
  wire [31:0]c_shift_ram_30_Q;
  wire [31:0]c_shift_ram_31_Q;
  wire [0:0]c_shift_ram_3_Q;
  wire [0:0]c_shift_ram_4_Q;
  wire [0:0]c_shift_ram_5_Q;
  wire [2:0]c_shift_ram_6_Q;
  wire [0:0]c_shift_ram_7_Q;
  wire [0:0]c_shift_ram_8_Q;
  wire [31:0]c_shift_ram_9_Q;
  wire comparer_0_equality;
  wire [2:0]control_unit_0_ALUControl;
  wire control_unit_0_ALUSrc;
  wire control_unit_0_Branch;
  wire control_unit_0_Jump;
  wire control_unit_0_LeaveLink;
  wire control_unit_0_MemWrite;
  wire control_unit_0_MemtoReg;
  wire control_unit_0_RegDst;
  wire control_unit_0_RegWrite;
  wire control_unit_0_RegtoPC;
  wire control_unit_0_ToggleEqual;
  wire [31:0]dist_mem_gen_0_spo;
  wire [31:0]dist_mem_gen_1_dpo;
  wire [31:0]dist_mem_gen_2_dpo;
  wire [31:0]dist_mem_gen_3_spo;
  wire equal_5bit_we_1_outp;
  wire equal_5bit_we_2_outp;
  wire hazard_unit_0_FlushE;
  wire [1:0]hazard_unit_0_ForwardAD;
  wire [1:0]hazard_unit_0_ForwardAE;
  wire [1:0]hazard_unit_0_ForwardBD;
  wire [1:0]hazard_unit_0_ForwardBE;
  wire hazard_unit_0_StallD;
  wire hazard_unit_0_StallF;
  wire [31:0]logical_shift_l2_32_0_shifted;
  wire [31:0]multiplexer_32_0_y;
  wire [31:0]multiplexer_32_1_y;
  wire [31:0]multiplexer_32_2_y;
  wire [31:0]multiplexer_32_3_y;
  wire [31:0]multiplexer_32_4_y;
  wire [31:0]multiplexer_32_5_y;
  wire [4:0]multiplexer_32_6_y;
  wire [31:0]multiplexer_32_6_y1;
  wire [31:0]multiplexer_32_7_y;
  wire [31:0]multiplexer_4p_32_0_y;
  wire [31:0]multiplexer_4p_32_1_y;
  wire [31:0]multiplexer_4p_32_2_y;
  wire [31:0]multiplexer_4p_32_3_y;
  wire [4:0]multiplexer_5_0_y;
  wire [31:0]sign_extend_16to32_0_out32;
  wire sim_clk_gen_0_clk;
  wire [0:0]util_vector_logic_0_Res;
  wire [0:0]util_vector_logic_1_Res;
  wire [0:0]util_vector_logic_2_Res;
  wire [0:0]util_vector_logic_3_Res;
  wire [0:0]util_vector_logic_4_Res;
  wire [31:0]xlconcat_0_dout;
  wire [1:0]xlconstant_0_dout;
  wire [4:0]xlconstant_1_dout;
  wire [3:0]xlslice_0_Dout;
  wire [5:0]xlslice_1_Dout;
  wire [5:0]xlslice_2_Dout;
  wire [4:0]xlslice_3_Dout;
  wire [4:0]xlslice_4_Dout;
  wire [4:0]xlslice_5_Dout;
  wire [4:0]xlslice_6_Dout;
  wire [4:0]xlslice_7_Dout;

  design_3_c_shift_ram_0_0 PC
       (.CE(util_vector_logic_3_Res),
        .CLK(sim_clk_gen_0_clk),
        .D(multiplexer_32_2_y),
        .Q(c_shift_ram_0_Q));
  design_3_alu_0_0 alu_0
       (.alu_control(c_shift_ram_6_Q),
        .alu_result(alu_0_alu_result),
        .srcA(multiplexer_4p_32_0_y),
        .srcB(multiplexer_32_5_y));
  design_3_c_addsub_0_0 c_addsub_0
       (.A(c_shift_ram_0_Q),
        .S(c_addsub_0_S));
  design_3_c_addsub_1_0 c_addsub_1
       (.A(logical_shift_l2_32_0_shifted),
        .B(c_shift_ram_2_Q),
        .S(c_addsub_1_S));
  design_3_c_shift_ram_0_1 c_shift_ram_1
       (.CE(util_vector_logic_4_Res),
        .CLK(sim_clk_gen_0_clk),
        .D(dist_mem_gen_0_spo),
        .Q(c_shift_ram_1_Q),
        .SCLR(util_vector_logic_2_Res));
  design_3_c_shift_ram_9_0 c_shift_ram_10
       (.CLK(sim_clk_gen_0_clk),
        .D(multiplexer_32_6_y1),
        .Q(c_shift_ram_10_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_10_0 c_shift_ram_11
       (.CLK(sim_clk_gen_0_clk),
        .D(xlslice_5_Dout),
        .Q(c_shift_ram_11_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_11_0 c_shift_ram_12
       (.CLK(sim_clk_gen_0_clk),
        .D(xlslice_7_Dout),
        .Q(c_shift_ram_12_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_11_1 c_shift_ram_13
       (.CLK(sim_clk_gen_0_clk),
        .D(xlslice_6_Dout),
        .Q(c_shift_ram_13_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_10_1 c_shift_ram_14
       (.CLK(sim_clk_gen_0_clk),
        .D(sign_extend_16to32_0_out32),
        .Q(c_shift_ram_14_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_3_0 c_shift_ram_15
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_3_Q),
        .Q(c_shift_ram_15_Q));
  design_3_c_shift_ram_15_0 c_shift_ram_16
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_4_Q),
        .Q(c_shift_ram_16_Q));
  design_3_c_shift_ram_16_0 c_shift_ram_17
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_5_Q),
        .Q(c_shift_ram_17_Q));
  design_3_c_shift_ram_9_1 c_shift_ram_18
       (.CLK(sim_clk_gen_0_clk),
        .D(alu_0_alu_result),
        .Q(c_shift_ram_18_Q));
  design_3_c_shift_ram_9_2 c_shift_ram_19
       (.CLK(sim_clk_gen_0_clk),
        .D(multiplexer_4p_32_1_y),
        .Q(c_shift_ram_19_Q));
  design_3_c_shift_ram_0_2 c_shift_ram_2
       (.CE(util_vector_logic_4_Res),
        .CLK(sim_clk_gen_0_clk),
        .D(c_addsub_0_S),
        .Q(c_shift_ram_2_Q),
        .SCLR(util_vector_logic_2_Res));
  design_3_c_shift_ram_12_0 c_shift_ram_20
       (.CLK(sim_clk_gen_0_clk),
        .D(multiplexer_32_6_y),
        .Q(c_shift_ram_20_Q));
  design_3_c_shift_ram_3_1 c_shift_ram_21
       (.CLK(sim_clk_gen_0_clk),
        .D(control_unit_0_LeaveLink),
        .Q(c_shift_ram_21_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_3_2 c_shift_ram_22
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_21_Q),
        .Q(c_shift_ram_22_Q));
  design_3_c_shift_ram_22_0 c_shift_ram_23
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_22_Q),
        .Q(c_shift_ram_23_Q));
  design_3_c_shift_ram_22_1 c_shift_ram_24
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_16_Q),
        .Q(c_shift_ram_24_Q));
  design_3_c_shift_ram_22_2 c_shift_ram_25
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_15_Q),
        .Q(c_shift_ram_25_Q));
  design_3_c_shift_ram_18_0 c_shift_ram_26
       (.CLK(sim_clk_gen_0_clk),
        .D(dist_mem_gen_3_spo),
        .Q(c_shift_ram_26_Q));
  design_3_c_shift_ram_18_1 c_shift_ram_27
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_18_Q),
        .Q(c_shift_ram_27_Q));
  design_3_c_shift_ram_20_0 c_shift_ram_28
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_20_Q),
        .Q(c_shift_ram_28_Q));
  design_3_c_shift_ram_20_1 c_shift_ram_29
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_31_Q),
        .Q(c_shift_ram_29_Q));
  design_3_c_shift_ram_0_3 c_shift_ram_3
       (.CLK(sim_clk_gen_0_clk),
        .D(control_unit_0_RegWrite),
        .Q(c_shift_ram_3_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_20_2 c_shift_ram_30
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_29_Q),
        .Q(c_shift_ram_30_Q));
  design_3_c_shift_ram_20_3 c_shift_ram_31
       (.CLK(sim_clk_gen_0_clk),
        .D(c_shift_ram_2_Q),
        .Q(c_shift_ram_31_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_0_4 c_shift_ram_4
       (.CLK(sim_clk_gen_0_clk),
        .D(control_unit_0_MemtoReg),
        .Q(c_shift_ram_4_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_4_0 c_shift_ram_5
       (.CLK(sim_clk_gen_0_clk),
        .D(control_unit_0_MemWrite),
        .Q(c_shift_ram_5_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_4_1 c_shift_ram_6
       (.CLK(sim_clk_gen_0_clk),
        .D(control_unit_0_ALUControl),
        .Q(c_shift_ram_6_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_4_2 c_shift_ram_7
       (.CLK(sim_clk_gen_0_clk),
        .D(control_unit_0_ALUSrc),
        .Q(c_shift_ram_7_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_7_0 c_shift_ram_8
       (.CLK(sim_clk_gen_0_clk),
        .D(control_unit_0_RegDst),
        .Q(c_shift_ram_8_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_c_shift_ram_7_1 c_shift_ram_9
       (.CLK(sim_clk_gen_0_clk),
        .D(multiplexer_32_4_y),
        .Q(c_shift_ram_9_Q),
        .SCLR(hazard_unit_0_FlushE));
  design_3_comparer_0_0 comparer_0
       (.equality(comparer_0_equality),
        .srcA(multiplexer_4p_32_2_y),
        .srcB(multiplexer_4p_32_3_y));
  design_3_control_unit_0_1 control_unit_0
       (.ALUControl(control_unit_0_ALUControl),
        .ALUSrc(control_unit_0_ALUSrc),
        .Branch(control_unit_0_Branch),
        .Funct(xlslice_2_Dout),
        .Jump(control_unit_0_Jump),
        .LeaveLink(control_unit_0_LeaveLink),
        .MemWrite(control_unit_0_MemWrite),
        .MemtoReg(control_unit_0_MemtoReg),
        .Op(xlslice_1_Dout),
        .RegDst(control_unit_0_RegDst),
        .RegWrite(control_unit_0_RegWrite),
        .RegtoPC(control_unit_0_RegtoPC),
        .ToggleEqual(control_unit_0_ToggleEqual));
  design_3_dist_mem_gen_0_0 dist_mem_gen_0
       (.a(c_shift_ram_0_Q[11:0]),
        .spo(dist_mem_gen_0_spo));
  design_3_dist_mem_gen_1_0 dist_mem_gen_1
       (.a(multiplexer_5_0_y),
        .clk(sim_clk_gen_0_clk),
        .d(multiplexer_32_3_y),
        .dpo(dist_mem_gen_1_dpo),
        .dpra(xlslice_3_Dout),
        .we(c_shift_ram_25_Q));
  design_3_dist_mem_gen_1_1 dist_mem_gen_2
       (.a(multiplexer_5_0_y),
        .clk(sim_clk_gen_0_clk),
        .d(multiplexer_32_3_y),
        .dpo(dist_mem_gen_2_dpo),
        .dpra(xlslice_4_Dout),
        .we(c_shift_ram_25_Q));
  design_3_dist_mem_gen_0_1 dist_mem_gen_3
       (.a(c_shift_ram_18_Q[15:0]),
        .clk(sim_clk_gen_0_clk),
        .d(c_shift_ram_19_Q),
        .spo(dist_mem_gen_3_spo),
        .we(c_shift_ram_17_Q));
  design_3_equal_5bit_we_1_0 equal_5bit_we_1
       (.inp1(multiplexer_5_0_y),
        .inp2(xlslice_3_Dout),
        .outp(equal_5bit_we_1_outp),
        .we(c_shift_ram_25_Q));
  design_3_equal_5bit_we_1_1 equal_5bit_we_2
       (.inp1(multiplexer_5_0_y),
        .inp2(xlslice_4_Dout),
        .outp(equal_5bit_we_2_outp),
        .we(c_shift_ram_25_Q));
  design_3_hazard_unit_0_0 hazard_unit_0
       (.BranchD(control_unit_0_Branch),
        .FlushE(hazard_unit_0_FlushE),
        .ForwardAD(hazard_unit_0_ForwardAD),
        .ForwardAE(hazard_unit_0_ForwardAE),
        .ForwardBD(hazard_unit_0_ForwardBD),
        .ForwardBE(hazard_unit_0_ForwardBE),
        .LeavelinkM(c_shift_ram_22_Q),
        .LeavelinkW(c_shift_ram_23_Q),
        .MemtoRegE(c_shift_ram_4_Q),
        .RegWriteE(c_shift_ram_3_Q),
        .RegWriteM(c_shift_ram_15_Q),
        .RegWriteW(c_shift_ram_25_Q),
        .RegtoPCD(control_unit_0_RegtoPC),
        .RsD(xlslice_5_Dout),
        .RsE(c_shift_ram_11_Q),
        .RtD(xlslice_7_Dout),
        .RtE(c_shift_ram_12_Q),
        .StallD(hazard_unit_0_StallD),
        .StallF(hazard_unit_0_StallF),
        .WriteRegE(multiplexer_32_6_y),
        .WriteRegM(c_shift_ram_20_Q),
        .WriteRegW(c_shift_ram_28_Q));
  design_3_logical_shift_l2_32_0_0 logical_shift_l2_32_0
       (.shifted(logical_shift_l2_32_0_shifted),
        .source(sign_extend_16to32_0_out32));
  design_3_multiplexer_32_0_0 multiplexer_32_0
       (.inp1(c_addsub_0_S),
        .inp2(c_addsub_1_S),
        .selector(util_vector_logic_0_Res),
        .y(multiplexer_32_0_y));
  design_3_multiplexer_32_0_1 multiplexer_32_1
       (.inp1(multiplexer_32_0_y),
        .inp2(xlconcat_0_dout),
        .selector(control_unit_0_Jump),
        .y(multiplexer_32_1_y));
  design_3_multiplexer_32_0_2 multiplexer_32_2
       (.inp1(multiplexer_32_1_y),
        .inp2(multiplexer_4p_32_2_y),
        .selector(control_unit_0_RegtoPC),
        .y(multiplexer_32_2_y));
  design_3_multiplexer_32_2_0 multiplexer_32_3
       (.inp1(multiplexer_32_7_y),
        .inp2(c_shift_ram_30_Q),
        .selector(c_shift_ram_23_Q),
        .y(multiplexer_32_3_y));
  design_3_multiplexer_32_4_1 multiplexer_32_4
       (.inp1(dist_mem_gen_1_dpo),
        .inp2(multiplexer_32_3_y),
        .selector(equal_5bit_we_1_outp),
        .y(multiplexer_32_4_y));
  design_3_multiplexer_32_4_0 multiplexer_32_5
       (.inp1(multiplexer_4p_32_1_y),
        .inp2(c_shift_ram_14_Q),
        .selector(c_shift_ram_7_Q),
        .y(multiplexer_32_5_y));
  design_3_multiplexer_32_4_2 multiplexer_32_6
       (.inp1(dist_mem_gen_2_dpo),
        .inp2(multiplexer_32_3_y),
        .selector(equal_5bit_we_2_outp),
        .y(multiplexer_32_6_y1));
  design_3_multiplexer_32_5_1 multiplexer_32_7
       (.inp1(c_shift_ram_27_Q),
        .inp2(c_shift_ram_26_Q),
        .selector(c_shift_ram_24_Q),
        .y(multiplexer_32_7_y));
  design_3_multiplexer_4p_32_0_0 multiplexer_4p_32_0
       (.in1(c_shift_ram_9_Q),
        .in2(multiplexer_32_7_y),
        .in3(c_shift_ram_18_Q),
        .in4(c_shift_ram_30_Q),
        .selector(hazard_unit_0_ForwardAE),
        .y(multiplexer_4p_32_0_y));
  design_3_multiplexer_4p_32_0_1 multiplexer_4p_32_1
       (.in1(c_shift_ram_10_Q),
        .in2(multiplexer_32_7_y),
        .in3(c_shift_ram_18_Q),
        .in4(c_shift_ram_30_Q),
        .selector(hazard_unit_0_ForwardBE),
        .y(multiplexer_4p_32_1_y));
  design_3_multiplexer_4p_32_0_2 multiplexer_4p_32_2
       (.in1(multiplexer_32_4_y),
        .in2(c_shift_ram_18_Q),
        .in3(c_shift_ram_29_Q),
        .in4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .selector(hazard_unit_0_ForwardAD),
        .y(multiplexer_4p_32_2_y));
  design_3_multiplexer_4p_32_0_3 multiplexer_4p_32_3
       (.in1(multiplexer_32_6_y1),
        .in2(c_shift_ram_18_Q),
        .in3(c_shift_ram_29_Q),
        .in4({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .selector(hazard_unit_0_ForwardBD),
        .y(multiplexer_4p_32_3_y));
  design_3_multiplexer_5_0_0 multiplexer_5_0
       (.inp1(c_shift_ram_28_Q),
        .inp2(xlconstant_1_dout),
        .selector(c_shift_ram_23_Q),
        .y(multiplexer_5_0_y));
  design_3_multiplexer_5_1_0 multiplexer_5_1
       (.inp1(c_shift_ram_12_Q),
        .inp2(c_shift_ram_13_Q),
        .selector(c_shift_ram_8_Q),
        .y(multiplexer_32_6_y));
  design_3_sign_extend_16to32_0_0 sign_extend_16to32_0
       (.in16(c_shift_ram_1_Q[15:0]),
        .out32(sign_extend_16to32_0_out32));
  design_3_sim_clk_gen_0_0 sim_clk_gen_0
       (.clk(sim_clk_gen_0_clk));
  design_3_util_vector_logic_0_0 util_vector_logic_0
       (.Op1(control_unit_0_Branch),
        .Op2(util_vector_logic_1_Res),
        .Res(util_vector_logic_0_Res));
  design_3_util_vector_logic_0_1 util_vector_logic_1
       (.Op1(control_unit_0_ToggleEqual),
        .Op2(comparer_0_equality),
        .Res(util_vector_logic_1_Res));
  design_3_util_vector_logic_2_0 util_vector_logic_2
       (.Op1(util_vector_logic_0_Res),
        .Op2(control_unit_0_Jump),
        .Res(util_vector_logic_2_Res));
  design_3_util_vector_logic_3_0 util_vector_logic_3
       (.Op1(hazard_unit_0_StallF),
        .Res(util_vector_logic_3_Res));
  design_3_util_vector_logic_3_1 util_vector_logic_4
       (.Op1(hazard_unit_0_StallD),
        .Res(util_vector_logic_4_Res));
  design_3_xlconcat_0_0 xlconcat_0
       (.In0(xlconstant_0_dout),
        .In1(c_shift_ram_1_Q[25:0]),
        .In2(xlslice_0_Dout),
        .dout(xlconcat_0_dout));
  design_3_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_3_xlconstant_1_0 xlconstant_1
       (.dout(xlconstant_1_dout));
  design_3_xlslice_0_1 xlslice_0
       (.Din(c_shift_ram_0_Q),
        .Dout(xlslice_0_Dout));
  design_3_xlslice_1_0 xlslice_1
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_1_Dout));
  design_3_xlslice_1_1 xlslice_2
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_2_Dout));
  design_3_xlslice_1_2 xlslice_3
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_3_Dout));
  design_3_xlslice_1_3 xlslice_4
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_4_Dout));
  design_3_xlslice_5_0 xlslice_5
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_5_Dout));
  design_3_xlslice_5_1 xlslice_6
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_6_Dout));
  design_3_xlslice_5_2 xlslice_7
       (.Din(c_shift_ram_1_Q),
        .Dout(xlslice_7_Dout));
endmodule
