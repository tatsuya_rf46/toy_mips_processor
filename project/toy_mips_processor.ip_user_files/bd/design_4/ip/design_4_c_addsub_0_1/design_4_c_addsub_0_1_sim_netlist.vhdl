-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:21 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_addsub_0_1 -prefix
--               design_4_c_addsub_0_1_ design_3_c_addsub_0_0_sim_netlist.vhdl
-- Design      : design_3_c_addsub_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
aC36OE/OYurVJlS5J8mBQs1msx1IzUf73uqgELn3E/TQwIDA4l5ZbpQkiEOG3krNQTNh0Agjb4eh
onxV/GRKDeacHtdJCk62vqLeDi34lN7LytuGIhDfyfuN9H2TeRx562A1/Xxgumo35di/i2SKf2hc
ZrgaT/9j8zqL7krFEj35BV3MKpluk2FOM1SoGS7/HL2H/jpdvMmieAqIzA8xWa0nu3+e7VYwACk6
HTAHJTVbStmY5He9SKFF3tDyfB3xJvuhXU8yzVTbhaWnphDqmkBKIDrvEc2MuMDibB5iNpNBn94e
8NchlzR3sOhHNSnnhW0hTDaRPh5Yh4osf7nW0Q==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
5bFHFmsC01adZDWb6zmuRKE7EfFGTQ9T1u9e4jhjKxh49HgyJKfpus7dSiJSwp8Qs95d9gbpMa4y
MinnFL6+wtKpO3INvYRrzxgwLQvzWjpKORBdZGAKD3NQOyIA38135OutpCEmGwAptWkkA1mZGrHg
JnIUo+MxZHi15NyBaMIX0TcU2RMpnSuAcSveK4codOh0QZxLaxzM7Iwz8nB32tejXgNAe/B32OCy
jHX2bNQ75JWjAdShUcuX6MxWNi6RZA60TBcZTJ5c+i3NTsh95jpFmYHku9uTDtbMaikWkJfVKJh9
T2nE21yGgoJGc9VoSogKZeCeF1gInr4PXVeC3Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 14144)
`protect data_block
4s4GWYDFVhK2Sh5/Su59k185HmP8QazYaLonvyde68IrBXSYQPxQkHZuz8xf6CE7pbyEjoEbLq6D
YcPiOXl1yXyu/Rb40XrkSTBbfxcNlJr9aw2nctWzlRa+VieZfObyEWmRBmnrTeKXMf89l2VQSyPQ
PJr/RzWMKszNQdo1+jusOfDpJykwQNeacpHohJpYjcttUNK4f+lR1V22iv/41dTelAlIfkekrrIy
vu9SFjdwjBMePMznLkkr0TMpndlZAHG3k+z+lcn1CHwoy61Bx6c/I6gu9n1NHXPgegfrtnzjxr7i
LmG1dPauYiROUGOXtMBp+fURbbS/3fn7p1/LhIIUAf0fYjzEekS/DmKAyiw8bZiiwXM4mzY9vFTG
GuZVIwiYXnGf+R6wOhSKqsoHLTDp1dtv2Jk7WWfIch1UPZRP9V437nmZjJ8RdmL+aoP5hzwtn43a
+ab2BnJzIwKf9Sh/TDLamvwnqToIJ2AnrX9XtIPIOuuxSA9yM37U/3NKrzF8zzh191CB4ZnUzPj0
DOIKidAa3b4ff4KH9Q9qWZPcPa+021CfIjAQWaqvFtBwpNyLZ5mytZkIHKzxGq295TtX8wMK4n6Y
pecyDloNw1Qb06XZkJfoGWsOjIr14ucWnuLmQgRjsZAGTW1uuB7pRCF6hETjg9b9BmnxlyM/qOzs
bDDRncTwGgTfGZiUXo+I7pIUYLYMvtcwPMbAw5TV5SeEVBAwTC7c8lGOt08znt1K/7Cco0YkYRkt
BOpNwmOVScSwTx3I+odfe+LyeZR5RK/YVFXdPl1V/m1vJTrFujOQRMf12k1alRN2LxAH2aQEzt5v
EmbTM+3s+zxAD8DKWdCaEdGUDlgi16hCjptumP01X4c/sgTZg+OCNMKZfLet2L8d9GJEuop+fjgS
BISmtBXt1tyRY5KLMkhOFXkcR2GJeGsqAz7xw9jQtVV2whFhCOQpIS9HWiHxAH27+64EhQNCNklk
cYZCoAZ6nFCk7/0E+FWyIyBTG2yfj7eom/6nzTYYsirBDlWSDBlyDj4mzi1Wqiv0OeboRYZkfMul
eTQuYLbFVbK5cq3Qon21xYo3v/RRPSVp2uHLuk0s9R2XEDFBKEc8VVVU6sGTdn82A/UAqlUMKMJu
4xxLV0T60UEYZaTELATG1yQhDx1WJ6al+XgCuubpORunK8K98u2DetMhfyCnDAFCQNe+0XxTWUE+
ss7NpL6i31YgYSnYFmMk/L/9V8pFxsj5jRcrWViYDoDps+oK7Zkv0hBqRvApY2HRwmxA+o53B8n9
/AXXSXPdPpzMytfKB4YRDJJwNbiYQAgCFmwrr7hTR8rCWOxFAFNsXeOGzqzpz1u79fzKniReoEcb
WNudbzl7nmvvg+guu10/DlBg7ZtAJYdUf/uwaKkBish0vYK502j7YgT9riSt3HHyTZiQyXypTK92
P0Xk6l6JWOYZJeCxTHVzAIJJICLFTvcRCN/be1tO3BnAt5/aNCaCHp3oqYx8jXgqfFAbtKe868SS
6vfM/DfQuIVGk86upQillb7AoWa6N+HLAkE7mQaCJU4PYjmiObW3XDWsstPmIsnUf0l1j3Y6hFnL
Sg0XRIyAbgif1qzvjkZ/7v0XasAS2EqO07j6gpyG6rWOF2i9JSBAC8hMtD9QG0ReEodB2YOrbeV+
PTlcF+LmjMXyaK0GDEXkXY1/rK9wVHH2PQTYuLe++h+i0vpCzpJBt4mfr7XVtOZ6Q2Y4sMC34M9X
9bEzFNFtRmMZU/pe93SZB9+FKuMbz51aYf1OWMnw4FjuW5UVLJ7WeKj4erfoW28WwSbQ7BLIIEch
6t9yv62WF/Chv9jGaFHoRxreMEwjR92AR3xRcsBfoSAREB7DberB93jBQaNOTwFhFmnWFV7M4if/
pzJlckfZvgn/d4WIq3WWMmd72F/K9diGA+bO2J39BIbOJuRiE9eeARLs8iBUH50I1nNCBeuZs6nw
jggh59YgLPFPiCOt+Hby8G8CW62SMIV94Ck4OvQvxRX06O5HUOwYftfcs/I0XSyIUhfqnL1a/Evp
mIBG/+4fHGq5/zj2ypUA14a0OD1/GKOMFHeJ1OqUC1ii1OBa/I/z5SPS6j+2FLZdKb+Zx2sgFE5z
r731BKWIAZk9bwZIE7+N6WbO4s5wfF05LBfZd3e9J6HLrbsTyQUF5uNnMZBw7sq1mUAnt9/Ok8Lc
Jde+SBlIdUO00PRYTzHfNl2sExtopQFMWUKaxFitxlEMSGJS4lIZjsdCh0KnAuHOCzFVt+CJUptS
003Zhw5z0lRmucCzVP3M8ugHmg459Rg2pxYf71wA5PIIIhqz57kKelHadbtduRlYXILQUcnPlT/c
BihP0PM+C2AemuLB+QQpjW55sOgcRlwIddBXXqGN+DvHGe4MEiR+7adUqYdCkc+AnA4TVHS9bIK+
EgYBXSV9JWADVBiX55xx5n0WKxIhkVgDx0K2YWBCNDLOUt+BrBhJ2J4whjr6b1aaMMXsxWRkRTPl
K5FXIc4bCAyksaoAF1EHjW+ZTB5QmGnGm7eT82J84e1f5BmtHg3TFMiUXMDJT8LCZHjUyv+j91U4
UnCJaKcAXIAzYF7A/PO6LJlxYP7O3aLF7AS9Q2CTa0Wy1VfvsoO0wT3CmILUW/pfU7tTdu9Ir4H4
Nt7OIMW0fAfOKfPp/M9+Jx2nEtXEexr9aSX6GuOTFLUrT4KvjCX8Tm8QxaWoLxZFWGorhX3bCvRc
481/PQCpR2zq2lkUtXWOCfteaqa7T6J9yosLh34uYsgN9SYeOXSxHeBQ3LELr4VsuuI2bCTXzWvu
Heia26rkgsULwZ4wmUnobHbEd8A29B/GOJBzRF/X4ZJNvkzkXCKo+GJYFHAmZntp7WJCaPMfisbw
tcG9qdIezUM2vlH3TQfZgjF8kM63H3HE583Dri5oE552KVaJi2j349yAha2VqBIQquPEuHYEgtHH
B6A69uk4sa1Vq62znormaCRil3n1MzfxwIuI/aPpXw7HcMp2IyBlEJQcCvEwZ80nAxPPF9KL3FUd
sNHmLkIqsT8GuUNutDew5L/AcRd2g0QyyK5HIwdSrGID5l7UjfD5aP6Lnn7AeBPeez5vlx0JSjWq
b/QwnXLZLAsZGDpkdyLOBNYRDNmjQ6Gw3OxAx/Yxaw/R+Za5IwrHlpA6tXoU9jmJ077C5E5fWi4b
3D1W/sVvg99LzhxGcrLNaF4opECV+tVws3ZIMwW9Bn0leOukKrvdv3CCEofnT0Gd2LkJFhVFLR2g
9JbqpVt22qfGeDp9fm8pb8f4y4anYHyOvROTNFQlJz4vGw87rpZUeufs0DYt+yqtrjGbOGB05OMC
fW/s4qwHofiC7eJ5QgKbQw6nRxc1pYLbQA4gCy8t/A4PbOkV9ISQzXKqkuujvwMVWxRCke7oAjmF
HD+TWlu21NlcrhF0BvnOiI/Ecyb3SHDDEsxJcmWFUPYSMv5mJ7US18726SXcAuJxCxuEzZxo4Arf
zFHPcF48iOKAazXwhSJ4JegHVHwY3E9xry2jQFyXdkBYlPlbT1yHRoZpljlHb/d87+plmE9LWBoT
/a45E6mzHS0ytgzDGBgExbDz1uCno9lG/cyv6iCoMXMD/RBv4HcjSfJHoB491zRXhW/3L+k5vtKB
k2FvoifkD2ewuj3PR9gUlDxjr19IajjMYUU/DLM3zbMtKe3zVt6qJzvkY7d/XQcrQtkC/P8SkseU
27QIrDSSnsHomLrRTw2sSl9/VgifDtIQnHPwtcKW/O84zkXpVbUu/S7Neo7hGIkb4pzWNUpqayd2
8miINMMEdyCrBOJt6xNSpCh6kEzWW1+t4qN90M5ioVC1yUDhFjtsOX8E9r+sTBrNuQmfTTkb09eg
bad1W09aJ7/HwysmlEpDtDTlwNANNOxmqo2G6me06IoclVcJDpUwVGkpDoFo0pJ2VoREOXL4S3Gm
uK8QmBfxdJ3+80zhOHiu09wfa+NgUOU/vevgxguLqF11fTjTYqWRS8Ty+sYaipv6CI5z3GxnGtEg
RG1VGPJbeWS20B40BavfcZh5WBTjMHilIDnmfa8TwbpBpwdJH2lJVoA38/+J7gIXlXRNZTGfnUnj
DDgBp9yKnUmM9xeH1DvlqrOWfwf3oC92wvNnq+hQjNP/J5mvU54vIslhionOk0Uiu371yudf4afe
sN/fkEpDPWq5aQ4Q/A/FkknChv3tOAO0q3kvs00VTWjki/gx4g5kaGZ6QpI2839aZSjZivJDBSVx
svKiMoOqc+SOxQuWdIIPv+q7zRkyi8Hrf7ApWW5zWAHmFlemV9OvFbIxLI1h8FWRMed6O04bgstZ
jHNmZlkjb9w2q881fx2K206ExMAmPu+DEiF1f/+zIyQZVNPtuJX36oDvRR+TU/t7mYXkU5HEC645
gjRT5jvKHUTa+5KphPZxnB/chN4P2+7K20G8bXXcwNE5qZi8Ar70WMK9n3hcPMmY9y7XEdKyIw1v
0+kpplr2xL6O8xI2J0zZ5cnPB+9tN6h/BFc+oMbTsUokSTbY12VSv76r15hoBinuoHnvFodMC6/o
RMPl+jA/E3r7LLlvIvF7h9lm6hO5l8jbBqNKLj13RtKiuzt+Ehw/bQv9CdpFH0+cfqSbwgYPawmc
O/vbLzEPzu++Uri1L8kP7EY1iwgsp3iCjYDZ8iOenuyh57MEISvJj58JAWtbe3lpFMBZNwn44Z5v
VkwwgzsG8NoCjvk/eNgmHQt/TzCXCuVE+Nw0qxAgewTDItjcAePu/vuP0OvuzKw2YP2uGg66o1Fm
/Pfu0E1SsT4JkiIiLPgW/T8o5csD4U49jxfcQLnGotcjc0wWIQnfwwIiuiPw11wxMzcs5tutz4W2
escuMVScsyFZPo/88/6No2PEYlffK1sjP2LyW5jwPaTt08JZ5ZYGk5hA3fTe5U2bx6iQ7j+PV5Nh
sBhXGmPMto75fA1NaJs0Ziz1TTn+7BsOkkcYF5nvLShY0nSRZU40b3hyS3x+DWxDSKLL16B8jGHX
nT4eOzlWOgtN5ijSor39f4o8w03DIUyHEyCjP2JNuIQGFm9ZyUgy+U5f5NvUYZwutrGxJWW7kpou
FPU+N6ny1NH3d2vcQwwgOG502SWPb9FLTpTBGRCc/RsF0Mb14YdbiemKuUs2c7KnEHSf0296maA5
VO+peWjKYv2ip96XpKU/yJ0bEzn6873VJ1zkLhlkhfuWyxJkngqchxfFKY5z8nI3RHPwniAJuRbN
WFUDXklLMc7r6Bex7CDgJs5z2jHxCZxoQTcfAz2dsQ+z97miaCO5eANeaRgyafnsWSDwsld549kM
pHYB0migj/DA5rApaKQw0ni+60oVjCIgbh2JzyQ2R4tLLT11JQwrTPiyZ+tt6yBUQabdKxEZey85
C1Ej6275kfxSCkRUtl56YpKi5CiCPu6glfY5FrC8kvtuExHgCFS3CoxrMXNCr4DqSmh9imH36FfH
pVlliF/7cUjBf3+huAkrZsqpy/yPS7WZHMJv8SVazDX9kQ3+qLNgfWlkzh1XjC+6ZnsAvhaajNl2
jRkiFswwAOuNfeUx260a9DCC84jvOKFnML/b5VrzeK2IVK2ESrwIO3CK94BKaJe/eiIM9kB1Wx7f
nnCty59JF90IzKcjXH2VjzlgylfiEGcDeccauO1BXXLvLTSWBLzusOKC6hxmaMghOd6CkIewm1yG
KwSP46b+IyNsOMQPMTfsRHC4HN87O81gLo11jhZ9z5sYECKgm1N0w/OJie/hsnFycpzOp+7MUqbY
9901qcSuZcucCil6Kq8eNwZuKlGhCtRKOmphAamnN3TqzYOacR5cE84GUD0qbFZePxANQSeC9h2x
YDPGgrLOuJzsCnjpGN7fmFaXF6fFiLwVlHdlaHlz8OCXbYsmjzx+hInZ2qCBNtEI+eKi0GQq33V0
dbSMfsuIuS8BI9B8Tui8q0ivpQKyuMPRnrkY+yXTaz3YjgTfgnI1IbXeVHKtV3Mo5SKCCW6F+oRY
JtLfkWabz8bEvPx6TtZGNJSrO4YYLr5JMYz4QQ/PAP+Tt1pTVhNyna1kYWCU6EHtEU3SxhN4kpO0
SlK7e4vPj8hlf0CTUd5GqF4alcKSnUQBm7TOJIDf/QGfwi047l55lPoWegVEbvDkMSv8MHsQiX5T
WrB7MYnhKI6envK4pLnlCSie2H+ea0v6LfQ1MozJzUst5jZ/avmt/QsHYZP1QTXLtIVWKazhUWL7
INwIOfICUiSShZg2p+mS8jxRKDxeJfXhCjrN+X2/+WeVarXRF727mkFXbEHc4swROCHYHD/mMo6W
vv4JMP2iMKCPqb2CK0XkjUkSdzr8FR6Xy7VovslUpF6u0Q+9kZ+f4Uk2psXdRTMdsqnLvxeIbdDU
yiCo0AIqudLcX/icc2it4vzFsqVjrsWf1NS8Lyev9torjoWDEh2DRWexRuIJ+oQzyfvGU3vkP5q7
JhKsc80Q7UL9QvdQcWFB+Z2Zxk8LUgFTUSoI6/ITuYlEt55XtCw+snz1cXC2yfUza1sflEOVb51X
DZmTlHD4ecZXs31Cv7i4XA7tpzitAZiLqY+gO16jZzeBi2fbN4/3UMzlYGH7TCWUbuT43W4Qlbml
ldUi1fSHrH6L9LjZ9a5GptoALR2zvv7SG9db/XJzDFT5iddBfipJQ62ID0kXuq9GoKwknqrc62m5
rTcJR39FC4GnfEYUCfuKvrH3T4Hw5/KxB28UzeD2xymgxLNprK4aNs7BdtIBMklk8bz581DXIowj
9QDUEv94lX9fgX9MIK1EdX+vWxbmyhQhIb1wHlRzozzGvwUMMfWQ9wtvfH5/r/Pn7F4twBTI2wMj
a0rgDVoKbYCTSbrGVAAddW09COhj/G+YCobC7WN0CwtDHxkqBrnUgPuFvLSIZRqjCvshqcchQQMj
ZJN2Jk3llJ4mTULbXVfOJ5PQ1EHRC6sImDuk6LIdIlE8D0CusXTytjEPxHfvUrJy4pzlnDSmmql1
9osgyX2vUM57TepUQkzMDT+cXs6Rw8HAnejDG6yxkEHTiRyLnt1r1asfAJ9mAmZ/YgRpVV9e1qWp
UMOOWnQ8L1kK6OA04aGriYmmI3jnt8cdNYKxJl2IYHV/lKkprbHK/fvkevEr1hDPQN+9swGjY85E
syscUEW8lVRt3q1r8LJ8XTHQIRQBIfKHa1KHpn0JzgTCybVUDg5TC3UAMW9Df0aVPdTTCD7ffmcc
6DGId2vVKkq3LNYZ1a0f/5Tp0H64z8/0PxvrIMPEz1fl/nI3Ylbr1pylITkbO+ObjmbEHO+dhDuq
fO6pZbveQSJrTRMC3wgNjQkpibyvfY3BVAkEISD2KT+KsFapP2qnGP3059DvuKf1dReaJZGh1QhL
yKpQZBjHNr1FhvAn0fKq4dnLxx+UPjfURrGBlN9rj94vH9YGU9EMEDF5Tal1HlOaFXknZr6CL+Id
rFhXkVeRcOxUhuK9aXot7f9ItO0s30DkInD6kbBBcrxzxdtIa6YSK5Pd12W1w1oi8fmDvE5p53TN
eUV5MNRjX3gcRR3ByTfTaelNICrKG9yXBjNo+4VLLyjtjT4WQnQJ9RWQk9swuBbfxg1vjWr9AlbP
aMtgUCMfGGGVGovIrsifnulIXwsaCan+nd/pPsRvwVbgxHGTcv1V6sBHJJcQlquuWkxL40h+J/tB
kiX3enzDZ31Mdx5wCXDUr1e2dK6+C/VF1XTrm1o+ml0nDLgby0bSakn1ZvOVTPBUnmJzLDLZq540
cU1SFVWCXAsVemd2DycxVtkvRz7u5ineivkPP+BumvHX5T1A1AsLVTomu4e3cHypLnNjxnZMsiVH
OhXKvHWbWeuW6e/yojmWed/pwvvg7cry+041ZL62gSe94R0TXNg+mCv6bcRzbZRxgiHWi1+ginu0
rRJl+Nj5/G5K3JFQxmK49eFtLpowuF6HDp0g/OH/BW1n5Bze9DsZAtf3/KV3OjOCZwzHE9NKoMN0
zWqHtUdpInro6XD+v9lj5fdhKAuDC04dPvOOxFAMUNuuObtQ7mFKtTeLVp0G9CQ3PZv6P4CDsThw
MSEWmIn7jHzacW490CucXU7IsaUqrxl46atjC5NVbMMLgNjPQjzhvcY4VsVbFZElJyAq/ovY8X1L
d7Q62Vs1bUXFp5Q5XdYMSLh6bC39pAvdQMbIJuRbl95T9M4XEWD3KlvysId5gbQivrxZwJO8xZVZ
0W/sQPNqOyZ84SoJ6E3pYYMrt2wjDeyu3xj+yNf9sJP3PGLwvisDG/n26poaFeOiAzrSfBEnekoe
sOJItF82k3kjvsM/DHrpEGN+1yohK8GFRDCf34D9TXopJQOXJTiT9o/MsoemiBg3QIcYdUDNE+M3
z/GvkPzMV3a9n7H3WJFEo2JJbpW1n0R1okhVC3O1BiIEuxHJnXS00QCPZiq7ZDNQssDn+5qY9/di
P7uUdZ26cMV7OSQIMwxifTdpvMRHQwLoDVIAwxL3TDt0z1tUqed1pOtpcQgVhTKOR06IcWCvgcm5
uVY+v2XBdjcDyR6WJ5x7Ql7epHttQDqjXgJ40QZHcyk+X0HuqmDixCKQZ13+BsE+nWkrfeyTM7UU
TRuXYofTt/bJrZPmI3/eXVfZh176i28z7fdF6eLUCmpzxLy0ICYFFkUenHAoRTgGmOLEDl/tZmnl
O87xFBhylkyNDWNAieGCwbRFaJczvWYrX0JIkSlJ33rpWcTNEjKDK+7Sqaj+rFvF7w1f151GyzL0
xW7KsM/REjQqGZm5q/O7QRQQonpTQ8fU8RvFWkr+vXz51OxzSG8BkQnXQuTFbPDSWkpmB+fRKQqt
V/c3jyDZi6j6qbtfGCq8iMfZz2MyS88zOmtKZkIbcO2KM7nIMKgJZJ0UpdKOSYIeUXO/ygZWHDsS
+nS9QpH8zQhUxfy+8CDHIU0raLzd/QM00Na03vwVc0aq7ug+taG2dv+vy4mPOvJnc8N2N1xqnAh+
CNIpkX+oCQQSGYr7KVldIpCGO+8X2kGvceAMY6Jl1YoGinHPzEjI0ic684XoBsTxuAAsDqTkbh0p
IgR5ai7c4O4e4T6iJBwvj2BX59e7KWdAxLXVIhQOiF6kz4mteZAqKvmy33CM19Qny1yabDSxezN3
RUhWAHDc8frECJ3YZsl1Q0TiR8E1OKNxZIM4q/dK80rJCE888eCNH0PSN1chRkWAWn4OwB2ttVYb
5mZhLnN6QLg1Nx44abjn71VP6jVXpWZKF5hgH4xW2xiF5Ei95jr7VK2rxnH+zBlZtxlLlUKTGkGh
Rb8qik9Og5vjnNtDprYhE4Ek9DFLan0q3dOPyC6lClkdTQvlalSaOOHJ276ZHFXM64wVPY/LNJ+a
kc6BpKn3vH7AwiDnZsJhvnUY0S1IdGXJLM1Nbew0c9HZAfEZRZFglYn9aft/GZVFnox1mJI2COit
Gi8E9RN+8Lh53jaEiW/tqD0xUixPvVp68AxFb707SSdKFDLr3J7WNl6eLMOkII5NKygTvliXyTKO
ePIO+8Tla6N/k3r7HPvEBKJz3l2lndsTe4mTYQQXd6iVPBy4/MDWutjA4eU24zXySpYwh3rNsc8W
L1v035iQfc2nKxAMMgSgCz48e+hYW5eQY2z2684C1pDotZu4GJL4uGLtg0OLZFO0FdCLi+BNL3HI
gssihzayz6VOcPT5GPgOmO/s9B2fH3/XDs/gkP4psWAeoslfejEpAjIliR+onFvStgXQKvLVCApf
G2oRNconwLy1PVMLms+g5txDO++SIYS745ILzF+YpHzIs3SSvfi6YyCQ/25S1wH/2jJy/hT3xUp/
pdBkl09VN7z1mCObXHhPu6fJRzuncYgsnQwrR9D3SOk7yCTix5W/yVPavGs8NSskSHrlvDsCy6Ll
B4ao3r/j5NESwTrfVQk+iB0doJ6L/4S3M9vBOkqzPKTaNPhoo4CGSttXjREwPablNInJ6zMI89gw
EwNhGyNKU+IM0IH3rRYzWnYW8NnMQmP9XayLaXy8THjkQqfBdHExUGR21KtnsSCqKUcl4MDu2a2B
h4rSeM0LLXlAv4imBbrKvva8RI1cTywrfQiB01UDyXMjpX6jNQ6U7GwKAMWjBJdSHp/xujmEuW6O
r65LAOcxqo4JxseDu9m5KDZCgf9c1oQSiruRL3qgpk3NUv7LGUiKZXZnjQV3u95mLQIQhCpGNA6Q
K8eVbEI0KYg89jJVYuAGyCEeqtRfhox6zuf9xyWTBTmwZnLlS2pfyeWuHcJzYVXTShj3klLOPZrx
OGePn/dF7sAZVh51fTn3xowO+ZAogJ7b9i0DPWB0xa72+M/eveGi/JbupHkIoTFJvS22z7M5/Shp
DsbWHvTQMmgwAt8bRQXCKRZBkzvt/tWwnNNBr+W/8eqcArsrAMlc5ObU4bU2i/L/J5hTiqNhw/Qd
xXyzdUy1XtQw9H52XOZvA+ixQXyHqTFncPsdW2N3mvWJg019dqO2gx46byvIHrMYIaxFAwvzjO4f
q90PqWt6lR42uHzSsBHn8QLuDv3ehOJY0IbrwW6IF5IN68SHzqh53d1CqY0O55xT0idKK+I6FF2R
DGtdWgx8YpQEFbd5gPmh8/GweuQTCHlBo5/uuXGhoDZA0fAckxrGAaR0yn9gZejS+MIxQ5Al3LGD
bKwRF3/4QRGWIeoRkrES3nvYodot1ZvZ/MFYSRtcBoiywtE8JLt3ftXypYSo6FD0q/iT67k/88wf
W1b04dWNf1pISuhd9a3yyigyppwsThY2g6pWiNEiunfiPFkQHTd3psP5yaDZaiYk7dHAb2pfYExK
ljlep/mNmqwAK70QruNMu0CGeOPkzSnf7P5k7LY2/NRL8xC9+g1WdEpANGKcnB/2r6eJPa4Z3Ple
p1eC/FOCBwJjFQRMf4TY7l3XtDV511/Lgdp54rhIG6FboDemlX1hQ+/odL13r8JyiryRZSP84OGb
LWkkOoir340oopG0mFIE1vS6VXNzCTIXpkEx8xD++DahF7q5jVhjznGMH8hqcPWQ2/42kEUsqF4E
m3wLWWM4OW7JpUVwO5hLWrwImGlWXJHzJKjrD6PYxF0BQf7FkHYTjsA+BJp2bK9MPb79GG5ArJ6i
rMH0VqG76d6nL69agMQl9oYlmhBLZgxtzvJsT8eKNTDIfYUeeAbJdppxgxG0Bx24k9GG04m6Ktop
SEQHimZ2zmYHe+VD4AhLqzHbH2W+Unb7AxoK7zKY5cER7yvl7DgCcN1ohlRfj53hI9CFR8iQ3t2U
lguyjdgRH0YEQ6IZAPaLJlBD6XT1BFwNHTs6poXNXtWOe5qXq8mDiwf70G07UpkHQgDS65McxRPp
kcwQ6SyrOTDdrFxBfM6IZkkmShUkLiHnFPSiZaUPPZlGzydBSZAtO07YcikwKmfTcEj+STKUM+Ax
Gu7lkhp3WIGlLMBAbL+XcK+sRTFzQRBngu1pL+IHUjYa9Td7P2NTos7USVI0yBIiQMjy6dqYbVaG
JjWi2nQOzOIFovsGuNjBNPOA90LFgNuv7ce8oS1DuWq3jBqO2NTzg1zvq+ZUq3vZGTYhjS4LdVz6
MNtMNMmRlNbcaGeZGl/PRvbFV4n7crTItnTlzCxaDqHqPu1o40PkhkMJ88Yp3ITXQ1Vh1q2hwAKM
ytR4UNfxMOCye6cfI4QNWSJfLqsjJgG713ERso90hevbw64IPeFCbQ6nCvgugxaRnCgZfKZOp+A/
LwOL1k/ILW5sp/KxIXJK6skkJJBB4iwhsnsCdy9/kKAz6yuA2UF5oQt2vh/dcSYhvfrj29ouX+MD
Qcto9GBfx5C84SSOvF7isQ7jaSx5Bu7i6usqmS0Tj6yHulDKLABCSEg4kCtz3vQOQlbD15XXIucB
y8c34dKyDk6fAgcwFMs0J1IryurnHqT/ojVcZCizl1MFYoneNptNBXqpGYLWoHrg0vTLgli+TRmK
jybmL/3JIS9Rv5mxQ8RKw2FA0jBEvYtyxw4Zr8KkEid13gsFTIho1y8r+dBTO05HC3QzCke1iJPQ
B+bAQCWoMCVhKFlP5Wf8d5oBkEJNXGi0zpZjNOiF3ioN/eWciIHlPd1PhKWDCX+uny9goujGtZ2S
sjgjxhVszJLw9nHLDETPloPKFn1952HD41MOYlrKMFZXKCMS6JQqz7ZaWUo/j95W1jhwF6o8+GYD
4tew7pjKTFza0MaZNmUqJ9zMwgt5pS4FTyWxjzd47Hvg/wcqCyeBxbfHj3++h3AkQ9nUt7Ju7d/L
aZFw1CwuSmZ10eN8Ws9BNGRncyAotZLdAhsjzhUc/Y68S4y5N4ETHLxzmh28//tpaNJQtXGcUGXx
1crY/a+S2nbUZRJeDNqG7kapMumqQyRbZSdMnjja+ZLUj5DGZELy/TeHTyMh9c3OLEpBqVsTuK+Q
E+0/0fRu05Oz6OWNxiIVAdnyHGtEzrm+AiPERYGfc7fRUcJersm08sZNU5t9+BWV8uQJ+Ym7ZCw4
l7JFxYSUtQlAlcj8nYeLWdILa2T+Jr7ipsekBmAE/0hL0KAgW7sUfl4L0ugp0OKknSgdP+KVHQe4
TOGLpXcmqVNy2NvdxGF1COQ4z0B9S1KXp6+b9XxwcvpqE44HSwAsryshk7NcC7sOLL4y/sA73xf+
/yNzSWmBZTwIsRVUsFq2sqjegYj+vpiA5rgcEBOPfrzIwN6HgYGyVqLSNLTNBhOYi2KacR1EUqKi
Neff9c+y4cHL7+ZSZMBtx+TaKSC+Qnl6ikqPCyPoCispTd8xAotV7UL252I5oLc08sbTLWwN1CTO
oFd+6tfdYWQopW0RY5qqZz6qSHvvbQrlLUfRxTzdvozVtdR6E/rF2Dsly2QsAvGMajl8kh/zIoVe
497Rh+QWK5ewDiKGcDNL7PS/cXB1P4ojHEqiFS+JwDbFrG8R+og+354Ms6vLbCOITHwV32Papswr
R4gY2rtdOYw5mPoHeS3hBd6xnAbbav4D/10lS38SS+mi+sTMKODODX+4i94Vtvp9R3GN25v4+/GH
ge973DRIjj7TNdIgxxO/6nDnbmLRMQG6mSfnzwNNPRAOEtU6VYbrgmVd1LQGo5BaBc0RfuhyKwBo
YMJkZ7rhFW51pW/rDwB6EgYxeyKIFBhhdnhDuuF8fYhC19OAeetzUWWHzWMHt79YjrDQ7DugpZAd
adVNZaivIna+/U9OGGCMptgSe8K3uZrnASueU6y5eQidvbwGXVRa0YW2/AdZZvPIX51bDkxQ2IXx
ILcCrcOBl7USnPvnrH0dLbVxJwpxPBM48lBfLuKNid63nslHzCCgHiZ9BAGwXqY1oUCg/dYYoTZE
hOtqurrmvmJGZdcxC2Jo6DJiA/5cRZY0g7xQfCfc1SP9EBmWbGO8wnf20N9U/2hQ/zV35P7C0hIz
XyJJM+DKnQU24Nko41/4wks2wOGnY6nzMwlaybrz9A2QY/RGruUf/QRpQZjFG8DcV5AbsM9oE3ee
yH9x70Z5pGsA7kgUgjRr9QvlEYIgKbBvkEyZtB3HO1ysIKuFM6RW6S6rSh1mU922eUL1Q1lQmpKm
QDEYwERNEqViTMJ9Lr+M4znI3OWHVgKH1RQP2pVGmabY9S2b264F9iNHjgfeRBxZYGcsWbMdf1V2
MsmXDc75Ke2OxFEGS4cFOPrvPC0MDmVaKh2I/exmDJn8+RBdzgtdMri93zF152ZMe3v/aB6U/wLf
CXR2ieYnKTvrsWSAneSyJEvHQa7hVy6t+NhbBRuhDDzDzgIwQrfC/FfyxVfEfEzi9uwqCNfO/Akl
E123Cm09zZJeYVqSJtHZBXmivQYF3OQlbrvnN5G2/AojQjsvX9m7Sn4h16fNs0Urf8EePyxRR9Ja
NxglTTTq7grU+FF3UcMvq2Oo460TIO/+GvFMI2Mcw5EDLw9dEShpSRG0BMmCvfWE+xDJMoKijwfe
7L93CUMDsLgGzL1ZPzl6nKas0kjfGooQajC5tNRAMS+bwvEXLAog7t0cTjiqMdBAcNqwuN3+nC8c
aSJpz1KWot0CL288/f9vdq6CgViOh42TmkJfzN57p1j+wzmYA+6KIiJqyyng1D8/mYcpxC87f2mI
PdHCxCCGc3mJPUvc5SVB4Wpw87j7Nw9GQa/P59HyFUqCxzT4Jkb3wmmz4DrsHZZ/mwZ5IinEeTN6
NhET3yH+w/eDjV5LjWxeKz78konUwGH4TrVH2VxcxP+D3hNhWLam6RrCIaZRjqagHX52pZPiE6Lc
R758tQwcPw9jyOrRRZD7joM4DrmRB9RVG/1PgdHS4J16AKWtLMNjFIDafbBdUl6BtoSgx9pegEPu
3YBhhX9Z367nHfKg9bwqKP9sRLiCXEnWR5xwFdPUTAMwzsATmVFKv12SwcTGGssIXlwXlLwvNhoX
b3E8oteqO4w7rpZNdHAwjTEyDlwHVKFabHmk6sYlv8Yo2yrtLKvdiOFj5PGj4bo3x68hdlr5TlVI
FdbmXKVL+1bhGWbycYnhT9KPbwyJBArEWRQwaBF4i2TMm8JfUt+QqBnQwbzjCT07q/iTaJMlx8b4
j2XJp6qp8pyFTjPVKJQ6w8iU26DrZYrZOJdYqsndyAe4VBaqlOR42JxxtvNYTd+hjveVQcepaHuH
81DLeZkrSLBrHza4XMknMaVg16jHCsKTMxravjvw7sXJW2EDWrh22txAgLOJSL8vTgNxAZUKbMKN
DGfSRbvn2WHeGRrt6E6CuWpA5C7EfphmOWIbDlFXNvaei0vgZwMP9VTPpciyYrpX0n5pYbHNCugk
D12iZ6IJoEHQihYSn8xsIxdGezmBJpcazC39oCztGUBu5g+eBn8dQqEwwWtDFkpOxP80qU0HNIpT
5mRvrhbqOQx/sXHesrzOWujNgsAcVjM5ItRFhnpL3lkd4g1Ke8zNi01YwW74DHUjFlfisrWRzRMs
SuXROe0zAjlQTYJwGKPmaojtP+f+rWY5LR0hO4qhdyqQN+ItKqTV3TdzG4CTw6LqmR1PbuUo5K6F
U77ARp2flRMM9YMupZBpEEqjxuDmkIcBhwLL8REHbzR3xzRtHn/u8toNPYHFw7mcs630ytCAulMM
tmaWV6qqxHGTIXhzn9/R8TeknQGL0eXxeHvc3ZuUEtsrUmACAgr9ItyzXF4d89d+IdzS2b3CuDS5
bjkjX+0kIzHE/bE9snLGNnv/aglrLyQmPy3FXNwc82FQP9JCgmJ6aemFHv1RqlypMIBHq0iZ9BjO
EelfjfHQKtiw5nyYge/bx4gUU9gx8D6+twI1e+hKmQMAem/gHG7OblaRZME8Om3NfsCksi9FccJY
fsE79JkS4pNz25AyWYMLwrhp2qFoFRCXc7DrDZ1Fj3AzvZA3j7ywZy2ALbjUJqBfGYP/ykT0WJrs
/B7GFT9+yT2JDFxH1r6I+CzT26w91LRr8A3gMd3yaBjf1w6HwsGeOC7mZfyLGhHf22h/8rA0WT7/
AVP2qecAsSij2sIW33cePclZ/L0EyC+RjNLDt7VE4NoVFeQZpzwck1EtAqeuzeeN/qyQ5FZX4YkK
G8rM5dnBPHrv1b0KzC1Loo/GM1faH/rspRurj1KUyQ8/G2PZxVa9bnLPNrNwaZJIIs4LwXQFZyvL
ccRPkBocj8E5512pEXwP4CbAH07Q9LO0jqAYDwKlXbGH7UI+KK+ojJCv5dyO8olEoELC/Gr18d+3
C5u2FQj7OOFJJombjk+S7Fcj6R7AEomLx+zyak3JB4nifi3g/hNvNdwIk/KHu8NUbF1krPMDU8bz
u0YbCfldOAwPVRapdv+lqJ5DOG90PknEk9YMVqOLEGHW9Kk//HQk2/qAqBi95h09AV6LwGPsTbw8
iP3sLfUsOZGFr9OtNHyqRSfIujAh7IT7Q9kO03k0JpSa0o11DmhpLCpnnK5V37J3N+xeSJj4gdq5
444Wc3QBPtO/aENeAGvtnJX2vLyrbmS8Axc8wP+rmef9sw9r9aBmWYC9N/U54eogCqpuK8kYt+TZ
MRXDMRyw8OYMgdm1xSCCYvPx4FX/g6OyDXiYdCL5UWQa1vvlOgS00pHONSMwUCkSJ/ElGhbQIX2x
w72PKHEd9XT5hPFsn4OtYG2UMDVLbZLUDLXyT+DIodCvZhdVnN/nhm3Y2aCLlT0vALrCCdedhcwl
XLKv/U/wAucLE2zsNp/18/8fOtdpsaRZqc71nALGDaUAZXRdoIfqAHH9jrZBvkt0XMPUMq6GDAc+
xhkdTseG/ehBVwew3Rof1KMhxpOUTRGYpw6R4lpJW54nHx7/KArCGeMFE+77NwmA/vCIdABd7f/f
1nH4SsORUiOZSZIPMeMtXT9+uzgosUYrvmDwyEGABeTfQdW/gssOkSTUh9dw27PhFbzCoPSfSw3j
X+V1jNLO0XeT5T7LgtD0VwDB1I1UyrTImJDV+Dhf1OXVf53NTSD3Bg8yKMYgOcufV5o/GKRKP8rn
HrNoik6T5+ckAf+ZTzEx4PExFC4oPuln6G4jRs5jB1r7cJpyh4xytbvOlke4fTHZcXqG2IIyDCnG
3A7JLwiGQSYwoZQmzPfBbUjj4R8S8g07i47jjUfTOB2rQfcibPO8DH8yc1qxgMleWZDXM7dHc5kg
rgHfERM+r3Ce5jqoowmoVF0wSIa+mAOutFLTAejIdDYdM+ImypX5UAgA/RJD/d3135VLY1xujOGC
opnGfR1TO0keDgeDAXPrnn6Z6Lxo9Yx2EPqQQSxYXnTKC3VBpbrnlHiF4fomIzYICvYp/ZnCs/Gg
auMZkeIcWzdsr9gQlM90eK9iqmDmFgwR3cVjQ5fxUNxhQCKVC2S6afLsnlLq7hEBl3IrPN7ky+qm
FqGqw5C8E5EkOoCncRTE/Fp4FC8i8o5G0DeLs9LN4SVy24b/33gtj3fht/kiW5Z9KvnCsSHvxH9o
5r0HZzNYTxrHTU9U80kGpuPhjVPFT7tvtOa9lxWsAfweB+eODhf8xEAiL3KvYAH18ydutmtzdhLl
cZKE43fsd5YQoNzwQrI0DJRunbyi5lu6T9qlfevHAif2r4CkjvlYmywZdFZ2+A08KtrkX8DNTftF
mqBhyEOS/aSEsgrQqdz3ar2cPXnBwYNfvswTkLfccLmMVW0TkoGk9SRdGII+djPA3vaYmD6yyjNN
M0xGGK4a8BjWo1h0Z+w30EGT2kC6p7wmnmR12+ur0U/QOYZ7BEMhl395AmrtuPnMAqd0XuV/ZCo0
wG8NqbOEetqvz4clxLo2wFFnusFeM0+9DulD9yhhwxVuUYxp8MotC2Hsm8w1oBUXfn4GlE2kPqd5
dL+LiredUWVp8Wr5DlzCsjEaze4hSy0F8Tmvc3H2+W+D7Prod7vn64LXehcNrlyGfHPaHKOsjKRZ
3+9+0K0zmMakbejAgdJ7Qmu7cbhr0lDAYZLLaFz0RTNP9gue7c41bybSKELT+k53f/iDB2gcQI9+
QWDvima1pB17fX5KbuavbP+tdeJ4kqZ3EzZddbYWlYNj/YYimI5YSIk6gMF9vsVAHTciF9PED8vl
HHiYxJBAiaeYlUswbRprzipdfy8FK1987EE2zA8gFfi32nZWSvuXaVwxKAcEaerrRy88PKNEoGmU
yf+ZUF8DlB39n0YT/9ENely+h51UUdpNZ7eyHpz2ULHNcZIl2fHr249mhvs/CKe96HhyHG1KbVUF
tH3jfdJqYnV4QxdS6LT0/+IgNtySHLe3hRKjiu02Fvl48OYZAhOKEGgU65nNXW+XKmhvhO2rd+AO
VD/Eh3q2mNxLKZ3BK2ylSwqi+cZSHcvOrU7SCAOphieiwZ8GCMwAAgjMqM27dcApg+hSIbgymwJX
I8118t9pCi3eF2JlFAaCeBXmTIaV6I+ijYeiKYq/xgrDKakOV5VJakJir7u+C0InjyKltuafdI47
IJx3S11JTBd34wKrC4w4ae3VqgxjsiGl4HELLRT5NOnXYIMOWrgGU+/rB5uFldUBB7yb/jHFHmlo
n7AxtGyIwPID07WTDbPlHhieaiuIII1cBNnMvrDQ/BTOCxtoUzHzCP1of0OT5gx9SFJJ+je6aKS7
GwJSfKMhVp9feZIsndIPOcntQwvlhmQ7ljIozTevRhtrE9n5L1o37+Y0djiEskVMtfd94h3A4SuE
fRUEAtnpzhQR5UnZ48dvDXVHEGoYhEDSGR5ossGT+Xv35tYLCZa3+yvkF7NA7GTzkvQ9xz3IBDDr
Xfw4ZqNcfO73CQ2+X9nLisISUgbljagqpXCcSwYux21dWwSgLLZ+XHpBx79SSTL25vOZznkEW70q
DCLnujvxdeXDF6Rz23xNgQ9/ORtsriAMKJddS6mTRx/tGYyzUaxbfwtaiDbQ9AS0FO96UVZRRUtv
DH1IcMeUscfAkFnPItt63xLNDmS+sq67+b2YC0NevsmJtEg2cGTTDy8y2dUNv7S+brNhveGa85ew
MipjutpRpOoU0vMwafQE450MnfT8nu63cL4LmhsjvuFLJqky4ePDkF6EDUxoyL8owuDyoTpfSlTL
usDGnCnffYzG78ELKXM1SsPNk2cgJWqBYmOJmcd1dDor1z4rpwGlJUcPvYrgmx8cuRWHWvG4UWFR
fjT4pHhgmlsjpqv7OAL5YPFTiPujUbWqZ6WUwuWk+geK+7CnkSEjKUjZRuPOs8ZzPOHjjOUVopKx
gDhCQRusLsmwlS++oWfZEklO7VrMxYhKDuT2K/N07ZEkE/NhfI8YXFRZg/vL1wQfRsKjT8sn28td
+k0wG2bzFkkmOPhT+5sDDJKSC7C59a2lAlEu+SZSwgJWTpmGxD6QA36OQM3uyyONFflciwyIxdIo
LQWbM2gOPvUoqVcJwJ9UktBDIY+SLVdj8+9COk0h17A+oE1PcO2dh1Pn9uocucce9cuscWDxXm+k
1oKOA2aRx4Q=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_addsub_0_1_c_addsub_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CLK : in STD_LOGIC;
    ADD : in STD_LOGIC;
    C_IN : in STD_LOGIC;
    CE : in STD_LOGIC;
    BYPASS : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    C_OUT : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute C_ADD_MODE : integer;
  attribute C_ADD_MODE of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is "0";
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 1;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 32;
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 1;
  attribute C_BYPASS_LOW : integer;
  attribute C_BYPASS_LOW of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_B_CONSTANT : integer;
  attribute C_B_CONSTANT of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 1;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 1;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is "00000000000000000000000000000100";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 32;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_HAS_BYPASS : integer;
  attribute C_HAS_BYPASS of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_HAS_C_IN : integer;
  attribute C_HAS_C_IN of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_HAS_C_OUT : integer;
  attribute C_HAS_C_OUT of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_OUT_WIDTH : integer;
  attribute C_OUT_WIDTH of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 32;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_addsub_0_1_c_addsub_v12_0_14 : entity is "yes";
end design_4_c_addsub_0_1_c_addsub_v12_0_14;

architecture STRUCTURE of design_4_c_addsub_0_1_c_addsub_v12_0_14 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_xst_addsub_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_ADD_MODE of xst_addsub : label is 0;
  attribute C_AINIT_VAL of xst_addsub : label is "0";
  attribute C_A_TYPE of xst_addsub : label is 1;
  attribute C_A_WIDTH of xst_addsub : label is 32;
  attribute C_BORROW_LOW of xst_addsub : label is 1;
  attribute C_BYPASS_LOW of xst_addsub : label is 0;
  attribute C_B_CONSTANT of xst_addsub : label is 1;
  attribute C_B_TYPE of xst_addsub : label is 1;
  attribute C_B_VALUE of xst_addsub : label is "00000000000000000000000000000100";
  attribute C_B_WIDTH of xst_addsub : label is 32;
  attribute C_CE_OVERRIDES_BYPASS of xst_addsub : label is 1;
  attribute C_CE_OVERRIDES_SCLR of xst_addsub : label is 0;
  attribute C_HAS_BYPASS of xst_addsub : label is 0;
  attribute C_HAS_CE of xst_addsub : label is 0;
  attribute C_HAS_C_IN of xst_addsub : label is 0;
  attribute C_HAS_C_OUT of xst_addsub : label is 0;
  attribute C_HAS_SCLR of xst_addsub : label is 0;
  attribute C_HAS_SINIT of xst_addsub : label is 0;
  attribute C_HAS_SSET of xst_addsub : label is 0;
  attribute C_IMPLEMENTATION of xst_addsub : label is 0;
  attribute C_LATENCY of xst_addsub : label is 0;
  attribute C_OUT_WIDTH of xst_addsub : label is 32;
  attribute C_SCLR_OVERRIDES_SSET of xst_addsub : label is 1;
  attribute C_SINIT_VAL of xst_addsub : label is "0";
  attribute C_VERBOSITY of xst_addsub : label is 0;
  attribute C_XDEVICEFAMILY of xst_addsub : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of xst_addsub : label is "soft";
  attribute downgradeipidentifiedwarnings of xst_addsub : label is "yes";
begin
  C_OUT <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
xst_addsub: entity work.design_4_c_addsub_0_1_c_addsub_v12_0_14_viv
     port map (
      A(31 downto 0) => A(31 downto 0),
      ADD => '0',
      B(31 downto 0) => B"00000000000000000000000000000000",
      BYPASS => '0',
      CE => '0',
      CLK => '0',
      C_IN => '0',
      C_OUT => NLW_xst_addsub_C_OUT_UNCONNECTED,
      S(31 downto 0) => S(31 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_addsub_0_1 is
  port (
    A : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_addsub_0_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_addsub_0_1 : entity is "design_3_c_addsub_0_0,c_addsub_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_addsub_0_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_addsub_0_1 : entity is "c_addsub_v12_0_14,Vivado 2020.1";
end design_4_c_addsub_0_1;

architecture STRUCTURE of design_4_c_addsub_0_1 is
  signal NLW_U0_C_OUT_UNCONNECTED : STD_LOGIC;
  attribute C_ADD_MODE : integer;
  attribute C_ADD_MODE of U0 : label is 0;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of U0 : label is 1;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of U0 : label is 32;
  attribute C_BORROW_LOW : integer;
  attribute C_BORROW_LOW of U0 : label is 1;
  attribute C_BYPASS_LOW : integer;
  attribute C_BYPASS_LOW of U0 : label is 0;
  attribute C_B_CONSTANT : integer;
  attribute C_B_CONSTANT of U0 : label is 1;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of U0 : label is 1;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of U0 : label is "00000000000000000000000000000100";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of U0 : label is 32;
  attribute C_CE_OVERRIDES_BYPASS : integer;
  attribute C_CE_OVERRIDES_BYPASS of U0 : label is 1;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_BYPASS : integer;
  attribute C_HAS_BYPASS of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_C_IN : integer;
  attribute C_HAS_C_IN of U0 : label is 0;
  attribute C_HAS_C_OUT : integer;
  attribute C_HAS_C_OUT of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_IMPLEMENTATION : integer;
  attribute C_IMPLEMENTATION of U0 : label is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of U0 : label is 0;
  attribute C_OUT_WIDTH : integer;
  attribute C_OUT_WIDTH of U0 : label is 32;
  attribute C_SCLR_OVERRIDES_SSET : integer;
  attribute C_SCLR_OVERRIDES_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of A : signal is "xilinx.com:signal:data:1.0 a_intf DATA";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of A : signal is "XIL_INTERFACENAME a_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}";
  attribute x_interface_info of S : signal is "xilinx.com:signal:data:1.0 s_intf DATA";
  attribute x_interface_parameter of S : signal is "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}";
begin
U0: entity work.design_4_c_addsub_0_1_c_addsub_v12_0_14
     port map (
      A(31 downto 0) => A(31 downto 0),
      ADD => '1',
      B(31 downto 0) => B"00000000000000000000000000000000",
      BYPASS => '0',
      CE => '1',
      CLK => '0',
      C_IN => '0',
      C_OUT => NLW_U0_C_OUT_UNCONNECTED,
      S(31 downto 0) => S(31 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
