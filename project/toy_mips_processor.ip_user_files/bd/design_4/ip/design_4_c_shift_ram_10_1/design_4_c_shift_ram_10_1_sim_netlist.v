// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_10_1 -prefix
//               design_4_c_shift_ram_10_1_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_10_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_10_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_10_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_10_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
vwrEZlSQeRVTyIn3EmNWS2K5eIu4WNl/YZXk/ngWmCaIPWHy3HtnfIoQyaoxcGaUw/ESFjM8psMV
fAplC6gzse+I3lY40IR3vLymYguKhlkJty3QF18RH/miGn8QshkvLFDnGEgD2/TjowzqClTov5/U
z9OSgt0mzvLIxlIGLQm4ZGQ/hLAqAaKe1OFLfWNJqUH8kGkbYtsLRA64IXRhfL+lJqJU5v7in4Fo
O7pBjtMj/ITVsgjuTXbPAg8aYu/k3E3m5PpO3gLWz92etEczPTh2gJCNIZEUx3kD3mgBp56a0Bbg
Elg2TeWUpXuNLvQ6VF5f/B3pwjd4OEnumpYSUrt9Bs/a4pJWGtP1pksMMN8elHgy9bmZ6kfEHHea
vuff8MxNZ4YdrGwhnrZtHo96A9bBt22MTCcsUGXgOuBOyHn7PXm6CESVAhSR1OkRC2NU38f4YMr/
Zs+/6z5fzr05o3jAVins3d3Of9+gYgcA48DSIQerBgFhE7km3InlCWUl+/e1ayPW/lzMAC5nPW5f
RAh9cUs3UNFJHdIIFRtVzS/zCJsHb5FKghxAUOByiYI/5vZXF+omn0VvyW6DRkOiV0gBPErIGdXK
E/l/GrqBKb0oG45vwEaHfHj6mVN6gzheIQcJndU7d29ALGrQvjSP1YVIoaEj9+QNM0/VMChMbxa/
uXbokMWJ1yJnsnVfzkxUuksCguRq25mqH9tiXUzw/EUUnMF4i6Ls9LSXastLOiU/3Xc4APhVjMTY
SAZ0R2laXQRjOhEKh+2T4wVwjnwt7BqiTraCLH08bdcZtVmADwVLOY20OwrMHmUtoDdb1EvSzZ2i
3DnQDNsqorSNJxgq2492wLzG1PwYiqQ9sPw1pQVIsYdZeBLWweOQr1oMmkuwAw63vobQ0JopQItT
X/vhzX1M42BAd4xj1rx08p067iJAnPpQbgY6149jhHTqQJ7DKUshbTtbwrFsyfEyHVlG9jwP0hxJ
oSJ+v5Blqf9x+Y0BY5uaYRkdQmGiioR0D61ylqxluOsyYi/RpyFbtwzfuUeGqjvG/Xs07iaF4tRZ
Wz24ki6Qs6KkIgmFUE0YMdxw0xRx/+BWOKl1MxCCJH5NKWp/47suQQCYioKPuWNPeUtMbBo5SI9W
ZojXxvc5hiia1Yz6NOUw8gOLnncNxu26r94JukZIcBxGhDgawQBQle7ylxsCVryoWQ65vNSJ8j8J
XIHqs9KAOd4Y2z9yMsK/CvOJjK11vhi/H9kPync5m5vYdPxKr7BACMmqUdE4sFAhti0w5Tfc1rj0
4dy+KyV5mVamOZCslWz4VeMFJzj6qP8jNK5ltXouxjL0+MGmIbWS4aO/mzSV9GJnHZku/duMNnUV
VniCFseY+NmoRHfjsrlrFJbq9jNcc41X1R6sS3Br9/SPAOa9ce2QrV1RfcsspK4Y6qAbJYJyd4A+
SqQapUcBwff3OKTmmR1tbDPEJIPLTPqNZxlfZvVPXaD6BRGjBxE3UBQFGT1BVbz4WaibfMiW4czF
iSClVzstmf1Z+ZDm/KMuHHWtAIkRATbtyfdWshdPTF6GDXIefRpUTvqIi/f0YjuGro4K3m6uT6cS
3KvaejH7wbVYUtYa+yHX8kaH4HYXndECq+TqkD64TkOeZXSGlSiwrmn81JL0tMa46jWJ6In4gFX+
YiHNtpnXJcGngQysrXfVtk4NrdJblgECBToqR5hVCipBUvewLJ+Rb5Qm3QsvAY//B11UpDLcz7Rf
LDKpTykHMTRp+FAYB5auQ8AkJKMF3SKQYRrcPiGFDrhPC6D/LwhyCEPxBPcC5RPPO3EyBggiwAsR
LqyKz7CTyli8L8GVT6BBeB0N9fxirpRk9QKn3JbX2LErrPHQ74icbq5u0VjeuIIsiQdNPbuBareA
fz/kIwLt4h4oPTBzL0H7RfKKqcjOyY6vLzRMd0jjSb8YFzn6kRbUg3px2JWShjmG5A0ytj6nWsJm
OV8SUwAgRo9BS2ROBQ9NycOY1xfipXX77EAhCUIsAgu8w1e5fboOrVp+6wJ3BGfp6M+xrVIYoIcI
nlkn5CVIDA0AKwfSryk9kg02hfuoQ/B0jNTH0Ta3EADDNSNLFEc7k2AGPxEu+w5UGwt7VJ5tPXxy
BueDPqiTFtbkrZvD/itNDZ+2JgHO7xiaKdGUcfX/LtHHZaGsLXsWuvu+OIE319KBn2I/LfqSOWeH
wN3yaYe/pJzje83tWLYOTeDjHQMo64WZm5ij9NE1smB4gTvt7229BYueKdzMJScs65C7x7N8uBVB
qBWeok4fdOOdMONlx7tozy4gYjg7vLQaqNUWHbwQP4wKaCNLeYEGLhupAEjxLCZYgRUhbRPukfiX
cNld1n1KdTGjOAM5iYTlJ/0E+motH7/K+BPzqHfJWDrA7LLmHpnr3CZ7ioH5mrcl1ECr7dGQmio6
VQT4/ODC4Neq8ndtij/lzDTZwjdUpDtUuKeoWIpk05nF27c/R4fp5dUX5vSrJMRs420C1iEUgK77
/N/5HXfM0AwcrJIieoHJ3tJriE4oi3OeEGRxS2rGE3+fQy15WiABArX2BtV4r9vF2gw6BFY9ucUh
ZieggQFG295N+V3+EZeCdTWU2oPlg8hDjZaxFlRKFHu/+Re/rDlYyzL9y+Wrf7wR+9ZoO1sFPMoY
HBZXqHlufBHEza4u7IhXzIta1xc5t5O+ZeqFY32wFupTicZ9Be564//swbNSZKeHqtVgN0YlGuxk
vqmvFgJuz6sSKEnrbX4gpNZvEh2aw5+Shp5W19MoQ5auMyTYDMvZiJedxHjLMdFjDkRYhaW5qtNK
2gYPG1+K8LoSSwVSTdCg//CZW8BV+BkIoNGcxbsve0B00kcMUHIv/UCQM5Qo+KQ2UbbEjIE1e89k
q8cnnzlAZXNq/Z4lutRbtPck20ztneHhG71Y2fTaJiO8lPYbGpIH7uiYsnNW8fEnBUX4d9rmi9LE
W6iglS+nQB766uQsMzxs4dF08TTC9Av/M+WR+mGFk3ZiyeaHBDzrfGKL5Rw+lFHxGGM5svUbHsuc
q/TCMTaRQCINODpNwoxvBP7WXsVji2jRmULa5NpeJqRshErNSx6WCDWG9sMZRxlzRR1qag9zlsRe
g4C0rYxGBic35QClsBovhLfeNR1pZshl21RIEf3gktH3R3048QAIR0WI/OVBa0avwticcbeSTsyO
529c3pfmzPxz6bGOHaLZBgEdj7UNiPedIfveGVIJrx1FdJcGZ2DP3oDSQbXqrC2Ska/9zi9x0V8f
uNWtgChx3pTelZA6irjddkKGWwi9Ter1ak4xYKX+E4rsoQU0DcojMOcT2HfMvZE2oHjSIMHgDLBk
xa78pCeyjQFJLve7L53VTBI4SgjXao6SQJWG3A6e8mXkDMN1hNvb8DwrmPG626B9kX9pLPA9w+nq
UWkf1j5FnolhZkzuCXLsf62MNL+T5VEKPXHPUFXdqZOvUvKpwYYQXMt7ylJB9VnC+HEop58+vDfE
RfkbrXJj4/9KpIsVBmI1FAgf9ATsKpQdmv6N2m3SqwDixTckxWlOeea3yi6f3BlLA6X6Z3ipNJG2
yHfyyBagHz8A2pZd9iKlMFCozCRXfnPbcwd1iFYe9DMcB6EMoP8dNsklIDb5CSYi8A0VVeDnDdNT
KzL3688MTNyTWJtHeO4CaduNlP8myhfJc+GFSt77L/MyBwsOFJzu3i0xifa2mtrT4DbrsFTn0odm
lQbA7/sQb/vIkjXs4l7bZNdR18zp8FZ8n8B0gXx9ojjF6tlsvX4OGZdHkPgFeGGibzDJVfDlY3l0
EY/wN9hJrrDeeQAm9KE1Cyp3IfGPpvJqdBSBGsBSu+td6CJ6Ze61Yr6eWO0q3cAbCTRihdpsnKKN
KABhvYu0Z7CEzVsrV3zgBbRxTT5zJUZ1uROlh4uDyZEOcb4PvO+QK0iJRD8TMBl/CwqbSW8ta2sa
bCzGvP3Hg7Lz24oCNdGPAiege7TPQfNbG1A98MIy2wh8VawR69ue0Ehg/yhyP4sV/48Dp4mD6Ee0
zthbXZ6BYnQJ38kMaXXPv3OtKN34HKXjTzA27OurNl7YlY+CeHLUlT8PleUCsWRc4aXGoVoYd89u
XTuWw5dhdIBp80Zk7zMWsmskV8kP0ZzCL6Fi/fgInlWM/n+6878IaaSnuy7Re/NI5LTT0yAd2PAF
Op7Gf3wRjPW7F2wuMezDsppYRZeYyym3LtJYMmXEeJUHbuW6/eTYLw1O0EctQ6+dGJtOprE9uXNO
baJexIKRruhLzFUPjStuGhgt2x2TgOG8fyT6V3rmZG3vBg6fvRgznz9/Xdn1V1h6llw0RbmmuTfy
byM82g5oCS38x7Agl45voFslS0byeJkUMff5giCCw/rqY/P2hN4IX4BbE9XeLtmR4o4DA0lrufed
7BaUmw/BCZd0S03t1YBrY0Ldvs5vAEB2GjI7/x+Kl45r4z7PDu4BLU0iwXlpT6wNYvd3LpgGPutm
FQp9vPSSq5GhF11BBlS38EW25I7mVJyG9yVDs2CSq7qkT1p1s1OEN+L7ikvMzqQjrVbeol8NzXix
YTNV1B/W3SUk2/H7m2O2rAIxqGN+N5zL4/z03bsm06YPpdw7OPvK7fTAlb9Demh+f/XNUoQ0khpN
hkEnikH7MIDU87nwnOo395jH9/oMX3p4mKLBoatIQqof9rPbh3zEiW4k0a59I2JV7UEG9b/ytToq
j952FBA7WHriIKciNGPJx9ONi0sK/yG1q7kcv0AuWyk5vqTgiqtDIxIdoSOxMhC+vIgmAZm1LSQ/
AVecJWbJhnmoWIh4hA5p2YyMp7DRXNKJHyTyQrQivZjmOdfvKQ/z12toiNGyO2rna6uj4I7lAFLI
l3SOor7u9djY/xXjH6+AyvGUjDS0qQnJCo2AVrPwrNj4ePy2NEZBV7yADLKhlI92j9NWcAAifQ4V
q6hysL8Z1rz+GM14DDa0UV1tq4/yolSyMEK+0oGRvPoJGeaYNW1epJkurO1xrESHWeYqvlp7Triu
4eIXSk+TFY62Q20Q/IowYpk39qpADK3pf8zGPVP8G7CaDbqH05L9YeKcXVPsg8gdQgj/L5QMfZrM
C82ZeLN7DFF5LI3HyJWHFaDF6ye9UBWMpyek4XXSN7LqZQh9KDCQ1HeIzYKEpAiClZnRrVL9oAIk
CUoFYSdnm18SWr1BOJNug0sAS94Hd82ZHv+1NxqLRpXPC0El3skyr1kCvUpLCsZRdWlqAJOtvhuz
eiUaX4ERcKwSK86p4+ifwQqzHmWFyEkrtVYWx9U6jLyEjRXg94Jbunz3uILS+ANMoxZwRr4oF3e2
YtEz5bSk4iqr3TLNrealQGwfb6bFL4/6tgk9OvfOOYMBd6VJs4xLwhu0ke03WGzsnNPMKcW3P/h4
j3hBtks3+lAG2FWKOT9aP9WBvqUcpaG+/ZY6dn28OwfLlu0REpUKDRwmFgYqmb6TZXpRidGSNmG6
yUBrtV+Py89Fflc/X53r+33PL7J6nOrGqSIGiEvvMBWrZPPx+HLLDVsYH9S2YD4wMqpCDJmX0Lxk
GOfW0rmeHI4UL8yLiAoBEaXpLs+AtzpZvFHAf2rWe7+ZA1KFxxxYsv6XdoTEjUPRFy7zCX93Acw1
1icVWbnCSzKUVxw0zqbskGyVUEA08WlOfWtsJwkFKm18OQqs7qHyp9QcQE0m64DGr9Oa7obsErkN
TRtc++7FoYqSs1d2Kjjgw9RdczhiQJwTBDnVgMoX41ppscYGNoK/wLPY18zJYvlydKogkvKwrPjd
q2UvfhYC/0hoxj6z/6Wy9e0Zy0fBsSP8Fqp0kwPtM8Uwzn4M9xDwQVE0f1oap4t8awal0GLafj6L
DniddcRb5XuK7XaG50DCGfOV0MMImL/hLkERdYyIEYErLShepXG6YW2OB9aXasXDtHks2BdlAh4F
iMqJX/VHXrqoly38i7pesguwF+qBBJ15KjyiwjCBFnmZU4l19ck14qoAeXzTzXB6wq/KYSU9K1IV
7QmamhDqtdMmJg2GZrTn2Zv8vl23DTw5mqC7O61bt2HiS2cZlrOwh8ikylWSgvSsKy3uZUqkmaqQ
nAqsB3U87Vvcm8GG4fRCbCcJrXDsT06XjWNCrNaODn67uBnkfq1yK80vKTEWYhhwTXQ1FpMNKb9I
5OIWn/+J4C0RlMcUgIGfMdIlgQbvH7ZFgBJlgb3AOF6wr7hl4I/GqnZSsLHPUp7Q4MhjQnwkHTso
a+QTx1Izw9de+EK9op79YdQPj6v84A0zGSJGUxnc+TA5i4EJQS/m+slevrcylDwpzQe9o+c//LuF
1+6GPqRsx9wEuUfWOn10HTAioRJmmYaVmDAxSLZHAiU0i1gUwI09KV4AqFVFC0cKMlJN9HWFKIfo
K3ONUCYrJRQmDu2TNaqHr671NXgf3agz9xSTvqRVypyK0U+xVr3Zlo1eqh3/DeCaHNMLp+rQPiw1
ffqpsMyjuF9qH+ozykhs9ElqRdU2bnkXOnJiiMxY9w7YlOUAaRdm6hT5F9d0xeBD1t3MMT3yvZ8k
nJhcUt5mPoB4XDf877KK6mu/tlmnAHtmKZoyL94kG5sIlWN07IiL6PG68d3sCN3Ac+iHSlu6HA73
YueWJGe/lH8qlv/QO3Q9OLae+c7USdz65oV0/CGaJ05GjLPZir/2lL5lXDzMw2lWKsr2wbpGgKUH
uy+JdPXKE/rx99UH/KeW1gjhvJk7nilz1s5wdN45mu0g+Nhy1LzzHneTgOJZqF5M0OCfoJAiMGWu
ofasb2QGF0aNNEu1l5uWAkUQEydaLcWo3j3rC0GFL09QlqwSFMi3mIt+u6OTnxyLqNrU4yRz6etb
W752OUfPx6ACyFZfC3HjzkWEwaf8F7OvcktjcgClD2Dd0y/oylAPFIThb4qwfsuHDu7jcuborODK
CVhhoAD/JmUO50pantUR7HSNIDkB3VbNDQ1jxkMQyVxBzJnDgSVvfhh9z93+QoCM2UwA32rH9oN6
o8JVQjVHfpxYtxqRN39uGcntJYWC8zgMjoJHX4ttgebynY6S/oTQpWqbOdDYctEy58XbNms+Gzn+
dtpdlpkhONUH5F2AnWKdNv4vVgiKm6r3OzrZg4tVJMH5nubvTd1EdZypNbiqBMZZvPO6gOVaBe2/
wni5/GzYDFmoKc3Xx5firCSQLhCRIl6F/Lh8xVSl1pEH/4rdzGI3sXtHJhQryDdRLOub8rv2lNKs
ENa0tGCMX4SApaaKfEBaNVrm52EtzMRtlxj693VOev5mvQVoDAwjDRZmcKlIT+3xsXxS9qdRMgvz
G5KiSejUObITrSMyaXPijom2s0AzEwA2ot/BRIbNKZwpXKD/xUz6YK5bd4w9lfijMkKJd/et72wU
ClIy3R8/HklVQwqdgocY5kWBvC+Jfnn2vIFYU4Nn6PByXQpmgg9WkJYPVRypa6b927OqbHFJspsN
YdmhpuWkW36tqUJ+j1UZ2uhWjXyQOWgZ1hZ3GgsJ2zK1HJgSFh09pggZQYEYVsBg8HoSnhRo0cSu
RT+7ojZOJBxHLYwy9I+VszR1M/W7YLpShd9IKIsH/mxtTGTfW8FISX9JIXBrOduQKpQdAsDxNsed
QDsonjl2ykwNLrBGjj11aSY+b5wd1W4a5RrVcMKvNotgxSdZPPHIiguf2cfwEuf4Yts/JDZbGKCT
WlRYeWJuwh+enFJHwO4EAZLfAWVtUonX998BUmAsieaarnlFf9Scsb5zDOXXNNXSngnUeGgdodpN
p4qJ76pNvuUlDv5xmO4LBHPmD0+rP4nPtaDKc3Ah9tV9BlHszgMg5T+0mXpG1doYAsU1kFh9Sv0z
8Dn5S6PN181/7OC2EVWsUV6fB2WQuvbsogsp3A6Ctq/W4uZqjQZTUJvOk0llKh21Jhc9qX2pTExd
0FURqvtcDvHJFoFiDuUPOonnTG522BYJzhM7zksL3BlIA7zsBoqNlGVS082rfhssasbvXSWLAp16
hKUs1SCQ2adrDjQHWH2nR564jCCbQpGpr7b8/NMBnmDMIJBWaiAqlfwVS1vO0Va916fodgGC1UdG
d0xG4aScPD78o0459LuoN9LrY0o9RQYjoBmxSbZL0MAHJ1yeFrJLNbqfUC9Vvo3h3wk+vFa8nJ0/
CzC1y+SfpBBvpHJ9bol06i+pDD5fjBYCPlw5ojz0RFwfEufWvEMBGlgATiDK6yMc+pvBUpxl4g50
uE1RE755w14QNVLh7SsXCXK9mrrOUgPNOPEuU6hBvbfdM+del0/Dv/8bgoDPULReUZoROeNrPNMN
/UTk2nhADG1aY5kh/Xn/XMd/0UBIMBO+2pc4HyIWAw8irrDxuoa3bROe8990zbQcPG4JB34T6elu
RSiOatxwmcpenxKcB5BTNErMHnxB/GJXjnIFYEc3cxChh3yt9gYWd8v6OD2kBV9azksf8LlGpZh5
tIWpkWBrgek0nf1dnqE8XZebVauvFQzjPmnZ2arsPbAivGO5xRGQmOMyixrD9cnXB0D4sYuqtLDc
j5CNIf3v96RIIjPO521Oyo9B3jFxeHAwicJRxHnE9AOSdV+Hatb2oc9ktB1CjcC2dkRx3HchYa0E
WIBC3Ywub6skzivdGbDo5BuSY3zANv9piODrTt2TrnuA48fEj7Gm2sqeBFEgVChcjiNriVNpUumC
YdSjm52y6aX3/elKIf68IWFuW+SA8e5ZzKKrDQvoJIObhaTvW605kvwepzGTQnXMvRchhNgsUyEl
/bIawJPzi7CINkcHhvenMB+yGOOmJLQQ2Rp+6uqiZrjvgbwEEXdxmUv5q7w1xY1KRRF0y6QmTZYw
msm9cTsjOKlByng/22JClnO0bTaOMcKrZNLKkEK7nywk20mVFN6Gd2N1OLAnm6l9ra4TiriXaz1p
gcXzvNEcaeKumDyFCNlENE7Rw4G5izLHyHieofktljsqM1YTNb4XueIorsrXoLR7yiAcMaf3puKk
tCCMa9MetYFJn7B7EEzqcHESN+qYZWtJx3vkbriX6YCy2MwyDXdyQkYp3YY/8L8+twKBaJuQva9G
aG3bh5dC/YQ9a2Nyrgt4XTIlkQnoj6XOiRoY4MAO+mLdlBHRSe6ffyUsj2Dub+Q9A0YaZS+Mce8e
fcj3WTAeViMWhHeUhOfBUfMWnm6or4eImDikh4oWbl0x682sI9pkvN5g4x5SHcbqVkcnVNWACoWF
caLMcmNGwXEw6PEe/TQXUF5SXqfxK9CHAXTzd1nV3LNuLgta7SZRR3iPkGfmeKQRYNMoZvqTVCfa
EQoc5ZlxUW1Lxa3vLNwTY4NSdUC0TXdbK9y4gQti/4cryLvRC9hbKB8urv/QOHyyWtPGNqLqcEna
Peu0CdZKpdhle3JynfKsnVblIFwXCU0d5uMgMHhdg3OqXaSv57fjJU0tmLfdsA0WOv7Bcl4b0Iqe
Pq8u7pcj56MiNEbGegLSRQ13xgrRwJ2f6DNmH2z3fyaYTBelfsNQ9qfC5TtEHhFseaGS/EO+Flgx
eyWh5eidPM92bDoljz/deG2TFwzufa4MBkf8JI4PFyRNEB2e1OAOtjaJmyIZ5X0PhgwpS/X3JCL+
51pvJ9uQ1eAp6qe3znKgPkRU1LSOkUe5KPSHsZxA7Qmlsn26BFEUEgNTiesyLXVJJdc9FeETqsvw
kfotmL1wxfkFcADz9cUhcAidMmceqGtru1xNsp6nLMAswfvX3TlcF4xkUADgwqzpxHsEb2dlLRkv
Omuy0LBtWS3Zu2c/bnejHkmzc7U8GXxkz9ePet01ujalBbInG95v9zK1cK2bOzfqsgl6UmsHqWXp
ukMEVz44UJ7O7XqVTDz0WarZksdWE+vLhKfYN0zwO+2kb1r7XU0dLFgt9efK0oTA9Yy5FCC7mrGF
MSH97j0gYOLmhAw3em8v6Dd7/UaRW68gjbWPfx+xSaD/sZP5VPWqt8ATyXRoZrjj0wSRrzKRgtCX
MZHX9PoBuXDFqkXOAO2RYxSkfBgX3dbozYPnKAHsxkmXsMGXfWsCcSFhLr51GfPcEHFJyH/O4rwy
xuATIshIXVJp1xw730SYYVf/Mlq8lGRMBSGhdNwn7UDWHbx8NR9ITjjIRjyr4+bXMRin+pcFnkaL
XDFegmZ+M/qnBHrZms7+ZW6s1mIyQCSe3YFUGWbJcU3yvctrmyKGvFFk/nZ/qJMoh2LIfWymRkPM
JCJbcB40X3fyCqQNu8HlRrmZYkB1E7KnfglKmeWy6gLy2FYDmJ64Pc84laXoUb8j6wsSydRxmf0E
ws0JAsPmf4/mIazkRMswvbdJUrtGKjLVzZZ8lmvYTW5JbW3YTPQhPYOadmZd6DfAUlt6QeexNDgp
u2xRxYnjwAVDakp8zrHgsmuTiph0sRc4elmrGEL9ylSsjldyUe/33ORaHqtgqvPfwJKHNu1U4RST
r+jPwFkMAc4rFLktgR63FJm+O5+r/WEc6uU9d1GmabTqA3pxHft7bcu5eN5kGFwKraiZEvU7lfRh
b02NE7nTcI+2Va/R/VzMBMOqeSf8rl9bGeESXZ4Dx5rNvm/SF8U5TP1m4l1yI51lAtPxJmslj3GY
WXH3z+OCokm3MiANM5z+VtdO4aHMGq4mPaOP2C0OPP56gnTpzQ9ZveKLwujlf3FTZjK4xSqckLss
q8K3QlRyjtN1JlrbyqncPP0iEs7JGZo0lYjDMfRItO09MgWsFPtT3JMOmwcpFC9jEIlz65/Bc+Y9
rBDhErXMMoF0nwSgYiyYahK86sp3sOP8Im1C2x+b9lQm0RnKq0/ToWIcKbWSyEqbH9jOKLWYUHcd
e4XCW31z/LjGPIpyiD9mh6uEOWXcBEbFQrFXWTrUGGBa7TZWfLuJ29p4mgdlVIVfq+6ccSb0E7Rl
hHprBCMpj5F82XihoU2nzSvEtis3Oh4gOH/qeCY0oRjO0Ld3KRjwRhOT6J7zaYPgogpHS4grLLZB
tBN14FFmGXHrQT6YFrv26th+Q0qkOq8IVYe3+iJVn0hwc1W8PJjuXCajxPXG2ApKnWjuwuOXWIiX
Rp+YZ44/NB5eWABHJdqg5byeO8J7nliHqhToD91OpNzRAhpXva/ycKRIi1jnRY8loVv8HnI4J/b5
FiKZ3ELxOoWZ1UBegW26Z3Yap3qbNiry/0a8NpCGKa+F4QYlqrsQB8BDRZCLTVmDuFpRkWRKOdLd
pYvpb1RfkgsD6AOPqcDdZoH4MPkhcnlF8wIcgtccg5qxZGCQfKkE5uIjNLU/E7XC//vJqRW7wOHd
VFp1w/+0ykE/7iK7zAtdm5ETPx+8JHk/2DXAbz913P79hIG1p/hNznX1tHYSEZZRxxgSTI01lJug
V2S+tMEs0dMG185ktVf/9h5KSiC1CXsS3p5R6/l5n0o50bnMaooHgBPm15cLmB1CHz7VqMlYOrmP
+iiLLwtXgnQsYa5az2u2JDDKB2WVmNdnHWkQ/AT/jUDnWqAnXGQQ8BEZTro6j612dVyYJy6RzgRU
Lg93dMorOPk9jntZEsSEeC++VprSWVBAWgiZm7iIYieYAQiYDLn9LxSl3huTgAy/AG/2mcRW9O/l
9YR6cyzJN9rNjYioPtAA8ySrRMdTEMI45CFBQ/LghZGGhu+3raOtmGsb2Bz2vrMIXoEGHsbYIJsH
+BMWXhNWtLbqJ4i3fegk4pLuXQrqT9xv7sm0tReK1p+7Ly7d9a6GgE4UFzrg6j/bRTo2IOKdluE6
pIpua7e8GyvuE+rQB1jA4XqLkLvy0xvBCLLBHOfHx+TGt0QrJnLqiJMaMDhXWVevCMd8kGhbcTXT
V8PF7XBqYjNjKo5tGS6yRaE9dQ5qeAKn0KLYRwaFUfAF7GCH4eHuhgMM3bk5eTwERdWMJc3UQQua
o6czbx9p0yPN7qdqSRHM8Wp7SRwPefB3wbYsrjFiMS2NpBR9/ZjmvoTUYPXX1yTvyXL/1cymhkP/
jhJfZ60Be51n1vye0Y+aI2/ygyZnZDRRntXmCUJlZOhI2FwaYoQIBeUZP3Gc+RaN2UFqnGsZBPOo
lGlAWBgJ+4qa+V6zxrAaTJ4IeZibxoc7j0Z/J2bCRcDbWsO1p9oieqZMBzsXuE1+8CvlNQAeJaNy
uUIH4b9nfcxcxx4RweavCWc14K3cHfNkq2vMHBgIMFSA/Zcd5OFPziLKcv2OXGPWPOxdgob5BYE1
sl0cufC398AK8iGK5t1eas+kc1PzsM/bbRB0IfyatDxi3Cos2IYAvyWq4Om2ze/rFkLtJfLpFPCl
o5MRwDnwdUuSV2RaK4uRqIKDi3UmOGZ4Q64BNJQuAt12Eed5xdS3JxNGwiq2jXmW5kjNwgZqVdbV
czSqBNlBRjp6amBAT+DIM4hXnRjclk7tO4rflruEzhq/ewMdECIY48jAuGRuUGUbTulXA+t7JtaO
Wk67OKfxkAa2B2TdqhgAVUV9muNQ4aFUbX3wwWAdAf2cSVBhuESfhApyA04oYIa2Lz5bd3DuF7At
A47vzQEFOxDm7+CFQU6Br49kcLmCBm8MWT+L5iY/SZQtgyVq+Duo2j4k0DZ0F/8QSySzKWhFz2tP
2ty85yb8B+ZF/XFBeyhxXhz+7mMNKdQAhXuPdPmsl+q6Ymrj5pxpruhmaJ8/S+JGiShLZvAPb/H7
MjNy3bOAiC4MI5DjJDCa7E8nKZs4WsyFpGEUiU9XUgV4p0HCa6BXcKcZSWpIscWJAlcNuicirteZ
n+uTn5H9hLQRawbj2/O46Eo=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
