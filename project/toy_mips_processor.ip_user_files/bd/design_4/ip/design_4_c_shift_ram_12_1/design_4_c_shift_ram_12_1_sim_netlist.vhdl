-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:44 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_12_1 -prefix
--               design_4_c_shift_ram_12_1_ design_3_c_shift_ram_10_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_10_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QcE9JgSZJXlEdJa9l5/KM5UAOqKlcDvjOrzeoonFUtyvaM2j7QpunRFdD+nqKIU1Inva1SMB8nrF
gKiJvHkHyPRrcgMpXzTdYW3AoEFHbPojwVnbf/Su2642m4HcBlU2w6q7CBex29vK233dPM49gTvL
cCjsqfF5ORhuiFJh/8Y4Qm00d9ykN70WGas/TKecNJzkSyknK2KrDJaXz143djKBIW8o0Mw2Uk7X
u9QdFSxR2lHbs2ZcYl101/+tUkRm20+ZB2IFoubpRsfyA7Zr/1ruVO+cCC8bYU+935iCDHBrBH8z
gcfGMlWwQLEwQs0j0AIiXXCzbBIbg9jzFgAdRw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jEDgf3L4ROYKqe9m3y7//JBHbzABVZ2xw8rr+XLkcU6/1i/Q+CK1RrUSapYT3hns63S3dguuynOy
DYl6e2OTE/klNzp+twCnQthUEs4Pi41pIRPX9I6M+LVPP+pxJUfPeYAZhW+hSzxOFiasxpKmNxCl
+RmQ7gT/546CTxEn1w68kZXGG1W18g1AfLfg/nwd0jSr4BYTMeRE4jhnhSpINxIL+1iEWeH2e37C
laxtR9iW6eG2IkceRmemrmDcHn/Wgh0+jSmwlDtgbODRuuDkUu9xQ11TMt8SFzrXR3jPDxHZnvcX
n/Oy/YIB2p807TmxP3C0h1wHG/RyvKj+rBdBsw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13904)
`protect data_block
2zAhDkvqB6odJ3r16yCTcK36uWOL5CjItLbftTNtKCg862wdcWYXVsYagkJQ8xgsyvVtSLVH/NJX
OBQrmUdUsIYGVzMaqCJTw/kBYRs6Y+4wJYCVl6Cm/oOmCBhVbOcMNx/ke8LedWmtQDgLdXJupOJ3
xSbRazFdJYsyvoFiHFV2R0H5w4enXKwu9oS9RE06G1PHn5AjxuHZvLq6eoJYcqttFNExtjVYG8l1
V98rSJvzCX492uVORc85l05odAcGdcJn753AjHn1g82nSoHK+iSjL3YGOw5ifMq/jtAjAGVmwx1c
jbfTo1RGW+gy8QYfo2VIx96clVQBcMYOBecQgZ113CiH1Y0x9p6sgm4Eqd0Swj0YVxuVxjiJq+9g
6tqh3jTv5z4T+uzIQyVxPkvTT0MMc9Z/zkdCXa5IbIa/nL46Ng+tcSE3QhmEIjC8smbvoyZAKeb3
FuoeOhXImCpmR44cZ8itkD9gNUvl0wAWPlb41LGXgR+Lpw4r5z9/9LiiQYs3RLiMfm76gZwHAQpz
eCzdEaj0TW2G2y9atGmwS3lk4XA7p+1P8bn7QvWkM+aY7oNIgDzWKYivjCB7xdbsHeFPQrBeMmnm
ewQZ/ThF9DPneqBW++vRiw6dilHJuTbaPuwH8HlUyRYdtdVGa3OJFCSlAKcKFXRpqqmyRA5MaP9b
8OmXjTHKDz5GsD0Md/UwcPS/3MkTznctVHjFK+kpzmWbnekACdjhnS4auiJsyNnoKner/FP9j3Us
HfdJUNTA5CZDaoVpKok1pmbfuRvxVwT8ewZKJMdNV7jIvoAgYMf2cdA4t8Ng+MhFWm8eAuAdj80B
2edznIvAfqgLqcVAjuFxBOhdOUKYdNT1Z27ru/gCPKuGljYfO9CFnxXrGbWAHwfgLeVjEVnUkrr5
l+qe429iqmdpqAjeJXiSqwaheM/RJEwf1hMPz3HHwUy0TfTyJu6u0ZhmlbPTDG6KuAzKSa5LlNwA
SB+BVBbPgiw6igS8dbLsoVcjuxnaAtZUzyg2hD2xG3rngSyMHzI0/YyawOcGVQQi0jDNv78HlRrU
/I3KdFYCgWUcIK6Q0D+/qfXwGRi/SpC5oWdEjTOV4YTz18loLS8VZv/JLi3N2WU8Zlxd0faHOXLb
8cqFLp2W5Y9eWWhMd3KrLbPlS/Ap7ldVAs93aQIIyDgKCIa2QRj0c8cDRa43fdIIPJv/rEGUUpNc
29alD3Ic8iRy2v7JGtRi86XW0m17WFXTISt0QuVbKYGy7QbWVZ8N1O7RPl3E0724ctwaHiFcMeVY
wrDHpYJylevhwdBQ+jZA54jegJISpqMI1/62roSsO1343CyQcJ6IEdKZOhrNyEGSouwZEk3dXXZl
qWUViGO7ek5IqIZKbxxl6oHNePHfXxz7RwXI4nj/rOP9GyK8Vrz/41OqwcrHCv4PHp7DoSvw1rs7
ibJ+bTEasI45MhUIoNF0BMh/wh1Az7XkDJLkuVAPLsjfiXHW5IZO6PqsVpxF/ztqUQNN8GlKkFDx
ffTKy/0mSdYrZo0bqNBPp6dvsyfXUEJvJEACc1ZX1TlAd9q6EqADOmZsgVbdpfgbeIO3jXwPKChO
3kgKhDXCA8NtfG3jlQTUOch6J3wo57SBnAbWaTCMbayN7cQYaXeXxHABUquJ4nlVZaaYGi4CWWmw
kCRRKuejhA/OIXYm40DL2qgk6i466Aew8Kd1oHaA4Iw1U6FdQ9BIri/Ja63R2Q0xzaCnXtnNjPT6
MhpCbxramBJ2cYQeIKEPFoQxWtbQasABYIUaz+3Q0LbGpbx5n+tdGDmadF7SOATRsn4czCPQrWzj
ZrqZyjZS8R2sMSZ663nvyzOZaFMc0EPw9uWsghXikpu2LNYkJ+syE+3IpfhNzFSg1iKA5iB8oEPl
bYwCg11myMbdGxjDV9Zy0AeDUcIa3OtxLDQQFUR+7bQrNE53OLq2JxBMqvHifQM+qoexKAIGCt+K
yNh9bUHbwl6hoiGO5C72vrFLFsUuKHh5d1bADVLN80TO00hp3dx/NirvK5v9NyL0YQFD+EslJ6zt
v5YOwmy42fhmx27SQzHWEs41jexh5qDtKYIZueo4LgViMugu/uUBUyzmaSlm46mT3dXwU7GS8o7b
E4J+CwgMlmJfCyUljvE3+ma7edt6toFENRu0ZyPQFrOIEBfnlQ/3Yrhd+UQZyH6Hma1XV42ey0yd
EqvZAr44lLXDMVDJNuZLqgOuevNs+QQZtZfIkqrDdSaMvWrYJXZJu3wljJj8wYS6AwLEQS6n9UCt
YglqW4KBYX5vQHKV0cpOcubzuTNKL8ko+xbfMu0x9kCHYifybWdoXNgjFA3Kd3JXCZRHqK5ICnis
XrxngNiGcNZ4JfULR7zZUEYg1X0qw9847j0wmVRqxvkXob3invcp3F8gny7jKs9s8lSfYuEixfTD
LzEk6JjZji3ym2ZkkbQwMHzLQeJBbc0XkapsBUcXDsoaNWdye5Cl6OT/TWbiRcTY1XSGwwmtL6BB
K+iKkqLjkSKo6Ql4Ym8n8E8faEsonWBi0T1x72HbMoeBr+bMbY86kBWoQ03iSgzaAOGR4m6ShGKg
CllQsGGnUVrHZnC6LGGSN+xHknxKmLGGCB8dQmRXzfm/MpTU3iUCuXn+wr16P77hkbVyUzpe763T
gYNBaU/D32CMFLGBX5M1UvmyUjTrJ2XIE6ZQTORshH3QhZTQdnUaWsayEww7N6971c4hrKiphxHa
8DdGpVX/JvzCNzvo/YaiPmtgZbsSYyenuVcDePHpGT3+ws/qpMRBIXadO8LJDp7ftYGSOGO3vRWA
OZpNOrBLhZYICmf4iDND3dwnQxyK3q6ttZgo9JL5QypbUZMZ8PquQq8567ab3FpbUC3hYSUvtjmk
/5IJAA/+cU9u8+ewPWZ7aPyaMAATs8UpkvnQDsPGZKjhDhy+yGwmwikO4FRTccaBR3qrMc9xS4aT
OYtm5QcDkixl9ZS3g5Mm4fr0CgwZaMplIkSjtofim1+4BGgz1IOk3Wetds9kTPN5lsU7bVcObMzb
3LKB/HtCISD2sRXno99pO5cZueNMwWoTd9l52qFXmDtKGKFGU52bZCumG4iblEkbfsbUXddpbCH1
uWoiNp/BxCCe4Q+4Um/0ipuFEWl2JC8Cdg1cCYYNuT9zJx2Qhkj0ASImfaGgvoc1Da4dmXieaxq1
NYn15doWWkN7znCX5Lsdl1UNhUxDP4zXgxwVxsLox8Xs5QLBlMVVUreRiZ+LtOkt3gc497kEiT7v
G9I7QOm4uhyRZCvJx6o6b1qQL3B06YYY3RbcfpnJuSkxQqnXA1qx33H6jBeBpvnbeE7VTkZWrZj1
sF1fuZOnqyT3RqWFMynZIJhm2KQDYd7esx2Isin3tbl2RzP+egTOMbWmlO1Py2HYIflGWKp9YwrL
Iz3z79gTcYK7+fPymZRw5ko3pVXxclvOGc089reW6Yj28aZ8jFGoh3P02jNx5Zf+nZBB4TZ/esct
jS6x94YQyGI8elx1iMqcWADixgo8b9ji0JnF2awg4yXfRTbSxZu03NkJpm3GYK3hmAxBh0Gfj8sJ
mxWLSy2+7cWF+7E3GhLItrpfdGnL2hg9Mj8PH/oHpeYHiXyN47hJrBe1APYuocWknf4TcH9sgUX4
8U1x3oJ5r+u6G9UnQ81znBG7mlatrp6VKbgJZW+d+w/zVXgfWwMeSHHH295SdkCMe0snsLjFLH4G
Ww/tA2i8sQjpulFiPqbCQTgBVEVSSbcwLJXQyCbTf0CUozjGhzu87ed8WIDTAnOLfBYsRvClELeS
swuRVuCk0z3gChPkuimW5AAkiNTtsg1tI4qtpGvAq2KWZo5EJCmk5ts5KrotelXqHYjI2+qaJeNo
/bbQyMBuRCwwioCc57dNQZp2N8Yv97ryOL5F1qUbHJhp+zrWM7Pz4zbo7aiDOjC5zc5hbw1qCh7W
x7/wgjO373Q2fxYKnY13v9UtYZmP7SzYQLog+I19dJ8+khCIcypinFO3TZOftZr+5AbsaAYU61wD
nLeM4coDmmyGbJzSB9d9us7f+oNWARRI1SM/kyQJQ/CqQ/lNkRFyl493u0MAjimLlwo/6MnqJCBg
jMQXlmuL6HZi/wRO+yK4ErXAHrsTXgWliYE4+6vwqbVAiN945ZaBQz/SyFRcd6dqdFbasGPCSn9a
fgtjPw/GotmuzM7f2w0da9eVK+KHvz1lVdhey41+P4eyt00Pbb1iSXlJTOES9jYWk+upXkOuEskU
zZgjIREDL8j49A5V+iyZoMfPIL7EPXjS581EnjZvQZpZscbcMzsI4HXwq4rgE88r2KfzwEkTtiZ1
bN2ksDnN7QK7+5LF519ca8zkKQwxEaNvQ570w2sWU2oc26H8LMXz6Hqq7AmRHm8MOV3DXYFZNA8m
It+boAVAAxeaIOqn32D/uQOHrRdvopOTw7Cep0cNF4hlnB9tvE7kwrIWvZnZOJ8zR2E3fa4ha0vk
Z4DFCJsmwLOhPgajWxvHsCZ8yyxFxTYSm3P51qeVYK9DuhLW2owpPy91YvcfsZB5AGzpANjp4AMj
R3LCSDOo6QDHMfHldHMjy1uU6Gn5zofU35vgSUHOf/AEKOHbyVP4dA7fjOciTwcPw8exWofR6cQt
qXFz6+5Vunb9xUMcmL/DBNJWxUvpvew+xIuSy4MHZjUwJ2ir8vEW6gZRQt8fRHoOvHfpBQ90CDSu
qlc7O5wuFC07wCzWNLdsw9K1ZnuO5txCIg92Cl9crlBgbbvN2HACsQnBYuWfXcblPMZtXKDf951y
P2lKx7wR+kNHH6kFXpPeIJJB6k9fb7qvXxCgIjE2KOQCc0Y595yGgdN7EHrudVdj7Lmt2nfbbg9T
WhlawV8PdSCfbn/iPr0dxbnFq3WK4VNXUFhGxf2xjatRaBk+3V75Af/VnQXHjbimcqeWkzqwJ0Q+
RFabBjmaMxlrmZTAdVvfM73ZhmQezN4B4S6N3w8iRgwYGObGtxfAq38zzfBBVoQ2s9dkgEPuRLOG
L1izogi39RSJBI+uYs+lcjQr0sa7ruCgv+KSd5lqPeC/paB9ucSdVjuahhEIRsa30ErJCpOGvEYu
leHKncuJ/K8xIFDtdEZ42qYLaWWLaK+RiK/m2lc2UvHDkH9l1w0lzZTSJ7/CL99jtQ+8YIXiCYBS
HM0nAHr6xft8ltN3BZF9VwryA5G5ZMQg9g7GXaZ0hXOv6CfxKAAbPWCw+6HT9LZG1cuQV1eL6SJK
6tejkp/uROmxEXNN76Fhw+4XVDTnL1x2vQzo8WMhoflXIjiqz52twBASkalcL8TD/R/3e+swkkAN
q65dH09Cz0aht4img3D9Ld9z/2yW/3TNxaqo/1M3f6c13TcR0px0QXRNhmYTvU+9MWGumlYU+kgC
H2TXvZ8v7MMxFysEFcrXd+uM0c1a2iEzj0t4ajSDZdciL7eBVORxTx8+v694M21Cb5I+37wT25XH
qvH65UzpBEmxmmFHTmemrJwpK1nt2C9Nlc6Z0lWMiXo34O6WU362a32UeOHvSxLb5qml3w3fpGbB
z+BskhnJ3IcGKviAQy4N5Fzpk3lbr2N027ioQNfpENczJIKO1BNJc/RMWKCIuXh0S80WFoFkVKTy
rGjVLayMFN01oLjxyI2pvmVJ0BZwDfG/6bdvgQAamRM+RlviBkx1Nn+Y2NaHnPyRLH974RqmwHB/
AO0G7JM0UeGTpAYXRBRUf/SvsLX2xFI7yuc3JFkBHGRuHfv9n3+3pktZhxaqe8xPtV7YKVyiapQq
UbwY46DdKxZK1iiKnST0T5vs5wD8T/7z2pcUOu/S88SrDUFoB29689eTE7pX/3fB7VlGJECYqQfd
N78pCyazHsU8L6ZlRNHg0LnPw2Ghs04vdB500Ff01voQpFKWE+GnCMNNhDi4+pqO89FT0xnfVdZq
ywcuRQUWXZBEHIqhdOtgL0l/gTUJ1q60MFFWDgnvkq/wlOICeR64ppUJYk1Dc/bdIlJBvO7ZMBHz
OwiE9Qdwi99Id1WzHOYmrzLs7OBEmIDh7qTiFI1fApN8WVzSgfZtJSyo4x4C+O44E4rejc2IHVix
i7TqPVL3gHYUErc/lbLFJu46V6cf5y54dnZA8KRAeu7ZhDiIGgsxNYUpJ4fZJ53hga+5yphWN9W3
oSq0EzAAA1LqqntaXMpdeNrHn5MzNN+p/GxsOahOSGtRQbbK6uQ6fPG4Tfba/zob8ACGLlPP8TiO
Q4EPNSIhR4e+HylY1rXIP357Kzf99PkIQYrbH483C5+mzABG/mYLW3J4cyzNEt+zpK1bGsux7SdQ
Cn5NVPXB/h0Qlan0IltERPz3xt4oCekKBUrKw38R1B/jYZA7YtjzjkLo5toL3FbV5hlDwVjmnla+
Bji9lggzH2smw5Fu234z3M+TId4NUISc3PadzbG0GJZSzOcC8NS+gqdRDzajJLhq9kkRltS+rv3Q
YRKEl8iHelZvfQnPaeSvoqnVxnbkYeafKt1CaqgryBtv9+5sAGZnVqiOGZJkpUxqbHo0MZwM3D/B
iUg+7ds5wlPAGwv7a48FJn8mga9Xa4aINHlr7jX0rS8biQQSEFCi+vPmYQNBw+EoMSjSHb9VCipT
6Z8PBfxShU5cFr0eFrPhdMVjEBSAYLysRBGgmits9+CV3KP1CjiFFhwbT6N6Z5EuxAWuJtKANL/5
A2yy3dGaVzibHQR0sRPhmTluLo3PcOclgMsiXBsoArofBl7fq16ANXorx3h6zMB6+kWQjtAWSepy
YIFozSmJIUJNyJ2DlVrahV/VzStBwZDixq2bP5NiNiPOdciEW8dLLC5LLMCn0+xsCJtBxNjnRleb
gq/lOGZ+ydvUqAcFJttvPg5Lq9WNzDbom/iERzK1E8Xz3JaQqYca6h1WdUBUNOKcryDcw80QTJm/
agEgkS4YwHGIILrO6fldYYTr4HwtF+5N/LvxGHNnrRoLMYfYmNHrHQfLAocpzeFI5JJboq7jw/dp
WNiwUsAVadTjOJJq+PFwYxnoHfWjt0kZyzc8D1UZWuVVadwOHgU60wJ2jaIJihpVnfTUFXku9v67
o7SN/VsK84fuEaCtbame4hcBfKaIG3o7miJGXGoULLYLCk6m4dAAMJIil3W47tSv2uFG4fGsZwFa
efJZrzOkZ/gAKZMjJuOXeuWwV2Yw5NzmrYCle2MvNRx/Llw+piKpd+zIHPx4Se74PL1R5r6cY4Ja
Hg3n0C0dLrcXm4LdLdT/5hMpO9zVCeUsHW7r7kecY8flm5OdvWmHmL1+JHSke6uV7lH+lGraeZIY
wUqLyWcvS9Vwl9UjH2G02/4QMmNUVVwFqEeYxoqtxV0Ep92j3Wv+Are0ifh8AV2KjK08An+1K5Sn
iBnTJrZVU7QYKJmSCsKS8v4fCTmLkpOaonPfEEbKSpOxqrn4cpF+YFD45WTKYG+DY7nPEkFmZxMh
SLlTGwUTF3UaUe1iWBLEzuSqrGP8d4GquN/ZEot5+8umvxlYZrhDK+KoztSAbOMgbYISXXal1s/e
bD0vteZ9qRo2FI4yZUcUAB9QvzBCtRe+t5DvyMT+2bjQ9FQs62MpemOKx0kvUX6qpTBWxcUz9Z8G
noe/NwlmfB6Df/5ACXJGsUwYpglx+i94O8dTAjr+t8jF8p07LJdv9w5g5qYHmO3Qigoq0P1bfvT+
KB5QamcuufgoKhJuku6BdAt/1SyhtmdoJL6pbHbDFvcSJoM7n3Q2R66SHr4wCb0iwxSiBHuqhFUC
n2p5fUvyHXSYDFL1LuiRNf5eXaiqHeUStkRCh2bMr4cWT9kcLTtXqwWhSfH+AgmfGGuXMTKaUunB
6PROmxueVFl2NBovgWILVR3k7adRu2O1PA8mwTdXDrwx9Ld7BkjMOmkH9xY7K1tY1G8kUIojVd/m
aISs4E5AAnWRLm/zdOgWkhmdKh9mmnOUdMa9fRAcjfBrLBSUc+kH6fLiAP15Dw59+oU2neV4xAur
3uEfhdcb5dvIf6EjmxpPePIwAvrOWelo5Nd3AWOyZIgMaseIeXwcJmZTkyqtxdvGJpNByM7Gqtso
XbdrNw7sMUVThdZCwmKkoabLT0LW0qIuPMk5y5EkA20JDgRtofqKwU/kKlWNKZystjdK7BM8RW0Y
1qeX1LOWGHWn6OWLXnAtAmIru2FGAbOTVJgXEQS7wpRIXLZ5YwktjX8fBnpQD7tgvQohhdyqNfX+
r8s1UHy7LrdVjprhdSUmhWkNj+QvmCOONOGa3XHL0LZ7ONBfvw7Zal3srjWN6aYdMPrfXmR48UKx
xF7jqtHGj9+uvwbdo9hfgfIdTzaOa+J5O0XhMDhA2ZfPe03Spcg2fckriOEugySaDYtwyCaZtLe3
AoaED6cdaScvXl49qHEesEx6TpikgBXzX5x4VM9mdkSzTrktGeK4nsIUY6iCxKcnzYb7Z+6Cqe3k
IdUGyMv7LiqHNMfHX+bb0puJP6oyNy6IDrRtYsPSdDVZgWeCQMGUgpY9wwt3uu8oUBDgFHpW8Esg
4L6fviivy7nAe7Kz/Tu02Ip4ybbxCEeKbvF1x+4/yvEpjPJIpPDThQ60bB67VbP2zJXjdUMHvKem
ofiEdSImaucVs4PtFd9lzl1Y0fIWY07MEyZc+zL1VW2D9DcfN4ZDs6NHK4sBxJS2wNevbqY5FPmA
3iyqsBeb4odGUUM7TLAenMy29XnSHa7hvZToXQeS2G5pWJJ0iT3bguz2Pa85c5ryBx+DhvPy89aE
mS0Ww1iKBO+8XYa6PSVMFH5P783/b4uUHTsRKTCV/TtrtNM4OHem4TgDi5f/tvstBgTcNKmyDZXw
IAczXMKW618UNB+bZXruGA5YCkQXf3X+qswSl8AJKh5xexxUG27mXJwuJTmG0Dz1miuOvZlb+lUi
O0V5CntW/KFA3K5TCMOGLpHkelJYqK7GFjerE5i7kDMKKsYTy45lKQKqIBmtx6gk2t3XBFPlY5wG
vDRiz032u0Uh7R67R5keW7xTNS8ZpERhD6hSe94SVhrUGDE+AEauuH9JvmXD6b7dSRZbsZBDCF7B
Jh1V0SBEmUf65CNbd6i23F7r1usCJlswZ3YKZ99hLDisHAE+sZ9l8LVcGH0EfiIH60UZXWXZSXNF
qbZU1iZcbsDzSu4DuDF71cpKzji81THWaSlOc05QTXdFNqHWZP7n1jNnJVSDkHQApl5VkWxHjLVN
2FKr7gfugGjXVVxp4a/OsxjRjdvgwR0bCUWNtsIpfAlchIVK2AqM6hsX6/cNkiqfW2dQGVhwNQ7Y
tTxXBp+GDk2lSf3k968+nKbbA86pFuUQ2Fg/pH05YBMtXnKMDElXLZrwoZFA9UDXc1kfj4T/bWSL
NoDBcQs6uZiyEtGXsJ3Q161VOoskEe+td11P3Po/dl2aP+O0ILeZGPmdSiZ/C8GRTayjeLpxM8ZG
73zQXfy9hIrDbc7n4pIhkVHWB5ay8NfuZdI44BIQJMpV609L3VeM533HS8iGiELMoF+n9o5zOn5w
a8tfmRWtM5mMqXoIpdcSyrtwCtw1N4LgR6FUhenrYRcCf0vmZyUwLQr61/t/ZTz84fHAC1A7r/2G
4qxYB7cTckiPPpLcS3YaiPqLLQwQI/cZEXmnh4LeaHTOfxFlrucVXc5/LW1bBSXGTXf9hn0hjqXx
b/6hlLV1871hZWl66U6us2i3R31AewZ5covmAhmHMUsttDxCwrlBKBarDsprOzzHBuV1/HmrqOwA
qfCw9UnDQAf3sb8Lp1N9Q/WGuTBFYzWar8HJ+T4okIG7/m964j9OdmbabXB2sNpPgcG2bo3VtkXm
pCIfhc9KSS4fteMRR2lhFN2dQlK9ZN/evquAn3OYyMDbiiQLTDBtVCrWp6Jy1Jmfov7jmIhJ4qdA
WQZkgiuY7FilC6p6Ars51HlMCXLaC/wtk99qEj6s2qTiLcsVpTvrJwsySNzc/GDsnWh+bA1Iq0o1
o04QZCMuKXrZDiF7wxY+4T1pk6Lw3qhnuNS7ZVKHZbuDfoNOW0EOs/r+pHvmd3NjQJnD/jPQUFV3
XEGg3sjQrKJIjvYR//fSNpzo6GfsUdNvXTbgRPy6wq7UlQnkPsfMrxXshKw36NEk4BqCpCsBDp6w
/mKIx6FqGLv+OqOYoBhx2TdCt/Ic0++J7LzcAPxou9weuhZu0WYYURrfD7YGDh37pkNSOy7wxs3E
+DJGyeU5e5hJ0mUy+IrESCxnOxIvqneVGJXFpG6Lw7W9QRRxT+RJVrY/Uvvh5S21O0lIoH5sFIwQ
ywJKZ61gU5mkIF1yN/+F7yJRZ+RmKMT4Obez0PNB/q9JtJH+BGdIfYk0JJjJHYOVi4Ga1YmGGNao
w26dWPRiZUgf9RT3noVv75sHVQLDNJEI8nEQfB4a6IDEIJXUuh/bWcIlQTRPGDl2ybXLRdbdqXPL
yEnKedpXxA11YZIZ/O1VuM+iFMh0mZPaZ0cXqShJSYIisDQ5gqRUC4yx2DfwVoyIzOOS1rHtiq+a
XFMGwJ/ghYo/1h7gXo32NzzXpRyvodLx49dCPvG/XJ1bRPFwr4IyBkWDSvhWsl5lNt/SRadMwfFG
+2g7EJ/B8JAMhxNoeVbG7e/LbfmnKD4aeWsmR0KmoHIj2qgOHj4xKtt7wMjq+1uBhSlb4xfcvv9L
ezOLtUXY9d+GSiNC6GKGOxR73eOZT07XHs7Wv+EUG1LtS4WSAMkcXbTxuMUPA4Vl9v+m6ieFc8CN
wYbSfx7+1m/uHDE/d6ZA8OILfEPwSnv/zBzE3IGkMHJf4EZfo59nHeVuGkmP78mjTWcXPwO0mwjT
we6pjgBo8z0oNujgnb7h/q2BAyFCmqmmHL48B9MzmmzOvd0vfOFroFm1oyAxddtjSc23WtkajyOO
6fojT5MzdL6tOzAqVBx2Zl77eAEgbcGZ7Aki3txTbU56GAsbQZTfJ2puB7WQviWDmIKVWCPFE459
aaA5pHZBIy//aGQ6s5XVZd1yuWtujdXTFaaStx4wXiKD6b8GmyK7ErISaI7/SIFJTz7YOLfEPnpB
sbGRi8KNTE3LPazWth1gB5mrLAqvbgmu0zCJrMF6rA488rBUWEi7Ud8PnhjkP5lFKt4McoKlymDc
BljOeI2IrsIWP4GCdIJrQZE7acVikFfoRw8u9xws23/xltY4MY466i9tBH8reusUUwpPX8HWvWmY
DHFP4EOc8D78tKuSaxnx9uAPmKrfM8f19aGFPQ0R0ElYgotq+IBUV2326xbAG470uXnuRXVuNFl2
TbLccipmqvDCICYUPHy5hNJGQz7GwBiPEJZokHXnJum+zxryp6sajLLOjX3cxOspyClD4HDXersx
ZZ8ZqORUW0/bJXkDBvNK8tlTItCoilKhqQXVyRiupxr784voGGCLr4HuhBbz1Cb27Z93tL0qNCnp
Ama4sMDJzkBS0Q3TobocL/AkowMpS8PrWDJxggSqIAYC37dzGa036Dkc7w3+RX6fWdSMzEyuNUal
2GqJi4F1526SK8Sne7C0WmkdlDhVNEYo6bB8n/8sztfEiS4GAZ/eg/LjKyce0QFKR7hAxPEL2bA2
6gtZ9ZeYDLPFG9wcCCXerUwAJsQuK2nWfyKn6Q7KPUxqoHDbFOh1Oc1ckSWINojzu30c0HFoYWjd
nF/7dWpiq3fxV6CH4CMbDZKHJAdKgSSLmkVzz0P4rMnr70zPqfB9RY8KmpWh9TphSXske78aMNtb
LjlYrha2aIFkeWoXVjAexnnTGafqwyWbFjhYcfcFjlThMzriiRcbmrZICqndSx71DXUnBIg9ot2b
hKy7vl/GAUcrHfC7ezJYN6ypYG2U/mXH/zfL1xMG8QEWdl3UFTKv1hLc2Ca01vzyJk8Pghqbx6wc
ngC3284GOfGCBQ6rFGEqihlPTocb+06F64j0uMpGxDK1koRpZL4lJR8WbBaS7CDU2ur3GExz38Me
qw+KHqiI0lPkvPxvoweEGzj2WvY8FxeRhjku8zOIzs7lX7TgGA+7q9bLruCgFV2irLM3WpLBaIoI
9vcoKgUk0ZgUDnY2pXkFTJXCvtHTDO+TzS0qPpuDBlPHbq8iR8N9PbjWJ3t8MqF+txgMLokYOFb/
Z0oujah4NlOgaAyQoqjqEZBqtkboQtK2DPoXctJXs9+9+MxRc2n0IW0K4X1lfJOunaiV2RiLAYxR
77ngrk3EKaczX6V1KlUJ97/0YDCZTNGerFoR+mLUTJr2lLQCjoCgnrrjvsLnukN6wd/Og535dO3C
FzT8ceA4fjUVZqKtr0XQ7amjLj/nYshTnezDeuz09jsfK0bRDuJnPeaDHCEisPIhDjFdqSwd8Ar3
pYfPJJkbsMmvgLYnokKzKmjbRBxFkZJLF5sO20rX0DbWGD0oThZHd7NO0EowplFuhLVhoVH3ARQO
Saa1ESIzbghAWAHfDalx62RTwn7eIwNbtoI4WsVDa3PlPJPz0VlVJMErC5U8p9Ha8jajSex2qXFy
mwaQBcSg/Hn0pFKNBZiUsa/HPuXApWqbK1K5zS6b5/F3YcqnYYNyvV2eLKYEN1mHVHqdtqjsJyDI
4Yu7eCzSyjq/VFhDvNawKj8jcLJYEohgBEEhv6XPG++eGaMKgrcV7EMV2572oyhspEfka5lAbaDI
0PDwtJqjzpzUyBqo7fAnCrAIwrCEE9Puw0SPW4GoMaCsLci2FDpORluBTnzwqaLwrtT61Qu/W0B3
ygCnFH4RgosRrTnE1KxSZ9oC0vjWCMtx682XabGMVrcq4p7NiRPhWMA7hGCsHZQ5ih0VN2k7aAx0
auVeG8jA8jFHtdlp9FdZGsuZjsKnu5cdj4aCOW4LAbAT2tq+c0+W9W5wKUBJGP3qAAPa9ingfH+d
5XEj2qIbiqPnl237tGRtO5TxvafjchQ9f9SMORnIaaGTyQ2CblocV5N45uASMdeGt49mqAqoYRDR
EYazAVj+Jn2eyhh8nyzsYhw59rdIh1kr/7ywoAyINQ88LWntWEGf9h/58HTMuPNR7+jigNSu/rqm
GOm5rcYbWwDcTtfwhxc9ZNT9LvwrAUCvXYsnVp3sXEcK71Kp+EnvW6Fq77h5fLWNIDK5z3J5OxVR
ZqvTvkURY/ri2LiHXQxEsZQqB/JIOovFraf+tLDowp6gMkMJmkKFLHYRwAJlJ5+cZKasc6A5izgE
5llTIk9jp5fHsGsNu4cXr+QEKBV41HA3Hx7OyoWgrL1PUw/u4qGsqK5hWKfMTMnJuJD89RM8ND64
o5AUmFKYJ6ae5b7vRga/6yrq5i8xImBiYrM56Pjk7+9vhQyTCQtYXEpZW9Yjp2DMbyQkSxdApgs9
QIhBYchH86TmCSk3IBQ1sZy+5Ala/MmdA0YA8Z85pM5h1XuGgrveLxN5GO8olWTCWhEgh+FZp08M
3UjhZzpMFO/qRo/PgmeFieHDvDWQ0iia7RKGA54HVMti9RkBWfo52Fbi4Uo/0my2b7O9j0KCZBrZ
Heb810BKKlLYyq/HzG7qFXATs5ZuoNGgRYh18LESnC4jCltUs+IFzqEhIx55xAGgcbxKbByRUAhv
Zq7nUn1a2kAx6pUGTB+UghR4XZgn2MTlWXfilfwmWz2oAeIxat8xgk++ojVMmB65a9GG7OHcQphT
U1O73F93I/yWPN61KcYpm6SM/LKFV7qMby14ksGzfWYeMlpHXHdS9rCnlFO6y4XJvj+vBFoujrL1
lttg+aMqC8LQcvXtHV1AgTW8ks4ysAM8sXhOvGQ1KD+2YpOmwL72KHYFsY4ov0nEjHIENz22WcLX
RvV6CUMXSRvVyWL7sWdJhTeRgh7YZ9C6vEah+8weaDmw6jM+otOMFU5e2sMNVq/qzPEKanWcFtqZ
gp/F+C/co2JdoPsCMsVGS6a+Z6RD1ZUF+QksGa51wfbsFTOvBXCiKQcImg5mVqYZvoK5BjPMUskc
758mdEiMqDdv1shLrlfCM5ty12mYHYL3R3FMpOUnYU7abVUYKOacUmvPaZmy4V6tkfMeK8clftNz
tBrjU13xnpsmKZKzuOyHtdgRVa9x52cTRySbG07WIDnTH0sQ2wmAK5xHWIzFNNH+FQUNm1tiEyB9
iIUJfl4gobfeNH4SMmfS/XdJjlnx2EGnnWlvmuZIYFE1xaGbCMA3L8OgIlTL0tbgJHN2sHnzklVG
3/Vbw+eL/FPA9tMtMANv4U0iZ6lNQQbhufWPJHvMlH19w2ZcpqXJMb+H7p9cFW1tKlC8NwIKC28b
wiQvA56FdMVDK92LZGIw5LA/5uaHS5u/bN5WlE1ny1PtIWZ6Jmwh8NUrV/Tplbv1nBV5687aAIo6
L8xdmeiSnauCA1ixrZtiUEzyasZ1yOc7DwAQxv4gsRus2Wg2WT3eI/NFhPj7l627VNJaqaIghMVy
1m0loWqiSQLxh21Qu44cAlpRNwgpZaWazGke0s1SwMUKpiJ8+0ok7VvdcHQtHVr7B/cqjd3qjS5q
FCHd5xOhDJd3m0ecuC6Jmu91M6EW//AEfG4EejrZbDLZgal8OZyDkXxjLIbidVHe1B5HRTW7uhv/
xOMyFFDDENfFhpZhVYq8o+6H1quYghir1661VrePscLICjKaahxcN/Rfw4IamomplIKiLDniiMkj
bYe/A4OUtAItoordnsf7j0RPjsqI6vh9LozWxguLP4gE//A8eU+LkUeCc/PfoRJCcdrZ3a+EVJ5X
/oToVtiTe3oNuUczu3cE/e3dzmQFRur+7SB6SGEwau+umsYkXGeovySP5YDFVGkcnCUdOKbVQYQ0
jwlZBue4hYeD6jFl2/ku+I/DPG1JBvKKvRCihAozegJO8QPHhDw0gZMg4M5D7I074LbPsm1XhvKU
RuedIZIhXkUXWUrHHYJ/JuiDhGzkCEW3b5OICmFfcEvdLfK2Slhe9fPtGICfd03DJ6mtqY1EFSOf
I3r1ZR6rUNFLCuwY4kLSy0tvpBdWxj6SNRdWiu83YMRROK8uCNb/63FtBKMKhrs8HbaRYYr5WDJi
Wtk4h6m6R3+EvdrE/emCxPHfaqa2Ni1lKd4+THv4TzfSOv0LCOwegUNJ0QoJHKxLEbwTY4mMCJ8f
wGtzN47bYGpd0NPbpeBcBsBzXiKt+LueMVf1czFYLXrIg8gMMFHxakBSO5VyzSu/Z/KQ1XcL9AFN
pRjTUaNQvHqYbm6Sko6jNou75jA7sJUt4kiWNnI4512/YpMkk4utY2+WcWC9f4CQw3SsCw7dt9Y4
nlk0esgIIbDrjy05zDTbw/07D7MehU3urCvMWbmu98gb6w8PQZsZ1PAyXYCQMInOsTjTp9E8jP1E
l+7TPSSX6tiH+QugghDRlSFjSqhBF9ig102jv/fM1VCOU2uXci3CSRuI876Yt7bRZAQnuQ3ZX8j9
YH5TUnrairAY7qcjii1mxldELdpMOcp4sTrm+0xWa5ctPUyzPslwNaGjP59Z+z03Nd5yuh/LekLQ
K6Hd4CWN6HI5+nId1gbZs9AJAUyK/3ZhVt72mhUqHHgomnP92H3KrFILp4wgdicvGpdviJ4DQfS6
odP2+nk5u9sy6YQBZMjhCQfw+uHqcK/h3IUK8LAvrIGgw7yvXLN+GCZS0rNPrY7EJOaxJTWcVmLt
KMCTBUjnvsa/1mTDentK8VNx5K97EsUq7+3War5ghq816Jn1DGxM0nPHbDyUEFKqWYFjyeVG2Jfp
dtFBeYEShECDZ0clv4gRSLRJY5k8OOoId+oDN6RX6ydVXL+tE0h9wEGn7ig2JWbAHoRnrH85lFyu
hddBw5eilEHYGceNcNFVXwNgYwyQCg5+eP4P+zzvfVXyWOu4IoxONoQ7l1v1Smz/H1rcJ5HFBGEO
kFFpWx4ikfUL+ebisEZMyIXpUd1X2UzD9jgUou7i/wYIg1d7AqK/fZCzWCr9VDmzH0BCOzJUzur1
0fKK1L+c5tsqbUCym657wp7HZ/rB7P1NJ1MAN81Rq09JS+VgfDjArsqNqNSPZ+lk0dItDK3r0Pol
DJr259AWe9H/THWDTfXCTxMD8kmpne92UGrxIsrOPm6ysi0BbPw/McylhuTX+Z+iWFDzXHJISHDn
oX04+Qe3viF5W/6oAj0puqQ+xRp8V9SZobtbWKV6fr1zng5gQmKMHBKemeqScjkSOY9Ys8DqZUvu
L1/lY0jQxzChtY15FvTKi+dd1t+9RS4rNilIIWu9p5mIY2YrCvXbzZ21gMewt8yZWIds9J5g+DL9
UQtE/pHjwBj4nSa53wzSOSw5S2BSNYnE1rh+d8kXSfpI1V5Z+tTksbZsLNgMko+2XJ/QOnem1jwC
RI3ERlbIfRskpfmVUE0S0JqcfZZhQmokIjEiJuO5YmcD/+r4WwaeNm7+EPdoeOZs/ts21XLwd0r5
CN3NZ78MWz4OMunH7d0h6tYd4gv+th6YNOs1lbBwD0kSeXw/WwB7jtyN5FwrG62WeRqFx8l6SfPU
hJ8h4VA/P2kanJbOl2biVAiWTM61KGDXrk+z85ruNVR2gqQ4YKyNqkBopPwzdfU0/51WMd4dyrRj
/PW4Gn0TB9USzmKVoFXvoiUoUrgco9oIPxGmVgCcLf4hJC4NvnKHFvsgI+y0RwUXO6k9UGg1cd9C
PsiuMuiS7NbR0fqs7Q6aS76aeiKI39Faiwpx5aoH3mW2BahqqMHkrEhF6t0h1MGl7OkxAGQotGTP
XUdc718AexIK9tMhoecwHeH+O2FK9X4huOWEuV8vc4HRIMWKO2jdO+nOAXsZWE8P8OkE9AlrDK2U
KNfirM17PQmVrAGlnlfkyQzOEsNNg9L7orVEa1WE2Y3u9UKCohDVOqUqgiLeLkXRRD3H0GPC9K0W
MqFRzLDn/VjfN0nkLNHOK/50Tit7kmWDH9qQw4K6AOoWPp8zIJCKUgsTWoy8/ypo1DGWksF34Kq6
7BnoVcvMjKb0lzXi2cxbM4jZYNFs8/AcMEcs5xso1/mdNPE+UbYxG+zF8NuoRwO5O4xXtrq7//O6
CKfyd5j2mUrEOeDWHw7t43UuZ8lfPUqTqsmi/v1gNi16e7QUGHn+JKWNNR/h/iSSUYKgQ+/COUNy
I/jbtRtgJ9fcam+H87kmW4a5ZF/jIj6GISQfMEa3pkab9xkJ8p/d1aqNrkFP70KVrvWXvlKLH9Yk
Lh5dZyxk/alg7x/uiwlnzjPoG+arPcRBBEDB4lOsv7cznsWEiG+k+tCtwO0KThQAgwUYVIbZLH3O
QdUfLaIlBpR7I2kFeAW1mQ2SUCJC1f150ipqbE48D6aIzu9KU93KeYuNvuxHNOmAp8iVcyU/dz50
LWSGadhrHPyeoM7kkbGqY824A9NiE7joAUAJSBNazB9veWIJxhxpuAFoqXfr6hSognrQ8FIuoRFy
6j8uSaW5VKzQOp6NbPxczuIxVAQFdisaax1SsHoAppkM14avBEm7egW7ee/A1dch3BA4InQI0kK8
sqe7PjHur7PoLFCtMVJ4Mw1LeQZrs1uNoC0Da3Mvq8VfIaiSUQV310VzxjvYWWazdWSAAgg4APGH
DmSCpYQJSDyLB2gbA0Aeq8PFRoKN7qqVbyFvi+4Xyf5+pxvq29SikFmC+UVJFWwYeZ7SyxfHKLqZ
bJ+ChEotWDIsUCN2cInWEf1icMkxjgtOr5dDduewpUGtpp+Xd2g0xHfnE3Ty0CPXHOoGXlAHqhI/
5DymNy1VurcoeoaMAKOxr5d1xfQiDajHk/0ImiT3v9cPUdTw/lsd2u2BMJRXX5bpnxZmm9bu/+m4
GMhkTPtOIsL+4gyUqxJEiQWmuJP3R21e1QjLEFbZIAELKDX0Y5bHLwTYGuCiufRLUf+gAWxu7j8C
uoNEy+QeWfuAVQJtx/2HhBdJh3CnCbZOU5P6HOnEBFbCN9ZwEBeBN+ecpUNVFsSbCNVyS1b0N7Uc
+qcdhpAoOyi4ug9zhjZt9YC3R3MpiBGb7mS+dXCm9ivNL0s5VX/tXfZhD3azhGEFbJiORMV//jdN
6PQotKOQ1caLCyBkPxf4p4mT7qkEGpE1XMQPC5VGKudzmnq4hyxJB8Rno9rWetsS/BaOi/L+/+T1
HoZzGCTh2B8uyACxnMiA9QXzeJW7wZsJ4koVYO3SzvJ74gr28TEfNOzZ6OLK8XbNqN0Zx85nqjEr
BSRnNHqjRyeQjjDSxtHGw8TOg60r0rbzfveBAdZjPHLNZ6bJbrUc1oe9c+Vmbg0AhLUdGzWalDQc
ePegg/4odMTdZicVU9f6TamJhnd//EDdfeG/GSbOIJjXd9WjrGTF8DLIJshQdUCuTLWCS0LQgH1W
LyGrxX/hMpc0V+65UJqgDs7aIRfPwztgpP+wJSj9XgSt/eZGEw3IboJXbvjlOOhMNnjy1d+9oDV9
zcB76SXBrG91Bf2hoU/5zsja8Iz2VRK2GNRplj88hLbwkPsIWTNm/balWrZQ1NcATt3EiEEoCLvM
5DDJTHh2VkX5mJ514DoZfbm9/rdaMpZ6CyXemWnfPOUwtvND4TICmJ4PcpWsb0biZS+/v+/AOk6j
U71jDQnVDSHWTpYBujG/GHDziqQboIOZvZItQ9yZYcGujluUvxkr11pOktgo5+r6kXgef+o=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_12_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_12_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_12_1 : entity is "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_12_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_12_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_12_1;

architecture STRUCTURE of design_4_c_shift_ram_12_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
