// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:43 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_13_1 -prefix
//               design_4_c_shift_ram_13_1_ design_3_c_shift_ram_10_0_sim_netlist.v
// Design      : design_3_c_shift_ram_10_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_13_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nzu2b6+2pwFmlGio+ZB7ip57VZx++Axe5c/M4tXUaHoMXfu0Ohu/62s2rwnnPIytcpVXOBslp8m+
qmnxZqj2/kr4JFOLdjsU3kmGw4y09Iw3NWvVZQvNURXhEBeu8b18tO3yhg4GxK2+7OE89vkbg8Qn
hcIWzjnRy1AJtD3WyNO+KjXhXaXQgrGE55QmPnHRK0t8PgoIYfRm1YpSRTjIKWn9EIkUZQD7/F1p
13uwGN+/APTI48AiIskWPu9DnTL7EbxmlOJMHUXb6CI9vDTGrieWHvu6j/orETsjkqvS3AHf9Ou3
FOsXBbDUikL5aKt+bIOg7KwwIscAfP7rRV0IJA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FPsMJuR7NYf6t/1KkSRnSMiyzx4Vy1ffb3vIh1+BfkPUR2EZc4r6K/q4wEkI9VeaDw4BGVDIFn5s
uzN/aGbo8LjOE2DYSfH9uN97dsuMA0CCt0SHZl3y4Pu/GCjWMF1u35wNySBfaOzOa5n6LuI2Bp2r
8IozQes9+g8hWhoD/xSKbcX1xUKbrVgIRDewAnWNC/RH6HUABKxv9nxO4oOA1ydNGRyrQElgn6Ci
krySMYkpqM9ettb1bQcIY3669AGl58tSy69xLpH+TVwRftsAY9cug/8/HmdMWuzpn2hTWoWGeoTz
q7PJOUrSf9bBprkQtRBsaNJLbx3pPmw/FAk3ng==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4928)
`pragma protect data_block
ki09DA6XyNhuSrLzXzd+5SXKQ5dYH0NjoHE+qfpDZRSW/S0i67b1SaJ55RHBdZc3g3Ny1ivfKqY3
bjRLy5cSs3LA7O+ieaoTGKhRA2B5rzYxe7KR9uU9jf5ZRM+zSzpOs2ZifuwpLgrpxfc/Ic4QDBQz
lz1N8twkEEAprmXjV0NG/k4NLhXwgs1TkqCWCsGqlbXSFZijEFCZ8RPURogYvGmiowAnXUi0KGJQ
T19DMz0Qo8u7BzI0BMfGKV9DyGUkbvkbEO+KYFtqrhV5l64YbhqZ2Vo6DHdRuxS+IBGbdXbIuvac
abk2kNUgYbAf0GBioUwxkhDxRysVHp42IlBQDylLAJhH7YFAJlVg3d0UqaSMtkORGugyr4YGO5mn
Rw+sPjiAbJ9B85tRIe3eZPB4rhkATLgNZXAU5V9TJpvj685cbqLGC7PbrRb/WH9UMiTQnfOmK3fm
TnRi2lOATeZYaqavJNcRgXNnZlXyN7KKJIPvDZYgAZTxfFuOi3/liB0eH3VXf0sm6m/XYxtGFruh
Ry1oCTn6N7GZhWiiolUUFDSiXS1Dq1Z9N4G5BgSo1SyQ3LcR8l0jRiML7v7gqjwHwYPm+EqVFvOk
K4uCTgJ9KkUUbuUyStWZXR4SE1kTIMpjCvQ87dxMVfW/+eHBJbUp/nfTK1r9Io3yFQpOCnen5kuT
7AlSPtBnYbjpoXiueJdJzjzNWxUGdz7MZYzKhyX2xyRcpzm03wgWMQKG34X+gSbeiz3B9AxStS2w
GNZkUD0L1z6FyjhJwgSWlFvgNBa2lFWmPkTtRck2F2Mhx6D7i/1eKNshCkGXUlcU1YSMn6xcJ+7S
gxj1KAms0gN0zVJm43T50+dnAaUHEskh2EicFBch9esalExcIANfXtRHJ8HvxAZIQmajwFZtjp4u
mtFq0yDEfwW6Hm1w2aU/fnjQpLfuaiEro2IRPwxvfl/U0liHIcT3PaOofGYfuBYIkRtQSLJQlC61
qv6RvyZD8sVsKCEKvSz9ly0FhFuGXwmTPvYoyJAPiJm4jziqqjnZ5eh4nKsUK9lBW3KCxhPkAo+i
Dmzi+FPEt5hkJL1uA0CdholEPxTpuR2hJIYl+J6NWGeDtbiIrj95FHXvZqZ0/QgkwmTcQGG0SDGS
LhUT6z8jTn6kSZsHDWr9Ht8DF96Rc70HUL2kbx6A0brN3xXUD6Mi6hof/Ow+n7PbOsxaDLEgVviG
k/BFU29IQnpNJ4eDAlzqQ4FGY93KVIFmm5ugPQ3Hy4MRyPKlxaJFP53Vs4M8TS3cc9NX0w9FmYPj
lC5dc0ovqRRndr2c82hEtza1MngiUNie4LXtltDtJawsEIbwcx/boTC+nms/Mrn/t/e422/rEQZ7
w7i2ah81HKX9SEL3cQAkayGOkS0zk0QXJBX1PT5mjzfKE4uKVh6PH6w5Ul1LNwaQWU0g/II/6M94
M1nZAsRkDKwOxzBAGhBgZHvaMRgKX6VYIMWX3T9ua0Hr1qQqn0M0JlKty5JTo/irfVQ12KTJkgbU
g6/8fxdBLMspqyHQx9eAvaot/MZmAEQr5Z9B+qZ3DYWfjlMuZswFhIcCTLljBVPunIJoWHdxUtCK
jbW31Laxh6HJsDs4eCuGE4bGVXbfnxYuxZpCYZZm/o0im4gO7XStB3krkSkmmbR9qg8a6fe7lhd5
kKSzo+0wsApG3yZvjybmjvP5yVCBvyYJwZ5vIu+Q2imCc44tmZgIWo25727eqv3czRQmtu9V5MoL
fFH6M/CYkdTv3Lg5MgV9Utn47Vi7eHl4LVMghmgkvp/96D0O43KF5jBEDD3GCYWTwqjgl/Dzgude
MfSqIILyitSEVLXWyrTWtXxgWhUVOHJ9lOS7dJNcpSR+0/If9+3IDxV+kEeQ4z3fPktjfQvzJWXp
3wnBx4d9yb8G0Av1uETv3+JQkCDd3jFH+v9GALLQ7DmtvqvBZ9TKP1GAjklmVyzAfwB69b9U+bOt
og4D84LXi7hODQvLJRlNibUNxMHkc0RGPPPOpx/oukgk77W9CG0cQxSNnf6aNA/cnUrT2NRE52n+
jPsIClXfFkIKP/VHvQjtW3pzxBn8It14wtunqr0PEbhQzh2661D2MYsWr8SvG9xbcNVSLS98U7e2
Wt0X3YeZQivwO8bQKAY/YNu9CvDFwDmHXUDw03NLoKyrVMZEMQr33FWzAr0C820n3CRYfbQAWscP
wDKGfEfOsv9rMO60ZUWAJED8yGdyX0fS8kVIqrQx7kWjvGiDAk0XQgmL6U0VJU4NM6zyqJ/p0j+W
FvL1yNxFvyA6cJWCeV5YvdV6Nif6EyKj1w65NE2696Rp/mTgk8l/N/Sppn/IAfW15Kkilv8BkLO6
Hi074KsbVAjB0ZjOFyiSvukQGMGbZHP/IiRu/qIUJrT+QLbBY5OG5wp7urKWFdlVI+RUedkjhuNc
wMTOG+Jj8uLMiYSJ1GumiRdwAwRrGaWs1iB8UGmN6lCOrFC/raSQx5A8UTfChQXggm902R19jNr/
wIQFkR5vdaOG0plCUrqzPqMSF/PPVarexA0aTQco7lb1QPpFK1HGoWWy12yQw6oJBo/8PJ8IgoKg
JqURQxbJb99UsyvU4JCySG6r5vmQHp03CX8KVXpL3QgcbodkDafjmd3g9aR5EROa10pZ3bXI3T6s
cKGbEUIDTg6/K04BOkZaq3qRnkvdiQDnBR8KQfsOQdG17HnjME+xnX+I98q/OkIbjuMiR2I4VmVj
j8uHDUo+9EFHHtbJvtfcHdVtAEd+VO/07gh6zlQIwhG7HxmjRjBa1c/59AZlrnCvqdvR4FAPDdjA
/Zh1A6vAABrwASYQjnB8+Im51UVITt+J3eCDGom+w/fLqMj2PYl/l+yb85Y85iyfoUOd2fk+RN/i
ptEbH79jaBHyxkyZE9mGIPs30HrdXucox2MWc7DHam60oTMNzmD7aTC+B6pn11eOEBYeZb7E2iBR
1QBUOCjg/AJOnD/D5P0vVHQEL4/qOMRSGaC1U4TfVxwnf3DL0+b3M05OArHSsxCvuQlF4o9gsc4A
32IjRDHnMBgfpY+RhsT2yfhliL2VjZCcQygPdTJJph6QhWKCEb+bk5MtqnQihc2t0VuFLjS5dRUd
wevDK1ZDFYCKZ12MEbJK/H4SlhXpGT6U9Wu9fo3fW+EqTCdN+83tUgZs/T8BT+Py3bdZWr0M1GOd
0vndbXqWCa+Sb6VPPAZsMjBaU0Ts/6tb6COl1pTzSo/J/UW7+p6/9bErl0q+pgAmJqBCVPoszOw6
qJ9H6G7PBHfpVUgr8GipoMnuTmh8TBxxIBegu6YRKnhRmZKTRjGlTk/6ozGqW87skaHt/3Ofs3FB
Lz2IOc2dOYFZxRoCBkv5cT/0TO+QGr8d/2R/zSnKvQlhTjTtQw9497ZqK/Rsdzjv7hRl9Oa6ij7F
AFWp9qPkSJfsgteFDyBhUhtIDg0d+uJCoyf0V01TqNCZeqxEX4A6TRdpF8rCPaW2BbH56hNCsJpq
UEmn+BfOCWg8jzdFN5FkwqX8bs+rCera/7dzYKqaiWdjnm+u1A7d2xMrmjeclyOXpLQUi+FUNbOD
d65LyP2tV83Ydv8AGfz0huI0NM0Ou3MdVu69D06Rn3cPIqGqugPEZS0ITpHCPMaV1ReWbN02l7og
GMUr8koCjprqlhblYJmw4PK29G52/fvSUlvWLvzumPEwBD/oh/teBAsDdOCUyuEillxX7UoQlnGC
gH1h080hEp7ys+2vRUwKDjkEjNxxWkO64UvoJ/FDGw+SMEY0e4HRctP/UH4mmK5xAZg514nsRL6p
kT+Wc0vWKQBFpfxGIMr6TJ7Rt+acgSwoozKvQisWdMaxefyv3FdNYAZj476hYLcnCLmbQ5HZbkZS
SI9L/56x+wNXTc39hSj673A270IuwFc+kBYDB09nAC2AoTlELBEnHF4zq68MDQ+vtmb+Z0t1acJq
qxqc9T2G2fcIDzskGttlsaUPvQmmWxcaQDHz29j/bVtsxDo9nxAKcibvOlfFC9OByrmPd1UTXVTS
eAavjFp1rFb/+AENclwDF3DVT6AEOMhr3/cXaIf1VRZ5C18duGVWy4Fqvw1w1mS74QUpzyv60wJ8
vH407YXlFChNj5K5IkA+pLefV66td+DKilAPyxj1GkvmOUA4LmdXe5nPk2Uw/Kw5KXf1iG843+CY
ZMXyub/B5ziFfNkpB7hPNzw3wwsWriWfZGP5Nz9FZ4aHmfAqGY7drzoU4Ee6Y8FrQGrd606RPK75
aojeYtTdKGomdQxGf+zVkvUa+CsdHvjF1IcEQ8e8xvvFfr5p4VMEC+rBxf7646u884qaJTQHsJk1
D8k/bqIhQK3Sm+e+6F8VR/Fpo252TfNPruRLuck9h4uMbwDi6oikiSueJfyvxEK6FLxDqJIZsOKQ
O+paG4ktn2kR8g22zekMdaTEb4WQ5X608Cs6fcArid1ehwBqL+7jH+To5KScGe2jH87+MnkVAB/R
CJKh9ZirrFrV+xIHuQxj5yEUOPhfRcKBRjwh+q738mE5ZDhuh6zXJOQAIDX6+l63wOeuirBROq8y
affKleBbxeL0lZbDxnnVqWFrJDgTC7lPVgnj6cKgtf+OTSoYua81X1jZoXBM4U7nKU95Ss+U5Tnn
GkSzDrzyxFE9uPWY8eiWPlb1LxUrB4Pilf9lGC6lDsYobIjtHveUsoOzHoGXY/2oT/9K3qnKW4F0
zdRWYT+cA53fdByw/mKwmytIQo1m5w/NXHyWcGp3IJIhEbkhKOlV94J1NzGI6M0zCA/nLLtg98RH
g5442pZ6hSyrTD8OlBSkQTANLQhc+9tXTryPdYvkn/IHAUTkqNfGAH7uLzaatHO5mQ4KoW1WSxSY
T9z2e2anfrPhTKrq0eiePYwmIK4/C0SHEByALoHvTb2gEz6OHi9XGYQ00urNA/1IwFFuhkRJXtQi
zCRXIAvbCzJXVqXUocnfPvc+h7lgR4tVX8TZTH2e1PhbV6BFGHB7bllgGW66+hyaT5NZxAr4QKRd
C16kXSpqJUDuASGsou15rq28SoTbVaDLv6fMiLMdnLAw9wMCmGXaMrK1JlpLKQYG9bviWna54LuO
Pe1foOLjzVHU1tzGG4y2mh+2X1t3cApmARc3hnAE2k0BcamLNUyRsOejKBC+BUC+iqj2cBEVpnEQ
Kzomd8U1exVGND6xx40sBJMGpjwhEBlJJq6AmG6ClL08hVYn7psRoGG4R45II9a3rJ2dPjCYtAoH
QsohdXa16WQgBZKgBuewgwlzwtN2AiZ8Blh1hnH7RtubwindY3bcJEO1Z9V4f8tjaW9lNVggFAVO
TbAu6MGtC8ncsmB9hYsIDVwoBjHyPY4HwbhVSy16QS1T/Xz3ETGA6pMZqexezkCF2s+1VSdVlzwJ
1xtRcUzsBu0LZ5RNyezY5ZSuxEsYJmLvqtD8g2ICNPHD/Rgg7uAF63qtFUDF5J82XR2yRC6ooGZ5
T9jJvxjPOb73uNEYBe5oWX/zx2+SjDP6NoS4ALV7bcgn3No/rmVJm+m0j366PF9hrzI/pElPYMBL
Q9Z9T6wm1Z+eDkWlX1PI4mrqZ5pZUjEeuSlAh66YQzWUukbASLZL19H6UpnPy5vpi6x7+GcDOjLo
QEafZoRJAFpEMGyXS2qzh73IdW72tFA590N7VPWrsfZXXOzDm62Ep2nYcSGtTIhmQhzqoA63YzQW
yi22l/xuXS8vsOjhek1rSoNGD+Nujwd3+jqd16r36s8iKXS4fibu+wWeT0vM2Ij/y8Q6lIvKmCo/
TLn0Rqo1W/a7DLtN0SlbgNhi0ltj7CVxpUktPUlnLlO77UyN239BU+yXfpCJoH935np4/fbCsZ/B
6oNnLtXjScM7Pw1LUPWuey/JxyTqJJA7Q+Py1uA/WklrRAM/v3ZPFPiFn0ibODc5dtpEbFH04adV
ztq3CVJ9QhmD+etvFqXwgv/xz06oHYF3nLzBrbBB/wWkPfsCkUhc0kxpisVhoEGlzfprEGjWwT3c
yJAVduMSNKI6FxuuR8VqochwY6srmjM6aLdNj52SDqvwmBvnzjSCA1UxmDH6ekiOJM4EoLwOeWl8
+L0AbMX8i3JaZGhofqtQX7orIC2e3p9hpkEiEELcchOTjd7LkDypB1UsTjeQX/TUoosB4FI7Yigc
dq5Rd4nqXz/dYb6iHlQZ3LeX2tUeOe6ng93XrNmuug2oI9fbjQ3EhlTpC9as34zuTnwzcuqKAP5Y
50ZvuF7vOgM6YWRNIQ/J8WouHH1BBDBYk2SvmHnuxdjM5GNRZsojeG002sQbrhwFD0UbGYNZ+b8f
Pjycc1kvOV2Dl85K/cSQCszMKkArzhIJbpra0hIZUBbyQk50GxXSZj7WpXgxH5kxE0FbczRRz7bK
MLl0ZWCQaw4WTdxZeWqHYMJxsNR0/Lv7l0YXPYbT3BYzK7DMRoY3c596c1q32xS99fgkjMRyqQG3
MV6eGDQVLtaybxMjcccROQt6bJrNP37ACypOWu313puetOQSDX5DGi/8ZN/twXNDU2zcYFSXSaRf
sdtAhTh/+YA1ncTef7G6ajx+ueQ0RljTVV8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
