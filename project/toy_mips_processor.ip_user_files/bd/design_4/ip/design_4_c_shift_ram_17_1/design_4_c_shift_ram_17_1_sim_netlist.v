// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_17_1 -prefix
//               design_4_c_shift_ram_17_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_17_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
hI/t7ZXVJZqop4z0rjFIsagC2Muxiy7Ly99z0KFHFt9q7hJ2kof0TpmNHB4wt31Bk4bHGbubSdJH
ai2bRqwmZ3bwIL9msPwaC7ugcKbfN6S6YROvW7D96mxttfR0Y3G1zr7keu3H7VXfH34Uyc2hVStF
8/t+yeAcIdKCNkfU8bN8tu3MN20Z8aAFUFWeVsSnfRfFbi6JsiGRHBJwGzHEsxZpQsdMSFDvwsoQ
InX6dUypL7hdZUmZr05wBlENLi8YAYT5Ik5VmewAr36PYOxCt60gmctXhRvbcCemmQGQQe1Rzucj
cFsijTs8rNmalnoDG2qIjMofRDM8DOeI2l3Xom3ANMxxXhwe3N7gyoRbgRcJ94WYhsPWdUBW8deT
QNZezL4wkeJlWWsHCjTym+MbCwyDQDHiQQ9dayNjowMoKlk3wtjoTQzSeHL7V9siKXVfqEwKa6ib
UHHoxdOPUS2inPrNsSFj5NMOVwR5x7fg9xKnrqz/BTjWsCaM0JFwAXqHNwsGfBEXOggT4mbiGCa9
zv/UOG5yXNXmRx3ufwHNWGmZ5EMLcBNHLkjgzT3t8GVJgTjUHzXbsbv+0qxjZDpt3ctR5eW3z4VU
tL4Ic1NN31nm9C0/cND1aTH2PRRjeUhCL9ryzLzp4/PO2VgPQ/eF6in2ibpPuO6oZj+jGUuWNj0Y
Wo/sh8YLxfaY5Qcj2qnbufDQfQ50BBn3zU1nUj9VHQbwViOIlJgBxi49mJAmZW2gy7EDkjWNirIw
kqEd7dWFjBMdOFyhk/oRabbEMwLoLMto2vCcCDnEuVQeRpcebKyczHoAGU0Mu6FVClVNxj1eYLq8
SZBWIqV4DFycyTW4ZoI/VUiJ9y/0SktDTD7skzcrxekhFVgdd9/mQkByOfCaQbhbC/yYS8xSVjxU
zjC5JGEolcw1swrKPkvIif0MVUOF42Zq49XYsYjZ9V4M1ne2xPR9uVJ0SeLUlRfENf0i7k437guF
8V3KuutOXtrSH0IC9XVK2Cm9LLZKiFd/JS+Hk9ltebLYmWIKf4MBedJN4y+qbpmcEuvNMSF6Wf6b
imv+iM5CPiJBYy7ZMS2uvOw6OYkg6bQXeBV/6W0wKULurGZbVl4NQaTSYh13DBK1B7QfVRM6S1Bl
h2XaR0CpGu2JtEGxjE2EotAgktlgtX/Qum4vhSmWfuBD8iiWv8taeIdwN0CzTjCAq+Xi/XNf/vYS
755HU9PurlIkK+vaddq9+finlgnVL/FVhoZtmYxk2UABrHVltAhf/n1FGZiV0iOXgKPWKqtLeoYw
IfeoTeXupvVwlBigol0T+MiFMh3Ovb3W5sb5BBWg9G9AmKPKtw6euUKJmzBlc08kY4kFmwdicKTY
B2FPOhJhhRUf/sJ/iyoFUWMD6ExyHXynFyXBPzT3Iuq9nXIpuLlA4KNWGVI1Q2jN2xJD1Fv/l3a6
+wBA+8/5N1VYe3yhA/tKGySGFRfxIK1/3bOKTp0v33MRVD8hUo2QZHLsHx/knsX9JSkYmuhWzRqF
DW8LYDSwMISyd1JRW/NhuudNmBzcqC8Ee3FFm9SNYEf6KQ5QCJSZOHanlKD4nd+h0fB9uJYt27L6
AHCQt88YCH2P5LQSA3PH899+hQ7IpEvtdYVN3j+nb6ks0n56pckKaZxW0/uKRDEcdkLNxEkKTXEp
QYxKFriS8oMIlLfp4uNk5ONTwZJZ74YaSGqooTUxdrtgdz+HMshnqh6+CQcElXA6BB/nuxOtPlSi
iRboDpEOIQrJpWpGBZ89TnM1wDgWhhQz4N4CBct4/LYP5oPw9xVd90wi28nXQBRujseNxEAyjzzO
nJL5NBJueSpwnnzo13pb8/SYVTbKI6gdHRs0UyqrOOI+3FaEFetwTpdmNKvGZfMiMfbkgqydqvQT
aBMXSOHvwKsVyX4Qj2CJiEFAJ/jbyqunvSfBpBZ5rfruoNHECXfdxvdf5IPEQ8ehuzCwwIwFw3Gr
rc605Vv8kM7nDzDiJrGdR4mXxJfy2ZAl9DFfsRL7t+1S3GasczPKute0O2eDhEzuC7X87Pq5QHO5
PtyveUDuAznKi3nuWd9z7Q/lpHAmVNQhZvbj5Z9rLyl4rMouU34bpigL69xcalmN/LnosFf/+T7A
mO2A74QFURKwnbtCgysTm+IzhVUdLOh8v9hKGkVPBEROnIbKbzLwZzkuiAHALEKSgnO53dBaZ+V+
XhlCR63th9DHmHHPM683I+fSLSySOzX1cGom4DN/ZDVb+aX9okzBuGNzpHZoIzTFAO4daOZO73Ti
HP/O/tzlSOElvA7eufkxqii5LzmY3ndyAXr9LbGsSRIQ+cF8p/zI07pr7bEKS4d2LlpWt8OXxksO
nCHP95Rw5seqdL+tn8Bl4ZXaiSTLEXej1xfZx6pj4GsBcK+cUF/91uk4XRrPn6nkYYMnx84Esm/p
K/JjKWsuPpcNZkQ8FzS5zP/bz7yWCq8+yJmu4nFxn/AxB43bHILxpqWr93/d1KvES/Fc6VDquhEn
Kty7qc1VoBUXX0bWWNd8D13foQDNGSBPze5bl3Qgmfd5fhRRyE1GH1VwVwN2pmJRf6IDZcXTn+v5
awgF7stLnVQOsLSk4L8qb/uO9xsM18Wjj3hqop4m16D6j76fISJUSHvNhJsn/EAGY2kE6fJcqnvy
sJCdQ8CjhHb+FUUE/SJGQgcYpZuiWWmeu9H9A0wRrU4nU5BqT9iM0qwLyzXun7xgn6OWy3Yexrx5
+xWyD+G3CF9mdJSPYPgor3MIttRejZvT4LzomcSSw1Awulcj0G9jrbgVQnYksoLn1ZO3l09w+68s
QnSYrcI5JO+namiFfryrxsTBNy3t1L40ljWLoXCiXIRVM2v9x1+sU9hLLDJEbYLXIcaK05Ksx2Hp
Q1oMgVyMyuaZhuto9mme9OgszJk7c8Y3FZuMjgnWbNyBoibUvYlheCe12atyzyE1q6p+ciln5q2l
o8/UKLWeL3bhZd0cKJfIECE1HOIRFNmovpZnn93eGHYmqhKjXRqtdc1MSsR3MS/Sz8MJ2Bgj3R+B
u/kfW9lZkMsZ4I2j36l0xfQMMnAaCvS7cgQkZnZGSj4nNIuVV7G5qXLmm1Dri3lDfqWoYmDeM/r0
CSI6F0I0Ie8qCyjuqJt8xCupGGOdzu24AwAS+IJdhGSoKLdmso8urAE+DxfgqOjtPyrO9sgA6jC0
XyEREDNqHtURQXw9TUapkPoV0fC8YAKAeKMsWi2ZbO69wBgHynI1Yf14k6HBllsf9fxDGjQ2OWwN
69MYdrv+JbRwwHmd4nzoesaA1LO07JqOGYcvNXdSjC1e7naER/LRAfTymE3NsZyoN7HnzuAK9fK7
r4K3SiaAQ9+ULTMeO9Unf7+fLHUqg/kgQQvMhHOX9sT4CjsGmMaGIxus/Pd5hiKcq4phXmeY0yRv
ZviHImTosuhdO/FUDLWncs80pvdYCyOZ2mHsex5ZR1pOI21AXdQOvLCWc9sqBkTFsbCVhuTR7sxH
Mz40CGtWhRQcjwvH/RVmlM1Ew7nj0ZBPFEH7y83Nr0XuD1UQpPMHHg8qf6fk2OvfFstjWiW6lrZZ
Vq9v6RMrSig5AVd7EdwP546r+sUNjuKSUapy8gKcYiZLDv3TNtXjHNmBFxds+uWgDBj7Sd1hYRDL
FrLUxVsFbOCorhYYleZFebPiMPVf7ZoIYtvnSMU37sTJtYeGc+8XjHIaRJElnIiYSPBwsAGOaSca
mjoCPU1UyEWO+f/h3b9udLeb9vQC0oTirNPNSTa1MqEj2iruYOEVdI4narRO2aZWIajqR+FYcSDs
tO6O2u9+nylu9GzKACZK0FS9R/8SoRFWuycXBjqEqyVi5GMTplE12/VWMxmDRzKQA5GMv6gwG+QF
g7N0423itKzUcWXSSWXiPEu6WmI8a1NtDVsg9M8zELRkjmQYVORWvr8wONHzEG0S/0gGMRf6ri0M
3YVhJDg5PCYW0iQUrVxXQ69x2ItHiCvDNAa8Ov0rjsXApVzlIemps9l1aqM5CX1Le0owagUCw93V
FU/u3sc8PTuLPr8lpQuUIz8cmK/rHqw6xbcHiLYeoCOQKOzjMT68zyHthloR3JFGGqbefcQ79pBa
K3iTuawkhLRdB8fBafOwnlV9xZ8uj6VhvII5r0qGMyT0bMUztU2/LojfDDprCwyQEvFhpgudjn87
eJ9vo68lmZFVLZe+ZynmyvaywsywStAnBzsXl1RA7CAAgjH143WdA6nro2jEV6vtggpfOCzEBHHz
Nqz4surc/MZChe2atXN9J6Rv01t9XZ6sbvT2XK2CkD9VVbuEUaH2feByWy9BR5tvE1JPWs9oWXvK
Dg10t9Yp+kJN4bxtgKiZrq0N0XUEbyAtfiJIo1Ecb11lDVtyqJXiEu1Q9FcqU72J+N2HZTQeEm7L
AYY7KaH7oFHqxgtRMzFLt4Ux9O77fYYQ47tWxfEDnUNyaK0DiZW6jllbt7g/3QaGNdSWDZ9/oX+T
ZPbE4nUDUwR3GWSqOAalLyeA7o+N9d955dMc4czRwjbEhjGmpRNxV7IDThrXQRdeHroUOyRIWt0Y
sVsOG1ha3m8u1ULaOScYPXCgtj65CCiglTjfmtq1UilXETfM6VV7YSxLrIiZSW7DIhFWpLXFtJfm
mfrGHFSo7haKlRlLdrjT4Tx48NG6jZ7Z0uo3/hXOkr5UgCGqhWwzpH0JGeRoUXeFetD/ArsHl476
plDFmU76vOvVUrteISzTGU88bwF0A4zy9hTSqH4ulWUL6IM1ndZ/ivV/BNmX8yMXnaxpr8lCTerw
58Shk1eV1EPZXucnDFS+anzPAUT2acOWJs35Kaf1Qf1vd/wdTSgQESudtCc+xHHd/Okz4fmv3eDP
j8QlfRXBKqsgXn8Zle6JKLDMX1hmuKHu5wekyDsQmHlTIdAY/J65ydc+QF7StKQ+OoJXJyrWVRXu
r8aREJgg1F9EUjeeUmWhHAQoYT9z+UMfUowPvyrNtzS26/o84iu2xRvK/cbO9Dh+PWY+TkXiOkF+
0XwiEgZ8zx99INWfHrh5sWN4Lrp3Uly4SeHEs1yNsjjo4vYqY3wKcTVvqwZ2YCGnXqvGm5DxXR7b
5eGSkHzTn17Vi8SZdugwutxpeiRXtD2H/6Arfn9ya9+DanX8/8S28ujqa2RJO6j3njR1HmgZwfer
VaRmNDFV+TD3KCejZIpY/jalDqAuniT7+5F68bPzlo2CJIxOOIKYyPXTeuCEMOtoJ4aH9d0mRO72
WVvo2RqSOVPKifOTVboNESzYYYB2OwF3M5VZYuxWVsFyBDseK4wUt2ipWVPg5yNdzpF6AdVXv2DV
/lHS6WJY0T8T9Q2M+m0GX/oAIxbEDpv1tv0n3/uMfosG5egvRLU8OBdJE6AyU48D+BzpRUj0FN7W
jZ5RZmb+CEq5/0pg1OGBjDdUxH9+P5uhoAVcg5CH/lt9pphn+9M66WKFpeHcogBUkkZEeynGkMUo
CAAUUqzyrvfc+WtErMgtD+gP+gXFBO7joZwfoPA5dHIEpzJ4Uq8h0bFEjC6D78iL+A1SHr2S7l0=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
