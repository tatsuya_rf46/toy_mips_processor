-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_17_1 -prefix
--               design_4_c_shift_ram_17_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
27jkO2t0pU84MJJIyjQ44NCnNBAUVFCeUi9L9BhQ9nXr7GRTxGNPefDN/zfGF/HKLuU3xhQYy6aK
jlsl9b9/Y5uwlF6xJNHGPlHtzOY6wGum7n+jydclQZqq9ebVZqBkSs66X+DwSmZNHmWexrjkDUXd
OCd3ZR1Jd4vKN3pWoHaONATmaYnbNKzmk7Nf01laK0zH1GYd8VmrEWD/yDjM3BEJkaG7ifM9gd67
ePQ9n5AMl9XuKXTKpDLqU8kdnByPHD3UgScoWzFUXuXSzrtAchoJ72vOIEzC8E9KwBM1TlSh+4tW
b4EBuXhjnbO/5+kFpp64hvMoctue90CGiterWy8YCKEjPVumVHBk2GluKCmvQslpjkoI8w1++mN4
uSDBuuQCGs5XpHm527Xm800Fajh/owAsDc54ctrINMSclPb0pEDF0EVCsb4OHPH7jEQpPU4v4gGs
rhloPqTnNpS8hZzfVIDZpvUrReaPrsYyTPlbh5VssEhahzQChBAxA0c/YJXwBEEwE3UONunJN5sX
SGk4n7/LDN5BN63g832bxzV0VLRRWiIVacOjLD/VsBcGtHdZS0CKVjY9waSQ2bqTElxe/DApMJmR
yaMLY6j6BeizMyPUa8xKsf/WixTZwnEhIljvYffNfedeeNBLAJKU5HGy9FEch+fBju5oQZu3Ry0j
A1BGjnqOV1EFQQhV6Tfl40redicHB4ZM4l27sU0CT57z2epN/e4ZSD2caapQYZChZap1xCO2o8gP
+RL/Rp6VVcSM3FIArjc9yXgPlyXoUqZ9oi8LVf99YsU7rVNLfInTbLKsdynzjdcQOSFbRWSP7Vdh
UwEs/wSzz4hPlAixFS/NeiXe7Wd9ExgZkjz3PocU/kunKdvOHF9Lf7jm8bN17Wh6kzngPVEyFtwN
JDP48MXwvC7A4GQKo1xQWA5v6m8kMSMu3QWzdnx6tFA/QnSP23dyqbVQg7Q9I+468B4vfv+B1iHh
VGeEeCss5I1VETYj6TGdHdkn9jHvoHHxDhS9KdDe8M+mJIRZdeI83SBs79Jk4lkeUAbpEj/NNbxj
/iA/jbyssleTHZhZQ8KshW2DsxDGqrIr21OOex11rePK5QntTHiWnaxf7RNIPhJCBqyJXjQrw9k+
lvmeZhgO0SZsn+Pq7qs48tbLuIk+ZPXKVHIkVL/Q1r8aGDn5iBaSMSC6R2V3eC6R7+MY3lLgLgOn
8H4zCGmICMjo+4ye6CpgduUmMFJ01gzTCKJCivrxcng4k8ToLFLtI3cRWFcDO2ovXO0RVZnRsFPu
sq3sAj4qlVzPWswDmHBINaM3wIE8WXKT2sm4jRGvcFfCVEuAoeUOUcIIUGXeC0EovxqKWUjxqNke
cHg44XKFh1wcxbbTgXUdazq0RsbiTmeZPJWQ/0+6H2at4KSNeQ+dNBclln2PrzQaHCZxmF5HOHjh
agLy5qXLP0UsODrx2qaEPIp+WiyoOrlCX4XRXmRtHsSbwO5QYRKJLG6RJeg8IaM0XKFVVR16iWls
mn2irsVp/ZAB11Y6o9R6ZrSTpCtbKJzsq80ST9SltPEUDD6LCUT9oY+yNAXgveJXTMV5bodzcRDG
vuCVLQ/NLUMBn82A9SFg0zYLsK0Bnkq5Gl9gRZSB1XKKgqcIlUDLE12l3i1Z87ZSbAYt/2AbQHPY
ach985KNXDFOsatCuzbOGN0nM38UQXNzmU1ipFXm+iwetKJjnE0L6z1D4ugwLnfpXKykf22MA8Ct
2lYPTvbhBpn+kqLb4pfnESrjZ+RdJ7BIf9YfUkK/b41NObxBc0JgBOTL7GHS6MzmRKT8AGoER30M
psuS1g1RPnZtOPfhNFCAzTLIt2LoBpkESoZ5oHf3k9iBKQH0VjRdKOp9MoMgtAOrANJnyR73owGS
CkcNGypW0Fcv6va38HncA9z7EmiFHmR6uuIeeV4Wq4bGzh+ZrrZ8iJ6bhvaS7Fpg081moMGHf+cJ
lnP5Zty+RoJj9YDw6SErYJChUca3va8TjgQsM8SoZlQnXlnFGlNgtRE6tC1eKtQ81vRIuN+oglCP
/u/lytm4VcUShqSSY1O2sARLNCVcrywyQnKzOTGpHNg6OcC1Y7Mqymkas9w5XjSwq5+AiqDMdADv
kztMhSxXkHX5SSIRXyt4jLj8K9HSc612BMAfqzsh+gm5cGtQFCS9uwYABck9Av9bYEkxA4LXzZoG
MGwEkDOnovZPRDtWucna5sqMVA6MRVF6eQ2C61tmST2tqNPbGU1uJ/sy0R6oyFXSNk9oZRaIi+M2
tog7WeY7UX4zF8o3O6cTOavP+iNuFU4QFr1JBb6jystG7M1V7wEhZnFeP/uvUogxOFLuPXgDSse0
dvpY3OJAQxlv5blvTggrBycZBRsTtc4onYhVuJyXjT7UZ3pw+vSDCqlPiCpu3wHOogYePf3DOfj3
A9I3sIc/4IXnBimXjwIVP+HktU77IqRtwNBwqQ/ROON0uKdDgcy3Siw9UrLLqYsSYUkmXfA7oji1
2nOlsO3LEOD/fCr7P8HKdlvPxZY5tDYT+YoAbbXrFK0I2dz1bHbFwz1YZM3hl2P40nO73fLplEDB
XTaLZ3ac+WL687beD6+TbwZklWjSuesDJG05OZfaHEPqlMwA4z08CF52GXjZex2oQHfN/AUj0veo
xCI1PAxi0iCPet3b/Z7IeIpfXmqsGB9UrcBttU2csramlBQ+R/3MGi6xstarZI6Mtp/6I1coYGoy
U81DZjq/i3o+Bx5eX/0ZPfFD3mx+ZySd2zaAi/gAF8AtPuX+8gQkaCNmfD8/5tmI2mlmxGYSQ8Ke
Q2QZIkj0nvM975ZRRAISki4cB0y/0P5nAOfyPoUK4+K6NBkIUPjtHy5yoGwxhsBM/aqeeLqFzxXk
FPjyjiUKx+Beil9n1/ynJtdRTVqepuOoxShbuEBT1BVgyIVnVc+R01pr42uSBlvKX4PlXeRVVOw3
VDPuCKTlG5LlRlOh3Sdtyqn0cN2o1mWA3nxEsSHoc9Yb1SKM7JpQot6Jk1EfAjCnfuv79KNRp3fj
//VnEkDR48MVVoGbT9MTGRH/aMdpUuq9A5KKL4S0Q7Jr3ZLK3MRkmGNrmVRBamJUMuphiNZhlAm8
KENbTxLfkxOetx/ePTX5wrfxh/IyvI24xG9/KqTcebF9GTm2h6W1il6EwkQQOiugn9vCzpaWaVd5
x7nnrymAQ0HuEbcNHhQMCI1RBEryE+MzEz5tZX45j1Od5mck0QTfcWxYX0hSOqHm9e1z81240j9K
SZJeIzcB7BAXvB2dni01QgXMv/wGHT0tp53s7sn1SMFbHOKHklxB7f+JN8ABNrOdcg1PPGrzpdzY
DU4Aj7zBHTB3NHZZX9MzaV+FHS6lW7DrvVk/xWe5q0BM+sYFwbfdhSMyZhmZGg12d5+26Dn6Z1zG
kTseVj63gfNr5NvuewIaS3eb0QnrTYj0hSxwDnL0NsU8yEioFr9sLFvTKy9L0mdDe2KF9gTbmp63
bhNlwZR0L0d64RL6aW0OOAX5HptyfaYJVc8SwsACjbifVnL2wwaplsWSJ55quynTUG787Uv0JA4A
zErypLszinz31+/pyhoGsJHLAZ35uDcOgxlmo2c80Ckb351Z0oSbGmQDo0AqyHTCAFoTGp+WtOc6
+wOu5usXYgCqUWimAn2qj6J8diTgjmOMa7n2RF40O59YuGquYdYTrTH8SPWc4kWGZG1bDETlXmtx
lvpQsgnjDwx41JCth1D2JaDkKuatEuJgY6tpR7org64O3f/1ptIfyKxLRaRLZfWiU16duFq2qC3S
PGMZZvOr2qwaqz9SuEVVuM3Kjqbz9Tyq7Pd8KnjwBTG+7keZv3D8wgTLLos8ynd7kaV47tPTztQm
CLz/hkqdaiO8YgLD322GjxEf7Dzj4FVy89K8+yes9q5Q6KlCD88OglF7m/tdw05K41Z6J+tu8WmR
5mgCuyqUqMRmHjW083bXYcIfTqTA8s1Yr6W5RLDFD7kU3/huHnLgD2OAtHCxAWyhWOKZ+IYZ5Pkw
ROyngKhj6KJlT109np10cDRXKMLgM+JJNxCsESyoFS4mNeWbZX00Zx9awqBQyLeayfIIY5fjqTka
hi/0el1X1EkniCpid72CYI/EVn4B19QuzzhQUb7/ucTnhrvxzxhJPs1m3kI/A//nCqTOy6ju4R+N
jzzVFg4Ro06knej5ST+3pB2PzOf/omcr3rRT6vnxwHLKoZyq0Jnkm+XVxXltqcIj0nEQ67RaKrdE
lZaVeRwUlWuvRiTMGPcd5Vljgs/cmMuUVinneEaM5wzUQOhFz89wYyGPuoawUnJRh/2s8wF+JSUw
6js8iRfeAuLMAhYPnlGv61E82rFZWco2/GSIy1jwCZze9o0JTeuFt7ZNvkylRAhRB16mYQkWAZ9R
Rd7MjP+Yr0swsFjeWCl72hSJejixj4EUC5naMj3YI8CmZA7gW1wajjKKf2pPKHTMPZIE/jN1caos
oI6kuFcKCKYJtYEubR+Xw2ibukEeixBo/wIBCYocaO0gTv1abV6XlkfkjJezVrrQ5Ec006hU8z6K
T7MIR1cIYjbWJpssMs/X39aSCxKsBlUudMGrbXY0DnQhW0xKmcB0X0D5khplEaOBw8HIlpGIMJFR
ZONHtuv83AeOl9yXFJenC1MZKbXqXGLhoEHMCsvEo+tav2VwREVE9l547ohTdibXDbAfnpiOmbQT
Y4K+ouGA5RVWui2shJoefJ53vV7SNzN7Sc1WlDDAUeSMsF3wkyLmLlwlF0jCEcp7gFkY7GsLFPSC
6qgnEjs3qlBOxDhO2cz2C6wCWWQxgcrao8wT5a9yZjPPmiHU7f/Lj7DtdTGUSgtgImjtRBhAkTW1
0FGYzIWomNzwzohsrHuvMjtxH0CrdLgstQf4NE5xm9DU/iqC6Sub00aMzlpT2JGuTXcHZdGB/6rF
GSEUCzxsxc5QEjybmhP/w9zeFwmS7m/WN3kW1yXJ/846PQzsj10KY261b7dOetjwP2ysGEPO871Y
7T9GSfQVPp8dwn+0HSc+r/WgqakWIvpEGKaSEAC38U+xhSGmygZI6in91EXjJWSxAZFdPa+A7SmI
LQDqyRkGkkB7+sjlFii3q8dwXg5opEji8A7MdR5XbrG4dvibOmZTSv8lz3z4tb3jOe6dcvS+N7jQ
oUpbx5zn5dPV9EjD+2xX5AVfvQAtyRSdfsoD/0ty/IuhoACAnnUNz3ZQpznoQMIqgd38SJ2O8iM1
CcmJ2ic1dA2s2iN/EaG904jXApWsdh0zOJBwvS94kQsup8GcLPlj5Pv+8rfopRVzsF9DJjGwOwBL
ipvaIGBZlXPz2wyNH0BJ8Si3mdXhjP7KJa5tW+jgtWpuJ77LxzE/p342tUPlLVXrWAPpWXtw6aNU
dVq9DnSsI7PKDS6MqryEoU1/vKLKqABlVho3t0auZt03KebNWMof/W3V6PR8/d97cIJrzzanbIww
y0XTbcu2kY8vnLxHp8AJCjlL2vbHfHdywtJCo5nr9mj3VCrExqgjBL53NMBrJfFuNMgexmLOijQE
Rt7TR2QDuQCSZEv0/WxuEP+0dsj+D1KjmXEiVxFCvOyyOpnMB4pGiUjH/yXn/8Nn/AcZ8rs8wej2
zzkQIteMEKI1bYF2TuyNc0r9S6O/LtYQT3fGhSzxSJfK+ele6nX60BTvpWA/88p3UNd1Lyud6LUm
2oTvew/sJZ5X27Syf5kNBVQkztWwqhn6DLYG3c1nvJ97ia7NulTX3I5xk4sS9jjHIxB/me4BY0Pq
h4WEBw5m6eewatLjkmqshyU+IXx6UBrPWrXLZnGJ8/2pwVwIpzkzFTN97OiVq9MvKw8QhqwpGRFF
hp/odaNa0mnvRwijLiej7GIh7wD965qHZJCBtJjsJLUKerl5mm+jS2ajCRFWWfrWUy2fSlowhpML
+rst4yptDul+QJRs1kck0qlDXBD21+kv0XM5mHythtHqIft57SI0mQpEwpireYNkTrvSeE/l5HC1
o6qRekM6loWFuOBjsvhO6CGntTKbjcNC6scAejuZxgmPrHJyVlGgfif1FqlEZq1t5I/hQ/FzGpXn
SMuFo2ZSNXsC7Pm/ob+rc097tISW60tsRR4EcZnbLEYzNuzb6swshksVx7MagJ2gEpk/YLuOT333
GzNf/ddLQfcxueIu7E6kkmSbA7IOaW0SkaoVq18jFJCmAp6yNQwbfvjOxVmsGGSZlrmVbIZuSvqz
fS0BnsBE4YfZPpDl3jc8hjDnD/V5hwD+J5gcfQNUZnUFRIepj30o8inxKUjOBiYwrXGpD3niyngn
/p3UbNkixgsHKg3o93GF6K/IeQoALwyNkYRiViaqAoqCq5RJXjePj+bWKySfxcBHiP6KSybhtI8O
OjYjVnK1fBOZoch5n+1GlYTa+bOy3DbyrJnosGRymNLTzhXQR0T9hJcL3ov6EnwgaFBMNnvZgyRm
WMSFQwUxkRh6lw/lNgLRP+jhRC/gEKSKAIkxfHZ1CVjoWCrhXfeK57qAJ502dG96rlkc6tmkxOU+
CxWm9OHpCq4fng7aacWKna9FAefX7ia+caxFLsCDUwPN/HQz6qAU0Jpso2lFrYQ31CDqBq3XY4LA
WJOWfrT8lNQe95u5aFqXsepROGkVRv/FnzXvero/X5sousmHzLjQ4jY0rn5s7mf3adCGXAbyWxtC
A/4G+XNiL1PLas6vof3+2sTpGx3lLMrnCSDvwp26BHRBtNUeM/PlNNU9uYPnrByqxejOOBUMlnl8
3XMWg6fsnhPSUbyd4CjsKLEGwjBh3xl6vDuvpFOXGkSY6+CXV5hu+EsrvYGIcK/WfYiod4tvtlVR
0cmbOdIMxmulENm6eIk3l4TA0F0ClT+xY7DLY/jMGZtyfWKwQ2zssi18UuxDixfUTF7+/e6VhYg2
Se37Fe5SNpyfFScShZKcL5TtAb9p88WKOxv1AfU3ueSmTH2Z91UUllCXhI3NlLTcdrkh09hJsfP5
e7aIyJ3GogphGZ8gz8f7DAk4yjqgljq8AxuJ+d8O57xekp7FdnjJt5C/IQMKEU8LpWLrBCLRqtv8
0xgjUC6YmW4vNnKzU/kpWlkPzqP9pOYLegNdYtFSYitCjD3r0Dr/cdLYNh3GzwdMli3gsr+vJf71
bOAdlyno21MzX8vdR95GDbggysefYwkmbn39ekvrT1/292fCsORywff34/mpErFoxZKR6CsGBk4V
muWngz0csjzjlIzHYL0Z4G7uKDBBOwHfmxG2KXLpJl2X6cSK1pIdvmA0kzBAv2x2fDHNFk7MUX/q
3lGg5AEZ3haFFklg/zkgeMcAty1oNvc+ciX6U6gcAyCn8ja+O8GBkpdXJ7aTt2v9eQz36Av4zxIE
CFW3EKC6ic9PCPHf5FjzTBaZEbebYbniloNG+F8X4AqtKnTERBzpeuLDAKhi8m8DyiafeqUmfkIB
I634pabdXORc8g2jSK66ND9lqV1gBWPQQRSdlaJha3ScZ0Z1iDQ8VGzN7qGIwzPOS7RTKtP4Gh2z
Hkv/rPqcovpPZfL9eHaFYNsywHUtZ0PMY0uSOMqIEKZr0zMP8e7LrtgbLBzYSdE/aUbfN+EltXbL
NSNoSULCsKK5iOIww1TclIIwQjoTbihXPWqbgKxfGCd5SK5ApdGjEikP2foOAw4hRYnNEjnn/tGJ
3mXhoilS++4K8Ijh6VoHQxrjT7qOBh49n5ruCosg2Sq13+brLlj5uPOmEPcxGVctk2UjWFsAVzRR
HoPMQ+J2DE64J12wXogf8AFt1r+qjTadqz04+1TnQoBjt6EN75L8fNuoZOdSOJu6d5PoVrpuTt3f
C86YCc18o08OGIZStjCLJTNtrARjKegjV8U1ijJWBDhy5nrmadh+bfuOmYBh4XjRihLwOf9n4WFH
eBQYO5d7TlGrjCJb5xASFpzlHXdwMIfXRaKYPTKhuLKW8Dt03jjLcUcAoSwsNzr0iSL9hX4aUGIC
1tRWyMjL6+G/M07Rf968N+XQmNzQC+PJh/PuiBg9emMpt1Oj78FZVeijan4WOLVWIlVz6uCqXH1Z
CLUedJr1Z1Y+zF9Ko5B4zG2qdsrecP0SPAeXBjQuzdikGYFF+EYGae2CDGt8EYLF9KeayeNcvycq
vUTIHyz8abi36sGI1nbFdFG0sowazMO+xWJfWUTNVDku2CQHYBDctXp1aiXr73y3yz/CfRu7Arf3
J+A8FtLOTFGFHfGcWpquE7W0Ge5/2OJTiGeNVjd0wy0Jk+AVOAtOvL223q201mnfUkUTbDTm4rTW
EpIZg0w/wjtcHk7CB61EKU0N8+GSXJpiR10Y4LipjoO9hD1T0pCWqZF0HHmhMzgrdpOpjbFyHg26
cY3CjY2xlno02DD6Sjy9Lakxo1Qa1QT0x89WOXB1nxIoOFJdalvOv1290gzS88xucQjCMbv2goUS
wS0Nvvn9R16eOr95AwhyjxR8KPcxNSUD2upImWAnhr8zIm1IaBHHc6wviGzAbyNfJdtK1K252Msc
QfvjsePe7sCj60tFQes2DseUDbufLhGc/iu4pmvFG/kua0iutqADNtsCsf5qWChBMXg+HjTBxRu6
SvaLoo5x9OvID2Aicq2GyWfFJzofYhPZpYm7211yhhyRtW3+ZqVECc8UiceNaR/+Zip/RUcy/qIn
2fa9mPcUSpMveCEXa7NyxSkBLAw1EzeWYklTNF6B/kV52RD7wd/YUSXUyQOFJA7t7T0x/F1OCY9h
0fk3ut4meOAB6k919IneAdl8dr887hwjW3C6+UNhDZmkX7ycOO7kAWdOPpbiIILmpmNuXD7A99X2
4zmZ9RQLdN1lwfO5aT3oRFFxRKvJJPY9S19QzVhoht+7z1hb1/s7mPtAI3TqrGVF3MZZJLOKyV6p
sW+S/fb720Nk2phNvLBZe9R6PaTaTVrt3EhYRvZrsdtKtgQwZxXjnXfC+YVM4mZhKwNCH6ae9+So
iJLcYSbMSorUFoeKK3/SAVUBt9qNsVOBx+b1HwWhRRlXZjrdpE5o4Aaq0NMxSOoLj7CB7+MoFR3c
HvWnXzjS/x3ZD7Ed0M3v9E8IgJmNGCOMVaMxGANblD/kfUWbsSf0Xv6C8yAN8F2ckKt0nQIsEMTH
bHiaYPqk4awfU1JnraRU64s5YZBe4D57Odwg/qcL8cIiDf+I8c1ssz3BhaHuJ27g0grxc8Ar/byw
baIg4c++yxQwqRW4NikmD1ARJfMdT93mahCkKF5cSD+QtVu/+L8LtrRDZB+oPmSzVq1CbXZK6oJg
w3U327wRIxhWxbzMeIvd19in8EqPulWDUZL9KrijiltTKeX6Lf6MvGMsiH49d1R3MZldski3ZVPS
N3P1oiwOkRgprdB6qBxdmuU4aVJZTGtMZRvRTjVktM3gj6pxcB/7ujMUVOpTqDkcjSXLuH9x4Wv3
B9/u4t02wy7s5htRwdTTRSGtuHTQh4OJldGtZgvdnb2h0kvzSTzGfF6cT4EmTKw93dHPKxZXu+T7
ZC/UYGm4zg/7fkzC8T6lL06hsTXkowaTszOLjLJV+B2495ujjmVh4dHu7BJf2hCqOlPdiRaU8PjC
2/3K1NBE3rLQFzULYPaqfjbi2gMe8W52oaGbjcbI67/idrsHIT8yT9jxzNNOAZjjeiMK5tq3al2i
u+QAkdJzx4mT7rg+3mArozcHw7k5c7lcKuj6+8TshWUQ8Gu5PWhHRQacbXoD3npUUW8oCi4W9Z8I
dQCHqbNKHeHOX2dEis8sZGA8h+zfJfH4HBc//UFzkFStP2uyfyW88Xmqu1I2l/kuuEvhpF8QyYr9
GvNHrYnekGCwGjRTAvrdfkfnIPWYttODAF8cQTm8aRXT2+zX84K3IJvjd0U71BTwB2VLMcVzVW2K
iVSYbz5Ux39aw/IwpGnMi12PudBF0ST11J517fAP247K91tO1u4EIJ6nsStxK4aHx+gJYeoYPMrt
/0Y+FbLUBhisgfoauOkzuUNHwQcP0tFT0aHESN3NOWNDfhQmd1Pt5mQJKdk9JZ0wxwizpQBsQ2Ts
fLebtNWswqLelDb3dPCot+u7AD5sFFHzAsLa8jyuKciN2gXg5n95SsAg0munc+k4XzlTVcghkRZD
UfTQKttyaVbJF9vutuf58S5eDh8RqZHhkLmxQrsQFh6DoRGX21R0x4JmD6ioNHzRNrZfAqx6/A+8
g+uSqgDwTyMAMihHO9AdDIm/QXS2bHrIh7k3QXxT4UJHdgOFgBl1MZUiaH6Zito4Q/SkaHSzdQVH
amhyOziN3Xhvaqdw8cwMX+AMCuyRP+IsEtoa7krk7/vQ7sLIr/MpQebxm6YCh3AQuHHU8/BE2OpQ
Lo96hwUrxJoG56AMvtnXXmiSDguFbvnrOnc/QWKD7uDHYm85K3YJqqP5I9pteM5p0eUZzOmH6rxy
BdhGhNjudTb5ccVdoJy1mFwpKb3FSsDhJ46yP6uhfohWfaXhRGgr7BjdCIsu3l7jilzI765URY3T
Sr5igN4yllE4p9EPkci/2+vGk/yiNb2SdKLaELAhcCLdc7KvpJ7ZAoNuz0xHDAz+uLF63v7X07LB
uXtyHESy+1auIE5nhBCtZTULre3w9MO083GoHZt3FbkqWGG8tQ1mSnwnMtz7lAbibvjIurKBUOI9
P+x/sN2bPsur7l2NrUOGLQELyJJbUB+DtDnra2vBj2OIIv/wADy2LS165rBXXTI41X9cZbps/I5I
7vwWBAOjfPyWVw/O0ekYtNQEK+jyHU3kLOyuPZ6AUlNWF0SUBB6knrcaYnrfAfiMbHq42C0+qjA+
39Uxgtz46MbGY2MGz/NGbcARVgVFF1Wo49tg6fx+DSDfJqO/M47AHVOb96HbfF92BjdPverLnXPo
3H+doZMtQipJLkwtnftrr3ZQa9bWf49JNXY3FRJtbh4jAER7OEg47hXK8zXjg9a1BIU5HBZ5HZUS
zUwZxZYkvmzNeIm5NfT1LX95twSdp1WbRBYumcgLYV3WzLxfg/zla2gFi+NlOaxi2ajz0Fn5fn4+
A4i4JrLr2jtDHg/Qqlykc6QEJApJE15HCAn7JMJTQA31gFDudwbOfrWgdQybDInwpHed4loE6/RO
0uAHF8jn3VrQ2gEnCzG2Zajr/h3wVzsABhkMyfU5QKF7nQ+tVCiYTuO/kHIv7UXuQZRRxTH4oda6
sfHYN2Vk8y1EQIz5SkKeKWLiLQWJNpZ8ZcYynNhSxAVsb069m0GTh5izfVSuc56QsjuH269Wt0VS
eFYYOrvgLZ+/ZQBVZBitbMdCflz0746O4z1gcjc7IUA29WRSS0fJ1cNtdpC2nbgBQBooe1osJEb0
wKh+Z76nmB80WGNB/1jF5Kx8jFvyMOHp3IGMPMxlC5rQrLkq30dDt+nWLG4432kBlT87Onq1UZKa
i2Ug58K/+ynY/zUVqSBB+9RsETXozBp4EwmfCdkLJe85wJQ3fgM+GcXp4Hydr+GvWLBx8SW7Cp6F
Vbg1VtMxb0+aErv4bY4JnaDxaM2+Ossh/Y90volqaMLYu2wKLYKYXStSfpN7s7ukZXjdxw3tp4b8
qajXk4TLBT3Re/TjAXmtCuTsB+pCeLk5KH9q44cIYCBOJXPH1tBJIWlyAyVR44EXUxwk+LzocR7K
370TEQMNZj1uWachRkVZHZjDOCtl2KV0SingmbyYwWZV+exs/lZCilhJ0tx+KrSHKftmkDfgVpbP
+abdwU23phTrE5/B0rjlocaj1LwWPJmZl94nrLvOkTX/EnhA7Lx6PHn2kuw98bNn2GT4ykIdtwdw
hBe2DZn5D3fnnjGm0pSdV26EzsX7G3BLipyDrvqynUeJx2Kv4vnkvu/HrGxk7E34WgTUrPsbpR8T
zG+l8UgSyG3BLZ77R0fFPzfhR0I1p9uGFQzigIfYgEWzSTit7ZmtMHZKsX0Cxr8wAvu5cuLhjssC
MCZA/jPI29d2kPyLxrag+ZCWRLMFm7oyqGThiC5uAaIFq/HMtR1Jz4bG4fM9Aag5PJ5QjP4yQPAe
f5UFoQQKaSHlSz+HtgHmMnh4lKSVMgVohnxoMjG7zNJQS434w0RFVM+1B1t4bQYkdf1aRIi6LffY
AqBfg24Zja7u9PDDIQOoFkr76VYI2LD9Rc0Gk+9MTL+ZS5rbvXcdBeKZrGGVZOrboB9+4nNbfWT7
r8e0GLTvnuKrlmGxMp8mQl1KANvEr8z46a/S6xR6vFvrGIMrDSBHhenI2bHn33X9P9h1uDsXE7+R
VXh2lvsYBYpW56aXOOgNDgMlOFtitz3lSm9DZxnARiyNqHWVZ9AQaxOt7SFaz+NSfN2zCVpNu/Bf
y3mpWpSpDTzC24WGN0b8KoUrbPlAr8v8g9w4y9FNK2zH1HBvMGbxh9vBfYQbtf4+jVcSo5Dx3LrL
qm7C/StOsn9XTdswQhE/L/+bcUgQUHYFf2sNQskgjmG4uXStMc2EpbsODZX/YoSmKIjDBVIXl9bb
Ii6J9Oe2OOINYkMm/WA8G7wsz1yDfeEZKD+cRlOdCGvgoGj4FeHVA9Nbi2nSW9CnlqY8m4GaOmPo
tNZQAyDKH6kMohKuBfKJeTAZJm8nZIG2GT1OjlBmZXYGRy6+Xzt3CCcf6lcbFkXvRxtQ5DwQQVWl
xCS09PVwApcVWOWCkJ2gNaRwL4IEgBwaOu1895r6s2Fo+8bqhEMYwn8U/ZGumX3/sp0JWiu59L3E
iiTtjBBF7WR3kiJX+kaGlvbYizOlgwOZNx4cRkYgNGEANYb//2lgRJLD+C/UGss3WFGb0j70gTJC
160OWBabmMj3Gs6lBI8dQ0di3kZQPscE7Z2WjHuBAhu2mAxq7cNZD2lpeFY0HNVZZQUCcLBHcRS1
/V7qHnPHWLSYTj7vErhtAjZ0T35nIcisEjP7tzr1nJWyaHiarSO8dz8EzRS1jLViUzgO2BdOWZ2X
ebZNYPa7MHN/il7IlxFFO4FuOBbGHnFFyo/oKU+n97Y5z8OLpXvCK3KFJAJbPtL7XtKhH8jH4HJC
kQ+04mMQkreSRzc/O1GZqXQdGgdYW96OCDQ5zUroqD9itIYHJ+OCHOh+WIwebl/kJAKgB/j0oF5J
ij2aK0XdB60LCaITM0So5sF4Xo6EB2wvcKp84MFagkr8DiRaBkC2b6Cl97uZo1cggrYpttyBRnIJ
+/48GfilnEDP/xu2BMW+2eh7eISKJvpp8dqqrHNBzCF1IighdriyN2+EjjseaOoDEl3Mnuyt0F8f
mSDSsjg6LbXdIJ3nE8xcrpB+T1VfJVnng2KyPsG1kbB8YijtrDFOFyEXUkAbilVKyDjd6st0UgOn
6r7Zj7bMveNXJ/gl3zN9sFVoeBdSO5DsqnQ8/m3JJrlfbCgiifdVKUbOtcS8Yv+TduZKtOKEKwP0
TVJt/ltLycfWuwUYpXtPuwLlTyOMrehMhhiEuYnv5EsHgDwLhwlvhZlBE6OnYKQW7JEE4yktcWFa
vyx/H499x7E1MofGpYiC9Zyh1wehW4ABVJqfTbM3LiI3mZlZInF25NfEr2I5OIx85Ma18Kxp55KT
VXL78w8asiP5yGezKcOZ8AazxX1ghQKYGJrDT3TK3rbp8CAEX11ldK/3gZ707y5wJt1/q8rg8tIu
YGczhjakIupsNFItkZ98oUvkVbmdMGOzpczaftxyOcmUS6WmrcdPsLbTl1slUEuYZPs+qGRVMfbv
29FCpNEK8+LrNtD/kHCES+lNKVryDSfGFzETyp7DLSmxwKNMWRY1T8LuM46YgGfQiGYkqXFTYLUc
7xopCAmDi75Drc2i+8HguYtlbpomoUoCLwX3m++z0X7h+YWr/27ufWkrrrFtO8phBvZZYqd4YNYJ
Ib9mKV03OB00JHkxbd/OEt1hp7aXfvRXmMPDoQCPELR2WGzDzEn7ZmATHLR+7u/fA6gzsGOaYMvp
V/xMpPeQPbqZ8SZQJJ3Ik3oSns1Ad0UYxHB0U4TfTUWoDmwGdp3q+nmi9cA+xp7/oFIZm3r5o1J9
k/S6ENmNnTPxzI2tWlq3Kcp6g250nSezANLtBGkwECRDdGMlwumgJQX9btIPlSmUO0y2Jsw4BPae
XyC0FxgNzncOFDaX0Ex3/DYUkzS80b0+JpVjFEVhbvppt4eqjILPh7zTVeQu1wYf0wxXL9VtNtfS
rat+Onkd2ATG1xCtI955CbXESYcSDdJiKxH+teIRkyl4RsDPJk6ag+NajFHqCsq50GkcQQMkrwYa
FDOL1H91HyVNP1MQufc5fMOA6cX32wmoCuWlCUGclrfw40wTiL2SgvtFjz29Vral8nHeQ86/Jg/9
H9UtxUPEi8lFuu8GoZjLJWYK6VjAZnXt4bdOIjHuq6VMnD5ttLjtBjEqBt7bzJfsoqTAILjM6+i4
ro0XjJrMP8yedNk2hG6yik8rT3fmyOj56yJ9Q+/nPh812FmzsuH9dotrmbuZE9fhQe1GZW45B6hB
BIddeFBlDBsCoQ+jzuSUcnrHKRUYtmL2egMdiuwgH/dyyNmM+ej7JF5wn6Ultx8axwZ6MOc4klL2
5QitZi7NyJRIo5lwKa52VrEtLEE4K48hh7BPbKwfCr7P+8cG/DaeG5KzMW+QYo/1OpZy2DbF+sVf
HlXvkl80nx5RstUb5zGmboV+1VwQT2Tmd27lB1p+w4yCwsCv01Sc5qigP4MxGOG2EEKzFTg59NdL
DgDt7zM08xOujBXkFobUDqdR+CYri73hLJxE5q/GzywtuGjusrsNVUitLmAf7AiVVNQg32a8VCcP
22FXYAxVABQLbUqonF3DqvwQ3WeZDsIj9XsO4KBt0apHgTtycD9+f7mJR4VKYszSH6v4FLp7pLvo
+eetGXI4Kf41ovxyfc26N8YsfyYRamXcLSonKdC60PonaqKnLOo0PG4uPKKLjvFf7mnoi6jt4COs
EDsmQnXz43aOfw+oELwJHOkX0X+CGZ+gId0X9oFl2EIXJ8VJJ8DvgsWG9x2KI3k0LHv+weU6yNJb
QkaixSKK/ZeUJIOIrwFzXArEuW5G+zmWwuaBXhDaPnKD3WOqWm4QEr8DJbCZGy1HFFKnOCZ73Ue/
rs3Be0mCZY1DhftHOgmZgw6Gv9Zi8EGE8okh9//h05EEyCJIFWEArnX69/Y2clMlQWi3FxtZm8S/
gOQVFZALZ1VMFg0xNGvuy4cxORHMOQgq6/m1yPKgZ5gWticuu1VCEol/36muQ9PU1O0ogcPr+8QY
ybHDTYc5S8vNp/Igp5vvZiJtImcMD4GW75ZB7zpV+6Av73AvmhAjCnLJ6PIW68gX8nn85F9qJp06
SkArDn9nsbsCQjJ9j+pJkKdJguneJ+l2RtIzaVsQzgA/anFg1W18TA8MkdTtUXEH+xOKKNbLMvq8
VbBa/se6+bULW9LK3GlvWrusjE4VKiYXWdG+yTWAB72hjBO3dkqbHvGitQm3cuiBr8b4NDQsKwrZ
6FtgKX2e51vnbHQlhLdAQ/arHA4aYaltb1Ai1rFesXFlXYGtkbwozj+fLaLcbQQ9VMj5zGDVa8XV
wbr8M9n5XTYUuGwnohQhcalrjyY5hWxdNNH/T3vtlz7mI+P5I3gztn6/9oratHuTVa00T8BulEiA
ayFCtCmyXe97JIB4M7ivRPsuocpk55m2QdMN7rjzYMy0YQ/Lp2nhE+cZjJFUbjjsdcOdOBOr2W/9
OntoF0lC/yuh9mka7OF+dMkTRhvDR6SRjodbkNkR7Ho0KC053TWKtD4xjfFjkJSExGmXOEBt4dAx
2forETJoz4ouCE/XuZEvHiTASIaPeN8m/x6WbAYPWSZ3kCojgRghc9+XeF/h94k6V9Bw7uf4LdZy
XWGW7xfsEhFCfQcX5gQSICIb9oS69QcvUsvH5wwI0T8gvfZh12KRQgKcEtvFeLx/ES3taYWALQ4u
54KIKCVQ8eXM/1tfCmNyjdMCNGVUnbhBM+0RgB7gE4UPj6egjPxQeXgNyP1KJimJQCrU2B2I3rDx
SW35boyxUpq+5GqEAaGK2hky5z88dujc4cFp9827KkMRRTUKivee9FQL276tSIQ5p6VwN66lvaBp
wrnOq3ozTg+08Cw6TJ53RKOO960ExWt77ip7bqiHxHMz0KvKRQOAjEgSOaODiviXlSKvJDHrOxz2
/ubOVxmmhdtqOTqaMNOM7Au9eTkMVlZVNXNXoaRpi6uKDHARbpqxUgPwVdGe/4/5xpMRLOD289Bu
yvATpbdj+9BMt1Aab2h9Z87OtsTkn/ZSOV6lHJLxsFNRW88Pt3ma7GaCf1pOnIPJu+CrWCigrvMi
HYncCZRTpIERrrhaFKI9sfmXrNGk1N1gkPJnNpnG1ok1NfCnCBAVniJ5F7YDTonoI4CTzcqT+H9h
LtXOKbWW/h3V1FmXfXqUOyR4YyV7ZnCigsr6kdd7KcYHWmCAb4M7NUxIeOjPWEpP8Tsc8bSARr92
wy9OA4P/m2IQtcN8lSXxF19I4hWVBdMKkMrJ9b0mAjn0SfjHg0cuhFHYg1PrxyPgUg6vB4dP1LXN
gsK2sdRc9RA57hYWaVBo9+0rt5/HvYfhpx4GBQ7fucvnAM1JcWPJTrx/Z8uhWpd5JqoI4X7f4Bxy
PfUd0DIlLnAPpimdf8dJAcn8KLD3jeRKcdFezDwB2U/TJ6+jtu49DKZOBWG+p3q5Mzybj4QWAUKO
wMIKboqWJwjzrCVwTd51nRdeERiHrTCn7u3cStwcYQj9Op2/8+bkehxc+SHar/3VVWscEucmrflj
FOYFs5/J+XU7qYox/5TYo4vD/vxyTpJBl6OznBWPqVUS5MXyQZwt4PEl7AMHC+1jzvUQuKcfPDfe
RSyjSJHF13HEBljg/YZH+4dk/rdCVUTnX9Y4aHkas4N9TbtTA3SO/iv8g3uF5vAWqu+HYNyf6Sfq
mVHAad/StaSECAKSyBDHdeJffzR5RJ9gjYNJCYwgvF+/rsR2xINuBX3EsyxbMuKwd8KIjifyi6Ft
GvWgrtPTP7SU75CU//X8aLFBzOLMRr8uNiAJ4CoQ7hcjAyNp4VWspiWdnsBVY+s50QJLI94z/dmN
+6jtFOV4Wc0IDYbewgPeaM8aiWdTnrsauZ0j3cSWHckvtV4YaXGncDaNLkBDvgUp5YYInmUGaxu7
TeYLaFqw81kVdVz5SdqilwNbABZqmaGc+kHYlyKPj0905eW5QztOTG7LOp2QLv6BvaL3V9qziQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_17_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_17_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_17_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_17_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_17_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_17_1;

architecture STRUCTURE of design_4_c_shift_ram_17_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_17_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
