// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_19_1 -prefix
//               design_4_c_shift_ram_19_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_19_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_19_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_19_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_19_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
ScAgHF4lFYDafJorYvglLnLuOdHACKdZ8eUvyDWt8PtowSeTAKHeq3CX9RdWoNeRFHAwJ/LACMrS
2BPfLB27mVbXa395nEzK+E2xCVsQWZvlGLLVXxpS69zTe+4MjZhECtabTBlt/Fp39rxHsjMudemM
GcOxw7ZzzrzNz1A1zS9AelZZbsVzHH7EPsc3hkw8xG3gjdzfEF0RoNhYE+jaCqc+tWUsYjc6Fgng
t/TRqh2GgACkN6czszaeI+qepeipRllHN5Xsp9O2FKL4l5KLWAfGe0g/aJ9o5qLCNYxcMmFQKBpI
nUiF8x+Hq9U2JdLxymNOR/hCUlAU5ibGo7BW3lKmCHfcGCelER5ZX/pMZ1/flDvVaRiR4QoSlbg/
r9WrV1Kfu+RgI13k0jfqRbrHZ0lGAtqeCB+U92tr/sJNfWkftfPBcPt0/ISDgubIQZMjZr4XQHq8
+4qrCMlgeJvaio6SFPfhnbw+vybMJYMJ5E7TBETO9k7umdyTuReGnC2HzqC1BXWw8V0pgZ4bByI2
Jvujv2Yb15JU9HQo8GqfsWIvdwYUpJdRYpcG3RNTfn13dhbsLFcvauXAyOxAbEymEujQrYBhka1T
HKlzqPsynPrjr3cOYz3y3n5Kwa/kCDOeBcxq3Mou09/oFqXpjBCjhXszeqtu5XOQyih8656Lqvh+
6STu9Ym4nHCniZuDEM3W6hxXZ6zGJLWXwdBPLycS7PjqIuhFxQ4qbaleTF28/4SkJDdOftcs9jom
v4RdH/OoxERZGgIWOhA3Fx9VeBVFraW4hlMnhN9O0/FymeNMd2PKhCkcpBjx+UMTFZgHfioCLim7
nyHyKjDdGAbLy6sAPGgjv5jG+OPZd1CK5e9hdusukk+91queapHONGM30oiT7jja4HKiF/nmmaUz
vfmLttIrboAqQ7QOFROi2zYWvRw8k/ZyiNIAZQ70O8A0rEuJkVyS48QZF23gbA+Me46AgsXOwaXV
CZGh3/XUKnKKbpPxYt9uT8+ztzoNDbpJM0790DJ/YrkbTX/8LOGcA9v/2pWEMsh/ejPdxyYi5nQy
eU15kHY4+Z7RJxlrURwdPHLiX/Pc2L97F3tx50NSeErcdcwlGQ60y/12LHvPAOJ0a8z2WcaJ+CHt
ag7fy4wjvlFH3QtK+SdpaRT8l0IVCe+uQZ1HOb55Fl89N0DkKpXXJJzr1BOyuePy/6osLVmC1Euh
50dstHVM8iORDHpytpw2vKpshd/1AF3PFeL13ge5+vzKGKEQMS6XocBw+/czAivJnMgyyB0N+Njf
gXOFs8sW/86OyEtVYpjZ+9Su9wym6rnu4oPWy81huLfw6wNw33QEU/VMGYsgCdNUPM8WSu9hd+Jt
tT9OmAkgrAkvK0tDiVKyREPA1XseXEI8aV16PJxBRSjHSnjoKGPyLSndcUFRaOTqR4MUPziK6FNb
nvgQvoXdbWqycdB8+ie9TEe1u1eUe3xj6hNKiIdeFuepjdBeB/Ms3cFZNSnIYJPAp1B9MrjeK6SK
DNXUMGome/460sHRg5JEYmfZkX/gKyOA4ClfmBeO81hn1AQLv1Bdsl3czlbd0twt9Tyk2Dlo7284
MBWgMs6OK2AWcuTUsY0HVgwWGCTKdfzQ+MGvZbpziYj705pmo580mQRdcmRWbcchCxZJWieZ6Xfk
ViMuhAm0Kl0UXQe6MuTyiV4Jx2AtAak4MzwxamyqnPblF9seFUoAiG8SAdt+xzrWwkzNuSn0UX/X
OvGTBVA6+W6AHdEUMBouwmy08GFVyeeykH97kwieP4Lwu/rgScfzqTKTuCKRp573EBJvBV/4sMql
q2PTXMALwKAdx2tL8/4C80CwPZySloCslhnHDWu1BSHlLHBG86fX3zzxh8tdPmGhPUfH6QRNRKkD
TFXujyQIPs+BQeS6F5ghO3e6A2lE/0OmIHEwlRlmrsxZbZQbzwiD/+Hfx/kU+uikHWT8VuuxWn5S
wcO58025sqQXTywgYBtJIRG09DFWiAEFc/a8mkUFzK0p92grmuoyIovRiHTa4slksy2EdNBhJL75
nEtIvudGHcxZLYlbAbxaK5UvyXtmrgtxDV2acE+KH6mgBtekbepKlKuITHGY6GZQvs1wpz8/feWU
+P8j/BjLGR1q4CBO6UAaXdOoWyHmcBwt8HwvB4tgUgpsrKtLQHJWJzLscWhFEL9CWH66bBMV41hN
VfYdesWjcv4joqR5HouwE0qzI9m9salyWxleKKE38NabAkprN9qdjJmfuUr2d25KmVcUDMBgmOuQ
uWE/OelYFwKRJgh9tf09xMYV2XKSCei+fQy3QRZ+NBq1gjDEWDtdxYIInwhQflCI3yUhGdINH89H
pJrmqUFY/BlU+UJYlG+puosFev2j6tv89irJqHU3Z+jrN6JqBoSFg7m3sZ/sQZbTptZvmbH9k+qN
dxW7UUIlPgi6f5TP9rOqRPB9ijZMRwMZf+SacKehz0KmlZ5kzEhXWewJeOwqGLSpuefXVgv7/FLr
MwsQX8Xw00xSuFubRp3sS8uNSUWy7Kw8RINRXAf7yXUqVb68JHAtNEsrLz/Z9ZZtmiqhUPAUwgOg
2J/gRzpIoSFSyZu91JsWSgLOrtDZ3nty8TSJlEV8ktjZPLXOlCu46ErOeVBOgxu51Ul8q+r8LG9f
Qaiqnt4D+RTWoX7YjlKjDDwVmKrYNikO3Gr5Q3Svavkn1jqdDR17jd8djWiF4eCss2B8Lzbw5Ger
AQW6DBCm481gF0QC4VJu1QiTPMcePUkjl6UoZHk5InMgdLH+ciy3WaRaGPb11STbIyh5InkAAwia
1+ISqVHIB8v+EznGX43/pMFsl+IyJuZzDjpcR9Kz5byUJovaQEnyjVHOX7BnsD9lGa6sKfQClSav
zyv5cXdUFCCmJ0tjAFqebBYBlj50WZPsiRwAhaZvVAsQKOHDIT01lPnRGEciZnV/v0am4k5FuUNo
IzH882xMtszVrTQ0TqYFofhYILcorVGKCQYKbioVpk2stGz3INaxMTrJAmPptQGCKJnRGkZ7l5kM
UB/G35E78UYMJt4xtFkgJE46qWp+FbqpiCXkHHtwgL78o3ctLUJFFSjOAY+1UqJfsTPz6jf00k2l
vGdv80nbRcRlzff1FgllT5TnzCR1SZ2ozvGb4b5aj9sLC3pEShXBPOC8+h6vZuSsnXZknAcvloEg
sZAOG7FkwM5WMgEfesZeFFx5TjwGG0VUXaRo87GjRnStSTYYEwGDu0uB0sF3T8Y7X/t0TAptkDix
WLZtRDoLUMtiw01pv0/tYWeDnsMf8bSDewGBHaKKCi5fDMJFdAxf+Tdk4sw7TuHuSv4io9V+1BiC
rnnoiA4JrXmJ1gDuAy2Tth2vOH+Db8t3iiXLX9/ujKvuiQbHKSPBmgM/bRwsk/34bAYvUXXRZldC
60+sJCWF5GWNxzz2Ytf1gilZDDQK6kEIhqcittrNq6KQZN2Fir09/eTg1rgarhLwD3UsoTXcD9n6
FiLf7mtqXrjNuOyX1OTfrmBkvUu5Pu7SOJOqPopmmpMg7qgDgJamq5gmAyl4g807bVPTBuJxwQBR
a5ACRzN231Ud1RQ2pX8mVHPZYonxadNCkMuE1NvnYkl2RrZCTcy18AsqkL5gRXwpqlVEzoo4ZrEW
slYgMCNwTwGWyFF+dPoVx8grXHo337POM9+hJAypxkKpO6ipPhzCYJxVc0cDKzGV4RXPr8td7HxT
+N7L0ZPqbAGwa2FHWAx6UoOsBsu/Vt2wgKe4ehAfhGcQtz16lE0bO3EX3qjmLe4e5F7slZhGQ4s3
QsOrusftUWMh+wHZYA3HOF30jLoDotB2ab2FqvYeDzkmU/uNyDe4opLcOkBjYANUpnGbojoP5JRK
Un8f82ZklRB+NET//asFTmHU6sAl7DL7s0OIWv/JB768M+4tnHZSlDADUT9kKN6tHw+lOrOhvUwU
lepiGGRDu2qJihX9UyVEj3stpEGnp9PaCOFjaTFHdRuepBzfWynylmMOo0XE0V4GRnUo1dlPMfFQ
Q7tPQPBkQipgFtD+nfjpjag9tbBT64mOoVZstHu7uGzVVIUiX1dl9vLy3YQf9dBmZTN4ilZOg03e
c0lDjsDOufmb/pO0mO2ZyO8S74MlUcvtj4AWyqaqILYcVplFYl8M4sgvvHPlylRF1LJUNHojleBG
o6oswSCg4zpya1lfn/8o3UUqXoMVyHlm7lBIQ2EnXYW+6P2Bhn4iosMOex7JtGxHdVvZUuDZcpCW
8TCIAjIH3mMzh55jpO3jw3ik+1SesxGSfCwhvmttf/7KXwgDpKgcCro4XEWQPRkncLimcizusvlO
tvDsnkGCXOt0ovA1vqorugQAeDRUu7GM6MbQmJSFiIXMj9cAY5Az643xesBEwNzPOquiGEMtO+xc
5uNwQdko75kxMT0tgY/hz//zqstlMvwwvbluHYCMKOh34XmnHaSvM7pSVCOAmT5VFg4zC8fOH4Zg
LULEJ3i3xCFRKljJvp7ITuasNP1B4mVBvJESlw17tfbfR5K22J/ysmLbNwANHNEQKH3Eqlg22CGR
5op5KmofzY3JK/pEFZZA6VXbKRy0KrkfrfCRJ1S/qzejWYPiDXdpPEqg2gZztKXyN/v5H19MOBtF
/ovgp9yq6PsCbYBttaNbpdG9JN8iwDqvF49mc6zY6hfvuRik80uRwGgd3eUiPudYC58tdWyg+g6p
3DjAMPtt32dE6fKCADpqPHt8CZgv7LCw8BIN1LHXgN7Y8JJ5BOeN9Iu8yxJxzDFEWwAs04DXdrNf
by3KG5Pn1PakgHMBPfK1q1S1lMrmHdlwvodORN2SFX5iSn6OgGm/ymEDKze8zpbAORjflk49MwTH
EcOC8BJbmTamAAUq2apzVbc8q8ii8lwBRXUqeP8UpTT/eKVSm48PYsvOtxAcRmCffa+PdCjBWj7J
WyDtKN0y0Tz0f1fG6tN5SxVC+WYGpLlyNVqGMdYnBiPTBNHoi2AwMkXhe6lCEdcDIql/NItw5tme
WKwkcaJUBLTKU4GEy/VRcgTNnZHtUDIR2XbpVr2zHbbBzNUEv72jG0cSnx1d8BqtlpC47VeMKzij
75dUGZu2TRprXGobGrALDo2H8V+lxsj3J5/s5KZQQUuD4E33Zbkl5ahHcdaWQUVoeTtWmiFi1by6
rFAOUSc8XsnkIU1a0M944j3r7I9xmQdA4JJq01DWfMw8pq9kwPk+4SpS3ttXBdNfS8csorwS5vOT
KXmGSfM445U28Q14Jj2a6CJK2+V08zpUfm0pgPrKIb+aMyojApGeqDEAZnp9mbK/Zwl5AqKuFNuO
jDEonE/UvXCLLiRxCySSb2RaN7At8VY2Qytm55UMp9Tlof+9MyprfiSufsoA+RaUaGki/568x71E
VWYpKqaD3WtIgeonI2ucVdLcusUEa8859ckvwbf9t+Ko4V90uLZAEzmr7dGilk07+e6QOPgq565r
qtSjSG6W00Lcozoizm9jNmsSqm+AYampSfKcBoCy3y2CYpHF6ow0cYIjbNQM54P0DYYm1oEnid7d
ByjQEsSRZcp23FfLhJVo+g+QEHuYHluVUv048UUfYp+k3cG6DEiVk2OM5tMdgc6Z6y2XuF6eAmxC
IEc6LkCLfRon1mXfaPXBI3xqTFPN+mfP08V9buE7cpGJjnmvVb4LkEFTfVqSMF71d/hXv694KPa4
Xufwwwpv5xwzqYie3oqkzq7sQ3HLLXCtqzWw2epWJLodf/EU24a6K+L760PZbj5gbEKAGiIIJmYs
uvPcxWjVoyMdzw2ceuAly3M+URQahI4vC/tVRZw4Rm0K+jXGEcD5O05/sjlGGWBYYCdlQAyjfaBe
BI1kMNXrxkzfl3mU9EM90BUCbj9z6HuUSqB+VJr/UEZfxq8ODjD43D1vkcC5GGxpn7neBCx6HnrE
/+HM1bisjVRowvhJHf9wYdA5lqiOW/9lXEozTbTlDW1z08W5eH2OeW6QeqWnPh+8HJykWyhoXpsb
7l+0Ck5cB+bcsXNZveB7LnSO8ykMnppNAw/R4Ct64W3Z/WUQkH3vtF2FCGB4rfr4+bRgquKiX5i2
H9i4C8bSHB4gnv1Gy8uGsJ47z6BbHFjNPPq11cs9fTfwwY0k4txgsuSbVDZk9Pr4IqRRZ5H3U+La
lX4kG58w6t498xxWe2+PZohCAs9hj8ImGKNX8eQbJIBdE7u6ZSLO9sIAzZq8R9q9X8QtZjbBCbTc
w5d+3UATklUv3Er5nKrSwUaJiANz4R+Vox9EsbJNJ7GZ3kfYGYRggux9tvD3ep4ZCG6wJQVZvWe3
LW2ndrZZjL1YGLJIm9zUvfwWeO+ZMfeu6A8yCuWFI+WYnrAcrNjnn/AEq7ujn4uWyC5a+yz6yt7K
n0nbSOlAyW++fpLIkkNDjENdg4hhLSLrFzqBgdBRnemT60EH7tCZG1GSMBogY/vWiz1RfberxXZY
WkYC2WnH4dsAfFIORhgAW0TcQYZ467iA16utemQPoRCEL8u+jzKi/lKpZjMT1ivNtu6DhxFa81Lu
zvtJx0d5QxnWBwtp/1oF4spB4W+ieDbkDhUtIII8QPibNg1uw/i4xj0QwpZzVoDvEYrICP14kXaM
I/yRrpP0s9c9YmcSVSEiBgXuYQIsqoBiP6oB5XL/4tXBz01vOj02nHnhkayemW2PMAqwsQN5WlHv
YJBW3kz2EYpXNzush21CC50rZsUEbzseB2jUiNFGCAuYDyMcD8+HmpvVhl015lG+Cj8e/81Idl2v
1bggzL2NvACo05fcRD7T6WkNAmqsnAHVNtAVQUhb4oXH7KIWbVqNRKmVnzQqsyCD+uIblJjb1E+/
AZ31gxpGdQF2IGtGvx6vsqUEOORu0IT4tlWtzom8AFKBjsSkKZkII2r2Zi9+DxAdneCDHAMOeM8p
sQyIysjQ5OgD5GklidN4EPB7hEKoiTwz9myWpyHJzScoPGNW3EsJVydHtiubZyALbWF3i8GKqzDl
WqcWoOM9YQ8nZ9z7rfsiRC2ay2zfzc1G7LrlMq1NSqrk+B0skSej34Hk2woQTXP5zYIhBYuESrpo
W4ZKRWdQwaMr8NB70Xpuxg2T47CeFwMd1BQDIcEQNZMFCEvptotcg2sY92BpurWcPDaTLr0lBA/q
/cSnl8SzLOnr4Xlv1p9cTZjU/IIj1lx5yTQQrCcaxU2x/sRpZzi4kyXm4MKey/vV6GPLQETMi2P7
ksnniGz31IXFD/v0XFo6gRoTaaGpWVKsPEU8HisXMte1D1Oa8on0u8nidnQjrmB2pduC+Q3KT0iJ
mJ6VaXvYkugTPn8mDjPN/Fm/H+3MYxcgVY5veB0KXAKSg0Yr9cYkDoK0ZZHQQi9CacKi0u8wcVqW
AD2MB7L2iTM2aA18lYkLjdmTfTtnRvPg7s4hDdQgc2yRXTsaYl+rl4hhyp9j5UqDNbcCNipSJVI1
JdEEfAEf53dKBSwOqNPMqIJX+S79zOp5xdlntiyUDTvhycJFk9CXkzaeX4FG03m1zmbpE8pKq5D5
ETWLMc2lhrAoxAuxrgK0fqSMxZOWmNuQDvz61/ESS/FkZdXcPuL0Ud1CeQKrzrsgLwtuy8Dg4o77
egGXn3MKSmh93fODJ9sV2Ha3Bq9iKCXGzsVQRlmHsXiiX4NPKjRo/oOuWqu08ELItrmlPw4laE+T
cICymaRZACiVGenHvWLrT/2MHddFxUT6ogGIap02pQ5uZj16C8N0kGShjYU8PRgh3GXpLt9F4yqb
D7QsTtdaAClTibwCMqSspqovoj1cNnUT5dPPeK7V10V8IrIniwhHmvR7Usv0/QETDCSe/eW/YhO9
mRF5KouJO3YygtYPvMhn4NcNk+dUFMC/0VNXL+Rw3E2B9xTyVX+S7r4qaVbqcmeX2llb24/BQsPJ
lPUJZKaoUjTtndk4AYxOCbwoxy9u+FrUrm2L7imhtdrB1HmII/am8f2I8G1vbqn+Z0ffTVe3Gz15
qyz/yu4Y4H5LDkAssi01lKr066DP7We+SXmbiMt/vkxoq3isB/bfuKTablLG9WL6JfpLRjnkynnB
8Umt5oSlMnf3VTq1lm5wBkKYYBvdwujZ4RR+Eqi/zCFJijUVq4fLagnu0phuVtzjMGZjUsal1kta
/2qr9FTpRnDzSoAP2SuVBShdw+9QF6WDLW0Tr6Oa7pJikLuvQIfOz6FE3XPS+xobl3Am7GsGofk5
G9ICwgNjD4rLEcihWfLGI/G7BXotqp4y+vh6HEySTTRUFEbsx+D8XncmRREwcMnXx/DnmxZUxuKM
oGejra8u4rXdNNzqZo0zu85Ebib7AFR+vVkr3sfGBHXQbY3/4B9JimfZlbRwTvbJg9bbPT9QjxD4
PW6a7umfY54xMEQm0d3wPZ0cS6Y/kQGzzLnK71ZPzLZTgzq1ubhiDpzQGDdJD+bNRKChqZgiipEU
gKpyphzjfNzT4bRPMWZHP+ZMNATBeOdKxieOb/j3Vc0jv9Z3EBp/3SN3zuMIjNk5VHFRtTT8GgW5
Q84i7x/z4kTJgcm/rGr7cBG2i8eIhsV5FztkZ2hd4n4pUtwNF2AswghE4U2gtZCTgZYAr9nEADvx
chpP3QO52GeKJANHKRwsVgyNmb1rMANRv4EoPkyOt3m+dezg8mFrppUcqQmCUoDma30M9GeiyUmK
PubL8+HgXlADiuuAvZBRhVvOy1LLZG+0m3Qp6I5q0CPwLxBnEMWM3NYyNdQzFQWuBJoR5eEGc9gw
4SGCkucZTjMx2UUIumxLfqe3TC/vKmWYGEeU/CkbhxnB4dcICrqfHQ2zCCuCgWxNHUM7UixVFE0F
Jl5Ksc2ZYwlDJICUgGvbCfDZU+g7/GLysOJQ4Pz+xSjI5tYddlarzs5IgpIb8CWrImhcH0QIQvv1
h9+o4uNF0BOIEPTvBc5YPQASfnyB+TKoyIA3OYJnuwMuA67+Kx5NeXASiI0lOF1RRDygkwKFK2DT
QQ0iIYN7lYPh+jEdwcime+CEPACJIxwVIZKhWoEpi6BvmdSSNlVYh8Amr4NyDQgxniyx5TjbLUwb
lpnCHOMxNauuNSDGSCLC+QiXNFP8aFc6T3dKKe47Iy36zE6N0ILkfzdPscA7NjOlSmvlFAtZH6lZ
DVS9pw+KltM5xt/oTiM69PMn5U1w6MNk07ybN0Iz/EYcKgLrLLlREMDMNadSBbR6WjiDs+V4dESq
Ujz8XmScy3Ejz8Bbsp0Xu9pKziscz5qm118eLaZIh/fhq5F1aDGoTE3uDAqjXxeo6XWTv8p7dyCV
Gpc2pKfY4w3v8kKFopaRaxq23J9NDw0bZmYtSJpvYAhEribmsLNiqX2Ef96NNlGtbyN5MaBrAGp6
42ieEWEWHzNxDwaNqFqsLRHhCSjSZkq+dUC1c4vRTNywNGHeeGnkQdxHXA1foRc6x8H9bHm/bfLY
KCXTycMoeaGS7YeWiN6iKecuXrWJS4HklX6SUxoZ5ifhVKqFj+ScMloXiKRvW+R6SZ6RJTDa08/A
JGvJOjMarU+flEBeH0vNWM/gm/4Ql6KFQjdOanuUsSaLpSgRSF7DuT3EKkXtk7IDiUqz2lSaTho5
Po72v5nGN6DP/Qs1OqqCHxwBoBpes7eagq2UUSuaX5pzW6hP5pw7Z7+54hS1v6LbuCKbjQJLqvCG
fGhHwFfCSG4ddv4H9Ugs65uA2Stze7N9XpDmGZFLOcCOmH5GbtRuy6vG+xbUiJEtTzrl2Nhp8p+D
/P9ifA+64Aws6eXYgthk4A7sPfMefkDDmFY1koZGuLdJn4YFROq4qpQhHRzChBrgzEOT7RxW/yFf
ueldOuGYuqfktvbLLToY6Tz4T/x+83nTEGZM2uBJieLdbRjRR1aCLabxdgtSX1Eg/DNwNUloJl30
NyhbzB52crz8yo218AZnZW664R+WLCk9uMrTTHptheLehrdzz2DvIZvH+Bx5ZlcDHu03GIEJe1Ta
tK8CA6S2NyKmcDWN8nJVPb8Rl3w7vNZV2ADRJY8wFloo+Jexfb3sRRPKanVymCHb4vhUtfduWQLI
EYoJUwtOL7JoeVlOlsUYlmlJD2CktjanymLqomBK3W6WG/XcUvurEeXDJ2jlhauiJXOxxMW+yXD8
ifaPayKz3EoWC/aEym9btX6bnnz2N+M/vw13jz9cG64dS+hzA6NLPAFdIoeJ35zkUqoolNzcpQwU
635K5VK7F+4mqRP88lpkJdKdWYt76lS+pdRLZJ3R7y0Kg24F16KyWdGbT+skmME/5+lQS69VGoQl
iZ4mzG6Yfo3luxbRhDloU5s+BjITJ+9iPBbWDg+uSYvKkvlhwMK9VfGSuRjh99MudyDxX6x9Ahpd
U1yeZfQneXNfEGEdLSDpnbXSmFy1UYzyjEn8Y/WKUqT+0jGL/ZmfkxctuuhgRz1J+Zxz5sxdJn54
SJtbqNYPSVUwH9UXSQPS4R4QItQVw4/U7qbZAMyE4pQHe0KXfQQpgeuBblgKBUJ31lgBp/7fra/6
pUsivuWGKn/lTRHv4fM/Ss2kwswWit1Fs3YC5/hqjHX5YXRm/g2J+484yKPwf874bKZFBlcFSkmV
WbhsKaE2QT1paWHfwIdDsCrh4WDSvy0MHxcGSJn8gPSFo8/wvdfNG03Bh72pItQV3TiQMu1qxeRC
XOZIMOSeUJIV10GnZMW09P2n276MyWsDaRyHnIIZjhiHht/hIvD4mooHIyK5Dm5o548Wp8ty/geI
J538tGlJIG9QFXbmr5yV7umPSEwxPM0aavYbKeG1O4cgIKRjlRcU//UHehQto89icxzlnY054aOP
YhvbXjZGV2w4emhmWtWslu2DBr8AiVwTI2tMX9HohDWJGgnex0MGm12OdT3Fc8ayMhfwwMyxa9S0
A2DtKYtCtRpSK7c25wjnhMl67I0vj61jAojH0SQT0EHGujiGi+paqExfIl5gUR7M0FftufRAKocr
4u0Rh83TFDP2UMs+Y0QlvrYyX0IDXDj0wwsBuKmXxAIAtLhL7cfEZW6WXMBrbmXNm21kyPffhMLr
/ptlIY7coQwxH7JJmauT5wd5klX7GZ9MvkGCvU1XjoNAEGuR29OihiXut6LFUU/XL0DlDqBlixBZ
Z/8/TatddIf2NqtUqBi7Z7a6vYbMlU65YY/jzMW+9UNWu6R9vBEdVrARQt7v5o+eqfiK7Uo1sIZh
L3/HifU859o5uC7FRdFGgWpUQstI1RpRPsTys0jn3oCDVaoHHA0zh8w/A1FfdBAowN3zQRG47tRA
WXPCLevu6Gikn6Q5Bwunt799CU+/8cAHjnYXcirTpm82++rRkwe/82dg4296FwdVDfIYIw4N+W2l
/AqsEv/7xO6QiBznCdqHSjKFCwYkmvRpH5UvPoacDiJsxTVtFdwbZzrcXfLHO8TBSA3VwSeu61L0
QDgF6/s9Arn7tUNdifiJ1Ll7/gUa8Cv/J+anhs4mKWMTDlgFpn7542RpyBg+ZqJvqIlwEQIYBZqi
ESAgKGBlufGKAjwI0draQDNixVhCmUNB5mfiCaKU2ttUcKOt5hVU76SvLV9uyLmHCTNMf5V0aISV
u4p0+on+teesiM2Mwl5eP4BU4m5h6HxoQ0QJo7uf94KYi2hCygg36PVqjjweNHNo2ADDuQWgDkqG
W1d2YRe3dZ9wxSEK84uRJK3YnUnwVDaeR0AcMEnv7AKBH7TrycMqEd2DikY9MpyeyjTsbo9olwV1
5oSzLwDKuA3LiMvZ0/IF/c2hxmsa1lR2XGFNW0AYNYEQEKS77if+kNkfMZEZXRzwiMfanZJO48f8
ofdp38L8ja75yiOxNuASK3e4JrRzJWDKbsRf/4Un+hOs2metrS0Fvnm3XURQrREHG5nATfdluwYq
A3kKaQGNcf4JzRv7dpZF6I2XsmtWGKZBB0gZ9Aa/sfwtq+AXoZywtKqlAh3QOPDzW0H/bCWUoMpW
82lrt0F6ntpsWiI9ZfD35G1XevyAdvuV21JjnQKrlOjJqRYZ1ib1pAt4uX6Z/QsaOBW7Nn/H+sKE
dHfK8l1NozWiY5DZHKfVqRqnNWLDVxn7+/rZLXAM56porJtjrMHqO71gFYIQFqQ3/RJyBNZt7kjB
Gu1NnBb3Opx8ukrOrSme4tux9IlZri8uFOTnlDKCNS8WgKJYzlYjZZrhZ+AToAz0+6K8Sj7wXn8r
nTBqfg2dUx2vXLoQtxsrOj27JvfN6qAEun9h4BdGEEwMGlosq4hbFjOREizoHV3cE0o5iiYulaI2
C7xnC+ELGjKLSZGN4+DeuQeQMMBjWioqQoInyVxtYjwFsFiSAsSollYJnTn6iYdGlheRNW3HogQ4
+NZurZgFzqqM6pMzo0KT7Iw1iKUynPgdqJ9z9b9silJ8RPvEQR32FMIXS1WXG7uIewlR+MbPk1Nj
YA7i12POcAAykCR4uR6yuIcNKJMWS2s3CGIx972cW8+8N8AlWIC9BBI3Vvh7jDumKU5nkcSn/YTi
HOc+QHp93ROs4K17ikv7JAoDm0UsXTNXXWiNL+u248nLkAbeKSBiku0PBMgvsEInl2JYDVp55foL
Db8+iygrJXUC0xcTtu+7RhTMEIsYAuWQIFQ8EODuEqyOzi6CZh+tTST8ilG0rv3jG5Pm
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
