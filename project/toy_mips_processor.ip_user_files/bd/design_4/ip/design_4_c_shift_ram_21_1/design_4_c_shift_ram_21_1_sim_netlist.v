// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_21_1 -prefix
//               design_4_c_shift_ram_21_1_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_21_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4496)
`pragma protect data_block
ruGbSx+9cofYtlPNGO8ZDPVBMwW/HJQvIYx59nFvbOb/Myhlx6o9RCVt2TwUw+QQ9fnlIXHHOMAI
Fypj0hauiqIdVcoQi5fVxV+I/YHTx3Jsr8mEMhQhzBRqbilTYqOP7T1LI3ygjeQfioNfAQ0MRdWG
CFXWOr22BO0vLYfHASd7u/rIdvwKKUiCPYRG8GXx4ZytLAm0sK3ExQFCqEC/wcQ0sCf0/hjFmWae
94Mfb1k0j1VqMK7nNSUN0WvGupYYrLtxLxS5mpMb98I64wXrWeCc+NSaawtFttPW6VjXEGUVvpXo
gR5QgYavA3dlBeWhRDdU5eYDdNeYE7nQcsZDvyDUoUw1ZgINpwY+WILcbQpri5gI15cD3QEWYBUe
YCJATe1MUSKL8+nl9lVewc7NvNuKFjmFOUeP8UuUojpPC2H7pHEMS3VzPxoEPzUYUwPHWyqZ7Adt
NgKIaHlNPUbofozlvGitrW/NXtiNLPYgn5uFKQNUW1i2OILcjaqQCEeS1nANRp16PBRP8DaeDm3m
Gb2w6y3NZMCevXxWtQVxXySylEKnDNC7lAW9XDdBDv6NO/P26QsWQVmUgWnQm6GFp+PgiBv5+rmK
HkIytjr5kGkEXbtn/HGnOIUFSPsWWtyw06kkK/2Wu5neY5apznVNTBYcabeT2Vqz36yoCagQDnJQ
Rp01ktdbWsZbXB/ViNQlfl6PRqXUAZoIvIMCnvNhVpstA07HdnxBenwBgVL2pzE0BWuRoyW585ul
/j+sUkCKePpuvSrTn9TtUD73F4Zp6LXnmkTkVrEPwC2gj9s0nn7eKsGbnLfR6LXU0IT1wdal08qg
I/tgto0DnqVoJ2XTXwqSQ2zpXouxo3KP0OKrbn+o0T2fM3wzng0HwdHEgcSuQaCjDtZZJMIO/Fcu
QWR8UY9wwEUEXBOsfR0mBc0c1MT+oY9TQbsfdqDdFx7ZYFyUjAn7IANWBBFf3Tbqb0KOA8h5s4A9
cjXgLhrX//5gtGk97uWP+1YGkvsgtRF9lgPDv1OtnOjAn3Fdko/w9QN246Y7stUHUZGIqMY3E/JK
CeU25mkiB2bkEHrPwkCXIcmixZAE4W+SkN6xheG8SkzWAKSmyijtB+CeADw7JmtTmfQCqOzy6UE+
iy6U5Ru+urXNwHqwAEJU2MBQXrHaKpwyuABASFnC3wc/7H3k+ZRTMMMtw3tAwj7XnH26BwdnylP/
SO3+OXAUNJx/SIIviZCyN5ncH+PVq/0lnmo/TX95ayvdkqXMpphfxwjC8cgcWSEWxkuCfMnevnUB
RTcrOdSCDGUpKHF9Qa71LYNwPMVwVmIo2PgXHhU9eJ9pSOvPmQmlZPTFfQIMs0YD/hdlauItXjLU
x40kisbE00FzTP6apM1CqCEoaNW+fP8Kz1cCWSuWWRvPsogFt6NmhbPzUkJlhR7HWwMmEe7kTaeW
0iqMU2J8uQx7hoZU+U8Xxlo82SsvKM0ZXhjvBYnSxoBI1UFH91q+4RyTHZj1IxqpD/5GbQkMwLNH
NlENwoR09Prxk2vgaB9GCBpfswPCv+cu9dOefJfwOcVl0ebzSDMHGqRmUC6lRa6+zF0dKlQR4vdM
80qbKFaP64usvemNnWJhLvnRwSQG90yb1fMuTk5Uqnm820ZCtbWMEzxoau1dvp1EJQiOSWb61TvU
ixj1vzZ008I55KNNV6EUEynoylpl5rYCxYIof5YMCa4jsOaDp2aKIgevFIXN6AbJdtKa1VG4zsMQ
GrQM/ZgqJ0zuLaeY5/3wCLKJMMuPb7Oqg62iIDbB0KwYoNEDo0TNxir7Onq3DvwoszvcGtqbq+hH
abvl8P7tSUUig81+ArDYJR6sc+gDBbuDV+6zvbhmCqUr/5q2NMzBO/mRehJAY2qkjXiaQ9/pefvP
DXodmok5rZZgCKJhuX8JzBZOQHRFwl0dFARvirGDPSjPvRpXov3OCjdk/4GS1Abv9onk6XpT87gd
4i5lEcptb8c1vTAmEhGT2ZGpueZD4ydhhGKDhDNwqNZW6o3pLAlLzgvQ/ScGfn4UljWcmar8Fy/z
iWUZ8PXZoDV0yxxg9y49HYSIoThVD0QuNBNwx2d16E7DuhFm1iBJG64CyvG1N40PaUFhQJg12po/
Y2aiSCirS/InIg0AVIVC01HmC+g2wFKLQk8DzEsSZunYZr9ADSlXdQxUeeQmRB6rlwWeJvg1ePI8
/HX9iSIJOStlSH/raVl9gTp4n22M9CmQQ/W7U2h9YP00LJi+cx+qsocUSg0EMuRnhjSU5Ewr7una
UmQrfkhgRfyDB1pndVd/u3Nh5rNYV+6dmHbmLWewODDeG8vweqcqncf5UA/cMaXCpFtFBdMrjh94
6gQPU8FBILibFFmIxuRAJqUr/N04YEMjUlRIfphiT1GU5PC8BPRX943yLKia9ZWguqs2nzp2w/aR
ST00pLM/Rw9Orqq8TRfORmw2ozrLC+zWF3l33O5WoerPImjtgnf6eSnvA7JnpUrkCE9rUCdgR36T
rmrOb6VvlowrSuCezL/FubR52seL5qE0Pf7b3qZm1jCO2NjZJMK0SzISprm8EURt3zWymu85EqQB
XAs7GYbH7SqpgHYWON26UrLjLrjjGlRZ3D0h/6hk06jBoZHigemsINIkadycSLcJtL9x8VFiN5aW
fUN0JM3PZbgT7ljabK8ol4EBfKAsBFo3bgKCFa2q79lBIn//q5sXC8zIeHykAxXFp+jWjPp47daQ
//W48NsBx4KqRd2nLBpawdrrTKvQ8J6SFjgGkv1k8DEYowUsnHcx+7a4PupB/gt6eehNOWIe4Sup
1TWGVprGj5CoCxMISfaZ2VbhL/aZ9v5xbAIvgK5Ofk0jz5qHEpGiZVcQ9JhLhEr/XN/JUxCBgl05
oJEBQvZ+00CmyNGGZ97beAMx/UtDJ6sqRy5ThdtfbG6NCI0cBTnzuBYz1QczbeMo2VxM0hY/B6bB
t3CZ1O9VMNAYvoz0Bd8iLhmrvxFVu82I3T2ISuYTTi6hs5UXVOaGp/QzHXcPlrkH5c7FyXNi1H44
EwOQEb+FJuRduzglfvi1uas+WaE3kfEEO0a5RTZo/cMvMYjHz6XeSM87/QrNazlJ0diT10ozuAl2
GaAlaBfc+GohsjnmGy6bPzhCjIZnz5pZcyC3TiwFJMWkMdEmEi5BKsROA8UiquJbJN1Lw/ei1Pvq
2+L6bFqZ2mvfABT4uUONQkSu5wB2DjPXXNX7AwDUWA9biKP1p0zlbKW6J5hsg2ZXyrO53VUgT5tH
tJ0G45WULt15BYqGg7eVtDhOoABuQ2Eexvegi+7boJhpERyFEUO+IPNJmh+fTqRfbQMbluQYCB3X
sapVNjpT91IQ8GyabqFkpL25MGFao/7jUYLM6UrOYx+aWs2D7Z7opUfG8IpzINZzB6j/p+vOpCOH
rAAORlLpdE8Ql8ntv1x1OM+pGs8frmYxk2w0lGc7ke98eljPZNlLCcF2079PgG3l/MoV2I4RMTH7
khkx3WWY3LjVnTx5qt5DSNej5fSjSK276bg/5HaVQNH5eXoxf+f7iLDnkKlczCewmJC1HuGCEO+7
kyO/5+xWp5NoGAT7C2xFIE1AqRspMseLU1uBLJqLaGcqdhyq52+M++waJ33moSxcaGBF+gMfEinJ
IN4aQ52fm5xalKCGsNGm/hRdbaMFpu6PoIAdTTu5jlZ71b8kYcnrsQHnhVg+6kylDJ012LYIsac9
9z2ieEMglZBxBwr03EiBVSNhLod2vhvpjQvEeYWraTP0+UwnI30d2KDqVFpsaLBdYgQTPHDJ1sBo
ErRvPcAN4oqVOpMakFO/T86HPZhrB0f7tGbZdiAx0S0/lh2u4nkJgg2fvNQtJ3z8zlkkKOYemAIW
F/yhV9pt/h4uc58iI4TOUc30jnpVxn9TG13T4YIjX/q+sHXV9HHZUz41Z+558M1wGC/z4Xe8Dr/B
BIsiCMQ6Dyo5/yeLU24B2mS1sPRJfV0SwoeTAWxvXVHqLiIXRVrErcn8R0axbGCE704z21+tW81e
08qWG71beVSCXhMgrXHJ7qCTStJUnJEyZmWEMU/lGg6uGgSF0kdvdd636fdjeC0q1jnLZcpzEss8
TafuPt+X9OiP1iMMyF3J48wUhhAVgPuscVoT6X52f5/A6KQeZJSCj9924rP0BoGhc+CvgSucJxbI
Ge2SqkPxlXf/pbIQYPJjEY/Q5qYujTvquhiNmDAQUahoPq4otrDFJWHrhpnp9sjrUIdyi3yxaThk
9P9qj5ar1TYzD1aNEsDeW0tP0jaJYUO5X58DGxuBlqwkxezyiUcZwgJYAU+AVxMAthmF+fFIKzJ9
C5iwY43WAHBI0tpO2U5oBsHpDBBlYi8ZIl77EhcpqKeB8QGs7TlbmNKoKCLTz0f4GCAXEe6fKrwF
T7AP9LgNvX1t6WRbPWdvo8QX3ZZlkaJuVkNiCh4BkKZ05riCUuGc/h/ihGC4I6ns/pZzlOtDuIDV
D2C77TgkcOU+cwIj/hCcbHvDHkH+8n32QT0MKLkXRkbpOF0JeKfbdfr2nXOA4FvZLAtdyxjzZrmv
f0h+NurdWM80uePMPN2bONKsOl40dLD3jeZUe1ptuiZ8e0AhWeenI5eq3ZndsApB2viNDbfWiNkV
9QcrxdxEFSj2XIFNSstzzPIkUWs+90eocVwc87vlHEHGqxFQFEzSjaxqhSIL/7stD0U2SG+cH9sa
PQiUGMlkWrbg9PpqeNNRpT7ugKOJiame6gO/E94qSxWMtBzvixrP219gCvMsugwBcU+SupNshjVl
foW7+EJWBIjcWFEjzLGOSOhw6nqXKY1HoxF1vLk8rCtmkcMiEs+CPXWoVFv7H/v5l34v3Gt1hTYx
qT8Jpmw70qHhnZ9ULCCbrtkg20TL8pRLva2UGkzJmG2FVFzW7sMinR+SX7QDOoesnzOsdfc/gqg+
kqgNs+1EgrBqvYbZONeygqsd3m2vzY6WiXgJGWc8Qkj3Mnzgne7TjjjUllFYnh7JRbSay45eabzQ
u2/QXsHt1hAV9sP59waDcgWIqxLQlsomSpjfipek2JDDM0EE+FSJYw2FrJdpsOFjcqI2Q3PqeElI
/42gV4mDCqETMl+Sppq8mwELoyk0NizkOp25oVVPdBnVSJVuSMBT49rr1M0c/cw+/6ufzgeGxgGN
psP0MPlBeqFaNKLBdeA/5qfiegJOu0ds5PVc/UvRF8VeHIi3AV6Lpx95EuprWy3LurEBD8VOVvrK
mbkb9E4mGW9AMFnKKQMdkUkx9X6mW+FnWb1bJ6zXCLFY1NQAHBm/jN93KrgPrZ3whU5pMqdTld/Q
0K0o7Sp3DCbkCxLsy7Az9P1zGum1beELS9sgVpxZfrWF/hGcJNQPOjm4NQz/VyRnQQ5h6Ac2smZi
UdPwCnNcI4W19zLnFUOTP7INJOXzOillk9nMw6H4knltF4L3ry7E3zPd839I/a14/Z0x0WB2DHT7
MT8tcrrlYTpPtbm4DgRc4LBuvGffEh4iaIPt90ZpzRvdVCD0SbpVUyLZDm7VoNvcCkk2Cd2xZx1V
n81cnxy+w3q9NvDPzxQsDIdMggKhMlbTSFGjOkUtWd7xFT0XWDKkmSdfQXSdkyi+a2OFF0/dE1vC
P0W8knNWbuLD45heyW45IiMjf1+PTt6DM5tnmKKxzvJ67G/ZhLOTZKZkawdWYhzpueNIl1TWKzdZ
APREAIpSz+QcD1xNhBWLJwb2qAp4dObVpo7oWqbkGUvlQA2ovBtvR3KH9vzY5mNvtjL++OgcwT6L
LxXa+ZnFdqo6HUDIN20rqkuVV6eRk6CqUVJ30Qlv2wbuMddghvb8toh+oVTKnhh9bhqbCiwropWD
VtS0b1GvjfaV0EO3wZDMT7OzeVqp9EBpaEfnqIo3D0MjfxUo+WU1yZ/tW6Vfoj4vNzqSzdeWnXQ1
xXln/zCPgqvH4ni7U+NoG3Vd8eb/jEkS6NEGcc2Ap4ED+AYaENiudGzy61CyRS3yqd8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
