-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_22_1 -prefix
--               design_4_c_shift_ram_22_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
svzJ9veVjDKKD0YFdigmhNHhrdtBeP7tODa66Z/U/F7qnOvuUiH0SdxDgQk5reBeITh6deHYeJYJ
QfAw+/HZsz+R7DnQJwTyp2SiBJ/58VWhD8eob78p9o5/leBkrlpFiplGew4f1qoA9LO5jfq/Klqz
qvKI+t5OJ4C+i5AfPBIhqlBf2BiWkb4TVUcvuiCevl4hDWVMLfUJrQA5Zc4l3OZNyrhiigsItquy
KOE4IhUqy2JhpqnWXZkjw/31CFsOFxplgOGGrl/Yy5cR9UWNqWC48P4zDyVoj4QkjN/i5MEHzzbY
OL4mXAb5rIK62xWDlMCn8euwXPY/vgZbfU5SrsHANL9O+wmI/QIvAPwNb92uQUSxGC1994cHMZ7n
BvvKghkebbf7U4ay34k0yVUsJULZQ6xsE0oYhmig7sJAD5+X7EAtZsC5t1btb8zNT0pREjX6DMxb
8EvPKiKmBF88e0g3ZqxddrV3iNust9OyWJ69Tml7FY3VTDEO+QtoTWic3mfvrNCYYoduwBTLjyXM
gteYwSzS7UOzKmEcSi5gF8s1Ko1NVnGA6uqQ/PXEHlFYRAhs3Jpr2m50gCIlmqhjGI4jbVBUQTSg
SNLukZcDROIfobRIlo+8+fQEYTWwinuC7OJIjSYBBvmaW8ZrIevv2IQXSV4BgFzLCentPie8Fdks
HbDMeEtlBcGLOYgx2hqa1V80KbPV6jsfgnJJbrn/x9AgzId4pvPJORsxahhX/7huMjTrXwcEYxs5
xAlAzISE7jfp2iuzrMC8E1Wd+tSP0HKLeQYSdlSMXLQj39XsI4LL2b4DtD0vkfwWT9TdgZkZFWMe
Fv/w3O7CLc7CWctwQIsJ4yWIbjDj6J3ue+53X6wwf5As8eokCC8wP6h1e062KCw5ErKcon4FVuGV
Kda8JplLq2ogkGmATJf6Tf9D5MSrmpFNprGSPHW0YWLIS4qsgQeP/kLwC/2435bRJcObVqesd3PQ
+1vK9TNwGG0w2WYJzbIrZNaXytrpVHhpjfQ8+E9xvNhOXveEReEKB/UPncUHxq830vQjWsdOSm5F
KhWhrP5LnskhQv+eaHb17pG350QU4StilY76GjWwtb0K3u+II12EllhUBmGhtW1AyQ7OXoK3O4ws
a1fblOe/6EfurGXhnIdxVKymRCTTqjQu7cFW/A2ItMSYIuO8R/vWt0OZ9R4/ua1N1w2h/Ogofl4s
jQwDffacbmnWloUQWaSJyXdQqxZ/OXjklsvrr+pnDRHlUBRLhO6N+D/7UPPfRWlQsKBK8yvSUF6/
0qDJ3cXWTJgkBTuFGLoN1przW06oB8aXswOXFeGD+8nWwNzgO6mETIP55y92/kN3vZy17PCRH+cy
seFoMTEyPNr7GRw02cfucBOVRvTBJBPrfEjTqIcGb+SV09IZQ6gwP2M/tkJpmNicMsCCK8ZSVZXd
5dNu4apuCDRVsCLCJ+9mV6aUuR6y8ZdPBhBggkxsal28r84zl7CVwunj+P4+xCW3BqAOIVhU7BVf
DTJ8IjnmzWFou+hwha06Y7W4AriwwfZPHqnO/OwqAh6Etm0QBuyJm2mapircFJ5eNi423kZmL3dr
0FQrDGTnlzSD2vI5MrUK/ZqlHZIBmoJpB1DS8ysdlozrjfl8J1RKFq0eql2efyYcWqhNNqjVyrBk
P8ImlLHLREcnJOtGinRD4UH071wxv7F567PU4ukZQQYt7beCaQ8GBHarp/r7P9hs/xzeAMkMYx9r
rVQD93mdjyqRptQFBuhaqRkSiuJsaPEnqn2x2GmSCo+RoFvOlww6rSBMPe4fCaAU//CcQbSGx4qp
jkXuwNWpQAZ3d3eMRWE/9nWZnTG3t0mvXRpjh7SZAEqi7PTtJX/9VCReavGbu5QzE8EqfshRyrJr
2Mtr/AiNWiQ1ODP42sQRmsWSNfHAn+NtUHfhsYHbBEkZUajzd3lSoJKNhjTNUy2GNdJK4wbZ95aE
apNZulpkYc33lyTeezoTrWMF0isheDk1tSVXEvU/rIkyGsk5cOJH5p0vRZGVmxy+JwwWYbQ2d3rZ
5KwiML6/2WXlZwRO0X/IKrKJ3aHyX0tgzuJqsHi3nMHBTBeNghg9wL8DI2hvFyLWrDAHKmO5LHM8
G1yS+bl0Z8I+Aj8RB68sUCu9/B2QxU11G+3hswjtCSOoNf47QrXHeZRxMz71lwFrjpx7WeK9bkOG
ZgZIimlGatZs7sJS7beh94DvrjDD45UvwzwdbVF8oyty8isOTE9ebekf5U9f/1hpFM2cZJuKWV8/
RHWDpcYZkQbr1hI2prtcbTmD4P3OMMhyDD7amuBgFTgkLDO1EEK0BZoeyqAE3VX4t1D7RnTPrc5p
w2jQoJpEdlDg3za+Lyu++F3V5H+anughWE35dy1tvH6ACaEspII0E8b8xsE3bgC71wqSbVCiygbp
BmLo16FQuaJlH9GLLrjVt1W7kNrHAZuyxW4OECB1pCMw315Yv+WmDPUGN7kZU2s5+7/jtfFZ5dwp
NZbV9e+i2zUyzTjB1YsDYWUkYDbOOIM+Lp81awyVdwY56YNQXQV9JxqOu5v2IZxtdwQD7DJjtOoq
qJpsAT6ReL5OqlL607wdcxc04xW/LChKMwaf7JDLHj3PSdPJw5Of/hu9uM5MJIuqva7QtGTwyX9V
qdQ6Ejvfyn0ns/oYj8YtT0CHCFvm0sknsKebsij8CgQx0E73SL2AqSAKyeMNKY4tE+FufIl03/7v
Kf9RoC9QJxnfhmYFT+XVh6XNSqf8w1HJqbSkj9a6RkUfdAofJu+AO4ptrG5IakD4kbfw/f6IA9lD
3bdVH7tEoXOqNebDVJCH5RdlYU3tp4q2O1b+19oQes/3k4FuMox13ByWYZYRQC76y6UsdZVFZjqq
z6vohcQ6t3yPoqGElSdmi+EPJupMPjXIVzS9ZWuc9PzdkhUlSUdLxN/h0KIQT4JovqQhQjhAOc+J
bzh9r9IwFMvU86veIjVipTew/FcOO+xaCewgflwImX8WDgkenXEyKnGcli/V9nV7PltQS75r9rv8
cPDykWE4OYeGE6zo/uzCHxThBlLp1ShG/yKm0kDRMJ9Cx2Nb36LydZYHCIrUDG0i9W0ctkz87RJ1
LT1WsdJAO2FITx5qeXii7PFqgM1dU2D+sI/hh3r7AmGHAoQ2X89Ydrmc6BP2Fo+kfbYleFXEubwh
Nef021i5tSEphXLsP96fa2k6nGcRE17To8zngA70WGQ56XYOKgG8zAZkBxfBghSQtSu5X0+nt7Ul
V+PwZRWO3jt7HDpHT0KLcu5AREa3xm0USleknVd5c11z9or25UNZ2qbErd12HwHHrdNlDQeidFXj
Jc14xX1DDaoPJ9TPcKoo2D4fKhOObcoGTVF2o9ELRs6RcqqzCr2S6cDDpM6C40S1KUfXQYY+rvyf
/Tnq35QJH59fc4qAxuyCFVjqNZ4NaFr7k7rMCjfSqW5lJXyGG/hfXE2vnoonThUxIZSoXogNPoHS
EveyO9SfcOCCOgFR/9hjsMxMlnlHlfhuueNagpFZ5Gwls8Kik3SyDGpJEZcmuIupO42fJmorjfbI
il4oo+xZi4KyM1kQIZT1i1jRsxuECCE4N4Gb5bBpaXsnpsDUWsFDrNaR+Gc0lWDR/GOCTqSxtU3W
HIC2wAjdjHfiRxev77T0oEBu3ezPSfaawbXTheGjfc7t7qwqrOc/hjo2n4qfOkwAl3zALwU2eiDv
97xcQ1SkvrpaH6BrJPW53kEf2mxCA3ZT7acTjSKAXn1ssYBWe9Hv1jrDXwosL/oEEWpa9yQawJIs
V6UVabsaG1SIT70nkjzBCxmGi2o2jtiDNbMYOYRBHamv2aUkwq5NQEHwjP37UMOC0FS3NAbUTQKH
X0WiZmMPVW6cwnsYfUukQHCSnYEG+kZbNW7XqURJX6pxvkYwru9hTu9oezWaLOkxmCSaKh3oYbuK
ufhkSf8cSLnRfb/RGmS10sAAtGZrkIgrTsxd8JdCUSuxEu+5DJ7BB+NBSVviLGoi8DS7dDohsRis
3Xm4+AFrhj+HeeIq/doiujA/TfgnfHzghq1MK0A3/uRHHbeSVPJ2ODq6QNkK0o6XlRj06CN7FVhk
EsbVxMJTUKBQ1XrWX5n52hb2woUkPtUz0fbpZ+CUWkdehQYOYzmuQiWjFkUDXbOhFUfp6H+948PY
Ik1iKAb6EDiWmHzDWLAY4aWkeJLof2Bww5TcdEtPvRcvvUhJ2JyQQoDzyBtsT+XOKhO7vsx9zgEo
dfDLKHXiXbn0aqXndeajaq7pLp9mkFXgPMvBA2p1edjKLmIjOR6cDEctVv84/Pq4KZoQh9BkfSwx
3gHCKpMWSFB3ucvxbxZzfFpftY1vTdWo5i5gcexL6VgV4CzZCterCUf8/LBe0AoGSpNMD8jtNBWF
2s98t8PrVa9BpSftOEWOItmcYfpfNOOj5zj4iKKD+pbZrg7i2+vkNds5l3uXxwuX/tFoD2pHKs38
pJ1bcM6AI7god7iPzeBgwm9I0v7lEKYPPoBGX14mGepkrwq4z6ho52v/7oc+7unTePdz4AI/6QKO
0AMgPkHjeNNV7y9lrH/g8JB3poR8a1UyWrDUokr0V1/X4PC9NUilU58de+OZWn3iJp2UFxwNrjjm
dXWQmHM77kZYFAok/KL10n0tR+hHiePzIZhbRxb08E1m1t0R5oRrhZFmzqKbg2XAqYu1/qch8BZQ
YheKZb/rwnOWJqPcacsIvh8gsP8Als+mUZ8AnKpvdEOOTRkOzLY4BVt/zUsxnRBjGcLl574lWHgs
NhWAINZZnjryZXuEh/n/oASOgvxXiOMsZW88zhMRAGCCS9mmDxNLrcJlE03cA6yExpmriNEedQTL
FAgpmqPF0K829IV4cZuiia4AmwWCb44Qld0WmXkDcABLWYrRM/e+SapyGDRG3RizWsfY9C41KEtW
Q0/JXT7CHmmw6KLsypaaJDmKeTohJTCyM+7Dsj6lhTq0oqLt6DoiEB9xpkvWhzFACcur/Kw2/sdg
sLFvKR0WU1CT3Bh3WZ0B9MVF5Pi+RHTP73DsY97ouZCkc+IMoMXuHpj6Makn56QVEhe9LrG8xMwT
nk1ag6r8tpY5pF7TodQ9UikWh6IpsL1KJjDEz0ciwB4Ke50MpNWJBzjD4db/FVg/Ws9AwVyZe83K
xerMtIj4eDBYgo1wk2CwiR91DfDxkCWZOj8N8+VT4rmUFVlh9QuUJneywJDpND5sKZQHircictLQ
2LA1nwIr3sGr6A4lRVnYD5C87RpbSV6/OPnO64E+5vM6bCbIx2eK92M1t91XkutoRtiO7Kne6Ds/
7ylq3GO7VXLCvkmC3PT2HQOm6vjfrvESPf5DQpAhFzxLBWwVWIpmHx1ViU+m10p+5qlIpjbHAJcM
8Bfspd0+3VqTCcnEl8XJZ2tm51ffiJzv15vipUj8i21rr4U7QaCk7EsGpOXQxvzk5tQ8PuXYZb3b
ZwyOcpAooTnp3J4U22YY3LfAtebrywFcZPtUtlfGWrr1jiqjrT9+NohBn6W0JjEHNzgGnCGkcxv+
esImL3MPr9rm/6pu/UCKKf8Mbar5tp+fv/UK9Geq/Jq5+PfrFEhs2xlvMTk82Z3TIB7Oh9vDt98h
nH2Tly3bidBzVjPH0jH6apptAUnCPMORMRGOF6CiLYwTfEpAye05FFp+CfjZhq2oNzKIlvs5PMas
Kk8+JRNk35Fv3oh/naaQKNP6ANGrUeAu2JzZd4o674um44OM4M0h5P7ZOErecFAVmcnwpPEBWrlm
4aB/W6o8VHN11FHHeUSXQeOdwGDX77dW3u/EjAEG3NBUD9VRUiLS8sYgFo1Rq9pS6ZxOlEHAzTPn
lJos2avRkMY60A6Hrg60zCVKAeYdjY0LB2unFIrzrHE05j4NBnvE6oStBjgz2urURbAn9L3cKeXI
w8ckdsOh19wLHwMVvDsblrIocAsIbYxHaKlqnKnLEmM+XXi+VKaSfa/7qPSlLNyeqVrFvGXMEFDI
Lvhg8OntxhCePACVfDCbaSiJFWN0zKdSCX5P6k70NKc0f8x+NYjfiuhV6YikngRWiBwMQmXVXGIq
O9csDvjUARWb9iAN3TeQbF+a7xWY7dasccRscGTmlCRZvjn78K1tie3onkFThRR2RLkgqw3p4f0q
nqkxsKk4uEGS1cbFXY735qqeFPldpW8kI/LqsRP3ws1Gr7NKLe/RehxNn8lDS9gntU1bExzgEVds
+imVkN+4RQIVEyUn6OjdwUHVGoIvsK5CNrIKUSgTtNoxmB1YoKkU61mrGeCLB9kJUohdBd1Jwm/6
CM6qG1M/IIV+/sySRwDYbiQ927ceQTtgMSvP7EhnJ+Dw9t1iQxktDp4Grm8o8KW5kZ+SqN4UpTJF
7rJo00BW8Mk1ZAC+JdTuy12dfyNzzOBNLDo5cHW2iWIgZIuEhNVKVLPSbOlnwc6rOKBg7O3vVAS3
RLXgXx/eiYt8Ri7vt9fD+/RJTiT1OQAgXlGi6olW04pmFRBFqddRuQf90wAHYWj8zPYOelQ7rrHz
gsp1cqMyfHavL3vUHuAo0+f278YMvmIqBCuPHWytToXHQwRoinXBgkTIksxJYPokkrpkCJQXovsD
cCwZyssC+TwYaVUFX5qmHxdRpJylBKLWzTJocNFqrl2ywthdExy0ES9xw62+CAMWO/Hrjn2Dj4/3
VHTogqyBXdmAlKPIRiTKwLcXPJzCplPbmGHWNPU9aFl+iX93v3rOHSD1PmLtAR01DnIJRaR/lTF9
09THtYKXgkCU4mc7mKLiRQQdikX6xgG8oK9HQ3Bn6X7yv6KwFDF1CTHaR3w8G4fltXnmUK0BOYpS
NKvsrhYenRTc+2DD5lR3gQ36gS9EzF1eUt/gKxslTj+wG6OJ0E7NEwQ5qSSzlEO1ikdj+WbHOr6j
twqZWgWxOM5tw9YXplTcff7AKY9xzTVhvEvfAf5d3QUfpbBATsaZXUWBsMnWFdq7YSVJBF4dWS3v
Nkb1wnCmbYGiXMoxqJX1oBEyk632Mfb+YzaI8ltnReq/E5ewQm/e89hp+/nBqjGZnVidqmpuYONE
w2jx/imEc31xxZK+wftjg0fE7ur+/E4BWdkyA9ZTUaDcG3QH+apzyjcNUlJGilRdiQFWDOk6uESb
nkyDjOaJJ5HXgvpk+Gk4k50XUkz9uCPICkG8wD2Ox4QkBjHQDqOV0S8LtaTt2WWLlpuOg3+ZFghy
5rP/L7qdacbx3d2pCjW9ECeIeeYlAAXISHS6x6lkhg3lArFKWQYRgrxegTCG7id9UFaoMuJ1Abg+
3GkhSIjLC2u7tz2jydsHbhgpVuadbYMSSvEw/Tpf2tB4oQIl0iXtiYUUZwmKjWY8cOTvCUHRTDhr
sTl1VoRB0SKOiYfu3FLM+HoBVjkHO/wFWPQtUnckogu0XXamWp9Mc5fjMU3Z+uoIpyCEeimy72pp
tkURAxoHeSUlQ/BWe2vH1jHnIlncXKjZSA8TBK149X2V8i1pkolJUOvLxxVzHqpIdmxDAF0LKTyk
7tBweQcdWl57Ur8pFQkxnKu8XmiNODVHoXEBsBh1KQ9AkcC0ToSXWxcPDIF/dB8UuZUxp5XihSEV
dM1aiTuU8bqyfk6qxzQFBrvE67AzrknslnhC3R+VLyB8rsjW5c3O8ZQQdukv+ApcfNFbjUd3HNYy
R0ies7rABSHqwB0eL+ApphAY61JQJG2lczaP9a6JhVj1oa6ujfaztFXu2L8ISNzqiXlIN6lZuFXs
BETF5mHqwCW1ZnbZ+jv0B9UDJ+QDz/aDQOBJ7MEg8lKMkyenRTwxpccrfPm7eSq2Lz47oA8vC6tR
3tQAVYa3nuxt5ETBX9IdFB5akaWMZAJKHFwD1PTaf2VXOKk5wDCGSJ4bLiV7UXsVbieyJHKfFp4U
AS4t+WNr8Tpp52+vYT8wWjajZFEG4Yp+sjZUdCICiE+hrqcXeAeKwkDzSri50VhYYED674sJA2ju
hG7wAUC1FnIbkBWo8QO25sNaPbrSfs1dqs1QDCyYEE7un9EvRleN1btGNW8fap2c2xvoBPW2XN8Z
cBfWMA8pnsI5ApkBCcGip/sEekGqul48FSXBF4kdvi+uejAK14Vd/X4huP7+AwE7mLi/mWTaC5QQ
ajgRq0xOBf7Z3W+dtgggq1b5abAf2/ZxZl06vepGzvBDEMgesQHlrY/HbMu82bzZTgLSTtdX6/MZ
ApfmvAaQy1D2tgdO+J9q3FBDFWak7EkTYtzWckcMfNz+0K9Qqs3bpnltcbV1YB5Q7oobITf1ajQT
9rzTD0kGkCvqAdYLZ/ZFaq5eVhTTPWqK7v/JKRvR429QGR77ViFA0Ex1svS7uG2k+GP+VLJ7OSKh
3tCeSTp3KxXyNgTAIncabgafiDSvfTay+8rRCLOtMoRT4yAlYlgyPlt6l3NoZH2biNU/XqN1Ef16
Yqc6g5BuJbp7AB1orWflgdUgLKrat9O4Yb7CR2rSC8jlSlcEIxewJf8cUt30DpNLglCh4IiusJaZ
mj36LPfMWoxu0LUHAztoJrOGsvvdtpac79XpM06Mdqgrq5SYQF2kkkValI4TnBz+B1KScKKhbufK
+9VbLyuM4LuiJ8sRl2K0lZDv5trpbOVsqYvp6PVqWz4p3W/HHmmfVoORuVulN5f0uK7JlnpvyEFm
s9uSwjJlBPvj4g4BFyZzrCWO+5MJ0RzjJyziXKUXIvkzmFXjtpyNHA/tciJ8k5GmRRhf86nM4prz
X2W5V08IAjRYfnz5cvD0qBoDZkC8+NXdHuIHoch1y0XmNcaz59zQmyfkpsOwmZVV2ET+Lx7PN3yI
LmKRMh/zFFuXc8CxIl3FVx34BfMIsDKfyHloE2M6QtIgDKK8gl2fdn/wmXhs49QvkvHNWr0M+FIy
DvVmJt7DPvHWvaJAtqKOtQBjK7ryAT14J7uv5eNsq4ehXjwjQom1/go+uuIGJd1ft1yzAKOdZCes
2Md22JCMD7DdcGvN0h5wfgLVm915DjyI4vkiHZnz5QCzeqbz10+Si/bUpAn+93OJWu4xVD64YP23
PTCmPttVruE5s5tG8Iug9Hgwn0PHz8Nc6ddUZ+TdGY4pIu/chAMFyKbYneFOtvsB7bZViHuYReil
Vyf0FoOaegtwXplulEMt/25gFWUHkR0tmJ84Je4/bIXmPdyFI8uW5SJ3nh6YjVmicQ3doAPq6+y5
T7yCRfNzGVjKC7sw/gTmc+z8JEqob017BS7te/Y4t81gcLkLFrtStkOe54FWqYOOfzZP7Re5F96G
MXw/c+qS3Q698qeEPG0ECKLAZNjuFY7JnBnfNbQwZqW/SaMbBbieEzKt90KHdvD3Twrn91Qf/vwz
/hcJzE2ofB1ooYXfbIRidB+EFc1c4mYva2cT42Or3Z6yVUvSzMybelJ+0zCQxSxPqPznTi0e7Ukh
TQ3L0xQptUgWg+gfelu7gpPKzY2oPFhZo6Ux+4B55hwc1d6YSvEaDZYaDJNXSpN1D2hdCu/J3ZcP
hjF4e7K/iSiVK2+lmIyzNkxh9AE2IA202Mmhx0mKKGWxK+iEk57Qlc3+KZ9hBH8HYC683jWEwbCc
Rzp9lryMeKOSDCXBsVQYSSYsPxUGPi4S/Wc+TT1yJjr8K32eZ/P/4EXenP8JwMRpPLlsXX+Fdup4
0IHZo5DetGTFo3l14iXh2/53yvsxHZEco149YFa1T4/0MYx3DBbmfJ0W3UfU6c0E8k2Doe/eQUr5
6c/lJQCsa09psxyznU7jy6xaucV31wu9lUx75ET3tJRnCkGxF5yohcr4/z8xCf6Tpa0cXasJKB94
QSxvuzmstDsaKO04Pi6Bf3l7z68r0XC8+vmMwGKescbpECg5rGonaiI+arExtonJ/8cpiIRGqIWE
uyBQYs2H3M1Yl9U4WPEaovBJJP70V3to8QYWXM7iqbxEwnX6jzc1gmYy0kn/vQREK/E79UoMvFo0
4h35NFfNxH3XP9iBcwJZVmu2eU8IQI7KD6brPMFM4hgw3TpL17PdYcEoLYFpMI4NKsKs48OYWBY/
noi2EZ0SIhQsz5CnOWSgU/4PJBIC2oYMfBvyPzIviriSyob9+epk1kxjDxOSAv5rMeG+rTf5t0vD
kTMzyoPsYYocVMoblPH+ayvj5o+LlBZiS5ZKaN0PJMQjrCbURi2KAscVRJd5piyfQzeK2waen7uN
MDrzijxzi6QL0WvNJGu3hrL2/DQKx/zXgoX7VgoT/JZq979iXAHqXSFagdMuTzNZOHmXlBlTY+PW
1ajl5vOV3JCytAaMbigpfrbH6+nLKsly1np3/7nv/RaV3VSrfXoiQhZ0T6tFIKf3UZkEy/6FG8cA
/nOgC6uMFdfIkfKOHbP3/fdxAG/WFjSnoQf+V9X49pw4mvcO9AKpcO33U8g4ZZvyUxvhsKUVevAY
2JzgsBMXoudV1S/KBqglp9lvhjJpGmo1rmwaWShG620ecjX81EFfYOAPCmGgaLRB0H9Os3YgJyaO
9hmr8IVVcNE6tnB668YkcUHBpXYHy3YBlhEc8KnHhj7PlT7V9s0wyFB4lXNcGXWFw1+5J9hyogdG
BHKgUnMWBDMplqUMmoJw5M3MWwnZ0Vbjhny+uv4M8p3BB9LL2gGq4zsisYDX9s2ryywYcqAzOxVh
bZ8FVGX31v1RGWOtkX467IwFT+9y2dIj0II9bQOURiZbonCXcDCpdrJ84H45sFfooayg49C5yli4
m+LlX+QRzKXEeKsL/qsO5mgZhJzJHeYktm8qdiRhxQCJTGx6oll4wIZUkZ/uPnO0BKF5aMfV85Q3
D4iaKqcAZX1eZ0x8/aHbVWvmgMpWYVG9jzSej0llPyMHm2imRlbXqUqHF/3J08fYrbQ4U3l78k8+
mM0xF3SnNFIzoIw6G2QeYh8IMD+3L3eK5xpb8Qu4p2NDYuXdLd+tQHRF+ErlMbLhceORvspba534
P36GGrZa4+3tk0N0W87CJ9K2ZjZyKtA6KHsE2KB5/cbfIXzarg5xOCbog5+8BLA6q02dE6wlnrqH
DkDaUuBhfOXYYcQOSgTlrxX26pCmcGwVb96BHeR+z/ZPh8OSn4evP7xoA6SNV03RCgt+65rSGq3u
9h0WV0c5sLXhnFjXS2oD7DpkJ7zAlotv3ZM7H41xoTKFmo47abJaysY4OO1mRISWVdXmneV+9opK
GpLW/O5hf2hJq/JxxUFDa+RPjQidyFNSRrQBeGex2KRmYfnhJ1raPefcQ600k6MKV/8PaguLb+Cf
AxTtrmBQoY8svkrRa4o3TINzKpYKmAwpEQYbYeOi3Zn/QLrbmO2NyrBNPEjP64KFCgMwiEOam61M
J5eZChRrKKor2Mm7L38V9nT2TkGo9MAJgPr9j8714KOFoVON0LxNzXxQ4nIegEsGMsJPKMZ5wON0
8s6vU1UIKg6/JVYZ/jzsLo2Ykx3XEZKOpyQ3Ez3Ye4cW6bV9DWA3ktlWlxN2u/Czwa1n5iI9RPVH
hQetzH71XUVxkgG1V2VKmWVRqq+bbrBQmdXsEpy+xt89+6boyoXKxUVQKveKwUnhdMFHhUfwaCCs
XzB7AZJi6BKhgqDrsd6jzdvA+YcUb/WUi0c1MX1EWnUNXA25B+/+WP6BKIdpMeePFnPKGCFv8FQn
lBFfhOumTBhHTTAP7uUeaxO2WdQR+zAtuYCjxqC68RKVryPBQPfkHI1tzwrV7rpbfEJdWo8cCo+o
Bua9PMuHTtAJW7VjNfRN+PsE9S96cPLGeAv31o4qbDvTucDx80PBrBJAgGllk/AZ0wTf5mHxH+IR
r9l8c3W6EmizhJ09AFudZoVqRCnHfNupe6NqCBwX1w55gHPunx4nGEsJDZyNeCEqInimkLKMXaKn
MKRNgMzKCcwrlIlM3fVvd4L3q8kDOSPc6LSZNZYagg6sT6lnFpu6fR8OmOBHM5aj+9thwlnZ4Hw6
2JIQsOkes+xWyXl+O2xvbiFqFsQZniQ9nbLeKNlkdiWq2UQjDifcbXDknj8FicxNlTCQdIrKCAJP
FaWjeLaU21CuFLNpRqQ4QkpkcGoj4/aKPnt4IrzRLDTy9sRQpek72gF9nUZXFP3spPoScSVxqRX/
E/5AXVTxXhCPi9+f3SStaeAffXeSKjWAVxK4yjjP09ujGcS3MgIeFvPnI9yL6F0pAsqhwLuAbMtf
+a+J3oiqqPvH6BQ/9OpL72ixJC96o/ISDXWyHU9Ma3ccSOnzCgJLXdy7BgtpGyn1uvIrRXJMKoYJ
iHFFSvQJyk6zVb2vDzca66OlbnGiG+tmcyeQUJViwWGiGaS+2+eWvmYO7xcMbdJLuFlYcp/f+Qls
hifInoEXs01eIQ77x+YK/nKSpAcVZSRnimOqtMksIX4j0vTE8SCX9Mz8sYVx3OBdEKm+iarW5Xdg
xIrHCDa1kOA0TDTcQ+N7nPAhTVmJOST4UTMC1B5fmuDxpYPFRimozT4HrGVaGyey/p7slQIa1xc8
wQap6RS9qV3SvhDAtOI+r9hfVc5fwSyGC2Jws3EVKXH9Fea3t6K1Iv7PSUW7PkpU7WZucUEyuTIj
D+7sdUyTte5CB+SmnDja3Xp5hjNCIze9HyhZYfe0s584BbT6rI1qaniECD2E5AR8UJcXzhanpMa6
ZangQXkQ92kBz8+lV6wZWkilfc6Ndd+FRzksttRCeQH9IqnCL9TrNpDPnVfkXKbTh/D1qwALI9OK
dCRSBLT/jddaDzT/vsgp7Dlja79qflHKmgQYUPVasvRG+PxealArHlP5r/t0VSn8YjBLmX1QwHhB
S7+52Is1lsHy9BFow7aC2t71Dno1Y88PCQyMCCKRYXbM7Y1kQ4qv6hbuj5mI9XB/m0Nfp9jXR4rG
oDUnNaHW+2uziwE/rHYKkuzdzGlVJDR85H1YOVA65IzO+G6V9ocqdoew5xWzCtkj0Pnv2O0BgGV/
dIg3utXD24jp/uXFG4xhOPkjWMs6IQfGuO8jX8NNN7fF021NUrXRJT2xIM3+e+w6meaCmQTXSx+n
P2hAlxQoyInwUUsUgZ/EMNIO3OK0yRfTio/CR2YsPJFHJctHVwAyWTxZ2GgF/3u70w65LvY6qTGC
bf7VGd6yHH74fnsmNaiP3bu6OcyP/qkbM/pyRLHI0dG79I+GVX5Szm4FoujRVWU+n9LNQV+iz6On
qRPAMfj63hph5EtJ0aglxhvAKTX7z4irmITJZ+ozBz5uT5aZAFPzxzjQdqQkAYsECNAqxyJAnbT4
odvXLtJnsjDh+/sYVxz86OeFPYz50Y2k2cZxjANufvwfXWezLaIx/3NogmC4kSPm4J6kJ7oM1E8b
fk646/cYazAIvZsxsrVbf9kiZ6Aapkq5Py9KTvlZsV8xy/fvILoZuk6boig6zgzAhFACSCmtlT1G
t2frNBirq01/6LfW88ooxeSAaYHbufgvyCdtSdEeU4Eaz76nrod5YbnsLF4RSDzk38ENcPgca2Rr
oACMtsgb2i0lWVrbR6kRSzgpSNMgH2JijzkFEXuWILqtk16+R+ysQ/KnkQ5y53zG0ZXT7j1LY+no
Myvh0zmTZVEjfEhIeyVP4KDbmwpqBna9Ew6t0quAuYTuB4AXyjy1B6PiaOaAVX6Ox4mmsuSFInBk
3B5VIZhWT5TImuCvveAkRozJLFaAnBmhVX6/OqN94MwFmkNGsnujppAnKGnxwlv8JEwhcdvcR9T7
ZMy9vS+cdZCCa5V/JeoF+elUKl/9OXm8KIw+rEawgT4JPIsNo6owTH/93SC38ccUjB0d1+vrzT2O
WtP5agFXJMDXsEzAPVVwTOYOC6JhxzmhkbqvNzzQUJfYbs5AvVvjjKjK6wBS1FvMTi9YK51Je6Oy
j+X3A8GiRsQIygRoXyGZmvs3QFqilJHruEMjMaG7KiMqyoqUhmG1FAo/IS8dO6r2um+5K7KZQknP
TdrQzegNBaZmww3q8iwvotjm86mzbA0NAJmb4by1DXvpDC9sMR3Gk93qoVuCTLYKTWokYuVhew+q
TFEOGXBTNfUIkmKZD+A14HiQGKDCnRBeEQ1UcYPWt67mxNixmHWnFTW6AxQbgNETaDIz5pAttO2Y
1OJf0p8fzNfBP/FyP79XVuKXv393WPjIs6ntwMv49bI+JoVRhiBlvuO9/Y209eert7FgBCr33jE4
vdxJktnwRAdHhiqWUVbTKa5rF03f7PEG92tCXkP5JXcYT+G5v2Qrso1PfZUTo9W9TAAMJ75PW5Cn
2UqhgqsGB4wyU4xbYsLehyIAfCy/WF4xNhk3pWuFXSS54bwhf4OZi3zY4MbhTiu3ujVzwuFUmB5k
0zLTE8Kgzd+f+IRtEjly6e3J0wqUtuiquptAo0Px1lrRJ17t7HYIfM2qZIiWLvT3DdD8W6n1ITxL
bxzFtdD5RJhhte12zwn8j/0Bwpy7cjhYuosGR4RVi2JA7rz6lS1yT5iw6GPCWoBS3wnOToiVCMIH
Wj9afTDBFNxrFczXX34RNy+d+dJNy/BQKMBder5IB79/7NWvgUFRFlXuBGgNUk2hOojCuqZgUFER
mUciv1cGM1+K6QJoS8iukqKLkGW4r+SVsVYBFUQn/p0TyOFwuCGsrxA2DyJ9+p7N2faizKujGyBq
xvhRc4HG8/ulOlrpDl3ii1sSgtpwUDI6T0iZ6WXftV7NZkbq2gHYHv9PPoONThYfMf3F8FPswzXG
ezhLTKwH9NM/62wAEdskAT/dGXL4mWK0rTWTvjy4+M2m3DgldJ1I9DlULBDhj1PoPaC73gsFtbLW
q6xiD1d96+eYewnyXFyWYx7kyZfpbNDQ2SVCO4M6sIDFFw3gjbNphvBNaP8z//Pwa0GM5/+Kf35E
JJ4wgL1Izo4IGLO7OFEBoXxYHKFosHhOIZ5Y7ZMQqIyj6Ea35JzMJTCXsvCQOquSo49+3qMDs4/Z
TiLvQSKP+n+RDRHHkA3AXwV5FkBoc1YheqKwsPsjntlSaT99RFuVDqbQwXbgWsEdbVKGJb1vIXzR
/mUeLRUhg2+PHJTUKUfhEE6FJi4sStUdqaHZlr6GO+hrKb2soeo7cGLi2KQc+jq+UK45ih+P0LY1
h6rrajX/HjLhzmCEJ1kbRFeT1XKw+cJpkWIc6gA4EVnCy8r2RyTApd8N4BeFVVXeanBKzAdrOFGD
tG1Ft6DtnEJnDRkA7b/Oxs9SNwyQEsJonFtRkyAu2ZbZml85Bkt91lKWgyFCAFgLB4K8K6GtUffE
xyCwiP0mttE2AjLpZVcbE6gFdvcWA+2j/vROSQ188d9SCpyCh/uYK9qG62UafNMEs3fsv9N7U0Ru
ttLFvxni5dGdM4aaB3RcEKG6fyi//CrfOCc6LMuHyRCN1eUwCRnynR0H+6RF+afoNCGML1CXmSD4
JpVS7cRy7OxwFPRngC9jcD2lVk2QhYzd8HcKIn2FtHQNMcie556qw6Xq48avczslsvWM4UJNSQni
zYruG591jEOF+UYxVnoV8VmeBBWe4Pf027Bwu1ofC4l0LMqtvYJBmBsDkoKQuJkRO1vihKbVYYVq
Qs7718PCNHqVEZPafWPFK/0LJX5Mm3qmqy5WV65Etv4kL0u/LaTlTb4RCQPdWLNQLtwpE7MxbAzu
w3nLqlff5vGwaicuP5TO+w/eCIJknyZXklHoYt9C7j7lo4zkrNxYUimmXQB8sYHAqVddqKRY/MdH
MR0SB4ID2K14lSOWWgXFOvwA9/SLdkck0XYF5IgtFJR4TffSABUZiqvAWrU1Hk/pdMuSho5hK8P2
/Xm4tmJIrHSEdvDnbPkV3ihqlEFiLORhoRX6qbCxOlDaTqIC9Pixd5VJYMvlnHsbXfmAxUTZFLxj
l7evWj7J1yIp0C0La3Lo75y9vDjC2kWSl59IBmNNmm0nj6gW5C70fre9CDW26mh4HEgb+0BJ6VAC
e81Gp8fsC7ZCDyetLQo8xLmU6sZugKM+ErcAUPbCiKrleEVkPbn6KxOV9pdH0/78Og0pDZ9g2hSQ
/ddJmN+ZxkRN2FJvugxfzhy68dpmobl1FQ8MYmMM+0+plvGaLLBJGeQoV+p7esaUznE9kw0QwAQI
rwqlF8h7nic4+5vxYkgaEi0QtpZBiubVqkcVsB46TYiOviLb0mrddWIX1LboRrkYqiRXLBqwhz/i
nfHmp6ZJ5yM6IfayI/rKtU9pLJalqyHEvIzSNLnXieCTpdVy/ObQzewuDaUlxmhcDzEnZVyMEsB6
blQvEILqqCd2xGaPdcfhJIBlg+XKpZ2PinGAdB89y2bPWyFTI+wvGZ7IxU/4voxdhOArMMWq3ctq
/kwqYxJHc7KEm02K1GORz6z6KavD2ckLT07/3BLwMh/PVMBPeRDZAoeKDqd6Jb4L3lvfM7dYaV8H
6CNGekK5rgzoLMvWrgxD5lDRwS53Q6+7hNtgzOMWLOsv7GzzESla+7cPY+qmMA+t9ivQInP0XnQZ
DVzdul/BebM4Lig1SHdQ8Q801pZMZOGKL8kJVTo/dadsJ8nX9j3QMY0C0fk6zjLWRffey+jhVekG
Ww24gC9NX2Jy+cyhUwfBvsxOErMQBx9S5RfH9Ktc7/9bU1nFghUJOq7lsxEoeCJfiaqrsL8QmXbQ
oeG+LZyz1/i1d/j8cOjebDGCl5m3rVX8NVPN2zQ71NmWDfrdt97lDu6Sf+ErMD0LRLph7HjjGqnX
c9bEkICVPUFzbiYjRGQLJXf3F2jQX1HLczdkQhmmrtkVn2LRBkuIgjNnzctRfC+caLt4jDT3qsgu
0wPLqfOUhCfqa1xG2YFcFK5zCxZso5WkjOkp7fKJ33VXNI3Ba+q8lxP3N5G1fuMZmj9/E4LCNEix
yBazMNs0uQtFwJCJXG8rVFG4uA262YQRVwJs0d+wsIHinziKPogqHwca95HD52BkSp2VvQDAvTN8
bocbMl6atXrfox9+y8s9u7deyTt2qABtB4tNhjIQYFTmPMz0APQtiZ8eFN6xz9jl49NJM27zKpdP
USVyljv1zg8OwROsZUo/8Ceqc06uEo8VXK7Z+iqiriEMcTQ7G27tzNwcdVH8Nhgdzrz8Joa6sKEu
nkbM/5buLr+6Ef3OUg1T8mv58XJfFUcO9LdHBadZPm4OUhvWALe7KlLePu6CTzkWuTUb5aISS8qf
ekb7ie9sP/5qWEwzP9rxYkX//KkgiAk7rFCeHxpJYpbvFJ2RcRMBvnSXoD59zoeq96ATzC3FEQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_22_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_22_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_22_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_22_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_22_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_22_1;

architecture STRUCTURE of design_4_c_shift_ram_22_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
