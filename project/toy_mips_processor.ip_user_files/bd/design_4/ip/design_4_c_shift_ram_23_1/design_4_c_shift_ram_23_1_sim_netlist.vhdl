-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_23_1 -prefix
--               design_4_c_shift_ram_23_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
OjsvVuyt4J0FESzEN6OYgVyggAJsK7tPSWQxFKQsO+XVE/c9rCGKEJHoIiy7xbj2Ha8BS+a7Fm0q
5T69UPa425B+tvFoH+oAVEB7IOUr4s/K0ucZsghsctVRpLm33nrcopa15kGdRYHAo1Th1DTFIHDP
IYk5jMbPSSI0pfDvJbjQMlOjHw6U6GSk0S0MOcIWFYoKK+7sRRFsk4KEnCKoodAfUvwASv7ET6Ek
UQIRLEi87oHlV+Hp3z45g8K2PlxHTVT2Re3zNAzTLGwFj/MwxHE84krmgNRIY/3zf/+t1pAs1Sma
voH3pJ2FvhM2pPs8E42US/uADBUJ/4XZP54QAv8XJ8BvF2UlPVYCEERWBfNDYjtOfn0ObZrbIUXp
MvOnD/4jOHx5XojzkxIzFeqnqMuhIRcp7VUVPPifiVIAKTETbFqNTaLt1CNXx1XvZlnMcq+hWHWR
QIgT/XQjhNmXVg6dmGbwBawnI4z4K9VltjxXlwFjZhxjuul8he4hiEa9/ylzO8AvjxxESHP+hJTR
rJFe0xZfBXZodlm4kKWZxSLpBFLm7OD29hlsJGjsdsf+I9siOsCoRXLFJ0+soDyf0tN9QvrOI0ZO
7DQ713E8X/EtAih4wZ1b4pbRxqhUsUEZCDnlueaak/LXwO/GToLnyroI8LOpdXVpGu51tzO6dSFZ
PtGpmRXs9iYizFVtCFlt0rC1pO5Cvsmpmt4doG7bxaF35htyul/7WLo5O6qxV7DsMODiX++DVo5s
J2uJnaMaXsgxeHtbau8mGidnrK1nQb0VQ79d8NplqpbJiOiJyJLaISy8TNg77sImo3qMb0/iBi1X
0REF7KFnAcrP5zAgVjQYk4Dtlw7Xy3eH0VKa5Z0rWLSru6yR4Hr6G8XC78yyqtV7X83R5uaVAfSW
8lk5oQ35bSA/01khM3fYccV2lLfLfdgnNfdI8bWUAr2iKLMYe6iu1Cz3GDA1ZKvJ1SX+ONkjHuy2
Nxwu+u1yTrrDlxVecASmJpxGiBYmHPVHNHW0vA/OdXd/LMIplHO3WORXgQ1D99BLzGap2ezIi5bJ
y4kocsr8kEVLMF0p2ZFr7QmCcm6Z/f73Ms5Bfh2lB25Y5tyYt/X4rs2mkuKhgyefrP5iMey/TIHv
tJ8iVgms0zriUEQJ7NwCFimM5n+QRh/2ZY6Lv+OTohgkGIzZi2gOA4eXhQHIKLGt2hRJ8sFlTT/L
3QGHCqiCRWUCTIppUsx6WsQmc0rFXXBRH3exOYCGoA2JM7zD+XnwwTochYiWxSzIZzoB4jTrYAI1
pn1/59WZx85ayE04p3H2RGDpnnjyvI+w4e2zenPJl/CzCSs5S897Xunw1+l+8efq8a/U+DFkTBTe
l6bg9dcU6hsfNLiWEFyVm5dvxjtTOnjKMOA4n5OIU9LCGzJX04PmA1HFR4mTtlu22/0jfg7eflt3
DNSsY6z+Z/qdLfM0+0pbTW+Jaqb7tfXUSlszavK4cQkt1k3ZP0aCiNa8n7T9U2o1n0w3MpMD0Mnb
P59LJ77qkq5Q7bwiDau47y1RW/3L99CI9mnDauSbRMeXDs3WnzteAlztFa+jv965aXmMgk4uKb9i
68AYuLuWSPGJhDFlUh/A3OaCdImk+9vJc8Vo1cBxeaZpUdrvEyc7a6dnnjlULcT+dCpRbOtHiSAU
MLjCulGywsQRJD/z9FnRFtYGAYntpyrAufwP1G39Pl25SCwbEBVWccDLdsm1xfVvwZLwbG3nXZpi
vh2u6F2J6OB4k71K+oPfagEbugda1on2BKQYV9UqiDHFtwqYT4PnbkbTZWyb1t2HZfy+x3iqDUba
5AihrdOWes4UXV6WrNZDNVN91Z6dPmNF06CmC5eTLUBfUXbZ0jd6XXlY4LQgxtIr4bVtew1CVkKw
xOy6K8QqLiSTX76pam8pUAM3LcRWxTh3AdrXGC83f0YdE8vjD1S3cIiOu4bCc1od1Iq5N8eJM3gM
qStHy0Yzqf8yaQDYf1yviu3Mymh6PrU8VDUoxwAGp/xA6Wvoq2fYKR5hODcKRGmSYZWr2QKEx/RE
h+6tekPXy+qe73sd9+uDb0tSfgwMyiCMaBU08ELhA3Lp7tGUWI+9B6lfJQy+WqGrc1LF/lA/sODK
ma+5PKrQD0eYicvM17AipjXtGsyePunJl4VMG7/vgH6SIm7ZOl51HG0d+3I5HS7wxZxp1I6QifYU
61Zvc8Ew3cvfyTSaAUkfT5Hq85VfYKJIyXGJNgliNqdQ7Ho5CPoi/VmTT7cQUeZKrbYSqP2uEhO1
+QhzkbI7/FYU3eDz02x9oHKufWXjaX3z1MlcNfnIeDf3n0byaYoTEy1Ace/lE01bvOxrXA2WK/hH
JV3+jrw9udolhZ1hklO5OsGYrkoXsxcaPn/T0BlvIbTb9NG+CkkW+wkTLppyf1+gr6aXPvUO4ExI
3X2SSoLgdlt62xfBh879zu50+ZGU5g1qfCVoDVYGL5oArV+npRr55Ji1NBr6zY8j7eDtjaxu6qLe
CtnV/ImMR9jolVqH88IEOCc2DD4Y2bepUaXJGViyz7Q3IF37gRz15WTHdFWLiv306NwJHVlbPZvg
xwppk3OZtynpdLWvO0sCd85axDMPYNpQIBcxbbabP3OOrNX8hKiL1L59Au8QPLngANGTlaa+jCf/
31gRCihIWeeMz6/LchWrSitUloSu/KiIo2OawhZFDJD1cgULU6/gFt0GsfmYP/HpjNGHN5++u44j
Me8uto8no8pJneKnxvKdI0vVlrfd/JSFckFKzOVBg6Bf8FYKB+2jb6IwfLKbzLDhulovzOmxn9X2
L1sR6SizSTDHAExBOYidc1Yy5XAWGdAHmwb3uQP+n55lPxUj8awLki61LHezdVuQMetvXVxbRHCn
OEVFWy+Yx+MTBfRkjf7MwhgTa3RGxJVXJvIpf2vU+tuyr9ZCjBUXGsDmkE7HzEfSz60Rc+qWOhLv
Ns5FGYqRqU9Rm0AAmERu9mwm1Qgrp6jMvRej1UaMNLOYZItS5u3wjfIG6PpI8qWiqOc4OJtaeWlq
TBt/hjtMRnoJhlesXSdbEmA/9sxthce3zX9lV6huUMsr5fC8Q0kU+7iNRDKjM7Amn+Mb811quyi0
/hFbAqelT3euhZ8FYMNiUm7M5F7LHnEK3UBd0gBgRTGjvompddakqFZ8PKb/hE6RhU1DySSG6Lj9
qTXEBRKtzzzFKaiGyZcOW6Jkj5sudXb0sOttBqkLgyA8U2/sreska1A/IuJCuFR0K5Vnpe6eprpx
lgH0aQyl99C7vGQBKgRX6o7mnBy7OUz4SWqZCJMVkNxRboqD+OTtyrSI9KvK0optG50ficfR3X34
hr4tMt6paEbt3Bi4RvMWZc6HM8ntLFbLnvDNfXHe0MMt3X0EwPjWy+qa2yH16Dm/iJMiZP8ROd7P
FF30FgMeXh69Hdlnrs2yOozoHXJSC8qx0CDAdG93x0nu77rtI2qRfwvK8wkiIe1Wf2dZ8FlYcvAp
OrSAGPjX1e07BPHyKG0r0ZQkBdc8BIq/1Z5tCc85lfKSvcVA2+h+CgXxUI6e5YrKusfWMEuafo9g
vrD5/yz88G4Gy02unqf4NArnC80z9Kn1Sge6ByxHX9IxxnqdzU0e+743SVC0nu9C+4y2rc17CoQs
QyxvOBolo8qYrbxIzQ5Xn+6qMDhIPQzh4VGaWUqG8RaCVsfaVquQMw84yRu13SOEA0pMaewQJoTL
ROx8hHkJDYI9wS8TxUiQKtI0K7xfBT4U2NAtDT75CdvJu5aULvldGk0+N/jyrXE9ujpRWpX6aptV
e9ilWohXhQZTExb9D8b/p9wi07UwSSPA1Z6VOrvPj/z+B3FlrB/QuCahozn1yEQkR8018okoh4hc
W+RDb1/imjcjLvdwAfi2WGS+FCwKZZYZB8SVBj+UxvmG8xiEsKEcAGAp+K4PsDrRfAM9WSoJlnyv
MeJjq8DPEEZsneY/LN+z3NUGCF1G2CoKVlVYabwyOujkTwL3AOM5r7iqCCxoDQRicTvz3nZolgyV
8uT4zsKKdanHkw64C83kujWAy3jkuMWJnvbV0pRUuBfqEWWu8mH78UxPh8x6FMfsupQhX2yJRyi7
FUVdg8cW1/CkIsimG0i9QL5thUwCS5VGDi8pN6kSQJxHUR9EUtz+yPfETLBg8EzkrrY7hifnLhuZ
fsOxQfjdQZKD5uv7BRq8vD3rGY2tngFT981YMCzIEFancqX3yRqd3gWljWvujCyd5ON4/y5R0nzo
XWt7NH/euzQlokLav5DqeUIaiH4cEyUY/DkPotCWdNDxQQQW0TkzE1zwJ+HPRCvFuN+fRFjDwOCb
BTsQSVKpImf4j4slc9R36+tn4c4RAMFYyPe6ueTTCCDxhXYII9Ypod69Db9cftKltKq7a7HU5bG2
Z/l1M8EpF1r8cA6pgDM2MZeus+BVgQFb02L0Ga3AGXBAspKhIpRD1NECRurNAaqN/sNwMA9DD5xE
WmLOWFUOG/EbF/p1KHDKdlGbaK1YW+kDwv90QtBd9bjhLKk6Z8KShe1vlyYrv9jxk/F0enxc48ol
5/qxE9I3Gjjqvhft4Czo1Myl48ofKg90gq/onpXuDDM12FJsE5xwVJiKDQGm3mXA8bKDOb4f8qO8
T9DF3bWh4hr6ZOD8f1W9mVwszOJwDsOqJoRqMvcAQsHLHdYUAAmizb5lc0wE8GozFejeQxbd4+MQ
wCxbC65/pi+1yZz6/WV85CAnF1Z7C1DxJrjpeo85waPw0tQoqkxZ7MMZ2zK77BH6vFjYiRp7nS5q
dTiir7DtZqz07RS9hEtIhefliscGbgATkzDd7hF9n0kmNPOJcfU99plPidJfU1316rm28bi5koAe
zefhdkhC9qsHjdq9UXEgOYTvrOs6reD/1rI80L1PmpZGLkFXFNuGRdAfurBpJ9XMftwj+zwqOybB
rTQw3MPRWqGqTvDgyXuYZ+1Km38iOJArC75XNz/ig2NRSCBzymWEho8tFaCP52kzqxRPdEkeJdeY
8ZHdhahhVU//2JeKKCLrX3haAVGyEpXRoh6jXDL8SGhISzxahGlLBryOjGJtP+fcVe4bHBEGMIK+
1yLPMFdQJjpp661Mu8710D0lE127i4ug6dTweyxzlOIURGsS87zXukGE5GNUybOlqGBD82N1y7A6
mN1UZtvsaGG8/zMgU5pmCDRMOkOH/VqbT5SCSG8AaSckYcet8ccuzcA+GCybRvOPnvMsVQzJfLxM
S5PQrTgFxCZy7UwHkh/8rNs/n7Wv8tefoLP1z50QiKHsbu6RsCR79Kaiyf+EvBtsaXiyRU9wtyqJ
1bO4Fm5dsQkAi8rodQAHRiox9nS0cGYTZ+heAd53nt/1uJQcA7tcHuo3+HX8tEz5MtxjPTU4izbu
iupPSbRZ4TakZJ/hkaDF5R0ZbxqbwAxud8XMbspIprbkLrQKnpTVyBYqDS9wwZoDT4jojpxj4je+
4JTtsCyNVJ8NINmQr875YvfJiygdfA/SoVFp2w9ui3DomAmTv6Jlx2viT4WIIuPe54WvW4N30HeX
98qRrjv/LHpQDgai4TvivFKWHallDxcjswlYwl7qPDQEmv0ekFZ5GfoutTyXsiHTsibX+ZG9iDaQ
O3ay50HTJ6LwMc2yU8a4+i3HrO/iopFr6naFTSMFHlwle8prn7HK+cyJQV3x6F/Jtla4/rTxJCxM
xhO6TM6qv3x9MC1TGGfc5qUehrchiKcC92mahnVCG1BZEKk/3E/Uau+b8u6M+W5JRRh+AlR18Vjq
AeF+ydT4t35W5xCPokoo3+fYzyRJHgs/9CppOqsvHO7t4M+zfx+zXoBgNY/MRZJVB5M/5jxuMCrE
K+I7Cm63wWMklOibRqIaTEdfExYu8QWX/bWgpK+uMY3OeXmRFlwuj78H9OMCYjorbpSw6sY5rX69
XUjvtnues6r2Bk82oI6VJTgbGD4lGgsWTRmt6eg/CVDs2wAYKkIfAu5hI8n1ojC2p7wtgjxw4I8t
UNM0LOlO+eH5Ma9vp8VTHed1BNi64cco/qb21h5RCnWR8TJ+6SWZWoHkCga/zDIs0j3VUHjC0Thb
mYB3mF2iMUTgUVwL6cjWwMRaY5a8MLg07nAytduYOXr91T/lf1QQmEfEwcXt0os5LnAu4AnRLSJn
llZB9U3Ky4GemohzrtHcKxv9J8oQsfV/BHUE5Xdtg5ze3v0jCOt1STRUUxsVpwXZeyS4UkYLyOe9
2voKCfalqvP2hcMryngjDGhhDVzcKyRhvrvaANjYAFLYeviVkUV912OWuRqCAB5eG6mQdz8hdy7y
j9McyfODEgj5dLpDtzaUl0Ftjf63F367gT9pQKLcR4o925i9irpUhw8Eie4+9Iu6xIUNFiwB3Q4I
echvRFMPFAgbiH9U64Uv+OiPlCIpwr6ifpO0+13BuahDjXjG3kD45N+tEY+bvcQxUsb0YLBCo23A
NN9b6VH4VwkDcr5T+JkxhmfjP8kldX5Pcfjww/1lqS/ewluQhevj7ibKb9LWg8wENSpUd6mAjF20
tBGnBnQ6xZqYSQL1CuKlJd/rymhyQlet9KtizLzfLhR5qgdGitX4Ax82vWf9byBHqdpqTr0/gGn+
7mhfnuhI3fQS79YmqjA229mPWedR1Lvzvwp9v4INeIA5BKZW2Pr+pYXBWZSitCUPSdaHAsAfu94i
Ynd9qATJGDvhu6mg6Ot8KLOQYBhXb7rErn+ZXVo2Oqr8l1EuFHivEQqwWvvlFhedc21JAERPwaAi
ipm8sVze5rwP7chMDq8DFFuojx5yeR4WsCwPs0HOKDXZ6eijQCYUQnrxfJVoVHbk9YGM+aWbQHOj
aDmJB6w+LTcVw9dR8HRU8DQO67NsKZ14lUn6zsf2RqQn8EkFhnmZdY12rEgDIBR4Sfzkh7nsBU7o
DWT/J389uiAmx/FMPKTHh14EZbXESZofe+q8WkRqachDln6J86ufk6mc9MKYbwVWfn7qgbQk6DB/
6vHIxvnFnYF/Gp6HHPj90pb50+FvpoynYo8HMX9apTcmycS0RwomGhRATW87pTXcmHPrkHPxq7nU
AjsC2kEQUI8fxHVnWOXDAYU1Zx8pBjn7togeGRlZQRUif4hbZJHbDOQOHG0bJ3e+pD8EWZTCc657
nxWjL3jtvUqFdgAxL1Bm1CMNFkDRElz2o6PsLVMN4yGRvkcn4Qvi4oyePBobEzPGyE5GeU+SnV3r
z7Ts380fD7v2QZBdXbaVEJ33yf+3i/OzyEn+dASpwHRVm3ivsik5vAA49vUNsmEMUveHaWlZjBzm
50TiBWUIrDVVN3OXZ2AVbkC8MANw1qPRu8ak+txBJPnOurxQ3sEqsLcpgL4lDz3ch0FuJmvbi8WC
wTrMnb+dLpe0jWro0SrAiWv2/qKF4QiNEnQOukNbM6t5EmUFN9qtnjkG51M1f+3yyE5CtK5z6zd/
27oUTnQYCCO3uRbFYHqEP6M5kl86GtKra4Nhrf8CoRrOJeyMRwzn09FemPtS9twYmY9p6XrHL0fF
0mhD8ugEbaNb6gjPBKNdgZwEeY4VTUjuoFLkibKZ85GjuslFAeEu6deiDlX6RZMUB/ZJJt29erVL
0VfUMNhrQmrv50hBYdtITnq+oUYOvGl0YOcqS9rGx6nqtaWLIHx6M/jlZZRHUFPvmIGzQwpToPor
AFvgj/6Wt/8r1zC/nYW3JyS+0wN56UEGZGs7FNPh0cOzcEM1Zv2a7e3S2GRwPPkGSHtHyzLe9esm
4TSUu6TxvehkWQnAW0c9AlGXOetADKEPX/eohkj0R9+8r0oZMzF7z9NtxxqLDC4thwQ21rSUK6vE
eBqIE/kEwP4JpRulNTxPFkKTcTZtkBdP9cER0BQtrmbhwH67QTU5J+xSMJ9ZuL6VApglRGgO7O+S
4hHJGZCV7CYfcur58dwWa9jvDQVojrv66J4jgFUewNL4HA9U4pmu3S3bmvy3OPlfJOxFNv+P7vbo
VgWdUAe10CmZfyHu5kAF0SDL8S5bQGazfFCayr0piVWji+5RA4uS/ZZGAeR61fJHC+y4daLkayt7
VPTH8jOIsS7GfAw9k03SOwLdEJHB7c/nbyWoesz5jgMVMQDJfK3e6fxwzGepTPhNEyUhMsez8HWm
w65FZWxJ59Rhxbj9kU+JXRv/UBLN1ieAz5SAGTrBmPN9glKnG+Ql10z1NZ71C2DnJi6a7z65BQye
L0eaP7lV5jrIhD67ZY5G/ZDrVaRnMruworNBMN1GOxVApS/xMS0mhIFEyg7ULKRTKW9DT/396a8E
yAZJZF8uDCFmRV5xQERAtO8NauGV8tmDJuNGz0N57exsiwAWrafvFKinO+5/70OPT1S/NutgBcoY
MGwMgCwCuhDHKiwRdQ1LTRDrf1Tyj1v7SQ0S8nnalrV3Od7n7BptDUTfbUuHyNT99dHewuTYle2V
sVUPBu41cESAnD7Q4b2rKi6XJOkzNQnR7nziek7hbuQGoy5t4Kz1QaeMh2nS67qdI60hn71YSqI3
phaiGagVnMPDB38P6sW45gbBYOjtezlntoRgw+1ese6R2YfSGuVdKJagaDswgScvoK4tE52PC+ut
4tGDxLwMrrwkBYZbNGSSTiZr8vxBSgqKHoFgk3o6QxZwteVG5KaqSAzFUUF1t4dKVEetdQFfyJvd
u0T8fSHeBeAgKkChagJrWI4MclSm4d8sz1UM8MwC5l/1WSN8qBqL3jOri/JQ7md3+JCF3YKXijZr
Ge/mV27U7NTM3NsDv2grcRwmb8rpKg608os+WoOKAmRrdNeMy8EeUE7hmq+FaHpQNTf6JHwEqBG1
ka2UId2U9xlMELnxPl+jAMYgsGjrUQ5kvT/n5UkR2jKPjJrkx9YnQ1Nlc2udzitg4vwf2nOnqgys
RVOG+KuH3Xs6oMzNjMaJ7/juWpM9TMaEgx9H2/qvyjgThvitYKEGWFMfs8dqhpyi3hF0f6ruZ6kN
qWW2Q3V1o7ACJY57eGBVT8myOx9gOItoBLJMOcxogWzM3UOj6XFOHhp4tRh+VWphiTtOqVLoEyPC
wEjyjsixHm4ZFHYFYM6os0SvHUF0nSPoqhknn1aZK7z42QBbIRek21ToKv7RDc5lOoqnrDY1p7y3
SORDKzzn1VofzDxo5dHd5hFPmFHdviNlp5HdQVhYm8sfuPYjZ/cPJYNhiuDin4w7DsEA7nnnH2Nh
j2zoIBqJ3aGrcwAUcCkPWCd6q5nVlkpPW4+T3rrUAwu5OCu+c/xummyl668RQU48bI3Bp7uzJYnk
54Dn7OQR7OQTR5u8n++ZP86FV794CxzX/JYJYmTeDExSEBnnyEIlzWuutseZSBlDcpKx7ib48s3T
au+HBYBst/1uGo/cTc1feodQi7FzGJfEOhNCi4witT7nSdx6NjdJ6n+RfIG+0ZcVSKom2CK2ZKZV
2S4wm3ZQtPuowyhpFZdyBJKJTSAbbkgAAKJAnZWrbxiF1AGm52LoBOAPTYktsWbAIXb3PyInvSKx
bBgBhS8yKeeMYOBha/5TYnEFb/h6u7+gb9Sc9Vsc8qdjYdGsB6erCAVA2R2dbsFakOa2mG2uNIeG
V1/LpIxOrdLpxGRU7VQMkujpHRXdqQBTzmL8Wi551z4L2aYJi+H/H/+eDoHV8jM7Ly06+6I5shfS
JB+FrIv79FzrTZYLZV3nnxJTJzHIE1DXCL1xBeH1TEkva9X2r5QDXRlmX3vAUZSK31eozbN3UdPR
LYUCGsFfRm8T9TCXpaqWp3dxWk2tz0u9SL4tqsxx1hLqIGnXKJQDzwJ5TtBzcCq9uhSPQ7M64Izl
MRHw3UXzK9g6e6MGOcuncjXqTxy38SdXFx1m6uNzy5R3gq+KXJ4PV08Fw7IPukUtFdXDRaKHgGbS
tYP2tGyHOZ08NKDQGpuoRi0jOJlRJIaIZF356ISXq/+ElAxnNKIhvvap85VDDciZu5UkxqJ1/65R
KE+HVnuN/U0+9CR0Bo3rdMlIIyBvmE5qohpiTD8MuTmfO2DffPBHHU7F5mam3tvRVn05yA67rj7Q
dXc+VF1n5GXmIRBlDaLpvf6GO0D25VuQo5d6aL0TscXfUhEElz8TQkkeryiKLK/UWnLm0W+OiK2h
VG5mpq8Q3T0xEstIn17ZarKu7NqmTeH5ARvRhYg+7Q9DYif11H+P+nmWiSppvNZOQw9I77H9tb8z
NIHCY0t0pwgs4lDX3oR/dTm3jlostuZJFUtw0dG1AymCGCCTHY3DqKMq1FQFwzFICMIkCB9TZgfR
VpWM88vksqwRtl97BC+WyjUiuURTgj6Hx+X93etHJhumKwwiinuatHeRDJlksU2R+dr5bu7CcHHa
0vj1DFZCCThBMEIVK6xnri7mP8VthTC39bNOsFZbvsJD7dGSyUkPsohyjNX6VhDa4Q0BYoYj/HpD
DCM3k/2TTtxCmFx89TH+tRfFTsufOiaLq14xZbqX8GacZKX4GfmXyCRTGWDMLnTJjt/ONLPPvODr
2PkCpeifRmh8nq2TKuncvjUdSx3IW17Kn02YS3MSo/d4xLlTYsDwV9FkpDIkVZa6q+kzlqA1GQ+6
5+0T9UdDh8LxASl68TR2jaT1EvGVv0PvEcCzyAacf+lLvDdXgyy5eMD3ohA9SVvv4oa4DTLNGZVw
PnVzcqTvw33ijS4zxSQvsYNIx5ShX6vg5GA+fuhNGztUzry5UiOKDg88sEwYIQN9IsHTDaRzuBRA
ga8QdSYRoOEyRINeYTpCfscjpmeVxgt2HfwOnBm4BZ/w8ttSj5B1DgOFhc0Nmv3sloXhvOp5YWWK
PMZBJTKUvW0wxHQxxCHclizzzw6Xp/0FA0LsHrUvtugj6OftoFIXER46qWweEu6BEMYYW+JwZkox
d9NIFHHVOvZ0sXB7ppZg95VFiIUKBVG0hEuhEJD4S2n8PvVTrk+sDN02k7RwRsMGpy8S7r/21UXq
FiP9hv19mmlLga8iEdvs2Fao9x0BnUr4mvjgLUhAR14jxUQEAImn2x6bKRHZf912iUEpkUohF5O0
Zhd9HLTFx8fkFz3AMXe/yFUmcLGl7IG6mvxcuJJuJKnh8eAPZC6RDH20kIIPYLxurK/ZTOPTuHcy
8A3focEwxhV9+PW3Bl2phYmTuKkR3mP8RMWrhDXXsIKFMbUZ26FcCuDsmPFBEehYlRSV7m+HaVDd
K/kRojTltQojJYTG8DWJDYJ3ugeWqItW3KEO6QObL3fNnlJ/9Ca1MaL8pdtd1tKvj9m/Yy5SEHna
DAk5FCX80zOKqpgKDexMmbmJJZCzwxecBkiKRjo6wRK7cGa1dFyqF+yD3BQzK7ULr37cugB91eC9
L0C+9v3tb07HwA34HPk1VF9+hAvpvFGzLoXVuVqNCFtmHaApQDxE5JPxDNH5pU+GvPmIklptO5Kt
i10ZbX0arZ1YC6oOTgPFjHwiDLrkF/ml37/T7ncJxc2JKV0DiPEr4TWa+dFgq7/1WlO4jGr/2iLr
2b+KUzVtDlTqdJA3YyP9btjvfVmLQj/AIF+fY3yh6IzhK23QdyyU7lUHy496DzlNLl6hBWKVoxO+
Swj6Pq5WeZ+ZPYQzNW1MYy49fRr+UskQtMLWwbo7aQU002jiWBcQsbK7BXRKEsMCobTTyY1cd2nI
Jyu4LIeP2wdhQiAnBQaR2PveiPpk0G/yW562DqQW7hNvSqr/mX63sPBlB0yKlcYwJkYjw+0Sbzok
X4rgo3Ojpc6DFCjq7fqi+k1lECayXOaOP5mxHb2d6iBhT3si3rJTmG+5d1jD22RAwb0BD9FonI9T
+nl96VJ7xAuDDA6pKo5+D0tpEImT4fx8MQpNPzkaUKtWwodgusOFe51gh+VEP2agdEyZ+fzuNPRq
yEi9cSlUiojcIChcUbQc5weGWLM4s1mpsCrmqJn+LVWDxKlwVvxiobRwmSBepDnUkm+jE4Tl9/ir
1xUH7tsNtcCFIL9l070pTxsv0xsO20fPrMySbOPM/6L9GXRE1f1DvjsHBxal4wVQGur3sljYycx4
b+C6NAbqzcnZRlzGFR94OkJNu4YgkdLG3xWof56C2BfHILLWRX55fX2oVkZn15iDhHDQM3yIMcft
1DNtYJ+GrxupjAlUqax22gn2cKBvwSwH9LVBPVh7Gqb+YcJ8zhlHwsJKFtQe3hAH4+nzx61QuBTx
ECWk9kDG2jTYWPVP1Ls7V/HxJihxJDZgsp/DDoaw1re4NIiBFDIC5l7bT/fku9vajNmhFTnTsHU2
BJSyYqpWCx+CjN4H+n1XJc52JPywEvQ+iUMaEgiI3S2vwxwepuL2zNfBXZkhIS8JAHSRZoAuKdr6
xzlYvZtOA2lsMt3VzpHtt7kUgZL/DAK+HUoqxOjU8FOw5Q1UJGN6veO4TrcWyEzoG4QyNyT73WUW
hv6k4lNQMY1amqA8eg5iLIXi8/lXCYIi01G0GBiYD8dkUHHGN8Jt3G5abDsIgTu7+DE+EzuDKjjN
wK0rXdtQTs33G4Q5DTFGuMaPk6T56PPLyhxqSVES0GHg5j2f4Km8KiBLAPAvuDoOIak1UtzoBjPO
CY5Ms8jFQfXxXdBqSvWM/SShxEiI7sW85Y7j/QEkrsf64imiHOPBWUuaZXdC3dink+1tBinPvbPw
3S5Pl2NzfLbqSbrFfpVc+VMHw4ZqxXfSprRt8DUGdjUzZWLY0zZogrLyuXRl2MyJxsYYp1I8YI0w
LwiM/U9ft80I7DjOLTu7e1Qz93qy+BEIZRS6U1Vfxa6U8CXmvS9S+ZHN5Kvzt7vDBpzB+6nLdZqI
1+I7eU2WGJnDFzliqJu0Hq5Msv7O1yiIwmEJMT/0PQ1bkaLHqBiP1C1GHKRQ0yeGV9SOmz/F8O2Y
gr4H6S87OOTbM0tgweBpgxT3+zTIiXtgZRWLvzNMUPYYFb3uOuXCK6aYUPjEa86wxW+5xx32DFq5
1dE0Qkkilk3pzR9BQpOk8RKhf9BAqkqyE/dNUuXiT98gyqLE+u1EWBS22VICZPcuHzYgIzf8SCb5
ru08+DoCHSd4wmp9xN/RCtqjrYdsy8A5XmrvxSIbuy/j/MtimYrScOauX7xxuERHdjMDelOVI5YL
yGCNv+pXs6nI5MJiKl2IvripZBSf6gTp0A00pRXWv8LJ7QzQSRHvqokBBrkPE2rd/VpHkoxAUzUd
hz7VkYktzpFTLicSgtTNce4+IUOJBLM9s8lNDRpuAQU8u+0ggzXrjtN1jmi+UnlibWw3EIIxjuUk
BhhOqLCND4qPW9YCo1qaB8hjsSa1xcKqfenNtYTQJsF+cv8xKZXulXDed2DjARciwoQl8rhZeWV7
1KHheVfLqa8uO2A+iIDuZS0LhL5vOQhta20p7hftdUBUt0rk2xslLUqR+S0/yZH81Y3YeEipXK9k
PkpxfJHmYx6/yPWrcs9EdRKwYcWJYwYcym+xLgxRg+HWD6p9o3Rb900Q6ZE4Oz36x15J2uDqd6mu
DA/mEgs8MOlRU7bXh9Z9iyM18a/uWN/8ZQrw2OPgCUp2tAltaffvGT7sQugIC/tgJ8BFwXvUFKZF
/IT9REmU0oArHlx8CSZRCdBDA0wEmrYPZSbj8Y4+ezxH6ZcjX+sZ/U8sbb6R9AfWjW0dOC+MCvq1
LQg4fazmKMMjPnw/VviwdwwwJVcJaMDBQ4ModOy9NxmML/0gypX4LrUrSbPW52CST9lY5vRRmruu
jdPI0iujZjHzsT7gtuu1Il/dIppIy8IVth3kRqHpdyiMoPFh73d2/73C9KwnRvpHqaGr3kBl20an
URP+HCVjcuii/0y25Uy6jdfEzUbEZ0QOD/kPg1S8S15ak1KT3LsMtQH6s0+FqOczYKGo5bXT3pvw
FLn1CERLdXRYrkPXMVWPJNPRr0Is5VKmsjZPD9EuxVezmpHyKY20ZnRiNaC7XFQouzGKvGA1VAZk
K/mo7TlWHlUPjY2qZhwcQRZQ9SonRaTL8lHAFmZY3/LRcVyD1k35fQpoZ85c/r7JbhbChux8HKeI
+sRBMXNrnGA+XhBu+e9E2O0ukmLFJ6eXgeJc79LvlvT4q7ZYZ1wTJv2GwAfiQ/JoGKeXaw27F8/0
tJLalZIN0M1MnY9wld/I7NL8KSbom4u5y9v9vrSkEzFIlJDtaeXAPTbvuPrKlRpjRX/S3A2598RT
no/WDGWRMnByh4P6VyGbdxshTohiGol/ukdR33x1Czxc19SuTK7SU/M51hiaSvWhNF3FTO7Fl2C7
8aphhftLR1oTrqJvBsvTaEmGSAUmUyrMcciTvHnTGXCK/XXN39Q75FJP2IFqPDiAYc6v61wAtimN
kvFYW1Iz0L4C+GPng0XUurbB82UCIARRnX/iE3fA8yIU6LjEPMK+xZUpKCja2TZe31ZF1hSsZ3w5
5SyBI1uiOu/AXMZh3QeS9SnO+ZB8rW7GtIWtIVZEP3ft0xRWZRtGBuMQKMMhDh9QpNlHi2KL8Ucm
vWTmZktDrs5F5TE/wakNZvNV0XqXwAYufCkwOJSdqI5TT7yjNAhE4LPex1Dy1tLrV5JTnSJYEdz7
erpmyLq+xJAcrx3g2X92DSjg3lFYKBSfnM5MF1pWOmUvrEdAnvzET+mIwjQSBui/tSUHDzdJobtR
aerWUIPtJMKMxEW9b96Q2MTcKSzgU/pY83mwH+fy9xOtF89tfJYBT6ahqJvyzvs+cDta25azO2nq
Al2rfLho9p2Udl4KZ5lB+so/klOn3FPFs3RRYYPjseMad8nQKc3/PW74pN3wDNm4+uKqpCeOYGKN
gJhoby/hfNxsgm+h3ym0Njxbtc2k0ga4YatofpaGrKsh6EuefpgKglfoVvcLHplgQMt3toJPwmP5
4wDZKm5O4FjKhbDw2OgI2coKOPIxdEQ0o4FqVCSHHfm1pK9Ww9PFr/biPG9rvEIE3w2JrADeNIKC
j3snqkrEccs/PMkfdJuu/sr0XYaCxcpm71RNABULsHvaIXzlA5GKGZ1nVIBjZHMgvD/BcCYwXdY2
pq2Gt8kGyQFfiUVx/B54iCRULD6WlOT7nGD9s12s5325TNfsmOTcpYF2DxnxPaGNxmyIrgCH275E
x+rVAJRdR1il+5pvpWue8ALNqQ2RRBHzw8DkPO83eV7U+MD4SPQmOCVu4etGMLo0oKLo4k4DDk+u
xx9vtO0KdgzzWYLgEFxF6UVptBoL7F13W6killcb7J5hJyw4uUP10dOGX/Lj401BJqQ6c5vc6y2b
wMykjGWqYxEqdek8t30RCmSEx0kiOqnrkaZshPYZAH8WknRhfF1bpr+Chhsje5su2A+TZeGRyecp
6HE1E4MggqRLlQzK3s0NAVn/0HIFYCR64sk6tSWI+zPH7Afjo+CY9wpi8XSC+fEFXa0KUhEnOx//
FW4OBm/JecIfKtB45kGese/ngXt7+RTFs1aMTCHotZUWqcYUfkbz5RaHkTcRuxqtp1pbjLxIASk5
ZzGANZuQuxWFvsgUCELUJVxznjGTXyU4i5dRaOMGtCa6+PRZ1V+oA4i+5HSkWMKImRWFCfpNcMsy
C0awRhpdatsLhgmD1y9NhkMVym6oXm3YKm4eet0Eixz1OSPuXBU8Wd+G12IZLSHKnmp3tUroejRs
OxfmELcMwhzjr/XTElbY0M+COnZ9RVRsQdqrQlQcaKtBipeoy3WQNuQlY70plkhlxUzW7U8Oijf+
OrIDDhgW7G+0msLylB8aoQKdEyG4RSZS64xHrqnqYMzwEkuocgrgx83TGND7u95qjRm44XXAGRF3
MsmsXvpZHKy3rmZWowoWCSa0WpzI1d7/6nQ+QTBbF506EV3wMqND+Mh4FKrYH+BviRaThIef5/7R
YDM+L7PkbVbYiKP/W9TiHPAKIdCXi3g3/8IhZlIEfDi8Jt1D/px7X4HsQQMf+FudrZn0FYzs2VoO
NYxXW0D85ddE3MrMG63fMuSdTUu0p7as18fisZBnnYBtfgBguhnT6ZW/U8Rz3XTXyh1ec+1FFJ+z
cCKnQnIR48wOHprhfLQjU4/RT21fA2IUVfJeoaDc3gEZz+9sUG67Zf9UyBn3qMejVR4UONAPxPHk
hn/kQr21JlXBrEdPIq6zVOO1M0cTMTlJbVZH+ulbgkCUv0ElAgUbrYU8m25GsDZWNFkfgOXMz9jS
p5Uaa3LLLTLmjXbAuK+OcDR0tPKA0pSui/wXutzB5V+qSE3ZjmfGJuomIgJR6zplI1TJXG86dykL
rd3eQXUwMd+F4B+ARi2oEagkg56CIEdBoEIryDfv1Vky1uV69P8h6GTXCw+CWQ3bZsjfqYhjpyfm
8iJjB9jvcc8ox3gVXSkDBam4Auw3s/KtBVZK0lEDZPNRDuGkOrzQkvKkzkFGzarzjHIOiyQj/0V+
O2YWtEb87RyuGaDXxucv8DYpwvWVNZqXuwP072bo70xhYDI60MHN708odRJkniQ1cOUl3E8BONT6
fqmAL2RGK+a9skJ8nZspF3bNP6xWcE7wuCV3twBFt+xjG/3W/38R4nqAOPLr7myTJUPRCLHZQSNE
yApWuhWfAbbGafyKB4P+0gMkozAOKE9XoZL7iTNKyP3lTfcEz5H6qpy2QlVG01KQp2xy+Zh2yiVm
Vt6vjyQGV+rYkWL6XbcHCIjEA8maTc/30CXMeNkcDnXBhZ42cq5bpsyt/G1xyrG27pr3Fd8ZHQjb
n2kau6WBAC1a0u4aLQUC+7yx2ZVe0jODX3PYwaJ3OYR8XHMcouJ8Zbw33fSUcAcVHKA/ueVIFVpG
rM1AK+s9y6ruUHCBepwjPWxq20mf++5wDDey/Zrdw4XefhC6znHOnAXWTwAeAbRa2vCUC4nr1ct3
GRDV5j+WmiAOG+503eIGlNuJBa1e1pG/lT81ZpVX1SuftxPR2hejr8QDREFS8ko0hbqnus0nfW7T
tYWVpF80JHrGr7UZLD/W55uNhnjrXAMXt3WRv8O+cmFVC/SHKHJA/sn9yUH2Rfsd2+KCoXn6pbj/
ernz/Dkqvvj1FoZs9GqV2+DyEQksFo7A8zzgyMeQYGTYdb2W/cbjENa0GAh+C4OpYFLTLIrk/FTV
u0VlEEJLYyG94IWbJbKYGnCt8URdCz64iKVT8gHeOgmHmxdxUiDIpHye17pHgo1R6yfiGHnf3af8
/7lMM1mtsI+0OdvvG2+OKZGiEHmZ5dUk8dNPJjMa35Jwez6uWn1ir1b9wOoO6qEAXZicasf1zw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_23_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_23_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_23_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_23_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_23_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_23_1;

architecture STRUCTURE of design_4_c_shift_ram_23_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
