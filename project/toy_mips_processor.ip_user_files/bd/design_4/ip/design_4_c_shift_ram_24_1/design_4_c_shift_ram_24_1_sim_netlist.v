// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_24_1 -prefix
//               design_4_c_shift_ram_24_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_24_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
ZGI9efaTteixYusKoBVX2m2yfrRzhQENc3UXezQ9as8C8PWGl/xI1TX1AQn/4vMHOngD7ZbRd54v
sTMlLKu2yQFIFt27bnB4JD3MoZtRmbNQG96FW7QGOKeF4eaBGGOeMHRWXnqh3+wYfb73yll5YqM+
0ZQCzqQeedpVJ6G2NnJx3Sn+CbrpkA4+DJmFuHkfsH1indtwL3oR56rIWfGPmWN6hUJOZIr6+FRj
bN2NAY/+Ju120ZTmAgWmNggUQo8mdWKKyTK1NEFX2///9vKB6eQNWMCXTbZ1/jC3hgvuFJq1WB4u
0MI6Z8s/rMMcPN5uzfneepGUS9+sxvVEc/NXe7vmKnyHZ6wNehquOXPBXF+Q1bqwZoY+2J+XUKaR
vkisOaro+nVDRSirPqkVFphPlksAWKmpyFf+TCHKuTPggGrbK/EM9cP6lpme/FqlCL0MAoJaZ72q
Gai6FEDeTu+xWqfP7kmOD9Y2SIc8Qh+y4ynes82bwQ82lqZcHvh8AV7TaZPBUC8Dlswkm44K9Yny
d9dVz3cGh7tYcbvYI3+MlByms+K8B+ynxTuo9Ezw384MNp/GBu4tJZu10DILIff3uWaZGE36KaWK
v6d6pssFH57uQOyGz/sHn/GZ1tSO4na8RPnayX1K7oiAVbcRWa94bYWsBIoE954Owg/6PHzhgnUK
cnkhRlSAPJ1tYm5ZCmlmIJfpfw/ygYtEURUWZG1w3+nL+A2Zqv20IVGRGEaGH463q/jSXcdATKlZ
SM+j+zvnCxDGCREm9tzbRoKIiPEt477q2lhcDJsFwGzhjbYb+l6rpX8OH0QvUM9L5mynQ9MWXdkZ
ju6ghQxPHt7zbdj62KJGcJ/Au/Kdx+SI9AJZIDQbYSBINpVZ5MlDuBPTZ7oIyxJxXC4YDQetPfpF
7X1M+OJ+4SEFsNgJk274fco408DBRRj54dxd/fstUXWn5EmM0jjkLsWdTQFmJ/cJ9mV2nwbi5kI3
oZXRUqKJu11Ldf481nS7G+mZW3lGuOKCSRzMcF5YK4Fx0QZpCkhrc0WXSu+4sOdNC8c60udpF9Cn
cyViP1yTlanQxPb2wSxmzjs+aQ77tJJivQmy41a3Q3mEWs6lglzPMQYzX0LALooW8dqFc3hNw7UE
ZOo10JY5vZZUvWmVVUC2hYNHP7D3ckXdmrbdm2XiASkEzRhpHi2d+KXUbQs+fnqQslkgU8/TLPtl
WpddhhdoL/EDWUmlwtpStSFwdcS5ZEfUBKV3rMG6eqGNenPnZ4azXQf5pzt5PxDC1Spad6fsD89b
qSKHJlnz0O+wLBF7VvNUTUncbCpFVrBt7VByho9xvQocpwo5g91u/Vrs78DuHPqiKDWggb7VOhVN
+P/gadIzaRzYWhb1wsEzaXjNXFhX5k+KfEataPyAvm5XUh6fm+gDgzVjzY/9e2m21zFBfGEVWXap
NH/T5SN4c0YruOFT7HBVDZRPOiX5xj7LcEiG8zEvykF2KbAkP37vcm982fi4k1o3LLaYQLl32TsQ
C4kB5wcMKkGsJYKxOUBOdvfzA12LaxCBBd0mDyjtwNZbj9sKAQ4pV1hKaPVTbPo2kmqiAFgAHAzi
iSSW6GGFV+znRT6KC4tySXGmLjAYVjEq6rqO2J4N5t1dNxOqbazFDNgzbRcMnonuHZKlvLD4dEed
/KZtO/+lz2FoXmWtoaokxI4kC6dQC6oBAQgqaPanoJHKRzLHtUabJvdrA7LnHY/As0Q2t5xhGCZT
5gRK/BPfYWQEST2AzqSgwUF6vTtY+TulRCNd/kazT8S7KfjA+/kI3hemzsKm2Dbv3FdNYv2IIbUH
e42kKbMvxqf0GKGBu5X9d158w1LR09PDHoO15/vIyM1Gf4ALT3DKgEPZZhzIXfJO+ZHWBj6K5uol
lNhwCcZABpka/20C/yPx8KQInaHPmGMWYJuuLZHuD1aueO1ZHdKEgArpjffiz52KAA+LxFKjGC1t
CBRsFBA1jhAlj8fGStf9trfjSyOI99Cb/0yiHSQci+9wwfF/bNjEyOcF9gEta1adtdaGgHCFFbj7
wzlB/rNJhk7xdhtBNYuBhrGmpVTIdrunWP6dmO1ERKOWGCVPLnTg8gM5eX/25B/Fb2qInJERvAX6
CR2KoHBdN3IhDWYEtBq8jOgNzVoaru/fizRGji9CPO7cY6UheiiRJIVmSiUoU5TQ6B/bPreem+/A
fiN2H8coOT/FIlOmE9JyrJc3BwKbADNwxi69sXdYEjofEliqowL/AiXF6rRMui8ztRTmL5Ub1et/
vA08FrGXApDvSnyOEbFY4UiSm73SXq7qgGANbsj2h+ZbsYYcAHZgwC4f06ncV7K+DB/Lhlcsau+D
s3KDS7ag0WA7fz1uUKhXTjD/Z7m0HTUoITmeydE8ZfVcLDvhhKtbR/qMP3uAh9SiAvmQPY4jn2u3
Iab+lVY9nmiV/oa1o1Q7K28Oy35VgKvDHFnUeUGQWVQUqZDeS3DmpmOHpe/MEMCJvMnA0Re2Gk2A
Jv/Bx1HVotp98B0BrEwMW29RdNQzE5Cl9IKe8+aBzU3TBsK//lHr+Dgbrz5GtcGmUqGp6JjC8ova
LwsiUHBo6whiyXeXlutipL7uYj9aX1kijEC9req78dsvDvD8Uc/2lfXMGdw1HtqebkpvVyevbBWz
NfuBtbdCV7Y7+ecTFkW4GkLwCOsbvURIbBERzkPKZZd6YMpW0Kqzmxypnrh+va2sSLRhGeurI/66
j/LVbuCVgy1xo/0f/snWUAxr1FArhKvyXJ+/cAo9J9NeAD+Y99tx2Sj1uda0rR/mUz9vansOriWl
mQcCJ9pRxDI3UfuprGDXgpib2FjUS5WfG02Mzw3zj8ukRnf5m3URJmnROWDydlLgU4UcPjY/blvJ
Ic+iU8/TN8oUXSDQbkIbz+Fc6H3il2m3RBfIdRWFiqIUiNsmKsqKi3CcRWkIbImLeGFkXM1Tsecz
id6ZYqwQO93HfM1DmZ1y9a1FcYHCNUwuoigzq11aFGIhLkCJJyD4C9h2ynyJStufsy2w/DHs0Nfs
iTVb+0YAjOXh+xxNTjR9KuzMM4V5zOTksMe83mHLuTlCE8FF1nKis/uZWt+ua5cka2AJ+txoJQAX
6K8AmZsPscTKDWcKaMm7Cw7OALEdW5c3HuQF6D7bvrmn8ZYeFlw6U+PnUxgD5D8WxFTCvHbH3GfJ
iOrP0L7iENGRVMxMma8GaJ+FpZSpJkdpeEaBr3KAmGJjisqLd4/wa/OvweEmfGeRUwS94uqhjOeH
4naNSRBvFo2M9kXgE6PwL8pYVhaMz8nwD4q3U2rvLdbFDoG6ISDrMguh7P97kWGlpohDpJedTECT
BoOcy9XDaCx/I1ad4M3SUgKimz6CbGDo08C197qPkixb73N1gGfiIuLMPNeAVYfslpLAW2dgHYFB
Kq07SIElaBu9piIsmkBAr3swENr1ewYKvcKv2ujiaGzSPNE29ZJ5+0PORp0No+qz/9BG25pccJD1
Gm4DAnMFP8ssPIYkDH+k02opICxflyH8JrrKcvzHN96Hs5HShQArEbt8sHKtO/t+8rfZ0t5wIVRR
GAL3XsmJI7bC+6sKyft/t109DoSuNzYx62tX6FF9A/1NSkKWmXLumBInWkhdPKcZBhLjAuo2Ffif
pocz8KHfH+4zBw6Msa+zr548tSjgIS3hk8gT3+nRDTDxGAd7E8iZDWaKd/gew7FeLi2y2oP2stt7
sxYC7iJQWi5U2vrxWgztZP/m5m+PgVpJHUGIcnFzTuHD44h9y9JYdrdkll2fCHMcxzqPObbBFMbj
Kvh6Cq2h5//C+PO6n07LejNM8MK9VWh94AZcRpsVSvhJWMs5fEGJhZily8J7vb8B8LDCdZxfxnU0
DQgn/MAlYGLlQa53RCrxyrMEHYTytD1bUTLh/FnuejABFcsXdAXpE/73tXXhZH19YOAw2/o+bTNf
F0fTUFicX78pBnIG3aUx8XXg1QV/2Jk0Xtzl/OkcGLd4yFflgT/BGrxY+eYTY9lFIMj+B8vFv/5Z
zKYALjZf3ryg6I0vGSXNCfObJo5ThdReQZVUKPh+ngHddLOQaW8sCFlD5rFaWYENkivLvGkddfjC
O8Rk2aNU8UiOOk0caKdE93fR7bSN7QcTfWbItFVSHc2eTGwcdxIMGOIOb3HRnMTowGCJ8N4Ha9HR
4riN8fd7QP8YkC0vLJAT5jVbDFbJd0zaDZcl3DkhjdKjM1BscdpmAN9pDTtqCttBUD8D36/jOWLX
XKHGXsqYHmjhIP5Qb6yWQ3IykbHzmKQT6tIdEj1kp57GDS7MheLlGfiPQukjQbJrWJWBBEPsm4DY
j8a84FrM1lyUtxSTsAy7HurtKuMnj/nc8jCYN66e/lLEoHNHaVP8+dE9mCwKhLaaD91X8DLe5At1
uAUHJminawwkzPKBn9EXlAKnehv/zVoxfC6sCDAJ7PZrddvLh21jqVdC6tm/rfr194cj6UiJPfnj
TlXLo2sc2SK29iPKJ8A2apBLbS36biQgdtvtBzV/Kj78z1/dCz5ID1Tr+FfWpsmSTfaajTEqzeKn
ytZeFwkgSFF+kIYKd+z8cE+uRgfyQ0UizeNmAEr8w1+iUk4ip5mGc8OUxLGGgH4NKfxkJtNwKszO
6DLgK/EsBFU6zpF2IM7KJQWi4y9HpsJylOYD6dyhbks/X2eSqqSkYZGAa2G/yQXw6dO7wdgwZKHl
5P57YH9fP/gFmrGPdh7CWH+THlC8qsoOo64BV0USgDcFm+pZsbtuqcs1D5KtovOuM0k1PHWvvaqB
nnIBZ02duppII8vosqfj7e8pf7zpACpCIp6zuQwf4ZRbOD/YE/b3aVV4Z0bXSRYryVQNU5T6onVf
EBGXrujQ7jCv9/AOpd32G5WXJCpZDJAX4t7p3+q4k+NIMBpbuK2CiQk7HNRbEnMaCAJaGqTnd4NP
5dooYzY5d89xC/BaElPca7GGe3sU5HtbpcBQ5qDasgxsCHgNAicn2MzkPs4CpfZLzFBJ8puBa0L+
fBv5aoPP2+dTLle+0Zjl0kBV9wVC1gIoC/7QbTxgPlWSU4zuBbMq8DACHY0aHJrBdjeTeyYA+CE6
Sgor4XKehH3Crc87ZhSUHncd2QpvownsxSiY5mI8+H/KYsQdx2tN2HW8tMYW5RqBCiw80xVvaSOK
yHQEmQyfpOHGDlcWeE3Mvgk8PKEmItyzmxeas9jyD8SEbZErhTyJmUp2UkvJ+93GVmJlnnhshF3N
L148kOUidpJD2Am2Mju9ZhkofUhHOJ3jS9+UBcUYAqPT/7hiUoYlYwHv7Gp4mUcyeHE67utk9mG0
jbVn4HYW+CJqAwmXgVIPtCx9fOpEYWYwQ5GPOnjC7nwzdIP3p5L9Ixu/CW88N9XROqZk3H4Oqp+G
yz8SzFGSBxVoKB6yBChvPh7BsxOhAcydPopD6Ayh1IzvNDxbz/VFCmeI1PwPXLFHBUNLXe6eCDHj
6fVCL2WGwVva2/ImtdWRtl5SGfbeQO97b09fbv+E13YmZ3MAj1ZKt8/ApO8IoYNJrykOXcTOUH4=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
