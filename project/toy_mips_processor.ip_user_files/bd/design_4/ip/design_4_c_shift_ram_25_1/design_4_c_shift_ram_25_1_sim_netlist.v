// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_25_1 -prefix
//               design_4_c_shift_ram_25_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_25_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
UmdJcfWLMgW0Kp9FFjBmZSVLrsIik8bQN1iqgQD9zVpV7iglB/JQUAuS1aOn7U5xH1W9D54a3Ef3
u9b2vEKENSOSzSe8aimYzU4fBx8fE7Zz75OBwc61XQ2kDGZuWBnd2jKVwOniD3AR0s0wDz1Wlm8J
6sJ+JBW2ykvPjJf6xTmWc2Mqask7KQ7wtoFD2+5obJeG6V3WXdBjTvaFFsck2beSZFiAsWln9y4t
CWaJDLeJOcnsg3XKj68nzCtt4kuxmqksNo/4AlqrR95a5J0BK29R4VggD+34exSkobgACYatO0j/
nGduCo9jZPr7n6+oLqP3bTmnBrYTIiQueLLZiqHNvaJS5AlFwQxwpMch+OWfGOTXPGhy8llq8nhR
todm47EiCmd6ecrQrIrwytBBEbZHNJFAIt2QhmrrxiGkxaCCXTBK+Dx+DNenq1+AG+2511ewTvcN
tR1Fw054yS5RNJic2i5Bs2ku848Y12cJSmkkIVIl1AH/P+CkA+SKnM8oQ/Kul3/qkJRax2XJYc58
4ypRQJxiBG5grsE9d1iCii74OQufYHg6j9bDl0APt/Jnjc/JklkO38peuK1Kg62ytRA/E4YBThSP
+LoG8CApyAco0vYhWU0DRZcZ3ya1cTD9qFtLPUjT8KiV8EyPAvC8zZAzPH/Bevj0coPorbTXgnra
Kp/vo9ZCISx3mhAPFqq9rarTOvKTjGtSjFD1p5vhvHKRN+YF/kwBt9LJ3I0uwQmVbOeQ8FwoyWyJ
goeo1upiN9FAr+2EyDOYr51Pl8HLSAOQFAxvB+GRdUVevMkQYezZ3lZG+37lyANUb1ZIa97vVif2
aB92HoTg3G+t1lyhJSunxPsjOVNrkZ2M3WaG7horHBysXRxJkcCdfBcxIyS2hRpLXz6IOmLF2sXq
n5Xpl5hwOo5tHRt0I51kqqT+hQK6ogcndm4Ed+2A3eT5B5x958LGWsnxdfLXr4Fz9CqsJAlHRp32
SsprPeUUPCzzhv+e6azUSO00oDtmzWo5dhm3d9FfZrjq2fdCPCO7fMSyVnqfzxaGCaIwtoi6w2UU
7mtetx0DFQ6saBKJ97YAq8lblQOAtBPkQyAAWiXU7SxUjZVNOikCXtirbpLXLrJN/tP84A0GoTp6
Og36TQ/2r/odYVafkrljgaBYwbctQuziEXFLc0FfaVSmzFDgIqYt7xqWHtKlnNGYSdT5gpnHVYSj
UqGA3cJSEUkp7kz3Tr1j2misB4Zkd3nGQD889W5J/w4LednO6h0c/cGcLmq+dcjHOJ98SAvW3jyI
OjuEitmi7+Uf4cjzICv143vaDwdK8jyoJDXoITgHTFZJ9ne81aR2p+rMNUtkUH2LU0EcgSAeb29e
qsuFLxn1FvO2hSjZ64CIOJV2d5Xa8GWrN9KatynBBMSEPpxsrIfxMtjYR2Tni7e0BKVz29SjNc7O
qL6qRlfYTLP8KhNejOFmGNTzsDPWA0HHVnglOM74lkfR2MAgSVMJYkvam6JhH5sck0M0jtoMuklG
maOyZ+Px8eIWzZjShswwdDjEOWTXe+zSVilTKkH3A7bl2dW+uLsewSL4iDXeBv70lH3VezoIJ6ei
u8dXcX7U/bGAKJ4ORB+GgsRQXvlvMZcR3bcQ5kvpvzcRQLatDMGs7OU1HMpu7FBS7sucPXuUvzMk
6hRxcF5NoCpAH/CFoeYiY+JZHNpL+ScDAthov9llOZb0sWdzgBECoeFLto3qim7SQihzNDSqK0gE
nxhbx1i0Rb3Qe0cZTqg0o8c+jdRXGE7tqafOYOzIdqjcUqWfA2F2tOPdc3DTmAuTcze3M3IxtUwC
Qhk8zi6YqNpNgS6uWbfb/9nB/XnFyfO0mrgx+EyQGDzL0RAi4prBDqNq0UE3KKbI9QZWgzqNLEP7
ps919kcqk6FpORjsJZxY9qEeCqbRq5hfmOLSigkq6F2itr+29tYar7H9dmKtPjGLpXDxACJtVMFX
JKuDEd5Kpxt8fyyYogKi7VaD9nnVny5aef/6dOmCbN8Nsd/12OKj2CiMepsjhD1G1+V1GhcccnBB
qG4WHctCJMnXzIRFhy/Jlwu5TiiT8t9wKwvqT5dcNkN81Qug1dhHVYjZKJfTs1iHb9uQkK+AYV+e
Jq40UENRfYlk6O0/Nj1EtkzuPcLqgthlcx43A9XaTLlXP2OWNBE/8X+ucpwE+KJ7BuYLH8jt+h1j
CPcEb5t8f+OElQMoFkJ6gGSsYxiOVXz0NGDAQ7QuDnRBBCr0hzyez9zly16X2k2Oyo+UZqdV1mMO
osaos2WwlYJBzCcriofiR6Hr3TTHPbE1mx26b4AhjssA58YPLfZ3+4GRey199RHATyH7SSihSy4j
DUbXR5rAQj7oObeONXu0GtrSY42lokupqpRNufLKslDZx2vep8FsG/0hbLpWRYuoka95llzOorUE
XsoIxZwC5/yWwyvGJwzgISQkMeywbhXAMBJfzQORMCPpqUtyIUD+2XT/M01zvJjQOgwaZoWl6eqv
OzequfptRy6kIGxA6sAwznNKs23arRJ2F0krbLQoNn4jaDpeSiLVWhnRCWDd4WjrsHnTsbRXDNVY
XVtK2pXVEXNYF0S+vkED0L+5ZG62krQ7fhCUZ9spXVXpvPWY68GH6ZEQutB8Q+DUiJ2cUDKzhuQo
GWUPSFwhi76iPm3tTjqojuZmV/jBEEjfgKqsgwMgGivGvylQWlQpg52qkh/ebP2Rm9R7NI0YUqA4
3QsjYRgWct0WLy67vgWGqe3ZpDj7aVCFe6GDeqTArWLvb02YyJk3nRVQK8R0gDtWrWZ+f96k4kqZ
MgrZV/wNREniwEkmFkpfDKWJUyoS2/UtB3azeDOKxSAzkShtGRLYx/uS8BjxE2KwtpNUbr4dvB+p
4asKlcncE9V6HHM1Txg7gVOEt9zsxIqvxBC3+PTjDfmMn5FN82QZHmtWNsFfWbLXOevMfbJjPc7J
Y5oxl+QC7xBhO5mxed8De5b3X9Mv5VHTrb03RySMqhfaJMl5eMtJre96LYSNXeiKzMSsDYMsaTXB
JKyIggxwTy8PhcaaPpnrQI2ONe5VB4i2FivbaEA8TCjubaRkbMmhG4+Kg+6+xT1EVLqJTWE0E48G
oW5XaEOUeAwsZnxNW9Wp2iHmU4Z+oUdzVtEVly8QAQNZDzv1Iaxi2/cIGCXYsC2Quvvq0lL/QZci
9WFrakC5jNWzRh7giMh3EhwMcWDBwP41F8bhZ929ZtOEqDstNZxoqXh7x5tseofBVOqbDAXS+8XC
4nx1MeT9rp9JezRx+b+KgakogzoWluW96wl96lfObUcf8pKad6Diry0BfGErX2PibKONBBNKP5F4
ggC3NDi+YFINaOYT9MDwA6xwfGT20NWymODx3ygsF6JcnIp1oKi3TT9aIkNicP6+R1KhkMnMNhZF
NGdBjhGlDVCAavq/5QBg4wwqBW0BM+ipyMK9rlH6QjmK9vw6C8XPr5MaIGduZAN9UGI3ep7/XU3D
vasSIHB6CGa1eWUWb78k+IRfpjBdNd0fX/Ib2YsV6J3LZJe9YnL+aC2e+dYMT57L5Yt8S1sg0ji0
ExHtQNK8Xt00UqG0azgauCq/dO3wxbuqX3MAkL6UNIMMkjxVbw+Icq7X4zJ9NIwj0/WBro0bh9SU
eodc7iPE2H3oHVY8N0BCMEOqPXmWQYfrFcOFw9I6UotZq81kN3K2aGV07EL9WMdJDypCxZRUYXaj
gHKXD5QfIVm7e0ITrZDzE/KAdvHHOtX96oZkKLcOn0mVjmc7htr8XLHuNt2S+/HY22IJHr1PDYjX
Un6BSZOv6u4Nr86y+MpmQ0gC77rJ6MFW238xnvCaUkiKjvkZ3TD0CTiq3xMbjQ/7/egTy+E0YSLB
BQsLXlY36IISL3U9klhCCM1J2INKPaKR/FZmBWz6GAggc8oWPVUWSsgtd7m9v/NGrXRFl/WmQHcC
XIzOfAoOBM7sETjmsvnXacrRdkvDlQZkV2MpOCbbHGh+Ap4GN8dIZpUNVgpbS7xUJvN+Xx3uuzFK
pMeQJNNP1Cc6rUWsRikXEqyHncNmJ+P7CS3hpp1IEgvpkYkmIeGrfDOg2SSjcpQif4PjdYfxtjXC
+Hr06o1naM9O0iEj0/3MZ+2cO4yRdksDUZfFI850Is6MnM/kNhh00n2x35w4JRrxM7fxBLr6+hXi
kuFZTqwp4EWczdF/jLfFM4K5MvVCMORhFFltTuD9QW8LRWNANvCoOYZama97lfYnszWYx1n/EDlP
8dXzgoGQdmRYjbhDlUbvtI2VxJf1NaZhkbbDjgne2kfwijtwyiCsO4q4ACETzxcnIBM25Gj06u5G
CuVnRmrrwUiq8Vb5tvKifcA5oa4PB/QTskjcCFvCLrSgJbH7ZXLie13SNdOkC1H+P1W7miLFB8KR
Cp5Jw4XAZTJgdtVP56PRP66EQcLw++AxxcBqlg+HXZIaP8FdkhFNgQ//RTlz+f28WZs0xI2qDAfo
3YEdaYatyt+/sz5A9+OjZeKK9+kwoPkzya0EWh06buBt6M4p/qG//iy6D1EW8GTKyO2NWZAZglRY
2+pUZ1sA/SQImO1izZ+LlAdr13XYpBVeMQiVMkhWhTD8A+lO0OxrshMYM1ehTAX/iaZ5S+i3gnPk
d2K+2cJBP6qMyOubdgqkG7Cby5LfHbSZhzdAccQuKxvaYG4f7basnj/yioBhilQkbDj5064KJEUe
aPIjZQj59Kd7TYuXi/Ml3syK9eavZBlhptyXF8iBRE5PH1srEy8PEIUB00covisrcaRWHvzIlkAI
EiAyOOi2NSxoVZW4TOpJtSzDriJctyZuxcUCEaQ2NNw/GQ3kuBbZJpR/5mDAxO7g24/Gq0/hGwsZ
ds9q5tC7It4C6Ap96DyHSJDyxBnaJVLFf5ZRG0SBG/8N3KM86E8uw9C+tqp9KBfRfn6Jr6HmReVd
UK5nXToS24mblgCD45YsZ7FsE7BcMF1+zf7tmAxm2RP0k7JceN+QN57zQnZ4BzcF2sSLDvObnffM
AJ00ULGHXgO+ujn4BWoIFbNRP/mwO54rCt1NKKiJqA2NtzbducscHPrVk+4XaiDrprU1WOoZ8bcS
TDgAgzSXCc6zKD+xbr9kcqFaB5C4rZll9F7VgEM2DlVBOt64oYh1Suk04O/zvcmcNaWN+III8hxL
hFxdg4hxWd4ACBNqdqUtaQBYiG1cdbpLZt6125WOpIM5rWjkSRxYtDV9OV2yY+Pn20y5iprHETW8
9yIx7rkFUwQz6UfEMCuCz8mDTVq9T6ZXe5bgJetvFv4NDSb7VOgmEdIlT03xD1eRg4MObV++zJ0v
YpYfsopMVDV2AeyWLFLIDXGlYIUl3nua6MpU1MT7Kt3FqEsmq+LkyFqD4P7f+GXuQ5KV3oNP/Rse
AWIUGZtuc+njzZ0JZt6PMPfsXl52sziagdbalys5xrCUziINTIaI1Prfes2pAsOLBwnMYi2PYJ9H
TSgSLnFnPfN/lV3sOju7D6SjluzMiRybZ7V89MyxN0/aieCTT3K6LPcZW9KhkabAwDQDfdBrOUA=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
