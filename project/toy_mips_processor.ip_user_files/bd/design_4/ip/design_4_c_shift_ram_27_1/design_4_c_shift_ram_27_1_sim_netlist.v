// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_27_1 -prefix
//               design_4_c_shift_ram_27_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_27_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_27_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_27_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_27_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
jabnG0TJVfLXBcrsG4Z9Vx8eNa3/E8CpKMhyJBJvo1/7h6WV0LlcR/kd1zs71U0pQeAhqV8G9c3z
eh7IVTrkKIT7Pw4PblUw9tBmTNWqeWmFjfQUPxRoEViskdvYNQV1CWj2/8woEDGOpyxEwpETCxCs
sGWl6fzZYOu2TxyLcFRkLIhXjZQD0Aq2YNe7Oz/Ty6m/9pza+jusNDYw8kMxfcTQej2vLrz26THw
kWIg77qnuxJtz7b1tmCInWx1v+0mw/ZPA3Ud1ulsw/IDJMjGB2yCQKfaIi4bgONAaD7t16dH3P/2
LALib5kYe5/QCKZ31holYMD4y8T9XijUpu3S5nkI4oe4Hq80li9dI2Z3K4SA9bns6xM1uydaR1wn
fMWGebBBkyYgHeIKtX3xuckjw+8jwcTh1SMQVOQZpXi8W5PJzaEuPVpP7MwQwfhSq6pXsDuEBcVJ
nA8XLfjBD1IIe7h3HhTnQwGvmvYa15tg4Fi5uRli8YX97pi6HVS4h6y7/93+81YHrXqsB85DZv0c
KDizNfz5WR01xXyyZeU8iw8K7o0kloaWtiDWj7huro83DWOUngfOb1X9I8DFxyHHgruE3rP1a9N9
d9whvNx07F0GAvhnxIyiFfMleLL68ZmDJZfqixfL2og53IsmSvx+rb1UGz9ihqh9SiqHNOErxYTL
2QWURed8ijL57+xBM3l/eyASNc2k0TFl80RJc4mDsVUpPQsX+f4r0oSazjb07hXfDxTU1zaUCTu4
W9zwVZ3KVsTlSuJqp+6oyg27GM/43nZkJOGu6qXMVqhKEW/dTg3e8BswZACJDZDwWE751v/V/Hlw
nIkJ6yOwFqeJbC4OvCBNazt9zMJ45LEcSDZ0QoLue6J/lUh36FTdKK772X/pHWMjtB4WD4Jt4E/L
qcDecu6b9XIUrlmPruRPoem21CWLMdJmWGl3k0g6lDM5Jy52aXtlXLcZdv117lM2ig3TY/S8mDen
OwVqojUcYA1tHQVmrh/3ewFrc5NqbjOOqV6ZeR+l5k5uBbzfDlyGLJII9AzmGtfxpzY39EvkvlWf
tClU/KIFmEIwKI8A0tfr2+Jl7aNKxKJtZE0mWnDXHkrMF6fAhPxjnc97qirdKpOiimL5opx7Ehax
tOUqeJ9yiMwNPEaGwe+gd7Yy7vt9uWsHpUE4nHYH8E8E/KoduyDoK04XsTRPgz6wd+7o/5hg6bdl
DSJRujG/E/cF6WPK31LcpusCL4u3QdvmipYCP5W+/5WlD+Z6qqddYMj4yy77OfouXLtjLFrDMnwa
z1bLv6HxHQ2zukZ1DXYVh5OxUoyeG9TJvJmASju136ZE7yzEkcrZuLoLg9EaXvQU9/TxSMxHNx/L
26OuaKg7+iRQz/fzy3b73zdpslb8K8iib6gXymljyL54ogu5WK2mLM64J/Idg4CGSOGQsus9cyJg
HCfjum+z5ZtGUIgb0XVOv5I4vK5J3DrF7GqjnAWUy9CkRvtzMXJCsKJc6yZoBt/nqhpbnWsaRynq
jNqxzWnr1x/xxZlJJ+GGl6J1M3iur5NhvM7CNyryFtZQle0EiX98szTPmei0zQp9rNtvbeE6+hKt
oN3nU6y/EEdBNFJcyxhxcyStIYwriDBch06P0ARnrZlRxgHEgPop16+Id9Vtr/6RINoKZGRFelz+
uKBKmelPqVnMiagGaoCQOh2mVDkZCB77lmhkXcawi2WoJ9H8uMPqIYq4hQujv/4ev7BUo+NH6aab
8Q+gbWkKkD7mQGKh+Ez2VoNPXifQZtKNWoIznq6y07UvuP9j4rRWuJBWWTAlUvf7Xivs2oEVTtwx
4IZUTRc5YCT8GfZMP9RfVogYAfQRGeDS4XvJJStrxuky1MPVEdo/kNPNP/h6L4u8yN9HDjrccFQH
8Ts7YTkngaMJnKSWPnxAL5xeFwRrMiejiOOiOZtZFxkCKGPP0b0BqoOIXejm/mlEXNBlFQ4uO6y5
fAEDq0NHezTCedGx9qiS393N0aBCAEMSh194WXKjfDrDzoyrtSSPEDc6ouByyo9Xmt7bhDRtlCtw
UtpHmu1y/CP2L23vwZy15nBu9Td2fB5d9LBwj08jvFlkhRGqXwkSbHOjpw+DnPM+fV4yKLIclUry
Ege4XLloHPN1iMcejBRa7btthr9yEXd1Z8Rq0rMR8fIqpqPsC+1ceoXlWt+LTs91OZMoNXlrSP0F
D/YY+a0AwaIXoE+Dk63btiaHGFaxv0fV1B3zqyNEWzs2/+LJ1g0dg/FL83kgMcR7v2NUcN/3Zuaj
A7rM3ALBqkrdSeajA/2dzvxEHmuwhfEeR79mcKgO3jpLwRnn/3GEjTpB8dE55VZXCuM2+XuQejMe
LNlOxJstQufvx5DDEgvyUPMh+uJU4AiIbsqcI50KLpSDWbJDJ9/L1taNNMwdcHV3jqDzi2p8cdWm
E5OGbCJgHCoRfo1hdQubdGIVGeB2Q9z4Ey75IE0i0LTxGZpXptqWOzhWjSSFqPXZ1GWyVGXZ5BCF
hWHHxe9yGIEV2AFBliGnS3KltkbKqCueUoiYwBxTEt2CY+uh+hWpDjNN5w6a57C0XGH2/ZIiaQLv
W7/9mi4HJcXrYFhdNoEfWJA3ULdYQgt/Y/MZogIeeCe/dZnDKCShV7z2z9iAEV8GQLgjaiISPdrY
LZ6IY0psAgXZRYwgKb3kQ2b/MotFEsIjM/9l6tqGcHN47KxCmGhlGbgawVblcP/6we+BaZD6VZVu
xkPLmkF4GzXbMGJNrpLaL+tqGDuGICZbiDNu+gG3FpGr43crNNqCK5G/rDUVR4j5d5WcI+CxkCiq
0iCb61HWpg7FRMZt1Xv/YE1QWJzX5Xsx4Ox7D+EtTgCDhwtuhtLz9Rysd35u2w0AJun/jzc51crl
QruLVwmmKsSR/eYS98nztdxWrSuJzaUMn+z0gpNhWKIepe+F25poTJeTvToCqh9ifxvSweoVf0ZN
x0WJL9ZyRb8ouUJrnzE2FQygAvvGDK0G3gfjfem9Gbe1089Lomj3VGP1r4ZXPkgsR1mx+7Wlyi8T
DbXQ2WJKeMkDAp9QGr8VKlUEgaJYhu43B7U9muLnAIoXrOi8HoQgvHppahLwUJY9QyvGw80HOpgU
JFFCllyLWAWQHoSDabmIDIJNkmQU/m4scHf76WtutUoZy9OYojiQRIQ9PxgUIyElGsID227ApUqM
a2lq7O4KtzunhnC9RiT6njQqRZWmhbzpMlJCFOBkN0PLAA3FKSA4x8Eo8iOA4U8yJzqO8ubIKGUC
DfGGdeIf/xqK8l1SunJ6hEm52p0ZAhB/ggMjOEidcah/KK+XZkFK4N8VNBLN9PCjZVINEH3u5ZMK
ktgIjmxKfMTH919JAmdICdRikWqCWPZQxKd/+RnnJ//1cAsEY0xYzomuR8vPC7I9UN0T3I5qvyiL
djZ3NW+oiNKZnbhypDHBU50DC3zI2ukopCUl101NR/pNgZ6owWaWPFf1KNE8GqiOIoFOktIDcBkU
SU7MIQeMViGzVDifR+gevWb+GiLe9x0/xaAx6WDRJyYVA0eCrjzvT0E6W96lx/W3y2hkaF/VxQOE
Lc3P74J9JHBB1ixtpVOZd3LvJkJcbRIAr34oVTyM4DTPHswu6+l3rGuNqM45Q4DOWlHs4DLlKteJ
TJw/3pKHmnP57SXzVgr8J9EjlZ3ZgSOYusrpXxtnsn2olOuxk52IvkUZliyGjIKwrKmhVryG6d5B
yT+QIz99wP72qJ27mZ1XjgJ61ZnebSf2jNKb4df7WA5E6TfsOxkA1vwc73QYPlksX7HDsutXYayZ
jWD+AfmOM1G/YoBgvwbRWevbTDfHl87cL+p094Cci3OSTqj0HT3teCG4ByRcT+13NkMhGj+gaIDg
Oq8MosvaIN0JGhCcwcBZMCRxaZEOI83edcJKAjOD6Zd8wQduV+84RkDDyKz4TeAt+C+SYhFD/46s
OhzQPyZ6cbhNHMARurEnUd6VWVeXkHCK4jcAaYGf+e+TfbftWqFdBEGlVlPyURS+h/HOsUsrWHEY
mA8eoV54T2aukGQ1NsryCb/P0hjuYZK71r4SOcilVhNl7qJaNkX7eSYMGBS3ItSI1969ak0QaO34
FT5EJ3rFAH/LjA1hj9rJJ079nvvF3o2gev8NTPvCCBHP0GBWzURRJ7CKHcgxhLviVMnveD4hR/ri
JXVEUh864wDvQBCGesx3+MMD/VvT9CaX536ySHPAxwB0ul843TlFLHZGg873mPqOQ8GQXx+Kw9uz
c2VKJhO5Cg1BxJS9uQpfC5FgWH6dW5JCC6+sSKjUZjEtuziMr5AdXhDUQ4UrXI8LkOVOgHooGzFa
a0EhsJxvkd+WpcRCGqYja8Hwos4Q3L7mMgBUP0Pf8acQqbtf/OUsiZo2qFlyt3iL3tf9Qgyk4ij0
5nm79ZilmYt2yb5LJyswDkV7m+SRabTUUHg3CFe6UJ7pqX10cPgN5/TGkTmBS3RftEJ5BlhZEvLK
M9Y9iRD0Y2s2x/04Ox94/XAo0ayfakYFsX4SFxaPaegaduGD7x5Bmd+hJdX19qyzuUZzbkpVcvP0
LU1amHYljJdAvAf3EgHtTLL5CM8Df0dJgBOImiwW6ACIIcG7+xVfqLmjYE2n/qBL07v5OhWPIh0Z
3WD6v7g+N38Gf9x+LJksBTBIRlQOdpKBTL/35R6R1q8D+RIY9dX7L7cfYicU/2nOMYRoPtPA2nma
lSlSIvFZs455hgbzDqLcib5YaBVu1C8Wikw4jaUaTcg8UwbR1phK3hdoUjioQbQZgUTLGn/q3cw1
14bXMtR4UDKCK4yJMuEMVvY1LKIbgBfLmDH1AcvKUSgkNZ8Ay5ZJaED2iludrEjz6lWv30JS0itW
KlohpSbMLh+HJSzRy8EQcE9SX54aTT1JCdBpa7ZMM1KrjyTLUIVRSsMerpKVZriaLuv9lPgC/TlL
snuGgYXhzFFF3fOyK6tuVK+3z1QC4YjvEx4FmnP/I25DwFRk1Pp2zfFQKTYmfXvVKIoT55cCWCnd
S0IkTnZZK+S+0p8qC863CYdrbdtKVW6VlQSxPdnz1z7z2Ahas+0K26Q1eCVYZmEtEP8+64nHlc2r
ietkjwG8acvJYLSc19ydlW648gHpGriaTS3toLqVaf5ZEE9eEmcPVrFHW1+FsQ9h0E7KXSlJmlrc
Fr5jW4hMAOFeecbB/Sf9OZgU81AQjC3xWqDQP9AAEB/fVEPr/LG6qEhGTdygfFROHpwiUT4nl1rd
Qwb0XnSRvyg7baw2duWsSHHvAGJS+3+d62bi73eM2aMVEB7OHjf249zPNKuqvTaHnm41Igk7ilsx
FGemecUS4PMX6aJB0aiXTBDRgRrPPSq7c19DE91aQz8FDP8YLyAAcrJ7DHRBigmAQz7gDgnJcGAX
rh7iZexJZr/2LOAksgUfF+SDcfz9YZMztG9NZX4efnO48hKTuBueYw6TbHapwNC+y2KHpEL47QYk
Kd7LSxpdqDlqOgtok8lPx7nwSTjYeceFjvlO/N5Vp4N+tHyJJpuHdRLPH3SHFmrYCJ0L3KFDP7op
AobBifMMP+SkBdaVe2g/lyD7fxGJJpSUa6nZOgqdvu4sZiAmIZ+nDtORFcO4RVhS/MbqhzMyhHJM
7MDcCiKYmGhI92fzmSxRTqXT+ZKkyN9F9I0XVJFNPzzTFDkmvu9W5nMAu8zjDZEFz7yoG3YcKrFB
9XZaSI9YhlOcU0CsmD2LzQ+ON0VwWIeuoAK4Zp0AYkbp24+77gzMGsBxJ08qQG/4IMstZSn4IPTO
IVEfepKb1nDaWqb7M0BMIAvekoNjjdjaXmcjWy70AZiAQO9gfnkkLrFqpMIB4KYZDWL9NsqQbwjL
bOIb5sffQOW7WAbr6M2eESeXEyZBUGyV0mJxvglihPwZpLDENJapB9pZ0UXKwnl96MVTA4KdK/ew
pLgzp6iwZ7ZUGIgM8aNwJP7UYVL9rZKy7ukH1t5VmRl4O8WjFPb9SN3ntMZJbz8KowLLWY5ufMdD
cY8KbfciJ9RtHLpPcOmqOzFH1sQ61Z77+tbvcJ5VXrvZtQyg10wvMoq0XyvEAoiejsAS7vIUVzsk
5yD/l+C3bkwdTX+yzbdf+dn3R6LHIZaBHu/n3sqsUBr0Phu8OAb2WO/hlK7afpgRZH3+WKIqSwZe
Yii1j9pUYeFVbmWGEGWffsDWJZRxXeXiwZPqao4/kIVJlTNwrSyLjLq1laChhxgsC60K8ktFXhP2
PURw5kNJObUFP6z7n6huBTRADDLiU1uVrtTD3KuJiqHPIr0wzjVTlcmeGmJZi3JyYlDyfj2sq3x6
z4eS7SR0L3xaKn5/ksXf5Pr9KavB+7GZrbN+3Q4CgiSQJnLSB4G92q1PV3XaA+0FOwaQ0BWqAx+q
4+oeK1ebOqWxG+7YfqKhvCXAhKYy8J+GYIaJgXFriAnInxDQmnAow0J05FaBiJU771u880JweipW
UvQvucYzAFg9Xu9QUKtx9aKE2dO4M6CMGbeN/jqNMLE6pdHsj95vUl/d/insxFmACDav6+kdDIMM
w43U2x2y0iiuM05AQnKQDZTAssz8pmizM66GMchoqV0MjLS38rkeN0S0vOCN6zbomd4sJRx7a1XC
X6JufheOHQhA/1i5yrkEm9c1NIje4LMXURvL1QLv6DsoV4Ueegr1TZ7CbYPD1DsUE0tiwBpkYZXv
fmkyW+/j5NMGgRa+XZkpIKMhRY4oZicnvIZ4R4VTB28NT1oJIav0vaJi60LLIz6t3lmuMc8JZMu2
uV1yMBOOpC69N1reJQVHLcbsZDX2ZLryyPIvlVVY6f/8HJGCQLi7Ca+wpte9fCyZWyeMSWgPktZs
lvsW+kclI4cVzu+H5cIkmodwN98Uttj/Um7Ohobye8SI9d9TI3PNuLI4S35W0sA/ZbHnP1BqsTyN
yitEgJMao68qXOABePOGok5ROvEy84kj7SYmje7DcZ/OlvokxdtiLQ6q2vhvYxaXBGlFmzCztX6j
1FZcoaKp7fWMThQpjnOwPMLVo2HCH4CWeQqbPbpz4gpjb1IV72PUqg2BkrWOaT8okitAjLaPAjJi
EH9xEwt+jk/g24ZMJUxhZthtUwgr0GcLaHw/xUOj7yVp8T2YlVlNZAbIVFCpDQbl7/xoJ7pKbhWn
rvMOM4cAa/UwqUw0byNzz11lv/G3MC3KuhE5oAZAjsOyOTNlbiYlTM/wxuEwc998O0qmU/CLC5SI
xb6QD92klgYD6TjgorPphuloK21/JirRt7mPDH0gptttKNMbVwEKrYrNydGx9CxaN0l8DfhRJX/J
ESclZlnFqaAGrv+OpG8IS485gOhY/3g96ZRiYJc8xwXH+BHKqBlxrv0HgGNyegZXl5R+rSlryh6A
ZtkBYRZkrjfqm/AGm2Ru4NyMBZXvZet3MJk/PE25/e1QyIEdJ2FTAyvoIC9ruuhsNzQot0Z5sn9J
8fYYyA9JoB6CXc/x/kBculnt/WJjLS0ZZ/UUawC9TRN3FD9kaHqhlPUMgHxuJQdVY6rbmbl86sD5
aigRxoae0aFV3gm/QaozL7WxjjnzHFJf3KnNwnO2GHHC0ymcECz3ceOzT0RNC/EjrmJxiTstKcWA
yS8gjUfthgYIQQNc54bzvy94ucgt4+m6ioYObV4/W5xsl9xQi96I3OQJW8jIQMtLRh941o9UKlC2
iron4x8OWNrke/QjNUL5X2nkEbUfisABgxZvqfoo1rT4p5nZcHUpanVUb/qsDHb8OzNnGIxxxb78
EcVCsYc+D/GRQKocaBA7UkZFz/tbfaqKCRlu4xZyH4D2JCXvN4rznvun2j/Ph7hr/mHOUQKuRCDP
+I3QfD23LufbFnI4k15l2Id/3gzGQwitE8DAoBTo9h7ljlPpX2NCxsbCJoFwNRModJ3J77zfqN7l
4QjyayeTTpRJbJe36/wzOFSS96P0zBkK2hyEqu7cxGzgL9eeW0ngjCWQEa0f0yoyXDW8i1xtlIip
kO0Vr1JiNAAqnnATbQuXURfg5cg/Y8xfxyAje/TqR3sswM4GGfKIORLDbjkzT0+hIxoZLAvhxeC2
lb5d0LxY00dWSFMOZb8bdmuDRpUZ/P0aEG2UWY/QtuvkcJnYb6WFrnh8H1cc9cYxnSAU3Anr4GLj
T+61VDHztK74b9EPY1v97wEfXYdqakgTg7KdX4kbIc+DkkEFPTwvzkq0A/ySKlLbaOjhrj4AMqyT
auhl7H+uWrOb3/FVWpWEafw0E3Dcg/hTuG/OxkNkqp6EdiBqLgAUuMaeBvkTlgVKnkAb8pyn47Vg
6bHJEDHfgeAs9Fzs0MPCrGil/ADwR76txJZjOCMQK7gNI5EMMq8jXC6lwQ1gqGPHvIip6B2/SLkc
tOanR96YXxz2PRrdy2tBW1kmrID47IN8PQdjJBb2UHol3CvFgYItjafutjnhYFWNd8bQxKGsgnJ4
E23qpGrA7Ar+vOe4UNNE2HFEQ0qfHUuJ/NY+3lKfTB99/Boa+lykY5xqa5C3r7nW+Zk0+vJ09VBr
llmcwCc42UbFRtA5ydxoedV7KesmwiT/bt/gUCLQ8aQxELfzdQWvMmzHsbw0rT/xQkhKwnuuhd3p
/w6X+ZqXSL3MHLPwvCm19qIgAAESDsZW7VY6m/x0CNucL/DsSmQq9XFOhmy1SxdYgck/nbtMNHwU
jx/nnmLxaL9SADBBoTqRRuCP/rsbqg+D32AOJ07W3vBFaXmdv0D+8UFuualC68T87nWgo5A2Fxal
7kVXzf8EBCExoz151LL8toJgPyqt9WQ+zhlqpJGivtv3LPmJeAV/vxqeSy0VNAwNna4aHEVG8hJ4
8QHQRLuqdvcgG88F9ICHZHP0OltGOvxBuKq51zmMXBrZZ57mzpYk1egyUnF5g55Ih61zg+ahKHio
A5OCrvwzSdsCQoI/7pKFfGLF0QuEjqDTciHKB/7HQm5PGWKQzFuMR6cocPT9uIqKvdc8LTq09XkX
qA2v++UQ3126Ly2Q5m5zknZsI8LpPjR+ckL/BpE5uhStY96V+skclWyT3ZXn+OUTWI4Ihn2nbPg+
hD3Bwsau5Jg64zRhHjret9HhINOq/jn5MFHokWX+yaE+oo5dxwVSyFt86ERb9flDsQ5wgqT/9YAX
Xp4eYed+Ynk7lvPRegSOqZ2hRI/I/J4qD3Nxo5CZ2piNUdF7bWk0tgNS2B5asGBGfKMUmPNbb4lh
FK4075fUjeSYgJapzZD1PsLmk1JmrEL/8U2yUpOGIL7CCp7uOdZ0mCmld7HX69RZt553AzvVKznx
S5K9GpPBBu7G0+9lQ6/5sUOFa4sgeCFxWIc6hO4s5WJL/CRKTI5H+hBEVP5Guwtri6sQvkMjxXeH
kj2KawYIiWaBLi9XI/ck2v+dCzkZW+cK5rY1adNCM1OvzOg2zBsD1WPB4q248mKvkWv5NRb+HWJy
byG2PqVjzrs9XPlvORrME/t8pZ7PNI/demHC/xramaMhRvqbH5xBOuBEqKyXXXQFBPwhK4eMhusq
q89FbzVD+f0G9pdpw6pXKPgnYpVZlPZezw+4SsA3kiScwuu/lv8cJMp2I9Q0DXgY/wANR+gbmsgp
6CgMbLZiq1yhcKLxWzZHU2GVozHDprmErr3DXhuN/VdkMnXLPzf8CVyP46qyw4RohFu3ZTFgwcIO
Ren2+YV3YEZh/AzoUkjQOqV6tRicTXuqbCezQvpd//QkQLAO3RLPyTNYNZNv+OTGOkmVW6WZPJPU
SAz08LdAhmxsLiHJjCGP2XnZebS6Pze40TE657P4CbimA3GL0szsKsjvOE/bkNVAxI4uEM5Cy2f4
DyAo91PXe7gQJxXyjRGxIcuASrD0xyLZm1127SFvrRSWVZyPF/x6SZ7Jr8rweGE1fv7gQmfpDG3I
fpFnChrL1rb3qsaJAMLhsKS3+pZEZkaOa3QnBMtEs708sJQvuQAWwWIraHAwusBhPX1AlZggisj3
R12i1AmRRo1E4mzjIYcnXNGTjqO+1VcmFH8CZMZEiIUvvU6fAAlPa2s+Ut9mrwWHEDZRkL8LXAwY
YeEv/tos0flAYP9OHr4PG3bez9lKWchshNKecfcBGMU2uTHxD9tQ7h6FBn9+gDaXzHM4/nkNU0FU
VJWm3XNPa8Mysto55bQBmDFFXU3tWkGMIXKcXvPuW9WVz2ROpQHW+/lXg1WcW9naETrCkLVvKGsY
u+DYl0gG8p+aM1ezCPYsyUnYhzmRnWIJnzfQUSa4aGcZGFCcsgZcysz6tvhF+t02SJgMzCsM3vCM
Lene3RuZOlRF9F46Y08qfP6hCbwxSKVOvteUs0JabUkwylOXqvX6eNI5iZR8b0kBLj2FehdkrF0f
5ftzPwHmwwkYkerH5DnK2IWGibE33m9x4frYbmlSvOl9I4Tj4YL0kOHw2/auKSFMgZ74fiJl0scY
USNL55KvqN7JudzlUDGDIKh5M1ZKPLXhZrwQ9Uz0Ynjm5FuWxen+SpwFP4aIeugF9fv5PsLKDvc+
YbrUxSjJXrqRNWfgWQKK7yISwmddYoRWg13Pi8JJoLmETR0i4JeB6KEyczdwEj5Pj4SsYDkU6GpN
JkgzKehsZJYthuOMnALyxnbHvWNRSx6NkzCq2RnSf+nS6MvpjVO9ZMe5+UacqnHC5QDHhHy2RPyu
Fgc1XDWXIq+8pujHM82x2iSxfhw4O2IcWwnvoRCSR3O7qE/p6mDSCGnuJr1nGJAYcLq5Ii887bOk
dd3u/t2TjsLWvZC11uWefahkcH4YtiGPb3dVpsLLwrtaVyiPaRjg9ySM3rzzRtAC6eA9V2kSt2bI
K37P6CFxUP4i7CzX3oPBscElazqT0rM37VAklJT3Gb8idgW9m+cEBOL1Qf99O4iUhyLqDLwHpWgA
3RbT/BApKvWljeMomhdNyjx1bX8c+3UL2C3sQH+U71aa4HRKofI/k1ldF6s/wgfbexBGQZDeRSgd
2in1vszoinlPFYjF+UKa3B6U+/zqcc9sdnli6+ehjfnucAL6SO8O8YUHGl/lZnnv4f/krizW78oS
9vTj828ErmVgv90udv4qirRbAlwBu8rWYG1/NPCg7l+tCxvfzF4e2J5kUKzp0qh3gSw0NITf27pW
572XdPB3xgon44HhH+u+r0P+kpBvJfc682Lcw8MpLVLJGArOHVJGYEbuxZ09Sc2BfnudUO2YEj1b
mNQ+X6Qu7NNRKoZWFY/K2hpzOQv30aJ76vEBqHi+NFzPwZih7FJ8BBkqFBUDtxyXYfXxExptmpjk
+6YV9qhbT4q7EB4StKVnXmQli9QpXP/muK/EMdRVxV7funrlxLjuixna0eOB4LHOCHCjlM/IOAE6
Kq9Qkcs6K8lrVlUFlHkivJTg2J+lbzbqyN5+3DJA/+BO/fOx82cC+pInhAwRKBClS0ZDD0ATyAQS
Huel08NWRhrIDWbnoE5BbKpsIDt1iMZt1nS3XRrSLqYSvX2VH/XSyzgDAJ+gRUOsSLk4tmt6Rg6M
uB7fvFvVI/hSFHm3bnWfdo3za2yMke9qcWFDt2LUv0ihf1qvUhMn2GaaYvVLHgf/LBNOBOarbqSk
FFhW8QmNFzDLJMKYObKwwDWa7gDiqioBmPX5iV2M8Y1XkOyA5PN7aTwW9Zp4jSdPpLzyyCa+6BEi
HTzLl7qr52PtX+dRpH0AvxE3WCcJsMikGWKZmGIcm3kH3hyT5Dk16fgO/7pGUNJIB1w3AAhrBiLr
g4yPZNDtXat5ntW8f14MkIkVn0EdtErWiWGtktmc1vX/UCm52q6pdBYUCLLp/VUsJzvPQxGBF5lf
VXLeI2OA90xpqHnQ00ZYI3oOH8ptJpp7uhCpEZVpKGaEANm/dqQ7Fvfo9rEIm3kviZi+DzXo8gE7
Pthn2/igbf8W8l0muIcNRp4yzk/irmB2ThNJ4BY/934Ra8s08GIPKg8QqDjOvYoKgEbc8fZn1fQS
22lFgbmgPDwipf6ehRqWwIkYOiRog0AV+w+tBQZlo2a5ivFYx/HRUMqsUAaUXcT0qajyK1MKw7Qn
3JfP1cojb8VjB1cGv7qgoiz3F3scPo1tWrHzkdG+OTUcerwTlIUnZ7BN91eVTnOUF9m3tR49w1t7
y0gLzd0AR1WA1PNrSCAcvmeXRdCz1H+VS3fii4zGWBvPmZtaoU5Ajms/RaI2xtRxuMqDM0tolEaF
BgM0Q//aq4jZO46nvFLNfsGaaymh78zH1f1j1Q13AiMuUH8JQKiw9s6oxjKffNKN2b27+aATtY1L
1at7jVNslVERyMSQiSPld0FBjVppJAy48Ry+Nvit2Rle9XR02DmWMznrJTAaw7Y50037H9LLfIsn
O21cs7v54GZpJHe3ptxIWz3tUDydJ8i5n6FHe1zvyvcOGhR7AmiLpekJE3+DjZ517ved0SJMQ/a8
6Cfhf9XNG78lRSMgPlaDjAvoDbLPlJd9+3RdT0S4CieCjaQfrVKmzSzhWRHmbaAGDQThei97KhkL
LOwFqn2zlJlymitKw0HiJsVlxZpnBY26diZwZ/D5v4gQDq/Tk8KKHgBt9knVil5evyrKaCKE2alW
dB5ilhu1cmeGbw31ndpzCJAOKx2ZEglavF1yeKXSsKJ7fRoxUNdOrqj4kZiUX5seD2Ha
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
