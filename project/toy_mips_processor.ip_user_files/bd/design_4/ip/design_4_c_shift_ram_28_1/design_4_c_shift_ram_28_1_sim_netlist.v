// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:59:51 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_28_1 -prefix
//               design_4_c_shift_ram_28_1_ design_3_c_shift_ram_12_0_sim_netlist.v
// Design      : design_3_c_shift_ram_12_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_28_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mEL3Zs2sBWwq70h+LUnZNQceciepEbrWHUq4x8NxuQ+R/Xo0qtAnNBdTgK0YVw4cfjk3HOeBPbTc
Ucr1RFYKuLGiegSPB4xT8O/tg00dD1T4MXFADWllCy5Kck+jfn5iyiGYNVhwwTzYVWefQlsgrrvu
d3da33N6Pa/p1O9VzL0QTdZsE2uhr+qG6Ztz4QO2+/9yrcx2BkKkMHwfR43FfFvDk8m7EMPYnSwV
FTIdjGaP96U197zOch3ex68ttlCjoYusJUqDKpGfAqxv63ogq5k02yXYGEXVVTSOfUW3+XSVkLsn
RIO6md+xL8AYcmsGNsayUUkb0mHYHteqHioJNw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NUxscx4eYr+gVYGdkcesDcqbOiD+CSwPwnxLDU5WRjj1FNLP+bzBs+17nuDkEHWISuMz4nvIeWSo
bL2oqzak4iG2OpiAZNtsA3pa/aDbVHguoNqozSWdM5jZ4qrzTkl8qvlfpvI0bEcbpjdGi1RjGN8k
gQkrNfdm7V2qUC2Zex0FaHSM2iNAzOoe9M2YejPTqngZn6iQ+t/LUytGpn9up1z4W7xX8E/CWahP
YoUigcVROQdHjscmxZH5WnjSoUczc4tT7/Q+fPLYcmccgiIHxZZzKrFPmOnI15nkyMVFzpwIcgzO
zhF7AxpCslLjG0GbE7kbcoOGOtOUMUr0F/38TQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4848)
`pragma protect data_block
ziE6tn43HWNgTe1RfD5ITEC9kLhFzG15lPhrXYKP4YycPP4LDZY5vf/xJ8BqJPp5D2UKauSrx8lF
mKSgnEoHsX0f9XS3+eR80/VcZCecghMxpXfHQMQLWnJU4lpN6nZu9Ldgu7uBNYhd95ghF1kPzh20
m2YVDVfI2xUSRHcC8EcA//ACWMrWdFMnAf7U9LZpNBpkdFj4l8fk+P2up6KXReSKQFT78Wpvmm1Q
8Kwq0W/X/VA7ZblGNxui9lMWzlt7mNly5l3PR+TNkqHw5WOF7yBcKmuYtcENn9ldU9pKu6WQWQ+d
tNhNqbszM9ecdAtaepNf4qA3bUmD0QDJswfpgxYEUdcVwDdgpOW8jlxsAnZOQfIrW77z2hXjHCdf
CF9DnlDvWdYFkdKHDnbI7Jl6joZ9iHKHRvs6pnGxVCxUD0qaDhzjSG9XMqO9vmrXb2ufIC04hh3y
oYVyObCCM73AhpZdKYV4OothdDs7U02i77xStr45Y2UqBHHDXR7b4OKuXCXdZIc+F4A41wvjJxzI
8TzqGy/uSZIvjN1D2YYn0lDCZMMWvwJXoEdBKmHF5wp5dSksyBNuqLierOrlYy9Hg2/VeVMzlvD3
3opA9HAaNO5eHKrBIibzjbPmsGWTm1kmFgzWRZWf3dbztWqv+pIEhcVIxMexnNiGWRmoB2LswLis
i6nHd2oKD3RX6gapXhgUug5cgZ/x6ziZSMLKwTdDxIKs/lYWhgKM4sV01SiXWyExwj7u2Ae4ML/I
tZXlmlq+dDp/T3Vx0aV2+ZbOhLHDbLwoCuvKj3OcKeHNtLq/Cy3lmKlIpP8bhSuYmsyvXSY3uB3B
irwBRmP5w11u2yXLAnyjdfBhu5ZmslG6K8WVE6f7pwcUas+ZY4h+R4lXL5jBNUCj0PZUHlvCVt5d
bTroCvtPZsXr5WwQ+sL2CG/soTpxhF5ZpoZzL4WcVgGpJgxRrzvHaRvCwfueKHZ6hAOxpi8S37Hv
kKn3fCZRBH629FoMkwN8hN//rOLMsbCcN+BtFp5/pgXfq9ZfM0Fvo65gAVBzqHUzGhA4IXC/Z+DK
Xl30veMrmNsWPePbWkMPz16j9hG0qcAJaxuRJOeRgsVo19m67diXg6NzCOpA6qvev835q+8D2DCL
OZ2n5N+Ggd9j3h+HpiHG+nK+Ufe254fhTr1Oz00jFmPQQAMBYNyjD+sZT+rDUs1Ddtyy2puAoxq3
mdbcRdQWLa1WwURfukUGtLOLs63NkqE4B4hALSsmRUN4BIuLHNlwtDhxanDY9Llm7vqvVQchCEzv
/z6ZDN2GBHJzE/jWp+EVq4D/hrXhI5VyF4L0CLblHGGSUX0BQlvXShd7TQckgaTy7zxJb0Pj7Qz1
8GV+Url3C1cjkT5UUmZ0QA8wg6entwYr2N7Xu1At3EZ+fump0ciFo7B0to0d42ErYOvJMel1+geX
1Mn0RSeqeCbvPjPbblnMOrI5+YjmXWWwGOeu8w7biwcF0tDvz5uIsHp9CCAhyiCLNTd1QjR0UIoq
navYAvxpSz0QkpdJ8FyadDIlIXjcZdmKTOVS/nmKDqtW1rfgmcb2EdZCKnQTmfNMK3vexX5IaXa8
gerOUXHRQbez/cDpeQLHE3eVbfBVW4DodSgRQnKQ5kqoPpU7OTKfpUnAgE0M90pN/fvVgcASG1z9
FNeTh24dbSDZkoQKhbVTWzVGufgDQGe0qlZBttNqxy3wzE7ie/OKkZYlKhRbbv1XmzaqvIWrOpCe
mD2Zab+WrDEorvto0AzHmua9TcdFTUUlcGO4a34shwr/Ztmx/RvyWyehI9EelIX1/4HZn/6qtdaP
/uKk+HqRqY2jyAOfhUOAa04z2eDe36DIjaQNlzLTh2al8c9Z7N1bUrI1LWKN76UVq7VgCGhl080k
QEB/mHfS+p+NiJqqiP4Jiq/oNlfcfBYMf/Ug5ESwmbXoEiLXXK6BfjlPID37TWRKAfdDQOM4ohrA
vo45FF4kzY+DCyfndEJpdt4e2WIqDcGI+SVbqoDObRWzu4YPBPVosUyRLF/eySB/hBJAfdnbzdEi
Q9bfXm4A7PS4FVH1LTJmg9NvtPNRtu09fIwjdPLCC06tj14mWbFRS15YAWR1XWwI8M3rZvC4KHY2
umX5gn8x+fPbLBa52xHTgnLkHck82j/Gbbj1dn9Gsg4v3wgrfWiPKk1LLBCZnrLf5ShCK1D02+Cg
YgpqDzqdcpVLxLRlBP10lCrtcyhWms9lLSOBTfq3roQHf8xOx/fkZTB316aEqpRPG09LR+LH/MMI
Y1r//0jtzFIgymyR3AmHmAYU/VnR8UU/Ca0MAIgFvxYa4U9nGETYxsmoQVyJX0o/Js3P7mzsojtO
ss/gL8ZHjdat5VZI86Xm3rFmRtuiSOGXQoSc8MBezNtCHGn8sv7VmCC7OoET7a85v8FSaViDRZmp
Ai+DBLApf+iwopEs7Wr5HiDcaaI3k4l5x9rFyCCPgJzP+TfF2BCVU4n0b7VZbMTiYCpceLMA5U5W
+mvYkart4Jmz8KNfk04qdzda3szbrakinndqFiM66P/gOFJcswr+83lKeYWoexltNY0dXBoMH8Y0
K2cnkKfti3eTI24GF8gmlBpxbo1oxK6OY2wIi7cquB/68ARCiKysrzKVvPw91fSCSZ28Yb1Kh0BK
5cBKFdcBIeHOnrE6xcDUjemEyFZ2+Genfgja3/ZGiEdfIksr4RsSlIlwDsy/V/8jJJOHVcVfpFmt
62XRk/usNXq/0byoS88MFPIQqC7flzI1GdV2fTL7miZRbdDCU4V2UehYSzgyX6M36Pdl80Mh0zbs
ZZEdGkqiNIDM5djBOXoYdHXOKh3m3o0wgeILRVZ9d+pilqt2LgHUtG3iuAQ7APkRBqxM0ZvwZzHC
kR7IgSLfjJjCSAihb6iYRBmSyMndfIl+3jgVxeDKVg3M7sCBgl9HuDyGp0v3I47mPQQuFP/S4Kwu
7kXJDMv/gW+Zc1BeFQEVdmoDqhhPGFL5LBUm9w8flrARSsL1yGT0+HVGxW0AF3BwmM5zDSpdeXnD
gTidsX41RKLKBiLBZIYmzimVwgxLqi321BQtepdHd4GpmDntwNrRWPcRcPBo1xtEDfpqRNTCVV5E
jqu2CpRV//y0KJvtlqUd5bV9T7qJNpQYVzE6hwZY1/yFcQnWbys46kHNH9/HjW5h/UCaCaOMljgj
ypf9cuFzr0mBU3aKbymYWWseDL3ZRsrwWwyyriT4+d1E/LebSyzIWv2i9lV58ZjQnukptg+1llOf
+zvSTYF2GAqHE/FLU0TyMrPjKyMQzgtOufYfIc5+B8/e0xgp8BN+VJxURgB0c48B1gnwDF0GjrwY
bRlHOA0WWthjCq9eK2gL2cjB1ZHNyOIgpwCa3WwruroqhbEKARRREN/OT/e+0bEqcc4x31ogGiAO
9FxSzZY0czLPv2C8k4/sdKdf9Mv0tfRfDfPIWLcVvVjvFu/GDzZzTBdaVYk1j2Da4qcL1fWgGZ7V
WqxtHxB+9R/LCXYvoBnNxQuDeiEH3YuLNla6q2pB62FsVjpL1JvhumeyxMzSVyj+bSuxHVTB7jNi
ujU0g/mw5k9fRHDiWs/RVSBUNVVZcIe9n9riuWVBP8P+OvmmHdigGKRONCEZ2HYbIWYiF379XgZR
9TmyWxxOxgsDR0GmR39QR+mmpn/26u7l/jsCLjBCEX7dq5PQH4cUbTq0Mr839OyJHz6HJ/GoCEN8
qsJv15c/VMwcvUZELMH1wL6C7mCkiRXNNNTUedIm4m1/BT704zmhofIA4er7PPknfFEYoFepdDOG
Mxl2nTsCQIags8x4nNdZ0oQb9kyf9zPyPeYl2UKoyfW+Ls7CN8pamAMCchm62jIpKWHKUTbm7aVX
1TtuDuE3XZhk2oKLzI8VyAn0g5zfI+cqHtutag+xliQKZr6o7wRzGMgDQIAlGYbtysMYyF4bJT0f
voJ+HsLE5lpntBTArkAishyLETPM12oG1t0Oy4l4SaIiF88b753YJmycD4ptKqlGP/o7OueNWM2N
qYzSSTpkS0IlurLXfZAFHRDzdfCpAhnth75Zz0Caqj470d8DUwd6WbjgLwAgnn8MdKi8YWRCfOoo
Ki3Jx2+vq7bf3D1rDG7RtgoKGe/fT1b+twmUBHh13coJAHnssFZCktGIYo5O+f8nt87LUm5Fccgx
f4TrzFY26zJPQnGX70B1AekmF2TKWtmlX4V7TlW22sdmXOZx7FsTexNwPPHJM7ByQZ4heGDvt/hP
SqPbyUrgBbhhkV4IOl2fm/p3KSvyQXrcoxCrLOJCqmefnn6WXv0IGGPAM/b9tqLnEQmU9Pw0YoBK
2UlZjnogdfjfIG4a/RukFhXsws97GBrwfnmWFqe8H30ztfNlOAkHAMa2toWVSWlo+fCfMv5mu3gZ
YNmndK6nCx/28Ks7zHWN39CZoDtz4ac/usSi/aFu9KkdvRtRPsawxhTNyOfEMxrG65N0AlkSCNIb
EdXsGAU5jYdBM+jKeBKmu6hF8hVvHG62FRwu4N8bujn8lHt7tZp+L2uRHpDd4EtgMNklk/jId4ak
BBIsmmurjhr0iraa9JMBPghBnNXtmiPK5pNY+gJGCCLZSDpy0XvqIk9lGr3xv1sxPOdE516V1rgZ
k1MK0/jD1eztAs7K9g99OeuXEqMGwindem5Z799IHUhI7sWryvZgXzTXnEjuIHa2dlHl5iTO4IwZ
qfnPLEr67LKY6kKSU1T+Nl5FZcAO02qch40m2snx/Huob9gYFcyNZ8CDm1NQeWb1UXdl6LXBDH1g
50q2S/pg02d4K8/krz6U13DQYd0D9rpmnGNjHgvoYvD64ZB9dkjfSqnhaZgf9kXdvkiZB3eWwd7d
7G4OaCHEbbScQa15IRmq18Zk3KBXo/qpKdv4cGhZN8ODuzSlngy/BO/Q5Rwjtf94fDW8KdE6zgpH
/nP3DGkd7YfANMlpIIg7i2RV4f377F23a2GGak2pB7kH6ogKKZnjDnTJGdmaDvtPcEzmgUCc88Zq
SP9l5HuRy6w17XztINwmV0SRisBVOK3xtd/3c6rSaFYBoDCifk+LDkSqymMgY4M9G9HY/I9LpOA7
IfqFgy+kXyCFedHXtvs6s0urz/k1xxrAZQlFqiPWNClDsikYizRnkeHRf6TTxJyq+cVKSxw30K7d
LwHnwmFXSn8wsPEp6M45XYCjrztFf8LNASFuYxf1lPaAeDgoEVEBiiJeIXIuGNi+Nn4VHX7NA/Ux
hJepfqn6TbehHGeQ/Uz/yiQBzKJ9WSZn4vrpysyUAmpPW4rmB3Oxn9ZcdPm5OJWQT5peHV2mMfEw
1wdj+FAhSYbWyFxpl19nt9NhZC7BUSHWeMm9niplA1DNG5U4Mjt+cOfmN21+9w7C6gD+lsVNWee3
YczzSIJYtdRlouKPZ1y7xo3uHqWkziI2UT/6qiF0sY5EEjDb+HTv5CxkK3iQhJkwLEXFfMWfRbja
zrz/Nd/C0RO90BIKLxWCBp8UFGscCdmRU4UNwfRJRPp8TfJQBegecdBnIZAFuzSFr7PMhOzdiN95
wzJLqubjOhBN4GZUqE/TsPg/e8X1bYzH/TeaNQ0XlCdlEeYdC7nJ+Dt2h42JgFPH0tska+S7UioO
gSq7kIj2xNHjf9bdn4JKu54S5hqZfgl5PH0qfjJ0ZQ3ZTyP/R2fcQjq2+nQ5jMvoMJNDZEBfdavY
v6j+neHbGN0vLknqp2FafobZceSE78JMaa9Js0fNm9H/MGNIGo7H3wfuWmLnss438T2VnfGKcfsy
Io+dy0IWOe5ZFTgwQrmUjOoI8qqpevuHLUk3do59c4k+iLI2ksAu+whOFnyljL4au1PjYzGOWqFU
BvppdzAUlwkMdys3H6ROUlgud0X8ctB000XKTaiOhXt0Xn/nnMQlj7S2Nx25sdsWS2p4LlN9+ZY1
jAE0ZtZJ4rmoBdxE2bG4WypKDsejrVh5Z3Py7blMOjtZbv3DhofDuAwff+Hg2q/9vMIiTyilFuME
5qCWNXhUfB9JOtyjDJK8RFJFUVrNaO+C+89acUJr/hT1YZJsA1VIZh8xzinRwrS/HtDQu55k0Kyh
XZrcjCqOBvgUYjDYQSF0Yn2E8eMdNCT0rf94QT+rZUIkABl5We9kCa5aYJXhGTZ0zQrxQ2dZL8l6
vk+P3ZdPKvgLT1PkSTIF+Jh8Qqr9tuRjVDV17aHFuba3lPsXbiRW4RW7lpu07rDo3hN78ax2b2+0
mgTtVmCXWYiAlQA6NQm3LPFa67urnXS4W4iOL/dIoGPkla+vFmRelJeP1sdOeX24xLkS2bG4eFpT
no+rWoD5WM1Z14zT8LHAA3nZgbUFY4TBBjQbPFQrcu1pxR8bZWULABGsAkuoNgG0hCNYefxVpPSQ
ylRc2O6/SEjAJv1UD13GyRTTW5Th6ysUJLMaDM0AwAj4zbKPuT9Sb5YKCFeRbuNeS5VR80DW+707
1mz0
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
