-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:59:51 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_28_1 -prefix
--               design_4_c_shift_ram_28_1_ design_3_c_shift_ram_12_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_12_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
kUUqDeYk9Y2B9N1jVaHGhWR6OsL2SRZ8juKeIBX8zdZZ/KkYEwKz4m/SILJ3ZSybpuj7b7vYoRQy
wJVpgXiFvkz2J+9mWCdjy7duqjnJdZp9hPW71erfNae3uGorx4fZW26mCum1FvjuBLMhwUkve+7l
CPqnytB88l2VXCQbUoNrUALI/dStvUEep+sX8BqTO2NhsSiAMuJ+U1f9PIW3+WUgdJJ8k/UUUdqN
NOAGM6gSt81E38Fu+7DzPNszo4bEBf7n+Rdww6Si1eeFBhw21R/26EFgah4wVkNf/A2zmjiGnJZ+
vIqnu/nkpDDyK7jcvaJOgoQ6Yy+wQp3nFOjQJQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nSBRS4IuSoqugUyTfcBnhp03EBh9iQhkWX9JL6RZrYWbkSO6fPP7iW9qDrTIoFRno9sYx/KMYhoY
IPDC35Va8+vtcex2FGP0e+TUxUwXEAK7AeCs18B+uZkVcSmg/zpYLlajxQywjH9yTFCputLlH39h
DZUI3QtQarYRwf2W71jsvG0kduFSNefyqi8n+/L/tG2jaBskfVZhaWJB6E5BiKOlHU3d52fzfoXy
DTHhJlvwN+vmm9P5DxE5idQ9NSCUlCtOQYHnLvOM9HRhksw6qmVANRjb8CGYIty5gzHGvJNqhbU6
4UpzshdNu6wOyZou1YzalBCfomeHa5H/Il8HbA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13856)
`protect data_block
po3zMxMsh19L4f9d2OspMpAfvDRqoXw6hXtQAayAfJ8FaRXS9d/12vzkbkHwfT19IrhqO8yyvVUu
4A000u3OIWvr50EWwFfUL7bb6uW3JEkRmNW19iAVLn86z0TyGUpxLqZHBc3SfBzgek32ge3R7UPN
lm3wd0cYq3oriWd4ljMePffg2NbTzN+cOOyQMlGdYTUmwJUFinZ3EmnedQkvJ13nQWW3sSnKt1D8
fHEVsjXHlBu/LQ/XD5W4coo3b2Xjo1U2i0tznn4d6OaP8BhT9dqdvXLQBx/8tYQQSEYmxpFzhRh/
sJCvZ45NjdLq1Ja2sB7STYuo5SnqMgyd8j5CROUM00q1+d2BkJwE8YQ8h/IVykiMZNglFsrYHfdv
CXDoy0eY6oGDRYJbrr8DbVWK1KDHVhcPkxZ7OGEaGs6ILxUGFEKtWHgEdXc5jEzpDDsvrlPp0Gm6
tKFrFjUm0tP5/t830DIKYA8GsC477cvqAlNvQkUgO3wB6Z+4UzvCv75rxBWMNL+PdOWFh73Qor24
ATwwL6sNrcBMC7K57os2OYFrlx/rCx7xwwb3rV+geeYDudtv9UrDFWOEUcqFb7cbQ/yb0aNgkKFS
qrffLnWASTVvENyqrFQvRXf3+55F6/LRf91MnFo3qkNvvtP2hXghJirhXRshpKaY4K8fjeH/jG/K
pRf2J0xPM2BA8nG+1FSDOp0MiSmbUaMIIxpfV+cnELg99tX+kvcshEXilfP62+Z/EUt50H3yjQQZ
9Ff1cUuY+Yo2MHDLQ9XDzlIZZrI7n48SAPLxqJVlsRXRusj4pYTEOJoKbaqdJwdnZ14cHvUvFktG
zryHJ+HndjKEzLc+zfaLBU0Xch6ItbmJ8RAz1QsdXqiKH5B8omZbzygWw+C+2aj9zOLNVMnV3ybf
RlMHzkQlM6p+8PcRACs07xNg0pSnX+7+ZhmbsIhCN4cjo+tq7Euf03C/mML2IC7jFfNdmrOAYgv3
OgrKfAqauLtESxUMROo2w0JSq20U3cd9w2ooJddsDaBWvBpBSXfRRThUCWTQNwHgB2PaxoU2aHIj
cvYV5Vy7DOeM7MpJGpRqRVbJJxKnTGU8Dnm+y1Rc0vbIrzuAclp5D9/lpptlMyimkElrMms2cGwX
9m1UksVwVanL7XrraCl/25F+PxLDtU6TmvFifOnsAg6r8Ze87gMU1VrQIJXf/+LK6wE4Hy1dAmw3
6+Z4F17ORBjs5UHnHK3kL+ubpZaWqVeqcvcWoMXfddhB1I0hvUx7BpKNMF3iZfH5+N7ML5uoGR0t
cRt3quVFNbTKY/Iv0L6zkP0PecCn7BAh5/PQ0+qzhVh0kI+kry2Pr7AlHKRCezO6LXR3iAAaJjLX
yldrW6c+4uVXojy7W2wQJoY8yZIMsYqCy9gYST9tzx2Ag52XknS/8Gvp6fBv8ukVAg19OJggvmPc
O0oAvamf/mf1hdiHEz/w84ChYkvq0UiCQJJJa3DfF43EAm98pp2iu1l3R3gdJbcZqD7vFjB/HHZa
+bBars7TB2/R6fgg3lErle9T14RqNYsehhsJRVIqdg/HO6tig/tPKVyeYNWbM9AJQRck/DBQW4eV
MchSESO2fXWpk9gpAWvIjKjTBMjzTkCmExtgeNqDLfie1F1vyL/HGZ/N9uacPmf/mbfBwE6q40JE
jyj0rj0jWPmDS1Abn4cXrHTDfsHQQi9/DRXQO8iwb7icP4qjAX47bAWvwbiGilNqK3kuIahEruUR
yoa7H7DHDDMV+JQlO+QOUADtzmfp0aXDXkfMsKbqgqI/eWcgaOfunLHXtvZxm5cX/0iuz3a+Uzpz
44w+gthLO9mCuyF7hsIgf47RpX6SDFbTVgTjgvr15yYVSqH1rBZbY+NuSNmeRXBZErxQPdU+9vNx
8ezpw42FmCHexB+Bd1UmUzdWpHTNAPsS6Q6D8dVoyXXYJE/NDS07GiLD5noQCmB+hphvitMW2MxG
GTf4nuIgo8Y3P1/viSB7VzqHZMPheUWeNUHLrr5dspmmdOMUuKDs+/sG021CW/QVfjR89qEskMFF
Eo2Cp++A7TspYVldCZZgy/MFjK0z7oq2j76fedxI1X1UESjQ0Db/yu/1I7F8z0cjRa3KWf++Lq9Z
B26uN43y7ry/dUJIx9boj8f8kLbhXmsb9fmn+H5BkEXstfquIYizqfhi6XNA7IM49FTawLIWr+bB
Vsg4tuH+Ozzwb2M8f3vpEx/+wW+U7x/rw/EogF6b9pYjSc+N6xYIz687mDOqtMuMScpPPU+74ntU
++YHLQ3Ecw/p9Obvz4xK2Tsxxgnco7qpD01XbPnE96M6K6mF3roZgmCLdQ2e2wzJSFpZG4N8/RF7
we+5fjTUH2ruA4ykUPGDWVmDREnpvYkFftIgR35QogHwbS/bBd8MNE+lgCK58yBW9p9FBWXlXiJs
KsfD9MpJXl6kKCbJpG/EHQYVrEunfBEigPgtqdAYEBjBwsRgBaFUjbfW7ngAnuw7fdytVhYkiUAy
BVkJTZ07EOMOMvcspxnjxRgyM/D/bbEVmzi7p3aHl5vImb9Q2tAFlUPBW5MYb8iUj4wmDBCchs7B
oa33Wv7VQ+K8/9XxSNey3Cg87R+O3kLrsLzNgKyIOL8g7+/jm9VKha2t24zdTmOaneBRcLnqDw7O
vP/8tFKBkUDarAOE+dMAQrLXNyp4oP1uXrD7P4T6TsIyej1Wu5Lw62o0RJliIGJV5EaBF5BzlA43
aQsoNBZTlB0IyZcNZ5gNcp4A/fXVelZaefeWI03D9cHWCyXhJIBmNAhDMA9GCHi6CNSCNKeFs53Y
ounIl1h54VzVFUpueHdWawA8mR/hsF51xc6l3eR0cJqn21EziY9NE91TnzJ69pXpFT3/2DBRWxkw
Jy5FKa3iYYkEEeIh6wB0tOtp+x8Q3C9fm4RAZKohbOrm7A1LCiAXFKoO29LjklEJ8lFPa6Hjzb/w
4WV6cIFg+WejvLq550dhTCTEHJCa2OBvL0Adndn03XaxeVuyvcv8TfffndYpt9fgzH4pnZBOU7RW
50EsBXlep3nsO4BjgMvYS6+X6Wj6v0oZpTD6AIfoocHoqc1MemcFPth+M+IH72GHLE3QuWOGiaGc
KokdWwP36IqWxc+ZlJ/BMmlXdQMTg//BDnYQ22myBK3Uk9taew15Oty4ZanxWL08Td2/UDgelo5Z
np4j2T5e4woRJb1AVMIMx6NP7gBIlXAYLHh6/HUmbkiBtXfG7uDDocfj9aey7x8NndwRMsW6ebFR
N/M+EMwWC4suJi6c65J+WfJlX/gq5kQDBDM9Le/CmZKTjfWwFCKXK2kK6Bwb221hn59w3YmEr08i
X6XxV/+3PBwZmHQ2wjZa9yBwgO9iVpQRRveNQ7R5HWWH8jn4JYqJSBRU56KHFNAiqwnwYHHg2RvU
n3SZVqrW5yeBwF6/QzJh9v26wEO74J78wOy9Cb6cexA99LuvfXPUWhcBXoZVy/NraIXo/a+4ZGOi
QJOiXMaZh46f2PMRM5qMTJySLmW2MS/YzYRypkSHfsZJ8TK8lKUNoBz850eno2qC342AWIf7KlU9
27ZAWPjGAaFZr2DjG306Rc4pSIWW48YouUQ5Aeu0Qe6h+IA/SLitNYHkv0EZqg3WOseU0YUMYK7B
9TJVG20ovOgHr7E4CCmLHnCjeaOnce9a+LbR8FFdWOCVS4K+gC8BQ/YMx9h19PsGZ8OPXOaYoI6X
1kIKEs65mZaZXCpx/nikVQZtg+C5W9aD1htTMC8vszdfH9WPmn+5I8/RrsiJvu4UAlYXzdl3tu2Y
6IW3U+K3B8kq2xJEnIy9T45PKb6Qerm2yPzcVlYFEJ6to9BLZCQAnKRXDHawjxsK8Iuz1JRAiXG2
5/ru6vJR+uY7QbM3CMuDcPv3E+fAezdLHarjlsBBcrL+i9iRLg9n1CVmalFQi0tOv/sYoxYPbHvv
aN5mdcoULX+G7qgCZhbXCx+xOuHZUmE4d4MLS8QB+iiF5ocyXQ8rGABBrOdNGxww41IuK3ko8psg
0D4z4sk2YbcecCktakqkgpU3qDEGrxHp+yfD8HrksbUkyUXP23Y/2t2AKNiD4K3znhT7sGORiRMB
kwgIeEcLb+8zLNn6a5ZTAFilqQoo83ChiJK6UnM2t5iJd0NfsEaKP60mKsKdJRi7vyonFpLT3qQH
LkCHIIV6fFWuoYTgzqj1De2gCt7RV2MBXePoZJEJtG9NIGvvg6n6cvZpqsuLtDqok0UDTjcBlnr/
LKYT1v+2zpTLHg7Eu+dImWARpoEmmz91R2ZsLgCzSvKSc4IBm61hZ2WqpirjC7XIjGEOgzQHDLF3
hmKyJRTlkRrpiX29HcoK+JvWm6ZpdoVcl4UF4/RS6HnacXr4LWD0W9AAst86NfqGjDxc3KNRlkhw
ZfU7AIurbcd2BtKpLT07GsL5BBWV5GD1wB61mVxHy2673+DELi7OTFH4aAxhPaM/LZqikiOX7xwk
YTcT7cqiBGwvlrC5z+xKpfCB2RB0SR/4sGZHYA5+2U52MWI9CymN6mAJEd7CYU3ZHhOcueVHXd3v
AJ8zp1DmBNB2iSQrP7QMoj5HZhpT+NL/CwBNyPb+4oTHViKotqrQKT0dWWpXh5NSCCvNbwtJ1hFg
BHSEpD7ome/Jqg3p4TXzSyvUsA9j3oUTs0K/P+day7OvQ98JgzHhSFSwOjXmBKU3edIA+fW9k+5e
z1pJL8v/y4GJwDa/Sq/nyui6MBESj0NlJwq82nTeED5xhXa6caUCI91SeuBh6fpowFPu+W/bOiiT
dc4eHXfhMiW5+vF9GRoiieuyMX8iIDGajXHCO68ghDQd25brRy9xCHnu2k3Ych6ervbj+mWOit8W
abXiY5nXttfMPr82eWzcZB6PP0Quev8/YsI/RQl9D1iVl1QUlvJDIN0iXzhHOJ03dn9Rcc/HhzjC
0ZBj30g/hJdMUXCGa76hF4gWm5Mw+y8TrEDZ/k5C+yps5dZ5oYgK34q4T5/6SFF8kjiEjkJpvFag
7WbkBwfspbIXB35vqlCOe9lUe3vDBKHIzanXEHKU2NkzIbcVevhFOmxNiBr62hTy0C4Hwazq7mAp
ObkxBgksfv6G4OrH6HVPGwfxUotU8u6hssCM63LLqT9IFCxjKepVmTpRlWi2rRvh+6PCQQTLEHI2
dOoW8ROFvYk58ogOpSNbeppxfRrW/IL0ppks/zHhhRB27GsKvcwPrFMP4ABvdZyB8cU68Hmhyq3J
52vjtDsZ0AJiOQFefxRlaBiyzIGjoq+3XJw84rRC/yAIds28psUMRDnII8GqlHZDAnqP3CtpGxmU
GN8mLEgjQvYOddlVVqLNHhkmDqAd9b3a6XHfzCjPN1VkQHw63V1541SEdhiwDL0qw4819QOIuU7k
0StoOTcOzx8WX6by9tVzP0+YehZijuPMO0C566p9nznyUbVPto9tFwUxgC4k84rpixgTL9xdkZUx
CwFp1XnYj3HDpBvgcVd6vlKssOIBiW39cbNQ9opoJ2Ggbl9NTANepfxNAFAppX3kupQxVvW/O+0e
jzKxhxILoTyAaUjsDd+wFEaqmc+Hcp10OopiZlszlbMt9PlPVCjiAkgWabXOFsFN7xsxaAD1q3yf
pkxYCpI03W9JR527uZIWFj0NT4ODAKVA/av1RSBBVheNgJXDQcK1oYKC/qbkqG2efhSJMFjgbZOH
yU2Kfh011iKNe/qmDPURMb880WrqpnPgjIqHIpm+SpbZUZT4VUuTOP6OupCsrbzN0croQa6egHEr
3RsbBJq3Qft6De89YyhC6uwACTCdAWyW4SVVJ8U7LYEaSYPDk5DQECASdYyoKh922Ewz0tXD7YMR
BrGNCy5ZxckxAhYan6HR2RpFgRGNYzPsOTTs7y5ndtpRkTV5WQ3F4mXYkV4dRYiD8GS4BGV/3uxY
ylYi5Gr8vqkcz3wGXUXhNCLpJcgFpaQMLuec3wy42CfjPg6j8NJRierHlmmTglE584VMxoTM0Gm1
eDCyKOhCmybuCagV3Pfd5EZ6EwRt9Qa05vNkQWuZgwP4LACWCPfG6TkfEknp4J6XGEPKzrczYH6a
moP3lhRrPbz2JYoSnEbiqpeR0cZe1+028CTSra2b0AApExbtHEsq2q2QJGssq6vlrYJwMeP8q3H5
pz68z4V+UwNPjCUnnSsdziPvD2wG16iqb0E0bkMQf2+Os4MkDmlVZvc1EvSCG6ofC0vvwDLv7E4z
BDss133kSS55FSllM0d+jsGwumgKBa9wFUvVRuMUjL/hAxHXx6wJDkvUnwi6aAuv3e6x9qNAu9Gn
7hyqv2qSk/wYcrNO6Y/46c9I+ffCkkqfAwseSHZByDDEgrS/dNPYxnRZcw4xqblYlKlyJUh2ynEU
6v6kD57194QIyrFGkP3FXIv3tS3pmAQPd2SFt4utvfsc9gIfky32sP4hzRqr+dCxELFHFThAP/A1
0mFLy7i0aSBz10N60NOUQM3xdpRY0PSS1uN4jWneqEAkKulHbZJyV2Wc7ofurr/pHWHmb451u/07
YfjqcUV30L/M+p8gYjhbOGiZptdRz5FpXbNgJ+LC675PmQD6hif8BfGzl6aPQAng/vGqBinh6lRl
YhitzAXXhLFSe1tMS1ZZmsjXqkyLm7N6WACgK5TFkLDs7WiN6Hj2qj7oN77tANKJV+Q2QmqRqmsb
B4h1L70pcRBsc4JWC4B7/ZGwSl3hatR+NzAyFZ/vBLntW7N1H4OLxZereW7N6rt9G/mgnnPapqxL
SSaccP4sq2yU6WxCiVYPI0qrIvnVP4Kv9koA+l8Y4CtsZ5k/9elEjHj2QnaFsLXApNOUWNp1gkEA
0fR9Ps4hjPDNhHrH7JsL43/UKX9nntPSHvjGwe51TzuHDR/2ZoMmFoShbXWeomyb3R2Tz9mT/TLJ
owtzMLcqzrpFNnE0jO4ausGXHo+AtFYdUEypqs6eGjkmWFdM+DvS3emhKnh2KR2cbwOvIk/r3C5k
xg7LkhlIA/dwXKBvbbFovZtYjfbG1RoXQVGrGXgV2fkU4iHXDoR6sqTJTM+Ra0ce+40lBsUYNryP
MBoNvv8orRD/TfOmdFQH5m8IC8jXUnX5PIhvoXPlr+M7LqF+iJ1oJEx1g/NKxKeP9XHmbUuAfTOi
7FJwqV2uhmF008F8VvvW/ECoyMaBhJHCa05x3OOGGa9/L2tOotPlZCIJZEWBXQO0UWLIoafNt/xc
dkDp6eTJp89jQGj7mdHolOZYq26xP0zrS5qBdiDEMSQ7Mz2mfDd4QtMVeMPmtXlGc9iNaql3xSWV
NEXRNtB7HoW6RTeX/vVEYeHiAuUM9yBSYZb+pU/mUwvah8ElT2g8Jy+guIDFtjrlTGGOg8G9WxXf
w67tg9m1bBg5QbeCFLDvLR1cTC7vSxzrFJo0apeNUpxa7cnGxLBPKh+pG1fw2UoNbdXUxSaDmt+Z
cVp6eLv7OHGmrAQAoXCNZVNxn7mKB0mTO4jvLxihym4iHTFnnWs8N19kJf+3NmcmGvyB2Je8YFch
yybCIZY/2PddiZopUGHaCAeG/5uvRjlyXzZ9oPtslAGR5McSYgt6ftcmkPKcBtAZtPSBqJbx/AV8
TQrt86Xu7XYFIuKLhoKDckGsRe0LkSDWrfpBPylwPcjWoyeOw7zd52lp/TaxYOg9pDbRLvpPoLcw
eTgP8Z7UMKe5rmdwQOv1X1H1Ao7kghjSq51fSV7fxn/5OYiiHYppjuPdBcRn2wncjCT0lEmRU1Nq
63+NL3mw8AnEmJtyNlmE5iWSo2yIVm78cAbegezGnCso7jrptJEKuDJGcjDOGo/TYbq01OeB1ci0
3AkO+3i+8LMoAiumRR1XCldyKI9QbRysShDzhO6G+xFXCQSnXbhU+180+fE3kzon2AuB6eYWQX8H
f9kfdesoB7jEQG6m+87PVf61Xv9xlzPo2R0J1KYZNZ7oU/sFV+mMBJZ1xjyRhNjdYlVuCWRYlAm8
1zgap/6YbrLgxwe7OMZ1HwASIqS0dqohz/f+YL7ZlTiKGd4G5Cp7ZzPU/lNa59ipn0cJvVxJfNEN
Y+8pgzU+kdjGhOT2GvRoxLxswaYeIjVJ8PAZvpzbokE3+eChLX++Vg31FsLBr+d4KZ3J2t3yZS1H
hYmShaQuvMmKGKgSmFUoSoDYIz5koM1AE47Rktdxt50CynX4+GuKXEBnCvNxIA1S27WC5P4/tRiB
5RZpzlmXhFXvB3UUjIKP0FurRPbrxaBDajl3+8kKkBaa+mnXh2j2VwYme9a3MhNJZi4MVzAOaCXy
jITLQ23JkYvBhsnkT0K/IXKCYDpCeLrJSqvDa0+nqgo84C4IMDdpMSB0pNC0LNLXehxUVDcPF7H8
6YL3XniG1+3YrRaCS2FGBUvpX4MUwF7n4u318/qmolPdufk5amTW3Q8Tn5HFuo8/vufDYfmyeFA5
5ROjwyfh1AIzfp0VaZp8M3QpHSsMXGZI0WEvLR4jlR9PEHHJpTbn4pbu99SJH5l9+vGZTwut2bAG
qCh6okQcG3Q0agiN0eJaDxGGjjUykDttNA36/0B34e68ndcYEx0m3Qz7Cr2g2ly2QPCAFPj73NXO
WMZ83wzyubaZij6p5ke1BPZxBTbT+R26mKKrheiN+Aed90cx62czze9aGSDlovwXkbVlaNPA/jDT
D9E78m0odgnvzRrGln/IeOjqQ9ytS+zbuJA74cOsNvsfZceljiV7KY8F0Da8f+n8eVFyI9pNVknH
JNN5wJcG2ITQuiP/hx7HToFdu66/36SLkyp/x6rMjED9ffT4cXs2hDLWM+Hjgl1sz7Y4pwBUHaay
pzA4fwdsMvqgEYPQGP+mw1Nn8DI+LTSB0vpN9Y09D98VM9Aviy79+yPO/MHUzIAnrB9nqtkDvgd2
+wYG+PORQ2rr0xKKB4FjqAC2tjNQY8bFpRAcIaTHdebuEHN2HCi46TvdoOcR6E5Hb61+g3shq2WK
WcgqK+4noPKcnbg+kwBbA0G+WfhIdtRC+z9kwepxQH6f6X1BtITUq7uyQmoyq8eZorj3AIWFH1G+
chJlHPlJbadKFZaKyiLrDJtfQrh1DnqR/fbCk1mXwuhzth9wOuf6MvUQ4e4fT0UkEkRhqUi9IzYA
Hgi+juIlZ8jmMauuoMrjBrKhyU7QbUCl+sejmWnD5oCuqdn2bX2Ec80qiAhY9KV9oensDknm69r7
eIbsTb2HRZ8s+ARQeqxNbPPC79JGnyQtXf2u5tw/ISQNzWS0fVYjQFzJc3AN2AXj8tSHVzt5tuo5
aWgB5SUNR/Joy6V8ePEbQ57Wt+Ieti8xIQT4KjZ2SL9r4jFldxCHmVsy1OEA7+HCBsZrnRxAnP7H
pjQe48ZyjS4JJskH1ppBCN3A8BauJ/BFc9cSrBwEl8jXq5eaqiCsVJDv/T4itkDOUaPyWrLLutkv
AKP33iqSrwcWK3JKQyL0ZyMlpyJye0ARrRvKIUBLurxQySoCKRTVgZfYh8rcUH1i9ve0GXMbgF2R
bqybDSMjSUkbu+rrfNn6BP2uzorkzT3ZzeTS+6D5ygZ05zuUekNfnIdrsJlx9MoPLcYrAuxCwzHj
Ac57XUhYmylvsBiXpM28xKV5fHJd5NBCdO0y44fDAY6C+OJTKNek4ZTXs6SA0N3dGkqpQpNVLrmy
9W7gQZ3dbR+bsNitvYcaJhpvdZWfapI0YdNsIAsptig7YrdOQYyUk6F28dOm8xK7uG3Zk0zCKOL1
wwDyfg0RU7aPDPLVhzMe8m5RJB0cvhHzIA5NV8cQoM2Ii1ykVlND/lT1w3MsqSLN/3iucpEmtHvF
u/CCCD7b69SjvBSKu9fNAMvZaYQInvxn7G+JLp82OVALvI4lOnbrN70F2KfjDq272g7Kpb3QWazk
9DSrJrBfyA0zcvNer3tzxEQfjObn2xJuzLYkx1gxl6YR/CJrqCVy6hVD69eo6/WIvdMpU1o6Ffyj
qvgbLIS3GwK7vgSdoJ7vuhYgauwYArodQxqWTqU0ktg0ZYovJH3bVQY4E/SzSaEe5pRcKIUfO5DL
tECCXlK6jbDO/zz/h1Tn+KlXxZQnMLcHk28ioOHfygsZ9P4G3PBAIwSG0nbZ4HtoG3fz58LoBqnu
UocWRxjPTLdO9n0qfh6qV08pkYT7c1sbJRwZLxSs2/pIj4kMzHun1ewTkfQq+LZ4ZyEyuk1ouzGT
5t38lQ1crqnroKkqjFKxGPg4lavzI66HK6oY65hQLjlvHriBzsDOoyXhL87Jbe6w6Ff04qf77Gtf
JSk7zUZg1cTlfstFaSlInT7naXhOIkvXvELqZ0ztI8z4za4rWHGxVGWONTgvGfH7PJZ5hoIz5mUh
Rezf0zioODvVw3y/TVH5k/a6QlTg2O/qeV0VAKo6FM4Dp4xfKVrpl+jxTr4JGrnC68O5DxNoKQhA
hDsjvNWQhVpfHhJLMpWvSbCrOvlnhNUXz9YWAIMSit72BPRfsnsmgd2ujLJ58yR4Ci6noNosQ1ak
J8wNQKLjuViBukmZCbN89JxZzoRYRnY7eGVHwZq4Qt8h0vtdEe966JoC8bPFCO9lavJTySrAtxFm
K3vvZWIXqTY1Er+5TbIbqnDnniRoUEuFsMiFeYIyf5vNY5qtgC4g9z0YbWJLyo6ubgddPHxchw++
LZzkHuNfSEsfeXpvSBbTOTMgVfJHKN3j1v49+vftkhS+CDzKqjMBOY77cChECTzkDX3XYdkiGqBR
hv4MUckIKDF/al4jyL1VjmC87sNYtR+f3yQjBo/9LFqTqoS+O8fVGWzyEJXrh2jMs9JfbW+nMR1g
YRFxeGWQh9GoqKBvvW9SGTFHeGKa+Iq7pDFFlQaAhJSqz992jbxXKcgE+A64VkTYjWswW1QWecJv
7bLA6bjFarkWQMmZpuCJk1GaE718sAFrQA2YJ0iQy9r8LxWywdX5kDcDkEbHR8Q0MlZB3lDiAMIV
qoNhIbrY2CrygxXPCb92j+tB43mhWky9T1GM+VOu3ED7C2lItiqtxBtQYh85eIBvV9AbM9YD1Vig
rMNUqQ9w5CBx5L5+G4nL2zNKYGjvjBgXbhFYCBVpT9Kdxf5Fyi5cwht/MAx2ND63WpyD9xmYd/oZ
NIvaXXBDNiFE5RdFAMsD+2bvGkxPAZcxiUMajxRZy0zZ4sw0Z7760/rmE+/II/Ak6oEwSuN5lRrD
8WhvetmTynaApYDKFZD18edMycvXPshsY8xK+jsxXfSXBke23vT7zUdSi94lCcYu3CmXUNEbggr8
Q4xoUcMYuQFS6+50SW7WZMIHBU3fvl2+2AJfBRiQ9vRXrNkfoWGxor9svQ+2IWW0/wdHyRE0t+v0
xNOeC9A4sJ82+1W0H4piZg9TsWQuj0GZDaZIQVZjVl6TtirCmG+XTjcsQSgWCGpPn/M5oMZmwbNB
JXFMvpLCZKYKHdXo3uBE4XRDI2mAzMkRGMa+5+TV07zKh1LYBKN8bEMrkUv6vwwLrTZ9NIPehBGu
OW9C4+HP7gkkUopz1f+LqQ5tUHNuP0ZfeCtXczxWSDfPOMfzjmjgtXCjmV+7HNHGcq/RmzvZj9A+
9f8eS4bjtwAxKYOJW9QQ8WdVZ4GdFFirkKOr84q2LRyKO/pgbJYMAqsZGRHogC+z/Ve5pWwwjdhu
amGMte5VpXBae95VgFiIFWHmWJo3cFH8wHQYyO27ELQncZpGBaV2gd29DBe32FLdPLLsqZ5lUhg8
kI4sHCkEJqodhEx+vxujrm5ce/yYKmfH4makkkMOUSTxoqSRQtS3yJBjBHlzu5pbKEh4u3CRFHKE
qGltbk4ioDReih2+ldYgH/qVGBer0nd0eqPjEnGqV6cBvPZSdnRCoof4NQgPqwr3m6ymb6jhgZV3
63vmuKdXi8wm9cF8f9bIbcOwKsQOdxP9Ht8TbtGcrlyoyLygDcFbbpU1VCcVE32u3tecQpQ7RErI
pzH9y6c643fenJ+S5oQIJpEFJ4P26XzhUHZmkT4ZT744JOoECpLDZcv/TwnOBp7WhlzHW1cSc6fI
uGWbXfPo1YCb1G1fJ/lb8HD/EjQTtZDq299SVeW9aV+jhSlsy2Q1HBjn+jF1ob2BMQ2pP29eqwoi
e7W/3FnC6307yFC3N7HrnBuwEdwRdO76HI7lvou14RjZPW+Mbdq8OHmBi26ETXKXarIo7SGCg/8N
9MysEZGjAqYxnRnkAmkA/CUnjPSQInOR1e0SCqCxJvzepIKcxmvMjMCp10Ru0rzaQ74NA8yD4Ql8
k47289+wY+zkFzHkj3TawoJoNdv3SzS6jLrqSCGmtG8V/DItwEZi2IYbbrm+0JChpcsTLHWLvcXH
M6W6gbJkwPSwka+A7kXhSa+EbR7gjcsxVJrc4IvKTQfAxFQL2nztvreRYbS7X9h4dSNNR8KLqhwd
JrnzW9gggi20zrxF6GY/WZ/W732N6ntRfy3MF82aFP6dLrHMGCQXqHoLJmmGU4dVSVtCVP7iKNMh
DCBCrJeINanOCRHNqfZzK12274+LPsB6mNhvR3pEvdRYdzT/ORe0ZF9E87aoH72pkl0BgjKpJcV4
pSF5VSvV+iSkDt3UNmI6Z0616NpwDRK92TJ5LekedkTdTjLrguOwHkt+KGc8n378qg/zaOuKt2HZ
swZNjIZcNrHtLPFgXF0tAqJBqnvv5tiIWj3lpHCc4wMTkRacpVVvPvfcA89iDOTWOWjnLeFuVSO2
LxqJdsERjXleoF3EkGu4gMM1UxKsQcko5nECaPOqsnCO1/PT+4iB7ww/eIRBkILDMJkhPmIOewJZ
UWhckrYamxiMfWgGuqjsKdUisIb1ZmPjtJF3Bh35A8T4Ei+i9R5eL6d6Z404oDK2DHhYqGo1HB/g
D3A1z2TC9pHhOPlG9KbXNv2SBCTJegBdscZsqsPVOiS/l5wYx/2WaRO6wHNEC2MYPRnazpVW8DNX
PkXQ9h1eFq0qz3vAwwxilwWHKY0iAG0UManHpqCmHjuLwEQmts1t+IrNCIxWxf3+0KCXLxE/rZ1K
34ggVvQVCCBHLh41c2Rz/KQYpZbV4z/oU4OzY66pBuzbAtw1TbIoTzWISM1RX5uqzTbTIHldNJzU
H0SodMxWWzf6LKLA+wINWUbgEYy5uTYgpKeIvIm/pwUt1QSS0H33HZIt25HE2YLaSun+WzzZZLfF
GaW/y5GZOM+oI/IGxHt07JDvt1htMp7n78b78ZydMrZ3ZTZ47UO2T8xU8zFLWZZZ6FqmS9RMGP4G
ZAyC+hOcXNBigRwkxwLN7w1YWVHM54nqcZATMUBpireC5Uydt9u9ygn5UV+B0jA/F1oEVzDw3eZc
I2YocIgt41DssDTEdXqGJuc8+bz4hvS+EIKokF/0mrWeis+xs4XaBXU99R/eezsXX7YJXdRv3zOG
UhkziQ2ARMfZiBZcTTckw8zkSfoFafc7Mmfqbnd41UkiBC7nBQuZ3TD0lAD3k0+fZGriMjNUqciA
dMXH72y8yG1q+C7ea6EnJhja1AR2yu0Om2rKHFr/udO5ojqxPG8oGHXjZXiLRX8O1LIvFwHz//bv
0juPggK0jsBWRAyI/UACsxxIBJ6K/HJ3gry++pNgodpHQHba8xuIiz6XcyewbSztlZVgAk7IMlse
sA3DTMU9JxHPcx36qIqK605Gw1cm9JXOX4MR2Ij4hhRtQhbGhDSDdqqip+/sl/vow2KsNlYBXsIg
9vgvvegvr5ygxmgAuDCq/q1Cs8tZeDWuU4ofN+1ZfA3aVEqnbqSViJ0aDLy7d0zOFx4po8DPE2ll
hz8vKtOVNVqmpWkXtvBQ5eHyXq3ucstqUE9Pb9J1rSwrV+I/do1eAlAGiICkRHWmJaJtjb2LUf9u
cRFalVHb1pRsDLsbNjybRHs2s0GgnMY1Yl9eKZmSK1mvYuEi8TicoPBWVl7xGc36gmsXrcgDxmum
04rzyrnbQoWhfPzJpBZ7Fc3jKbTR/y7++EpjFlOHFsy4pML8C6/r99b/IkccwixOpynCZ8viQLg6
nRcYMa3VA8/Pji8LOltXOm1rIr7GKop7JSK8nb3AqkLVA3uelmGI1uFmFd51p2tqjk7vQ4X7qOho
eePzfEcI7j52AeyD/ndT9KdZ+rJ+L7/H1YdY/OFFfVD6KdUuSonrzDHAYNYgxwxlnCot9XPfl4dB
sZELid2q86jCXRlvlEr3G3DidYaZvzi9lovTsVwBt8qhrG9BTdQBFcaC5qGHTVYfaBwWlLr9H1dK
E9jnRw5YkXpZifKvQuCsr1c8FrCMw2oh4rHktLxHLR8ygiXbw+maiXygUTMK2+PNIRbQi1LxZyZT
b1r6mUF8CK7p6fkt87JvLHcvM3vaxZNFV7f1yCHcLMyvZA/MyRW/1VI+XBvb6w4stLGzlOvkssM8
C0FhZC+gDrPZGUGuLgro7+6MImXCzZzhtdNvZaqjHr3oYiKJLa6dSUGBKImG+rahnYb05/xpAofF
cz5iwtEgfMDg/XRduTpxNJwRYUsKCnbreidb5/H6wV+6/LAjUuND5Q1kktyayO/uGBrZX0mmKX45
Sb6adeow4mWvB02hYPcKSySyfUIQVgwFgICooSSpZapAprT7Cm3SPi7tJispjGj+JHDzEmD5Q+pM
yzAXejR5x8kpRDaQFD/SHgANRRTosIC4001nnGsckTlTgVEBH4AR+cPa0yY28V9bPcQw2SHABmXn
dLMZfyHW60cS0Y8qXfOnfIqF9EgOptNu2h7Ah6Oaa05/aoJRg3polJXDW9M40kiIYYJXXWsTxmCj
BZwYlg5aBSrMypTwZVn9QLsPn5sjejoZYnzhvX9qbnsQ/qF2Cbi7Kdua4fLW4I2x+6OAbUF0zx/I
sbamfNZhAoP0ie/KzeBrtzss4PIW+GQ6QSNovNLpUlcMr4fv505TH6iKR/Rk5EBBPt1zyAvrKRA8
h9fr911kBMMbFXj/+Wu3KxjGrua1gB21bw7Kl2WZJd6E+/79qX0GJDVw8Wwu6bBkUX6r/7X/VWX0
RkvwNyvZDMMi+hhL1Uo7GEI6owp1PLPdMQtdbcby0YQExWLmxWhjCPeNIm7PTtn36tTSc4dlJKDg
sSBm2Ok6H4Z3oBoP2pgp4Blq545D7LbVzkmkgoYF2kIOqD7FynaCEnfs8EMys7l2GuqvB6PXjvoJ
3leW2bd9A7gvybYrSlxyHKc4JYdy1eNkGMPdiqTMFp19b2KY1cfUWbZNeYc6tMKL0/eVBKWMedzs
qBXSeVLzNuN021FWuHjACqZkfOtQYH//daeLRC2zNaEr/L4+4SA4iuRzoCRdBy1uI/8Yp6o5Wq2j
TpAltwnO+CQmHyp2+h7uvq/PYXTVhHtspY/4rnwFa/rkXRCazTHnFWKjZs9H7pDhIu/vQxDdGq0F
HS+2A1t3uluGFiQdKDrZphi0waX4Y0qIGXzu2601FXpcMPPefvL66m1EzLNPQCgoQWiqfnXcpD9K
2yj5+dP058kkCtVXN4o3NrKT/MIewH3HPQ53eAPoB048mKesotbtEYciqio+qOQwX3tApuZZK5if
4g+L6/KPqQcY1vPBV+Bt/XmYyFpHUQ47jaKxZp3xUuQYkvDZaj07VFj4lvjy1be289ydi+3sHpGi
oIsMGtJH5Dj9YmA1+IYbhNP+1zwtkcenRiV56b63YFYW9kvZisiU5ezCHwyYfmHbw2w4a5TEkm35
khWjby04t12xLLRK5mdHez9EEjCRBGkjhDmzzkqq3GQMbon0Mz6eq5UVsR5zNqDufrcB7kGNHEa1
9ZBN4HmFVfn35UOwArYfshytNNM/0/aJVlO/6Pgt3RIWFQ9nnOeeaz/9vGUreQTSDV2lmcmgw4DY
8JMoXj3ug4nkW3G5hB55tRTjnvC0WAi7BYHuO1A64LcEbLbIZ2sXvV+bE7MQ1dEFflMVU2ci6cvM
vMb0tzpscv1jYRBhlztn9sL+LvnxT4SOPk3t6yPiuEBgmcT5SVFNBeJTb/v4AmhEcWvRCNYshnO/
5sMmudlG1czCKyGpjz/LSlHCB+4UiwtYv5v/S7olWmVEA/Oj5jvM0A7dFqkmA9ZZRubAnEwFN4I2
BSe/sZo5znTAr9MAOUReuU93jkPUwwFeNRf9sFRtmWJEI3mXCrLROZ34iIqMMFRYKiYD0H6+ubur
aObAMvluBbZYE91MoIfmN/UoJ1fiw5ah+Uem7vowULXcRWEFBwBQ527QMJB5gd4psxVCdxCW/uVj
tNIOBhRercJpLoQhKv6G6V1P3S9APFYaj3qm8oK76JSYfRspfO6t6qUgcEPAfC8McXQy4/5J4Sze
TICJZbcBSwJoYLuZEH7GwaL4YtLNe4u1fiaRbQ3W+ewcVyQa+sBDlkslBLI6Z6ZueO9braIvPUmk
XGnf830PZEn4Ojk905oPC2BTvtmYUCfivtWLZlHa8ZoRAn225ZiwXfLejSULUc3RTKC4XY2h3yOR
kAMR8b+Kcvmpa93wgo2gAUC1/Yt8RAkMS8kok0nUddt4+zhJCiHCpjQjLXULKpusmBRrJ0aJYC/p
GLCQ1D8QDZ+DjvDzBuq6icCpcJ9LCqEC6+021z1aledah8RFC7qKklscnxM+7aCJj2n7TvEDt/hc
DIJmNLikhNeVWz3OasqZ4S1bZP+bNhxgMTkhkAgMYo3hqCpI+lPZk9x9dChYpNFxj2CGadeZt0dX
PtmpR108ZkmxDtte7v/vmkyhsjtmehOcHSXXVbNgugo4mJgIMEMbfYkknvor6FhPi9qv0FOq5te7
9NpdZOBgmsbW9kmFaTDc0glFEcRcG0ruhzPDh8YxEkpEfakaa6yUSJ44NZM8QQfVX4rBUkpadOng
gfPYfQhD8khmtwZD/Hi7YDJY9tzAudwxDU0eDw9KR66b9BOdb7kbOvU2cQbdFxdLfMd6dM1efwjf
orNyrRK1xRncYJzYxfFLfvXP2tDzYEchpGxwE1v+jCpHSCKckn55IBkcLYeXSNndmZQRxy5zPb+n
/N5pb8PT5K259XjUZm4QAKe7EUd05C8OvI8r5osn0BLIY6KvO/SmqmA1J33QydHmtt6dtWf0H8F6
/mz5XAOi9HRYmRtdDRJEVsDXpCz2gifb4ydVny19S4Jxyc62QUb6G6CB45D0OKXbSSGNWJhVh/47
d6DljiUet3j3qzFpIg2C0IHBYrPbvm4c1uju0JcI8l1FxS/pEsrdXeViVGcEKAQ7TJgxw2ubBPD2
hH33HrBHChFQSWbo1dxx0bapqllH4UPNQAuTiMRtG/quQYXVtQS5kwpsVLNJRTBzx0FLiunl6Q8t
JGNw1QKoP12lBCFI+HxfjZJkUy9bspzat6bqcC3hsnyTrshdoOqsK3WuHK7nk16RY1P7BkjSc8fR
5VlYAJ1XDX604+RTWprAHSR9p9iuSoZtxq5bIAUEDZe/QzZZNJOLKake0N46D0smivln4p/u5b/2
UKG1Se7io38qZqE5GjdqV9on4LVegahKabtjvdTktAEp3HOIB0SEuJrFEMlrSXQ0k3e5CrEYJWgB
n+i/6s1HE6il4/DpaVDhrEjH2I+VBi1bZfHLfdlM0rPwEnM5xbr2tr5xI2k/j9qSpun27AqsOEVu
T/eoKucuNUzGbgpdcPceV3jf9Fpc9JM+MPOAlEVkOKt+sycH4a95pz8dXtEM6RJxRjMOueeocxUr
fdlhY332lFTOFKSycgg0oSKASjD3l7/LJGEKw2QfJqtAo/txx0w6mdT9bPA8XDQup2SPEyfkDwkZ
Gds8Xog4Qjo59AC7W2oPBq1rxEKniQ7pyEULIxPBTdEnyqFq/v7jj2x7ROtCw6IWZnFxK7xTysVw
gp/cGsl8/qSdg+8MZAoLJP4Q0Kx02M8SsC+UQzuANQ8RXJXAUnZMTH7mDbYHeUKCpaR47CbIRwRE
F9k897ogT+w84Mze/AWTSnt/5d8HV2jUGmk9BbUb3IvFsQccLbr7X5u4aIeQAm7Rvq4VUP99pqQy
V/aIYVRwtpAHg2ZGlPRxA7bYZMh9tyhhc1ipHweA91CxaHvr5oXzYHhwWPDH12mHbhi21jbCUJqS
K5LQNC/gA9cIVBZIFI0z1cIRaPcVymCzpEZIVg2k4EO1lFYRrQjrIvK7i8CfImoP/EjtSLcNO7EV
oULDqay05rPFerSrxpGY17q+y7n5FPsYKr5ssZaM7MiQkiUiTt5de8PSap92hVOzv6q1tLbNz3Q6
wgnHWmWcXVUqJqpdKScgVWJIyVyqI8zSLThBcGmiE8ewBMqoGc85zYglXiClOnsFTU6FmqCleY99
sVQ9YeYLUY/BBTERgDq0YtMxu/VUeDWDaJ69FXi/5MEgjC59pNfKKtuYfRhpYHC3hEZ9jm3PLHS2
IKWAXAyj5PYrz+hm4HinnFoTj3SeRol+oENfN/VIWymyE6cCE9ntwL7X5MEJ4ZCyF10sW4oEP04d
AC5OwZpAnTerVfeFRJSc1okNMrMO7Aemw07PoPlvThvGpoyW1H2oqBUXKivFjCQXbsG6oDYYJHnD
MZ0ACbw=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_28_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_28_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_28_1 : entity is "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_28_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_28_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_28_1;

architecture STRUCTURE of design_4_c_shift_ram_28_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_4_c_shift_ram_28_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
