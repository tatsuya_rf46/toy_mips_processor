// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_29_1 -prefix
//               design_4_c_shift_ram_29_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_29_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_29_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_29_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_29_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
9h8WiI4qeMecplqb0BrhHzf5rv0pMTAoON9xXSwDXbITTnLo/O/LbOwhdRmPeJptTgT+UEV17phU
fKpl6TtkVTppNmvgZvQOt04bJhRdP38dZLlgjqTKd6/b7t6udwr3R/9FfLA30JtVGPgS+Gkwwz5U
TGijPnipaXOzO9igrjlQuZURuko4BuBigr4ClM4UcABd71ZYrTfNKqN0yFTpgZsO4JQfzILO439q
28IAQr+66J+2f4FZ+6M748ftZ2qhQRaoCriVh5fLT46RKC1AU8eFo26sF647J6IWTVgtdqZxh/qR
wS+wKgt6pEZAfR6hEiqsNoLJNZY/sgQW65IManlslsI+97dAmSEymRDVeB0yqsX0AOBt6D2/ke6r
hWTkPtK6LtqdJlyBK5ngAq7WABUUrdtUIb8HL7EIJ1rNDLBJ9Tm7O9/xtA4RlyzTs4MKbMurKsNz
PlrmYKpYdUnz5lC/KGSBJU/sFnq4rAKJhM5CLx2qYche0uRwgXsfTe6vulV+p/W59mkY1pspq1kt
97yNBxjtzc8LisB1mI6MrM3tlqVLIYjTHzE92EnKq51PTjy38uYr9uHxWYqr4CU/+3kqW7VXZ5Wv
H5RY3HvHc8uPlNXZcyclOzUB9Sq2noH4wkbP9tkVKAgcKPdb3YFoLtneDcGPzO4+KwP/roAvO6OW
qTIJbv+4qiEVBEdhKCFc2ESYftpwLUjSxZGxXEkNr9E+PzvEBoAvcgnUhGM/8i5MRAtCfPtxdPsf
/n7ZGU/4MEzg1MFd41pj76GTVMXO1brnke6lzr6P0TU91j5KnrAoBL2Bp4dNwVKcpOSmzw6wQrT9
j1ilUY2axtvMVFS485+vYm5GofA3rWcwQTi9VJ0QIQHReloyw894o0Thp3czpLSzl2WBFQLHJSP5
HnDDN3YMvPyJikKVzZoFMc7ciovC2QykttaQ/kF8rpjwIjX4b6Ml2ewcs3jBawiQx+Dv6fVm0YiR
O6W475SQzGpNVmOWcDsCieCPWU8biJHMIPIDgj238wuEn+WoeEInIpk1fSzObPe/fOvOfMYgpIxb
55aID8+b7oYkbH9RpF2BOmnh4qfQCzc6wE702tIq9lD5em2NKLj0607z09pt4NipqP0X/L1myT8D
P5LoNz+UPgEOZhd9loi+PGZ1WyxSdYKctqfVEfQkMR49xZtvm0Q/629RNGVyxXH2swik45rv+5/4
SsRoDO6Xgall9SGm3biJSaxGYJDa4g1D0ckLyE5JOWNLvZzg8Srn5NMcZU2/9GOsQPQWJb3tInaS
EkcTYQDO0v5RAiqlYOpXqx8IDaJQ/FECEmMclHGkBnl1nJuO+FmyL5Wd6slW5Sg85MX/TLTNn1Sl
HQ2K5lmjbcEqTBC9ZcyBzmS/KGtZZzD0Wvd5CHnb8MOycAohC4+y7rgoahECk58TNZuR/FdyOB49
5YjJBlEIJN5VVuQmrD90SrJxvoDY+irUPEWytd/5mHuwCr0ysasx9GAwYv1cQIZYwAYSbVjR8O+9
1LnNPAAijmhsk5SiksAOeE72yTDdeSwcJQfx04TGfKmWm8vGlBJLtDgcHpPhaqSjllmGRmB3HcVq
vIq11eKJUMtBjUR9stDoURSz6hQhUcWhwbDguun3/GinWhIt57EN/YtyG1lvAfjoMl4XdDNRrX1S
IotUVUBtbd+lfYOvf9swD/2CzXi2UFqVWz/UkPKHirKi40vpdSEkN8DC4zy8ns+RLBTk1sjHjkOa
S46ZkZeQSs2Ui6L+IfmZdjXcPa+tTHpMdXQg/Yyh513v20vPSW4QYxFm4Qw7QSrVsKxU9KolLRrP
zpgRhUgVvOdfWhcRfp4Cq94bs+pGDESPj30losYf8p7gyS+hxfsNU8bc9bRuSSRbMr1e/oeOYIky
IGVaQavo1nqseCRl1b1COmaymwmqrAc/uIGu4v+7KKaNYLZHuEfNp37I2GDtq7W5OugDxF/iJUGU
Hc2oZ/PW3lYwZvKqnv+FZOeB6BnlKJl7EGLNsbBEPk8Y8PuCbWji3MM0zwSiDO5FjuZoYES4hTO2
a10XEY0yEyy4d4RpHE/sTUvKFiINMP2U5V9sge65R4HDHz5NPVADTSlnWj+9lIVytp1tDNRS3xeH
XR5By0F/ZXFHWSmCnNeTyWOOdKAPGzfAGlpmHcGywlQ3XT7BHxcgYxKtKvZA9glbOVu/lCO30qDV
kTMmQTu7dERS8zcSFDywem3DyQ2V+1caBta9noNtN/tZORA7gWvQMHTiu6pN0GvQXbUTOzohioNC
0gAiAcsyuf0Sv1j92vtSCMWgdD7nWlE8EAQUqH8llviJ8rHyF6gh1SFJgMf17GKQXLcb1oAvVzGk
ryYQnwnU+qYnD1N2Sjy9HkUYpEl8+UO8NMzew5i7FodXQdv9s+h701MXXjmHADFyr11w1S1c2t1R
878Ta1mxCL6GZx0OBxk7/8lPRfFrxOFrsA5zQlqwUnNygAdpiro2DLRjA1gef2c02UyIgtjJVuMD
F3V4VeXkTunyFnuVPB/olg5ChD8Vf/hF8BG5ON0BtoWvjFTKT4DmmDYrEyIXGESG7iI9pVnGGHK6
J/DVJTXqVdGImswPvx4w6MOAbrvEsDarQUTjaEs8pzXXAQbYLToO4PGCuSnOPhXg7ZTs5RNeFtq7
rRtgm212STV337nrDD9+STkbpSXpi+0L5+RCEMK4Q9keXr7ajTOilQpxQ4Kn0A8O6s0k2J/rB4+j
2A+F0ELVccUmkl07YIcoM6RLHjjQ/loA1MpI4ugTtLdTDiaKf1y87xFs0tSKlaTv3XO8KnejN3Tw
gfZO0zbjG0PXN+LrKlCZYW3UFlK4/2y40hkK6amtC3oPNUrGlprLUymh6i8obUWBbLJKa4UXP9Wm
ZFwAwLH1TFVtaFyZTt7pqKJbYOTN/6GcrKUoXIiCDNNQO1kTdHu6vuKtd5JAYAC5b1KwcZzQ0R3O
bHn8Hcw4bQYjObXZb8DeK2mwmIZbSvd8VBFu/K+T4mrIqYYoYg0+f3v7qeLZr2wekRiitRns0RWN
yc9WkyqWO6fP/rky/c9157xo20zwGP6CyuhhSbUwfQ5ayJ9YLZOp6msc7NXSx42uNt2rEqULNDbt
nvgV8sslRhXW7mnvTED0s5Y2XLY4Cq6P0Fsix1SKDE1zlEAHCyU4VLPXRpU6f+aGFqXo5mbQ31bV
Y25nuYN641eO1TLl+bR029jjwgDtR6/sgUGKvEh0NUIAjdPHr0VSRK9S0eL8iFQ1HWgIEojrlkwW
pOKgLSUVOBvXEqpkWAc0qhcgWHflnLB5C3/ydnrHcyfb+eBiGBB68+kDT/fnq0EqvznGYsE2eEXc
efCuZ9J6qjpoXh4KR0imDIPWS+xYzaVwdKTsqEAVhuXxVkAZzXwIFa/pIP3p77UW1lbVf6pkzzXc
K854l/yNb3yv0MZbrkjkDCMPf88yi/dE701SWdpuidjTsnbw1JIsAyLYgSCLPvsW8pnuq9xD0DaL
LN47Me092vc3rX6RBGaWIfwUgyRdtUdPe3uLUKt6J06FGeI5KBMX45H5f5L4BSxgV7EBNO8+FoLm
pbrRtAdf7Xnsfs3MQdiOLWl83gUUDdXywM7BXbm3MamXimmwwF6McQQJQptFAMTI9JfbG/MK3AiG
iSkp0BrlAehOzEfGiRln0cO8qPQP+kQDHg6RTSPUCYhpK52561DqoSGXP+6tFHPwe4Zx2eqe989J
K4YyrbOq9yB1lejf7l/3z3cQitx+iV0jie8YNOL/kVfFEEP/A0hXXcw81pOf9qsu/bR1OnCJ+2s8
+TbbL0vYcxKCs/9uQY9bv9hobRSGEFKbJQrL/pma/2HpJd4pG/XtKO+7dc3+WqjgrYF713xWdVle
Z9Hw7wBmqfDNiiWiU5+JliHbm09kS35/3WPzL5d/6KHP9FaXb68nq56g9sGexrcwgXE4p1oNkjT9
FMdYWBdMxARHrlkabD5l2fuC2mqjTeQqBV8DwGPAk5EMOR3aVOxHBTaxNFi6xP27UM6BVRGJvaJV
/e7m/LxqXGIw1+6O0suF0mS0Fjm3ruvFz+Zm1DnNwS/pJaGBgshX+odUh4KWtYmWgKQR2/BW/OZ4
S/A6L1y7+t2QD3Jg+k+tLthtJUc5aj1jLImKAfM/33gD22FacBK8KorEBA1poWIx9tLqq9YfPrA1
2er3/676l1SdosU+OJ8SzVep5oR7h324NNK5ud1mdexLWR0mO2H965B9kNSE7PBpH3K1QhRwBEJy
fJ25SdL3gRlQO0XU5qd5E/u3/HEOBujx60map486P92IJIyPWIBYedu64QlPKAGgTm3rWp6i1d0Y
tfnWLKxb6f611T48umsy1PZCrj6GmgcQoNfgMkbvV1Kz1lQd5PmK4pIzdyb9t6YKqNNpZ8YmMjTc
8N77nxLrkX5kAdKPaNeCUnrmrFcp8+Euw9mVR4KXxePfbjmJ0x329ByhmLCop42xS/nib3K0pr4p
j1klZnDgowECvT7xKxiasX0uX/Lux4B0p6Hi4Sm2Ij1mXSQMxqeiQ4HQ4qebg+ZlcBtwrz4CvytN
39cv/KoskXMG4bzIOjlaVb6PYUxT77rrE08A9DlwGnWKS/3Lpf1FuBai7YmJ3xdhjI+vUczg5+wI
TUjfJVkeifGIbbYPmSWtlV3Mygs1M75nPWJsFqk60aCPfzOCB/V5+Xs47tAwkl4hVVZElusVGXuQ
P3RCUxPfiH9RiDYUnvNrL4Ksppt7YjY5+ZPh6nSYuDfL7xFt9buwiVxpWMFQRejvlGQ6hjW6a4Iz
Tix4o72GoRzwY4/j8MGP25uApjN0klv8M35Sj5OfyKwc4ugvrsgd0AzilZVC9m/F1DEChopTZqam
EWeukZq8tnWuQ1KTxHSAqgfGa0GcA/M4S2Kx7hi3GtoJewsOygHUpymM/NMBgk+/FpEjOY+zcmuz
DradkPbIc/RpBu+YJdNQK+pbJnvaStDuOZg/7yfe2wI+LWeTIUXxUVF5kpjEcPgECu4C23dFbuqh
Ysd3SoZWLKOM3+QuBEVydQmUr0BY2nScIGzwalL9FT0x515Ygx5XAqtu6Vt4bxYKeei9DXkbYEEh
gAT1ggjbF1bLj3Se+cZCoKmuHVmaLXcdmmA0J/k1zwC+HHN/ymGFqEomEua+A5naXaqYtndlYtJz
mi1Qam9yvvCeTQ7YHaSaveM2wC6FF74vJv5A+ANuTvg+CMRUSHSpJDBQbJkjhkHbezpc+nzkcWpw
mpOT+j5ILejQzH1nZLsg5lqQYKBcktKYh18mpzYtOX34kuXcU/dmw6Vmfp8fktNcHswuFKXepUBM
gZvBPVpyYc+L5qDPec2Dfy991xzf8Rr477/8ejjS1T35RolO1abz3FYvYCVSvz8/prslobCBVrQd
yWJ+5uGZ1SJXSqfpfjpMXjhDZPsMjPxPOqStCTCN0sxcSdG3LEY+wQlHljEJX7t+iQK+vhwhQNcG
DDIdnLxZnnIsCq2J4DmA6Cd7LQCLP7LWrD/VLKoahBRS/8+8U3W+OY8DiyHgfnCndI3kcPDcb8CZ
PMzPFjFujLXP8+w4Mexk3zN74BmKQLh9hC3Y4o7reCw0GJfYU8ynEVrx/xHgku53RmAVLTR/IInf
MaS3mTSjgMJLT15Q+ae7SbcuHFjCIMCA1kv3WkCC44Ypxf72cPddlgX8VAp061DNFnxFhgUaqNPZ
smaKAQTHDzcd/Rne6Xi5jwCjxEz8l/+hDU49jAJQ1SjtluZDFysaYLZVBZK6nTsEGN6Gm0Y3HBTU
YrEeoq/elwY4a2igMAXX4rRZORHtd7Et8ppnzz5ETQXVBwGPOLwomn90IsVLs3iu3rSENqgUcXGA
J+cGiRXtEqbBSrT4PL9FtXEWN9qkZsYQXL9lykeoYVZ/LaAJ1WS+14Uc6kjRo1L3G4R52NtioRxJ
3VOHgK8NwKL+2vzMmgkWqw+ZCZEnQAwXy4zcxPwi3f+S1D1u23/bAKa7U1Fkg+Jwd0WI+6QKMEub
CZJeLSfpahnuOp0sLpZMxrEhtr5znZJ2fUPXYm0IDTqQUuLmX+jFvHITq46izXHxtcZAB2qHuKCl
LkiceeuKYgh7hqRe7bYJt/x77rEZlOZ4P8qviRxxHRto113cKSwsP0LCGtmbCklunvitEye57rfP
Zn6blw+Dfrm3nEcTAOp/i5G1EIW+jV4iA8Cjdm2eulJkQrDcJuOgTlPyv3/jBzrwwopmChKDaWZb
sFT5bPki2ubbUXo9i6+vVXlPkZAmwAM/d8ZdY1+L2jhxA6nE27uEO1uE7Igxws/K62/hlK0Zonzm
cw+LvGbTWmzcHGRRaCBRS7rNdqpceDS+MJPQjqN6+cJh2xcJ+sPLReY+LoNjRGcBpBSTq9s7VLkB
87mj6ANMXRYHjN/NOKsuTlJ3gt3XAXqH9wM1tDlW1N9Qqx6UUkOy/jOg2Ov3spC5H9Ec0H56WVUT
2X6rqJs9oHhjIUWGL+nZjfmaMx1CEPszBaHGQoBv2xyptW3ZnGlr0E1+Rw/CZOHyHNGHoWuacJzs
LJV7hjCGFrxh/7kJMoMzmpDYHVsTlyz2DjDwN29rG9Kt8Tp/3Nd7f/iNzH6clcBdKxMPZ8BuXc7c
Kl/A4VSZ34zsZpDq1wOttzukRHwtgaDNoopJSutgXegOEmDv5H8WX5wgDfyzH69kq2zXXf6e+Z44
SEfoAgStWv0B5dR4pQZIKnXGqf3OI+RCP8tIN+Y6PfmaOgMj4cwFkYzDolfIZ+VVWck5pQE8rSlR
btOhSQMFdkFRMjqyfht2whTPZJUjs7WtynXrJh/JiWnPXMCktmDhOXDtUuvMt/A3iKiMyL7Dat/w
t9ImbZKaKKf/V8HQqmcUyl6bfX3BGkCml2mPz4sFh/qGswj38DQxZ9lSAqQfLvJrz+hJVdN/h2PL
GgsArpoe9NN1wxX2AFFbn1fbVaBsjiVXpz2L7SqMbUh4R+ogPoFeW1o1ASGPMenePtvyEgCTrPM+
bGjeeuMGO4ET+LqJKvivM7JlW1thH9HEOohtEuo7fyF6K//eZBVdn+o0nhhb5h/Dn90JkPOt/op3
SjWhpvx2a9M87+95jeKVPzqtcPtLYJbT1JzsdZpbV8CPNBP0/3GzEY1x09VthIHon28RZVnjZptp
WX5zXEf74p2z9du5W406sK8AHfzCbJb4YXz+pSJJmfOQ8SsQBkAtuEx4LLjUe6Z/lgtqK84g79cC
pElM8gksCUPa2jYWoLRO1lu7iNn+LL15FcJU3nVPW+aW+9eEf/O7mQ5AJ/XY13IlkJ0RYojcLCdN
xIiCmchseAoMjr/kOZb1oCHm9FDBhZibITHP4+sUVmhUVTNvmgJE8uK5r37QU6/Yi1He/oI1S5P3
aPHg3Vsj2vk5S3eNVGD1d4Ihcz+krMeM7oMEmnz0UppbbskU0HAH7f7oYftMmDL1MzDf0RYl3fO/
RDtoQ0oxSEtHJM0Nrk7zxqthRzY3Y4unUVVS4NWDgTrI0H7k3luEu67ah5ZLh3ChgCAjTJknv/to
CuK4olwfzkQ4gHpmDHDXBSCa3995EdhJD5vqdOLmPJTWF/bhLWCy701T459yETx2ywTDmcsZxviO
SjnyC89S+gqx36b3qur0hjHJIXBDj7TFCLhKXZCCXh8NLhUCIAgNyW3V+np44CrxxV223mPpJQ5k
zhOuB+2vv7fKaypd3+juk0xzZlk4qsAkc0YeRWyVTgdwqQBsZ1fpTMbaiVd8n8ui37TT0qomtxqh
hpkuNF04JUMWqAl+DTcTnsFIGu21jtXsUym9nMnE51os+Fd8YubOXxpihXdaN4s2b4Hqmbl3MJFe
8Kak2UgeSw/vkLgx5G5/27fpfvN7Zq15+UVnRtiNmK3qhOPiJfEWzmahJneCB62HCHv8FgmjFVdr
OBqo3RFFZgv+HYf+lqe2ogQtJ6mTmv99R6//QxX12AHVo/QO5IyYsCp0/PWefOnZxLHyAXwT8WiV
g7UQeSyj+tX8n28NBOmtKJC1FDl1bBptBvKZTJMozLlx2NR32++B/LmpKHCH+DTklp+rQ6rXbl1/
hG8Dm7eLZo5NGMqa1laER7CSKZyjjzeX1S1f0vxMp8HY0Ve2IX11qRfsOTxqmF6dmdNzFgqjqhoX
scHQMhk8yPAIaNoWxIiHCVpPRmh2puCLQl8fudmwSBymIZ54GAn3qPpQu9pmp9oxo9TWXFecsJyk
4wMJMMdhs1DIRbAEToqP1aNhJSTVzrSsDrbLledwalU9C137qxeP/+HO2UurHL2eXCfGU6jy/36Z
GhcYOQRy9Pcx4jrnJYGY/yJadgBaS1fYclAeoRIZi46OlwNpsfCP7GX4GrnZbAtP9IWjX3hFcTx8
eBGFBv+EaIFiXlWYHj8N0/9iZgyBx1bL7yI6aHzBw+PVddEynL4h6XcnwqL8+7c1EKemVshW3HIP
Xq4og+9/M5tFpRa+KHFSzsL7D1T1EWB6NS4RIYhf7fcqTgnsk1cFLiSnd1QA0stQnT5fI1MqCmyb
E7AbhghBMAjlTHFC4ciIokJZ6cAwVYgZ4rnzvb9zW/R/NGRJOleTWXUcQTtwYhEzQaW7gYlrNHeu
gDj/zJIengSUyxFuFHf0uHvqHbnqUUCSwK0TSVeVntwpO5WZPzG0JzqEMCV7IC4WWRo7Ktvh2Hzv
Gv7wer0d+hDw1wmXu5HROI5S772s9DA/fG59r/2R4j+ssMRQOz+gnYY7L4Y2dqbsTJc44UwEpddi
IKOjsTgwwNMQspkb+LcymKo1iNKKXzuj+4TKVMOQOoCS6xPkXsV/oNPRi3xIoP+CmWFF+K5Q+VtI
OL/YTCbKs1E4Lc34RISpeCfnyeQygT7yAPu1TjpmwNarp7uaGljd9kXc6nO3zLkaAJVhTPuwhUCN
eHPZqUAQP+tM833kUyqRtuY4qVHue52361UUZWTGwKjiSXcoSZDOQYHyPZWK9qeA6bwzPoWWv0Ar
cGkRSK+tmO1zfZiDQfpFSjvlTUY+9VDFgip7r5CkpQIuzUkbFxpW4D2axZKi+nLxd9oZdTsVz76S
iY77IZO/jZcorZkt9qtJyXoWND7HRokqWG0ZEiPUaoCjQCjLifevpwzpbuLG7lxeT5eEumKFE2/i
11EY0qYtSeqGfVL6T4AHBdBCI3P5nMNwzfDDOd4lGDtuPyHaluCfq+iYP+d8BNWgpBQcwaJF6/Dm
U3kta7V2JXIoO6AmJwtFRHzm+JPZyxcZIwGWRAfQOmD/n+KKtO4Xz3hPgeW9xQfJv9Y3Y31s7pv+
iRNQsImz96GPDCVBtzJ9xCGgXyw02isasEngkpIkPbmTk8avkqQyXnysXZV8VOaK68g1Ff2HBWUo
Chxyy9oP3mnFHt6YAR17s8dB6QIpTtwowKibha/cgp//WPyIAFgskaF7XGg0rROf2qvMYrSM4kzG
un9/gZC81NOb3VI5A76NFD+45RSc7mfLvNx+i46h7/5WtQ0p/7fA4cqv/C+agUF1Nq+DtHyClNgN
t8bOO7wecL6/BM1AmvsMFCl28+CjGpNN38g1s6ZunnPYa2Z8DPce/tPOvsMgeIH0nB3tjDfqx64k
gIalan9sgFbCXIhFCTZftKttjAyFbbSDxypWl4oQKbqROfZEyA1100/rYQaDZFldZHrVtFsSrhtY
g9G3lfTVt5buP6fWCPrdRBRsGk8ptyWHDXIHRZ8OFkjB6g86l/saFvTxqeJerKnVkR2cOVlIdbB6
n4htfSyLsDUVHvOKPKcWHAE1Q3ttNszManmPT/2yYT5iVITkC49wdtHOkKzUntJrn+aFpCCwf+50
fNsluKXfXvpdhFNDwM0QhtIBaCiXPOGGlRq1cnKu3cyGA4z+QvuW4lug014ye+uoBH8UnWYP89En
TaNnOnMnLxB860pHymtDHqcmDgMtOeKGyFnTJ9B2qN3aTGyhBif3CQv1Sqv/UjrZRDpb0pOWw2R0
rlSs6v0QYIHQ8g5N0kjpB5gIfnCFL01kwANCOalpnGHah52Ghw0hdORiJK4WzxrU4/kOoAor9yQY
KUgoG57q/YdLJxUyZtMPeSAcZxVN7tY+lCWcrXJvIjoB/yJ4nmQgqYr6ijOK3//bGTCHRpKf+kXM
wBsD5R/JYa3EX39g9ZqWUuzYaIce/W1NNXRJ7do7gOxuGse0fNXRwnYa0fiyhGYjgn/SkueEK968
nn2Hnr1SL3n1eBCox3Slnn5h/f94A7n1CIi2jQbAD7w2Cs3BwnLqARNTElGEJs2dQVqf8HohF7d2
LNyxuLPkYOGmX0amQkBu00Z4qVXO/WTpsKVz+x9/8ONNMzCw6Sj/84B01RjMiiq08t+pSUBmA50J
SfjSFxLILm46HJoF69utzxvoGL9NzU7voCYxMS0uWj396aWmE+AHiXP707jK06zwsi/ew2LvkFHt
G/qeKFgJUq9skebLJsHuyCsppkqloDnyrS+yRqJbQNKwLWT6j0pYhIbA1Ax34dlhPdEmURF0LgW1
N7U3chgvph/pWc0EpHtzzPO4NbrGLnPNEa2fzi0mQaAX1ZIRpfCkkSY6WX5VvUfuUd/OSdet2QT8
fliDV4/Uugo2eQwsIkmrqKP25wc7TFu59CsY2NtJXhP86DuK0+rr5odclOVZyzUpbdnoPlaBBkV1
CPylmRebDnCYgdzIOn/NIM3Tn8DPkeEi2Bx2DKmffcKzKBRu3HeTljZG/B1rSs11Y+8ZT3VYbAsK
TLdHCqg+L3mI4P5sOzGpPVjrcSVr7nSpo58pLt7L9BOWcnRHQQTXkX1lbxNBcooRbZwgt7DeLqP/
WX7K2Gw/NI+CzhcqBTHd2gk9jBeKLK8FmDsupSiEYn82JM1Cpg/D8ptk55d7h7eaP++7BleK+Z0N
0IKYDm+dJq6CelK0DIY/c0e7A/paOmocikQdYU1x7xr+JwadFMCmAwOlNCzLxVW963jsI+yT29bp
jTtGG/gJvK7NfFUu1jTrci0zWS6iNw/Zd+jCINUT7eH+2Wup9r4EfR/AHL01SyHfPynSHD3S+UfI
djkXHWuCcnWfeXuQDJAwEQNeqTruCljy1xjyRzViosTecqFqkGF7I4zSO484VYhJI7lNbDYBf6qK
CPnu10McvW79iEJNiUTv+UhWOT0c85c4l7Rqkus9aAISWdzETvEE7eVibexKFPSs368beombG7yd
euFp80fZS3G79dHI3GblNMLgwSZIRW9H4fa5pRBG73azopsnjG5eGGYPB+MHJVJT85XdEz63csE9
8tx7JQl3WJlUAoeZkA3B1vP0IY4VOLjI/eKsu4oHztxjOoO5Vy4mE5eFVSR4Wn53UCuqaA0+vKHe
yi4QDNvtnc5pAcepHyPshG+zGRH+1nIn0fhNEPd/YuyKvtpXP7Z2c83sRER9rDXXplx9VFY4Hf6P
6P+HSAcXGmLbsCkfOF1htX8PYjwLEUtjR1i/ibZS8MFh2IGnyTSKvisBYbToveT0KRojDFXTf3At
uh+NqbbEkaSzBc8RisXH9jqqYs1YrB5tegZu4+zlbLXnma3To0lRzxQAv76OlHmPYEXB0YUpi2pb
VIjlK5l8OgD4Zah4RLgMR+GgvQSI3LIhnSqpN1F1c9DCYzWwv7gHnomeX/ni8d56rNJ43/CCybD/
Wg0f3s3DoJDcL+a3khsaVtj2p5v+iSkCBObNKCsWHqidNC4AM2/RUJflFEaw0Ywb1eEFNzYGRlJ4
eoIT3vOJvIx+BJZGBWt2mNa1WL5z/oMHQG3o2CV+R+pCg05tydxZA3vWYFNP1rrcjSwcVNaF1p99
nqKDednxegsbc0EeTEFPoWVlGgtEHiJ/miXYzQ+2fNIcJH0fhUzZF8ELGl+gv8xjdmkovMS6snEQ
wWNES0Bm7GEt6XmuLgsJLRkH4bOT81jqSj+jcHnWl2bPzlkvmyavlbvdx8Li/dUc/T3sWApEf67i
FTeZYfjjoAc9ZzQafLZpaVnuF/U3tjzaxT02bBcq2Z7e4jNvh1JytlWHnmcL/Y6pfeWzPbC/Zpbn
h13cFvDfpN4CcKc/7W8GN+19EPhRjSrWhxYXPMI+psYHf3ThBHN7duLTEr0fhDk9K8X3RHB62XC/
uwf64gHKLc3V549dXS9yU8KAK4FR57DR0p1W4LKCxOCwGWSmNLbFV6Rr6yZm+jnlo7a/pihE9j+4
9jLSWSQpXZnUVZaoGhiWZAfki4HYaiU9wKf4ygKOB9gNr7xgKiUMTZdxMu7i3/YuAbwRziAcF72X
xGvHXfwV9h8q0IorAd9UvRMfZSmg4660ysDc23nhrE1qIwxM72A/GxJqvLnBXtVaxxRz/f3tbgC1
OHjr9d7lLrRlTX12NO+0q5g9GxTKZHAuRQaY0Zi8EnQpBJUR7FlcK0URIYEnmjqazv5IzsW0HfPk
gREayBU6D4hGI2iATdCuCGKI+/Nye+BrxjkK8WolN0s0lEVnWtHQ+p8igsknhnW+7GNmTTQeGBXx
vAOd393RWavRNZHypjRzEw9D6npqv56JpByiODbg3cjN0lfh4CjILMfiC1bzb5QHHi5v06LA2Zke
nSnBDIZTn4nDTySchl7Q1MNVHe4QfEfXu0x7baenVLrUFdc7r98tujrp2NTbEgArSPKb
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
