// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_2_1 -prefix
//               design_4_c_shift_ram_2_1_ design_3_c_shift_ram_0_1_sim_netlist.v
// Design      : design_3_c_shift_ram_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_2_1
   (D,
    CLK,
    CE,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}, PortType data, PortType.PROP_SRC false" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_2_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "1" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_2_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_2_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZEvbP3BmpbsEzNzKwXv9FfIdjnyEYErgGV5uZ6/x45t8rDORSuraYS7IefdTdRyF6pufcW9y7Oyv
5XQSBclEYkwRVvIi492OO+8OaLrEq/x2zHIRfWeH3fS81IUwKQxvshTdd7sDOCTKL+L6PIekfIM6
NpTWW4Ne+bmEjS8+gslJvV2MZekDCwimPb8Rn/PXP5arWs3Tea0x60gLUTH5GaLojGiUGzck5IR0
AGvhQgc3wO5lJB3oOtQ6NQvMrAMSkAM34REA2QN0uBXZN5n0V2SLqf58Q4XE9r+aX2Yy7wEx56aC
s6cD/9H8A8HmTMWgUkN+GzbRkjG1CUvKMvu3vA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RkokOkTBcySD/H9LYkaaYC9NrMv0V7F9uD4DLqQXWc/hgKA9ZRWXyhoNH8YjahdxW1lYiWKbPWgw
1tV4fXMlwNi5BtmqUfztHHNKYA236ShmrkKo7cbeEoPiTYVISyslj+RZO1R2ngC7g3PTC5XMiWCh
wDzOTIx7UmycavTRmS1iHCTeHzwuRYXJLOSHmuYDNBRdWP9JmX9KnPIv9Q9WQ0Z1u8ZeLHcHspuu
k+893AiQMEGuX2lJm6Ziz3s3FgxfUf4kxi12+fYt3yv7rHE9n4y7lZxuAFk2jN2Cv4J+GYMch8qa
9gQnbvcgt4aSYXZpcRFn066+wsV16Es7S77rpA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9856)
`pragma protect data_block
HujrQAVG7AFlNqXAX71ey99n6R8V/WF0GKwRTHbazcT3NooLUVBr2o+5/6Y8T9DMbsZ4eZVt2cq6
4LYcL3yIAu41/jt3YCGMDeeWEis/cqdcz3dpOWVWT0r2Vrxhsbh+fNtD/Q7MV+dEgUj9sQq2CQ7L
9NQ7Kh+sSqoLt7Mol7D+7qQpCxXk8q1vTdxpUWoVcBUoYHv2Hjkbegz7p1fqjHzBXgaSNsDlwkeu
5RY8iQToDDQAPvZOCekSvI29CvmW8DUZyvzI/PlxJCuJg02Y9F+lLGa2L6WN5U89hPCB23yNkyGS
J8otOq/plMLoNt4Jwq/+2imnQVuJgmWtd2ZghkIXedI4jDFfJXzemrbnM2WUs2RPrbTQLQ2xo1Oh
S3GPtyn25s95PAuFkvOLXKOxEWqh/DSXmyC2QC6aaNDK8JzdnHJ03BHPyjGkQUkRETn8dr0p4nyQ
gPmRVU48V2sl1cJ4z0RbDInA5pVbp3E3cwkSimo6vJksM5aCJzQtPwu2kQKnxkE47wStQJHzudEQ
M5Xi/uRxvic13EyXJgTiwTndRn2u36hlOQZp8YilfudeWlR4uV8dXZXnekYzEq6p2FZp+AFfM0lu
3NckNCOSYX4iqTfP7GNSmBJGu2sXpDY5/NrJv4uzfhZdEZCDKwk6imeF0BD4qugmmDrF5HH09b9F
uaNgsItZ9OZZCUJiWeaM8oMMemgYLQqGDFWQ+5ZJnXMmlr+mgvVkY2qNq+X6fwwQv9ALJAa5sMnY
YpNAYnrHQ2aC3tS50GR1o8frV5Y11/EuLuYZzQJt8cgUjHBcEJeGgOtribvu8Bs5wzNGkf2JrRMJ
2OoUd5qqc3gYM1P0okJt24GDDEBvWte0uRugzxbbqtIa3yMctjoiD/QUXQy726I/1JdzKPjd7TjD
+1ar0iTCmxm2jdoNt65Wg2qRtMu9lrQYDLu1yNnvGyjjB8zC9t16PFAJjSAcVU4oJllG8A8zycaI
uhYQj3e4mwQxrbk3JktxgD0euCRr+BEKXfLDqHkIOSwtO7+CdnQduNAtjDh9grEJ+lOJdITOhoKW
zakFHsW7UGMVNgwDrV4BnOoUOvtCsQQXjzgfAWMWlQIBufp1qBYg605BZxw72A2n00uedaKcFnIn
QruT8Pk/AevN0pGBOYm9gEuLI0tKx137NwMY6G6tLwif/MzOLsBz2mmSVZrZozlM5ikgutSRCSXb
Sv7f3k9pzQ4Y/Z6bVd10RitYyDSZIW64ZkCP7Q9xlxbo0fhkkgx1mYcedBEs6e9CwiwvmUTRqNM1
8MUAGJhO0tkQdMWYDDwRH7Cft5C6okrfZq//VK8COSC9eElJZQwfPXbxfL+MdJ53SVOss6OZSnso
MfQQxdh6a9ZX4stiF/HsqptMf6PCnFPj/Fe9LRGgrMpwVlijca+9ELd7qie973qrgPL4Bng/hD0W
GY5qxF2GIJtuCGrGorLlMWWIh9euNBaA67GuKrPP8ju0P3mYScwspXo83C3hZtv+LQIj2bp4ihig
qiJ6fdu42ft0A3a9xrRkd9cmyuqsKHNMMid4LZz5Xrq1zNMGZh9TCYAAlPW+cJAqSZsXRoogVWSx
takYaBbojz5aVyhMlPvKYXfXotM8kY5jljUmdBMSHdcoaCZSKc39mZDKFOZXC8k+Mu/vd1U4G72Y
oThXC7NKrI3PfoQLt9/dT9WpeUWShy7vFz+vIisVVk16bz2k71WDdH1/hUDA7/xA/JwTDOq4Y0Tc
+1R/k7YJ0hxCTwph0yEO3ao3ChYo1xE2B7v8mdis0z91POy2kY0FdnA38EBpGPKHAdrmt/BIZirv
rV7jIO7ny8xqKAbtm2+rR7aEbup0LDUpL5pSDnD6t25O0wX/dCrXBJYwWN0VdHiKs52pbFp0SxxW
2jrWXZdmSNcfvUylxsQg0P+qsMDZzMEu7A07+/G0TuXaNF42JpO8ji7YxlcdgjLXYCFszYE6c4mt
5dgS33X6U97MNFdLIKPxWTN9wXnqSFrdMHvY+krA3TUOh4IFiM6rU/hdTc0nov9Hly/yXre5JwNm
rr+vkrM7MLLCdtKRKG4HQzBfCGtnLycN6hzRuny/Zus9Ua7uOOApexAmrqHuXjaxcqoEFsrQILIr
GbiSTmW860YaPpXQv1xwLghFZ/FagccMPzYZecttnA31Nmj7qK0PXTqYlCYobyzx61LQkRqbWpam
Sb1BNkMi18rCddkWDG6nXdIcSfspPNhjIh1hsNmbIO8vMl9MAWxY2hJvt++EKKgeF8bhSnRobC/B
JwxInhj8w/1VcD8YXvWu6EbA8Hztr6I3FxXdXVfNsAZaVCK0oE/HTcxfWCD6/ohy0wxO2oF+pKPa
Mt/NVB4eLCGETzIXaFbriq51E+BmivkcWCcUq79+zXeLBJtOF7RGoQ8gQV6S89vIkxq206axXPWB
hOpPDFJe2EC+c6Cts4QCUrIj9kA+pQEI+ShkQP7lBQWttW82Gm+r3bNTT9xUbiMDaSVyCnyCVcJa
fbvzOZUlyoKHbIOf6CY1uwfgUXXgjGoFdE75iN1jkiXWZ157qvZAMoGU7ND2s0eip6+cDaINuLy3
aSahvAsYFAMlEgD3oiJF8MSQXhwmBMXKdt9ShMwWamly742ggG7cSGpGZ4mD04/abNSvb3K5qqVs
5/evqIOghLlUFxT1Jis65yMyfSUQ2FpxRn0egw4U6E7Mu967R7rrJXTRHk3Re/OKrNcfZ0SZwhWH
qpbZdEXSav6hHdAlCEyqQ3yoclJqFRGp4Bvf0LbqF82qjRWBEv2ZNSH7iZc0kgjQZ9KhZI/22UAJ
oyrqwGlYONDbQ1e+Qbf6dob7bTj9066K7Rm8BkhSh2/TO54+iw43HhDFueBRGOxMCfxr8SV3ofLA
AlQV//G/J0ykRX3yXUZsPSDgqFicKhvQfQ41OuMNlbO0684QbYpFMKyB6njwf7SzhKmaKPqpyQt7
5TlI/iv9b4N/N2shnUVe/g6UjrlbfLMY5ufdmohVY52XLvpEF6F/wbT8kSyFWj8QFPVRXwrYIiEN
xFhzQbXXTpyYy/HnSRYClzVn772FTLgkSgt5nAG4yQgaFnYHnpoOnxhIJUYfCSO/kw507JYD+JNN
SF97QS5hYtMfqAvEsZ9WAdCsP9EKICoJuaXy+AVGhwrBrJgdJcNInfe/uOvWxj8DtoFBdJ661SNQ
BoU5dvWSazGPF2eR4Tm+cU+37QeYBha8Eqo5vO1RDOCca4xRaj10hgpYEV3wCwfzzCOkpjJygRfu
7NCTg/Jm6lN5jodGiptP9uUjGt7vUChnI6MdEnUGg4MsbCQEoLPfhV4BX+sUZSpekjh62qsBsAEU
CXc3r+vZ4U3aOcYuNfkOgdADOwYC2zc1E62vpu8jAxTl+QbLzDen0x/DrpeFtpB+447HUUpPgwIz
qf4x4RVV/khaqu73YYPBC/GfyJtQrrkYRr4Wpman7oMiD+A0udPW7pu31eBcPOYSwqhGsLZ7HF0x
VMNDED2wDWOoS4WAv4XJPWKzBRioKY3aQS48vtL6MC1cGV76W6caGCQA/+5SZftf1FrbS8Qe1MzL
AReEF/07TGlBt1S7oZVJgoBlgdbgj421UgaGC/o7b7VlJ4CHSFzF0Z9p3XTLGtVuBCNnogNmVQ3F
Hcd3JK1q3siBaBEdrRowrKKMkVIMgGgT+BFXAyZ8cHIj5X4dQ4nUh6HAm6IWps4bZYeFKLvfPZ4/
U1b1oz5ZkXE1wiPqZ/zZmdqzu+zC/uuZEA39v+e7SpVwwchohs9vigkIpwz4pLLwoGwqabIUuej9
a/zGHRgDoNf5M1oF+MzDANGcoPIvxuaqeuVUF6genD9h2pBIfBZG+qyUr76yFE5YIERcrvBnIw7C
hx/ZMVcax+J6VqYdnf3zzRRgX3SD0JiVyaZ8BfaDhSFap6mTa0tUjASwuIQh93zWlzCvMd9r6yFf
Q3rWtxbxbSiv+Q7KOjbG6s7NVKSB75gH9f7iZ4su6Z6DmXkKLKmPCdJ7ISQExn8JwzN6rGRBI97+
pIVI0FX2VVbEsbVnE/zeMeT8u+LFaU2o3nbSsO0xPPw3ofyvOWujDyy5h8WrqUbxY32JMI6whR/9
ohWdXhVlXkK1vvIB+W+Cp2/tiDJ/ax2pdmsZhJqbUJQPSTp1p8OSPYo52pBxn+nOn/yEmzt1S1vJ
DLqsftfIUDx9h+Iiamm68Mrs4p3JKXZ5znHeXEbwovfxKN/UVSyk43T9POmS9EHobl3mqO9G+S6l
HxenrYML0ptwq8Uaf/Ffl2SuEdilYm24YEIWBcTSU37zDTgVE1aTsuTzQm6Bz7BqdWIDGBoJ+gcP
wGeJeRi/ZQS99Eo7QUM7fZnimLNrcFb9Tm3QqStB3msc7+4U1JW2BOX49NRWUY2SSS6bOeVxH8yK
AZR6u61zfD3vu+aygZCU7ad0jMDdlp8iOZeZldc2MIV/Q4BDsVqwSKL/Wx70HcwP20ZkIsmBMWv/
sT7cbfxTXupT0O292a/un7JKe+K+vYFyWLHVY0vFy09AbpEx3X1m5rR+tirePrVC7rDkGwFFckAQ
OWYoxreHGvRrlj4nmCKX4jnLWf/jaldLVCm6wnFCfpHaNmkCLN8OJMlS4rpM+BOPDJ7q2rjulaNz
OlQij5gkmSm1fJjg3h1jD0v/SKLa5g94t8RKiaz3H5CSqukM1vh2Nq0H5QI1JS7x+b88V7d2gl6k
MZn58TG4MSnn2az+NuHAwLwW1f5YA0L3T5MLaIIzbKQJqTmtUQUbYuDIJp7pD1gZGmt7DdVfEkpf
DijSzIuK4SmlfuW3oGLqQWc6tbJA7+aRuAKMAobsUx41PhHSxq9C6R18+HDsUg654A2fvlzxDBqR
PMdhNLvAdK5zMink609Ggc0DMsjk7RQDeDB+vxjal99O0fdbnrq05NF8zN66NP04IzCxlqYzel9P
9ifs7jR8KTuQq5lrbkOJm6Kzr6w18Fbv8Lzb0MpUAyjTmygTAh3htfsRfSce/sLGwYtEee8VeKcl
tJo2ailv/OJQUeod2yHxKJlcN8x01wUh7BNrIi7kQsLdRTxrBLr2pByDNbc3iw/KUxVh57McrqPu
pSsMZF269t+j6kZw9NqQ3/RkyXA1bvlaalD4x0oZcyyv5RUnnS9JeCf6JmDGyO22dZKfkGEF7IpB
0Iff+UUV/ARTqyHGhFOXQGjc56ZyqX2Ii7lcag0k/YiMyEJgeMI6JgY+/dyI1yf/GPBgyw+ULSdt
INhsH9H8AWT7oX6kkhcG6va3+mBN172ZU8KqPo72Yy/UG/W6f1QYS9yrxLF0tToQ4OZzCD9aEa04
QguxUDpzH0Ni5hFOtcC2rnfq7RbYwdreLOkmai1LbnIQIDoOhcrCtKM4/tcHrPirYy4RqpSlQQ7h
w53yY9lhA4PK3ekhWhL99kJiKr0KXEyMuZ7FdWXY0f4xOdtW+ylva6y4K8ZXcjywC1x2btBe131g
oVYf+Na8MvXsv7gbkSO78EMYHtEw63aRMdLGD/6+wWE7Xm9b5mEnkAu7fRGuihzl+TIvjmWnT2jb
HYfbi8lBGrV1D9Y7ErI7zZqf3WJSAlu5W5BlHhdVVIzZ1SMcBe2SePUUZAW5ggMZ7IwnseoVcc3J
BL/rQZfdzk2giHyHuXErQqQL6kl4R/6nCoFHevnq6fr/5AYhudZdzAaImx/1wk3nT3UvX3BzplhY
Ae5KcErfddXC/zweVa6BIjjaMTLA0hXWLnaqQC9uTXieXBDz92JmhxblBInxz0Gzg5BMJb2qc+tH
3DX81IQk8j5mdBCGTOuxl593H+tMisQ+7XSlN/4cvYcHW0vYGfvqo6E2JQLUpl7DrGiKSAOG+Syo
fqoLfUKLyTL0pXTzL3V8DEHCjRJffUZioHUXs/NGVWqG88HTRj/YV8w1w7Gy1HGGPez3/VCttEc1
hQ/uNyD3UnAiCWD3SP686vJtp7GorlwmffK+a8mvsvhzeH8HtLp7xjSPDIzbu9yv2m4DxY1UuEc2
oNhb0MmwBXYrEJK04ILTEWZLwEfAMRHbslXhlQyq6V6VDOwsj6Jrg0BAH9Xo6CheiBi4uBPp6oxm
1ZfUyD6f2E8zwscj2UO5E78tlJZNW+cuFEAec1vtBV8Ktv2kqoKY109Rl0s6AH1bnGBQTNDZ8rRb
K2kuKTTaZs8tuVA8TABbrKAmbZ1TBwjIPzdVAAME6AB7ZTusBRX7apNjn1ldJYyXiywGJP5K9I38
rhu2z/cRdtyab4M80V8PtzOYRQbOKVI8VK+y4hC9wsXVdXfv/G857y/RhPGzobM3cY4cZCHDaWGs
KALDgFRaEeeeNTEvbN55rM3tbeobVBJrzeikRLPQh5pI6Q2LLqrmFykkx3FOyRzM8OdcrBFEa4So
xmFlaf+TNktB15oxtcLHKPy9wRIzw8aQTuZs53CFiKuie4WazgIWnLctuCNwsOmFi1R3/rHXb9kZ
2P3G3drY7F0fO7V66xywnNlig5WKNfU9oLBOLqU79Tb5u8uf8EinujCWPqp4j7TgdT/l2JRR1hQX
0zg5LchXaqZX8shdeWf2IKHhA0DS9Bww0nCgXXEn/L3f5b8EvU4mcAtr3vndEvertX57nDSbMKGe
iqEs5m3RJooZwQ9O3tiQ+5mL/J4NtBPpx3AQxQvzlTNLTmG87Z/XX3kxIuZxPgjYWlpmqpK831fy
Or2Z5zfDhRj9WaohZs2Tdv9YSlFa+4uUEVuRW2u04/NDcUh89SseWUassyPblWg6ShIR//o4Jf25
Ruzz6FgS9D1b61+piWk3vDTWxogdPijn0Ig0cvQw3fkzy5D5aLlUGpe6vyQF+JJIrNpfkyBFtsBE
2xhO0LVUj+NMUStF4fLdtHAzJ1l9CRz36EW0IHDssjWgqkT7Utivg5O4Y87+C3kOlfL/9UJalWAX
LYp9XeVf0rdAXbRQKJ6VBhJ1oCooaHbvyB2wET6qttDqeFfXHLBuV9aZD8TsvX5w9kwTqDxBpLnm
iLzU+gS9hJydwIrZ+sPvmBEoaLn7oYr5Q5DhVvsTy/ZAaWedVyOQ/DGdV/ivxV6B3actU110tOgb
Uj/VchW85iV7CVX9l2hC3FBTrnWdDKqExiQXJA3bHQV+VAs214PDalzDJ7Sq+2pW5FOOCCNoQeLF
JtI17r3Nd4R/szRKFJvF/XAa2Q4A5ahE0zkC3ZYyn+nDEgPVz2Zx5ehZHfezL0+Waz++kFmCz+Op
Na5alrEonx2m1LB4IbdwfDkUFNKakJPoeFgeYAkVWenOJiif0VhfE7kHSa1+Xxpzxz169z0dTwXa
hk4OfrWQJfaqjvoY/BfpQiQeAsx0lg7LsNr94lIEf7jbS8n/FRZ6Z8HfgwJpuJZo7qC1upRyIrV1
1b8sRR5Q2XSslQdxvaiMOESMXLde2XndAuANXSYbNr2WsaebZPTf8H/7kvSNmQNyUTROZ2FlnY87
Hc5OnpZXPKGeAhknHeluS12WAi2Uvp7mkFwI98FYD2KEaVXWQ26A8v5T0v+miUwMD8qfM6nU9kuN
mu+V5WCbS5yP5jh4a5bCkH4kBc44vIFPl3+oIWtoUhHpdauzXmh47nyr661Ff/vQYdTz/49TyC/v
OdU+g745bRygOdiRaYmrTvyakPYORJRYENc5jiPmGFB8r425drboDTnGGWkqc2sHovvOA4ZRosPD
pkUz6Qh8I20re3ZzB+8pUgMkGYLCTJEnwIbm44yCNX+wEO+c0Q7C/1xUdnCclHnPjMdAiwQt0T3n
v7Mk4lpR5pGiE/AEXoeOVT1CZFc46xdYW/eFqVNyPNirdACUJJuToV6Z0SAkU9CbWwZaOHHak3dJ
y2wQ/Q7VZehPIUVgM56fcpOLYw297r4metXuD2UfHnBm8sEdkEztNMALf1nUinFmZIo7blwMpUEl
/J2/2NTS74BABO9V8Ow2RZPYHMj/58ilq1Y/FAGH01SCuWjEIJsrnaJpElSA4X0yPB37gKBBKfJY
G2waS77trtD097ucexlM23CuNMQXDiOFI9VZ78oDfEWycnVjj51XLmYXSq2tMamHAfuJAmZ0X+nz
GJPedCDSto+d8Rvq0rVwGf1fcyOFHyPJqzQqqcL6RCHuJVqQH4gDr8rhLmRnwECd5t6Mwb8n/HYc
lSdhINQo4iOuHvDvOg+hHEmuKBk5icgymUbnmyoyrw+qiuXyzf4Ycur9UaFQwSBHf9mT4HHXAh29
5S+l2dxp9f67UqvorA99yr5FLfLekJWe9y/oEtXBOUSQ7RaNdGpv8gPhJUTBX9HWYZbQ5ODYDtrH
hfFzhPDb9vToc5fPCPGB3IxlRQggWqLS+gGgQnYXBYf4vPYlQs76XyekCJRuYQ8zl3tj9ETATa/R
gs37flGxplIiIZpGGoJ5jM6VCn/RoIVKw6Y+TA4oIT3acIvAwd5RnfnVcxrfTCQbVtjF3gnGPlcW
8NXCWandutv7U81yR4mfJzfO+ES0WReRUyWhchjJttezO8fAZn/HmEWZlEHa6wnDy7fjP//d1xks
guTEWcy9iANJlgt/wLqU3PcwCU/Ka0OXQVMZSb9RCaUQ/3ASkPa+/HGeBbjUepR9AmiVg+TZQW2d
p9w6sRmbbMVcKK316hBFMkBf1zRLGxj6KJyGIPd/r7XnYub3LgvHGviT1Umxm2NmzdRfmgz5uaA9
5UAwj6DA0lsjOGRwFSC00dfBJdXk49CBasqJD6Vk/AlhkaAi81yBfNcZ0oFyM7QycrcScJ1GANPk
G+1vFO/OXq3uWg/RXkDjOlpezhUi/aohkUYOrJ81uRogQBX71AlnaSmtEtovDC+ve6Bot7HDfpaX
wYF7d0zMcnT1m+LdYYSXlqu9YG+nNcFCkfKWak22vIPGCqg8kHO+1Hu0izyzCCvyQ1ijB62p5oPR
5hOAmgRhg2MF9dRDfpLUZcVWP0n2yiYaCM7ElWgFoHfM2Oprjrrw+UMpO4PtE1DJQ6EDj/Qh5cyg
OnjH9oopNprgf3IUa4n4kg2KVMxi7uRyxFjNi/YlVy6b23ehsvb9VNuXdaMbH/sItZDqPRqdKKgs
kPpqjDRDXksexfXWMPW/sYFsx6dTlN2qbzSju/4RaFGZjiEjfGNAC7O6VlJToOhIt2qht7iMgZfE
YszKGhGTxcpDbSgzOEZptwj2PRPWDlOKgKvumGl17ZorBa8ZXIvdbCkAHOXioDH9H+FHcid01hJo
jlRu3n5mlQYW3jVx87Ga0fXVTgZnZmV3pvFhw4rGKk7+JOY6uqWAeQr5f+lyqTDNceTUrFY+S+Ou
0ygsmuSn/KVZdWoqmPucpbRys0LI+9kX8SecnIqBn1cL3+njmUTwVpQMzlhrOiW3qGwLst1AYPdE
vGEDZk2O8SwJP612SHBfaIkKIu3YKuJeIyWGDzLzvo9bQejimBjQ8726dmjLmzpakoT+oFVltLGT
KZYa0RltTGZc4jkcPezqBZdI/9QYw8dX1mAc4oDO5iJvGMPwce2sH7JmCjZCITa4CG1j2+Q6twH0
FU2k784nhTQy6m/oqp2MQ/fLAf//vivTgsDOeGMl7oPR6fl07Hmlrx8XZyily3E9uajSMz5EfhNn
1oX4QIBlnru15YIF1ln8/8t2y7t3/BZlyPOuAt1t7NHS9iYyf1FphWmeOm944IUZWz0Vpt72Zy8h
2Ef0/0/+F1dPqZqnAh9WU3xJNl0wAU45Zk3gmncOTcEdqca7ruo5SgLcOSorZRSVd0anskPfMBJu
iOXknItbA5xD1CaxEUoxdhYTtIx/0wvPxHvPE7q8yRbOoTo8za7NSPNvmmoN644t5QoskeEuIR+u
fYG84BNA5E6XDklbF4YddNNAU5dVsDezR68ECydN7VqBJBbX6HLaH5MtS1QYInZFK8aFbCnIjemw
n/hNHHzKxlE6KNmnYCDrl6tULV/jQITiehorYTPzzspauIchmhKyOTCSU+NH353XYvzOR32nriiF
F178QV+SDpvGVvjhrbywpZf+pUagtMB5OUQWrSnPvDDrQGjvDVqUcYHPSPNJvnqZKkokw4R+x1Ca
TwqJQlcre33VlmDpXv/7KeKKkiZxgUSlAXdsn4IChzID+dDSv7DeN4v6+ep/kSjPUEE09prrM35s
yb3r5jMR9DvPPC0QIODTCGcgz46ja1+5LHybVnlmI1OA/DUgua4vWi3oux6XSEUZFPYCPQpQFKPf
jRy2sDbCLzJkLo1Zu7vCX0bb5W3ycFC78IMjXT/z1iNKBbUMM2Iohj97zV9czGUKD2vDdOG9Lw6v
36RNbIJMI7F+QmOhyCsGV0fBhypCkz20CSvDyj+D6V/jAQcxHUK1H2SxZXsEweiW6urW9Q6mQeE6
kL+mKiZvHJgC5LAuR5YWuWB/1HZdzVg2+X91mOFrsTrDWwukmBhbP6n3ZS/XjxXyBlpaKEJtimQt
23/jkkip3g0LYILBtiD0b+jZJf1gr5O49DFECOvIjOURspWRBLewlDZ5MyDFRdpFCn0qqlzBBFuk
K7NSO7DiHIJi+VUlkvqa72T95lYyUZqna0B3j/kf78LlVS425Xr8TzSAL2Fn6zby98aeNKuQrK26
KInDVjaIHBJo7oRJzhiILT2SBPl9h1j6T0ZMBy72T3p1WVQNFqSKexxZ9G+ziltjeQLQWMoUVN9t
CK2VG26ZxdGbSBXhWRelO7/ZvLbzRqgHhVF4axC+hvN3TbRtKzHtWhTyFiYGBCcCcKRUvmgJkg2y
iJVp9iiIAbcY+Ub6Xp+9IGeC7y96TK6e5WsF2o0t6oToAiTn20YU/pyydmFWS8iwY5L3ntrBKyhT
AsyLd6/HbQOMJn3yA/cM0xvPqqvEj96dR6NCWA8A8Wj4z3J9zkuLQR1TORW0heL24mZojbYsZmzE
eW5aifNVV7Fa54OZK+DWWYC5cgTI6d8Y/osED8wCkcPzJu1F4tSCCLAma4EmMy23OYsEaUq2RkQZ
9/dRSL7Evw4yw3Gl0dy6LUmdRous196BromMAuApVop8dQ956Y2wdCeZ0Z9TYTo+1fRVaC52/vb9
MWWX/meoJyzszRgLHPMe5bF/S1ajWhZQvbp7h3ZeNYFSFckVok3e2ZdYYmLgjOp3fTT037xQxcha
F4E8kWJIGXQje6lZig95xhtPPjD/NSs4t80C+DF95FxWCYGq6hHUTGI3LKAkOteHeaEbxGLZbCPP
ZKCdqiqS8dWcMU4N82dLxaf+g5E208s/iptE5omYBa/FcTwzN7oeOhToSbpxqlmLSvubPXSCTHDl
syksEbC0oBbkxKP/t2wXe43DKyqRRV0KDMG/MVtYn4vdmeju9vp3rlKb4Qr0JKOxHmGVDZpZIAhE
ZNAPDhjsABwmliu8UWqKflUnscA02iVVOI8w3R8TVF2eTolVniUgs4NSwP/afwo8ek91bWmg0SpG
NULq4C/yvErMdq7kWHeE1kdrkscbSWdYKUJqfm6a5XXkt89K+VVb8HSu9BlupycyVM53fkHonQFn
rycmqeVfIG71NMdJJ92hFcU/lm+ExjUU2YIb6BfSP4bSawfup/4si1726kQFtLBSrQzsNibTzdKB
BAQD03TEvVUU5hbMfBLHDNG3BOT9KRJTF9tdhfnvwrjJkDa/Pp3aZBufaqLeK//LhttpQdTqgDfL
8mDVeDuMNrfEvvWhXBY5el/RCI6Y0qLFK5asEy5wlEPNydjngws/K019jmAf8EtTi3iZcNsmzKnT
u69uDfIC4kqFFBgmCGrc+JHNc8+fNsrOta1GxLu+0xXL8CbE8XAJelNykUBhztnAlpQD/QqgIeDn
OQeUyoFUu/aUBtHm5xCJtWn6tPuPKTuWCi4cA7QX/BUvxJitbjYTcY+rIVson9iapVF0WNQ4I5D3
OyB7LgEigHEYbvybXMO/C76Hv+8iHrwK4bQfbHuUawG4IFjaYV0qkr01/5z7PK1T7/B6maaP1QLU
PAVr6GjGBA5Sd8I2cdnfvX3f57g56jiBqyB0E6lwWkSG2hPe/VCCn8YKLJiusjLVJe82r2f1jeJa
i7zRVIQIYwArUsHFkMAwd/XBGanQBSg/kFzHKXk9jCAC0PRaUoHp7UfGjkGoxiVcpnF0W0+0UdN2
Ql2uDWI+YtEK4mtSLIelCXmmHxzm4pc201ar8H/3Z4yHqJeb7MyfUcAT52WCz6W/bS6k9a5NCw2A
2HWygCskEjY+Rh2UDixN8ht/N6pe/gBm4krpw0nX4LY3+DLrKD//NrahCiJSjwPOAw59kY/gUpUm
Lter6dry9v/qGMCLU3Ir7k23c2dVh2EQsJMFAzwYoQQ96G5yyqwJKcbGC4x51qWD/wTi4ZJ1qWsF
T2Knch+9/LpldVWtF0V4cDx7dJPoVJPpf/W6lCQ0KJr2xanfN9zwYrsH1Dmw/261aPgILuYzYnXz
QTVUQx1QEZExG8HPc7Q16dhDoPB6jjHfM/2ChwNbu+3sDqfKRYfmbzIwK8OW+Uy2WGOm3ngQqUWC
lFoLH0YM4zW0ytIX1bBxvLugy+1N2JQtnEeh0tXc4uP1BXzZLShXR9SZerGQ/pYOE3+XGv9E/zKC
gafSn025IFoP0Fn0+bmsv4fdwxeHBaTn53DJKmn2C5fyKR8XM11LHndeZqCFA8ztU27WQ4o4LJ9W
WPUtIu+wFxh8EDLI74Fww6dte/oKHBGp1mAYHxKG7Yv6LgZklUBAyj9OwlBgDrrTLLscwCbGtZVH
Y8VLZpEWcxahXZBH6S6lA3hfNoV5fJmyqPlJKtRNo6fdUoA/w9p4QsJEFDFqYsdDMQhCeKLr/RMY
5U7Y8k5F8Xr4DOi66oAwGcXuJzoJ5KD8o7OisH+of5GMsBMbtkzsX92mWJ/WgfpLQk43gm2tflLU
dIotcge9fmngLBF0LDKvWFRHIsuaZF+inDpP6APU04A26p8lJuIPJAiuk51v5bt46XcCCRHZURAx
EBUhAYkLbWXxWCF0GaOsvN9FzKuOL+m6hS3LGvsXSOrCF+afCjfJYIejiOY3RCE96TKMsSBswdi/
wV4R84v+FsbjCUOW/rkaRR8M0EFCuC+wUi0kpmOi61FIalub6MlyziTSVIGTPabhaxu/Hlsk6J/o
RXPyd2mZTLsK7UZsZfmAvdJq1JkjUdc3GO6EIB41TN19GS7NEvAWdMza8ZRttvJHNaOndA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
