// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_30_1 -prefix
//               design_4_c_shift_ram_30_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_30_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_30_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_30_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_30_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
PAni/QhYfuz5VyHVKCQQgw/uFMSahOokZYpiIbcIejdOfDBctSZ5ernpkvmewmZVBET2Aymekgnx
I/vMXaCGBbUHiIoDPV9faUBRGNXQEgpnwRlOFUkn1w9QB2AaGMfR/E5hn7PMhQCNpuKEDk85IJYn
nmoZrxfcAqkCpMjk2q2pRWRNzLFcw4asMzTguoPAcKLYo7Nv/2kVfbPfHWMXJmZOtr4T+Sg5yGgC
uSTJVMwFIDabaTWw+ICrqyTC+ZDGLVxFvQsUfkBcuiI0xfv0q7JV3QI5CD+kLSI3CUBAEGLCF7ju
PKpCjdoZUxNEZoasisQXtTlhT0Jfnv65j1I6D/3zcLc+AY2I2wtrRjsqNwb6nxQ8StaPxS9RYzQ2
h4z8HcuT51mJSKLKR85WFhhXLm85ArfemyVjeC8n0eNHkVXcbVey3q3oRyP4IXBfF8sPCOWitUcK
YqvZBn/b4XZFt3Kl7yDeArx6d/TJkgcS0pB+QlNIruAoS5XDxuhGfrenGtL8uB+3I6jr9N0TIOo0
HyHCK2c2Jk8jtPWOGJVnHy0aQakUth141pQOsMb1kssr5EtJuYsIlkFcZKVd5SEwU7HSpaZon24q
RQIqVCetIl25SZV8bSlvsn1QZh+YRDB+mcp+hmWJ8N+10S/OG+g5UCXbZf8n32XslVAwaZPQ69MY
3DdRcV96fBIsrzSQiJPMeSqOMPkzuB0T5DELoLxwXL75hTTVXXQkRzFWHFZDlfenuMR2Ce8b3R5Y
dQ7EUidBPJ4jWM49ws4VA5CrueILhckjEsdf0CvYJQYI91bqZQFxLQJvIanCuOjtV/nsnfjsci/f
dVGSLsKFjwLsboQ7sFjlXVsQvZ8gIaC9pU4jH92lin0IOE5dyXG+KvUDZdDq68KiV41ApwsbHUCH
FMDQdWt6+qhbNVEqUIwT6zYAgoqqQqXIK8dv3g9MJ4UYKxxmmwrtDf5E+5e8ae4v8PGXoFWKy52v
2W8IrF0EnlcsW6pGS68TXLhP/7qag3Y7U53lFCYsQDwcdVEbkV1sBL5pQHUlu0GiLbmlzvpbLGcC
rIAy7zOjCHSNHmfZMW3N0GJqXTPH70wUSw61SsNtH5nNHWglJA9w1WE7TkzTcyVak4NtM+IpmZfP
aT+LBP+1Q82ToVtRaFk1qDhswjqzZVablfWFiSpmo0DAsrTBz9y3DSY0MDmd4wu33GyTQH2a5p1r
aDmSU8BW8L6VqzW0Ff2jw3QI6rbihIpiv2fE3eSK7mZKU6ccfHCDPKXyGX44hq6qoCTNthvN2TIw
Ggy4i7bQz+JLWE0I+jaUmyegDOFGYIoB+odLg8MidihpzBRH/eEjoP+bADd4Ric9LxOq8iR4e/7s
PcIfRoG2IIFtYG0+qdTvomJsC5jqs7gzhdAGLbtcY6z8BidKsyxIVr2a/r0m8dbrwbglRB4B4ZX2
N9DLb1r7GDxcOCn0Y2LQpSSDoBzHnd2hbL3gx/uzPWyG2Skidji4ZuHrc1TqnXjvEF5NloHW0O98
EdsBeK+molQ0z1idWDInQFCGKG3LG/wxnC3Zm+lSDS01zyo1YYxvAJ8+VvVwelOrrZZDCb+3agXX
fHJAR1OnukWiK0G8856ouxZk3VYyjPm8Xo2PpMdYPsIhG9YIidKTzKMSdinKXqw6IxaCA++7RVHb
5x+vJzIC8LoHwJtxKxjdoN17Ui3rqCoAcKmQksvYlW5QrOnzRgBLE2GtOnLBkc46pEBJXAIELiLR
Q3Z2hJ9Sw/W2MZJ4RXt4uhzNHDHnVT6DbLabE4HVN7T6gr0GuHHv4VmEPKmzXSA7qfsgdd9imp37
PdJ5OHMcTtS5mCtyGq00ylqLD9XgVz7jkpiw7hT6+LmtNLNC4i3olhXrB/QgRXeuL/3PW1sWCNBv
Nd4gT9idc3lHiTyerBRPFCtWCpaIAs9aNgTlA0To3JgHX9FNXDt4kXfmHqSNRCd4toE1yEji1BJ4
2t3PggvZhBRFr6E2QaWMccVl6oxo1VS3aZqS8cOd0ezA70kekjnslvm5rtJXIP98oBOHRy5Xxr/5
rrNjvtgdHRFOCrwHalSDG73s02AmjiOg95ZJMKapoiimy1dCG7hXNM9d3l0dMxy9iQe6CuMfhZ19
nJYvdFTgHS6Q3+53TYS/euMlvIX0syAL18HU0g7mH1U+08tSsSalS1pQN774KkDJryevFH/Kljin
XMB2NM2ilH2MqWeHofU7Z9Wk841WHVkuGbRCPhE1jmaSlRtveTHErrG+Tns8Azaw9yJCw24wrsNI
ab0PeZWxn+L+Mt4FPwuuWYLmxHWqQtYS7Bum3/ZJEcx/JWmUqGveQnY4bgEANXdBl1X3iqyWzrXw
KxhEhXHmNvx3bqZmbe/848TBE2jBDwuAZKOEERBnJXzV0WYfSxQrf6YtG4EwYsfybRNA0sbQRlai
t+kL6kGRZganKCV7rRSCJaybNzlFXv3f2xiSpO3QdKUSCfq3MguqPwYsRA+D1npC5h4Px/y7vdyB
kwoC8eqUxb20skU5085X8UzBYtg/yzla9JE8PmlEoZ6DtDwE9m+l5qEb/hGGeH3FKiEhKeE8mBhF
EEvDPcNSfzDA8mhuDSe+x0S8u3xQrihRhTTt2A0M3yUWwV9h/sHQUXn8b+Uze+/Umzc0modAtQZW
3JnngidYhLhhvb2yezZV4lBVzeJvhI7oWvoD7oR8W5OhOBr4atgltgJUgdHyyKvy2sT55UMfD8Mb
ndhJZZd3sAKk4z8jbh/JcbICzWjtTjtmM5uGk8SDxwHwUgffeh1BSiH+32Gapgyw4KkXlgmBsP5S
3yJGrPzk33FRWomrAOttuhYBFSV9t01lH51NMMrkm3vN5nwVh1yvfTKvarfy7G6yudxuPjLjbREw
YcNu82yxth/NNYEsl4nnpPS7jIroDV5q8Q+hgc059IkjcN44XCHywyDyfmJKBE94Ya1YaHs6VY3a
Md7thW5etoDIpXO0bKLNF6NQq5811c7+iqJ+iK5tf4VemhxPbWWbMe3sVsKFJ0zT2uDijn4YdEDQ
8koIREiW0uA/mAyRs+KeL6vim22IlUc9P0FMsohGaXXCoxk6npEeSeRfVS+7V54jCCBCwk5wrwsn
GCZ1GOvn2jY8l6hfYH6g8fp9ppv39RXAlZ/L6aOdgwXpbT6glEqv73o3HfrskbH90fO1lCVJmDGe
3ouK8aiLeG2HOn+BwkCxH5fut9kAhQkfq17sDZmA4y8+PTwi4EHvNTN5VbsR0NFxZZ97pZt7Iirr
GysN0jIff0iHlq6peBl2U19fsUvQ6WsUTorbG1qo8PLHZ77T5HE81XIRdXBk7KussRU0PEJ0JavY
K8LitcLDV+Zoar3+Tfk/80ZKyCNUHnpI8iJp4ZTq/EzMZ/y72QxmOlnmO90qNt2+gLMX9uIQooNI
NxI1LZ2FRK9xlTSt//+CiZDxO0Cp9AtyFbpJVMvsa12FaYRxG4Tn9BUTsfypTmCw0ePl1cszx2Mv
7MWg09WIBuUsGNbKxxnZ+s/2xBd5LSHAKYw1fcyS6u7NuM8ISsCIl/1fHFblZVLpJMqQh4NP0rZu
ZbnaEdKbmolRAXRuA7cfCUNdm5eRd2mJIc+ldk7Jv8lRc3CLNHqok/aJtMIywekBk/XbYK2IrBvH
ADZYb49XDFe4vRBllp7S4V2/yQK8XqZz1gNEYUmXK7xDsd2Sf7QYSIxXX/sxXFDbhOMpO9VwENGc
bRtM/spe1MVAKSivSMtbXAh5FKxQqQdusO3FTmWvNfGvBZ5rKgOSGQPo6jWvOB42V7Y4LlvkYuvc
JrGscwCv4wdhLasV0LUx7qNHU1/sKTXOvhgyhRk5zuFt40B/lpisUrX41ux2pjnNdZMoZ2L66O0H
HvxCwqGyyTpZ/k4VLqaLPjuypvyNzWIZBtOBSSRzdaDr7XEd7mplo9fBWmClJvoAEcoSNWOfti1J
kU96pKYCx32SvKsTqNH3bNhziilqeq1aNW7eduldDrgacymeGufQugP1UFuHsM0g6exgUZeZo9lX
D56oOzcRHetRePSqZJSWJVy+qSzPrrXwHTw+m6v+mgvQAY7SNBNygtcmK1dqAIhS/yfRsjfRba95
gfostwQHR8RxUF1eeRAp3Cx4b64zn7AkMq+vfAB/NbRGeHAuItZWYkAaoCYiUoEEyvfXMSOhB3+N
HFGTqVR5rAjEK1sdUT6d470HNkDXKlqz8RhUjnABFMbaNBs4MPOlaKdNM5pmBS5QBLtrdbK1gGUe
8ELWr7sPewBbnjkWVCBLaKysmHZgsFjVQ5HxGmrhXsOEsnxx5hmZuOfcRjxVZ8d1wjJiHwVXB43e
N1Ax/vHeAIYBzX4l9wzBQ7pazDMOXGjh3k0TVkJFnuBzxyjPDLWwb6dWg/jdSfZuI+eWDcY08v4H
aDZ0XmoUhYwhNVcXDn6Y+ENjXo4Pd8/v/a+tgiOMXtEEXd8JhWDsVr0k/2jPWSm77ackKfNe3O1f
B8Hp+v+B9xG3tR5rojfW6Rj4qtImSdohomQ/67gP6JaTsO+uibUoueKhi3x2AEXGL9Xx8ok0DScW
plyoPvCu43UuDsLdWrM5cM5GW4KZCaafQ0dt0/SUAw5bQSRrqNvBrX3krZiHSMxLLlYnx5xnGdgu
dlaN48FJvsdBX8QCq9AEw9HNdSVFtOqoAUi89wI6aXFA/mpD3LC1CB4KLMLjavRnf6D5wCWs7Jd1
3XTSjrc9ibfNa8bNW7o7GZclVs2D2H/eAQf4EUP8C17h+KhvE9VOA05gP0Th2cqHKui99Wkmej6n
jOYePR0I+cMfFWQI8iJ5X8krzNhnLndut+tAlnVAeT98dedKxKjkKiIVFP3Y/qIWL66Oy+osZ64p
cNekFZYUvoCNC8K8+0adb74SZiuciu1fP97rOKzCDHmEgeQz2OMwbsDwZRIuKqqON6nXYbja2gBA
0b3SpO31eBs6Dag6EdiP4XRDzjqLjf85N8Q4zEsC1m5ILXoGFx2HALqb/8haj7rbXUWof8/AvU0E
W9cD1ZRRrJmFVfV+HFDBcpxvQSZxgOSTipSgZhf2F1lvwOVaSGifxgBKmt3FJOFMtVDiwFQf2V83
S33P8SRhbs50wUBafWxyIhN7keUDdif94/fmxAHCHEPO7GnWpway+fv4goeIsk4Yv5StoGQSaX+4
SOyAAKK2pQBjEKZCEyUCPY3+sEs5OHqppc2//IeI8rbhA0MYY8+gmWkBo79hmOarG/OiwqCIpBU+
d4Yr1OA2dKwlptUJqyGGswjPHibQQSsitQUwHNPHNn3Ev09h8reV/gvrSnY1/Qi3kyes/oK1VBUF
enmcFN37TN55H4qJoqi8iKvfEPLmIkL8FGiRn1gH1gwJM4FAezHZFAMBNGdoD2B/+p9Hdea/KtMx
tPtIIFUF9c9Ooi37kF/GaxDDuXSXa6RaXwa/l6s33w9r1OnyblGxkU3cMbTV1v8+UtBrARIbnc+u
TYqnueF9BQBQFASJuwAwmLIwToHWfLckRnlFqBwYVUkQ/pgf5WvwlCi1E+YAkrvT2BI9u00FCTH5
qGGZc8w7ojjkHcB5aQIrLKdTQhM3WexuUYP/q+ffE3GhHYwRv2VojLQTwzN1gXXHLeGfbWKyK/mV
LHhl9sPBsxTryd8OhE3a0EZsepvqVft1ARfeF3atsNEBwyUHDVJ0tp13cCJNd7FkVhNdEtz8ydIv
WklE6rz4/3whW2FFe4AHxgB7oV7P/7ZRG45zgpAOz3gr8pbC9+VnfHgHVgc6hazwaDHMGYByE3kJ
1vBzc2yH/9OpkSdPbpFQEEsABHz8qawFO8OPGiReRnme3Bw7A8R+a3K2Zz5MvaFnzfJPyv2KQHhl
nl3YAl8KeU5Oa/J08wph5L+sX2dw2yxNz6DZKu9Iqn8D5RZFDryBwtGk9OecYVJyqIcLtR1r5hII
o/l5xdju7oFZ6RGSc+WV2n3iv8Nu7D+XQMD3m61Wt+vlmYtDfBQWhD0yqwC1xFXGLGAAmrhqY6YN
bsKu99V3nLhiM9CQXErvu1Fo6K3I5oseO2RwTt5h2TQ0ii6Amcq1nJPeWaAqeo/1SfBbqwF2LClW
ou8lqXiSTgKEEVPUtQ0lSpk9alQ1WikrhegNSqAC9NJkVR7VwNYTcLoRKhdJXrREQ+xT3J3a2JYp
LzNa26KSF9FSKUyM/sZvnGYQprAXwlILmFh+gR6CRago/tiQ2Mh7DtDeIGe36t5eHgIQWN3xdqt2
T3m0tAz5Y/LmD4zkdwSCYLPt0QRMQ0EzV294Ym8u63WR/1NvIUmHBO13AqbHjNbs8sH6oMvvSuHz
w9rVM8mV44OXQkZvmuu84ApeXM1in+/eq9hWZhVevM2CQG1w0uJvxRmeMfezryiLZ9lsRkVKbHQw
UOEA26rQnSVrcJc68DH5Tzzy9SDqybL6EQVFiOAeJ3BoQeftIuX6OSFbYcI6U/NL0yZeSPJNEhuF
lbMUcfNKTZbQKxiDjBqR1NIRCuKGzr5a9N5OiwSW9cqpWlUoe4Ww9XX7e+5+Zvouy0wxNblu5mTb
oZ1iStTuTnBQ+ZkS91xwrpwlJsTKifBmQfFMxky8KRYRjnkycIFTKDDVAMHwo5BZ40CKyL5NbT/G
vr5sFShXY5Iqe787ikXonfbdtNRVNSG5kc/mGX0o/loVO9Yz9UFF+HP0q/gm1bGL0SK2aVUElfx3
wVoz6Muwnd8PL7zOcs8g/7s3znaI3b7WVMgvK3fB8K6HHvQPQXzU+DkEcn+P+pdctnlChi8KKioM
+IHp62Ahu0gH4xgr82sv1+b9KyTEDDl5Inv+1L2GhKwo6ow4kRF/VxKMpQ+N/4gi1TD3bbqymkY5
GJBEPgpTitPp3FU61dZzJ7wqtxkqrmW2uBBXZ2xbSrxhlSSDGU8T2kxLhqm6W5BFFVwoA9YU8XSu
h6KO+MFOgMFFgxJgtQPgSsucyW1Gzk8xOS0Uuamqc/f237ObNx3Wmv8TzEX09jcbdN2B5VocJjcM
xeaySGOBYt0ElaBXKBQg/EguesvXnNfD2urXUbgfrdqv+v2MjkmEpsGp0OQdK76kzvFWPNUeqt9i
l/76jEScyqZKph8MdsrsGM7gRLWVUBrin4eu5oRs93PqTx1lx8ecIAwfIyGZLZyGAliD4658W6hn
xDNbVcAj5mQZOZo+BsuZC97i65YrZKU8gLWX9SnrvqV5Ou2RmXdbJr1dUvZSIF+ahkxzL0SMeTsF
li3qYrOJPjqu4I1xwNyKnvx9JVfQgiZ0Pc7cv4uQJO8rBl8I0nsrWkerOO4G98qrgNrTW82gahnr
iS/6ZMKCoGCi4jKou8x6Sb6iD5G/FQSQgd5GOBdLzxPDT5/h1VmKjcAkNkHvtNCKqvhSjeUiXJk2
4gs7eKkaXHRAk1ICM5AAFBdxUsAnq98Y8xWyE4KDD+YUeJSoBQhqgXWhUCQ6+5urbheXeXp0MHfc
45mIm4AKqJ7UOPEQUc9k3VZ/m6k2OSckarG1ue/0q9ds0Tdkf6KM8VrmvrFcmP/2Q4vsG1BGWmDK
kQv60yla69n8EiUrHBrAbqlSDXWVajn0CjUY9TDyEd5VpuLCqbJ608USvzFyXdqtNjm4p1IDn6Ny
i+ivHrOzpiGIswGpym2lSis2wOCmv2JKWG3IyumEkWHLMqQpVc2g+n71Emi2wUvDPEeXpuyJrVYy
UTvbAVtAQkOOmZh6S1eM55PfXCtAVoTIqnXXNK4SSgSQ3KRjFSrJ7Rt6/dKxWoyLTeuqJbBEAb2i
Cihmzg0vVXMqhOJs10QWFUF070UVpyxqcQxclt469zK9ZcQ/GwWuavmKKQturePtXdg48+90laq6
a09517SFRlePm+xza0UeaAP24DAG41xuXmjzPyGgWImPudxriSREv2HJ4ijl/6KwGNo+EPTGbS8+
KxqlOhMgXlQkTk+6LSviQbRiCA1bSRlKCzruOt1SlZvxzPqlJh8RDWnx9gAMX24i5roJj5wTkDw5
zqcBxJzzMANQCk6s12wjCKeXRZhUVd72no+btACzpZ0qr25N6qTguX7tdehRRAJ6Arg/LFpBPimT
FKCCh3xnM2tZoMm9sFSzwhKpUNO+gDftONt8FTAZ+MY2cItsQ+L45tVE3FMPweqSIEVyA9LVUO2m
iuITTv4ZUrDozmrNJagDxN8Ecb54CD4hOsQc6LLJ+IclEu9W/IfbRHco0QUs377IoNlsPfrnKhHg
LKEtTT/g7+AuXVcPhntMZ8LIRBLaz4Nx1um7LoVbBTPYvBY/rcTEQePvfq9Jcwj9Cf6Pi3lqesXi
9AKBhblhTVuQblI/ENH9XjM6l8pSsVf8jNvnaMjNe1YMkOV85Zft2g1gn9sfg/9SAYGQ58lOjMj3
aCC2vm9M1NyeT9yGWjOHgrf9srb5/xVXejux2HMTAN5y8kdTCv07ypp4g5jze+PnmxP/N9qSx9pz
MbkfOGac7aNCxJsuXO84gex4HSRwwaaVD/ek9fZ+UThGflmomuT1ZBQ4wYtaL/G04OM9xIO/lXVk
8UluH2Tb26OVB4RvusM++J1+iaqoV6rBNXHlFrwj0vKVwPAiHUgautlrcUwipCax0LV1o2zH6e8E
bgyGFi9az9JE019lqgMGC/n6vgCZpiVaBqsvZrKRwQkgk7FxV6sdB2i+obvZ65Masu6KKt/DupvQ
2hkWpC1kFPuijyaiJtByi34qtmWsZgcTdhleHHmey8qD4Rnkomgoqeps5JGCewwmcCmDoHk+K/dA
RPZBS2MWbq3Qyrow4JZBIRmBt1JeW0duIFo+iOzU3s0J7WRl1+PQV4ULUuvL1HNrCNWUT6yAIvXO
L2R8pk9wRPdNvdHhh6ONPk6F7yTFTZrDi0xzsPCKyX3P5c3Zi619XG+q/2W9AVnpHPDdNK6wPEps
nBThjufp219Csap/PxGCF/PbyzBMnCeGQP//zhDEZzn66VohMeDKMi4QQkyAyZDjG4G7QFaxJN5l
jYh9SYuZbeNsy5u0WZtHyzdfpd73eV8WhZQ7JxmGuh7PvQIoDASIv9HkJ8PDfgrUe49UTVT950da
6cTENRE4+pXmMKxOvI5/qG7cYtPmgGip6t3VemTM8kgYQxjUr0g5g4k+pS+DfXbv2hkUxu5JD9aj
y/hFnSTpO61YCeMd5KVUrx4/fghO2izwtgsSRy2iHUaYdJzMacoBB9wLdMN+ue1FWpSBsYOPZFSa
Hnae6cJWkbV/krVYeagRaiILaqIZHGpKlqo8aIAA38T/QZEwOIdEuHL3h29g+jGE99lpzv03imxz
7NwIuAwClVRvpPVJrjvGS194NulR/WP01eP5Fg8KFviJ/qyTF79qowA5xp1ObBPWdj7FEv+M6Yda
mYg934Z9dmPWcxlZNRVWm2iRI1CLKa+S+PMOjrdmYNhYgv0tf8E3qrxN+q0NdFLekTsKfXYFYHxh
WmFGjP5qejL7tl9e3xWTmPErdyNirySbFXIIH0elVWwzYotsqNSBYA0zb9PTeBfFkT4Fciplk4wp
7SOFzv02ScxLBnclj7LEELQItzznjR8RZFqGsK1par1ekWtGGwddJjNYyPznavP5wQWJDOe+62AB
C+VDkBLvP8TfZqTezc1FeJo/t9mEuy9vaCybRQuVxMqKyXDSCH04Tq430XfkYtVvqaDNbafG1dsO
+ddX5hLvJQZ4nzjI1GQ3x3Xdo+osNOe4YdD10C76c8vH0P2QFD72EXvhxOysdo74mqeQxIFtXRlr
NvdI81lvqpevillC9jY7MkLzJXzD0cf8azvW1l8A4+5UITXbndbWW2tcRwgC9/8bnYnzS7Qnq8EG
4tqsmbk3/totEoz0E3n2U4xWhB6VF1TbpeBanbAKQs/YrJjVJrFNSMC4kzRGbOEj/tYdWwwavXdF
VvQerdOazXkb2fbFvEJOIrlWUgETw7B2CQyL+xehkUehkd7n8k+8WrF0esvvHjPK8cHt7Yjwog1w
YRTitazaBLVqfZsOgN/wsCXJ1/f6GjSRPNn2rxh2iNZ1nc5BhuPeFnLy7ova34RqGY6fDJSc861m
no6gktB8VytMCKlkTSkrfGEFG2EeaIVnQ9jvW6At7yG7U1Xf5enWRioPZLaV8kQ4GicwlCvw5Aj9
LwAjsbFF1XpnLFLlC4RlsPtE5RYJmxc9wZn7txEHDqlX0jzUnxryZ9d6AKqcrBDZ7zYwsdnV9mTn
fRFLJd68Yjgg3EAIpXqIukKFuQHop0nU3bXuJmH+QGXl9h308fH5Tr7YvBfFXu+CZjykHpeQmKL3
JkdCk9d4dr4oU2n7NT1dzpo7S8aILCqLuM8TqC0irlQ0yPmdneZnQgPGL1WsmB1e76xL0psNnuGQ
57yCwqZ4Q6vnfSBUFTMf+UYCtVqwukObSkk5EGbK42NTMY0rSbIuWInysDjwQEM9AUkCjrc8kkeR
iXwRsiujUD9NY2244F68yiXBQxyoWa9Q7N31vA5Ow27u63LeqEJs76K+DkHUb8LdxNbarw8EClad
wgspYguLfDp7VoU+Y/XIZ3iGHy+6KpDiMTIYFFM7tOt5HftVKDx3uYbGBzGlmIkD87p2fkVvPbf4
4JFUjlu+/QYz7OOKufy+qn2ok4U3VJX/40+us07S7NzACjOiyel4UuOjNY67cLOIWXXUTWfwBUge
zrLZ5UepI+4iUT3Ill27QMzNFelWgZLqKiiLCCCmHiW+jgVHw4LZqO9dIoPp+ElSdoFanOlWdYUE
xzOnPmfkfo3yPW74JgQTaUHKKsfP7Gk+jG+/DGZ7yJHNKWb+/Qrwyj6wRhC2e4FJ3jp6TGRjS5/K
QM4HMZDrT4+y+HuJAipDj9YzqAD7INaiJj/x1mmtmsq4JKBfOpENproRfMW0HcDJ8sQKg4A5Orxb
PPzc0zHgipoJJWHOEY8OPQ4pAC9ppNCq46SbdDFPrx+rhOLo+EA3yAB2B3fQqbVn9ZkHSj8UiEO9
8NwvZmY4psfyMa8BTJbgKEjoHf2YSvlWcFwti7nYWdE/mdbACdN7QmZsIvYwXpoZa1usvW4zY3Qr
uw1QkDblgtviDXLWbYc6nUA+hoCeEQGuwxuoIvUG2S4xjBjRa7FWJnVDZS045THDrKMyL+sYC2Uu
qQd47qHmPE5HzhGserhOpCSb+wGfhjOEXMGUoCCIPdZYX0XswVm80sit+VTwliMZ+SJPaxJseGjM
eDm2FivUJxY9nQ3Tf6Wv3tackQIEIy8F+nmFu38M1Fh8+ZFysrnMuPW+jpR6Uf2v0VgtFHhj9dwb
TJa+cBW8k7T1Shjh2OvhSib+Jfc6Yota47YYHtNhjnbtLQ3qogvL+682h5/GWWNzsBu3CfpA3ao3
YL1KbQ+ODWC6qrmCZIIFqm4Th0ax0TI1l2kdQwESn/n8xz10UmFfOWamshGtYAoHVnqjDHJJywsE
eH/shA98+7Vv8u4thiHv64gTIurWIe2upM8HP9VEhbnRLSU3VvvcO6btpzeqxoWrVIZK31PmBXK9
DOAULTafWb4LXTq7o3Pvp2ID2voQBpanlzbDffidu/e4shkFBud2fX5C+mHtIOYXfhTj6h+ZD2wX
P+iog1UdZv9c1r1A6FhoqQLrcjF/S+aAr4EkHz77MMaO8CN4E/7koXRkKoCjmLL5GimsCly87YA0
zk9flcohKCzoQIkuzSYZ+28/ZWIv47C0V2fH+mPQC1fii/GUPBl+GjhyTFYdDHcNNg8M6aptG+LI
kG78DZ7C6jNN0bXLAjo+8utGfqv6hV28W08pEuo8R4bhAQBsQ7MvrOqmSUkE55UIN4BfHB1Xzg7l
RD3psrKl8TKydjehLzUfSR0I0VIJOCfTW7gKvXSPGHNHgKyHiJhT7d7WYzEx+mMhCCyGKsRY9DW7
eG+dZyjTPLjvDAA16zSfZvGirUt3h0k8NzRsPxfeJECA4gjcjtsnGKs5V0ommfdUYntYwuyqXGDC
nbwlqVO/3AVixN/ZzKRv+hYXSG6eaX2OABx1AED6Q/bDd4ALBzYFocw1YpLSkJqMkhwOs4FrqGEI
gA8rq7V5HjnAQEZ6o2xARQn5TjECmxy59MthXFLMj3x2mBeSYTVuLMhvt6XNdq8enCQYThZ3x58J
u2Fsd287mNglQwIBHf4k4a7Vj/nB6jj1ymvzbg7rEVmbfv5WT/W/il6VOk04wHytNX+4y3TSbmLi
SG4gSltx2ekEvKMtY1BrdeW4hzSPcQEkQk436fxx5TTRN5leRIcDJ7dIBlaTkCcCMVeEJI9MIyzD
vQdJ0t7lwM0ZRrYR5rFQhVkiGM7lllB4LrtZSC2yq9uVdVzIge55Haa+gHYTZX6gcS8d7t4Vu3iQ
3l/IIoLyrllM2Gp7M/soqNmlLZiaPh4GZH5Wh289cXXrXzNs5vsPiiYrCFVRHHf14b7R3pxtKA6n
v3+Z18Ly/ydjzF6uBoKqrfgIdwu982kAF1+XeS6JPGEPuslrpjtYI4hvbXdgIedNikh44cdOxBvJ
HW3qBh6Kg8mO0sw7aBDVWmWwc0lHaiH0ySQJH3WJwhToEbp7zSGR8ANbXuGozfOZY2iYrQ5GVmdD
K9Ix8P80PP7UZ0+w2UCr7g0u1ahZJqsIUHJXMgxS5fIineF6JqYsH57jAOBWulhGSUsk
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
