// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_31_1 -prefix
//               design_4_c_shift_ram_31_1_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_31_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_31_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_31_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_31_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
tya+Aro5ugxWUpWXIWysQaVoQe30ozmVOcyWTd/pxUMkiX99vLJ95P7vuOAZtp8CQhEet9iK7mOs
T3bH90hfiHVEQ3Xdh2/7pwiQbT86pQpPRRnk5u3FLvNBj9GVbtGUCJsEwKCZhH43kR7AHhHTjnyo
w4XewtG2hfXH9o3cvDQHtk6jnDdXeajGozDWTtICRk3pfhJGJEeC2Q14neCcjEd+0KetkKDZk3Di
uJlHMRmteYBv+ubNbEy5yJQAGHObxE5H7o6Qbq9limhXnilMmnxoLYul+NqpGtBdmizUsGDCCMmq
Wn3B0k3i2maEc14RjJki/nJuHRtUet1bMoEZFk7n3SEww4EBM5GdV4qtlocnM7Q40jGl4C4heX5P
8X2GEZ17zEjescoDiWeengCMrsE0veOp0j2A5ue5xKY1o+6g4OywYpet1fqNo3kQLs+cxM+FDed3
cVDtxW3v1QzLF8bd4sKw2AheepaDFyR6cVF53mz9Wb4UCTCB5DpzikYSg0EbffBIT5qy2OSqvNlL
/zgjJqTooSaGhmjV40wkJuFPCpcfHa1Rukb7tVx2amHgWw2k36GJLTpgUPKr7gxZHPQXWJgZX2eP
4jus8zaqmaBlWgiY2XqwWGfSvwpPR7Y5t5BkzQPJ7rKV2HgDPD0JdQ/IU6e8IrJ5LxYIDAGmygkJ
G8ubR33ljGAGgRGtjCA6H/Cgya9jQLxtUxDFyr+WZS9TwL8f9bLsWjJd8nHUwXoOaAWeRDrql//6
NfJ6SbILrA36BH9DAxx36EsedwtPevD73QxcMKbOwJKJuhRCmAXgQRaJniBgJTpqxE+JWGfNaWtk
gXQiJdj6efLvV5/nWPbORjuYmoxKysAe1HCw7w4o34EmcBZcvLNAp8iprxdY8pMd8ZcuSYOrPVaK
QWGjx+sUuv0eDC8uae8lzYoVNeUn40lDUMrxSnhwEluKP447xGUgYmdjU8IIHf5s91oTButiF5VA
Ehr954UZYoUp/TmvmICp9IChAtqUO67FWPZTC8gJLJLiSSPA7SnjT1sRW0O2AxCdtGZjAuK3PqcC
On03c4uG4Gy5P6yEkS3vnbwkB7vYq3D8TmuDlEEVZjk6DqFSjUGtRKdHRG97+Ehxo/3n/lkBFXRK
wsMwd+Ycdayq7l+T07RRbAEsHtWzGBeP0KIf0mA9YegRs2anj8l/HwiUzO2uiH635VIbIXwDK31m
b0XxyBh127PqqG5cgdpntR76D7EJUZY07OWsSL0+cW/c/NcJKuEHiLfNmZBec/80OAhi55yvPMt9
8aPEk96BSDatSKtl9BRmo6IyAPwR1Phl269zdmOjlpYe9rtxtue62r61t/ojDBvVBYGGT6Wym3K6
lslr3IfEUba6jp4W65Jzdl23sw4Q2raDrVgCXl7mtdiVceR1Vu2pynBqdLcoUHQ7/79xkneknyhu
2RCsoZBQZCzoBXFApr46+4LOLxKkdcYJxMR86zwene8HEffjneuMWDQhZ8PISXYOKuaHhIIUcXSJ
WerU/GQP4VJMhLY4pL989UhCzf75ig+RbLsRtmB1cD363TfuT88wfNvaboDzdm1KRbNZN+A+6r+B
voSd3at7EPGRlXtsqenmJPJWSAC2Mh/iJPyGY1jLzVWWXHfnAMLIzDcRO7YqslpmT8uZLL+roqQ8
ySMjkefdlna2CeXjB0R4r62a6ETU1hhpWbmBxdS9Z0kKl1KhM9HiUIdwNau4adeWcm9U0VIA2Nv3
XpS0ImNn1UgFzLR3wdQohwNUR9p+5XhbDHoMeZxor8V4yVsCwTGqhxKhR2fY/1qLFdMO3NQNGObK
bx8bQoKrw0hUAXH2H5XPbbm02RBug8ek4RsEtcVxg9zHWMBpqPIqvq26LywCSL+B+XyrIWMXYe+X
5+jc8Rs3om8TAF6tIGFYZmub0VRybIBGXjMCOUwbaqTjJJTnLGYXEKtFro1K3AS/8FwMcE6eXqqv
FkUfGcqCdQpARopUWJpZlLDiDzJQeE6osnyZonk6tTl1eR6dENlQFROUEDtgoPeZWI1YuMfGMPp1
0FyaWg3NOB77cGYDa7dLuLCu46hC0eJZQOe2Jrqp8lHUOEhsQKXBtUrfn5j52ANPtx3VRjH63D/r
9Ubary4Fn/rcz+v9pzo+Hd5pdqtp0GmKa4QqyhBiRu+blcqRNu3VtI+PEANS9C8zvNOQqRZAK/m5
14En2EDnVb654+tkXgMsgpQIK1msGewGjwUz0Z3Fg/NjjguoiEWO8DPcfXm1suBnObpPm0pg0egS
Z8gx8twqzwXQj6xHn1QxYVDjMeNbaCq46c05Um17Yl3qpMCDMmQZVYCmlmqnvue6QNH3lzriLpQo
JiWpq5OSPJUz9+sgHXbPhAhjRHHVW9mi2NOF0f1ChjX5UuLfIEBStsUpUphMFL1dM/IAERh8kgWN
pmUh+AMP0oHuMhgMMsoiFyuaJfoQscBR2+CEaWjPtbFy5ZjD8yUCeVP1bUqMQx1Vm/QIbhGGz6mi
9AhImd/T9EJCRpDMfYgexb5g8OsmcM4OaDpwS0qrmdeA2dO8shFKX/Ukdu28KAg/ukQLFdckFlga
iWBk5ggjJkjJU8dl3SmzLQzC0vx59j3ro3CpHhTy5wVGAxmJfkdQr7qN4sprbM8lBBD+cHukbYjJ
0qJY106y86Osn+CZrRMPEiVRBLsVIpr2j69FCjBB3u1wBZL+5og7pu6ZgGA+UaY+JrQuuFOXLiTM
Aj1NkEpn/Ci6D1F/RoNtJ1zgFuynH54iP80B7zSi4vOKoHLhzCbMo1KGbB67T/mAavUp3K5RDnbS
tFcSTYqIEhz9VzPNu/Z3H58doLFR6RHS0z5rcns51+4dF47mLXnogM4bzQBJEVYZ5UOiqQFGQ0RE
e7dAJKl22qZQQJwqARKme/kRf5yyIemQ/UqNqLWXjdXnBPkPE6LYb8+vnWfCq36hUwoiioXBlcNT
ePBxNJ2RCoQPEKOnt2o2H0Q0hEICC/IZVDXlLj/7U47NLSJUg6Oie49w5RRE0b5FqLaR6iesjH9g
ccnsFfmidfg2KUIi4feNVNNMJZiqzudwA6AuQNEUpArnYuL6T8lB+eRK+ktOMmIFWyz1cRrZm0hU
DT6XaggRkGt9E+oAs+1vQblil50vdUMQPMy9PYPQ1Xods9RdIHADuuBw0nbro3C6m+9RnAN/RIW+
qoa8oehrxpuSKwxBrXteR+LIIGqWqFA6Ci1yRnSvrCCL7hPV6CC+wzsdG23QLv3yGGggD+YnSWut
bIFIAkeCMKjuYqlUPf/6re6UBojdAIgUFvb5wadm4dwoEh6827PzLJDMLjy0dxKSLoGzNewYt+pk
lw2GtcVu8CsCz4h/Gc5ub/ok9B2iendr3JyHwj2yGmX5Ck4Hy4BE8F7dvOKlj9xynSEqsWzCaH2Z
wbJH8w58qojxikhYB9ypsbspxIHKuYg8GMK5DK2YmG7nyt9+jJGH3yiU+9xdc7VXfrV7el7eYD/P
wEpPgph6HdM9pliSahYKll4lQQz6qTU8UUJ6bYmH3z2wPUBVNm1x72C8T0il1xsHU1q4gKTvPLvg
LBLNBjbD/NDPLLXD8UJQsHrytX/1fEU0N32zWSVpuj6UR6UKYdTSDuPX9MOMcAKYOaIvRF/q0ziU
E3voYBZOCGYfQQ5q8NetD+ag/rzsyr1treWxbK0YuTYO0XwMo0GCWBKKqE35tgIOuizrIjwGzyM/
l1sjdoGoGwzrQPHQZIEOiGU0fzy0cfaG+PrcbeACN0rZofCnLbjSlI9MJ4DXMo9vnAtWxK0xFzFp
2AspvQTs0URKVxZ89aC8/2tx3UK0uXIKsFiuaNsasZYsCmUXSf7moWdClPIqpHyyBFTgctzwao4m
bzqpqfUhpWj0cc+mMnahhrXFWVo9alBhO7i3Xf2P8gNk7Rot7IXwuhuuQ21PXhk9Bepm873dUh40
ciE1I4FrLmNE/lingC43OQyjeCTNsdR+2oss7rPGhgdfiIgcGcdLmzv5UBRrtsjZdFLRE97FlkfJ
BesMkSWryi4X+ThcdPHzI8/D/KnH3b/iv29jLcWvNfsoEIfpXLGKXr+gE66Fv2/KlH45LregAJOz
bxAPE5V6RbN0S6txkbwmg65Gif9of95ZT8aAgC/O2FtMydEE2dkypKDhzdysjtvPqQGo7sncgx4h
ufP4o+8CUdrnL+rbPjJfE+R82JcDcLDsdqPFvtvOUvcEurmRWHN+xyFw/ADf3mlsb+OXkd/RkrX1
oxNlMH811Jg3Wf1+CqDBArqT6+oKg2qloofSYzKbIDfpCLpd6XP2PZjQNjH31xVUMylR5pE2oFI/
puvp71ZnUs39fY02o3gsSqYNsax3DvTrEOGyV1NQsZcNX3783ksxnkwUHvAPiplxSJEgPbSo6XMb
Q3SsQodU3XgT3dDcPspecSiDxDx5qezp/1KxZkiudr7Og22p+FZdmfroCPBDus+W3hd1eCfVsKhk
hZchv6UhsysgggCSxxvGU+5AxAhlGrylcE0eHPWT1vnMjl3jZGp52OQ2B35SmZXtaH/bCNJXq6si
MuWfX16rLOVKZ1cbe94I1DGW+r2n5gGDDO6V1n983pCgjpUJNfaEG1DzsEynX4alhOpcBTZ4DMgl
JPMUr1cfpE7UabACh+K8BFI21z2meVEtJbB/s+olsVxlzCmLuN2yzGycKiB5e0r54YiFBBmiCSMF
D6ObBXFmD3Z83iO1d1apiL7hhOSefcxsxbPz2FT9vrgAzHXCxyQnAKLrhMtDdGf8TbkQ7IjwixZy
xVWxnRX5qtQDaGkNWqxvelS00ncT7r330yMYCwabD5c3O8exUhxiySF5iai9G4bhjpAiuCkc9bHo
djXEdklBwvhdXyuMMRjDvAH9ZH5VkvF+DymiM/gQjkw2TGnh1vE8tpkd2LzvNCUruLEw4m3C4/uK
jOgJhnPtYzTQNj7phI5JhnCGtO564ZpY/Ce4LOYHiTxCinsG/2OuhzoaaJo17VqBqz/fhS+JIOWo
VT7BKuIthOk3Et/D/5apUg0Ep4rrAK3rng11KeGiSVzZ25FG059V3sw5X/8GZ5Agfk7IrbM0qkvZ
UtbX7PexGBqnERGC87CpMV409xRO3NjICdOjJse/sAif0Aoq7R5zD5NgpalM75EzlFCBlinQaXrT
gM6EJi+7sRgcl7w5Vm8+qb8JUtNdjAyHEm+5Cko/d/DsRT7cSeXUp0QIAB8mwh/4vZ/iq1gxgzbe
pKcX9gtSeTdKaD0cjwj7K8ZM5cVbMreKrnnIDNGowp+AH7Y2xh8RZYsukNNIPtcc2sDFz22nnGkS
CXPNlrZtpN9a+AroQnrCDTJZtVifhlm74+0VTke/dOEUury2sZdxoJPPqynbVNj6JjQXnO0a6PG6
iCELtnAxR3kKP65dABnnjTSzjnblWVit7oApr2uhvpIqQynd4B98dKBFOVqUaMBISOyC7CKvMNXR
v0mRtz3vYn58/zyikRszZlvqpZXh9M4VsKbn8DyB5xZ0bI+jn04cCuLfY2avZyz7saX3cotom76f
BUQcS4Ivus2Q2EIPMkXZRSfElZlnGgMuG4HkBXpktVUC7H3Q4k4lIl5e5GCGex5rSMdVWby86f8y
LacSU13w7v1MJp+vKTzKoehr/LOzVjNV9xsK1aPjEu5dT/xBVk5XSD2whATGHcHHZD1mxCg8A8yi
3n3GzSYgMHyzyAceQ73QoQrfkT7h4pCktvcig20p60vf7a04uMcpVMD3bo4BpJPdUz8c1MdTVq/2
m97tGHdYxInqZNz0LNYKt7HgFLJwSvAPcM+rWtqaIF4QDNr9UJ25Aa4GWdi8mKFkZUGC3BpSlFEY
f0TmZohO5WDcm5TXE6udRTaVlHayUDLZVXZsuJdKDwQw64sJAug/bFbBXDC5QCgpzVnAvP6h1eGc
k8ew4ySEKsP/wF0TpdogE3LClmny5LrxrL6Js3DG+3UycPR/Kfs9ep+oFXERBRLsunIS8a+4NtUt
F3Q+JnnjJAqTEnDcksBXU1cFXPwIxUrVe5WN4FoxDlRKNbmf624fFGlch86XqXyUZelmzxoKeaoG
olHtIJnCVr+Ve7NBg7GAJwgR0E5yUiHlkQ6CvGJTjlGZFwc/oKpE1ckD6IYOoXvFuWut3m+azQ13
1ubjTmZuJGehmrkzmIJg4aBQ8jlCrn9wG+alMidQBacRulS4Zledgn6snvjBT4kCMO8y383ex2CQ
RlLaWcCli1XlibPjet2n9mrJWqkn72lfu8q8amS/0q/ztpJpambrtlc1KZjcWkO+AGqWYtWWvwrh
BUDZMViQxRt+eMJgo44f7fy+9UVFaoYoSGK1x5M5KxvuR1bksmUNpZBVbjdUjGiM/xf/KJp8id1I
Xe2EpqhRbVfr7EXCTqDKl+LBtQzrf5RJCWlrt5XIPJzewHjkXCF/39c+Xt4Ads1mGFF0tdA3jEyl
BUG6UC7Kj34k4m6YEduy9hQivvAvV3l/QzRJOwq9d127amuD8H/1tE8YEMVaWGV6EdbmXDGetqUc
/ZtsiOrVG9CndL/OHVBTrWq4XJaLJUlu0Vf3JDfBbdqRu7rIlG6A07ChLvSbzds9SveQV/YFaAQT
gJHGMRnCYBZOz0CaY2C1YfKFrOpvM15cB9T+sLFgDR4Luz/E7m9fK8Uy/IMEug+kFHyqZuEFU6lq
vdvJYouJML+fGvv3LprX1fzWhKt8M6fMPUqpBLx8UWS1yxi4TZYDlOUEbo4yBxT7qlSHAO7yREIx
BJuljUCQEuJV067rE0VlgYvssqgnLYdzvAu31oN/erRPvXd60fu8DudpuTYnXcm0oWeNyzVbLVEo
QWuetpcUUB5JctMS6T7HpwvFyI0cDvBHHLL9XwWQ+f2JlQ+F2klWOqVAqj95oRrDHcTECLqKNy32
fSQ1GZEglAFXfa5O1NvpLx4RL5xNRBVeyP3cMwh08ADqrLdedzCY8kDUo13BTbySVWbohiqtn4Zm
XDH01mYEAQujhoIYKixm4Gp6GItlHq0CkLdFETkqmYhO4JX5npoP8ZP+nhhJ4C+JUEYZRkmuvQTR
i9qEHmExCkeaHesJ/2N5tmIP1t78q2JZznCJUh7o0GTxVD71jCFpvUZDs6hTPMGojGaZVLBvcwe4
01EcZInHNcM86iuEKP4/zxwLp8Q0lQ9QaEgARL1DabVybhruF91e6uNmb6cqmHty/4MbjMYTA5Em
/ZDOYwvKnCG7bztywTf3llDd2OB2uFGpVpEqXZnstGFDbU9s3+PMZjBU2RojSDB7aFnswW4xW+FY
CxjDW24pxcjNp4gMp64rNyx3FJ4DLOMWP7cXdm59i+PRUZCF86/0SNqGKZslh3mJVxYaCssUimv+
SZtdlR1fKyLT1gviWouUsYRWgc4hjunva0sAwHOt+Nk6TdrcN5X4ISvJLhB5a3ncJ6t+k6H9EYR3
d9HP0YkqfdgtuaOjNzZlaYxnsNy8VC/ihhKNnz8NQyyUfk8WtRx7y+dTFmSS2KobjBDzQhDQ6P0g
82qeOdOMx/SgJYaxuTN9Rvaxl+J/PkJBPDRLcrO1eX5xL01S/ozPYfe/KHAtdZu1/G/H5OsMAl/J
7v5H7ibOYAcdGFFQsWWxVk9ot8UXv5xTXifv0APIQdyVVfitnI4hH4GatYAYZUZxgz0dYJg//q+f
GhTB7JHBFlrZN7Lw6CmvWSyT9xMvxwaTVtXPYuEfouhK6SL1Cyc6IDQfUTn2z3hGIybQ/trqVpNT
ENH+3gJdUvp51dI/h6W0fr4v9hStFkJj/Bt1nyzneq7eSXfFYWE0I9+SBLzPNI7f1oiajPv6r01u
bLtMIUhy0fh/NTV+7qGbpJV6AlyPAvOMixOvDw4f2NwlHgrQ+AHprn9r+p5aFwm1eXTJ6spP5s9r
AIinI/utOy+cS219zTf3nzARAAPvyrQZ75xhGgRrrAYrMsOcbTeQfuZQuzY5QcuaSI1Ce+6xsvaX
vPLANXe1vgL5yUftZSKqimq+EQxyrenUftrmLGZ+57yqcGMuKjcz5hJLB1asalNJdQ0if6CD6m40
KMj1uV3xnLD9GJL9ZSAYbHLFjxxXA1s23AOqpaGBNsaCSR79mtYWqJr0qaWSp0BWBwW9twviiF09
5GdUt/mUqpunlfUrPcpf0fuu4EnMhlDUxnFlanHQ6gE1q4zOaG0sZHRcKpuTKjJ6LAdh+hdqSWr/
L17Z6LbL4sGU0l4o/7FS7LimfOuiwsesAUSxo7Sqdh4qh7y5nQiXL8c9GBWbh7E01lzmpzTFo027
jXBbGBrmVFmkIVU+rucOVRn6eTn9rpi/iqr/CHbQH6u1INSAyEVujsZsPC8ITjOr+AxZuUPyW0qf
yNmVobtG/a8VtYnYcPi9SPe3S2kGsHwa0gSujwxon6UBOk8ApyVYNqj4HfY0KM4+ClrS38axcwTT
riRWkKlWjPPzPKnuETQ7h5D1kthEKk7hAs4UVb45mMXM6HPpDx7HnGiGJQbrXynSFX0LfSPb1xnK
N7DaqP0yl67gIVfczIezGogcrlKceeEeD/5iZdA9sMGHjU6BDpApZXGXaqrPIFraINh3r20w76h4
rZVNjhi4mdJ02y+NH5/VDuxhuxIxNzn4gH1dY2nOT8sSaAA6o9OOv0oOY2jOvmkAuBwDIi3RIJoT
zxrBTWYyaKokbgxes68hGxI3+0NOhXcQag84q3FT6MyqU4Mdz/xZRxpCJGpVGN8MILkvOKs5FdXq
o76fQYjrRpXxuVp3mhIN9/CHJTkLn30oURCcx3ChfsYeEOONxBWm/dlSILG7fvzQZmTpFJVk0lzz
xgj51hX6/OdqMGCNnDtit6SOP6uGdQwukBKxwdKnjE4WB0NhcKeLAipPs+hA2fxLXLglE40xD3xn
aIdgIPfDCuAEdIuPT8VXAbRFqddXQ0+818JHEd90sbfjcLzCD9zjFXTQq8iwfdpTeUeFyBHXoi1/
X80qzrdM9TyhX5fPNlJK16boaezhSXVyP4v8qABhmI03CUH3fIMKMVk1fls1k4bM8gf/fmiefMUy
yJ9Kmke2mbABb06qocKEzfJPkJ2FJoo4SXcjfU3lQ/C/YcvwtQ7OM1ugE5+1+6nepqKUcvDo/5AJ
1Nzms5ydFR85XKeWs2HCnFUESOayoAijBkI5FWqzeGszWtD96PhvFWk/EjiEZdfCnFLe8SqyhSVy
8KjNAWUedLL5MzcnxpRBklCWi4NjeV9i/ZJSIvhPEax2hqb9wmhdJs/5REScDm6+h+i1rX6788QO
auQDIEJebs84ckPasQ+1xA54GQYxLUxeVowRXj7xdKe0w/RR6TOjG73w7F0mK9WqaiZcsdiaXxwl
8Z80hh1MapkdxjyEdDJsmHD0dHIlE6xUb0nCXQ+y0r0wp1XXTr4vizILRGCwrBbsZkcPoxbo7x0N
HG69U1L3DXYEtImqkyT9Z30IdOSbmXWMycc+JuBF7sSGW1adM7MtxR0PRPLwp0JIMeSxmnSfU4Wf
6ha9Cj5e4ZrJVBlV5x1BODEHyd6BLvFOUSFKeESG8mGDoOaUSflofK9qwUFM7BS1uXzbtrxjhl7X
v/n3G4M9rliLAYDYIkXa9CYeItAqIH7hZ9AVL5dFNQrbaxP9yRokUiwqjodrzr2URK9N4B1k+ZpQ
5sTZ+8i9upWkt77CUWET9ATaWrG3QgyBFjHVkdTjwNFKy6RU2dhgxdzpjcL9sb9Hpw04eYBgttUY
vbVQ3cQcCgfVrrN+9TicLc2xu/tYVc8NKn/CM1KSIwAlscs1bvNoacQGv8qdplp2I+zC/FX8SFLj
bZP9vzq6QGig4Vr3ViMQi1d20wn+jdmeXuAulJ8GYq52rlyO21GxJpdJs/JamI334LF4joMnr/Mg
LAVCGi055o4UHWc3GereQfapdIzq8Va/ozMdoZ9Y4TrtPpL2q654W6JZohhOUdUb8/SkH5PUuvSc
dDuEYZqtH4hYBaFLBpNzkNGcIURNfXSJ/ejpMmoG/RGpVJvZhDwbRhllAm8koGvguCwCnHw4n8rb
lCaTgt2jPLJjRwHlR0IIsB0xDn6MoZNhitH3nTnc/9sIKBhZahNIC1SYOwfRgxtQzpLKZdonykNE
R/8H5PJpi62HHECLKu03Pxux5Rz7OnUQhhVHMiTUQX/gnSisrWYOue3RIt3cBszjfbt7vphgHjG6
eXdTwBPYxId7zbYsKxCgM6dPp5oYB/3PX7XWqkR57YqiLa98GtXL0w9P4I71y9MZeJTM0eGngQgy
DQaHByePYA/MC7mC4iz1JaRXc64fiTwWkmiJg/lVOEQTq/UIjjRwA8Gp+vu3a4SHAfcQQ+OZKz7z
OEDkJ1Y9tTcdgQrfR52VKb2ka38ZT37muDRz10nqqjz+3rO1iU0i6ukzbZzl8uBpwOSMXMa/7gFZ
4XNqeaK7+VXKwux74voBJoYbr9B1cjNLYg+m/z4Gh7plFT1ErmdVC/KPE8LLbp5+0o3rASkJTjXV
higI/s9SiBmbwThJmXGY1PdJvNQMJ0OSDADmjO3Nfj3nhIQM66HYK/3A8Hsq5pWpf9rAwZa2fDbw
rOLTSeIGnedYqiYEe+BrbrPP/DrZZyGb7U7ml0nDubuzedaJ+C/ZnJSdEgxhkTP7z8QakhniypAY
pc6Asemh51SfQbjJJZyDKSvPvH7LOWwTormQMr6Hd5DYevn3hzgT2uMmKF/+Bq40y95BxFXtq5EN
rG5HUFQvhxjsOVeiHiA4uLQXqOE/iT0tpwqKvX37nxorYtQwqfsKtjXrZ7DUcFDzT6AZ50KkMmxV
kN4jZfy8ojzlXk3ChfbHP9GKWcDyGFpmXRY5BFuhopxywPXENtKDCSucKHwO/ZOpXiV1sB+z0iaP
qcm785ie5AzoZeh6Za2rj0it/NBbmv4R5o7qP414Vkhyct7AShv4EQOhXvIT8zAXYuqVsV29co7u
/3mLMUhXocoW6r51b0em68DB9Ro9otOgq81RbAZ+++SK5FFihP2XLV6/Z8RBiT2/ysXlkWru5TBT
SbBOu1640AI3PivuN8WxpavUtmLXlbEoGetb+7AYA8IH/wWyLKjaXJ3F6adwPYo3JxW0Qfb8qGUF
ps8US5t32ufMUWYylTahwat/y3dWI6qRJ9Fq8zQCh7G864NX09aaHd1+7R8hJyArGGzElbI1tW0w
pP/JG8FMtfB0mA+AlsxzCW8xVHV2j5mYx75F3Ua0zGOXRJb8LzMX5Crz0zk7r+fRjDYBdrtYWnE4
/QpCiRnT7wLQiTkGT4db+sozmHktakYlFU23u+48rvTJPwJN5eInIaE/qqz27lYBCwjru85UldFb
ajdSczKhJR5wocjUE8xX2/7ee+bR/QlHORy49WY9uuX+B8Mo5a/wJPk9BYsCCobfbCm2dW4K/r40
QS0RN/sTUOFX7KoXjDPhHzcVwh1GZ9SVnY7blsUbg+KcN1JcqbqRV4sfOOS8cFoXTYy/L29yVNg5
kxGxrgVzXihOKL52puF6UsC6Q2v1jLQKS2dMScFzYRHUU+AiaAyKE+wOSMxJY4WjCj5Z9a5LtBAd
JAQyOZRMW2Dvn6lyianwrnGW2uOLe2Dd5nij9j5psAu3I9HaBue96l/gS+vyrzfzv1kRPFc4TTyv
I0kclC9khzzOzZp3rYG8/MuO0KY10BHVWtf2kn3O7ZgLuDm7/c8kRRJuDPmAG/y3JHj7LznHPg8k
3BbCTOTiFhY80VTtNfv8e91pEw5rRFaRC9v7pyNvJ3gwwsrEKM8Hmqk7MhDf9Iy6lssqelz4W9Pe
+AHWig1B/Wrdo3ZpqHaWAXAAtvL2qFGa9PCv7aU6lmcW/R5/B9O1hV6giDvoY0tNik3dlX5i6iGK
aZSbgWa4MVVV33oqIm4jJXT2MicjWbCherO1zcz+nYxFfZ69z6EXdwaQW0iLpqv3pJrGzM53nlJ/
95BKA7329XzL4/Yj0YCsg9UjNnP9vyWQDa2d15IAX82TOaScEga+g0ElaymNi8sGSLdyOqAZ/39N
A+DzhF7u4Bxmaxd69WvJIrKVWb8qWChsHThO0t5pZ3I4xJvPg2n57Uj+San2ck45hOI8mJez3TiV
9Xvq6YJQhepaHt8tEAzd8EtaBhWvGD16g5VBwkSTIqqqUCx2552/KZIfEMFg6XTJsIHCQGIYfhpe
ljkgAuAUaGolmMnxS29myyc/ED8xuAaGuwZ+fCPpkaueHnyk+a3KJ4UZOw+XjjOBK/CiOBc88rqr
0oF39pREOg8p9NH58NZtTf9C3YbPQ3PWMP0GrXn/Dw4g1uo8BjKQls+NA3UFPm+nL+wq9GvveNhe
PJSPUOnFidIIH4JXhEodR+Zk+sCJygrz/gS01TO8Tr5EV0IJ5+JhSKi1IruLdrBgQ+IZD6F4u9sP
u76IaM4S1cln13+Jr7b9s31CZq/rI5Ok4IHPIfD7uTUc9pw0JY+hOdoYGlN/diStGlTWDI3+/hKg
5DFWJiV2fI2sZ//8Ie/xwG+rc9Ui9kqUnDgODa8CYtBLbIoFiKyVxw+2Cx6FkepVH+b43hXU/+zm
ogo3Z1ealZh54JNi4Q9w8Zb44v2ATjA9EFFh+nqBTBZZS40m7fgulIap8XzrFcDLfNpCcFCJTx5v
46CImLi1tY6Z/A5XeyTzgyx4p+6lFX16VKnySGLZhOIeKlzabwTCmJ8tGsQSIo4vjsz3FPgwVhXj
Ty8X658AMqM5x3oZ+L1opvw=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
