// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_3_1 -prefix
//               design_4_c_shift_ram_3_1_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_3_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
wXRZa7kpthpIgSwIR70fuCOJ4s57Qzbixk6nwcEsNRBZCR+5j4ZB7tphSD0EF6T+Xoic8mJpWopU
GuND4ErEgyuE43cd4N9ISVt69Ye40jHVux4xWbfzyF944U7y37HaApZRxEw8+cs2gay8iH3BpEpQ
AQN1aImvsUPlzkhBTNXJXkxvPnjagkAe3gqBQ9S+QPhlmcCVhWHCYrvOHVXS6sGH2aXbvbDx6IbG
fNfh4pAFpvG5WIgRhhz4omCZezixFYFEtnBG2igQ++El0d3Mj7OiPGrR12Hh+nc0omLw/Zj29C37
czTNv0uJJodUvPshPBD9fjMKBfyw7TwKxqnkDhBsdnA1Ej8X4ZSNixoj12GqRqTrVhghCKSgA11u
trUDEPQw8tsgwN5jiv4ozHYownFyOVvnULsIrcwoAVodFIuvGAtsTI6jVYvZrW8AUDL6sdvBGsj4
pb2McSyFKuhPQto5kH4NuKaQUL+ZxiheR6vSXVv1uyv4tLlUHvvokb+EBm+ywUrKyF+wTBo/d2S2
1KjDjkhHwpZBLcCKipaeuXgqXsgcjQ84Jh6trRvlDlC2GeZJ4uydINVs7ehHbokZZyeI/8zxMVsw
gNSEC1tYinQAi02k1LDSUCrElRV0YlEZ4pK5yXHjWX0+OQgnoSYNp+LjA83Kvz25JgQWqBFdfBqU
0CsHJMROX925fMI1JE1qKW/KnIeKQnY++LM+nVOk2PCunTFIPb/wEOjDw9gn+Fatxb9rPfrwA+D2
7oH0raDH3QVcYWQKzRTal2VMxdQ6Fc75n+UrprRYRfQtJo+1ZG+ON7sjgO+T21OC3PMLt2MqPssH
mUL5WENAF0NgypsgsyTgohVBQB+jiyYkC63miecosA2YqXnSO81i0xaeyV/tfKBe79CPK4cXc+Hw
Ic+yIEX7ZoJ0pXAc52l0oVoWJFQM73QyG3hkc+cB0+8mL27x9fgOXwtD5y5D0myePLVDc91S/FP0
jI7DnRsFmHzXGjuv/FgsO2wBJxSldL6tJosWp6V3TDB3Sffb3bWM9mNGX0XCywuvvnXydfqEM3cM
UCgyN1xiS/SGPUOivevS+xdsStXindW0aMosOuZH0wjMeqp8hpaF58z5pgkVPDgUgGDP42TInQOx
a+jp2+wHnC8JNSg3sNnXSlyNt+KHcODGS0Zd8RmmozlAhNkWqlsJKyUlvuahs0NLXTi2cLej5iB7
syUTZo9nR96rhWnNFWPws1mrYl5YQF8zqMs+zM84Rad1ToskW7tZPJQOVZFu0qgp8bHC+gjdQkg4
3UL+RTh0bTg//9I0jieUvGLVqJyvWdopE+tRxyhKApK67c1yQ6XOLVdm1qFcehpL0BC4KgC4T+AE
u+AEPPyJu0fw2Obmb1EulTryCJF1R9rylYVB/2lLpB3nlXJ84nNHqUHtxS1AazE/Yx5D4puiF5p6
rbhCpFMDu7zlszIvdeSX3t8wr0sKbeGxRmaDnrVcOMJjKqe53XSS6+gcRH/nMi89t42uyKsnGYvr
HqoePeRpauKbZpMesrwSVaW4wvWuBzEQTApe9OIEmUBvMvhuwV1LX02TE64rllyTGLEOf00Rezof
Fhp5czSaV1J7lqCFivIo6EK15I8/68QgND7ODVNtAs9tagsQRiT3T5voKLkhrDoikyw21k6zhr2W
wY8U/RJUTjwXtn0afDSmKYpvlBS2yAE29qR3QdyVJEqbJh5Z0fkTPGXGKkvvm2DJiiUGVrNEk2bu
oaV1zPBOk60TYMaC4NPPCpJIPZPy8PGi394ZPBZrbrvup9eP7kdcI5YY7vtOgcv3j+OtaSXmOH5n
KmHppkrBmWaHkltS+i08JY9tDCyrp655GwK3WlvfD5kjgH3k0VolQ0DC0MF+YT13SAb85ASBWXCY
YkJENy8FzHSgQ+OwQeb0Xi3OYlb5EBqy523H1NRG/D5EG1DFuPJoeNLhiZ5s26/cEkBjB0CXQKQB
b+PwxVsHQ835NmY3ho7ZBSLL93i35p6FtOyRTU5oBXmeqtC6KGTV45x8+SuiW0RmapkD7TaIE/9R
En5WKV+0y3V7c5x/UZVMiUcV7xItEC2fre5o1LHtDzKby9JJboGZ8eh5NmO+mtB/F2MMUDUZGdYU
6Od/ANAfBIGNW+lmzJZYUaqsxCyfs6wRkHZmo/dFEToQhjBNiFUWGEa1bQ7GTgQHHJZ3JjpsecWP
f6F4hQQ5433Rz+4si9ddN46aQ0NyvzWIKEcrTzI6nyWjn8VkJNyoHSdeksibZD9ENPm1C0HB091L
Ynz9lljFSA73giLCcdsauquM39Peaa0iu6DOV3ibp1Hus/B4+HJd8RU8wEpffE/8rmfTrMCA6HqI
cqpQPzN8JHFWNJGpiLwthLLef1ZWmCHqckdYd20nkB72IDRVOxarlmTR5lgP4nHDReJPmXCYr5kR
uJQOqvs/V5N7wmO+eTeKaQzzMfb37Cll6rQGCKFD8CHagpx5yoCcBKSjA5A2eC/E3yqldDipwgPL
d3h0Whv/bDw3JnNZ3ruoD/tgXYmgy/z6JMFeAEv8Mr1onB+kZfnqvZZWkizoRBiIxLjbzBzxB6AX
IJZwEi+F0lqp7FzxYmlrmzzXUnVKEt5xft/FHCaQ7a+7hmsv11NEkIcKnn6HEQwMmfsLGKDUT4FA
Pw2UHGzLLilFwZwoHxJRfLcAuo24q3JWMp8B086SXwgn91CXm4OFr0IN+ufx3Hf4eM3BZqIHTjay
XqKEkosLTOrh1WQPSgt/KaCkItFl/FrLa8VCH/EPj1ptPfn4Ca7641EYvhc9yiS+YSdB/AHtRuLl
ytOBLAXqcm8CmWj7M509ubq2b6hTU38WAOTCEq8XEsckZf315pDAVVj46xWHXqT4BOCMIDyrPe+p
RhnGj7DM0fk5VzmfzyFL5+9htLSS7tekCKg8NrbWnsTvrSx2hEjLk0w9QIL8gpnzBoM1BlYhsFBS
Mgz/0XidOFuEngYcL5kEvctNbLK8IFP4VKqYvZ5lRvRp6nohV6LoFzeVwQIhFt+vwIMZYbuIGNPG
l7s97tPUyfJmHMTiRxumZtEjNX846hvZlvDqj970mJD2w4vws7GUkxcbBpko+AJNYPjb1o/ODJDn
YSxxs19cFNubRP1SbdxoshCNdFr67SNpbr9JUPTnrr3A0UWTU+oPW5NVuankMg5a4OWlOLGyA/9d
62hr9dZEhPqowWCV4EUOdJawzy7Fgp63IbiMg94dXQ6haTxLAm0rA23LLwV7TH2uILqTbJ2cXLye
NJOoHORb77xtLeeYKxhqbYAxNNmBX9ZnfoCZYDkuO7J2p9mNs4q5KbGrtWKCvVjJn1G+VsJyGoZL
HjMagMnLFY1q0D4Qnubnbrkhm7FO+OcTxh0lYgBKlBoQVf4JKmxNA+W6qVeMu4W19jMNTcBSraNY
1SMfllH6rAcmChB/0asoqzHlEzkzB9Gm1Hl3m5Lqm3ESvvEbsj0Qi1Hyz7oWv7wlsm6iDeZkLozO
1HuHUft5GIwenJqdjw2icwEAU3ivxRf4WoaWXNgjUQZCfYd8MY8I5+ffLktb92ZRGuPjhWHbwhWu
ddL3n9isbd3sbBR/x/gnWGw6g0z2H5QvXCH2as1bE7GDMcvpuz4K0rIBNJBVO4VWsQj7MVCJnumt
Fxbe05pZGASRJ2ySIqF86ZELBt01APiH8cCZvaJHIW2NruQ/auQDIDUfG2LvUa0jfANJ+B4Dhfkv
Ls2FO/yzjDFpeSklWeewChEQB1nFohQkhkEmK7OEPIePU4tVkwvWzx+FYgjt9vOIqeW3zNsjEXfM
SrtaTE2Wq4OyYhGYwJGyFhssfd1apfmc7oshTfZDpxuUICHNYTB9K2pnakyyy58LF6esjntyGn3G
WXYOInupaMh+N70DqHnPr/bC0Q5E31C0Mr8esmfg5wtvn0Q9QN/y79XdQp+8kYoGYbi+Vo88rtZ1
5qZ6I2INxeOJaahCiFmkZ5fE3edoQRnslVDkMZBjJKirjnln9xKRMuOMdMk3HLcyPpbLsGwuAU+f
pegKS69stBE37nMziU9ZMg33x3+QI2pzFWv0eA9g4yS21Fc4q2ZM2iFiD3UKEF4tdB4njBJJecaQ
rN0bRoLtR7xIl+HmqYpu9ku0N8XwqwKrwDiMsDDO5lYFh9DIKakeZMAu7MDUB23WSiQdh+NUVMlH
DPjq26yJZjceZwxgEUtmg3fVS01GYlW4ehbJwuhXPSIbNIPucemrvqkQbUhRguNPua54Pzsv9wW6
d3EQV+57e/IJjUucJZ869u32CkBDjcCWBMkDQW1SN6EKE7VVdHn6VdXrlkWXspSg/MIQZZAONhsa
LxpSky7QiiEf+sGGkj5qXitQ3x7FhEYKIVoIWGyM0aH9560X7QE+ZrBJqXiiAPtFnr1wv+8+6Va4
FlXHpJ5nLNUkiKox1NCwhAn4E+2y1G1MC8bOK7kbqe2+wddOgDNJ818pkIZOpMxVxTk0zP/DIKHy
sV3OzpnqY7ddQXCpQNRJjlxcTgMd3zd+w+9/UbG9faxxdydRP4MdJQmvwns0xsy/itHiV6PvJaeX
/6zFiRfrlf3KXecaNJyBnzrbndxxc6rCQG8wyXJ7nLe/niNgks6BNS41XJsgu+xGqRyba0bwAimR
2YV/oXd+wDda35NIQ4vDp1BN2ksmtLONosT2OGKsSmrG73Sgg25ss4t8MR2zWNvDoYgAuqjnFlDh
rcmbslhLMJzg8I/QA91B6MypmobdQGC20mbsBMxFbfuw+FbBb6d1PCSbY1PWRPUUDkTzYzkzTPWF
x5H0kI4R4K/sbbhA/UoSasXiKhZWULe1wXjQ70Ea09SY6u3be5xv1AY4xd75AUX3zMtYVa3TndfG
jRg7DEXHZ8oMiYjJKWO4tCgDa5UhsHP/HjaMizVhh0l+rJ1nDdU70rZDlAMkVEvilCqoGio9aFO3
eKqMuJF4l8gGjDg90xfl40UD1tQNexLRZ15vKIrlRTugxbcXktkcUcSVDK1fbEhLkexMGL+nPVng
YAGqMcqnvAYs1BIAJHiMvjbEcNRlPUyUAYAYSaWUz66TXpwCAT3Ji75NGBlEXClnxfhSyk3AHGt3
cT+oWY39FEsDanvp7oSFqJucwMnfPt8srWvXpXGWujEljlf3FXm8sPPWx/MDmFnjcUaH3j3cpi6G
p0uPeHqlBioXd4VleeJC2rZa1TsPiqI2bbeWxTrnFRPbkqb1T1q9A6US6gxzn+VFw23RECmNH6iR
od2TGD8CtBYtfJO3nDCxZ35VaTLpzlO6k6qT1Zc8+APLLXpByr6Ziz1tH3uVN88tTzodw9SU02G9
DKnznWsHdnPqHSqi5BSW1BeWi4LeWMUvzsrofXnqhzX2NK6LTdkaBUT9l+H/5TGFYSNfdQQG3c24
OCb4YjfUTXlpy6pq2ITgbSPTdSb1bvWCru7PTnC1yhoieBYXgLWaHbu5bpaNpOy7R9AVeKyFevGF
qJiVcP5Lj+cQcU+RSqUwTrpAyh1zWluDDOWOZvki4dsk86ZDOwZKQoniw0BoFdNo7O237wc+wJ4O
nEHIsyQ2qQf84sYdp7MxSB96cp1PzLxH3eE9gsDoJdlm2ctwChWfi5gLOJ0xbU96dudIoXdPgGa1
riOMvA2ocNN4ogv30+9JpOTQfUwI6hnbaw41/fbuPeX9v8jwDPr96xcIhamwXv3ATxmB7DFfyEvs
C/5A1U1JAc3/g08qjzPLsLyCi3s+BiF3tdwIVeG69mID+TbYkGmyHtVQ+kPtK1CjAzJ5X5hNuL+L
ED9h6gjDDix+Oewjwumhrf/tuU1wRdcwvKoRzYB+3L6q4jW2XWkP7NWPxOHyeW7ZxGkCZr6erY3/
IgsZx4arFPFIC6Qx8MN2bOumVFn+mpUj+7xqHborIH0O8obJ4EV+0iziKJo7/imvrWRA6woW/pr5
N919aTyuIYASeZcyvSydARMAcoqYzkMaJzE/vyhO81OsDQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
