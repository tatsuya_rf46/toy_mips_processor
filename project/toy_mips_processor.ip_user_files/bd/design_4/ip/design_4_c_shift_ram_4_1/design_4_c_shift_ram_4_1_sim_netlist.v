// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_4_1 -prefix
//               design_4_c_shift_ram_4_1_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_4_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
XvZhKg3J8B8QdG9hnfZ8GwUOon+zznNFspnnOQs4tMmVmjMJ3LP8Fx2IkLc3D+eio0p5ZbF+aSkB
TfxrbLc2V3OIQ52tvREPy8n6axdL5MbLx3tjtjAQa0GPs2DDu/B/SZyZ2cKLzl06MrhBVtSCNaIM
y5QIFsg0fiy0bmef9y48merrAwJv2c5WUXF9Og52SAzAtdiZFBdhC9siVVFRPm5LDHhY3TcUY2xt
yGK1Yc6VInNLfnnDkO4Ok6ER6zwBMOI68UQIcTWYTGqOLnrWfctQmy8qCedHSXgYIVuxMhL9U8i6
dpOo+1xhizJ+I4IIlqR8jP0Mix+/8RPlVfs/clCgdc/kSSliOXMA7N3uTEMii6lZL354ATSjqjfE
/yKgOe/Pc5kMlBOx6NZKDNHHheDohZ0088z3h02kp2dxnChFMh+Qai2+skzmlIfqhlfkkXEYPZD1
zv3GogaGO57iSVky8jnO3/K9pnqXVAaTvOcFytAIzriK3hCIeujkt6h+wRBMZ8Ks5jppiP9Hlm+e
2Ya6yu23N1l/Tc7Gv2atoKq9ICj3MOK9r47MrWtYmiwFJGNX9UkBJGDYXLZ/Z1BX9QyY/tmTvehy
9/W6VZu/tz47XuVVoXzp/9teBdvg7yhXMdmDCEn9+3Eh+CSZfI4Pa0Hh82D5fLs3e/Dr7XBLK9qQ
1UZdU/yjE4lDgUgGi1Va7Qe3PPxy9+utKRbN7EoNSVNpWhFQic6ihNRKuFnqwE53Y6pjSh3UiIZd
CAs4GQ/vHFhEmcJb4L01bOJ1r79U438iWrQ+agR4aK5d/jufofETJ6+DC5VtwwkloDwrItUVbrsc
ktP7qwpy2otLOwEl1RjzLVoKvABugpiKwjLda2R0ZEibb8jexoDChCKQvhl9vmK9DOvtyVWBmdnM
KgBIw2+K0d7xROSDxMGYyO6GOFPGJBlvH/v2I7rd7acqc0l/b/k7/0hlOJIWaUPHDtkLj9gnr6WB
n80X0qS5i+XuMVhXQQ3NbDsv7X4eKvmSBgUn4qYybhxQreJzEtSZIHsQx7SXlCU6JYKGW6iAHk0P
dwkHXQynEOGbUGTiC9e9yfnhK9QON7F95TSnqobNzzLat4v4XD3TT8f8iGmaMoWR9KhMSd/GW14Y
R4IOSFIFHbHZ/ESxKb8OMR5b8AQjB4HrRSVa/Ngc54LWyuQ0v2Q6PM4L55J+gUUllbDBwTaHUeYc
rCRVuJhvP4Jl+I0fSryOCtJ/aiRgnpDphHV+Qyny6MpSrvs1cZmv6z6vPm790MlO6SeOjrojeNyC
MTwiAlwH67pM6aKDkg5V8oWo4iVWGI4uD3G44mwZhONQLmX4T83vVg7q5Y8ExINTAT6vPyPnRWYj
1qrMnNo+LPGo1s8unQ2F2JCcQakg+9w7mUQ48iEfmaTNxReHhqXYwqbwVq5wfHeKpsoKJkE5DGlz
AFwt0ikWGqnJMxxE764y587TYvNl+V3LGvje2CJsw3/tkEOMGraWax3eC3qwocFMvLwB0o/4OJy2
9mFF443SSMBG1aTRfgJZmTTvlIZZ2bFDZbOgiJtRZeTwK/bZ7i1Z+GZzaHtwlAkzy+WYVHIUCX73
qv5roE89wy94stLkAZoHw+tNHejzJECo4OqXl/4XqDnj5G/Tq0K6Ybye1CGDmLMM+axo4gPxSc7v
eBqD78I4J1VHl9MW4kSgX3B49in4NPOSYjjz+KRv21hdjFW5acTaXMpOpoZ1QpLJGW3SFha5/Dd5
pMXp3yynAgvHWp82all4NNAecYGprlOnPul90qqFUCBQ27HVJphiYybjm3NTwsSaiyMGXSzetIgf
NMsKhb7AxyYAYr+C23K1UTr4isUH0WCBoQj93ojQD0x9nTb7a+gnPu4onFAMfYlzvo4NKOXgBbWI
zXDRhCSzwsi3SElxU31n6p32DykIgo+jnWqLloyv6zjruXHchH/cwDokfU7VmRtc1irQDe1p9mm/
bn8eFMET6sdihSCHSsZNPmgwB+xwO1Qc0B2ED2GtzhmMRiuUmFPwwP1+8G4JgyZEJ6mF4MN372oN
q0/CdLArZlafnA27egoutMFF3+eoqVqEVRqyip65yIDzKt82gUw273KJHZMvRiWdHwWSVxjw/lvY
rAcIudZLRYsKCeC+1jIGkqiejESC72vrtYcLp4Dr+cLer5JJNrus59dXclZCU6Aa8HB44x042gjs
s4fQGkaarXGZzpcwf9ucnip2QJaqjBvkuLXtaYWmHCMEFY6pcwLpAD5+vfxgrEnMf+7880Xaz6cN
ke7AEtjEDXL5muIPZPDfpcCJzKXU0I/mDXhBTIy67SabUwqPwZp+NCwmq8uEQvKt/OjTGCzV7+o4
EBjnWS3PN3m3w43s/+bexdCMVfGfUM7m9tn9t9PZ/mIuKM0O54DCSTTSnu7qFOhzKhLa5TuI115H
bmx9PRJxDEkPDOxPYLkxKLgSnJTwheFH/R3HyruL/mOIuUwsVGUIjItpyHDr8bYngJuf+TXhneUB
y2U4uKDSWXu/UsQCSa0A2STo1cprXbvf8gmqCA5/NMv1NclbW26Q97d3yJdzRuWfeegeZaV5v6m7
5pmdHk8ijFLBP4bZJ1vpD6U1fyc5YIEp+b4qbUUEJsEP39JKwRW+Vr3UDZhq5TjwP65DS73e0Tp+
PEhpKmjJ5CpBrcpkMi6vHrYYydzdOoNu3IHImeh2bAuAWFywOayxECLOCQnWdZbOfsYNo8c20TUh
oS8Rgns2ekFufm8QD9WZF5H5i8X1m0MrXlyt1oNaeGohhNARpsUnGzOtqk+2mZ4uGhzlN7BFFpTn
14rs1SO0FaVfupvNs976dfkggFAWOTohYmiUn+kh48/pMBLaftpkm6mNDlAJW97sf4vPQ8fOlNS+
TqGkUocn/kegXy7H7epDaWnb7Fak81elm4mZrinHKhTrz/jps2teoTqfmKmAVAPfLPIIeTeYSQ2s
q5Kujwxxbcjq8G/7/2bfilusccXGniEyqGEHPQxTPtt7JfF7IchaUZ671Ss/9zKNMGCaMYzuNSpd
kLQOvKXTVUJ6jGZvEhvvZPP1OlddJOFjhqnfC4Me3fVBEGWHK8bmvw5/HZ7jFJe8cphXjl2tcngU
YvaZnHEZi4UOAcUarz9jw9xKqcuonLO3jPKVi8yNYy2ycnV3SxLfi2sYNVJbKlQkmvkGjxV15tsI
eaTtUk/g6N2FhVFecxOiqjQ9E+uIMG0Nr2igX22LUQs2KtjN0CJvuNcu5rgtUl+SOj0H3cg1Nr28
f1iUWPZtH/bGkvlj/CN+X3EEJD0EAGV5FaUre7jFqeI6c0t/ntwoXbmqWY5tlXSyT2Mu8m4UCMd3
qClOfLLw5DbH5bc/gflkrFyRSrjPQwNX/Zr4Muybbeb0+rl+ciujBsgry/VWEGbuKmgBbV80Y5WW
c0YxCNyTlqcqEoTEPWt7sQ/wsP+cqcsFm5RfLKlXUAPQIllyOBnB9PtA9EAwTWmtI/oPPqrLmlcp
tctsE++xJbBNXcmXFgSQ02mZlFCviTq5XQvD8kK7cK5M2lGAvRd8+stTfvVk/So4+HZt4Lq5qd3s
JxHkh1zgvjfR8jZdZeSWypuqp24XL5A84cxMnH3d1QPjnBlaULBkbW5IqcvaaRcNVpoQmWr4EEcu
vRRkkpLp2T++2366o9rus+E09hLrqUc35GdUkox88GZDn8CyeeQXlBprU9jDIj5eWI8mqsziZDVp
Tw/RZxHJdKhT0HIer5dob/Yo7bMM9+/FvyZAQ7ePWw0N8Xi91c7VGLFwnfpbw/CRjSncNKpvQ/tn
0OAO11J51SfWtCYcWDoq5Jm4WeWrVInCvMVz5lNTZ9phd0bNWT7uHQZGiMpWPnSJHfBVLojLbfk9
jzPnms4BTbcGL/y/NbRqPYqBLJXUFHop/jhPGeQpcb9nSLQvAnIdhMUJ8OhvwNR+W6CFfHQti0DG
cCqBA6k6h4ie/DGMNTvPTPdDj5tU7hZUPcWoAxs0L93SFhi7mKlaiPrYDYa208BN2KidfDpdAtzo
XUHCY1Mt1pMhOmjgEAEqN+M9fTPz92ohc552da/TudogHV+S3PLW6hJGhUDe9lD+lFDgq44v0n4Q
nvVGBUKp662AbEn85UlLMIsZSSc261Hp9Y8X1fVRX2OIr+NmOX6QDjlnsUwhk9Ti5+sO+zhDOnib
qvEiz+QBWgpI5Rbo8fv8DFSyek81mTl1R86vH1HrYGXsTW8+Ww9nAQzdffETx9sCR58UvnL5VSbw
JmmDka0Or3FHJu/ZDwdZ8veYNhf2Xz0b6x+gpuoIJvju2YpPzENAjll0Z4xZVLitylxUaaTcNE70
jv99ZKb3jMErcKw0yCIgLzvjNDVi21WXSkwnUF4aeMkPbDQaI385p9lMdDOjxZ0KdqxDJ6ydy8AU
lAHLadn+QM37q6Hqfr2sfxEaxadObutJ4GYMn1zWRxXbSYQSGEUEFCua3jMsB3Clend1s8MfGso7
4RLpUlSg0xWs6AILDDYl3m4Q7Hz0f+MK6KyFzUQw3LOPaVKn/qwD3ILMK5zOb3kb4UuwzwvtQCeH
kfFNGMGIEVNsY4A6WKHme/pEGBrDAm8uCyL/mwGV1bQziLvdcVTSjNythTP6LyjEMQqVx/yyHtjV
kAazB+VFyKl3QxnROsDxtClBM8qPTLtnSh+7R3rzjTNWjWht48FVHRrM2uIRRRDTrGHT6ifiwjFi
MNYaxcS++7XlT6fDou2+6p8jy4ziEap4K+B6LDMU6o/4C7vcePz3Pk9rmwsHDFSi95poAvBP/Oii
T6mgI+TMXk4/y25PHVWyJd3W6kdfZSMtJubwZG9Wq5J+ZYbi1oHq1sBe6/7tcfIq79AhDWI11iGu
Z0F1hqhCHCD361vSf5vl513qNfLf7nXYlmh4b7CucoKdueLmK3IFUnidaP9jxoxbu7jHFSQ3DUil
N9MKwIyXgxerL5grWFq/FfUPfHUKeqWiz2EmmVughy11vlF25cAgpfCRH0uPI4mr5e7UktEdOlD7
yE6jz1ImeqlnfBNHhL83FKwX3mKnwnOgz3XewaEhXCUzhv/UmbFlG65c2AK/gp9po2YHrryHgMWa
/tIfs19QmEyYQxBl77pLA87Bd8EGEDOL6poJoyReVvH6nGPzMZPbgZm1sBBpjMI+vbJ5dy3VdgpZ
40e71VxGzlBKcziQRUf1O1U6WLPK223GuMevVl7BYnV8c/GTnaLZJ3AhFaY1KU/NhtmGeZwnZP8v
atKv+MYe9u+0Q2kqfe+f3Ub74O1TUY/qH1iyup3EhzQT4HvUnm7lvJxm9jA6fN2ftxCIoHyhMdQG
BNll7h5v278NkbJMaZWYKyAjOB+60sNpabBHFqvSU6p0EoMBpqGLSQAAWbTzFD90ABnehcaDJALW
8126fLSyCKBBbRhv1hepYX1F0wtGpQC/rISCAfEHKkDLvcNdmkEqI0+ZWeESc+d6nNqQyU47sQ63
qAeaaEfS4VuQGkuda/ahSE/M784iqaVuLgJob8pMdqqhMplVAwX/qyTF/MRHOfWQ5/hOAjx+kHbY
7YBbRVegjc31W74wTX3DgJzH8FKMhRLHJ5H+tVS5wWoPw9KOISOS5WN8hkGaEjVCJ4gpxIQwTNoK
v4I63aVRDRBI9jKERQdPkyLIXDWQPK5XoCWdj0u2NyVvl0rCk3BraXwB6k0P2j1qnoA1PWqxmKaO
j8fOlkXyC7Yz41VZdJBIlpuXK6EqQwMR/GV4AB7fhj7wYwTMhPNP4NBEF+7tgykjsjt9hNzKm7uh
x4c2LpEodBzMQrnNK6NwsU+IBKr94yt4Rt4T4Va9zf0+ZBImX3dpCqoptDf3hbCQlvQo1FaAfiWZ
MBkNerOY1rcriZQ1VP5xd6Q3KpPgdJa1lGITGdZRP9N+wFUbG3VPE5cNih+6JxgyydZN0D9+4l3A
0ATyuolWg+aGETCa5nPjg3a+RDd0C6vqzRMPg0owHHmohg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
