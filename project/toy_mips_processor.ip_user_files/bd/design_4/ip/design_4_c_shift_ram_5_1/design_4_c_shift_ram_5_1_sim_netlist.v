// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_5_1 -prefix
//               design_4_c_shift_ram_5_1_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_5_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
kzOAzeaXAieJf/F8wCnjUBpBG5R3H3j6G+QZqvxIoDiypVi3HlFrPWNerwRret54ZWbvCzpVN1xb
avIUp3tHDbMZKSwXlPMRT292B+Isk3PTBNishQah2Xg/rfSQ9MvmOCURKPTl2qDRfpPeYPKCndbO
43GK3sDQtO+jnz5F2Qjw351n15ADzRum617SuESIIJQdO3Zr9h3qI3CRuP7SqNtwvuCjjPl0Q01t
MStTnSLqLTYK6adiTA7YwTrwzzOlCF6ajMSkExd9aIckyjM2NFbOFy+ddcek1usekqxvIJMEyH8K
1YK1IAEjzVhlmLpdO7Ae5ejetYH2JJJD8HLZZkVl4GNv9/usoIZBA8ryuJ568MG2EOFpcqGrJCKk
84qiBpkM00bAfelD+gBYDRSPV7gB25AkHRf/0IGNT8ouV3XwteY9IFNGOUN/Zx7moCGkF1Vmhn4E
UxDDt7zXorC3bCCfeK3MHbLvmm9AlX0Kh0vz8NuW3h+IaGLE+wSZv7iZUjT2YfswPPNprH9Ph7KC
OLWtXf7UdJb8/id1uI/q+e3JcLbNBVXpWEhSI3vS8walhs0ezN+FOdfv6o/OUe5kubfiHeJlszRn
LAUouEssFUa37hI6khy+/hfDkHBHwThTa7vQMUxIiavQjxGLHm16bRZtefaxlQfe1gXm+fH0dEHf
NFxhBqxYmiswIl6450BOXgG9wuIGdBZdTDj7mKBLOcPZAta8QDWx2adf2RoAPvaJNAlxFTwSbYQK
YeWblEk3YMi3qrGqAPcKws5Vxh6faXOUqygV5fwHYVlruQTOta49uNVz+xvDWPo/A4laQBydWSZ7
f0hgMAplntfg7NUOSbf5mjwn4MSP7BzEp9wYR4IDoYHScVGYwYPWpxH0bQcONNC4g5xnqLDv3kuB
Qhx5IhJB4gJBB6FI6lG/KBq4I+GwDt4GxDyMeUJ0sFtgFYynh3DJJfda0lY0q5EMdPczQqbDfMBr
E7YvBBztndHfLoDO/zksH3OExMzEUPFG4JMGuuFERiF8ToYNnAdRojemT0w9JUV/zh0kJkjfFa2e
fAav4mIaW+YCeewSIfpxgSo3uH3vlFMDcPL+pGW1nQcN753yXnLnqsSMyAWqQH9MZoW+05KI1xjv
9uyUcf5B7Lm2n2tFYeaWEeY3rl0cQBYrnOlhO9ytKrnKuvU9IeoVax1Xi3wIqdmTMhWHxbA2uUag
xbSRVyvlDvo67YFQMwIaxtNShp9tRwShL+1Bv47JTzUKzdxnNfQfVcFS76wt06VjuwBpqUZxLSi3
ySKkXYgQ9mJskglwVvLil+J/oRWOHIej+AAT0QmM0AkN9RS/8nRs4KukbIYZ/CpFeLiGXHj+mKiu
FX50jZc6D9csUJ7UjnoQFhjzZNowf6WCDaKFdRhHk9oGFyKtP2YMj1/ZEtzUyO/hVfTlIZOLijPn
YHD+E6idDv+o8vfjdw5poiwNm8yFhceyNfU0RApSxxhVezQg4P6g4azLT6ANyAPlziXfTZ5tUpJx
nHwkYEzap5Ag7pSU68tR5La/ukL15Tw5CRTFkQxuZ7s/CMPsuJ3VEDK4OxrwpFwvXn+UqqhPEwz2
5/8IYJStg1TdqJzbDv6zUsd+nhXgvobKwIlgjeML0npJ3LbyxnqcieM4AOwsaS1OUBJm3ecl7n/T
9+Z/7mC474OA3l7FmMVa57vcfU6MDKFb2gze6QyEOIdHy+xQAC2y57SwyKagUAYrYOgFxjOevfoj
G/hr5Lin81wDmC40Oyn7ouUza9D3S60oXlGXLsWZf2kBmGXr/Lopw7GtePAmIgMNKRL0TA04soRk
2nXFJFAJGW7OZi5blb7ovA6PqmLlAMs9PgSusFwhQOAplHEAp5qckLxwy7YlGPFxdlftXa26Rjd1
pE8fAjAjZbhwIYfHn2mMKMvD79ijEDAyE9VuTfcE4ebOyl2wgEQbmiBSp/YdyQT+qVTlIgVYUx1v
eTb7OqLFUCreNOjGxQaQH7EEwBJVr8LjEItPYScPcodMPaeYrBSgAzLVg6zezbDFm9PyxeDmByUp
srG8vl+AXQ3LJSYrF5H7d485VUL1j0dNMjPPDNisrQeC3sbFcOziJCtcZFw71xc/PKdlSsklx76o
tpbh/s00n9LX5Rkq46BBnbGh7VYq9yIwUJN+WCEf5jJ/Dd/t/RRUiqtcBsQNnxAJNB11vP928TdT
1WkyXzjjlP65FgH3vBv+22scRPYRLur9nN2HqcRkjPpTZzBvTAad7v0CerLiVrip2z8qZDgZ5/0q
O2qcaR76dUvhQQNGWoxdJHfIHAFAawdfHE9odBk9r/EWQshk6Cr2F1TKutqguDO6ds5DYoeJ9YC9
VQjIhjMUbGJVXjLFk+U7uYRHtXldy2+0B8Q3RH36zHu8xNyt5mNnpGF9f8+7y45w/El/Q8eI+v95
jnSv57mxtEWtxrkHOSxesB0SLMwcxFIQ7WpnkVVSux3a0jjFLzX69Tyi9d4zwshwVmP22dveQNGo
gbgzgKHrYTkyJk3GQiuxXR3YD0JDEzwlMNxQlnK0BIc/9R9hlfNW0UNJ5RHhy9U587o0kMfEakOJ
0zWMvdrJNOYwl0+A3rGqxwl9o7MhqabtNlTmd1+9I6C7IsIO/PsA0NtvgAed3+6BNbr7brryzq9d
5hrwIbfpwfIVkRhHVJ/vITNv0RsO4aKULevcvD/HN/GvfmgFVACBXFON8PTZIuIEfmUyHpCPcJnJ
cGiiOzLW94IQfZtfziSxiZDh3VQRPKdpJdmf3r9BjSoYz2tagFySwo25gIwLcBMB/405z+sgxk08
D+PTsQb/5L8WM3p/Os9/+twywbnxhr15AoKjYojNOoV/uLjCB7sTEYyXNzEr1mpFPllBnzm60uO4
xZ3+R6mybx4tY/Jv92ar0MxT/5ed3BS5WXpJZoAZCXWgnRMV72/ayzGNoRwfmmCcELvpKXmlwqD0
6lQ2VAilpIWG7DSDGQ7qO6w6Kw52chucvZMZATTgzxVYeLJRJuOI2Wmx9Oz3HpsTKvDz8AKsuAoP
3r8LD8DVsluE8D4bNTbyOgb7mecbveoqupWww+ZIoGOpR/c/BEeMURZXDw8yldR1U/zi+qpKlH/o
1RABQclH70ugef5Qp4vRXj3Sk5K245n3pSk+wQWI4qDZCyTixkVLxsJigP6YN50xdLOv0PgyRstT
l2/espI7Z3WmKxaDcV7fnwS1/26ym5bDfJWLCKZ8NmkPqc3+S6LJjkvnjRRBRXzie4uDMW6hpsz7
52P2zEckDzcrTQ9gbHHoufBIgBnKhow/zc7DpAZh3dQeiR56AdMblJqAcxkykY2LZ+sZfkj/cO/K
JTulLT4ZGK7VnRE5BydezX+P1n8SXxzXSeLFObsd436hyk04v/LHtHh8lGvmHgwcFhYj/ObSMs0X
iOvdzah3anyzZ9XjyonCEjczd135npwxEHUqTi3Va8Xn614wpMN4T0EzfyTSoSjHTPARkLI8SuIq
kHicpdFiGGJPFtok8HZ2AX3qUkPaZQK8ZAO9t44EjEYg6WPKgYCv7c4S+Eix6qAsYK3sESJRlz2Z
62idpEi7Et7UkNbiwB+XBRHt4PbLpNP+9EpHCakt7R2AR3hC6PE1hQ5kNhKR0VxXnf/QMsrrLgCA
kTOljDPKkAl88hO2+8a6Jsln6RU76ZKN2viaNVFiR9dTzwJkBhRlbftjyQpaeqvxbfBWBuehyIuC
QCRbjVtvtnNEEQkX946KrkmcI6CjsdIXXEUkfnVKmE04QTo1CcUdrYEwOWFQGmCvowbyLnhC4jK4
ByROkXPj9GaGJQ4U7RWdWrf5VGsXdeafXy9bFlswlBrseHwZoCJd9UFUVWrLxc5wHVfuek6gr7ky
Sy0fNwFPqDVaUtUhvufn4hPfbGUl6la+i9yTOjivxTlcSqjLVaGtnhusnOnytRdKB5g/ueW5JIQx
AWCJ7byGlajz5e/hCbg+4bjXHxrGqq4ilt53OsUFvKESvdqzZxQ3QMcq5NesrTN5YhCoqWF7TyIr
6gQe2BclHcbeSrUPx4ri7aWlsHErmBv5D2oeVesyiQBasjdlU3TiF6cAlUb9BGfICH8AoWeVGgC0
L2U0I19ZD+MEFS4Vw4wo7xpTLhYUX9om1yxzST7jQR5DfO/32+Fd4l5DLwYpEqzDrWdWOjkr4rSv
ZaYsGZMy0eo0JQweduviRQ4+fnUasrqBVwiWWcnoIl8r7B3RUt+F6Muc+aPlo6I+5h9geiDWhavM
rfwjWnOa1Xoyn31t9BpNyx1I17hgfDgoEN9bWOqZHnQ+GHgwlmalFqZ0Z5vqXCm6+YIQjedLtvZ5
hDauAnmB2HOyIaXqn8tiFmaQmgSKUjgvi3rQz/3HP1zwv15ftP+iRX2Wnyc5LoNbTL5P6Nro3Xjr
UFFmVqN/9E7361beQ2ly86EaxB87b2gtHUINdnD8cb8azN0K3GKydJ6fltlsnGekEOjYVxR8LgpL
VAihE3ywwV60HNrg6gsW/fQlAsRzBqpQq9QUvNfbi4puPmrJIKm2p36AtMQtV+iqSGU6Lcw+VZZS
Y+VicuOeGVx4OhyFcEkoPRr78jpvYoHsgeHvdn7t4po2miXNFZnlIaeKHxpzW7EJTl1rLOxJO+30
0H4NAsjpCF9sGgSrx+3jsX/lsDFN8eJ+R4n+m3hot/UML9hBU4rWrrgjfbGjr8HG2AfXHrH7AevO
3ZFb/NUkSXwhhyiqTGsKYXSDMjFMvmEfnx1pPB5ywCQfSx6QCJT5L7l6dgSGSR8QzGPU/Y8YZx6a
7SmnINJ2VUO2d2HPfz22ggYymWAXESqfpNrd302h48TTFFzSX1C+h5QSKv+FpJtvaVjkpX8+rV/Z
n3DRszTLtXn9tXXTk/GJFRZOxxZyglG88ccLvEBBL4Jw9dfluI1BXvq6hqK5E3R8K6npWMWZR7Xc
G83XxC74NCZlvg3ziK05bYKqVyCeuC/hxzCr6HkfycCsEcg2OhTILjxrY7wAj3IeoRCcx9qsz6v0
Z0fK+SVoa14CR9PuAkEihfZgs5f+XxUOdBN1MiS2+4mUp1YG1jxHSNMUzLqDl4VnstzxhWI30aDf
GtHxgHSJNt3YDW5VAbMELYLg2a6fkXTi2J5/ndLsubsMBRs2QXs9BA5OLAtqcrMWNpqOrJvGDJyC
eP/jp7bAaZnnogGQBD9Cl/wMemqS33ledZoafpMpEefTv7dSeG1oInRMXOdxdV/WyOAOt346SHzq
uJAPgA1/R3CmIkqRhlhD3+twpZOiW7ksjpa6jIZ6MZs7F3RdxV5HcDdSRzSUhxDssPiB/nCwMf0+
UtKI12nHimUnJka5X0qJsyU7giPi9dIWsDjk2LqJqM/iQSJfQnsSzlV5iUbOxPzccZq51EnLU3JB
+A75dYaQtkj/n7BKALyZIKslMHiy4MFiksc733uzpN7lpNHBUSvLeaGBj7CqMz2SOwPM0XV3w1A4
b1fNyZ8riQhtwW/FHCvsf4aDWpEgFzxWTkSD4YXuGfbYbrPqCVUeyul5cPY7bnWBkdzc8nJDcVB3
t8myA9hLRRhxVzzb2iGutR/MCNty9oI8O2RsSdXpkdNOJGX1oIM7w0KqtndgmDF3PTI243BLGunX
ULbHqO91QAAGeL9+Fu357zVLpdcSQtcqcTDzNgmWECAY6Z4ZKDNzZ8Ai9i71ePLzkE5tBNmW4aNB
8mWYvKugKYnLQW9jj+NF6DWVOfKriMl1I5zv5rAbDb1/siMop1NGqysbzbfDiiRKRKIwsWBhQ+Zc
wXORPtD+4GZFTP7p/BQ3CYTnnKRxa8sGxCShpI0apH15m67c7znMyQBOMifqe3+jnN9PFcGXJbxF
x1JMuX1HMJgIgijzSV1G5/iO1ZnpJ/IRdW4IQZEQ1g7PcA9iAgdWiUJY7lbEarZMknyYB7a/h2Ue
jVPdY5XR9qQ6pHkLVv32Uda0I2AZrEkr+tLlDjkRP7zkuA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
