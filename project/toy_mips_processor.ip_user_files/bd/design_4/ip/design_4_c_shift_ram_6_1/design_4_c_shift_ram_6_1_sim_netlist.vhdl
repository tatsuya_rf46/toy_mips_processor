-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:03:51 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_6_1 -prefix
--               design_4_c_shift_ram_6_1_ design_3_c_shift_ram_4_1_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_4_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SICF7yO4l7nPmAEkWAtdfuquqiR93zDD0cfU6n7zu/bHv5KZXjUKE6TUMHlwuP1px47HBrnMy+BW
Y+ljCnylYYPWd7TTlyubzjcwqQ8tVMtIo0eCcVft4QpPhurC09gL036iTFoO9hIvdT4FzSA3PzxT
u9V8+wc0BJQKq5wZP7q0dAXT+/iuaUlUVN37dCHvBkdjpPcDjourfMdyeCGk/PckwHvpWLWhOWRn
MnPStiFzgxGMGIYBRcg3/mNnwvE1kK0IjVHvPxm7d61CDUrpZ9hRktL4R8jNH8O4wc2oR8hZfqtb
TRbM7YreXxAbzpras2Qg2nVe/2/YjhBFHxNZvQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
lDkLYL54VhiDGiHkwsjqITKt01FiNY2XyXsoP2MQDtiRLS6ftypcOxCfHrxmb0Y5DUx7j3ZJWxBr
goVu88q36h9eFPNMQvDxBh1erK/F3Mj32RJsg82WMfe6kKOqaEwJK1InGbH7tZaVMxIUlSD8s3bv
Un7RvFg9leLWZLrFlB8Y+ds7On6Q1y41/e3YVGw/6w/w3Vh4xS9qpLeYP8CCEG+90FHYzyLPoFZf
vEewmHiCdpaJqm4XmO4+kFGma6ueVsCSEkcH7qb1gLaoRQJr9EMZIKIAwGaUunMKKr+hmeayZaLO
drXiENefExe8HTza7TdHGDPt0BMgLNLYlo1Oaw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 14544)
`protect data_block
Pv+woMshpGAUOC7QIhq6q13aImwbnTIMtDsobtvKJBez5yg0mMl1/zW6kjl1paE9gxaUYOjuVU2C
BW37rZjgT1+ZKzmm0f0E/alJ5sVuL58vEZiMtvsZ9lOiVAxaDyjrnzg6GRjMaWVCOl5Fkv9xqFmz
ycmVXHw9F24l+z8ZVkX19FA5xvqf/3tpJzD/UJMkRh76fjssOGhG6XpnpCzzs59r3ZlONEb51gco
LZHpYoxNfa6Z7k23R0CQyWhUMTXipn/PUAT0p7MLf7Fvvw8uk55Q5rLJxJTczIqyG4sAHjzCEzML
aBsKEeE0urLoXgn2S2o6MvHvdHwQos4N1HsJBwfu9Y0YxMagnSGIRwD+BINrB2loWQ8xPFQZppEu
azMHTHFB/V4aKW8WsxLf/vxp0BMTWgr+bJOZnPOj/79AnSBeSyRIliMjoLWNuTaWc4m7lbXQsSUx
fjZjVZlEiwmwUKyMn1VCpNnc5ayIuuTl+ZRGQypRiWNrLZ2kacAU7xwhpDfNm0CRLD34nmgRc9Zu
ygov8v6JagWGYPM//eVKdfvs5sH34YLQNLMjQbIAmGqWfxRosK0faT8TLhzJOBktGK/Aek9H/gCA
UcXyKTzeF+8B5eMNYPGKuiaV8JsfMM+ym9EMB/CWirkTjafPQS0cHF2cMENKh7VPWHHLbeNxDyE3
PVM01oniOzIgbusHHItZfPoZryUciSW9LhnJtR3+cO62eKimmOl4pXbChsbOb74IBP1Mhbtv6W7B
URlM0/aM5eSO3H0PQ1hgrtGbT9wzSXifo0Q9niK9p+0UcsKVCDFWNB3wJnDlKJgi7JlZofeJ53NX
vWCKRzqORUOFlCdhhnifmHYITGadA2Yv6AlyDxqzbEBolNqsJ9ACDazwAwPN7Ej8eb/KSdaWDfyH
vFFl0kG05DBKYwzNdUjSLrcUdHNUONLNEJHnjVUlQOXK7XShLz+w9BHMy8yBZUxSWjQsVqJ1VTsM
q0A313NqnGSI0gVqwQGsnj2Ey4BOoL7U1PspFOmi1MFa2Ldf6fLjXXSMS2Tpeg2pY4psmDlYzF89
R+lBet94OIgqQxhXUYgs1SSkhb1e4SlmJMwAcwbKcWsXhkS68S7PWwGuqTmlbdLQBKWwqBNN/lKx
TamV1/LxEB2JKBAqv4eGDvoG/mGZlinOJcjcoBjyJH4mS0PIUVkBHudKQ8CkgrmeqzIlBfGxAIno
YQm5nBSpih1rPc/+9TES+K3PrUURbwKmP7VHTi5+TqKqM5Tov6zdDw5a3R9NhScJ7Q1Mq97tL7Zh
t6uiis3BA3U8xjK0h2v9a5HXr5sirO/gKc9DAv/AEsuveOJqXMUIHjXaWQAAZV1j3J6GdfqsPojJ
jpRFGwd7aiQjfmWsPsC0WKZSib0pp/hAa5KSrEiofJKFbvu9WzH4pxgJX5c7QIgslhWeQhPCp426
mg3DUhTEjnq0RRZKWeo6mBtB8dYPC/vUiPMxmgngjOOafKNuxO/0wyrcOetrg/fhw0QromWg5vfW
fHjky69j20+Ni38OXIqlFgJWLaUkSyUB2uuXzxKzjqXXx1WUf+/xlksiqK4t3z5Z4YWOjvz7UUoR
AFyfKnx5Ll6rGYYQeJVvTJSeeeDlApHdHzhQJDG04AZ6pd7TUrf5rKl8mC8NNRbAnlmwcnvm+J1g
BjE/ChKHn+AKBMDwYLJyj44y7CKPoIxAWBi4uL0QMha3tjCMJ88ozN22T1mStVi5xB3nJPjJdPRo
jIR8DFa1iW4DwkqEwbIZoNPaipY7C/lIpIRKop6KspeUCWwenwz7NQy4uV/yadKY9pftV09IkluN
0vbpv/duzGmb308ENF/rL7xI4exCFKyVkJzwaeeIEVk44opWPZxzJilcGk9fnMMZmI3WW2UgTG7+
EGIEglAtMs1g7UkRuxs49CCtRpkIw9sUyJ9m3IRCqjbCxhgY84hrXLtLTGPiQrQN0gryeWaJSX0s
Ht+hozQwA5zRE6LfTMQuejrrn5RRsPWjXyA83xDcJz4NdoQ+ww9LAewxBfHensxdgeG9XUjZlcPS
diFpMaXy1j1Av1oImgOerdXbibN+rpcwbGzDUEwevspN6+93R07R78/+nnh94/zno7YoSVJz1lMx
XlSXGfpdndPQfijQ3DGTtWWgh/mHMLjqvV6LaII1a4c3bGodc0VTR9lJAsNnZFwuZ9yas4aUeHk8
TgtqCYGApvWXlCYTRWwwEUIFovyP56VtUVOe8Aq4oKUarIMtKEVIP13hVH49WeUMENkOm8ZC3ZTu
ujpBto+ZzbfmrEMdUSvRZwoDq5+8/zryCT701VJo8vcorqdq8im0BY6pZctcIn6GmO6PYX5NYMHg
X6/RIIojN8c8bRjJ11qAcr+azzdnzNZr+FzBc7bAw83Miz/rj/mIG5fZwW5Cl/Z+QjAImNZtwc9e
9SPXnYTuIAmTZ6i6pKE4y6YQNrX/zk8bTsLIHmM02OOj2J7YYzhO1ZVNQ6nSTbfM7q26ZTuvI6lz
XL3WbaJz/ukah+/2sQ7kWQ+3IfS2A9F8kOZ0msggda4lMZJeVsaHcSLkVZ9alBQr8KkGRkVapBC8
U3naB7kJgldV3nnJ5I8/iwB8x5g2HR/hi8kpjMcKTMNMiEMHY67KshDi5yVMJynfT7z0zYvMmniz
jGMVXjpC6d/NokwPsP4LpOf/CzPP4YeZso0yRoQzMCVh11kFXoI+E9QVZb2PyvmT0qW62IxkFHIp
2agInVlNpHrq2LbecWwFdZVvXa313oQTXcYwCmAbKYZTiQaJ9uvCIksBnuaBVhXRqVv7p5rufYmC
yNV3/++6vQXxbJdSELXvtUO5Vzd7s0Vl5LEAKSatq77OarrkEv2ZxhMRU67W3EporFLIsAAuRzf0
uSRXfKQe9JibvTSUdl3dlEV9EaDGz6ZsKO/3ycnN1nYgQhYyjM9dWVLgQBNgJLVfXRI02lPNTcR7
o0DQsVTyEufvtnpZ865nrZVpegbLH3Maij8qPCRNc2D7y+Uv0XcX+eVOHisgiQoKC0pPUxGIFy/2
0RxBOvGhY2yDffRRiGVbQ9HZ+ubkHApFblEVwPT42al1LCAmVrvuqaKGYbdr4m640QIlG38Egecs
4zFX4DcmR5pSt/FaEApbObsHDT+CyVWdq2PURA2dIW4JxnPmrTCre7Vs7EpKrxjY8k4WwNc0dLhu
FBt322qQhBX6HP0trDBlOjARHWtx4QNLc4+aoYveDmsGX7mT7TQ4J/gfahJGOmXeewHGkTP/PpIs
EMQ93ZgUsMTRECHq0vMkTe9tHllspvgrFJAz1C2rNGzZXUOH46DaikURAe0xG/SleMPLls9KD3nN
JjMaLUNNnVVpFqyGr4NKqsplNmIXTYxypOc7jpeMF/kPSdiUTP6tdc4Ksr6JJIHswPX2JtUQuPgF
0+jvBZi4rqCA4FBCayLdG/hvijog0Luz+kuVdhO7ndTlDExdoP7Qt/+l7WorpIh9QLQcn1saru6N
rcz0tpZQQUuG6V5P7T6mCn1/NFhUejLDl8xznTduhvyVTgVCQwRq2afVdD7ZtMiSx8Te5WHXYvEv
17S41cRCxbKFv97u9aAXGEKUcSKGTtu5Vf1H1qYnkrejwCOIeBqcBVdZNVwCle4iwquzryF/Jo3c
rP/u3KvybVEXxI3Uhmm1oeMKsAqMxdPYZVQObaKSVzbcZnVlciucMCIf9+G3B73nfnjkxMG+u4sY
oksuelnQIHW4Qop8U5bHQnD90/uvy/7k0bfzTfCoL9q+6gDqpJTwcyWZLtqTnXhj3Nuq+Kz+YaHD
6wIhNOzrF0rS411wV5swQnCzC4qqXhH/nHBgT91Y/AKr00mSNwFM78+or+XJhgGnCTuva4FoMAwX
Lya3Vd1x/UMqplwB9TjkmO3FdtfnnRQzxp8RGTx01MbMMtnTVkLns3wg+uH5p3W/Zk2M1YsNbGwA
aR0DQkEzCOzDCmdSJVCV3XcAjytlXsozlFMn2OePSOEMGiN3dNiuFyLapLFlgsjbthhP+QsPFCkF
HWq6KwtTTnb7rP9f+D80t8/UiG7ELHa++6BK+lbjoxTAFCgLZ9CPpKrsNabpWtF58yuhhMQHcmYX
+lx2tGmB6tD5odQmYk6zgiZs9yByXDqa5nnC6DKecthRawDsJuv4P3rh8J2vhhI2+YPRiza8opI8
4t+U0NdTjmrSVygDXWheVAUxTJETYT/gDW+EV+/UTnBUVAN454OgOqy5F3i3CZa8PXptFPbdt7Lg
6w08S/S3F+LfxBwEDZfEn8cazIjU0Qi6AzS4MU4pHiV9DwttZsP1X2UPjFn2pVrBwyIWyU7pk9fx
nbRoFUm5gIZKrgOSK7Q6FFmOPfAMf4xtFSuYN54DiUx4ZAnj+i8JfYLmQLj3zE3A4vCJPyLD2bqJ
9YXllVppaHViXJwUrgYnBrz2TpXCySKoqxQnLZFYnkggcK3yvX/QwF5LmlTXpSwwvPDsPyi8A3au
2cjb6602qZxRM9MGfzEP8QbGsYJu1hHWS5LDu7D+WlK5qRk60h3NDjg5p++7LiYzG+fXseWENeKu
rlqPvYQIC6G7waJGmZsx9hO8cq5bvNeJ5rpcv8FhvZLUFLA7jUaLKq/yU11gcNfAGNgqEFPsuSdG
HwZbPBy4EWBZPyvRnNGfKF9cV5WRqlOm4ml96zWYvvlYHF0lYDPzducDqzhrvFPlHqY8vyVZxWfK
PA5PAc6GnpXyezBnEH0DYcdafdJ6bGo3vMVK02VtFVPnfNpbHjaBfNa1338MEWYd9m2FUHa3pTK1
fDnW46VfO8Z9nPgGVh99r/CAiXe0YqAbC8zNhEea3ZZjYB6imcfMCr5zLHPW4BbS8OmLJuOMiySu
QrYENVE7gs0+MeXHmL8hhE2goMa69ynb5OjvZISzP5pEUPlmNP+4yrtTa3fhcYkMfPc1jHPz3Utz
/9nvhMVMRgJQehpb4YDwqWaDPqFSALBm72EwcYMFyB162sU9mlB39L2P7ZchKirFnEIxvZQIEO/I
oH8zI4E0rP17D8AEzz8DpnGjNLm9pRYyWh7oTbn46MetOWtAm+kwFq9Tm45D62bH+pro7Ht9ojKt
PLJ5KMMhWzXISA83gBa9vyJV8dE/m+fxn0aw3Ug9jAoHY2I0yGfJ79NSBbCyIje14I0rTBlZWJVA
HavuTAPNFAFaUcAQzLsnshISMzJXpHnq1ST01S0vBm9ZZVj7E92JKkDOOrQ9diUCfay9szjMjbV0
lpatFg07KiKDpdtIqvzc9CdFi7oYg3J2FEtinQKYg/7kJfTGReWZbBWIpM1kYSncxtmJC0Kvl+Q2
4ECi9yqe7vCI/etcw4DSIKGgSuLyAVaAbzIKDfo3mqsn5XQgaKmK1uKX3ZO8QCD3WabGnhdjUjVI
gRv8DgFCyeZiEE/XBZUh1dw2/cG1CvszHydh0y2uxELeyUCYtaRBX2dFaHxRTCvABEWLEvR4kny0
FnsEMODEasz2FI/HbvLLH/hel/Z+XIrH3xb0fBkiG9IYYHyJTMi2a3jw+wXsrH77sFdqROqqiJam
jEVWa6UXxT2drJufCajhXKFW10sIqbr6O2GHK4+T/uWGJlpwmpemYJj3EnbTW0mDYjrp40Rj4zfM
u+7kU+GJDI2DY3nolBlqV9ycR72sgdHullcFhjsqp4eHfeHL427bTB8VeuPsPSGL+CIfoPG06oNw
u2WJ/dgRCW+VQQV98BGdUgeXMC903FNEIzi3lKP7Z6KMLTzmTxYZVAFMvCHpl2YwNH7Z2S3iVtYK
Y2isJVLzeK2rHyd0/IZAIXSmA6G+qXnBE9LibZlzcHU/7RVOQgOblu+6V3ReTGNxqh2y4LUUZjQA
IfQnPQTyoVpe3RgJ29S+2fW3sVwgTi9rWd9/1LfPG41OZG0H+J8Rx8x67ULG8c09gYv/q1WZsgmz
G3otAB90REp2nnwvWi7DloBoXkGBe7E3MIKWp77rQ1WS2xler3YkrzaZRjU/jDfeNhQIhDviknDi
vPPoDSyul/BwDTTguzlly6H5JtZqQTfzcI19aw/6dTFcQBqm63OQuDzSnAcFTbNeRac1R1rSznwu
kCtseZ/Ny86BR4rtytzBddx3Nq440up0AbGRHcN5Ljj8Eu3maPfCM58A3fGyuxZnGKwFpA6/P1pO
8C6zheLqBAqFQArn54GwLNunBi1WLiOFoa3z9IiAINNqSRqycb/CiFj7MgtV1D80MSQerJUs746n
iBSuR6dTh+LBR6L8de4Qg/q06Frs2wZXm5lycfvD7pJ+Kx+yOByppCAPNDGaKnPyjvP7OkMV6BZY
nEvEieWurUzI2yhZIQ3oYaw7SPgOs+tKbQA1oS53wY84J+fZJ75C8RI1vISjy6dftZoCoxmbdNLe
vSYHIVklZqr6DLHvrV1LjysCmLY92K3jRKvVztDxhMf3WDtpIj+uKNEhFcCWrCQKnUzKbJwKJ/5H
Ltc2oF46iK9hKhD1FHV1ewJ23xNzBJSrAd4Kaai1/+QFI7WhxecbDlzVWXuTkXCChjfJkhwBTtZa
DVLhtMrTtKnEBrMXTuKgQw1AkKVwSJ7w/+gruIBmyfU0nvkVCaiYIbuWeOVkF5H881dSFveU9dN5
+TEKbL1itB0ven/daNa37ekY3QMa5yADX/4vDkeP8W3r7G/ZAsu+04/UKSE1E8zd/Ersg7VpGv/8
TqcPpCVMaDIziHS1zFT7XbcuHaAjo8dJLlyaSfucw8rESH6/2ZGTdoTW1PWSWVGAJ3FuxGJV5rrE
uQ+FOuRhv/qLYEwZd6ZihsQxvYGvv5npDQfauJx0a0ug78AtC8AB8u1AT34Wv7l6xt1pGCnKAonb
ERoDcmlINx5lOuIuD5vwoO4N2zwG/01+i1KoSCIv330gITPoCUgBS8KoFg/g4TtZXBB0WPceCZBA
B6OMcFpCRRYIT72IjT+lqIIEel+rVwm5A5ksAepQYqvOHUfi1e7ktz0aVcn/yl3JIPe+zoO0osr2
NRpfK7iYwxFA1mlPVTqGztZMk/N4Dogf1x2A+KtcU3g5LPRYWapzi1zwSkdml/TSXA/bQpqD5onx
5PYxPTsWNlwfn5+mFG0rZPtHNzcivIhgLleOp7ukOUlboY1zOxlCjSEL7pgDRaXezQ+zQOYKzYos
kpMLbNmj3yyIOByb12c+aia4cKZ4w0K9w7091zRdvfA5D0gSrRUfwUyIcFQzJR9sy1W9RSOqgSYf
ytG2I4m9mCwe8tr9uHu7W2tBEsTgiZsecXUm3YQDPYw3d+RHEQCLBW29JGeGs0F3IhLuxDknm9Tg
UBtZ6WyBA13JJiwt2Oa9TVc1aLMG/AJeltdAehwecZsuIcxLQVbvYWuI5/JH7PQ+JfLlwf1ES0Re
nPNqO8hF5qWMYMGJ9swoYa/S9NlFQ+JjM79amtQFWcY0/Ab0KkRiwlfuKXSVBuE0FwDi1POsi7cx
F3daEkIU/iupHhf9XBfPTHggMBwJdRWSJTyzuKpDl5LqOLgnq8M7DOSZD8tVGbMj0zMHumUNu95q
f/CTlCgbing/h8le2nMBKvRFNpKnXbwn1sLG7AdaMBYuTqHM/zTLCOhl7RYjrxLD2d9UoH+/TVS/
v9i/lJB0wq/PrQavCoUd76j7m44n+de8m8y7iPx5qk+HXwCTSR495lnbdt4OUN3dtVXqr8cz131C
rJDRPXousgN2Zx39IvqkpAI2lcoptk/ukGzZze7AohDCqW2TA/aHGgbeX1idxj+9FRc3kT6sFiBt
7mwdbaBIoTaJMNIkaDG5JATsiMrLROdtoUaVz3r7+M6IOUsr33XJFqAG64rxTvg0eXFE14UsoR38
Em72aABLtNkZ5a8bjjDwKVfu0NHYS/yt/PUPYI908ebkK1QObkH4LpfWCk2+Pc91QBnO6fJzWMXJ
CC5VVT7tWXVGYZOHtfREKxkGGTFP+URSdE46RboHIj9xgQKpM9GqAHPC59pJq0t6+7cOVOj1/ERU
8hZJzJlQLtocrEFYAh7Rz75WyLiIaqdglV+2NoxHS+3WSiolpuZZjWJlZORiJISFhplbnBnrPs6t
d8vuXYMSQK3f6/apXM9p4pJWgmPmNjtTm7WU7lFqzKkZG/kAPFzcQEQ0exasOfVxg66dB0AHxNsy
tPVhL+nCDZh2Ba6B5KbdtKuy9rbeIKgZN2kJMLYrL+YE7ITXI9ACAU+bp73p5NigNdYnmErJoG7Y
HZ8FtCNKp5xIxbbGqBox970sFqbVIm9PJ9Y87lD4BqEbgKmSVEibogISEQ2wA7Mg8M8zS7k09LHS
Ddf8ISCBd1U2lJ+oslPh6ZglnKKXi8wpNax/OdTeWhoWHKhxE4l7AFb0lyCb/fLBO1KaQ3ASphS4
sOI3QSA5XCUOv4muMACXZh736ZsigM8qmi9RPQOyiDqQgO0Ve2v2jXFxD9TuqTVx+TH+yz/kkIP2
QXXdupcQzvD3LfwR7PTYZwIyNMiGt2vZJqdnKR+Xil/7AEdPP9OOIJr5GWfDexWn9RWimcRW8AqW
YZCqhSegxc6Q8z/YxmS0gpYjOaAq/4g0LIj0m0xyBNFLRKGcai84ahPRWC35Z0xRvDpwrBoCLNhl
5dHWbEzhBjI+jCBkbcjVDnsyHMoMajMJhSRHVpg0uvt6LMjMEECW82WSo60AxDpydyLap2ckLJ0Y
3HP6KVtR8Lo4FLHyGUm8TFq1U6GrWXax8HCiTpa6EaUZx126jp9R0OK2gnJXQqfkHNKWZYl8gdxK
IL85ofLsN3khPJRBZ62xQbelZOpVUzWLJxtyQTFBEROIZB/ssVVmhFW7NPJ6QTOKbwk4KV4s8BkE
iZR4CGqVzz7oTnAiJmNGa+aVyQN89H7rM8PK68J6dMvpRFaXWd0GzRyFUv0d2kNsT3s54QRvEEtL
npNlh2JkztqZc3fofj7ZEzF5BqeHWv+rFy0pkYBLsxsEejrF6CRoJxtQE7gcd6owh0/CuiegOqRy
KyaqzZUBd651DVvRaT5Ejpz9fsJsrtS/qAXw1SzVdRdaT3ix04PP/1/jG8DmCmOCplUULsJcwORx
ST4bCdqacPMeDWjIoaZRndKip0HycCLXn877iBhp987FvkMSJyU7ZYZneXuoCSMA2MjEYPPoSNYf
JrVFE3lUkkJx06xcREQyxcXV6xckm2lhCbyVSadlR9WozECO7XQBHJj13/S8Yq+FA+f/rSWES02k
Rd1CBTEB2aoOlkvk67lzrlJkYI5WVEYsKXUq8lw7+/BTxgeOFWuOZAzgYcmzyNiNhicTCCNJU3Fo
pyEqeVXIdBTeWHJp2/85Rgk0q2/69Msm54h+UCVjtayLSc/Oo8B2wtTGELgLfU0+vZcEhvnTs+dz
NKI56wJFQY4DfFInwM0xYhflXjoFkJLgVNKY3+78jJp5LzZY3T/MEIpC+a1FIrBo+g2daWcYnFt9
LVWnFSY987UvLAyfUS10/prXODMakRTT5isbfvE0Ky+UEzPcPuZvuMDcMPOxcIxhpf1qun/+3EGP
T2bRd4HMu2sjM4bMyoiJPbjfh4by9FGbYOTh9RLld+Tmz7iLJNx5MS8WqjG8GYvZZGzAA2y1xwP3
ZRyY2UzcAHcb4sjAOr7OI07chxdLxMBQ/DXGvbdjSbXRCiC4XSzx/l46COBpepQkmpDt0OKQj62f
oysiG47wKdE6cNe0nWdp1xPJl5hKxgaFQv3rXmltLHN2nY0uEKG8MJtt+1g31M6FTAc7Og8ZNa31
kUQDqhHxAVbBv4Jik0ws0QfuFh7S04k49wdZBjDq/5uH5hvXMVbxbha2VdeI46T2ljOMlPMMuW8T
nF1pZqf1B0x8U9tQ4d1cu3AhZwc5kwK6icMvILqaJ0fxKkpszFf4GW+O4CB7HZOQiJjIOQUxd64Y
CcMen0+JeOrLCGqkSMVqRl5w0YqAy17jSIezsKPcDcYMjPh1hz/3y3cixaieMfU6GvpNvC2L0EZF
4hScmS6NOAEreQmq5UJnmueATy8PLgARbqxsATLxKe1m155I4lnqDAxLyTQWOSFBkspTLeFJ0lgc
GoafY2NDMrciMtgbcBMgH6fHmWIq34bA4HaMjSs0xS4MNyRRlWSvGm2nyoptt/RiFYW3p0RdM6S9
N0uewYxeh/p4aNDKnY2JYgyzak1mmfUbDNvJ80Cay+nyD77Z27uolPvi4wl4eKXqmHsSpcYzSwfN
U2UL8QQS22HL41CIsOmjkbdAfhllGX1FtSxOSjgGdIwuRLTCXbgRM9ARVtv8gxveT4iOrrAjnhKH
vypIgQg16lWgFXhAbhmQrHnBfmCe5ztKtL91W977BWWkdjYka4QEtPYFDxxNs8x3JqGM3/W4kpG5
k7RJamHkBWMJRzp7g19YTqGbIPFfEHjvkBrUm1U2ez5DGdYmUbafBFQPtbVdQaLOoYzk+f9k4HwX
FfnIPkL9HDGUN8iGJy67/3fsUPB6j1xM/11W64d0TDe+sEaoe3XvbhZ2IUZadI9l8g6aS6H+YS4H
T1FFoPzTgVSY9Z8d0Bu5ub0oVbx0s1+uJP0V0p2i0daRq9WpKPjS1bKqGV/4ZgqJR0Qz21srIUxQ
jWr6f7Y8s7nBJ6lTm8V0BcYj+ckmkE20mMJ7VNKsntHewhTbyPCrMUOmPl1XSuMyZKU3a8Shy2oG
gw8rY0Pi2/hYqRUQ/onYDLJr5FfCd6keIdd4oEcNnpt2nXRWkG+sZMN3oTqaUVuma+1K44eNXVPM
wNCWtFgHQcKv08+KFLJtV0/V6g/T9I+AX1ymBzgzWEymyOm7aZLXtStMzzM+xZFSLR8ExfJV51ph
un+JVguBNpDPd1/RedwYkgjt43uDuvoQ9u2sY6KvEIlYvmC8rjqvYMblsKHovAePJ2cqVlv9gQZ3
BVyNAcMgTJUz3ge5wBb8H5ujCwqONQjRd5YMP5ccjs+EWA3B83I6rtvDzfk+KP3bSV2qvTjyXTqs
VK60UBymtsXVG+C7kUS+Y5AthVT2Rgo/W4mP1vVzjOeYCev5J0JZ1/TVL2RHFiQTlWFEatBvqSDq
QuRC9W+mvgNBedhkf209zoLQnmAenpLi86aZoIyNPKVgviXVRvzsWhiqUu6nbxlqnpg1qJdOlbfe
wkdWpFggYCqnSOHgpITwON4u+ImlCvH/Qp3aqWRLn54WJ+VffPNI+OK/xs4QIzMaHO6o2uBOgB7t
qa8XEfQdfaR01xqCtUF4R9KiF3jFHKQrphNgddpv6smdXVEjJFnQ0hho4yB3XqvZAKKv36jkDUws
JghXbD0PFcmqMF6uGq5XB4UIHC1KSp9iaAd/FA41hNhIyMAE/O7ASAp/jMPEpOCLPGihkktWj8ug
7BToUS0Hpx/oDf2Nyi9AFS27OHJs7o1Jw/91FmsBVI1zXg1fT+1vLX9uGrHtCa+ZuTj7c8SuMlSR
TVpNuRsmux4524sd1m2y6yfYvq7pa2ZpwC2fUBNi9Y4SGbFkgfPrOexQHFyQ6cKXhyiKXam3O4yx
keHJ3FLrn6+592CmlkfHWSyrdNyF2U0PEi3ZWRQ9CTiHfdiFk0TyPtZcl8/mTG70JAxWDe95qMTC
Q3lhCIMtdPCxU/b/H0k1SLykUX8C9B0gHMib+xANcACn92qL+oJU2FNr7cYMKJk3snvwbxxtnWrt
xHHqZ2S/7EbhWZT4uj+IP+aCJoB9oJneKZQwrM6Pn3xtSXATxzrMn/rqE5ICK+J41oJZCjbytUgN
mxrHgMAuwP3AjSLFROS1DZO63prn4dfYRW18TsD1PQNYpK/cB49pmB7supPNX6iwXW4IKgvYJc5m
GLkZjeS254qvBUydmu8HvICP9TtfcKzLs74jTamcVYv15rPvbvYc3DTNz3q14v4gPUgZ+70Zn2Xw
SfT0y8QPD9U0e235uNynSm15FexCSfu4tH/2LSEoFFiw/dBi4rlPQXI4/zZTTxWAqveWGfmO89SE
6TsFQ7+qgUDh7vR7oiRI6J61k1ud2L67ksCzKqkjmez72pbBhFm0uHJG3xTJ8VoqMYPrZw2A0rWo
WrtLwP5oPE69Vcd1AdZ1YFPDgXxHdhgRTZtQunMO5tnTI4A7MIGelonuNi4bwjeQshMYjFTDzk6N
c1mRhRYlh+nbREVaguLHMJbPLZ+J9cvded2ybsU2k8PpKabwImw879IkhMulTnPc3ydiBY17/b4Y
K0rWU+cLz5Msmp45/RQuwK+5/+WLOyWZHjDyhSdCtkKn6rhm10byBJezPB/0YslpzSX3UL4FyJgw
jPwBQmxqU8AJGzeg6YFgEZNDlVVswHHsx75MUTvHA2JnWNdbuOu70krS2Eoa5Z0rO2RFxaiiUaI2
iAx0NoD9aQ3I+oAXbyG0E3Kpi9MFUgpuEeddwdn7wynFnuxkeVmUmjXlJlo2leefsau0lKR2BNor
kSQJhzCHKuw5b7fhR/Yzs5vyMPdqQPQMUS1EPDYDvaookwxU+RISGbu52+p+4qTyUxYaAftAIm7o
TumZk0+THNFX5ofvSwB0M1SRvIZ2asRLmoIxauyq28RhnCJ/V9dHMk1EZoPcbUsQi/gJYMTXKv1v
2N7cpfYxfy46XvCbDl76prMN3YVIFWDxCpbEcR7h2uXq6JD3UN5/LhEc8EyMjPkzIAFuGeX7q5mY
PWlbBVsvJWIg85+ecQIU2JNgzRB5SNecs3u/W0i+wa1F9bsr0MK3b9GgTw7ObjZsbMrdPBcMb4HW
faXyA2qnIAVBaX5F1ZYh9sbE0lvb0Si1UfoWaMiqSplONYOAYjEHKLbgRLH89WHgRkNRVrRz86Z/
wisJNj0TiKFNsKO7/vd4ALJsMibEw6QhOtEEuzgpSZkL9gWxePz7cjXoXgJvEUPvxg4qj7WfxT9m
289wbJgAPUWtqjaXBAABzxXOgMb7xObG93f0Ng4x5OliH9Zot2a+rJgOnUZo3ROZcSttDICGMRzY
pwo+bqWMRg7nqF+M54FuFIAINysfy5mtAPuGMOxqihLHuWrhZfwTnACafmrgLy7fC5NWneHdImRR
FUTils+zrU5UbhrBJPHKlBGkkTtMNnAk9Rb3EWCEV+Cot0cFelAtw+DypuRelyqneJgj/2DM4R5m
nKg7F8vGXCrF+aR0WqZsvcGF8EOOAUanFh6Fc6ASMXVtBZ6KDaaatHsKgKRjB28f7/8wEfsOuUWA
RW27LdZ2QxPBSpR2u/YxjHYdysZmezdXRPrDfbUPw2xYibj9noX+HO8ziRq8ZvUNuPV2vQ5+JGlI
bQSD9YNbhjtnYfH1MSvhyhJJSkXSkdRAZptYpt5Q5ddLWCXMQToPzMFF9fjah2rl6QIQRIEuXaBs
B7Rx44F/RJH4OZ3sZN4icN7ve4p1WncjKVdYwBeTEh4pkvXQwXF9gO/Y2TKXRnvtuT25tI0Wg3fu
3uvwg2K99wwrZpU7paf0v0HCkIkdoL1etfZ+/MmrSxoZec+TFJhp8joAMDBS7vRoPLfVPqXLe502
prhHXoMWgotdJVUhyCJE6pcfg7q0EofKIe7CHF9WW3U0MENpChQwjOAMS7LAqxMof5kNgxB4TBqO
TFeJH23uWx6Ek+EG3mUxcf35TGXhwXcsf1vmwy+v8Rx6TBAzdlQB4Z6WpTbeZYLViV9v/o5IYPgc
AVd92m1Bf7FQuT/F/Ig24rHNqltF0B9dJtgb1NxdU6y/GzKYgAIp9ERtbEAb+OK0IitmzHqN5N3c
ECXt83I5AZ946lNyS6V4rhhUNCjwKpWkevAEupzdHll7R3YJaYjHgpq9Kc/EV3eJESGjfx0KU9+y
7IxpH9D3O1EAHORcb8PMxoa2GY1A2khkoxfN+5MkNu0/kA6nLRygaXnqDQDPDl+au9Q/m8vbMf4i
SPBen/BXZ6aXf8PlukzZID5hjPJsNKL91bIj4ZRnV3kXR7wHwGn3jIwTOS0UZ0L0yHta7JSTrYVN
vwTEigEUrP9Fwi/489LbGxhRcBy34Hefu4Qmwl9xRj6Oawh6h1s/KW/TwJsNXZaJOTT40YALE4bt
8SjpL9IDnmaxMRP5vxrKwrDM2yc3sPe7KrWr/ChiwU1rWuT3ZNM/pBRdPOH8AxgpE5IFxKod+gXw
SPmknvtE9xr3SZWAW+Rj5kxg0TDNCvIkc2buVBwolHAG5KjA/9OplYpJSC9q6m+d14JWo1efElJe
0uNZowE7OYexQ6stdQSRxXdchS5D0LDoKOdijLTeCYQ7phMMUKThZrIGGbgjSvEwHS2+Uj6onbzZ
1JYFKgPhkoDbQOvZrcgwZwoogoxcSj2tlU/kowweQjif0vwCyJDEDv644Fh/G9EOuO11yKNKfDww
mM7xmjLreNqFGQ/2aeCpydGlTIjuuvTyfmKBpZAe8Nu9NOCN/m+ZF9tv14537nc+jUiIt2YOLZrV
u94DqzbDq2meF3K7avIGyITav8+xjXvA7v8U/eBdejjNFpr0fk/zNJTni9hBvMAA6Q88+bi4jVaI
T4r0/5w5QCUt1VWXsrjJSb/jDKe5bUWlv+JeA7GEQZbdQUSY6SSyc1kJ5iJ4Zhds1AWkRwKdMXQP
1ZqUlHiWOGY/RoXTA/ZGOVh23aNbDovXJ7rVHFUUVjeQFoESPAp3s5q0uvRO39k+nTcf2DYJTgw3
qv728fPVp2vjINDxDCGR7iubeZbaeMr/cu2W3Qpm+10vPFfuhNKgPzSehTxlaGYupFPt+d9oJESz
XANqO4kf/RAOFXLn8TkKtcQF0LYloOHygu0AuQWmUbpvR2h80o0qKgO9dZaR0wQ8m1S4WxyshHNo
T6C/BT9tREF32SIeJl9AZz9fTn9PLUwLYZBqdPym8SZp7IBu/v/Ty6wNcJSCKoxj3PFPkfF43s9Z
52uI9h+ekxqB/MgmMCKDHgaXXcgtq+15YH1qI1r8zabs36wHBRpbhHFDRibqIYzfIdYNDvn6rSyc
Mpq67CBHZuQP5TFBFOyU+GfMoKk6AfnOkrNOEMF+81KyPfTTYul+KGDt54ggg2ZzQf5E5WT5CJPl
VMuqHtWemENzf4FiYbOOTp/ItjL05dHSPIUv6oPHE6ycRgOOHxJ5bQRaT+Tq+6Sk8nZE90eBOTR+
VEogaFnifiuTP7kO4sP7NSaAegr3hn5Tz2x8+pUxOGCN9dCh5THOz7cv7DdTonMClbsI8pw5aFo1
crtcsP0GbPhnbWVOE9pIqtyxCB3oa+dyY48OkmHH4GC1csRZneZqNhvcIfYR+crAXbJGe0r3zCLR
e1I6vi9itUI/LTVR67RfrylP7yulgmJXljXeWEY5LL0FJsiRAB/ZetM65gXE1Lh5T34hbOacTo22
THxlgeQmK8zXoAPT3GV3cqhmfk1KrkEKhd5gA+aBD7j58EBn7DSAFFRbu8T5750O6g59/cUfHrQh
7PSymm9tBNE9mPaQy0qwffvC0O6j4OikOzv8JR1QmR0N7whcawjpkPOiP/F935I+YW2+YwCFn6dt
5OiKnx0vLUS8/9nAZCWrUVw3dHn/HjKX7ZkUmIY3HM/xuebdnVKs0quOfzPfrycqA7MqTlIkx4Hs
pllnrBUtzV5bHLcvn2VvMP1b2dieH864MD88b/sc1BhyYz11X21+qu9EmmDtf97tnqCC4oJtoitI
GyfC6SRqMVCY+XBMZt8pB/NmUxngb3dxSnrkoVm9Qs/+pywXUUaFQjufaCfshxwD3VYs2WdBTDnE
V8oUeCn2B+lrrGHBbtYrt/L1epz+UQHfiJ2qoHILMud3g/HK2ggByLiUcBirL4//Mst0J/sFfyXs
bXZysqLvucuzJIvkrtWfQqwdtoRPrJF4cRHx1N0zlpxnEMyF/WGapUdMiXRbYX2QYPcSKAlduvyy
Lra3KrzNB0BvMPuS/IbCKGlYor1gPq4vQwMg+e7QUq+R7jg2Wq3bkeOZ0UH/tI5vW0XfSS+0pB7M
hbnTW8hxiM8XRH6s3Uf9hMaRdft/l1EnBBHGkefFbzXtPvh5gNcHlRsYVSZSyBxqq4MUgk3oV0nH
Z+8P2Z5wy9acwMF/KiQomuh9lzhsTMAp7MXybk4ewRAEti1mSWxpRZgKmIiHF/YDNNOclJetQ70O
DZriIwoG5516Vf21Xj4Kz8uDd8xfgGO2L9qpB41l6tSwLmZdd9kFT8iTm9nxkg8HO/aRwkJr9M2h
KAaGylaydVKlt72SJ34LzGTDHnwUxOk2Ho85bO2zNs7m+bTn5h8pWWiKBK/dL/fqMXrJThqR8Ugv
ZgRfWUZjIk8lGXFvGy35/hsJN+1S4TFEzwI2TY+JOM+ZmV4QAM3UpqGoOBMdc6vP6Z84FLS09KT9
hpseKGz3jgXl0vi8AvxmML2/l0eeLLzbN/1IXg+F1rmTSrr3yn55/vLZFGh74IbsEE8RcLxMNL99
IyJpT3uJmEaaOnEPBtz8RCY1h7cCDPLEiJKd/XMqZechYGdUc/usaW/bvL0e2OHyyNIm0CJbllVe
HPVLEy6CT1J3+AABi0phLXOK9yiRseqO6S0qdCjQr5hGs4KOT2rbdGJB87POw8OHhnGVw2dXPJ1N
TtC4ivUFXNFZlbg3VmY4sJwO5G9GBkZ0KVVgLioGKegNVt75OWi7SMFJokSrn6MAEdacC01UyHoQ
Xb7AKI0WcFkJ9l5kmbgaw/d+Cdkf1m4aAiMZYPbo50OotFB5BDeizF8Eqw//IQ/8D7pl87Zz+7fa
t9Le64FkyzL0XIV4/+x+fbZhrXo9pZHD3MIlYyddLK+h6EN5zwFk8BJJyBzZSTgISCgQ2F7B696d
AiW1K/m+N6pBVrf363IWiQfH/Co4MifT2CBoaUJuDij7Cq7Az61yFNVDM4tR05dPbf3D1rMM+4BG
gR/9BV0c3F625VpHb7sppmu2RJhDAcY2DfKDcDG7gMP0M9eGDbKoP5x1NgagqBWPFQvKjh2PNBS8
5vpOV83Fne+xCq4xPRL6EWfHhjjp90KF33ekajq/2p0e5ua85sjHTwGctLm5PriXs+L49NQLOAil
8GYpE+uiM3cuztguZb5CcIA7UxYilYNH9DM3+dfEAnPoC+BJF5gFU/ZM0WhxRe5l3fL8fDqyRw7R
XIERs0fPWOgpQEfKiq3QdeyHHgqNRqjJcuxPng4ajIaNzyflkNUJZ43fVLdCIu/qeL1yCdWk4f8W
bgf/cGp/jkyPGki8QUoVPSTnraAvxzPpBrb7JX2PnMdhZ3WRKjd2YPimMe4ZNKh3QLvLag/ug+RH
O8t33yQlLVD8zJvC03pG0b9s3J4XoVHMX5X3lIfGTHSohUqv6RXKIYCqVPUxFErX1/uVwA4LL4xx
3Oyd2/r6flPBr374v2ZOdoPM2NZLcSGWthvvg6aJgRoXgs5mfLc5YpwB73o/M2xySGSVrT1ABg7c
ung8Y1Fb78xxac+U6BJrcBv+ry8iOo9OHKCDGRPIlCaBvVXKWNendvhjkFTziXH1ASi7UgC0iiiv
suQy+drdWmtyDtL52BZwn/1L0zR6XG4HuwML+fu4Nm4z7f3d128mYhuIJKAuRDh5f1zi9+bmrfLo
FAH4cVJFnSEgA2rNsBVsILo7ia99nEUBDBI7FJWbFCKxAXKpKUPhzlqKoXxrQxKWrOfsBmK8WxUh
cZOkbXpavSUm7OomZfrvysujT2ABPpMV5uP8OfMW9QwyEL/y5p9NwHcR6k2YclqYd/HZe4ImiUbG
L8BwN0JqSYvMJs78dDQ8hCRoYpQEWzfgXwQQQEFt80scaNp+51tWPn3Rku7Rw+4eEL3UZL89y3PB
ufMoP989ebXHjkv+OxB7NSnVjLM42iry0RduBsFrajeJuXbrZjTKTYX11UyPNWfq73fArd9JFKua
Rlh11TweONH3hhGw8oxfr4UVVGJrtHm1etlU4uDhxq2lXXkKpPjjkCtATXfwswrCQquPbzdEj6hk
a73/Sz3KgCKnn1FQlG1nNxpOT41nePIvE3cWXo+Ir1BLwLq4+8Ju2UDXbm+LpyAeokrolC1/RSXm
crAEq42hCNlH7XmgG79hR6X1YGb9u960BTayaMdgD1s0zaVlZrfl8H5UkMseWkQNqUbnUKTOq8+0
kcb73Ox2kOa2BkGZNidrsLkiK2tHCbayjSvot93j2A7HvUBL3pzBBjvGT/uLQPdttHbD6+sLCBlh
Oz7njeZFmgAwxFwPzB6/gpuakuIplhrW63VPHPIRMkQW+Ns2g0zDmNqM9e15lpJF8/iAag+LS9y0
vRfXDesGI5/xPw3QI/uml/IqGomvosbNQDjNXqILWCNpgXqK4OPbGv/1RspnM1Y8cIaXtt2YLBuw
9mgwKeXgI0WZsZ5tiZ39bu0F3hZRGbi+9n9bYxu8wsdD4MH1tpR4ZEnFxcXGI38fBwXfqNDkYkPA
FXbWXenOg+nWmc0RjIG/qgO5BQvNcq5Ghh5lyGCBca4ChxSpf8mlk/wttI7qPs5DQZSh1kRyAbtn
hrMSFLTPaPFxnnCQoH25+WPz4JO25urcI3dDDV3IZCuReBNbe62B7bLw9LCbugeJtVHpVuwJiGP0
1UJyasb0KTyn8v807rUnK5q7JPzOOvhvGKSmgdHmN2vnfRhMLoh1TVbzvx2KFHy1hL/8u3ENQZpH
IoZEDPiImMKlOY8Zy838Pzt5plsRHoK1DYVk02wn4+swIfqh8Q6FM8jqdUasr2anxEnt1dZQz1aE
77biKh2jzpBV8Z3wFIlSqSfMdtaLMv7NNoQVZjDHWlDwED3ejc91FwgZ9SgmYFTI1cwKfRgm94eA
Xl6oXu3GytnV5PlU+av904V3c4Z3DU5x9aDLEJk7y66AU4+KQ7KUCrK5mu/rPmJtTHzs1CQwrxDU
+Mu0KMVCDU7HMrYV0mOJ3MV2pn+oZrz7OFFJPgggYrTappPjfJG6S+8ogS8ti+zdH399ij3UFwic
U/cp93fDoNFwiOcm5Oe0lwMKizOk4b30CNvHQ9Cs948YGVqkF6tNTdBgYpypvp57yyxwLvttFHMm
eT0EWi9DmQK6zfjozFqazU5v4KTRluX58Fm5hY7xVLWOfeTfvxeWv4P5MZhjDFFtUBi5PKS35oS1
HXf+sfX4s9zZDAfMpyjX48NxBqtRup2rmqeHQ/hpdgd+2NkokIg/9idvX2XkRZJkzXpVhBBVrl7s
kGjalgVdiZMTqvZJL7w1VsocInVKzE8OAbeY4BNI6C9QxzkrVL7UG5/X70VUAe7mP2gF0kmqR8Uu
TAov8XBgueN3OY47XCO+3g/XswlgtJKZdRUg91EwpgtIiLlGoI/DPhtesBh1Br4tZLu92133hNlU
dHSR34X8LDos8wa0f/ZjfpB9cW8cTSdH1eerS9oqStWev+nIkmBfGQWXn2QEVzUNYSdE4opmlO6F
LWuuO3TEoyMoUE3oraE0LXbibHt8yf7VWnpQUQ4Q7GxECLbejSIQDktdaJrjQnNh0TyOs+4NIt5e
gTvgSQQ4eUn0
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is 3;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 3;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(2 downto 0) => D(2 downto 0),
      Q(2 downto 0) => Q(2 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_6_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_6_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_6_1 : entity is "design_3_c_shift_ram_4_1,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_6_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_6_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_6_1;

architecture STRUCTURE of design_4_c_shift_ram_6_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 3;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 3} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 3}";
begin
U0: entity work.design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(2 downto 0) => D(2 downto 0),
      Q(2 downto 0) => Q(2 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
