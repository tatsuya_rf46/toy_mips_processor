// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_7_1 -prefix
//               design_4_c_shift_ram_7_1_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_7_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
Le7xF0fBEYDyHUdwvZ/9J6ZKPXBmCc1M0kTm6Jvto6EoO6rWw1H+u5v76UcgKnntqPC8pfSh/xzb
/sjqSnM61kIs8ebYq9y59Lii7S2WneFNsmTNGID7DVCS7tfFZgUJpBLNVgNXEfxfKH1DAN5Vre1X
s0pw9j6w+GMZOYqLcxunA62MYbBWijyDMsPATh4viejgXVi6rghzIroU7uxEDnu0AjQQXpsZzgtQ
qb/HknX+Z2Hr3LDmiBL/Javwe8OZKzLtQkmmgUV0stS6uzFKSiXmDbb0/JLXizgjfL0r7e4J4lGM
x8dg+HOBQHVFGmajhptYygm3MY/uAAz2G7N6I0wNFqzxivcP8RLkiVk5XFDwp2TNdSQOPvc6xOZv
pMGzdsRzaxEO8NV2UROjE5ebF/RgJUkImJ4SD+NFSyqeHq3XS6rbC25JlruR5Sdn1Nrdd5u5Vgv0
xnBAOEv7HlW9RWlf3g8pUQg0Vo5dbwlpksPNnLTpOFc1ysQDy8oOrtkBbThqzupHPmi6dn66Ba5Q
CJ8xGJnyeiCrWwKF/CxQf1fYc+bhYf3R0xjkJ83tWSxU7VTZYDs+osamk8RROamEzQaJbxqoDy43
Mviq/gruWykD9cNlBeDkJnHRnhQk6pqfKFaU8UtBQwuuYwscPfoNa7WjKJL+tgNLPUgnS3hUG/3Z
a8NOvvupjiKObTW9Dt7od6EdjEx+GTRwehmCmxTVt2U0SVKbh2krizLvwebPFg4SHQ2122JOshiw
ODLhE1w+0OMZXQgqU73bKx5bAnNKxtIc4CG/TMYhLY+8r3R64Ctyf19BEimjcTxSG9Kpqn+Po7WG
IjWp/EQUVVeTXLz6BYEVm8Cslv9cRTKVONGI0wnBqdiUfupAi0noBoCQI/xlUPf/RoJkgGYXYzbA
mT6W+xWhGGEbbevZBhwwAeYDIWUNzvB1R9BcqTpVkhk2XBXz0V8dBSKfAB8VNJX1hNgBCEJYVEaB
TYoOv5aIWQ0J8NDclWJtl9ZLerxr8kxUI+IKq1fRyPqSwPRBmFWKqgWAvZlFujlDPeOwmNXlKg5S
X5F9vELSb+NbLG3cutdiA1e9mNTRvRXGAhVjPlxvU7kMFpTMEhKRcARfgGdXX9ZZ1ZXdfG+iMs1i
AEWawRAqWqkGyi8IkDmjgkDkgmOK7gbE2lCHTV0JwlqMWCFPh4wpKeKjDZHVtojKLQht4pbtZjne
JwZtFclv00YqXwIjfySTswGXZfkF8tsSKx2QzGHGd0N44zkcTW4jZHKh96OJPr8jlq8+PR/KMSmJ
uWT2h02fTniKJEjkKia2GMV5ows93rqGD/UPHVO1IECkJhUDfQaz26zfULu38zkreH2GEk1992Kl
8znwY/f9dbzwAzwGawrfEXGqaDtSq1JQoCBjxiLM99DomDTQInRsN9uWnHuXhXOY/demw3AH0AEG
xwy5OUQ6imKH010akD7kDsBYsrlUfY4m3hSnfaBpRJ9PBG+uaLp4Rlkk2FpLe9IViDFxm6Y3uYLq
ad7brhMqfjF6oj8aV3ksjMnrWetmBtSYXoKGxPSCkDO68ZopD/9WnR71gDTsUi7k/+n259ameIZA
8yfiSRonKwFAcVBk797oqGFluzskety7mxwd7g3QeOboZ0lNBDA0TeUSjypIxD5vcPfqyWzWvf5w
i7083OAHcUCJeVs4wQbb50OZxQRAq5nzxugb6SqbhOOTYOLjRCzTbauS7Db9cH5C3B7PSgJy9Bvc
WZmWAc6LY/rHrXkEwbHS500oF+X/yR6TCPdvq9XVdwF7lxBujEeU2BlxmwSuuE8kHXb/4mgh7mG2
0shOw2B/2Bg7I5MSjV0WtCOesDc+pnIwHrVXGsefsWDOaan3glOnPrdv7owBeca6UPWS6jQ20SGY
YzOtBRnvPQxk6FsA6grIIbbYJsK5pjZpPjITBNNXXq4zpJMLib2RGsTACler2EEgxGg8Ae0Sm31m
J7+2pf0Hwil/m4/enkKytKgyrHazazNTfshshxFlckifnKGqEfzpSMzGnnhMHHoBuQ2p5h1a1r4R
L4t3KpbXunjYgbv6sa6wMYFdGVsMbg7a7NwXAJK8QH4HCgRC7UfNK6gBYOhrrjgxq6lJ71sB/yOM
Qi4ZDUEY1hVVCnJEPFKzSJHWD/xNaE0p9BidkF2D+enO8r1jq2sCPyzjNiVEr9/wQTCCAqYqEpOz
MKn/uXMTf1kB4rJ3E0N1mW8xxjvBI4KYeTqL9laze58B/wLdPiKQ8/A9AwZTwfxqqC9kviUfuB/m
h272kY7JYVTr/qy99Sa69njfEpqhaaQ1rupn6x4NcKTTVosQSt9DG6xucSKn7/9rxWqKEcYQIdk6
99Yua4lx1g/VrreS/1/z/gzir2XV3Uu/2T2eDjTSKLU8lix8X1Y6e6VH9zdik0i5RnddVUExTZG8
bNZqxtw81e9PVjeR/e3oa1q2lxlWbqUkIwgOuFEyRjs4ZberHfKOu03croZ5ZoFBWlPYTlq567Qe
G5NA+TVh1SpAtZfPZ+3zFaf2B4r+Z4yK/9H8W98fYBzrnu3yimv/MdEmsvwZeTM7m7oIz5Lky5/6
4ugHSsZi3O/sCKxKWjXml+WBvUmYHQGWICcI9beDutpqQe9YkCKu47Diq11QFCeDDumsJ70P4biU
CjUi3NGwkcD0J5Q+U7TsUUbdPM5G/JZsanRsF7Ie5mpxSCHa6K+RVCB2IvnAFEmAT/WmnpOuiCZP
I6l1TNjhmVRO48/NGdgG1zP7CXfQjhsnq9Z/Nle/PyFZFwnBkNCd9E3a0zRplqP1Bf69W4krD62t
xGOcdOSEHiW9Mh6fg6HeGiUOD3rUX+KmV6ZbzdfWh8zak5o67/72zS07vj52YGgPjAaf24pT3hrr
WOjkVvnxbMibd/sa+i+CP/hoKhISPP2WknXjQlrgAW/1+L64jKPm46oHJlvnWndOuwlCAnK6QXs+
eKZrTDaFOrY+t3N6Ha34PJ1ecJaA1qCY4SR+dYQt1dufqMi5t5eTCapKGvta4i/uhOQAl47S46+4
AWDZj26qPR5W2GtsLppMgVuz2MbCuWZeBYZAm1rMubXX1l0wOs1Ig94jdwLfnquhf/UA1U1JwM5y
iAawaeEp0rN//G73acU+6mUvDqsxIczvPyQji5mWrrQAlq+Qe2BuK58flwCLBIgfZknSnnABiR/h
KdLN4gcZ3oHd9W1z+Gz+3WAv/4FPm2VKtTtFlACLm2NIi9BWv7QqWEel9SY9kBQf3XsDzFM06wR9
Y5mx26ld6Nf4fiMhW+LJ09q1t8aGl9fcXxBNWyCruOrQMrdC43oYYQxJW57h89AVIiH8C/b6Frp1
LZCiDnLv75Ut4/Dz+wnlVPzzs6WauZ9aqUWqJr83OQXQfNxSWlhu3RMBfPynaxGL5jsxLkmUYkTm
VzPtxBxe+FvpMhJI2nd7N8wco7YyPZnBf+ne+8/MyRIu14I4NyyO+uO/yYNtg0x11QpWS90Cm3LM
Dr0CVCJTjyWJFebR2TKDquXSb6kFyA59fNkzkpNMvW/KeGfXF77RkPxvXcKfldU+0gMYZUCIYe4Z
OKmA1LwsF27hm95lS4Dy/gY7bDgCDoILhbX1HNl3YZRs4sxS0vFrLUq99GJqG3rwilpjbLerARjE
DAYUNyoQ7gypmB1j67JBtOI4v5+UaGVx7rB+sVPibB+QGZ4qiBeaSbVnfQeC5FRqf8TGv0jgepct
Ynw61Qev7EO+gTSorZaIAqNik/zoFlb+mwyJDW9e+7+Eib0LPzq6I7kiuvs/KoJ/lXitXfs9jGde
qmfoD61Jkw1lgwrqj8v9BiIkeH346S9xdmhvWbr73+wxTctZy6ANEbo5WX+jvsnYxwFeZhYCKy30
wasrT67eHOgQI+XQHyc+Fhs+y142HmzmH1KGgznFNmrOofLjCXH/X9uRL4gH/5TqwMGgRe6YGnmr
ZcFJqonlD6oQIHOX82+ZBM0gbEwgDLmmirlqqTrkZ15PJp2KCNqC2w1pmDQRxJ8P0x4MHofW7H8L
qJwdnUjOnnm5xc5KVx5j7ErXlMcAoGCs8jLSny7sRg+7pll0Jj4QbdZlMIDLt+fNgpyFZvMVFdps
R1YVNWfE0GpdbEQSFk8JUVCQuOavO3pGWOx3mGjvOZq2TkZnJEFjEzG2xrpK96qr13xdAgzUH7+m
xrbu1kQWqMAJyveLAx4l1PoZqegsV/VrsfPn2/wKp/T8MUWdA371xD84X9Vyi6OJnAHVmNupM3kJ
XsbdxSpGjJopf/wkG4VD15YH+OChAssV3qtQ1y2G3T4epXK80bF6D9p8M3XMyaojjpv3JrKfDqhg
vZOPCjCDIEHN+qgJTdfnCgIktRJHf26Cs57FA4SeUoVg31QPLyf9Q193UEJ2IYTqMidDb3Q+F0YF
qM4zJkPteZDWwFy+y7QUv7i1Gtmfy6DLVgyvsgVH7BHotZQjwUZFyDSPltR9UNrZw4hs1dR4ftAF
oP3IbvrwurJ9auOfM/Zzu9J36mMQUvZpgSkKYf1/s2gQXzdf10A65DZLMY+OvxC5EoRpil3tD+nU
vRrKRnSEuSwXnuju59P6vZLSwieBli+Tn1C1x6nrj+xGdW0SUc8XNmh72MOrjx6891ncgbTw5MuE
2bu7uPgb+xLDmuVq33+e+fVIoBumKcqQGIRAsMkhls6zXX0LGQuK6YiszZqF6FWH6KHn8hByY36M
IDaUj2Y7XHwO4cdx5ZXhGpQTeSnjRBxQSlAHXxRoS71t0K9tu0HcqccxjgHqI9SnVpMCq3QLMYx9
JBOji+qzkb4n/RnjEHAMyN+g++V5q0lsJHP6xctlpILGLDvf2PCSn8mEvqIHlKTvi7O/gPVAjXPs
O0o54XpSK3tFid17tvBonShDqLMkSKUe6Ux6mpGf877vVElaU4uqGL2Dk5RGCA9ALy/RFCx+16jI
jYkVBKowLCYQZjecbawFYTM7YUZer2XdepADq4cGFAXfR/dI9vYR6cXNvntAGBy7qCfco7fnq5Ff
FPzytTN7QpNUUWj9uCqPwH18WpbWmFt2XJCHGOKPcLuhu38d40t44ORyqswXERFNyOxAI/gpSEZH
gtNMYHa0VpXZjgkc1i8L/lsev8wUt5XL+EgGRRQ8xLQSK83DLmXAheKVBPhTn/c/XS9xAwTL73cI
RMFogNxXkda0Mpbnr3CcGpI52bLHKZqoxdoayWypx16k+qb3gW1E3DsnzLcrPSUa5qhvLnP/DZoi
J0qBaIrDjX8BIy6cKmvfo8qLO9XMP3C7n7jLj5j0dDh3NtwNxwa7+mPOt2qS/CjnPFYJEn084qsX
Yq2k0lbm4Ol0kVX9X2BlRFXmpvAXraovpswrVIoZTCB2GwkSKeyzS3Rolb79q5a/A6Qze2RsfIt5
zpzLkWzx8jj6wH1IVRNnd2EwxlP8DH87KWDEaNu5DmHf+QoWzl4eFrrvuTiPstGlWCl3FR3e1nFt
+UpfHE/VEiaENJbBcRdA3El2Qztd8DGhJPULgtuvHGP50BU0XnHD19c3AI0Tl46bf6MMcQ8ColCq
+ghnmq2/K+ax/UgTx4WuYGCC1OtWsoZFCQnPRh43ccWxbTeU1FQSMUkqzyMoLvrEmVUfneseF14B
/HKVOAW/xj7Sd1tOj14IihpsErACuVEIKiTCAk9/PhfPPDqY/HtyBPU5hlrJ3H1W/tOUdQKnqKHD
SZvw0pB9ao17LdLyHO4APpbJm5EawVQY37v+SCcE+xPt/8NpvA4tGODZdWMlGgGZq4vb2rykrW4S
nZUwOWMqMpZGgDw2F+Mf4cnR9V/NQpnN+r0EijolJa+Z5kQI7lfCfkatyjdaWv0P3iMW7Izm5EqW
lHVQbvfsNZgoAjSGogCb4er6kLx1h8ancEAwyyhUJgTDvZ4dmxY3QK1s0vUiAbvYgFljJxNR9r6Y
8bZtP4rt5Sj1YQ0ff3omDUgdHTiC6EeLBnxkZ7K30pGMRw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
