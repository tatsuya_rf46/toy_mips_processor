-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_7_1 -prefix
--               design_4_c_shift_ram_7_1_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
0TLOYjxzK78OLoJPSBqOccDB1eCjbgrnAg7O9jf1aCdbR2+kw5qPshCCAW/Ao+ASGHLkedlNL+JN
vf+TMipODGmfP6wBQWrgTcPOmXjAzDvIIvVprACJkTRGgFr5Kqoa3ZfOBpsZGaRr/LdtSWvvp+qq
/uyISdMG+qSFtiJeClnwuz2LRkONRKNmUjw87ACmutpeRHFjTdUZbipM4uLgxbbPH7b5S6332nW6
sZU+TeNhPi+NP0lCAFHVdDdcYaZuMeczaaGuwguh8NQ3ubZsfpvPhOEmQqzsHSrSIUkw4oSNHih6
1Dcj122MlnF7PwjJLmijGsbw3+J+IWa+hgJht0beNAabqyoCpLUCRWp29PYn2aII2Wib/6OZvuzl
CehhVjDWhLSpaE6C/xeyour9NakyMav4ZsFugkYt3ojMxl+KXcxUs4ji7b16uoH76B4U69nGnuZ1
c7GHg1AijeKXp7tnoglRG88u4KjHVZnrE1Lm5Td1BWtt8CmdfJuZECs4o/2pFlARsR5Y022Anehr
KajVuo1mbiWwofBZdHx9CVm3N8inj/aM3Uz8Xo7sYIcGnJpXl+xONI0fHYWI5nRaxi9ksbRwOkQp
J04HKiQK/r5vud9z/qg/O+hEkydl2McVihyEd75xKsYsvwaLvzrXLKzNT7OgJvCOiY2EhjqRCrDe
st8fVyEVPROkwYzB0BnSTMx/cOWGecDbd4n21T2FjSycR0rMI/MfP+D77iCgZXjf1vcu9TzbnrdN
ETQLgKnNOlFsnugTnUZEx7JVybyiUFq1AHQfe8VE31rseWcGm5/8vrIrUzvTbKO85jXTLW64orSB
3/Wo8BfmRRk/UZcgmaalxKRBcc4IbYxIKwv99Wn3X/egFzh7Axi4c30U/WVFbS31S/+lyRF2Uu4X
rJ6AKWlSM0yMAgifuy7fAmXJROqniiF7F+hbZFWOIbNGd2i5RIeRHxJHUNslFls3Ib8NtTwMLNX/
S3N4+8c1rSbwjrTqDTQ4H87EYVg9D7LSEYxYZxnL04aL2Z3hCIaQrtDfzE24PF52C+1ifXY5Jy79
8z1DyMayqZmOCorH+MyhNelv4ialghJwoDFEaalGfAmOwUFbe25afZO0oGE6LpRPDp08Y7f1CZcB
CrP14Nq/CHuyRAcfKQWXgT36fe000ZRyJzVb16U8aXL3NUxXmCt/Yq12+s7ez0e6ZlVHD//g0M6s
pR14r6J7qxI3zmIi/4uSY0rj9+IPy6bjPT3syW8bt5FhDQoGL57UMchMS0H72aXbLZnFCi/Qzw5T
p2KlAAdmGJsvaFPyTU3Y8D9hKiC8blRy/WrToGf9WGBd3R710eu/w1KapGiudC/zt137fMo5TXyv
yBIkh7XrnWrGX2CQVeJARaEv5BWwkwMI3vFuQb3rF2hxMX9LIxY4k7LUNzcT7yHu2WR8UQ8JTJiW
IK2jnn/oE/ZYTvbi9oKYGrsP23mhq0aDbaJCnBT6j0Oh7/2AhPKs7XPEIvk/1YR5KoXBdwYWX4mQ
N9g+cTykyu+uPT8sag6sXw/HSTLUAQVrRhrQRSBkFJAnJmYEtrSQKJzynyXioFloiyfz9J1YHTHW
75fa9drmuLUjtE+TDyrwVyfA0R0C1ddbf9JPenJfbMzJv4bZ8n8WsnUe81L5pEgC47dtp3yxvkx8
zx2RXbZBChSQqXO8cjbRzdiNKbfSEfupvhlKvuFmZo8jg9impHvnrYWmB4h9kRjqGYvDGWRvJM4k
OVA5k924eWnvLmZKadjev6P3T64bzsCskIsP7F8NbYS2ARSaz4AxYX3O5MfnW/o/DS/lP8BYS3nK
+7AISRw1vZtdWcXvIktQCgzNK3HNfZgc5VZxT/MBDZLbV4j57bibkiMC2R1LbKvE8HAS57SUOpdD
qiFznlXuHU0XhS2RKxSDh/chEp/Xn+bCXMNMqX9FffYFqYgFWByktPW1YS5vHhx7JhYT3O1oaOPs
uNS3neNHL1ps8DZuKaphj7gTOpq4iruAYDRf2frdABLdAVcVQth3ymxxPUu/kx1OUDW+aiTjmvrs
wVijYEXNekjRgfjO0ArPzqlikQqRzzsVfc+Yop70DSYvP208kctzODhjizbbmjhZ8JZZrwgMTKK/
0UjR7DoqDJQ0LsEGQZtRzdClAEmw5jymoO/9mPgbRv6wEcZ5TF2sRZF+/5j9FMGzZ6R3PwHc0Xvb
aeJ9R5M8kfKoByS9R4FnSmgwuRIBPAVV27K3XUOtvVAsRDTOXfvhPopnyC84GihkrtxOHdcrXv3h
LP96vQ9Tl5PCjpJbMbWSdW8OzBH3S/a79NFnpWrYZBj9xpi4O818ap/FepAxZ/uE8WvB2lwN9tLw
RawsOqfDjB1U0AfKlrpL9bJFun+bSWKEe44cdQsaurvJ6F8nBcbPwzmKzW1KpPwWhmYOvMwyV7xm
nePgiX2OJE8EE6cAeJYWsnfQ9f2VTovvZMgfTuFYlaSs77vEN/6v55Qp3OShO95xvrhloITXL8Xr
/Yqe7Pmdq6JjOsUH574mdNoY7dq89n7jgQ1KNBZflfw4Oe3FxcgIchtmxDOiqziz33FIVI6JwxDo
7cMmNB3ZJ70USWC8bquAiks4t536MIa5lJ4cy7THoMp3lzVaiG6niBLXuUwWgKcTgAf2Pdv2ecSG
fzARVT9IErAJW8AdUH8O0rAwyv7UJgh3eNg04edOJw6Q082O3Q2/VgJ8fVzXFlQQF7n63x5E3+fi
FtHFmW3psaZLoTx6DoIrDpILgpq08NMFQbWtYw5Rf80BqYbJZpT4KKKEN0AUBIolwrmYyvwoDBWl
GW0dBl0C5EH58riGn1908MNEKFh//xraKomztjbzwKmM3SYg82w5Eu2g+N+1FqzIJBOdxAaNMHp0
yk6K3lUJb3g2RgWZsyorBh7h7IP4K4y5vHEoqMPBCMNm6Ob1Tfp0wq8IMvxWOTn44N/dDinR7DBC
ztKKglEGFaDEMZLoptOJGqXcijCHabfFpoWQHvYHndv1zC01vWkat5VaoLBpm2Kxow+Xi/Fzo8G9
2+q4m/iHOzzdU/IfPwWSOmdzTh8QI+MNIwkQctIgBF2+6MDtg7VnwQLa3BmufwsPOleCeiWMQwTy
3XWZ+fVZ55XSBkVlST3XLFsNAeR+n8oMFD2TjK1V44CQHT5n33nP4yWlQ6/HPbXpDWgV4hF3o06T
kcF2WEvhkLdOqYf6tLXvo1ILI8WzWqMD6+Y4n8poBcb2VRkOPmKlX4C+8KTQh736w46VBHnCiCYk
nx6n84Av7zAoof+gPBKdxAXzd3t8JdoJXoQlgLj8Z7xLk3Fak58SHU53FseQsV39kgYti9f5UNku
sm4UCfZDl6gJ2II9xnp0ITGs8Wi/Mu0qt/iG8vZRRrSO+QSBmGys0LPNlFsVvcsKkFQCOwwpkmZc
kK6Db7+sXF6oRGwvybMxzKI27M1Bfb+/OwL7vjR+HDHqwueCGSgkKbHlS2tsrBV97w8WSfQpfmb/
IXTQ6VaXh/aFjWXqWRgJ6v11ImTQg5b+YJbK4jWNNEurBXHmVBb4qfv89mNlAmt3pETmTmYvgYhE
shfAhqP3ZlVEOLbpTX09dtQsBCo9JmC5PbUFYQ5J/yfO84oBdxj08gjyGiQKAlgc4gWso2xR66H6
pH8D8i+QKu65L2ud4unVch9yrA5ltNp0v4nNPMgSSt8mT0a99ubEdAEnuQ3CTwDmuTzyFzkfe6yC
hBy6evvMXjQ6Nxri4FjeR/0j7Mz0KRTHMtcIBmq9pzYoLfSOFDz++7Z+6q3MQMwdt0ZenN8EyCfF
XY3pDu+eq2Fyz/y99/1+DFHXV1U6Oxta+1qDEsYRPJVrU+oHl+4UpDQU7PP6RnOeNmS4MNJi8Fq9
5iazF8SniKy4lOQPSyhCOUbgmVhd6iZxXPStV65cD2kr+hGk2CsWedeQ/u+4+cL2sX/eZDOT6vCy
TRcuE4lEuj74lKdXW7R6EdQyXf5G6zXZYc/hIqGHBQdLbhAUJUXL1X55Ja56EeQBqV53VqerASaM
rQ0i/rKDB29wbcn67bVvI7woHF/fx5sobhFV556m3Z2EGKM8QhaagMj0h2684FsVJlKqYw1RyTn8
umJaF4UZAMGYmiGJ4PAHkHBaP1dOD3Ln5DHAj6jPA9pXomV5gMRcLGwri72ZQ9Nczax4jFOmOCT5
+ZbonEiOSPsmrU7XMvCUiwfQKC2HIKIzySBana8yOkXJyBFaWHmv0zZni4f9c4mJb0SI5XvmhPAG
UYSmkV7B8YhqyOSeBIWMvTOzhb3WD2gAaTBzfDHNhmOVWHUAM+lwZfHstcfkp/pQW3Efn5GrVokm
9Adr/sZfD0+sjS7FNz0HEOGxffD7Zl3c7bnGnuZxUXhmfZiT0YLuLEk/N/QMAtxtjiVCrRtUxz+A
oW32tgGu4+7AsLpPE5a1X2iVo/FFbc2xu3orGdx9ajQRQfkA5PJI//JTlJf2tYImXy41PVjYgMDJ
cY6ekX3e5sDKKre8PHjEAYjnjv/i1jFZuKeOhaO5Uldshdy3bg4VZu+30G63XoHVT4lesifb5trC
J53A23mR//Nj10f/TYjcOMMhbj2qV/1eX6cVNOGgauj4iZO5j+Wd/UedfKsa6IFCiCfCqbbePq7p
t2q7m9GkIrPHzWJmWH6/+75zdlWy7iGPY1ykGcXogYYJj0v3PeqaISD1F7wOw+4Ps/Iv2NZS59Mo
6ntXMAiCkdwkr5J9iRmj1QY/HurxLn+PsADKD8alW+d0Vmm9fD70Z9gUpPnRUOoNmcFhvGZoQedt
hjuAi2LwTlICBMg3R5x/qYO2ppIytkdbLM6y+Ny1VxoaCZOLLbxUCmUZS3dUMTUGPFub3CYvQwF+
PeztJMcuF3XbWfLZ+aOcb+m7kCfoRt2rN6K45Hxr6iArzu0B4TvdU+hVtSxZa5fd0uUmzBSkNJwL
UIGOsw0eUv422a/3LlaT86ns8JBzqN4PgX8+ULtrLfxdsf8b9ZUM3fLBIVzb9zlf0aPxrn96GV6C
C7keBNKRMSoR856uvXpYaWHYwOJpjXo4T7s92/6OVhkIUIXws0fHr9iFfGTXWwl9n6jf0TRwPvUb
5/36FpsRe5bFZ9QUZygdtbnN50hUEyfWFbhz/pRCgqhmXu4LqwJVkimVvgtWTZznBHj6iYYOH+wb
fXNJo6DT5qTYpu2NaNu8op/PXq0hoQwbn+rMOsixmsFg/ZfEvqVUgEnbBBHMAIBIagIbvsDazcOe
mDE0Hv6XpKHus7X+LsrsgopVH8Dt2LYYvDbuRWRCjtPAmC+rK76F6SUqvGGhwzOfTeV58a9FkL5i
wsIpeIogL9/z9km9nqsc/N9eiNenqqm8gM523DRec/jrtMkyEKOasYfcF8qgPegBUCsyBcHgmrI6
rQKRMNDy7m1msBfpsta0UwDz1uu4q88BlBIXv5ZnhLTtEx+I/YVQo0oc7ZQ+rEem3da92ThwS8Ze
dxjLbhO9JqQxNPkynQeNuPtrzn0SIz2BwxDymQmV+BqMe+nIGl94WWd9FzsYPGX+gfKIddBMVyH4
NmxzEONg/KRSJN+ANsKY2F5EuenXUxgZf44dYm0ObNCf5jRiDmmD+LIV4rKz3+XWhOx74k4fnzM0
8sQnep/CtOsyGJL25/fUG/ozYApofGHgzlikyUhjxGwjTRwxxfVCqr8vdgPmxYZs8JBqH81a5rb+
KhXR4aUubHWRzzeZI8BF2LYze96SbCmB7zFUiaj964tFnu8l9YWKxIoSZ0HweIrcr4osUeIoXFXD
l4n06+GymaV2LKj+dNrk+qjfzX1jifZBFXYr1gEHkevvQoavSZuOAi1zTYE4pjaAcFCFlXWVCGYF
A1USL3mbJQIEuVUfCJcR62zG9/yvMrMOJOGpd2CHYzAs+XW4YNIOK2RkP16yLeS2Gcajgn9MLvnx
ZFsCm7BlNGMSSPPHIkLAmnOq/Cu6q4HUH0C+8Psmh3yLWG0iKhZVcSTGE0zf27+kkDV+lqUI7EyQ
GeBzrKwTf8n4FQp4Bgj8gyPN3jVusmBOfljfMLtxie9c1Gp6cHZ9sUY2nNzh6j8cTf9hol2/DrRk
HnruRfijAyjjCzpUO+AhtuvPm7sGMR/LC0M2Y5joIrZAlWguo2xFXIHPMPkC7w5eU5gp72RK898X
fBH5S1CKTbxWtTphfKkdEd6pe2xwFF/8zNNf2XbAzILOzIPZ7A5/w2XkDLshJDM0vtuboQXIdwQu
b3dIrvenQBTQwELK0hAu2T+dvhCr0VOswNk0vNgCanU8CQ4sr/pPBc0esDQw5hM5OUYMrWl9m8B6
zRkMqs/eak/5kyE61OIRETDmba/+vuNCozQLYP4iAKDMf4R3EoJiIEEzzE2k9N2CsRTMvF2fdTzX
BGEa9NrLPpEGOA1k05KpSRaZwH0g/XO/4j9cK9NkxSa/CeeFjJlPRJ9cN9es/obWdns2rD3TG4ef
OoMporcrS207aOIQX/FfaTI45AAfQciWzx7hMDRLoTnQk8M+qiqDkE/Q+Ot+p1YskUKoeXrST7cg
8AYo3s9ObxAAPNW3eTn8Luq5T/av2mKFBCIbKZBDnAy7LLHkkQCBU0VwYvzjpoZ60/WJt8cI6Eg7
+YBOJAbaSmx/X3XFtEzPcgVxxE6VKGJrdMKJn8Gy9CpyfLFjNu1i4BKjiJs056+dAPbmzbtDL7FK
kmr3NuWCScACtZOUSE5ewRySk42LFZCn0cS3psS/UOT52g+/Ot1+eZFhpenYTAF6DepUpB8J4C7E
bluGt0ups9Xubng5GsTrtio/DbUG26ZjErcGUCD6lcjiLBqLJ2CaCm/4HEUM1PoG9wYUxgbkhXY/
/iBF+kmjby0RdB6F20IqZVuFlDNwstEKFf7YPratcO/hfx6qYsWtnAI6hODxc7NYsCpMf33eab9c
mpwf9/pq5w9R/zYKEoda1BVE9/3ve1zexDHL9Xt0AKAvcew/WsWIq9AQI3dbuR9zjCV0wZjzgRJn
9wQJb/MPa8f1P7bMdku1kswF5Ygr2yswnZ7OgRwrWfkdl9wQIPg52qguxEEgwAGqPfAMQF1Kdy0U
JlmbpvjgZq0e1n+kAJLpA+2U6Xap5RMemTdZ86mGBt7ftlCi+bp8/mbDZSbBsvnuafJxBckvRPvn
PBPL2qJJxfe7sXxSIPCQvQeLlAPzu34U8vNMJxxpPqCWehCiivMRICRzYzPVKh2U0kgfqyqxR6zC
LtEmvDCpANGqASEIf5++ij9D7TBljZOS9zaVn5NRncOau4y81zZKnBmviAmkPG79NYFNHVywzYgn
lKQEYOclJGOX7y7DD9uZpKp5sC4V8ZoPZKNLlH+ooeNx+D04VQBmArZC3wxwTl3XDANB3sSMgjX9
nzkzCDWZ1j3JxUUyq1AmknVGQjy5h9BsetHoetom3rICrGpPA0tV7KWryqCPF80aF3NHrKK3+DJz
NkCWdGOZqXUeq2Hk9dJHcjgxYVkM0IZRimMztMHBS2mJEYA9sD8yCPqD9cqqFKlT2AaytyQySPp3
ViV4z4VplNgj7yahiCqp7Oker08trT0RCkPBEepfAz3X+PP98icG2UVC+SRflOrHB6QGJfJcZViq
rWGPlmL8xRNvVp53r9MDvbzllqhHzqF+qBCBszt9kS41YjO7ICD7oqLbw/PHq/oSWEfGW3t2pV+h
BRIjxbv1QpgdCvmpeoVTFYhSgPOdy6aWj87OpxJ+Lnr3065YU43GGfsbUoWMgd7PEZDG1m17H1JL
P3Yx46+WTavc5nU8w6mfWTVw4O0LEeQgzMaCiL1XCoSh1lDODgexHxsnha0RGOwfbTeIVO1LPVAd
K1qHV+6dvyyk7l3jNeMiqY/Q/QFDDRwYjVCpVSwb+RYmhH52ZSRirkZ6U9ia4bJ/BRnVOWhMJwmu
dJaOcXd4OYy6qG7JExNFicJz2naIQDHarUv3ntx5XJdFrICkRh93K9V3AawmaGSZTHJjAlq6IdZT
Jo3l4OJyEM3otRk0wQR74f7CMcxVE7R11ZkB+Sfv8BRT8FW6yDGZcPfc7fyq2axI0IAGlT9lpR+e
x20vAJb9t4C3PzhOELUUhNYZeQCCiL7C+peeNAEOu/nwp2wQAdWA9cedHbzDs0opXuFdTvN2UXq9
FPmgXI5QUfmyqh9TmtuA0AgTfbXJOZlqhbHotFMS+fjzg2zPpPBqUefXAjcIwINyYohxQZyWka7/
ORBxXhVsZJFV6/NBA9IP/aKRlV/5glaFcE1678PrCZslcC07yCuC58WvDp0GwA8lT0t+ryHRTa+u
gRB6kFcrmuK3bykiiYWEieyQ0hHFxaGQJDErv26joHws9md1bf/T1bJfpubB3QT4E2DPm9RsvGnJ
LK10HOpU23lvP0CBo3kFReEE2chIY0IZfPqsLhu4+CE2tTTIsduc1L6rOU718sXtmZCYUPOBF61n
VQGz1altTTVXNPI+Xui117cW7e9raiyMRI8HHfEdsHPgu1oWA2eNlrnb5KN/fLixTwweed4uEF48
YbSH2uC88v7SyD3LSb369zfh4eeWKl1qvc2HoYcqESGL6ilO5JSlyEsqQHKZe+z8Ss27XwBgGhVp
OLtj+WXfiA0r1J3GKcVssUnbG3jKn3ln7JEAmffEmRXMQaK/K/Rjjy1lLGIAD/BIi6ssFZ3SbAQm
aGHn+1A7DQPviK42M/XKYf0iX7AlssND42AtHT81qmlzAnAeYhEw80mV5ENQy90586Xz9VZufG6H
8XzLnQEaC/COkP9hzeyQ3s17TOIIBFJ1Ql95zJZtbm/hqAjLUvs7LFUeatpKpws0PrLcv1G103Fo
7ArjZavSADxuJ0OKXQ3cAnkn/AGJnHo0b4EEMHoWzGoU0vS/NCuNHvS8SMoGuhw7FLIKNN5tRmbz
lUPqV+IGu5NFOrwsc+blUbkoX2c2wU+AHg1Y54qVCSZPKnksVrEXSQ9/XHd27xxdGd0pfXbfa0QZ
rcrnROcS1+5VIz5u7WhDRPRtLOEEU/hkh4ZIpvdRqXfPlY8EpZ0Jqh5PCZ61NKA6faQrLNWLjagN
S32htdgnAbppgpPhQ9ovMqErEuhUreAdgmLOQ8ijquspKtad8urc5dS64qFxfMqesKW2D2n8zEKB
v800ZZaU+0jLvldjI5aPch8Z6PJEKAsz79g/tgfv0wvhuFLYeKphN/gW6lhuOWGnHX8dusgF1vGW
rZIuFXgA+d8FBN5+HzJUqufAnRjFxCldIEJ/sABGmmjBQG39KI+MWQRpwRISmMrCL1sDIfh3WbNB
0FegirGxTa31qrOpmOdF8fkZtKpmBOJ2cnRJEbJeW1lHz/e4Dn4yicVerNz+r3mENWZQ5QTrHC6P
vKrv+l/HySmcfuuledSphuzWbKP/SuanzBQ7vpLS9IFDuFEcZWXPzDJW0XCxn4yay6iYA/84sdR7
/jkw/2d3oLrOqEFSZzxmCNYr8YO0THsiCIg6fvLCkR5uMG5QacED/Yfb34yq+r9Y7m9cUD4GGIBV
bh0FNhLMcSSpQwgEM01h/UtGEGysDOFkKtMVIzp1E94iy1wkHg0N53cJPODnd4sA0XyThwrNGe2g
z2ufMizSd/Pvyzb6J0RUrpw/gwMPimWh3XhZEjfHA3zJXvu/DMIa24qnMOjfEsFEgcc2NHoWaVxf
LpMQOn9jAbJT4iGF3hB/e5OhRP02jAThhlAI3SFWMaSUAc9KiR2nGKETuz1188EZ2giRxL3bpviS
1Eez8+BiUMz1LiOkCqF3jHfpz2wcgQ98cyniAP/AvWh3xLqOj50rSF3dxUqOkyzMDaZBrDYZIIZq
7RwVmj1U/SnDV/+IWCNjrU4gD2m/NsBeT5Q9mY3WY/4wxCq+IV/pgyrPkQHAlvmojDDi9bnkPnUA
ZR+w8c5nga6feRiDxaVvFcaoTYoL1qtDLXGwWOWIEWKngF10eQandC6gH/dFfaYp6o3+9INHfpXy
4RSCqmeXh8JW2+c8F3lK0lGQoeJiQwjsykejROBfeumquMva6lW6fHxi/kfuuvmZ0myvSFqZxb/G
t1zjCJW97tzBIBKYVct+iCwVWjR14kxsFGasZfISQA4+twxp1mQOJTY82g4sVZZWk69Bb+AwTOv/
wjwE5K6dFihGUWPMAKdBkdfJMaR4yu8BhOx4MvfzY4dSk5mjo1Nq4HGKreEPGu4/k8eOM49UXJTb
w2KHuX5uw98jgEOp5Cw56v5Ulaq5fYRsBU1l295mIjXS7Ef4tPssJU+d/musdH5vJfxptA0KKipa
G6sVPVRntaDXqSZfqa2Hfv2rPtZaCMo+NxsHcwfN8Xe785faILUoGkdwgO48W3FTE4fuDIaWxqZw
zld2aUiOCIR66+/03M392aCUU6Dc+OTrr/RFP5zsyQBR7UKCN8S6ueNZsxLjV60KcPly82yeI84Q
6SH0HCEt8m9+5k6xb+nHR3qiBPmgBsjHZVvy/7fHJX4Urdj8+o/ZQSP9Ex7cZHpPS/RLCQUBUInc
4tUkZJ19AKuZ2bsIbMqU7dBOiyl0UDOThZDJu3Sdq7MGCXfnG6Fdh1z5mM3Tmq8bm5qn3Fo+/efD
a79vGNJUfFVWSVCnE7XRsM3R223R4HwP+D0PIIz6X0bw2gdbmNdFzYVLMMGvK12U9uu68hX8y5Yq
DUSb4lXxE/h9a8yhcqq6F/b8jv8aSsqzuXGfQUUFVJsUTcC7dzLHaM147RZxY6tL7PAxOYMfwIzv
DGekVKhPhOkoSIRUATTf780s6+lMe1O6IZDyitfNzCFOZBv9wtO4srI5b3V0R6x04lDJV8MKzqZu
/UULeVzWc/1KZ6ndwDiLTsmD6MuHJ1+9mbi3UlP1ouWM1BkgYnYjXt9H9Yu0A8u70o/cFdI4p5Bg
FxRT49XB7vL8Ureh24CAYfK4tgZ4XFhaEo/2r+3ru06CGdzNp9SOujjbtT/Ua0i1xKz7sL2EJQ93
2lhDILhD3dOgy/A2W+3k+Q3Eu8RktWURUYBD6KD5z7u1q9PsSzU0eBCOfQgm3hfggSO7RTF2RmJa
yBGWbebH0h8TbKTLeyyof3AOEgH+DpmMBS2E/OMGeNGC885VyrrWfUxUpU3JrrDQoOCTpUU94+gX
QO3Gdggh4FimSUK/sd97FeA6ffS3lOwmybEWAHkr42v4/cU9MCSM+Z1m09bYcgpxCP8RGR2kve17
JQq0X5VqOMSHD6dz0aic7gSodhwGB9dKLewPIJGTOwKGP9S3CS8KwhFf8OoRN+hARBiiWOiNYKlX
TkrQJtHWP5xeL6kHJ2EbYJIsSPvgAISEVUo+USNVPN7NDFKE445vHN4lxrXdz2kWwA3UngEgmSLh
G5/QUBuWZQqJ47a0sltdyYK4KDjFMLc3r8QjfvntNdskNRlyIhSuTbkC/j11iMx+efbTdX+kTKA3
chQi7oHPDQ5a+zO3bp+qnVM4pKuWHYeL5vyHdT897eUMt3rAmtY49PNCky4raqwxHp6MDu1PUXTp
6AHSu+b5TsuWqHgFaGZBAp6Zb5/7yx/z66wF4DDsKRzEA6RcT1B/GW/hgXDFlq0+KboTSiou7NAE
Z79OTbLIpSPCTL3Z5B+i3P78OO01FKiqKB2fg22DgHX72mENCHhXSjNUgK44qRsTRm0rrw/ydiO6
xMdsSLEGhmXmjV4/+SG09LeGH/3yfBwT71HdBFSGuEjJJShdMfo/OAmlHi4GOdbtOCuF4JOReNXv
3UAluPXBY/AyidVIgJTBPNdvP7oQ2I7Xdm8tnPUEcZ5w9vcIoJFq+Mbu+x2QI/Db1HNCgv11eQ3e
jiETC2wZD2uqgKO3qgS9qjtZIyXTf/5ZVy0btEGu9vRmB+OwYH35HakTeGMyH6x5EFBWN6Kew2fr
5fLOo6cZkuu1CyOWit4NDScHHXkuOCtgR/w2m4VCSVvoJ9EMiAx4GGJjjhnFG684AwNPMzjZ53oV
MG7qu91v2o76EzvLeA9rJmtodwkhuz11Y9pwKImo7SC83mzhczMayi/wv77NccDTUAgHfK0gbNry
C45g0V4leCxuuIKvAPdtHlhrC6qAQvc1vmJTU11Ubda1THOEkahJF9AamdK6QDmEe4ZEkfYdrloO
7wAmo5KdV6Td7NrG1h3hO8h7Sk9LKBbPLlfPNzjizMNkvcf5ehqZkRsMTaPEB1jV6ErHNN52Brae
rn0Ez/9VdXOU+SXYowGTmYJqldnKU/gZyYJHwCZ1FtZ080dP1J+zt8rY46CJLaxe3SkbmcpKhsEI
IkcTTpOMjjyD79IRfe77JPSUDY3QV7db3TiMXUtdNc4fHJ08n/7vPBnxYEnigTXSJTHSp61Nxh5P
ONgNmUZW7LYQDXsKODJDUqX5L5xbA97T5PTyj3iaYQi6w2AGRI3Mb4x7IYr5nyx7se+aK74BNyww
jq2fXwLuhdLWvATeAZbzijzp/ZcozknPi09oLPI2nyAR9WI2MR8B//yncY7QHKjjAdNoyTv0FxQP
B93SuyAYQ4mXcHkbg+Vv8zuOAjSPDTSADnXEQ191t4E9sU0s9w928j04favWAc+JErGRVPcFhRnz
Yp8b1RPeO8GerG3qGEYxOYnTvlh1JjNVUUZdxp2WG+yFrlkfhZhjGn9UYDH2Hu9vrmFTAN24ZzSl
LCUn42F/2yWwsXBJ+9OaW5sv9BHGZs5PxCbPmHnrus4H7knjrcp+E0dvoi4nQQa8/OF+Pe++TUTb
vEp8K++Z5AXq6WxlGgpisjz5GAbwcOpQnuJXcj8T32IIfmXUPVwHXcCcqEtdqbVbEjHpuWYMUXU6
GyhJoLvzn/jnMsAOMUQmh6yN7YD8fhTooQafzslYTNiBVpo+SSx8fmISd2XrwtMpb3bvAXuTFvv5
wjPmrO/5JRrEPOucpjle1FbmDF9Ht2WdFZfPa+RrAawHWf9wLTzFtvw795/thELW0JKeIl1gbySc
g2AwISGQG25CJi/Z7wkz1pP1f19JPtKKkkfh6UV0sXbHr1HBZ/SPE/aeySBhH97brsJKmZzK9eGt
YqlC0CsfUKBY0zxfSNzO6aqfpkmV+zal+Yb/RjXHGUcWpklsB0X/J83ecl4rk2EQXEe+awIvOcMd
LzEZ0M9qif1MvGTSNzvsti4bXz1qTrLP8YSXRvaInbJK7OWWfwK/vYOrnsucWKap2zSdPrycl25D
gy0PXsA3ucQQrGie95wRtMruGGEd7LnHqY8LddBK1oT2yCMvEnlnu35qS1/tI2wiJS2WDHSGXO00
wKoqS5ZvM2QhZC7xyE8PWJJ5o+euqgnVlQ5KRYbDiR+BxaHd1MVfUpHg5o+pY18UkIAu7TZwmqkq
2O7xjkdTiO1fX+y3eL84X8kbaAp8ifDAWJqgI5NvcATfzooK+2HMtTlN72uhva0gQtdJq1FUaEPw
0KRCxwR6WybyFyHSv9X/kp1SPGlBeizZwM9BkusRKaoAN8G9ObDuMWN3kjOYc0gOmNCVHzSrNTGm
btudTrkPCl2/s0P9ZYLTUXOCxNrgiccqumjqjnGCA05Ey08QjxKO+Ygo/hWlFPSOZktcVFE/ICCX
lJj/98fnY0Re0dQqBKUv/2VfJqR2B6GRwhMx8psuul9cRh7PqB9BUsWEs4OfPJtlHMo9n+XrBryD
P/W65btBbQNtlF+/x3jalq4k12WeIXD+2Sklzs2Kznr6EtV1knzLnXVxAc2WCn5n8cBVFdMUOYEN
KfkFVaOkKB+3o9SFhieh50EGjtvxo6Eg0sG2SzNcVARp5NGeRRwJUZ2XSb1yttjOycqdyE2vF9fu
QoEdYRTiMKRpO9udaA5rA2fTYrO84Q1yJXpYcUeX46Spb2l/mEJhcbuYYmDpDkYbbxRHTB4N4/fJ
1mSeTEwFP+TnJNZ0LGUd39s+nePgnRiMRzMLcUINryKNnyEBEZdpOn/QLMV/ldAhVvSzZGvQfZNO
wzA8meQXfj0ez8T+4VAaMk5uHgRhDvL3YIPXtMRf4pjSyzxB5nym6kRk+VRLj9WavahElpVsV/FO
Na8adQOyZsQ2s2OHdXBHcT6/eUpPnkI64fgPSmFkB5hgcpCIqel5U98l2hFk9E16aT5U28HSgwdf
PMKpcaV58QctUC4y+iKutcJHlDK18fJvf6Hn4LfmCOQ7B2yU8v2VcaQ+WLr2i6gZsR9df3ZOhnIi
Mhd0ZXDhFbRDW+EVdah40u0Q59L/aEDc186Gohlwf64m6KQriZM+VIz0kaI8GjeE6sAp6uUYGE4e
Vbwxan9o/WY5cDBd+JKci0k3SI5TjaVnjF4oOMU5dEc7nRAHsQpYFT+aMsn8yRKVEYcfMZfC3Jc1
mri4dJHfAZ7FfT0zaQbctBir2jBHnWCygev3zQeCvnedji3aTswOK2xPHaxVjgdlsgEJSSg8MigW
N1Nl6R5UQ5tWI8fC3+2+6AZxBOpzIBxtXGayHi+VtnI5k+p/sUyqpGb9Q1PFi67wNNagCaeJxPIL
wme84BM5vS5ZcrM4WdeXqGY/yQstNkIsarsrYFLMSwWQ03FSoTjKE0hiCaS6rzEw61UNT9+WQvOY
ArhTKxCDoOwxfzLtaHQlKjp4DgIT2J3X/wtsToPxY2Q2LjUH9TFt7z/uTAmr6yfJsPzicwK6vqtu
toAAjVqP6eFoAVVICXeTzaS9v5SD38kcqSQgx2jm6sO1hcwoRLJAvOEXGgKsYyEQuEub7BWKerjr
0cxet9Z1infmiLLKcI/U0lhf/xaJ815Yze2m8eLxgwDV1aHr/cw7J5evnqbnuNQ+MHypQ4H62ybF
xtXjhL7/8QrmRR94NTgc9cqZVQL8BDweAm/tj4AkXZKQF7krNE5JvQU2ZmvAQX5iV9OVSu0REmvH
hTEEzhoq9sjxczr+Qs09LvSO17WmmgnzBT2YpRsdG46Ya5DI3ZT2lK3kObEpRc+UWMJuFtLzjV0s
pxsWzVT4nNGof5pSy6Pkt+Sj0R56v7hJIZno6/DRlSNnDAyM0cCaI2qFmaZ3cN2rIfZGNSjN5KR0
tnZEUiMR5YlhVn8kzeBPznk79m2RLDYMm4nGYvNigBfmImC/MZHh7hdK7K/GdT+u2D7VrTd16L8c
KVxqAgo5yYM0E6bPGlxarGvG9e04v+qOP/7zzuKxu+OMsGiuYI0VCv6s57G2JyjkWnC1rYEqrRAj
g77BrvShqdu3n/FS4ltkpOhGd3gr1rDN5xq1ewIx3xGEkaqiptVc3oQyjiM+58o3Bjpz3fk3MgAn
Aqbgryz8akEtx3dI9+cpHk+57Oo1BkpCOl2kZ0DsahIcMej4YwvSgtzTnokXgSOjkxCGYeHeuczv
YjN95t9yBsA9FOClR3lRAxWEeUK5dLSPZ2cOcAJUSCO0EMtnTZLnkPJzogr4kx46aBiG1Qqhc1QQ
4nhF77nvOrfOz173hn6xDLZvq6Igj/38xZ/8mptlTkZE0B0gxVkMCG5h2TaQl04wrT8BQfVB5XT4
vdKwM7g8nqWJNP12qzzz3KKQmeQhqitNmkgBP+3XFh/2ZFxc7sk/wRr9xJ+TbkPD3fc4QMJsRhGy
6otarVFaisu+0flDkWLyvFIgVrtY90v/7KbiUU4OU0h8PV3mTd2bupafpExOMhBtwHkGQijADw59
rn/HFPbLRn60cjT1K5as88M7MT1ewSOrTQkT6otcslEHS5y8DPWyOT4AkYn4BEuNQ31Y+OGO4iRJ
CW0Afj5OUeHj8c7Lu92gtQhwtS/NcHKc7pKk8bQ7m24ix5LiKQOL2ftCjFIrKwDRkKL68i5AYgvj
6pQFK8h7r/0LxkSrzw/9C+hjzD9QSvHl/6CpJu3tZSzRADV3g4pCakKwGZsiE0B+lsM8x+USqS6s
o3Qh03s8bmZDObtqT2mcqym9TLLG0zHnPk3VQkXOQCvMHwchHz7DugMvHzUAlUzDHlIj3hQv5c0N
fHLOksB2YoPNqHNJzldc2tT3koS/QSSx/7uX7wE8tQImAAGsWsW6612jzRutBvObPkHyAL6Ag16A
RghnIhUp1ysIrIMXpBWCiKFFCWMV2rAgMfzxF/+bLt99vLhXWnsX/0g3fRObsRGuhFjBvcbgmQ8q
FzIY5k02ZCGGVrudb32HhHzqgGUiFzUPJaSnVnrRKwY57dJ2nWVCOfCK/HHAVTLs2MQk3HmZ4E9a
13iWariDGmwp4t7QsErMRUwPKIPgb22kJb5tslG/V8DzYwrGr736g7iRe571VwHbQ+U5wvhej5kh
zUTyrUhyHNik5Up9OrEw2M88TgcoUWaNMoONuRPvX8alfGoDenWkXwnkho0rqjQBaF6dVAq9bhoM
h8KLYxS6MrE6QZ8OK8Mt6DwS+nGS8G9Knov/DkgOPDAW8ZKB9Vhoyv7YFnW1VQcrPellM84Rumqs
OD/AABB7IDieNGJndL1d7EiX7m1dvLxUa9KuNCO4zzzVcXwxpF9OWqZVF1Vh9bIJvUIzHUFmMnBz
WiziMoA8A4/fcUqgqP+wkf5LOQ845p7MN649Q8lLeOUcA77NQeLt0KCeCaj6QHPwq3ik1aJ2A8AG
4FbcsbmgFm/mp2llcRKTh+PecbQstEJ4x1Y+9WZxxlDlm4faNJ/sd09cwjwxHZBKw6WGM405+ewQ
doE1jJYFhyk7uBcqR5FkSZ+9Sc6tnrI9hW74ZGMlOk31zJO7kfAWIfh4Vn2QVPeI0zDx0+O3Na5a
L1nY4sgmN9L4hZM6D4VZB7IhCGGATFv8uaRI+sSPbGe3877VpHl14BVGahhVvHavXBiOdKIg9zqZ
CD00haQZM5hk3nDZbqPVOr1qZ7twy9zu1QO3cw+1QS3DyhCfrcnVfzoiURBcO+QualbUi2bVCGDT
2jIo4a9INBNOfh+lmqgArCMomIZgZ1rbizq9AjTMR1qbFZsSOm/XXlycvayR5nRq99XGOtSawVoO
LBp50uK3tPO2WhL5aAUtLn2Y7j3V/ho9Vrxh7JyNXSTQtlqQgwRdJqz7irl15IkbrjheG3PZW4O1
YydRnhgZc/I10FPHCLJUkPZIYtajJxm8kjVSw1NpdQOk/dN30cRKjtzGcyNuLHJ+rgLRFTQJ0i0p
bJrKw2HibLYyVGVcTFAAcyNzsJcdq7BfRopD9L8S+BO8XI6V4QOc7dfkbSpYq8bmvLgcAwQ2aKO4
cxe4dk4cI+xxCaEO9Xp8ek5wO1BU+R8XqXLz9O5ORdvVyZNH814YK5i7IxbQZiOtD58b4DY1n8Ln
7NEyX76IHGWYZAXhGJvDoxjHR2SO0wnYU3a8Qy7rZSCYGt1FMrLrIABXRox69jGiKeMUcswwHPpe
9/U8N8QRSILiqbSjNWpH7vqWrh/6U50RZUSLI6vs2rOn5EbeoRWk8DAg2HeV6f8F3Q+9boMiGVPP
J9XPFOkF9jk88jDOHyldp6y4C885WpA3yH0iAYJnI+IdoN8mF6kcoETGRRlBFTbSK8ZCS907e/Ir
+mVPr2sFv6cx3A5dGAL/HFTUj/OavO375gS2bA1Amzw3qFt0FvPnCCYpZ6ezdCPLvNppUPUH/HAO
Bo/1YQcVwd2jUiTquoqbmW3SKvIEVoJtd/GYi1ZQTIl/Es2zUb/DOlAnmW1GlYenBOnWvHt1bifx
+g==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_7_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_7_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_7_1 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_7_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_7_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_7_1;

architecture STRUCTURE of design_4_c_shift_ram_7_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_7_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
