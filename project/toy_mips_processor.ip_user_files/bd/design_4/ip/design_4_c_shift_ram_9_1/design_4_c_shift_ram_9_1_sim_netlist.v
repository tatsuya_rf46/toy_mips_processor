// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_9_1 -prefix
//               design_4_c_shift_ram_9_1_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_9_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_9_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_9_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_9_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
6gFPU+1QabTdJkiT9fyHHepTV7Dcn6Z0o1vj3Mdnj2DtGzOyYwlua/XRaaihKs54n929XyBaFVxl
qYQ5Ze4HjEr0yhhGcoHhwOYmGhowj/Xh+dssR58z9FX8txpaiNrF8XEAlgTGgLRTKPYsTMkqoHSU
Yr1w8AsU6/Bhob47l7zXYrLr+QslgHILWQVsyVKKZvxZ1V193GG6TDJCLs0QDgFR+Q4UTdBzGBi2
EqwK5a+izNY8bIxpwx5ChpcAePXrcnedET6qyGpQ3SePgE3deL55tqEy/C4ORB5Zuw+ADFxcbgvg
lYanjcilT4BPZB6NNcct9lSn5lD1bXXMtuL8Sy+X/zOaSHAZg8otarN8oRfzL2tt33a/RuhMbbTo
ViN7rDYaKoZJFxI4ibOjbhzYqrJAR3JIjGKzIHIixV27VyLt43Xw4BLBu68qEYO8QcP9/Va8XPrb
OVEB4Yte8pSHO06wiD+5mcdO5/0PXWtG1B4R5j5rDEJQhqHIFmRoVeX/vSMfgJE3CpNerbZ1WDas
AqIOFBIzcRQIA1LJbvG1KVLFmNMT/nQCQHjgAGdhw1QLdwvRrCl104pOd807YgSU2Tq8j5KREHw4
Sd9Ei6baTRlvSYkrQvObY7USlqFjyY1APX854snWRFjSYKDvUDloENnCa67F7QKJ2YIGkSDjdzGN
K+MFQ4V4Dx8YMcubHifDEs7MKuSbAIK0TWGt4peI3p3sexoXEE0mKt1mcDpfbhUfQf6hCbOeDBBd
PAcQDx9uaR8ymOgEj17G2ytTtOA+mG+/FFoI/fBRww3JXw0E/k2CDHZA5lJWbU7rRWEFpF5LXc4s
a0fL/Y5Baa7Mj2zLDb/M+hP4Z8HzzYg+SwaQCIDsQWBecrut0722CtI/3uAHsO77vrfpuA3M89KN
KisKD2paxgtRDszwVC6q/Y5+xoOiu6NgFlgxk5Z7vm0ylwNgL9xV/CeRONG2h2/audO+mIaUagRC
6y8fpKjzMVCg1LVkuJEN6Ux4IhG4aBeJy5an4EVCr1Edy/ldUOR/oSV8CC2CsTyPJ6mUgfVCYMIk
FqXp/aiSGMGfS0PQJJ5rSWrafqxUAy44lNbbClkWpFDUbYQrFymupYSouFxCwSkTVXj7pcVq33yT
mMbEUk4cO0RuVVuQElDM8Yd1ZnQuWBf8wFSPAFAy70g8RkoM2EqVSESKmBtFmEvl5XDzR/vbz+jO
+n3I1qxmUBGDhB4fgD7ZKsKY4dMMD8TKdflrRYdqGjm8MdNkRjtYhblTBkWWbO1GcoJ1eMlhSaTb
F8QVU4LVuRoMzVl3CGkV+WgIk4ssW5IyyNnX8k+yEuENS4jr+TasFkylR0B2WBKjohrecM7Fz+jt
HYZqOpLvJUjONJrpqwEpDU2jDROQHyU3/fRvBTFmRx+54k1720v49Ndf2NG+Jk8dyEwzgL2pKbcl
/LpA69707GAmlv1AkSKt9aVPK5McATeO3tvIsWPFdCOIoNBuYYgvUuOhFBqx4hxbIapIul9CD2eK
GvZlvJIzXFfuCabsR/0bny39kbGquMZvgyGhyoMLFFf81g0CbDcJWrECgYhEvT8U4E+sOTbFyW3C
LaaqXfPh4OyGuVhMU2vE23miVgAcYLzQKgJCya5Ha6/yv9bPxKrtAzs55lzN5qlXFrcRPAthbBhl
amC8KmnwjpBgNFVvsGazGQucExXo4BGm6GgKBGApLOhAdpCnOa8f1aJXkcYIVVPXL+Zk9YsrCHbI
nfP4K8dfZONAbw3KU7uvW9amHMmJ4sOQ4ld+WboWsAx/tDjXw19B1FjR6x+zRIQboomPuyYatDOp
JoHKQ4Xirh/983A/MtIG4jKw0QGckkZWBr9JVXicoMJweywMzltgHOAFPvZRP2Q9YhQ5OikOX8Dc
0ihxGVSuWRcc2yVDK82AUxkX1AZyxgpvQ3LWl2v9jSJ0shxVDojLW1SO/rVijO5dNegYbJNwZYHp
4OXFr/RdXX0+QWBguP09OeF0FSGeVP1OsOICvnmnimULC1J22zOGDD8rfo3lgN593qiZEAAnRLx4
UkOIQxg0fDq3Nwm9I0pIYIi0zV9tR2fmWJZTG1KfYQPXRlzfQw+WmTNQ4BUs7c69rydO8ZcVPBik
5lV9uaNkeDrPwtWj+8TFYgyA53Px5I4jtm+wdUpe/+eVo2i0IlUG7d7hcVZWF5RJq4lsdy5ordnp
iSXwLj6uBrUZ+WTqp7KzHKOMtc88uOLn+ih4lIMXissUK2/4bwmjXraAt2PHj/qTMufrG+ZVYD/g
TYXOkzG7OXtZxuSEv0OAT9JgjRxRKhAsFRM2S14/zjEuB992AN5E1KkH/H/1+egTb1xuBMV83wWL
eZThhWPBRdqP1v1HN/Ate79NJN5lEpB0OMOf21Kg7lxr29MqpQPh6ovFRshizjKZsyyVFhYxwNOf
0v7NOKWJ5418tvD3ogOhdXmvpWy0WDpdLb5Tq9kz2GtNVlb2l6mkNMA58q4JuPi05M8UxWL1X5Yx
Jrks6w2iD2dlmBo0dWcqDnuQBcw0JyPDlV5dCoL4IjT8gH7b5nzqrR0lihBnlhUxFYH7KR/0ho1f
ajwE7iTtzYaFi0pNsSqjgzAyGU3HHzMub+NEUDqVEoKd8XrlV/5uOf1pxmDNfwyJ2yvGFLSmdtCU
JyG9XHJ8W5/Ts32NjRmRAOK1BR0ykY5Q8EOWudqOV7XvsQPPWKauojXRP8dPqTEP0D0erIgXyFFl
PAZ3G0YutcF1PQkriTZiPvdxKT/OZOJ4LYC5Czca9bdIhp0RCSDWBvC6fhwsN0neZMFc0HkZWxSa
b688th+ePOGFqfhVkxJuYAnDbX+UCtXEttljYDuAouZVjJE23iXwDm/igcNIjG30fEMw32LAqOd9
yuWa5dKKwd0rFCLGZ33HbiR8lEpbhDJlF5f06W6nfqqddSCbI+eX9EhFb5G+1LX7jx3rnk0YDgtw
vNu5QT2UBe7nFFDa0fp7nyChjYrFWlUiaHpD+0OAMeXlTWJQ7fHR/at4V6jUEEZebEoYkeuj+Eyv
BRQbWo3dOARzYkwl6SwNGiFkbeMIMvHVGnY5IMPA4FHoPAP+xNItBYpWJ5uqf/wOyjaBLJxugbi9
HFjv7TX3GlwCrr6V036HsxYf1Ype4TKlJZfK9rlDJSRBTbmABgh2ZHJIR9qRmqW+EZg9VBSChw+Q
Am8KcwEG67V6jPVWZDSCZOr1tnSA/JvzYGNxvw8xOLY1/9On1+icuG9/MWd+BLNPsMc0uiz1E36e
wvk0KyWFLPleNAvA8j96+7k/+fng2vGvTCt3CLpL9gcx8qnPeZLVLsxi8BI8xZ8qObRk+wjf07GI
TogNrr/+8FAbZrez/UIImE+ptmko4IkUMc0LW6OzRHdDAaFMPFHYzBQjrPsijn8UQjwPfnO9QJ64
kG01U9j914Zq4109FX3Ct9SC6DdjvpLsdE9FvDiz34J9N6JffVCs2wRBQa5X8CWAinhkXIQ49WlC
KoFFApgXNXD1ohlxmt2e5pta0sKcrMTcqbDaczJ2QAcRgRfGGskG9qEo6z+AIpCQcy5qipEaAcuO
Ra4v6hfyB9f6mpd6mlYRCqTUiTJsPM7y6UQWWhWOKBz32DZTyrTQC2hC0+U1L1bzOlAp873x4PjM
t4PFX2QGLfAw7zBoOYi5Ez/0QO+K9SLRccWuwL+7uJn3GsAGlAPyqiHoJYXmvKETqmgfK9PyDb7i
Owqn+wto++G4hABWfT0cJHcsqWFdxWXelACC6Wazc0W8BN+/QRi6UBmIZnPzXy65j/Nk7tF0wAoc
L2pD65Unb+QwK3XfvKJwdVrgdbeNZFWz65B4q57h3zyycb1L1tf4kipl+GJCP0sCAjr4Xy7XqNdn
Zyq48ijtF5UFzPfE20r3EH15TFNzpW9FgZRRo5FEPzwjFEKPB4z/Xhbci9OTlpT9A8EzSRUcyYVl
uiekJQXJXIu8vY7IzMqDh5uo/cokUKJaEY7ZgrOPu9j+2+7itfqgbvk7yB5deN4twbyVL90NNHL8
McLfBuEKzQ2TE+hyiLT6ObkSeIofSuVvyS2Qt/n0Gu8rrF96WtZU5hr7YND2RIhvq9/G2FjjGRHe
wvdMRFIalkyYF0y1lIHx96Bre7XnHb7k4MhXwpwiVdUdPt20jvg9bv8TZqcn2FXJ+VwiBeN5PEOh
B+lDEaKBIX4chTRL7VQbQtE5P5jxo3F+Wgh3+QeOZWNannDU4XzRL4FFOMeyWdmQd2s8eOIH2+QT
8GHCksvJBJtcGcLayT4MFk7HuG4lRw+tVQzEiCzXsH+e3Wb1mTytmAbQuch7brD2t00GQgZBHGKT
TaE7kOSe4GBmrjAsh3ZysBqNqdOQncrQAGuSbuquauPlL7EtVZBJ1S/0TfyCXGpBhkW9p0erSHTn
RUy1SODz/R0n6blWSnlVk2OMb1dYOPoiKdc0HwEKpzaCuM3W6ts80vMkGgxFeViG4ihCppmJIFvX
uJFNjpQUWYWWFn9b9l2Tp9sZlDyTCXMdJvlEMnv2UWeCKFNCv8v1LoINWK9PudFs9yEqrmD2imdE
qSIObTNZ7vnw7ox4pEy7NZJxTYB1thdc08lPD0eMYxi9OzAV3vrRYEz3ssmpAFrcw8oZNWL8eVej
k/zlG3t50c4lw/pfp/el1FFxtpMOIDAEnPirfzSacoFletj+OgTqXBzvGCAmSOKu57aNe6jCgHLQ
40o9gm5LiKttomw35IxPsKZW/HD1a9aXJDTUzFc7J+Q3+GryGFWaOVkfh9qAC/HXF7iT93YgHH1J
OWfcQBfhV+1v1cbbntrQq4rb/Zorwb4+V4nt6/yf2DhS63psRE0NJuUW9jA5msRs5mb1Opvly2ci
Ubfmr6fPwmUYa0Npqqb3FFchKR7e53eH81N7GmBU/Ja3ZvTNUv5OTF5LEbOS6ycv3m3FD40v+rzP
jD5cZrMqW13JFT2JgI18GVL9aUPzRVhmulQ9pp8M6Mlhr6QlivNuo+vNVl0Yb5xzlnJCISMRDdWK
5CDn96b9AuoIhRRTujc25KtlPixYkHYuxHg/S00TMtL3c6ba7Y6dqcpSiYD92p+7IQoF+r+vtQiL
bgDe2LBjsiR/SHi4ws6g0/odR9Loo0LlRtGUtvbvMJmFRoi3lHxkCIYb5lYOQKWUTNig5FwVKBPw
LBKKC9VoUpY79DwauASpufupuv9IC/Wex6p6ZAQHwwjkm40Q2As5fMDPHOKifmeSPQUwHnMz871b
3171gc2ZwXk+TWONO1RXKG9zhMs2eo/illcZZFNrsX+qYiVedO9FF1FAgSCk+XAC2hi9BvyCsjiO
4njWS92T1iegthdm/u95llYFTp+nDG9p5ShG0GIyIJqA86Fc6TI0QECuXsd9yAd8fIJa8oX8z7BR
uEDSKutwM+2nwATrMQb9Vcvi6v/N9/hyA1dome0UKoDCyO7vp927vwc6MDp7N6uDmvmk/aljkynC
Jb0pE4UNNUg7hHqYgVaCE9+OGcKd1ZIJKsm+3kxPrs5Fk05P/CCMeeisyURQrZkOCSvEUSEe+tgj
Q0E8Pdk7tHgevxmeoTJOpI/R4CAusmhSLI1lMb6kRJMC8oJ9lY57yEnIzusn1TDJ0eFPCnw73OcX
Sx4YGGlltB0FHsZS67qsfbyWlUB/3nB5CczBVmTbvCXumxMzym2m64XmzCc+5+g7T3otMxWRe4UC
k1HPJUacUsSk9sUKYLydMO3dYSVS64tEJ2UsRk37SKUb+aWTNk0w5zi8f61SiIBDPJBIu4TzTtQc
ksMDXIgndBM562U7DG7Mc+AVbon9AAmCXkpmwkiKOY2gkddZTp3MI7Ag4JIdmuT/ZM7mCZpkFQBZ
4LFQblKwcj5pnMbHdrmtCiEL3PoPIxCav+Qb7Y8T1VKwcuCC47cITmvF5i+R9/75g0I7uzKSsSn+
tgl1yHXhG/ybPmZzs2P02tXj0/oagvMTLWlyE3ruIRo0xg71+tq0pKqEnkeW2hvtMHr3bteknU0e
abjkdD5mE257OztE+HXDh6BeH4oqIe/bY1pqJWCtZdTIVFGhzyNDnP4war8ghR1i1bikjd+WF/RG
xPWdF95/7XxNT0J6ZDm0thm28JiqjYqzGh2MNOxIbInW5LVijX+4tqqtmTDyWedO5seslSzElBMJ
L3d7jSZfnMbCgGNJZCnqvr5gqiexgM9ebgaXG50XxvGaJVfvkJaBZAIuFj6ajejt3GMSYAVcR8EL
iAhz0FgSIVVVfkEmDo7vWVCITqlcwQMfREpsla0A+8a69ehY8ZADzeH6yexgaI60IoVB7+FpOJdE
45JD8F6x3YlxeQEjx4UwFsKbyQPUNFlwK+y6/g9LFG3azqb64gdfogMVWS9ecBla6IcDunooY69d
kD0vnZFqkOeuNxfnSYWVXB9PjzKM3Q7ei06F62mkbXB2kUi6+BaGToqR8S8FHaW/FY3yVkH5e3ws
OXiRyPP4xlLPG1I+AgeX8U1mZtewK8A3UQ+pskNolPJdYEIDP6svMNejPVwhkJsR3ubtjAY0BuQ2
lF9HCXZEI7v9e4lOcf0ze9pIJKAuePVIpfMfXGyL7UVVDXRa6O9029C/lhKANOr3h8UGB4ZW8Vw3
8ea/LBiqIyXr/O7+I8+jx5WG/vbppREHpV/vl/SXR0bSNJVLYGy28cUKCLgQ1cJfwiit6AovBtOS
W+cP/deDu/WNb6s3RMW358o+dcXbi6C0u7fNVxm3+zGtZ/qT2hkwtUnxJy+pG8lGadotv26KmQOz
NlqtSBqvneXvpYhx+HEMdfr3fUvAH+Pk+WB7YKcT41HaWy29QDG8sK4ATie7rY0WjGCGFAduLmRK
p6EWfE+jq25P7O6O9dbLRVHypnwLyt6wXMY5XnahyeElwTNwryvM+MunTrzRPRRbKTg87Ei7wqgE
FelqwRp99lJU+YZ4sZB3AfbnU//nTZioQ7AvOtfWFw/TMGF35zOfOTdB1aGyyEnBiipmgCEm2Brv
Lu7hDN/7A/jMFoPTE1/C3PK5NvORKk78qtJwT4Bn1Mq0/0+czMPKED07a+zwW5Z0oFgLqSU0tWe9
40Sn8n5uiA9TWSLH8eXJokcFzlyT8H2zyrtfwPrmrgyapNT0EeRPB4VoBsC2KE35qniVZqA82fbK
pbfL3tQXUlSjPiOekhCwp0aaNwQRxZxgLrgtbmwZkJQGQv7HdNxvdW0CFh4JViIwiV3u/KeUCmKc
HVyVcLx+Jh5MGmkI9rCCNGiqN4Q8Xw9qB25tFiAbFYOIIuzbU0Hh577MI1sKWEXFsOV9bT7gscWv
4K8jsQGah6apmF8QufSWo5jPi82XGx5sDeI5525QJicI2bqhuHt4H5LVNLT3Mj4mpgswZW8rCNa0
8hEV/wNyyQI8QHDpjwjWtAzoQNwcecqHx432TbNXgnXZqqL755mFVWb+sCAo6e7NQssqcY8nIKuf
O6fEMaRv1moj05deyMnNeghFmfV6K0mcyMH3reA0wDILLFOCQLnC27GHNgcII9oN/HlCYG7E1//q
R8uW30D3M2Y9NejxVp3vF86LVxCkNeSDH4Al7ENECiDi7exBK7Eu14OFtn1N7Voasjrv7h9H6r3f
ejllc41UPb+WhM7EObM6ydiWB5VfzEbwiA4ec0qsJes78QgnNVEQYP8Q1/hI9S9rK60LXiyzi7Yx
TEXRV9VgR4hwQOkIv4vGC8gkiuls54Kx6U+zqwfwX4ceNPxPNqMkDzcDVL3aMkraHXbOMGxYus4d
EsUNZPNPTVZs3pJX0mI4F5p3wd5/IBplHzMRG7wlzrbBJ1hzRGEoi0jETi635I2rZuTHgDSnYW4I
Y2R9h/bwX+um+1F+uosQt4V8zAHsYWeGo+eDjDpPBqbdge3vh9d9QElXac2+OKflJ+ita6iwPAzo
IWioLGNzG7iN6tOo9WhKEgIuvIVWqHUTaXGFcpTJ/NcgOEL0Q6V2G6GRb5qaOYm7vQj6STgWcTeY
C5Hv6xko3lUAaoqVigwlN7/NLy/+nB1nxynYoxEcYu/WezmQD5ipUjirJ6IA/HrDnbP3YAyQa6G4
OQXtw/LBqsJSdxwAvLs76EKbUoZe6whBNdwvk66mqE7E1H7BhHMq5wa44Td+YNU3ImyryjCp77Iv
vPP/vEmjlPW1ExqVipnQJuplysCogy93Yh1q4zsj8hPHNuHnmTqB7xV5yZtna+8jp4S9b+QHQJHA
lU+snffCfXK+TJiz9ksbeqOU6i3nKd39xFPJuUjtOBfuk32VkJOIxDEd1reboI+zEmfFnuGkoyWg
jW1ztPsCXupAlT4RphywykwG3+Eb/yovKVCul5L/xtDI+dQk6xpXw7MnnMt8IDMNTk2r+O+eM19C
8vG2G6eyMTjYt4uSmuO/U1qBZYPdJjkDAR5iUMqn3Qj98zWnZ3h8N/lJ1o5Q6WZBvgBg+90wQpyD
MZ1EJthUBaZQCjs/3xhg/Fe1Fd4qwt+Gl3CsCeOq98jfpmuUgPDDGFyrHR8kZ43CWnnPr11f+hNI
frIef8uHzMFvvwF6DGrLHmlv5UPKWhnMX0HHNBqL8OC0OosNdwfUqsWG1x8Xf9VJ3nlpTuc9NUIH
aOuDUMZ0GiCa/7eZYXWclkc5jBpDvfnbrjDFtrxTYPufKr3PAp37kEYxxQeccdrwrB1ibxialYVv
eyzVRznUxhUH8gAWtui+iFVcAnrQ9ELjLtL13CFukCFLASza0hoVUq33tMMq/GDTFkPWeGdrW3jJ
mZlgoKF/ug6Y1RlhPJe3mLshUuRv779LrRyahwuh7k/seEmrwWn1mAiYJq5Uxoc8t6O1Ztz86Yy0
sdkSmimdFIqD/RT5ViM/mnVVxxODJZA8CQeAlm+Qwt9Apx/J/Vc7NI33NSGxIVgGC6+4yWDDzrr+
DuQpbT5SfKJkHKjsR1uQXkRXhXE1lgpvxIEQrDuT2BmZijKmsXMu40vfjBB2PTNreHDuOr+0vlgV
v0dZis6CPEr7MkK+9mMA8lK/DLmjt28GwZ0nqJj0t369EndkresxAd3CknV3e/gTFEVnlsHMZiTs
OSltlTg2ZAg7cG4aBcDWSUOZeeCLE6h2NXwLjMXu/ba5fFCQMYhGla5VNi7b0DGSb+mvMXZT9PKc
be+AbHDD1xU93+Eh3kCwa30eZKJJq2rFPbr7vC5IYpcHhIK4BptSxHXoYf2+VpbRki8kyCg+hoC5
mTrxqHujclbqhmaWP8gtlmL/GI3cSMqGKiLOyeyvXj/HYo58+kQnV9xC4MvOLilqAFm9avF1pyz7
fyZX8wAcdnt3WNO4sbZGtytR2Kymk395BV+85tM+Ek6i/UFL69eygAB43XZ87EZNOoW+u8jvDJUj
ot+YwyAykI7j6EoFx82wLkWrCh71czudJQIHjySu+X1KZDDJcUU8cIUWy7wQAPN+0maUa1C50FcI
SMSlhRa1uDbWxFEBFP/SdGhRmgF6jWn0UlUptxfmidK8lx6ED9Cgs97tZn4EPKvJddehMqfJoHVc
iS7BUg6RCcGhufiyd1HNN9e6VCikntrTStG0Pb8LmYAJg0YnIMWFaXzqOLJaXKISsPnd48NqP4aP
6MsjRrog+Kk0rgoeWXU/5yDL3BKoQRD/Jy8MQ6KkepJ8jgAXJ3122GRhtaVomThuSeck/CX1pRlm
L1t97szwtyX7ScxwqYh5ulki5zooX39wQAWpJ6NpZoWGIOPqQzY5LBAUkCUxL0W2wybNZSxb2uWr
m0t5wx++spwvCAY9MzRPfcKU9HX5v931WF+cdW8FL7W8Fxloip3xPFJNxeGEsMLhvFGPRvRYGVnP
6FhRdU4q7NRdoGEDsn6hV657NzxTRN43yJMQ2lQ86LDFsdyvZ+ZYm9U8HK9tevVRPoFEtoa+H3Pc
Og0Wfgr3cTKDQcFWAoUCldh/2lMCLRxBSepIOqvbVnnWxhKbrwF8e0H9wC6C5rXcr+e0cKiJmYLn
EX/XN0B/x15fO9tJp9nFaz89KcNuczC8jZsm3KoMcMdaKQmypeufUGyeKCqGIIR8K7BspRWf7It/
qu6bRaPA5VBIsHse7Tofd6uMQuDVWkYAxCOfHhaGJqRTI0bc2yNnAv8v76flsgAVSa5i83X5aGFP
xZdsajX8o1GbMPUeML1KuX04+jvFRguTAzRMNIzyKiacgeBJKdgYKMB+mOuHPWOgE+Pt7JMDl1L5
nXahKl/fXAaMx9NtIq3z3KTe8YBktc4TfpX3yALpFu4RdeJ7AtFyrEEWAxA/DJEC7off8vAlZeKN
dPwUqWtiSQMZ50krO4knKwT6HshtmWGM77eJjtr8+6o1eEyS/hAjbDZfLuYHMxpzK8r3mVyN/QUX
1F3gHXxHlGsw7wEgradWZUgKtSnunZSI+nywabOxwaAtyBwMr5C2ME0/wMbpZ7UORNPVEVCbxcmw
+fyWjpipMTTjPfuZMWz0jNe09R8qYAWYXbkpx6tTEBUOGPU1Ncl65ZrdncHDhPBhXT/8vNPALREE
3bf3OmbOwhWsufpbryMHPApSodNhR/HGB9qKplLcxmGxPXhsMMyhbY9jDm7w2+g8nTi5P1GNZ5is
VbqrzqCjg9nSTiKqzs086ipdOc1YEMMkhI/OSnP7+e7IGYHEcHyFY0H7d+5KW3Q6h1cOytb+02tE
1x8xhX2QtbNFa9d0uvzNUEKTyOeJYZaVn6UDFZ7BuoP8HtDH/VcxXBphlbyrLiF+l6CHa/bSE2yz
IAdlUmh5KF3w7dw5GIONwrw18ILqJZd3y+vIO5K3ufA1Vfh0ljjIhh72J2bFFxQXGitNobTT/Y3Z
uDMcHE116hwYoMJeYQkfqDaeM4B5Ko74ZKNGPfD2cma8VXanfZZYHS6Bvhnmxt00tGD2ihFWoEL7
FhrsInkAGz+NSfajQzq/LOCYbzlzQC5jVvfFVlTaRI+qVtQYo9VGRePQkwI2bn3a83LKyajQogOl
igDm2mbgtmvqFqsTLUxLDG0wJ9+LEsBsAv58AiuoTCiWWumX0qP5hFlOnlookw9oO0tiotR5VmyA
P5oxwfY9nlzxcobbSDJ0MOofC7XKpslXMCJOh97VkSbfrC5w86houxgqhr++VhV5deCrqEW83Rwf
vdjaMp9DXsqahqOzIwNxC+MkdZsbDmYmUhEsHqGshcGklN+XzNbJKOo06MEcPGvs49r48gApFRmR
nXKaxvyL6N2U0rbYY2y5wkMWUMBFaeDKyqjwlER88cttXA4cRhVPLwDISgtxy/dF8cZ6dB+4XVir
LLndaChz25Z0roWphlkU1LY4cjbdjO63ONaZguBJK3yVSLixixTqqz5Z2VShBb0H9oEWrohcS/2v
44C8hQ4bgH8XaaKt3dppmNER7z6H8rx9NAV7JBzAYvkOhkKjQtdOoEifpjthlwGRE7aOW5+Xafw8
tpmc2gFFpfjhTUrhM9C22KSbAMe46nJTire4FiEePy/wtcjciYkH+wNHunSRbXBsp6jDfaUHXyQK
65edAS4tfC+6Wjp8iKTQ3F4QoE1iT0zXCwttbf4QYCloeooV/tuEb8p315HJo7Y630viwCrRqRxf
TL8M4gDcJPvEvmz2pH2NVo1M/9yeO51swDbvEw76XxJ20cY/2/Oi/gX4bHKTAwHPU3W0H6UJ1Bzi
a7BiZ3+MRp0q/FgSDuu3VKpAgg5wFkCTlmLvqARcCwu5vF6/VwV61sbDEyzp+LCIo0vgENTHiseR
qSnN1ufu3VTvaAbYZuc2OPXxywulIsEX4PSafKWClYbTjBOHI2gqDvLUycxVUQvplJ4TiP79hOJ0
qEjvwTR3BD1FT7qtiYjy6dr1ObVcXECe/I/D1nyH0TDUkA9Q6S5Z3vQTyieEkkkylru8/dY6kejr
DJqIymRifD/ucBqZzT/BhmodsFLx+T1+oSR4iAirAA9Gs206bEe/XflW/GvJb+b8HV/4JPRNQa3X
slRomZ8rpfHy1DpAt+IpBGDR+lC/lyPP317fVa8J6E/lcklEMWwQNuLva9qjYOHeWGiGOxP309p2
DSENhEuOmNDdkT4xQcSywvYWcPN3cwh8W39gUokOEiVOPHP2STQclOigcZ8zeIET640ZDFckzCAY
h3jgl3wHJ1q+qWk9EyqwFJU9hU0fQdhmdJrDcboII0ifZTue+gSh2SmS3I+WVd81lw9g6uib02Zq
CT3NMiFA0A54i7ZrOFDvE8izUFqGmWPcDrXIPf4TzpeI3HdwP4LzTK2KX5ZNgCauHRyvH8cdFVl9
WSy5hulM1ASId/PZxiwcIXnmo1wFs5lTZE1ZFGo4TUIbM2k4tODq2Y62H45GTTo9jBb+m/Sh/2jb
fTovlj29p+baiWzYEZ5VkzqG24Nnm0KmwGE/3bY2G8wt4NUq2h7uAzUrEsVBZKBI92dg9QQlH5Gc
mx16NYPuPUidtN73yTUoUgMe+5sp5Hk3GK9o5EAphpZfVA0c51jbHPWxa2AAF+vv/i02Pa8WrTlx
gmOix23WIIm8tKI5YBtAixJe6b8SI1bbRfU74HpLyFkPT3RV6vnvfhals84UaO+fjwMqfTySlx9y
ZXvAYSMIqRMy4GcIzHM+svRQavC2vFLTQ3Qvyk5wO59o0O0UQihZvrC+sLquJQp0go4sXNneohKN
k/3quKKV6/sstHDAg+APjapp+w8SS4/dC6RIlfS2XhGZCoy/RF3tYhqCX4BDfLySX9nd7OZkZJvr
PMhdLlNz/DL/SzuoVwa6yxc=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
