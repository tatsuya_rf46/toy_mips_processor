-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:30:00 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_comparer_0_1/design_4_comparer_0_1_sim_netlist.vhdl
-- Design      : design_4_comparer_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_comparer_0_1_comparer is
  port (
    equality : out STD_LOGIC;
    srcA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    srcB : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_4_comparer_0_1_comparer : entity is "comparer";
end design_4_comparer_0_1_comparer;

architecture STRUCTURE of design_4_comparer_0_1_comparer is
  signal equality_INST_0_i_10_n_0 : STD_LOGIC;
  signal equality_INST_0_i_11_n_0 : STD_LOGIC;
  signal equality_INST_0_i_12_n_0 : STD_LOGIC;
  signal equality_INST_0_i_13_n_0 : STD_LOGIC;
  signal equality_INST_0_i_1_n_0 : STD_LOGIC;
  signal equality_INST_0_i_1_n_1 : STD_LOGIC;
  signal equality_INST_0_i_1_n_2 : STD_LOGIC;
  signal equality_INST_0_i_1_n_3 : STD_LOGIC;
  signal equality_INST_0_i_2_n_0 : STD_LOGIC;
  signal equality_INST_0_i_3_n_0 : STD_LOGIC;
  signal equality_INST_0_i_4_n_0 : STD_LOGIC;
  signal equality_INST_0_i_5_n_0 : STD_LOGIC;
  signal equality_INST_0_i_5_n_1 : STD_LOGIC;
  signal equality_INST_0_i_5_n_2 : STD_LOGIC;
  signal equality_INST_0_i_5_n_3 : STD_LOGIC;
  signal equality_INST_0_i_6_n_0 : STD_LOGIC;
  signal equality_INST_0_i_7_n_0 : STD_LOGIC;
  signal equality_INST_0_i_8_n_0 : STD_LOGIC;
  signal equality_INST_0_i_9_n_0 : STD_LOGIC;
  signal equality_INST_0_n_2 : STD_LOGIC;
  signal equality_INST_0_n_3 : STD_LOGIC;
  signal NLW_equality_INST_0_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_equality_INST_0_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_equality_INST_0_i_1_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_equality_INST_0_i_5_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
equality_INST_0: unisim.vcomponents.CARRY4
     port map (
      CI => equality_INST_0_i_1_n_0,
      CO(3) => NLW_equality_INST_0_CO_UNCONNECTED(3),
      CO(2) => equality,
      CO(1) => equality_INST_0_n_2,
      CO(0) => equality_INST_0_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_equality_INST_0_O_UNCONNECTED(3 downto 0),
      S(3) => '0',
      S(2) => equality_INST_0_i_2_n_0,
      S(1) => equality_INST_0_i_3_n_0,
      S(0) => equality_INST_0_i_4_n_0
    );
equality_INST_0_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => equality_INST_0_i_5_n_0,
      CO(3) => equality_INST_0_i_1_n_0,
      CO(2) => equality_INST_0_i_1_n_1,
      CO(1) => equality_INST_0_i_1_n_2,
      CO(0) => equality_INST_0_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_equality_INST_0_i_1_O_UNCONNECTED(3 downto 0),
      S(3) => equality_INST_0_i_6_n_0,
      S(2) => equality_INST_0_i_7_n_0,
      S(1) => equality_INST_0_i_8_n_0,
      S(0) => equality_INST_0_i_9_n_0
    );
equality_INST_0_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(9),
      I1 => srcB(9),
      I2 => srcB(11),
      I3 => srcA(11),
      I4 => srcB(10),
      I5 => srcA(10),
      O => equality_INST_0_i_10_n_0
    );
equality_INST_0_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(6),
      I1 => srcB(6),
      I2 => srcB(8),
      I3 => srcA(8),
      I4 => srcB(7),
      I5 => srcA(7),
      O => equality_INST_0_i_11_n_0
    );
equality_INST_0_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(3),
      I1 => srcB(3),
      I2 => srcB(5),
      I3 => srcA(5),
      I4 => srcB(4),
      I5 => srcA(4),
      O => equality_INST_0_i_12_n_0
    );
equality_INST_0_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(0),
      I1 => srcB(0),
      I2 => srcB(2),
      I3 => srcA(2),
      I4 => srcB(1),
      I5 => srcA(1),
      O => equality_INST_0_i_13_n_0
    );
equality_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => srcA(30),
      I1 => srcB(30),
      I2 => srcA(31),
      I3 => srcB(31),
      O => equality_INST_0_i_2_n_0
    );
equality_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(27),
      I1 => srcB(27),
      I2 => srcB(29),
      I3 => srcA(29),
      I4 => srcB(28),
      I5 => srcA(28),
      O => equality_INST_0_i_3_n_0
    );
equality_INST_0_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(24),
      I1 => srcB(24),
      I2 => srcB(26),
      I3 => srcA(26),
      I4 => srcB(25),
      I5 => srcA(25),
      O => equality_INST_0_i_4_n_0
    );
equality_INST_0_i_5: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => equality_INST_0_i_5_n_0,
      CO(2) => equality_INST_0_i_5_n_1,
      CO(1) => equality_INST_0_i_5_n_2,
      CO(0) => equality_INST_0_i_5_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_equality_INST_0_i_5_O_UNCONNECTED(3 downto 0),
      S(3) => equality_INST_0_i_10_n_0,
      S(2) => equality_INST_0_i_11_n_0,
      S(1) => equality_INST_0_i_12_n_0,
      S(0) => equality_INST_0_i_13_n_0
    );
equality_INST_0_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(21),
      I1 => srcB(21),
      I2 => srcB(23),
      I3 => srcA(23),
      I4 => srcB(22),
      I5 => srcA(22),
      O => equality_INST_0_i_6_n_0
    );
equality_INST_0_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(18),
      I1 => srcB(18),
      I2 => srcB(20),
      I3 => srcA(20),
      I4 => srcB(19),
      I5 => srcA(19),
      O => equality_INST_0_i_7_n_0
    );
equality_INST_0_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(15),
      I1 => srcB(15),
      I2 => srcB(17),
      I3 => srcA(17),
      I4 => srcB(16),
      I5 => srcA(16),
      O => equality_INST_0_i_8_n_0
    );
equality_INST_0_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => srcA(12),
      I1 => srcB(12),
      I2 => srcB(14),
      I3 => srcA(14),
      I4 => srcB(13),
      I5 => srcA(13),
      O => equality_INST_0_i_9_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_comparer_0_1 is
  port (
    srcA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    srcB : in STD_LOGIC_VECTOR ( 31 downto 0 );
    equality : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_comparer_0_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_comparer_0_1 : entity is "design_4_comparer_0_1,comparer,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_4_comparer_0_1 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_4_comparer_0_1 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_4_comparer_0_1 : entity is "comparer,Vivado 2020.1";
end design_4_comparer_0_1;

architecture STRUCTURE of design_4_comparer_0_1 is
begin
inst: entity work.design_4_comparer_0_1_comparer
     port map (
      equality => equality,
      srcA(31 downto 0) => srcA(31 downto 0),
      srcB(31 downto 0) => srcB(31 downto 0)
    );
end STRUCTURE;
