// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:30:00 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_control_unit_0_1/design_4_control_unit_0_1_sim_netlist.v
// Design      : design_4_control_unit_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_4_control_unit_0_1,control_unit,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "control_unit,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_control_unit_0_1
   (Op,
    Funct,
    RegtoPC,
    Jump,
    LeaveLink,
    RegWrite,
    MemtoReg,
    MemWrite,
    ALUControl,
    ALUSrc,
    RegDst,
    Branch,
    ToggleEqual);
  input [5:0]Op;
  input [5:0]Funct;
  output RegtoPC;
  output Jump;
  output LeaveLink;
  output RegWrite;
  output MemtoReg;
  output MemWrite;
  output [2:0]ALUControl;
  output ALUSrc;
  output RegDst;
  output Branch;
  output ToggleEqual;

  wire [2:0]ALUControl;
  wire \ALUControl[0]_INST_0_i_1_n_0 ;
  wire ALUSrc;
  wire Branch;
  wire [5:0]Funct;
  wire Jump;
  wire Jump_INST_0_i_1_n_0;
  wire Jump_INST_0_i_2_n_0;
  wire LeaveLink;
  wire MemWrite;
  wire MemtoReg;
  wire [5:0]Op;
  wire RegDst;
  wire RegWrite;
  wire RegtoPC;
  wire RegtoPC_INST_0_i_1_n_0;
  wire ToggleEqual;

  LUT6 #(
    .INIT(64'h0028000000000000)) 
    \ALUControl[0]_INST_0 
       (.I0(RegDst),
        .I1(Funct[3]),
        .I2(Funct[2]),
        .I3(Funct[4]),
        .I4(Funct[5]),
        .I5(\ALUControl[0]_INST_0_i_1_n_0 ),
        .O(ALUControl[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h24)) 
    \ALUControl[0]_INST_0_i_1 
       (.I0(Funct[0]),
        .I1(Funct[1]),
        .I2(Funct[2]),
        .O(\ALUControl[0]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h55555555DF755555)) 
    \ALUControl[1]_INST_0 
       (.I0(RegDst),
        .I1(Funct[1]),
        .I2(Funct[3]),
        .I3(Funct[5]),
        .I4(RegtoPC_INST_0_i_1_n_0),
        .I5(Funct[2]),
        .O(ALUControl[1]));
  LUT6 #(
    .INIT(64'h2000FFFF20000000)) 
    \ALUControl[2]_INST_0 
       (.I0(Funct[1]),
        .I1(Funct[2]),
        .I2(Funct[5]),
        .I3(RegtoPC_INST_0_i_1_n_0),
        .I4(RegDst),
        .I5(Branch),
        .O(ALUControl[2]));
  LUT6 #(
    .INIT(64'h1100000000000010)) 
    ALUSrc_INST_0
       (.I0(Op[2]),
        .I1(Op[4]),
        .I2(Op[3]),
        .I3(Op[5]),
        .I4(Op[1]),
        .I5(Op[0]),
        .O(ALUSrc));
  LUT5 #(
    .INIT(32'h00000002)) 
    Branch_INST_0
       (.I0(Op[2]),
        .I1(Op[3]),
        .I2(Op[1]),
        .I3(Op[4]),
        .I4(Op[5]),
        .O(Branch));
  LUT6 #(
    .INIT(64'h0000000F00000002)) 
    Jump_INST_0
       (.I0(Jump_INST_0_i_1_n_0),
        .I1(Op[0]),
        .I2(Jump_INST_0_i_2_n_0),
        .I3(Op[2]),
        .I4(Op[3]),
        .I5(Op[1]),
        .O(Jump));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    Jump_INST_0_i_1
       (.I0(Funct[5]),
        .I1(Funct[4]),
        .I2(Funct[1]),
        .I3(Funct[3]),
        .I4(Funct[0]),
        .I5(Funct[2]),
        .O(Jump_INST_0_i_1_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    Jump_INST_0_i_2
       (.I0(Op[4]),
        .I1(Op[5]),
        .O(Jump_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    LeaveLink_INST_0
       (.I0(Op[5]),
        .I1(Op[4]),
        .I2(Op[1]),
        .I3(Op[0]),
        .I4(Op[3]),
        .I5(Op[2]),
        .O(LeaveLink));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    MemWrite_INST_0
       (.I0(Op[2]),
        .I1(Op[4]),
        .I2(Op[1]),
        .I3(Op[0]),
        .I4(Op[5]),
        .I5(Op[3]),
        .O(MemWrite));
  LUT6 #(
    .INIT(64'h0000100000000000)) 
    MemtoReg_INST_0
       (.I0(Op[2]),
        .I1(Op[4]),
        .I2(Op[1]),
        .I3(Op[0]),
        .I4(Op[3]),
        .I5(Op[5]),
        .O(MemtoReg));
  LUT5 #(
    .INIT(32'h00000001)) 
    RegDst_INST_0
       (.I0(Op[4]),
        .I1(Op[2]),
        .I2(Op[0]),
        .I3(Op[5]),
        .I4(Op[3]),
        .O(RegDst));
  LUT6 #(
    .INIT(64'h0000000111000001)) 
    RegWrite_INST_0
       (.I0(Op[4]),
        .I1(Op[2]),
        .I2(Op[5]),
        .I3(Op[1]),
        .I4(Op[0]),
        .I5(Op[3]),
        .O(RegWrite));
  LUT6 #(
    .INIT(64'h0000100000000000)) 
    RegtoPC_INST_0
       (.I0(Funct[5]),
        .I1(Funct[1]),
        .I2(Funct[3]),
        .I3(RegDst),
        .I4(Funct[2]),
        .I5(RegtoPC_INST_0_i_1_n_0),
        .O(RegtoPC));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h1)) 
    RegtoPC_INST_0_i_1
       (.I0(Funct[4]),
        .I1(Funct[0]),
        .O(RegtoPC_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    ToggleEqual_INST_0
       (.I0(Op[5]),
        .I1(Op[4]),
        .I2(Op[2]),
        .I3(Op[0]),
        .I4(Op[3]),
        .I5(Op[1]),
        .O(ToggleEqual));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
