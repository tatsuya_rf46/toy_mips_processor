-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:30:00 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_equal_5bit_we_1_1/design_4_equal_5bit_we_1_1_sim_netlist.vhdl
-- Design      : design_4_equal_5bit_we_1_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_equal_5bit_we_1_1 is
  port (
    inp1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    we : in STD_LOGIC;
    outp : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_equal_5bit_we_1_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_equal_5bit_we_1_1 : entity is "design_4_equal_5bit_we_1_1,equal_5bit_we,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_4_equal_5bit_we_1_1 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_4_equal_5bit_we_1_1 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_4_equal_5bit_we_1_1 : entity is "equal_5bit_we,Vivado 2020.1";
end design_4_equal_5bit_we_1_1;

architecture STRUCTURE of design_4_equal_5bit_we_1_1 is
  signal outp_INST_0_i_1_n_0 : STD_LOGIC;
begin
outp_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9000009000000000"
    )
        port map (
      I0 => inp1(4),
      I1 => inp2(4),
      I2 => outp_INST_0_i_1_n_0,
      I3 => inp2(3),
      I4 => inp1(3),
      I5 => we,
      O => outp
    );
outp_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => inp1(0),
      I1 => inp2(0),
      I2 => inp2(2),
      I3 => inp1(2),
      I4 => inp2(1),
      I5 => inp1(1),
      O => outp_INST_0_i_1_n_0
    );
end STRUCTURE;
