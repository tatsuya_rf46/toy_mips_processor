-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:31:01 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_logical_shift_l2_32_0_1/design_4_logical_shift_l2_32_0_1_sim_netlist.vhdl
-- Design      : design_4_logical_shift_l2_32_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_logical_shift_l2_32_0_1 is
  port (
    source : in STD_LOGIC_VECTOR ( 31 downto 0 );
    shifted : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_logical_shift_l2_32_0_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_logical_shift_l2_32_0_1 : entity is "design_4_logical_shift_l2_32_0_1,logical_shift_l2_32,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_4_logical_shift_l2_32_0_1 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_4_logical_shift_l2_32_0_1 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_4_logical_shift_l2_32_0_1 : entity is "logical_shift_l2_32,Vivado 2020.1";
end design_4_logical_shift_l2_32_0_1;

architecture STRUCTURE of design_4_logical_shift_l2_32_0_1 is
  signal \<const0>\ : STD_LOGIC;
  signal \^source\ : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  \^source\(29 downto 0) <= source(29 downto 0);
  shifted(31 downto 2) <= \^source\(29 downto 0);
  shifted(1) <= \<const0>\;
  shifted(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
end STRUCTURE;
