-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:31:58 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_multiplexer_32_7_1/design_4_multiplexer_32_7_1_sim_netlist.vhdl
-- Design      : design_4_multiplexer_32_7_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_multiplexer_32_7_1_multiplexer_32 is
  port (
    y : out STD_LOGIC_VECTOR ( 31 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    inp1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    selector : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_4_multiplexer_32_7_1_multiplexer_32 : entity is "multiplexer_32";
end design_4_multiplexer_32_7_1_multiplexer_32;

architecture STRUCTURE of design_4_multiplexer_32_7_1_multiplexer_32 is
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \y[0]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \y[10]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \y[11]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \y[12]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \y[13]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \y[14]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \y[15]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \y[16]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \y[17]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \y[18]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \y[19]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \y[1]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \y[20]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \y[21]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \y[22]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \y[23]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \y[24]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \y[25]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \y[26]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \y[27]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \y[28]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \y[29]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \y[2]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \y[30]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \y[31]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \y[3]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \y[4]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \y[5]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \y[6]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \y[7]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \y[8]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \y[9]_INST_0\ : label is "soft_lutpair4";
begin
\y[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(0),
      I1 => inp1(0),
      I2 => selector,
      O => y(0)
    );
\y[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(10),
      I1 => inp1(10),
      I2 => selector,
      O => y(10)
    );
\y[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(11),
      I1 => inp1(11),
      I2 => selector,
      O => y(11)
    );
\y[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(12),
      I1 => inp1(12),
      I2 => selector,
      O => y(12)
    );
\y[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(13),
      I1 => inp1(13),
      I2 => selector,
      O => y(13)
    );
\y[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(14),
      I1 => inp1(14),
      I2 => selector,
      O => y(14)
    );
\y[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(15),
      I1 => inp1(15),
      I2 => selector,
      O => y(15)
    );
\y[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(16),
      I1 => inp1(16),
      I2 => selector,
      O => y(16)
    );
\y[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(17),
      I1 => inp1(17),
      I2 => selector,
      O => y(17)
    );
\y[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(18),
      I1 => inp1(18),
      I2 => selector,
      O => y(18)
    );
\y[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(19),
      I1 => inp1(19),
      I2 => selector,
      O => y(19)
    );
\y[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(1),
      I1 => inp1(1),
      I2 => selector,
      O => y(1)
    );
\y[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(20),
      I1 => inp1(20),
      I2 => selector,
      O => y(20)
    );
\y[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(21),
      I1 => inp1(21),
      I2 => selector,
      O => y(21)
    );
\y[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(22),
      I1 => inp1(22),
      I2 => selector,
      O => y(22)
    );
\y[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(23),
      I1 => inp1(23),
      I2 => selector,
      O => y(23)
    );
\y[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(24),
      I1 => inp1(24),
      I2 => selector,
      O => y(24)
    );
\y[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(25),
      I1 => inp1(25),
      I2 => selector,
      O => y(25)
    );
\y[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(26),
      I1 => inp1(26),
      I2 => selector,
      O => y(26)
    );
\y[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(27),
      I1 => inp1(27),
      I2 => selector,
      O => y(27)
    );
\y[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(28),
      I1 => inp1(28),
      I2 => selector,
      O => y(28)
    );
\y[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(29),
      I1 => inp1(29),
      I2 => selector,
      O => y(29)
    );
\y[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(2),
      I1 => inp1(2),
      I2 => selector,
      O => y(2)
    );
\y[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(30),
      I1 => inp1(30),
      I2 => selector,
      O => y(30)
    );
\y[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(31),
      I1 => inp1(31),
      I2 => selector,
      O => y(31)
    );
\y[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(3),
      I1 => inp1(3),
      I2 => selector,
      O => y(3)
    );
\y[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(4),
      I1 => inp1(4),
      I2 => selector,
      O => y(4)
    );
\y[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(5),
      I1 => inp1(5),
      I2 => selector,
      O => y(5)
    );
\y[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(6),
      I1 => inp1(6),
      I2 => selector,
      O => y(6)
    );
\y[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(7),
      I1 => inp1(7),
      I2 => selector,
      O => y(7)
    );
\y[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(8),
      I1 => inp1(8),
      I2 => selector,
      O => y(8)
    );
\y[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => inp2(9),
      I1 => inp1(9),
      I2 => selector,
      O => y(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_multiplexer_32_7_1 is
  port (
    inp1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    selector : in STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_multiplexer_32_7_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_multiplexer_32_7_1 : entity is "design_4_multiplexer_32_7_1,multiplexer_32,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_4_multiplexer_32_7_1 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_4_multiplexer_32_7_1 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_4_multiplexer_32_7_1 : entity is "multiplexer_32,Vivado 2020.1";
end design_4_multiplexer_32_7_1;

architecture STRUCTURE of design_4_multiplexer_32_7_1 is
begin
inst: entity work.design_4_multiplexer_32_7_1_multiplexer_32
     port map (
      inp1(31 downto 0) => inp1(31 downto 0),
      inp2(31 downto 0) => inp2(31 downto 0),
      selector => selector,
      y(31 downto 0) => y(31 downto 0)
    );
end STRUCTURE;
