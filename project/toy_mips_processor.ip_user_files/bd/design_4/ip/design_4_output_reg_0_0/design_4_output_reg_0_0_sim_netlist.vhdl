-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:29:59 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_output_reg_0_0/design_4_output_reg_0_0_sim_netlist.vhdl
-- Design      : design_4_output_reg_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_output_reg_0_0_output_reg is
  port (
    O0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    O1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PC : in STD_LOGIC_VECTOR ( 30 downto 0 );
    D0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    clk : in STD_LOGIC;
    D1 : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_4_output_reg_0_0_output_reg : entity is "output_reg";
end design_4_output_reg_0_0_output_reg;

architecture STRUCTURE of design_4_output_reg_0_0_output_reg is
  signal \O0[3]_i_2_n_0\ : STD_LOGIC;
  signal \O0[3]_i_3_n_0\ : STD_LOGIC;
  signal \O0[3]_i_4_n_0\ : STD_LOGIC;
  signal \O0[3]_i_5_n_0\ : STD_LOGIC;
  signal \O0[3]_i_6_n_0\ : STD_LOGIC;
  signal \O0[3]_i_7_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
begin
\O0[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \O0[3]_i_2_n_0\,
      I1 => \O0[3]_i_3_n_0\,
      I2 => \O0[3]_i_4_n_0\,
      I3 => \O0[3]_i_5_n_0\,
      I4 => \O0[3]_i_6_n_0\,
      I5 => \O0[3]_i_7_n_0\,
      O => p_0_in
    );
\O0[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => PC(10),
      I1 => PC(11),
      I2 => PC(8),
      I3 => PC(9),
      I4 => PC(7),
      I5 => PC(6),
      O => \O0[3]_i_2_n_0\
    );
\O0[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => PC(16),
      I1 => PC(17),
      I2 => PC(14),
      I3 => PC(15),
      I4 => PC(13),
      I5 => PC(12),
      O => \O0[3]_i_3_n_0\
    );
\O0[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => PC(22),
      I1 => PC(23),
      I2 => PC(20),
      I3 => PC(21),
      I4 => PC(19),
      I5 => PC(18),
      O => \O0[3]_i_4_n_0\
    );
\O0[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FE00AA00"
    )
        port map (
      I0 => PC(3),
      I1 => PC(0),
      I2 => PC(1),
      I3 => PC(5),
      I4 => PC(2),
      O => \O0[3]_i_5_n_0\
    );
\O0[3]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => PC(5),
      I1 => PC(4),
      I2 => PC(30),
      O => \O0[3]_i_6_n_0\
    );
\O0[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => PC(28),
      I1 => PC(29),
      I2 => PC(26),
      I3 => PC(27),
      I4 => PC(25),
      I5 => PC(24),
      O => \O0[3]_i_7_n_0\
    );
\O0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D0(0),
      Q => O0(0),
      R => '0'
    );
\O0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D0(1),
      Q => O0(1),
      R => '0'
    );
\O0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D0(2),
      Q => O0(2),
      R => '0'
    );
\O0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D0(3),
      Q => O0(3),
      R => '0'
    );
\O1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D1(0),
      Q => O1(0),
      R => '0'
    );
\O1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D1(1),
      Q => O1(1),
      R => '0'
    );
\O1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D1(2),
      Q => O1(2),
      R => '0'
    );
\O1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => p_0_in,
      D => D1(3),
      Q => O1(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_output_reg_0_0 is
  port (
    clk : in STD_LOGIC;
    PC : in STD_LOGIC_VECTOR ( 31 downto 0 );
    D0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    O0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    O1 : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_output_reg_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_output_reg_0_0 : entity is "design_4_output_reg_0_0,output_reg,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_4_output_reg_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_4_output_reg_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_4_output_reg_0_0 : entity is "output_reg,Vivado 2020.1";
end design_4_output_reg_0_0;

architecture STRUCTURE of design_4_output_reg_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_4_sys_clock, INSERT_VIP 0";
begin
inst: entity work.design_4_output_reg_0_0_output_reg
     port map (
      D0(3 downto 0) => D0(3 downto 0),
      D1(3 downto 0) => D1(3 downto 0),
      O0(3 downto 0) => O0(3 downto 0),
      O1(3 downto 0) => O1(3 downto 0),
      PC(30 downto 0) => PC(31 downto 1),
      clk => clk
    );
end STRUCTURE;
