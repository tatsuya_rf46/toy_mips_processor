vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xil_defaultlib
vlib modelsim_lib/msim/xbip_utils_v3_0_10
vlib modelsim_lib/msim/c_reg_fd_v12_0_6
vlib modelsim_lib/msim/c_mux_bit_v12_0_6
vlib modelsim_lib/msim/c_shift_ram_v12_0_14
vlib modelsim_lib/msim/xbip_dsp48_wrapper_v3_0_4
vlib modelsim_lib/msim/xbip_pipe_v3_0_6
vlib modelsim_lib/msim/xbip_dsp48_addsub_v3_0_6
vlib modelsim_lib/msim/xbip_addsub_v3_0_6
vlib modelsim_lib/msim/c_addsub_v12_0_14
vlib modelsim_lib/msim/dist_mem_gen_v8_0_13
vlib modelsim_lib/msim/util_vector_logic_v2_0_1
vlib modelsim_lib/msim/xlconcat_v2_1_3
vlib modelsim_lib/msim/xlconstant_v1_1_7
vlib modelsim_lib/msim/xlslice_v1_0_2
vlib modelsim_lib/msim/sim_clk_gen_v1_0_2

vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib
vmap xbip_utils_v3_0_10 modelsim_lib/msim/xbip_utils_v3_0_10
vmap c_reg_fd_v12_0_6 modelsim_lib/msim/c_reg_fd_v12_0_6
vmap c_mux_bit_v12_0_6 modelsim_lib/msim/c_mux_bit_v12_0_6
vmap c_shift_ram_v12_0_14 modelsim_lib/msim/c_shift_ram_v12_0_14
vmap xbip_dsp48_wrapper_v3_0_4 modelsim_lib/msim/xbip_dsp48_wrapper_v3_0_4
vmap xbip_pipe_v3_0_6 modelsim_lib/msim/xbip_pipe_v3_0_6
vmap xbip_dsp48_addsub_v3_0_6 modelsim_lib/msim/xbip_dsp48_addsub_v3_0_6
vmap xbip_addsub_v3_0_6 modelsim_lib/msim/xbip_addsub_v3_0_6
vmap c_addsub_v12_0_14 modelsim_lib/msim/c_addsub_v12_0_14
vmap dist_mem_gen_v8_0_13 modelsim_lib/msim/dist_mem_gen_v8_0_13
vmap util_vector_logic_v2_0_1 modelsim_lib/msim/util_vector_logic_v2_0_1
vmap xlconcat_v2_1_3 modelsim_lib/msim/xlconcat_v2_1_3
vmap xlconstant_v1_1_7 modelsim_lib/msim/xlconstant_v1_1_7
vmap xlslice_v1_0_2 modelsim_lib/msim/xlslice_v1_0_2
vmap sim_clk_gen_v1_0_2 modelsim_lib/msim/sim_clk_gen_v1_0_2

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_control_unit_0_1/sim/design_3_control_unit_0_1.v" \

vcom -work xbip_utils_v3_0_10 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/d117/hdl/xbip_utils_v3_0_vh_rfs.vhd" \

vcom -work c_reg_fd_v12_0_6 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/edec/hdl/c_reg_fd_v12_0_vh_rfs.vhd" \

vcom -work c_mux_bit_v12_0_6 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/ecb4/hdl/c_mux_bit_v12_0_vh_rfs.vhd" \

vcom -work c_shift_ram_v12_0_14 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/2598/hdl/c_shift_ram_v12_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_3/ip/design_3_c_shift_ram_0_0/sim/design_3_c_shift_ram_0_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_0_1/sim/design_3_c_shift_ram_0_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_0_2/sim/design_3_c_shift_ram_0_2.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_0_3/sim/design_3_c_shift_ram_0_3.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_0_4/sim/design_3_c_shift_ram_0_4.vhd" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_multiplexer_32_0_0/sim/design_3_multiplexer_32_0_0.v" \
"../../../bd/design_3/ip/design_3_multiplexer_32_0_1/sim/design_3_multiplexer_32_0_1.v" \
"../../../bd/design_3/ip/design_3_multiplexer_32_0_2/sim/design_3_multiplexer_32_0_2.v" \

vcom -work xbip_dsp48_wrapper_v3_0_4 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/cdbf/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \

vcom -work xbip_pipe_v3_0_6 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/7468/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \

vcom -work xbip_dsp48_addsub_v3_0_6 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/910d/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \

vcom -work xbip_addsub_v3_0_6 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/cfdd/hdl/xbip_addsub_v3_0_vh_rfs.vhd" \

vcom -work c_addsub_v12_0_14 -64 -93 \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/ebb8/hdl/c_addsub_v12_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_3/ip/design_3_c_addsub_0_0/sim/design_3_c_addsub_0_0.vhd" \

vlog -work dist_mem_gen_v8_0_13 -64 -incr \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/0bf5/simulation/dist_mem_gen_v8_0.v" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_dist_mem_gen_0_0/sim/design_3_dist_mem_gen_0_0.v" \
"../../../bd/design_3/ip/design_3_dist_mem_gen_1_0/sim/design_3_dist_mem_gen_1_0.v" \
"../../../bd/design_3/ip/design_3_dist_mem_gen_1_1/sim/design_3_dist_mem_gen_1_1.v" \
"../../../bd/design_3/ip/design_3_sign_extend_16to32_0_0/sim/design_3_sign_extend_16to32_0_0.v" \
"../../../bd/design_3/ip/design_3_logical_shift_l2_32_0_0/sim/design_3_logical_shift_l2_32_0_0.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_3/ip/design_3_c_addsub_1_0/sim/design_3_c_addsub_1_0.vhd" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_comparer_0_0/sim/design_3_comparer_0_0.v" \

vlog -work util_vector_logic_v2_0_1 -64 -incr \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/2137/hdl/util_vector_logic_v2_0_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_util_vector_logic_0_0/sim/design_3_util_vector_logic_0_0.v" \
"../../../bd/design_3/ip/design_3_util_vector_logic_0_1/sim/design_3_util_vector_logic_0_1.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_3/ip/design_3_c_shift_ram_4_0/sim/design_3_c_shift_ram_4_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_4_1/sim/design_3_c_shift_ram_4_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_4_2/sim/design_3_c_shift_ram_4_2.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_7_0/sim/design_3_c_shift_ram_7_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_7_1/sim/design_3_c_shift_ram_7_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_9_0/sim/design_3_c_shift_ram_9_0.vhd" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_0/sim/design_3_multiplexer_4p_32_0_0.v" \
"../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_1/sim/design_3_multiplexer_4p_32_0_1.v" \
"../../../bd/design_3/ip/design_3_multiplexer_32_4_0/sim/design_3_multiplexer_32_4_0.v" \
"../../../bd/design_3/ip/design_3_alu_0_0/sim/design_3_alu_0_0.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_3/ip/design_3_c_shift_ram_10_0/sim/design_3_c_shift_ram_10_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_11_0/sim/design_3_c_shift_ram_11_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_11_1/sim/design_3_c_shift_ram_11_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_10_1/sim/design_3_c_shift_ram_10_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_3_0/sim/design_3_c_shift_ram_3_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_15_0/sim/design_3_c_shift_ram_15_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_16_0/sim/design_3_c_shift_ram_16_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_9_1/sim/design_3_c_shift_ram_9_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_9_2/sim/design_3_c_shift_ram_9_2.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_12_0/sim/design_3_c_shift_ram_12_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_3_1/sim/design_3_c_shift_ram_3_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_3_2/sim/design_3_c_shift_ram_3_2.vhd" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_dist_mem_gen_0_1/sim/design_3_dist_mem_gen_0_1.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_3/ip/design_3_c_shift_ram_22_0/sim/design_3_c_shift_ram_22_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_22_1/sim/design_3_c_shift_ram_22_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_22_2/sim/design_3_c_shift_ram_22_2.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_18_0/sim/design_3_c_shift_ram_18_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_18_1/sim/design_3_c_shift_ram_18_1.vhd" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_multiplexer_32_5_1/sim/design_3_multiplexer_32_5_1.v" \
"../../../bd/design_3/ip/design_3_hazard_unit_0_0/sim/design_3_hazard_unit_0_0.v" \
"../../../bd/design_3/ip/design_3_util_vector_logic_2_0/sim/design_3_util_vector_logic_2_0.v" \

vlog -work xlconcat_v2_1_3 -64 -incr \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_xlconcat_0_0/sim/design_3_xlconcat_0_0.v" \

vlog -work xlconstant_v1_1_7 -64 -incr \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/fcfc/hdl/xlconstant_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_xlconstant_0_0/sim/design_3_xlconstant_0_0.v" \

vlog -work xlslice_v1_0_2 -64 -incr \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/11d0/hdl/xlslice_v1_0_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_xlslice_0_1/sim/design_3_xlslice_0_1.v" \
"../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_2/sim/design_3_multiplexer_4p_32_0_2.v" \
"../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_3/sim/design_3_multiplexer_4p_32_0_3.v" \
"../../../bd/design_3/ip/design_3_multiplexer_32_2_0/sim/design_3_multiplexer_32_2_0.v" \
"../../../bd/design_3/ip/design_3_multiplexer_5_0_0/sim/design_3_multiplexer_5_0_0.v" \
"../../../bd/design_3/ip/design_3_util_vector_logic_3_0/sim/design_3_util_vector_logic_3_0.v" \
"../../../bd/design_3/ip/design_3_xlslice_1_0/sim/design_3_xlslice_1_0.v" \
"../../../bd/design_3/ip/design_3_xlslice_1_1/sim/design_3_xlslice_1_1.v" \
"../../../bd/design_3/ip/design_3_xlslice_1_2/sim/design_3_xlslice_1_2.v" \
"../../../bd/design_3/ip/design_3_xlslice_1_3/sim/design_3_xlslice_1_3.v" \
"../../../bd/design_3/ip/design_3_xlconstant_1_0/sim/design_3_xlconstant_1_0.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/design_3/ip/design_3_c_shift_ram_20_0/sim/design_3_c_shift_ram_20_0.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_20_1/sim/design_3_c_shift_ram_20_1.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_20_2/sim/design_3_c_shift_ram_20_2.vhd" \
"../../../bd/design_3/ip/design_3_c_shift_ram_20_3/sim/design_3_c_shift_ram_20_3.vhd" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_util_vector_logic_3_1/sim/design_3_util_vector_logic_3_1.v" \
"../../../bd/design_3/ip/design_3_xlslice_5_0/sim/design_3_xlslice_5_0.v" \
"../../../bd/design_3/ip/design_3_xlslice_5_1/sim/design_3_xlslice_5_1.v" \
"../../../bd/design_3/ip/design_3_xlslice_5_2/sim/design_3_xlslice_5_2.v" \
"../../../bd/design_3/ip/design_3_multiplexer_5_1_0/sim/design_3_multiplexer_5_1_0.v" \

vlog -work sim_clk_gen_v1_0_2 -64 -incr \
"../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/b740/hdl/sim_clk_gen_v1_0_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr \
"../../../bd/design_3/ip/design_3_sim_clk_gen_0_0/sim/design_3_sim_clk_gen_0_0.v" \
"../../../bd/design_3/ip/design_3_multiplexer_32_4_1/sim/design_3_multiplexer_32_4_1.v" \
"../../../bd/design_3/ip/design_3_multiplexer_32_4_2/sim/design_3_multiplexer_32_4_2.v" \
"../../../bd/design_3/ip/design_3_equal_5bit_we_1_0/sim/design_3_equal_5bit_we_1_0.v" \
"../../../bd/design_3/ip/design_3_equal_5bit_we_1_1/sim/design_3_equal_5bit_we_1_1.v" \
"../../../bd/design_3/sim/design_3.v" \

vlog -work xil_defaultlib \
"glbl.v"

