-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_control_unit_0_1/sim/design_3_control_unit_0_1.v" \
-endlib
-makelib xcelium_lib/xbip_utils_v3_0_10 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/d117/hdl/xbip_utils_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_reg_fd_v12_0_6 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/edec/hdl/c_reg_fd_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_mux_bit_v12_0_6 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/ecb4/hdl/c_mux_bit_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_shift_ram_v12_0_14 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/2598/hdl/c_shift_ram_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_c_shift_ram_0_0/sim/design_3_c_shift_ram_0_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_0_1/sim/design_3_c_shift_ram_0_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_0_2/sim/design_3_c_shift_ram_0_2.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_0_3/sim/design_3_c_shift_ram_0_3.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_0_4/sim/design_3_c_shift_ram_0_4.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_multiplexer_32_0_0/sim/design_3_multiplexer_32_0_0.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_32_0_1/sim/design_3_multiplexer_32_0_1.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_32_0_2/sim/design_3_multiplexer_32_0_2.v" \
-endlib
-makelib xcelium_lib/xbip_dsp48_wrapper_v3_0_4 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/cdbf/hdl/xbip_dsp48_wrapper_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_pipe_v3_0_6 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/7468/hdl/xbip_pipe_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_dsp48_addsub_v3_0_6 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/910d/hdl/xbip_dsp48_addsub_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xbip_addsub_v3_0_6 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/cfdd/hdl/xbip_addsub_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/c_addsub_v12_0_14 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/ebb8/hdl/c_addsub_v12_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_c_addsub_0_0/sim/design_3_c_addsub_0_0.vhd" \
-endlib
-makelib xcelium_lib/dist_mem_gen_v8_0_13 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/0bf5/simulation/dist_mem_gen_v8_0.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_dist_mem_gen_0_0/sim/design_3_dist_mem_gen_0_0.v" \
  "../../../bd/design_3/ip/design_3_dist_mem_gen_1_0/sim/design_3_dist_mem_gen_1_0.v" \
  "../../../bd/design_3/ip/design_3_dist_mem_gen_1_1/sim/design_3_dist_mem_gen_1_1.v" \
  "../../../bd/design_3/ip/design_3_sign_extend_16to32_0_0/sim/design_3_sign_extend_16to32_0_0.v" \
  "../../../bd/design_3/ip/design_3_logical_shift_l2_32_0_0/sim/design_3_logical_shift_l2_32_0_0.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_c_addsub_1_0/sim/design_3_c_addsub_1_0.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_comparer_0_0/sim/design_3_comparer_0_0.v" \
-endlib
-makelib xcelium_lib/util_vector_logic_v2_0_1 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/2137/hdl/util_vector_logic_v2_0_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_util_vector_logic_0_0/sim/design_3_util_vector_logic_0_0.v" \
  "../../../bd/design_3/ip/design_3_util_vector_logic_0_1/sim/design_3_util_vector_logic_0_1.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_c_shift_ram_4_0/sim/design_3_c_shift_ram_4_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_4_1/sim/design_3_c_shift_ram_4_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_4_2/sim/design_3_c_shift_ram_4_2.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_7_0/sim/design_3_c_shift_ram_7_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_7_1/sim/design_3_c_shift_ram_7_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_9_0/sim/design_3_c_shift_ram_9_0.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_0/sim/design_3_multiplexer_4p_32_0_0.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_1/sim/design_3_multiplexer_4p_32_0_1.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_32_4_0/sim/design_3_multiplexer_32_4_0.v" \
  "../../../bd/design_3/ip/design_3_alu_0_0/sim/design_3_alu_0_0.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_c_shift_ram_10_0/sim/design_3_c_shift_ram_10_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_11_0/sim/design_3_c_shift_ram_11_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_11_1/sim/design_3_c_shift_ram_11_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_10_1/sim/design_3_c_shift_ram_10_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_3_0/sim/design_3_c_shift_ram_3_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_15_0/sim/design_3_c_shift_ram_15_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_16_0/sim/design_3_c_shift_ram_16_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_9_1/sim/design_3_c_shift_ram_9_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_9_2/sim/design_3_c_shift_ram_9_2.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_12_0/sim/design_3_c_shift_ram_12_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_3_1/sim/design_3_c_shift_ram_3_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_3_2/sim/design_3_c_shift_ram_3_2.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_dist_mem_gen_0_1/sim/design_3_dist_mem_gen_0_1.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_c_shift_ram_22_0/sim/design_3_c_shift_ram_22_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_22_1/sim/design_3_c_shift_ram_22_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_22_2/sim/design_3_c_shift_ram_22_2.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_18_0/sim/design_3_c_shift_ram_18_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_18_1/sim/design_3_c_shift_ram_18_1.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_multiplexer_32_5_1/sim/design_3_multiplexer_32_5_1.v" \
  "../../../bd/design_3/ip/design_3_hazard_unit_0_0/sim/design_3_hazard_unit_0_0.v" \
  "../../../bd/design_3/ip/design_3_util_vector_logic_2_0/sim/design_3_util_vector_logic_2_0.v" \
-endlib
-makelib xcelium_lib/xlconcat_v2_1_3 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_xlconcat_0_0/sim/design_3_xlconcat_0_0.v" \
-endlib
-makelib xcelium_lib/xlconstant_v1_1_7 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/fcfc/hdl/xlconstant_v1_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_xlconstant_0_0/sim/design_3_xlconstant_0_0.v" \
-endlib
-makelib xcelium_lib/xlslice_v1_0_2 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/11d0/hdl/xlslice_v1_0_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_xlslice_0_1/sim/design_3_xlslice_0_1.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_2/sim/design_3_multiplexer_4p_32_0_2.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_4p_32_0_3/sim/design_3_multiplexer_4p_32_0_3.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_32_2_0/sim/design_3_multiplexer_32_2_0.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_5_0_0/sim/design_3_multiplexer_5_0_0.v" \
  "../../../bd/design_3/ip/design_3_util_vector_logic_3_0/sim/design_3_util_vector_logic_3_0.v" \
  "../../../bd/design_3/ip/design_3_xlslice_1_0/sim/design_3_xlslice_1_0.v" \
  "../../../bd/design_3/ip/design_3_xlslice_1_1/sim/design_3_xlslice_1_1.v" \
  "../../../bd/design_3/ip/design_3_xlslice_1_2/sim/design_3_xlslice_1_2.v" \
  "../../../bd/design_3/ip/design_3_xlslice_1_3/sim/design_3_xlslice_1_3.v" \
  "../../../bd/design_3/ip/design_3_xlconstant_1_0/sim/design_3_xlconstant_1_0.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_c_shift_ram_20_0/sim/design_3_c_shift_ram_20_0.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_20_1/sim/design_3_c_shift_ram_20_1.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_20_2/sim/design_3_c_shift_ram_20_2.vhd" \
  "../../../bd/design_3/ip/design_3_c_shift_ram_20_3/sim/design_3_c_shift_ram_20_3.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_util_vector_logic_3_1/sim/design_3_util_vector_logic_3_1.v" \
  "../../../bd/design_3/ip/design_3_xlslice_5_0/sim/design_3_xlslice_5_0.v" \
  "../../../bd/design_3/ip/design_3_xlslice_5_1/sim/design_3_xlslice_5_1.v" \
  "../../../bd/design_3/ip/design_3_xlslice_5_2/sim/design_3_xlslice_5_2.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_5_1_0/sim/design_3_multiplexer_5_1_0.v" \
-endlib
-makelib xcelium_lib/sim_clk_gen_v1_0_2 \
  "../../../../toy_mips_processor.srcs/sources_1/bd/design_3/ipshared/b740/hdl/sim_clk_gen_v1_0_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_3/ip/design_3_sim_clk_gen_0_0/sim/design_3_sim_clk_gen_0_0.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_32_4_1/sim/design_3_multiplexer_32_4_1.v" \
  "../../../bd/design_3/ip/design_3_multiplexer_32_4_2/sim/design_3_multiplexer_32_4_2.v" \
  "../../../bd/design_3/ip/design_3_equal_5bit_we_1_0/sim/design_3_equal_5bit_we_1_0.v" \
  "../../../bd/design_3/ip/design_3_equal_5bit_we_1_1/sim/design_3_equal_5bit_we_1_1.v" \
  "../../../bd/design_3/sim/design_3.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

