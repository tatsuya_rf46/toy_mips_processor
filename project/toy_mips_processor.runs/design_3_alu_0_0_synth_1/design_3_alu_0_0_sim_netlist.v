// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:02:39 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_alu_0_0_sim_netlist.v
// Design      : design_3_alu_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_alu
   (alu_result,
    zero,
    srcA,
    srcB,
    alu_control,
    alu_result_0_sp_1);
  output [31:0]alu_result;
  output zero;
  input [31:0]srcA;
  input [31:0]srcB;
  input [2:0]alu_control;
  input alu_result_0_sp_1;

  wire [2:0]alu_control;
  wire [31:0]alu_result;
  wire alu_result_0_sn_1;
  wire [31:0]out2__93;
  wire out2_carry__0_n_0;
  wire out2_carry__0_n_1;
  wire out2_carry__0_n_2;
  wire out2_carry__0_n_3;
  wire out2_carry__1_n_0;
  wire out2_carry__1_n_1;
  wire out2_carry__1_n_2;
  wire out2_carry__1_n_3;
  wire out2_carry__2_n_0;
  wire out2_carry__2_n_1;
  wire out2_carry__2_n_2;
  wire out2_carry__2_n_3;
  wire out2_carry__3_n_0;
  wire out2_carry__3_n_1;
  wire out2_carry__3_n_2;
  wire out2_carry__3_n_3;
  wire out2_carry__4_n_0;
  wire out2_carry__4_n_1;
  wire out2_carry__4_n_2;
  wire out2_carry__4_n_3;
  wire out2_carry__5_n_0;
  wire out2_carry__5_n_1;
  wire out2_carry__5_n_2;
  wire out2_carry__5_n_3;
  wire out2_carry__6_n_1;
  wire out2_carry__6_n_2;
  wire out2_carry__6_n_3;
  wire out2_carry_i_1__0_n_0;
  wire out2_carry_i_1__1_n_0;
  wire out2_carry_i_1__2_n_0;
  wire out2_carry_i_1__3_n_0;
  wire out2_carry_i_1__4_n_0;
  wire out2_carry_i_1__5_n_0;
  wire out2_carry_i_1__6_n_0;
  wire out2_carry_i_1_n_0;
  wire out2_carry_i_2__0_n_0;
  wire out2_carry_i_2__1_n_0;
  wire out2_carry_i_2__2_n_0;
  wire out2_carry_i_2__3_n_0;
  wire out2_carry_i_2__4_n_0;
  wire out2_carry_i_2__5_n_0;
  wire out2_carry_i_2__6_n_0;
  wire out2_carry_i_2_n_0;
  wire out2_carry_i_3__0_n_0;
  wire out2_carry_i_3__1_n_0;
  wire out2_carry_i_3__2_n_0;
  wire out2_carry_i_3__3_n_0;
  wire out2_carry_i_3__4_n_0;
  wire out2_carry_i_3__5_n_0;
  wire out2_carry_i_3__6_n_0;
  wire out2_carry_i_3_n_0;
  wire out2_carry_i_4__0_n_0;
  wire out2_carry_i_4__1_n_0;
  wire out2_carry_i_4__2_n_0;
  wire out2_carry_i_4__3_n_0;
  wire out2_carry_i_4__4_n_0;
  wire out2_carry_i_4__5_n_0;
  wire out2_carry_i_4__6_n_0;
  wire out2_carry_i_4_n_0;
  wire out2_carry_n_0;
  wire out2_carry_n_1;
  wire out2_carry_n_2;
  wire out2_carry_n_3;
  wire [31:0]srcA;
  wire [31:0]srcB;
  wire zero;
  wire zero_INST_0_i_1_n_0;
  wire zero_INST_0_i_2_n_0;
  wire zero_INST_0_i_3_n_0;
  wire zero_INST_0_i_4_n_0;
  wire zero_INST_0_i_5_n_0;
  wire zero_INST_0_i_6_n_0;
  wire [3:3]NLW_out2_carry__6_CO_UNCONNECTED;

  assign alu_result_0_sn_1 = alu_result_0_sp_1;
  LUT6 #(
    .INIT(64'hCACACACAFFF0F000)) 
    \alu_result[0]_INST_0 
       (.I0(out2__93[0]),
        .I1(out2__93[31]),
        .I2(alu_control[0]),
        .I3(alu_result_0_sn_1),
        .I4(srcA[0]),
        .I5(alu_control[1]),
        .O(alu_result[0]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[10]_INST_0 
       (.I0(out2__93[10]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[10]),
        .I4(srcB[10]),
        .I5(alu_control[2]),
        .O(alu_result[10]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[11]_INST_0 
       (.I0(out2__93[11]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[11]),
        .I4(srcB[11]),
        .I5(alu_control[2]),
        .O(alu_result[11]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[12]_INST_0 
       (.I0(out2__93[12]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[12]),
        .I4(srcB[12]),
        .I5(alu_control[2]),
        .O(alu_result[12]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[13]_INST_0 
       (.I0(out2__93[13]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[13]),
        .I4(srcB[13]),
        .I5(alu_control[2]),
        .O(alu_result[13]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[14]_INST_0 
       (.I0(out2__93[14]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[14]),
        .I4(srcB[14]),
        .I5(alu_control[2]),
        .O(alu_result[14]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[15]_INST_0 
       (.I0(out2__93[15]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[15]),
        .I4(srcB[15]),
        .I5(alu_control[2]),
        .O(alu_result[15]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[16]_INST_0 
       (.I0(out2__93[16]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[16]),
        .I4(srcB[16]),
        .I5(alu_control[2]),
        .O(alu_result[16]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[17]_INST_0 
       (.I0(out2__93[17]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[17]),
        .I4(srcB[17]),
        .I5(alu_control[2]),
        .O(alu_result[17]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[18]_INST_0 
       (.I0(out2__93[18]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[18]),
        .I4(srcB[18]),
        .I5(alu_control[2]),
        .O(alu_result[18]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[19]_INST_0 
       (.I0(out2__93[19]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[19]),
        .I4(srcB[19]),
        .I5(alu_control[2]),
        .O(alu_result[19]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[1]_INST_0 
       (.I0(out2__93[1]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[1]),
        .I4(srcB[1]),
        .I5(alu_control[2]),
        .O(alu_result[1]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[20]_INST_0 
       (.I0(out2__93[20]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[20]),
        .I4(srcB[20]),
        .I5(alu_control[2]),
        .O(alu_result[20]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[21]_INST_0 
       (.I0(out2__93[21]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[21]),
        .I4(srcB[21]),
        .I5(alu_control[2]),
        .O(alu_result[21]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[22]_INST_0 
       (.I0(out2__93[22]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[22]),
        .I4(srcB[22]),
        .I5(alu_control[2]),
        .O(alu_result[22]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[23]_INST_0 
       (.I0(out2__93[23]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[23]),
        .I4(srcB[23]),
        .I5(alu_control[2]),
        .O(alu_result[23]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[24]_INST_0 
       (.I0(out2__93[24]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[24]),
        .I4(srcB[24]),
        .I5(alu_control[2]),
        .O(alu_result[24]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[25]_INST_0 
       (.I0(out2__93[25]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[25]),
        .I4(srcB[25]),
        .I5(alu_control[2]),
        .O(alu_result[25]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[26]_INST_0 
       (.I0(out2__93[26]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[26]),
        .I4(srcB[26]),
        .I5(alu_control[2]),
        .O(alu_result[26]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[27]_INST_0 
       (.I0(out2__93[27]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[27]),
        .I4(srcB[27]),
        .I5(alu_control[2]),
        .O(alu_result[27]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[28]_INST_0 
       (.I0(out2__93[28]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[28]),
        .I4(srcB[28]),
        .I5(alu_control[2]),
        .O(alu_result[28]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[29]_INST_0 
       (.I0(out2__93[29]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[29]),
        .I4(srcB[29]),
        .I5(alu_control[2]),
        .O(alu_result[29]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[2]_INST_0 
       (.I0(out2__93[2]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[2]),
        .I4(srcB[2]),
        .I5(alu_control[2]),
        .O(alu_result[2]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[30]_INST_0 
       (.I0(out2__93[30]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[30]),
        .I4(srcB[30]),
        .I5(alu_control[2]),
        .O(alu_result[30]));
  LUT6 #(
    .INIT(64'h3033333088B8B888)) 
    \alu_result[31]_INST_0 
       (.I0(out2__93[31]),
        .I1(alu_control[1]),
        .I2(srcA[31]),
        .I3(alu_control[2]),
        .I4(srcB[31]),
        .I5(alu_control[0]),
        .O(alu_result[31]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[3]_INST_0 
       (.I0(out2__93[3]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[3]),
        .I4(srcB[3]),
        .I5(alu_control[2]),
        .O(alu_result[3]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[4]_INST_0 
       (.I0(out2__93[4]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[4]),
        .I4(srcB[4]),
        .I5(alu_control[2]),
        .O(alu_result[4]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[5]_INST_0 
       (.I0(out2__93[5]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[5]),
        .I4(srcB[5]),
        .I5(alu_control[2]),
        .O(alu_result[5]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[6]_INST_0 
       (.I0(out2__93[6]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[6]),
        .I4(srcB[6]),
        .I5(alu_control[2]),
        .O(alu_result[6]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[7]_INST_0 
       (.I0(out2__93[7]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[7]),
        .I4(srcB[7]),
        .I5(alu_control[2]),
        .O(alu_result[7]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[8]_INST_0 
       (.I0(out2__93[8]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[8]),
        .I4(srcB[8]),
        .I5(alu_control[2]),
        .O(alu_result[8]));
  LUT6 #(
    .INIT(64'h38083B383B383808)) 
    \alu_result[9]_INST_0 
       (.I0(out2__93[9]),
        .I1(alu_control[1]),
        .I2(alu_control[0]),
        .I3(srcA[9]),
        .I4(srcB[9]),
        .I5(alu_control[2]),
        .O(alu_result[9]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry
       (.CI(1'b0),
        .CO({out2_carry_n_0,out2_carry_n_1,out2_carry_n_2,out2_carry_n_3}),
        .CYINIT(srcA[0]),
        .DI({srcA[3:1],alu_control[2]}),
        .O(out2__93[3:0]),
        .S({out2_carry_i_1__0_n_0,out2_carry_i_2_n_0,out2_carry_i_3_n_0,out2_carry_i_4__6_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry__0
       (.CI(out2_carry_n_0),
        .CO({out2_carry__0_n_0,out2_carry__0_n_1,out2_carry__0_n_2,out2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(srcA[7:4]),
        .O(out2__93[7:4]),
        .S({out2_carry_i_1__1_n_0,out2_carry_i_2__0_n_0,out2_carry_i_3__0_n_0,out2_carry_i_4_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry__1
       (.CI(out2_carry__0_n_0),
        .CO({out2_carry__1_n_0,out2_carry__1_n_1,out2_carry__1_n_2,out2_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(srcA[11:8]),
        .O(out2__93[11:8]),
        .S({out2_carry_i_1__2_n_0,out2_carry_i_2__1_n_0,out2_carry_i_3__1_n_0,out2_carry_i_4__0_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry__2
       (.CI(out2_carry__1_n_0),
        .CO({out2_carry__2_n_0,out2_carry__2_n_1,out2_carry__2_n_2,out2_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(srcA[15:12]),
        .O(out2__93[15:12]),
        .S({out2_carry_i_1__3_n_0,out2_carry_i_2__2_n_0,out2_carry_i_3__2_n_0,out2_carry_i_4__1_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry__3
       (.CI(out2_carry__2_n_0),
        .CO({out2_carry__3_n_0,out2_carry__3_n_1,out2_carry__3_n_2,out2_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(srcA[19:16]),
        .O(out2__93[19:16]),
        .S({out2_carry_i_1__4_n_0,out2_carry_i_2__3_n_0,out2_carry_i_3__3_n_0,out2_carry_i_4__2_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry__4
       (.CI(out2_carry__3_n_0),
        .CO({out2_carry__4_n_0,out2_carry__4_n_1,out2_carry__4_n_2,out2_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(srcA[23:20]),
        .O(out2__93[23:20]),
        .S({out2_carry_i_1__5_n_0,out2_carry_i_2__4_n_0,out2_carry_i_3__4_n_0,out2_carry_i_4__3_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry__5
       (.CI(out2_carry__4_n_0),
        .CO({out2_carry__5_n_0,out2_carry__5_n_1,out2_carry__5_n_2,out2_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(srcA[27:24]),
        .O(out2__93[27:24]),
        .S({out2_carry_i_1__6_n_0,out2_carry_i_2__5_n_0,out2_carry_i_3__5_n_0,out2_carry_i_4__4_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 out2_carry__6
       (.CI(out2_carry__5_n_0),
        .CO({NLW_out2_carry__6_CO_UNCONNECTED[3],out2_carry__6_n_1,out2_carry__6_n_2,out2_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,srcA[30:28]}),
        .O(out2__93[31:28]),
        .S({out2_carry_i_1_n_0,out2_carry_i_2__6_n_0,out2_carry_i_3__6_n_0,out2_carry_i_4__5_n_0}));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1
       (.I0(srcA[31]),
        .I1(srcB[31]),
        .I2(alu_control[2]),
        .O(out2_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1__0
       (.I0(srcB[3]),
        .I1(alu_control[2]),
        .I2(srcA[3]),
        .O(out2_carry_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1__1
       (.I0(srcB[7]),
        .I1(alu_control[2]),
        .I2(srcA[7]),
        .O(out2_carry_i_1__1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1__2
       (.I0(srcB[11]),
        .I1(alu_control[2]),
        .I2(srcA[11]),
        .O(out2_carry_i_1__2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1__3
       (.I0(srcB[15]),
        .I1(alu_control[2]),
        .I2(srcA[15]),
        .O(out2_carry_i_1__3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1__4
       (.I0(srcB[19]),
        .I1(alu_control[2]),
        .I2(srcA[19]),
        .O(out2_carry_i_1__4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1__5
       (.I0(srcB[23]),
        .I1(alu_control[2]),
        .I2(srcA[23]),
        .O(out2_carry_i_1__5_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_1__6
       (.I0(srcB[27]),
        .I1(alu_control[2]),
        .I2(srcA[27]),
        .O(out2_carry_i_1__6_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2
       (.I0(srcB[2]),
        .I1(alu_control[2]),
        .I2(srcA[2]),
        .O(out2_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2__0
       (.I0(srcB[6]),
        .I1(alu_control[2]),
        .I2(srcA[6]),
        .O(out2_carry_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2__1
       (.I0(srcB[10]),
        .I1(alu_control[2]),
        .I2(srcA[10]),
        .O(out2_carry_i_2__1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2__2
       (.I0(srcB[14]),
        .I1(alu_control[2]),
        .I2(srcA[14]),
        .O(out2_carry_i_2__2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2__3
       (.I0(srcB[18]),
        .I1(alu_control[2]),
        .I2(srcA[18]),
        .O(out2_carry_i_2__3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2__4
       (.I0(srcB[22]),
        .I1(alu_control[2]),
        .I2(srcA[22]),
        .O(out2_carry_i_2__4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2__5
       (.I0(srcB[26]),
        .I1(alu_control[2]),
        .I2(srcA[26]),
        .O(out2_carry_i_2__5_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_2__6
       (.I0(srcB[30]),
        .I1(alu_control[2]),
        .I2(srcA[30]),
        .O(out2_carry_i_2__6_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3
       (.I0(srcB[1]),
        .I1(alu_control[2]),
        .I2(srcA[1]),
        .O(out2_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3__0
       (.I0(srcB[5]),
        .I1(alu_control[2]),
        .I2(srcA[5]),
        .O(out2_carry_i_3__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3__1
       (.I0(srcB[9]),
        .I1(alu_control[2]),
        .I2(srcA[9]),
        .O(out2_carry_i_3__1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3__2
       (.I0(srcB[13]),
        .I1(alu_control[2]),
        .I2(srcA[13]),
        .O(out2_carry_i_3__2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3__3
       (.I0(srcB[17]),
        .I1(alu_control[2]),
        .I2(srcA[17]),
        .O(out2_carry_i_3__3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3__4
       (.I0(srcB[21]),
        .I1(alu_control[2]),
        .I2(srcA[21]),
        .O(out2_carry_i_3__4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3__5
       (.I0(srcB[25]),
        .I1(alu_control[2]),
        .I2(srcA[25]),
        .O(out2_carry_i_3__5_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_3__6
       (.I0(srcB[29]),
        .I1(alu_control[2]),
        .I2(srcA[29]),
        .O(out2_carry_i_3__6_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_4
       (.I0(srcB[4]),
        .I1(alu_control[2]),
        .I2(srcA[4]),
        .O(out2_carry_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_4__0
       (.I0(srcB[8]),
        .I1(alu_control[2]),
        .I2(srcA[8]),
        .O(out2_carry_i_4__0_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_4__1
       (.I0(srcB[12]),
        .I1(alu_control[2]),
        .I2(srcA[12]),
        .O(out2_carry_i_4__1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_4__2
       (.I0(srcB[16]),
        .I1(alu_control[2]),
        .I2(srcA[16]),
        .O(out2_carry_i_4__2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_4__3
       (.I0(srcB[20]),
        .I1(alu_control[2]),
        .I2(srcA[20]),
        .O(out2_carry_i_4__3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_4__4
       (.I0(srcB[24]),
        .I1(alu_control[2]),
        .I2(srcA[24]),
        .O(out2_carry_i_4__4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    out2_carry_i_4__5
       (.I0(srcB[28]),
        .I1(alu_control[2]),
        .I2(srcA[28]),
        .O(out2_carry_i_4__5_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    out2_carry_i_4__6
       (.I0(srcB[0]),
        .O(out2_carry_i_4__6_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    zero_INST_0
       (.I0(zero_INST_0_i_1_n_0),
        .I1(zero_INST_0_i_2_n_0),
        .I2(zero_INST_0_i_3_n_0),
        .I3(zero_INST_0_i_4_n_0),
        .I4(zero_INST_0_i_5_n_0),
        .I5(zero_INST_0_i_6_n_0),
        .O(zero));
  LUT2 #(
    .INIT(4'h1)) 
    zero_INST_0_i_1
       (.I0(out2__93[0]),
        .I1(out2__93[1]),
        .O(zero_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    zero_INST_0_i_2
       (.I0(out2__93[4]),
        .I1(out2__93[5]),
        .I2(out2__93[2]),
        .I3(out2__93[3]),
        .I4(out2__93[7]),
        .I5(out2__93[6]),
        .O(zero_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    zero_INST_0_i_3
       (.I0(out2__93[10]),
        .I1(out2__93[11]),
        .I2(out2__93[8]),
        .I3(out2__93[9]),
        .I4(out2__93[13]),
        .I5(out2__93[12]),
        .O(zero_INST_0_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    zero_INST_0_i_4
       (.I0(out2__93[16]),
        .I1(out2__93[17]),
        .I2(out2__93[14]),
        .I3(out2__93[15]),
        .I4(out2__93[19]),
        .I5(out2__93[18]),
        .O(zero_INST_0_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    zero_INST_0_i_5
       (.I0(out2__93[22]),
        .I1(out2__93[23]),
        .I2(out2__93[20]),
        .I3(out2__93[21]),
        .I4(out2__93[25]),
        .I5(out2__93[24]),
        .O(zero_INST_0_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    zero_INST_0_i_6
       (.I0(out2__93[28]),
        .I1(out2__93[29]),
        .I2(out2__93[26]),
        .I3(out2__93[27]),
        .I4(out2__93[31]),
        .I5(out2__93[30]),
        .O(zero_INST_0_i_6_n_0));
endmodule

(* CHECK_LICENSE_TYPE = "design_3_alu_0_0,alu,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "alu,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (alu_control,
    srcA,
    srcB,
    alu_result,
    zero);
  input [2:0]alu_control;
  input [31:0]srcA;
  input [31:0]srcB;
  output [31:0]alu_result;
  output zero;

  wire [2:0]alu_control;
  wire [31:0]alu_result;
  wire \alu_result[0]_INST_0_i_1_n_0 ;
  wire [31:0]srcA;
  wire [31:0]srcB;
  wire zero;

  LUT2 #(
    .INIT(4'h6)) 
    \alu_result[0]_INST_0_i_1 
       (.I0(alu_control[2]),
        .I1(srcB[0]),
        .O(\alu_result[0]_INST_0_i_1_n_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_alu inst
       (.alu_control(alu_control),
        .alu_result(alu_result),
        .alu_result_0_sp_1(\alu_result[0]_INST_0_i_1_n_0 ),
        .srcA(srcA),
        .srcB(srcB),
        .zero(zero));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
