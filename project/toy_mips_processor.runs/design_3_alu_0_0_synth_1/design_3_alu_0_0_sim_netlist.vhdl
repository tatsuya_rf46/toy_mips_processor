-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:02:39 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_alu_0_0_sim_netlist.vhdl
-- Design      : design_3_alu_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_alu is
  port (
    alu_result : out STD_LOGIC_VECTOR ( 31 downto 0 );
    zero : out STD_LOGIC;
    srcA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    srcB : in STD_LOGIC_VECTOR ( 31 downto 0 );
    alu_control : in STD_LOGIC_VECTOR ( 2 downto 0 );
    alu_result_0_sp_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_alu;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_alu is
  signal alu_result_0_sn_1 : STD_LOGIC;
  signal \out2__93\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \out2_carry__0_n_0\ : STD_LOGIC;
  signal \out2_carry__0_n_1\ : STD_LOGIC;
  signal \out2_carry__0_n_2\ : STD_LOGIC;
  signal \out2_carry__0_n_3\ : STD_LOGIC;
  signal \out2_carry__1_n_0\ : STD_LOGIC;
  signal \out2_carry__1_n_1\ : STD_LOGIC;
  signal \out2_carry__1_n_2\ : STD_LOGIC;
  signal \out2_carry__1_n_3\ : STD_LOGIC;
  signal \out2_carry__2_n_0\ : STD_LOGIC;
  signal \out2_carry__2_n_1\ : STD_LOGIC;
  signal \out2_carry__2_n_2\ : STD_LOGIC;
  signal \out2_carry__2_n_3\ : STD_LOGIC;
  signal \out2_carry__3_n_0\ : STD_LOGIC;
  signal \out2_carry__3_n_1\ : STD_LOGIC;
  signal \out2_carry__3_n_2\ : STD_LOGIC;
  signal \out2_carry__3_n_3\ : STD_LOGIC;
  signal \out2_carry__4_n_0\ : STD_LOGIC;
  signal \out2_carry__4_n_1\ : STD_LOGIC;
  signal \out2_carry__4_n_2\ : STD_LOGIC;
  signal \out2_carry__4_n_3\ : STD_LOGIC;
  signal \out2_carry__5_n_0\ : STD_LOGIC;
  signal \out2_carry__5_n_1\ : STD_LOGIC;
  signal \out2_carry__5_n_2\ : STD_LOGIC;
  signal \out2_carry__5_n_3\ : STD_LOGIC;
  signal \out2_carry__6_n_1\ : STD_LOGIC;
  signal \out2_carry__6_n_2\ : STD_LOGIC;
  signal \out2_carry__6_n_3\ : STD_LOGIC;
  signal \out2_carry_i_1__0_n_0\ : STD_LOGIC;
  signal \out2_carry_i_1__1_n_0\ : STD_LOGIC;
  signal \out2_carry_i_1__2_n_0\ : STD_LOGIC;
  signal \out2_carry_i_1__3_n_0\ : STD_LOGIC;
  signal \out2_carry_i_1__4_n_0\ : STD_LOGIC;
  signal \out2_carry_i_1__5_n_0\ : STD_LOGIC;
  signal \out2_carry_i_1__6_n_0\ : STD_LOGIC;
  signal out2_carry_i_1_n_0 : STD_LOGIC;
  signal \out2_carry_i_2__0_n_0\ : STD_LOGIC;
  signal \out2_carry_i_2__1_n_0\ : STD_LOGIC;
  signal \out2_carry_i_2__2_n_0\ : STD_LOGIC;
  signal \out2_carry_i_2__3_n_0\ : STD_LOGIC;
  signal \out2_carry_i_2__4_n_0\ : STD_LOGIC;
  signal \out2_carry_i_2__5_n_0\ : STD_LOGIC;
  signal \out2_carry_i_2__6_n_0\ : STD_LOGIC;
  signal out2_carry_i_2_n_0 : STD_LOGIC;
  signal \out2_carry_i_3__0_n_0\ : STD_LOGIC;
  signal \out2_carry_i_3__1_n_0\ : STD_LOGIC;
  signal \out2_carry_i_3__2_n_0\ : STD_LOGIC;
  signal \out2_carry_i_3__3_n_0\ : STD_LOGIC;
  signal \out2_carry_i_3__4_n_0\ : STD_LOGIC;
  signal \out2_carry_i_3__5_n_0\ : STD_LOGIC;
  signal \out2_carry_i_3__6_n_0\ : STD_LOGIC;
  signal out2_carry_i_3_n_0 : STD_LOGIC;
  signal \out2_carry_i_4__0_n_0\ : STD_LOGIC;
  signal \out2_carry_i_4__1_n_0\ : STD_LOGIC;
  signal \out2_carry_i_4__2_n_0\ : STD_LOGIC;
  signal \out2_carry_i_4__3_n_0\ : STD_LOGIC;
  signal \out2_carry_i_4__4_n_0\ : STD_LOGIC;
  signal \out2_carry_i_4__5_n_0\ : STD_LOGIC;
  signal \out2_carry_i_4__6_n_0\ : STD_LOGIC;
  signal out2_carry_i_4_n_0 : STD_LOGIC;
  signal out2_carry_n_0 : STD_LOGIC;
  signal out2_carry_n_1 : STD_LOGIC;
  signal out2_carry_n_2 : STD_LOGIC;
  signal out2_carry_n_3 : STD_LOGIC;
  signal zero_INST_0_i_1_n_0 : STD_LOGIC;
  signal zero_INST_0_i_2_n_0 : STD_LOGIC;
  signal zero_INST_0_i_3_n_0 : STD_LOGIC;
  signal zero_INST_0_i_4_n_0 : STD_LOGIC;
  signal zero_INST_0_i_5_n_0 : STD_LOGIC;
  signal zero_INST_0_i_6_n_0 : STD_LOGIC;
  signal \NLW_out2_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of out2_carry : label is 35;
  attribute ADDER_THRESHOLD of \out2_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \out2_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \out2_carry__2\ : label is 35;
  attribute ADDER_THRESHOLD of \out2_carry__3\ : label is 35;
  attribute ADDER_THRESHOLD of \out2_carry__4\ : label is 35;
  attribute ADDER_THRESHOLD of \out2_carry__5\ : label is 35;
  attribute ADDER_THRESHOLD of \out2_carry__6\ : label is 35;
begin
  alu_result_0_sn_1 <= alu_result_0_sp_1;
\alu_result[0]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CACACACAFFF0F000"
    )
        port map (
      I0 => \out2__93\(0),
      I1 => \out2__93\(31),
      I2 => alu_control(0),
      I3 => alu_result_0_sn_1,
      I4 => srcA(0),
      I5 => alu_control(1),
      O => alu_result(0)
    );
\alu_result[10]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(10),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(10),
      I4 => srcB(10),
      I5 => alu_control(2),
      O => alu_result(10)
    );
\alu_result[11]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(11),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(11),
      I4 => srcB(11),
      I5 => alu_control(2),
      O => alu_result(11)
    );
\alu_result[12]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(12),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(12),
      I4 => srcB(12),
      I5 => alu_control(2),
      O => alu_result(12)
    );
\alu_result[13]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(13),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(13),
      I4 => srcB(13),
      I5 => alu_control(2),
      O => alu_result(13)
    );
\alu_result[14]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(14),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(14),
      I4 => srcB(14),
      I5 => alu_control(2),
      O => alu_result(14)
    );
\alu_result[15]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(15),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(15),
      I4 => srcB(15),
      I5 => alu_control(2),
      O => alu_result(15)
    );
\alu_result[16]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(16),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(16),
      I4 => srcB(16),
      I5 => alu_control(2),
      O => alu_result(16)
    );
\alu_result[17]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(17),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(17),
      I4 => srcB(17),
      I5 => alu_control(2),
      O => alu_result(17)
    );
\alu_result[18]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(18),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(18),
      I4 => srcB(18),
      I5 => alu_control(2),
      O => alu_result(18)
    );
\alu_result[19]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(19),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(19),
      I4 => srcB(19),
      I5 => alu_control(2),
      O => alu_result(19)
    );
\alu_result[1]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(1),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(1),
      I4 => srcB(1),
      I5 => alu_control(2),
      O => alu_result(1)
    );
\alu_result[20]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(20),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(20),
      I4 => srcB(20),
      I5 => alu_control(2),
      O => alu_result(20)
    );
\alu_result[21]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(21),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(21),
      I4 => srcB(21),
      I5 => alu_control(2),
      O => alu_result(21)
    );
\alu_result[22]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(22),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(22),
      I4 => srcB(22),
      I5 => alu_control(2),
      O => alu_result(22)
    );
\alu_result[23]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(23),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(23),
      I4 => srcB(23),
      I5 => alu_control(2),
      O => alu_result(23)
    );
\alu_result[24]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(24),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(24),
      I4 => srcB(24),
      I5 => alu_control(2),
      O => alu_result(24)
    );
\alu_result[25]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(25),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(25),
      I4 => srcB(25),
      I5 => alu_control(2),
      O => alu_result(25)
    );
\alu_result[26]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(26),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(26),
      I4 => srcB(26),
      I5 => alu_control(2),
      O => alu_result(26)
    );
\alu_result[27]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(27),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(27),
      I4 => srcB(27),
      I5 => alu_control(2),
      O => alu_result(27)
    );
\alu_result[28]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(28),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(28),
      I4 => srcB(28),
      I5 => alu_control(2),
      O => alu_result(28)
    );
\alu_result[29]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(29),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(29),
      I4 => srcB(29),
      I5 => alu_control(2),
      O => alu_result(29)
    );
\alu_result[2]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(2),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(2),
      I4 => srcB(2),
      I5 => alu_control(2),
      O => alu_result(2)
    );
\alu_result[30]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(30),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(30),
      I4 => srcB(30),
      I5 => alu_control(2),
      O => alu_result(30)
    );
\alu_result[31]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3033333088B8B888"
    )
        port map (
      I0 => \out2__93\(31),
      I1 => alu_control(1),
      I2 => srcA(31),
      I3 => alu_control(2),
      I4 => srcB(31),
      I5 => alu_control(0),
      O => alu_result(31)
    );
\alu_result[3]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(3),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(3),
      I4 => srcB(3),
      I5 => alu_control(2),
      O => alu_result(3)
    );
\alu_result[4]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(4),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(4),
      I4 => srcB(4),
      I5 => alu_control(2),
      O => alu_result(4)
    );
\alu_result[5]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(5),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(5),
      I4 => srcB(5),
      I5 => alu_control(2),
      O => alu_result(5)
    );
\alu_result[6]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(6),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(6),
      I4 => srcB(6),
      I5 => alu_control(2),
      O => alu_result(6)
    );
\alu_result[7]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(7),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(7),
      I4 => srcB(7),
      I5 => alu_control(2),
      O => alu_result(7)
    );
\alu_result[8]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(8),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(8),
      I4 => srcB(8),
      I5 => alu_control(2),
      O => alu_result(8)
    );
\alu_result[9]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"38083B383B383808"
    )
        port map (
      I0 => \out2__93\(9),
      I1 => alu_control(1),
      I2 => alu_control(0),
      I3 => srcA(9),
      I4 => srcB(9),
      I5 => alu_control(2),
      O => alu_result(9)
    );
out2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => out2_carry_n_0,
      CO(2) => out2_carry_n_1,
      CO(1) => out2_carry_n_2,
      CO(0) => out2_carry_n_3,
      CYINIT => srcA(0),
      DI(3 downto 1) => srcA(3 downto 1),
      DI(0) => alu_control(2),
      O(3 downto 0) => \out2__93\(3 downto 0),
      S(3) => \out2_carry_i_1__0_n_0\,
      S(2) => out2_carry_i_2_n_0,
      S(1) => out2_carry_i_3_n_0,
      S(0) => \out2_carry_i_4__6_n_0\
    );
\out2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => out2_carry_n_0,
      CO(3) => \out2_carry__0_n_0\,
      CO(2) => \out2_carry__0_n_1\,
      CO(1) => \out2_carry__0_n_2\,
      CO(0) => \out2_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => srcA(7 downto 4),
      O(3 downto 0) => \out2__93\(7 downto 4),
      S(3) => \out2_carry_i_1__1_n_0\,
      S(2) => \out2_carry_i_2__0_n_0\,
      S(1) => \out2_carry_i_3__0_n_0\,
      S(0) => out2_carry_i_4_n_0
    );
\out2_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \out2_carry__0_n_0\,
      CO(3) => \out2_carry__1_n_0\,
      CO(2) => \out2_carry__1_n_1\,
      CO(1) => \out2_carry__1_n_2\,
      CO(0) => \out2_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => srcA(11 downto 8),
      O(3 downto 0) => \out2__93\(11 downto 8),
      S(3) => \out2_carry_i_1__2_n_0\,
      S(2) => \out2_carry_i_2__1_n_0\,
      S(1) => \out2_carry_i_3__1_n_0\,
      S(0) => \out2_carry_i_4__0_n_0\
    );
\out2_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \out2_carry__1_n_0\,
      CO(3) => \out2_carry__2_n_0\,
      CO(2) => \out2_carry__2_n_1\,
      CO(1) => \out2_carry__2_n_2\,
      CO(0) => \out2_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => srcA(15 downto 12),
      O(3 downto 0) => \out2__93\(15 downto 12),
      S(3) => \out2_carry_i_1__3_n_0\,
      S(2) => \out2_carry_i_2__2_n_0\,
      S(1) => \out2_carry_i_3__2_n_0\,
      S(0) => \out2_carry_i_4__1_n_0\
    );
\out2_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \out2_carry__2_n_0\,
      CO(3) => \out2_carry__3_n_0\,
      CO(2) => \out2_carry__3_n_1\,
      CO(1) => \out2_carry__3_n_2\,
      CO(0) => \out2_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => srcA(19 downto 16),
      O(3 downto 0) => \out2__93\(19 downto 16),
      S(3) => \out2_carry_i_1__4_n_0\,
      S(2) => \out2_carry_i_2__3_n_0\,
      S(1) => \out2_carry_i_3__3_n_0\,
      S(0) => \out2_carry_i_4__2_n_0\
    );
\out2_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \out2_carry__3_n_0\,
      CO(3) => \out2_carry__4_n_0\,
      CO(2) => \out2_carry__4_n_1\,
      CO(1) => \out2_carry__4_n_2\,
      CO(0) => \out2_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => srcA(23 downto 20),
      O(3 downto 0) => \out2__93\(23 downto 20),
      S(3) => \out2_carry_i_1__5_n_0\,
      S(2) => \out2_carry_i_2__4_n_0\,
      S(1) => \out2_carry_i_3__4_n_0\,
      S(0) => \out2_carry_i_4__3_n_0\
    );
\out2_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \out2_carry__4_n_0\,
      CO(3) => \out2_carry__5_n_0\,
      CO(2) => \out2_carry__5_n_1\,
      CO(1) => \out2_carry__5_n_2\,
      CO(0) => \out2_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => srcA(27 downto 24),
      O(3 downto 0) => \out2__93\(27 downto 24),
      S(3) => \out2_carry_i_1__6_n_0\,
      S(2) => \out2_carry_i_2__5_n_0\,
      S(1) => \out2_carry_i_3__5_n_0\,
      S(0) => \out2_carry_i_4__4_n_0\
    );
\out2_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \out2_carry__5_n_0\,
      CO(3) => \NLW_out2_carry__6_CO_UNCONNECTED\(3),
      CO(2) => \out2_carry__6_n_1\,
      CO(1) => \out2_carry__6_n_2\,
      CO(0) => \out2_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => srcA(30 downto 28),
      O(3 downto 0) => \out2__93\(31 downto 28),
      S(3) => out2_carry_i_1_n_0,
      S(2) => \out2_carry_i_2__6_n_0\,
      S(1) => \out2_carry_i_3__6_n_0\,
      S(0) => \out2_carry_i_4__5_n_0\
    );
out2_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcA(31),
      I1 => srcB(31),
      I2 => alu_control(2),
      O => out2_carry_i_1_n_0
    );
\out2_carry_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(3),
      I1 => alu_control(2),
      I2 => srcA(3),
      O => \out2_carry_i_1__0_n_0\
    );
\out2_carry_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(7),
      I1 => alu_control(2),
      I2 => srcA(7),
      O => \out2_carry_i_1__1_n_0\
    );
\out2_carry_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(11),
      I1 => alu_control(2),
      I2 => srcA(11),
      O => \out2_carry_i_1__2_n_0\
    );
\out2_carry_i_1__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(15),
      I1 => alu_control(2),
      I2 => srcA(15),
      O => \out2_carry_i_1__3_n_0\
    );
\out2_carry_i_1__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(19),
      I1 => alu_control(2),
      I2 => srcA(19),
      O => \out2_carry_i_1__4_n_0\
    );
\out2_carry_i_1__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(23),
      I1 => alu_control(2),
      I2 => srcA(23),
      O => \out2_carry_i_1__5_n_0\
    );
\out2_carry_i_1__6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(27),
      I1 => alu_control(2),
      I2 => srcA(27),
      O => \out2_carry_i_1__6_n_0\
    );
out2_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(2),
      I1 => alu_control(2),
      I2 => srcA(2),
      O => out2_carry_i_2_n_0
    );
\out2_carry_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(6),
      I1 => alu_control(2),
      I2 => srcA(6),
      O => \out2_carry_i_2__0_n_0\
    );
\out2_carry_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(10),
      I1 => alu_control(2),
      I2 => srcA(10),
      O => \out2_carry_i_2__1_n_0\
    );
\out2_carry_i_2__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(14),
      I1 => alu_control(2),
      I2 => srcA(14),
      O => \out2_carry_i_2__2_n_0\
    );
\out2_carry_i_2__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(18),
      I1 => alu_control(2),
      I2 => srcA(18),
      O => \out2_carry_i_2__3_n_0\
    );
\out2_carry_i_2__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(22),
      I1 => alu_control(2),
      I2 => srcA(22),
      O => \out2_carry_i_2__4_n_0\
    );
\out2_carry_i_2__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(26),
      I1 => alu_control(2),
      I2 => srcA(26),
      O => \out2_carry_i_2__5_n_0\
    );
\out2_carry_i_2__6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(30),
      I1 => alu_control(2),
      I2 => srcA(30),
      O => \out2_carry_i_2__6_n_0\
    );
out2_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(1),
      I1 => alu_control(2),
      I2 => srcA(1),
      O => out2_carry_i_3_n_0
    );
\out2_carry_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(5),
      I1 => alu_control(2),
      I2 => srcA(5),
      O => \out2_carry_i_3__0_n_0\
    );
\out2_carry_i_3__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(9),
      I1 => alu_control(2),
      I2 => srcA(9),
      O => \out2_carry_i_3__1_n_0\
    );
\out2_carry_i_3__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(13),
      I1 => alu_control(2),
      I2 => srcA(13),
      O => \out2_carry_i_3__2_n_0\
    );
\out2_carry_i_3__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(17),
      I1 => alu_control(2),
      I2 => srcA(17),
      O => \out2_carry_i_3__3_n_0\
    );
\out2_carry_i_3__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(21),
      I1 => alu_control(2),
      I2 => srcA(21),
      O => \out2_carry_i_3__4_n_0\
    );
\out2_carry_i_3__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(25),
      I1 => alu_control(2),
      I2 => srcA(25),
      O => \out2_carry_i_3__5_n_0\
    );
\out2_carry_i_3__6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(29),
      I1 => alu_control(2),
      I2 => srcA(29),
      O => \out2_carry_i_3__6_n_0\
    );
out2_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(4),
      I1 => alu_control(2),
      I2 => srcA(4),
      O => out2_carry_i_4_n_0
    );
\out2_carry_i_4__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(8),
      I1 => alu_control(2),
      I2 => srcA(8),
      O => \out2_carry_i_4__0_n_0\
    );
\out2_carry_i_4__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(12),
      I1 => alu_control(2),
      I2 => srcA(12),
      O => \out2_carry_i_4__1_n_0\
    );
\out2_carry_i_4__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(16),
      I1 => alu_control(2),
      I2 => srcA(16),
      O => \out2_carry_i_4__2_n_0\
    );
\out2_carry_i_4__3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(20),
      I1 => alu_control(2),
      I2 => srcA(20),
      O => \out2_carry_i_4__3_n_0\
    );
\out2_carry_i_4__4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(24),
      I1 => alu_control(2),
      I2 => srcA(24),
      O => \out2_carry_i_4__4_n_0\
    );
\out2_carry_i_4__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => srcB(28),
      I1 => alu_control(2),
      I2 => srcA(28),
      O => \out2_carry_i_4__5_n_0\
    );
\out2_carry_i_4__6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => srcB(0),
      O => \out2_carry_i_4__6_n_0\
    );
zero_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => zero_INST_0_i_1_n_0,
      I1 => zero_INST_0_i_2_n_0,
      I2 => zero_INST_0_i_3_n_0,
      I3 => zero_INST_0_i_4_n_0,
      I4 => zero_INST_0_i_5_n_0,
      I5 => zero_INST_0_i_6_n_0,
      O => zero
    );
zero_INST_0_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \out2__93\(0),
      I1 => \out2__93\(1),
      O => zero_INST_0_i_1_n_0
    );
zero_INST_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \out2__93\(4),
      I1 => \out2__93\(5),
      I2 => \out2__93\(2),
      I3 => \out2__93\(3),
      I4 => \out2__93\(7),
      I5 => \out2__93\(6),
      O => zero_INST_0_i_2_n_0
    );
zero_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \out2__93\(10),
      I1 => \out2__93\(11),
      I2 => \out2__93\(8),
      I3 => \out2__93\(9),
      I4 => \out2__93\(13),
      I5 => \out2__93\(12),
      O => zero_INST_0_i_3_n_0
    );
zero_INST_0_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \out2__93\(16),
      I1 => \out2__93\(17),
      I2 => \out2__93\(14),
      I3 => \out2__93\(15),
      I4 => \out2__93\(19),
      I5 => \out2__93\(18),
      O => zero_INST_0_i_4_n_0
    );
zero_INST_0_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \out2__93\(22),
      I1 => \out2__93\(23),
      I2 => \out2__93\(20),
      I3 => \out2__93\(21),
      I4 => \out2__93\(25),
      I5 => \out2__93\(24),
      O => zero_INST_0_i_5_n_0
    );
zero_INST_0_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \out2__93\(28),
      I1 => \out2__93\(29),
      I2 => \out2__93\(26),
      I3 => \out2__93\(27),
      I4 => \out2__93\(31),
      I5 => \out2__93\(30),
      O => zero_INST_0_i_6_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    alu_control : in STD_LOGIC_VECTOR ( 2 downto 0 );
    srcA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    srcB : in STD_LOGIC_VECTOR ( 31 downto 0 );
    alu_result : out STD_LOGIC_VECTOR ( 31 downto 0 );
    zero : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_alu_0_0,alu,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "alu,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \alu_result[0]_INST_0_i_1_n_0\ : STD_LOGIC;
begin
\alu_result[0]_INST_0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => alu_control(2),
      I1 => srcB(0),
      O => \alu_result[0]_INST_0_i_1_n_0\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_alu
     port map (
      alu_control(2 downto 0) => alu_control(2 downto 0),
      alu_result(31 downto 0) => alu_result(31 downto 0),
      alu_result_0_sp_1 => \alu_result[0]_INST_0_i_1_n_0\,
      srcA(31 downto 0) => srcA(31 downto 0),
      srcB(31 downto 0) => srcB(31 downto 0),
      zero => zero
    );
end STRUCTURE;
