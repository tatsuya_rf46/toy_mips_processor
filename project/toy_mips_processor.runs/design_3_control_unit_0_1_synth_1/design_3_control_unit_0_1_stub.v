// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:03:55 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_control_unit_0_1_stub.v
// Design      : design_3_control_unit_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "control_unit,Vivado 2020.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(Op, Funct, RegtoPC, Jump, LeaveLink, RegWrite, 
  MemtoReg, MemWrite, ALUControl, ALUSrc, RegDst, Branch, ToggleEqual)
/* synthesis syn_black_box black_box_pad_pin="Op[5:0],Funct[5:0],RegtoPC,Jump,LeaveLink,RegWrite,MemtoReg,MemWrite,ALUControl[2:0],ALUSrc,RegDst,Branch,ToggleEqual" */;
  input [5:0]Op;
  input [5:0]Funct;
  output RegtoPC;
  output Jump;
  output LeaveLink;
  output RegWrite;
  output MemtoReg;
  output MemWrite;
  output [2:0]ALUControl;
  output ALUSrc;
  output RegDst;
  output Branch;
  output ToggleEqual;
endmodule
