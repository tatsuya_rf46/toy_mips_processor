-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:03:55 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_control_unit_0_1_stub.vhdl
-- Design      : design_3_control_unit_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    Op : in STD_LOGIC_VECTOR ( 5 downto 0 );
    Funct : in STD_LOGIC_VECTOR ( 5 downto 0 );
    RegtoPC : out STD_LOGIC;
    Jump : out STD_LOGIC;
    LeaveLink : out STD_LOGIC;
    RegWrite : out STD_LOGIC;
    MemtoReg : out STD_LOGIC;
    MemWrite : out STD_LOGIC;
    ALUControl : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ALUSrc : out STD_LOGIC;
    RegDst : out STD_LOGIC;
    Branch : out STD_LOGIC;
    ToggleEqual : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "Op[5:0],Funct[5:0],RegtoPC,Jump,LeaveLink,RegWrite,MemtoReg,MemWrite,ALUControl[2:0],ALUSrc,RegDst,Branch,ToggleEqual";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "control_unit,Vivado 2020.1";
begin
end;
