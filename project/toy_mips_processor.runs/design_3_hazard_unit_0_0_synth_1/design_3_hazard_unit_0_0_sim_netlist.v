// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:16 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_hazard_unit_0_0_sim_netlist.v
// Design      : design_3_hazard_unit_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_hazard_unit_0_0,hazard_unit,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "hazard_unit,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (BranchD,
    RsD,
    RtD,
    RsE,
    RtE,
    WriteRegE,
    WriteRegM,
    WriteRegW,
    MemtoRegE,
    RegWriteE,
    RegWriteM,
    RegWriteW,
    RegtoPCD,
    LeavelinkW,
    LeavelinkM,
    StallF,
    StallD,
    ForwardAD,
    ForwardBD,
    FlushE,
    ForwardAE,
    ForwardBE);
  input BranchD;
  input [4:0]RsD;
  input [4:0]RtD;
  input [4:0]RsE;
  input [4:0]RtE;
  input [4:0]WriteRegE;
  input [4:0]WriteRegM;
  input [4:0]WriteRegW;
  input MemtoRegE;
  input RegWriteE;
  input RegWriteM;
  input RegWriteW;
  input RegtoPCD;
  input LeavelinkW;
  input LeavelinkM;
  output StallF;
  output StallD;
  output [1:0]ForwardAD;
  output [1:0]ForwardBD;
  output FlushE;
  output [1:0]ForwardAE;
  output [1:0]ForwardBE;

  wire BranchD;
  wire FlushE;
  wire FlushE_INST_0_i_1_n_0;
  wire FlushE_INST_0_i_2_n_0;
  wire FlushE_INST_0_i_3_n_0;
  wire FlushE_INST_0_i_4_n_0;
  wire FlushE_INST_0_i_5_n_0;
  wire [1:0]ForwardAD;
  wire ForwardAD4;
  wire \ForwardAD[1]_INST_0_i_1_n_0 ;
  wire \ForwardAD[1]_INST_0_i_4_n_0 ;
  wire [1:0]ForwardAE;
  wire \ForwardAE[0]_INST_0_i_1_n_0 ;
  wire \ForwardAE[0]_INST_0_i_2_n_0 ;
  wire \ForwardAE[1]_INST_0_i_3_n_0 ;
  wire [1:0]ForwardBD;
  wire ForwardBD4;
  wire \ForwardBD[1]_INST_0_i_1_n_0 ;
  wire [1:0]ForwardBE;
  wire \ForwardBE[0]_INST_0_i_1_n_0 ;
  wire \ForwardBE[0]_INST_0_i_2_n_0 ;
  wire \ForwardBE[1]_INST_0_i_1_n_0 ;
  wire \ForwardBE[1]_INST_0_i_3_n_0 ;
  wire LeavelinkM;
  wire LeavelinkW;
  wire MemtoRegE;
  wire RegWriteE;
  wire RegWriteM;
  wire RegWriteW;
  wire RegtoPCD;
  wire [4:0]RsD;
  wire [4:0]RsE;
  wire [4:0]RtD;
  wire [4:0]RtE;
  wire StallD_INST_0_i_10_n_0;
  wire StallD_INST_0_i_11_n_0;
  wire StallD_INST_0_i_12_n_0;
  wire StallD_INST_0_i_13_n_0;
  wire StallD_INST_0_i_14_n_0;
  wire StallD_INST_0_i_15_n_0;
  wire StallD_INST_0_i_16_n_0;
  wire StallD_INST_0_i_1_n_0;
  wire StallD_INST_0_i_2_n_0;
  wire StallD_INST_0_i_3_n_0;
  wire StallD_INST_0_i_4_n_0;
  wire StallD_INST_0_i_5_n_0;
  wire StallD_INST_0_i_7_n_0;
  wire StallD_INST_0_i_8_n_0;
  wire StallD_INST_0_i_9_n_0;
  wire StallF;
  wire [4:0]WriteRegE;
  wire [4:0]WriteRegM;
  wire [4:0]WriteRegW;
  wire \inst/ForwardAE4__3 ;
  wire \inst/ForwardAE5__8 ;
  wire \inst/ForwardBE5__8 ;
  wire \inst/branchstall20_out ;
  wire \inst/branchstall22_out ;
  wire \inst/branchstall2__8 ;

  assign StallD = StallF;
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF8)) 
    FlushE_INST_0
       (.I0(\inst/branchstall20_out ),
        .I1(FlushE_INST_0_i_1_n_0),
        .I2(FlushE_INST_0_i_2_n_0),
        .I3(StallD_INST_0_i_4_n_0),
        .I4(StallD_INST_0_i_3_n_0),
        .I5(FlushE_INST_0_i_3_n_0),
        .O(FlushE));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    FlushE_INST_0_i_1
       (.I0(RegWriteM),
        .I1(RegtoPCD),
        .O(FlushE_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    FlushE_INST_0_i_2
       (.I0(RegtoPCD),
        .I1(RegWriteE),
        .I2(FlushE_INST_0_i_4_n_0),
        .I3(StallD_INST_0_i_16_n_0),
        .O(FlushE_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'h8888800080008000)) 
    FlushE_INST_0_i_3
       (.I0(BranchD),
        .I1(RegWriteE),
        .I2(StallD_INST_0_i_16_n_0),
        .I3(FlushE_INST_0_i_4_n_0),
        .I4(StallD_INST_0_i_7_n_0),
        .I5(FlushE_INST_0_i_5_n_0),
        .O(FlushE_INST_0_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h9009)) 
    FlushE_INST_0_i_4
       (.I0(WriteRegE[0]),
        .I1(RsD[0]),
        .I2(WriteRegE[1]),
        .I3(RsD[1]),
        .O(FlushE_INST_0_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h9009)) 
    FlushE_INST_0_i_5
       (.I0(WriteRegE[0]),
        .I1(RtD[0]),
        .I2(WriteRegE[1]),
        .I3(RtD[1]),
        .O(FlushE_INST_0_i_5_n_0));
  LUT4 #(
    .INIT(16'h0800)) 
    \ForwardAD[0]_INST_0 
       (.I0(ForwardAD4),
        .I1(RegWriteM),
        .I2(LeavelinkM),
        .I3(\inst/branchstall20_out ),
        .O(ForwardAD[0]));
  LUT5 #(
    .INIT(32'hA2AAAAAA)) 
    \ForwardAD[1]_INST_0 
       (.I0(\ForwardAD[1]_INST_0_i_1_n_0 ),
        .I1(\inst/branchstall20_out ),
        .I2(LeavelinkM),
        .I3(RegWriteM),
        .I4(ForwardAD4),
        .O(ForwardAD[1]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \ForwardAD[1]_INST_0_i_1 
       (.I0(RsD[3]),
        .I1(RsD[4]),
        .I2(\ForwardAD[1]_INST_0_i_4_n_0 ),
        .I3(RsD[0]),
        .I4(RsD[1]),
        .I5(RsD[2]),
        .O(\ForwardAD[1]_INST_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h82000082)) 
    \ForwardAD[1]_INST_0_i_2 
       (.I0(StallD_INST_0_i_10_n_0),
        .I1(WriteRegM[1]),
        .I2(RsD[1]),
        .I3(WriteRegM[0]),
        .I4(RsD[0]),
        .O(\inst/branchstall20_out ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ForwardAD[1]_INST_0_i_3 
       (.I0(RsD[3]),
        .I1(RsD[4]),
        .I2(RsD[2]),
        .I3(RsD[0]),
        .I4(RsD[1]),
        .O(ForwardAD4));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \ForwardAD[1]_INST_0_i_4 
       (.I0(RegWriteM),
        .I1(LeavelinkM),
        .O(\ForwardAD[1]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9000000000000000)) 
    \ForwardAE[0]_INST_0 
       (.I0(RsE[1]),
        .I1(WriteRegW[1]),
        .I2(RegWriteW),
        .I3(\ForwardAE[0]_INST_0_i_1_n_0 ),
        .I4(\ForwardAE[0]_INST_0_i_2_n_0 ),
        .I5(\inst/ForwardAE4__3 ),
        .O(ForwardAE[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \ForwardAE[0]_INST_0_i_1 
       (.I0(RsE[2]),
        .I1(WriteRegW[2]),
        .I2(RsE[3]),
        .I3(WriteRegW[3]),
        .O(\ForwardAE[0]_INST_0_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ForwardAE[0]_INST_0_i_2 
       (.I0(RsE[4]),
        .I1(WriteRegW[4]),
        .I2(RsE[0]),
        .I3(WriteRegW[0]),
        .O(\ForwardAE[0]_INST_0_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8888888)) 
    \ForwardAE[1]_INST_0 
       (.I0(LeavelinkW),
        .I1(ForwardAE[0]),
        .I2(RegWriteM),
        .I3(\inst/ForwardAE4__3 ),
        .I4(\inst/ForwardAE5__8 ),
        .O(ForwardAE[1]));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ForwardAE[1]_INST_0_i_1 
       (.I0(RsE[4]),
        .I1(RsE[0]),
        .I2(RsE[1]),
        .I3(RsE[2]),
        .I4(RsE[3]),
        .O(\inst/ForwardAE4__3 ));
  LUT5 #(
    .INIT(32'h82000082)) 
    \ForwardAE[1]_INST_0_i_2 
       (.I0(\ForwardAE[1]_INST_0_i_3_n_0 ),
        .I1(WriteRegM[1]),
        .I2(RsE[1]),
        .I3(WriteRegM[0]),
        .I4(RsE[0]),
        .O(\inst/ForwardAE5__8 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ForwardAE[1]_INST_0_i_3 
       (.I0(WriteRegM[4]),
        .I1(RsE[4]),
        .I2(WriteRegM[3]),
        .I3(RsE[3]),
        .I4(WriteRegM[2]),
        .I5(RsE[2]),
        .O(\ForwardAE[1]_INST_0_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0800)) 
    \ForwardBD[0]_INST_0 
       (.I0(ForwardBD4),
        .I1(RegWriteM),
        .I2(LeavelinkM),
        .I3(\inst/branchstall2__8 ),
        .O(ForwardBD[0]));
  LUT5 #(
    .INIT(32'hA2AAAAAA)) 
    \ForwardBD[1]_INST_0 
       (.I0(\ForwardBD[1]_INST_0_i_1_n_0 ),
        .I1(\inst/branchstall2__8 ),
        .I2(LeavelinkM),
        .I3(RegWriteM),
        .I4(ForwardBD4),
        .O(ForwardBD[1]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \ForwardBD[1]_INST_0_i_1 
       (.I0(RtD[3]),
        .I1(RtD[4]),
        .I2(\ForwardAD[1]_INST_0_i_4_n_0 ),
        .I3(RtD[0]),
        .I4(RtD[1]),
        .I5(RtD[2]),
        .O(\ForwardBD[1]_INST_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h82000082)) 
    \ForwardBD[1]_INST_0_i_2 
       (.I0(StallD_INST_0_i_8_n_0),
        .I1(WriteRegM[1]),
        .I2(RtD[1]),
        .I3(WriteRegM[0]),
        .I4(RtD[0]),
        .O(\inst/branchstall2__8 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ForwardBD[1]_INST_0_i_3 
       (.I0(RtD[3]),
        .I1(RtD[4]),
        .I2(RtD[2]),
        .I3(RtD[0]),
        .I4(RtD[1]),
        .O(ForwardBD4));
  LUT6 #(
    .INIT(64'h9000000000000000)) 
    \ForwardBE[0]_INST_0 
       (.I0(RtE[1]),
        .I1(WriteRegW[1]),
        .I2(RegWriteW),
        .I3(\ForwardBE[0]_INST_0_i_1_n_0 ),
        .I4(\ForwardBE[0]_INST_0_i_2_n_0 ),
        .I5(\ForwardBE[1]_INST_0_i_1_n_0 ),
        .O(ForwardBE[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    \ForwardBE[0]_INST_0_i_1 
       (.I0(RtE[2]),
        .I1(WriteRegW[2]),
        .I2(RtE[3]),
        .I3(WriteRegW[3]),
        .O(\ForwardBE[0]_INST_0_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h9009)) 
    \ForwardBE[0]_INST_0_i_2 
       (.I0(RtE[4]),
        .I1(WriteRegW[4]),
        .I2(RtE[0]),
        .I3(WriteRegW[0]),
        .O(\ForwardBE[0]_INST_0_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8888888)) 
    \ForwardBE[1]_INST_0 
       (.I0(LeavelinkW),
        .I1(ForwardBE[0]),
        .I2(RegWriteM),
        .I3(\ForwardBE[1]_INST_0_i_1_n_0 ),
        .I4(\inst/ForwardBE5__8 ),
        .O(ForwardBE[1]));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \ForwardBE[1]_INST_0_i_1 
       (.I0(RtE[4]),
        .I1(RtE[0]),
        .I2(RtE[1]),
        .I3(RtE[2]),
        .I4(RtE[3]),
        .O(\ForwardBE[1]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h82000082)) 
    \ForwardBE[1]_INST_0_i_2 
       (.I0(\ForwardBE[1]_INST_0_i_3_n_0 ),
        .I1(WriteRegM[1]),
        .I2(RtE[1]),
        .I3(WriteRegM[0]),
        .I4(RtE[0]),
        .O(\inst/ForwardBE5__8 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \ForwardBE[1]_INST_0_i_3 
       (.I0(WriteRegM[4]),
        .I1(RtE[4]),
        .I2(WriteRegM[3]),
        .I3(RtE[3]),
        .I4(RtE[2]),
        .I5(WriteRegM[2]),
        .O(\ForwardBE[1]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFAFFF8FFF8)) 
    StallD_INST_0
       (.I0(StallD_INST_0_i_1_n_0),
        .I1(StallD_INST_0_i_2_n_0),
        .I2(StallD_INST_0_i_3_n_0),
        .I3(StallD_INST_0_i_4_n_0),
        .I4(StallD_INST_0_i_5_n_0),
        .I5(\inst/branchstall22_out ),
        .O(StallF));
  LUT2 #(
    .INIT(4'h8)) 
    StallD_INST_0_i_1
       (.I0(RegWriteE),
        .I1(BranchD),
        .O(StallD_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    StallD_INST_0_i_10
       (.I0(WriteRegM[4]),
        .I1(RsD[4]),
        .I2(WriteRegM[3]),
        .I3(RsD[3]),
        .I4(RsD[2]),
        .I5(WriteRegM[2]),
        .O(StallD_INST_0_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h9009)) 
    StallD_INST_0_i_11
       (.I0(RsD[0]),
        .I1(WriteRegM[0]),
        .I2(RsD[1]),
        .I3(WriteRegM[1]),
        .O(StallD_INST_0_i_11_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    StallD_INST_0_i_12
       (.I0(RtE[4]),
        .I1(RsD[4]),
        .I2(RtE[3]),
        .I3(RsD[3]),
        .I4(RsD[2]),
        .I5(RtE[2]),
        .O(StallD_INST_0_i_12_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    StallD_INST_0_i_13
       (.I0(RsD[0]),
        .I1(RtE[0]),
        .I2(RsD[1]),
        .I3(RtE[1]),
        .O(StallD_INST_0_i_13_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    StallD_INST_0_i_14
       (.I0(RtE[4]),
        .I1(RtD[4]),
        .I2(RtE[3]),
        .I3(RtD[3]),
        .I4(RtE[2]),
        .I5(RtD[2]),
        .O(StallD_INST_0_i_14_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    StallD_INST_0_i_15
       (.I0(RtD[0]),
        .I1(RtE[0]),
        .I2(RtD[1]),
        .I3(RtE[1]),
        .O(StallD_INST_0_i_15_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    StallD_INST_0_i_16
       (.I0(RsD[4]),
        .I1(WriteRegE[4]),
        .I2(RsD[3]),
        .I3(WriteRegE[3]),
        .I4(RsD[2]),
        .I5(WriteRegE[2]),
        .O(StallD_INST_0_i_16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h82000082)) 
    StallD_INST_0_i_2
       (.I0(StallD_INST_0_i_7_n_0),
        .I1(RtD[1]),
        .I2(WriteRegE[1]),
        .I3(RtD[0]),
        .I4(WriteRegE[0]),
        .O(StallD_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'h8888800080008000)) 
    StallD_INST_0_i_3
       (.I0(BranchD),
        .I1(MemtoRegE),
        .I2(StallD_INST_0_i_8_n_0),
        .I3(StallD_INST_0_i_9_n_0),
        .I4(StallD_INST_0_i_10_n_0),
        .I5(StallD_INST_0_i_11_n_0),
        .O(StallD_INST_0_i_3_n_0));
  LUT5 #(
    .INIT(32'hF8880000)) 
    StallD_INST_0_i_4
       (.I0(StallD_INST_0_i_12_n_0),
        .I1(StallD_INST_0_i_13_n_0),
        .I2(StallD_INST_0_i_14_n_0),
        .I3(StallD_INST_0_i_15_n_0),
        .I4(MemtoRegE),
        .O(StallD_INST_0_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    StallD_INST_0_i_5
       (.I0(RegWriteE),
        .I1(RegtoPCD),
        .O(StallD_INST_0_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h82000082)) 
    StallD_INST_0_i_6
       (.I0(StallD_INST_0_i_16_n_0),
        .I1(RsD[1]),
        .I2(WriteRegE[1]),
        .I3(RsD[0]),
        .I4(WriteRegE[0]),
        .O(\inst/branchstall22_out ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    StallD_INST_0_i_7
       (.I0(RtD[4]),
        .I1(WriteRegE[4]),
        .I2(RtD[3]),
        .I3(WriteRegE[3]),
        .I4(RtD[2]),
        .I5(WriteRegE[2]),
        .O(StallD_INST_0_i_7_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    StallD_INST_0_i_8
       (.I0(WriteRegM[4]),
        .I1(RtD[4]),
        .I2(WriteRegM[3]),
        .I3(RtD[3]),
        .I4(RtD[2]),
        .I5(WriteRegM[2]),
        .O(StallD_INST_0_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h9009)) 
    StallD_INST_0_i_9
       (.I0(RtD[0]),
        .I1(WriteRegM[0]),
        .I2(RtD[1]),
        .I3(WriteRegM[1]),
        .O(StallD_INST_0_i_9_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
