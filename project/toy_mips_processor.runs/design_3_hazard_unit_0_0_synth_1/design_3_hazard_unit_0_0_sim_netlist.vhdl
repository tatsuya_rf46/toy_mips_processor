-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:58:16 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_hazard_unit_0_0_sim_netlist.vhdl
-- Design      : design_3_hazard_unit_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    BranchD : in STD_LOGIC;
    RsD : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RtD : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RsE : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RtE : in STD_LOGIC_VECTOR ( 4 downto 0 );
    WriteRegE : in STD_LOGIC_VECTOR ( 4 downto 0 );
    WriteRegM : in STD_LOGIC_VECTOR ( 4 downto 0 );
    WriteRegW : in STD_LOGIC_VECTOR ( 4 downto 0 );
    MemtoRegE : in STD_LOGIC;
    RegWriteE : in STD_LOGIC;
    RegWriteM : in STD_LOGIC;
    RegWriteW : in STD_LOGIC;
    RegtoPCD : in STD_LOGIC;
    LeavelinkW : in STD_LOGIC;
    LeavelinkM : in STD_LOGIC;
    StallF : out STD_LOGIC;
    StallD : out STD_LOGIC;
    ForwardAD : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ForwardBD : out STD_LOGIC_VECTOR ( 1 downto 0 );
    FlushE : out STD_LOGIC;
    ForwardAE : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ForwardBE : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_hazard_unit_0_0,hazard_unit,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "hazard_unit,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal FlushE_INST_0_i_1_n_0 : STD_LOGIC;
  signal FlushE_INST_0_i_2_n_0 : STD_LOGIC;
  signal FlushE_INST_0_i_3_n_0 : STD_LOGIC;
  signal FlushE_INST_0_i_4_n_0 : STD_LOGIC;
  signal FlushE_INST_0_i_5_n_0 : STD_LOGIC;
  signal ForwardAD4 : STD_LOGIC;
  signal \ForwardAD[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \ForwardAD[1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \^forwardae\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \ForwardAE[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \ForwardAE[0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \ForwardAE[1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal ForwardBD4 : STD_LOGIC;
  signal \ForwardBD[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \^forwardbe\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \ForwardBE[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \ForwardBE[0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \ForwardBE[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \ForwardBE[1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal StallD_INST_0_i_10_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_11_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_12_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_13_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_14_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_15_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_16_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_1_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_2_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_3_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_4_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_5_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_7_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_8_n_0 : STD_LOGIC;
  signal StallD_INST_0_i_9_n_0 : STD_LOGIC;
  signal \^stallf\ : STD_LOGIC;
  signal \inst/ForwardAE4__3\ : STD_LOGIC;
  signal \inst/ForwardAE5__8\ : STD_LOGIC;
  signal \inst/ForwardBE5__8\ : STD_LOGIC;
  signal \inst/branchstall20_out\ : STD_LOGIC;
  signal \inst/branchstall22_out\ : STD_LOGIC;
  signal \inst/branchstall2__8\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of FlushE_INST_0_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of FlushE_INST_0_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of FlushE_INST_0_i_4 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of FlushE_INST_0_i_5 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ForwardAD[1]_INST_0_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ForwardAD[1]_INST_0_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ForwardBD[1]_INST_0_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of StallD_INST_0_i_11 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of StallD_INST_0_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of StallD_INST_0_i_5 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of StallD_INST_0_i_6 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of StallD_INST_0_i_9 : label is "soft_lutpair2";
begin
  ForwardAE(1 downto 0) <= \^forwardae\(1 downto 0);
  ForwardBE(1 downto 0) <= \^forwardbe\(1 downto 0);
  StallD <= \^stallf\;
  StallF <= \^stallf\;
FlushE_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF8"
    )
        port map (
      I0 => \inst/branchstall20_out\,
      I1 => FlushE_INST_0_i_1_n_0,
      I2 => FlushE_INST_0_i_2_n_0,
      I3 => StallD_INST_0_i_4_n_0,
      I4 => StallD_INST_0_i_3_n_0,
      I5 => FlushE_INST_0_i_3_n_0,
      O => FlushE
    );
FlushE_INST_0_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => RegWriteM,
      I1 => RegtoPCD,
      O => FlushE_INST_0_i_1_n_0
    );
FlushE_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => RegtoPCD,
      I1 => RegWriteE,
      I2 => FlushE_INST_0_i_4_n_0,
      I3 => StallD_INST_0_i_16_n_0,
      O => FlushE_INST_0_i_2_n_0
    );
FlushE_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800080008000"
    )
        port map (
      I0 => BranchD,
      I1 => RegWriteE,
      I2 => StallD_INST_0_i_16_n_0,
      I3 => FlushE_INST_0_i_4_n_0,
      I4 => StallD_INST_0_i_7_n_0,
      I5 => FlushE_INST_0_i_5_n_0,
      O => FlushE_INST_0_i_3_n_0
    );
FlushE_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => WriteRegE(0),
      I1 => RsD(0),
      I2 => WriteRegE(1),
      I3 => RsD(1),
      O => FlushE_INST_0_i_4_n_0
    );
FlushE_INST_0_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => WriteRegE(0),
      I1 => RtD(0),
      I2 => WriteRegE(1),
      I3 => RtD(1),
      O => FlushE_INST_0_i_5_n_0
    );
\ForwardAD[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => ForwardAD4,
      I1 => RegWriteM,
      I2 => LeavelinkM,
      I3 => \inst/branchstall20_out\,
      O => ForwardAD(0)
    );
\ForwardAD[1]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A2AAAAAA"
    )
        port map (
      I0 => \ForwardAD[1]_INST_0_i_1_n_0\,
      I1 => \inst/branchstall20_out\,
      I2 => LeavelinkM,
      I3 => RegWriteM,
      I4 => ForwardAD4,
      O => ForwardAD(1)
    );
\ForwardAD[1]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => RsD(3),
      I1 => RsD(4),
      I2 => \ForwardAD[1]_INST_0_i_4_n_0\,
      I3 => RsD(0),
      I4 => RsD(1),
      I5 => RsD(2),
      O => \ForwardAD[1]_INST_0_i_1_n_0\
    );
\ForwardAD[1]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82000082"
    )
        port map (
      I0 => StallD_INST_0_i_10_n_0,
      I1 => WriteRegM(1),
      I2 => RsD(1),
      I3 => WriteRegM(0),
      I4 => RsD(0),
      O => \inst/branchstall20_out\
    );
\ForwardAD[1]_INST_0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => RsD(3),
      I1 => RsD(4),
      I2 => RsD(2),
      I3 => RsD(0),
      I4 => RsD(1),
      O => ForwardAD4
    );
\ForwardAD[1]_INST_0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => RegWriteM,
      I1 => LeavelinkM,
      O => \ForwardAD[1]_INST_0_i_4_n_0\
    );
\ForwardAE[0]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9000000000000000"
    )
        port map (
      I0 => RsE(1),
      I1 => WriteRegW(1),
      I2 => RegWriteW,
      I3 => \ForwardAE[0]_INST_0_i_1_n_0\,
      I4 => \ForwardAE[0]_INST_0_i_2_n_0\,
      I5 => \inst/ForwardAE4__3\,
      O => \^forwardae\(0)
    );
\ForwardAE[0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RsE(2),
      I1 => WriteRegW(2),
      I2 => RsE(3),
      I3 => WriteRegW(3),
      O => \ForwardAE[0]_INST_0_i_1_n_0\
    );
\ForwardAE[0]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RsE(4),
      I1 => WriteRegW(4),
      I2 => RsE(0),
      I3 => WriteRegW(0),
      O => \ForwardAE[0]_INST_0_i_2_n_0\
    );
\ForwardAE[1]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8888888"
    )
        port map (
      I0 => LeavelinkW,
      I1 => \^forwardae\(0),
      I2 => RegWriteM,
      I3 => \inst/ForwardAE4__3\,
      I4 => \inst/ForwardAE5__8\,
      O => \^forwardae\(1)
    );
\ForwardAE[1]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => RsE(4),
      I1 => RsE(0),
      I2 => RsE(1),
      I3 => RsE(2),
      I4 => RsE(3),
      O => \inst/ForwardAE4__3\
    );
\ForwardAE[1]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82000082"
    )
        port map (
      I0 => \ForwardAE[1]_INST_0_i_3_n_0\,
      I1 => WriteRegM(1),
      I2 => RsE(1),
      I3 => WriteRegM(0),
      I4 => RsE(0),
      O => \inst/ForwardAE5__8\
    );
\ForwardAE[1]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => WriteRegM(4),
      I1 => RsE(4),
      I2 => WriteRegM(3),
      I3 => RsE(3),
      I4 => WriteRegM(2),
      I5 => RsE(2),
      O => \ForwardAE[1]_INST_0_i_3_n_0\
    );
\ForwardBD[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => ForwardBD4,
      I1 => RegWriteM,
      I2 => LeavelinkM,
      I3 => \inst/branchstall2__8\,
      O => ForwardBD(0)
    );
\ForwardBD[1]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A2AAAAAA"
    )
        port map (
      I0 => \ForwardBD[1]_INST_0_i_1_n_0\,
      I1 => \inst/branchstall2__8\,
      I2 => LeavelinkM,
      I3 => RegWriteM,
      I4 => ForwardBD4,
      O => ForwardBD(1)
    );
\ForwardBD[1]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => RtD(3),
      I1 => RtD(4),
      I2 => \ForwardAD[1]_INST_0_i_4_n_0\,
      I3 => RtD(0),
      I4 => RtD(1),
      I5 => RtD(2),
      O => \ForwardBD[1]_INST_0_i_1_n_0\
    );
\ForwardBD[1]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82000082"
    )
        port map (
      I0 => StallD_INST_0_i_8_n_0,
      I1 => WriteRegM(1),
      I2 => RtD(1),
      I3 => WriteRegM(0),
      I4 => RtD(0),
      O => \inst/branchstall2__8\
    );
\ForwardBD[1]_INST_0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => RtD(3),
      I1 => RtD(4),
      I2 => RtD(2),
      I3 => RtD(0),
      I4 => RtD(1),
      O => ForwardBD4
    );
\ForwardBE[0]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9000000000000000"
    )
        port map (
      I0 => RtE(1),
      I1 => WriteRegW(1),
      I2 => RegWriteW,
      I3 => \ForwardBE[0]_INST_0_i_1_n_0\,
      I4 => \ForwardBE[0]_INST_0_i_2_n_0\,
      I5 => \ForwardBE[1]_INST_0_i_1_n_0\,
      O => \^forwardbe\(0)
    );
\ForwardBE[0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RtE(2),
      I1 => WriteRegW(2),
      I2 => RtE(3),
      I3 => WriteRegW(3),
      O => \ForwardBE[0]_INST_0_i_1_n_0\
    );
\ForwardBE[0]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RtE(4),
      I1 => WriteRegW(4),
      I2 => RtE(0),
      I3 => WriteRegW(0),
      O => \ForwardBE[0]_INST_0_i_2_n_0\
    );
\ForwardBE[1]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8888888"
    )
        port map (
      I0 => LeavelinkW,
      I1 => \^forwardbe\(0),
      I2 => RegWriteM,
      I3 => \ForwardBE[1]_INST_0_i_1_n_0\,
      I4 => \inst/ForwardBE5__8\,
      O => \^forwardbe\(1)
    );
\ForwardBE[1]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => RtE(4),
      I1 => RtE(0),
      I2 => RtE(1),
      I3 => RtE(2),
      I4 => RtE(3),
      O => \ForwardBE[1]_INST_0_i_1_n_0\
    );
\ForwardBE[1]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82000082"
    )
        port map (
      I0 => \ForwardBE[1]_INST_0_i_3_n_0\,
      I1 => WriteRegM(1),
      I2 => RtE(1),
      I3 => WriteRegM(0),
      I4 => RtE(0),
      O => \inst/ForwardBE5__8\
    );
\ForwardBE[1]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => WriteRegM(4),
      I1 => RtE(4),
      I2 => WriteRegM(3),
      I3 => RtE(3),
      I4 => RtE(2),
      I5 => WriteRegM(2),
      O => \ForwardBE[1]_INST_0_i_3_n_0\
    );
StallD_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFAFFF8FFF8"
    )
        port map (
      I0 => StallD_INST_0_i_1_n_0,
      I1 => StallD_INST_0_i_2_n_0,
      I2 => StallD_INST_0_i_3_n_0,
      I3 => StallD_INST_0_i_4_n_0,
      I4 => StallD_INST_0_i_5_n_0,
      I5 => \inst/branchstall22_out\,
      O => \^stallf\
    );
StallD_INST_0_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => RegWriteE,
      I1 => BranchD,
      O => StallD_INST_0_i_1_n_0
    );
StallD_INST_0_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => WriteRegM(4),
      I1 => RsD(4),
      I2 => WriteRegM(3),
      I3 => RsD(3),
      I4 => RsD(2),
      I5 => WriteRegM(2),
      O => StallD_INST_0_i_10_n_0
    );
StallD_INST_0_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RsD(0),
      I1 => WriteRegM(0),
      I2 => RsD(1),
      I3 => WriteRegM(1),
      O => StallD_INST_0_i_11_n_0
    );
StallD_INST_0_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => RtE(4),
      I1 => RsD(4),
      I2 => RtE(3),
      I3 => RsD(3),
      I4 => RsD(2),
      I5 => RtE(2),
      O => StallD_INST_0_i_12_n_0
    );
StallD_INST_0_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RsD(0),
      I1 => RtE(0),
      I2 => RsD(1),
      I3 => RtE(1),
      O => StallD_INST_0_i_13_n_0
    );
StallD_INST_0_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => RtE(4),
      I1 => RtD(4),
      I2 => RtE(3),
      I3 => RtD(3),
      I4 => RtE(2),
      I5 => RtD(2),
      O => StallD_INST_0_i_14_n_0
    );
StallD_INST_0_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RtD(0),
      I1 => RtE(0),
      I2 => RtD(1),
      I3 => RtE(1),
      O => StallD_INST_0_i_15_n_0
    );
StallD_INST_0_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => RsD(4),
      I1 => WriteRegE(4),
      I2 => RsD(3),
      I3 => WriteRegE(3),
      I4 => RsD(2),
      I5 => WriteRegE(2),
      O => StallD_INST_0_i_16_n_0
    );
StallD_INST_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82000082"
    )
        port map (
      I0 => StallD_INST_0_i_7_n_0,
      I1 => RtD(1),
      I2 => WriteRegE(1),
      I3 => RtD(0),
      I4 => WriteRegE(0),
      O => StallD_INST_0_i_2_n_0
    );
StallD_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888800080008000"
    )
        port map (
      I0 => BranchD,
      I1 => MemtoRegE,
      I2 => StallD_INST_0_i_8_n_0,
      I3 => StallD_INST_0_i_9_n_0,
      I4 => StallD_INST_0_i_10_n_0,
      I5 => StallD_INST_0_i_11_n_0,
      O => StallD_INST_0_i_3_n_0
    );
StallD_INST_0_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8880000"
    )
        port map (
      I0 => StallD_INST_0_i_12_n_0,
      I1 => StallD_INST_0_i_13_n_0,
      I2 => StallD_INST_0_i_14_n_0,
      I3 => StallD_INST_0_i_15_n_0,
      I4 => MemtoRegE,
      O => StallD_INST_0_i_4_n_0
    );
StallD_INST_0_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => RegWriteE,
      I1 => RegtoPCD,
      O => StallD_INST_0_i_5_n_0
    );
StallD_INST_0_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82000082"
    )
        port map (
      I0 => StallD_INST_0_i_16_n_0,
      I1 => RsD(1),
      I2 => WriteRegE(1),
      I3 => RsD(0),
      I4 => WriteRegE(0),
      O => \inst/branchstall22_out\
    );
StallD_INST_0_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => RtD(4),
      I1 => WriteRegE(4),
      I2 => RtD(3),
      I3 => WriteRegE(3),
      I4 => RtD(2),
      I5 => WriteRegE(2),
      O => StallD_INST_0_i_7_n_0
    );
StallD_INST_0_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => WriteRegM(4),
      I1 => RtD(4),
      I2 => WriteRegM(3),
      I3 => RtD(3),
      I4 => RtD(2),
      I5 => WriteRegM(2),
      O => StallD_INST_0_i_8_n_0
    );
StallD_INST_0_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => RtD(0),
      I1 => WriteRegM(0),
      I2 => RtD(1),
      I3 => WriteRegM(1),
      O => StallD_INST_0_i_9_n_0
    );
end STRUCTURE;
