// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:03:35 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_multiplexer_4p_32_0_0_sim_netlist.v
// Design      : design_3_multiplexer_4p_32_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_multiplexer_4p_32_0_0,multiplexer_4p_32,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "multiplexer_4p_32,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (in1,
    in2,
    in3,
    in4,
    selector,
    y);
  input [31:0]in1;
  input [31:0]in2;
  input [31:0]in3;
  input [31:0]in4;
  input [1:0]selector;
  output [31:0]y;

  wire [31:0]in1;
  wire [31:0]in2;
  wire [31:0]in3;
  wire [31:0]in4;
  wire [1:0]selector;
  wire [31:0]y;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer_4p_32 inst
       (.in1(in1),
        .in2(in2),
        .in3(in3),
        .in4(in4),
        .selector(selector),
        .y(y));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_multiplexer_4p_32
   (y,
    in2,
    in1,
    in4,
    selector,
    in3);
  output [31:0]y;
  input [31:0]in2;
  input [31:0]in1;
  input [31:0]in4;
  input [1:0]selector;
  input [31:0]in3;

  wire [31:0]in1;
  wire [31:0]in2;
  wire [31:0]in3;
  wire [31:0]in4;
  wire [1:0]selector;
  wire [31:0]y;

  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[0]_INST_0 
       (.I0(in2[0]),
        .I1(in1[0]),
        .I2(in4[0]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[0]),
        .O(y[0]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[10]_INST_0 
       (.I0(in2[10]),
        .I1(in1[10]),
        .I2(in4[10]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[10]),
        .O(y[10]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[11]_INST_0 
       (.I0(in2[11]),
        .I1(in1[11]),
        .I2(in4[11]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[11]),
        .O(y[11]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[12]_INST_0 
       (.I0(in2[12]),
        .I1(in1[12]),
        .I2(in4[12]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[12]),
        .O(y[12]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[13]_INST_0 
       (.I0(in2[13]),
        .I1(in1[13]),
        .I2(in4[13]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[13]),
        .O(y[13]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[14]_INST_0 
       (.I0(in2[14]),
        .I1(in1[14]),
        .I2(in4[14]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[14]),
        .O(y[14]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[15]_INST_0 
       (.I0(in2[15]),
        .I1(in1[15]),
        .I2(in4[15]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[15]),
        .O(y[15]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[16]_INST_0 
       (.I0(in2[16]),
        .I1(in1[16]),
        .I2(in4[16]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[16]),
        .O(y[16]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[17]_INST_0 
       (.I0(in2[17]),
        .I1(in1[17]),
        .I2(in4[17]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[17]),
        .O(y[17]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[18]_INST_0 
       (.I0(in2[18]),
        .I1(in1[18]),
        .I2(in4[18]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[18]),
        .O(y[18]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[19]_INST_0 
       (.I0(in2[19]),
        .I1(in1[19]),
        .I2(in4[19]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[19]),
        .O(y[19]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[1]_INST_0 
       (.I0(in2[1]),
        .I1(in1[1]),
        .I2(in4[1]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[1]),
        .O(y[1]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[20]_INST_0 
       (.I0(in2[20]),
        .I1(in1[20]),
        .I2(in4[20]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[20]),
        .O(y[20]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[21]_INST_0 
       (.I0(in2[21]),
        .I1(in1[21]),
        .I2(in4[21]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[21]),
        .O(y[21]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[22]_INST_0 
       (.I0(in2[22]),
        .I1(in1[22]),
        .I2(in4[22]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[22]),
        .O(y[22]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[23]_INST_0 
       (.I0(in2[23]),
        .I1(in1[23]),
        .I2(in4[23]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[23]),
        .O(y[23]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[24]_INST_0 
       (.I0(in2[24]),
        .I1(in1[24]),
        .I2(in4[24]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[24]),
        .O(y[24]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[25]_INST_0 
       (.I0(in2[25]),
        .I1(in1[25]),
        .I2(in4[25]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[25]),
        .O(y[25]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[26]_INST_0 
       (.I0(in2[26]),
        .I1(in1[26]),
        .I2(in4[26]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[26]),
        .O(y[26]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[27]_INST_0 
       (.I0(in2[27]),
        .I1(in1[27]),
        .I2(in4[27]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[27]),
        .O(y[27]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[28]_INST_0 
       (.I0(in2[28]),
        .I1(in1[28]),
        .I2(in4[28]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[28]),
        .O(y[28]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[29]_INST_0 
       (.I0(in2[29]),
        .I1(in1[29]),
        .I2(in4[29]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[29]),
        .O(y[29]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[2]_INST_0 
       (.I0(in2[2]),
        .I1(in1[2]),
        .I2(in4[2]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[2]),
        .O(y[2]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[30]_INST_0 
       (.I0(in2[30]),
        .I1(in1[30]),
        .I2(in4[30]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[30]),
        .O(y[30]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[31]_INST_0 
       (.I0(in2[31]),
        .I1(in1[31]),
        .I2(in4[31]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[31]),
        .O(y[31]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[3]_INST_0 
       (.I0(in2[3]),
        .I1(in1[3]),
        .I2(in4[3]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[3]),
        .O(y[3]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[4]_INST_0 
       (.I0(in2[4]),
        .I1(in1[4]),
        .I2(in4[4]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[4]),
        .O(y[4]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[5]_INST_0 
       (.I0(in2[5]),
        .I1(in1[5]),
        .I2(in4[5]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[5]),
        .O(y[5]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[6]_INST_0 
       (.I0(in2[6]),
        .I1(in1[6]),
        .I2(in4[6]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[6]),
        .O(y[6]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[7]_INST_0 
       (.I0(in2[7]),
        .I1(in1[7]),
        .I2(in4[7]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[7]),
        .O(y[7]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[8]_INST_0 
       (.I0(in2[8]),
        .I1(in1[8]),
        .I2(in4[8]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[8]),
        .O(y[8]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \y[9]_INST_0 
       (.I0(in2[9]),
        .I1(in1[9]),
        .I2(in4[9]),
        .I3(selector[0]),
        .I4(selector[1]),
        .I5(in3[9]),
        .O(y[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
