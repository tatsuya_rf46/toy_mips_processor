-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:58:29 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_3_sign_extend_16to32_0_0_sim_netlist.vhdl
-- Design      : design_3_sign_extend_16to32_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    in16 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    out32 : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_3_sign_extend_16to32_0_0,sign_extend_16to32,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sign_extend_16to32,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^in16\ : STD_LOGIC_VECTOR ( 15 downto 0 );
begin
  \^in16\(15 downto 0) <= in16(15 downto 0);
  out32(31) <= \^in16\(15);
  out32(30) <= \^in16\(15);
  out32(29) <= \^in16\(15);
  out32(28) <= \^in16\(15);
  out32(27) <= \^in16\(15);
  out32(26) <= \^in16\(15);
  out32(25) <= \^in16\(15);
  out32(24) <= \^in16\(15);
  out32(23) <= \^in16\(15);
  out32(22) <= \^in16\(15);
  out32(21) <= \^in16\(15);
  out32(20) <= \^in16\(15);
  out32(19) <= \^in16\(15);
  out32(18) <= \^in16\(15);
  out32(17) <= \^in16\(15);
  out32(16) <= \^in16\(15);
  out32(15 downto 0) <= \^in16\(15 downto 0);
end STRUCTURE;
