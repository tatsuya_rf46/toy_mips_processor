// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:29:59 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_4_comparer_0_1_sim_netlist.v
// Design      : design_4_comparer_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_comparer
   (equality,
    srcA,
    srcB);
  output equality;
  input [31:0]srcA;
  input [31:0]srcB;

  wire equality;
  wire equality_INST_0_i_10_n_0;
  wire equality_INST_0_i_11_n_0;
  wire equality_INST_0_i_12_n_0;
  wire equality_INST_0_i_13_n_0;
  wire equality_INST_0_i_1_n_0;
  wire equality_INST_0_i_1_n_1;
  wire equality_INST_0_i_1_n_2;
  wire equality_INST_0_i_1_n_3;
  wire equality_INST_0_i_2_n_0;
  wire equality_INST_0_i_3_n_0;
  wire equality_INST_0_i_4_n_0;
  wire equality_INST_0_i_5_n_0;
  wire equality_INST_0_i_5_n_1;
  wire equality_INST_0_i_5_n_2;
  wire equality_INST_0_i_5_n_3;
  wire equality_INST_0_i_6_n_0;
  wire equality_INST_0_i_7_n_0;
  wire equality_INST_0_i_8_n_0;
  wire equality_INST_0_i_9_n_0;
  wire equality_INST_0_n_2;
  wire equality_INST_0_n_3;
  wire [31:0]srcA;
  wire [31:0]srcB;
  wire [3:3]NLW_equality_INST_0_CO_UNCONNECTED;
  wire [3:0]NLW_equality_INST_0_O_UNCONNECTED;
  wire [3:0]NLW_equality_INST_0_i_1_O_UNCONNECTED;
  wire [3:0]NLW_equality_INST_0_i_5_O_UNCONNECTED;

  CARRY4 equality_INST_0
       (.CI(equality_INST_0_i_1_n_0),
        .CO({NLW_equality_INST_0_CO_UNCONNECTED[3],equality,equality_INST_0_n_2,equality_INST_0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_equality_INST_0_O_UNCONNECTED[3:0]),
        .S({1'b0,equality_INST_0_i_2_n_0,equality_INST_0_i_3_n_0,equality_INST_0_i_4_n_0}));
  CARRY4 equality_INST_0_i_1
       (.CI(equality_INST_0_i_5_n_0),
        .CO({equality_INST_0_i_1_n_0,equality_INST_0_i_1_n_1,equality_INST_0_i_1_n_2,equality_INST_0_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_equality_INST_0_i_1_O_UNCONNECTED[3:0]),
        .S({equality_INST_0_i_6_n_0,equality_INST_0_i_7_n_0,equality_INST_0_i_8_n_0,equality_INST_0_i_9_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_10
       (.I0(srcA[9]),
        .I1(srcB[9]),
        .I2(srcB[11]),
        .I3(srcA[11]),
        .I4(srcB[10]),
        .I5(srcA[10]),
        .O(equality_INST_0_i_10_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_11
       (.I0(srcA[6]),
        .I1(srcB[6]),
        .I2(srcB[8]),
        .I3(srcA[8]),
        .I4(srcB[7]),
        .I5(srcA[7]),
        .O(equality_INST_0_i_11_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_12
       (.I0(srcA[3]),
        .I1(srcB[3]),
        .I2(srcB[5]),
        .I3(srcA[5]),
        .I4(srcB[4]),
        .I5(srcA[4]),
        .O(equality_INST_0_i_12_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_13
       (.I0(srcA[0]),
        .I1(srcB[0]),
        .I2(srcB[2]),
        .I3(srcA[2]),
        .I4(srcB[1]),
        .I5(srcA[1]),
        .O(equality_INST_0_i_13_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    equality_INST_0_i_2
       (.I0(srcA[30]),
        .I1(srcB[30]),
        .I2(srcA[31]),
        .I3(srcB[31]),
        .O(equality_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_3
       (.I0(srcA[27]),
        .I1(srcB[27]),
        .I2(srcB[29]),
        .I3(srcA[29]),
        .I4(srcB[28]),
        .I5(srcA[28]),
        .O(equality_INST_0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_4
       (.I0(srcA[24]),
        .I1(srcB[24]),
        .I2(srcB[26]),
        .I3(srcA[26]),
        .I4(srcB[25]),
        .I5(srcA[25]),
        .O(equality_INST_0_i_4_n_0));
  CARRY4 equality_INST_0_i_5
       (.CI(1'b0),
        .CO({equality_INST_0_i_5_n_0,equality_INST_0_i_5_n_1,equality_INST_0_i_5_n_2,equality_INST_0_i_5_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_equality_INST_0_i_5_O_UNCONNECTED[3:0]),
        .S({equality_INST_0_i_10_n_0,equality_INST_0_i_11_n_0,equality_INST_0_i_12_n_0,equality_INST_0_i_13_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_6
       (.I0(srcA[21]),
        .I1(srcB[21]),
        .I2(srcB[23]),
        .I3(srcA[23]),
        .I4(srcB[22]),
        .I5(srcA[22]),
        .O(equality_INST_0_i_6_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_7
       (.I0(srcA[18]),
        .I1(srcB[18]),
        .I2(srcB[20]),
        .I3(srcA[20]),
        .I4(srcB[19]),
        .I5(srcA[19]),
        .O(equality_INST_0_i_7_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_8
       (.I0(srcA[15]),
        .I1(srcB[15]),
        .I2(srcB[17]),
        .I3(srcA[17]),
        .I4(srcB[16]),
        .I5(srcA[16]),
        .O(equality_INST_0_i_8_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    equality_INST_0_i_9
       (.I0(srcA[12]),
        .I1(srcB[12]),
        .I2(srcB[14]),
        .I3(srcA[14]),
        .I4(srcB[13]),
        .I5(srcA[13]),
        .O(equality_INST_0_i_9_n_0));
endmodule

(* CHECK_LICENSE_TYPE = "design_4_comparer_0_1,comparer,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "comparer,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (srcA,
    srcB,
    equality);
  input [31:0]srcA;
  input [31:0]srcB;
  output equality;

  wire equality;
  wire [31:0]srcA;
  wire [31:0]srcB;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_comparer inst
       (.equality(equality),
        .srcA(srcA),
        .srcB(srcB));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
