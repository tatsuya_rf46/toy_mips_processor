-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:30:00 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_4_control_unit_0_1_sim_netlist.vhdl
-- Design      : design_4_control_unit_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    Op : in STD_LOGIC_VECTOR ( 5 downto 0 );
    Funct : in STD_LOGIC_VECTOR ( 5 downto 0 );
    RegtoPC : out STD_LOGIC;
    Jump : out STD_LOGIC;
    LeaveLink : out STD_LOGIC;
    RegWrite : out STD_LOGIC;
    MemtoReg : out STD_LOGIC;
    MemWrite : out STD_LOGIC;
    ALUControl : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ALUSrc : out STD_LOGIC;
    RegDst : out STD_LOGIC;
    Branch : out STD_LOGIC;
    ToggleEqual : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_4_control_unit_0_1,control_unit,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "control_unit,Vivado 2020.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \ALUControl[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \^branch\ : STD_LOGIC;
  signal Jump_INST_0_i_1_n_0 : STD_LOGIC;
  signal Jump_INST_0_i_2_n_0 : STD_LOGIC;
  signal \^regdst\ : STD_LOGIC;
  signal RegtoPC_INST_0_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ALUControl[0]_INST_0_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of RegtoPC_INST_0_i_1 : label is "soft_lutpair0";
begin
  Branch <= \^branch\;
  RegDst <= \^regdst\;
\ALUControl[0]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0028000000000000"
    )
        port map (
      I0 => \^regdst\,
      I1 => Funct(3),
      I2 => Funct(2),
      I3 => Funct(4),
      I4 => Funct(5),
      I5 => \ALUControl[0]_INST_0_i_1_n_0\,
      O => ALUControl(0)
    );
\ALUControl[0]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"24"
    )
        port map (
      I0 => Funct(0),
      I1 => Funct(1),
      I2 => Funct(2),
      O => \ALUControl[0]_INST_0_i_1_n_0\
    );
\ALUControl[1]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555DF755555"
    )
        port map (
      I0 => \^regdst\,
      I1 => Funct(1),
      I2 => Funct(3),
      I3 => Funct(5),
      I4 => RegtoPC_INST_0_i_1_n_0,
      I5 => Funct(2),
      O => ALUControl(1)
    );
\ALUControl[2]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000FFFF20000000"
    )
        port map (
      I0 => Funct(1),
      I1 => Funct(2),
      I2 => Funct(5),
      I3 => RegtoPC_INST_0_i_1_n_0,
      I4 => \^regdst\,
      I5 => \^branch\,
      O => ALUControl(2)
    );
ALUSrc_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1100000000000010"
    )
        port map (
      I0 => Op(2),
      I1 => Op(4),
      I2 => Op(3),
      I3 => Op(5),
      I4 => Op(1),
      I5 => Op(0),
      O => ALUSrc
    );
Branch_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => Op(2),
      I1 => Op(3),
      I2 => Op(1),
      I3 => Op(4),
      I4 => Op(5),
      O => \^branch\
    );
Jump_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000F00000002"
    )
        port map (
      I0 => Jump_INST_0_i_1_n_0,
      I1 => Op(0),
      I2 => Jump_INST_0_i_2_n_0,
      I3 => Op(2),
      I4 => Op(3),
      I5 => Op(1),
      O => Jump
    );
Jump_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => Funct(5),
      I1 => Funct(4),
      I2 => Funct(1),
      I3 => Funct(3),
      I4 => Funct(0),
      I5 => Funct(2),
      O => Jump_INST_0_i_1_n_0
    );
Jump_INST_0_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => Op(4),
      I1 => Op(5),
      O => Jump_INST_0_i_2_n_0
    );
LeaveLink_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => Op(5),
      I1 => Op(4),
      I2 => Op(1),
      I3 => Op(0),
      I4 => Op(3),
      I5 => Op(2),
      O => LeaveLink
    );
MemWrite_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000000000000000"
    )
        port map (
      I0 => Op(2),
      I1 => Op(4),
      I2 => Op(1),
      I3 => Op(0),
      I4 => Op(5),
      I5 => Op(3),
      O => MemWrite
    );
MemtoReg_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100000000000"
    )
        port map (
      I0 => Op(2),
      I1 => Op(4),
      I2 => Op(1),
      I3 => Op(0),
      I4 => Op(3),
      I5 => Op(5),
      O => MemtoReg
    );
RegDst_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => Op(4),
      I1 => Op(2),
      I2 => Op(0),
      I3 => Op(5),
      I4 => Op(3),
      O => \^regdst\
    );
RegWrite_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000111000001"
    )
        port map (
      I0 => Op(4),
      I1 => Op(2),
      I2 => Op(5),
      I3 => Op(1),
      I4 => Op(0),
      I5 => Op(3),
      O => RegWrite
    );
RegtoPC_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000100000000000"
    )
        port map (
      I0 => Funct(5),
      I1 => Funct(1),
      I2 => Funct(3),
      I3 => \^regdst\,
      I4 => Funct(2),
      I5 => RegtoPC_INST_0_i_1_n_0,
      O => RegtoPC
    );
RegtoPC_INST_0_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Funct(4),
      I1 => Funct(0),
      O => RegtoPC_INST_0_i_1_n_0
    );
ToggleEqual_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => Op(5),
      I1 => Op(4),
      I2 => Op(2),
      I3 => Op(0),
      I4 => Op(3),
      I5 => Op(1),
      O => ToggleEqual
    );
end STRUCTURE;
