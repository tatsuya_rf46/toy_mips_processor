// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:29:58 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_4_output_reg_0_0_sim_netlist.v
// Design      : design_4_output_reg_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_4_output_reg_0_0,output_reg,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "output_reg,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    PC,
    D0,
    D1,
    O0,
    O1);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_4_sys_clock, INSERT_VIP 0" *) input clk;
  input [31:0]PC;
  input [3:0]D0;
  input [3:0]D1;
  output [3:0]O0;
  output [3:0]O1;

  wire [3:0]D0;
  wire [3:0]D1;
  wire [3:0]O0;
  wire [3:0]O1;
  wire [31:0]PC;
  wire clk;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_output_reg inst
       (.D0(D0),
        .D1(D1),
        .O0(O0),
        .O1(O1),
        .PC(PC[31:1]),
        .clk(clk));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_output_reg
   (O0,
    O1,
    PC,
    D0,
    clk,
    D1);
  output [3:0]O0;
  output [3:0]O1;
  input [30:0]PC;
  input [3:0]D0;
  input clk;
  input [3:0]D1;

  wire [3:0]D0;
  wire [3:0]D1;
  wire [3:0]O0;
  wire \O0[3]_i_2_n_0 ;
  wire \O0[3]_i_3_n_0 ;
  wire \O0[3]_i_4_n_0 ;
  wire \O0[3]_i_5_n_0 ;
  wire \O0[3]_i_6_n_0 ;
  wire \O0[3]_i_7_n_0 ;
  wire [3:0]O1;
  wire [30:0]PC;
  wire clk;
  wire p_0_in;

  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \O0[3]_i_1 
       (.I0(\O0[3]_i_2_n_0 ),
        .I1(\O0[3]_i_3_n_0 ),
        .I2(\O0[3]_i_4_n_0 ),
        .I3(\O0[3]_i_5_n_0 ),
        .I4(\O0[3]_i_6_n_0 ),
        .I5(\O0[3]_i_7_n_0 ),
        .O(p_0_in));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \O0[3]_i_2 
       (.I0(PC[10]),
        .I1(PC[11]),
        .I2(PC[8]),
        .I3(PC[9]),
        .I4(PC[7]),
        .I5(PC[6]),
        .O(\O0[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \O0[3]_i_3 
       (.I0(PC[16]),
        .I1(PC[17]),
        .I2(PC[14]),
        .I3(PC[15]),
        .I4(PC[13]),
        .I5(PC[12]),
        .O(\O0[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \O0[3]_i_4 
       (.I0(PC[22]),
        .I1(PC[23]),
        .I2(PC[20]),
        .I3(PC[21]),
        .I4(PC[19]),
        .I5(PC[18]),
        .O(\O0[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFE00AA00)) 
    \O0[3]_i_5 
       (.I0(PC[3]),
        .I1(PC[0]),
        .I2(PC[1]),
        .I3(PC[5]),
        .I4(PC[2]),
        .O(\O0[3]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hF8)) 
    \O0[3]_i_6 
       (.I0(PC[5]),
        .I1(PC[4]),
        .I2(PC[30]),
        .O(\O0[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \O0[3]_i_7 
       (.I0(PC[28]),
        .I1(PC[29]),
        .I2(PC[26]),
        .I3(PC[27]),
        .I4(PC[25]),
        .I5(PC[24]),
        .O(\O0[3]_i_7_n_0 ));
  FDRE \O0_reg[0] 
       (.C(clk),
        .CE(p_0_in),
        .D(D0[0]),
        .Q(O0[0]),
        .R(1'b0));
  FDRE \O0_reg[1] 
       (.C(clk),
        .CE(p_0_in),
        .D(D0[1]),
        .Q(O0[1]),
        .R(1'b0));
  FDRE \O0_reg[2] 
       (.C(clk),
        .CE(p_0_in),
        .D(D0[2]),
        .Q(O0[2]),
        .R(1'b0));
  FDRE \O0_reg[3] 
       (.C(clk),
        .CE(p_0_in),
        .D(D0[3]),
        .Q(O0[3]),
        .R(1'b0));
  FDRE \O1_reg[0] 
       (.C(clk),
        .CE(p_0_in),
        .D(D1[0]),
        .Q(O1[0]),
        .R(1'b0));
  FDRE \O1_reg[1] 
       (.C(clk),
        .CE(p_0_in),
        .D(D1[1]),
        .Q(O1[1]),
        .R(1'b0));
  FDRE \O1_reg[2] 
       (.C(clk),
        .CE(p_0_in),
        .D(D1[2]),
        .Q(O1[2]),
        .R(1'b0));
  FDRE \O1_reg[3] 
       (.C(clk),
        .CE(p_0_in),
        .D(D1[3]),
        .Q(O1[3]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
