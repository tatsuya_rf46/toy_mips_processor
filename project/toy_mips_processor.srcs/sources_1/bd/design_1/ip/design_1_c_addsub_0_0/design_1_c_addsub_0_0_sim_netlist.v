// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:28 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_1/ip/design_1_c_addsub_0_0/design_1_c_addsub_0_0_sim_netlist.v
// Design      : design_1_c_addsub_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_addsub_0_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_1_c_addsub_0_0
   (A,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire CLK;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "1" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000100" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "32" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_addsub_0_0_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "1" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000100" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_addsub_v12_0_14" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module design_1_c_addsub_0_0_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire CLK;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "1" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000100" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "32" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_addsub_0_0_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
liBIT1PiBmJZGlrkk5QTzP3af/Iq59HobDgFo8rAfhoXTs9ORa+LtfGZyW1jODCoLvtckoOf4TjE
wis63tz0U+gC3rHiFOEBvh7yicIH1yVsUBga5N9X7T8wodn9q9FQHY+rYLMSAB7eWlUTkh/00O2w
V0lYiEoHTXD2egiefXTeEnH8AZoLV33xepKs3mE2u/DZpwl5979/QpOuA91RLpqxYYl8543yZJIl
vDqa1Lp3zeCFMxNEigSco2JUmKigF02laS4Q5ouz5BZ9ZEjzCAmM43JbOXc9IQbSNkCHHNTP9jWA
5p7cMV1vzveIudypIX4n+gDqGJzdF/3Otgr7Ww==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
u+s1JLEMG7C5vWfX3uWwW9pQp+d3RPpZWiBPUtS1wkrZ/uAC0ZcEISM95SXEp1Pa/evispw6dAJq
ZPGwMouNA4MH29BVa3gQ3HJEyNUjwSuTGNcly29bx+KogK5TnKjsziNrX/LcYkY8KahjtI1zZL2q
f/w6kRFTDX3Ftzz4oMqmthWRpDmIVz/EMHf9ADtrkRon0l5hobL/zZa4nR8uRgL5C2GIdJGtDPQH
KjHTQlUfc/mysdoSa1PWSfgtt6KwCFmOf+OUvu2X5T6/USq5AX11ZOYDkkOdLoKBUtUQRDAq9Khk
esnetlUhPgwkQJfV3IUKmi4MJY1asdhKM2COaw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12480)
`pragma protect data_block
ST9abh+2b3TfzDmuw2gHLfFU0jmRPfT6rQdXVohfRnLepFYvtrPVn1e1BjwuYA14/O2qQuUNwrH8
/V/lkxDttb2tPYDIiyUzr3cb+mVqcgqZtBfSdqUNyAsuqHWgD6zl2Ty5HAE9hvKdPl0JhePyJPkz
sz+Nq+PrmbyQjEimC0WvuOKJ9qvaVvNBA17xQ7CsC4/5atSDs9zklBuMwm7enzTWAuXNcGjbyvGV
YNt2LGXdrb8hjyPkrp9u7fLLMlktTnmumdlifcyjsqv9N1z/1pXAt+o+Ncmmc5e1hY9V++SLLBYP
okglQkmAJMHgWnsGaYVyk+SJYadNnrw/ZuSrk0+W4K7CJnvSVpTqmVTTEckCIjx08EuRBWsSOD33
bLg5R8IzEnR49tLyqJN2Gl13ld1YQ8vno8LyIwQ/UguvwO9vjlvEZEzPvGTpc9g1GVhN3TAy26q4
xgqDVl5UXLRAOlX3uIKXD8Rvw0Zl2HQpL8fJP15WQIH7+iQNlpHQAZ3QZtabZJcbv564sd8YfXRb
GE52XfXYNtgv5dE7VnTipcRAvJTtwDYrZSK8tH7Ff1s22WyTjV2kwYxJFXn9RoVf7zPVxD1c9+xq
03c/1XFPUZ9PXVFaJsoQXawRgk5PZjeI2oE237YbCIRWbSwJur4ACkC6Rc/L4MU1+ys8ZpAdmF8I
NtqK6GpMcSZbOvJrwJxdx8DfR1YKd+wCDAT1pHIjUj2t4EJVL7ws/2vQ4AYxkw3rHqJ7dbR7Rmtw
mi1omVI9aa/N481Aia81By8hFyvuMQQWKPpvSEB9APQp50JZU0AylZgrH5eTFCqyg9iskGSzxUUG
oPZTxcZxaSHUS+XSEYrfTh8ku0+DOtV9BcjrKrEfyIZ+MnqDNPbsU3OgXtlhP8LzwfA6I7QN/YdV
DWe20u/NkGj0TqjKN0sGZDMDBUYcqJlm3zGvRBoN5qqXNxM/A1ZHEcBs/bVEZFYdMrRSmhg/0FwP
rirFA1xZK3LVh7ZF9M/yV9YXplYrY7PUBtTccNTiZwOgs2ec+oKaHXqcCt51X2dg7YWFEwsevaSW
oFUzyEGzyPbOC/IZHjnfd+7GvGxSpRSbR//VGToBwmJyWR7ZbTEPrvAYIgFTEHToI2CFSJ0Kv1Xz
7uTm9gsKMyPUJwmitEmZZz6MIRnMRyCFfFGDYnN4L2V1slo03GJVwCXtnOXnHkz9xHVnELhc8zF/
nJQfCOuMyDPREZgM3hWza6C9GwPcXYiXiq1ll45Emt6SznnW4UbzivLdS3hGy1ctaYJZFC5VBo3X
cCeQLGYAaMBoaW3K/dsYDkoNxMHo0Hq+TFclABqt0+DT65guize2kLqVB0Z5YbkM3fBeQP3DHF3o
TPBJi8RqfNb5fw/B9DVOUVqfqqyALZ4Qcnj/oaSfza8zxjrVAliluWuiQzs0QfBEc08HGK0YCDLm
DI5+EoFuZ5uqAVdHH11kNzFx3Y0Ldm1zM1a797ep+uBg7pXg2b4Yw6NEvemjMsyABVEzMoXQmIN1
95bakBihiPxxTAOkQRL2KP5izXbfd/djJVdkmxsV5WA3wv2vp5s+LafCVj9jsKdVj2lw9K1am9Zx
J4v5gBusuhIgy7EO8eZ2mZE99w0GMqlP9hsp7VkA6SRhds6jseTWpduwGfDe9dH8wPuZ1C3mfaIi
3Bmrp7INVPO+Gi35gktxrhb4B30QAhIr3/vFLq4WMJaygsuAdzUoxIIdcKCDzBA9BkVkCiFX6Pf+
VzlgY7CPPrk80cOTXvqoGsc1Fk3c6lAg65z2dXm1CM4NxsaMTGMBtc9bpTm7XROKQKkfQqeSyOUB
8Ix1jwoiMvPaDxzOjPwOSGoSzMqAzDwOIDnmTX9eqsxL0tk6ocBcd/M9jn4PAHte+Wpfl4H+iB8G
2CriTuWYmHFkvYpgPT9iDWsF6cez79EE89e+N1F63UcuwIKsE5mPa0PqXVnbZ/cFOSjP0e//1gE/
IqGrCgjR20tYx7LLmCR9huN+DZs84cwRAtVeCvntltuOMJdxRieVM8KFRzVnlFLBh4bHDAKWCJIp
DHKU1/zHxthqtHLJwju7xpC42PdKdIE41QUTBR3OHss5pMfIQItioa5MFPZ7Luybgf+YmMEWi77C
xU01+cXuXuufEDdslx2icbZN/zSdGwv4PXktOdJUcPQ0p3qpjQmsQsWSdB4QiUpoAauy0YAEAufB
dkTyJTThHxCIaCV7CEWcZjpw/tWoO7dlO44V/fW+o7O5938gT5cEY4vDaIzx2a5Tk4GByAOqBlhu
xS2CBPPKuGkkALbSYrCb09s7VubdL2ubRQvTocf1d6YvQxoAuM8qIkOXe8jRHA2DXOqXXXK8KfFd
1z3zVfa4uIoXnzZtHQJZ4TM5W2SrigE1oR0Mv24S8SuyiDSAyCOpOebtcwh15W7bfMs6hddpKUNl
M8QsCXxKcJNVeaz2put52TAH8M3wI6SIrrBw217/yKQQEwt6ghlL8HKeEUUkN9/9j6RJ/t2W3Lc5
1t9J+uQxqevhZMHI9Rz9uEV3LrLpSYOYFvoV715X739+Cp5biQepaiyGTVDZTOIVB19GfQNBEVz8
WPtc6zxlf+FZ921xrySJS10llP/wpRQSy41rr85NXKnjAksEWzxQW8o/jn74+mUM2kHxbiDuR/WG
eogPAOwtmqHTdxYhqjTjaoh/GDwxMHol3umnmOdCj1IDuHgMO3UObfKc6bp2tK9LC4J+4rZptf4N
nJ0sBUVAg0gf1+lKfPZTu3iiLnJHimBWSmUXwh3v8kytNe5gurGMGQQa7aTwBbInvS4F7fuiIyIU
wfcXE9LwgcSVE8yb7nS/oSITqmKVkZBUrKCVsOIrUd3u+aj1yBPzTrSkEyQCw9N3bPgCha++noeu
NHmUJY8EdfPLLGgMEr8eI5nzGYTgpext8VBcUctaPn94jDYSTIb/deCFJ838kEV3ju8chzdXELNd
EV3N2lE4CkGmwe64nPVpKKzqrTyPI9H/2TEUePVKb7WMQw/rMB4Nj2u4ZSXJV0ZS95SwAnLcPoB7
avvDjazgBB8OfYx6OC/AK5mnObJzbCerH8VgL1BVwFVM5S0x6Ci5iK7FdVugdZdx2rCHqorpPDJH
P03SFwJBS0uIHNcK1DeEuvZoBZJ6kxLb5CNQDe58YEug8DmLn8yQhRWbYEZktOxLN+rIZIqrnwwh
MCpXlkNMrrHclqoTz7aLalTPLAVXYroXPZL/fsgaYpKy8130EDvic5yo+EmcFpYeJPmUrEJzv8ps
i6oyIq5casGa12eeXuvw9XYopHNrE9bwv9OGGbbxjkTDpE8XA/Zvq+h0tIVlY7T0Ttds0+GswYoK
kJADyduacs6dwKJWNPK4WCngyUekJ0rS+d81fiptTMlg0HKAMzfDyfiz9FDpeo6v+nZT+dtKIq3W
25tgi+R2GK9thE6j9cHIzp+5U3rplc7SZYj2QS2yOBEDlZjdWzQ/6iOBiwUEjfsiTxdVnzSW5gDn
ueuxhdSXAzofwL42EGKP7ZpyjiyZCFHqBL8Ri/5BDaQSqGXlBZ75QW1ukbqG5yfXyKKrSDYzw2hZ
Dl2czDKwsAX+593yeGu8itKbzLrOm2s8DhMrJcgyrJtsMK1y+Lv5F3PEGuDyIuuUyhCiCareuv7R
GIzmA9JlyjzciiVhI7bUTlbfi1an08rnXjmM5LjTqY6v43lJSRDACLXK+R5fo4SMdA85lch97osa
KIM/6X8RtfBXGubV5X6vsSXcg+yEG7vzSv/ktBQETnGro+jm7c3xibn+15BHXmM6lXn/On2g0NtS
cYqp//YvMNQeHM8lpLZ/vuNnElEVrxoKNSp+l2LiSkwfSiT0xgaoQo8KJB8eUZqoIJwUElafkL/d
8MP/gR3v9QcrXyuSXQlQz2SRfPrYHGvlPK4ySF23A29Va8xpZeTkYpK8TNraU+okAhPGhkqll4vJ
cbL26h+IzgQtqwDOed24EHIe7Hnxj30HB21L/JbkOhBVzuVhLvC13IM6ZJhO+fdUxw0RL56muy77
r8RHNLbpLpaAzVeJBBHdyOkupQefiAnZR6ByIX6RbNznhB9Zjk7tCZKpiyfVh/Zq9nIpGZM0PAfC
HfFEk9dVfJUTcIFU9SOO3ywAds/OhY+nPHDJSmGpYPcvXrD+nrFQmK41d7xzjFhpkP11BfVGzeTT
BmJWVRgeFFHGEBC1Vscj3pEgnB7tDejeOj/VuqXNwQi2/PPBOmX+pk3TE8pt3/FXSThGvP47ir5+
bxUujt1bS0bAf8AbLx05+Me50qcf0ATsZzV4Ru++X6lD+IIlu9qUOpK5aGiL3IAIO+KJBaxIZyPg
9z3Sv3xgcaZ8P+qZJXSsbGMPW18TIk363kX/JFK/J4X3SXw7HAfdqQI3rcGCbg0VY+I9OP618/Au
EI8C7N5guHC0U30J6+R3hho5t7DGAh5rKeL6oJ49eWSIh+oVva5xvV1HD5uDymmqX306O0MSvp9E
kXATBlda0fm2S7lzfM10Xl3plaDlTJugBZB1CvQ3ruO/6XzteYtUu29m9k/9t4Kzjx3PXpjlD2dI
DfjAv4Kb9GAH0Q8PFg2jZodeHTGFsD5MeYoKn/mdIzM1tKyBE8PNs9PejiIsxf9Ok6HbTRbvhSKB
swBhJf+Vu9bP2wYIjQDjhLLOWYKjVy8SalY3PfkamFG0iQ6rcfu197OsapntVKPrS01lJ+WQn/jP
WWutRS+9TvLaVc35sfHK/FshevXg2YgBufeTWbRZ5mz9iKNJXs5DkR97FqPcg1+RWEwC6oAeNI83
X4jp4A4kWYfhclW68gVOm/oxdvZN2ky3FNau1M5i20LG0HaaPgXgxymKCwhVR2dcrF1yvRvN6BBi
YNqo7yaIDGrHCjDdIQeJOUrgeV1KmVaGqCUoVIi0b4ciwfQnEFuhIrc6Jrh496iicUNKNh7KaSu+
kuRQLF9rT2R3SGP4ITXdTYvWt8+EkUHfwWQE2Lp5cKpyXCgkQ9JXE1HW8x8d73Rzv2LR3M3suT3m
WdgTr/CHtG1zi2Miqj0cJIq4fKFsMEMt3PAuGUqVrguAqJxkAazRjD87iPmQpomNFLTptol7keW4
6L7wqfDHtSLCxx2e0pnsaGOjCNCcw91hYyL/7gXhH7MFLFiCREbGWxNpelFEZznBzUq7HDKpXoPk
UYtPPuIblk5DiohlVBvc0gBy/bsJmAjqt5Z+QRXn42oaFNLMEBcASGcG5/B1umM1ErJYPIYH0Ya8
p3ivW5Zv1cnS+nOMc9Goce3Y6fxSvYqz1JLhZgtiVNcqFFuziBqGLGJ94Pm4+PGv/Ob8THyGyVAz
+iSgyJKgK2VTxylWkSGkXzVxZEe1K2oCFqcyZfr+0Sd/TabWJw1mcum0KZU72NtIgxyD/9F78eVi
DRC21EExta0LfSzqAbc7ksXaMFvHbGnQcdtYy2jLidT9NNzE1KfmR1kR76vkuucCiW+mcbb5DxwL
8qcxEf17Zkomhhm7R3kOWpgwyiNmpHGgG7YuWHvZtl4fJsjUfR1KCuGPW9cP7g6g6b4/kAhAfYCm
tAb763QAR9Fv8UFxqoBi/+R04jpOqsahri1hoFEoJVgYnVt1mgpGG9g/VnzejsoMdS41Af3fbtSk
79Qy4iNpa8tmvTy+0BhGvBfSr3NLy3doe7kBYv3IUUzibciK7c6SY2DK1GY8MAxgk8cuE2lwJ/W0
SBcbVk9Zy7bqE+nHGpac5tQMC8lvGrGcvlf8sQaZVT7upGTh4+uK3wwwsd5I0j5egTwiIhrHawA6
lupfk0n14C2uFmz/XvpqdnJVerpwlBczXGcOv7tFKnP0LrOT3zmULKSdvaHSSJkotmw3Sar54oEK
UoktcNmD7ckVNxBHzJeDhIxOSBF15ZfIt/CPmQe9mQaw7s93Vn/TZpROU8Sk+RoP6ecZJPQW1v6V
IlP2kSyyI2AAAZSXWZnxqmZuMkFieEHoDWabJKnRfDgjmhnbrW3PiAyjlOuPdLxovOwwAAqz7UYf
uyk4y0JguGsM2B0j8OQxQ/a8Bgo4v5DJJED8wKyZK9nwDePESFfErXNpJY9QwKyx0sNOvKlsqU6F
XBB3j6Jb2ojl9plyGhzuSOxjQTfwY3aJ9yFrAVdvVr6It9Pu/wmM9AY/WvwcyiG1sHaEFMK1jnKB
SkM3j4lNOq2B64YwolHnn90vlDURWBzdBjNSPdfeP0Hiw+cZHwUoHjkCxA63IpvmwZluXPdvFfqy
YmvJyl7AoEwg330YkJCEE1U/q++1y2UPlvhww6cDbBavfCDvh+3mGmoW9/Pr+4s/fWhuoqzhMu9V
BRUEnQ5K3WxnRuRmXJQ2+i2UvrDILyGfzbuL4P5DzAKMz0UE0otXdYYL0VJOsGbLJ3worjxiCta6
2UKV1jtwzQA/6kk7s4ert/JLMM3v+R5hPRy9Cf06Hf/czw5xYF5NkGRaZShPlEfZrnImgccS/KxL
jA9024eTqrbGDQoAnZHcpr5pM3V130mKzeyvyQP6RDuR1xIcHyLG9OSPHpbe6d4qnsmzetbfG1Y/
d/ySOSM/vKik+GrK9DNqHw0ah3uHDJ1J/fL4I/tU1ueARCEczYyDFMG3HRRSG6/ZY3theAGC6Bhg
oWHASskYbtxGu3muYG/1LxKjPOP/5f8tYtSepcRAqL5570YKHXVbNzxC1r8WLJK0afysDhTLCpUo
jMbkpENAnw2F0WirymTpVJXMOa9lMHcpuB1fGY24ZePoexCnuLwghXbIf7+8JLpVEKQQLkcSjMIQ
BXwqlxvnGrCyBmEDglD1doRGEk6QUPlWEcmlXmxPkSeofSimFWeFFlXZmpg63BN1CvwqXDt7yJni
i8A8i8a3z9WMk5pwnGQln/+EV5xKSnbxX06xfMkOABZRyQ2hWjMeMEiP6QGZtARQrKQZMEvCLXax
0jNJrOgLJUfRKPmj7pI+7ogrdYb7aWbI8j7hX1aUpqC4qA/90HAcdtuWCrDeF1b1KFWawbAXFRI4
31PRf4CSKG+Gf8+/a0hDa96lhtKArsaONg1DizlvOyU6XiTWPRJ1kMM/kO/JXRfYFRbxzuB4rI05
T9JUlTEY0q8ISp2bcUsgd9Jh2wKMVj7KAPO4IIzmZJT0BVOTrcpvTpKxi50eJeWcEOmfoc1Msa8D
PhR76PwwkbibiYno5myWjz0Kjnm78aCMWViAjIEbragZmbogCVoOA73/oQCnq6CVw8ePe3+TPAyv
rtM/Az+IRW/5fsqJq2UPzaoPzzue9Z06h6SqTO3b0ZPPnRn9S61u7S8boZoL/pyXWu6OwC9x6iCs
y5s+JNPW5kbO5o5y5jgo8e5Y98oz61fhrF286xPGs7UeeYq07P0dB0jjJEMp0SbF+xsy5Y0A3VSI
qELZEXmlck3AJ78DnxMJCPI97EbEaqDbG94xrhSxkHikC9vPAH3h3tuR5S5W8QzgSVvuYNOrCZx1
wQ/v0EmN1A9Sc7VFfXUJGCMtLTl8buLl9WnX1iLMCFKvpcyHRxW77698P6GRSUhdI+S7INz79zN6
TAOtNEZWY+Oa7BcapRYPA5H7vecFN1idEAgwtnr0v46OGPlN9aZLxY+9QXrpnyx2pbgfi9pLLvOY
a50jrOCX6PRVmYgdULGTknohytzOJ3LecWeCQnKf2WzUyYwvDfGlA7coXHuzb/Sm9V8XPJnzzA8Y
S46QBxlzseDd2756w4e9OMvljqbp7eshj+cAczuxZ4pqr+cQ/9CkaHRz//rIpO3t4G990k1c11X3
C88cnGoy6TqME4tsz98sYb2mqfeqM6feeJTbGUVaSywKKKu7BaLQhcVhih7cdgftV2/536of9q06
lqNvkoCWyclzkUnw6OIochfZvEYFyDNpmuHV1gCfQ8r895ht/hGKCAR+uJaEwHZvfrfSqr1yxazS
O4qU7WLpBIWSUgprINHSIGubp6AwGgrM5Dv12WBa31Uv6sQomtIKuUI6tPXh+gSMS5/fxN9NRpxV
+ZuS0S52TKdcNk8yemR9ywIJurQzSKaPFe5XqQ6ybkOdlJU8zkjMZ8jvZZUScS9URn/a78PZpN+l
hwaixD0oq71/r6e1EhwgJnKqpc/QhB7F2V7r2eVpnl01/I8JMgwnjdQikUARpxn/Eo4OW8fxajS1
Ac+ObNJoD3ImTaTv2vkZI4CsZgyE4z0rfqibkRoCn35GWKyZtjWXD9/KwlIU6775JDTNBmp++6f/
chO6x6Nr5wFMm2gDbzYUAXw7NlZdXowvhU2HpC58s/5Eg4mPlY8ncmbf4TE/g2cTAMkdiVMDaUVj
P5A0vNkJGtCaV24DFMVc61av/EO0IFlNJR/urro6AdV7dhnyBGng4IoNxuHbUorK1O+M8PwEFUn/
t58tsak4mN6shsCiCHBRzu05rn9A+PAlfrxBGEwYZ3Y5RyGZtkj+R8e9ZfsAa1wtfzG8TQmu8Py0
Isw1pJ7WTpSCNXxGLowPUkYM1bf+M0CJqXDWUMrEds+eU5Zofu/MfBq+40aoEYezQFxj3Cqrb5kS
zwUXgpzSc4BN9wYAA8Ya7KsLcdN0Dq0P0Pf/4n1yi5L69YHPHcdaNKKW3+gakwfo3suIlGiwwGCT
oJCH37cdpCqADK1asDB07QFDxfdu0DYelKe36HrTyzcvNfZ34LnHMeP2Pr3ivK8MxllbBH1giCMo
Ke5l3D8Ucl4+cbUPdOkxDbUXgjWQwGmJgB0ngj4MA8T5aMLykUWNC+Oh3jg4Ym0CPH7IplV85cF4
IQ4ZEPGEru4oXdI/pmBkmzVGfRBK0g8OT7L35t0Havh67BdZWifpFq/Mz4icM/svqkAcjXNzACqZ
uo/zwP7IrIaGXRQ0Bkl66ltwvr8HgTvNQGEr23XxJ0FwfDzLWulHgmljhPFDUqeyGdpwMEgwJqTQ
V+Q+cCN/X76pnLNW9lmBCY8GPAlMaATn+n84qloN/abF76QpiKUqENyW4SWLRfB/riQdaT6remBI
ZmK+YLb3Ea2ygrmZ8DPP72YxjcDQJQnO90ZRkEo2PMP8CZrOXBfcnYZOgAbPgdhBoEHIPrSuz0Oq
pU+0PruHXJO5nihtRRhrikUVmBz4X7XgElFY3Z5ArKQlEyfHqZNI9hM/AkHaaaM6Tsi/lhAIyf2w
VfPiGO3hKLmD4cqDsfIalaDswfh8+vV/kYqJXfrklHGR4qVisfcUL4izM8JYzsC1o+e0Ox0E37A2
ELtJwk/0nqywVEdcQ33Y2c59aU8rE7jt0JgCzIWfsscxJfOWTjsaUpT1n+L2ZKL8wbk8ZGo9tkGx
X6RPmqTqb6P4+pkqV1nC1xj/tJ04+FxvtZAYXEaPlV+76HGb4iF14IdrtBritMjAm5CSOLMmBst2
Jz7cup+LQHE0zu6hyHJDC+MaPwA1zPHmAvUfB0u1xz+XKOR/YWkR3TDqNass0WSMN0PhqtfbZOqR
/tlF/3hlBYNL6GoiMCeQRpg/TZ/SyAUg/NdJih+KrO10aSmOdHAKAmOIRt57EFyHSfIAAsKXQZxi
xS7NZ4Ert3OoBhZmBRviW2KO+m1KpJru64YhKkNv0LerSCKJoLAxAaFVVbAlImdZE2siI4UeC6kA
VswA02XryOZ2RRFzFSrd6ju7bjGnywxXJcwrbNrYG+fm727zsGaWTAn7TGzhpXA2CxIDmbovovZ5
/H9NFMnjDeviyW712gnKhKdVbnAGXa2zOamrHVPCF4OmjRUg3jU1QribfXACSfVRs25VOshVqHdK
QTqOyZuJi0+YJuU5BLWie4hrzN5/6Wf7hv8SC78sQMpxSfB1NXVQVjh6K3rmShIDheSy+JObehVm
XWkkIDS/qHqIsxSyJ6VZ/s+2xAwPKvMaVg3Yj8Dahjo7M4Rfmzri/XRDjFOJx/BMjjXyKKwfcadr
KRiRquY+TBFludm6c+dwdnDarBeQxO2ueFLtD0lpKtcs7jIxUHZujyV2SmGkGjqnONc+p/GdxF1Y
H1miDKZWku3iNFQ7wOs+12dMsyhrIAd7wFTEcq6Eqyu9qmMpNcS6bb/XUx0ixUJHgv4bpSsP8dnE
M+81264FdTq3JmMfnMKFbN7PrjLQ4vZIpoZ7G0yLT1CR1Q8bb0FLcoNzSDQa08yTvtZcaYaXp2GG
4RJ8YPUWrbtcFitOQC5QbhnYs5l+pEgEzyHsk1XcFtCD7adh83ze92mI7mwM3WcjjxGvob3I80Q0
bVSNhUpsfrGmc0GviARzra2vS6EuE6z6bqQxtxAasS+CUYiC/aaXw3nh4oJwMW3rPqu2Z4ZErjGm
KQJo3//YZEV6m+jPPFZstKQuuKEM3GvAME1l5ACZ3A+8TMXOF2Zz2Pg/7UBbDqq4HvQkdfw55Lqm
FWbg5MBd8sna46G//zez3kFkJZ2Q/e8CEccBCHDV6cnBt0pWJbUxQE2Cg+4FhY+Cz8edKJZe/KwO
YSNdL++NFqixXTBIzzah78Kgs3H6CQVTlXtqViXzYGNwViJHhXxUXwzq4WSHX0OHXy0mhxQqVK0X
ohkMRuj3cJ6/gKgquk1Qwr3UAACj2u+QYLLcTdqx2b0w2SCHBHM8uImAAOOv3Q8ETaDm7nvW1K7N
fz8dlN2kdkdUMnwSLvnsknVcuxlfuM+NpOYom104mC9RqpGtCkcllRs8NYvDG/Jv8suPm9dTCWyg
eFUhZnHzWdExMgn537rYxude0KzNbvhbQC+fduZpwNHcf8IeVB6KeSZyQnMYYAcCiQfuAwZo701Z
7dV4fCJfbtCitrhS0hxQDXEZT7QIaN+nJUd1iHnO7xfDmbdLTTShE2LNE5atT6cD9bw7gFnwwV7v
cpdgeYTTotl1Mte4JFA0uTRSVBPLEeriFX1UBJPhi+mO6InUmPZZRFTzKQOvn+PXJolKVeuyuG4Z
qxUfvviFs+0QjV9bwy5KiQqyV2i8ECqYe1quIXNVagxgxMM5O3x+NzRYs+/n2Tw5IcDsqHh977zK
l2n3xC7OQtPmdv7cIWk2Xi2KADQLShhecxpS12Nt2tei2io4kEVqBGXElNc6oU3l6YAZh4WdCrO2
eV010vFgfc5N7x/vHYGgWznUqfh2oKDXhtOWa0sDFICWPIx8W2IFlRE2HyauSkwu66GailOvAevq
LV6qAw+bv6qhfJfZc62q2+e0YydRIivgL/yW2BheqL05lYOKK7/gU4oK5+KR1fvsW6ZeO9KwQkLr
reoL1NBQM8gxYK4l/OEp6Z2nbiHhZnTm1wHnveFdE75ntmZmKwLyHvN0EEy/+NVdNFJT+3bLJAhm
Yh+C4c99N4jRIisc2i3XtvZgVoXJXF4CWH02bbsgO8NRzWe5uEQEq48RvmOvDDlM7qu1+NEqOdIN
6hNLgSQraP7g1hoDs+h0Nc/t66f6mNvqrgNl7dV3yKqiDN6xyC6wAdF9S+nPN9h3OKnTphJ9tQSj
BL7fYUXGY+OGGO68pheR86zwTwLUID6EDJo8YMmkKaDTsyRc8IrJ+JXFv6JyVS1f/eNWIcQiwPL7
sA4WCsMz5uTyhXsHdd6IxzSpINEGZVa5/vytkrSDIlNvxEdRccA78tWuo7eNGmkEIN0/pk/Odw60
VOjw2jPw3ozctZmtrjo1kxUJqLK8SXhzSc97XJH9O5fbPZCKiZTu5c8z/6lFRURpQTSQzPaL63TJ
FiHcefbg+l+KALTOwOtxfEYlk04LQs1IldwpPHwVQ03sL+qGfdoofwdz6swr3dl+Gc8On52CCKd/
S/O/Sh+IJWjlSQtsK8p4kU4fpRsfD7HECgQOgMl9qHL5mvhzdtQolbFy1yvp4mJrIm/KoaV7VVDY
X8PSoyqiuAQ56fDcpzR6MNdJOinZ32VO9jkp3D4UaNP7tU+LTW14y5wlYhy0c6rPaK6Wrc3WBjp2
RXtsnTuy3bjVVDG88W0psQi6BqsdBXSsSIoj2mWut3ThkfGO2dwB0Yzw9UrPfdaWoMrlCvkpSHBA
D9e/J6yWsbUM/ma06cTNxlfc9cjbbnebY+r/YSDxBU6e7Gj3E2Ir1u3LdvzuygWe9ib0zls1WRz0
xKPhPJ74y8e/kFjm4Oh6/KEPW7DglZp2pynkHArAusdpSueJiTBdev8FlMaahWa5vtRIBUvmVnBI
eY/Xg3CI2wXo1Rp10pTNApodfAvf97dW4R96bmajkrlGVW2L1k7mIBt7yEiSL6xmw5rPOfi3Ub6V
yLjKKI8Iczjxbv5lN4UDWzXKD0odzIKelF+tU1lOFZoJVksMia7G80RQcqz9QnLs0mi/a/gEWol2
iJzfnAsoA49LNmg5TDD0KUToYgqHP7RQCDbnhLwcPhjZJHiNPMvWx2bUYYCBLzfC/hVr+P6Z90Is
TnsXefYtQSMaSYX6MpeiP1DYLlqmNc3ko2ETlrYpUjzunG70n3Xznl7cjh31ZWbeM5mGoQB4Mi0z
Ii1U3OF6pgVD7i/YpO2Y4trv+8fo24Avl1fGqI+4lfiq7QG6l3ulK9/7O1JUs44q/ArUGDQooawh
6p7C4+5ZpnXNie1Xdp2xf/L1POKABsWOFzkNtrYY8kRrlm3yER2Wf4eG1mvXo62mSIw4Wxip8XnQ
UlTlVnJCXbmCImcBIPdP2yixlfYX8yVOjPz12VYC1r0/lhps/XA7kB+eRfqjp7oCOi1SejA4hpB0
/wYi5K6R24MiHClBWu+V++YkjBt+NtSRsTGcBsR6LVdbgvnXSNbcF16gFVkQmuSeBijVIaSHjdKa
1OCQK6VdTZIJ1wINljaS6DWomt76pyhnU+x9lwoFY5m+oiqLKEhwwu7DRx+tzaWaSJHFtDK61NGo
AjJsCKA5MyvtlGMHo4hjcVn2gJwq/nl+gfRR3u8H4h3RsKRF6T4/NcMDFYGmFD3GlA2DKgBrx2xC
VaWEFKIqGBLSt9Rhc9ZGikh+6RCqKpItUS6ED82wkS37P/zWqCunVyl4PgGyV+NDR+iMBXiKGXqQ
z8cfDwqg66P94a4jyirZnyHIunkC39ujHn+41Ry3XPVrQuV4TF/vUyPFYneqNm59/ykXQ0Uz1ALI
ETYgDjSjYZHfX74NJ3f9loIxNnlGFlmn+Qx2p7Rjmpx3mMaZW40i5b+rZIjj1/ifTBn2V/5EPgh5
VYTMkGHH8iI/QuvmlaabPZD7+2Mx8ui+LxiXTjW3RDx2MuzAvRtMuQpZeb5lpnmlPvK1NwXOy+uv
Lc5or3nJfeWM1CYdBtTyDfLihge/B3RIJknjXNcy1RIqlMPYyML76iGn8/jw8pbWjVuLUDaJQ8Mm
P6muCnze/+KE1efnkoF2/D2sgJBRt+TInwY47vpw8wfyD+vlEmBptLl2JcpZFTi5Idpm41bjS4Et
x7ZiS3IaCJoQ9/NdMeSE538uSrOyox1Bm/KKttIXJM+tDhDjxkCpm+1x0GWck2yVOPoYZQ52/nU1
OH9nYQ3kPnfwNV1kIzDC9iFrWNz0RuTeFseTwO53dS6ZhtjAo4O2J6sSNUCIeP6blRXGppb9MkBI
Smqsk7CajOkmf+2voKJKcaggfOrMQk4u01499AXB0CPM2NTCTT6qtnxP6aS2D6Qv+ouC3fXYdoVI
DyYqQdnQLHLt3igIpPz9V+4nlIEWMpZDhBwQ5uO7ztQIuE2uuteDCAa6kxzak/sOpJTpHBo4nRKy
UijKdTp91Ht2dZH++U93fk/ZUXKI4HOzbKjvv3mj90QPjOV/XkfrPCT0SSavJCJNjUCcJo23zbuA
POgyktrA/EDF+TQNHaDfTxFrXPntQtp/8CQKRWxpD7H43RRu3a4KAZkBD6u896khhhXTA5ScOCA1
8USbVJerpQylN1v68h/Otfa0MFvbxZgG3rfHVq7kRoGIOxQprDJ4oAZSmyXx+y0bSbOGxlXJ9VmG
eHhgvnMptQMlhylcnmlAHlO6H+yYOQNLJzN++T+VB0Cyo9rJBkyjcpqlTx8sAGyvlKDlrpmMBjuw
w+QKvr48FPMiewKUW1aAh6jZB59/hz2MON81KLgsvlSoSWGBISzQuq96NgxP9GTEj8gRwBeSc+M+
WEnJtrURh2jmjbEn526LdqShSUTQNH1pqM+pbvLr7sIs5B9UOEklamx8R8UyrX0GR4DzUqqTZEEy
g6pdK+Gw0OSyUJrBH9uSqHvz9ajx41G3FR+Nh+4eh/yqJUoRJAY6NbFljgLoW44KvFyJ9gmm6rmG
MB6GvHvhgpmXs9vzzIBAQ6JVSMvEhukn5xMyuneiTzPbZCR4ityVtHjeLf+q3lvFx0bQMEQxtR2n
hLlI7vADdax99VAMddbrsgwck+GlOnwr/2zQQ6TP35qVFh3mP+6UvSCMDPMkTI5w/pRu0/OBrvod
x3qO067w56D4lt7Xfkki6sfvx5uj4czSmyAijFvQJysYaWeLIkGm8LONsebebWohZ8Rs95SrIzsr
HP/UEIJ9uw+CdcmJ03T7OA77185V1bdcAXUMV7nEHKDgqMzMlpt0Ck10IoWblJ3rjMN7AkRfMVBb
UrERhL0dfuwRczSN/0OMga0grR9mkV3Z0wcXxIX1Lh47cxz5kZ6rnPZkdpG5tItGX7NdrgL4tQ2P
pszb24vXKy4jK5XbrabtqrL+X0wrFfPzEXOZNhx+Cbd7hYQaRTLGOaPDtWbWMJOwleFbP7r76jmT
8Qb6mfVLqEwaByt9anw5CDOmPlXkwSh3AqReWJRw6R4Wfv5P+fbTA7aWf9UPitdyEMGzVOlxDkaL
Fc29+ua3SYp++J0KmqTDw5oxsPCm4xffgkxt3981PRfeSh1L7c99j0v9QYsPRJGjNLR/zsjKXkbj
d+mmPnKhmtCaiTHvdbim9lzUvqFF22lD1VeyhZTlIgyxvnEyrmn5jViWsnU7bUVCOBRsb66jbveO
aO1qZR55zwK2MRLavQ3P7/SrDaTQhbE8mmsGrGXODHVHNKOKlb+uO4erz8nTz0EE67ORqOZKsfxe
0VMh3vHTptKTtmHpCnVh8ucbb0Mz25NlqHoon0LKvxZHuHqyBP0A6EBrhnYocyioA/zzR26rD8k1
OT0aZAHg9qVpEzlpqH4aBoePyv4uOqYvB65uv6c7e8dCP7r4bPGinwJ6ZeYa/d5yhJQCRvQ7KxTk
MFSKTHNAngYyHPih3Tqtu5Co/ZRKmJnA49syN2Dtsh8kvKRvm6WUZIWdxfnISkhZ+BM2CnNJKxCS
5czZJxLtibp/1WW5Kp3tcO+RHAC/6HTeo0zkkpDZyofmP6ni4QUWURV0PpG52hGsFcfP62IoRau1
35ERu5ZkJ38LncBfGZNp8+o7tXRKfYKPgAIpOfQB/2OO8ECFb+yMFJgP+2EGPGZCjQpKhuUqyMnE
8l/cPwErd4erpB1Y3ArxAGZWgdQLBgBIifhe7I3XePp1UPyNe8BVMz7bdM9ymEF/zrK6mXigyDyY
kWu7owd3hHsw9AAR9t4Q+/Ysd9PrCtjChQeFbIy3OTMhNLYBUwGE48abjzD+L/I6r/mOoCoOH9+c
5xZtSa3R83AoetaTsa3kFaTtrd3vaNmG9Rgx8KcSC2Wq8VnL56mz/CM63GNPA58qQP7F0gNxo9VT
JPBAUEw9LaN7/2uPpkQGkA+V/BSocDrDyH1UaP+bgWo2PUJlaCb5GqJoId29BV5wWGpv1cZ0Fho9
wCQCHUhnCluc+hLllQ+Glbnc7bsqywSJlcKCeexhmUmSTBbST0fr+U12y4TRxp103WD4iemOsxNU
J5wZ56Y+qxcvnHjFLWC3aWU9M44bJYmHawrKEZF+jQtZkPaSfPwzqjG4E0bUW72sbyOvjk4sZLl0
zc+inYyEo7XXXMQGcN2gf3B/ows6GQokZrPvhmJMTd8ZAMGBD9/YBrcHFGLKcjtnkbVEVmx2/yB/
QzTOekm4z86Ri2GAJ2APMS1uQ24nUqFmWUYB8pKYdLyUlrT3EgPkzSGiPiefrd8RCfczszv9P//7
3kNZwb4ZYEOcWO29xJRV18IKQ9mLVLlCOiGDv9yiBQ4BHHZwD7G01bE3p/s1uWr4cDLN0Rvml4FK
cC2L+OXHpsdpuehA5I0uZCMTLO5On38pivUVFkKVfinc9+H3TU4C+efcIh6yrxeW6myXiCxitY88
ujMrZf2pXda2h5vxHCiXwhWy3i/2JhlEgbmX1B8EPrr6CqOKZgJCU0l2a1xTIHXuyCtU7xVS7YU8
BQC4Xnk9PituaZPgQNVOV3sgxrsy+Su2v65rg7zQ4aFUKZMey6oAfhjrWHbVG+tXUufnJ5likL3f
ztrWPHaLamwKAvzhdUpZB2oCLRHcrPi/52I1frGPsqQbZ05rbf44RcEbTV4TWJVuL4IzhFBx+Ku1
QwFzYOJpCnRmj4C1d954VcUiW98vD1IhU0jXFkLxQOA+Eq7Rme556HLlo4EykCe4uzAoxhXyHf9J
i2m71NJI0VplkMm6ZhvLubRGNOlULZ1SlGJvVhLAlzLgRKKWUWuUUHU4ipZ1WWeFxGcqM7J20qD2
4APXlLMZeG2yVhbmIz090W5jEvdYAvd79rzJH4lf0rapZUlMuvMHLgiBHh7tWqr2+ff6Saod09Zt
+E/gsAlGr6dfT8SRX3YtdW25oa3Z2w3n9hmtUlXzCMLYBXB3cJXO2xmnZEmwK9c9UaM1Sdpacd82
mTO5o3dC9sHn1bwMwNv+xQqQ/kyCDFvHlzJjpD+GYWMGh/HtOjyAl3KQ6wrs4Js0/la7N9zf
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
