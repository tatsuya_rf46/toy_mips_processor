
################################################################
# This is a generated script based on design: design_3
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2020.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_gid_msg -ssname BD::TCL -id 2041 -severity "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_3_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# alu, comparer, control_unit, equal_5bit_we, equal_5bit_we, hazard_unit, logical_shift_l2_32, multiplexer_32, multiplexer_32, multiplexer_32, multiplexer_32, multiplexer_32, multiplexer_32, multiplexer_32, multiplexer_32, multiplexer_4p_32, multiplexer_4p_32, multiplexer_4p_32, multiplexer_4p_32, multiplexer_5, multiplexer_5, sign_extend_16to32

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7a35ticsg324-1L
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_3

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_gid_msg -ssname BD::TCL -id 2001 -severity "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_gid_msg -ssname BD::TCL -id 2002 -severity "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_gid_msg -ssname BD::TCL -id 2003 -severity "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_gid_msg -ssname BD::TCL -id 2004 -severity "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_gid_msg -ssname BD::TCL -id 2005 -severity "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_gid_msg -ssname BD::TCL -id 2006 -severity "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports

  # Create instance: PC, and set properties
  set PC [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 PC ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.CE {true} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $PC

  # Create instance: alu_0, and set properties
  set block_name alu
  set block_cell_name alu_0
  if { [catch {set alu_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $alu_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: c_addsub_0, and set properties
  set c_addsub_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_addsub:12.0 c_addsub_0 ]
  set_property -dict [ list \
   CONFIG.A_Type {Unsigned} \
   CONFIG.A_Width {32} \
   CONFIG.B_Constant {true} \
   CONFIG.B_Type {Unsigned} \
   CONFIG.B_Value {00000000000000000000000000000100} \
   CONFIG.B_Width {32} \
   CONFIG.CE {false} \
   CONFIG.Latency {0} \
   CONFIG.Out_Width {32} \
 ] $c_addsub_0

  # Create instance: c_addsub_1, and set properties
  set c_addsub_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_addsub:12.0 c_addsub_1 ]
  set_property -dict [ list \
   CONFIG.A_Type {Unsigned} \
   CONFIG.A_Width {32} \
   CONFIG.B_Type {Unsigned} \
   CONFIG.B_Value {00000000000000000000000000000000} \
   CONFIG.B_Width {32} \
   CONFIG.CE {false} \
   CONFIG.Latency {0} \
   CONFIG.Out_Width {32} \
 ] $c_addsub_1

  # Create instance: c_shift_ram_1, and set properties
  set c_shift_ram_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_1 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.CE {true} \
   CONFIG.CEPriority {CE_Overrides_Sync} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_1

  # Create instance: c_shift_ram_2, and set properties
  set c_shift_ram_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_2 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.CE {true} \
   CONFIG.CEPriority {CE_Overrides_Sync} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_2

  # Create instance: c_shift_ram_3, and set properties
  set c_shift_ram_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_3 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_3

  # Create instance: c_shift_ram_4, and set properties
  set c_shift_ram_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_4 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_4

  # Create instance: c_shift_ram_5, and set properties
  set c_shift_ram_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_5 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_5

  # Create instance: c_shift_ram_6, and set properties
  set c_shift_ram_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_6 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {000} \
   CONFIG.DefaultData {000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {000} \
   CONFIG.Width {3} \
 ] $c_shift_ram_6

  # Create instance: c_shift_ram_7, and set properties
  set c_shift_ram_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_7 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_7

  # Create instance: c_shift_ram_8, and set properties
  set c_shift_ram_8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_8 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_8

  # Create instance: c_shift_ram_9, and set properties
  set c_shift_ram_9 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_9 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_9

  # Create instance: c_shift_ram_10, and set properties
  set c_shift_ram_10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_10 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_10

  # Create instance: c_shift_ram_11, and set properties
  set c_shift_ram_11 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_11 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000} \
   CONFIG.DefaultData {00000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000} \
   CONFIG.Width {5} \
 ] $c_shift_ram_11

  # Create instance: c_shift_ram_12, and set properties
  set c_shift_ram_12 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_12 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000} \
   CONFIG.DefaultData {00000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000} \
   CONFIG.Width {5} \
 ] $c_shift_ram_12

  # Create instance: c_shift_ram_13, and set properties
  set c_shift_ram_13 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_13 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000} \
   CONFIG.DefaultData {00000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000} \
   CONFIG.Width {5} \
 ] $c_shift_ram_13

  # Create instance: c_shift_ram_14, and set properties
  set c_shift_ram_14 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_14 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_14

  # Create instance: c_shift_ram_15, and set properties
  set c_shift_ram_15 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_15 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_15

  # Create instance: c_shift_ram_16, and set properties
  set c_shift_ram_16 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_16 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_16

  # Create instance: c_shift_ram_17, and set properties
  set c_shift_ram_17 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_17 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_17

  # Create instance: c_shift_ram_18, and set properties
  set c_shift_ram_18 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_18 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_18

  # Create instance: c_shift_ram_19, and set properties
  set c_shift_ram_19 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_19 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_19

  # Create instance: c_shift_ram_20, and set properties
  set c_shift_ram_20 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_20 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000} \
   CONFIG.DefaultData {00000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000} \
   CONFIG.Width {5} \
 ] $c_shift_ram_20

  # Create instance: c_shift_ram_21, and set properties
  set c_shift_ram_21 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_21 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_21

  # Create instance: c_shift_ram_22, and set properties
  set c_shift_ram_22 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_22 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_22

  # Create instance: c_shift_ram_23, and set properties
  set c_shift_ram_23 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_23 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_23

  # Create instance: c_shift_ram_24, and set properties
  set c_shift_ram_24 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_24 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_24

  # Create instance: c_shift_ram_25, and set properties
  set c_shift_ram_25 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_25 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {0} \
   CONFIG.DefaultData {0} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {0} \
   CONFIG.Width {1} \
 ] $c_shift_ram_25

  # Create instance: c_shift_ram_26, and set properties
  set c_shift_ram_26 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_26 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_26

  # Create instance: c_shift_ram_27, and set properties
  set c_shift_ram_27 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_27 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_27

  # Create instance: c_shift_ram_28, and set properties
  set c_shift_ram_28 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_28 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000} \
   CONFIG.DefaultData {00000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000} \
   CONFIG.Width {5} \
 ] $c_shift_ram_28

  # Create instance: c_shift_ram_29, and set properties
  set c_shift_ram_29 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_29 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_29

  # Create instance: c_shift_ram_30, and set properties
  set c_shift_ram_30 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_30 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {false} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_30

  # Create instance: c_shift_ram_31, and set properties
  set c_shift_ram_31 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_shift_ram:12.0 c_shift_ram_31 ]
  set_property -dict [ list \
   CONFIG.AsyncInitVal {00000000000000000000000000000000} \
   CONFIG.DefaultData {00000000000000000000000000000000} \
   CONFIG.Depth {1} \
   CONFIG.OptGoal {Resources} \
   CONFIG.SCLR {true} \
   CONFIG.ShiftRegType {Fixed_Length} \
   CONFIG.SyncInitVal {00000000000000000000000000000000} \
   CONFIG.Width {32} \
 ] $c_shift_ram_31

  # Create instance: comparer_0, and set properties
  set block_name comparer
  set block_cell_name comparer_0
  if { [catch {set comparer_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $comparer_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: control_unit_0, and set properties
  set block_name control_unit
  set block_cell_name control_unit_0
  if { [catch {set control_unit_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $control_unit_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: dist_mem_gen_0, and set properties
  set dist_mem_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:dist_mem_gen:8.0 dist_mem_gen_0 ]
  set_property -dict [ list \
   CONFIG.coefficient_file {../../../../../../../src/inst.coe} \
   CONFIG.data_width {32} \
   CONFIG.depth {4096} \
   CONFIG.dual_port_address {non_registered} \
   CONFIG.input_options {non_registered} \
   CONFIG.memory_type {rom} \
   CONFIG.output_options {non_registered} \
 ] $dist_mem_gen_0

  # Create instance: dist_mem_gen_1, and set properties
  set dist_mem_gen_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:dist_mem_gen:8.0 dist_mem_gen_1 ]
  set_property -dict [ list \
   CONFIG.data_width {32} \
   CONFIG.depth {32} \
   CONFIG.memory_type {simple_dual_port_ram} \
 ] $dist_mem_gen_1

  # Create instance: dist_mem_gen_2, and set properties
  set dist_mem_gen_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:dist_mem_gen:8.0 dist_mem_gen_2 ]
  set_property -dict [ list \
   CONFIG.data_width {32} \
   CONFIG.depth {32} \
   CONFIG.memory_type {simple_dual_port_ram} \
 ] $dist_mem_gen_2

  # Create instance: dist_mem_gen_3, and set properties
  set dist_mem_gen_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:dist_mem_gen:8.0 dist_mem_gen_3 ]
  set_property -dict [ list \
   CONFIG.coefficient_file {../../../../../../../src/inst.coe} \
   CONFIG.data_width {32} \
   CONFIG.depth {65536} \
   CONFIG.dual_port_address {non_registered} \
   CONFIG.input_options {non_registered} \
   CONFIG.memory_type {single_port_ram} \
   CONFIG.output_options {non_registered} \
 ] $dist_mem_gen_3

  # Create instance: equal_5bit_we_1, and set properties
  set block_name equal_5bit_we
  set block_cell_name equal_5bit_we_1
  if { [catch {set equal_5bit_we_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $equal_5bit_we_1 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: equal_5bit_we_2, and set properties
  set block_name equal_5bit_we
  set block_cell_name equal_5bit_we_2
  if { [catch {set equal_5bit_we_2 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $equal_5bit_we_2 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: hazard_unit_0, and set properties
  set block_name hazard_unit
  set block_cell_name hazard_unit_0
  if { [catch {set hazard_unit_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $hazard_unit_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: logical_shift_l2_32_0, and set properties
  set block_name logical_shift_l2_32
  set block_cell_name logical_shift_l2_32_0
  if { [catch {set logical_shift_l2_32_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $logical_shift_l2_32_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_0, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_0
  if { [catch {set multiplexer_32_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_1, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_1
  if { [catch {set multiplexer_32_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_1 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_2, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_2
  if { [catch {set multiplexer_32_2 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_2 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_3, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_3
  if { [catch {set multiplexer_32_3 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_3 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_4, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_4
  if { [catch {set multiplexer_32_4 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_4 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_5, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_5
  if { [catch {set multiplexer_32_5 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_5 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_6, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_6
  if { [catch {set multiplexer_32_6 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_6 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_32_7, and set properties
  set block_name multiplexer_32
  set block_cell_name multiplexer_32_7
  if { [catch {set multiplexer_32_7 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_32_7 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_4p_32_0, and set properties
  set block_name multiplexer_4p_32
  set block_cell_name multiplexer_4p_32_0
  if { [catch {set multiplexer_4p_32_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_4p_32_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_4p_32_1, and set properties
  set block_name multiplexer_4p_32
  set block_cell_name multiplexer_4p_32_1
  if { [catch {set multiplexer_4p_32_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_4p_32_1 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_4p_32_2, and set properties
  set block_name multiplexer_4p_32
  set block_cell_name multiplexer_4p_32_2
  if { [catch {set multiplexer_4p_32_2 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_4p_32_2 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_4p_32_3, and set properties
  set block_name multiplexer_4p_32
  set block_cell_name multiplexer_4p_32_3
  if { [catch {set multiplexer_4p_32_3 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_4p_32_3 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_5_0, and set properties
  set block_name multiplexer_5
  set block_cell_name multiplexer_5_0
  if { [catch {set multiplexer_5_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_5_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: multiplexer_5_1, and set properties
  set block_name multiplexer_5
  set block_cell_name multiplexer_5_1
  if { [catch {set multiplexer_5_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $multiplexer_5_1 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: sign_extend_16to32_0, and set properties
  set block_name sign_extend_16to32
  set block_cell_name sign_extend_16to32_0
  if { [catch {set sign_extend_16to32_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $sign_extend_16to32_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: sim_clk_gen_0, and set properties
  set sim_clk_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:sim_clk_gen:1.0 sim_clk_gen_0 ]

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_0

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_1 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {xor} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_xorgate.png} \
 ] $util_vector_logic_1

  # Create instance: util_vector_logic_2, and set properties
  set util_vector_logic_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_2 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {or} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_orgate.png} \
 ] $util_vector_logic_2

  # Create instance: util_vector_logic_3, and set properties
  set util_vector_logic_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_3 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_3

  # Create instance: util_vector_logic_4, and set properties
  set util_vector_logic_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_4 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_4

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.IN0_WIDTH {2} \
   CONFIG.IN1_WIDTH {26} \
   CONFIG.IN2_WIDTH {4} \
   CONFIG.NUM_PORTS {3} \
 ] $xlconcat_0

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {2} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {31} \
   CONFIG.CONST_WIDTH {5} \
 ] $xlconstant_1

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {31} \
   CONFIG.DIN_TO {28} \
   CONFIG.DOUT_WIDTH {4} \
 ] $xlslice_0

  # Create instance: xlslice_1, and set properties
  set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {31} \
   CONFIG.DIN_TO {26} \
   CONFIG.DOUT_WIDTH {6} \
 ] $xlslice_1

  # Create instance: xlslice_2, and set properties
  set xlslice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {5} \
   CONFIG.DIN_TO {0} \
   CONFIG.DOUT_WIDTH {6} \
 ] $xlslice_2

  # Create instance: xlslice_3, and set properties
  set xlslice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_3 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {25} \
   CONFIG.DIN_TO {21} \
   CONFIG.DOUT_WIDTH {5} \
 ] $xlslice_3

  # Create instance: xlslice_4, and set properties
  set xlslice_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_4 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {20} \
   CONFIG.DIN_TO {16} \
   CONFIG.DOUT_WIDTH {5} \
 ] $xlslice_4

  # Create instance: xlslice_5, and set properties
  set xlslice_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_5 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {25} \
   CONFIG.DIN_TO {21} \
   CONFIG.DOUT_WIDTH {5} \
 ] $xlslice_5

  # Create instance: xlslice_6, and set properties
  set xlslice_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_6 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {15} \
   CONFIG.DIN_TO {11} \
   CONFIG.DOUT_WIDTH {5} \
 ] $xlslice_6

  # Create instance: xlslice_7, and set properties
  set xlslice_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_7 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {20} \
   CONFIG.DIN_TO {16} \
   CONFIG.DOUT_WIDTH {5} \
 ] $xlslice_7

  # Create port connections
  connect_bd_net -net alu_0_alu_result [get_bd_pins alu_0/alu_result] [get_bd_pins c_shift_ram_18/D]
  connect_bd_net -net c_addsub_0_S [get_bd_pins c_addsub_0/S] [get_bd_pins c_shift_ram_2/D] [get_bd_pins multiplexer_32_0/inp1]
  connect_bd_net -net c_addsub_1_S [get_bd_pins c_addsub_1/S] [get_bd_pins multiplexer_32_0/inp2]
  connect_bd_net -net c_shift_ram_0_Q [get_bd_pins PC/Q] [get_bd_pins c_addsub_0/A] [get_bd_pins dist_mem_gen_0/a] [get_bd_pins xlslice_0/Din]
  connect_bd_net -net c_shift_ram_10_Q [get_bd_pins c_shift_ram_10/Q] [get_bd_pins multiplexer_4p_32_1/in1]
  connect_bd_net -net c_shift_ram_11_Q [get_bd_pins c_shift_ram_11/Q] [get_bd_pins hazard_unit_0/RsE]
  connect_bd_net -net c_shift_ram_12_Q [get_bd_pins c_shift_ram_12/Q] [get_bd_pins hazard_unit_0/RtE] [get_bd_pins multiplexer_5_1/inp1]
  connect_bd_net -net c_shift_ram_13_Q [get_bd_pins c_shift_ram_13/Q] [get_bd_pins multiplexer_5_1/inp2]
  connect_bd_net -net c_shift_ram_14_Q [get_bd_pins c_shift_ram_14/Q] [get_bd_pins multiplexer_32_5/inp2]
  connect_bd_net -net c_shift_ram_15_Q [get_bd_pins c_shift_ram_15/Q] [get_bd_pins c_shift_ram_25/D] [get_bd_pins hazard_unit_0/RegWriteM]
  connect_bd_net -net c_shift_ram_16_Q [get_bd_pins c_shift_ram_16/Q] [get_bd_pins c_shift_ram_24/D]
  connect_bd_net -net c_shift_ram_17_Q [get_bd_pins c_shift_ram_17/Q] [get_bd_pins dist_mem_gen_3/we]
  connect_bd_net -net c_shift_ram_18_Q [get_bd_pins c_shift_ram_18/Q] [get_bd_pins c_shift_ram_27/D] [get_bd_pins dist_mem_gen_3/a] [get_bd_pins multiplexer_4p_32_0/in3] [get_bd_pins multiplexer_4p_32_1/in3] [get_bd_pins multiplexer_4p_32_2/in2] [get_bd_pins multiplexer_4p_32_3/in2]
  connect_bd_net -net c_shift_ram_19_Q [get_bd_pins c_shift_ram_19/Q] [get_bd_pins dist_mem_gen_3/d]
  connect_bd_net -net c_shift_ram_1_Q [get_bd_pins c_shift_ram_1/Q] [get_bd_pins sign_extend_16to32_0/in16] [get_bd_pins xlconcat_0/In1] [get_bd_pins xlslice_1/Din] [get_bd_pins xlslice_2/Din] [get_bd_pins xlslice_3/Din] [get_bd_pins xlslice_4/Din] [get_bd_pins xlslice_5/Din] [get_bd_pins xlslice_6/Din] [get_bd_pins xlslice_7/Din]
  connect_bd_net -net c_shift_ram_20_Q [get_bd_pins c_shift_ram_20/Q] [get_bd_pins c_shift_ram_28/D] [get_bd_pins hazard_unit_0/WriteRegM]
  connect_bd_net -net c_shift_ram_21_Q [get_bd_pins c_shift_ram_21/Q] [get_bd_pins c_shift_ram_22/D]
  connect_bd_net -net c_shift_ram_22_Q [get_bd_pins c_shift_ram_22/Q] [get_bd_pins c_shift_ram_23/D] [get_bd_pins hazard_unit_0/LeavelinkM]
  connect_bd_net -net c_shift_ram_23_Q [get_bd_pins c_shift_ram_23/Q] [get_bd_pins hazard_unit_0/LeavelinkW] [get_bd_pins multiplexer_32_3/selector] [get_bd_pins multiplexer_5_0/selector]
  connect_bd_net -net c_shift_ram_24_Q [get_bd_pins c_shift_ram_24/Q] [get_bd_pins multiplexer_32_7/selector]
  connect_bd_net -net c_shift_ram_25_Q [get_bd_pins c_shift_ram_25/Q] [get_bd_pins dist_mem_gen_1/we] [get_bd_pins dist_mem_gen_2/we] [get_bd_pins equal_5bit_we_1/we] [get_bd_pins equal_5bit_we_2/we] [get_bd_pins hazard_unit_0/RegWriteW]
  connect_bd_net -net c_shift_ram_26_Q [get_bd_pins c_shift_ram_26/Q] [get_bd_pins multiplexer_32_7/inp2]
  connect_bd_net -net c_shift_ram_27_Q [get_bd_pins c_shift_ram_27/Q] [get_bd_pins multiplexer_32_7/inp1]
  connect_bd_net -net c_shift_ram_28_Q [get_bd_pins c_shift_ram_28/Q] [get_bd_pins hazard_unit_0/WriteRegW] [get_bd_pins multiplexer_5_0/inp1]
  connect_bd_net -net c_shift_ram_29_Q [get_bd_pins c_shift_ram_29/Q] [get_bd_pins c_shift_ram_30/D] [get_bd_pins multiplexer_4p_32_2/in3] [get_bd_pins multiplexer_4p_32_3/in3]
  connect_bd_net -net c_shift_ram_2_Q [get_bd_pins c_addsub_1/B] [get_bd_pins c_shift_ram_2/Q] [get_bd_pins c_shift_ram_31/D]
  connect_bd_net -net c_shift_ram_30_Q [get_bd_pins c_shift_ram_30/Q] [get_bd_pins multiplexer_32_3/inp2] [get_bd_pins multiplexer_4p_32_0/in4] [get_bd_pins multiplexer_4p_32_1/in4]
  connect_bd_net -net c_shift_ram_31_Q [get_bd_pins c_shift_ram_29/D] [get_bd_pins c_shift_ram_31/Q]
  connect_bd_net -net c_shift_ram_3_Q [get_bd_pins c_shift_ram_15/D] [get_bd_pins c_shift_ram_3/Q] [get_bd_pins hazard_unit_0/RegWriteE]
  connect_bd_net -net c_shift_ram_4_Q [get_bd_pins c_shift_ram_16/D] [get_bd_pins c_shift_ram_4/Q] [get_bd_pins hazard_unit_0/MemtoRegE]
  connect_bd_net -net c_shift_ram_5_Q [get_bd_pins c_shift_ram_17/D] [get_bd_pins c_shift_ram_5/Q]
  connect_bd_net -net c_shift_ram_6_Q [get_bd_pins alu_0/alu_control] [get_bd_pins c_shift_ram_6/Q]
  connect_bd_net -net c_shift_ram_7_Q [get_bd_pins c_shift_ram_7/Q] [get_bd_pins multiplexer_32_5/selector]
  connect_bd_net -net c_shift_ram_8_Q [get_bd_pins c_shift_ram_8/Q] [get_bd_pins multiplexer_5_1/selector]
  connect_bd_net -net c_shift_ram_9_Q [get_bd_pins c_shift_ram_9/Q] [get_bd_pins multiplexer_4p_32_0/in1]
  connect_bd_net -net comparer_0_equality [get_bd_pins comparer_0/equality] [get_bd_pins util_vector_logic_1/Op2]
  connect_bd_net -net control_unit_0_ALUControl [get_bd_pins c_shift_ram_6/D] [get_bd_pins control_unit_0/ALUControl]
  connect_bd_net -net control_unit_0_ALUSrc [get_bd_pins c_shift_ram_7/D] [get_bd_pins control_unit_0/ALUSrc]
  connect_bd_net -net control_unit_0_Branch [get_bd_pins control_unit_0/Branch] [get_bd_pins hazard_unit_0/BranchD] [get_bd_pins util_vector_logic_0/Op1]
  connect_bd_net -net control_unit_0_Jump [get_bd_pins control_unit_0/Jump] [get_bd_pins multiplexer_32_1/selector] [get_bd_pins util_vector_logic_2/Op2]
  connect_bd_net -net control_unit_0_LeaveLink [get_bd_pins c_shift_ram_21/D] [get_bd_pins control_unit_0/LeaveLink]
  connect_bd_net -net control_unit_0_MemWrite [get_bd_pins c_shift_ram_5/D] [get_bd_pins control_unit_0/MemWrite]
  connect_bd_net -net control_unit_0_MemtoReg [get_bd_pins c_shift_ram_4/D] [get_bd_pins control_unit_0/MemtoReg]
  connect_bd_net -net control_unit_0_RegDst [get_bd_pins c_shift_ram_8/D] [get_bd_pins control_unit_0/RegDst]
  connect_bd_net -net control_unit_0_RegWrite [get_bd_pins c_shift_ram_3/D] [get_bd_pins control_unit_0/RegWrite]
  connect_bd_net -net control_unit_0_RegtoPC [get_bd_pins control_unit_0/RegtoPC] [get_bd_pins hazard_unit_0/RegtoPCD] [get_bd_pins multiplexer_32_2/selector]
  connect_bd_net -net control_unit_0_ToggleEqual [get_bd_pins control_unit_0/ToggleEqual] [get_bd_pins util_vector_logic_1/Op1]
  connect_bd_net -net dist_mem_gen_0_spo [get_bd_pins c_shift_ram_1/D] [get_bd_pins dist_mem_gen_0/spo]
  connect_bd_net -net dist_mem_gen_1_dpo [get_bd_pins dist_mem_gen_1/dpo] [get_bd_pins multiplexer_32_4/inp1]
  connect_bd_net -net dist_mem_gen_2_dpo [get_bd_pins dist_mem_gen_2/dpo] [get_bd_pins multiplexer_32_6/inp1]
  connect_bd_net -net dist_mem_gen_3_spo [get_bd_pins c_shift_ram_26/D] [get_bd_pins dist_mem_gen_3/spo]
  connect_bd_net -net equal_5bit_we_1_outp [get_bd_pins equal_5bit_we_1/outp] [get_bd_pins multiplexer_32_4/selector]
  connect_bd_net -net equal_5bit_we_2_outp [get_bd_pins equal_5bit_we_2/outp] [get_bd_pins multiplexer_32_6/selector]
  connect_bd_net -net hazard_unit_0_FlushE [get_bd_pins c_shift_ram_10/SCLR] [get_bd_pins c_shift_ram_11/SCLR] [get_bd_pins c_shift_ram_12/SCLR] [get_bd_pins c_shift_ram_13/SCLR] [get_bd_pins c_shift_ram_14/SCLR] [get_bd_pins c_shift_ram_21/SCLR] [get_bd_pins c_shift_ram_3/SCLR] [get_bd_pins c_shift_ram_31/SCLR] [get_bd_pins c_shift_ram_4/SCLR] [get_bd_pins c_shift_ram_5/SCLR] [get_bd_pins c_shift_ram_6/SCLR] [get_bd_pins c_shift_ram_7/SCLR] [get_bd_pins c_shift_ram_8/SCLR] [get_bd_pins c_shift_ram_9/SCLR] [get_bd_pins hazard_unit_0/FlushE]
  connect_bd_net -net hazard_unit_0_ForwardAD [get_bd_pins hazard_unit_0/ForwardAD] [get_bd_pins multiplexer_4p_32_2/selector]
  connect_bd_net -net hazard_unit_0_ForwardAE [get_bd_pins hazard_unit_0/ForwardAE] [get_bd_pins multiplexer_4p_32_0/selector]
  connect_bd_net -net hazard_unit_0_ForwardBD [get_bd_pins hazard_unit_0/ForwardBD] [get_bd_pins multiplexer_4p_32_3/selector]
  connect_bd_net -net hazard_unit_0_ForwardBE [get_bd_pins hazard_unit_0/ForwardBE] [get_bd_pins multiplexer_4p_32_1/selector]
  connect_bd_net -net hazard_unit_0_StallD [get_bd_pins hazard_unit_0/StallD] [get_bd_pins util_vector_logic_4/Op1]
  connect_bd_net -net hazard_unit_0_StallF [get_bd_pins hazard_unit_0/StallF] [get_bd_pins util_vector_logic_3/Op1]
  connect_bd_net -net logical_shift_l2_32_0_shifted [get_bd_pins c_addsub_1/A] [get_bd_pins logical_shift_l2_32_0/shifted]
  connect_bd_net -net multiplexer_32_0_y [get_bd_pins multiplexer_32_0/y] [get_bd_pins multiplexer_32_1/inp1]
  connect_bd_net -net multiplexer_32_1_y [get_bd_pins multiplexer_32_1/y] [get_bd_pins multiplexer_32_2/inp1]
  connect_bd_net -net multiplexer_32_2_y [get_bd_pins PC/D] [get_bd_pins multiplexer_32_2/y]
  connect_bd_net -net multiplexer_32_3_y [get_bd_pins dist_mem_gen_1/d] [get_bd_pins dist_mem_gen_2/d] [get_bd_pins multiplexer_32_3/y] [get_bd_pins multiplexer_32_4/inp2] [get_bd_pins multiplexer_32_6/inp2]
  connect_bd_net -net multiplexer_32_4_y [get_bd_pins c_shift_ram_9/D] [get_bd_pins multiplexer_32_4/y] [get_bd_pins multiplexer_4p_32_2/in1]
  connect_bd_net -net multiplexer_32_5_y [get_bd_pins alu_0/srcB] [get_bd_pins multiplexer_32_5/y]
  connect_bd_net -net multiplexer_32_6_y [get_bd_pins c_shift_ram_20/D] [get_bd_pins hazard_unit_0/WriteRegE] [get_bd_pins multiplexer_5_1/y]
  connect_bd_net -net multiplexer_32_6_y1 [get_bd_pins c_shift_ram_10/D] [get_bd_pins multiplexer_32_6/y] [get_bd_pins multiplexer_4p_32_3/in1]
  connect_bd_net -net multiplexer_32_7_y [get_bd_pins multiplexer_32_3/inp1] [get_bd_pins multiplexer_32_7/y] [get_bd_pins multiplexer_4p_32_0/in2] [get_bd_pins multiplexer_4p_32_1/in2]
  connect_bd_net -net multiplexer_4p_32_0_y [get_bd_pins alu_0/srcA] [get_bd_pins multiplexer_4p_32_0/y]
  connect_bd_net -net multiplexer_4p_32_1_y [get_bd_pins c_shift_ram_19/D] [get_bd_pins multiplexer_32_5/inp1] [get_bd_pins multiplexer_4p_32_1/y]
  connect_bd_net -net multiplexer_4p_32_2_y [get_bd_pins comparer_0/srcA] [get_bd_pins multiplexer_32_2/inp2] [get_bd_pins multiplexer_4p_32_2/y]
  connect_bd_net -net multiplexer_4p_32_3_y [get_bd_pins comparer_0/srcB] [get_bd_pins multiplexer_4p_32_3/y]
  connect_bd_net -net multiplexer_5_0_y [get_bd_pins dist_mem_gen_1/a] [get_bd_pins dist_mem_gen_2/a] [get_bd_pins equal_5bit_we_1/inp1] [get_bd_pins equal_5bit_we_2/inp1] [get_bd_pins multiplexer_5_0/y]
  connect_bd_net -net sign_extend_16to32_0_out32 [get_bd_pins c_shift_ram_14/D] [get_bd_pins logical_shift_l2_32_0/source] [get_bd_pins sign_extend_16to32_0/out32]
  connect_bd_net -net sim_clk_gen_0_clk [get_bd_pins PC/CLK] [get_bd_pins c_shift_ram_1/CLK] [get_bd_pins c_shift_ram_10/CLK] [get_bd_pins c_shift_ram_11/CLK] [get_bd_pins c_shift_ram_12/CLK] [get_bd_pins c_shift_ram_13/CLK] [get_bd_pins c_shift_ram_14/CLK] [get_bd_pins c_shift_ram_15/CLK] [get_bd_pins c_shift_ram_16/CLK] [get_bd_pins c_shift_ram_17/CLK] [get_bd_pins c_shift_ram_18/CLK] [get_bd_pins c_shift_ram_19/CLK] [get_bd_pins c_shift_ram_2/CLK] [get_bd_pins c_shift_ram_20/CLK] [get_bd_pins c_shift_ram_21/CLK] [get_bd_pins c_shift_ram_22/CLK] [get_bd_pins c_shift_ram_23/CLK] [get_bd_pins c_shift_ram_24/CLK] [get_bd_pins c_shift_ram_25/CLK] [get_bd_pins c_shift_ram_26/CLK] [get_bd_pins c_shift_ram_27/CLK] [get_bd_pins c_shift_ram_28/CLK] [get_bd_pins c_shift_ram_29/CLK] [get_bd_pins c_shift_ram_3/CLK] [get_bd_pins c_shift_ram_30/CLK] [get_bd_pins c_shift_ram_31/CLK] [get_bd_pins c_shift_ram_4/CLK] [get_bd_pins c_shift_ram_5/CLK] [get_bd_pins c_shift_ram_6/CLK] [get_bd_pins c_shift_ram_7/CLK] [get_bd_pins c_shift_ram_8/CLK] [get_bd_pins c_shift_ram_9/CLK] [get_bd_pins dist_mem_gen_1/clk] [get_bd_pins dist_mem_gen_2/clk] [get_bd_pins dist_mem_gen_3/clk] [get_bd_pins sim_clk_gen_0/clk]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins multiplexer_32_0/selector] [get_bd_pins util_vector_logic_0/Res] [get_bd_pins util_vector_logic_2/Op1]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins util_vector_logic_0/Op2] [get_bd_pins util_vector_logic_1/Res]
  connect_bd_net -net util_vector_logic_2_Res [get_bd_pins c_shift_ram_1/SCLR] [get_bd_pins c_shift_ram_2/SCLR] [get_bd_pins util_vector_logic_2/Res]
  connect_bd_net -net util_vector_logic_3_Res [get_bd_pins PC/CE] [get_bd_pins util_vector_logic_3/Res]
  connect_bd_net -net util_vector_logic_4_Res [get_bd_pins c_shift_ram_1/CE] [get_bd_pins c_shift_ram_2/CE] [get_bd_pins util_vector_logic_4/Res]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins multiplexer_32_1/inp2] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins xlconcat_0/In0] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins multiplexer_5_0/inp2] [get_bd_pins xlconstant_1/dout]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins xlconcat_0/In2] [get_bd_pins xlslice_0/Dout]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins control_unit_0/Op] [get_bd_pins xlslice_1/Dout]
  connect_bd_net -net xlslice_2_Dout [get_bd_pins control_unit_0/Funct] [get_bd_pins xlslice_2/Dout]
  connect_bd_net -net xlslice_3_Dout [get_bd_pins dist_mem_gen_1/dpra] [get_bd_pins equal_5bit_we_1/inp2] [get_bd_pins xlslice_3/Dout]
  connect_bd_net -net xlslice_4_Dout [get_bd_pins dist_mem_gen_2/dpra] [get_bd_pins equal_5bit_we_2/inp2] [get_bd_pins xlslice_4/Dout]
  connect_bd_net -net xlslice_5_Dout [get_bd_pins c_shift_ram_11/D] [get_bd_pins hazard_unit_0/RsD] [get_bd_pins xlslice_5/Dout]
  connect_bd_net -net xlslice_6_Dout [get_bd_pins c_shift_ram_13/D] [get_bd_pins xlslice_6/Dout]
  connect_bd_net -net xlslice_7_Dout [get_bd_pins c_shift_ram_12/D] [get_bd_pins hazard_unit_0/RtD] [get_bd_pins xlslice_7/Dout]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


