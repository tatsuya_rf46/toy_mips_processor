// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:02:40 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_alu_0_0/design_3_alu_0_0_stub.v
// Design      : design_3_alu_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "alu,Vivado 2020.1" *)
module design_3_alu_0_0(alu_control, srcA, srcB, alu_result, zero)
/* synthesis syn_black_box black_box_pad_pin="alu_control[2:0],srcA[31:0],srcB[31:0],alu_result[31:0],zero" */;
  input [2:0]alu_control;
  input [31:0]srcA;
  input [31:0]srcB;
  output [31:0]alu_result;
  output zero;
endmodule
