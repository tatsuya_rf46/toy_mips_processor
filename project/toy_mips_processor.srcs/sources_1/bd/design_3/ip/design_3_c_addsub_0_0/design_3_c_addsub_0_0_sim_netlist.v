// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:21 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_addsub_0_0/design_3_c_addsub_0_0_sim_netlist.v
// Design      : design_3_c_addsub_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_addsub_0_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_addsub_0_0
   (A,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "1" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000100" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_addsub_0_0_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "1" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000100" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "0" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_addsub_v12_0_14" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_addsub_0_0_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "1" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000100" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_addsub_0_0_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
PKDCQPK9erPNd3OTdutMc7OpVW1KMj13N4QY1LzlyglX2V5ZjNi//GGEA8Tcm/T+58pQNbIlZRmM
R/QXzuGBad/lNs3swvr2Ipjbwmgx99QFefwTldK/nFVbV/yJG6XMcy3cIZHNN3ZsWsJU0kaFaMd3
sSu3CNnSm6DVTYUcME6HdNLJcRLYQPvE8Z3+CbyhKGivi3K16k3jNUq5MpxPbkSbNzRtJpBhKiZU
VWcNcVTFbG9UHoxcInoRNHmEo6u35yGxHMp9rncqL1Bt5QqDmz2Xy/eepcUv5qS0phiFuuun+i4A
yQoLDq3lcZoGrvxkyeW683mxnGo7VrLAjS2hIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XXlfsMF0SJGTmaSMK0V+83f8fYVMap3Tch/NVfip9o7Wr3j18Y2x0c6NrPbybKQf8aiKAT+Ic3y2
n6PYEIm1U8a7DW0BsavCWfgB+tJiy6V/mrDqkTwoGEOF7+7vD7e548IKxSBzxG8EGnZj1sk1kso3
1mTuZBFMz8gb+66QZWPbN+FlSF683JYGFrG+tWluEi+KPpKSiRRY0ToxBF6dnpyIoekq3FlLZYoI
vIvURfRMY2eH1KW7pbvXDoXhSVdakTRA0RIJ0iWGa6RgP7lZc1OHDh4hQl1vbGDnkq2/nAXl7LHy
m2HVrIkMBfeLAUZIALzEEsIbF3Mn1tjc5cDNCw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 6720)
`pragma protect data_block
jq4pYZcl9k0kjpd6UlkykpaewWIi/EEUG9IZU20smpHcNp9xcOkkXuR70kLdlwOE9iVfSI8YPKPJ
E9P242sh6Oqh/cWUkk2Fb+pT4yhLz+K6A2wcNiHaoKZXajlTCI5wwK0aFEYZpYs0fcTBYcEILjgG
P4WmGRn8wTrE7ZNvBhE75iDOGkwCrgX81X7HAMuvc2vtgT+XG3PCNdWFJViiSvkm54vXEnA3qnSt
Ek8c/aJQWezbcXlJEPcLJuD/IIqkR3On4alEIigVynnMfUSz0UGZ4pS+0SjBle0MjXeDw+dbszgi
/rNXuLnB0621mzBqmhkmEtyItII0XTZyPI9bIKojBIrBiGnFZt0Pa5NY5igIluGhA4hon4IyiwP0
Mrz4R39CT3tASLxbFJ8jHtLWB2NiJddtRGnK/BhhKQu/nhxvM+vCpLxPx/wCDHbqahsAFAgWAawy
V8j4tEavNnb2elWQQEXRqAcaT/HSG7AP38JAOkVGZKnXdlm88dzsPTthyoknTcAI0RuM9imsZPtU
pwjtqEaB9nqiWWyf9bfpkGyjRGASX/sXg2ExtHT/eadhVhQn5/xT+aTACgZU/LnU4aYNp5Ky3NO3
d5nTB+A/MLT7qrqfGly81PbTVClR+Q8HzZfVvdAuXLh+g+LRt0IAffsY5dz2FQRNimOQvawn2ICI
/WdD43ufXDXb0Q40ZRD1zVDbYHfMFMh6wSi2q4VuPrnyrC98JaCFpJ03IWInMtDf193958D4YBli
EDL9/CrCJ0xxZYzF+749qUtAzCUmf/bVQyZC0J7j9uDgeuNWs8NhmmmVLlZM+zF2zx8BGdgLSqnb
JK7bVJn0Zb4Nn8nSBwMSDu3EbpCeqlXjvo3/WMjVQnqIcZrnPUN1tgJK9tP16EnP6g6M68jCxpZM
DsMmuTvfjumwhzAdCyRgNzS5LLBMS9QO7QB81OQIjTT2xlVPV4wWfrvZ2WyhB/ajG5/7Yb3PTFJv
5jImobXpRJhNCChTYVGtU1OdQZWYKb0JRXKJ+RUwhjOAFR60Ink+PECdj3Amxr/LIpI2nzuYnE1i
/VhqwyIBOSRxzCmkNJoFNadnguNYicOlOZtpZqCh1GTWwl1BnlFu9jj/V+8rvKhWw00BdRdCrrvq
scaXpgZGTgNDi1wr+nZWYLHaxvLk8jyFtMarJ3rlEAPyRypYqsz+4i/OMIapuQy5WppclreZrteY
TcKq7GUM9AAkGJkh79zKLEf4bpSOqT6HtSI+JW2OpVgG6m3KXCuSkmB3XsPCj3yiW+zKr9TeuzEf
Y+4Ck1MfD2su9WPow2hsNkE2FnUPHI2p9NxjQLDHNpJDQVgS2tRcsbuww2JAIc0ZAYzJ6DPKE9tJ
b8bYL0Q9kMLPxB2XufpGcTdyC47eWiGAieGSievSx2ditBY5I2WDvisISWKvBSZXcXhkbpHtfhQ9
7W7/CEd7iGmQLcn5EHAVn52ASudeKW3Ukxj67oyqsMPvz6zjFziZeZFSdLHhFlORduA3ASrevMiO
fiTIGAppVP4abnPpXoy/yVzEEqRcwMwEzujy1iCA5l6/5fDdKQfwJ7q0jz1SDmHRVvqb7Xzq7WiZ
LqYMLa2bPDOJ4F0SpeJ4sZ2X9Q+6q7afkmMwa0rBuKZp15qvR7WVP+uuzY8TnE2qzBbUGNsCCH6W
1jyBbZD2yPVo4zXKPk/h2mX0r5mKvuU/fw4fyE/SMIzkRpT6dCac3oSFiM9h1Q0YhPJNQ1QASAQm
ZwihSNWI3v2TRibxSrm3QdZgRq+Wadln97HaP3MUa4bDJhdKCYYAt86kca2Z3gZzyViNTNKWmDw2
jxx+Ru96ysCkUf0ipryEVJT1tbI6/6LNfMuykGYzHj4Z5kNv71J47hbERgP6sDdY0LxDWPsk9pRX
UM09HTgKfi0Fu8OJA3HhW7uyfz9T/w/gO3J4SFnRpr+WYkMxngGqkCZteOMxbcKBVgVXO2TB1JcM
SoBvdi4Kt7iYv9rhUAqqkc/0PtqBXRBF/1mYg5erHB9cP7O9A3wZf/8bkFDylp0cs+baKQleHnOX
pOpdBvW11UIthbaqt020VW/4k5DyUk5eS0FhirK2TkL/5HmPthh7rfSK8yE1fSTmfaWOYoi80HX6
G6QjYJq7BJZats9/yJ5i1N/Za3v8mmnJMYeog/uAUbrMebnCOTdMR5TR/P6HCt4TZVUwr65OBQam
G65b3kxP51va+kqmCQTMLEGbiVHTB48zHMfKftpwo0sT3wgWWW+SYiQQG6+mqSHwflfq9/V2Chp6
1EJKyUytyuRVRNJJ0bGtFqZW0xHdz70Z0ikxcknVPnq+kiEODXmW0Rxbn29Hu0+sOvgExUOF0x7H
ygwM+A9+UxZUn9uW1zZ7Ndazly3bmxIY0Qe/yRGBBF74N1dl8j2QBibUBTEFl1yL53L6RwUX5lks
VTfgatisNlNZR6eB5SsGnYP9h5P+u/7mdvnlGDeYygKGjrG/i884iwZfbMjhxeUGiK82/ht0GFgo
GUQ0TC5Ti6Ptz0PVD2s1CXmofHes3DOQIJnDH4BRRZ1csqLboN3mYhwnTd+wlDE5SbMPnjQQRGjX
VOOJp/yAnSYB81KnIeGo109r8UN8SOaKhtqNtYLFC8RKjyyQ9Q4NzkJpm5xfcIJCmifZZQSvu9He
mtKytitdRBnr78921rTKs7lqrehcHlIlSB+1VOYTOYiEpZpCSoTS8xWjxAYb6y7AMbIbhNIF9zQ0
z1Ub6VQ6EyeMtS9U45JtSUG8ruypZarH0Klv5mNeK05EAraGhkTzQO6BEhskr/aVP59+C8QIbHeg
KgldAOv9VlZpH1wdKmQGvBYKNvRYdlmbDgPNMntspxK6gQ3akXo4lfKdS9dTyolDvyGaTIPX930r
0IyuoOig7Hqy7R4FkL+CJeNukUzmNiYz99VWrlnftbPXljKSg0nr7Ms7G6mqbNt3foCL7nfPAAkU
A0jZ4OkYJr8mno1fuvmrosS8ZjcHvpAO68chn70C9uoTfpibmCDpl2qeHpI1rs1nZU8mSoC6YX0V
lzfbOfeby0gQOhOyiUwtpGIx7hp/LW5mmZBovphP7lk70C6vrpxLQB+Fr0gGCMtpPKwmX/LrjsAr
/UYf4f6mokWMUm9LaW1BbdmFrc+jL6d9bG68SvRG3VsT2nXwcg5KgExTJLDCAeRUy2cYcfAEvkWA
UwuFJn8ZZ2uFDWkgGkwT3jsG9G93j35YoHlTvSXr8aTuuER/4Yq4Rz0MIEIqb6kAErSXt+xWWA5n
pQU6T2sqxsujOekB24+XaBhwBat0rilGVrIUGjVk1KTg9LM3WLJLrihFAReCGrv6VC4zMrw+YJGB
uEl/DJ3dorKPFeMxYlB1O3z4c3DCW9amcb7sKYpNvz31CQqsFPsgWm7RpvNvIj0y1AyvxiUaWUWA
2KcbJLydvr4dCYWfdtWE6kzD3gAAC7JQCuwcVEio4dsyEjAIwP0kuX0qx7IARQMgvI6IMIdrRyc/
PaTLCZnffP1eYMe/gfzLK8EyhE15/GM+Fdu85qwF26aAdPGWm7rBs+G46xaXEYnqwktoe7QSrODU
CR+nVY8u+5OBGOnF5WAx8fVtfKRvVr75zcRlhzKxKf+89LDxKBDPFccq+JFO+H+g3HGSxOldZ7GP
Bi30DmZG0oZUQragZ21FkY6d0nGO/fKaTQBwwJuVR04fpiwNLx7/I2hXmSBgr7ficvxYUuFOhev9
EdlE5MgXiDYrcPWIwu0F1aY5NMlc/71FhOVORJ6eqLRB3RJzAOXroAali6jY1rxnRh7PQglRJSsm
8EudOlhLURAL2CxdU9lgV66mUSS+iYr9JokD+NCKktHMtFMMW/flbOh2Sp+6LdIkk7xB0TSuSUdb
blYUWHH+fjflmUX/VsirmBQPnZfL3GZydBb780+fqWhlAVXiWtO1rXtp1aXYisyqBh29oDD0ogGv
lzEUBwoMLPjnR1LpFNXHj3NtS57EKel8cMiFd0vx2NeIJtv1vi5V73n4GUiY1uA7RDrRbTFq69c+
wqEkrgqFwDo20DXyEMG2L5VuwUAs68ILpOGkK7YuEBzfehqlWAoi8pwSF+Im6UQsavsWnKiQRsKE
fyKM6UZ+b7uZcTYWbayGVMD+HrMucwImzxWta0C1cHrsv8Y1VEFvZbzJEG5JvIBXu+lqiokXKjv5
sc6s2kjoy9PfocG9OJ/1iP9OwF0yAOWhWt0zUz8MtMBqjaJ+UVSvSdm7h2Ljhtfec2rFnrTgHlj0
+1EGMYthcUbXH1zIAE2ASYpadQM0nnOip4ujgrZZrl8sHcuBIWExRBFcaTzpx+H2B8CbgkFGdpbm
ULCGCbqsZYV0XYKrVD4NLkaojEeTTk9FH71vax6HFL7b8yuQNdi38dPqLR8L+Xg9kD20UhGhkWrH
WtIWXvRmFi0q0ti7pHuyji1tmt04/0pTG13HthjNuVqyAHL1D62uX64a8t+qWL7y24FZTqu1b8j8
k6ALd+v71mfZJljhNsl4xpK7RsZI4ObTqjVqUHocVd4UnKhc+oXfQmlUOQBFZNUzMttgxePYEwE2
AsOl652tkqVpuH/CErGQHEzrgQm7ap8EMAujMAd73Shdg6gj9PysRMUVXrL5EGfJiLXvDuL/UcBj
sLcxvOv2/epxHZA1FDZzh8VhSlq7RiN1YQ9osOV2GOvuwGd8a3bKeSyulW0Fe/ElZUN41gcUALLr
lY3UzOcKerAKVboatN4L5Z4x1m4sKyr17+eLojdYWsNBW9HuirlGQEo7+Ac9wCdFwgGYYLdKgKqd
MrtGBo/w8JW7ixLyeq8AR2SIa1xfcbF+42KQA6H8ycvrDJdPKW1GOuOugLPBwOXtU56l9MsGmyaS
ShBo2zjevS98pbzrSmcg3s0WBTzw+rybpfS6GnsvddlK4vRtJ+CfGqPQ/6VC42gFa/jpYVxDkzG4
msjzyD/WHnHxo1OyTyYcGtGxkWMrcJqq14YVI6e5VcMR1dKUv0ik95I0tpVtvyT0pX8FJdhWmxnb
xBnyoVkMi0BvRB7p0uqOGbFALzeWWrrOUYGwDMtF6yBa0HoLxrxznEORu1tl9wScxBP3FoOl/RBT
+uzTmEoTjmIhVuVlilbIzHMpgrYrnkXXfZuNc51ZI2c+N4/hlcR+5gpitQ9n1morsljtoz0iBK+Z
Hj+RUvan6B3ENBd8XOt7rwphP0qHwetSxcynTUwkaCQ8nKJIE0TkMYiiR/M1LVGxgWkHCZSNoyFw
zUzcax4V/ANZlJrMSmeXojjrfAVi3f39gzUi15BICmgAR6Dz1Gm2sXrYea1BcAyP74TUKrLbSl3I
GckJ5VbLNqHBmMUFjbhMQX1sdYTWvW9AShhT9QIJxMSDihAC0kWgomRmy+Pis4wEDHkwKvt3/zdd
tUqL/1xr0uWM1mMoDFomnEb5UL3h9ISPPr+BdbOfyg12LkR4sJIgtFV6wh1mql/7K/WrIMFkczXt
E0tfRhzBh7Ly31/WDLQOWfD9Gd2bCQjBSua0U69oCLRyR0f0kj1ZGYQZt+SpoGdDPCw64olnOOv0
v+Fh1nN8usdiVK4p/eLX1Mfnk0QVtRRR3Ykfxd6BtsnO6OuB+EZmLW3dysigPSbC7IBLoyuRaBvi
XESjqWx+r7J2rTMddB3uuy7iuLAegOZiuZl/FFvS4ZA3UVB1XSCOfVzgzGuXWhxRYyTUTv+su0Po
PABHCV+D2v4pvwzaPc7wSfUaU5lQldS/rBh+qahPZ03bejOYIxEZODcv5AA+hOQEf8l4GxMyCTKn
bDB1MQ8CCNlRZVKkrIOTDYqfUH4lg2tfTazi73noJ+Kwc5K17QhQkWQ8HEwWCNRO8nnDC/gfzgUH
VUYFhcjhMIrzULT35E7y61xj/b/X87V6xcHY6DjuSK65CsMQ0W6NoqKoW1lcrx3xecH+WdoBa60i
cI5/VaS/j8jwpVAA7k+OdNMnpIH7arySa7sdJ2twBZ+76D3/AJaGb48QQrQ05RO4dRXh7qKucWM/
ci2DfSo2KE/ggWMTYRKvit04lgS5Nw2+62tH0p6eGajwBadsLi9reqt/WqKKLWJeMb7IfZ5Xk0IO
dAhU9evLdpJnogl/HcFXQpOKWVSToVFB3SeuINk9/YBX7O8V9DiFkZoh+yopeK/3nyYvJukH5Ypj
nVtLRngTF8V09HKLFaOgfnhjPENJSjkq1EtIqav8pV1aiNG7yTzsMCp807KIBz3j3wV5PfKCEWh6
71YlVj8csN23tBtrA+8F+BrZUNpBqWO/l6ulkRWMhvvbZOZWkmNWTiJVcedu5HrQOtR70ex0g8Zg
5KJJ3zOkb82APwdLWpnJUBzdxHum2U17H/kKgGsnyRweataZ2qZt7jC69ulcK7bf0SfHgYJzaMsr
t0C1NdrrYSwyjk7N4kHp0/TnWAU0zv7lF1YOjOGvDN753duG/wYgB2umZo8EPEaZQbdiks+mpVnY
3SWvMcJj7n/bK+NqW1HWXENxInZWdvYqpnCqYPS8AMS94rLp96h7tIjHfy9r8Jy+DmihdoC+Hvxj
qiBibZ916kQRWM14phQe5Ad+q7hgxxZUafRgdANb2TZjfUn4npjugMKV9popCVV1dZ3rbOMFbe5z
vW63ScqxDBC9dlW3+oX4r+ms9ExpYWfiBbmKaUgzhwWT5ubDMvbwdSKNB4j3lV45xm1Ch6CBT/bR
MvAFUhSEP6h4ym6aMp9FPwvZY+BiXAqZG2MYu8rA3cqmHwNxah1atcSDly10oz02j7AnpDfvkltk
F1jTwtP1JvhFwY4HkJHWtOJrtUJQIoNXv7T6bPlVMcAoJyFgeot8s6eSyauNnS7VML39N5glUaup
yzjJPnx30ogBiU2ZRtaAJzJ686s+kCdcXdZ2727xRwGA38kY0mywieEdCApLwFJ02Cp8IXakyEBQ
L11qhUs1RuPwSKEe5a3W31ZNg3uhAt126hU2PRrCV3xaYbXa+pel5kIhHfARms6f1B/qp9OFu5ix
cyhXWbLDbZQvMUWKSRt1bOnPqIaNnS7Jn7DNAxCK/VuUyAZNPIZQi/Tu+847NzABNaU6Tbmgj9LG
iu5qvo5NQhKlOxSIVYIjhqntzvyDXAOnS3qbNbyoMk39kWiarRReY2ulcrbztqJv7WkCxT2aYcsn
9PbvYUbL99U0jtOPsQVHEXfqt87EjxdgkRl7GixWVDbImQ7spaqX8S2dNmJ4W4AdOMXwU906MZuv
T5s6GfuWyXmHDd3SZyqY9DwIlKcAQAhv4QlOxyXHH31EltAR/mZaU9mXLtYlmnQp0aYk5jgfyMqc
JBzAzNHmZcg8gfVOoHgaR+Rgq+YgUmVSNyqPR7uczZG5OOAXfXaGc3O5fxFKSa+eYSZlBFkUupOm
MFN0HDfKprIKz1aLJTYRBrX+10/jNvh0/PhdYPpumm2LJGC7i9kK+UJw/+1yrZVzw+7E+xmFlu0E
mtSG36oKUsNQZxLK+qixLkMz+VDeq/ZB305SrdAAw9McPBhTsDPpDDu4AucMu4ZffxwGh5eamy++
h6KvF4l/y2a9S2tyu1GMHBVzSR1cGXgCuEmhpitLWbVmuS6aLe2DFGZFaRKgsMehn8onFRk8LMCO
kpb6zCkG1n7QK67WyqzJRiqSUsp5Uid6EYtK0UlH4IeQHzXGlgZufGiIpM6LbUtG4kTXO/XhcBrL
LOqD4ilF2SdN98dBaccuLzOsLDaZvFqnmdVqQvW09rMZZo/WwDHrZFGRNM1UvfsS7bOcw/OQctqb
Lcp/5JHjPJStsxRU8HevSLLxZE1Pffnv4mzfwqOzYqWXLbJKpJg6aixAp2p31cUjrjI4BEhpEoB1
EqlFSeBU9NtHVNYhv9nFFax/zoQ82b6nGK1ITmDE3mdH7egEqY3CR7aVPqYfYIjO3qncF88huACJ
QumEK2ao72t5pYDm2oKr1rCF0uFM7XJEysDtyDJrw8huC5+xFGZqE5HAbGMBLgpKQDZBiNkLf2oS
MeC7QhUE+gXw0RX0ytakZuye1goJ+Vv/AKKfALGPY3U9EaY9nG27R2LwGeAreu8AVRHCFQByWTOc
aJz+3tMJuL3b/3aw8V1RzCqHwu7trP5LWdIbIAa0MvhOrm00wijC947J1o8xOQhFvL1HBM2iqEw4
BxYrEJC9s6K1CJJYcfZuqxudUQ5lDpXQ60Of7v2DEYVKsAcRGONDdmUQu75lbN7nLtYB10lJd8Cj
fAkIrQ2/Nchai4w+BI+O4HtGm9YLUZdYP4KyeQj5tA4tw1cL4Qit5GV/wGqAu3lq2drC7z+VL620
Jf+Y0MupzeL9gbmuWQhHoFcwB5yqOLUGE2NpERTnPuf64o6iC+GcAC+tg8keWfb72el7LA0qebe4
/qsYv8ZVnNrlW2qX3R1Pm0shldjjvmxgJRInC1nHTWafYkhsHxu8UMwK3ibktW9jemtAPslOohNw
67YZdIrsMaefX1MGvudExKVLIc7Egi4gLY8w9t0jjP38gdL6YgKqCfSXA3DuF5fvn8TNWi2oUItd
a5PtLCJsq1O4HivT4BYjulDiEumZwbhh1HYU3JOFWV26JFNikIe5hbYpvBOP9Ck7vgvzg3EdzJS9
eQJzSAJcXRxJstLoZHKp4L77SpmlVNsPF+rmF5PxzX5ZpldONBlJB0YZeDpBKHMy8lugE4zC39Cm
qfm7VOhRTmXXLiJkM6SlVGb9LjxPTOsQ6MEYxg8d8hn1tCpdGfiN2OX8qTXrMc2R9KG8CIoWhb4d
81MC9RM1596r9fN8x1CeWipkRtEWDsJb6n/P9Y+JSd9MmRQ3Y113mb9Y2E6wsznV3hCMhyxYnn8M
vDUCo5YT70IaM9NOY4eld6vUqS+qDPfj+VRSpah8IYFLzfW1FPTYN2PpHafbmRz/GUVLdcbtkb1J
JFGx+TIt56UG15NIgeGCNm9GQQOohnhuHVoiW8uv16UEArEq5uRQLkWQOaE3wQs6gYUy
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
