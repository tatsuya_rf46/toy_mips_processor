// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:28 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_0_1/design_3_c_shift_ram_0_1_sim_netlist.v
// Design      : design_3_c_shift_ram_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_0_1
   (D,
    CLK,
    CE,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}, PortType data, PortType.PROP_SRC false" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "1" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_0_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NvoF0bQgdwCI3x0giCvlBozXN7DmJQRne/Vd5POm9Gta1HSdyEnBfSzixi630ggSDMq/uPUP5Zhv
NnsJ+NCmMvU1KBU1A5z/i6JjyVjM5dirdOVSl9J/fQLq68F59LgSg3wexAZ4Mjw9MTUxvbJ4THMn
52+MVd813rKONOnTsiS/vsZEX4F7GjpDcV+kaUlx/cOh/M82o1W4EkLbGN3tDYMXqS4rqZssi79A
gyj50V0sPtSGW/J7EJ9Y1h3O2ceX9A2KOTMaBpUFBKrCWNKiN0yNsF4BcO5rwK/GccAUxDUeCJrd
CuxmcOzqvd8UWj9eMHTxY94lZHVT25UJ486gdQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HikCRorMwaVCIP9qTLwQvmfN/eNMXVgaTcX2VDRrOpJL8o2W8gBZztlCKSZZEh44Vexvj5Ok79T8
jB2OQ22a+oXt1yYnlkbW6Z4STHOUAiM1U2UzhOVIlTkPX0mQwMEdlGGsQHnv8yyYa6WSxdedHVoc
WM5BlGErYJX1oMN7rPs3iwyYCMcPURTDSSG6ARKfaCf2s+t8r9e3Wp9gGfKoLxr24cjWi15Etcko
q0cIzlD8oejCW8DjmOhZ71aH3koG6R+E6YvMNszqLuKP2ykc3/KWKLacn8LjWO9hcuhPfwLsuVAG
sBfAoMjhCA+Fry5QLBCKw2OsBCEYWwipog9j7Q==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10000)
`pragma protect data_block
lW6QYvh4jdY1vbwdIshVcdcjppAXNHs8eevlWxD8XuY1zWxZmfJaHJ3bzPg6nQQ2AF7ut3jXPatN
16Bpd11yyCd8kmNVl6vi9VXwBOLId7ZE+Pi07VLTOzo0vwM+yp2EvyMj1Bx1LCUDvaF8YJwVMcF9
HkuFrpeR/s+9Y/ay/T5+cjLZZCryVn0ucylkyqMpSLpuO95w/OTufScEygDmz/GbUDdqQ5lpYZRU
A9H3Dpe6qGboYgDjTYFyyiENvrwCBOmMx9K3d0KMDwar6ZaNcxajH6aNA6EbsbAj9X2mcyJiBhXA
PSpd6/IPH7wDv7IcjvMc6Kwgh7j6i8kR6dmQlJUTFlqK4mIuDp9x5UAI+7pkzuF9tRo2O7tu3MjO
PHQIE/SUpkYWiH1Pl45njL6FOaU7o4P++ky6hdeJO58/bqhLVqPkUdnlk8iVpE69frLG3cS/T6Re
Xo3McBc4vVHO4NChT84y8kuOjvlC+bMg0XVVn9sUeZFkDJyYJwKbxodMfVaPyN90kFSxUpTFxjra
Twrw7V7w4W1oTV4yQHgf4Lpzb0HIw6cbq5wdTNDrDjIXN1io1o0qJfwSSRo1clhMaQNFEH3saxox
FYYOk6iJYDHJKQ+w9IE3d8/hsJ5iZLlwaf79zVxXkvSL9nAQ2H3bmvA+7E8hGuIiIrTywqXQRPzB
LtlTSvAi7pTKNAlEws4FI21v9nQ/k+pTBYhAajEQhh/6aP/03R9ddNWVQTlgUTFkC8Yq1URcAtiC
YG0aImiVR70tQGquO+JwKQPR4U6fl4Ytv9X4suttXZve9EQj2eAu85i+uOxhbejfI5h5hYX4wiBh
qD5NHnGe+IUXuqS4JThz0WvqkIcDubnuG3LnW4gB2Mxp8u2b+aaoUT5L9TRUlymu5JXU7zR68MYB
yj1nN0iTeSLUYqiOeYL/twmQvG4gFPIHLQPr3eOtFG9GE877CuUC0DXL9bh1jq+Ld77dodTf9mHJ
lmWmnFKgX1E33buNNu648jYGhEYKLvW8OV+SKMgZQ/twVOgjl/WgKcJ3IgZvbeumEnNG5bI43zZy
UgX9oKKsvKtuVIlCxYXBfRnKKYGFqEFtdU4MAQ0J4jmAuF9JpOjELe3e6lkupQ3O4GRm8UWH3qS3
2xjvX7aUJdOlKpbFUiwKaTQGAZbO3rffueFduYoQgGiZZpYV7oq7tjKDYfcjFRCIqb2bgJdj6Ma+
wT1RUUtRkv4raB1jSXXK6VooX2d/LztjwscwVjEqwAGKxpdtsSX4ztJchE+MoO6VxpNRCP4k+Xp3
JDncQQ1x+XCmvsHtLGtlLJjOKFaUH1q179nStDUKc6XC4gzDo4dvu6Pspgp9xbG6CGE95zQTVh/t
FR3G1iCbhv6yyWRJKu8zXNkN9QX8KAj7yXaQqZuQDDJwNn7iK4bn+ovJEN7Q6+UYdsp6+t0Q8lTl
6qAOJEap6E/OKammsPvz6vZ9T9No27aLRmYZk0BDGpWNPkTyHTw2u0lrCtmsxS9r2eg3dX3gDLVP
/v+Vy1KM3S6WvMEBXUnXzTaQssiJez1QtwfEyouhOzxZtTid+DeFkfdkixOCD8nlEsioOaWoAmME
oBheL+Yga2JVsTSEtxOzb70OX9FRN2EAixmDcm7DRrenDVwaSTXUANvGewljXxRRi5AtLnwI0RuN
0ZAMOTdXFmhMK1Q5BCEQYsKaQdPrCMsCmRQM4m6jBk8erFRabIVmb8ilc/nVNEqAQcHCieXPXmWR
akKUFELLOACzKJchLsWeFoZ9QJuCTeiFPdUOSQwzNUgLE7CqXgcZNC7zYNaa2RsrhrsPk2xZsYCy
pXP7+GPsDAJbsDYWAJg1g0uFwSK5FRW7/y7+6HnfhaxXxGPkib+0mgCb9ig2Ew8CM4jseqpxwBZ7
uFxZdnQpZX5rajJKx0EW8kj9s7ywZ0QXl+bZa5fZhg5m0/v/DbjdB6Pp1kQ5reeBlmX4yoOgvHJW
KERm/CdJJ/Jc/VbjECQabC+Ew/NDsdDvKcvvoZ18PGxwkO32gmkiVaCd22nzuvMM/2j4wcOqFoYq
SK9z6LIgHNEmS84qa+PUye63bQtj8T0B0g+78dA7+x38OPoD4MvggfbQZipfYahGdEKqcjJYmHRr
t4E8MgqQ7IwpTNVR+P7da5Udc/UEOiZWFIyx4HwDBxKwWVvJUnWx/y4PSUVPc7bvTgLyMuduTvXf
PPMa2tEUfiDan2mlF/d6uqNqbfUU3EueRFZP6womRh8tI7M4KO/0XGkcrzNAJ9Op1jzAU/9sV5Sj
TAnlUjZRPSBHRcyR+BWFAKlhENapPLHEEseEF7tDFayhMNkIba5q8fg7jpraGA/E9NVaIcm3hRrH
7ApxRqUVPw5J9I5Ldv85suF9ps0Qkq8/55r6kPzLfiT6LSJ69Fc/LFZDSrSMl0nbNsfRPeV9w6jn
Qc7prbYH38qmvcsvq4kcvYnQnXKlezwM2ZxP8GwGkrYSz6GtLPn72rtobtoBIKQUqa6PqZkWg9Hi
nDIXY9Q0ebwq2CjnV2a4gZqueT5dbqjRMnL5KiczZfBDPRb0hp/g+oDcjMrqnAhP+XzOBc2H+QIJ
vqoLvut5n0X9IJdQiResnomVzN2oTxljhXDIJrDsf5f7FWoILQjnUEb4MAAxXWmdHKPjBlrGT5/1
s4l4IC8OgaWo+j9/xN2+dIaPjBhHT+EgF0hziRu/FHNrDqcl8o3AQa+/GKK/Ntily1HWBckwr+vB
hHSnHjvnjYVcaIgQRyGVCDT1yR+DLUDK/AeHfUMUp8fFaP/1orkzXLhYTC4pXqmO3j4tmUSGmX39
Ptugu7gJrKqlg2So1HZIPF5m+9zk5f6StaPFFsTP795KTVQqoWujOkoZyqb4xhJOUTIzlIikpie9
SpSiKPw9EXHdaI68CLN+VuGF6hWiGdGDXthz5NgVLuWeCfAxGUMOVTM+ylDeDgnJSSlXbTmTEn4S
+xI97KRQuEuZHgjkY2RQMs92gSPUDyN4HkrSvfJKYTQyBwPHU4wcsMpY78ZqNK+rhkDl1+gGoGLc
6fYqQ9C8M9lKUi3WagPR7+ZkMHFrZP4HuEvrGcu4w3cCSASaoMiEJlz9QrpCpP2d+x1Riryrtje7
/TXWL2KuGep1HmJVN+1Q89a0db2KZQaq3t90ACn0/nx3gR95sEltpE+gOH2l7juL40W81y+Ciz05
CuQEYLb+A6HVheC3BM+D+wL3oEMPUeRxki+FOIF+dbF1xMnq1tWl/nF8NZBeNONzU+wLtflnmE8F
Tq+Rs4RJ+K8O52nigdXMX1YfHfZyLpjHRIPIf3cbbt2iBQW6ooNRQiCIDBhW8ScTo5RWsUFKPh+y
u+0jlV36+/B+NGWzqlNejNpds25iCBta7tMRDU57GehcmeM9BKGHVT4MQprMz9X0eNcfHkhulYdE
89IvNmmsSiHX+xscidb1yaoydaPor2T5E2Hphaz3cnThOxeaQdgvy2+s+ljb5o80qqNUCEfgnLVx
qyXAgo7OeSy02ZoKu2bN0r3llWYXtiOFrL4TO17Vrh+eXbLHtiWcEmKVII+zXPYi9CvjElPWxMzG
MlPjI3YuuK24DDaEKCMU4QdzRQcgjFwzJamt0HsVHpbi2CISTSnDkA9f+iPPNDwjiynCzfzERTLo
OnAiv1jmNEcn3/SqlweK4a5mh/YgCVVInjwE0uXFYVVcTduFi7wtxhSIAbHfcq/rszt61G87DyfV
v590hiWnQ6GXO4stxvDQuWIFK7iO0k8N4CdYZIF8jI1ovrA54TuplXUUrd7VJ9Uac2OkryqusJgZ
R5/oftBR33nzeT2cKw2s9xf9StPpXjs4I5fdjAk1pvFJ1GHemGZMeJEPCRnQnum9w4jiLcKhOzQZ
DBZxAUthECdCCt6lr24ia+y2WnlMqutggkgIf0KwCetqYYZglgO2CdoB2Xo+PcTdnK+S3m3w6DkF
PK3tYei4FUTGTqjFBORuPgyJA9aAfarrPV1omYc+GeySW8S/MPhXuia6YMX/JA2Tle9tlAWQZanN
oN2sFlRcvV+1oS1CVcoHFYDWd5H9xJHpQXj+ZY1JFIsCMgIUwlDp5qi6Lkd49C0i+weRRAn8fCuX
GVcL/fGA0Hsnpr9M9HjkUAVOH3wUhQsUBRgBh4H1Z4tfjQIcqX7xJ7PIw8/OvJE2GtTJkaQWxXFu
OZx+mV+7j9purY+1JlkNc9FnZdnXpH+o/zvM/jXt6F1dkMYZw6Qce6D3+uUhSq3vLlAWT47kDf2d
+ANpqYLuBPp+fw2bQt5QUMauhxLOA8pS56byrq2IocwR1eHRMRmIlgebyTKZkNey7/ybD0caBmvZ
WlPM5+eLsC4z7JTggHcnLn1hnDUaKPbCtduy0cZfQSb98GZn3/NN/j2QWZrA/fXJoWqrex9qpw+X
5AY21CAp7uoTzBszy0y0tqY+Yx2/xwBHcBdFwkTvgxkQbg06plV0H2TmJLSj7v6+Kl+GQ0p+vFr2
IOlvG71l4gsgW/AXJB7BtXT6woHQMURYvOw0mIVXk16ROC4BqsA/4EVWIJ+RPDuPvAIb7BOlqF1y
JbE29sgirIDJCmKfHouOxj1tcnUsg7BhG5kVgWhbpGNn7VMVML38RJOqVP0wzbXvkAr1ElqcXKsc
eV71VdWJRtK7tiPT6SYycif3o5wZvOtFluvUcvMRKaBTBvu3eL7RGzvtqiCr/veqe3BSqIHciaio
1xmGfQUfIcOR2bTpfDArx9VzI06bgMlZhBxj7rZnlSCyuzieEqQbc5npykBIEmdF25GX6pdhIkqz
0E1wcbIBiWzQJ6NlUTDB2sV/q4lvLUHxUrnuqS8+kdw99dPeprabT00X2zQXH7bb3AWFHsem1FI/
kwBg6POx9nLcLJ5vt/4axFy3qi26fBOQtiZJv7P2RLZvlXTMtF8JcErGNY98E7fhl0vrbBVEWINM
1ftLzPeqpRzp38Q6xJfohjlyt9sM8VUKqGjuozvNixRHMq1opahT19TAg4qaQYj2yYjVG1h5GOBc
I04vvRjPyhGGJhTGi4h5++Bg5/wi3aU/C778SoHGjDccez4EyyUTgnpYiD/gNDPa5CltP78hXn5Q
NPCidy6xotY8aHfRMTVCZqnIiAA7km1xsp8WUe6b0sQJGrV3sC4F6JCZ7k60oB/piNZ6qBuKiVKK
vF6mxXodRmipA1D6B1jwn5UL1pQ2fvQDSAff40VaK0qG6s5DyWmV9B2+AoexL4bhbJkOtc35yveF
VKdfhwy/XpzAJio4q0u1AgvU9Hz8J7vmngcNwOT6tqyUQc13iqaqdWHhGne/krpOfsX8aGbsGfz6
+xwK6uuXpOYAX7Cn8tt9Mh5YOln+8Jhy1uXCQjtUybrTMxO5SfUWkfkf3WKeixjcE9fSLJJXMp8i
riNwRjU7Wm4Mkrn/nhISye+usD57iQsUCHo249pXBEPEyYTmbUfzonp7IgkZH0KKsWIOWlsOL7Pz
siis14R5X4EAClIyYai5sC1KmDJRtgteQAn/WkRvU7MKCodem4k8CVur5EmoH6G1HgXplR0P+PDb
u509CZxScg6YDwO1Ta7O6dTYgQUTPuve5PtkD56gHwloLRSdZ9f4Dya0x4ipXouhr6031iwF8xRL
LFSSb2zE5FcIGDlvfB1SSFQo/qdKsNC6AKCkTm4fRZD1rvL+jtEmm/K9djNQxblW7qArgVAV9Af5
TAufHG28I1I2e5s0uwSCQMxfze1ImwJ4/1LMjMhfXff4yEOK/0u71ADBpo69R87tz0Dfet+DxU40
xUu4axYge3caz70L21w0PUoCbqfY4xGXT9kUXaqFF2gWqmuXaTgsV8A5QokFwnsfQmyoc+jqb0nJ
I/ZLUimKq+Mb9Iw6DlUKnbj9/4jrxbOrdQkw1HmZDgddXXb7Ysj2YONnbvlEwGuo1JtXT2iwU8hK
tS4FWF/zzU5lgekZRWeV5zlZHAet5BvZJUT/ZNxvh8c63R/OetYGhBtewxNSElvUjsQaASHshfb7
n3LT9W50gaes2wUxoYaFIyhRppXZ61J/C1+zs3SquQI+iOw3+yh5kh16eJetRyDd/qMK/8b4pXot
t9i2u/i+DWNkyc+wLKD1fyQ97PYCruf2Te7HaTmeN6d5gDq5MnnqE5WTt/kSycLYT6c8x/Lb5r8Y
Dtju0QA19eWhfjAkt3KPrtZjnk/K+FM3xDzD97kNodWeymGYGuizIbOlitvnmFnGNqE6D42UvMsC
YQTaqCLs7V4qxwPpSMNPhGwu5uilxduOEveO7J/ZtjG+t9QnVNzfyHA06wVQGlrZBe9JTHjVlqlW
8yd41uXYxF+dfzKqm6GHu+Bp0JdujJfXqR027fQ/x26LpRO+h/8ueUakxoXMPtEY7DvdulCBnq/8
LMlI8kFuoJhbu0DjSJRjRTUYyY0FyslSurihyqB6JZHz7yn7G48uSttULK6S7rE5wGKv83ajbZsV
qXicDeF/1p/M4VJ+iUrn/aa+4A7J6X6eyuq/JgEHo3uaDX+zu7kd4IIr1vMszC3SI64qvdN0gzHd
RPhH+ZmksGRveYWhjJdelTg2XhbjQaLxzyOTrqknwJowZ1S4ASUYaF4/dZVye+f/Q4bIvLwRGrT6
CLG+XQW4LRQhU9xL0ySUWAwH1HA+Kkqhx0h0q9qNEarg83wFRjU3glDoCCFV1YfhVzH4ijcZjE/1
cRm8x+HXzgerVjnlQGyM3bH/oYiDeZhGYHSjW7ftIFdP7SBVAVrwW4jdiktQ/icHyhyJJNZQtoe5
ty+6uG0r8ZIlLQ8ydmzeziAYOaKDC7WdmLTv9JWRHxHEfMWDKqdIZieP/OUaAmTTp4MIHd3gU6QG
MNwgvp5WYYKSX8wHo4RlPOdGOfdQk+qmtcktfWVCM/NoBQa0QnDAWniO8LZCdfpJ2uqPvPpWEIHy
+L/5wqOkuuJkmYalXM/pXs6imrtTQjkf6zxT4CFxcGl/8FmqJ5F0w4KPsKQvQZQQzPaV3j+8cifP
A1kQWgd8kYEaCt8aOpu8WqnRv1uv3WYa7qlyYQei/LsCliVSxnmVexUT/EMVnntWKfy2BE3kFQBA
keozRL6Dj60MTFfFd6TJvid86CKLvw729+b7DfhR7ygANcGxglV/Wcp3bOpRuQua/SugvEsmdClr
r3AxQipnIFqSExQQw1KRwV3g9zJFSccaeAQX1emFjkXQxR61j7gCHNvjNsYKtraLHywt7JjTo5Pm
xJYuSZxJbiJS23XM5o068Oucs4U3vvaTR7SB+aMgMIDniZRJS1hcglazUXiAtegaix+3AIsszSfN
EqRDgVWuz1l9n+qf0rB9Bv0lxNlvZ7kf3QGVIe3u5ixpvOdOo6k25TzBR/CHCibW5hrP5iBBsC1a
1G/BTj/SiS+EVQtWGffpCFZun0KJ1DKeqcqN6Nc6nA+v5lyG+7v1WIyZGEAS9kDMNUrH4t+LMhJv
2opIw9vsOC1X6RMsNL6Zo9Fmoy67L/jUi9kjf3SL40s/8oiNEHf9s0eBevYM8VCR2COQq1AmN93O
EFKL8PzLJDRoPSnBpD/zyQoEHkdKprSOREbW5yivmkMhFoxi+QJFZaXZ5e1+cYCOpKTnk+hrCSLk
uOOaoLY1RzP4PkJKoXtZj+gxE9KQiZ+BcEGYlRuvUG6XBWRFIDdCFXKrZF6Dt8+dWNJzfoWnIHKO
QuC/GzlnOqaj/BZtHKoWIWsydlYD56nTHqOuANI1HQcD6e/v48FKQAKZvr2X0aYcboKy/9ZFsg7W
Qs73GG1s5sczImgXBYIiRH7iTN0Isogn140fbHx57NuespKGZpXvisB3vZFpHT+c+8WAZ9qh8qQq
DPqqS8Z0ZD83r9IGHtFR2kcn7CH+OmWV5u98WgMBc7W3OJFgaINc9emWKepzHCikJBtOi4yC7cr+
sevW+nPRZPKEBXWbDW+xxBvxrLpTJrVce7Z0mEokrfW0LIM2C1jlaI21TN+6nO/FpFj23RFWlC5z
+dwXI51HuRrnmbjBO2fp7GY2eLi82rcQka37wawf+BLSPgXiZ0/+/0mTSBN0vLrQ0Lbq0e1d3thb
rO1HI0RC7e+6BedKEVLCTB2TX4mV1vUiHrLCRyEBgmHJ7Ikt7mdbS6BCa1N9IGdpcjW71ERHove4
D0B3AxcxIcT4appm1v0xEfNdEE8wp8BXt/jIF2jOAYUnf9T93TyHpr7UTCmelWQsQ+M7cNquRAWG
7B6724h2kUmHSbjFij7jhHIZzO57p1R23G5LHsvnjbUqBAr6K178iYbF+nfpYYq8kDaz28Q90I0h
T0GVmy3/19njL/47rR/KL9rVPhm6SCWdMl9JtuBdWCNYmwhmnzD8jZ0N9GvulCa/S+H/XwE2x2l9
FsMcxvPW37snmqhc8YDF9VvstfxkzgmHswO2OXBODkmfK+Np6TXWBiLmyiBdTYwQGXwn2Op45nox
G5yGLIJBA7ZOwPTduU1PrX4QF8qP+PfqjR1aAc7ogySh26GnEL0B9VY+qE2ISt7fcvoKHMbZ8d5v
HEMlc6PV61lit+x00RZnCW0fFlu8kU6PZAp1Mmtyr1+Ius2ZmogQpccPZ6T4jG9UK+DyYpza7CvT
vnzMZaKtxERkOvQkJH8964t3ba5RLKQ+dhIsPLIMUXVtyiw+7gR3pkb1sZc0nmsfYnuJFNsE6PzA
s3ZKjmih+T0mCI7D/eBaXcWcMIjOZVtUPQY+SQ17gOytLbjh+vix4ODekaRAJciICG88VWB3TqzO
sDbf0GbGJWh/mOxd8wz/cYz8TN9wCHTh6oSCZ9Wy074++mASrueMiIn0+X937lPiuW1cP+R6w2o5
NPdX2258ZsfWF0OA+K21qFGAvhRar0jW5Um5mbHQ9GNve84uAaPgMXxusOD8CXV0u5i/mdlhJ3Jt
3O/K56A0mpneFrwLxw7rPaAeoIE4dp7BLCRjr84BXvjBIlArKl9T/B56GQpU78+DZyhTDKDLQ+pw
q4hf5CpNiGJcWOkkTboDB3DdrSdyQUfNTvOt0DugS+Dem/psPphfRClTtnHeWB0PGIfR7/BcAQG1
ByiIWjK+XMOnXWzZq6ppVzC9NdCQ55CNn34hmfxKFphuVTK2JNdMy7BVlC0Sz5jittR4F+cEdK9/
ffTBariKnB5i8z7RrVt2q43t3J6kGxoHUA4DhTiEjdGd8dJQw2FCce6T/EDXUP3FST/kqRp+2kle
5TUr+r2GgqMf+Jl//k3Ud7zGDm9oDibM/MP95ZILJw44fWCwT7zguENMqqOkeKoWiQIzhtLMP/bt
wqC06hEOzY11xRITrHgQxyMXI4pLYs63XWz0Tk6TRh7jXou6P18UHW8QKbNQOY+CE/c3j8SxRK9h
gb1c5MCFHrptM8UjZYgbLTSdtSj8mrHoXWG/bNVyuPCkuzuxgu2ZduvWD+MCdmMhasTIo7sIo2HA
6oBhdYM7kptWegx51zyfOdNu4cRQR9buK42oB8sxeD2yxbY3xK6rFoptZwVLlG3biKZId9dhvlSa
pYptoP6UZRKUytkXvfQk1EnL+XNcSIbYrE3YQAovEw8zhe+4OzdzexBUbgj9mstt21iwDG9oPRjV
jfNxeS2uUCZ/hMKPYqmYYF6Zc8uu2La6NcOOqGAqmX8HwpR/3lXG4OaoVKY4hTji0EzaTPmjhUB6
AUdpnL5gBqj2whGtYM9t34r1IUVBnTsYWV/yw+XvFj+rKqg2NHMHL2CL8UuZ6WjtJSSe7330YCMo
5I24tNIQ3apWzPkkp+RyWGDNR0JyZmZIWC/rh8M3oB8lmLDn3eIs26U4HZ3IiaIHxjva0LrAcB+Z
hTn4SIZCzJyxYCJeRiaGbhEOZjGE8De6+RIcycXSx5Z7jN/Dem+zxrTmzihgGpFYW0//nfaXCNbI
vX+IDEE+n6+tTxAYzM2woroLPWY0O3fhp7XcW2SVi8eOL8woSibjKeU4wi1wDXo2UCgBy/NYsJyz
uq1EdcpxTS3XWSc7kilEF95Sy6u9B9UyiFiozH2F5XEGHir4i/a0/AvKgnxAy1kHXV5tiZ2C7i5u
HU68/GCGCX+eCiRb3cpS9XmDq655fLp7yuEXtBVMJ/TCOkQwMU89Y6LREc8uocl76o98dNDgP6UB
CFvjhrCKO5QfGySYqP1PNKbrl10FTIIf9uNn3dYhBqD4eNOXa444OFOG4jJGt+5gnBjve3rHOd/F
HCuoKZM4M8HUhm0XnTmrMC4r79wycrty0anaJaAlLZGJZ3m0aRx2k0eOgpFI/jqNfNypPhqcdYic
yRGP6vNZfsydEAxYDuAclL2Z4lThVNXnX8QQOYr38a+/74i8CUUWUKrfn06ylTKbzsgzW+prtJQ3
i9n4uc9IIQ943gnKyuZdMyV5d/h1s0n4VPle6LO/onczhrvizjqUn0lLc6eysxIOKsLVyK/CfqyY
IfocqcOSuFAumTyfnbPjheqtzrNM6RDzufocepYezdEKMqZsDZ3KQq+hnZpwtbCVToHl5Tzjh2vk
djR0G3HD6ZAKjRPk3JL53jKgVsvwk4WNrHNqU/0yPhpo+y1+E2HJt3sBZuQG3ZloBQ0B6dh439KJ
g+yD9iKgg7c7RRgpsdbIiAnMUcmDtsRQ9lonDX86Vs5pUzBXflcW1aUdj//NcUi/KvD+n3/RsjLU
rKSLutXpHXC0cIfpcURqATGDYjU/BD+RmmZfZPrBpreIFvfMw3C3mYcwwSJUOyb8eRRaUKiz8MVX
90PGLXXGI3ouhOBXifDym1RQJqDOMZuUaJ6JJ0ee7hrhc8pPqPt527MKtb3AAZAG2USBlx5kiCTm
WqPw4j/9GdYO42fKyND8XK+w8hrWiLw6hZqbn77PY3oI4838qkFn8qUhktKsfSDKz/SFfTn1N7Tj
2d1cBsUipER61OTOvX6OpsWnNCIqxf502g4mI/agRze2R5gABEYjvhLIMGNoW7LwrvprIC4twCCk
CAGE7s1JPxVtzfr/ujDIiKs3nWmGWvD3W5CP2XfjCXs9yxSl+2pFRE9uFNbwSGlIFQ/D2nE/xvce
c7F5BL1Brb+YpNBSVufVrU1EDQsrB1YLETcCosRhGKbCD2gcOPpVZIAjgfZA1vto2Y9r5tYwu6Yc
SoY3hc0DcbbxdAsKdYEOKvZSPnUJST8e6wV+jz5d21ovUdlidL2lg4qxOv9wX7cXi4HXM37/aHiz
zMwzJURTRmOiIwSRgLUFB34bQ1GwODtWzGigzQCN+92616jrxft2Lmu5BgtYP6sAC30NzdEdbuH6
8fckLdtqbI+lBifDgPdJ6pKT7w2DSL6tT64jZ0SPD22X0Ibbvr/Eq2enfi4lQS6z/5QzxJaaTHng
lGZHv3rq/rEWlB4r/USOcv0tNVgLKnN9XESVNWmuoqfzSOeiRDl94IcIHS7aqri4wqWH9T4HC3Yg
9sQ7rR9Wzu9NCLvUFndq1hxetm7gH2uuy9b0pZlwf/WK9fch8Jpm/QOxOklU5RCnoeSL9HMx2siP
n5OqWoYcUcSoFNJW3rn0fFGxXydTt/lUNCqtwk7ewq9/3rvi5i9EaBy7D55RglTP/8BdOprbma9e
wx8AixaS9garsXWF2BOQX97d8fcdweXoededSdHfn+UZThTDLOwqGiMC2geMRnVJub9/+jF6quMX
UqqY8FWdruZBCJ2xOjEP93/Bp/0xg9C8lTXdg6LwFJNOihXTUA5/gS8dh7yYW/BxQV81ajRUqW99
Yux5ZMaZKGELISTASWdQ97e1kgOjUarq6XkGooHvflYRjuGNHfYSQuhD9N0UiyMdx5o07bL0okqr
x5PEbH0lGI1QFymBAq991vlBTq8YyF0c+RiWg6MinCfW68z6d9g+OIy38DbYoQ6gFAtpURJv839G
3jn6NqXeTRpM+XR2qCn8lmejM+Rr9AChMjaN5VFvnSfmLEuNCLoSFrxupsr9NCdqjBA0PceCcOSH
QbJ+rFP0pQl1tBhl26EjPGNKz8sO2DFNy0dIGbMhaCTPSsqPX4SbPTqY1HNDoSOUZdcxKcVEHk/A
k/PKntQa5vnk8TGpvFSmapSF/w5WO0LoyJ7+ahThq3s53167a4tMWMkCbn4VkfbOKE4D+DwTwpeB
Gq3BjEP+Sq8rqaxtsLx5GQwOYj10/gYcimve5uCNQO1ofuiE4DG47zLGujUQvAelJ7/V9k7T9kLH
2e8FowGjElXBp3TmX/I5HuZZSjTWRTwKYmPkjMLAHNODhZf22JdkXDZISI1hvcbkCQHOndBY2z6x
wTr1CRLl0Pjk2c6XAa33M+gFz6O7pGcmkmV8fI8ldnm4fcw+7sbG1LRNCG+7Ovf8nFNtLfqm/JBX
q1qSpWOGpKGmt29VMs2Do48Zo9gMmAhmwv6Ft7e3TJsO4UVhWVWQ5gRIaWO6cIzrY4NvkmztBYcd
aTeZzWbMaXWbKkp5y5JLF9z+0wiOFoQeS2IskKAbVYrYu8E7n7lGuqwKYXRAfWEfGw12LW52+c+8
9Bfgt8Wsk8+Dk/x8r6EMpPBr311kfSHUdKvJFaJ3m1YoEKKbyQ1q04Pdq9N0G6d9sQKKxdjg77hh
7qyEyqZJpckJYCt2KzxakBvVzglWdK67nYhGDiV8y1J9skYiExwHHGLnjM422kqKnL1jpHVH3IK5
6EZ2leI/liWZvvR7b5JrxAGmTdm8MTA/hgXOcMaOkPY1lDztstx+WTkgbeDteal3EUtrh3cWdNSE
TWLJwikdmzVVgqQ7pnQPLoPHxfmkSy/pcR/2cwYUbaC/inhirldv4L7eXEGhDejt2kKogzVKAhQ2
npDkn3ySXuPT+YC0vpg2zjQ6brzpZygtQTAZb3Nsi3IFByonOMO67AS0pdeoMF8a6w0/3c2L3iDL
AhB3PALfMu9KSC7AIWhJfBER2439JrGeMmu/LC9lcBipGMtwSo06sGzKsoiV8M7XR4Futi1RyH53
37tkKYLyC9PoCxmyZ/bBgjcMVE3y5pt+U8ODPRkbbYr8u/3uLEge3boMKuMSKhlrrzFZuY74EOwc
Q2z4aVYSb2N04x/5mcuXYC85B/YJBs8f9MwHei4jTiTCcv7o6ildiJXCXsp2BEbqIsV1uxFY7wA3
RmEQPzk1/4tRiVFHHdHBydz1sW8cgXiQa6uuzH/42J0H7JvjWI0Cwen0G8TSnshZ438wl9FqH0gF
QfHOrZSQ14vUPzalHYLOn4M8iQB4TJLtZ5oCc0hfGH7emEMoBjjb4ZUXeeFYnzc/N1wj5CS8CZdG
T8Z6DH0AE2fc2OQTREruvyV2TV1wo5X6P7fKENYAQzfiizinlyCVPhN62Bi3SNy1C5PpFy5b2PAR
45UieINafZoqswSPksL9C+Hs+jRW8qSByQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
