// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:31 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_0_3/design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_0_3
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IoGaPu8OFbTRciIpGeFFJuZjlsGNbK3WfMPBfUVjug+KkxtNSZeG7vSZYcSgL15ivDpRkkfPiVlp
2Dd3n/743cu1U2aM4PZPL3ldkBfszzRI1RM5+eJ2DtLsxjvoh+i3cngPEDwnZ4WDHARN+0Q75dlZ
jWw7d3xPo6lFUcXj9xlnu8kppSEXxsrHKmZXvMw9u4Sxnq5zS9xm/9Shw9yITD+XoJxURxTAq3Nx
v5c6kCXfSAkiOcsmrD/YEO+CTSvYTTA9FFEl/EojEDrpezt3pX7g42WuqqtZZMMIdvdkLiGBhBjL
rCGWM5/ndcvOie1/6NJr6VmQr5+yCdXDEwni1w==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
1WZnWZeDzbxQ0zQ/J3fLvLrdvx8v6FGpazFRB9Q4H/Zj9+sHIgIaZSZ5HNzHYbaclObesoKbUdC7
SxgG+/uiFJfH6hKTaqG5CfFmZN4vh7p++J5sS9/Cl1gncspSDIGj1Gy/Wliz+ph6lIyuNXr/RrTs
mA1tycQzAV4UoRvIu8S35qVDq56fiVxKsBQ9KlsLxSUDvwHgnkAHpKBzwy0yK7xi1z+k000afQDv
R/RMujqqxHsM0/GzWdVIC1vSjEVP5Vll6B8s4XiJPE+FZ9H3SmaW08HXRxv6PAFrwCGGvpzEYW5e
rdDoIu6ai5NVjWdq9I0CFu5eQ6rkCU+mDNL8Bw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4640)
`pragma protect data_block
AjnRu6rvoB7WT6oNABYzHGyFx3B1nPy9AxVa+mXjtAxN9WWR8Wdrrmj5YDVdJOS7rBCOl5bmhrSF
hnviCAPiQTxPvUraghpiblhlhDEVqwgCN86wZRd1O/RgPM7lJSkOUJhBpk6PDOjKupkD7JlG3q1k
7DoZnOWyNSR5v9HCRExwSClovf/l9eLjHC41TQ+gML3o8W3WgQjHGfDWG4sC0JjiZekn3MC6OfLM
lnrFzmg0XK0alNfD58A54fusKDfDisUw9p3dQ/VnWRKfvVQv8haeOCzIPHL/snbSSgr/W4GlfZFN
W+ZxT1dZr0BCNNR8vQHv1QkuwSzN2qt7Qmtd4CbVJ+f5SzyQp3qdqenoTzZgubGl9Apr02vVsqW1
95Kxh6h7+9/ehqEttP5l5dnSxTsW5HzB8K5nK5QiMpC7MTjXvSwFg0rM9v+5+8WgGlo1b4gh2sdp
+H+Lqu0FM7DnfFNEER88y6ANVBuqBkTzdmkzEv35zBJ3hoTaizOFl2cdt87Hat3fR+Na5E0Z+6d2
PbGweryExH2CbCE20aKaG7F9mfBP/fdXxQzyfQoNbIWqd/HyGqwffOY7hIhzbAzqTfryIcs56kVp
R6S7me8mxhEADhjwiSgwAlRNDWNaJj14CRnL/0psmPEhTGypbcZYpSY1NKSyzKtWWxni4loRVoEO
lAstUqOVCTQHh4UyO8pXIwCST5P/Fl3c+wiXEHK/GmmXXhvVAm0KbKTurF5otdm20RuA6GCnQaeA
cgXj/OddyiKOOhm02Ly2yd3GaH92DnTHwsCROdHxHy0gCBW77w0hyErKbH172qn2U33PsrZsRxCd
weLbqyZoaFYg1y6uq7KWOl9XnTk/4wP6kV6SzDKMWJH4nHt2coZO9DNIdlEGCYjyOXqXkQ0FFgp5
t9bVqQSsQXNQl1YmI9QW+YuhYHzylUVjiWq3QoeuL8Q2BWz3R9e2Gum5J0XWrWLWQVkQXIcaWmiv
uAfDboF875uXe788Q+i/mJEbb59FfL1vhak61nlpN/k/UF9XOZH1TmiSUNuFQS55Swx7Ecb8XGpf
j+Brui4oINLRY0lI3+uJzSh971MZTA2hhSVlgMDS8V1ieKngUo5hzcjrEhNvM/yA2FHNMrRK1HFT
QpYVedJTNFhtezdrhSSgu+sJ3hOt54a2q/tAZiy7jPUq1+zX9vI5EubEbfRvH923VrLT5Hq8Qn+k
xdXqkYvwNhis4oT5wS4WIG2ZrOrbyzcrfN36uC0/shslLBUMLrLecW8U8o1bIlMOGPwy/KQDLxRK
MryizObvPajgLxaKI6FNUaivtL6hJZME/41C2gVJ72NZCgMUNAJLyYomE+C3yeTpEy6L5J2q+U3t
o4bRGRASBJF6GvqFhdDfWP+MAc/v3hxUS+Lq8Y8QZ8Qn0DarxJxMPT/dzNRXpkdcz+PMngvnu/YA
GCA7oZvAY0/J5hewtCbi28kKhDpmJh7DOe3RO7kVz76GSUz4+kwc9KwYPUh24dWWcws3eNDtCFLt
wdrAmWb+Zu4glDWnPTrfXFSUwZN/jBlm/uRKntOzLuadQDACixjIqk6YunUBHEZJkacfnP/iW34f
E8FYxHaUXkvsAuXZOurGAzGuBtPLOKWC2TD29N4mo6xUcCAl4bK5QkxipOHbdey4hgRUCh3ineZY
BIf4vC7Pnlm4nxf8yNNbGfoaO8fT/Np9JRvc1lqoZXaDceyaGD70CNTEUBrWIki6VjLTUJcIA6uM
6VlBvAFjsxYoqFzccj0ShjCcl2mfB0Zw3lpq/H1zmNAsoOXooq7BTjnLGuH4IK7YWvdUzBT/Da3r
roUkcgrDwFQkurP2R64lBORJ6zLyzZdl42wU+gA8HRBuR3vZNnb76T4V3x5vD26GKgSUfW9gYCX0
fD1OsfytQ+teWpVl3jTt5lGv+8e31zx5cS22D4Qfb1jiau4Dveko+gePzmIdkLBHHnyUACQoPu2d
+W0KMXdvpLCum7ubV6Z8SmrAR4YurksbMAi9kp15lzUYM7QCMtABOmm9VINySp0mxDGjVr3+BExK
NAxK+7xLycHHxq2KXlHYm2hyWRGpHbfb/ZBtpByze+NB+5jMYSiRPZ2uCv04g0iQdH8ei+jEupms
KNW8Da+4l/Y19lNeFJPq7J7Co4NosEJr8dRMcJ0pk7dD1wGldDVl9RUD+SoTxXTEd70/UwK0lG+9
CTAEXpunV1CAnMP5VF2KuoxTpXTtuAvywN55zBZ6+VnipIRhxpLxJSuLqLyHilKinOmYe0k4k9oY
LokMOhIJZ/iQE0mJFqVDisuE0WyHlt4nbc+qXzWTUvBODyxmRCOAXgE0vmOgkYgABkEPMUO/j/m1
+6/3IiyAL2X3+wGpSLGi9kufbJRd6KUSbvsdoaV/uqQxkJnw0hxIuHsoHviWQqnerw+Dr8eXj88D
tMnIh5j8GtQ8jxMCt0+ulszG7sYbnGw2jhUCI20A42+9H4eTx/FnMZ24Uhaqajed85saSH/Akgs+
GcD7Ik5GC1oFGownAgeYFaibV7b5j/HUXpscb5gIWdvC1iszl22d462Wt1OhcQKcakvegIsVtqwc
bmLNNtztDr/CyOMQ0dFrfmXv1kN/Eh5MZe0LRWhzDELenFg9MA9VWjRqExgFQ9B8eGHE3KN04NjC
UJvn5XA4ayrlRHm3PjMvDzaLfP0XyHGInkrydHU+y9WdMABqO5ORws8n3J76kP3iPIqvBZKATArh
qO3qTbChxrJSKdcUfuHMe03e8gROeNfEdllcViL+tESgDFekavVH2fs9FuojfdBsnBAJw7p2czdT
cNv8icyvAQym2tL+KOPfG/EVMpNTDpu4wHvw9/MxaYIXuHAwY3D26cezqfBSbCDs52xowHyGTiNH
yP5tb9iCEwcnXl7EAIY4tjoMtaTaneyK4s5KtBvN9258KzLSw0xE4Ch+B2vsgVUFUQh9IBYFjPL8
SV8sKfw6epq2D+8I1iKwetRMICFxtlq8pmmgHDbfKzo2GB+dlRoo/Ope1MvnZWZW9bWCakFfst6z
i54adT0q+o12XvAQVZpg8DFRg7hlqL+3/aCaX1h8/xehkHdllEMJXjo8CAp/pivkWZ/eLvjiZ4FB
Jg7rd2FPPNOr9hmpsFSTj9nV1mH4yJiK2CN26/3/BDhMcGMf/UEjdzWitZK1/qS4xxxwDJvY7QhF
FBj2kidMSFosL8R6KLCj0mVFPPnKKyK8NkaCXyCn6i6Y0fwzmFIki0ifJMC+H8CBnFJ0ktW9q1lY
vlnmtHtStLF8go8qB+3ipZspmKLGGn8cbzaJ6o/AiS3ctSYXig58S0bCtiJy1SbGPjSQCFN0cezM
pdmxyZ39ZjzuuBCiKJtB2KsJgRLcoMxgv3YEmC+qk422iahrz2Z2jVoTVNHBzldPyuWBzoZlkGoC
sDDI7VuCSbCkkli2H6xJzbUln/EYFYqNyxQNvybU5lV8YruCtNZAnflwBSIXwZVndJIR2l2iLQb4
psrjjtG+I5zEp8KAgDE2jwU6NWIhljfrwd8qLrw94C6NKZNSH9jS0UrsfqbCQD4kNTnInxljisg5
E+Dgj0dAVmncFthefKJgSkL+ZrDxMUNDUSHzRRB2a9uMHHFdxkcQNO1/cJ3bBRZoGJumGsg5VURw
SQs82XtbTn43xOgDif956M3OOC06i1dVfC3MxngyGf8Pdl73Da+cYKOG119CEEdS1h5dzavbF+Om
qQRHmibBZ9JO1veh6ky5wKy3c3VMCk6rLgLUTP5xZzmHhX9i9qpStA7oTFCpM9CvvA0UJI0kdAum
LVUJE1VnJ6TucF741fJ7OSa0DSU4gSCVOREgZ+ZVZ3SpCG9gKro3GiI93hFBhH9uzfeoDmA33QOC
1rliofyby2Q4sXURLJiB0JXb5p0TZHLEkRJSnnkr8MMmjRWkxPR0WXNCEaK0BBNUZiE8aPHkY+D6
X7VSGgP8TovG1lY0Q5sN5RpNKIHWOv71zuwB/UVOFXDTZfVLiunny3rF61OF48u7Qy+045b5e2D5
M0VzrGeaOrnP0DqSYMcHUMBnpAj/NLD53paFiTT9k2sWKoIUIjTFmPdPcdwaDJXM0h+4eNjKuVRA
VtluztwlORA+a2z8hxPJqs+BLkTS8OdraoT43C4FDjPUBjTG1pkT4ZyosUCi07ojPtVLSOfLo0t6
AFMh492I1nJoSrd/Kr1aYg9gIkXW+x3MaTkuNswFUo1NT6wuHxEGfe3HbfvlrR+Bkv4H//zNjrYb
TvbX1Nnv+RJ6tbZVdf0tr2CiflhGBZQNM89bPbZ5vQdsHZYJo+6Q4Qfq5ICO2ufs9cpZ0J/5Brxg
Nx8O/Talp5BcMU+S+jEeTiBQ2NDttm2W0HBAu1jhU7kbTMdjtOWa3NxFfNDMBJDyyobwe8wEI+hE
NR+PAH2RylidyMPbc7C4UI+a3KC2HryAAIwSKzFLNLfy3t7LID8YBwS0CznMDQplXKnDTwV8zrUl
IfI0HjZBfJFNpDk10k4mkfxTEYWXODG4GJ8IjvTyUf1ZSKHPhM648GaAvwg+P4lSXjFTTtuTVdM9
c4YNGtDfnmkR9xPAK0oJpBFzD5qsHznyQFgoHnNqQ0UAlgy6eN6m+93VaqpmcUmS+gjZOjP8kA2u
VsICvj1ejg68Zj6zOJ4q17W0WeLX++TX3pxppI27RvqofemHeRBvkMTWsO3tIDbGIW2D0ePfMDQH
NHXKZ4IKHg1pcY7owGZjbcqLvHPuRfgKv+pxGNeMsm2AcP5/nPdUam5B/Ag5HlkMwHgurtOAk4wW
DwmNYxmJZADR01ds8wabPAl4gO7DMyd/a0Q5BjcOe8eNDspuKH3vQSKeAkAT60gwXvtiOJc3NfdM
BSEKsQVoTvG0PkfoqyLImuQUQXmESeU5xhre1gQ5jj0GVg6H8L6vgB+EbYwCyAcxoTqOXMrylUM/
o1DHiRMUU+OWPnPmCTyT3lQYuWJjkhu7G/BmDawx3Vl/Oc5YrInj4XV0TAv15p3cG2/VgVNgzJ3D
or9ZRkgJMAry1Gjhp1E0Q3Y9TcfhHV14NV1d3KdXABk8Q1wliVHByf7ebxnGtIeTS4Zi9CJXUfPD
gT/XW7EqFFydwmr6iK/4nd/KNzCnqn+AzhwPYSr6t/OcjOrbIPRatM6SkawFDCOHIBIy4XE3ikfD
NjN42RrgSLI3DbRd/n9XUYMeoHPDxlp5p9MLErDDZHpDIvNHusizzOeAFv2g2kcCzl5ggExY3249
+o1Ba2NF5lgbGt9dDvhirgew0qA9zUtbP+Vywv+x6evqWoA4KweImXu2rUllF2eZOuBFxGcjOOcs
+XYSsPNy/MYj4rYZOuNo+4oCLFBvAR0XXpi5CEjq7bM8W+uajwDK5H0znAds32YK9MY4cCABSZnl
Jp747uHK2CxdutWy/NZZyFozIA7h7hu1SMIninpW5OrsqSi/4IvVPEjQTDHjYE3cYN8LsEm1LG+R
uZCSjhcmWJzJ4EXmrhgehLi9RHy25aowbYqOVcPuLKkbkjcYccS0YBOXonbKDocRVc5p4ySC/Bid
dvuAvgfsLYb/iel8Os1lj1E9R5w8/HCZtMPFtQYOIZFjgKi0wU4OUHOWW4y4l8Wy2OW5hnQKNMaY
UOx/Tn4nX5zM6BxFdFjgQzzGGi48dBFhrzw7ccd79jLMJXSTSRkKd5HAwMRjKzPXHOPt9frLr2ej
lqn/ll0TOUjQTK4/FTWR4rLf5r5LlJog/5/KJ70Y7msnOTfjeSuqOBPUeE8nYW7f5YAJfw7NSv0x
4n4ngkyRu2b0NZivITEl3oAV6XR43RFduTGa4sU1BqFgkvz0BkWi0//8R4CfK4aUOfwAEaGXkaH2
32pK8Ov0k3lKiHoDo6GQMIcrwwKVxsbwMcbUOztBB863T/L/bQXWqEEUfV1ktl+57RjG7rvGnedH
3aHs1ZOxwJbX9eWeRq0dse2mNFs58ryHt9UTDGB1J+8wdHY2oio5n1glvI81UI8vHkzQ+8bGNBPn
9CV30PBJqCtcJ+cIlVFAYmgS18HCYLF9hkLkVpBNyhzJEUzurmG7o71nns4Ym0peh8k9HCZ1HA0G
UDaKp0ZxK+R4aog1pYFzfGgJm+1C8O3Ps0y8LgqUpxtrR9SD0xtdAsckReKmKIHiPH6MVYbVJg/p
q44t2gfORM8+h8rsfP0hl4Zw49eULOE=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
