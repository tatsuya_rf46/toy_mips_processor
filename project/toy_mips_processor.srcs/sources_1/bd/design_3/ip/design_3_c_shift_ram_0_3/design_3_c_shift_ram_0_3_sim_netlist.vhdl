-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_0_3/design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nCqNIeZxxZDjaZKK+pCim/lmQaPvRlxyjsOKxxqT2WUhJpJFTJn21hrQzYDjk/ZTwb1ES4/J5fRd
p/tTegIahUs+g8fglbnObivz7x2YCHfNppYeltBURmE31Pg/1Xe/32g+AYKI2OgxMNv2DH5UZFK1
j9Zm8qe/Pxh3m775Y5V23pSt0Cl1a+8tx8ASslvfGD93u0IHFoAt7FGNUvGB478dpGWAESRvhcKh
AQal51J6rQ8Se8YBkyudZN38usQc1mGg5AcV1ww8okvTaCCM9m+tJiqc1SQdXwkuSGtyyT3d8GWT
4rw5+67Nhnjrhb9h0h8hmSR2DdemajIolv2SJQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gw0qDhE7hC/fldkaZI4NjB8YL5qkAjW7Ub704TPkZyB5CL1C2qPaJFtLDvBXB6EsY5Cc6EnsYkbM
04U5JRbSuOglHz+7NudONOGALHE3mEa7a++X1mBGmnITj3CM01xqOzGMo6tgfbd5/TaUN0r2AoYo
vlf2J+kq+uVNCTPWC3sNSWehvOozei04N/rIans1qDD9Ukj4T8rHcHhmvta6/yhx/7Wmr8jjIBZd
vlI7n3oUJzgV7HTPu+Zzcdq4ccwH7kwrx8h1yQIFWGLa+Zp+1ByplrUh8y1RtNpBp6jyo8V9bjrI
DbyCYCAVNYGJGo7DUe2GhUw4zHXztk/pvBPwuA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13632)
`protect data_block
PiUEesNfToGs7TDyjOLwWVMbHKz+LbXBJ2EVpNJ47tBO6PhEq9hEYF6QLR4sFu/Fkmz3ubMatHIh
JkMAcDdNTxcpzFOuEX5IZPR1B+A21ns7mmo3XtsPBxNE37vY7RcCXMufY+kdYWfUHCY6/lWmI1HA
G78l+WQ0pl3zXvRBFNEOopR0VxkzvgTwRA8UjNxeddHJLM93iBYNF3wpST79qVfboSfFpc/Uft6E
hdYBBgi6KhJZJCLfxm9S5Tup9qoOpzHoYcm/6vPciXygQamixqyE/vsqGCSJZ7Ukx3zmONwCRsz9
bR/eHhIFu2laSaHtBZxpY9cHm/sPyVPbKKW+Nf4ZDQF74uDPnqz46VJDHmEp8ELs3nNS1eUKaRwG
Gv/Fhz6rjUBRQz3s6V9Li0oAmMSpyC16uPZHi9ymxTQnTOqsQGl+UwtPUzSabA8YtCGRbceX/n5V
JryLa2HGBiPoI+iEy/1CqtMUWvZ/wMbWUHI4eVYwxn+yyVsCuq/v81rpevukKxP7Q/TeE13wYA06
qmWBhSLYipjvuYL0kXf4XL6fs53V4gadb8YnqLbzVyufgydzzHCwcPs5oP+CEzS3xPRFosgLPLdw
/XUZ8lcggaZIfk+I8xIxz+PpIKkZ7m5cAUD2teP4LGwOsO8GHA8GDZMK4sCxCewgO5x+4wwe8rnh
CSysLVsbLiuCWB19W9dI1MiNWTAHRn7yiZi/o7rzN+hFJERD06NFOpGGRiHzQnuraRzB2Jp561/B
G1UMB+LyuzIkqmb9ivIi543l6Q7KVvhmOAsYONwbvDTKlt+7sLfoMv2jOz+PFqGjUpdTFdk/yKG0
9E0cvgEJQRIZRQVQORIl/Mqb1OFqPdStd83ezb6IDPz/dspvDMda8FQWwTPfRHytNf03otXXVHjD
Az5sSHdEHZz8RtvhuWE5Q1HeF2Lg531I3DGVxaEgYbVU+9mn0JPFibLaD2E+k6NdMiwVzmDtop2z
AoMJcnhXgT6iKDxLyBCxBhWnkUqWGRkkS/b1XTO+d/mf48EBkEZycCOT+3j+iM9UP9NR1VrT4tGV
zhILo5J7DT6ymSn9CyYkxta2y9WPFbpJYYjNbwjL14cRTXuxLVSz5FPAoB89TCdH+AV28GQOcAHe
z1/8t7mzomkhLgGxsQhyd0a7UMMrOi11k6A8j0y7Q8IjMa3xv+8IwPJn2MWdFiNpu6ZICLd31lsK
iosYFYLo9a9OAPUiT3K+Pivlmv3OvNMKFn/z9KRHMcVDxqcghCSQn5H+iZlfJhuGWs3O14K4GB9d
6UWC5WwbS4N2kRqCGiUKcRlwiKzDCW65G1cgZz9ydHIpVb4WOAjOOyx/Iyg0zIxDUdAx8KeMgMHx
i/MNtB/hEKnRQOrQJ4xvvwI75NhXJFJp4HznyZAZmzqYTuWOHqsXv0XfHYzXr68C97wVUzuEnVDW
niR10Nv1mCyHa9BeBpov585q3uNk/lRZpcXuBS6FJ0svBJjfOH8snWP2skZCiH8cf63E6HIwe9K/
1HyyrwtbrjlZXVQ35U2CrTQaXnZTYYa624L7ovrl3pAnpEzLuKidBV0lMKOj+jO7I7gHX3uMFWHc
y5iBe0+7N6KWeVE9fHwn4P0B4y0VAqsrsXqHgjm+gKbtt7P3pOO/R6L2d3rPvqxoouncjqj7y0Zg
eV7b8a2tWg86jA1diyLyMbPK5wRw202Vf+/jCNtYuNdS4LxL3FMybe+sZL19Ml0XsL+3SalmxDTr
g28mwj4saNhEiNC9Y6ir6a9S4TEYUMN9Hp3T2Q/2BJ49e9u/qEuxvDe0z1yazH2mFchqNvp5c4f+
oUO5HaW9fEvw2hdbGOslKHPjPs8uQx2xnD8aoY2BQvYCfWjjB3e+kRmgBf+34SUmS6T7KuLrEnSx
7orbYVom2uRfgKYbTQn5LfLP1HXelXqYaug64/p8nuXLTwnaEbei3rjWuftFwCJHxBwVqh2B/yWc
v2pZjO9DzubkjnEP8FHK8z+/I+IA5BmF6BdoSWltTLEzJVdUfULE5+y/av6eId/AgnqZC88lchPd
EHX4B+Sshvzoc6Di/PiDhm/rFJkAnGnZn8ruHZanRk7AbWrragD6tVn/18aY8ZhMKILPrLJsM/kC
L4ffk9p0ED7xZZjgLAV/69d1qUn1PkTFDjC1V3+enGb94far6WSoUstedvMFzQMNlJ73mhk8ILxD
t21JqO/6tqW1tgQVMTDK2H4lPm25WA4skrY/Z8y6krK1yVU+Ca5WIrRLI6lHLdoZt/wSixqikVOw
MUrSBGjj/uCr8sYnnK8xHJ+NDwwFfIeh/p4Rl7fzRNjUksOoSa/OO30EUyOPTO4KTRHs8qOClt6a
c1DeTuuZ9Pj0mGeu7uqCii0X4vzL+2jkiTuzcs5C/NNWM1GY2b3pXLJfiSKYpNYffxcTaZDtl49e
YCeRkTDPH2AwQiKvm9qnhOBhA7eruU2owZo+zoNMBIrsY15D8AczAxLopPoCLwJ+5n81+Nt0brry
mE3yM/JNe3wEhAG9jsousLsb2AdBKvKmj5f3x6/ek/UNYgmiUl6taI/k+wrGDf3oq7yeOmnGhZOz
27DAqRz0XO/tneZdHyq9w7qc4bDF0O2hYKa7uxYDTVAeQGEg7MzSqfrts54DCC+Zp/53eCvc3Fg8
KxS4sZvJDhdkSCBdSZpRMkq0fUC4X57prpAGPz8ZQ8IfRh6ptPxhDVr9KPdZrefjFJY1tNMDRMTC
iRVF9Vihk0GgwnVDyohAdakAX+VmuNDsZq+uMVd+LVwZsXKjDr2mqPbVlMCliGJlNL0CuWOzuBdk
ldBuDMzKX6FYVtGyKlkeWOqHo43VdF0IWOgYWIusBGLa0rNudkABvbNVqHvj8f3iOe3nNu2/QYwh
Ina1CqKAvYwFN0TvtOTDk4/sI7SMFCvfQJ0lMIGiQt+2vdMBhiyZZYuBXSW6xsp3yAB6jVkz+ZFO
3aRz0hJDNq2uy3NM0hrpj9z1BsGJV+7iyX687qCCul0llKCMAfvEa/iEjfy4rzbrDjgokOaNUvN2
mrkmNrjOxZLKJd1El2aaiini7Sh0OGZ6QFF0kyUXUsEK3SGdeYLNEgJXb1iO1/HkgZP36cEfXJ5R
OiigwA5fgkiC5RoNMD3uWz9VGrBUJ+lGmoypz2QK5mps639lPO6tsu6NEYabfmSthK01sY9Oe/rX
nha0/P0h3YxUpg+AmsIFfUk0brRQ28JPVGJLsqQ2H0ipluBntlIcdONUEN6rtdJTs2QNFhmLnCPC
XNDOmufMtVoOvXORjTcPwKLoekpncJ1+MgZUJfgXn8zE21dlg/GrbxdVtPnIeWIAJeLD+qaOe7Ww
lpW4rYVOpI3Tzblo46iXxpL90rJnWqoDJUDiEFe7a6Co1zL479kzOfQyb/QSAmClbiS7QsotE4dN
WnAv1x8mWV8qA4QTKqBaQFGoQZ43/+Ew7PmC0X670ud1RT5fVZxkSnJ3GfEYgOx3bV99GBukWZS+
u5CJsIGyRz/QSGc8wEGf/G+ePSyvV/xAjDoXpnslp/ZDua/6TIV8EUSdga5SkDrxPlY1OZEiLEsZ
OObz31l9m4bKfu4FdVjw24ns57BYjiQaKzkbZsJEoRFHpE693rzb/iqKJVGDaMEbO4Dnt56SFiB6
QZbJq67TG0kNobQVp8z/GdV7k5BalGMvU4tnd0rNoYX6JDNT0qdfSmVDCEz8nwednHWkvIPD6wzQ
hFKUNHtaQBh3Z/LbQ8+YSl17v7f1lbPPC+ALf4jDDUFZe6oULSyRmDWSgOICvtvx7WdVPHMkZJ8l
s88G8ugyRTgeG95F0j0MRUwcLcLNOhZgPq6dUuP4Ca13PY5XQz6JDsx4sT8RlN6A3sBww7OHkDoJ
1O4duB1E2ouAaQfMZXEuU8NJOOArrKfuDzewfDQfbrVhmtaoIdIcMm/5DF1lUtGkJkOlza+pNvZc
fQt+FLWl+GtbsbFmzll/j8Pn9xpJHibo48ZxMi8FSwIOV6+sAD8DCdilPnNQGBxxzcWF2XOj1nFx
3hsNmjGarkwS9uKIDxoF8/Y1JJzFQJTqlwEfBaB8zdBSQ0c3I/+qZ7Sr+pc/XP2o2IJre2vZCmxk
o7+mpRNkHFovtAwaf3VQZH9q/OTHTxUe6tsLw/utjzSZp5J70TTaz62w2bat5N5l9borMoL4+HR7
q5Eghj3GGSoqnK5GWWf9C4ELd20p950nzWV+ZiY9OHVwx+z2SlIvGSmdXDfOsusOM3RMz2GkF5Zl
vCQ6fWkPap2zvre01gwnfTyZ6Vy5ocW9FTiYDG7aKkKTAH00LsfkCbRXLh5TSi7r/eDJNlQpUEaS
ASTcNYdzPCY5f7/x3T+Vz7Xx3bLco7+2yni/JPd6owfeSYq/0deBGM5rDO5n+SWVRIcjJX+qM4E/
s7Jgil31a0K78JdNNnCzpgEdHRuN7NmrP3MIw1lOw1Hho6brLC48QhG4J+t2izAU1ocqKY+hXdct
d0U2O7D5eSnUormfieWrnmnzuNLwxoyOV0tXegmpxXVcA62GlLBCWKjyvhjf+6Cihzq4Oyo4cG1U
RyodXgjsI6ktzY6JiM+BazI8gW3pGjO3YkNqkqQ1TAxOy4hozxHcAEdi5opLZnDqxfJkSHdkcCPm
FDdxABEOjR8c+3ZhgWBFmw1A1qCEoOAAjd1YI8aj4Hk1+oeVGy1TUZhGXZXdt3qoByGorW6/TyAt
IGLVau0xx1C5Bc61nweqTCnXlrEL74d8Nf1JctHWlcErs9XHbZqCeYXfv6br8raOMjqy06jvaOS0
2v9oldt+rQEQ38wqs60I/ES6y1yyzVnAm63une/ve0sBGSYkvW5cHYSRpg2xR//590O10YtjqCvS
9+PlmKEZHdzRG8xUzOlht0c9fh/qtPmaZN7BTMI9JGxxXgYgBCIFo/0jzxuVV92/Biwo2v487Bdw
2eFlekRmduyDsRhZEUeS5fHnr0QQKOqrLKTrh7eYE7vpImT4NOXOKawU+VOEwFqlxrQrYpMLT5D9
WI5LFyZAvupqcsvzX7Cqbikp65ynqHAOyKxkDgu4RRhr/4+3YMBhjgzSL9/Nngmk3BL1CHfDkXsX
5PPD5+N3U01P8Puf1szVFdcM2wzQoST9GGIZpbsTsF/VdH0k/NdyvtTa1JyGrAkusBl2tYcGOpaI
m/jjB9Mqd1I7VJq6oho5lKO1Gj5cMqQG1d7iRp06B2W0LPgzbGQ4Y2LcWFLWgstsHI9yQRqyfOaL
uQj2deZBt3Tyb0zpjsWYAvb0WKQCt146ypOM0CH0V5La9H/KTv64AJePKWW09tjpqp5oaJAqfPOo
WXipL0SsFbZQdWFmqI93+jKs3pfEDvlWi8lARv4Rh35dE4yE3Lm1H4fO19gkRfzBx3bCXFFVBtDR
tfbPrVFY4UAqr21SjDvGVIgNOHsDCJsl5FcjhEplduJZBUSu9io3ExnThK9aWnrjpB7BbNLsggVZ
r0YisH9oOS6QZnUjKExnIMY0zwFcz6fBmVN3ti0gEFjP//6jPzRZX7+ADFrAV1JHHi7JxJnwyFth
q9jaJF9Cb7TcSEyW3xODVlsZX4yhsemDWhusYzfQ+c2ICZWtrbDsgDQ7jH8C4FLq5RtLe4sE9AYj
Jj/tTFk299uhrX2iFp8LxEbu7M/FT5s8BXaWprfsvHTaVtyGn9Q6qd6JhSlpIszMnFCzt5c6qdbT
RgfQciFPxDJSZLuwz291oKvDOSM6yXG+KSDzb0Z2yR/QmZY83jx2WSh47ihmiYubChQTv3tCSvAy
Xk2PsZvpF+uecnhMzd0GUjlqgoyq19AYmoIkW0dkldHpDKVPx7r/yL15yIlHdCRWVhUGrHhiHp8i
xqPn2sAKz5hvkpPSLEY57k52yOBY5hYMhtPfNZ4fkfcSy4e2/ceRFkMD305/c2CUPiNETSThEv7n
seE9n79qnwDe6kFop/yit1m+UmB3sHleeGqjCjEmlcjd6UU87UqZylhE6GXt/W/ySLUadChoxnaH
QZM3OvB7SwxdzBfLq7LH875w/oHjLAzylYdBB1YtvyOXDT17y/65jVzcXJRzcgdqv0lIjHaoX9n7
/1SfkSgsnXcDoJ4Bv3wVhv/htUVbADwZ6wne8yMys/bBpAVBiUtrwvVBDDN6Y9zGqNSqs2u6rU27
HuDGnVYJ2mxGECYyeVtsDGFVMB9LdzMA8YdSM6ikyMOpdbPTqyeHmfufjEPsGqnTysVVclH19oh4
vGZyotvY0v3GyQzObXeKpimc5DnvICFNzG43RuNyYm+IOfjm4ThAIU+trprifPNjqGB4kO0cu/6u
KuqINcKYLC4OHOMf29qENkg5syQbq+gP8LjjvC8Lqjfi4Fle1eTV9rTmgpwd79SdXptCaPhkiSsJ
bw7muolUw5IuLlzRezetJJTJ9KkuArTgpUoDL/1LkJQCGCDdj9W6Ej7CLgdKx5QidHB+/39acpco
Tv6hxb7xSVjeSq/TmqfYlLelIVmqClhgOJ63N+o5ZtuFvuc42dgjxRJeI5YpRTN+nKgmJ0vaQLeH
PJCzX2J+TvPuNSUm/mjR4ykkgMK5QQsTZRcG0ICGuzsMreFjlaM5YW+qPieOaHRRa2R50BBFPMzU
HzO91AOdE6kmock4rfzRAFHe5l7BZzmXN5vJJSXENF7xuR2+TBw4+jaru67EidCzKaseKY8kuYeI
q1FltTy4FvvgMa+zximqcQg7SyPnvL54vMoFOxbmSa5xpRalN41KXvKTp+XCI5nJySG/69vT8pxh
ueHFVatgvRGIjQkYl4Ud/DxGA+V9Dm4xwDxnOeJVkQ5wh/4t4I3giZv/eLh6c5P3frCvJuc9URrI
DQpbdM+wrxG+9Ck3CfGVEVa0+1DgWlKkmL0afd/Y3Lk1+95JEq9LDluHeTy62KyWmSKnE2etnUbi
5XhlvSWVvbHpne/chuE83wC6hP35RD8V7z8+rMYiCY3yGwEEH+4P4TKvw6huPVYueoK6CzgSwM+c
wwgF/xPQwaUk0mwnqhwnBCXcv7TKKXBjD+/Jxpiz5vjiwDgAdWrHgCVxgB/31gEuq8eRFXbfnT+0
3YwX8sV1vXyGGaedCsDaR1CBlzJNd1CnsqXpTHCNRHlNRIp1CD4rwbRGCwKyMdXAAT1Z+z7JqEg0
y0Suzhy6Fab5DeR/DhpaDQPUdcgU/L1R7ehmsBMeAZcHT2EAlIDSY9cPqQF1ZTlKulSMN7K2V365
qLx4dpetyu+/Ytx4HIbumvs8Vg/gWgOEaHuOyoxWKMERcB4BbUnEEPM2WpaA8FN9Mvc0GquikgLN
QBPyxhn+bysgPKr9VYEPuBF36P1cVDNdQG5Tejro46V+D3oWZYpezqkAGWcit/AT0Qa7anbnOKyR
VRriN3pJnDaL6/2pquTu9JIMgetO9fjlG9GAd1hd/eOLdNw8+qqGeynOlkqS6zhsoX8cjCZR9xEA
bLFkhVH20w/a5YKwXmtauOqKfKfpzgS20RuKnck70i/R+jZcT1DDlKIgivL4eUgKxWecRX6gpXT4
5cc0BxEJDFBQ04urpTSoTHzLK7BzBng7GP1c3vkuvP5iqj0IWc2GNYUlUl6CuK+Ihom9TxW0jpk1
UNi5WvsHNiwC+/F5eRHx2epHXWavuzM3eRFYIGxtpqIfnb+vf8kYQwGezUj+UikyoUJsrxmQgs+I
h/3VDUdOF2NZ8599F1Z+kcnHEwDGswwjiCgtzCLmVcT81KISwMsQ8+UVL6UeGVzhEnbPeE1dFKsk
ZCDyV8MIrIUTclhSUjj4Ytb43jYD+DmAtUI5E33ImcqUSwc2iwQchAxJ+vZd4Le8Jn7bzna5HCfE
d9kc64XrauTUJvigangcEeybfolHMjxM+b5gopwD9HAnmbfywmXcHC2G967/sCpY4zP8gIwc63Or
5vIHNINSt5glt1NhJ7lJyewAaoMCnXGhfm3yzGhQZ1lEP+hpH+CIb8Jzjg16Q6ZqjHdlcg6mHV6V
LmmH5tIPr4ue0MX8xFc7dsD7tZ0WlozMMWDq62yeFiH5km27bVmjRJMN+RZ1ZJ3/y+YvI7RAwkKK
kzYsfVQtMoKwQTj4jpLKhw+oUVzflG4kjzSovBDy+VVD4RFnlj/VEzcnrQDzbVfgONE4COxb4YSp
YgVJPu3FS0YzW87lICsnl1TR/0RCzQqt9MiJC+GZvuwhJ7cIqfHbi4nSuv+mqeC1PQOUWcvyBLjz
Qgywilz/SFtjfU8KK1EzFOcrwUoo/F/6kDHT0Y2GWmyezdht3/b/KNelbAvHPi5ocrAkbo5p1yoG
gOIjf3pmFxFZcEJprXyZAWBnJA/6IIpriN0bOVMQZJIR99kw/+B4x3pTHJ6uY0bNuGkbPEJcJ2t7
Qo8M5aKMkqNtXbxnEmbQmd+9MRNcq2FRF7ndM673irfCfwA+61CZq9hxJKdy9sLcYKxsRpZf5Sq7
9xel8e766kJDpZZJoLuBBF1ATbf51x1c4ChRNSQ4PTbXS2ZkeaslGnH8B5h3SjYuhhc+kAGtkF/Q
dPhLaULwLS4kJscWSKBwMWFV6glLRL0qMmCgf3dC6wBhaIuMe02taCiqz/g20UVdk3fqRAiRnePb
sEJPN0WmnEeSnmEyvnQivBkmWJBPQJMTb7BAuSKLadG6DDYK5iYJmxNEXGxLcEjac3rgDGjwpPt4
sE/2sTDd7/MIiWDt849txmqiKxCugm5ROIjt0vzJFdIZ38M52HPHF0m+z6mWBbDaijpCZEnG87AK
WBNifWN8lSaUr4+xSuCJ7DKG+1Sz/TRAK0A6bq6jiXxGZwpDo0uts4iWHYI36mvCOhwe3WanHDlx
AVX2c4XhOG3CDSOMRkcsXGMMR04G0VAGXW1A5lQac1RoIpwIhJE6l12hBHIDwzD4OYdr+jQiKYIC
kO0oNiD5H9BnxTDAlc3AqbQYDSn+70d/Viaxy6zbXaDRtupK4WP09FSgStx3+w03mYw/FGQnpoiu
BaazbEbf5ihpnRMiDIpEt4E84Py+PyCLUqBljXez1iK/YZ8aDu2FlgoBJV2SK6UsScmY8Jn65S9A
Nwg6irBKg22XbC+ImiTb7XFTqmqU6OE01RXm8giSry5bY8B1cT0Z+WsCr9Gxq50H8OAoSUhspJKg
kKO52YbhEBfO38RqPwvqGJ16rF31Qbxbpw4qx/wuoaVmmXJoCGJEx4osc743yXDZqAlnhqfdyJiH
f1KFwQH6RmS925Dfyyu28DY4Bv04qO0oc/YCc3aeuAlG83DCb+ibyT4RfLKRRz+Ty7+4k3+GGPJi
tdbC2Mc3n0M6esTz7ezHRpIkd1gwdeeZYvYZfUngSQQWyEP6qA4P9/08ogaxAZ76DD80OiC84QpI
tzBLcAekGTk1TBk5ovEElaoXg+1kmpGMqGdZYk/tWqQRB31MLE32ooRf11d28gGpBTLtB0/kWtOJ
yHthRmnI+YDyQmN1x/5UEX0xYn+Nfvpc4rNi74IdYaqZKkbv/JaBvwgSZD1sA2EHUgXIkdODYl1K
cGC65HVXnaKLQ4/CMX4z8onouyYGVl3ayx+hGWRN+YvQZSgI6AJl57OWvsyBQWWoFtMHHF/ZXlbd
OL5hUDC4x7lb16jQMXH8qnNxzO4vmMJqn/amZvKPAi4cT/h3xvczJAu1MlkbypfhxZDTzJjY7+TW
EDvRW/WWM8t6+fosrSfNjGlOq9nIgHwmWqN2lkuKd7vitCIiaTAnEmOcqGMMXVAtZdamLzBOOqU4
CV/gqGfjy/zORa7VYhW9N/QBWF8x2wKSRkZEHMdgsO/MQ3F7duUnGdAYddaOWrMpk1X03I4h4SpZ
pngdtokqdyTmYW8T9qIqJdFvm6xywSg2wzqD3hervDBmMmBUaGb2dWJNFde5BkMDuNtFgfwFCg95
oyE1hlqBJ/AjMR1JJaMa6Q+tGILY5gX6wwaRYn19GEnrX+2IGRNVQQ4m9qqwxf27XJ8JMOpBoeTB
zqIm4E5NddP5HJnKouRwIvNFgKCUcCvwZz0v9I1XnsOJbWfb06PUieoxtG8rsdK1s9/kONxlvo3d
SPpVhc95kE+YdOS1PJlKxXn0G8+tx/pqrMrryAI64mykp0aHTG6wSrXGLTY9wXK2ZoNUVRMaZs7P
zcdWd6lgj0RIHIq3yT86/ySEdg3d1jKGk6pAU7gGduzP8vdZEiwfdVDJJpMpEpUh5+xQQYXd04T5
nM9moQmD40EhjjBC4Hdy+oi35qYIQF1L96hcM5ZcW34PTkuXhb8w7Yeu/nQb+jLJDgXZ7RrzzlUy
MMF9olqurCQWLdZj0wyJGVdtlXyH1vXN3i8/to2Snifd8o/f6XIu6upS7ehwolTDFLiJmdMdnih+
eL9W12SmhrufxdrmAm0Nzt+Dqujxv60yWYQO0SEoVn7B2RKle9vJjmv7hUSeQzQMvUYVbjo4ZXiY
ljj2xzkruC8ShREgwpPVVPPurpQJDAILqiHDYkvnS+AftUE5Z6MAhhSVKpThB+9QANHC4e3lrrNT
kq0nQx5v5pjd9mUczrMAJbn5Vt9ePtcKBMoU4GWHem5YcCscGbpJh6gQ57veXIky5HyHLsSiBoRa
1KFUbP51PP9OS9g94OKGFoSFohQcpQ3s9dm1aPUv+LQYudbOuB3QG0obzfwttSgDkrdOPUb/mPh3
CwCEkRLYaZo+UI8xy5FlS470OccMCQvrG7sPUlqFuhw7UwmPqV8CVl+NzQcaJy1aGyLXc4jLp+sr
b3PhCBpjgMC4lqAMnFLIH8iNzozlj1lTEhR5ozLo0rucwmR5PNF6zaIDvC9R9ZAelBpXjL8o9zMz
DQSzekNgCkkfAwSsWFb+5blhv8b6H/dxSKKBjaUYwaY/b89loWrlxbQYd31p5Oh1y12phi+CqIpI
e8C1a6OMNiQjS3fRya9ySa8TQuf9bZiy6wszkQ4z5m3pf/JMneALzId12iFjOHuVJSij9aCNjGbo
5MZ1hdeAQEZv8IP4YhqWbqqCtaCNjuu41MTK4e2ITKHblaSFH1X8x8gwyTRcbIOPCKcmJvC6S5dA
dKggGhsDrmDuOVsEBsAZ5tnW3Ww8OOdEjrnSsz2W2gmgHzwsjREUedsU/v+x8P1n2lHNrJs2gcQR
rNkszfDTuMdh09lYwI6D5fMCafSzpCbEQYrHw2GcK3nE5ebSLiW98Zb7b7/QuNY4/Igv+kEQXrHT
xP45bs0m5mfmrqGtboONLMKmxZikd2iSYibm+GGV4Y9OUJwOEv6VLxiM2VfNZ+OpCyUbJTqu9/vG
NtmQCbeL+AvO4cEiO1Fy0XAFkBEIXpOC2rdjP2B8vjkdDbd5Z9qznfeKMjxme0xp6nyqGsoJ6gh8
ghIjb7PzEW/BwcKGP78R2fOkAnP6nMv1B01w+vBxYdp/iaQ4197d9UVwCBJev07pNANS/sxmhj0K
+d9p4nhj6/twPW6pnYpiWkvh8rghVYtpAipM0nAJ211B2q21GOXh8RTARTg/upZf7MZJpX1Os18Z
37eAT4neoNuJKc/hAciBDpsKSPJO/Fsf7Z0V2wC2xX4K1x7LWl/qeWjseoKrIb2siI4+eGAzC8JJ
29XydxpkIcvMeLTgZ4aANOeAMYmexQaZ3QiFRErum7f2z31Sr3U/ZlFK086VIcKtvoedR7UHmyZp
PyirLadnqIQrPDWIzU6s6q91Pq5pN9EwJpbxTpyGMLovg1wFmSxfuvvXh07BG/8GzRYvMeGd0tan
49BHUzmXyVqbPqHj0DvdgGcDHnrllm5NveYp3fFjWqMQra+s2Pt8dTRkg62dgwiHwpUg3TYvcG48
6t+p99TwoarlIjKtztyYFUhxpUq46ujz338rQI55aI95dqFibQatRZuytdkbL3WNIunIs0uorWuk
iDfbugGsRPAzGa5jF/fHhRVHXQqZpqZAhenGBBSxDXLbYY13Fi/ipFeH+6FBSxCAePmYmYmoccE3
Laozm5CiSdk9pd879HGFtWgq4XkTumZ8CgHDQ0juO8use32Lzi+C7txJti5oh/bLYPIh+MmlQwco
OTs+U4UDvEHusLbIOzeepKsszx2a52SoB49ZKPhw0n0ef7SdDotSD8YpQqZQhzJLVaLlimZtSBS2
wQsG0OTbEA9jK8Ci+kHO+9CnTLOCmmkl/hrfwC52Y5TfGbarkmdpdrvTANYqDWnCsUirUyGiML6S
nWNeWSmpWj0pGQfktv8zgpaNKHDhwpywCbZ5eJ4K0D3xMPYm+N8alRjdbd/DP8U92a32KFGCOLHB
iESIhjZGaNsF2iyOEMKoEr3O0WZ2J6Nba0IFWwK37k9Lv7ibYBjaFgnF6YKOatgeIoZKgaLZDPjK
AgCto47kn6aBXotJnUbnypu1FBm1PiMt+QDd1MzwGU7WHaWyCBKua6C8BoVlIAKsZE0c4cFbuVU5
D6mzvspyELHhqsQrk8ulYcPFJbgCQKRXkp89oVQH28sD0i9pxqDQKFiLO+g9sXB5kGYpuSBf/qIa
rUIn/IGfaHaMVUiRoUKqBeDpOssPaguKckRDRMtoLLSTKpDP9IzW7RUw71xDtVMfrf8eVRJRE0AW
/8Pk7w3Hhkx5tHDgOrnw3ZRWeOcnMjZa7Qvi4+8n+2sRz/MNgbokvbm8V/sXXO/MTfGv7nu7a/zO
ZgFxerf1HJ7piVXtsrPLfSTEfqgOABAExEb5lRwdebBBZ6OuKoZUxLjNMnyvmF29Bn+GJbUdjobk
tOL0ukZJdOJ3As8DODyjYGl0gwa/HQICQx4MTIk7HCcnk7UUUR/ienF4OB9DP0tcIM/TlbxKlUfE
gAcvh6tN/sZM3daQB87DgTDa0TYdhARN0f4QXgPmzNhnWyg1gNsmB6wbSbxFmCs/CdHNQgVRqDzH
5AcNqJL5qTEViDwjyp7vC3Xv3ZXkvZOZ5Hb1dX+2Ejvr1YLUBWOrumCokqSBOOo7LqT90JSGrBMN
xmHHSYPJBCVwqJIzsr5a5cf/Sd35ExJJRoFypt4W6VO/YKlqvZSULraZWR5enI+6YkbPbpizvn1S
I4vwpE4zXjo7JrttLkxIO9gC2PbBUtVvRr6q5JNXZcCPfAEblHcM5wdkLW9wZwWgRPmcc340U7Tn
s+BZ8jSTuDMWoBS8YgPTUKTTQeUYVGWu8cEk7SsRzQMJmd9Zuhhixl3wKU55iEVDDHlovMhPelXK
okkgDckZ+VttLcfMHhWpmFjyJrxmNqBBV43a8mwlnaKJMUApJWhtUh1hJB7Lnd3lg4J7wbIC9VER
Pyy69pXXPo4AmbHoAEEUcfQfWcHOB7n9IBdgYRzKXszPxLitWmPMqLzKLf8zokO3Vcv2po6Eekgu
X8Hm86BAfMOlVFcNr2uhaRTx7qyCUjlz7I9HlBLjIPNVotbXMQ1YRBXsZ07ClLCwVB5YxNkUBWN+
B56Kl6iLnF8S5rJ0r/p2V8qqNi59wl7TNdubtazIzdudtMNWhb00U1uHdtzhyTIAoAhvXBsJLLth
TbmYfUmnmqKAT0q499D3rPxj+Yz8fXCXqWXFCz4rZHbobBtqjOa31ciTYua8IuzDyltJ9wBXA9oD
ekjp05CGgN7mSkrvdBzhncKYhD8jxWJMJLwbz6BhaVSlpmS6JoGB41MNm42L82XHquJJdj6cNa8d
SO4EsaqYK0PUjX+8Ss4N0x0i+qj0ZnP9JCG2pJkRs21dFRXAhVsiiwYn88PYlH47lwqm+br9hE9x
3BCcowUXdLW+8kZqk3MoH1IDKPZJHTNxtwJRIHRAPcr2iE5gzNd7w+JzQFkZEad+1zFRZOVmSuOB
kRUGsTPK2dp69dyMAzDYg6aWfPuK3UPcusaBfFaC2nKdXJchLIA54fyMGKvlN+yXbuWugHYEIDdA
DjOmOevC3h2lKa/f8yvyrbrHQIKKj1c/vzLkAzpnaMuMHi3mOIHhSbtCeMQRQmWCui2U2tKWfQ3v
Omv4EtPdyZfPRGxuQnCFPwqFKGz8iHYglRNOnqUEXN0h0nGSm6GSUMHDHBUEsm601ydv9YWQb1PK
EgehGVIZt4calqlhZ43cNtjAssTc7dtOXTSS0YqGU7v0XCa2lw5WlnFt3OXejiy/fj822qkgK4KQ
+ibX4MKRamwsUjFzTFZBlQHRB5tTKXkU491XJFm7TpVXq6R9N8Vqjzqx5dzaw0T8PuLgxsWbXtGV
HRnlLswUXq4wh4dExwItF0HvazvfEmM7WIJ30KPzTRoOHS3TsDE+inVaeJmDOczSFNmOzItUKfIe
lCE+PWOhLoEfr91MGewz5nKX51qdv8i9D2+8Hf17uDg9Eiy6xYwq/xA/3dbVmrSLLnc/1JEhhjmG
2zHpdLpk6kgqPilrSYDuzihO9/WCkgIYD42FbETV5UODfP8iHup95YsdGuNGQRMoYZSlIX2Qf+1o
Vnmr4vWtJjwd+raPHqagHi3wtiyLH23XBXLr5Ncpp8oobTT36SPP5B4PgT5dfJns/Ky422OIi+IG
LnCl9wyt5ieqQMjUe7fSfMFhLBCm+yVrKbbJR7snstNo6lh2haNYHZK05G+hXlyCTvBlDtt4s45x
md857cAKiLVn4z/dmr8qBiKo0Jr5wnvOc3U+P4zYqzDnYsPM72hFKScGE5BlJhc0AyoFs2npOKjH
J6+KooJ5O4+Nt1Xkk5YAzHzjP9gziPUcIwhH8D6CA93Z5BiC8dmg9HN5AmKg692TaiN62QDC261q
s2ezLLNcq9NmDuVnoP9XQxXbhejYFpUezOPXWJN7n4wCmqH8OC8vdBnOtKGJ8RPgE14wBYEBUp9+
Krgik3Hvw7/IDYzqZiCd+aguGAkbGauWVE9DxYyWwfTBhH0FyTtntYLy1uuccaA59eLvhPIUK7yq
OMniRbevtJfT14yPwRhSISIITcbnsEdI4COWlcr5mPGqnnh0B9mbkFU5eOs9TV/zOe9uwWxGQ0LU
b90t+PQMFTH7R3vuN0gYMAQjCehLuEYshVb7wYU7HcWtN49pvHeKDqanEDn187CFAM3hHA9x4Aag
sJtAR8rmEhb3l4oWa+tC2SZc22UkPIaATG87gD1t7YPEDUHLgV3YAY/38YPojRt9fuw/p9MYnrnm
+aD1nCneyRApGDYIZuHbCAehtHVyIvniwQFy5vhEkmMEQfIzrFSLAMl1+E4dLorTfohziy5SJl8E
Tk3Po3ZbLsIvSnDFaDJWKvJdNrC7mjhl2Xtw9tlDX0h+xUwWEJlMcffb6X81/eQIaY/0FzPTFC0s
z4mi7+ZqW/TuYjxd9UOVCOn/da5iNy7aOt71rtNx9IkMfOCeQygKG2avYuZWR0DQ52+1WugMSkJ/
BgLQggzeorTJtGHt9OkFZ5k6sMNnj4fEuiuZO28KDIS7pJx3YpzxqKtuoCQN1DcPy4TLOz5LmZn3
6UbwpF95KdhqwQblYaYTsywx4bumY0JAeQkpVWinM2FznwbIOzZc2VOi8r3x4QSvbOeiHVDOC1mE
Zpw032ZAXrPjHE8Mq3vKqNqJ4Mbt2lpN7TjnyshFJqDOUhsxfL/HFX9z93BaPXiRb7ADh1bbLupS
RT8SjZlW8JvGovUVjdHaeMx/dKYKIaZQicvdO66cmoLlm1eyQUD3siHeAHuex32TtyQk09yYc+Ic
O9TSWa3idAX3m5Ucqkf5Vfc0HWCphIRXKVCaXrPpBCFopVVjBv2ti6nHiunMlp9J+85egpoAI6YY
jzY7VYLizdHUXf4mkIWRN5hfz0+tTosLapD5yFKtshjaG2Mn91CESlpERGfYgSWfN8/DN3kPy87R
F88VD57W87gJDRIFXHdAocQOSinsVJ+/ObHZrOqqfHyMH+DOvMZfArrajsn/Zq08iu9OUAFtGwWB
Ps5K1dcJXgDJmg/gmJOTyWXd8P54CU3d6DuTK0g/4br+QxxZccCjJmwfePpsrr+UwwIVfEw1ySdz
gfjxakWf/B6blpwMleJLpZuJQputtpBehFGT/c+zN4XsXkxGhDeKj+8JRcw8tso0uwK7anOJISea
Qz/F5LTOfAxvrTgboO9QhUYu2GHduUXvM7gQH8mGI+PEWW0Zj8YYViG7Qm/BBW9kAWluYMzn4cOE
V+sKe/AzFPHpzdJPyfXX8hRVSLHSZ+x0MZJX1Bq0OUoEq51H2aTSozMmUMSHriFEZ0ATbqHsDqH5
3XVzL3I7W9lec9WAAiDllGn6gsg9BRkREsNRDLKvJ9KQopdbUznUzAFtbMFM2E2fXaUDQCp3Zp61
uydte3NXkZv+hhZbl/pK8RdksqcwmVsRZfGd+zQhdF3rg9KLQxRYrTg0T/idESh4h53tQZ+Zhrl8
CP5QP779Bg/GLLQ1vysF4lc/vS5lfyt0XSuWSffIePkI6TXpr2FwtlkQgvXJeUuMvZEPKejkOf/u
H/UTwHz/g73YXJOSJTfTpinMG2heF8wHs3DObD7OpGdkmn4K0BemtnCO6e8Wl9omsVOInb5uysLL
Yw0eOktBtqWOyrVhRQZHaIXhkdnsrfbVGlP4G8smhR1Ut9eSn/T2D7ovqLlzq1fYB8VaLwU1mCzi
ITbbBhol+SDBXJyIpKutSyIOWCI1bf04YF7AzCLnrI4US6v0QmMkn99fi/qGyMgxAsmN40JFltmY
UF4aS+uKmBo/Z+mBHKLpLRLqOQk3VLQvJI4ipdCwtquZgflVD5iXhSu3Pv2Rv3kWcFp2Z0T63cCz
V2HeOa3TVOQUJEiBnDEleX7V1U+ManS46QJ/wVNRLE0WAPzK67LirtNkXk7fi2d9rGMO0oO3L7Nc
TdZ4IUjSZYBNBuSF2MQgK6KH50Mi5LZEqnLwrljYDQosPcZmLlduPnFaCJV/pPAsS5f48/m+UvcG
AQjzlPUd9hjVn9qAyj/4XrvUBLw/n7NhKmCRCRvqOP03hwcKhEgf83NXpSLIWTU+OpDlcEpAbaEh
ev+HVSeEPYTU0yO8u6LFvmkbwNuIaP93G8o6kq8EtkDDm4Q/Et1lUTYgPtuY3VIbYmBuW2oFdU+L
45PXVRUxLvm9woktmYe+J8AQ4Z4YwgNSIQaOATCha/XueTQr/VzxPncc3d3gNdp78RYDJb5npTNL
JwSDyvD5Nj0rvgnvNG/dv94sukuJD1hWMG2SgkaqqE5VRJK75M6tXa4r0DYJHNJO9puFxUIywfFS
Coivj9G4rUEwnNcv7CoL4w7qdWMpSlGQNeXFSBwrqPVJLYb3KYhChx/IVf1FvMnCd49ym24o7ALu
ut/chFHUEocRpdT2NtviGh+GZGdG0m1S0TOoqiWw7Z4tXHbrp7JclxV4bxdqvf5o0jz7+zrlxJJZ
K6kmfyBjaWz6ywcUg6a/3RPtaoKGaM9ITIaeXqfAGhxyp8PdQoSyT4mh+2NHsSnFc2O7kPsPVkSE
fzn56dMg2yQWjlDh3iqPe86QONy0pQ9RdpNKa+nBmY4eho2Z3gmiL3/uEwqsBdgBUWPxZ+DgjZva
yI76SzkGgywoKH4BC2AgkqTB7Sc8U+oU8KOIWdWugiVhuslCRhs3J37ePMPH6Yszf4wFygc8pctQ
nvtf6b+1ftkytcx6TBkMHQyW6n9M/hi4iu0Mx65uKRQXWj0gSN+yVAMi5yHG/jsKpPlZ/Gn7BcbF
GAq7fHertNQLc2GkVPKHYGB9HZiUMy6F3QLd2dciHaBPPAJ5h0AKdtLCXTNCOazzh03L9HzzMe6a
KQz0voDew5JCYrhssR282A4eMYPUBdfH42PXffyWYo4ewIypREeuMim+wzu+5b+sytrdyRpsB06q
YdveWafxdd4qVMKR3ASj/sNIyaizjBa/M8wcJIcs4YMMf1f9qxDaSbkAcnYvTgijZtaSXK6OJvID
qg6nAot+d9tIicxEK6om0SCxfNFQwDhA9uj3gb7rVWrWUZlwzaTO8zA+sbMuGX2N3x09Iq+Czafy
8wJP1WYbyXr4aIXBI+QIl2sXMTp+BnAnOqD9zFcopyT2L9krJolBhAjlc9SHQAi772UTTHD7TbBT
Xyasa4+SATVVDmIcRLGwW2wWSJ1TPjg1Hr+33lVmuoo8LdoUg0q0B9vBnl3t1P+7P6I1uEzq/0EW
JYeK8Z3Hp+/H099fw5rvQLboiEqaU8QjHE8mBdeqKTEN4KsHUVJeZHXvCiDMrB4MBaK63NbRKx2c
8lQKmi6SKwa5cNvaAN1ytqtV9JjmMBdL/un9dk6mAthv1GwMYj3O018iiQeqZ7qCe1wSvYWuuSlv
RJRUYuvJdMTE
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_0_3 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_0_3 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_0_3 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_0_3 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_0_3 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_0_3;

architecture STRUCTURE of design_3_c_shift_ram_0_3 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_0_3_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
