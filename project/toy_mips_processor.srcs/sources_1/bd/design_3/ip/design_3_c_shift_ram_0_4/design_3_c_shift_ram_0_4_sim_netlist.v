// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_0_4 -prefix
//               design_3_c_shift_ram_0_4_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_0_4
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_0_4_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
rxws0jK7jqFD/abKbzQcWB3n64bXJh3DRp/TSzl7/eq/nyTOCCtzdYG/j+JiPki7wf7r2WSWM8kL
r4jARoCchQaU8IbQWCpx3ZgA/IH69u5arLxVU85vw4cJEeUcuvSCAbXZVQFIsnHZm5qt//rxYJz1
QcYKElK9Y2H6duivO6H0XULcYVTgFbse7YJCG2PTXRvoloRSdUKcrdIHD50XGhjoltN5hVfywt+t
/FdOBizjYs0suh5Is6Uvs2LMZ7G5mOX0/k0dsRoA7WCEMMbdTj0Ru9kkVyIW3aoq85htr2eDVNRX
kbbnsF+iV9hEmV5LGxKqtvwh9EnOOTCu/vIAdeOZsEZ0MOyUeAVg0hpK2MHdKwhZhZgamNq/O3MT
a/6RYdU1XpwzU7hyW2orUpIJ5CwYGt0riPP+TW760wNkiBvZutXlhGI/J1gg/SaeZ0WbwqN1G3MA
jibpPKpDego7dvG7JmHYrVvHURxbG7arsc7IRciCpzttaCRiMm8Mn4V19h4nA/xwuuL2iDt5KWUz
ZfPJzDnsDqBxWSMTpCw0aTkac+puBPqKmO1r1whDsSTjGLhpXa/JdDuf+EvMWzENddFOjGgVZ3NS
6+lreeEoa2S/g6qzdKtl72ZeEZ6eQ+3HIkIoEXEj1aSdYfv2v8Ay7yPgO6sTmdsdQpcSEVRD7Lkl
YV679h5NHO6J8NZLxdLEMzhTl/IxlF4zRI2ufz3q1MH++0dRdRhdHSb5CcYYetvVATpAc13manIz
sy547a+eQ3CUlVTR/zPy2sol7nxKlOKWMKWN2v34AufFKqP/5V0KvyypPCUcyNhfnFhg9g7WP8rA
5xSNO/7+7HJJ18eCL8R+F+Kc3UXDdGIUnIMDaMieMKQLlvRaPvVH2kyAPajgr+hIDwBNU0DV3r2a
EMHY15CbSCeKZJuCAK43u2TqIQVPUVELbxz6WhF3foWW77qWyfY8cWfAY7yi0se3dkRkmtpWUgro
KLDzgyOfDIOYF4ofwmF90QRqFdxpJpY9m1beo1tdR8OJb+7WZ6zOa4G234/aNntmx1LKWknQ7f2/
ZT1IHRdK7EV3/UaJLGiJzs8F5uN4hheFM5NcU7TlsEAFUWFNeAIf8DI0JqVMRud/2y0+Wl2+jnZt
xK6edMEjahod3O/NtbfoQprD8LZ7ylK6waFlb+OSgVeKrnkvMk7VCbphroW5i/NbuZLyKVXwwQsS
IuA5YBxa7iiOoUnY+yoSVdgkB4Vp5e1OX9ode7NAlCqHru0U8rWbcNgiuKNWwpi5EPVd5wMU1VUs
9IivNow6hICvH/T2/e953KbDFSix9j8ZnvI6nHG7ffqC4fdk43W6MMH8H+8aEatxRFe+P14ATuBM
V3KpprGeuGy8hzSEspWAVIWjvEaCYCaxAcV2naaIiIyCD2S5Xg/N0eW5kiA5hPnHXwM4qPV96Y1Z
BrDuADsRfNhahr4FTP9ZSACqfwQNcyr1Y3fXmXTK7s334bDUYFid46ZvIzw4eO7v88+QBoUzc59C
yKLbhBynBhGMPrjbEOq2OAcfoflhU8or4fwiAN2H47ZHzuUVJKH203hvEr+UsTxXmk0dZh2aq+lq
ANFjF3wsVFOKebYgDXxbxk/lvmityxTCwc+r/Rn/Hiqsl+s41Ert41WKcQxCcK2Bui6A7EYBjqpv
LLbSk7xgbl80Y46ZY3uO0LmvQN6vaOjgF5YZXcJ9l055FH4I5Huhj119SxIW0ByMNj4holf8FT3A
+IJMOJeH7mCF7hFzPKwm8iN/y79ltiO4n13G9cuUdkisXacyPzx0aqVYpsn1T8tCMvWI0325kaqH
NFSFrnn9ifUK5McHpxwmj5Pa0KCtVRNNHlJ7xseANUcbGppes6NMfvWd0dEfk+K/uqeh3CKEZvNU
TRkFCqoPXcdO/U25R7O1XkzXg3WYIEHeI0I+LqmzsF4KfUHZCEnR8/G87tqUEcWqCu5z9qqkIkS0
PmsSxJ2bZazCQnY/BaNaJT57TgZuI2jk+D/o5arJj+TT5bWXRMBLDWvESAzmDXQV+iYCjeukKhYo
TtFnQNyoN0eZ6bRz8qpLM5Zg4lfj1C6OtHBUX4IKxwqS/2gjD7HpphSrSjGxtjSnejM9qaMH7ZUD
Zc+WxBwiG0ULhqiAIWAp1UmmX6H/8YO4DsA50Kmk6LQd4HWemDSTIGOjq1IAU4cwrtG8O50CCZC/
FzFqT/4va6VMDpT68N82jcjYFjJABqQbt+FLCWDLPr3WT/Ome37LewQr8NJG1C0oHevsfJbh+mH9
ZFPvHwO+NgA3TgykhU4UvhnoN9M07OnEGXH/FOmFVydU+fkRPZppcpeV2AjZb8+3MMo8oITTB7YU
4hiLWvtWB3Y7xNffDkm97WzrtqBojqr1iP5wwzDUZFJRp25fJPEfk0QdrtLdMj0GC4MDpKZ/RohA
uxFFqgo3f8AMZnxJ7urRFyQJmxForJIpjehVq1kQBij51IHVYynprXe+ckxEY7ZYesDqYdfW4qWQ
3cMhRTli+kTqZ9gcD1pJMGkojGgV8PsoQIu47mp2CWopXVBIHwMWxmFdH/4moaoadprBJ+hxlFtv
YVxISVThToBXz8yoKehND3YTwRbLVMrsAyB+LdyX+U0LPOrrTlhlQVn6OHWwz98Q5069t2GZ6umu
1b3o98ANgQNxjxx0xTReZPEgVgzPWhnYlS7E8QuFqkwgHKuG+bLIPG0g2Se1yMFSxNXKo9pKuUaY
jdAIkm2HwfMjks3LgVY/+eugdm1NTPw0NUuHlebJTRDftCRpi5g2d+2k89P3fmfbUwWd44B6bPQj
rBty4rmPdSDc7vdJuQyRkfTaTlsrdHJ5Y6WxELKa0+chH7jWYFz8fZf/yUxd1l/WIwAPI+z4Xw9M
L7rFD6NBzxTzgmwiw0UIVxvB9kUnOnHpiUSEXHymRMb2B1/8vOBu9PEVHm4FqXD0vaKqUpaGS1oF
v2oHUNxA/2j4kwn3seQG/COtHvELKnjqSimGwonrf/kg5R0A8JpT3FDGWugwr1tSrSVHV1wH56+q
zLZJlO12MrvhFsQqvyTOywJsfgfZ654sko7K8OEZD9QXyV3+jPzSbhTuA5zqKppWvShUn7fFYBEu
WYItdvf1IXO2VvfIVqI7RRw1CoCXb1f4JfKFHtSk6caISKpEeJQLvtqWKLHGEjllWYxgyMqm4ruE
s3NuGe+NPkvrVgoVxN64o45fCduhpsk6Faw/Rq7uoGFAK2u1jFgdqcCooi0Z1/j9oBKHtUupTvH1
SrQivmY11+0ZD4vgRg5iJ7QFSEwTDEozUNXdZvpWFpuMeev0SLSb4J00CfiLisrg+twiKGmCYo+9
RqghgWgc1152THQlSfEkaEbApEe4muLLv4BeBUfeb7+62ROoxPtn2+OkzrIgx+uc8WDvSOANAHyM
0ppPZYz6OdvqdXJjqjKso6hi1HjVSwI4HiN7+3SV8JT7U+cm8NI9bxPTVjXbbvtfdd3fzLxvWAfk
IM5dTZAJndbEtZDwRds5mGTKWllxEBtloAovJ4qqBN7RXcpGALDhRavp9GJYHX5SaN2OTlI3+Kpl
ml3OKwSn2FxjyH+CU169Db8g+bhAcqHZpOxUIgCDigefDokv0AKkWQoa0LfpwNuk/chsPGSOwIZ5
4ZKcjqJi/YBBDVjU9zDBTgmZCTjgMHOz0OoQ/QyHd3c4YC0yDr9+uucbCblvKIWBR9JBZQe8GOQJ
EiejqInM8aX2DZ2n6XwoiUE5XNEyl8Db813okmBhxddq8f7CLt+0UX3kMf2kfUSPoyc1JuB1P7OH
j9cev42+HK5UFYtCbFRxE14yHwY8FZxqVH2wdt/x0YGvHotJNXorJw7zEBdjU7AlLbd0yYEDmGY8
RNetgmLk3J8o3aFK3m9G/cXzBlLaocjCNu6vBBrSwlSn83Nf72aHXLtRLOrauVVTosrnFTnSlDcZ
cSfGWH3mcS14O/6imgquYhJCj16cM9KrYPRZaNI9pxb6XZ2ijPLVcH9aVky9Wywtpyy0rfFcDKpo
9RDGrbunCCprNubGKrDXFNqomou/jyhUOEZLh4+TCk7rW7mYG5pbHoSnJEt51Din8Wi3uoJXpdNs
DhdllLoIDdlIHQhER1JUB8icRLo3sDJK40HGEI+9jKqKM6UAu0hufd3Z///lW14fW38lxPulou59
ypHQkztvCQ6xzk+D6NmTRH5jc/EL0Vi6qKp4vMJFOXPziHZGWwWltNIaqRPyL8XsyvMcsdN880S6
OFuY1vw1Fo346YkK+Z3o22jbLNr/3qFdK07hrTVhFc93/qJpRSGKKwpfs3vDT/qAY1XMuSNLexjc
BravD/t2FHvONYG4ZbPd6HGyhwmQsipmpJrlBNmtDS4AwZHKAVP8WCkHZZ8Des+PQ9r//6E8qZnp
9Wcow6Q67S5VmZwKyfqk6NIjdZezg+XIUAOBuNm6qAOf8GxC1mRIyivJdugku6EW1woY1Je7cA3l
0UeYx5cmOAgb6kFZv3p3PzYtDniQfcbcT4cnKOiE/fW5cUpHle57Oi63GOCaEbeoPyqd8A1bwjFy
+wVQp1JuRibugIHloSu/A6y/YISTZ2KtCz/WwXAUH+AIqRsWYI51r4k1Rn9BH16ZzpNzWgSCPF6q
nq0B2DE0YV92WDBR7taLxuMHvmvplHZ8/nfVAKX4obd+XmKiRdJdKWERJodHmyFRcMy+6pTqEQzy
uubLcRsdroe/pwVlb01GXfT6MVJZxZqmmbZrI+OvC9nlFfGlZtXt7i+dr4v9HtJ+sgrUST05ZUj5
XEMOSLMNXtMlUpXSjKg9kwgUIyN8/V1bdf+WZd+tOx9aVRq4kzsRAE2HDooo66KnCcTa0QqJFJ+C
sONCULyd83lXlKiHBjXn1OeqD/QULYy2QQTAXQxqtloCHSWNzMQ7u3Yfp0HtRB3nroue2la2zizU
+VpiKVOda1EnRmz+iYrv7dSV6fvAjClCbgWFHVxczL1v93YD/yafh13RFqAhbLWU2VobETSYUS3h
vbCmiF8BF6PsEbrsbktgyEIq51ySI8cicMs7r70sVZ2nLQZ0gJogDJOFeToWS5YWfiSDExxJVFWa
S0loohBp9lRjVWqhcqn2hukVV3BTEyZ3xUCDoJqQl+BcBZYKxmQfaLHtBBGZMYwKfp/fjDHM26KF
UFmW7CDQJ6mWPVtLH1fG4jikmUCik/Vlj6Fjej1cdzipoo/nKdNcpdryd3yXsXvXeeM2MB4+1R56
UKCiQuBROTovO1WY/pui7tOHh6kXRgIz7lEN2J2k5gUqBNmn1ejvf4PPImdAGYgFcPtJAcFr4kon
ec7tYpOptPBaAC2X0olbpuNvElklOd2vneWmD4F7GrDbWGoWr7u/9jFSBm614+fSxUTCI7Fylxti
4jpAzws2wJ3g7L1U+eZCqhCdvTK9N2zSi7CSr5eqqDBYeU36AMYBPhV/j2OtipvhlZl47ViHK3CI
lolTZFTFBe7dRmaKmm0OcMlbWXOdvM9zrMhCIfISuN/pkIffkfbfwo0tu4No2Bi0FKdvpNHiJ0jN
ntwBGxydXDL6fW8Au14xKJyAnf9oEch+H/RJbhazW1Fkptro19vW9GT9VJlXoeI6ovNDtDIQB3Ua
VAMfTPYrQXvXgjXuQANMDskHcJD8ToVUGVnHC+/de2vRih6XvHWNo8LiL0qQPD6WCd98wAsvZ1LB
eI8UpUGmKQ6Zb2YB6jkqS2jK8T02SuQxOlTIkQR5lVAd9x/zxTcd8lEpdQYKDdepMGZsWX4YlD2R
LJTKd04ilSKkqpD/ZCbQsY7KnxdoQq1FhJbx1Y+wQ7sLGUjj4teJI05cpqTrMmZVs76UHjVvwNqn
MFvtqMmpRAqkQnj4xUSkEOreuefXsIfSC3n20e7wtolOB/KyRhY239XlykX/YN4neCfv+jy/+5uW
lN1n6Po9D0qM6X/9M7wVR3cI53ZNiD0k+RN1fObLuQlJjg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
