// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:43 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_11_0 -prefix
//               design_3_c_shift_ram_11_0_ design_3_c_shift_ram_10_0_sim_netlist.v
// Design      : design_3_c_shift_ram_10_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_11_0
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_11_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nzu2b6+2pwFmlGio+ZB7ip57VZx++Axe5c/M4tXUaHoMXfu0Ohu/62s2rwnnPIytcpVXOBslp8m+
qmnxZqj2/kr4JFOLdjsU3kmGw4y09Iw3NWvVZQvNURXhEBeu8b18tO3yhg4GxK2+7OE89vkbg8Qn
hcIWzjnRy1AJtD3WyNO+KjXhXaXQgrGE55QmPnHRK0t8PgoIYfRm1YpSRTjIKWn9EIkUZQD7/F1p
13uwGN+/APTI48AiIskWPu9DnTL7EbxmlOJMHUXb6CI9vDTGrieWHvu6j/orETsjkqvS3AHf9Ou3
FOsXBbDUikL5aKt+bIOg7KwwIscAfP7rRV0IJA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FPsMJuR7NYf6t/1KkSRnSMiyzx4Vy1ffb3vIh1+BfkPUR2EZc4r6K/q4wEkI9VeaDw4BGVDIFn5s
uzN/aGbo8LjOE2DYSfH9uN97dsuMA0CCt0SHZl3y4Pu/GCjWMF1u35wNySBfaOzOa5n6LuI2Bp2r
8IozQes9+g8hWhoD/xSKbcX1xUKbrVgIRDewAnWNC/RH6HUABKxv9nxO4oOA1ydNGRyrQElgn6Ci
krySMYkpqM9ettb1bQcIY3669AGl58tSy69xLpH+TVwRftsAY9cug/8/HmdMWuzpn2hTWoWGeoTz
q7PJOUrSf9bBprkQtRBsaNJLbx3pPmw/FAk3ng==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4928)
`pragma protect data_block
IDc57h04u6mdqe6u6P4fV0+MVNVpgai1Y4FkaoELq1yK4xmxHnEPTAGaAixhOEEhM1Cc7TrtSnqu
SWfltUaKz31ita+HrGSPfVOlIqzMchBlTUH0RK9gtgeo6eXd05UQNHl0Wl+3Ay14wACKd3BNK99J
zE/EotfLFeawryTh7IzpL6cVgk4XE5FUmKXDY4K2E6UwFiRGsF0cXwZ5mjjvdFFprD48XQDCd+We
w3JBOoov7OcZLpPpo3N9SENB08t3LZuwMhVC2Pfb2+EKOtmXB4h9cppA6YwIiwjQwfA+X9T11FQM
XrKjYDcEaUhb/JMockhf0+mxUXwjZUkVcxeKM3EPg/4n04M+vngKqy9BKAnoZY91mX+MlJxTvmCG
V+I3obgYoTGrFd9nNAscW53D/rd9CyJzCoDy5vtZqmII5Hz2VkJkTRbo2mFNV6zwCWGrJYfydg37
xCpBxq4lwVYVt/Swq9qSM0l39SXmQkQPyvX5M/jhdrSQ7/PyK91ooSo7SF64FWshrxwfGmysBS+U
p0U/fJJz7+MzaY++TNgm8/oQ1ryRNV2KjPjc8mHF2qh20sXaZlp6B8YuK833x+Hyc1tesY6Wk+5n
/X4qfqJWJHIn5+wh5De1fv+AZO9HJPk/SxPp/DdAKotgIO82B8uYPmB4ftjIcypByFd/nGvh/mbW
/FmOrPpm2j0TO2I6+UbkpgR0T0LHB+lF1fYN8RLEJKvTcM5/uWcYA2BDhqpd42g7aCrmaFP9dSGR
3SNSx+InQaTGKFEjA2oPbA4mB9ktUGqKrtEs0IAkuMFVgjgl+pQ6NvQQUJ0czym8xTmx1kuOEwht
yMPbo8MC0v77fkfxNYwQ4YTnjoDDjJ3tgGEsRwlmVSfOQxex/osSw+GsDM2ylNQCieckTtHkTzKc
WR2oQkcWq3poJgcbHiOxZCIZJC9fkNr2zIBggLh3uyvOCsC0KbjYOTcGircDLeuH5igOb9iMGhCo
ECv7G9nuN/tleJuy488Vdh3bApGfrJ4Xb9k7rnml9aCDGGFlQGgKPb32kwJwELNkhUkvIsG/y7Td
+88mSQcR4gvlXzRcSIjG9qRB4SQl8yKn9/yeblFhqJKgwu93oNYk/xOjwFZJH+dFDKcZMap5LWgz
ImfUCBkeofsXZaYfrfkH/462slG2AhUZiYEyKLHloH7DjamUkwXRNPHWh4hThjZ+C7mwIv5EvwmU
Y8f4gLulS0B7nZx19gGpRVYm7B08KJsWUP6pgAfNmPX2DiK9IHTwDQVoW47kK0VpN8mgyc5/Vqsi
krQDoKB4Mi0Fre2Lvr4M47Z1Z3TsRjggT1syYrLBLj0y8nWqfkQFU5RjSQn3cvwA7NiundpS9SSZ
xSjmPpNZ/Rf8Ao4/h34/SU/GYBVWCYkrmLY6aom1nmRJ/rww2pnI4bnC2yJsEXzZQBl93KAQ584r
f90PKdIiCKn14szdZqKqFjAJfky19+LXiN0OTo/Tk2l+FRijWjh1N6Z50X/Q/b+/JvaJ5p3KN0o0
gDYs/w1cGKftxqCiSyDoXWXyaaz/34/QXWjEBybjtCAS94jW4AM+EsZ6KmpbYImfiS4P9rrqdNWZ
l109Lg88GmuuJQfptvEG+VVG3NUz1a8sgLmYTWbPTl7oUr1K8BHsbKqaAlNGpm56VJTI6KbqaFK3
ztw+GqOybJaEJx+LS2Ap7dQqJRt39VD0rE7OpmArQOcIJaA9OdIQBMNcWGPJV7FoJJIFnHGWBENh
ed2E6mjPFigwh/GaQqnu6h9sUslSoD/EJdc6/QPZhkyr83Lq8ve9sSIKqw80cbiSce47+GOTieqL
QFDzZ/OViZzsAmx6VaULQ8hKxRnZlfnfBHzHzfHyUypP7X0yVTyhtmlDGae7ijnpxkEqLvd/ugnw
Cvyy3IltrRBcTvBYlpaQ538ysOrEuQXHzRvPLDf3ZyuJ1fjbf2A8iXJvIAHpGXcPH6UBGbx4bSo1
nhU6hM3YbNM8sIVTYeuUT/EYg2KY26ZsDn9NGMhAd47yapD38mmKPkfq/w0GwhJaGxyx1r0ucxBN
1VB3xGFMnWmXEnpkIs7p7+/5feR35oWyvJWZXqFHyNjpj8EOHRSfoTEPp+yx5u9KPG8sOIWk3OaC
LhEq2/31OFpJMlS8mcMFAc5ps2Mlm9o356xaNcAykG1qmB0SpixexICoG3kEFhBosjimplhuUv5Q
PSdj4GeavZXohKKRcB50mqkVShfDygEg1ffm0L//NgY9c/9Zjrbq15iTws/bRqhfoJ8KQHvkVU7O
pM0064x0SMlaLX/apUF195uZvo6PSBTrV9C+kE2MSw1o5ZVF/8ofuXhgi8K3fkRm8gcMl8ooiDuf
fCcw2ZatRxEeR/fMokmgpBVoI2pMSPb1L7QGi2X0L6IMZptVdCM4NLby0GyYD0A6ShNOZm9F6TGr
+9/GKCKuevv/ZB2u6686gPaglo/yTJb2zrHEV7+AWWAdVaoN+UOzyb3uvCA3pYAFrx8lfW4Qddu1
+RRVafxEqmEU5Q17VS9mQmtjyY09EHnRWll2du+tJFB1LkXcT7eY44UBLW8D8/8FsmC0PAdmyuYI
ug5vAAwb8zzOEbVOEtNwRJgAd0vlvBMRT8VguI4rVItUbZ5vPFiKQtfsGMTjj/uKlpZ5QAxYTo0x
iO7NwQIGywuAzREIv1JlNehoM7t/HdwdQxjXoal0BjIodCeB0jPNEyiVxAgRDMi9RDzSckPGcGU4
2GEsWTJRauowhKXhJFx5+RiUJMAZmGP0/9NP5Ueev1WQE8EmXEnQqSntciiNRJIl+HjKshaPgwhn
jXNVZxs/SYRALJ0QMD1nc0dFBqoDf25tX5fPETXH0ZkkDVIzYonhX5MpfBDpf/jlhnvPvc3O7B+i
qlJLDcGxqbIQ5VAivNQuwlStY7InfAe9/2CZLG2vpBtha6Y44MvYxMvDjnnHxz58DuV3c9RN5Uu4
tH+dRM7pz0/J0BUWVzE5T4KPMh6+xKQxB+Ec2ns7B5Or0nF0H9UIu3WaIDWQoFPkKGjus6TspTkH
0x1C9617H9ghT2UQ04y0qf8vCFgR67FLA/zL7UlRFEnHHez2dh083pTq7CSPwgBmrQQg191VOMhV
7VQUOiofkfdCqhWbYuq4PCNoMc27fZkCEQjquPcLxc1OLuFqMeCuCeg0v2ET/EaGyJ0jt5I6Q4hZ
iE+zbhs6FgqNdDbwldZqk7hK6MYu+r2CSlM1AlRvj/MJ7rjvHvIedA8XHioDPStbq4ehkiK+yFiI
eryXjL/ApOGBVXc341HlQvADFrgtMtzZVoXaKhhTkSu0YdrMe1O4pFa/3aI/UJ58qeZjN+ZeCyXu
Y78hLDNZlL91YyjeWDnWTebRVc7uundmR6a2JOs43sdjsp8W/M2lgULlkZkwXnI3aSBDIRZI8fRD
htBv2O4eYtjImpqXCubpQjN+X3S8EEuEdvKk7VNtAxozpt7Q4gYuAcv+QqKbBFb0zJBl/4DWIGBC
GiCkGWX5jKBp7LFmZ2fFokJ3UQfdh3/Wem+5CdoFL+rK8Ibx2KcsQkWhjIu+OK4TK0ho6RIP4HpP
Oz2GoBHhj2ky/n/r1urV0l/d+sH2h4Qbar12LtslhaH65u7o/Xul0Hsyi+M7qFG9gu3QGA7XA0Ce
zbr22xebEe7PuEyy9LDcxG9zMQhBIKzvFB9bDOo/oxBTo+9k8DB3dpSA5Uj4fDWppiucTbJMvo+n
lPEr91pm38s6rEZa0jqIiJ2iUd2cciBCNdqlawz4Y3w19+O72rsBMmE69iFgT2A1nZ8nr5TId9fc
XZf+CY/PNkOh0xydcfMTCUXZXrxhNFeo9/poPlYV+oAqs+R+8ruL9MWnoWtw7QC0wlTnPZDxOchX
nQI4mpxLcnaf4oNuk4dA1H+a5F1mAbQoUQj547wTKyufPBsMC9lt54EUMht5ZSHsLLei9AkYlFqJ
S25GeLrGZ3A5IZR3Vt5xe3TyK+pit2cArfJQ7qyjMW9gbXiuxt332q+GACELbPe6H3Mx6HpDTtF8
z089xtm2j5wfBRnyCb5YhxYsEqAiTnqTYpB8SFP1HX7CqHA1xfRgMldCGjWiEckKwqO3NaKHSalk
ISYdcLQek4E+0Vg3w48WQdYY0t4xNaywqJoKAfF7xhWU5DHhxw6CPdatOM7Gdnij6r61yj8kuCaB
l67GZoZWysxBw2HSMWZWKoPUXDTPRRN/33wuClP/L7KRVWalOWZfmXuPxw+A5uRQztLub6P/yaBm
EEOTrjV0b/kuo+IlWBKc4vQ5z4UqPvrt7pdMdhOZgbHbRVVei8sqhTYlSpwMu4y17o45B164YZxL
ZAsQgjDdiVdXi8kXqVeR31Mbif2Zrdef8o1kBQPP5XcNPLaJ2kXMOFVFBQ5A+Syz+u0zxTZ9xNXP
XWAHUeJruFItNg+2LPoylkc1m4rq+uztkuBlwVHb1sV2Fb7mJUWKSLytSCCSddNtHmQuZJFt4qNJ
sbYgopbGScSHBEPkih0/UheqBPZRLSW5qp9cKjL1nB7DooXcY5D4dFplivZ4sEv+cfwEcPS2Y3Xv
6fY3uSeBr3nncgNjN7ZMJXv0ODLNz+9duACAYEvEdLr0Yi9lCKeDHr751p0Yy0thaKpRGx391iyS
TzILiuYdKCsiAMbhs5oKjfdHTRDEt3VtN/CmsFXqSA05bxzgFtv/eGcrOeIB+Mbj2LFRhxn+dRrM
Dt1xG5EiUMlPpuBqX8NW2e9Yd9crJvcxquI7pxx05SJgxpwkyfEjFy2aNixCqdnel7niQrsJl7Kz
nNpBJsbXNLIL6VwgtZveh0d69rsXqv8mJ64/OmNR99SC9rITpZgcZapjFueb6t/hqf/a0sXdqsk6
ThK8HSCNibTd0hJJPlS8ZhFUDljMloekf/Fwrk4W2iExAoDUZZujQbHMaJHPDa3Pz4smFkzbfXOA
la5/aGjMdupBgTkt06B2g9DM2zJQ6JpLByPc98h8Z8/8IVmUvPFC21/oPff37asUO7CpitghmXEN
aM9WMD6IrszvYH8+pamNM4aSaCsYaGp2xeObdRKQ/c9X03xlJe9jw7TDz93qIuTS7oMlpYuVNKRv
OKwGDGHgYSpChOLa7vq2SKp7ikKLFAedKF6k1/isW3TgCGcYAfNmJy579T+u+1ILaTBnyZLkeTq7
sLRwmhe/KGJoLbnZLT3tKGVwiJU7MkGURYELq5w50AKM0zBPhTE7OValgDmoxxGjzE3kTcpkvtda
Yj/pzNYESRYX9BmT9BnN4q/0pAUKIIN5Quakwa2r9vRpJkz9bhPdg93cAXD2YHgvV20RRWCNB/w5
NsQOc97vbEohLfRZQgBWKzxtKtpmvKR4BokL05F7HCe5Aqc9KcvSzAxR0UqIvvBwbgRMKDUYl8ia
C5j1uW7Mt2Kty0fczpikOlctuUyFpBNXB14snduuZocLC0PbqZFJKB+bkNKycSqPg9p7e9RykVsh
3RqPOS5TsIe+sMcFcl2GsvV4PMfYqoJu60byAn33PLdu5xds3vR2OfjjOga2nldyuKy3YaU6Dj6+
2OayitymPlg0NwSwHCkjgc986OxDkwQ2zUNRtM6GUWT1/eUu2ESFWoFauy36SzY52iPSAqhWsCEh
GNnnT6vOGyzfj6waR8DfDdu9voCrauXz2xzM+BBzLR7zU3lZUX6CYLtPQfgUZCJqcqLG82BZcfw9
k3KUQhHjoUTn7LV/kdfi2mQUkcalXgieVfgOlmDPTRjiyiNLvcs14ym0QofEUR3zjrSQXk2tz8nw
Ac8ZPb6p7vxm+MLCUWsNiw9vYZgblC1A0lOW3ZgMyGTnV5jJzPSPM5C9PPz1fiCoHYDYxLRoLw5f
0kuU5XJI+UApShHZ2ZaB/glkZkbFD7u/7x/UZkLRhdKEIGpyMNal1Jl4EYBGFwhGxwtjnbkwLL7g
OW7yzPBF4XhCwPYfat4RMpno4fl2ztWzb1t9zvDzX7QIO31H7PNzUQkIvpaPWw7iRi5qZ7INTZ/N
4IvLG/j7v7hHHB1sXcwdT+C6ha0AzLlQgLDa+yUk0LXm5pRMIaXzPX9+qCmvvGfxWFXCzR3fRxKW
snetPu/E8b1FEXLYP6k9tlMC35yRhc2P/N96Njc68175Axjawkn1bT8FxPSbD0GyVb4xIkHtpkfG
jQxu/f0uA8ifzwXpo/bPxtho4mZd10Yb52a+d3nEm4S2RKoeSvRSuUJ+TCEIieMqFNCddz8BFo7J
DT6bYHWXCAOqmY9Dyy5yZxotqfPOQgxj8iykY24dkAGexDOJ0bAF9fPlEzt7D1KvJ0fgSAf3Kxpa
QwxcT86RRHGJrv/YyNHJrF4jX9NJEI2ARispsOWs+J+uzjphPBg70HGtKf/nrTee3QzJndwPfrmB
phB+O1cNtA/Xd9OnYWGp9kG6XOaDhL1TML0IiJlCUgfuiDqhzuJjDliQcRPAsI7IHxuQ6lPxpkKs
1fYQG68ubbbNlCVCg2rmdoENLhc01rCjfb4iY/iXwm0JgpIUvZnh5Hf9tTkRKQTBTcPUVqV/KYek
HOmHHjNCjADzYLZuWKkZZM3+pqf3nG5Wgk8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
