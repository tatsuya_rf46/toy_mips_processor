// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:43 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_11_1 -prefix
//               design_3_c_shift_ram_11_1_ design_3_c_shift_ram_10_0_sim_netlist.v
// Design      : design_3_c_shift_ram_10_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_11_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_11_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nzu2b6+2pwFmlGio+ZB7ip57VZx++Axe5c/M4tXUaHoMXfu0Ohu/62s2rwnnPIytcpVXOBslp8m+
qmnxZqj2/kr4JFOLdjsU3kmGw4y09Iw3NWvVZQvNURXhEBeu8b18tO3yhg4GxK2+7OE89vkbg8Qn
hcIWzjnRy1AJtD3WyNO+KjXhXaXQgrGE55QmPnHRK0t8PgoIYfRm1YpSRTjIKWn9EIkUZQD7/F1p
13uwGN+/APTI48AiIskWPu9DnTL7EbxmlOJMHUXb6CI9vDTGrieWHvu6j/orETsjkqvS3AHf9Ou3
FOsXBbDUikL5aKt+bIOg7KwwIscAfP7rRV0IJA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FPsMJuR7NYf6t/1KkSRnSMiyzx4Vy1ffb3vIh1+BfkPUR2EZc4r6K/q4wEkI9VeaDw4BGVDIFn5s
uzN/aGbo8LjOE2DYSfH9uN97dsuMA0CCt0SHZl3y4Pu/GCjWMF1u35wNySBfaOzOa5n6LuI2Bp2r
8IozQes9+g8hWhoD/xSKbcX1xUKbrVgIRDewAnWNC/RH6HUABKxv9nxO4oOA1ydNGRyrQElgn6Ci
krySMYkpqM9ettb1bQcIY3669AGl58tSy69xLpH+TVwRftsAY9cug/8/HmdMWuzpn2hTWoWGeoTz
q7PJOUrSf9bBprkQtRBsaNJLbx3pPmw/FAk3ng==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4928)
`pragma protect data_block
i8FD3v+QEEx8Lt1TYX8XtgMetdBMaoZhLLP1GQnQKkVuFaZ9zDH4wzg0SC3/FUoM6piwwsnYcyOb
8QKAcnZm6owel5SYu3MsmjfBKQGAnwrar62LMzpRUMhbAJADel36obpf9F/rJM/UCdVt44Gnzd9P
C6EVMs94dTcR1c4BGgfViQGIYnUKaqtR1LFfNPr3zzX096bDPgPydPR9/wCQvzbWEx337YenHFdt
8AD/xY+2LdL7Q9X81+sRXfj4ATlfBaw62wUysd/ufWD9kaA7GU0W0EngX06sE3IOG914ExN9169R
z3Nxs+Qwui+n50MvfOKmdnmkSll4z6vcQFZ6Aq6VGYwYvTcQd9Z2iK927OE52iPwRLs2ADl9GSQu
lB3AEsCuiGtT0Qe3O3zHiPK8YPci3p0CDnJowAxhb8CW60Z1e+fKZyv1tqhAg0oAvZSi8OFsHAEj
d5QAk6rQrau9cILC2cQsl9LSDuz0CZiyuQsxhQWcdgJd5NOMqtnq+qh76icjRz9n0JbYirQH7b23
E9c9d85X/BsLDDX5wtSh0u7H4DhiBnTNtzFHl4+UJ3lJOopBbc4fsLw8JnQLe7YvqVpOIOhZa9VC
dyDZXCDSrsBrNrO4pHw+Xa/8ggTt2iHeDcX8HXf8Ia+y9ZjFqjjxYU+lY0cJ+FWX8IUNTBST77tZ
eErq0dEqIOaQVpvnbt29/OSu5txilfkaHHnxuj22izQ8cxAcGdnnlAvOGipsUY8ouwD3OyEyj5kG
rdUzIXa+BXOyXjtSxdA1mRqI0jToMxugMxr23VCJcenwjY7jXR0iUnQh7ihiLKd1O+VJpFXm1SqD
sZIYpGKH67oEJ4qHhm5f+zWYHir0DtDMxArFyVzFF06lKg4QYOqEAch8wXyJnZGY0JUg+UrTfCCv
zMSiAHdZtSiNLTRG1Y4wA6y7ObjZ8ui5Ga6HAGiSApYQ5BdoWIGd5FlC3T8vn2p38UEjE2VW9mkI
ptN3CmU40dbkXgQx+edG0SIW8O9zpm72vVHOnnqqrEZ5xegQZ4kk09TJIcSbUDhZrIhTSJ8NP+Is
HwiIWJUd2j0QAFjm1BNnMOctkiKBdYxv/HqxIVSgkM8JluGp+F1mB1wb2Yu8ssRPq/SD/tzXFT6Y
BCQNJ08EWrxkZYyhBXxEm0Xly8j6dAGnfkt6MOfA7Y13EztfW5hPRlsUwJm7bLxxPqnGJB/RK+mD
RQqQn+ZsspwfSs3EEYf5f+PejUqn1xMvmEGuYB/hbZPqehEWGUlfpqy+HsoIQHdTK3XghM1jNoC5
RiSE33BNLg3fMMdAJYx1a1ebx+xCwLa8Q4Lk7hBvdQ/+wzBelPD6cqteGzDWrA4Ca4RPqrI5FUvp
eYvU5UAAnAfDYFXHbPMUt7lTLF7VGnimIEu44kAEr3yc8LVsQ3nZeXol+ojQd0cmSJmFjEyqizNx
ipP/sAkUVpK8EO7NiaH6oIrLvJ7xinR5+SxvDhOSwXCcc5ZFP5KcMJ86g2FAbpyhg9y90A1yczt/
N3nW3YDj1xPRoeUGvaRaN0wMkzk/xHyHFNgW1M2kkTA66oNyQVQWTJ2ivyxo0E1PHnEU9h/yY/r8
2/1ZGzd85eSgPtYNQiuc1+oj27StFlKrSh8ZqlcgqEtdDIYpw6bjtdcirfgAKdRNbkJUTVspVpwJ
T0PZAeyut/7nlKrBUr1af4be5t7apVd3ta8nQs6IwydE5XF73Kv0skipGtdmiDJPM3IArq9uYfMp
OFKEnbgszkYTpDchu3FB/qnOT2ES0k37JGuGnSJkYbFS1kYiBlOvhfzFG7a+m3jcqQr6ApZGx4Q7
4dJxIlJYYNCITfnkO+to4IMMtxOhuD4AR9UYwdgeUdDcB+A59sJXb5sslHH7/PgOQd+Q6oVKUXLe
QSd0Yt+hs7sH3XYxHnjN3BHNOaP38fIaIKDSTHsZAaN3mYest058cMgYFTvSaFdJLy769KXh1wpW
Bqow8QmGHC023vaLvtGgkDoMjqNszV4yaSp5NZFMBzJspIH1+r0tn+qDY2gkDZNzAeT3XmChWebk
Atx0DcLh7PZS91k3i2mpn7Eq6yz9ym13yvgh2gPjEjrM09E1NZWdx9lV7CtDmFa/pGJt+K0ZuUfT
/CtC6cPamFcMwRibFj66fe9DMKj2uK9YuFjzB6mDqDoHpRUvzMC5EsHb/LXhvrUwneoqoavqvWVZ
VMZAhl6htLGxXcqxs++3MbbfN/8tnQAudBqIpVljkwGilpuRESsMRxXy/pQArtUnQdzYyzqHOZHY
HbCIfG4TB66A7m+RDUAVkWa+1uEHHS9U+ERHcLX2UZxzRJRfnK++HUg80NXaFbWPUWA44kquLR/S
p6YiPuKcumzKL6cu/28oAymr3xLcDwwRVM7kN2brvhs5cPESgQ6U2k/bJY7dy3fCB9E3jjw5rAR6
vwNCZAzLPQjVQOfuPbAmQsB3bHNvWkCUj6nH1rFoGsNt4+yZlciMA+3DKDQpsZtTTCzD2G5hO+xG
SM6QKyq8Qw9ZHthJ5zMVWOwtS2DUvkF7DzaBq++HuqPa8ge///4KtnDml+33NNIWoED78wZGorNJ
E+BXwBkU4WlVI8HJlBT043rY9s5794DC2v/UFbbGvItIph/uyp/xORnFmXC5eoUAwZnTNPKXQv4R
/aFMVZ0YwmYiu7SD/Aa2YoNRKNAxG5zZvRwP4UiMeVghWlVGGTFWraX4f6ir/Pw8ksEtwcWLtFqf
JlYVDAwDHctUt9XME1aSXQ/P12+aieXyrlvY3iUsCcsLwzmokjfdLLvIzynO7R8RPISP+lEOKtTW
Pkt0sR9YJDz+Stu88vwAVQWuoHiD+5EAthcHFcH0ugHgRdDamev7NM+NXOQma0tQ74deD8/ukVtZ
RD2aEwvbzL0sx34ZvNdcLXgcLGFkoj3Y4ORmPPBy2a6eolXgmgwXkbKDxiVLBEYNGWbMcvsqUec5
muBoGpd55fUUVe8c+YwpkQB2wndgY8+QjBD/tVTnAFq2fE7nwc/mw0YOT088iZvUz3ciO6iP1X4F
PYDelC8g5MZy/VUBwikgZzbg2IKZQoLRuJ+5ytPGkKbHIuco4bhlWto3lIE5FYaA0MFePQ6nu/vh
xoaTpuSTPYedk5qcRVj9HMvWEUpM4l2kpBMUApTcl3hg49KDhdYyuhBE01/kgiQ+hk6RMsTgpWIV
hwJ6H7sfLOnAEBDJVBRwjkRKbboRcg3qlJl6umZv9l8G8bPK27UZJlDgrcJZICDCBzhkNfctyaa7
VE1fh+qo1xdrJExS4InvRkoERFVzTQwt0EHlAQ2yrQ5fus7htJ3QyNtc5007w5zFNy3b8js2Uq3t
Fy8OM/KLo2ta7mYM97FfQj3UDYKCCF9ZvLM31CCurtE7YGBVmmR7NZwyh0VEfQ+xxjAjV4naVP15
mlHW36mpxAcR83a8uZ1JjXfwQELO40RXUe5/43iBqI5OjxqO5c0ls8urF3CqnXaEiC74oYNvJmhb
sAfZc8VkFObbqEASGzvhS2BJ8RR38JHrR9Mq9mqdp6rwyq47EhfZoj6bd/uddjU91+sw8OlBwaJr
0PyjlWnP8oO+dOrnhPxbT/Qs0F9VF+RmfsWDoQQLeHKMHIWeK7plcMfjCVAZwwGbrKBnY0eAP6JR
5Q8+W88ExY4U5tHCU0hH8msAJLMrizHLvL5NNGAOAVClZO4dyAVjQQqVhw3nVw9oP4eF+HH877xc
K+CRF7iUnPMqpFwK6biMgP/lC+KoibCW2Bmg3VvP5EiylgP916QIW66DCI7EN2uwGq2zuaUyB1kw
8UxuJ4PzLGY36vN5FgtlBbdFfjtS1lHSnZsOTbJQIalhsMRDTagZGvnzV+YtvVC2VIfxMQ0eFXTH
M0FJfdn5rvK9jo3Sm6k47WlQtJgHIx5O49VF4wFwewFc23lRkfWN9VU1/czyuzEd5Q7PXkxmxhks
+PhwZr6U9UMje05Aeyxr9PZ9AghG7vOdIr0RXmmZuHAJ8Z/Pq8T4hhlLAe3UJyH2ErHsjiTTNxOD
XMQv0OHPjTqXZDOfZwd7LbgOQhYJY8HpIaeogP58PS0vNnI+LwGDkbA0nNAmYRc/5Dl/MItt5XyJ
HLkAnA3rq6d7mrlsbeyaqh4MOX00lgKpBcRDNpmBALfK2t2CGdz6+jRCkFrLZ3RhIThj/Jc2I7rb
F6i8HXYTOcY7rm804ahLdZ9NhVgoWMEj6Ntp/0xTqP1odF+bEk5A7TboUqLmfEe0l8Igzh3KqHZc
s5UCWBtfUe64/PLER+kr19eH6ZhhG4PhFkn4wFNtNkaoBPNmbOWXviIhlzSuUvOGmSWs7wWsW3/C
BJT1ynn1R5VVbqbm2l0Z/PoPIXlVK/85WgaEbVolp6dRERKK8VXqXcb94JCYVlpMYRexCpdyqmWM
c9XzOc2qANafoxS/QG0yUBtTrvACwgEjv6j9J4G3Faxoys5nKjuxADw1mNV25bkZQsbBd7kl9Lmy
Ka4l+2OSSW88BHZ0kiduzQ9DzTlGvNNLFIZhYAeRfDly4/S6xwB2mKTFk6ZUFAkokb34ySN2TBq7
24D63UdS9p6wVQvk4eGIEcMZuv9fT97e0rXg3yetsTwchvrto4AJyV8LdH1k6VTUH2D+f7BnyTLb
JA8W3ZfQnXKcFVSkm9j/FaYNeWuH+fLjdSqsCbOdWcXmhsvKSHLvprdDdwwr2nL5TXGBRwkDGrZn
f5V/Nbmahnm8pyhTfXOfa6h+Pa2li/sonuY/DU5zgRt2hbqTT2Bx+3/0vTG0yMMj0eHq0JwW/iyF
SNTsigMdwAW/30Jvvbt9k8vDbr/4p3EiOJvKwGywgPcxfMk1W2CA/iXWpKX/mm/aVhfYNu/Zel5a
Hm4jaefSN0eMFgTnc/ZAlZz8dBnUNtg8vteRluUZcZcYk7qX/oL0v55OdIR4elR/4OnEwx/wLEh/
Hgk7Zy5s4S6TOvNddiQuQIoGUMBQtVIO37UoNczP3q5S9A4trO3bv7bO5cSU7SPtZcZllE/LWMQS
1HVFcxuZ/FEXqYf9d2F+cuUJDW1hgiFVcCR2cK8uFCeagVpjrHuvoH8/dTUXYjdFCj66zUrEU2yA
hjCAH+wfeqhA/Yank1bTnr+ce7Srq5WMzpVX1srHlikvR9gGOkbqcYia0NFSsec1azt6Xj2ENYiF
k/+qmUS+uj305l3M02NytqaKeJDOZKPNrCOablXxPRrsZgxoc+PjNm4OQl7D2wSu/KwLxjdWpZpL
BTGAGo3rdF2N5vXnfCY/ujhdENBdt71rUXVHyC/yQyRdF5tzbyunpWutQIhZaGFoETKYfsCDrBU7
ugRQ6xYKQhKTZ/ulSV5tuHRY1723+yJ/aaZ599FmFqLoLyXrEJb+U3sENp0mB8Ba7z9V3WT4RAgp
El+uy6PO0pN3NoG+2tS1oL7jvQShixZrVEqm5aKv7y2fg2xDTai5XJxfEXxdyt6YTvrg7GmGVjiW
t50DbFLNPCC2/YxYWeQC/IdaikkjpeZekioZmPtrcJrMUboEAwzKBWAFkJ5qVGF0dEwTacfd0Khb
teuN1xZxEF7w7shatLkf9GReZjOmWdVYbNyi6R62/gbuM8zUMatIn9clX7FPmshmnIjPnjTIitRo
RMjmkjbjmvwL5zKv5JQfY7TO12wk/MEMs1QEH7aQFvV2LfZkg0IwM9MObwdhF2+4ImKIE3vYJ7RT
0Iajrs02glNOKuDiMvNdFzLSrjYKfBnA0j76bwjZdR2sJcvP8sjEVfcYn9qJapNNxruAf2WlTyDu
WMSfTKc5PTL7pWSrdSki3UUwc1EjOPnMEiTyZNM3ONWB1963SWhF0chdUfQtp+dEtuOua3aqbh52
4MiyXnfmhxtB2fnjtuvWPDAbpDKF0JQR7r8sMV8NvFoPdki6cn13iCMcktPgYfSvM6he5FpjkguH
VXD8rK3jINDfD0HpqrZU9qh5Bm6icGn81VgAfbul8rRign56eHIp8ZBrQJkvhNU8oaKOQS95YsVR
IHUeZe8l/IdZkVg3IWFeyR0kqSRcZMrvyU1ZNRwaMEbDhjjX1jDi5200ysNevlH+7gbzWM3DBzGb
7qarTdOdXeAfkdwPYJ4cGK+mEOrJ/jouRmYEJTEDSxYJETWCbpKvfvHnbNUUyYjwciDTqWd9mOdf
zfn36K5ZL846jI8Sr4vKAXS1nI6JnlYFXvwV83u6WGQGXcbSzEnl3U/bQyR2jQQJgLX6zXC0XjU9
j2xyr8iiP1oau6TswcjECb4uFE7KS0rPZ9drFwRVBv52cCXO/JseXHjBPjXtQn43Km7W9NlPDMjm
+1FhFUBo4fwR8VztxDfT/nDE3V8OtcHIVe0ZMDge+ePDgZRwjJ7GSrUQ6X2qEAeIX1I5up3V3KU7
uS37cmpIpHvhMh0wweePyjmeYzinI9rxXBeKhgX4NlSe0sMS9Kg7nYxXI4SMl4Q8OsnEFEcwqJdE
Z8nhXNX6j3UIcjDkOQptuXGiVseLnJIHuSk/41QQueUwUKMvm+elWL+kJKp7nGOAzwIfGvDW4mg6
Jlb7ivckKibq2U+CHINxERHa1CFhAbw0ZeE=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
