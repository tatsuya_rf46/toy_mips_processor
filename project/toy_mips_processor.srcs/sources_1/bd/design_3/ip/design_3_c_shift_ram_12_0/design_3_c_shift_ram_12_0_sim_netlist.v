// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:59:52 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_12_0/design_3_c_shift_ram_12_0_sim_netlist.v
// Design      : design_3_c_shift_ram_12_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_12_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
W8BLJLwEU7CkQNca+ZFsHhhZo8bhPhpPoxaezwu0M4LlUX3MOCojuvLYRVdoHhl5giTBlubTZt7q
WeqCwHFVfQ4vser/SLAk0BkDfTTpLGDNwOiMBLOAFsVijsUYxyH2o1zE0hqNuigAeOL/nL3lhOWF
++fK6sBoodAraqicOHZYWTCBD8/DHyPKLI3I+dM7NezN0d0MiZ/HuK+lGmOtUNDpixDBhyHHLuAL
3Gj6NGEasQMAmdfWfw4shVXW61ZWwjZHHfqr3y6lozynPv5bU0tOTv5bH11IWDpVfIXlPqA26bsp
wbmyaQmGdqVviooJUw0bsWSaUfoTbGav5D6ZfQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
k1DQdgIuyZvQQLURJE6JcxiKHY2TavGE3fkAXdRJDilusv4JvskaK+JIcQhj/W1lVcdDHkowMLA9
brguMly8sc+pT8fknNuapwDUrXEX8FYM9+DwZBXTReU4R4NjbLMjShxKFmVyj6DTQ5S6lBlbLWE/
RkvhDVqSi1nebW67j10svs1Ui8da/ZZNx9KYNSuPro0kCEexgMq/3gBIXZguqZDSqlYEjvrjLnw5
psjt+nbfluZzPZZTsBwRMt0zefWgKn9q/nXAL9ChDz7p8Yz+RC18XqQ1F1T5tsLAGDjOXkPZNpLQ
zziMz+82KazWGwJJcTk+n/VE4+NLzJmITESHKg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4992)
`pragma protect data_block
ISzuC3MLRwxEK7bZgQXKP6JU92Uqn1D6BzGyp7x/9ms/CvWLCQMx6jtPtbKHhp3dfqhO6O4H0Otc
rzAQTEQDbYLY4UbcQQlG/gglX9QxlC42bURXdVTzbTDGMwXGOxFquQhc/+VgelWcTV4WkZLqW5a5
gV8dKkaeaA47sf+gWCNrI1NM52PmAt9Iw6DTtSIzyuuFYpSAQDdNtC6Y/g56oPzM34ooAJbh+5Dm
xrRprv8MUtOzFLu7R3V6QTVzhQy76a8KxnksgwQkhhnNA8I7NATY0hBVupOzqmzs42kLJae86Eb9
hr10PwE53+agevV9skprU2wsWjFvU00PrJn0bh68T8J0sHcjJTywDRmQzf7Fh6eO8nxlWmI4pWBk
OmI/K8K+ngAN9R8iCT8wVd6FFZPefNRMDgBrdy8SMYsx0FPeArU9MvLB0AtgcbZ+ngKGiIsXv7Iq
MxO9Tri3ZP9OxSBLd1afp+hOvbsDnsNKeH9n5kuSfOofDVZH3v8WOKzB+jITMghVNAP6U1sFTwI7
2pDJfTd2cFa/LLOwBYowq4zkvEzaTMR/pTlpXjrOafe4iUaNwPU2BdbUKZbycRQQogUhB/QrayvH
aYKm/4IPaJbd9FiOhAdqkEe0jlYMCjFOF0KMaP4K5jLHqVIRAHY5iIcs9Skc4ieSvpgeG9Ek4KxA
H98SweLE5ZzGb47ncttM5QR/Kt4M33t3dh6b3KXtYV5KVdjJUO98n+A/0duW/1m4aA0uQipEbIHP
ulS4aa55PGPYYDvbh8X33incYEKG4VmJvteaMGNxgdFms1adb3nnLyScaud0IKmCF3aFoMIFA0ox
EnkosmgjIfDcnLw5tUVkMliQzZi4802yxs4hqOOwSZqgi3Sg7JPXG8DPrsc8/PcILGAePyL8HJad
p2MXDJG5VqKA57DvVbWhFzXgSo7PFsxiIFSyanDkIRjCObwsOnoP+F7m121tRnxQAV1woTgv9xqU
NUf2XsO8jWiOgcqjbEk4MwDpC3H939TwjRbJsrr+9nVYYzh4Rf9Z95qyx8gjlR15ZPgyE7Ef4VrZ
DuykVuii9RUzHo3wCd86E6DMCM/S+4j1k7FccBG2wlN9HMLJRHU6UwwP0J/rcCrCzYLkcUL+QUzZ
DVGZMgSIez0lxO3UxQqrNVuR9r+e+GMwzLCP0FpeYlkSju1qDPhFYgXgalYKN+90iHAIlOVbgVoU
SY51pZ8rzHlLrZR6UY5/Kle+ahEfNCuo6LOawucFIj9OHw3jRNb5ipFxIi+tai5b8Q7bY/RT+jFe
XzIRC7Yzy7Xc2mqJebrkVP0A3YgGRC3T2Bo7wWmAwjLj0s+SWmfQcLn1TNrBHIhuCtvpNS4YKFrd
YBR7+w4354ub7kw7X2g0M8w+5CDRQJ0Uu6hwT/Fx1KJHIwwQkvR9UUZzpCt8LxRNNhm/TNMXEp9Q
oo0XO5Eyvati+bad0Az+Nm6z+jWlOACSHwx0SrQ64cM0Xnfvdhi8MhLWCrENYj2EqSkcfiEk6+vE
ybdOaEAo8sUJLY3539QT2Rfmv85MKbTObn2ihzfVXt22F64vTjW1Ml1E+3zU7FW/P9JUG4dgwpvC
i/ImHUHAcueOZxdayQhGzZE0lgzX+SPfpE7er0bc1iUqUXz3hYTGbu8xZVxTnuBlfaOO6MRaUPfp
7Q6zaOicdVUVjE5f08bpRdAoM6dr8W+F8PqDOKec9U3KGzK5V4bF3ZH3XTSGDLsrSK1D0haSAgQq
hy1GLLhxCjCc15KwgmOD21cGuBbcK21JLDRZXHlWedm8qahH0fxlCVADTmg1n96rQsefkrLqUKB8
FqBS46DcGZy+7gqbn00cnYdS6lFDMXnXmJuEMJ9z4LOKQTAbGbdUUtXImhwIa2w4JccmRLEpjg3b
rnY/MRO6/ENcsMUhP+WNeF8ULQiPXWDeZcKpgTaqz9iZWyZKBMjw2vQpyYpAV2kk+3S4qAUFUWP2
NazZSb45ughL/yXq2VR6Maz7Qo9IfsufXdl7W4B6SCOFZbIw+QjQZPnwM6QYn1dm35qFEm1IaQhe
SJEI+zCns8DdMSi3sGvVjew3B4iRyio/2hJ8yLHKFmhodWAURCnJdB6G0rXikB0SdjDNh5LZ8MtZ
oZkNWFv5Ru0fPIpU8xKHwF2rhvSnH88KmCnNYxXAM/ctHlEin5XQyYGlgtNyrryWwQWOmxjpqq/b
m91XK9ZS9KqJPUTBFsqQHvYpxE4XJhI6+NlMM9DLnmiUXQfVcHy4lbGeixh/qpC0miuEen1jQvVt
TTmu15SEF/Sd+q1oGW8NgwRM9N4WFOF0GKYGaRrzHwFzzaQPetQjFwB59wDsiDY6yJPRdZt824Fa
YfZaGBWa+6ftOCPU57w4p1+FBRK2xMZek3VXLazHDtH4RCdkRPBsLpLWzmlMIE2jM+HQvz66pznF
G7Mn/0Qb3NBfSV++6iEiLbczL+iwPCl9BHd3OPF92oWWVKeGJQS/8yFu8pbunfb3gfq1MjDuf3EV
5h0N2WjuxW2JpGjkquS0ZL823FiWKk+o76eGRQyD6G093YMdImThwUJK+7ITf4OcLz4NLG7keBs0
kyPpUu2q/qHH7YroLBODhXm5Rwe+QMV7kPcAoURxmASxMsRqcXt0i79hedKZpk9jCjFyuwcOY0nI
cOeJytvdFRx4etTP+phsp2Vhgub3oR+Z0A0xmXXCukyLb5Yx60Fo9XdeQeBobTJADcjrV0HB4w2W
TPEJjeUFKM2GRY9mDkR67oVaeEH5f4RgdOTcclGYeYnbNUAie+L0tDYV4xEtzIC7uUMUePgntvUR
OHpCOO6XwEYUUz/zm5YdIGqXbaxNrsJwAE9V3t+Ohx/8htFqfCkwrY5ni1r8IN9iKDHXFcf8EIvl
xoXiRqj6/roKLaLFF+JSb6o1ZSbSBaCNgJbXGQ71rOrdIACAr3APRfEoKPQSiVZw0E4vJUzauWjy
Ok/8BQFg2lsDvouObrefQIlEzZ54KLlOxjHPuQvjnnJUso3L+rhH+TKuGDb2WwF5zgteiRCADKEX
IrTuEeuvAMsEZNqu0RWf/1xDNC+ra6JoIOinbGhvegHBE5uouu60K8MXwFPM9btgh3atNkT3tujS
LF5IpJvhZ/zKWJqYrs3benxNCIK0cvN48WuVACk+wMwLgxlSTy/IXUppqrEcUnuu/Ue6SqZBUL7x
5igTHXuoWaezSoG/HC4rZtCoLgBeM9aChKyvWkZMywxNLi4FahUT47CJ6Iz7G1VuNxj5SnDsocJu
FWnMx9vrHfP1H/MetK3oaPl/IRRcH/Mos/GksNzNbikUw5ci/ECuJNL0HBNJ7MI4PYGMO+2UhTNi
ucrEFmxJB9DzLjCu06GLtMksIr7JBVPQiFHzZ7KnuwVQjYmYOIzktQK9WRLJ/liKjdjz8tY5Ryh+
eeDZejbaIFpw/7lOJVVs63AVcZeGXNgVYqRZM8iTwvNVZTxZsGcGtJcYCNth/da6EccT/Pa1IiIx
eu6ksuByCP5YwikSgrTQHtaCcG8DziiafzjguIWBwu2RKim9DFOV7DnF/34DTMtQ+SvbTiduRC9N
+pdHRsHPpddgQ329LsnE1DEpPB++Oi3oByW6M4DTQOc0OJr20wIo++vUstm4UbHFVOkvbnz8qJSa
4ffVsCMu35s/zHEmFBNvDZ9YLt5exwqmtpgUrVyn1boXvBeeyXksNf2qhhAlyB9Q6kzfZ6q7tkCp
GAOKrZ9bdiAlt4Q2RbCiABvDyrumihvGcVG+cxCTxT9ufgYGcuHsbWE3qyqmK4StcnRqtPBKIHoJ
WaxZmKMmdbR+YwPWBHZoQKKseFpSePkq+5PYmOOsItFfxXJtrfH2l3fnWKGq8Du94gP6TH9j6zJY
BPLwPlZy9STA7sJjE8WyaSflA7KGPWEt70WMkNWcX1aD+CnzXPjSy2zdhjJtvO9lB87RkfNQT4WV
2ZTAEgSnjXV+SMX77gvyktqHMxInJuYAvh8oAHKqE4M0k7AIhssjHUKoS0zadWJ2CCHhdtd7n4eB
K8TnYRQzV/cZ6daAdjcKS3YYXuB4YV7lIpxUruSMwTi5ehZdaeUnY93+q4N1PweZ0Y+7reDe1rcg
5xqbBpbtSgOAuAt9xnb9NGf/TM2zkuFcuXr38LxTUGgVIuMQCEhQodHyvpBCLr02NBiel8BfI2sb
JombraMdamGUnZXojmDuuu+QGCOxFUrvtmqP3Cw3ffXRFRHVJV3WEAEhwTbVpCZhgnrSkRMl4Q82
i/utxRz4snnhj9xT9s0/UCRhtEmRvyQyMJqBneaANOy2rIEXhT9b//TyolACUgI5dnLU++7YNrOJ
r45bJu19rOdoysW5hqgeNyakFsEitxr6NYoeUxAYStAA64aqQxBmGr5mRxsacLtXgYTEm+iniZxX
ZQ15lPG+s3EVlbqE61zeBDL0X09CLNUw+msSLZH41NNo9tvEfc0pWAuGrGS6QdyLgPMMFKxp2EOZ
npmCGR1rObR4brua0kFkEDjs/i84UG0ZMljywpbtI0Qx/jAzUu0cnsd0F77buf+J/CMXc/wc1Ksy
cBLo3Zx3ycpc8BjWDSL/2BbNqkou6wrRKc7GMSJDiVEUekjHLvHF/jUz9Ge+lVXVbK+GJaATRjk7
FrQOA6dz5gGd/53Sf3AdmcAO4rATt00X/AbDGApYdqEEbjl2NvjmeESOdUiaSDiiHVxti7AmH0QH
7gJgWrkGXVp5gCQiGTwqKDQUlBwYEcRoD++RvgRhYrMc/o9tcPQz3htnd7EGx1LuGBluBkBQQB24
pY2zw9Md946aCi5bZX6dbGWe2c2YlKETByhgv6OHQAXZDQ/hX0jTpvgYQ9PfizTyDZ11x24KKLDz
heCRx1GvdR/YV8yqdFtfyHPVAl8h6dc7VHIbdUOw6JDHg5Uwc/mRTJh+sxqYtMU4HqX4sdgE2YhS
WMYJDm09/RartHA/3RD6WGwBCI78AKJ7oqf7y9lJkyjtsIgk7GqC2bU4EXVI2tEsAC2UwJ6XabmD
Qd3YxLf85ANVa/w7KbNe7AXTsnR3gGey0XuwQD0rV59uxnq+4Lwx6bm0QtG0oPrATeVOmTNFFwIu
XbYCj2NK3JRFggfYcTRtmjFflA8HKz8C+JB6y8osT8AY9DM+nSgs7hGzV8BDtJSb1yR7MFOIFuGG
901buZoI+XR8EGQt4c+Guupal8P3LA/LSuOTgasc2AoP4N7xjEprDhI7cnt0tCUfkyoRe89Kj41N
gE0tiGTNLxzwvkP+c8b4UcEqwYfRN48shrBYWzzRXZO1A5wkVd9MDFFXi46GZp2n9UY42pqzeQWO
Tk+F/irj9wP9qgoAUx6iT604YgrdXExWNrtiimTGR5afIL+WsqLLpZvZCoOYJMeZ7hUeNGhweqq0
XlWiTOXBgieA2kPaR5tCt3Z1j/gVDP7mJk6mAVQcGKJ/WQeIg8//yvS+KlgMgIpc8PYaQgv309ER
qC5P4+ISnH1vLJ4BwOZtHDR4nyCnZbB57AEwST+s+mL2xPqVAPQtnvgyK9/WrhoNYbiN0WCINGlU
kC8UGVot06levOPtaw0JSt4FMNHuBHuUw/cYqXJID3wbXgV8XN1SAlY0zkg81h4tc7SU5VzsG4QV
1zs9LgsbAcji2vC2OGqJrGcAw0Rl95LUVoO24JTHuy3PirnxRQ7EoZTZ+Q1MWKZ2yK3oP10lejDq
6iD048o+uDLVo59j0Lp/aM/WlrrJVdyab8SbpJEJVKjNeLbvjHqG0562LTDjsbah3dTprLZE0CE1
mw78zItdarr2OlqtOnGeyAFCismYZhlG38rxMutrmBtwc1MMRjhC4sbOMgAY7oentoSZcUFDql7J
BQb2vu5ujcm4XvvqKdGjKb2PkvkipdSZlKm7ZBYpvuajCjWxx4mljEGqA996SiXadaPFTB8Hj6e/
IWuEpio2LzkjmpTHmuV/QlvW8jAWVuhiprRvcAQKYd8yDNMaN/qFvU0gqVzfwk7uA+wHGFV7scCS
+Oa2w5QdnC88HsWGXPsrKTdg6aGZmoHA7Xki3pIc00jEMnojmDxtHYAMW/KZNU42Y4STjLry3A3d
8PWTL2qAYOpVYzu16WVb5fdcR2G9s2AiK8SAT1PxiwMgWoYoe72tjI9NOMliCFrwVxOJz6tj5uaS
ceo5Xc5WoMmftu4yASY5grvLL57TBY5Z705kmrB84kZ+L3uOCh2Uxthk+POSJ5bTKIc+/LadSYsO
jxrB4h+VGZ0r3Q9HkAa8yNu2+KMxThd3eVXvzAQA9x0X+1UxqEPKtbdVnHVHdjnWY/wxrk2Jp2FD
A2F/BYI6s0selqJuHOIpZ3hZWbbtq1UWYoV6AchhtWFNkfGq46jUu6tgBNbOAHG5kusN+L+XrBcP
oiGaBauHctjzhmcX4Ffl7G2qxTjCK/PAoXPY0mgGfqQSf68FmFaJ11GF4lnc6Npdl/gGx+2n+wml
+sNF4MRw4nN5f1R9tPoWpivV4o4BEX2EcxTtN+fyTnMZ3Nob+uuolPmLMg7DdudSy0FWsTrhbOpE
S7BhH5f79+awAoTFNY8tx7C8uv5f/RYuQ8/Li5wB6YUCcsade9+j21NOlbUsYI+pJf0y59l+Ru12
dAU7Kg2PPQvSf4/B7QR5+Gq0mde4dFTcXIuTkuG8wuJd
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
