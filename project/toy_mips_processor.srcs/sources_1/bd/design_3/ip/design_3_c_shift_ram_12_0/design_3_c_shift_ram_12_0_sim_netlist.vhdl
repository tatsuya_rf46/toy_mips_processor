-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:59:52 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_12_0/design_3_c_shift_ram_12_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_12_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
ocCI0jHJfbgBjPKC7TEWIkiiPEnNr3gUYaxibklJ6qi/dPr9anB8vqyEz/VDEzmHVXHTGSDH8ca9
OChikD3YPgQnojX7XJfNptnoMuf+OcsO/4FzT8i1/RiZ6+2M8AylYprWaRxFExzAwDxwszwTRN2L
cktXteVf3++X4ZUw8SKacR6+KTGQqB1Bvn+cvAg/F7OQpGQhx/dzPDz2Rr1Waa3R8mWcipFvTyzp
7Fl3NDTGb/o9f46jCGdW+brKD+QMrNLaazrOdfm0GQNU+wDqY1zh0sbmxDHJKD2+IXR0PSmbHp4i
ovYk0R/QM+Em3RnP6zaEEiWfwZOVpHeNKUTWqA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
kjIfBqwy+BA7MXOlfTBVkQP/hI61y+z41xedTnWui79ivr+kkvU/LufAq2PGjHEpaDQJbM3aYBpV
ecRci8rfSSXl58sKDuoAPOMS1Brh+jIpVAa6YXcyUUPkqL11s1naRgJmV3O1CPduNHUXda5JpVgN
xuBSZVQ0Gd9Zaa9fypKTY6nQhUI7kMlmf19ryjaBf5kBd3jTR1+t5sDM3AE6BshXAHSYmMU3CNp3
NEM5dHrt6qYyQgTqdRtvyQLnVjEyY+jk+R8Vzsf40oQOUhzGBILNkFTkQtLb1vNM5ixxz/anFGXL
Go3CVuvFwv1PGb4glCodn+e9ji844om3IbOQ4Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 14320)
`protect data_block
g1zllBi+ibBXL+LmQ6zFYeCENJZPwNBoCfBxOSLyU1SGUZrE8lLzhUR9V9q9U+yXV6Jw4OFIeUlX
wTrHslU8Xh1RGvW1HHo80iuiO7FwSn5A5mqqiduKq0TL/rWOCGT7vd7a5PThT5zsxIO/5/ZXVvPZ
JCSo0VYXMSH53sMBf375W+5EA1ccWv+nHsXJI0jevU0RSU8oEHoqy8rDey4TZlkKvapBBtrFB7aV
N06RLm5aACkVvmTzJ2Vi/Dk4J6/YcEAu3rMQ1e++ybNhKCNcWI/ZGUqi5GM1ix0zgZGzlQXZdqlI
lCSXWahOSyHCdzAiosQaJspXs027xbZQxnGL7as+531xDx2y0WbQ9Cf8F4rFRKboH83rnCMRTHP7
DihPH5xB21NWPttSMOt9KNAylUD0EVu45Red90/u+7xoI7z5i0HJwCNq7xvIQIl3a+mgc8uD4t64
Ui1kzv7XFgEe0506Jhlc3UF+EHQbbcuF8sRLlwh/14ANfUfgYYLwRTB3IQY9PBfDgWBTcy8uJotq
2A6d8bwkfFIVyk2ZOUX7iGYSYVOWsBvUhGNpnopxhuiL6JIfeOKKcVdayUfPETmkSSefb60FJzsO
N0QsaX5YCRcWGj83bthoBK7IkM1/dBZVXbNY+8rOQDg9gVG6e+/O/GxsciiVaQ/dOSY3O3sfJr5V
bI+f4X+iyWtNhr6XSEvzZ+/U8HXV6aZgh7qrX1P1Gm1S5KDU3XM5WgeQrjwldci1z2lhOYTWiy6o
Jbn3O32vsNWyaiPIWiktRasXo8FyK/dW1pkzLOmaRKiGDA7RYDvT5aLqqOR23XHTKL/1GZdeYgn4
slRCbCyXsip4gs+NmUOK8Lm+X5c9X9f+dMF8Z8rUK0qPrZvD4Hxwiwj0H8rvHGmS5szZ210cyIs6
VxVo/zHVqzY1srAO78MiwAWia35oRKOLil81/lgYE9k3HfZpGqZKHqmVGhdA7DwPV5R81M5jBmxM
Auc175K5AE1IgLDx5k37FiabrWS9crujarMsIMy62UnTdJ6nOLgsA3WuETaf8yZA82JcnkJrJEv9
mQ9DHfWyf4nxz0MpCMfwUzQtJxo5B4hfytyyFYvEtMDj9BodPiB6WbWEBsephqr7zWwjHPiqvhlT
LQ0tocpryeN9i2eYA1nw3tYYk8J0Ij2nXxjTIj8JzxBO9THCI/7GcJCz8Gq8dmr/PJdeL1nlpGBp
Jp9aY0heKQhgLzLfb10gXgAoKw0tATvD66Yr6IJ5FhhMI8soHLEZyfSdue0oYnLEjZTeACQgKacQ
iMP7hq8rWox+7qBfGkFnr/mMSclnhQbxmmEP8m7bb5TmJrqqU80WiEs5ySc17nlBbEn6OguoMqb8
KHvcL3nJ3FwgfgCbN51psJbdstVpDMgkhR781DdYf4LeXU62Fi6cMpB9iSGzVJ0skmRgF7MI1jVJ
I/EJ1yoXVvIkdy81Gys6kk3VDGEtZBBlxvraZ5ISMkJ6Dcd1lMwnIuj16k12EX/+tqjcphhWfMrx
vGAB5bC2KQpzsB+yKZcpic+WzBY4TJRgtfU9KlE7opgPmp7avcQi0/WUtEswPUUw6S8D2SIYtkc5
8NGvQ/o+0qididc5chnnKU/IwDGd6nhNqHX3iRDZTZwOEJutl8/VPUOFxuF0xnhn2fMnAGK7DCy+
StvQTQnQMB7rPSIAY6Zdo2VVh+kfDZhwA286lRMnELolSdv/NMof9dbcMeUYVTWn/Vjts83bNeY+
xbrjN5Cde6UIANfboT3tQLaaYGLgIvgAQu55jtHpD5dzevlbNNAUka4zC1Kk6LhacgG7jfRK86dl
GRsGcwHIvsfNAcCAfw1KYDmCbtIW3cYFf5a0tIzfy7CWL7DWapVYR9gpGOwAOV/Q58bPPEyg3skd
ArDUfkjzIWGVK0Jkry7bmN2K1X3sjtpMRJzg2wK1KOWzBwiLULVbqs/Iw7EPlna+eBlyNlqxmIkf
XmxeXnAXn1vEPvk/ScAaM9QLT54/5PA5kd0t2BANU3yuFYBMjK/k41CVJm/iXXlOwLSp5MZN3fmD
jL52bVJCEPyLx3KAXDJzftT2kGEPnmeYtFylbyAneiTeKLglNxD2oHpsHGklIt5Uacvjic+6l1t2
5S4ZYDYAJu9d3wUMPmXlZpcDWqpz9TLaL1Li7Ygp9WsiWJzsl44pHXXFLN15WP4K4RTHp+Q8y2Q1
OyospIgcG1VjdQQqv6prNig1sRfcsf6gB8XRlaZ+6Ilz6sw7nowBlokuv9byNbtdNVHirV975vZX
GRVlXhI81ftzXbCCn1PmlH3lyUaXOItro7+9W04LbrQh7JXUKae3ISN4GWeb61est0wp/7WQQhvG
deZBom2fl5bdpQaNG439nbVltdzbJfS1I/lawRDDr6Mm+IJz393a7EFcg2dshbcNj1p/N7BZc43S
sfx4E61TOI2Q/cgxnTBkIRzK/ZPcyQtKv+CZdO6OXytqMtsZcWXXiHeHmX8Mp2Kl4FMSM9ifi1OM
pGD+YUQ3g0eYMle1EP+vuBQ1p9NqSInvM/F+l2NiftUyp29AjnMtRjennBJrzaO9UROjsCEt49zK
diut8tn6sAWLQnAGGjaIZVHt2ifh6V8qjtI6Hy3qv0hhaClWAtoRRhNz0mlLooxKkLmpZRMO5+hq
i/KVgnrqWCp9drHv/TnoqUO4Ex/zT5OSafwk4aj1xXDoM86AdnbCRzjuWXxb/fNwBQMCUOsQlk99
0b9vGbTD2DwMg8zG0CfkWBKYzJTCZ82NGYd1ZHgQmxFuvpe3+tNilPKswUe6iPqf7DxpA0lkvutP
eAL4rd76q0qNqOjb7hoNw1Wc1Zrye4Kys6Yi/jLB4tHZC6U93hZn9+wfxAEVnU69o53az4GBavxU
63QERMNx1X49c3oHLeeMg7B9TuO2F9FuwOrrtU6bFvTm5umY3uvw96gZ1EE2NxQ2riaYO/5fvdVw
wceCPVFrsspN6vtwVHl6ZFxIGrrIDV2r38dZjDzL94OjEXuZnGhcAH6vHMHx2qKOWocd349s0fSA
PCt/ulOABp9MUhjWtMVyn8nf2BFM+P2McwLKyA7yMtlOc8ghkXQ6nzf4MsHcS8Zc2dPaSRE3g9bn
Xl6niNWLGYBEczcJxvO/tqLWRhcvKHfoyxFH0GBME5y9TNR5EtMt41XOCU7MPgHc18KclgUE+vzI
p+CIkBgHD1/5gzXOIQanJXsvmaRIFY+IohOawHkPXI9MzJylsWwwZ/H5HGiX57Aid/VIAtVHA/p2
isisGKTUo7sxFCnUvtmRC3LiaH1zNLgPhdv9y43VANP9Xa8DSN/Va4WUxkyl8yMZUEET9G/ge2GT
7bDWufCcsFr9LvulPWNqKfu5jhbTD69D3gwNLnGNkakAqZ9ytfUrUyzJLITStr/Ik0tCs9tERKq1
SJ4Cww61qdyZv48qEHv8okbu4iDgF6nHxWI2KYjzuYYxO825QzRN447uojL/2TTVO3cMJW6aHsX7
5ubzRyZOcoBvgwtCsmakyst+n2kFeGeDVEI8sNBJ72SMUee92lNGgsd2nqN2gExa1EUDhFfaXAYx
N8/tGMpovm1l51u2AQzzoEhAmQHWhEPcSUWJDN+E0S9nLvTBlUR4iS0lbE1lnMg2C/cCZD9K8+f9
fAqhAZravXNdfE05lKrNdkK6OFRW/Lb7FYC/qF4pYd5h9z4QWA/aTXlQR/ReZAuZzm/srCTUgBHp
eK6KjQjGmLElWurP1SXfvUgTI7JiCmL30s/eyLM6eaqMUU+LcL0sNYbZyKB8KLoxw0Qi0mcJnOsJ
X7mBVaifyTD7MIOR2RPg8K1x4b8ywoakrGxEKSdmawT/cCuo7qlmFTpUynTQ+Fo4QmpK0U9AM0NV
BsY2QVlUwXkuRzv+t5BXR+M/pKgDbPlftCe/2O15C/F8S1H0+hlB07Yz3qi76TiVX0E5HLzteUjO
CXqDYmtoXwqVTUxXtF376Ai/Z4H3sfeHJxHt09zHA0PCganG9IoC0Fip2XZOHmQGijEwkXmMMDYu
L66KVqpV3R/0b+f2JHtbbJStJkt++VvoUqQAC6zUCKvWQ6/c2tUCHLdOtlnXHickeMeZhrLKeoRR
0fi7oLTXTKxB1sC7MH2O/f/iuZ3UT74Wx3sin6zQq0HBc9JngQBY28d4CmkkgPYrxJp0tkOnzZ3z
eaW/oZFSOn5Y89WxFsAeOwS728t3gJVI8eqMqXAWc+wgQnrsEHNAmILjSsqEXmymAb5cwOZYCb0Z
PN7IiZdi/h6rWTORjg0eUYIcQwOFUhXukqy9wN6sVpTQvWzuULUV8PuWoD0RAciE8TSd1ecRgUvW
3wyWz3yLatXAogtO9+CiAG5GJtRpkQ91zCdTmkYT3bMYpEkegmLPOp2unEKimG/B+bPz1jYtFg+0
6590vZZXFJQ/mB7BLBYsQJmxHNouXRBliVQ1sGJwllN6tk+rGxunPCiHeI11O9JDIvdy9vBbuZbd
I00wyNbLShhR4kJozRtjviAeWKians9AHhsM88SNL0MxIXo4aWtQBtoC5v8wfd0xPKBjjA9aT3pG
/y96lI6JaGVR/xTF+ZYobfMiBaIFhoUjUi21gIjbFG1zJ5GdX0f6V4rrGa1dUgxvYPudKdrOxjwM
268GOX+5TKbqnP5NSeRfrePS0GQuSPz+Oy0+jZU6aNdryp15mUxFIZ2NV2TM5eT6QvRj8TEV4JY4
w5onhf0XVKNO24FPSbR8iBPL1de+TSER61flOJjE3U77hrLC0UlUizeyoYQsh0VqiN+n2INPuhuU
M4ZXP5Rr9kmNS6ZZznTCwLDrF2SftIFc/N3MXxF9GQBf5IHFgkSURhA9dPhBtYLiminSk8Bqc7zM
Vs5Yvmnfb4Rt0SPAvIQxPlkEAnCxjPiF9b58iF4j1Y3D5TPdIuNVmHum/9tYqCyHlMLI6j5cf1NK
L4S2v9KKLrKnLyR+akOvhx0Ek6T2WazeoiaOi8OV7KjF7aXTtttTzMoCaX7FBk5f8xiadpvrvwFE
aZenqKroLafP9Yh70Wo8WJorwJSZ9dRUb9I0ihqyv0T6yYJ+TOFjJ4N9QX+a45xQBJr/BcNJxdJo
cdwljFn1lSLwU/s+LP7zd8hIxiWWK+x0N0nwrN0lj1dhFC72Xa5YHwgu8XFWrq9uvFh/cBXpEkul
avDTk8cMb51lO7owKtT8d278dNW9Cns3ay0uiU4DEIEPfJ/pmOspUCM3TouO+PT+zQ5JOS4HK8Q8
8I6B3dwzFrJZL4N3xPolnOobvcDsxT+McG1qC5whPunAyXrpQoAazB4h2e2aSNDDnoJPF0HWatmm
+5WMUhdpNN6fBqMhYnQ6tLgK0eVPyTC+gR7tPeGaAQrA0WxLNZRVVrtcDaQz9Fig/10+pYV7+5rg
2yAKBwcnX5CrDKoxuh5P7UHM6y+DRc+i9+/97xv001P6TB1DISPqKY/y9/U4hcNfuhBO7B3qLFkE
x2LtrEPPzhKVZnTjEKFOR2KE+z4qSW2W25IESDNB8oeXd3qhFnneKhf3K4+oHsuFbO+u9y9xG+XJ
SMNFB0+tYF6fRxDAPZI8P0rloLDMb2wWi+UpMAPT3Ggg6ojCUtTmJeJ7QEE69GPmv6j0Z13UfU5u
NvPPrOC5E2XVHW9B+BlYYdRynhg3HeKkE0egpIFItf6M9UAY8hmRzlfHpQi+oCWH8GZxxh0snorS
voy3AEp64W7BtRa+Nb7t/kVHtb+v2DEe+Pd58AHv0gALL+euDBJEbJtigKjCnfMXuiwcCAjMqmBz
y7bz0cg8+hYNWBU9SXoDe/r3AKdQfSm1btABkkVgloEtiyvktN662NRbTYKQS02fHFhpQczLGwhE
9mK13T0p0trY1XGWQ9jhKI3185on3vM3p2cVvZx98uuh0PyV/uNsx7nLB1BBV3SSGDEE5l5ZGSgX
zv2/rFtNcduXuVATUA8p+HkNaTLB0mx7wYJMu1DHu92jmAR5tNM8liPYr+HU15vV219mNK2aPw5k
NB6B31ORRqPlb56aroD250E5MGbODVzGGHj56a2BnPxZoYdig6jj4tj14a9MYKzAw6YHIkjDZpxL
mXZ5ZGo2V4tzij9QmEhhCffkKRqseLIPne9EpGfrL9iBvtDn9JY9QgduAesmMIKkBYpLTwE20UNG
2m3wKnSImFgOuJ21G5O7e9WC5VHqYZqVxUJ0tr7SHKxrfgH/8K+5op8Q/zAluA/pLxOiYRTLHjKb
O/xPZt+1zkaMTv/j1GVUVyEnGHlWGKAbyXCweiIrpigkxnT6vPJTAt1bN62mq6YCzEWxGsVYgrrm
d/s26Eo2mNdesxYNEXVYbjLFOo4S63UvER8PenNAXeLeV9Lg/Tgs4wPsk4ayIL23IlqOzA60fOy4
9fvKwczibgMdyY+15zF5yCn3tW8nBV1jKeXZMvl5uxTytmJHG1i/19I4LaOuv3D7275O3wkSiu34
wmrFFqBFSNJjVL156xrIu+YDIuHDsq7/aTa5dklugkMOrKseZA+/u6Z6wsWddWoz93tqmVxvcqcD
3isuBCt9Ct4CRXdaf0Vaa2tu0oNxSYuA0EnCOb/DPjB3BBc1weVNlm6tdgaHSpNX3+e0fxOd8vOL
RKuEksNXycoeZ1DG7GqTgKN/QlZv4fj1fKNxLg/gjp0pZzMJsd+VceSzDq0rHSMfaVGxEiSmmJml
oVeOV4kniNlqYl7KLkSj1GoV1Tuu7F91IgQJlk6fsImKImSvDd22/FR2TWo/v64tZFU2Ee6MwsCE
bXL5fqOtJIyEh9NgZoS9qRvsBeG9yBGQHm1um9jCJglNgilxK8eNl/86EDLK53ROIwpOOJSV2+um
hVx/GMvwgx76YwUeodfmmbUUd3fthW7TntK4brCBY75MoaBF32A2q8YUJvuz+wD7n/Q5Rzd7Xnak
EKdwghSHzyS6ztxqVLHkDX709E4RP2c6JApKVfflpxxXGP1Ifcf8Xfx0jwGfY7VFCW0xDCXY5JY1
NDMfkoKgYayOFKVmd1MGX1+aoBo7kin/viUu57aF0Y7E6vol5mkkvXH9W2b1YDDUDHRS+OycLDEt
/j8ZP7AJGwsPhB62QqLZ7soL6/enJtmPIjhHse3Wnxn0q7mNPkL32dcp0A7WSrZKiU2wmFueuEYA
TPaZGwpTZ9Why6RxZcSzyeJV5HCRRrPLr0Jz/cCDNsz0UdTDqP8KDQzx0mMgsT1syOrx3aRS+mSn
ed+RJt27sSxyR4ptTYX7z8yqT0CYBNORep9XvQLU6wZJzG/p3lRr5P+nwEG52Q5KAIGivNdrFq7k
VA8/EIzpbDSvjIc9CAxtKv/V5ivd5qOMJ5YAz7JOgKRUpgQ6l5y2Vnmfl2X+Wy0ktRPem5eVO6WB
Bje/CPVPfISkRVq++UbZPAbmo+GSpNXj29dlPiNqHSFZSOZdVJUvLcI4wHQXyyMw06xlkmwazucI
x2FJObMWaqQSuXxvMK0kcEL/ETavSgtZ5wxaqiYP2qOGbXyWoCdfBMRBOmQ+a/pKLHd72t2GdA0l
VUY9hh5N7+BQ++lJadS3gHf18nGcfJ9TSo6AbwojU+sFLd0NzLPYvuF6XiyUQ3aroDdILTMNtPrb
S2vp9Wn+aUe2Wtt9tvxh63U0hQbNtjSt6/uHAaPhK0qrWaHmEcieBkfiBnxlYsP696TaU2C1jap2
h4/Zg+/OI3qe0m36riAHWBjfR/Xu1N12Jw1QwK9tFgp/ILGYV5Yx6WlUkJwuNC9+6+R2UicbdQpU
KE16z2f4mRSUODWhrztSmfHhtWnpHNaHf2Tr0qhnvQaoc0SDvaSvTZgf3eerXlTijwPwnh/ohObA
fj4Dcp21UVvVJd+r3XdcuP+UAnGWSSr2X7a2Qtsdku7DMqo925KR9R94lUnQ3SBfrWAQrIFs6+x8
6UYjdNKZDIMgkiw3mY0H/auVSUVLA6mb2xMcj9/m19NlIvSgvIQuCp02IA3644gkb7ZVxFkefP8z
TV/hJAvH5jem7wUV19gZkhLJaFuCCoM2LP3VAGMABFjjgJBqxoOj+WLi10QhDduboB4clJau0nvX
ZnpbfA73zXtzrPUyPxTn1p2daaiMHiaadLWRplLucdOgU/RxTXTJ0Mq9f2Q2BDqTQ+HpuHD1msaL
Hr0TltltdtNvbm+LB2QDimP5HnSFqQOMjr1lV6EvoRcxbbBfJMVdSOUnsK8+cHkGpKZQ9CgtFmzH
qUw8t1bpH1KnygXOWSMaduoGuZNdYBUe0/MAnXhjrlaUSCjBMDS9oX+tfsN45Hvcw5TZpRo1/mZe
T2Cin6ctSrGEUdoStwres6pgonDWzug8vzr42O3m4zi1GYqnQ/trkE7u2u0L/VV+ji79W8mwFiT+
aoX0QAUu2lO1BVeu4oDBN7QpvmXbHnKLxN5luPLfUMy3B4QgJEHMQqAbB9aw2veD9CehhAlKPfg0
A1W7hfZMS5tjTWKNwdD0JRv5MXD0olSlQWgG36Xfk46F6aCtrIjMvoYgdXjIt5KPFdWl9aRCNToa
ogxkYN7eNWU5hLytCUtNWkoqqdlZ2iYOC8T2fukutUj1hD2eLrZyHHMAmTqPpiJBkKTfZ40Vm50d
13v9e3Bby4sVhRS9cbALJgUsyI6sZgdh9TMIG//o+DuxKB7KVal5YJ/WkJAGbmIo457yIMhe6SKy
hJuD5tVK+F84f3wldKjjeWmu78CY+6P/ik3yJ7o9P0aSW69AxOYuaQz+cx9978g7qCHBjzrC3RDg
uOX7LaZtJ1aE2eI0E4ejXFb5mLntPFv8gPOQFPEqEINLZR7ks+9Vq9VdKUQEN5bU1G6faT2Nx+y8
YGF3mXBO46gaG8EXpWWL+yKxTzROQ4hjKone7MfsQpRbqEOUs+zD+CmEgqd7/GD7ECVIDLVux3Um
Yvwxs36DyDA0wKG8BdhxidH9qHfl1TF8Vn23ZSLsekL3sdAGYi+pDZJwZFgP9434XjsZPh5RrcND
2/VoJOSm7XOa+PWtpne92gr6P2lRsbAj39ifKveqTj+r+A6GCkqBVNzTccTqYT9iEYwAicfqbgP6
5JP1R6RTA44nsfEEenNLCSJh8ov2glkLQDG8kLEYUxYBeEBAOEpKD1NCJ0ovX57DOlFW73WsOc2N
ERAULgqhPGB+kprpB1TPb/6KQ6GXP6w+N6/9j9q7EEWupPRD/zyaLtpu/6lTGaB21OxAZiwiY91B
1TtPjOB/8nlFROONDHcO0FzwoNnbUHMfrf/9GF9BZwRJD+ef6XDTpDwjpceTjGuRAl172wrNCMHY
vzFDSA+VjCwbfRQy0bRd5MVLlgSJ6YdD1eXni4lWXeGd9c9bnCA+VAWWujhcNG0klmk8KYA2YHYA
jl/LHFq3Q0duIv7b7NNv1Dh02EUL3UU6Y9uEACaacRMGma1HC74wioo/CZWsWn+CCYSMN4vD4E/f
T3G8p2I04L16ng5zCx8Iu5DEI0soFb+mC5uIyS/HQfxep3XCVa/8Q0vThdwwHUVAZhchPSEE05e7
iXCkU+K9hj0BGYFuwoe/kArCqBKx6rBHvBzqgOxgdhHtX73YLnrYtYjMvdooYxzpm6LRQ4v6O/4Z
aI0uor3Ubqk9JKGNVCaQm3YbyeE0GxzBTiPC+zjggEN7Iv7HHVvTNEbn7CHtgKH+Gws5OHYdwYIm
plgcuFqTzBGtFUDuGXxsxgpbhgLpvY403FdXVcQWgO3PpaQq6lFh0cTKw88xaUXmOJ0Go7AOdKUx
LUC4e34z1+VMZSJE/1ntHqWqdmGLU0X0ylzExo+Z6i2992RPSsNuQHVqw2xr0+U2Np+w3l/FsgDZ
URabtAReSsKwu4lrT3KPHj66JiNdcnWZFyESjuhQP9BynZfGXr0Wq5UMcWnHe7F9GlhedO3jyJ+V
YmNlUTFCGWo+l95+nygbOKZxCauWBCc3zNXv/cD9G4Jk7j09MMhf37oBNVwPF4ctylc3i44YfNfU
nvFpUW9+5f89Ijd7dgouvvd4JsT0nRAx4R87d2YfghnRPp5chpnrYHLehMxvCI+EOVTBxnS8hJRz
SFAhcCbtlIcbL4t0SdQJ9RwdPhF1FceOLYbyeO1Aba8g5DBxa6Ox6B4e3H/Mv7xF66Oy3dGuYXCq
rjdHiaeR0sxGY4T+qZJ/XSxRkZ8h+qIsTlEHub0inJy4HXEb3K7QhjJ69BcKFVA4/nIclFkzWHCj
QUamJdmH/rYfZe0zrlIevFGRWWxkO3H1gE/3uJONCMU0JeZeLXq56a3Jib1ZlbybwawUFfq9lde7
BD58ANGgFhMsLpcthh7WLt92vA3zKqYSBPWUMpRf42Y7TFNs+TmkXvzLqDnz3FzgHSNcJ/1ipDKi
F5FQfi/jU+z2lKwQgVyCRmKddQsl/bWuhZfLWWq51oeYkV4DVCz1hNkfkhH1RgA0X7eSTlRHb4pd
FS1w3d3Up3zueXicJ4Bo8kunK3kmQB7unmtIGk8nn3xUJvoEujdusxwDewggUBCdJKpKV463VU+R
kw3IuHsvnsGUf/bbDco9PmGX+s5v6BAHrQSZp34mG0X412Rjn5GBVXShTYGKSljKZyksqSpieyqd
ZuwcmBF0LPSCp8Jk2AUzZT9Fvh0fgUp/8SegW4kllAEiErHi56ZnIkfxN6cofJxBk3wnK6aescoq
KwllNhVv/t8aBkJj74Lnb5nLvV+g/OayGISMDVscsciiHCFbrfgvDHPJhzJqpkAFRZYeHC+rQ9do
b0+eLC09S4GcLAo99Q2byvAy1XKSmiRsSdjB4y48bcSRdX54IXTZwwFHtn9Xk4vDjKyuSIpwnqn0
5hsPjMUBMraua4HjJUJ+suWAk1AVLJKtchZqryFzy/U7eSY8ArhdQ6s/mXCxWX6EtKKEAVjIQnX7
FNjWTOfIhEjMrRoFPKfQS2HL1AGh7wq3tAZ7pW+++tXaOiFdXgLm4hgONikfNlWhoCoYvCedg+KW
SudGx67i+9S7J32JkTVZEuhMn9i0CkJrwivvrOtO0+n5ppJOOIq0kSPnfPwYhx5UkD5vqsQXZQMO
8My+fDWNwfss7ruYARjecK4rJ6MyZtD8HmL7a4+nQlNmqYqvA59f5g7OELm/7vzh58NnyejENNmC
7Qj+RDCiJl+C8p9L69fgbC/BVSQVIL1qoOcQ7yc2eqlH1G8cEdsBwmN1FZRcuAbd8rl7ocsK/i3W
o9YjAYxLCEVQg8BF8PYXfY0aw7YTlTC59KKf36ErJAUvYmYRFgHsOOqGJN9WPewgI0N1IMD3WCR2
OCm92Ndn66jJUdpEtt/kWRHAloOeSbzHpI1HeSAu5u7fWmP/dZ5KspSUttVsk3F6Bd8hRkWmSIUJ
y2dGmBxAGwOR3PQExUsumL6qDfo2UmrlJxf1Nw9Pbl7zsFVgzOl9ejfEVxDJmrnC0sjoWF/ieS8h
4oeEC/97qkGsD6TIkVjIQgBixUneGTnZf6edniBoq4RNVME1mWOGC7akEAHL/fVTiVHk5+cPKndS
eLkTyRdBD/fY1ys2NUHUykYDu/K39zfAsg72pbdDIzSeyRtPkmXdW+etbMW+DK/WtK2aQC90nzz7
Tl3chobvDnUK5fStTLDXjkPxJM9GvAkgWEo1aFk0bJo5QKSUVa8tQmOPCHknilzGi0YmHA5FdZSS
5Tn7F4l6nJuu/6wM2ghf+/vnkNFbj2Xy1cxQVB/wm/GeCDfF0pOvYjRcm3Nc4Llkrws8G1M1Q4u6
FD1Oal1eknzTpeortKFiLZuyeRFUx57lODr0oWDHnXQu3z4xVBwuh3txeLMRXDb2s7hDUlyJOrmE
/U/hSnSUrHfTVEvT3lBlq/hOBZ4p4fLQm0BW1sfzn4V8WBGVSLe7JJXu9qJdus7F3uEL2bU3mRc2
tCBcC/EJPKormkqHlZ6BNjMCANBZGYXmnAkJ9YOdI4lDfwK4lZo8R5O0B6M9uA+Y/ZyHEGLGeh+u
9INjnw3/DL8ZdIS3rlhMTaw1vDTSe9paJNIxRbciDKtwUg/2nuWklDs7oUMmakg3w1j5BH32dN2Y
ikeZUrlw57oxZfbBIqJ4UEN5Lm8F+EzfGsyXB6IxSOm/GylBcKE90f3OujBEMGOEDThwKTRrjOXf
kiMCtzolRszZtVMLVz3+/X5335Po2msBBLcN76bkTZI1fWbLR8I+xF9RNS3LooMjeJYm7mtisR2y
CpV1xlCpdkcoc1siNZjL8p7ObW9PBsNlRbtZ8sTA+jjNjmIAsfewq1n+5Ydaoa7IZfwjCQ8M2ZAG
G7eC9tPc3gT/+1JtK40rUDB7TaYjhEHpqoD5ssqb2sYi+aUG3Zxa2rZe+CZ0zdB1nE6Tz1SVlmgS
wTSDumgwnFk2bGFmk9oC9WenyN7NTPCqIBYZvVsmsIgEZ0ooDNIpxUR8ttNM/X5SRtCZf7uME2Bl
HHs7CgPo5QF0bpycaJ3PbMOy+UBDavJ8JXyREAHFQgGT1DKrfx7mERnJMq+lVLFCmE5aPWyseh2T
ZnrxIMtERRqGxT4ua7qRG0NXSqZHB/npNHc4y+r9sDzNGJ4aVUGOa5FHGabS0uyV1N/jOBZXwRRT
T7ub9i/v3Yl6xKXNLT7vbSiY/wIk+fIYdclY/OLjUcu0x88IABffjmhD+0YVQ0SMrG9srln+FlHD
891/qk82GtYXwz4owaKVYSRZ68TJgEniNdKBq4EVDTFVaLF4C/piSZx6LFLZPyRiGO7Ze75hHdHz
QXP8pOhAXiz8KRMZarWKapVbJtxVce9PzA+wFLtRZ9AmozVhzpipCUG05qKevHGBICiuFWNH2kqp
DaqqdT5CjrtShGBFT+pe5VRRehBW+1NybgeoZV1q812tMdI0e4NnZ/iZX2CYJQA7bP2g7CXNP6SH
vVmd7ZcT0J+PNDC1g3GZQ/aIQMFKWZDxcd640J6U0kM+ypxASnqfGPIqeXY6iFLCxNpfSbLRuOxi
DRHs/9UeIXsQZNi6XhrYsFzK8fa4lAyV09DNHx2IQ+QENp9/jPqgj6QkPCWXITVLbx6PlVNd37LS
JoQLnJzzDP3frQ3xQitETO8GozHqnUH2tfVINIwLuHW5dR3WmYXHZEf3OyWIZ2FYSthr6bWsgWNi
uVDNQbjPl3TQpxlxxRxhX82N1QXlynasD/87KjRurQVuFJdiJSrb+lOyB0P88zrKXtWcVUFNAr8d
w38zJg23AliUGR3ie7wOm+51aJ2TrYepMnX1UI0ATVEV3xGGMtEE1zpLUrg9GK0xmj3p4YZO0Hfn
Yed9W5eSXHr2/Nb8dCObVuwtW/gAOaTDjL0eIkd0yl/MhfVau8yidAzaPKYrpLpRMh9+Yury0dU1
lMqfByCUZxrj28Xcu0oFR7+KNbpgwKGkBJGmAGCfE+o1YOlR8DkqyzNrTvlDExo6WvWyy9ktGDZE
sqSoJAzjwdO+1T3qss0i0xfzrG6Ij20FHqpkCNs1j6786/MTHZkNSZxnxcb5xFIVUbR/1PTHT3Wm
wLdVfxj33xaX/EsClDcLTW6dTyo2gKQtBLvpMvoophGOGZUS+XqV5p+2Bitemvww1YaXs9cTuvJ1
8a63dtMDym/hcxgWXf/uhQIzGV8k8rWR+aZWKlvMkJuUuNH+3uKR7V5QMYRA1X/6Lv5qFWnK9PYG
EvdXQtvfutlRYOS7bug2PPz8JuTotu4Jr6XxAeAm1jgRmdsOPRupU9MyZlwQrYvFFO4RTA9S7txZ
EpwBsyKYZwfWAuIl/pfrUYdQIzc44vxvdz6bkMWuRA/fM2Rjo7iHfxBVjSCzcl2K/lh8c0q2ZtzN
P/nzRgQygRskdIlCPBHRoPCKlp32OiYRI8mHHNH0ewHotnN+o+NwEaE1+eUMyCZ9vhpGL+hjgpXC
7TbgkwYN4PRlUujOhwmfUHh2QlcaRcb4hXqzaRiS8j6LS9qfu8Ktxipz3jIOw2ND7nmiKkT/99Of
i94eNRmiIqPSPBllvehR96W7v6a6ZdaNM1ILnOfA4Upfc3I3cK1tdgfs+nby+tkPUba+YJOgh9nq
EAb91ZaDESoMGhfmX7xgfSXH7nzvS2GmIgPCGSmOqj6wKaKLDa3w0Uv6dlf/Chn19pO4/DBZKxzK
4oIR4XG5U50CExkYvnDpdFnD/ee9Id8L7CztfPI9fNf71GABGgU/2fvqLfCFTXfP1O3xQnQcGlB1
gx8rkEkKiqZJc8tpJJc6ql/ojy0LSp1NjHEZiBAXooA72CX6rRf0sjFEUyActa02fqdDGtqWAEQj
EJBdX9fdlzl2syZwjYQpzoFzT1TRY7qbt8vRrifE7w8HT4Us1OpO+LW75oAE//v6Nvs2ueLU4rPW
pC858leAznsAJ7OFjYriRf8dZhvRMZo995l5OMLvKvWXQtbYf0+D5lNUus1uvxGoUECHC4yYINHv
A2Sqc66UhkpNEXY7m2DbKGxbufdKEniUMYcV985tQQSACcv70Fsb+GXHKa8XBrEbqjDs87uBJUii
HWNAl5Vz0UHUF78ZoLO5ScXa7t+T0c7darm8ef7sos9xG/Fs59k+j9tjBNH41LqORX4hADZuB00j
NGNQjKli34ueBa5pnxrCmR8sDDwd4x0qBODaIWBOdoFD7kT4oDEBMhrPAEu8o1wX9TwjpL6eCgeh
+yQVt0/0sErNIYFleaVYt2YPrt0Du5eKExgvc9F1DkG4ppoZuQHfl20o3DM25sRpVz5ToN5eiPL9
QyHxkPLRBPdsY1LHwBVUFsUHPbl8VOa5FrhbwzzfOj6luqtYYZ7QoFVlvcdaw861Fsa13I3VWTg9
JHMoa2cBKeajf+kulfB9mThmOpLwmSaTTuspX+ki5gTLjFiBQ21IUwTWM6ndSg7oQcT9iUIgs+uZ
t+AAhBoAuPkCVAX0JdOv4tTVwaJDvgoE2iYUZcokpgs0BGmK1TQg1zy9BWA9dPOzBcQYr9pbahYN
1H8ZucYIpbjNoZYQdJ4Z8B1KYhfxscJAPy5FNg7CRBzDC2KDlhi4HzJD8GrfpJP53ePZGBbu6+Yg
up233ufb42GchBQcWRGmK3J6MFo7F1xDKmkFzFmyeUoW0HRfQpVzcspoF/oIdxZqA7Abm9wUX6CF
jbNRJTNkbC0MZGq06uwvToAxh7hO2DROBkXB1zmdheC6bNgYBvNjM69wwU4Wwwj0jHozUtgwl9Lr
JiYNFB8sYznVZXpn3ppGyIaHerTF96Utzqyy+ZcYUYuFBFPYrDwKchSBSoEWa0A6oaf6KDDPj/Nk
LyrJMxAaVpYjdaRApQM7yNitmJdeMnSbUB0dQOh91rS3jHS2ts5Jy+7NAYabA9DACr7BAnEGqaEH
j02XxYjBP6AycG6fKiK+xTeQPhUnN9c7WFL82bNvsxfyYSrE+Q04iR93DLU68QDiIm5+48xRV6zr
wll7OsKP/L76Wc/n+8up0qtV2j72kHT3LfzuxapoWC0tQSC4/6asO1chH7StK5B2fded2Duw4RDD
7Tq4GdJQAt4mPry+P/IJHZi0YsR7iYW66plrt0pbDyVoV3OsQRhC28mmKr/pyQFVNJFU3Lf+gxxh
a6uHmrEyxP1DvQGCeJN6ye7YAv06rOTHvvLEazo97vZrmlvITWeQuCl7tNqjBgCybISlARt722tv
fK3gDGZ4tlfPgvmUnZpCi7UtLvcLMz0mk0LOvucKesHy0wF1ZK0yO2QTLZk1kmulpqJEt1PkvbV8
vuYkmTEDCF7rLhkXcDAHFl90jTCMn1UtuYy96i5ahxecYjuqntBohd0YDaAmhyTHZnam57gE4K3u
oqsPd1M2IfVUhy9uDxNYsWUONxbfmywhhtCWTqWlfKxeJ3IEVH8T/KTzIjMkxrehCWJNeOlBvQqz
nYgtKN/YDGfee3dZ71DKB2Vk0BHlUf0tCXal5Qd1vAHnDp97n2KVd5/OdiKZ6499pQC8rsUs5PcE
ijBHz+9UZGW0lgajUKjyR2qHfCTxhnABzlB/Vcqc52v1hOlqo7NezW31TWbmtDffhgY+RHrwDm2r
uBIU/YX0hg8kI4eUhaaKdTr8BX8v9PLvEtMyUjWYjtrmXe1aL+PPdopLbO6WDdQBtEe1+u58gT87
hcQHgpm8GpuQkgh5iN787WWeS9lvvUD53ILx3jeU15fAvk+bdoQb7RE3wmsN2lB9cL59ooWA1Ck5
3T+K/IbJ1nLbNSktYlDEo0AQb+NMrPqKb/SrDjk28Ham2wWrIMW4tsZ51GTOMH/brCPM0H0W3XRh
HARu0m+85FWj3zkmck/Ocqwqk0C1pbqPQyx8IY7FExU6vhtJKYYKHpS9f69eiivdXbvh5VWpulq+
X2VMsNeBI3/Mj/LgDDpWcFrinJwz73e72x7ZOv0buWyc3zLO9ozxLto6BUxE6UwzQ7pngB9KMKPM
GkbqaqWjV9lkFBmgMcQ4zZh/BpX0TRQK93vRnYyuFVD9bo20uz3/DkDh4X83l0dtE4S4tzg/S2Qm
NXunV0jnEAqMkDJ9s0FDuKoB8ioUcW6slykrUodWeN0farVPBgt+0lIdNu9swsQQkmQ7LFYPuDDt
kzBF58bTpyQPoeHmbGQpT47AKtUaLrfkLpG6FZ7PiSJzvHqRQIlBRQZwyM8gL+iQATqVhvsdZcuc
Cy3PAuA74hsbDlhjOfynGihrzt4zggOBXB1hY3SHu3ANXXN7ufUyQKxFnBCOr3uJN+an5klxUsjv
oAeC/7b5/G3HuM6ha/W7XNi8yomWU91ksBg+LBQA35Ho04tcPq+c8UiOu3s5g/MAUCsqqiS7ofFC
6Ecr9nOVjttbi0QYNphsyvSGgreoyphXvAoUmVu4UJwXz5SMBOCzH7WxPQYJFE94d20UVinEUFUM
XaQ15mwY4QIOvPg4UQTxTrki4aid8BFKw0IXmri0v66VJZdz/04s4rEoSNuagNroxVE6fnFnXBav
cfRNQyreVEnzeVTrTFO5hMCVy2ecFBhA3p1C6cJCJrsC4oXmriq5B21nIEuCMBCPGg5qxXLUzIh1
Wjw0Y/2tDFTwYuXtrodbAtMj1FUPJfc6RgtbtNoznFbe8gRJGjtlvTHIA4t/iy9VILy+IG86sVEc
3iIXTcUONfP6nNOzEf/dV0IcaNTW2FeHCSUDxS6IE+Rg7b5JjCZmCXrKyQPVdM42IjucoLWtrZvR
wpGVEIkb2zLAVUDimpnavY7pecKYRaW6uZS882ZR8+HiEFRAZ2dPHh5vNyj6xexY954gMZ2bBaNS
gDVD33aFwAIANKAaaIiTzq1HhMdYtXt3mfn9C+v6BO7ZNxaiJjzioxbFmOvnyyeq8rfTcRZXCVvI
4ItT0fcWaaacOrVaOMZ+Sz/uxfBzWg/rHF74/4MXcMTIEdaKm+kMTick4iIi1VzEiwlRm7lVPySI
qxz6Miap0A2ucSQxkYD9/8cBMhcDf8KraN58jEnoTwasimZPDhZSSTS+TNReKUYAdaf4NmD6p98/
h6u7nLEw6sRIpxd1EV7f54NzFAYk5D1B+deQ4OCLTBfTMSChF2rEk5NelX1GFT7AQbvcyaSvGeUi
diLuxq21IPTeaiGQi5EEZn+Jqm0rR3n8RCxoYaSdz3E/tsdczWcb7hRERYop91t5u/z5zAgqOKOE
As0fln+GM5J9achhfpxhplyAzoTGZolmovxGqgCgNwN21tRWZYUkWVjZrKtu+1VXcQaN/DrWSJSu
JdT6yXo3b5sdIXERos4Eb8tv2fA5ozpNx/lpZ47fXafY+V17GvJlHsuVxtYYOna11w2KA1TOLlyx
FKNbZZoaBLPNtIq0kgacxXRtL0aSKF5cux+kDLbOVBoFNwny7VF5x8ca+EHIkOMQLSEa+r3SXNBj
S7t6zWaPHznlOe9WX/+j9ZA1iQ2+PzIFQrQwZ195fVhVWNuFJxBEgAntFqxqVPv/iFjbv+Wvyh5B
u5yZC0GkiBYcggW3AGPHKiCixDmGrMGK3lTUxUL4MHr7RMEh0+T65uqmfwnvZ67PWUCsDr4bOKNE
9TaWWIWd6T+rXnRY3yabpNKndJqaX2fM7Gk6AWCUH1MfAa532iFGIA2AReDepWXy+OPXrSJbMLkA
ILesZe7qFGGccXhUcFfrUVAzbFK8ywTrmUZO/E+ixMafnv0ZhdiEDZF5QfDMwPavfur7shSq5NWu
7PsUqmbFL8M626KK1VwLfOfhH9T/jHTQ9bU/F4CpZQ+yDxX5VvjYYiwTKm0IlAqK/hNEwN2NJD3c
CHno4gCjnnesr9Kh7SH+Fg6poE6yEQNPg4EkidfUw/GqiEdHCeXpuvIJ9lMCe5iO0rrtMMs9X8Ff
GE1u/9oeDZz8y5wFGo4M5/O+7G6gVNmCaqOFtUoHUn2ilwwjPnY7DfAQJmumCYLb94Az5ixU+JaK
w5PtHdpt3AxLc6RHMQ06Pp6guqA6iVgWWTZwmc/Cgkl+camVmkgoPezQwpS+2F3D9uvIGjsCwzGf
GEsZA0tWw7+Hdoj9IjIQPC4c1RQWGN+L45PQMm0k/K0iwPHdRu2GgCj3MSFQsdU3UBfWbnMhGrf0
HFVbzj3UlnLSLfiXSGJqsyZVTgdTykhbzPfOC3UfhKf7/aRkOceW2uitr/KRyzDFdofBO0qDRDgu
RzONr8iROgpFqLu3tAg64nJdsgFRGagXtg6AZaY1WYoXibcR+lEFyAW8YjPUr4SO005Qs73U1wBt
zUVyeuEPu5VwVmGC2iK3IiqI/jV0dqClgpicsbiBaKiziLIM1PEI8oBTB6y2gzct1EKwJjNxm0kc
dKaiBvOWpNaMOedARsWg+M6ZoTx2ev16hspIni+4FFBAmYS58MSIlAgJN0O2tBsDolaf66QH2VH0
0uTZMNnBCcNCcOc7m+3UPvyse3COm0TrnvY+LfX+ImP9S1qoCHDiqCOJSIw2aL8MuTzG0plgwYRJ
ehi10KjXCEU48jXhbNzxYUhPou+WqSlE6UR94g2W10l1cJyZA7kDO3rX2SA2gUk65JBbj1Ahg0O2
qmtgsCoA6hiLg4S6kUbrAe+AnVd7D1VkXZIM0jh80LmbvP8q4o2i1Uca+nS4d0y9BLsOjocwKYjh
XE7bWxZW/WHqWA7OoOvPNvxnxIi7WRNsT+lCF7dKM1n4Yl8YXR5qnXYV1GdeJxDARY1ExcOJl78l
dY+OzWTRleF6pmNDSg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_12_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_12_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_12_0 : entity is "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_12_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_12_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_12_0;

architecture STRUCTURE of design_3_c_shift_ram_12_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_3_c_shift_ram_12_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
