// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_15_0 -prefix
//               design_3_c_shift_ram_15_0_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_15_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
YgB7OXXwwWZiGMzD4NWJR4zeEoomiahqNuKkpzsdRafiBrlakNPmxKqamIh9XulMyWybCZJeTDiD
CEr+dNGwy1eE7vh1sJ9qXnFbDZHUuzgNsxqk5mBcKYSjZAh9CLiD2VBiZNvLnrxJZ9nojE07TocQ
FnmjfsOdZKTds5a9bvczSngyftB07ceQTDyU/C+elNihyVz1h2DR8l1s6tfIAOJfXASnTJbtR8il
hGUVjeEw5gXzXWUQo48N5ltGpEunvBw+kaC1tbVxDXGrqKRz0WtQVrhPiLMQBJDivpGuP7zkpodF
i1anmgOUKcCtuCyK9FgRxCehruqkquiJiQVNOGAMMsqJoI8oHA9fbHqg6awnA82vk8I/hRdImtc2
BpzPiDhQavbrrWa+cA02HjgaipPKKoEkUzKrOpLmJ51OdETnPXkioAVLPxO4ruD0hbd/a9qUFd4X
6wtjyLtHi4B5jdedGXb4ZnrD4Vn7eaIXEyaO+KSSN2ndxAMA9GVItidLYDRqxVjxHdKbb8GXr9Fb
lezvxCmLk6/cqaJOJdfeRl8iOCeTAzcmi1YyRG8Gs8p77nlz+47RMUmSLJHWLaDjGJzVuXge95Sf
sDLSSuT60RtHBwfI3N7xzE3o2OS/wTpdQJTRgLa19qYgOV77lTc6CiX9AHlD51g4bBr6oWdgGiAF
6cB4kNR2kboo8c2HlId9IpRuIRHnRy8w6rtmyK/oCNDBfQHWb4ENmwT9Vuyq9Tn/Pff+kozblmSA
CKER04II3IrXd+402muYEJ+Jomecn5Dj/3Tx/p37tOfUWcFP4atc4sFb63Dr0BbZH0SK5Mc9RJ3k
FgZvL8NhcaSZutuCGwz8FqXzABLyXAb9Ds5IA58MhOgiiy7eQRMjX/+BzVgHCHmFAD7PMCRgGOEo
A/qq4HmdmtbWL+bKEqtIeNGLSutYp0n8jYUVka1BbHGsRDkT2DziVp3zUMid0udc69bLSX9rqQwR
zcsmauxcUIGKSnAXx2UIpNrKbjH66VRagm7+q3GZDTrYTA6wczUujJ5I/Vr3JH6zLwBEXbNj0QDx
MsRhHvukPIg5kbA7NCyCQbvU6Jsd5EnaVpuP7X11hulcvSH6GPFDsaz5KSHer/3kuYBh6vC28mzl
ehdhAwwR9ABk2sMOijCkGsl29z0MlXcs9rvdv588PFRdzyp9feGlSS2R1AWFE2Vr/ex0KQ1wU3ke
TnElbysPuRYkeDtdpMZf4GLb/0Ru/23hEOqRUEB7crmNPSjyT2OOc+aGZG18nc2raAB8CE+I+dAt
BI23jneLJwsUfRX7q00MdvfHDqtcGhHURy4IN9qjOak17o+4dD9uTfAE+uzw9LIvfB+uLmBU7Sb/
vQQEnpPmxdHpfxhLxNhmAVxCb2O/33oRjfb1qsRtq82JEa6T0yg+4JsrI7f8gJHKxmz6fgMdt4+v
yXHIHQp6f3BIbc5YwF+hd8xn6q9xg4auJCZBz7TpehdGZWzqKkpY4iICIDBBM2TuxUxXd9DE1lGr
C1uCqkuvFdfhhyMVi4oBpSkNkdD1ZYpOYvV/CsFkRigVG2FYxwedlAjo5LK9dUIisZREr28/JvoX
bVPDF/nouEI8+HPEBmXfreydmPMn15cI05T36MNB3ZtGDZMld66q5q1LBJtzRebJ5OOJK3QKkBBa
jJtfn/rEBJF6sCEZfgrDn/YL708a8+XNM/2KaRx2Z0zlZKVf9dwsvdkkBrpNcwIiLFUWlmwNVAuE
V8pYIfu1qPHR4cG2HpSLTCw0MwHvvXJ9P1H1qlnmCQn4QjZbqLxwnxICd1ba655dogNuU+hBzaZZ
QPfGgLYBv/iX26ej7cETSSdrnTuUfaQtHG3wzXWip9ms3Y1O8TX1Gm4hh7SzjmOnhUXpAt8jxNZ1
TgCXWVUkJnIELUxAKGoktEVMZIJZ0x+tOPG47rMW4YchZGT2sUWvZAoNL4SCZ+CI0CdrxFs4V5t8
BqkMVeqGFDypFhAINaq+ygqHdRXHFOOMmAMufimm/REfKcQew+WM0ndqaPl/CFglWdXZ06r/8NE8
RqIYqzFXhTmMFWv03ogm+2uG036ItaAy43B0nXoY7J9oTkieGNvJiti5RynTlS6P7+VvW+3XmqaU
lbxexOMT4WXqtuNPPNhbrr1l5ULJ/st+qojPN/xaiCGj6KT7SHBT+sefHQ8XUKQ+WqjUdtlSw7mz
/lWn5ZOc3DTcJEQ+tV5/5Nn1Dv4lW+25uqgXTS6yWK7IK101lUw6MSZrYG/bBRZhGI30Vgr4FiYu
k3CLz7jWiAOROU8E0+n5f0oIl/mKUj2p/2BWRl5+/O8RLT/nwivVfacNn3Z9IzgLbfaZ2xWXoRUh
ub5kDBArJHIDBEVsb/nwgo4Fs/kN6htVjjviri7YKGxsjK7gqb25SevdW4pW9Xy+XlUAw7pLv7Dx
4ySesA3CPZXyTMsAvGS1m1peFJhgoJxPPhC0t0MsAOwZfUSr4IflIXqC+ue+PgRfkAH8JcyrjFF5
7AKRcv/koIjtKBSSiyrMP/ut13YcLFW8qMdJq+azPjWSI12yeSGhWc/20aK55FyzLPpo9GpGcRFb
faEDAfGsTKdX/XtC60LvWv2DmfiVLXoKmDO5CyfAyR1fz1AuxdPBO9bf0vo+7pIglQapGfbl6NuY
QXjm1UDlO634aoH80a/edShsaUFuxG06pWnBT/5s8vIVs4dd1csACE2Xz4cVXSOQPjrgmSr3jWNX
9JSkn4+QMVutiqcLNStqFVjL2FqNY1cBiT0tqo2W/LQXgfjTc6RBjwNZPY2VWKUi0A45mFiZVfsJ
EAg0s1TlAG+jAEyRfnECFGshPOBX+N1DaoEGCgKiU1nCJ5XmH0gzu/8d1Pd3uyfnrkNUVCEaBgD/
n9EeZDGwC0NPZXOx1cDNYlCY63o8pSFHe4mtNhh+hYRMYquHD29yd+vrXx3P25nxKDAT/MsvigJk
qM75ifViPwe4UVzRY3Ec23eyOdX8Yak3xBxUzoC0cDgwep78PG/XK3CVNhBnkmqeQesMLspyDLfH
UDE9vjG3tErZeWArksOCvV/mdgcNqEBMWnVeR8+ehiT3VeIilvje1RGlsnPj4Skpy8je1AIVUFnM
DlZJTl8wl1Dp3AS+0VH9HlL7pr7UZS6kbuTnW1BLtqfZjxryVYLq6WDMiEA8Fm2eh4XDjoQvdTd0
LCr8zQFGc542Cwd/ICuH0yCZKMXmkS5zTzN59cMsL/5b7SIfV9ZQisIpiyJlzbsiOe6nSGomGtlx
lUYiNAUolSWQUbgF/1D2KN5iKd9nhcahPh25G2oKwXm0rI/BAYYeDmTReh4EG5Xg67+kyk2juRQD
lKRa4WEp2AYS0XTiE6XxTSwafVglIMsTPqwzRM1vY/IxyJWEfG1RZu+RG0ddjPYH37meJMxvwPZZ
elFdtURJOUNw5TxfnALl2LZBeDZQesd4OdaFVnQxf8a4oRNDJdnEsiSDG+5mMW23jQWiwUG4QmK7
VfsbWRQgU7yXT10UK3qP3R/2pzJjQLDjvzJ3PHtCxvzRUlpvShtc3+4uuNKQWYJVqtIdJZwsCCHB
LuXlrvakgtMovpht2GzSw4BCymjJyASMO2DZe/S2C342bEzAaXcOxgWIOg+8yV/mIpSTsa70o6RR
853BVpPZroVgUzwVHDflyww16T1Shi5W9dSrtXHa4BZ1xmxeDvw0wBn17mIGY0rDQ1TaD5+6h30S
8GOJ+jwNnpkBdMd/eD9xOPlu0CSrbx6C1/su3sGV7tN1llWOCvIOYQ+dRQXpy/X3dd9o0O6IXeqT
0Lg+RuQ1eUMqy0+Ym2DkGUYXp4m/bYNhmdz82EQoDbguHWVVyYiJE2lxt+3Jx+BLRVmyJqN4NX5l
JP3bBcu7kxYcjYiHipm95Wcn8I9GWRkhjXjtakvV27VPeGjRzbn+NFIHvTz6g41t2gRG8Dk8q6r6
I9u1NMYa1I1WT1eKeEF5H3hbWyINyzDm5PqSBSTi9rClGlYNcNg+fsGhVywcArhlAEjp8uwPDJw8
4j4XnG+oF8fMmFDbE1PdOJYc4tP2iP1QWvBrgjDUyTcVtLV2dnm5PD2a3II45JIMZtgLSKEl0h7+
m3JNH0SEnCqkUO02kVLJR62uiq8OfhFMHevb4BYw4iSxv4gKjKT2XHv7Nu4yvAs32VzN18JVzm+6
CWlT09s1ATbtgNrXngoEPnZdr9oTtbnS8NxuCeLoI7za1n73sgGPzTEs2R7MHnsjWC5DQMdyt8/V
zZlgPWY2mAHTEyvwGAB880zB0+qBDPn7s8KOWOed6WIAgXji2DUQPQPFm7DNfE0Cno2zwqVMzaQg
9XcdTkeoh5iaLspqPI8jSXppbXJVh1KcGvL9A6k82rR9IoHV41rSuuFYDAUDvGk+JldugyWugwB9
bllEQiYHrZ7GE3sZ6z4DmJ5R0p5Y7vxPuhHSbDy6g5cbRnQ7sSxMl3ZFZ13kNfMGmgtwgT9lv/nB
Ga/FF2vfcx2o9lnKWO7HNm1Y8EYnJK6NfB22Fx5/+RPmBBg+xS81yE5kuaGotJT80IZKrWbFApZm
Y/V7mQqMGAVebdgKFzetOoPOEMrt6ZaYOyCftOM3B9JEwzhfRlTIGYeA2xCC0Q35CO6vZbd7xpiq
WPMzjgCdDUyrx6+8uzuPhzGWKnh4ZoeKgXKwgiH70/nDpNKOpK9OmxbVfJsXJD9N3UKJ2F8bHdTI
lf09Gij2bCnHwzcuIjgoAaBiO8a0m8e+HyZhpTSnzRc79rdiJNtxGG85pBEo6lTIPX2+D+kjGkRf
BkOUFY+GrJdwevut6PD9YfXE3jxLSrnkWYDlYwIqhMs2yIwyMNkjvNsHH/cqOLqo0kxdwr86zYey
3NnJYwlvt8ydT67Zjz5mUC31Pxdk6eI0wow9ztu92WspQdYd9W98T2wylAjTvmAyfRKPZk4ZzOrc
psSLUN2AF73R03UY6KDK9YAl3AW08Fg8aKqHynBNIyg2ivE6/Kcv4J4IichyxXxflRdQt7iFO+qu
bKZvz5rzWGzpykGz+AwlLAjp7mtoKKVs7OsNtQyGx8XeK1gKwmuzxgkHK9luBZ0zPeKeQmaAgHcY
t97XxUVp2Nremp0G65Z6re4V9JHB5wuMdrcHZGlGaivtArT+zG8vO+wPeAMerlyL6XG7VfRZ/p+f
3BfS6PPrdilNg0Ik5yd491Rn7xU8WGQpIH1Cqgs9O6118w3GEyKT5EOyfFHChmD81MmeqJ3KMWJO
HXmivP2+CLCyDme7dMHKaNqnm5CZwDk63TmgmvNDcJCTMI+8Fhe9jfhVWAC3ukDaLDn2NG5uUZQ3
7mOuUxNVa0eGzRfvquwAzloyJM/PWEKIeOwVuGNeD4x2+FKwaSORN8+UbjLThLIoFW2E+RMF6mTl
UKLycD7Eh1/7fOI0KoOuDohRdwJSgK1oZuFxsrNSQ5+HU4EVeK8YHHqm+qm1xu7C3CHQwim+xtXG
lqNYgTrslP1jIoX5xP6NtuK7d4Z0z1qWNvDTTIb0VzcU0LWwqjemS0iEArofyCDPDHNZ4OEG9CI=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
