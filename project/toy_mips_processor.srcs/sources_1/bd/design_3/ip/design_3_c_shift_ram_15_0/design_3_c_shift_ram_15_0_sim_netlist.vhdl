-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_15_0 -prefix
--               design_3_c_shift_ram_15_0_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
Ma+dZGs93+LeOswsJ2TfoYYxpJRsvZcTLDBPI3xFGvruLbbUBdVizIfHDQASh1fMU4xyF6waNPiq
kjCHy53fcqbCNVcWMtHel990RL1pGGyx93jarl7a0HYCv0hrjXnFs8Uj0IVVHImvBaxegy2NvFfe
e7ZhaOP2k6aDMyI9YkFfxP+22Pjb1AUxObmdzuzUjkHMOFBY9MqSpTr7K67u2FTy8L2a/6e8aU5+
4H0xxJ7gpo/fcitawzZETIDiDX2DqBMijGT4o0m0jpz97lm34mCCEdvOjXvk881fNNCBskUBuS8v
AfpOjU3aaABWVPTn1jag/BLTZPJCO/XNKhyyajQQkp25IfWOncxxbPO84oTqttKPyeVjHNK9sTkq
BbN54OE8IHjLvPCrrWWuszBZbp38jefNc39lbqJLryEW4CCuHPciGUNTQN4rDUi/0f7C1+1pPiwb
1eVKPHdGDzOD73Xv0vRqK5TolU/yStEVpfei/4rGRZqIVJkqOgSz1jdpIzcrotJhAYmQODuTyuy7
pkG+5uBeqGXBEm3dGWBlZUG3FK8hP7hjYvTt9eBIdpIQ1xH+afWOdhHVwuA7e00g+87cq/gfcHVz
M/EOWlciWQnjxUMJqqdfPsXLq/xTjf2GLurXe26RBA4X1VuoxhdRS4CUzhj2Q86C1nWfAyF+xiOk
kYBfjIfbNOxCiP6n/F8qpFWtoyojMkZmWihW+4tQpM+60EqYfQz/Wen1VNwYWJo+ncIv3bOdPHtv
JzaZmQVQkITHdEIkTfFoPLax4IJ8X/GeHu2avQtc1Mu1Rbvm6vyruu9uiIP7Uy/fRORdBM+fwG54
2dRX/KFZxo7iDKeanjMOotm3U1XBvDAtw/OhqIUecfDuOFCcwPfxnXbIkMo5CR8N/+wBmPqMKWQd
J/ulStcw3xGf6jzmHsn9v97Qt7n8uA9jnlirp11958sddfiduy1MNn461RibFU6/9n/2X8PB/8Tv
y1Fg8vSj+xOwIN1OBFsD1I5ABXl2Rz0l+6saMQeqsXQq95psb5K4MMrmtYgKKOy9zMpFsciHjxtr
AwJio9EmOP3JtUsWzdrd0KsmOjKcNJkU1fMyJYR98ArS0W3mb+tcARK/YYlwdRzA8uZk0zKnJ9o8
OBcJ1uUxEferF2FnXoAadvkZmIE6zpf4pAg3m1SyCuCOs2mbK5BIe+/df7WZPprgfxoEL3xPT1aJ
sHJRsfJgy2NTFq7RfZ2/rucxBtdGMDYO0BMFvHH0twemS4YoOWPGVQSBOUig8naA0X+DFPsJ2eXu
MgFGulFZCnruPYBF+XhKcjq5fEEPbiqVjAIcFaEB+HU/CCGmSybex9QlcsqkITwB8RryVWi6saDU
gC7hd4Yu+YgN85fos+DAi0nXTG//jS4MT7be1xfzQxE/chPzCRPfNgGBVSteoCFXoTkE8l71+kaO
Ez5uEHscEYmA4Qbdr389PD63IThLskujriy2CTkBPjA7MtdVPLsINpja+fA0ULFN0V5lNQSOQhbD
aN6jweh+Jj3jYlWUrT17nGGGSzRGNtOS7C1MiPbhhVDPz6QB22WrYzPAu+6AAOmkd+9LiYcbkycK
oo2Z8sdEaqsPM+PGw3OJiVItTTpbhZqeHLeCyVm7BTFVPkj+XmEkzC4u3rlI3Y414TfrY3z65kCE
T7OKEIvu2jqoD5dU69hO74AofMewryfjBWsTJNy80C9YIJEd11PzgtBe1uLZETHEGs7mZrN0skdb
xH+I/nx/KjJfYbA/zlMJ/sxV/8MIheM88YKQBWY0/9CkNS7vvbBL5m55x8XojbswV/ulCvesJz8O
38gMvTI6UQnMppakebd2aWrRORbrB1ZCxG9520P3rQJvm+YLsGSZB+GpfHV0VNzAgVVgjVylNZBD
H9TUez+b6T8bY/OIeZl+i4z9nIBN99C9LBwqDMI8FhmtZDE4m79MjSw9V9rcKbIEcx8qDrD6f0sz
m6ltfDoHIHz/b2yvwc4eYd9dMV0dHAae2fyuKGDcn4I0SkwIZrUBPNVNA/BhcstBPnvneXtPKt2p
rk52rufVVONafnYbO+3GH77RmAf/af74b4wRaag96lXccTcnb5xuM/kot7QitQznGtxWJ3WrcFDX
oJ3idHXxEp9aRCmwNMlpQH6eXSyMjvHhRd9iWoGSRDLE46vmIIsBaeyqP0yK9vuArZWfVq3kUhLZ
dJs3q6ELfSUgh6QmMQyBsrqe5AMWTgg11o4Xb6Uf3rKHxtwxdtQq1YFqjiOSsbmnZyavtHWqG/l3
EIqxJSlqk1RNCHjs5a0ThG30FdxNNtAQV8R3eqwkySlHWzus1yOodxFDBjLf5fKC3SCa0aabfvBc
myKut4CMh+rnGRvULWoAA55iEERAp4I7jQa2JEG+AgQGf0KM9XqNGzniqe63PylUhKp0j5JlT0ul
R4HRScnLp9CrjciM/qHGvKS2BW4CSUAjo6OYRXOiA12skTlHBQqfW4381mJTJ3qP9r9VJENFP++C
mYDY9xNxzXqrEBM3VmY2NHWdy5kf27uZFpd+oop2IKjJkZJALev5upVwT9lN9n4Af/PcuoM2Wcp0
yLI6V/lSkTOchzwRnUZeCyw6YMVfYs93pzpPCA6HVbz4LM8DcPeejmVZZdXX97DdKIAvb9ojAc/J
JlNe7cT9Wl4T0F7Z4UBv8XZFSoSiG4R0Ps1ICpMarPjm+y0nRa4/Kdwv8ZsSzPMqTt15gnlMaL57
3LmCxMJ5DH1coU2/cWaA2/zlPkfeP6t7FADNPO6lFVNyRl7t2YmHdLDdH9ZMvfvuRKw9Dl1ORHpk
ll60ygFThHjp/TSxX+HEpwggKbgZUI3TdNz8mgqxLNqfRkykgDCd850E6TkRPtASgLiTKbq/4I1b
k/qWHnqlWg2WtG2zTt5xw69G6AEdTEGXeuCEmUtProCrrXvCYteOjNZQfPnqZjSvyeI1r9QkWzoX
Nt5i9tAwbrhuTT0aN3sZVSJPpgNQ98CRUXBv/P+a1Wv/9HOi0J8e2EIDfmRUdr7+E8g/86MJ02BB
Z1R4CCyW643YEWnS36SWSqVC6irYcR1Q9axWBkCpa8Cz4p0P0pJnxmtipwpN3MKVYQpxSfkSnQZo
Hyy4XrK8ZC/sM6tBgOvGMaeAg7WufFWss/lRdZQpiZexzqL6QZQufzYQ2wc2Q/TzTrUfraTPm13T
qcoHjTdtnyobopEpwoEa4qNBvhyXU35OWNL6lniOwntjWPMMg/fC1XJdQQ0dyoS2qk8iv3Shq10f
AHDmsvhwIZsJY3UsscOYxIQUPz1zUg+St20+tOQ+vyZbRpcppPbygNG3Q1ljefxrCItb5wRj2gs1
qK0xz51XeTdmGqRcJNDFET2dpfafoN76tfrhDB/2jl5ujh0frmBnKZdEPf2NIEEdO2KMgvIRJ8Do
tMpfwWkV9hLS5sYBLAKZCdsdUYYkoqxijma04L3dEJk6WhyU3RQ+LAzOVQnj0qS8o5LZEhBJge/X
ghqkaLINEwo8kKSxcYgbDxQuLDpAZz/lWjFCTqAO8LZUK/nmluB9REIA2+PRprtydb0wFaflZKVw
B8Mesl2dy0Yt+hemO1flJW0Rw6RQYFrLE4K0l5HD6SKbEmxVAIeaNuIwm1XASgAwOG0JV8qyKiaR
+Hyy/4Gjr9/qNe8Eam9mqOL9XP/BDqhpYWQP5uvbuMyPcpI53avf7yPzEtKmorasdE9juObvYkLA
5HiMksx/9OkFLcLMt4ob9RB2onlWToTCWOqpw4nM4dU5tEFSowhqBHDbsZoF801YIePDWDbPh39W
vI8rtN3tQKNL4kxjQErfKdd6eL+A7A1/LJPXTghA9nvDdEZskvdxJJAN46EUcDZdoH/SAgVtJNXp
rUjlppoMRrsaZZMd+FgyrNyPmClB+v/uBrYTdOCv+oOHi9kj9T0KkcHAV4yULeN1MeShNGM0Z8Dt
GmS8noDaZc2djse/3SOVwyTDObCNNv/xbGq69nFeZk22t9E4P+hkdDQoY9ZEVqWAPPEmqpJP1AMh
I/iUTC+cHMVWeAbUX9w7xdE3GPXIS4e+EERH3mP5s2+mdnj1XN2jjy7sPffMFcoN1ZYLaeMkvphc
pDeNZ4JlPDCcMYj8pBswtkvH/AOCtKIhwm+ek247k/9Anp0rgSeP1pkbv9hsXSFZqqK06CjA/K+q
ncKEZ1GB4gRvU1mGcBaX154WlnKSU28TKj2wiD2amJV3I1hc7eiomZ3tRlujOUfc/k+SYGWDTY0s
JXqBslLeZ7tZVXxkA3WBpvlWRT3L8A6QZEVXfnY22QPLOqWkXk4OMT2XUMUqHqJMnwbcJS9Mbf8P
tq2tm6MtK6sdUTGIYnbz5zzOGbb4ffl7dhURhc9qJo2h7aLn9iBeP9lHeyZnKWDC+nMHFwcEy8oO
5TzbRXoxcX+hiipdDsKbBpaDSjoQLaHH9jKcR3zL5IOhO80hUoAjLGTi3r9QvIh7RT2cG339QJMr
0QbI9GYPr8VZjcO4ezxgAlqlLmrxXM+xLJTH8qbdcl+pzJTQwtLIpbDSOeAGduHb7zW4p9SU+wCI
R1p9CSpj1/zAuPm9tRK409pcYPweW1IBfcO3mu/x4OdbUVaVbZRrjjtuWanOEMt1rGBGr7JU0eJ9
05d4LK9GjktqWjVYH89QrsRIHFaq8TO5qMfVGfGenRat8C3wNaSFH3youMvFbQvrM3+HAdm92t5z
FjJbVBmWZlv0TPBoV+KpcV2DIJZQkkpyMLUujXD9bRSebKNx2m3Y5S44GP+RPgpPNXGMcFGRYDQ0
0Vk0qbMrguAXZn8PP+g6vkI0sli2R7Swq91L8f/ZrOf4jQjHfUwj+E1ce1Yxp9UwRhh+vAyjw5XP
6q44q4vsLUTpKB2e1xkbKYvvYwK/WDhyAPZYTtshnoK24Egp9f/otzXYjlJE4YZbBucxiWC93Run
Hl7uqF8o3mAUDUcKdN9zefx+webpzoK3gFqN5a2xKvSVE7bw0z14/ZQ1Z5hnkONranLcNEoU80Bg
Xm4trp9Bz2C5WTVLhq6JpJRTjXZY9PX0s0hFvr6ZoDIj3w79maTfgU+aywd1FBcm6PuttlBoAB0P
7gHXb9wjRuEfIVmYGcPvg7Xdsdbi6EuMFBCwCe5AsL6rQoWoelfOSvS1HxLzwGX6J3K3ymubzBLz
VzCoty2E/MKvvqkiM43ibvBM/nrHEN+3dYgChl3JDpxvzC/M5tDxCNI509VQbN5nOl/r8+yJl+ZG
jQQFXbt3sUZ1VhUHE7SskLMczi5MROIgFue4z8u9ABG2hTwgMcnxvEtr16/14vK8ZzhFxj00dBx1
WVDkkRqUA9d0dR5+ku84/M/lwP4UZ7vRiTJeaO66uAPQY3rq2mPi/1IPwCo+Pw7bFV90TRamqFPD
r4Orcv0sLzUb9Z3HKn/Q0INdaN/POgYfF7S/4FIm2Hs6jyp0Q2GbeJUd+fmEnIZ4cPGj0eRDn+ck
msZAGpXCK1hYU8oOZE1ANNId/m0MUOqrQ+yzmsZDPU1tLlXEsDrfNHDqu+4y0OVHjvTmOqWWIjPd
fcw5nF4eweFxaTYtz4jidzADa0pykx12AjE/4f/JyGIjracvx7DGvRsouHclExYzmBXPw88Ta/61
wi4ThqmTcaf8tjTGmdMDe9jzyT/uMP++61oofiNdhkOFBPpWvVg6WQdTqQ5ovbde/OJcBiRgD7bz
+J6mGl3USlXXGuIwF/AGArR1NHxA2yvNIHWL9m1cYJg9OgxRWwyAlS/R1nQZ0NY3ohnhCvGTBvgx
7o+maDMUBZ/YKJtgBiflZWkIDcM2JDiXh69gtCgCv/MNF4KudYB6XYptL16JLrKIMyBHI4PPUjNQ
SdtZcdtFyTIMEzKU72miOEtjaodiwvecvDu4slYB6vV6FgltwsABgsQanl16D2slVSX4i0GnLI+0
dawlQNEmwW/HngKdOnQciAJs7y4oVGt1/0LyKFTbkQ4oiCr6DCimYTldvOydnIYhGHitNei1wx3P
s3Ms8CSmtmDCcKeFyxOKG7gJ9+jKbmw0EZyVTbIOPdqlRXPNy6xkR/80r30uoZ3VvxXCnO8IsMPE
s4NvXjaRxJg5fhWsTNUe7SOiXGwLIBGrWK1r9T3tIPbGX6wvHqdiIzUsftIu3s8+tD6/iBab6APo
dkYC2CmIx091Gje8SGmFSsBSRCiDmQFuMQqTli0Sqf4WzDaDRDeN3TrWhDRp9MAqKJthtn0CUIm6
G36lFjKQu2f6C5IGZbGxtjL/Dve1MbqMpyQRaGJ0aaDMkJRIQyNqAXOoQB91uKmHgKOcGtZKB7OL
mjnUHTltiqZ6wlVWRbZ3tCwz01Qk0/FD9ZV3ECgovmTEAQFKdkxFmwTEwFfJt1gtOaVn/bK4h0kY
lm98/0nxA5FAdBhDxDJaTQ8KoRbAdL19spRNcCwESsyzR/rPHlLB4Hp/QI774BBbH8e7Z7gYsEDh
ifzNi5zpvUTscovZzPJWngGC5YGB4EJD8B41c+DjihuSJUMLLor97Gwey3VpT5NjlDkgUxubScPQ
phArrdL/HkmzPSd8Gqm1ljfAbsjh3WLmIV2iP7L9DT/vfmGZ0dmhQ4vIH2O6Q1forw9ubcvI6WDw
T6Lp4bk8id2+LxLphfRQhlz8/48UerPubUKOTjb9OYUpk20UvPOgiBJQJ2pICPodvp13LKpnHRU1
p23a874dJThT38jFrW8+uxWkWXTo9byfecJa+7H8I8Wa4kYw0PxNEMLsnf3VXinvosQvCoqQ4fxn
YWNybcIOZI6g9T4Jxsxhj6Soe61+oh0j4Q2mqs23PUa6XAiNKXkagiWAZGBdXHGKLmDrr0YCchKm
ia4yU4kxac6Ukr0jVYLMtpAiWf+waU/8C0nAYSnGMGfO/Ohr4QiuDEkj8uN9FV+kXQbISKMBZO4L
gazvsUVPUR7jBL2I9zVrneikxFZhyPvQd8WSUSqd3cWp1ioPqldMpFpWNvzZHyCkzOP64Xd+mw8/
HO0cAjLqX5eI3oZg8XqB8PuGq7NO17wcDGa1TcDgCW/6gG2PNESEkQ0NpsukbCh4EM2/odHAYIg3
lYSB2Je/YCUyUjf+Ud2J8tk/9AyrNz2zVbhzv8ESxAGRY5XRMfHU4G8nKHkh5zTZdocjhWf5Q7vL
syZl70TbKpTn0vGVpKqjXtuTGEJOludTVwCGuOcdMlPW9/vP3iiT9i+ujppX+YtofSu4ev3bSBAB
w2GSx3iAjA1/J6DAlCQCmjdRbPTq4OOdZt64az9qPULJ1aF32uUhCkZqu+beJV4BDCWrJPx/ayJC
B/61ksA5QXV9GF+tWsL70qJnLyGhQLB2p03iinPf6lnISLnKw2b7Wh0NO/JM67FF1PtY+IcfTAaO
ABWWHHLCp2bIlCPqR+Mfmo6AbeWFTrEmotg6qYxsdfa3QByMVfOGzDsXlOy6zXIRPlJA8hZcIiFN
QXowG0yQk5pr/uU2YwuQ9/l6CjgPhL7g2a/agDZdLbmgCJXdIzYU8U9qMr2/zhN+a2ujBsTSDgRI
bRNYALxVS6krIpo4qr2BHOn96XBMVCiA/sXfLj+cRfkJM3DhUN/BkYhzlQKWgRR+TjYN/5o6UQoA
jhnvIUnlKvLXa/rOcX5r3fEXnPjBBU5vXRW5x/HGJZv2HJfFjhGnEa8qVlTN1Onvq6M3S+3GFt3R
ca0/dDMY4zWmzUzhwjp/U6E9S963amPp3fUCrG+8scTyipC+tyiCuE3+6c1JnjCOivicdHcpwwDe
5onfxd8ICWIGYpjxjfxL5yZrNC554CHPdRT7r69/lKn2ODlPjSssyx/vY4HVVqOUVLrZTvabURJw
oUDx3nnN43RB7uZ2TtpO5V5qRxF22qjoKlV1cemeNP2Zt3gglxhl8dm1R7M7dgNMkrlyQsZtmPSy
Sr6XjSGPHegTWxJrI4mFeK/k118HezF8h8bjL3+j0ulGh8M8CXBG8fCgiTNbmsWoeqKEyb6HT10c
BieIQa4Ar+CMoWmg3Mvg9nxwc/cG5YesX+kFDHxQ/h3xkWMPe0/ZTuqCgVgSmzZN7FKnJcxU2OQB
cTTAA8fIFxxpIGLj2KUuqYlSLaSfiyCuXj4NxVVD1BxDl5wibm1BddURKYve0HQkUItOVT69Xa59
JWoxbRNKlHXbxf+BOw9YCUAb0cJnOH+elWd3iAkyPmR9z3mHiZohPrpaUgmbshUeqVpBNVtOBEkJ
Y5NcQ2tv+eK9i+5I3Gg8Kn7ZkoWDWESs6GgtlH4YzGJ60XtvshhkiAaM3Zbwn+neL0YvzRu0ZlIs
eeTo9VBYLRkC0FiFz6WZxFU0oXC5z6Ks8hhF3yHGBPKpgWMuKMIhcDjtUY1IjfcP4WOOJevpBDf5
OijtNFtJMzHPf74MGzsRDRz2baKWY3uQF4G3V1RmU3SRlXrKUSHum4SaCttkr8sEg+QiYyV+7d91
DrIzOTeC5cQ8Il+bvsVvI9oN2Bv046v7AFTOVhEUkg//qJyrk1o2jzzjtz+vAQ3auQ6PqRuZPOJ8
/yHvQAW9HKNmJZTx3WS6Icl9PIOj28HgyLre0YzJq+9WcDsqpxWHtwvu19GaTo7kChELpdXnLqPv
CK+FGlw5JRS2bixQ0UGvMGRUOAlNjpUgg0eay10iBybLLFCCBG5A5pw1x9njJIAlcUM0fHdP+/8r
7u+F3FnreRZQVpKSrr4O8tmmkhyfLOM7ULI9BMgD0vDCqRB/7Ydyr2YwM1eDvPylvWxZfUrxl07W
gUCVMuxRI3PugPvhNwaQb9brYsUuDMwVUhdEHUpZAhsWOmVXCbXK40WoeLIYp6askO4OXSdEd03t
G72n+/norCc+g4dOVjc8kzuhHCtQa12LteYpD9Ki+RSvt83WAM4rEaiaPvhHqMX8wNUn4OtwlQu9
gdALjkecNAahDc05eZd09CcJzS65/wFhn0EtX+LfTDJVG2BZYvaDSbO35ZFVLZzl16jNSZ/9zmLq
avGzXUWHZ6rXcQyVACUa3OvWdDAuFLuSYBNr6VBJ84KZizXQO3Y+Frs1MhfgkMCr/IWyZdRHOenI
hhb10c/z5J3eQm8nlJcXOTj/32aZT8BKW9Zd5zOt+cdNzwxjdUiAv/kAsP6Hg6oI1zjttiCjIXRY
+Xa6zMAOf/4qt0GCCB/MPf4CoZjOIH4hTDdLG41zI073BoJLZfK33SNIz9OpxrWqUrw/qp/QWWPX
YdreqaJAQBpp5vr/4/7GaFLKsPLJtwoPLnebUJSwHKKQTZXP33URUG7bacnYGPwOc5FMSwzBsX7Z
DiUrYjy51y2b2nYbsEg+nvqSWSUMdXulqHxeqstU2x4+FKWVjenATxdLOc0FxL8qmQdV8LscCq+i
rxqj4c1T/SBTmrK18mvCZCERMpoFwALKfPnJq0At9fEmeevePks5F7brZFTS7KY7NhK8+l02Ipjc
1PDKkPvdIk/1GWNGVqA7UQcwD8ANPgeECF8KXkdtvwHjeYSRgT5O0wY5itKFsMyJ6ZZzH0KU3+29
yiwaR5cqHF1GEdgrlyGXxreQgtjXCkH60axIJBSLm1w/lvYhORwGXtK6OCY+bPKbuRxH6gl0+S7j
UwL8lhe0OyOTwlS04TeWMaml7LDKZ7y8VCkOk2BnOl8uFAswLO+mfu6oJJqSLniP8bYkjUNwji0Z
cwjSqU9NskY/RvfT8+QWol51eYDLbSOlyku+4u95LZtv2tvzUuAxSmcH8RQf4dL58k9TNG0RMB43
5FSXW25kLFKWK+FzJuNzfzokhjW5205hNR+RIkfky2bbF9EXbghqIVaD3N4IcjfoBMAKwA090Lzk
Rv/l6i1emyKJwNpcQsAfAbUS3F5cri+b1AFAoaYj3b2QRZxudqUchCGKxfgiwmwB1UNvql0QCjMQ
ZN112EkOQqSwEna/BxS4VYJHABsIIKCYMWqzJGrVsTgqIXiYr0U9ZpIuO1Iq4FHJodouKphGoCZi
g9vYQb4YDcx0ZDZszs347ug1I5/efSL3Ys/01e7VMU3e/gugSpz86tRtC2MNly8sMZRlTJv05d0k
46ejaWd4HxdA0bdxck7vNwsDc17B8bSjS/1VY4jWbAO/Tn4Lb2JpLxgKRP2N7FQG0a6RExUglHcY
JTuc4peVNTheYtqdisiZvQIReDpJAPGXA0fMi6zq5TSkLqd7ZYq9/5Tqdn663n39/HF6Boz+ywy+
TjnKAL77B08s7jL9R1lwqNiLzsn/kx+c953BOAnPG0CborcAURtIRktLq+/4NU2+FVVMudcWsa1d
A6xDDJpi+57c+JsTu7uT+n5HxHAfAozF4Nt3gGjEkOyQlA/VcmuBZkCMj93RaCU3S3FZxaHCJjcY
8J0EYqH2Ej6btIZ3duRVdgHa2zlU/i8x0HYICIFKCdRjVuV1kmQa4K/z3ZYbVsEKFgnEJKMWpv5o
YpgF+AiOUzjYbRYmJqIu0GCaLOwITdizKVHhSx/dqsyea+3JmThc0Rsk85dNPc+uCbimapSnsU1w
R3iLTF4ixiP9iuDXSlWlG+LradAYZcusbNscqn3OVSTw3CXMswnb2wZLvg8A+PG8YtK3badDlkB5
5aIDAlReW/RYk4ntgWizArgDQeV1XxNsKlehX9LYeVoIVP0N4bmmJuCmaPwVPKFmzZipmXnVl8GL
6rqT9Rd9pksWnSPHtE7m8XflxH2NmfyN2gSDorVwvIOGlA17TI+ln1fYJ3OpRMSY4O7bxvFWwKld
03APUx3umHae+o1+Pk0u64J9/sVzUjAyS6sS3cIzsd6dI+42jnoZt4mBrfBV3JuUdmCwDG67Whta
U9RJeqkz3IZmFhyUlyMrTXhAt9hm+DV33EwuBs4ZuXDDZNlH73csHPBzbJue2GIie0EydiUoGvrW
88v7CZamCVVFOXdIDqtwdpEu8cEsMz8W24kiAeGU45qVcQfbdadH4wRg4bsEYfQ6z0QwM1fIsspy
qq3OIyytan2QWtYJ8dugZ3YNjna6spnqS3ytE0fbath91a9W3lEJRe6S0rvyjImNXvH50VWIGMZ3
oWpDSfkwtfOFOzvRQ0XqQ10BIzezCPBUOnyNBi8eZn0FeGemNWW+Iirr/9gQlzN92D0tYrb6ifKT
hLjI9W56N12HTEsEcchuvlERFabj+cHK447ZySU9wlQlD8+4IwGvyhI/GZl/ihPEkCAIuSYzM41x
UDDyKDrnzGs2Adxvs8kFRpOilcw1p9F/7ufkkp8u639hPu0DaGzYPqSN5SF8/9hMvxesUQS+tT7A
SYCN7LMRHNBjuhO5C/wI2m+UL8yB7UOFmycbjc7vhRaKxfSSQWByrTL+EOpnFysKXk8APvQARqdE
EvOB/4bTHVs9bPCiLwqjak+smdbslNdxsvWuL3WQ3pP69CSj3x5dHRCJ1fKWXZJB66/iucy99FI5
xPh7DQZz53iWHSxnpXKexWUqUEX9T34SKxNrz3SBp8QAFEsnNYAeqs9Z0O5hpL5V4Ofcup3PLvVe
+ELGQajwE2YslR1sx4ItK/X5LUnAr7tpPSFd/vinWnGwEOGkfb9mb+3l33GEszfRMWm9kY2lerK2
kcYkfv3MtkDKLHjI97UfHMNOczeoXM3h1QOmai7pWwPdurS4MpXiunKU4OrktG1ZyyWRZnY6H+PQ
X67Jzor08A50D2z7luSHs0du5Z4gkoEPr/iH5dbmILLv6rFgvf+LVxsjBzdZyCWaQ+b0DJYynptP
tcC/OXeO+ICotKu7Loiwj8z1PL3i9HOdS2OVjDBMLfQLSBebtuOcfIxmJofXZhznjVXmqWJHYsSf
GyRi4qoQddrqXhucieIxwQUfs3iEXNJOr2UR1jlB530DmzOPnJCYSu/Jw4qqoHaqyiVbtlbU35jK
+g4EeEdW12cSCFBXyniW0DKxVQKVMpGOXeW1F9768Tqxlx0Q3coiy9J1f2HE1/OFs0/kLxZ1wAlH
jhaQUu6CaGhceAxhox9PZZ6+LV045LW2Wl1oLmyCa56gmEIH8kIQWTeVFsP1FfU9nkHvzNUq7F3B
7ujQaHcCfMZHLKlpIy3AEzVwbCCioxv+EbMMjRiR3MWZuCJx4bi8ejMSlPajfUuoD4QlpiI0gf4F
UX2Cz85AIT8fIWHtBsnUP9QK+0PoDva7jKfmqJQJinhTfjF7sxmTQMiDuoSl1g265BfPS872tUbk
XnS5FO8JcmI+hpYiWeP/leggHSDj1C+jzSrk0ts+53aku+q2WqfsJDGLXnG/Qy7+ng3I4Au6yLMS
ZO8hME8dA8qir2pOrVwzKkhTO27N5u2m4BXl/8xcRIRRv2gvcYG+Dk0USL/HKPXhBmFkVZWfGyk4
tucC7PqIG8Z20SXFo2YNXaSwiq/rPJ76UDFAdShj+oi3N/2hnESUCOb/GwVSeBx2VXsJ5C/JOC0b
zLJ2H95FW0LU6H0s2Olt4G7EfCJVczFdIi/a8ZdHdCmv9LoCsj1fnGAc+RKyo6JabOFojE/zMfeh
p/xxEe37g82Al1HxpfaYre1A8u4ZHpfV5Dw2mSzU1HkRCrjt8H5LqP0SrSAOC1uHB8i4amypiylz
fVG8ps7pCh+yVZN/lkdPNcZ/XEIidqiJSIfoug87dKCpM1dwLvj6pfu2N7qlDJk9kxnUHyowu6dn
ZVdwEwT42LWg2KVxMJpD2hwWLFkm1jgtfPCLY5br1hIxS0FGF0LSGzwxwAfCZhPTTP7MaY9134zp
QVMWWp0NVPbaZ3pAwkpyyRcSpd2iBt3pilFIrTcAbgriXSK5TTW4Zs+Btcq/EYdX8RyyGIqsbbiY
ErRdSQ5RwcxaEVdAE1MQ+D31osBbwFGFoRpDuq2t2Jy7X9phg3pc+CkR/WjnUui2pdi3xkpWJLGI
DGzzDqtFPrx+O3Ckv5u++if/qTtGF4oNAoaZv84VGaYR+oBce+s98cqqQowZl09iiM7DFXG+SAmJ
6I/QDopvaOx5irW3xO4EZJYWfABqS2P7gQ4q6N3rkUyIu4UQojouq2muNugEy6L6zZhXOJGCGntK
KVs1k4lTGwy0YrFhbsFhMU3YjJEXXk6odiyhP3FlWcagj1cycGdS2pOka9y+ISG1RGkh4ikxh4vB
Sob9fwV4T3s+cvcaCU4RwUV+rMGWIpQp0ZzyrUwb19sOXqnvoX2WzpmKlxDPwqo+DHHfYSdNRPXv
eQZUFjuOr1rvjZB5jCSVqPz3ADkSIhSD6Obz3GSYjyV2YGb5XNy1h95RzLhwPaLGhwlaSc1IW0U9
sl1xswn5SO3/7VfPLokPLQo/dxGEHJmJKrohQVLLVirbHGC/2RHTQ/NLvj3D3vcBWagVhWju9S+E
vFGrsLbn7F/hHd0G8fhqbjQRilrQnkCl/QNB7nXC3WIidkXvfqEkjlkpVZQVHLvUuNjSDnqG8dAp
ZqbGLazO33KQNnS0VherQVKGhTJJjmDCWc9AxL7/l8Lltcq2S+sYblJbHb60X1Ldywtp17GT4l26
cRzeddTUouPkpoYUqCuXPuLngMV10YyhxOQ5MkSJnwdAG/sa+epQGhwE8W3TTONxlb0KGajhaagY
KTh8rUfBH63rwKdZP1N7Zv5oUWa7RSrdpO02GWTTLQpZRtO9ENVzlZBc1Ow9TUFu4nXIbcXoYV3j
oo3dGzUI9CbkcxoNOgo3ziGlHcftiouKMG4j8l2Zj8WFXxz6wLAGXndlQnugyMR1dJGbuTbDGS/P
QBRPm7/DE4YRcaL929zJMLjGcBgTqVpH4ZgxymCGZQxQ8H0lq6SEr5E9O0fOgUnlMykKi/23NEtZ
MJ3kfl/XJw6TJInUeWg1W3NVFRSedPCqgAzQ4EnamCNGlbqMsgHEbqYoXPC/cu7i8CKcGoPF/MrS
Z5T50qKF7Ti7vQI7EZjHj9/mjw6rEFyrNwYtPWdlNNzIZGpxZz1qwNEfNtZVl/Ip3YTLAcqZYna0
o3N0TlPnxzrk4A836AL+m5us6LluepddJQlg+KOorttAWxDbqZVOZcZU/iakudVidkh5XYtaHzbq
wKSH23X2ss1HvLw+6RlRmstO90/iccOo/iYg5L1XxRw1R/C9g5AecaECbE03sz/5VWbalLZFJDaj
WqsexbyKnyCO9fprQI203gGV6kBbh89gk70Q/Qje0I52haGC4R+qQLPWNRdE8XmxObThvoB8gdja
jXcQlBIlUvoL9ejQhN+RdeyRW6yftXzz4IpaBTelg9m2YaD8baaiaiAfnijltclpFcxf0UpzRcO/
kJYEi1bwoOIp1IRldSaW61KYjI+XgAjJ1+QzGOT3SIYRU0oCRRQIR/tBcmaImspBn+UvJv6ngvKG
yPmtfGo3FXlFx10egk8q+dQ4r1BdWJbsFzgDCxqO935Ey/xJr4BP6RnRf0IVIo8tfj7qQSfXx9lg
O3D74d43QZ7w8hMU5cbrJBZNrID6K8SajlmdfKClYSfStBxC+rTNXMEIzy0PVWYq8aF2N4cUuUN6
zRBOrw/4eJp9wGiAIHlzZHavUGjMnGs4/5rpFBQbkJfmaXSpSRiKd8WzxO6TEOtEnryHt93ykdA6
yi0Cy+KXGpCKf6wNUB2fOO3oqaqVaAc2ADv6GnuKbtNyt1+vWwrGp096UaFTS1YHhMBkMssCeP4n
1FTQZiaTCxLcv0iAq/EOjyuvbIdTDS5YAniBRcSmx3uGnU0uLapoogvqI7EaIlz1sKKNHAsBGW4m
peb2gJLsPPd/xyOqX4DS5XReWW7UhaDQMSUrMRuKQd5yu7ZOvSTHC4053UkxbttPfOlDSfMg8uqs
Q+iI61BW6GMQ8DR12axiaMOQBAmfy8uvtmV/XKoPOQaMA8zfBpwQEqiSI1vhyD64DRqUwhHSvTby
4Th05MuEWbQ8nU3ndyADj7oHTqb2tz+/tDoQUawG/yZL66q5J18DgrYnl61RtOLtgTBGuQv76aL5
PD+LujrWYq299XHuCp8rG+Cz90dP1oUR1qNc0E1SzMagViJsiOiVRBJCGRbVHNrZ/HQw8ivAXaAz
vF53iBquoJBj8uIWE60oTF+DIaf2wdGCEbFqySlXgk0ygeuPeM6vI2qCYiDK5nNanRgYGJzj5F3y
mnUrBWolek5z0mfxRG91xFfffz+BbWND2nCIbfuZdsszTPiMD+Q92ADquIHE5X2qwYRrBsiLq1nO
q3q4tULBrFUuMvjHKsthAAL4NEAyozApoDYN7Sg/wSbA7+5j8nJ26fB9UAMmkk9UqU390OnpXqdi
nuBY4CMKXd19Solhv5HV+lfnU3oROkwJIZNMR9WqZov5C4CQFIKglrAy0I4DiEHe+2Zve7diuMNw
DLHL1+lqmDVKHoMpekpm1VBYARDJbKyTTuUKt44hDDgwVXh1J1S8AAicSU0ivGmvrp9NliQS4fe3
3+RDx3Xln+hOL9jXCDFpy712F7w16WD/u3KZfvFgcct2oyiif07qT/vujJPK09j/exdjC/GYCK0/
ysu43OcRWMkN2xt0j2RFzZr5XRpp1IL+nthKSig6EKENdVkzHwV4vlXBLXQgGn0ZC+y+2odz97ke
lR85z9lvf5NmOMiniznN/PjGrILIo341SSyGemPCrU1dw5rTnigA3q89fxuEeb2KL/GXOUBuYYhu
K61ikkPf9rPN1vCFGB7QRv3K0URXNhCXboviyqZeHaACL4aSg/W9zuLyDoXvFlm2H6IfYYPNp2Is
cBFecv0pdTk0cQBnaJFpiVwBoUrFvsa8JgtZM2chYsL76Bp/ZuJtK9dTemg1XvTMeShdG55AMGUX
yRZHmuNtcmjfKqXeXr3i5O5O55N++q7uyLnBdH1pnSDhYQiXBoRZSZxcsCKzUE9j37z9AH3ux0QG
elqJwnlWpCqy2iVlICGP2v4JjxLnPuin0r805+2nHkBN80QEF8FVWt70T/HoG2bfR/bmMbFqwh7m
YZoTGN3bgxLdjiE7VLQlIEpbrJGBrH3DCY7gQ51W0d30hljAWsHPHmU+ER+f668O/xzGKclkqVVN
D+PEmaHobnF8QYS31sIc08SIOa522UgHw+xRtwDZReIU0Qb8Dz+dUO3eDCV26CEPNv0LBFRRNTMy
FdPj6+n0RhgVx0+qmEN5FOOKPkQFyjMepeMnqtsbfq+oIwt1tcielzGzyZWiKE13NCQvM6UWVubO
6SYcWdUhy0Z5ktykPp3HmWP6z/C6cRzi1mHXeKYCJ0nFezvIRS9sUti1KN/92oY7cMWD9JankZH5
YmKo54x/1RTZi/Yl4/KLiFsVvG64I/ofRq0S0UzBEzqVzIBvBNAyhtshbW9GsiZ861PGJOD4cQsC
NTq/zKXoE/iVMHyJYe6kFwKEQpU6CvGX6U3I7YaKHX7s/qhlxhVehHfEp96A9zF4oeRx6mjQFCMI
F1UpnHK7pGxmpdcizbyGqxBwXwfqs5htOMdr/f865XZuMHTVW6qVGf91CgK2qiu0E+MTBBy8+TkU
TILCPmPlvOtJw+1AG57TIu4crfWFFQa9MkmBuajYH9hNLAfAGWvM8ZVlaplB608FRbCY+iCEDQUo
IhlaXWI+qzukOuL1ON3HmPwrIySZE8w4kMqtX4HA6jExpawwumObOtz1XmNzTnwEiacom8eR6q4Z
yZeXRvELxDNcPsJnNW0wjMKS1UynoJ01K+9ry6lFB+0e5RR7ctmc4f7N+RygoiM/vnCng5x8d68w
2USZRytcVI4jbvhNRDVb0ZjYvFxVsb2AXOnifgaiby9tfhojTG/u1mmYimFj3sHG6nGOtA0uD28m
aCF9/tPWX3BTY3/0WPS+2dS7tkcAStFwEVpYezbOrbioI7JYjEpnxLCkM0keYSu91hANODbsDXV3
SnO+zXjhR6xRicnBrwWPu6gBABdaR16aWb1Ag9VaXUwQq6Ct0++9780hW8bI4EVcXQTh5DLIFnie
wJZBjU4LAPs7H0QRHbK1p1GPXD2bRhnl9mRwzM8eigyM7lwcRlhqr+HmWnEYWRSyZCiqNdH/1ALP
/btHiBbcSvrRhvt71tjwnYWgDolJhA2pu3WK3v1FAKAxK+7Ta8Htd/T6L4L7DdYcttHpq/U2tCm3
NiFO9bZP02yaIFmmoOYRs3dbx2CWme9s66d+1J/x+Vezozh9qXlSZmnUcnqTImjHzGwkEN63VxLb
BxSezJ3gJrvW1AzTT3UMoWeXpx0ZkG+4aWug7gE7V8RK0EpgsvIAddWj1zpwTrRGsaLEadcctg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_15_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_15_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_15_0 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_15_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_15_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_15_0;

architecture STRUCTURE of design_3_c_shift_ram_15_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_15_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
