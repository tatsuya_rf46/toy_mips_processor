-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_16_0 -prefix
--               design_3_c_shift_ram_16_0_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
tnSeIH7yqeHZnKw7WGMyXQ47lV+8ssw5qjRUg0KqsjeqrUsiNhpIpI2NQ6oYrPFgQtEMb724/Ds2
6KcSDD7U+AYjt1StX9+zI1oJfG2mdF/K13PW0w67IDmZ3eeLCIyeAILYKAErzcv0pnSTjMO08fvN
pYbNOt2/0jGqyO6PtkpALO/Qee6zCgCm0AdeXXasnG5d656XV581xn6krPEC8zA1zgDLxygIcxuB
hJNUJ3k50kNRzhNtGjRHgSj5DKVZLglxdmEZ9+aow5TzOeA8poTw+rln4nuC1s8hRH56H6m6Bjss
pjXV72ILdiXaC12WNwkutHyLAIO5MJepL8+LY1wb0YMG/kY9200jUMO9BnyzA+aj0L7QnfRyggKq
4dnZNeT5FvQhwWmOfwWZoW6RIhDa4AqBlvmn/Q/r9g9hEzL6tqowmZKLHVvQeozV85cXDwRnlQ7g
nB5+MsskrktH2nlnUVfvXOdd0oRAGc04KUxh/8CnT3ujnopVUcrAGYCIPYgeev5j8+DwYnXAV74l
fXN7BulKoTt9wPiSNZtzPyd1F/NjeQAsyMWX8HAOR8CkCyk9A9/itcre5Q7ME+rlc+cLmtwJ8yeJ
YXo55CSni7C9FsZCV1Vc22B32yfwx5N1GVmHRSHN33BlMfTZN336cq5ezIxeH4YGp8aKAsLN3Mh0
FHenKN9lbFkaKgfDBCw9t1B62YPH9PXb4U++o0ftGNvg0Rdg+pYewMbfX1qZt0BnERuu60072Qio
hBFu5mU4o69+91RHcmxlEtO20f1m790HFX/YNxFvXMKiguXyqD7HShbrwBPwj+Ge/TBSk/EqewcO
0W4o9J/Mu2VcdVw69B8SpzxYb4woJFBgRiFL3zWz842TTsk/gNlDmgmoPx6P6jQHw2HeceJBQhNg
nSo4fha9+eobkXwNckkiFEBZTMwf/0AXUtFLZdl/oAOyX9dDAJG03D/ec37TPrw+261377drq9E7
rDHRXuLATLD8R5nj0isnI4a3Qo7CtT3S1aLTirBsVNEt5jhkMMug9pFOV+07YjAR1r/fYLwNuEkb
sMyYThbE/Y3epBZt3IPH9g1Yv8FFR8xSqAIIV4JJU6PTRx6zARQYKqeROevDLhc2L8EElG7S5yTq
3meb9o0JBwrIwF1ouPAzN3vzmT6LUTBFTkJdC/S4xnXF3cZrEzfl88eaN8N3G622EdEL9ddHIqly
EzfpXIfPTjYUU9ad/u6pSwiE2JLchuHA7QBFenZssqO8PTRhxgL5Z4hTJDbhLhzEhAAjrgfidiee
RI5kuJkftOoSPKWq/STfoAPzdW0dHub3IXam9O0GGBN1s+pZeucJxmXdB1uMGWIDwh7q8NED6Gi1
Iq8KyD9Sem2k7ocBAQsS1u7x9UBiJ6XjEJnH++fm4kEDlIWWZEQA5OH4ABGYURe25f0KE+o9Jqp0
f8LY8ZCNmJkEI/sFvog5M4T/TkYuGJb2ySnDbH7UWCplkFxQd4YF0c7sMUSMB4fEqyFS1mS+fuxE
48O+s/54VkuaMeslN90gRxTOgj6+XVJSLJKTzIDSasZr/wX5JBzoy5sAQyDkUNv6XFZ4sgnH0sMd
Kh3vSP4mYaTbQSn+oNRXjxPwzvSzRrdRWxJ10/LymcAYL6UyLjcn9Nc/akusFFcDjrgc9WWvQF67
ItFfFpWiKYC589ReyXvkrqg60LxKXJxec0xm88sD5BG/M6+oZwSRMva9mXuU4eeJzV17rr+TI8VI
abHnuhnsCykgkGZCsk11B0CATTVi8nkj1TGvZvUIoQxy3swJbbf3DI3YJpEmFncWr88wwXQTTbpN
4QOIaPaXhMyl/VWoTXALx5S9foA5QR0/J+pWjH8mZ7QEue2q2LwZtbIk8iSRSfntc5hsyNJlV0kf
qlADgWLxbRwqT+RK8ySHBfM/603hjtc3qKD/vNYJj7/b1gK7Xo99yPFXsMnTwB2TQXwWHwT8SlfI
Aodi8KxVfcd2pG9F0YC32uAbJrlyNhz51tj9/ds1MAEdZ/ap8cITL/ijTsmc+D7eDQJLBvkBxlkA
bzMSTCzpnWFT23/wyn7Twbj0tyYiAshmSuJ6j/tJRSBIWQeXWm8hyV6rBpLlrVJogW4rPhc9bTMC
AgwN6XwEf/ND1WOWJCemrel7yMgZrLNqA9iAIEIar8bczo5D44zhwwJ4sUIlm8NUpJQx2y30viLh
D3rnxw0vh0NL9In4mul7I/hmQQb/eu5RJ6qOwXdBoXU06WxfZ3n1sL7xI1pvkOfFTJfo79IUD5E/
Zf9O8VgOG2/Op1vNojc7OpjxvgugSx+sl+vS1bEzJsiqpdTjw3TuV5fMJ9ox4C/uM3pfssb4ofsg
OR7y6Q6eMRLRsZTxVjmnrjw8Fu63K0oaMR/t/0ywUCfXBA3g9w5oOaFqXOVmLp5r8OkqY/jmWER9
CGeor9GOOZsX/eaTmIONAfebcACmfjEbQul8HCbm59+qnZFtwLlm2GEO/S2JN5gU3iesEAWqRVyu
WMCwKXQTFGCZpUhGUDgYu/CpBjGokmbf195j5gcJyAMvyUHdXIWeEEUkK15r1X1bcT0f+iVOaOOF
IB2Utser8Z+QV5AgLwsy+UJbe9j2BiTMkhwYHwVj1bHF6kcrDIt/KQV5grK9c8AR25q5qKD9qmWP
zd5H+vVHa2PayQg4He2Djy/q9X/vwJ2GtR1lCphN7UC2Jq8/bY4O03YcMKQKWjG9QiudI/K+h/Uh
QbeOsClWbrk5kcUbszOLvqBx6CSR4cWh0V55nFTuhPW7qqNQN4fg9BJMFT4aHX4L3n9tPxSMY6+d
z77PeGvW00wG5Z7VhnQ2Ib/4+dpfFbgj+aCKDojHXbxzAVY/RYEiRx97tNErNE89xKHy4dhR55jt
GBdocRssnF9Oiqh6fJOhQKfodmH25DVXWd9+r+5BuSm7SANSeSsnGmV9fh2AkdnGU+OztYT180CE
UsiB+3o86noyZZyYg7+byCD169mj/N/wkC2P0TF/PFmqBUW7uW9QcAKZKK/7eLiMpC505OZmsJBt
An7J1m46JqrrxQ3oGd9+gBg8wAdg7iHbtELZIe8nl2PHbtBUMHa4JZxHBI8Y3v3R/eXxuHTGYm9p
567US5VA71eaHV6GempGWJw/PSRk/hOu0yGmVR9j4tpJNzKsi/ctWN33da9yckUrtusttaTUrNGA
94+rEwAUNH2XRw3ZL65Oj5EJgNnrMvffPo8e8/8+sBGWqu1WLsp1qFRXeSokEkESTHaFqF/dGGD+
pf0y9XHPZDIB5seI5NK0OMfP9gQ8Ald6GC2wTp8eWRnNoCXck1gdGL+d2OgmtZoPCrgpyInsKjug
wtiOyIWteC6zWqa4swGY4oWdt0rjDe/zKN6krSUABk3xip13o/Ie3weJnrg70h1MkhAW2AhjwVeQ
jeDg1btsHwtb0IqJet/7an49iHD6+qinyTZODolKk9cQ/epeDZsXhVrRvjrv0CXw9itw1nSEKzPD
31HFyfb4t5/JeDo8T/erFiGnwwS/rPWrkT6KSljwY4rI1DNy/NkzedOawIl4S3BnEIICi/bRToL3
po8yawuoD0XEuGOlBu3SyxzxHCJwB6h9D1lK8U94WBNfsPDhA5Z3ZL06iabhNNj+uPUunySgv2/L
PJUqG/CBdO/OmovbpkW4EdsHwcUnvvzl/tYCza5hhjKRxCnBt1/SoRvPEc3H8L6KOaPMELYD0A7W
8/iFJ69OesGBgSe2yC3y93OUY+XYqbMEYMnuMVIFv1FomYvbvcsE7f9I1eLyzZZ+WzkoBKdZJXyf
sr+XW2c7YxKmE9oV8x4/AtXMmAw00m4cU2xkty4vCLxo70t5v+AQs/ykEqXf08cHNYxgHCut+QTf
AfCywi1keBW73vhiNpkOZAmSNooi2cdmFM2hetbK73d2b/8nfmcAW4LTW9sfn4hPLTzLVciusYr/
947vlC3vIt5X7G+NWbrA5ML1dSuCrfnYSE84x4jD8fYDd0R9Kb/5y6+8iWdt+j0hkbpUe9sfNF0T
My45QlfEbWzXuSU7Ft1K0r72M/jrI1cu3CRab5QgDPPNko6tDiujwu2hNF8nf+jLW7aDgvf3O6N8
lRPZ6o4uQJbsF+jtdW8+PUJ4HxGKgaMuGEcoNr0jf9wJRSVG+ZmLebiXtCQzr30q298sj6VaUaXF
gmnI1HWJrrGfjvJW3QM+zvGJ/V52rxjPPvGwunEj+SFrHMkq+SN+6Rcwroo1SPB0msaI2e4DmHU4
bJQ1CB6ubTYdm6jb2BFnHDrL9JdMi39cuaISIeYbG9gBYqc659f6chpNU+wcWP52lPRDqhP8Al/i
/w5oCcX94tp0n0p0EN3Ybeg0W0Zeikro41ytZhrJ+EDi1onHhqspVQZ8wI7Qk2q68kfbeO/G6cbG
8leuTgF+iquZgA/m8clgpQOW6odNoThdRDmrKalzbjSrOXk4FFC1/hTcmV9ZahldYAg7HzBRHaf3
5hLF1mMDGtMeIEdmDjIn6/y7nynBbgzpu7/x4wukjST5YVCyP7FBTvNvuzrmTCNed4FdgeMgAPDo
Ejf1xxSrD87qhgk6J6MEndScdR0ObRRdNb48xbQXovV0s5XWtdnmA5kPLkIgqaIPzZGKEoPGn1WR
vlnmJbpJ4xXQiIyH2EnHD5nD2PSuYxkh3tvAuXP48B2gIvdy4HEJKDvuLHcoarYn0U3yqDS0bDiu
eZOWQfpJC/vvBPtyWLrdLiYaWKDFvrVS/pCp0KRv3KckV/AGAkrVHcXbqvkZn+C+emODBmg2xgSm
P6QY1MOZkRUC75D8eTpzNkZalhcEfIEU5h0ohvJ0qP8uAT3BpQiybRkyD9p8pLXubbCLuWCB3fzG
xecoyMtwS+jGuxxYgbcoMpGcF1hhAqCojPgaEwXH8Si5fvx48fK3qRYEn4hJFsgiwOE1agz/JM9i
4LC0nU19lSWacnvclEJXfJDUL/+msY8z9VrfzbhLfcYz7Fbn4wSy52TQxdVCY8BZuQ18DCIdKC2a
iKKVc+y6BCkdPhp6FRuLMYAGF2zi0/56750N4Ct8Kzw9gWxl3+an3PyZca1CZrwPsarogprk935Y
2UywD14DjHxXY1b1d3+3y6AIi6Jsc0sJx/iAxIjjhKlLTthJ3AgCIbbMejqn25SwZgC5CvWZSUcs
jK7wtDeKGNYampW5ymeEBwVem46FC30t1opI3XSEF41Oy9KXeqoidWKEcHKVS1+JtFH+eFnDEx0E
+jpDBUo3iflQWMulLBWkvy67HJtaQecCIpufHeKTiyako6Ob8adoqHlenMkSNxQgfm8s8c8tzy2M
b7qgITNeoeCiTd2EAXfUu+6ajKmGqiyWmCdYq99CYQ3xi2BdmocMJosYnIxzKcgNQxCIhPcerwPU
JYoa3BuMQOeY23yuiDTezC9OLkniMM16arWUKORcMiA0M0YBMxpN6qkfo3cBV/iYjrAVCG/II1Vr
+piyFJFTddK+oP7qeRaXH3mgSdu0iGxG9B+b/luYtIifeosQbLwEKdTN74bDnIZEGvZCjgrr6sv8
oBZgkKoVZxpZyYPJQbxsB0pnsqSo9tizd9pPDX2NOL8S38P0Be/V+YuXZ89s6PwjWi6PzmNRCDq8
kKh7VSltyG7T/+fTuE2e/hAn2U7QddujM09o+l++DfmjMxfnqCAk3/BuxckeUnA4pIN+gXA0h1Ga
7otUgDVlXlQY3zN+s+NOEMnTcjVPfK2rrbNaMJGDfZvTVdBmkSnAhFAoLneRKrekLd7R8haw62iL
nI5q0piNdRv7onfv6lSoJHsggKyHI7/rp99enZc7nlao0SvryD0gcVKcQUur1BPxK9y5Fpd6X1E6
wzXPluitmzu8esvYD9vDjzftLW0YdCEdnXh+l/JtzR7MPhJi5NvqtfXHXkb81U7fxDo3kqiMf5nP
F7OGnQdUK0pAsFi8+sWrWuia6k159belaBi6r/2i191N+tBnNHSfp5wS+/JUhxabwD9ovxRPUThh
hmMjLDuu3i8OdEimBm4aYtpSO68/mh9OXY8cJs9djzKpVtLMyKpe5P8Ew8NlzWAhGYOxnxLIsywo
Kqe3V8GYCxpgoY5uGdHn9gn2gMPqzy2JjBlUrjE6ZCC5Ev92DSRzFvv4ymsQohSVgmPwvScXyskf
quIvuIkV5YaeAWgT/oBBMiPAeGt8PiitT78J60DmD/yovUynsCjKCyfGB9FNT/B6muVjwcuaWKQn
k1jkkIFW/Y5ofH8/DD+YmUJXmQkMmbdSezQAKvp8wfRj8XcJbWlzi50RvxC/ZTtVZFFJgC1Cd07p
Mfhy1F9yVFDfMgdvcT1k+1YAcUhOYZRLUkBt950oJAVLBwyP38Tx/Cj3mL/G1xMTOJXIILpZSZ6+
RqolZ8H/GuTm3jR9j+e9DNmsd4GWj8rHLipbrzwI6xZm3K5G0qtDvKkRAntR1DsSWkEqJAxAPJS+
ekz/tIzQSVLGGu1S1pBb5y9OUsokvHJQviNsAz+PMHc/V6PgV52EfhUmAfydhAveXQkNnWU+kDSh
egBNVyENCB8ztaVNwJTEdGG8MbQOGBvZ8kGtybZ6EYRAkdt9Sdwo+KLCG6lu5SHAnjwl4D6gsFmy
icc+y47m1qK7WamCmMu4lD0Ls3pU/Q70k6EY/L48gu1O3hLKpa2Gc+bjbYkrMVzuDuKsOqAk67g0
aiObAspp4tyjqC8a5jAAb3kAB3t69wKaOeCx/ugi6DhEY15wi3gnoQotn5u31j4o2lXFr9CclEjY
hCwx1fA7NsFF6WJwi1j9A+HEt3QfQg94sJ6WU4W2u95q9XVlxCPjNVYghX4v8n5hWJH59y8e2wgX
/duUyJRPp/2Sgt2pQkgI/A3SGaCsgY1UVfO2IF9m2KAuL782ow067JzcRHjC7speydZAdyqV/VrG
jNarfRKdf4A/BIKgE3IQ4ercuSo9/SlOcqbF6b5XKDIq6MMehmf1Fo8upieAz4bXZDoZEndsoT8N
Va1emhCMFrsRWrsigcYqlvFRjSxIpFCL8J+dFO7u9QHR4Y2oggiFotXHkBxcAaZ/Vrlws5exo2PW
zjf+gwRCbKSDptZ7eX3OKcq9sXUa/bohXhq2HhluFFLj7WJbl2FDB94qpq+M/wPM4JZsLnSy3h/x
4vKnBzbY/41sBLu6rDUotc8TouaxpigcDamZTy2M6Icaw0oRrISUGbWJeMavTwb6siZoDH9t1IDU
4xuP+W2LXlMApCm2gJReIpFSwkJcnY0gTVFP66sPa2mKPxQXm66EHhi+mpwu/vom+GV+Ng+fguDZ
dgEz+OlARA/8rRednwPjVVX6NZbL4WMGC0ipffsqilWOBN+cAzjQrEsrfzs/Hw0jNoR92irwgOgI
hldLwE0BbGYU+c21tZeXou0k3JqYTd2fLcKDGkZt88rLRmnVCSGnbbRtSIlbp1gXDBPpDn5sHhwz
E58w7QXCExv+uctAtDOa826et4cVSYVThvDH6oxSgy6PRZ+RLa86Bw3dYgbAcgXiRLB6vsGqqPdJ
sPA/ZpT0RhbLQwsBxMVP3OXeOGrimrIy5nyN7Y/b0E/3EWRIatO8lfAmqI0n+D+3UjBBVAlO4qP6
Sde1Uwfu4KA0C2OEhq2/auE9C3ehX3omRy2/bL3fk92tQubyAY/C/4ycixbiqh6RR0hdmxYi0wBo
ZV9oaQnFYi0BuIUIqp0vU556M9kvcJoEemhvQS2n529Y63Hzma/JLHyOpX8THyZs1nvjAEFVRzkB
OKd7j6nj8pKfzvEgze7G89dq1XMNY6Gp846+Yych9OTHaOVQHeNv5uBxIy924ea9GXbNKNZzGQYD
S0Vcs48WzxgilaaYgLglKfnU/xZLCzbTza3L981l4OGvXPMnPcM96zmpZu+CKmZAOfFWTkukcXGV
EyFsOcAmWJlHshc6v4TjrHHTdUAn5blLDTnSoVe0pDULCSontXuhvnSU9YgwqXuyrPAkRF188bfQ
p5ZyvKNx5kxCKWju60R7jobyEiaw9vovfQbCdkk98E7rWK3XjSjgzBBDilic5Fz4qk1EamatVqmy
gsYokpqti2mmYE5e4K+zSQtQmp6WgAHcG4ZotbXHpG5JrXxwFWnKdV+pO3I86lYxrFto0jh+9iRL
ClEc6HFmRMe5yS6Zkbg2ClJ9wBbIvRMxSjlvFEo6JSc8WZd2dnFgsc0i8VKGiskm5I5Pq0F1/qpb
ZTgKTSUKpeNszFO12J2L2z+4unq62eiYzNsUiWZXa3cMNGPk40x0ODJbfUFEocd0aUrGaPlFyi7x
h7ZHwe2Kd26zYRjfPfeJVjzvvNNAzOB62NwWYHV3S3Tz2SK5EUWTbvKUDDbEtUgJafIlUTo/3YDm
mLVkAAWR0GHEFgIjI8oX4TPlrF6GKf9D1jCtxZ2oGHEHEGPuAaQae2+lQdOH/Z20hx3EP6ruCrMf
Kl1i+pnOAOo0k6KXuOb0IL9RGzzdt0VJGEfil/v04+1NTlKurv4jAX7lMIxtseKbWsHE2aLPAlQr
a04XzWHXOnSE+sn8EVFtUB92kL5WNMgnf3fKPP/uMdT/JR6FQz7egimEgMjiTECh5dRjd0j5sGNi
WSINXSz1s5wu4LGNgsGkSBEnQeZ/R0KoOYt4+aT8mLOx3JOm3AY+Ft/DOYlC+hXE27kRf2HD/qfP
bf6OTzBUn/WbAIPNfFNUUqhT3rG6n8J3wm4YaxCVVWblXubgMQVO6yu79LIGpW8dHIbU+ZYlKaZR
f5oI2+baGC8V4S8JhTfv3MDdXS0pEvsya+vgaGtuakZsaitLkpaxS2BDRqlqGEhFO8nDxJbnQsDP
vd9s4a23JOKymQ7WWEqPaeTR4Ku5nTGvwGpeZEFuYrDWlz2AkNcetlDFF2uOBrHcpvmZN2af29wh
vF/ifYm6j/tJ8Tm3NBvTXJtE54SvoDBf0i5YaAnlAZmCspDgDto05+63ct75xnwBbSRxKEmbwO5r
BgHU+1pFR9DAHYVV0WptysRiWruKQmJLOBRRME8lVueIQf1JpGzPX5vGel52JDIkqoOBDMRqYnk1
GkiVXiWj1ouChskVC2j4Mn4RFKZpvtN/EYo00hVYZn94jVGtx12wi7gfJzIUpyX0iOWFpsDyGG0h
XqeTO5TkyMzfGQZ+YbkjStY1zmJz+amwJyAaUfC1XHjEWmDCWNhAIrN6r3Jsb4CkysoWzVMiFsb/
lE1KN3oltmUqokPMhY9OQi1DFKZpykMFgHXT58v4n3O2iyRU4qMDxjAj02+ygUoLvQ8/dqAzdQTx
R1+TcK2iFn2XYSGvD2y6FMxezEW8lWJSzyvv3CfX/Q7oqMOOnJsh38kxbkHofT9SQ1h1gnhRIJWI
yZT9NUw+nfbbtL5QPBbuwfpiW+9T8ObeUS9NS5h37L99NvYOzkLwzByb3kgsOkgKcvz7/7MpBkDb
x2CqiqQ9i5cnqN04YOD3Gn5GWYM5W84VokbSDDCPRZhmk2UFcdtFYxRVH3ppKmAo4VNK1AISqkyc
VWMTV/7iYSbkge/d61RsiIVmvBz0MMSunfR2k9j3hyBnm0srLkAiwVEt/p1Cg6r3BdVuo10s7t+m
2i9f98Q3ih5+FWTZC1W/FqZrO861NTgOA9bLR6iZnttfmW0k9EdI5dF221VF6TYuQRasHaH/XxIe
GJC0CMkUqaYhsQcn2EzWU6tq8JPgyTH+nlnwRUBuZp34XQ4GpUx/NaBpv72dJQGQIH+vQfeylxDR
J1lkjk5+FL+cBrKeb0Qgb8PaQZvA7yeKTyHpIvv1HtxB9HFNLkJawRJIED4poBwzq7AQileeGsYb
UizmT8dPTGVJwFuT0d6GuDeKMXBNw7mn1sYuX/nQblL1saWCRQWFmahUktWFWOITY01/QzkaY7T1
TtNMMlvwLgBxPEKZf6/Yf/RPXfueavXMucw1W57UfSI5ZwaCjd1E7PW3s8wkV17Tr87ff/KT6h2V
/3u26mzi9p/swoYG0FR1K4xOolQcyMDweuWJSjwjl6Rbp7/Nq7iaG29z6Xf/3QLFBUahfhtb+w2m
iAJiltFNtM1V6uF92SFVB7ep9CVNQpqtOYeT1SXGpZgWMQZRciOaF3/mIS3XRHuWb33kwbSMaJ3T
zNrPg+FCG5UxoHfwyrCGVZsyWf+nMT18KRS2UPTO5zr1OPEMk7RrW1csB8hU6JXzWMh6OCiLrl/y
2cGedVwqT56uUhr9Wo1Cntnz73ASjY8bPbY9b4CvnFvBeOD2pNCZLQpZ5p6w+oIXRtdd45uRyFUa
2SAeTKsvAKznwmzNiYtBIv2aB81Pnbf6BpG+bFrogKeh4E/riCnxIg6WT3UhER3oSCZZIvUNjqIJ
fpewzqH0YR7B6LZOFiffZNDQL4S2m7Ez/9mljRetEDYaECwg0Jm3h2YsLWDwrySS3t6x95ZS6/SW
yTxtje6ZpYjPMsV8qtLXV4o0leaPq7LyFSlvDtS9FaZonXJgCZzWGcRFKWWMU/+si9kdyAEX/mo4
YM2naMwS8WmXXn4rH9CFVFYgRczQHyKuAz0Y2mEWh5RPfmiLhoNdvXeD+zfv2AupHM/vpdPfG4D/
3pWmW14uI8KVui290o1ilQhSZgdL7u4p5Bsv6RE6AEvxcwxKAD8OWtroYo5OULisWL69s2On042d
MCFOQQ30yXjUfbZ9QE8XF+MmwuFzkqI6jDfwV1j1sxAAYublwav43ZK+JV0bhTa+Zez8bi/5kZBf
LewDV38q4YV06ACag/LH8qghyBBx8h+eq9oUaxtRS9fORiOTTw4z3pgj2Ni8jksLzq/9pEDh7VqF
e/HwT+eVQ/A4w5T1r3Td30fCe4CDCdgsqi2mnqXGUIHVNQjU/9WIaQ9+P+3W5wflXzpUyVb6usQn
40eGwzn1B3nYPKnFd1iFOYb0Tqwu3KmBL0pj7I2yeTYwcfAoKa5u/BmCRpwhchG/MsQxnXxUXRHC
10ZKdYi9irbOq5WEXa5WEx9D6/ozEjNUv8+cr1L3tuCvC9WdCzF4A/x4I9+RZnT1K50Z6tLfevay
sCWOlz+wJAfMKLbPXbUraAS5JyxfaLknr1vAtiLftiKlpNkT3UEi4nzNXD5e7jQ/lNiNRERs3eGV
e5sl8sPAvMSeMewINOGzW6CrI5K/7jXe2pvVH80BXT1pJvqj/vL/Ocd3JzS8xiIU6frtrI8EnI7l
EZ/FEJA+5IBQcgHATwfW+3sqUbT73MB3oUV9w9HzDkmyiChP+w/T7BwHqvpBoHqRSmsjrg3NdoUx
XPp0VLMWnhx+yGCFunQ8zUo61Lq9nFrUH1LaMy86x1CqIes/dmf8+YqZeKnB7qSPwIRh6WPB64xm
tZHpDig/7aHArv7ifzNpPOWc5WLVNj65a3StdEAgJFtYxSim9a8HRQEcWzgKvZY0nYueCVZgRiTj
GKqkuYMA8W5NQ0nOFzGNo4VkYI7X/UrD64nmx5aJTZjRbSdBjIu0jilti9aS22txG3KsPBvZPGK1
0jIPcYNDL9CVa1034fsVfkAWjoH1oU+SJmImZC3mAUDcog+iPWYoKJHoCRYSZdBUMBtM/SPGwZQ1
zeyMiGyGi368H2plQh1F8+ySogS3wn5B943NnFx9iOuw3M+dHh1nZ4NUnd/qxle2yUau1FD9Ym+y
kVJhtZXHnGyodpu8GqdoC6lzzhn0pmCa302R+dmKPANf4I8CdjTPDA1dhoOzhqLPcEwWlFA57ahw
GolvsVH//twGE/f3Dgeq+w4LoR+eiD8UkuXjy/o+z2xIefG2N3e5efZZy/ziNI3NQ+/SVjWESulm
boEUunm0CM6S/NHIs4G8kxjW7v7hjVoV6y5Nt0rNQrxJXpsZA7HQfeUn8UqqVKBjxMzlbTb+AbVK
2uXMbngPHwUEV0X1pXzvIrQRk0siigPwaxBeLOLCxs0iy+nW3I6mr96xbtdFZVhLGGXa6b3VULvU
jY9H6PIUKxjw+Z0e3qJ+9ojbNwsRJe89Rr130t0HmqJ/2l2N/XcXRtAGh4glQRvURTmWh6pCJBul
pIY1O3Yy0QwERl9YOYDdOsgqEvKEBkO51UCh96bAbzrhJqEQ5+MCKGC9caneHsXWPB8Vfc4xR6wb
6ZWPs0gqsT/UCZzqQ17ON4Sk7fzgefOhD74f0UQrJ6iMGUANFzia4CADXPav3MRnVGbNEu3qkr9N
JGWM8F57ToCtnl/FErkGCC+BgaRzRDdd0Qx6K6MQhAUmoUMVFzzwkbNJwRho5cYjnpXpOmxgRUxD
vqLhDQL/OA77RG2LsyMmRW5kBG5v9qUdY3QEO311zlL6pB7g1dGl3gFHWosOig/3/hQ/tw/NtLr3
jyvOQSZ0e33p15GHBFhYC+dgR9CPvAcpqSFvpmzPo1hr1AsF8rmXD0u+WfykS7G/yqpcVa1cHvAH
PSJEdixUmDT1An4ISf7doORtNXUrAy0j7Oc/OmLE4f1+d++y+Hd3dr/7Jw4kx8LSLF1ub+xx4TmS
iZVaffk/e7oh9t8xGQ3XyLYpG5GO6KHEnGNFUsUe5BMw/KubuzAZeChp+UNt/U0galFNVnN4O3Xd
KpJxHIGyZHMmpvjRDphwD2aXyoDxJwBjw/dEb8fNAijiW1vG4T7w45ODqYv0bcP1aa8AYBM7p8aB
IPcnpUQPAZZ8ZCWObBmO/6VgVDLHmSRNHt14vfica+eeUXIYf+OdE/AGBFQEXBzEUSCxfES5794m
UdBOJkkwdzL3rmRVJL/b2EiLEMmbHBruhTqC6UVhbQkD81WdKkcciiFK3G+PWibV3MKPAORmfrDA
GUM8nYuMTFVPvjzn6We2BlWUauortC8vIOPZdrjcZYMaI7KPKoUubhDfs4VcxjjDGglmlCWsVQa4
dL4Lg/qwikxffOpoBKCelloSryyPKwBF297V+92wlR3rU45AmP4nehn2Rb1vQU4EKpacEZQveC1j
QD7e4AoP7kGF57PtJbSkVnm3ze/G9OLJ1WlFS2MoDYAuHQiPz+4CMMP7I/XWQZ5g3IqqgJaqkNGb
sNHQpXzjflWpesrm7zFhFCMCmEhyw5WUT1wRtDG1Flizq0tWHWxixQ7nlvMxViDx9R/wRoMNXEgm
zwHfLYPsGwDTkMIvPrjGk9vO4qwSLpS1CF9T6TiHFk9tabMzTGcgoMSlg3rCgOeIZ65wkK8IJn6+
8pDWlYvSgP4XrtV7zSHD3hUbsOQUxCT1dPyKgRl2a5cVmDbsmPi9S9oSgfY8QdsEwUWnOpfbJZzK
U1761mm3r/GxQkbk6J424ugxu30jqfViCTjSUdHz0NqJg6z1AD/gF84td5SaDB9cNkKcq/qutnGL
7/tPXiyIL3Nk6T21xZSA/3wF+vDWrldbYDjPWvaYL7L1yamhB2JpqLhbusktoakQV0FMFwC8L5tu
3d/AohueFXF8jHjJMFyGraB65InSXKpl865idoO9hPesbvBIzdC93tkJCurp52GbFb4bEgy6fGOA
RmZIO/fMOvHsn7M9bD2iYOC9kF/Okzr9jE8nI2NbfHY9t6U5M/IGzCZ6jA6jC+XsIkqQD/gGN63n
/4zMyKKDuEdmEa6mbCWoC7EpLi+P9UtV3dYbPBeHhBrQ4V/+1N/9D/KzSIbcpF7IgQaMjf+yXiPD
JAaGKqDIBCyy+zk7gXuUrMwaU59CxXASV1oclQfiJIUB6RSHd0jY2SutxrMA8nMOoirl4c8lHR9s
l1L8qn+1KwQAxLWKIRdULctr/on5y4Jl3NhaVpRSD7S76c7KAuPzTyHlRPwWVoQAacPLZ2niGeLp
LrrNNJaQrHqmylxcUwEm/Zv90ccLHjyj0ZqucQg9QfAg/EgweUamMSVZKdTAVeHSZrylbng3aZcM
qXcPhZ7qTQj+BdoN8yfR+6lPMn9qI4ncmOU6bb8nu3orQxDeNTtF9yBDHNjp2OOr8jdT9fVO3gTv
Kpi4di7D7PUCzj7j6MWPFM6r3vOjiGeKyK0wkV0NgAeu9N1589iBSWwot5CQNJNjm+j2MwwzwAeR
1BU9duqO0chaiXItgd9y3LF/4jzQ6fyAQFJctK0hXvhWz50LrjsPQtcovdRapGVxo5QeLrtmKUyT
nIKNcQqrEnoEIOtmfyKzvjyvomqazs8ZD89Dp5nKQ0jSx3/C07dBBgQLxcIbNFCMB/tq43/Trfjh
j9VTGQ0/quvhjuwdLtKwiok3WSNF+b5LSYhF0w62urVQHBv+uGv8XW3kLM7tpjpCF8glIB4kAK0x
HHWXnUkWNVC24qvwfpWFIGMrNUr5f3klA/WIxIWr2ahSAW5qmgiaM2JNUeZzCLx8YxyMkfnyBmFq
rL90cOG6JK6dNS37qg60e2QWqBCDHB+D24hMKswXyfhUHf2uFxuFph3ngGSR0jdJz25N5BSwd55G
55+4NKpwUvvFc0rW+FuiyBBx8V7EcXFMLj+4ZrOnhP7UzCvru+qUKILTZ4X/RgRWPeT/J2imS3HN
sR3ycnmWnfxLzKG5zOveRhtNU4GBIZJJh2nUFLGvFdSgfZqifVr35ODlItIuL1py9ouijWRA+5wB
QeRq7v/YghvHoPFnEhfgzMxwNpmRar+t+XePg8AqAXBeLXk0sNv6u0Ju9UIoDx54buv7gb6xuY+1
w76xYpJenZMVoE2It3oNi+5WIXZpUlagS/BSRFKQvT6rNy+UUylMkMma24QLtqLvdiwBrqvflcH5
1VFv9pOrHDsEna07UYUI1WeOgyGNmsmYA1sHLumdNxOkPfAvB6d5spBqN/S0jlE/sn53PYr6HQbV
KZadE/6kg8X70csGiLQXX8eMcMx9KRL+TSmaVrKIeFVMFhw8gtXWuPpY/UEKnXk97pYigaOPck+U
dTwn/QlGrKUTLS4kq64vFvo89SnSBJlK/LHfpbfxxmoQfAg6EijWNdaVQYhoUyuZARgQFmQG7lbv
Jf0gF01afB/GcYDmvn77m2+FSFBFNy6/Bl9OURBA7fz3yfCVMQkRbHldLc5C4foV4AyS2dg6q7Qc
owucPJSyp5noXagO4HkE/XrBqWgNzOF6fBibgzqxIwMLG4zk8WLHfiI5zuSFjWUcrqTVCI9wAiC1
2PyAOyyPryDRCc7FiD+2Qos3btDWZUCFEk9tB4GigeKfJZmIWEoLv7FKjkGaJcm2NUleYFDXB4SN
NEdoTD0AfB2Q0aefXyDqQgEqYC42xHXY7JaZuPaZjZWur86xXYbGGsSy6DZwc37kq89Ed8460Gph
JhBQgZFZci8x4jbM68kfneGBD6d6cPZsAwRBYHDAn2O8bnzFM36gSJM2uwwG14qerKkkIhyHBoPR
JRNfMU9nQ8k93f7nwVVqNh1vtP0LxV5+cIqN7AuxSlviZzA0C2ivAyIC0+LWBqHbbWXvbLRagjRm
oX5mi96W6ReWrI/4yTCCaBc9LRBqMCGltPBLONYfecVGey7AYaXKfq0SD4e21tkOjPN44kVffXbr
E57BRJ2qicXu/+e+inVZLnydNrmBFlpAhe4TV7qZVpIFhPavO/fnDU18AHM+8XYz6ZeOQeYbvI0l
QjXOhqRdGLw6s/XvhXpgKvmeboEB1BEgFZDAoD7UWN8H+HDNzkoKNu52u9LuKmRE0z915M9kMLy+
SQVFfjsD9XylcDd1fms43zGzSIeZHrdO09LfEi2z/uraxcxGHc1nHeCY2akYcsdiEK3PXi4wDZsw
PcP0Tq+1Vz/R4sTHmkoKObFeXKv2aGksqidmjfSB7RfTKqhLRmekXIVi9L/ihrTkQOqzP7stgXPN
YgM/fuj9MkQSIX6cUPwzzitgCuLU7UJa3JOGPZf4Icin9OEJW4Ij+xJRP7DJd0L6QcSnemEv1zWQ
JrvyE5oTRAFdu+HHBx13e4TepFkKsZU+oEsfVk8fcXlrAYJL1w279RT+xZN/U7gJu3l/hV4+sdep
ra0IoGuqvA7t8KjIdjRmbd96yaH5jFVq6LLHyqnoWbkbUyanvs70OyONPLwndmqxpgCcbSttg/li
Br8lDCk5gEerNzKYYWP/TF2b/rXtkUJCNGAd40ThhcNgjZfse7pV/YmFAZtIkKNTRKzxVOdysS34
TaimgPpa2aSS8aDyUeM3FCZB29rmgDvkMKuOOwEk7tyC9T78O3uiw3KUVfetNrupaufhuFTxPI4v
AGhI0H4BMmvZrN8uqF0TSKh2GdIup3AoDr5KwwGpczJAjzC5ahNRiVAbPXcnw/TkGNw96JQfh6Sd
qWlwC4Ndw3N6H1RoIVQbue3H5BsSYupPGD+tBtJ9xxoqRkuvhPxPF8iRT2JKDFdeqDfZxzNbB0mu
k5zmBB9b+kG2q9MXuqcEmYuuii1wIZCurkJvDykWVHXGLL6B8MD5Hg+huDKW0ZbBZoYyZ2233Sib
v9OkA4rEW9OPXRb5z5lEOtDWi+obkVSLFOG6pbCmPHr+3A9khWCh4LTi1Iz36j6UNqaCJTT1LF/P
gekRqX8q4rw2BOCQU6raprSTvtQUUKAecgZhO4/bccw+4qYp9e//V2I5ilVUC88xVdq6ayqoRGY3
d45Nqb7PBUwwFXL8CugRgQCE0rpaYvvSdyHr9i6NSedBEHaw0b9adRj1T4K/xp3ZJp2M0g8OevST
5X6U/lug729ab5Cdy8Oa2eYJORjdynaFEd+E5nKBLbQ1utShLoX4lzpQaW7aYmUmP4pHg1TmTUxD
YwHI9Yr+gZo977iSRSEdl9VDyl4YrGqyp5hTgJ0B/MkQkULMfHlQRxwaEGrwnTQ9Jf5BYE9RwEQ7
jcg4Or6n84Tv8LgyW/mkk3P8cxH8VwWx6zfTpBryay5jRIYn36swwk244Euz4fOugM+atgpRzWcY
6crSbmc23atAVRcB1TqQmvyviEvc14jCt5XxeIWPp80QRZfthCWNd7tdxeU3oQWbQtIgX3HASzb8
T0TeZfs7al1Nvll1B26/iK1VW3XU51uXKlVJHjudZOXpk/1He02b9Ujr6sSZGAGIFVwEfiQ6n2ak
RyVPWfCSrwnSIHGUQippUnF8VRxmtfIh3MPZ68yknKukx4v74UggkW/bVdiUsdTrzAkginJLFpq+
I5bFwD7xBjT0aOvov3utzm7kvtaXkQrtsgGdUuCJRWNOIBvdZYYU1tWsljkJDCTcTrifsK6+Vd36
m0Y9R2c0seZFYUVgaackn8tWflQN/dtKXl1Kib9v6I02uImBKcnVt+CLLWD3kHJUsQvveCqJ8Q==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_16_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_16_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_16_0 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_16_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_16_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_16_0;

architecture STRUCTURE of design_3_c_shift_ram_16_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_16_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
