// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_18_1 -prefix
//               design_3_c_shift_ram_18_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_18_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_18_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_18_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_18_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
sWoOKI4Qx1LbE+zJIJlD/7v+05GvDaNnLlex5WZa6MMyZKr2VF3MmTqgWKxzMXwyOa7VYHG1rdim
aMH7sdWJiH1kWzP12vfvboNywbGQmaJIHF5C5vLBNOgu1Mi02D2hVDzDJPmgZZMfuC2C3FPWNdIV
3zupHDJTMU9eywHDes78PaFA7pcO9z8aKdISgktDxj18jFDjkS1nL+T0DWWM9L/73jGRnh5ifjZt
20wk4ubKl5U5NzeudPUaKGUOGem4NZjcKIYOmtYuCUcPw4fiPsmcainaLBTK97tdeFG8+UgHQZiT
mP8Pk/DAbujCxq+A7WPTzTUI1rObcGaw6POm+yIDVtc7eL5lOli1sUhYIpeSgHbqhUxVdbNGcB4a
8QktW1M0FaqJn3ruSVDfL2hCWwsbmbH0Vgas3qU+cdsChK8cgrxaqNYfwm7ZwOrVvIcicU00nS12
2F7YEpgtbHbi6mYUqYuvirHCds5TG9oVczViQSy3kHt9s9kJTrC4WzxrEsP4BtKL8NZDY/Z+7VW6
8DE0WH3r/HLgW63KDTar1Ud+SIdb5hoTLX3PQN0KLB2glxpZRcofMJx0I6TbSc02m9FScMLiFGMM
bnNc33L76GXuqi70y3QBDMaUuJ7Ohwvb1Y6wKQcw9yMv+N8zQCp7VTGb+5r0/tgGIlkCxrUCctSp
nFsZMSMcfA0UPvsxhsLvTyaS+tqXd8LBKv4YEHstju28SWdodM3s0lyy9BphclCvNoPw26wCelmH
yRN34bB6bhsGqmTYnE0P2bbbTYN7zgWKTFtpIw39IbAyhu6yy40UEQkIVPbcj962J41wUJc6KuMn
SZJnN1LXUB3SMNu14GcqqST6XEGq2vpbZtMn1u4C1mh75+BjeySkCGF8BPpwpH7pyGWiWCA617sf
5yJekaf1aVDk8gnH759NfZ4mfXJqphL4Z5AFIYlHE/kdvAoL4rhTpXtbZFx6akNV1LIny24NBhj+
OhcdnruZpVoVLcpiHRfWbZVPULTWal7IBO0p5XVPLgKLLTZOWm+eofiWZWHQnpHi1FPM+LLNE1M/
HH1BZiNkhgBQeISwjHg3mwFaxRvRMKGsfkIv4huOZ0K3b9QjR8fTm5AVfNC0wmW0e+TqcnKl7FX2
DJE4ZD2WVW8k/7BTqc8Ss6w1Rpi4ZRAXqf+VwoHL4XRZZDmh6pTlDa7QMraVb06ZE//2pPB/hu9B
9VrUfvR6Fggo6MvE9z10VqQfBJmFptA5mhmR/10M1nMGGn4AFzS3A+zvrbmz87RZvvfLZs4w+vW4
Rzb8QlQVB8fw20+q/hyxt6I4naGGjtPeIM4RE0U/j5il6cJ25KMlaVJQlHsr8YMFuzqANOup70F6
C5xRs8CIk3HWaY151hh3fbVlpzHnx3x+qh2+iAW1F+WyfCiGQhdl5+8VG1cLpT3gbaIJqIa1rTin
+wxGRvxxwXrPwjD0Ba7Eupd7I3gHjpbs0v1PV3Sq+jv2CMBws15obDv1GcaMhBqgY+MYYqL1m7OO
xVhxHtJDwBHkt28rBpoV81YK1TrPdasnWflKJuZfitTQu1cY+gERVwsM5ZCl0sGGfOnQrY/yj1Ne
0XYyfZjgdc9F9wC56crp5HRk806pviISNqA1uf21o/dkUUXuQYJxWS/q9tYckMxFgbdbDIj5yDH/
+T7/emLSf2WYpodBU4mB8B2QoFRyH16iYJpl7bhzwp4jFJoD6aRHbu+O/lmSDHpVT9VmtBecgvyU
4LT4uD2cNliosuy1CkehoMPFwd937w2wz0kt1ssMsGl4utUVzYc9pRlk7cMv5t7o6JOPyTaAxjUB
FpDwjH/k7oUHNvoLtCMbXdZyqSC/FjuD9MZGMlO57BXhll3/CBVES2lj/E9BfaOG5u8Zhg5KeAPP
xLnT0PiqnpHrO9xdddWCZGrgKGO8b2caxeA6hOy1bJMFxTIzYmWg7u8rhtU+H/02lkTXD+MXqZ0G
rlvHDxmObVOug4/95cMOaouZ5VdfmDjszdiQolKPUIn4/LTv4sy9XB7qZuMvFMRnEuWLbX4HftA6
mi7NVbGosiSj/vg0cVjr7rrt1hHWN/5P0h/eH8dtTXXAk5d/9bqlxjbJ7qHCS2qm5mG1ekiiVaBm
SSKRTKm/SNY2AuA7MBr6jt8fxoQuMNlCeA81/1EVM34YYm5vDDjQ1b1caqfoFLq0gZ26wBU0CvSD
P/fto1YdedJaiTwxFWwsiSGayUSemlh6dowyyfG0Dccvvyc0kWlrCK20lD7fCPoTgdv4e1FDjPia
my64kM3k2AxWb/q2bD7nnIi5c5pUhQnoqYprg87favJzFKRx/U/jajCXDKMeZLzc0VKpJT0wGXnW
aiCFIVAUCgK0P/I7yIqrvZ8wu2a5WySwSi9CH8zezKWF3VgMp7cTYujipRgE/vKw7QGqMyw9lJnk
78dbm8H13bKEszqpFWIQ+5uPkaqprLMckrn6+/QsRhMV68AtuUq6Ca8QM+9Oji8IZfuWzMFIaIDV
s2oZWhwWaOfYQ0n4ULlicilhPL85KhAJVmZkwhdIE0SIWoya4gQ16/Ul5qcpxP66d4ThjgMS8kzb
lqYwAn4YsMOYooSfXabHo75vzlRUP96/Kv5JHKvQ0Qq5sRKK9ovt+805TEbNuPeaGZwyEh0CRtPQ
2+d67UprpS271M9JwzWqVf3ZOeGa+bVLslqQNSW1a41oQ42JVhGHXlXw2y+TiMEHaynmPX6WbYzV
nMKbdcCy/cYA16r6VZHE8QiwoAFSpMmWizU6Wc6iOKd/N7o195rjBbRiEmZKPNb/KpvfqSEIGO1E
23/AhiB9spMhQwbgoIo9GoESoOsZ031UKIocF/XioXBSx3mVr3jUCU021+KjKcIBFprDobERBOUa
btOOWiqg9NQlx+3IKcCQpIXtVGDca/labX6mJxEUHhWqRKWDf3lJPdiMGWePFpiZ4uvoCvTqkeQa
dh6hz3M3AgC+d2Uw96ANAXjZ75X4qpKCYBK0iObVvbhatS9XtozeMangUJzPYL5xExKc6T9M3diN
V4ZxfFS6On0CtiMztJ0FkXUx80s8rTiBiFhterEr2ThGjDTy9sZQwPyq37nRUq40znp2zgFpA2S/
ZgKV03PibPRv72oja58O5t13d0oZ8ef4LmXG0Fk6UGFAivXRGfPCDRWXKDG+jJKY4Z1QpVaY8I7B
eTdtmtmcnO9Ge5N0uWpBwBc4z8T3FDRFx4MVp4j2yDMya5zLJvbHuhl3XWSLwLe0vAyl5suefkKb
HtT0GvR3p9/GZg66XxmOeHiAUoCCJdekEsG3kEpCTskm6f+jEsbNs7HWXIJ/o3Yt1WrRhd5ZA0S0
nfGXZ/owZE/9iHFQ8sKGcWKypoS/igWEkUxdMCyOTVxBdK/01e7SGxrBrNwb7jEccUUTYPX4M9sd
nmlEPIwd1nOhNtvdvkWL0AOLt5jg9Un33R5DQ3epT6VcqecMuh0XnoVCsPw3zcLMlZEk4DLo4OqI
X66BSJaBZlwpELJi0BRkGTZiV/c6RAwif+9MaoC15MqWTw59PQOiZRKFhS5xmu8BqGGjP2Y3xTPT
bxW8YkPjCQoR5lmBdG/LY6/CvlG5NDQrZWErH5J0PHqRyGczAR+gLhTdEpOUcNkaFscq88qk6bdq
GP1d4VWdKthxbc0XKNczE8SCW6HgsqOcm28Xv9cqmvChHKvUR6et6RxmY58xz7vv9rJ/m7T6pc52
0zKrKQL+cbJW5bg8Cd3IhDbWzaPR0eukT5Z1dt7NZMNtcwxRVNjx6OVe1Zl7044fVOU11GRs0k8g
p3irVjZYR5LWDKvsQUBpEoiJ+W2T0QW/b00wtziEyXho9va3C1UmnyYn1FT2wOn/lRSbTOt5hi7z
k/1SQPlkneEKy2D07vCIlIlYMZAdUAZ2pycqU5wKTCIvWgCfVC7dGO6LM9SsCPqzH3ybrIVr3F0G
+xffVXOP3A7iHaORUehHq5Wt1wxMAlYVga1pesujGU1xxgcn1FWrPcbQjFdpyNXvwxoVMdTG7Ww2
CCREUI2AHbpVco5N3Yq0A7oOZQ6JeYuypvh32M22YjYflgPV7tKnArvJkMRjWjzqX0axTnApKJdE
AXdT9Rgf0K59pBFB+zVpB0YIxWmCrKFWBL8Vi2dADQuOJ8rMofg1CnuUGnmZrOj9ZmlnVXra0nTG
Hoin6+airR0gaPVlLeX4+JDzGNafF4cNyR/+Kz0v7UgOTp4E4tliNAqT1sNPzhEhvRtGcwc9FH7t
xZ4w+CP3GxxtfyuP+aezL5ESCUwLIblrr70Wp85dwiBi91Dw3TAQYnaqW96sQ0vwt5E/WOfZNusQ
/REmqpbaZlKWxQqKxgfpk8XNgD93m1Agw98JiNZ1gHe6ReTTCtH1xLObn5epMu/56NfWszdFJ/hN
nvaOK80qGBeL6tpWzM9MCecT64PzYz8WtNnyVK/x5Be6LzVNHnxVd4RcloM9UATItSbIe3fsYTJN
JeAU0w50bSP9t+GsABV/PakW3g+2ZQdjkcfQjNO6CxeiJQ4VVG3Z3Gyo1LlxIsTGX7ZC111blt/4
aRqQkseWV4bujficVQe1VuLjjSaDAT23roAGl1XAQf2UBgKPkQMG+MA95dFbnzmXoj4beZsdE174
yRw/7tAe+mnFvkU3egxLNk7scBV7TZhFcSn7dXHSg4vjrRzsbzHcFb4OWUIRoXjuqrbPwIKH6vab
WSICZv3aUpOiAglt9grrNXQYNu5cFM1YDHPwof8ZYOyQS0w5PjxztjTnbadk0Z+d9N6Kmt+OpdXw
S83vi82Y6ULOpSZvhMMRIYKtQncbd64T6Afldj0JleMbxhgrlD9hqJSH+itAG4wikkbZ/nMTMGNg
2GU4bRqNINqqz1g4B+I9II8j4u2Wzi6JKXpWe/zlAzd5ZXH2WP23wiYDYZIM6HpOx05yLz5hf9O+
LPYgtgJJRFxZ1mUqaDVnVPqw0SAANpjbXklrpCgbmqEtQ9l7LNlGU7EzsJsgcz+ngWWnu1WHYl1X
4OriqnaQXBk2I9RivlN0WLRtewYZTuqVIr9Lhzs+U65Nnvb3czdWJc1Ui0XGvWdUsfgm1hP1uoeQ
/fO7YUsvFMVib3nnxit4efQE51gUXcSme9ERnIgOL6unhMa1hl6qW1cZ1pS5Edx6cS20W34oFz03
KV4mnZagXocVkc1FnJOEIt9+6j4VZ1CBKGnkQq9VgIXgfNhXJFBcSELH3D5p9RR0PSnZ7STH+0RU
Lg6yXUdjLlCQhbAzQwF9eex6EZBacFe5LOdTO6Y93OY9Ld4J37rFLR2cRy4a7v074yma/m+SgYM0
TnGp3ylaEY7O7wNo0roN2f3/qGgO7vGhPrl6o7M44P23KaJmbGqzQlEry0CN02ocGADZ7ezfKZ6A
qmC3iYSmv7PoreROqx16t76WNKI84Zzh53jhOp9SCMXYPzukLQZvy4tScgZzzNLwb0EV1mwnFg7R
9s0EMyWg0yKFCdVU5vG2sDx6OZ5OFYBpED5PwBndm9VjQBuxWnD8r8ifHJap/kmVKK9BZIxT7lvb
Q/WY1GtjOKChKmv7+6djqGYr+e6VV0O1Wsl9cJZCBhhllUsiLBIr1yGgcyrUMcJaRwRaZt+FtYkT
JiKHmuqYNqRjPlveezzx8NfJWGudzDHG+iKjBHDL1W3RIpWOvZx2mN/LzgpupQT5Dft4tNIqku9C
dQ+f4JfBLHxc86Xa1ehmpv4OlVnpFF9UhA7Dl5FBVQ0O29lKFS3vCRloPL/RCvsSB8RvpFc6UXgh
+ZeYobKX4vd9SwSNm2a77cbDBWLxhE4216Xaf9LYTQZ5r0LG8sDKXOnfl3UNP5GaB3EvfGe6HvVe
6W7Egf3ub5JeIOkZAqYcVYn1lV7KjN/kx4Ppb/tuQZ32sji0zz202oc/UWuoLZOzsw2uwbYaiDUH
i72xigTdMNn+I32QaXif6xOVX5yl1k0lnN/px0dVwRjCpbbaS3BnHlSbr0ZcMzMGfIV3cmtwKOhI
w7LsJX/dpTZK2mFb+SSHFU+bdOvRzuojYde9opVPy0osgGzEs1EC9yM8P8r4tgXi4/A0o5YEP2q8
YnVZh3AntKHDr1D1MCI9uKwnzNKMBEBAo5EwHCk0plaiAse4e98fHS8YpLIG7B0Y41MRJQWMX9kn
+rqza2As2f1Qh6w8I74F8RxyaL5CRVizX5TDqRAB9nvhv/ueeq3BhhpN7ZA6Ju3s28Rt5amoQPID
hnsewE67cLQ9VXVOQ8kOcd4wMMpihRL2B2fXqQpTAc4odY69Gmwlaq1FOu6oo8RnZH3+NIa4rafn
cIkdUXcVQekAaOlJ8V7hIBO6jrV3bf0Eq33xm+aNpCVuB3F4pt01CANcjKG5qiNBjAs2pfTMYS2k
ttUNqU7AqzuwBNChQ5OxHES3iD/KYLD1/UQiqIrHKetgTUP8d57VqvCQ5oY1OVbRVpRkYDNc95iA
IWuhjys8BaWmmU7z773B+KLEEaQSe9uQN2sss3qNhtI6HpKlDqam4dwP4Iie9FahZPnrvelfeqDh
Tx7ci9h2yO/2DMzrWX71R0OXeqqrEuM14aNBVLJASa8wyyPOu/jx5Odo+l++uz6UA5ZDtC3oczt5
fqVp7bfa5VB3jfczhtVG1p23L5dB+s4u+tvHCZ4WBPYyw1wXlX317vGzmqsl1O4tkeKdObHD1C50
mm79gkXmSGekavA0/cyQ95FZHBsmZcXFJJrvhXTtAZ4wUr99loL4L1KfuFglYwlOvGEKENJVIFxB
SKysYAp1qAHD6xg9JndvNZnRNEcMckX5b2cIE4l0B4WatasR2Kh2BMic/azoftmEgUoFMgOweqUa
UATm/Sr1NHWk8u74S1I6hKBLFz6P9VFAOLRdxcCzjyUe3mYh0BbtgkhF27kUuQ65lvOYq1LBiVcm
oNwLil81Qf4rxo9DqMhLSfLBAgXXx+oiqvW4Wm0ZsPeJA6GQIbUET0ElNgsDqkxeHsOx5vFVnjtB
ptvWRGji0+yAMjPL7YcSHeNiNqM5dW8N0ERhgKBPuBPC/i2DdD8HLvJMZfmViw50fcAi2IP8l8Ho
1aAtddv/GYYrn0scc6wmNC/ZpFNze8JCvtHUHXrl5wPnq+GCh/e0dYhzSx8ZP6xCBbzqXQCrEu23
+iw7Yeymg4k/HSvUppRsVi9l8U+2F0d7j216ND5VD19OunISBNbLBdVGB1HcJRbvejbrVNtv0iB0
6drxvdkuY6DFfFMtiNN/y1iwDtmTKu17a1Ze7K6gunoHQDeCjmlQwXt4NZkHtIt/p3t0Ug/OM+Ml
Cimn0vJatPVpXDqpU3BaS52OUerMhAe9xJPU1gY0h7kzt3dFoB22vK8DZ23hL8K76f1wRqwe8FvZ
rVCuHuZc7GtoxKIzxneqPPJijfiUmBgjmAK53WQ2u2vSzc83Lbzm1BqNz+myCe30uwrwvCq+wN1P
ty+46jvLXPRa8uOm5fsve6MkK9F2L4AjU1BW80vtgV0uHRm0D4s1zhYkfSzUy/U31D1vL9/d97lX
Jdgf9CigoGDIvCTV/O+DJmxX0psFRJyVXJamh9FTy+wYaokd+tROYvveHfNTtvetXMxlq/OjCxtm
+paQXj+yXsCr8ZvSS0ZZbPvUQAwxz1jWPFKOTNYBAlAT1bJPKNLuXLSB/ZfWM9TIJQUjsqze341+
19z6uiRGLQiYg8iAnQumWtNBw83MibtRjyS+UCn1uM+LqqOEB4io9AAx2YfbazFCS2/qojERSq15
NeDHw2OrSTelSFAzNxti2rgeE+3O9tkrdfPrkuFgvRBwkrS1sUOw6tNymWqWaH2ic0Aq172Aynur
8Pa8hBd8NNBrNHFBBCZgMrxAS+Qi3dE0vcCU56sup06BWAWBmpe+Wwr4nzVK/4DUFHVDtgPN+s3Z
i0weTPtiJOaaI5UqFwC64LQaXnHGeCkbZLZgOAokKCYR2euzlrJa3CvRkiIhXGV0SbefTC2MWVMA
Pv5fTIyFOxz1UCfEVE6trV2Nh0DT/vhX6Ea1t7tdEe+1LOZYFzVEwS+YYEYxrOktsAOFzikXvWVo
+q+hjRdjPRcQZ6fridAZVDfwmy5dgTCiaK2b2a+wDWG4uyVP1VQ77hKsrXQn6ODV5bh+KfUlELZy
nKtzBzxEaOYEGgjNMSvKQUGNy6PFjRilHGLDjbN86mHlMop3oFgLHjDIErP8Bvm1SQ3QWXhclbrM
zdpfLQNIU8Fp9hK/fME0YNjYO0XwxBKfAAkpXvBufd7JPxQkgfMgwAhtuXUMeeii5IXhAIk7oTYx
QyuCoAUtEreUJZTr4HjjlwJqxf706CyFVtGHFkq/mvLywul9iQA91ZJHb2zEZcl4J0+vTMPseOY/
hfH4g/KecVUzQi3TkMUMLntQHElzl+Uau9DWWca5pjEX7LothwPSn3jqpNBVsgnAeOmDOKcUSfTd
L147HSVQ7x5f48Mg8cCYw1UVT38pZQ1ErVvdyMEq7JnQHz/S54siJlMnuAFG9YXUL4Qyud0uADLM
aLGnsJ3RhaTwuMRtnHnlclSJaKWFnvPUc7Rm8/qutoN/2nl5e6gpcX7q8gE62Q7WIeSjY58u3iwB
Pfx1pSSmn9bJKPRwADcFSIxMWzY3RLYV+wD8Le7yv+6EzdXvjHf8RjOksnzNzSeg0CnR2tJQfS1V
JrFlEec/cqG427bhyUitLYt3KN2spLW+ZfvEdAmoAAqhTHX8rnkE6l92l2BQz9gD4AATgeV9dr/z
vvTyU2+g+cUb9Sz33xIDF6gTqtbbZG+Nwf3qPsEBNbTsvNNZK7+ygMJe8NuPzktxYWuuWowu3uxE
bHhDX++M68HncGypaG1vQqSLAEhFuVGmduF1U02n3RhpgAxKxvKzn5tTuay15IoStslALcfqTZI5
n1LPi5dZ3lHI0vC3Rm0pUUJ6klRLlC7OWRcjTDrc9lSgBNsZhmqSEpzxOX5uHpJu/BCaUhqVjp9Z
5kpDcpQjQcWXL+6QF0Y4QXQKni0XgiC3ozSOOA+gN96raiwO32nJhvDPUS+fuuiqwc1rWuTaxpYf
1XIebZzvuaKBlcK2THBPsPVLeG0IEfibabRQvcYwGAnlwi5YyTsroayvkL5MieX2ZcN0FBsuodRM
gcty7GwKkoBJ1lvwDrVw50Mco7az8Y8S8pWLNgwX/dniX1/LDqvogXCRsWQuApnxzPAnLJbU0T+G
Luu97z/WqNqNhvPNM9b+JHumS1wxlPo6lJlbRH7q6bYwDLcqaK8hahsrPHLgd+kRwdSCl3r1cylQ
cIgb7wTXIk3gs0abqT4RwrUSKW7MemKpDPa96HjeDWJHDhcWlRCyN2tbyddxFc/GbJzMEjJZyQq0
oT+GBONMIKArMBOmE5PGc3+Dbq4d7cRbtUQmTwF5kk5xFoZW7q7lPBF1LM2DJq1JqeMchN54+LOb
iOetbTAvpjADE+jfySsiXVFDQWy2yElN/nS0rX181NJqiLz9Iih0HK7fw1rHUOclqvlenPTuxJaU
91Cu7jQc1SEwsWeq/UbBlnXa48wgZcsbiHfVpWVN9Jfg56c8N3INwndTQOp3Sa5dB/YmzvaJTtOS
KTWow4iCX76Zu+vaXB3bfrBCKuewDcFqFwyH91tSO02vMuMoIseCyy4jk19MiH04rtSPKo1crInD
UiFBCn7H/Fi4IQLdDUVlvgIADHn7jRE5lsPTKJW62Z827QaKW3ySVVckvw+PBMiiSkH7xS694x5Z
PGTxGAkwfu/3x6m9lnry8AcKT0EHvvUd4U1uJt4ztvFPfZ7zOJlNwaL+hLGBfs/60qxA2f2rxZXk
QJGirYjkndpIA3bCTGA+wJh/McIMxVYmuc9dNH6leHobC8GBZPEHDqIzMySLftQ/+X1bDQW8S30D
PFSUt5oXvoIB2UK9U2qlwpx91GmfzR8MxIq0tc6pYuggGFi99KVu1A3hn+k7Snx+jc2j8l2HZzVq
piMu85NN/mlsyYfdu989j1dzhBHDY5IS0urvwvAh78Ecy2aPt7Uzr/+EerQ1+k2uZ7C7z5gY/MEG
Y9j0P8nuh4RvDEYcnd0XkMrd1wUetH2J87DKh3Wv447pOOIG4ZjNSfBRFSq844prCtF3hkRJUQz+
y/RGS2aKmRlEcz9h0Upsc/OI5ulo4MogW8J8LO0rry+GaC18qZKKo45PYAsRNxOhyq8PCrYubSpk
97DKZOSHcFUOxGGgahdJf0S3UkNeJwfN5jf6s6UOSBWWthAztht1fShgN7idDByh7kz2YU0lqhcd
/sHvK3R7q4CuHBrqtxIRGmeFNgDDFMKghsq9xCc7ikKIVWvxftImvGPan3zLqm+Z+aG+oFnRlpm1
yCcbfRmvpXBcXrCxvQ6zY5RAWdF+y9bcKIUV2h7NgS08PRo5FSlaD8Eiu3MoxgYfe9+FAsZ4u8NT
KlIfLX29dqbMvrxHOyi08vz1Kvx+60TzDMpMUaV9h1m8PMa0uOI/5C2LCih7m3RZcfruqNlfNNSD
95lRR+BvFRNgoROZVbo/vJx0D9B6P+Ut28ZO+gudz5jmJvKnN8B7S/XS7y/vOkBvBfb4nK51GObN
xLtNXEc1lx6N6UHGNis/7M9qpwssU06SRdw4ZPsT+yOw+WplvUWaxWZ5Ri9pe0iKl0IXPDMqHwbg
uq2BzPb1ZJE6jMbNj7Z94ADatqPfWMKkWwpJn4h4PfO4t2Dyj/Dr5NRfzOtJEo37N43wzsAqZlbW
waDu6Lvjk9JsWSWd4HpDSyPBayUW/qzEPeLvcljy8KFpidCj7O2avFV6lbhExT8w3JoFe95z4iYK
KMb7R/nwR0yQW84cad10mSN3YZNxYBqLTATYT3WTBXAbvjvAL8UEo6MZ7b07R5ERqkjLWKtjOtBO
Uk5eMNNxDNqQIWDsgEDae9Pf/DcEQ9AHk6zn1fOGX2FG6UP/dN5wDB4NeLU4S/i9JhN5NZTOvYQo
m0VET7W8LMoZQcg7adFbTDNyN/EwK9twiOYmWcIvgt4dGh/v/nr2zcWVYtczR3l7vkc4WeIjgrez
zIV/nWHftySiunrKqlxovftKSr35OuQBUrd4VkSjtll+zMaiblKbVL1QE8CF3tP+Pi2NcJk5h++e
5ThTfViSdFBrmYNQYWOamMrG/emTkQG6T+p1v2Z6UKz5iKs4Bz5sDYu81xhHbxB00UwokjRZACe/
AopMhlM16gJkcPMDVwrD4sFnxc++7S+aDV1OCoQ05iVpcSEQgKa0gedN2y0gutJKWFlIxrPEb25M
JlHCChHg+Rq2QxMFcyUnSUEjIb7X5MxUKozNfJukwIu2b5PemUW51usTYfquUgvoP3HeJG8V7UtJ
djcl1P+mzaWGPZGK3T5wspKe6o9ZJxFQaGcZ36XYF2dTX01GRUMiI1vOeEEAS7T/lt9T4ds1FqMc
5BQN2V84oEiAXzltGmTgEWPmwGF6rJgOFkV6ScOxStI9RgwsfGWqHczA3qM0LagtunZwajB1xcQJ
3K5/Fm3LNK2CeaAE17PRcX3mMmgxgYxV93qUCnNKHTlHNleLo4MGFWOnIMSafAZ8LMxkY2kmcvVk
KLI/WgQBxV13ROW5SAQMoVDSip05y3rZ8ro6ULz/7vhZ1nYtQk2r1ZJpopw7/H6fjKWBDImNph3m
G5x3nqprfqwQ0TPzkq6wLW3jQ4gBdRuQerducix8RIuzDGfeC7BQuXInkTjO7Si/Mpbjn6ovkeRa
BydOWv0ZbrB+OGHcsdHaeAjm0jJpaYCqPSRx8xIUKFwxMk6NOzwbvgwgtghNR/QUE5kHri+SklsL
zAnLB91smy/fJcyW2NS007WCktFjWoYlCGLw3CTLMX4gqoPZrcH9eb6xLqIodHO6Vu9WeW3OUgPN
kd8XewSQID027+p3qczg/mbNR4KxjUGobHyuoTgEIYQfIj0RHybzUTb/ja0O4Ar38z8jOlmJqDX5
BI1vwAIMqTPawRBWGzpjxIR33EhO2pzct26mBx9ILtOiU3yagsM2wfrz4QZ+X0+fca5CF/Fjdfh4
VneKjetfunRBqKIecqXMRdqKLH0Tq0xlxWhyt41Q+78URZuFzmBUUvk5Oi2aw0fRyEqE0wsQiNG9
ocZF2w+C48GC3CDucEejPNQKU8JBQk7jufHDQgtLgeRL9kCpTvoR20WaJRVYQOHTF2OWUMtbT94O
7MHv0q6n3wgCPoAsKK6oIupv67KCmzobVrDwHec/mycI086LIYMlJ56qzA3w/q58qg93NlxV91F6
4nhR/P1OU5/7+sA73AWGhrPi1bq5JGriMeQikxlV0INY5IM0WKCBlgr3HDO16dW071zdLRdGbPzI
wFwHj8ZC18fbYtnoOUZ1GF5A0JtZZmIDTbLt6GQ+cDZbpdXaEw3Xhxjo6BqoG7z45svl/ka9L2BD
G4oGKWN3nuk7uHoqRi7tsLN7mqIxVf4u4+0C74D4HAO41FP7n3FlUKaVllK+eEbVps7Qbl8SeUnO
5vyi4+eOXocCtx5On7n9VC0GVTAboRASznhOPY2RUUbGWX/RaMZOiLitfDElvo3Gn952YPFuqZiY
EN0lrTKt+zTYzY5hsxY6Y5XZehL7nJFSr1DVhHkGZ9qqEyohddU9LzvUiPx6lGJ9P8W6
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
