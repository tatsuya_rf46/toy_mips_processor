// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:59:51 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_20_0 -prefix
//               design_3_c_shift_ram_20_0_ design_3_c_shift_ram_12_0_sim_netlist.v
// Design      : design_3_c_shift_ram_12_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_20_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mEL3Zs2sBWwq70h+LUnZNQceciepEbrWHUq4x8NxuQ+R/Xo0qtAnNBdTgK0YVw4cfjk3HOeBPbTc
Ucr1RFYKuLGiegSPB4xT8O/tg00dD1T4MXFADWllCy5Kck+jfn5iyiGYNVhwwTzYVWefQlsgrrvu
d3da33N6Pa/p1O9VzL0QTdZsE2uhr+qG6Ztz4QO2+/9yrcx2BkKkMHwfR43FfFvDk8m7EMPYnSwV
FTIdjGaP96U197zOch3ex68ttlCjoYusJUqDKpGfAqxv63ogq5k02yXYGEXVVTSOfUW3+XSVkLsn
RIO6md+xL8AYcmsGNsayUUkb0mHYHteqHioJNw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NUxscx4eYr+gVYGdkcesDcqbOiD+CSwPwnxLDU5WRjj1FNLP+bzBs+17nuDkEHWISuMz4nvIeWSo
bL2oqzak4iG2OpiAZNtsA3pa/aDbVHguoNqozSWdM5jZ4qrzTkl8qvlfpvI0bEcbpjdGi1RjGN8k
gQkrNfdm7V2qUC2Zex0FaHSM2iNAzOoe9M2YejPTqngZn6iQ+t/LUytGpn9up1z4W7xX8E/CWahP
YoUigcVROQdHjscmxZH5WnjSoUczc4tT7/Q+fPLYcmccgiIHxZZzKrFPmOnI15nkyMVFzpwIcgzO
zhF7AxpCslLjG0GbE7kbcoOGOtOUMUr0F/38TQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4848)
`pragma protect data_block
3L59DABICEEjhW9MH6ls78ytdWIxuYRZhBBZISIdx1tLU8H19mAouY7eQBIS94HONFGClxBoKskT
ZPIuHPtybSoHd6UEl6Xq/OnRCSAcT+oVb6n0KndPgIiXhY5+hd7QJbM7FfX6lx4Jvhv9qJ/HCiGk
WCuYXNfbsyI7WdHBlWMEETg/sqxqL0WrVW5VBOpW2V+C26LSLTM0efKojYLSnejGciqXl3QlJMPb
G7fVziHaqeieVSyJKdZCCdvRT9hhBnbdsfJiKDlnWkNOdDqIrHxoTgFkavx8VmKKQGd22dAmm0cw
pXCVXXeRX7DBCxi77UqT04V9BeMOBNDqCRY7YPmqTG/xWC0uKbX7BEJL+pXmERSMCCuX918Lpnjd
46UUYOzTGZaqmIAsRlkGQNCJ5F2UP4xtv4UyfjLpb0XOcGhCodsBZZOrgHfYSIVtDgM37a518oEa
Y5/biBNx2kFEUqRneSiAeube1gyDWcRESUJybBTPbbkjKLN/nvePOUlT6XOnMS81PfxF/b7efZKC
KqSM/gW4XwkJtmbLn3IWz3BS7Ro/LsfQf77s1tdXw/R/hAkryEr+eUfolk/FwHuOXkHZNmZYy84m
BA/jo2Gmqc6rzFs7VjA2vkpkOHy+XVAU/bqWBkERm7A0Dvx7g5t6hsOYWDak20mRXXQ9h3UfWOcu
Z8HY64Eoms8/AiCIug18x2f4Dx/m9mka1FEHFVbWfzqD+T28/4S3ymDeZKBrIs+k4vzMA+g+m1ZV
jIZ10jNVIvcZMo69ybMYdWP5ni6SPTWLPYLzUGwlgss7yE4IVxOUA0tx4VH3FAhS9wzOXU0IqB40
1azWb4gA6ODxQ1tKKq9ek8HrbavN8xtFiSUeLRYNzNyjdpZoe4qGgV4b5QOK7CZ5eljxMQh3tHIR
E/jJMM8ORS0SmtOSXrdh0mLzr8WtgMz/pWp1GovVGK/2kcAvO9hepRUioeTGDmHlfYCGLbBw5kck
eKt2sCRpKEKZzljGAx7JULDTSimbsovINik0M6aLI4Fg0yDjSG5hHVWr/xzM7udA5dNxihDUCx9q
ytwj+xRXshNTaT2wYZnS5Q5JSgcxkfKwvrLUUNFfqS/UcDAtWOInsAIvCx4KXtzJDbum2WzdQD1x
r6ZdfsS5vgThym3vy92eHjbojQ5ZyWoefVDb4kiVhYUF/1JVkPt2GAFWJ9BjOXekQwN8k05ZlMba
C9FywwK2vewbKlBhYHhTC+Q92w9F/n+xmr58HDp0UP7hzz9WEm7ZYhGlz5CQXH7fZdmKufegetoC
S3RDHGOXkihEnrX2U8zHjkYlFpI9dt65Ge7qATGL6xWWOEulUZ8jBsaNUKa8pmAOjJ9KkG1xcoat
WLwloHhwaN6pjSgxJeuSrQttjZ2O/gtzE86xC/LI2nU2XAQmlvz3ldmM0wroNGFn2bEVd2ME2/Rh
zzhXE4P8sIymBRWRXx/uXBxtOe6/KyHR7slFEJ3VT8v2RL0JnOp2uHsE+zYPyo2474oxBpQ3G6+Y
fZP04qFwOB5L2F9GDn/Mklfp6p141/6czRnEVRkKEL5Eze7ggffqNu04tNfnjY0x7Z/7xHoDlQz2
/YQMd+xOH+2K1RnuffzGRl2YOMul0r/s/mJsNltH38/YO/UVGNojBgb7kfA31lkPMcaMWxz+Wfve
bS6up+dPTfj9MZ1LNqcgSofvpxwXhDT5UD9pYIbWB8PhZM1xoKB0+8RA5cjcCTRmqsp6redMI7Zh
3s94wneL5EQ+pbmIyCy5hZX6Qph5jFcpH28I8vKINWB3VZl1XOVs4voSbBpBJWfu2GunY3cw3aOw
CnojLXVwikQRM49ieGW2Hn+9ReHNSJCchwQrWtHIxxR9vAHCJsYzZyBZImg12qfSwfit8TuPeVrh
TG1DqEKcTKIfs9hb98STj3/Dt1oLYpPctI4HlETEAP2GJJM+HWhw5mvsueo61LeAr+IcABjdzQZa
oqM+X8y/r9Q8SFvekQ3Zo1YzopirafA1timwODC4w+R6MXD7eywt/i463dyv1a0RI56bTdDUP632
mVVZgMRo8pQtOkVDkL5Y0nEO0oR6EDB4B2cFRKPaV3RruXIXoovFNNDqWjr6P7Vr4k/5TQs18ee7
efIFI9yJ46ihhGsCnjggzjbKGJ1lavQ7nTlBG3s9HsnmyTDJrIPdFdbI/9d1eAhpoITCMTja9pcc
AFHB+BbmzJkbdF6djDdmagOxJYCEedsWZpFXHHgLNmpTdDaTuMhe6yJOTajoWylmPanQMNy3Pkk0
3amB7wmnpWM2FV8yXGK+H5Cn6gviizdwF7pjZT2FquGrKjJYFU0DMPtnnF//5dPkhAiF00lEtpmd
Jm0TcmLEFhgMFyL2r6PdTn8S77ROGT+BvIWvJmL/hmPVSd4Q+/+V+OnxneB0bcqYw7UFf+HLXV4d
fL0mwejZWHDCBpdN3f8KldyR/0buTRMyfbWmCTqSrvJoEQCIRgWiHZopYvlVvZxd6/gUHGRvI5E7
oB4EQykL9xQ8aOqyc2hhl5ZFOmqgKzo1e5Ffx7Lz8ImabQvw+p+gzl5a5eucdamSB4+8VRSMeDzg
7FQ4XwdD9Gi65iukBIXUJcRAtR3stkIhd2WDK8tVVDT8lz6KBil1FifoRr0sSypO0CrhgZq9l08P
Ukpq41OHwYeYnmqSG1yj4ZNJXBl/yh1bkGoufYtNzbJYoNER+1fShq/b5aBlC9L7WwguuaY5VpB5
mTCrndCYSt67qR3eppUOc0d+hkXHjMgWCZfJKRpDQUrMC0TeosoSMUAAnRPZbjahgJabpgx6lCvl
AVHE0tASjBtJqUd+74/0iEMj7I6jrFW7KuiXdGgGQhnhXuyHTDnD6dQCaBIBGiFZGhDQdrsyeJ1P
FE82JAH80uqsksIQ2QtSZy9iDLe9hS427eHtgKfbKtvUgnzCIFiDkRnnbOdYt8A6cNpu92jCwQ3J
zcjPrz5o0qhbU3phpLaq1KUyASbc1e9bYkeeUAi/H45NofgJIDNUFAAkTmnrTdEtN/TUvQDfLS9d
PXBOdIqk1otbRBt8ndAjNiq3xl6pipTS6NGqHL3HNGuKYH4xgTliPdHZOg+7cLYq2eTXfadlIR3D
0chpj/B0M7NqEIAotCm9olYeArxreVufr00BXvHH7bsCpoqIwhFFLMJueOnAZFROdyuB+T9S6gVa
zBKT3qE4mvyIo70hQmJ29kYRp+u91CZ/14r6u4OJW4+GZqbYcQgxYXnUatgokUp07/W0znKy3NIv
K/4cU8Ojco3IT/52SpLsXNP7FxN2uvvVHOp1vW/nLdnb77veALlXZHmLoWbNkZpAT5pltmYFl++S
N+mvgk8xrhSS6DBqo/1hHGEgGfaWtnTHnAymj3WZ4dXObTNv8vQy0rrLKk6BlwIIe+mjHo6tmh9q
AKiSvOOEHbourXuRGcCZFKdo2qPAnqSJsE8Nd3oMJ7A1f10VXn6iuYSVv07lA9iy+ZMX51FWhnEM
uVXQQq62nuPCfKbv3IEVtScS/xklByhEFheUlTrn1HRDw/5rYG+/55aeMP4CeLz7lbo2ZtgR1Tkf
rxcIN3c1ngEAgckatVZrnCA0Siii+WsATij7izT9R85gOfUzSco1waIgmfgBIJggwgYN80IfNA+t
GD3Q5hp0++euwCbOp6TeV+nNiS9CMfZvD245UtOs43sr2jinx4ph+1p8HVobYetZ8XrBt/IL57DY
FrriDppzJsDsHmQvZIpRi3H1UD/AEj9EAkG2iEFHf5Sd2eugpCW+NYU1clEWveF1QEUMSv/NvOkE
PjLnU0sjP8ZRTCrh4V5fp0svMGMe3Fcnvry94uQe/c7f7r+Wg3J2QreXo07KAPflHcmswdQ/DtIt
mvhJzf/F0AwS0+ShvtbXYvCUMbiXlPLicYxM180cjH+Uy+JA4UBwJKQe67BsjGK2Kn8ZwBdVOaUe
7mDvfM5ZVHEDo1p+i3LnPcpeLu7mPmbGdt8hfgU7HT6y6kMrLU8rnkURN7MtouIP1XKejg/XPOyy
9fg4kgoEdWBPrvnZdmAyrYQLt0l9YrPyJWjGbh428yICu1an0LJA8iGj2PbATmtyLlyNsxG2+qYp
pJlWjqf98Util+1JVp+Ts6fvJPZ8VMY0F2wt+mUTrWIy4B5soOEKp8A6ecnRH9rcVrPCijLERdel
SKEWLcww3LblTz1U+PFONkxzRvxMTRQtuP1u2HEgYViXP88qKWTzd1E/fdziH2vJF/aj5CY6BJAm
P9LXQpO2ZwCo7EwE1WJrCmRFC1effCy7+lN0bUMm+FYBeDuPsIiWVQgbPaQaRHE+rCZU+85+czsx
r2kp3eYzwBe3mvJgwCJBTBPo6HlTGjPqCuJ1YQ84JgDBOShDKPb3pgBd5ILc9qzkOEjHi64NTstV
DeqQt6xNnhxPa7JCnPU7Hc08bLmtxd15B9KSeMT1bevs8qwiyA05uA5WmPHf3gNIcZnojdSFiJ70
fZ73MTWlo3Kua2PZSouZRid1In3E5HfXZzGNaTQhakTiSAnkuf0LGzswPOE+A44yFeHNs5QAm5Gi
Cc8mlWBVyDuizjFgUdcPCuVnzoKrZbUyG0s8Dfr2/oq2LhX3bcVdmSy9dkS90My9r2pLoVTlFczr
xf3qTQBTRYzqliRcsFrgq1Xg92r3A9Gdg6+nYPTSgjS9438M+KT/UdK+RR58udbLWBce9nJVTuMP
O7uRvMnakqSTsztxW98z9ogesvmftocAK5LgoHJw12mw8IpuQxJZ+S9948OdBxRUtmM/Fhp2ba0u
YnVT+1PTqmIElq/BxoSOtLA6b/XDafQUyKnb4ofzf8M9j0KC7h3AhWYMpY+Iz78cQo4b6k0s2Kp1
mf2urtxOYOMZNSkYAn15fWJNjtLj+NN4wr9IlCGVSG6e0LuX/LH8Pgb3JKdOfUnF9NNOX+cp/xLC
j3rUl47GnYwU3MSlgcwdZmHvI80LW0WkZoUpkNnSg3Fwgjn4d/QjmcP/2dTGxuLy5/7KBmGex6eW
+Dis2QkrPGzTBKzQnikzQaAV+aqPsSdNbyfhu38ZB0qcHWdr0QhrL4qPAsNJ2XIux+2yz6LIhB3N
EQWRcX8INHvOZ0P6fVvjgBwKD43sUSOQAnPKD4ua5DfFYSwWeCCDY2AebNHQP70BIZxjGvgVuvaU
745rJ7YFNVEUlYGe1KvDM0VnN870wKNKNGqZ3hdYhZ65nXvlDyyKrBCXV/QhJCnpLlvVuJS+k37Q
aJlxkwxk51SWiyO6zamoV9dWnYWGrD5xhLl8xGZpsPGqCm45/ROKaa+yeXvJZL3iWc7XBzlyfF9a
rRY11detRx9w0DetptaQplmWAblsHrhQH0dsb97W3YS5IiqRbv/Fal3six4VLs6GU0B8inGBlBbc
4U4jUDOuq6plEuCEwTFra/lWqJAijUOsoTrs6TGJQiXWIFWUTmII4hOSpHLEPhBOs3l8Q7fJNE40
8V/SIBKiDDLZ+gQvhqlDmPODdGIkDmc9DejAJNFE7IrsF/P373xft/BTfqqnQ0TlsFxPtUI+qKt9
qrzLatSKbRyy8TQPxUEUojnGDfPIBABwhNFuZ1SyYgbdnOEZ+X9zKbQcTc5V1zxdJ6TmHwxXdmaB
EwFAjXZLczuUaKlpuzX0z+ebsTdE1RAis0FixQ5Va/8rEIvt8vR/d5uM++glGUeIXtqjFVhdEdwj
ZzVuzan404bNfJsWOTs4rXC2bsnG/pqQEAsUMb1bOdeoGDYgStuxkA9kttevAA7SzlLr8lkIq2n4
ZR2uqIE9gmsuB0SaMs9XhLqsFm4IIh0XWeitZ1fS1WwPK/KTSQ7gvSbYoES/dqu4AG5KnWTxIvEi
ux6HWS7/Bf+Z7899sLKp2n8/RaMifCCJNkn2sXSBPDBHcMzesZaMIuaq/VT5epDiXohJsluDka28
yU/RbX0srs2CP6lqSrKi/ad130UpL29bWfvcwkw9+a3cbhOQSOfCXiTM9Te9SDnI97d2MR5v+UY5
VKWHDUeY5nKg3fNXDFJcILbhUWYHAUs7Lo5vaBwJbMBwa5lhiOvuLxEF5oyatc4I7pqVxaaS7gIx
rjCCpJgFr0evTuDTt/k1G7AF6kx1uE0oBZOrGQqW49XEheLXgK7/4VBSvtgJu3RS0D6lwb0DUJlH
uR0FmdZQp7hTi1vlJUei3ej9aabPl9M7B0ee/m+v6cjvgIkKJ3xXP7oKT5f1DkGhLYREhvHP9l4r
hFFA/BePHqCHAdrdv70tpTaJqlK5600gwQcx8i7XsIsqvUjkDHew//YSLF5YRtWeH1e3e4q395VH
DaT2b8ssKSP+bPRQA2H9dF1qJhguKRxDicH/Uh+K1sp439HOU95OeZduCYC33b+dhgYgGDCBgXq7
/i8Czgy9igM837c2SYv9v3P/vKadnwe6UhPgSZIgMi3ffqbvHemEggIjtLu+9CFihocsGjyReEox
9BWw
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
