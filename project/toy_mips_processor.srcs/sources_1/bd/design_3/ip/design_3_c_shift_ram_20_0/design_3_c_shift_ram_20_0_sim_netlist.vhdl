-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:59:51 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_20_0 -prefix
--               design_3_c_shift_ram_20_0_ design_3_c_shift_ram_12_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_12_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
kUUqDeYk9Y2B9N1jVaHGhWR6OsL2SRZ8juKeIBX8zdZZ/KkYEwKz4m/SILJ3ZSybpuj7b7vYoRQy
wJVpgXiFvkz2J+9mWCdjy7duqjnJdZp9hPW71erfNae3uGorx4fZW26mCum1FvjuBLMhwUkve+7l
CPqnytB88l2VXCQbUoNrUALI/dStvUEep+sX8BqTO2NhsSiAMuJ+U1f9PIW3+WUgdJJ8k/UUUdqN
NOAGM6gSt81E38Fu+7DzPNszo4bEBf7n+Rdww6Si1eeFBhw21R/26EFgah4wVkNf/A2zmjiGnJZ+
vIqnu/nkpDDyK7jcvaJOgoQ6Yy+wQp3nFOjQJQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nSBRS4IuSoqugUyTfcBnhp03EBh9iQhkWX9JL6RZrYWbkSO6fPP7iW9qDrTIoFRno9sYx/KMYhoY
IPDC35Va8+vtcex2FGP0e+TUxUwXEAK7AeCs18B+uZkVcSmg/zpYLlajxQywjH9yTFCputLlH39h
DZUI3QtQarYRwf2W71jsvG0kduFSNefyqi8n+/L/tG2jaBskfVZhaWJB6E5BiKOlHU3d52fzfoXy
DTHhJlvwN+vmm9P5DxE5idQ9NSCUlCtOQYHnLvOM9HRhksw6qmVANRjb8CGYIty5gzHGvJNqhbU6
4UpzshdNu6wOyZou1YzalBCfomeHa5H/Il8HbA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13856)
`protect data_block
nFscXK396nWV29YGtM47TWXrg6chfdwrlI3tS83WjIyxv6DCLAFVqdWLAkI5nd7byNR5aprKYCZb
Uph0hXoDkN3ReTaif4uRkkmkJuKwHubLDvj/ejqWAvmPgK+LKw1sgRbF4artG3UezHxmRzn6FRjP
yhQpN+tu1R4XOuXraztduZVz7TDBrP1NRCvbKN/JDtgBz6xSdfYgFJaXVm6wKJk+gFx5jTB43utw
KT/9KLEEyy+pJTZeMinwZXfRVsJFw7uhIKGadgjbpgnsRZWaiNSFCmKqgF2YOU7nHXaKwhlL36K7
OE3rXcPqwCua6AOXWdAL01vv+luASTqPUc+FlOuEWf6LATiC1wRwlWrUU8y4QkeCpBYuKmInK93b
pWtKsIrAKc8lNlrWENsfjHuPlWgi2HRFZj9o5uNaTL8pZhFWb8jli7B3Jwq08XBxAhGVk2bCT6Ez
FCQyCmUYanxYnJe7n1EYbDtYFJwi4vbUL8vZyZY5dk81tV1a1MS0S/6M+ZAnyWi1MYp2uBp303q4
pjhrMEsPGjhbAWpIXmJM1H1d3JRIFnqd9q37/Mryu79ML2VEhuzIRgHEJr0cCDSCPheTzvvEHjbW
sgi3TRgDlokiVRMbUUBmsbw1Xyf0RbeE+trQYjghQDRzs/W9OteeJJA1nm6jre3up8wthrWl/3gg
ynjga3BdMHi6nZ331jcOycmn5i+TgblwufRoymdxfpi7fzDuiP3utHexag3DnzvvhOkWUHZhTj4N
vlFegDrBjtoiplIXsgimafb18N3D009HETLUOsaPDNt8augnhU2XnaEULp1tBT5SzpT7299+y3gN
bpPPa0A6Tlt7UCSVHUyu6tqQCn89y036Tl2n+ej7b55evvpcGx/okBPxDvf8odpt6+M9vcDxxLe8
7ZByMhy7Vn5BloSCVR54WA7k5N3CBjLwxZzi8cBAkGJXsfatZHuGuAONObPFoVPpnogekwxaM345
e1GlVCL2aK6TM/fGUXtD0nwwCbo021nKuuneHSHQ4YONLjbcBdkYLgeUIGgDU/GAsH0Lkj3boxLx
NCS7noaguYRiRoyuHJ9u4LMX5dm6AiCSa+kpgdOqqNrOw6ysAlCsg87+dgRX6EeCMT+ctKg8qHUD
HvbJXAUxxC6Y5uqaHc5BWLoC2OUFfAS/8FwoRTwfujIII7jD5nfdtG/9g9KlgMrXzn7PVuqYRgOl
eKfVL1PnXqEghbxHBcUAbiMAiHcrmc8vMX77uUg9dA2vKfUcIlfmu4sEcSQUTVzdrUBmu/sCyfjY
o2TCXWqDWgogTbNs9rOAPiDovWNMnUH55Un2bF3feXFThc3KXEloGi1X1C/O7M/k8HeFJWRE9Swf
qm8OdaerrCZUIKv37h4KbIBtY7kyWLpX67sAhATN1kn31+mPPe9ekfJCh4zZJVzKs0XP/4KINyLH
4tqDoc73SiIp2El0+L4zqL2SapbAxwEKzorPl2FCb/alOftE2C0RENyzpntss86QEEtcy0GEKXX6
T412aFBMCBNv1VXdDtafozQK4b7ruqb4J7BN9KK1wPDr2EfYQZAs2pdNS50GEHk80dM0T3QR38xc
nJHgM0kwzEn5W0mBL6npIwvcJ33NhcSj6aslhEBl+61CEy6iAjdY8Nbx8cipGGR4QUZXi57O50aZ
/0ZZI55Pw6EM8PivAPUNjOgIjl5kJpmbHh8vCYB5kgIiqH+zrgvXUylz2GacDPbYcJqtYuuZxtra
rhoTkgHEByORgXEVzLO7Pc0iik1Go1s58rfR5n8BEcpv9WbQJebsBzw1w+9EJeA7GAAZrkn4iB04
I2wvYhEhRe5brlBstiuEtJ9Eel/NULlt/yBpIs7hpQlyw6N3xH9PNfgZn8JyhmwYTks+lVe7EwIw
T6Rbi9Dhzx9Eb0jt/GTsuSMoKql0hvmV8P5CEnLIXiiTgm3GRniO0ND26fmVnjkVaLm9J34ngOS6
2/a0b5pDppRQKWtDWefAAMnVeNIZmTlx2UiG7rgwafBkOP6OZNX47QEUubT83UYdgviDVSygiLsc
0PinynIdL/3OoT8/5q8oN+tWELIsShVn2xXk842wYwVpGtcUztdggjyKlfMPcO+aykZ3kzNCnlKu
NYO+lFTMjOjHWKoIWnHzOQ8ZDghZ7r7BD9chip8Eda06k9lVFy4CgmNV7mQA5Art2M+XA6+OEelB
2wvcPCZOy3orDYj9vHLQEu1lm8nBCseaH0jPYgN0UDYg79errU/tctktuAwh2UvQ2i3PRPsgAeqC
7ivMkbOqSnO++BOm6wizFTK80DbilfBx/5d7PtS4f3Fx+Ip45KEWegFMUXZMKCmhkWflz7fIMOwM
pBct75uhPEiv+pMO5o+IAWOYj9dwcvuHGVJ8Dd2JBAYdSAN1HAD3HRYAJyqvdVYMSFutWQY5uPCR
rn0MKj8jHrfmmNzTi4gyORPTKoww08//gPqKKOgHslHyVQsLkRFQBr+HgUO++K8VJNO4o2UWSs40
2JABNUu6U7IPF96J+CMGeGMun3BH3fWW9Vbynn0nlKlX8WFwguagLV2HwE2JIOqePHZanAp2JtL4
f6+SZ/B/eTPZEaKAf2v6udmY+y6zzn2ar+TsKelXPlWCAXrJbPri793+t7OAoBOxL1MmtkOK9lHk
r9OWFQLmV6kbJQbuD38FQuWpow9+vQiC0V+98YzlOt9XZAB2cGPOwLkQGOlheeeRc+Irw9YqTSHO
93peAQbw/acdaZw8o6nX2XX6atQ+SkpQygG8ekmtoUVtEwjvIRZhwWLI844ggmWa1NHnqHNaN+Ky
6ToQ8ns+IfvfwYp6dNtauX3VxcCvChgr01fYf4QfYnm8Zfgi9y8KrG0E8e82Q5chgoxqnfUJVV5n
Ji1w8kIsoz8SKBjiKu9CYRB5DzU8WrSRk5vpFrWMHdvaVUIWRwahDARNzkh074eCs7FV6+1me3+0
egtcF3cRDn74NzBMJaG/34BDYaFZOjFSGs277bNjRgpUPqSQhovCK5dHpZNX/LY4l+QAeEGRimkp
Q/KJEnM+TlfsCqqMjEc1kdEQn+AUwtqzo6X9BesuBS+ILPVu/J0WG52QhKkrtC6yVp/brUO4noVd
EN4Oh8iogMsjjkcxlgMZyQbSKYYKtmTZXDtGkG2NCxGJ3CXjraMQKISbntuhebHDNzmRG9Noiulu
D00DaCK04FkCaXiZ1E5KBSTcGWvilxnrqzxMmIY9WPJ8HTT9mh6T0oQW6pbiFguke/PAlTvkO33S
EZ5TSJOlwXiamumUgxckH/8fB6tILdbXJo6uHy3+ZjLePiJeSrgL7qh7YMz1A3sg30qSyi3Y/fPz
UApdmtixu9TR3eCZ14inQ3Pl12Pa68Ug2Xefrmt4VT8TlYPLu9l078Ae4Uk13Xv3eqzmbW3QSE5D
irdwZleeN0gLRtKBPNR4BmEtOC8kTcQLxMcD8/voRagt/EZzFgBH+6dUQqr6S8G+H/IRWrNpMp1Y
Ztihwv1WMyCm+XjIBozDXdioy/nHvP23r+j+p1wOejYWHy4hrA209UYGWWh0RGNJOdq/bohMroOc
g4gwwEK372Y3F1uGyAhRALeacRWr/S12bLSIIbjHr7J09+JySZPGctRTiST+bZVibrrZA5uATo+c
zPf58EbeVt4MZ2Nv0a/4TWf89cCYI8YoDY/AiYuemM3AcQWY/w9nL7DwaX4hpB87sjngHJ2cUdx2
rk/KLzZTa5WD38+v1O7ZgDvT2x4SEt9PCQvU4GjwAP7qOHyX6/wJf6Bm+X6FfJa0fP6lBOr8bdYC
lB52aaKdxyxhLchkslRS1mb+9C7dfxa+jQ4S4Eq0cHFL3lgQUg7Il3satqoXK2GH6RTOLAzuIr45
7p25TsMfeyyTsNiGRMPJCmAB8T1S9SfJQpkReWMAJFI//6yzrH6e09Ki73OjtFKdZLwaz9GsWIKw
wPyGdOqLP+dDA3XEhzcKGqplrNWpgvAKPTpY1s+sttOPx69N1QH6uniKYmvzypSSpYtlsXN02DaH
2MHPVQ7u9d/HKuVD4jOfgC9D9WWVKRERZVaMvGnQy7SZT/sIVNt2JNJb23uz2law9giufveAqRVf
KuBvGnzu+JgipRMpJwmcfcAu9IqysC5HBXsMweUZ8k0uLTErkIyyG1IbAQvyPiZy6QoEMkq12MUh
kEvR1WKjnGvrMrjJxlk4wBkY5za6fHrpBTmb+zhqVf6l1D/pbSNNpnAjgl/gj7b3DV4fRToAFYwl
dcD4kOFF9iCTBtH4wBrIWpkJhjGDX91T1kdkQH0FqYaW0lC+MKBHQnhRik1esxPPo/Rg7NFEErVn
C7Ol/bZngR0sTn7RIym9ZabUgijPQHJ83IzbpGOARGGFwKAGL83l91EpUANXKbETl8gRjbxbBOaV
IBCmNqAztcQU4MRIs5cmWaLSCyzH3LkKmmCbvjZ19MVAGHz/qesAXfZ/GOxKe5T6QKiZ3++o7BcP
Ff+IuvWatlFq7xW8owi6uALZZC6k3D5XFn0tizYF3eBdryi8Nv5c9pgodV59aK1mcEYugLf7URoM
SuPPLgoGVJiKGp05y6K6VE/iAW5T+Mki7pBppSY11zIMWEfAeggMaRjaRYDETS5CCxNRcTN9v/Wt
9xgqFmclE+DwZ4JdbIdZnyh9jMiJBLxKKF3wb9TjZGjmF3QKKvFW/v4IIt8uTbzhM7vAr638gohy
OGcHljQ6wCr50HYOVJc2tLfKDFCSXz9ju4bhq9FuQuf1Ugql9EUEE1l0oG9jxnWUE2EdzsNcSBS2
tGobr3qrpz2wcA74SFtfsAm26fpzAiDYJP8mFWTIiy5gWdsPZEGuZccLNpEkN62cpQDV28EW0Jqd
yqoInFx7Om0auSa/7VH2IMgPL4PceGievQ/Wco8BR/sAD5vrUvQbJ3/CqSJ+CIOrDR4kL5bDGyQ7
E9SuSJbnCdfc8xrBwefCdoUvlkAKHAuQTA8s3D8Rx5i6cHbFTjmBdnxqJa/bXyF7+uPbca0qJ+SA
WjroCTUuitXPyAANXouvoTrdGzfNbNr9YHV7zqfGAySF1y4zSBPAqoz6SdN3oD57IxUKWORLXSL+
QmckOuwbGzd02pKPG8uk4LL9DtZSGiG5vvp324rFN35zNtnQeqP1NcA104Hr7tOWcsESoMaD+ZS2
Hwr3Og2XfMZGfTUf7APaG5km2IScpVl/I9Dax+3NVNuowXGIEKwu3+GieX5tMbV6NCd66QfrKVZz
Sc5P+0NJyHw4ZlvXRigNcOrFrNII/HlXTcbY4/OXEr97N4i9ab7FlXHlGqKah5ptdl/GLBURDDcG
sd3AL/Ch3ISMWD0Ucw7rMZD1BS+wRO27D3oMyLcIn8D3PbLhytw3dTEepmwfLERXOi4VGhblKBN9
xYTfDmr3Amxz7R8trHy40R3EQUnjzJLn4/Tlj8rmKrB16Dxdsfd4FqckwwK4v2szyuWkz8WXLgU4
r7XZtrjoL2UE4HZGt6xPLKaZ/IuXUHUox7+TLTIAb7qxpp6/1DmG5Aehc1bk06mFn4I7N2AveuZg
dTgsczxHk/jBx8xmj7eEsrh0uDDaWzzSuT5wBGRGStVApVSoO1xw+UZ73UEXuehsLYb9MZhBrJBc
acNejPTxi0VCaQg2r+1moeKqcW5wTIEYFNHeXzxHLD2qB75wjgsQRsJSc8lgnOO21HKKMdroKq0M
Lcqqh4rYpFvKpEU+eVVu8wMpqe4aSDpe/2jr+TGM9D6goveHUXe4wCMYEb7/fE2/vvOY/RENMWqa
ktJQyiW6u99y+4fj6MYJ3Qio2GAzHULzdJvnEStou5VKJyWoLEvZdZtN8KzsXYG0RrKfgeSNroQN
hL5M/iMLCYPqwVidNZhsD/pjacmFLMd8bDj9E94GZVWKuV8j6TDjNDYVr6sIwHamlVmxQjQGJgeD
D9rT3sSPkcPaZOvw4JyXW8cOfmlypbAlgMMCxBmXcBlcYThktJ42+hIUg4wXqUp9PAjMsO0x/Eyc
0GUDpsC3dBZ88W7Ik42LwcFsFOQdZYt54dRoJuQRrjXQOJfskJ3iVvyFTCvQtavbY5+IK2auvDzT
Yz4PipQMDwiyMt551gqsz1aths+5Y1UpKdV/mMgCLd99i9IzcWhkcJMSsT02QGJQG1WhJaEEw5Hv
vraH4D+2V/yXFC5tTLDghQCcllYPoHFkFsyTu9UuVWxZ/iyqNeuK7wUWyOMCBY3OcuUg3aZ7mtXR
kzqsWmu2ITie7eAP0G2Y3AHG7R20NJvC/lpmdio5Ktkh1hDs7YCv6rbQmbb6vDN3BfpcIXdHpZGb
Rzw4dipSg+6f4s5pDKivMJGA8eLdJfBv0UrB0QZhZQDvcx6wE4Stb0K68x/4iGz80wBbqHMTXI5T
edYzypu0LIOmWTgsMIqstQhXGeVG3baJUeCfp0O8QJK+OWuzBK2P/irfr++TCTqNDFVhIUOGPfPT
ccbOfngqHeaBZdUkCHTKr8mRnLP0pknkBlS4IhuHpqp4lpOmvQRfKEAtUyRWm/YiACDEXkk1eUTc
i03MGghAQyZuFISP5u2FwLhjcjAKnkRqJLVPxgsvTbW7yLaBsbPDOXT3CpMXcK+J8F/OrG1LIyGj
IfmuchRYYE8rzOFia2p8udCph8LV23GFQJIpxyUhBT/M52L7ono6tnKIyS4QyPDN2CmxDHywdnZ+
FWAv5CpztH2w5282CN8nGayEJTw5mp+0CVAzkLd8sZ8D9fZB8AKbVC8ma2m0LehUFkdEAZUctT5+
EnDK4DXrhg3Mm730SbvSgJErselSKHTsZVnGEm4cXYyT/kPXqYGNKmEPKjZsEAhL5D3PaGusvK7o
rJwyAdtz1Nn/z9q2sykHPg4yC+Jdg5DX2cpWeoBs6cPZKuhNtWoirO28mxGiTLjZaMdXgOkRyd0L
/T2fVMxpAUz9pNiuj12Fisc3wySflKOJFVf1fd0glKIQJeS/e/6eZEgOfZfT4MgU53BYqNI9rmVf
B+yrrA4rdPI2ann7KT99G4EibfZpGQBQKqiI+wOHurPR9aJU9eZhc2ETwaeLS7OPw5rep3KPCF7t
q7I4VHULT8j6zC9J811bOLidsXNqOjJVVO7THc4W3UnSdc7sJHv0JMP3kroskA99/Zx88v7xUv8h
pcJEMtFiSi9NftscRHc1LIrihP9P2Z4OYstCEKjeyhsL6TjB/U+Wj9Jv/xRUTJoFEOgsQy0nqP4i
re+t4Nl8ayxdQU3s7Uqvf/oJNy6vMTer1vOm39cHMq5UUbLbuubpDCx6T3Wgu8OVmt1d/7PODekw
vwxjgIb3IgVMGq+sAR7QE3NHM6e8O7T4I9net6t2vVGglKm91XHghcPpUhsZDyqtE5oLZpbcArR/
m8MqkrwDT1u1sGc/h4kMpNirj/4Zd/RZ2cAmn7r7aXKCpFxc2ZGPpQTwmL6ahh8IxCtqJuK5KXsM
KCHJmz7sXdYzCeMjVs8IBaPMQcFQxlvTTZajyHUU7SsHDO9pxBqAXCDSEuEpifJkY5Sd6RLXkChA
6AJ0qoRFtcJH9jhQfkIVv3KBtdnMpKnxk6kXuej3mJ+eAaInj/NoHEYzOQEuEEPaizygYgnDrpdt
7VbJOPyz/ydpMEIGptk08RNJqgcrTVM5/Oyt44Ji3wL/p3eJenWKM8MlVJuc2YU57pkmW66a7ExP
9rSEPSofz9npbQZl0kQ31nV9fGKcqMnaHKQaALPeSprsnG7YgwlGXAjWUNoLjeg3m7qLoQlO+FJK
xeJK15U3uZIgstpc4vbt3qPrJ+Anl5dXoX/ezDW0cgOWPnvNv++8LHgaYL7y90A246SIwwfeXRhn
8i1ugNujZbk6sAcBTzYC0te8sZmMz1XtiqiUORHyZOzF+m+UivHOfAbu+TdrUSf8OInUzBFNfSZa
cQ+9/q9qsE5aBO12UlkRqJNRBU7nDozjJZzs3KawAO5mYbUGBDf2HsyrwnT/HPHmFpKh4Y7F6auj
Vwxx/bNeNTTeKe4bIJglC67LcjnEFpUU1KG+3mBjDrGn2wMbCtsB6gFR4Bbbk7iA70Mq1B2gg9+g
8jUmdw4tl3ZYrxh95a7B7JpGDtsIrLjMoihnLL9qLizJ8WKHyea0cbLz0YhMEFbAv1nQ7uRuVusO
XVJ43WN50k7oDBNOAf8Y/IMgFVRzJYCN835VxpH1/SbQFKg4fre4ZYFg/SSKVJRvXAaWL7g3Ne/6
STAvohEfOgOFM1c+FXqwCJ8y6HV3ps5VmJ1Kqj+Hmi/y4ncQ5hkOdfwZ+7IWatjqCugOmZQ3PjgH
Ykf6XelNXvoR91dP3GhtYMQuRNyRXjfjo5yTRg+ntdNSu++rtB1PKZStxftoNSf9DUOJh0Il43IL
tMIQ5DEKHHtbIuTobr8Lw1cHHNOtx6vlMUvx4CVlAwD3khhWSBQhyIG4Id/pL4sw71gxUlaWZtT8
3UBy+5Eaes6NKONkZjA2E/8MZEevamK92Rf8w5nyygphdGkrJcyRwvFmExN5T1n69/kQgW8NQtw4
C4u8awPSt8GEIxZ0CEsp4bMIgZ61Y7sINKIBETu1nXWtVlQ8sz5vHOGYdaqpZb2CeCjGqwD9vR/+
MPUkvddf8KRLul00TT90JwqMADwSpzA6kIBFfwPnQluflm8MwwjO4YpRViPQZcS3BbvUNAbhiHYA
UG8QyzQ43YX8kPCD7gUg5/WDGgS4dn7rISmtU/EZHkZRkxdnXi5ccbruaG3fbsLQ+/HHB1TRRBiN
anJlBwfoGe11eLakwjPgNXFeGs3ZZSmmkdQh8QbPzivjoEWj8cjYDrEaViUnUrQsGTaCjMWR9Eu8
UGxE9EkCOqIVwUfedIIwrYYK5twMwjv74m1h8jQVUQ4/+u2At5VbhJLduWIsR28nA6bx0EvXsWA7
ZkyD30wjaqqD40unvcEHBQ6NJi9bsHDDPuluKqiQFD/klcLfCEszPLO5hpijhAUJoZVzmDFbqaK2
Pf0N5+lyBmUemTscuFMWiIZv3fV46uoAzg2g3GlrFVee6H9mvjhLZn+NrIBoTAaxZlP0OZxjWvcV
kNgz73h542HohKrqmlspYM74Svb8mi4XBdI2HacFen4TlD8sXwht8V6F96pzcNqd7Yn+fNV/M8T9
UstVZwL2tdaJ5JwYadAVo24alrK4lJ4y6Ohkgpk/uhGK8USmwmhgoNmPNc6ItR04NTWM1O9sjxxV
KtejLuHwXLnHIcMUvDj69W4aZ/Eejt6SABb8q92LqOMbQ8tvNuFd76TwrcfnZZT/+Sc60eHYSUd5
G9PKE6sYH7gNGTS3kNFjd8aCPnGo6ecV8sqLStOuYQGOW2CXLDD8aA1oAYvGaZDyrwAwQESPwBNb
1sQF7rcnDU3bngR6akbb36xgHrMR0f4rd/XWCNEM4++672rMJvmgHdLq1OKqeRF99+WbYrXl6VpR
gerPLOn2UF220SUCOI9O8TIv011inXuE8jy1Cm00WVwo0h8FyC8nSbqV+VGpjXzFI1oJEUV4Ld5c
J/RXJGfR9Ttfo1fqE2nPHgw08r7N044ORRbF804hBqFo28ePG6Q0MpSzR1bUuAUIG+AP4t42q2T8
6ZiiQhKjdBKoqInConmRVmJG3iDxkWv0WRJHiwBHjewqr0bYlV4C6ZRllfsIqfkyCEsZM+/xCXyp
bpFApfdczgMDnJThcDyj9FVG4gQKKzQ8Pu9rPBfhMoKC9apAu5q1KzdAOlF7uDLSJQB1QpHZHrKo
YKZaJ0dc3EkeLx59fBYtLVEtJtSNNJATCPdyHMSMIBz68Cpz0CZQgBPg9XueBxx2f5oEQua7Ky+e
NgNfLtQo5KvtsHb57k74RuEA+cLf7a+OLn6vg/aGq2mE9V7Nwqd/yqBxN78t/qOJcDRIWtgyy1cL
jT/bUszi5DvNlxdHznX+Z6to/OeXPwnBZBJJmrMOSOKUgDoFOvtUCAdB4v2pl2hIRmwTe9xDQ6LE
4yk+26fdO8WZU7MEpEElq0TEeOf6/erfKujTZpFfhnVI8y5y8iWZDhSyJLl9hODSOZNqQzx3fNhE
xNp/gc0VLK+pgfSnJBkc/ant2ZZ+BzcFgeHwzvAL/q2eqBw11vx4/9BxW5Wn/PxOgO36n7HTu8P4
mLd7fDyCsKf1ue4nmheeGzoXiJSOUVqo5tJpmH+0OnL4g9SF0e3ejWxuhAHdhHVH4XBcgcojJLt/
VfFCVlgmjb02px46XcmVWMEF1w77AIMrq7EyDfO+10aiVO4GiEhE5pwC+gUi/o6Ygc78DDo4YREM
W8bhpqSzv/WCac87C7qimP9ffRLlPw5P8ezLBsGvOmtPgzsqYB6vSCZpAHVXIjagTv3NrUx6WQzL
0FLw+l3HZetfyGydnhBKPQU8pYqfugTpJJ/Kdm+AdUWbWx0kcAabIZzUYrvNEKFYvOt2n9zf9Wgg
abKX/mSM4FLfkE1O91/h2r6tHMhcchEi4+UoJzAnZjUh9U2s7kxo+sZOGXwuIf1dD7tRUI+NRXAZ
GqeYJ6Py0SQ/w5mxg694F4h6lX+N9B1YYBAq7UW9IOoMZC3ygrmN/jZKnBWsmtP/3VUrIHsFxdwg
2pXDQx36lTILz9qVWjvmje3tMlOSWk1Ckyyt6+Rzr9PPLyZYABM7RSrfqKxqFHBZt0o/emNn948k
RTmgafG1E6C4+NK0fkd4hbCalLg6j06H3rxKAuQ3xr0Tbdv4YVz/kQDZXJEkJj3ofwmiZ2OExe4s
V1PijUE13OTioP6+e3JMVnLGx7ycbxJITj5zo8kdyokQNTJE9jdNJ3/c0Sq9VVe0MFg7YdQzlzli
1RoQPpp7YMzTOOHID7VCqUwR5q0C5KFwJghXeKdKAHuH6RNvkgh72oNrY+2K9N9lkP3M+qIK7q8A
56wNILdiOcGE0hL5aozq7TrqK24plzIfyyIiLOWA7ZkEGkeMfQJmnCQrJv8gR1SbNPWo8dRs9jSY
3Q3YtKFyIuRYOzOVTg2tu27VwEcaOt3pJQwzd4uI4uQW3aqi4JH7jG3Xb447vV/rhWql2h7K8Fla
pG4+0Uk+RLNydYjDnm7k1sOPfMaVSyIYwHo24F9i7N2TYd8IodPIc4calW5NNXBq+E2R4Lc3IuJl
P5hK9TNQywhwAE61MZ7rQN7uvPNWUZlQp7mkbPCjAHNLf61viCFLmIURcvYPwx8phg07fVhnf6Iv
ADVBrJnQZIdipBNK4gg5fFOlFWWFTBqPor7Wj6zWMhYdJznHnN5UtTxTWEDoxYoL8zIDpM7wL+hv
UP8yReP8EDWgi30R7xrBLIkhfXR/m44aGzWFABmx4RmTcZUuRHNV9Y/k6cYzbQt32OholDLh7sMe
x9Yr8LROrDlu1A7LXDpRJwLsCl9hh78Qb+WAr4r589qASgJGE5bGFyye0ZSVgGJuL9pIQ6AhTMRb
cWI+jE21YwLH9xp1Y74pxVG5kOVEFdtW6xaXBNgVWPgfnVGwxOEMqO6prhNFQz1EtSuPl926+QnD
XsDLKhlnUl+BVgFjDnD5dvSXZbYsv4Y5sMbyZA+1YagxwNYgG7VEqs6hhlMYz3yv8Srya2PyV+tn
1R149aAvW0hEKlGgqwMsjR+whEilItfO5+UEMMlW4LYp6ybCYeBm4hSzTorHKlG3ptMQuW2po2fN
SejEjI6Zpw/WwvZ1lln1vQ6RZ68qeKjzaDIjI8O+IHMKdaRnCgdhvABw/Kxy0T794TfmOpcc0e0T
Ps3qBt/s5+yf+TwzG/O2jV40xUg4+8h0OoVdu9JcGmq6zQBeCxFl80UNRadQ1pIUsMt8yq0932fh
eW8Gxfulp6kKwy5dTQdOE3q7+SkVLfV8RgHfYo8QBA/6wGiCHfkQWTuMHqLsI9ux1VwH70W4AJTq
Nmp+HPzwEBXyF7fBOxSrKSmxTqO35FWanHKiul58qStRrWBENCLXqW6aN3Rz35FS8UD3ox78o9LX
/thlRp9dMVU8GwVGzeNjkDg8TYkyl3+DTpePvH6KqpqIdFYav8oG1yvt6wUOsfWFCAWy+YssmUBw
FSL7E0NH5zNK7OcrgqF/e13nKEsemTeRgxBJb3tIjmaHj7wzJln44UvJ7IdFynVbI/8W+h5wfzYK
62fvZy8g2b00RjRItjt1Le/hk03Du/KZrDu8iqD4g6zFU+Nk9stnQ4W4fvbqH9W2H9s+Q3TaeWVD
u/glI84W3NpelPB4XiqO+5sqwCifgodqVadvzmbwRSzfULhmta7VL2xSKMAI50RzhJf0IgYP1Rmj
vyxKPn4in47zG3vBFDLpxGhXTEQyg4Mby8+gjD75TO57O7TR0Q7+MP6gk4Yii/LxMsSm0yrJUHee
/aFqFBEm0vq8pCdB17Rz+yFq+E/VTYUMK+k03NzmdrJR/WdyYbbz0zr47nFitARLc6fNtuUkvpDJ
uMkTc0NbKtwRyw5itOP9jN6U56RUwS5RsAoUsSJwZHDb1gazgkvaFosf8YznjwVvBOK1S9x5qfO6
wvsptSAO+qIRoI/GxQ6+S+AVcev0OxHC7miirsuvf0Ct7DhdiGeigKRw0DH3+hYv94arr0P4PhB2
0K1bQygeYgu5x1rFLfpD223BLWRXpleN9FUW3pNh1eeqdG7JGViZPMZM7I68ScivB6tW2lm7+sLP
JZx/pbb76ICmOjtHJmZoZMx181CRmzJBX21a6DwwmqoumWMsl1GxJMllPB6pt9lKAszg8dDVQ2vd
zd9fbLNMpldm0cMA8SR+FIXc7y5R0J19hMlkLc1e1DJzAKey8A09DNrXaOnYXNbYKuxhvGhNh+Ai
5q4KRZQa8KTu9JHK4Uy7070Agn/NfU8mIngcUiCE8tqRt0bGEUok5HW9ZdviGiusJBl7Odi0kgMh
1cAdt+1H+rWFixEEhRjVomqsciVz6+bP87KIV7koI1xEmhWugh0jX0TLXJt0PbJY+vbQv/uyZPyT
j1jySinpPJwwLTB8enWJzvRCQnJB6Mp6ckSHe7btd4USyHiBAGfl+YST4DaPGWWoGLZIUthMh8Ch
gqDw/ZRqJ7w1ZwiL6t9orSe/COOEohlvxHZCAt1xm3cqQAvzKTsl2vuPn+iT8GCzv7+NO1YUSyrI
S+kV/sAFNSp3O0RvcAxbIHu4E4rrMCs9mEgBZsJPC/H0YvewXPeM5x+4SOwsXjsqU/e1em1kuTIm
yhBQ9MbFq3fvlIIR0cCYqUraWAIh9CW5sNxzH4x3cCh1XmOwme1/Ge2/kSr2FnW8H8cTSu1mK/SJ
cqhtLCEQbNYQVyAWyoo6HpiZm6N2uATFJe3vx0HF3KLDQHFBJEDPv5Jk5/9zWo00AFgJKI9+xd5i
9EwNUkD5Mx8fk/ZnKQPoy9spPAC1dee4g24sAN0uVDdnj4OU+9wgJebLSO+8ggVoM9/80vvzFtXu
ObPSoW4Xqh99MdG8P8PltFDQwm+AeAyD8H0ezHsq2kYIAPjSp9+FcMZgus8ERiELhiuVx/3aPvci
Hk1HjrFAxkuUWO4fWYYp5Y4pzDGLGiX2HNE0LM/BQnLAiHIfND9QlIF54E4oCVH7HNvlWTkEE8I+
sF7NSB8YptPA44vu+v429JhrUx+FLswzXWs0VAtKOvzhoeejxfQOGEQXMZuMrNJuWjZJAMSQuRBP
akmBhgUuUkT7zELu9kee+TpbgVpUQpkrMHG6KGDKSqNX9jsf6lqr4Og48zJ+9gzdQ94kDlqGjVYZ
gLXGmOQYDpqo33tQ4vxg6UMZB7euJz1p9L2b5LVedu6YK5YlhZSHpMEA00lKHaKsNyryV1ZzKVau
wi1MgjHWnDtm9amNxO3iy8Pzcj9Z0rN1GoBaLUH272A/q6UnkL2BDWrmMO23CilIeJjns8g72Mqt
ZAcde7XVwHTjVshDktI4QhNv3ZOg3+Kmc+wAeorGf9RHBFq4zjfh4YtAU0O3vkglwtDFYKjvPe7c
yahn0EV/KPlRDsyvYNI9whKOIyxY5rWCBi+gf/wilHsGTSec/lrcoGXyKdjQ/J7jS9McJDocdmLb
iL48bgNTUb2lIWOhDYEpRgt0j6lArTHbozqdZ/Kd/dSDHgGKXEJZ71Rbw7/uSJxfo4OhB5mbq4+8
llmpVKg5qaKnbVm1SqOHGSaR6sEwjfCxcRknyACtALbeK/bmxXQc30pJV5tOfiOkrb0F+I3SybEr
Wn+KTFhCLXjqlL2sMLWhG+5oIqwjK/z7vrPZ+4Q8peWSnoqoKtmqzsrv9hiN3g7NeMVQnAx65Tje
rqvkk08kVr1QevE0Azud2peFoDDTwZ8KY8Y8EHFjToRTtC5JKelgBEq5gz0m8jaiAHtF3bCwCapd
EuwXgiA+c26Bz9JQvZURHeAuTXS1T0CR+xu/a+QAdgwXcvzsXWp1oUgl6Ld2KYZf5hsC9feyOKA1
2aiF34DCer5XBkL1HBNDkK+Fduo5HKnyrG+ZjdMbpXofFCl95IPXpEDtiXPM86YVh9bdFIgx2R8c
cRmT21Ka9v3BYzJk5ZBkSU3VPXxeXUeDs2kPEOL/IIXOTeOTtyI/HzFYjA+iziQNYS+JDiR1wcuj
2OvWusNU7dkCaRtixtPD2zWfMrv+KMy6EdYiaXqa4KZvsLakR+QYFx5qt/R1YIFF0ZAg4xJlkg0f
Y3vqc56Ck1aLhHGV24nlesqfMeSugaxRvY13ARwQsR1t5jib7a2JFYiwSl7tTqFWneg4bc60OTLA
PPtIRaQcGxSvmG2DHqqUjMFD8cis34uns8pnDNHAxf3GgyBq+UmgXGl3w4eCtKsjuDQM+0NggWiL
2SCcT3TznN+RKJZ3ffUIw9RqXawxZAl+gB3RsvdojwkVnZTauosvApmDxoNcWGsI3Np5Jc7eMuVU
Pq3UU3CLu/gSEPdG0Npqxm5z7qoHQtVg5lzVULbUWKckB46+D4RDLq/UivpqIwrC8uYSDrxEJh5Z
WkPj0UsCo5Q79/3Xah6vhxIMLruHolG0JfZjBZMwOy5DLQWGLGaQgfWzOMR/cAdWILe/cnmTMVlY
gK9RjeASvTbqZmTzJ070OHuK5WCJ6JT50zaY6PQw8fYeTqj1DcFcWDzpUMgm6m9K7rr4wxsXy6oa
JbkJT7Bx5evzleyKpGM+Ax/OMexNbfpd6FalpdqBqUC+uj3fnX3y+7mVUSWgsgMBFIT7T0DRZNpI
i8QnW70SrSNSS0mUJXXpye85a3Sf6kvw1FSIshTX8dGhT8BvjoOfhd5yb9S3IZ4Al2EtW0j63elD
LWE0xF2lPlE6TCjjIoAJsN3drCxkBQzXG17Dge7X+3GmgZ/kE9QDPyKP+UHMkBj2NhAm8lyNjD6s
DJ1mCUhCOanfNlSw1NvnPt2nsjxnb5U+1MCxv3PALyFH0TnQQp1/4ROTtPBMUPrkfkZHh9JGfdet
0MMz/MFaDnjw2K1g7ZNV1Oqfovj3nwS0vPz0Ozoq1JRHFzo/H16hbTjPW4kmS1/B1v10V2jad3dP
Jyn9ZiMax3l0iaB1vRVlQrXQf8mt3DVgBFOKRtukqfcn63IgLivOCj+aLmryx6Wu4ZF/Xi5kNTbN
3DUzNUJDseFg+M0Te1D+wUMmx2Z+ecImlkoGGmHisFnTD9H6iBj8eSIDsLsMmimaWOm9evhppS3O
VIHxuP2RiwjQDlwKVq/BDtugxmFLKbXOY7t4vp9an1e2vzRL9TlWtZ17uhizclxotKe3PsqHwd6n
QS7YP6gZT1XZBFO7/4PHRPzcO6sXBTR2paQOE0M3n8jLV9p9zH2RFZl95mgZpe4xgUkcQ+3JUb7P
yQYbOykAz7nDMdPlocnKDcZQOr3jgA5i02utgrbxAU1dOxjpDX16XGSEDJKRdM92myEch/dSYlnJ
p+IMp/VnbxGO+Yh4yK/Tzhl8W4Wcu9gaZkQVmhyvcHjjaceJ7rvvsykLR0PnljotFQ3haZCTB0Ho
qLlcpIEP/XvcPi7u+l6yeE+9bvIRlKvMosgJz50kWTVmjNI7kXhT/IbWI81p/v9ES08dTsYd3gJj
4aB4VDpey/5wOIv1uKJMo/arzwkAVv4GG8vU3lYhkt2GvchZFENX15lkdU3LJq3gIfugKJHdf7oL
VGzeocesh00mB5JR6BFP2u2bCOtAIyo3aTArtDt9c41h/hKeOdSd/drmYTi+QbNT+sy3CrNwSpNi
wuUPy/jxseb6MwVvmOFhbYFIkdmFSBYLuouHBlO338ioK5VAJynusLqLMsh5tqSB9s6OfKHHcIzk
6JYbwtF3mG0HiPZHwHCTphbeU+QPKTFXgJe5wbSum+n6qTbKdnQcXuUkLk8h3tyj4mP/Ez5JdHKw
u3KflhDziFpbrzHmhdcbaLItLbwKw8MUuRI+xpuGBeQaGHhUGaT5znZx5k+fT0xFPGXlAHypMLxV
lUJ9nrSyvD/pTT8Qd/78qoFBqNrv4WbG/rmRhCw3gOyI51e6N6Fk2kOQ8FsGQP2wucviMGXoApAL
TfwAGh8M9xK3fB2GnbBaDBDqi5ZScwIUuRpm04M+f9o+vWO6NubCYyg8xg0o0bAJ7CM+u1eD6TPQ
lys2haAkrNoyhbvJvNdFlZwi8/mxK0f0JkabJGbViZdJhS50BWdSwKLKlWLyGIwpx7qeBibapJUp
/+Oa0KDIuyJAQrQkSdun0l/XmkALr6th3Lz5HaKJv616xYT+Pz3uweaZ0GYtY0Bq/SvRTqMd7k7R
Lj0AEFUh2Eog3pX0/zSDl9iL1hkM18rFH0ocQCBdLfFN1kNscKKKDcQU4lA/480WJF3fA3dsitEz
hLc1sC0CdUyvhufykg+tTzseLYfW7mych00Ah58I3keAFhWiLbWdXDFoclxS42zCw2twtxv4w0rf
eqxg4KyT9Ykx0fLg2xfLRv9sYBDoCiOQ0KbtLH7tUgWFY2vD7hJn7Yak2sQDYkDLPvxmLpqSEDRj
Dth8ACuuK/8y4OJyXPJb+SvkFL4MIxUhfT6nOsOM9mUxmbsMJe0j81s6/MSPc9yPIqVwpj3mC1zJ
PX0EajVG0otVaFfHyIhHUqOhH9czv8J1NBa7YIq1pgLYHv/IK2K5pU28aWMuhaxYmcASKlY+msT5
BRTHKWOf4nT/U4qytbC/N41JCHEv+6VaZ2wYYwooboqkLxpFjx5kYa8RzPAki/4J3OJj4EoqHBdE
kyCVGsbl4nIIvH0UiFYxiW3xyUBGAfCveRrxl1dY5VOOp8h0kmH/o96RiZFH2HCGd+818npplZVE
yaKluPozJNfvYpuD3JXIioShhUkbvEexTd7oUN+5PglLF57HDo4Xql/gOSMoNdnoJEoFKAvlBV/y
P4jWX+8P60P2X+bN4XAEp1qmaHc/sTssgMuz2jGSNAdM7zKMG+j5p5LLPvPQtj+K4RXrVQ/rPomo
HhgLgcUEI8qORrT3wzMzzrBg7Ok0MkG6nRKPh0hMpYS9VcMsNLKz73EVXgOTIAhNx6Tpb2P/1Tbs
p4pOV/Gix/wbK/PcIDV9WP7eadYwq2vJseoXLdxxnkzf8Yu34U5SrSy9iSF+ykzBBO4cRTBU492m
QxDf0OI+SkftM6/rWq32k55qxqiNUSqqJ0hChBt7NTXS31FRjMwp7dnBLRm7vMQCY01kXRgx4PNu
4Tt56hhLAc105vuoctD7JOiKJCGo2NqU6OGwlfnV6AlvakoyO4T58ud8mkM7UB7PseKUShbZUDy5
Zh9IbgoGTECWVYXvda6k0rpY8kVAc3mTT2FO16g2ubCyJUcksg8Y3D5W4vpCJyACgsbcUOOHL31w
SyBeYrOAn5a0PBFJy60WDMxbv2dGg/NDDM/3GhSHf2hcrHcF7r1dNtziu68gpp1AnpQ5c1QXzDPg
ypaB9St9/zBJbS2AGlvlLL/kDTOpQIQ1iNX9+BusrSGBKUMNYQE/Pe08H1QGoXVDsZkNaTrmtIch
PB0yHU+THD8PT9j3TfBqq6DHYsoEqv0zSvpq3dOUX8AiDqiqHPM4b9M+gvC1hHUSvPiTQ8iPxV3R
puE6tj3ENlPJmTlNoGfb4dFgxXudnKiKJ8lxtSGmqEj5+NUuRDUn5YuWpu0iWmr0DMvUsEtkjRUa
AUEY3+Nohj6md4fZdlzr0YiLcMeXoLftXpacw/+gC7k+tqoCDWcaG8wRaELwYbQ0QmWzgyMEfEdJ
j5p90XuCFKU6wZ0EH5tcW6//P/AdnMN1AKJ4yOoNAHCsBvEAJX9s7Rjc0IZ/iizUTpxNLONlWo3G
quyn/8lS9MsXqpj9Qj1AhUwjyu0w/7iCUL019lCElH9cdsI/1WthFvb+s6iYN5JQtZYyMl98JUUM
fsDmUVJjZ4E/LKb5vxQ9bEonetIR6eVeOpoGBaT3uNzv1JNHJqOfeyAbKy2UuYIANje2sAvaerFB
HtSO25EW3dPXk1/PkOS0DzUFHOQ/NsJtAh1JZRQXNq2S926UhXEpVdkLDvc8Hgw3pTjiQjRrikE0
XqBlqhAuuwikkHqiIB2xWZ4jUnRCGoNo9D7oYVUH1Y6hoD9+21SNFOmOa2/wc5sYnGVfmQgPE7eX
Hdc2ZOU=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_20_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_20_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_20_0 : entity is "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_20_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_20_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_20_0;

architecture STRUCTURE of design_3_c_shift_ram_20_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_3_c_shift_ram_20_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
