// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_22_0 -prefix
//               design_3_c_shift_ram_22_0_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_22_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
yURHbZ1/L/jdGwFSUy4dj6pBpIiEe4HxKvZpLoWgLFc0P0eZ/biGHi8QPBF0Le82vRnC6C0IZlTp
HIlohhar5HSGEB98i4resEYaIERAX4yi0hi8MHBf+xa8/YiVwF69vj9sI6hD8WTjoXNTGnL+OkDj
KpJ5l/nX7Gs20WGVxo0BBnAJekPbs5WxI9AX8AHuQzWTubH5DsHAhelZlb3jwEVXnHllck0PObPV
1u68XWb7P/k0KpPSpCcgzWEB5ump0kZlV0sbjZD5HUvuEB7/OEjaa0sJFnzNX9xXUk4ojRfho6/m
fle5UDDZd7K1t0tH51Nw2V9WpTxuD3b2oPYrbiuBIve6wvSLz1S3llDL1klEO7I/Q1WsLZhPXIP4
hcFjyqiY3x7xaRCFgwvUgoGyIQZJ7Y+dODqGXAEUeYRU9Z8XzHcEdPlfZfDZx4yxQqtG2sH7bf3Y
XJJqFDCuIKTrr87PmrutY6dWmVB+PkTmPXir9b/KMaeU2hu2qNkaB1ozrFm5u4iNg6+T92d8HqCW
JsAmEVhqgtYQNNhnRGK0tAi7WRICVS6scT6zdwPH1HZRH2WD0RVnqNBzgg8FB6ZyBz2ZU1COZGAM
jCY3pgVqWRiL2pX8i4p7v/4FtLGSt3iJhuJz83Kl9EnSlv1Kmpuu7giJIar30wJcvoPocUf7tgLw
bibx4C8REJRuEvQTskNBn9PCNG9QF2s8mM74lJt3bRrujAqhOqI42M0aV99LtuZuHFm0qgc+0625
O1loIt8nlBaOY4EUIUjF9tL/opncui4jg6Kzo59HtZlgacIgkMZ1kemyby25RVINequBJJ7i+iUG
F/3PSrCgcmb+642X2aZxecWo8spgOaKBLuvsdyHD7uUZjej8Sx7eJWo1qeAVE5ecEkIMH7farY49
1Wp4vQW8rqZZ8wOV19szDnnpn7rwwDuNQ4vK6gcuK/VgGbq8hOqs5EJ7CVoulN/5o9QBpBFbp5pR
fuEZ41ryu3fUR9PyUXTGmoMJ32+rEhuz9YXpUjDWjUb8I9HXj4mvzs5frqKxC3OniXOi/C9VC4ti
9XPcFaVdvBGWYi3U9yGUebUfKs4C2GZliBGwzHTqBs+ngNG1PJ5QTHSZwCNg8pxOewSlPb5a5ksj
N7PxKil+lR2vvUDSEN9qaJ8B3d1Bd+SIW4tUXDymqnDOhL/SmUbhM+3+/aFqzr3pQ8L0TqpcfIUs
RumdOzBGqlLufYwoSQpas9O9d8AWjh7DbxCD/aQSlQYb6yO2fkZshhwfHnXILStLhvvXVVuXqipb
k+2KOR8YnioYAJRLOLYkUJZ78U5fe89oKB7IoktleeMGcCF/VUPKrfIZ4uID8ow3shxje2M9OYB8
sGpEQ+bt3+azGjMZvZ3R7hSQ3YM0k+eTnShzg7gWu3h4revAlCXjjIuC8+O5NRnjwcK2BAEshtwL
L6qrtHx3kMhzFAH467z6x55B1VCA6B61dhNdVmOjaeu5tmvxvSiOdCJRP0d4lEsJZkpsVj6moFeM
9IpTTrYJA4y5u6Fp+JNuOcV6ZewoGjH9c0NjM0IK7GQZ0garbb73Jm5atT54tBCYpZlREcVtUIBR
FloTdIsfqYQFBQGYLPvsrjKUOeMZvk56SqF/fd2EjB2aYTdmIF9iQbioXu4+K3nMHKVVdlJvT6d+
EZoR6Ep8aYTQTUGsVpLW5JLNoo+1PyGO4q7d4KgfR1Oks+goXJu6TxWhKXVRi4Y3SJzibHnSO8cj
97/uTSSCDosyTPRPl+3dNBbbmd6la/Ikl9g19Bh8K9CiVwC+O+Y1MTIAlp5YEJs11nbF5dJCiNtm
c6C8ww+MjuKUxsL7DIkLjZ1IbIwTd7IEjIAc2IlrfTWEsriuFKQVOEmek4X4WqdaCJzb15kS9K0h
hQnrT5Qz+5fkHASl8bpzGaSPaONhQVvgjRpfr8fPp5oXFPfdMsKBtKkx6FFrvD6AE4nSEqzWY+ut
EbPK8PMh+Zb6YHyx8DHs8nDbgZJ0V1i59aPMQx02m+u3iKH1wflJUFP5ot8zqKopjmQ5TAthelnm
/Xr4QFQuG4OGAZ6dfccinFAU+O8XhOFUkHMI6Ch6wlbydnOrSREi6Ppxlcpwidm6Jme0UgIfdvPE
DB0YMw3LcvYal5OY/oRgme8bjNhFhBZ2SJKZNuo/39S+zyna91DHI0mCin76bmoMfO0Uvoc1lOPo
psxZWMyl90HJ8Butcfbh90LgsL268Ppwq3oUMDVcHCbfi+Rc+pCzuwwAq3j9kACWrAdOgWvVIlQo
jdoQHbag7LwcEnosHsB08pYjx8v2w8Y92EWryo8bOkRtJSU/EvOzBYnZe5kHvEsBtZ8eDyMOVkPe
O/u2thf+R+37LT+Z+s1XCJwFDN6Ey6nMwlxlLP6vx9Id3V7RGZtppTkLzS9kvGtRJWLyUZ00pSSu
1UF93g9j9/1YbUZxlM1g/4hNHWbxZfKyUHR0GXWjd2Xp4ULSK2aSLc5201rl2OTtd3UVOAND/kB2
l4Fi0VOivP5v2/+u/NPZAwEjpOU1pgUkXj3e458KNBgTy16wjmTMfkTX8PR1foyn3O5Qmw8gQiww
tfvdDkdvCT7to0q+G9JYrGcSmsvYMPIPydqp1zSAvSRE8uwaczN6MA3sc5jaPTm5czcDKia/z3Yw
xt9v5pOKLUf0JOualUOz5V0wjcubrTIBBFjCnDZWqmQM8VY+czFAxml1iE8N04vClUe7QpSWMCRI
Imcfy3A9/sffKpgSIdsRe5HOW4p0fGYfwlSeSVjyXYp+jwjinpXQZRz24n6QbdxeMtxTTrcs0iUm
OgZSHuj9u0mzHoS4HRDQPQ0jksAAxWP8fqzgfSilDL/P6J2HpqlDzhf1QjAzG9hmFL9cevdLB1X1
bmMTvyeoETsYStfBKiKoexBhW2nK/JDk1wUOBzGLNfwQ7j5YI6P/qZu2rgJUM31uCs7q5jch8ShT
/tHI67szXGAHgPPmobikimwHBywqGKorwZpV2qMGU1EcO1FhOCGgCi2b1hB5N7D9EyyVkiRoxlyZ
Hbt/VcsTjVru6IKmmBkAw1DTEBOrgZGhwb+E07XfhMt2yXmTDJtGCSW+YFNxlY2UGFexK2Gh3/Ch
Xnl1nHLgjoFoR2DlMQGq5kCTAtqax39kPtGKq5r4BH8G3fQ60cKVAaG01pznfkJtKt+a18xwmgk+
4qVo9JY187kfN+nRLsJFx84gIyNfWsi2TJHPRYeRhPdsCzf61R/rib086qzxUWqchBRr5TxkF+nc
TjFjwrOBoSgK27STRlyvrfgU4xriAhLN7KYdveNbuXwMs5PLSjaWTh5cW/X7KIoMI3tcoz9Wpktt
uB+eId9sSuPdvYULSPhSBpbvI0X4GxJgiIuswKPjZxYNQyf/IRyNUIO3pOtVUqlKZma6Q4Kzvlxq
dqgHVETWLNzEdr+GQsZtOPCYWW+MdlpD2BxxLZSwNrnTBwUP43bRUTdU4HXRCSCV8JuNqzudnk7A
XG9oTHxnzpRbokYdrHY2JBsrg2hZ0IkaY5mcRylqWICNv/+/SuYxNAHYYqFo2V+habtGKAY1SIMN
KB7wjX8wuOgWug7ATeGzj9uFXeixhcAlQ9l3x3usq6HljIv5CtxyVIb/r/WiBK0x4VB0p1ZZaukM
lIs6qRcUiwvysz/GR38K04ncqyhjgWN4kjdIxB79/ENkRcHMPCX6+76rVjWzJcw+XK9ODHu82DhZ
gaxJyDI1h+ctzRp2VHGBEmkj1lVTZ6M9fBArPFSaxecmDtXxXZP1hzGWp40CSXwNR9HIPoIRC45B
J4e5tXQFEPqOU+hEqik/zpP0C5zedxCZy8rDp/2x6rx/fjV35m/CH/rpSIa6bgjPZ9GTy2aWjcWF
i0u5BxgDuvbdfYFkk6BfiXIjy4rS01XYvYY8E9ojaSZMzRXRYvtInNd5LeSB/HH+tLjdmK6Yr2Iu
ymxeCXINI/f7cGmeqsiA4LioczaSXOtUoEz6f5/axdkJ6pUbHw8U/qlATf1vvRL13ONvMtP1nFBD
72Qa2RCi7RRy/BWs/K1t09qijNhqCcziA8tz5l63cV9mjj0YC7+O4of5/JMiQb+sOsFSwznP9AoI
TNOPBS7TUGA1GIoLSmXmwoWdGlcdiLkPejKUcr5lO6TBb9kF7DxJ73YO64rFmWvAmY6/wlVB/mjb
ckN1yXNdiPLc/rDw4GePM+xLUNCORbD+y3o0DjkLB0yGra62lCu5kSbHHbTTCpw1HXaHDRXVeBDh
Y3R2EWfaxmAnCyUP4/YB7GA8Xsmo3JvVSguFQdY1rFu1CI3EYu6ShNOPW3GeFTaBDoSBaH6Othxi
8iaPcAeWGleAQ+y7KnsQL9gGFUkawjdDrQipe9XY66Rd02E8lJjdsTjcedwoYY8jQxGEa+clksY3
3T1HdxE3avBO3y4Wr1SPftGWvbcLLX4VH6UJIuXUip67djGnFJtntgNQi1TulRS0W88zfR//h7cI
HTzRakveGclUiaAadLubiGEWzYYVAj5zdkL5mh42HkBLBQWdJFHrhgzBAiLCIFp23oX+jeVL0XIK
qNZCt1BdLArWqmzLYUteke4RmhhYSfIoFSbSuP6d07pIR+VhgmIwlFP0+bYp7ZZEduEIJxolllNH
NxGymryW5DU2CH+RH/hk2lQmZloB5V1MJbaDOQzl8VteL0vrceQf7OQyRhDNJkexd6AKuPLjG93d
C8PsnEl1XBx7jd4VE6ZxZpG4tA3sNLvgiZPctQ9CKEKGSGwQAgguwR0rAbsuE6vYJB31uT76ehYf
Lo0iaGoVwBp+lILn4liurzjM6AcAWiaKx1Lqxn+i28IEcR7AWe9UPtBX3L5aVpW80S4fMnLTtJ6F
18Rb4uLc6Uy9ffkHn3k4vBO5Jn3pPagCcDXs4/shcYCV2pfUjkNbqoZ6V1bAYr7raHVdU84QamuT
lp1FBKZZdGqDMlG7rm2N9rJfWaZ8WJS2Yz+PSy5xnY+mBEG3oO1ZCbbO01ZP857Kf4m+8zjL6uoQ
IlLLHNDKwCzv17hKaINqAyXTrRbQNaGGUo4ZPxlJAU9Y1FsqOaRCfoXOQW7v2lw7viA33tE68LxX
ApdO817Auzyhv68+zbqBRhZDRp7z1ObK9MNN5Tv4vrbXO7eW2ZVHNZH6PUHeKNWDECQ2kJkOalzI
vBarbxjkSzwSnJ6Sj06pEppqkBCU5+ybXs3W9ktZ9CENqoe/BY2b5vQ/v94mQWDZ1/M0gf5dveQd
d6dp1hjBvLLmd/m7Y1/eAgIAlq4bgXu/cHh0GAUKN6FEIx1QHqySJloutdaCvxUGJTxmCbMsIe/F
Nckyn//YoMS1lqS491H7bg+udzuopM6PLx5jlVHlAwhQ1R44CU/5hkxe4I5+jrOe94N0JV+jnRMU
F6tl0ibu1Cn4WOLp1xdkRyedZzT0hNicL0LMkYXn4ekXLGCyjos5kJWi3oVUNSJjMp08RtNTcHL+
1PlMhY+xgnwxWI+fIEbDs3wwmnp8AhB+s+D5MVbYdBvdwtYw1WglkX6XdOVO3CXmhmZU1iEnB0E=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
