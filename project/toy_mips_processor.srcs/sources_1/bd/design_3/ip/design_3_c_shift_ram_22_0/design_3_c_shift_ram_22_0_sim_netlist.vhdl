-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_22_0 -prefix
--               design_3_c_shift_ram_22_0_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
NTRARAna7+PbLnJ/C5Fc0bGMFjEBqEVOsFCZziHlFOO1XH3d02rX6s9CEUqRqqyX4JmajFgrnKDZ
SpcNjKUTWrbBpqUhUVR0edmnpQ9QnvRrEhYeYWdZM4CWZgzHVAgKrkqurq9y1ZQJlKuVhP636bVu
0ZTRyZeUwoBCLo/z8J6TsAKYC7U6SddrZ/dtDHCAr1h4ryzXpoCCZOlNO1YOdA975eubIAwODsT6
fpXpXv0c4wxdBJ/wc+RUUlMSISoeT448QiN3bt/6pOa1JjtV0rMnmO+Z93dsSTq16C9TibVs8wG+
qtlv/fi7OvAH+vlqdrJxRVpxXWVJoQfP5W+2Bq18/p9s8awsNzWa2nSKKqlwdLcZmD7Q5NGbsKnf
W54n7rE4yjsfy4YxCxHTbtOASLdhlyMafNOBgXc7Aot1xN5d4lA+UZvn04yIM6jjtBAZ1t3fswVM
fF/cnX2sTqo6VNYI5/K16U+k7UIv+pchX3Aw39ouzT4ca00E8Edz33WHI6NkSc0jBEBeAa+MAzAA
i+Hf4hvfnvpH1/oF8eysQiWTQBGWIbygOYkeL/tZEZF6iLmg/0GQJ3zlFMy4q/PPr6IldlwAd0Tg
wN1KjVyUcSM2/Fr1hF1faqSBC0tLuh9QdZY2djtJD5i3m9QJ9+/tl2PlM/xCy353ytFg5szSc33e
jdmQXq5rvSBy4xGVDjm2sb24+g/evkmXyxOpjsCm/GXdnGqBuFt5FAseG5wUMXQxWAhgf1TJJtKr
rh04NVv4DyueZbTle0YOr6n5kKZcoBIpKXOki5mdWRgbyeCZHC3uO5qC2G5tSLvx2ZAvchpRmxi+
Wjj4jelVcqAaXOyGIB4yrrhwohCLG3QCbW0uhmFrz/KuF8JfwFUNX9Cg3JT+Rm2zRZGgmCJsACx9
bcauDdZixey5ytxh8RXh7vNwzu+FpxPfOIfvyOCa1JNQPi0oKmk0ADavM21pvqq9oph8epkp3TfG
OO7MOtGc1yFeBXwXEY7RGmt5s0OG038XJNB2PJctu6Q8KTaNd5XpXmplg6Bmq1ZibqvycWgk8PyL
5UFz8XZjikT9/qPgD8vmj4Z6DBWi+IU3YSxayIFeQNm0Z9PtmCMc3IAGK+4qVckEsfWYRq6NRcNI
O5JW2Uu/aVm/ZknHw6nlefAukuxB6qzd7Dzbo2cWzGput5E7nt8T1ByY2R6OWPYY3+7dVqJ85TmG
99xjBxeGMaYQ7lTmzak4Cu17zqpHZa/es47dxLN3s2W9JL911Emwyjld9iYeBO0t8Gf+EoYJDcwp
HJNuDvlIeFx++Nn4Vxb2zkLk1eb9J7HoXZ1cNhhcEktCD3Hu48siCXEx7BK35VqPNaQ5hAfnIkJU
/EmpiwOow+2Nmr2XJq/XUd3h1O2Gn3TQEtjsZiqQq9QaknFp7zDzC6ZezFqGvmkU7Qg/21cCu/X2
zngqUERjhaPfTlZCuulpbzglA/vifG3SqfEeLbT/4HyMDivpfCdHrcgv0R29AOPJE7Ho/eTaMyTx
wgBkIpzA3JQll32EB9zTEo3zXyQErRs/IJuyuWD/kyQKUvv5NjETb7plxJTBen3ahN/HUwbL4LF3
nXVR3aO8KMXVjTlP4sqq/pN0D00disBWc3RwJAQZMZm3+KnVo/k+R5SIJD0w9yoLA+xdpL8EzYRw
lJz5aAJKP2shkBvO9SbPJhDRspA8itbV7bVYvLnum2taUX+5FoV6eVl7FysCvVXXIiidkFou09jz
ik9edb7PSaq5OtaafTvoQ+Tprt7tfnxWk+0m/EL1BwE9t7TdxXKsD/gJCQnRDQqgCB1jezyi/WOJ
IVDEYnLGR+dn2tn1Yu3oR7IFITsntNNgy5YMA7ykgOKxiKrj0clhMaNE6RdcI7QbZ0Y/bjepwk8X
JBXlZ5OeOp8qqLSWtssGUdUcee2ORtccHy7wKYo2vk6MDR+9rI89mAp4GEi2hMpW8V0QCo69hbwh
lFzB/NoJJlIvXK6IHdfvec9CnC0CyDTZcPgcvGEFpiQxRAh8By4APvYYPp8YkK60f1Bf0aQIJv+v
U27Gd827nie0LMuunT9xV1J6LgnCPvh2+JlQsvfIO7sjcec2Jfog6l9tHRDtaAkXjV32ivEUYKTZ
douHxPwTzbMDjT0gvP0jqk3YjYnSZ6QAReuagIR/dp/WROuiAcM7+za1C14KPKZXhgBOlCkR1oJm
pwWSLj6UZz7F0YQ/PTizoQ3jRJccJ2mVE8WGAPUUcoNiFSAxaNhz4eyNd7EmlM8WozemsGktUoDF
RSziNPT5bAvkWgujZoRFKbIlF5rM0i8kxqvXKL3FOxeW76PQRo+LgxrbXR1EaA4lKS07DOklUc57
5TFv5w1X2H/QPFUTjCnbgRYO/yiAq8BNbPhrAIr9UqKY6WUo+bImFReOOS9hQe9LBWhOEG5bN7wr
XLxgPK/yOXpi/fbgIbnCRg5QzJV8q+uWeNcWUy6+/OHHkNUiGxE7EpCPWSJzO9fhayRxCskLAuuK
JDdBXEjUhkDiOAakSzwkVxhiPc0DEY9X/MmPsp2ciE9EgBt0EPyv/ltPt+g+hBDikUf0GQNw9LuX
2ItWAtjR60HmBF0LNgOFwID9vknBHUXJYuKP5RgL8njhx48OenQWB7vyY0aio2IJp07/c3tdmi3k
nwiCoSLFb9nbQT8F8dyRWUPmHiYRJW+fT1RzCB6fs5mD6TufcZ2NY64YMSlqgoQxivQ3VfS+rVWO
d2hbCRyLsZU/BJHpoKVf/ZhvDJ+aunQy5e3FPOC3r6PUMoJ2gz5eZmE8mgZUWKRjpbEY0qGjeWBM
3lpbFx9bz2AAItvl3x8O8wbAaepRVC8MpwXOSLHavDTNbRuXg7hbJ/eK+HaHc0LEXqctdSAFvQgs
2dOZJOBlw1Tv91QOKN/4SgQhQ3gNcrz5O/2xPRszKslKXx99uYilNdnwdHE1N0T6id/FFRcO0M4I
lbSylovv8Mt6AUqOo+n226OlO+U8/5UczQtDFLtIm+SPeww8RLmH2cK5vs6e1OhmRAa9gPstbinC
KSxAzRl5F/EPFteb9s4mg9knicYW4rCDk8YsWm1KTmEumCb8WxSnrhfES5q8O/6EkTGlPMbclUsI
Yf+BUxHYsA+nB3mSlO7CxLvDXVSKDXlmohOwyGZj2aK9+gK4MC5PhG4OSra43rFK9ng1ALkAwfc8
qA9T0ApXGyUlDXgyjI/a/coJs4SH+x6g9oKH3qgli0bChDVn9YEEzwW79GChAN1O5rdAZwYN+Sii
cPey+VbOD2JxpE86W0/BTSqa79j/ufU/vaAL4NKTsvGo8G/wqOwv886fL5u4NKnw1VHQjVRxJmpH
sAt7o23Duv5zbtt+qVTlMGdhlgOJE3cbd0rPggFWCT1Y6+mRS6GTjbiQj9MgoZ/IKjwAIbGvMr4+
8IOMGCnEtjZSwU6luLLlRgzQZNm1tGmb8p2Ramq3DhgHX6v73tV9UatyTCQv2SNhGhnTMq9zqzX8
bJ45UB8g37Mx5qNTNkz9JYp2PFB8w6qnvApP1IZRMqIKefQyPTV2R3UtzfvoG2Sa7P6aFH5jsTKD
i92u4ae/TRYbH0SXYNvncrO7NBGev+1+qjt+svHuFCjPx2RbsUqoXhJnUSdjyUv+LCy5J4n9MzVG
09HBOmWDqjinVe87HQkkc30OOV+8l3XmIPO4hUaABb34ZW9W4WQ9Ffnfc2XWR0BMzxGJOWwMdtKn
fvxOZKRRXYw692lN0UVoqHVEdHYs8XDi8RZwQMFVAy+StUVLqjO45rFcEIG7bn45mPyhfw2toaw0
utsH8QwYuOYsjMoiDVwINu+tRX6tT4p0zp1BGIpZMz9W2CtvDtILGqdhT6SmrAfp3gIDREuMsJti
lnDUxbNfBlUSEXj1ZyiQGcIXVgOVZJdxHmCo6SfL3hn5uxAjk98f6k691RgIWpzqJka2uPaeEurW
EoVDOqOuAvGxMy/0cWguFsyiJlXH4gOBYs/GD84qKvDOHWBcozU/4+ZISPK7yuA4dMBW/jlf//Sg
qqWJ3yfnPer+QYdAKXadyGU0GqR3+b7xA8qXaohNh3gEak3mqHOeJYsMkivMA5H8DA47n4s/f+uX
jVm/ydw5FW8/YwdqlAxyYcGcGwxzN9acONGAeo+JT7qim4z3Rv7juXRCXjhxnwhiuEPpdDQhqt6c
FLLHk8bToA8Zrw+fZky5B8AaSemtWnbcHHYpyCpyUsjComh2fXPJlxcM0GPpJ9HfNm/lDGbiHom+
L/2LweT1AYjC1pf7j4zo+E/D7ywkaYj9wjQQOPDQV/REqaZJ2FxTLiEKw+k9fq3SfMLwZPh1jAy0
t1t1ew63LWCM6k3SJHJ3tH/Ha+nEP0cQo8MnjFL+Jg51VLr1jx3x4I8wDlfFvOWkoWHKH5ZZc3lN
l6WwzVMe0CGuGF+aZnX2BEC8qiUp3/svRPVyk0S+RpnPQnsfFJ2QSjIY4z2N6ep+M/lnO37sW2Hs
Dbl64M8nm595d8SXVuNZTes6P52Xc7kd3i0vGkKLD2BgLWvyaohFYacuuWPBEQxB/bR+B7FA+Y7g
7OXcSKJweGsebhgIZBO8foQtelA1IvgKO0q8WHHzXe8Skpe6z1UwzTj/95qZ4PgYOouKaXl1CxHB
c70Dn2/MAfE/7EOEhj9trzad9gka8QZZpXPJVoRYtcXi1XaKv27UnFEY0HbfxRxSapMfnpMMW5PB
kNzOAwMfmvFLGGXiRN4JOXfaYZRHdrWn2jaGaBOrWSyQpnuYz28vVqDhMTj/cCRCgTxLG+hyP94Q
/Hcczlwmp5ofHOdZHBTngwmw5hrmxwWzjAseitUMa4Vtn5iWfJ5EIuXrn2a1KBl6ABRiZ5A/zAxO
ZilpEO6smQxkPR+Rudb2vSkWMyzEKrQ+DUrYNPXcB2mFCs84EZeA3OhzVj+0l593CdliNZR+MnZ9
gnDkNsPUujsUfPitxNrdU5AODrrZ/uVtWBzN6hgbwjypSdGOs0jjbvwPY4ekH3xH9cFJkXg74Jv+
aa9hrB+H89V2wLGi8CIudYiIApm5epF/WSpP6VWQMrnxIftIeNd44sNIa5SYTfi8TrOyJGFaAHpb
xcNuMt4+uWBF//a85u27BDa6b6DQAOoki37bstpNTTHVobDNohBIvxt+GQQrCntBRyussPrG1JfT
2bRO8QH8QaZ60u3W6QDJ9If8VbqbNiEQOP9OT9BX3VCwkNmDReowLqM3Pqk3wEknXv/W/tYMGrlf
BCPNyyjlxXh/homudMdBGiYCulxH5KfYObp1HRPTWPgy/EH0BBx7AqP06m48Fvx8Id8wJCse+htJ
iqTrgjZTosnTiylUR31+7zL9rdoPrF+uS9DLC4TEfMxw8kS9Qg0gnGi6gymsm+w9G6Lm5xFHnhUd
Xw2YzhJG30ZKtD/xP3IXV6oZeTLFS5nzHA7bWWF1L0hHusJdor+vGJ82zcyb8x5e1/Dwku/QwuXb
wjtTm43nqBsiI80jWiXKajXRzLyUGVpUyXXnhZ4ZqkHHPRoUFyvgoc5vn0x58u5HaDPzF+nLJlmC
LdnPfc4lYwHhi9I2luQ5XMXvOkw25hA58vr3nOf+ILm1HGznD4XsAIsr55k8xaGSQmJFoEzaLD7E
cRLwBgnhDt99daZonMn1+UrJPUn3EG1cg6BmbZefcWxTJ22MeArzBY4Eu+D9VkgZRhMe9RuBOtFh
5NgzIcSVgVu8Pmwq9IOgHxzzCSTLsFM8vjpY2HPAyQ/QHhEYNCp5RP251E+k29EQK1nqJylIla4h
cOVSNUNe2n5nQZ5cQGffhGiQ8rbZ4aptzhWiQRBHSI/Cprh61mXRdzXZutukOK8dt5Lq2pToHTyg
W1KslpWg0q8/tKxrtMXeFIifuKhRWAiv4k4zDTWvU33SWaIFN4RCktlxmMZpYAZkV4aQ+VxfHt2N
D6WZufj9W6oIXoAEPGBUtPkbf1A+6BjLKqNe59klUYTyv8aEuYAL98AnwpLEMe31ox4+TFrL65E3
iyHpiaF2rVeNJIHM6+h/PxXlwNz0YTmcbyMUri99S7GrxGN34L3cTyNZv0/fzVIwboTZfQbbbHe4
21RPXmBEF3+3eyoHty4/A1diuHAOwJZosC6bcH7Q7Wyp5Ii4ky1NrF5csIRLpGFz/nXQi/TMmTow
+SAqnIeqgpVi33/1kug7D+pxZsHwAwYxh6kPqp0IBzwfzQkT4XiEZruGGOA1zJjtPAIERQaNM21G
JLy/kZjDTHUDuzWBVX69rXYGUeGRtvchxroJCBSRwpQMtJ9jzyDtFpuXT1LNgx8Hm3H47ecaC38T
yIDy5Sanqd0gwpISMrXlJktPOfT8+qyxtSavnFBIXXPHkpxjV/KeBh+Y5bONggOzgMUDOG36lwHa
OZHuqiOOBaHsyeGRic5QblwRQRY3GNOs+LLQXjtSnOGPqvdxZMQ9hTc1/fOSgMqO3lF1ydRvl/Tl
HFbDkAbvKo6+ursiZUMtc8RH/SSdGrttYZ7vHXkPWTqcWSdnPBPWcapXaZbLVvh691hFX/9isjYZ
IVkr3BBSyqjDBnkWiPg0L70cq7ug6ZP4/SNLRUYevPLk14zfyys6dXn/5q+g5PS4pGVcwHgu5YTE
BZa4e5F+7cWNBsVpZ1KsUV5DteZLqwKoc4PDyeqx3R3vAfedmTfqUuRMd5VlwSnq/iVok0U2Nmmd
+PA9WbC6KC3ZKGAkln6jgJX22m2hflZMDbSQNztKYxVLTYCQGLY/2mL+GlymBjalXeCcDRqxfHSu
Pw3N0FPd024Zp+rdWXtxC3HOfVIHhem8jhF0uKyG/kqb2gfGeARi3w4pCyyl+Io/RkHyVDGluimi
0J0YiU7UnjuyE6gHi4JWS0hnFePVIskubxg3BBa80lyZQmetBix8gvRzzIpkGVYaoXiYR5AC4hjN
TapkdyEhLvnSrpysAyJ8HCvaYVrfPEw2Tc0HGx3PZ8QiIannK35B2Dac1TFw9hkeq/ZQiqniBKPY
Ph7E+8J46bMRo2zc49IdCst4lp14lmz8YbafqMRnt37ezYCS2v7dDu0rP1G17Dn074uPVzfBXSag
Qov5vPqTie1A/zbN2NRT2Bp+iI6FM6UGYsKb4yeymzxAmP/9Hexa7GASi6CrtgDqJ0gSsiehVWNq
FFi3equlfusrsKyOmn+dt+XuYs6a2q/VK8UODpkliPbYeEQL9VWFRa7jAuZzH1/l4oL7TdRrQcNe
IVZOTmyvRzaMHQq0Ce12APmk8DViskhuKeawdunV9v1ovBtD6bYpqkQ9nDeJ03WB5wJwRNgRdo0c
7hDYCgmvAs0NBxYssZDb7K4DuQZGOH9WznIgefBAAF24W2gCiwO9eo5rnrP31np2rW55lOKC/cTy
OND44v6BTvtOp73/CEX9na+hKBcL4COTB0cgEtI5Zgw5XtGG+CtflpV8zDuu4+2PT6GQcRrX8WDn
Zap13qT8lDWrEuseQ9cyiYiO37Uv3jCk9FBIu4SdqFcB1nK/2WRoUa/3O/PLdzbqB1pJ6Ppz52hi
sKR/7vA+iXfkqd+UelEejbWUhIJ2mNUYOvPzn/Aql73tZq7oBS4Lj1W6mRlGxAwmKqLtnlAsFkXj
TsM7N+a21hUtbXkAqpPIQZPj/oQe3NNP4rQ0fY5Fm1+1QOY1uF+0e7N/AcJImWBulTyANTOEah7F
/Zww7Pj6GMVRukhgNc5RNLwr8iQToe6nxozveWBX2JUuCy1VDfZIT/SQr2/w8kt9hGq2rx5j4SVe
rm8qOSSW5fmoeucobmIIonlmTep5ztIRcntTxvaU0Lp+vou/kdjUiumxL9i6T9tekvIFaVn8/E0m
TOpyUvLvmW3dj1LD05SU5eqhsJ9pBKb2dBC5Y39K7yse67AR+P4QZGtqZDwasTJMmOLyWCE8k7Ex
Inn73awXKhSShX1ziRModXLn6te4h2xpFeD9XKye4PQsCm51OOMYTY/3wJe8SsmsZgAxbnk/Jarw
jYEe0LBbB+xVWGPRBN69VZm9vYgdXs1VGPOO/pYYWWTWN5WrNEpTHM/o1noOfwfIVSGYbczhHiAX
BmgLzscIx7H1VlpQNJjukmKmtjScI22PmHTjQwiLtAiEuYelhQJBBFVfCrTrUWHNmgR3sVDDl0jP
mt+Fvd+PBry9ZGdwp8wcsfz2ZmzbhG2O2Q2iQSSTLz8/FRsjr00VbcYnxjPT9LndHczDG4uw8pCq
8uRIdWyLkf7B4D/yDXS/Mbh5x7dcP9zoZVLJfQ/r12iQHS/FeujnSQ8FnicAQuKSNcdgf00uLpfj
+L1Y+DitevSbK6hBZkMoosodfFwPGvOrQtxdiRptteYTDHG5BPgkPHtgH5vBfAQcfx/SIQQj4LGP
H5ldRUCn8lc9QVwZOVCc2/dFcEHzipUrTBwZ5gLwAArHvYGg8S1/nofnaICD5R2Nr5lcD74oa8pp
1G8Vn/4ymr9LAz7hEl5FGvSLlduekDIZ4XLzIoJvdaVzXZOJE8ieJS3E5YogUuLF3+xN2vVQGK4b
nOoGt8q8XYAusdNTh6gu5nhUgOmlFdR0ANKGu1G6rvcDlvpk/feQ6Qz2rZsSoY5YzRMpYy0CmzNL
Epey8v2X4hZaCHP0uOlx3o+JeTcOW9juPAWkFDLiw+NXoSRCqZVfgPI2TKvODnAmiIKQ14OXjJzN
WkD/PHWaxBJWkfe6yHz1+O8+21Hm/NdcdDRG1xpiUCUsrqts2ZzQ25qhMIb40GHJDFA7cFEtLin9
lGfxDpUokvJrknE7yAwV1I5/gLYFBAt7+iVnDcxxCjY56lfWYzGgbVrCFAcqbQ7wvkJXYBaZsatU
NYHrGYBZ2jlyiPg95xxrJCvhjlT5tBHKt55SHeDYbcTO1bBZ3DM8IZTwlQlKQmq0opFyvACjMWV0
huJTDGS9ESHJnh+LeAhcJkC9BQ67S/p3Sg7b24dV279f36sXlc707EcnuRYSX7FXY4y7eRtRMOQB
wp6m9rqcU13vikKpF8nhjLnsgEB4GkNLaE5j+IFpovIWQ+g+AfWQNO2y14wwZwt74xeumH4WTdmV
CzZDC4ReI+gYEuxgyXoNID++HNZhftrkd394ma9ljjASjIYOaxHJPmXQ9yy2GwmJ/ZsVqO/+nWmt
oNNJwED/UtsCoPzcl8E0IDgvEMSizFIZARZWhoVtekIQ60Oo93GnAffIZxvW5tnHUfGUPpCjOtxl
hxposESqioTjc96r21RjeowtQqQ26fPHDK0QltUVINguVbp+LBs7ZQMfgbLuGYWywx8A6s8wxvBL
ReAX1p2PcaTJ7X3f+Dxigf630IDB373XL0t+g8i2XUv2R3qFE0w8/olXszHPizXrAAzZJusfThKm
F+/Cb9G+xfjnEpOmab8tW097AP3Q0QjcU6pSZR2Ipmo2YDygySksFwXglluHAPj9hS0iT800X6rT
daUp/mAixYGZkmAeuyNFN+RqXP9R5Zkwj9X2LzOwd7HYL/XLXS5KDjg6wsHun/0uKYMNH7TuIUQJ
Xh9mofnO/k6pbBbaI3yKTs1DycR5Hqpmw2tdtp9mVJNW3g+BtarMIl8fMAuCREK5u7Q1pKzCLRuE
GOSo06CiPlzEYkNTyZpMrRdKQDOJIdzCvydalQb9T1FlVwOEz/8Rpx1is2szqdWiC2m9gQ3PS10m
WWA+UHMQYZ6+FuLdWIYnVtD0ETBJxyUR0pHujJNSXvRF0bf6iokI4DzEVbAQgvlwsR9Zawwr0B/R
Wq1MsXmVfwVmpN0Z8nr4vjtY4he9hh8pcj7RalPuij30S+tmiRLWUJkjcRGjv/GGhbgNiqhJqiyJ
kHZKwas0gInjtVdTNVfQXk5WkWRlj10eKzXo+M6eB4/+k3/wUvmh3kanWhfkyJUqGh9FX6uX7q92
kdJRSbCe6YroAGl53okPhAnTCTQD7qm3AMo8WvGSPErbdv16hWM3KPitj1JYLqhlY/06wh0LoSg8
wVNiyFzntaCK5j9FTiA5/okTOyBVKzosEw/dtbIjS/lYekOFEbvRVMFGoQCGWZ0Knj+t6ud2rIF+
ZbakGBHg0ZozRyVrGBIgC2MzdzfqzosqIZ+GIeTX8wxWxhb3CLTxihxb31h4wysTS29fMH6S+YN3
22HegBXJjrX0JwzsJDYNz9xlc9nPG1qwQyaFz4REmUAsS8AUmvNIx3m5yFCQ5pVHMNm3k2U14PfR
HPgNQdCsSpXCX+ChhQFOdT5xmdosWgkLJTmt0UXfnVD7h4JIxWL14xcim60Ql7qXFoxANYOvhOTp
98fOTbvJ+9TABn+ZMbKuNvLVLn+npeBTR5nluYgdekTKgP36W/aP3/c5hpjN/37bSXUwOKfxkD/C
OJAQ03SP955o+Dnax+N+gldqxxM1l2wwFS/AWNGUuJ6lC4Xa+Qt9b8OOtt34Ty7AYpCxk9f+aUM1
JL2AR7eNUDscS00MGTSfEW35Gf3TfUNjuUUyr3trmxMNN/gEHoYYDhpBUT+2MFqhzbTceWtqkMK9
pthkTXKiQtp4ABPJ5RLv+HyktnjhD1OOxM1f45jT/v7AlrcKLKkJsxXeqKV5gjcHeswmpZe1v5pv
oHJrm0mdNU2izs+0hDttmzV77aisaW6LqG9jgy38QQRMqWIBS2tTpNWUgN8e0X/7IVh0dxhLW2e7
kp0neg1z36xJg35yct0J8f6Emp8I/VnjLs0iinZpqs4f+6fh+NJ7JNclZHrBitl2SsD/9fBD222U
C4KmcKBm2ww2Y4pMIGg6TSmVulITt4DEWgm8VXdteGdP2DhRyuC0wcTGB2Zep8o7EtP4KhgUT9Rc
gtGmmblG6u7VEzugqIJiYLdWnd3H9Mq2cQn7UrgSVPXE3sE6tDYp/eAXpZoG8bXJzu0gi+pUwje9
p8xrJf2X31gTi4U9Bk0x97EELVtTw/zCJpE0RTkCoPN2Gcys4NXjZdh2tTnffw4g5ltdgBHKXgpi
oHmyzXZz5ZFn1VKtt2lqMYvSqXwxsovAXkDpO3ZI3hT0oAT7I9gVo5X+jGqY4RsDBcIeSIVPjEAc
7/okkgo6Mjpx4e+7PHMu/QFz654cw6dByeBsbk+/aHT2x/MjrXkawBLkGu9tebbITGRsiZ5kWhaP
2E2gKMHLQnt1KRhlj/jM1kSoWLNDLefzkLGVpbFOf5fHbtVWJDgSs6OjS8L1+kTHxdjlxby592om
p/Tg8Tp1+sZ2mY0utSUon3ZdGH+1TcMXyln3ewp0SYxlcJwNNMlbu+eNxTaYP3e1I9b1VwC6BeW0
Rj91m8vMetFL6J9fGbEthrKwTRQirqotIA17QHg526pURaewTsZFuxUbq8wXQ5GAhuyd7MXtdhjG
FoKtzwNQjXWzFQk9D1pc0cbaoPfxvugV2C1c2+y8frua3UnS9uiJt0XS3uDA9wQjBOVyPKi0JYqL
811TPtxliK8HyL0Zye+2GzvFZgCscM3cFznfg0fqXYclIkOQwqmzzrgoO4GBqxSIAfWy9JMZ3MFV
HoMnp34Pthu9yZGp1ayA0cYXCFSmbJfzhxnpVTxzYYLEuiH1RR0GnXDVbtmZAWEKxRRxWddKn9tq
TaH5pRCGo+wH1w56uZkId3J+WXPXEpV2Uij4rbfRhH1uXPhPdR1/9jLwEShC4o0sFp6paV3LezNK
tEKpg+U2B0iaLC7PJdQJqSAZVjzZizEBj1Au1vfexHkq00FC7q9Z7R5CC5kl9ey+9CxRpIbMIn91
DJM0gnMYyOULwn83oySon+GPSQeJTPrjPNmeAcIscxT2eMgV91De72IVxA/WFGMNOB1uU6sV88Lw
nacKJbea9PoFYapwTD7uj9FgsFNpqN1X/nUsF3dH+Vv3S7VUrpcirneOGA5mhXsWWKAcq8joK7E4
0IiQpLVRcYOxUqu6dErE1pOAjmN2u40++txU6Lw9MiRC7Ni+xKFgpcg1HlfuBgWhbIWTKyY7eg/N
ExHXyQ7qVD7E2cEKe2ttwzLhJOPpYjkTFEbpScVBXuA3YxRv63BaaSMAi5vJswf0/6umonvuXAwk
4B5WhWI0ZcW+GNfOCoIH9qCGtLwXXtyuYeSx0NI8364SxHartgBg3G9IJDtV2mUz+cdlnmwx08MB
4lgzvYcwymUv3PlVyyByLz0bNZMkasMxy8FX/DGzbNcdsGv2knLQM09Vz2gIMyRiNfqU0jZNdMq3
LFalYnvBvPM83IeYo4QqiwZZfYrGcRP5ez0skFKkOck7cBIC5YPUMh8C/RfTMMyOf8WZe3YwghoN
VAF1OPDBsEWm8FKVDbxPqxYCiA8fXhrKJWCxjYQ55AYp17vFJ5x/AAaZxZnlmQfbTWiZ4wIx/IKX
pUDJx1z/IKxVe1qKvp0zYEo+BBY6UFoDAcxpZVdonULZz3MRnWT1Icfssxs07OB6z8OKDB71l9L3
bAAI4+N6q35xiN6Srzn+UBfqZbFX9NnbZmiiY3hOW+y2ryYfrn64vlwvQZHlrXzMaTVnkpRdOLk/
dBhDxxvUWpo3vcyBuVxrhjnXNXpeomPWPyagzCHU0pChckAXKgaRnBSgDMNsBYE/s6PXSZ49Qjk1
Pv1fLatO1XRvRyTbSQ59eXYdikCmhHCHxlY0Mkx25odiFpPqh+zrq8NhRAvJgUaVU1XJ+C5lYuiP
pfqOSGC/YPvJ+nQN9nIqikr+R8nSZG+OW8vASBlkzI18mCgceYobLwEHvyQPMb7SvcfwfOMPHh1V
Wk1nVa3NDfTeeMcdOvEDu8Ns7xTpNjCotk0OFeBEv0/Wki9RVzoL5bUvbbzblp39WLNCy+ID2iI2
5+SCM9+2+PSOWhabUIPrG251emp0fKu3YX39CbW9mR5tNE27vKilU8AZtjGfyoeob+41rPUAUgFE
xv4ATKfD6XEFyKm5Xnd7cv3zqaXZhw67OAroQexHpUf2YnoiRnDkUFdCQ90DhOXi8jTz8y4iN7qE
d5/ukYO/WWloCvesRMTS/rPWUU46+wikAf6Q8R95tseu8QLPVpSkMNlIiOCPG0sc7fEVl/KQduF1
TfAjG+gNDdMqH2T0XP2Ih6xf0WzKH5va5j3yyrUyT7bxe+njbRQ9SQU8KEcQNNb3lSJbvoDaB+G6
yL1NCL8P7hOJQPOTTJm1WzdaIbl8RXefUsUv7M/mPC4fQ+7ZrnKZE75ubseaEe/ru6D6MVYzJ3V1
yOi1R8VUzZxGE+SQp2PduaQhm0dhSWp8zURp1Gn8eP5w6JZbXikzE+268dUket+mP3onHMsJ9DMK
vWz/NF+wHgijOminK4RiAshO0Xe5SbLvcRy4hjwzqTF0coyArG7+X+cEwmCfC2J41AWxpONWNv/7
W77i7cPxF0vNmlPt47WrkSb27h33uP+AlbIwBEXm71XlM8x1tItNoiPteazZaRJGe/646dVSeUoR
519Z4/9+RwsPOx8dRLSpj/V/djQ2xfABCFI+yakDkSavU4/N++YsCL+YKYxOMa/an+34n/o9nKx9
EhP8lyruPEG465CaGkyS3rglGP8HQpp4vbKHkZqu8rJVn+L+mQ4CelOVhbo7tb7MAL+aroT3u4e/
gr13J34r15rWBJjo17FhTOSWZIELfL87G5KmPZMwBPc5QSfqRuaFHftgxZBAcyZNu/UaVWRgZrjE
pi5sbJoOPOI811HU6b6BwOVIlIq6Yb9TbYYG3qrfP2C9QftiBmoLaFM3TmYNrwGbXBvCnH1Mmr/R
S6m7VJg2FtX4ZQk9oogoeUkMlr+RYXmtriUiCa0Iy+sOVMd+Petw9yj+xluJLJ6TSh9T8SaphM1f
gqj3WqmTXI7e1XHMpI5tgRQilQCx84HkqL4jBvWVWzaUIUhzI8ezN6HbNmJiOCyELzaHNm592DP4
rdGIjHJKUK07oPuAbU8LMeUa2sv9nw3vol9u6HvYjRgan5Caic5Grz1SqGeFbpSnrXL6hrurnlgy
KfNuficL0HObT3+PkzMNukHipxHaWJOidqwKzc58otONJbwQKNClsnYfOclEQTAK4B33g44v3DHY
MqafX3vpgVylTgm0buWxkbXQvZt6xVwyMTQyk4vHOwMsBUyRgMSphWZG741UaXRpto2pKjsC96ck
GUOlu69ga9aWzvHNG8UCurtIo3cl4S326V6PNh6tWlbowg2nuDOlpYoYLdcYxTMUuVrYmmNGi5T1
g8JOvIqrnQDfkTKLFL3SMJjvKOvP2DFBZ6rP998R/Z3tRCl+tyLjHiolfd+YyS4UNE7ITMe3ZIPl
QzO4doABlhGv5623CleoFXU41Zv0Z92aX5kHiAPCFF2h8HYQGip/s385b0/OKVD9O+qVy5mmlUHY
tgQZj2RrUSJyVAfq1dcZb8P2DufYlgtofCWQf3Q6EpkfuUwtLUcQtwaSlFzHxg+Wi77QrLg/a0r9
Y1DWDteQ4tL7But37xR3OPJHeCI8djyMmhGt4RCnAh1asVw6y3r7i+gK73K6UT+s78/WSw0bkCCR
sSaA48456p/MkPX3QFNqOY9QfqQ+1ND1mIndyDPfkePOO/Jl3nSdnA7/B+kga0DW4T4UnrtyBQim
4wsWT0l1fedSRyp3nofP5N3Mmm734jDjc9qka5C4vGgbzI/3z/91jC/0r8SmSVLoiEo6WA5mMlW7
7ZEV/CQOsgrR/yvz6iE0sLuLx59NjJbYpEcN9tgNnthR4KC3Gl0TA5gboyGNElJkFpX1a6OCJ8yq
iYlHfOI4pjqTPEZLoU+wFBsdxhYVErTRTOR/RXvg4/pH9L9pRJI2kWFjQUt6O/avLXCj/UwIfY0x
zqB9oiUKnc7vmhhkMQ/J1mHZzE5czJU0KMzI7yLtVQFhTYMqmMu5kYyy8JbLXRCx5iNZxVQnEKJu
zUDbh3MInSpzTgCn21FyeyLqvWpmGGM836z98A6qw2wDGoSb7zfkfzs38+G+x+4j3RRV/xtCMtxZ
HhpXdfb9qFBDupHj3A/99WwbKe95e1tdL583rfc9E2HoSsIUN8aKgfy8X3S34vHVgPev13cf5bOw
KDCr/vjI0/Yk9nRLM26fIn2wihTQ5s6TWY6F8k9AmVdmCSCRc7x+Uz+RsH12vWSBbSOvZMpzt5rX
PBtl1P7UfFAxujeYDZHFlefW3bXpX4WepZZ4ixiQMiE69vWKspRCKLDMFVXGDyRh/hKQQfwkzK5v
oLcYPwOD4+xSFytFRyBPrpmDqokN9oQb1wfDJHf44VdSiFJ/PhKUv8+JUVafMUjYYhB2zpkDpuAn
DE+qH33BhbbieogzMnvTDJMFBlLxWskA8ntfj9ubTvYxq+evfTD/gc42RKWB18CV/k5SQJ8KlG+H
YXQDbFPq2MXxoctqg4Sh/7BFO1TMcNIBglsF3H5L8IJqiRSODac23v13ry0Be0iRK+MTuyo7E404
VX14zVnl4bF7NTZvsZ0rjQL+QhzbBjC80Bd2IVAZnPN8/kykaVx/0LMcuZF6p2NBjZLLBYuimTs4
sMNZGilcQosTrFHfas/N+0IT98lT3EGRvh/wbUMZmxegcXBBnMveUk+bcCpYDS1LxpujEJo5ukkV
dEhg0gvVlCV9/1DGPWA7bfHSWEN/DhR7QGoIJVo/aRsSe0p2Dk6eQCxSXV5xuR0ZLEMsjG5GHMNs
PDcRmNnsRUvY8hUawaMqJuxiTlEeldtF1AmmEmhZpyqvH5MqiufP/jGRKc4EjlVqVD1G20KJNi1d
zS/iBhBohtpULosqwElgBWnx2ANg7MtWOjq8XqZ7FMbejnsDuyArcxdQrguJ4n7gEpe6HE/UTFhm
LQijpbN7L0BSZ8UiiBJrypznD7vQro2/ddcNpp/8ABngch3HNVfrTOTO+N6uq+wYQ8snwN/00ydU
dsmCZUu5uLXCpglwBC165t8uoR0gSOKhDa4a5qTzCZGcoqc6kGG550lqYdHU/WcyqcbnFxmIi0S/
/rZikNHDKAshrPpCUsIXM7B2gmRHmUqlH9SPoYbM+bFNpNGAKSXA6HLxQCk55vW5SzpB6AcDUuyO
/figySYe8cR8JNwfnjLrigrUM4dbDO0jm3Jsx9Md8KE5IinaoZNNEw5o8dCX99oL4L5T5VesHuKv
lNAsCH4VB1xuYeTl30pr+1EIPjFuDfBbTfNteXEmU8lfbPJlNKO2zt0ESLKdP3QzPU8U49j3Mkbn
95ZZLSLwygHA/G2p/I4k6u+bXAQeNv+fLOVgOrbdW+3aseeIL1RBuzD53ffPotMBNCtQb3egWIlc
dGcJrBtGoCcZlWUeWpTJM5tQAt15ovMlf2FgmCenzYNt05dXozYS3Lj6ouGpvfnwpGkSVe7cxRkR
vea//2eQY/ELPbPf+WU9jNx9Eol4ucaCCKmEy+vvHjnccpVwt3jaob9Wtz/syKy7XR7cTbicja2o
x3BMcvVYDK8R+HLQys53p7aQ3rNNlcnvbW4Qs3XGbBTvMoM4XSv3jz6gCL0K0XUBUK5Vj3mtMSNS
TcEediOYb3m/kQkFDmoGgDFONepG6+SnCH8jKNaQyhacOxJw5B27SVdQhwdZco3LoH5AS4cLBjAA
UEI+51brD5H/I8sZ+iQAGyW1L0GVpM+5CSgbUGMWPL8qvVxOOa4jUUmpF6cOsXgNOL190WVikOOH
+DzL3RJZRIA2xM4FUdASgsBJ3IirojwOsjlLIqF6ynCceyZPmaSQIj3EPe8LqPhHsbtm1aCvDCKK
/l0gGbGTsSAJQe78DgqRVBTsCZJgdPvf3XO7MJPciiGlwXcybM31pZhhZULlm8KK9rq2Dt0vRVLN
xW70w6cA0ftUprfm16mAsltfFw1AOAED4jYCLO6TxUqq+ikGkoKqjbpyA/IKIXcs9D3qeEpNwwbZ
YFCqPkumszUWpdyXk44gVqU3mWeyn5r/G8rEM8FJCoDGf94+OaoLrcql1w7fEERKTG/DheDhYjKC
NRmYR9wp9qM7zlz0dgDlBfsA0ythsKEfTPT9+ECuu4Yc+k/Y7VVMDB4qwEOGTvr10e9aXRj1v6ii
US4iOEFKF0WMszqRn1jyESJLaG39brkAmxKu5pfco/4rg8itVTYqLXteatU/v0BUVEGN3WBPqRxj
ilVvJTY0B9NFYguMb+YXUyVs7kAcSxIEhu3na8AVcn6PiciJvNx9cA6gOf+6deiYDG+LjZR9p1Y2
kYgv6AQqEaL6x5D+EDKH0357xhQMOotF62YpRHhGxKX8HeZn7yl628AbbaH8WBDE4T0PEAv0pQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_22_0 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_22_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_22_0 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_22_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_22_0 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_22_0;

architecture STRUCTURE of design_3_c_shift_ram_22_0 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_22_0_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
