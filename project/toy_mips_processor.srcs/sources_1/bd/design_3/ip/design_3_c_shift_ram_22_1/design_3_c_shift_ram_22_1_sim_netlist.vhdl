-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_22_1 -prefix
--               design_3_c_shift_ram_22_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
cQ1KzVeTD9srvaobFUcWZ7hAEqYyhsagUF6qZN+sEvoyC1PaEjv74gNW3VfsdnNSIrj2x4703Npx
aV7p4Cq4vvgopx8P+CsCuXZWuclpUTHbIMQKd5u88M61Ve7dBZP9llLtJIPha90TkchwfDrNQy4E
+HTd4vILM4pODht4yI+OTlNV2TLvgg/cc3BTsGYv+Lu5h8DgdZ89aaWKDeDS0iz/+9Lh6yGVmdhG
AWOhL7A74F0PnYPaC/5eSAgUY6BnLCWkTEvAWJnajn9Pk1uypSJ1JoYKwJegzkXimWRpskerDfYQ
lK0++2s0TBfJmsRcfYEL7G1ZyMu8PUD0mM0aTBcAjk7R2pub7DcA+1Vj07rk12MLxX+TxQeiYTj/
1/ze7x4j5beJFkSsuoitmQS8VE+gitMHpGW0BceBp6vUSueB13zBQTPqYT2siE5VfS9luLuMLreH
TknIuqKW8OWqUHrx4kK5p9YQSZeoh3rtkxgbdEC0643SaZqA3gGMfWd6bSjvUpZ9zC3TjAj9fGRA
RzElEHSzDclMqg5YHN/RPbW9yvLPQ3EXS3PT6Xu1wk7DovrnV5UIjG8hr5Denp8YcQefmtTuhFkw
wKbOYs0wzp4R48S5hR1kQc6QZKa7itPkelwR6Cnd1V3bZIA2trVY+Fa8FbOA9aj/3B6uwDeK3mXk
MDB6dudFpYFLX2kATDQ2lPSFWh9dCfqZ76yQYojT8BhUW0pqj82FT2J1XsYe+Q260Nz09O64zF9J
a7ZTIGhmHavWVyo7vObHVaB45s/kuroh5o8lKnJcystAkZX4HnxjkHAyVqKUyt6jrqG8pZYImpQ8
SRyR/nU+wbEJhOFvynTvZJQS1j4b41268jLoI3oP0nB4bsCQBMGZaUVKcygqsiDdhr745qCTjM3o
tMQa131l6a+gsDQ6EoELhEWUaH5iAo5Bs1e8xZFKFumFwFBSbePVdU1TiPcPhjUXWcChyAJZv8VQ
Md0QuEKqifbZHdpTbhjOvaR4FEOJjn+VyDT2DlfTwwFESUL7wW/VOU3yHQrwe+0/Zz6OCCdnYVY/
NhxbEKi2Glw42R8RmLD6X4HkgisZkhUNl1sirTcPh0z83WKWt3bfIdcIsOx24acN3MVHCNfpPNxV
LJ8J3E6WzHNAJ1PQ5Kfg5znkFHhS5NYrXVePPsdQwvIU9GXhYgdI3xneNSh+suGT4Fj04nGuGbAM
KNLPrJvSWNL7yCVY8FNOV+l6Qz6wtqW0mpxk6kis0d5ofCYoOJj1HxknpkHPEfXGYzoC6EVJRlxE
QUvdfZI8QUXoT7K89YjmFghiPP98Wx+3GkNQszdUrhWwaBXazCqsxbYNVf2pIIr2ruRCQQbASopH
IosR5EL7xOYxIVvPxzx3g0RUwLAFqevxo+bauADfaeoG9VCBw8fmX4q+FXp7Tc5UV+EZw9JgRmM5
X8TQ5vyHceP7FZiqic49C+J3t0dxlKDK4TSRTDm/7om4TW7elzaiVC8zuD2v6bj6JzI52myFkagY
bwFdzgZw17pr3bscYphEkl0DeNvpwRLp0kTxeaHJcDzmlLghc3f8kthE2lFlAP55f/BHH/6dsw6i
9doHywVX6+KQtnDD4NeRuJVsMMH600CbHjJFt4OLgdqkfWHZVrn+lUsdjOgx1vp3b0cxJM/HFcbC
oh779k3+PRzL7XXN46Yqn+MWAUUosJw5nl82Y6BiDQxHUvjxK7/eBwtBZZv+om6fMFjHl62+A2Tx
XkejOabz5bOdrMHAdXH7bwE5hI1y3XUFfhmAIRqufsdV3R0QPUU7r/p8XYbkMz3fA08rkem6XgGv
jvC772d/L4Txuj1M45evr/01BRB3a+2Vw+tnxYjqO0AQrGZHyxiDvsonFQJ3fTi2wNbq6K3eV40k
197w8/JP1dPOLR8saz5r5T8KwlS0xmqdUWre3bvoAgboKIwI+kqIKPOxAOtL1fHzOCIH3Wks76mF
LswknQi+3s6yFTN5lZAqBuiY8FvkPI8Y4HgnbzeZ8rQ+M1JStkQMMwgsMJEpKFzBf/wN0ckqZHNs
g10fTUru47rpt9YmG0TPSCm7cBDJ3eADqrMxXTgB8oIUapqHJdkQKSGO9Lo2KoFw3IfX2nsnVzzt
TQB2m6m2XurHzHBN3bVlUs7tqGUzLamq7yLIIqA0ImzTu7+hWnewTqFfSy0T8vonpL8q1pVuyxa+
oQuAs+VbRs11sBnOBXTlA1APVihbNp9YyLZe4HGlW3yBcmL2s66GbJQVcoUMvRLiZ36BHPNMEpVm
fLGp7fihjk4JKB9zpVe6zBLaIUsbl/vlWtM2nKjzk28s+Ctr2hqctqb+o0UBhSzvWwphfBJPszbs
vJXcs0NpG5/tjOuAyLQvsG3jxLFpI98SNosgcLqN0CaegOKALYzpPEl4GAFQSKhwm5UYXaF6qTmt
ybLbGjCZWzjf5+VBY4U24jDUlwCSyCvWVrCzcr0Xu//LfgqlLCK0TCrSNlrVPJXlAK5I7byKi2DW
0aBaDKsVNRg9bA5R3Juz/x4v7sBAVjNqlzdc/4A9WHIiRbSaPol3NRlsbP4s4f+VkHQOTrBn2Br1
WJUvUiYJBOGl9ezttkijR/p65cOi7hrrev2V1oQHMswofppTzfVumv6n4zjabl9UFpSv2Yn4UtJe
fJ9c78QQQliqkZ5Xwzwg2CzYzkMVJjXhi/LxYl7Fle1EIW6y9o0tyQoKsy23Pmhqg39/FadnaaXO
vEXe3aOPoq1xHQPcDJ6Tc0m8rmGaAmwHebZjKtHrXJGfBs9obO9jEc2+vkt9oXduVbLmjFcyK0+0
j/YZw00Izuv8V3vsqXtkAlGUBX89TYUUhpvD3ZUsynK00y9sFMqTqP7m5iVF5Dw2zkbRrvM2XX97
Beui6IRxnYI0UraaT8nxIS++knHt1IoDKKugxKMeACwnCS7NOxISk40nzvWxfb4ejscvAq7mtQST
ipOYqpx4xRlubUCLuXA9kyS1hfzfWmPTyJ6vBF96g2FtHgYmV0eCL2a4/NAHNeMfuz6tronEwcee
d1MpecdaKpbRse5A+Gab5u2ovES1RF3syZ+VsSOUrSlDfjpeN/b/6Ci1G/xKGpGgfevRa/nTlR8b
L2vGsUOUGqwtgH14ucrzqvZ22I631WaWVMDij5epy+Eo5Zn+hSa7B+O5XOmEpUuEh1bjNL4iUGfh
4EUgdvHWuT3FueEgACH6IZ6Uq8QQ/BkJ29eE/IJ7ZbrmbIV0gGq/NNhPSCu6Z8allgpc2p7+t+5l
qEFJ5iYvzcYqFomS1qkqWfLRE6f7ce6gx1CaIrfa5REMP7F29HoxC7njy/xvlfnjW/JN1gbSM1TC
ARaLvMZdOCXYKTjGKMuvLJDt6LZLgY6joGKJE0UaZ+QQEByOM725ksfBcXn7j1O8AyoNf4AccdTK
CZaJHKIopWMYPRpQ9ata+WNMU25NLQ0Pz6oiQaRX4lGLHBNY4ICuvb2XDtbh2XBtMD5JmjjgdNL6
5L5QxMfcQDee/d87Q1Aqv92dgzyEp5P9UowCVcUU+BkWi5vKASVBB7CQY+ueFcvLeJjYywjD2QpX
jpZkIIVb6JiIUjkkbrWSVe7cb1kU/f132YB2wStNyoQk3w7M+sdIU7Gpqy4t/K0zWU3yBH/pPozW
lTMaP+h275c3h0lhYp7Kyz7TGr8VF5yGq/N6HRO3MoU+MWlae11EpKfi8tFzkZpygF9qvPZ30FLJ
45RGJq9YCb51DMr1PIHiM8bF3iNV9UQ66GipKxahkzceVGHiP4DrFJgDJQaG9EhBtzR1QrEtuoP3
78CrM2v8GelvmO47YLAxH9C25ZSPgXqFEMq5ZOHw8xZ/MWE8E27WEDdqroqbTkGPdPq2oCYrd4gL
jSOCZJubXC+hnmFRUCs9MXHZtD33viNJQxkAgqtCREQV+MTfSOuOjE2zMw7twwediPX5H5myOvtd
DVMRfSIgq/uK9xWjM1sm4lvrEcpxIaSB1QmPilOseY1Pj4bxJCo/WyAPwvtgJxfZ05xpwEoTqn8T
fESln7QypLcHqPkfxUqx02e3Is9GJdf0Y6YLcmaOuuneFVPb0nTbKlO3fjGMuLHm6WSL8252S6CR
5zwgUwyrHFwsDtbPZkEMyPQZNw/2/kpvIJ2f7gg3EB6Pj/bmn0ZcMknuQv79aUZ/varqQnYb1vAE
8A3A2+X9LlNlNMLlbJo70St44zGh08Np7g81uSZ0YvcuqtRWRDPGNLADdIIv5TtI3COzPhUt9nXv
ufVNXT0FLTqn/c1jfgr1Q/vsH8tGsTCGRricqLXgJAW7VL5j+qKe32eNmZT87azD73FwPY2lY9/A
9Ani214nPePqhsetNZwe9DruVgtatF/uloCW+JpFO6jCnyyBadHWsDkpDdXJOI7Fb8P9H2myir6w
aUTkpDI9Zeza86B/kOLsn3ZMWuijyTS9lzo4L091kF/2FJXkzVEGN48N0Y0KKAUUrTYecBpMp9sG
9WuqnFHkO6bmcmBL0hWvSoHw5kNYW2kQVYvjikWEXdZtrWFL+Fvf/cxyjdLjGKN4STiSumqEPfBc
D+in/qkpGkzIyqh9zPRAS3asKUadgtK8Lruh/wK6wPNmnyPOXovK1Q7tSsEDxl++cP8smyVLtb1K
M4FalXWW+rzT9g3NTOMVLpvFkcNPF16fkwGVR8vsasQhtMron2lmgfPzPRxMgIMavuzIMo7iba8o
HlByHpkjwuFAly5GvNGgI/SBA78XL52plMbLUH7dbbHyc6BavycMX68T2pGfvw9sLtMVnMkrz/FF
4k8tGRocpFis3dWI5nVS+ztfr7CIjO7lMMe+yPQvPosbOtSfZLuUvArJpOe59tQ/ZiaeK5SAVvIX
bgZtnHfxNren5IDuWm5SrWv7Basc/Ziw4G2WZ9n1vuBGhOHQGxj8Bf39sktq2r+krpNuGAWP7+a4
dfdAvSrjLFITH8c1mtnPXdyPOR7HxWaOWNWVu2YCYnSjrrBglWfwz+ZMmPMOydw6TKVyz494Cud9
+Kzr98NfLObfoxJggzo/FkuTHTFi04mlmAmTQ4UfEKOZINw9oMslcJWxHpYHxaUuyV0d+PkISBIn
YnMeQWqTJk0lvF4IEoqUp7f6uA45dR4JpLoEZs5I6M+s+Bv4M607Te0NmsE/boBbLtU8VjfCH/fn
T6hI7M+iogaYq+FO8vMUg5DBvUq96kU08Uf66JixBOD1sVnOpBipV7t25E1fIj3bSziki60ei6Rj
Tmw+QWoq+8lFukswqilr9DZv3DMJiSCsPb+Wz7BQzQN6t2vFLtoyqokHFw2rmAuwZbbEBLi/lojd
DzM/cKYQTYcpKGrYcLNnWtO9l2R/fjx/PBuHkDVvwVk8Je94z3hy0AE0LuV0g0c3W/DYQR/Gs1p2
j4I5R7D+qr/KLIVlSpJ4V5oIipaaVRM9TN67oCzIFv0Y7ITGIcs47HGge9NkLTHILpRvl2gFcbk8
YA2n9b+f2Kr6Ufap3g8rLV0/RmpPvltT1BglRyovQRIlsN6DZR5PPG0Xnm8DZ0/TI23Y6wak3qlH
Dz8iA6TVRxMTa5u5Bpjugv+ZBwPbdCghqUYlrWd/VWb6aJrkzJ9quBpEO5fb9l2/Oh+R3GBcGx+C
Hdq1eUWCpwBMJs2UvQavACLprZxtfXgrEKTjoo6XoShZNVoXe1v+rJ2KSXkWg1jumQsIjFmUh+wO
Q8Am3IEDnbuXzZ6LC+qB3xkI56EJ6mYmhIY17NvTdWvsp3IRgp/d4lIylw68KrgyzfPdUCVfN4KK
x7h0dbiPUpHvFr5p1f23PJG3lekyrGiHGuIgy2aIhYpk5yIVRuo3eGShiVnIhoZI/NfpbiRBu22k
OYG5W7l7FmNtVtV9Wh5oEs3+BjiQCnV3zvAo3dwCwLUlzXyiAH+CVE8dNSalL04zJqdIYOnvSVYo
P7BHS+kRdBeegGOo4qwdAoxNze/Rp36QjAr4AFSrBYmalIcfBZrzxArCBDt/65qBYUUN/TdMGA4y
xXM0CtyA2fJNESSRPFjpUgiVDqgrDPvgeIUus7o9FWitGajV0hWPbWwf6LEcMjY7sXikQfJLG8pI
MNb6VKJqYYxcRDaCBSDFi1+XZwrWsLJXkRndIya9FfHjzPWX/6rCNEy3v7XUd+foByaJGx8+Mpyr
W74co0Ldqur2F6noqcyZrTE+UNX/PgLMXPdy2wKgh0duPXaNGggKwXSzeYry1iM4bfZ1/Q5oSZvn
JKrzzbdw/QSNZiA1TJ8sk+tcvO79GDrcJe6qFHCCAK7pVEXxyOfkIqKTqmarYGt3aLWIjHs9lvF+
lHglagDOFTUT0CkZ1mDkGKWPSq/kXshdSXWi2QeCok7prd3YuL1NTXQ5Ij2M14P0T74qsRLn7rnv
yJkRFWhqDu3ZjwayBR+1cm0gDU1p2msOurmpglqIpqKIn+QhmHClqRqh+VWM1EFRh3Mh1CT8fqbQ
bHB/P09bU/kbn9+tq+odz7wKDsHaHNAwxmUki4oBqjNuya6bX0hwRPyWboNEHj4SSBc8wD0Zl6/q
xX41j4UfL0p42G/sSRzia4SfZ4DfJdZrJ+d4E4dPiSqEg3FCkmfw5Dm/7jSP1tjWp03J91xleqTM
T6Sg9lPuLnjkfjZrdb67GjJrzFz8iuAJWruw0cyiEjhK2ztyDxxeL6ufLPxMszBOiTLfSm5BRThq
OU+6yYxlJIaQFmZvqt+948i3nlEi8yMAA/BAhMrWlGeku9sb2OwgFhLLuCy+fbcrSM5Oty7iJhfb
ZrDbG2/3psmIJMn4cDbS79b1kIRGMbYrKmEPjTt/bAbiilzt7l1lljjYEuWQEDV/UcNDEuMWAnur
R0DGJatOM7jbF/vqaA230TVQ+ZjjYoQOy6Vb+mjy38mIAQ4hO5x/dKfBn8ACOVUZppfBCU9O/1XD
uqX9s4U0sxqZbxRhyF80JFtbKfxu20XRkPekDU24t1MEHDbZD9TIO0my2C3mKigk9jLdrUHgOVXZ
08r/NXxfnpSrjE3KK9fCLgoyyJBkD+8EAz7bL7ziAh3ZjgcnhhCZ2YXF+7WhZH7+AbkZkeYpa27W
HyN0+ofUThBigMg+5rHwi0kkqL5fJMJzpM8EyYaOOxiXuEbjLd2sGgYyMME3f7xhoYhqAheiqQgH
buG+/kos+CqAIMBdfGTFPfeicx0Tq9ZkEQi6MyUtAu5wgHQdwubVZbgjf1yLVoCjYaFXi7qvEH/D
pG0RDJ78qUp9o7DiX/NSt+3tnEKcOFk1RI+u3IoaqsFcrTBlU9m0fYkBpT9dVze/hvcxaF6QC5TB
qA1kywIbKjcqdx3smzR5yjZHOsv1ETldmt/JbNWgU5prFj8mOUi5Ps3oM90RdrP+5D1OqZhmE1Rc
wOI7duc15pGU/dUkpCitE2P9EKdgRQYeCJQnJK06BtLaUQkoHRDxVPA/Jl1UIuvXePemUNODiOY9
EZ1ypZD/U8f/VtwsBYX+MSMDxquv4bCM6jy4RfG6s6RjOpFzAytmCx3cRLWF3Cp3caPnARnca1h7
7N1NPNcpm04fBfrLhLlf1oBb6NSFr2z/DrWcgkl7ETmm2boV9b1g5DspjfnpJPGU2Qd4eMCvwfXn
6vmCW1WQvoqYG7MlgXbE7QCYVaR7YNDZr8TepDY8srnmekYH6q8rsnDdsn2TUR3hqSJdFWzyYID/
zqXLW9l8yjFItr2bcNOd4pofEj0DN9BGp1tOFtaSZRnoNudnvX2qhmgfoPwYjLjtBidfRMgyb2Iq
lJWJmtAqSQkAtic0C6q/PJWENYie8WCWO8ADjVMINn/+BEfgDlCpfy8EUMzJcasoB+NUA3qDuTrX
HlFbWYGS+pwvscQ7H04SPHM4TSYj+nC6x8h485/9SMVU9m4VPZmmL0mX4LgvN3P4WBZnoP1tqrZu
bCdoFxAE9J2BXuX02uDtVaEzZm2EsYwTELxwT8yrb64P98hSAnwONAcEMTBsfog+I4DwTR7R4DMG
z2ealHTyjZrXLla7MjlC66s4lRB0uGRVz4b4B9OSmvHiTAZIx/h0Jt9DqdzYYZNvSy9BZgD+9LoO
chN2Q7YQJkWwlkQgXXlxs0pKZj9hzjh9Zm7DouvwOilf2CmsAHYtge2GRHrJ4YRusfVg/+yDbYDw
sHEaD48CiPLJwegRpt/RLSo48ryF7OnqV2N0APYzgDKfV+kolNQrWNDdMjJc3+R4CiF1OAaF+d9Y
5ORdZnFhED+aWaQQk0BOWtCijgsMlTPth9bFiJZQsYOCH8eSCV8hJd30O2LBarqIsQ5SX9vQR6SA
3m9LA0pTAYUGM0ECH7+Qq7VqN4KAcHn0iKdIKIuNjTm1aQFyiQUa7tecNjs8sp3jakbNi7afwZyQ
V8A2bnxtiyFBLitGDHsX/ftUHnunaawYCqljaqhjICIJ+SdLE7G0qV4lAozsxApN1foqaYKMbNFU
66F9xOHRyCn+FO4EfoCqlK780p/2W21x/dYzuDSolNhK62nkXkrYjzblBgihczgu5OxYPnVMihq4
POrwxOoEVexgX//W/4ZsX0mujwee3aZMb0sRXYqhrzlwqXbwBmtOR0aMF4oEyl9a0q329iHN941b
HgsuwW01IAdG/4oCFv/XwEqBy230LDMPZIk5qHN61OnY13xBF/0UhR5awImJ3nYRErrgqsxoT2Wm
hM1lq4u2MQWJGykBaC79nXFD6PCXLRd7JnCmnEzx6eVDMabEuQw2jBdkwzTYA3k40kk+50BB+Fqc
/BSebaImhnx47RcOB/G9WU4YuxV3mT01+bASomHv6pEHpXTIEI/lNfjeIacU6M1ixuuhg+xFhaPX
TCIEzZ5X0uwquPzK8g56DYXojC5RgKkfnBg/4IVpj/9PHznY9eiyTbwNVetNj03tMwABxS+fAnQf
YzdHv7odgNPLOIRUzygNbe13rs/vUReGRmEtvRavBm8YKmHQso5SsEM6dqngItpTDmc55k5oFwl7
2M0cNXJCWGbG21fgjymsfQetzxoI2Rvjgzd8T08+J6pbC0beY7awn76GGhdDmzJNvf1ilZzdSgmt
mxTjGu/LIYZoviFBg6xRnd3R+s7kPsmt6uGCXydnUPQJ06FJjKwPXhTFEHX3ofa3uuBqtYYaz127
KZZma3zMudUJtEQLfuInPQVWpEmXVkuYskN7lVjm6h9GKz5sfZCgbbS5XUjEj7A8Ph3RmBXQLwno
JbU4sbPIb+94rfh13+5nPJU28d3LgNeUWVVQbo01gy/MI8xFWjObhK4a49KgwemHPMs9TiIXHu4G
b50z39dOEvyuZS70J4KhbTgtVNPCl27D16opY8VPX31NleZpIqzxILekuCdHB4mM6HR0KzlpzLna
h/ORROCkrHHbNopu0rZ/ljsnccSujkmZMBqSG/PbnXF7mI6MmWIhWXrT3huWPO7o7TAGsDJpIZRP
CRVpPZkMiFTMaGBFo6KS58lltSSREbkY6UTcVQC3xbJ4jNFEXcQNJj4Ap1hieZjG7fhNYBiEfpkT
3iG4hnaEVk02pnn4rbOzY45FAPM2irhAL8ljfq5yQubETMZaphNx9S2Yejox8+oOxImY9lCb/Lfd
Mi6vej/QgIUHOvFUyt2nBFIZdK48pSJCH6oeA/HLddjagbjyK2wMnPZtwrts7YEiJnlsXnbs+JFN
RoeJSS35HSvyHOJO32SeZkeL0zWh+HW+NVqEpPbnRyzCaGrOm3Mmm5UsqFLa0/KRlywd3kxlnwsY
iE02m4RuJUUNWULqLXqgkaKR8avcdrd9e1g4O85uQMu23f5WjgBt/1Ukl9/1rdoZLVNc75bVPJ4X
n3vEFzjCbFK4XJEJQWftSA50ymm+cOS6EiBS5sa/amCB9Sb6fvJjxuksYok6RMTQycJRZE8FVJ2G
QSjbMRxUSqM2LGeTiwbFMrsknMT4oEwSzbOpcBi4fyDN1pF8jFdnsRYE6UBZOQ7qAKWO2xpWs/Se
t495Vabod1QpyH5sj2ZV0SK9Aim+d8HzUPSs4NWojVEgUmE0YWVNf0MybXH0IW4a8F8Ug/6uk5p6
nPB2iMx4cdYC+XiFfNssicO7yi4WVSyUG27pOnxjK4GFvnTLfan+9vCavEJQkwhtaUPloK62+iy7
VcFEPlxY2Yeckljg+OPX0H6dYaxQgStLfQhKnaGLjxF0jtA9PPdhuc8TdbHSD0sSGuWKsKSDibsh
9yI2hXEefq53EoEeuhmQaz4atdeMLdY5TbAUw5OZN9nnu3VQR27Cs1R52r8ufz6OoEue4xRJH2rZ
DEa4F4A+uj5xMPmXfD4EbTmbZvmndLOkwECOUtVTKPbKcNKue3TlfBbEo+5BzKDaHiULEg0z7Z0U
Hotbee4J5dJgSgpDueTMtA/AIHm+s96AWkxF8otLLqY44tmlmDc0dPCeKfBnnAKA5rLtPMY082Kr
6HgfKbxgexfvwc6QRLnXEW+mINA+vhFEt3vNmk4ksY6CAuTcB8cxpbCeu9+4sO0Np3KkH+Ih/448
+YlyYpiYli6NjTfQ/zwUt+vB17xV2zAw6v6vEYcJ+HeNhkIesynxX0mM2hd2ITO+QGYt8CxhGhBD
dppLsNDBaZLoThlmTdBCRBnHoKcF0LK698RUM2KxasobA74qbi6odwNl+taEkqQXFOf2LjAoKCWY
d0Lzz2dfCcj4xjY+PC9LtfDghZYjhCj9dHsmtS7gzrKSRInNc8yUSocgod+HlJMJ1mYbVnGTzpVn
zC3GeZ4KYYNeaOY5Nzmet7ne4LQw4RFkHd8tr3PlK0SrMF5jvsKpBUFbGSVIKTYSWLrVZkLtGaH2
9dHO9o3Zl0cFEA2td8PQBH+IJQ9XTNMrE5TdFTZJwa5q1CnVraqUx16JGl7kZlKFFBAk7gA4p/m3
al3NDDwxfPtNJZSCp5WZV/odLAW2mDWKeoGt636616iS1CeNArVMKriiMLBa1mk98YL57nyGL9JW
lZBD6yadYNpZCd/JILWthV9cwiSAeh26x56H0fbp+p+vHoa8076XHEHvzmWZBtxX0gt2XAdtukh9
5UpwQD4V73vt63HR0oBXrUCZidsWb0diThbbNOBCghxgVZRaoeQK/ZFrpBhgOV7I9OkvtPzq2x7r
kjdfQQxFJvLsnVz3dcnPJ2WM83M44WlmfiP8XBRqjGZRhNKvjHT/1BZAHaSM5nBv1LYWxB9utbcf
ONmq2jW5fl+K3A0Ei+Hb+0gVAUiyLldoTj9dHYDc+7ai8sOabTzD8TGbceHKmWnfiY9sWZpa338v
7RCRnx3B/CoOXkgiPPK3XV2ZEtdw3AlysIv4YN0rI5TFqjpHhXsaNM8IUHXOo1kkgt6qIb+Ow/tx
79AZdE2ve78pQ6CceUB10iorLvLXC/aYad4HeFkK7hjNUYY6c06q2INH3Tv7DmGLj71ECQJ1JTIn
2DhNRxshlefugpbor3bdU2KISHMEw9OprNHU+7lM2uoTP9Bfuc6o2/SzkWJDlWtqTwL8LyTVzFZ4
6coZF1G1IZL3o7zx5oHicQrf9haEt5Y9yjYuc4LcrExX5Mfr6XSXJuDfsMdl5s69TtKCJ7zcgPO7
KdTSWWiwNp5WqR/TE5mYm59RzYzjgBcprST/kJdgsJyOn4AVgv3W6dYgy+NBDrtSQU0hHIpG7Ubd
g1CFcE384Ntt444EZGk+byveUiqx7cMopdHLki2RzcdkBQEIqys32dbCcS2ihkMbTko9F0bPXzWT
n1Z7nxGRkskeNgbFdev7DKcKTlWCH2OYpp2o6SeIIf1KoKVqdLACOgGWj/+85OVRQ0VGEjedSHYl
uHxVEWmqSLiYXf3UixjXKal0OaM5nSsXp6lflu35XrhZLHi1KISkmjNqYSLx0nektJDUzco/k/gb
YL4rQh7zGwiIK7jqrTxj0LjVmaW1s5rto4KTpxS1S7V5aeR0klXEFnBEhoGF4FLrcMQ43L2T4Ajw
PCZ9vN3uEvqXzhpUbUxVxY3DByGS/UhDfwWHQ5NAiryg1QT91VHrUPVcTj9/t1lOkPuZ189Jk7/9
k65wqVidtI60DZYnxmNJdQJoyNETlLAf5V7Di+355hhKdK7HagtJxKSFQrVQLYIRBTKIiEwv9Jzg
4Ugf09FAxpmuV9uZ+R/wJp5lkZput+PHMCkzZZU0Z6Tjpw6sCgIYiX1SoVDVxawrTnhyo44wQszg
eu8Its/8NiIq+TDGIUxdv5TJL6mBqPgVTJk22mSYotSCJBkL8FegiFISM2/gHJFjkeiM1/69T31c
Nl2GmoOvZvQUwY3Nf5UAvqMrbxlTdeZzx6uoIupTGl16v4KiXpL+B1plPZWZsXfJGJbrMUbAoubX
aBmhuhHWy3J5whhk5HBbF2ndSfWTj3s7F5MKF6QFp5kUBZTw7E1i5FKjxHowpSsuBQrSbVBelV5B
zHxALbu7SKfPexDSE/IOAp7P4FRHsUfUGcvIZTSxt7JatUJL9zg1CguLqmcaw5ECTOaY0kVLSoLC
oOghMbIcgyoCEMSFTx+qaLX/xRlRid4+gsO4zZBoscmSrWxEYn1auWUUqremT0z7TxgqnsYGMSKy
PMIMAYiIirTBKgvtzkuVp9rkS7mVyOJ5XAwrEE4IPzCPYe4+hy55J91rnB5diwCuA6Sh8gnclBhA
Tsaelf1oTBKK104ik7zbSafWA3rmEIkTFgbinGRJU221/VB6AY7MMYLkVZml2vlB5mpftsEB/13K
kCGDbDdbgavpb9VoCdvdlUDcO1vNdw/BwA3AgOwA8bf2o5wH36Y/M8/Okv7LzX6iPDhAPXrmzmP8
UbkraIHrNeqvqEN7X5AigQjaOXoXjOeIYX6GpjY3nLYaYgvxjBb0oOdD5w/P5QXCaPDhtj37g/5y
E8uE6XnEplH8/suw83eBeaPOoU65ZBsLL1vA3eup0dSv5IeZJw8jQJNqsXtG4RFMpEtH/Y+p16jY
lNwWo8BjESQYOEjnJgN7eWWrk4tqQlfaTygZh8HDcgwe4XNNt6eBoFuMyNB0BS30qvMPTI3AFCSJ
hgoKz+GMBkMx4X2ELd0WCtx60tDyZfrSZbk4MDzHZUF8ancs4wLHr9RuNrYoBmDWcvDAiR/7eIrn
31k4HvbDiRLlW/KY6jGbpcruwAzSqAgUjoZbbX5SM1bcqXZUEcgB5f3rQmtbh1RmUD3XAicYX+Sr
gfzBRVeIUGXy1BxajanYPYJVmp6VxCK/4cimpdLD4QUlnaEjcahfgE7jYZTUtr84mxGOlL9ASX5n
Dlp1Lih8NO6Uj8o2pEy7t/O6yHM8sawfcODO9+xJmiGHO4RbqNUBZmWsomuqjioJk10Ru+EfE4iJ
Svn/YDY0FQBwpirqKoKI4itqeBuYccM+Nb5X38gLFKO3CSgj1jUI/vGmFCKKcCHlvJ+s7xAcQIx7
prKA5jlowK8obrNRR/SqRstEsji/PwcVO99NQsV0mxPzHP9v5EFa0JKNUJEts73tkl0TvDWlFrIH
rzIShyQbqFmSHmdp/b8aD1dItTfB8fCukUmHUeUpI8GZuZG6Y6tWPvXlxHbr3Q0cyTYzJVGXkBUX
ZYaSm7doJofD7xBaydm9Ncb3XOtWl1QSgQw8PvJm2rgFJE4JZIzlGss49OJ15Mzpi/nJb7rRqLQU
otU3I1FKdjGe3NMwjSmxKetyDz5OLSW0rr7FuwFOsYspYKLGQyKv8p2XQPY0BnpC+enmWb1gpJjW
gGfIDe1nusT4LZkNSeCyfrvuFX3Ea0+K+0wekdZZjS/OGdx8SCp9L0KYNUHk2yVH+jr1x6mVU92N
jnNS839YK1aInK6CFqG2o15mrnQjA3PEHQ4hqWQdL+diFtkvZKOZyUiFFp/6J2lvD2ktEWnJl9jv
HR+uMD7LzbdOlZSMtsA7KPSo5UZK+ZMz+beHyonCwtiG88IlVHGTqcbWs2dtXM40GO1MiuuhOJDH
Y+5lqdZuxXhexBXakHSFJc0BAGy6oZHiFGdTNS4nt1rm/s7P8158WNC0sEtlEdjOqN16YU18DCXh
BVjgAKJs7eZg4bRAOb3sZvucCbsWO5DLWbnKeuUCgFit6QI2mBGECmwhxpwYwswy8jNWfsO7ZmEA
YGaiaOHyYEHsiVZIOtsiKUAoV0Gv+I1C3XbI+qrG6CVYl/gndw01s0ML5rrHflx+nxgdnZkNouz/
WSQ05qfHRRCMwECiD3WJD60Mp0uXmi2WqcvieCrOg15oIABQUf1NbfuyP9d694tba7blu7vjtOMQ
ECtUq3Or1vIgFnNXRJ1WMvBngxisiS7FnCDZ9ETBLJLiaG8DCp85kLsE/rIEojez6u6YjskBLmC4
WgBT/c55JtQlqqBC6GYu+U1MPQ/FZFAIstobgUTKS7ocuLf9KT7lklTqvEwgcFDWf7vUEyqRaxAR
mwExNFUojsCHy5LzGUC0rd8Wtp2SH14rT5HKeWUreVASTeb/wkZW82DMA8Df3mg1hYBCYtibsWNN
WBA0E4n39LKbs/arXYF68AupkXAn9x7HVjIlkZgqbNADzUtBlTJ+q0Ejh1bipHNV9xH5AzG8r8EL
WRP4vF6sNt/ZUd0vsYt6HnuMk52XUzV6DkVX8HA4F43R67yCnO2E3maQPjfzYToEo4wBPa8S/Ge2
WFd2f8OKb+3yasYu1HrySLlTZsiIz/Fdl9kez2PNHyH703WdXmTpUciqvkVKp87f1MYsy6iAD+Bf
dLvkGnaF27W/Mk5u+y6u0+MJMzxoCLVDU6g1Sm+bE4VCaFgheCcGbmBnDJ6lirTSxGC+v2PfTeY0
uVidwuupnSLDWIFUbscltdG/sFf5P7YfHNDjH13X7uXU5+r7F9DwrciZ448NugQ0MClDgngG0ofr
Mj+k+rHYLsdEFskY56fPK+fGiVlmtPgFc80U5W+OcCx/Ryk3jGuPb1pG+J4iEDdC6/jltGdjaLSH
TFtxwl0l+H0SsqToB80ISbiNDPqSe/VVIy0v7KtrJODPvsTnqhkE4nIhwQvkVsTlUR9VH3WfUZYW
t+aHAIb6J+1ZbadIuwOTYxMBDXHH10i0H2CsKGMSGM/IkXD8MYNgCUYL4H2IZcLrpyFN/9YrpFqK
goaKIvHfuE2E2HhITJBVzDRRkg2lyppKVhcERxcas92zq0Kxzs2eCsSoeIK1t15jFQwSXfokaeQg
XEeyPm6MJ14qwEdNUFJXjfpEIIF296pFFufnyUyRwRpJ3jSUNxGGaTv3va30ua63nRlMmw/ds339
sIIsZHzTsngyO8fsfklUA5YrtMGNyG8Kgu6HDSRNC5L/SS4e25DoJYYP81yxZGeEpkO4sEeVtbDN
kYEXTZekstgATF7PMuPY6OvZnqdkeO94k33XcOyJdy71JMi4nZmqP/1vj09IJ+UnueK6Fn1IH94p
XoRl4z0arz4Lxv+ICtJfM5ETcolJQWm73D30a6lh22YDeoO9wfp+sldAtm8wIeQ6OkqheIQXjSEh
KQNmGgcl0GZeooAoSUhOv51NMl6aeYsRYkaIoKwqlUtt13suce93uvRWi5GqqE5EeOy6do59JVTG
39CTC6xoEI/OnPtmv0GCNSRSleNq5kCkn3BnA5h6jF+cmID6DWIjSIgFEcrwEwJPuSpQDhAAsXrw
teeHx5AFT+Pu+0yJAipltTqbcWcqYWSuzSP0rhMzdouLg4SjuG0z2vuxCOu2VHqzBZ9UdJfeiZ6p
NlMWejAyutWsZhT4tOi7nuo41b707iPTgov8a2CYqs5u8SF2gAm8gMFFGPF8AynEj5eBnKPs6V2U
QjIikbM0sxTTRWosc7gyhFbpatBwHeah4LIZuJQyrl3QgdxOaDff3grtfU2xoUy/Q75VUYgJvu6e
UatiZgRX3i+zW/9eenuklwVhhE5Rv2YYUcn5Ejib3a3vJaj1AXmcRjmBb9mQDOfcF6sNvRk+6txW
CWA7YXPTMWmqTrou+xBgjFVsHqVAZLnIgxoFivtsHUA+VAfxPs7sZ4OFeTpGNI4jpnlsypmJqQWq
/sZ/xWlLs+u7xjopgQPDrgG1BEEJDDLBxpaEXFJ3f/RmyusORVR2mHke6AliZqCyKZAVRmjQwP5v
vjTbbPFtVuMw1jSr9tqvBe2LsQeKDlnMPZPsnGUEZLH7T9I6pqoRRC3FaScy5J5sa8pKua+K/l//
f9q4ptRFzkTlcRZBo/CMvq1D09Sxo4PzY1K0C0c6C3PTAzb/Hm7kjwKFb1CDuFxfmZJu5q5i3X0k
fIM7lsRVCdGhcjcRVV5fk4OVjhxXwdH92u6I/6OWR+3feT1PDJYm1MK+dEUhnXuPBlJSki/PEnBA
ADJGeXFsyMTJdo3zey61RJvgvTDOd176kB0rmsOXOYhOZf2qyAgHWpYRIpuX1vYLu5yxPtlpX7U4
wNf7z4Dgh6ckD+ZpN5dyTNm0v8isvoFmi2XHI8dFgnAbqnlavNg7Iv39CFOkV/TwJ1U/hj2UcM5m
wjz8DPoZkKS9ffnGIwFM6mFFaYoPCUt7sq4CdGv/+YvCGTc/kSQP1hRsWTye7GVdAURRxz8nZsyG
itej9tXOTrliwq9mnEqyJG1AzuVxBSXyo8oAnFFX0h8JGub1ZZkwuMGBrxIQOZle+HZiTwbzskDz
Y9fQDDFtgYvZHl+DLyS1zRs7otjTZ0AImeavewdPCxx5t43gEYWJo1VcSbMUNTXx174bOr9iii1X
jGm5fiR9hwF+auQBSd7YqZ6mgtGW/CinHxFjiJyuWGoB1/h4YIhY+X4VIiYjQOn1iyNtOTAoIScs
La8DApX/95jd4r45hzTSBZF/oewj7/CGCcDgIg+zZvv1Ed/rmsdG4D1g70zu/tPX7NLitPkWKvqX
P5DuxtSG4COXgyfXeZBmxFdLC+Zz5qIjePHZxGk2dPQRozciQazByZg++6XogWWK7hZAa5ON5gNY
iftqbrY8PCnuwjNNwS7NRGBoVEHxZYpHJVNiBpCm0SQgz1MJ1HOfCf3YuT/TSS20z571Yi0csxFp
SdHNc9/OJcN7aPEctbzjXb9RkoLjWG+jFQcDHcdhpbgJnDetd4QQ7umXYkvvUb5XrEHPiPSKpKy9
41TqnTzMyQdTpkhAzvENG6PzhLQm079sAd/2cPWIt7QgJ2ULIlnQlPnI+AU9ik5/D0RTZ8Sska/n
9Rt1WOF3Ogg5MnPsk5wyW6in7qw0MMMux73hmDZr8YDf75/MitIcYS1N+vJrvM+YKiXpu7FB9Q==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_22_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_22_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_22_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_22_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_22_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_22_1;

architecture STRUCTURE of design_3_c_shift_ram_22_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_22_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
