// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:48 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_3_0/design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_3_0
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_3_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DoYxrtPwq+XfYEWnhSDsLX/VfsoTuUZOad7KFFGGABczCtKLiNC/C1Fvz7KTNP29a9St4cI6UreD
X0ATIHLnepmqUy1eHoMuvci2eDaJzoks0lWIs7ImgWFkwPFa4YkXJAZcURckYrKkC+DCFNVXE9ga
mlZLAN4mJZi8VayWTbdsry9qZE9AN5drNDX0kYFm47mdntqKzsWGJ26cFG3QRV36ikFFEPg+oJRj
EfzSGzPW/WfZjzXrMwGwksCDP/aaL/7ZnWDKtYtQqSjMfed9rqmNqwteapwfLuT4EPkGiz0cOMLn
TaI8iTKc+LlJ7CurB9xCGPIQt+nVLq+RsClx9w==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
4+vhKO001BrlRaJudcV/CUb/HvPIM9dsROZWHoeU8EIz8Q+X2A1R1/icT/dmB4fgX5glWwkdxbtN
CzXxN9S3AjL5756lHRYi7dDeZ35Z9/23PTR4EVUIYb2pLPup3MPAoyNdHZJZ+MzSNLihJk4AQDs4
75JY3s/N1OC4Ka/PWdvnZrLRpAoDKAsJG/w2hNydz3LmhlsiDSD8RkERqyGONHkpE/Ql+/EROgJr
oENYGlyEYSmzBgkLH6p+guuGIzwKa5ZpdKJegBE+guILfGEmDr0us6DqBjh5Pnvf09LFy2tdhh0d
WPf2VM1biAdv7Z9UmP9O0rsebIUVxxg6KInFfA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4304)
`pragma protect data_block
okSPjxxUBOpgCQB3bWVGILTkvZykd5+r4cCm1726PGS+Yc+kkey85BpUhNsyg81WEaLLGze+Xjgt
AiIAI7PVGGjn6zCsOdHBHS9KN8akBKIkL7FrI4xMIaNfJnedNvM25vtJjNUrMDBlQsvrak4YM+08
YtZ50DzSCOGXzKK66oqzQMPpsqCjiKvX9d/12RE0wpzIoV6RZstb4+UxzXY0k94ZMreyqLMP/RnD
HyBw+yiYO/0ZjEqLR3Fnxng3StNQGVN3p/vVF90mwefzeSRsdCzkKFhKRkZX+ozOJmrw+9o8xq2j
QCO2IHXpV9JcZNYUDw8XvlRaUK6bibpnM68cg1Sd/THN7zd64ye9h3bhmXyJretu+Rqua7ROr5g/
AifiDL5E6AiXS9byd3a77a8pR6J9B+C8yX6PaYe1QA82olwKeqzvvIB2JnrJl0qbaGPtx418TBeu
AdS15AVUhai9cwoVVoz2XiQhwupd/aA7MkarqqpWR8aBjj/9tQa79330yrtEWYcY5WmavPY00rJV
cB+UQ8yXmKzBPYX98K/YQD66O9LQ92npvzjtOZLQcuML6lks+GQTDFZ2+PtSwxxwFjBts5C3/iQp
Kw8OoTh6gCDpjpQFfoNsZLC93im3BKY8Qkv6aR8IDs0935ky97MRuuFBmJZAvWZ3rcioIJJNsV1s
5Xs/WZ0mPypjLRdDqBf7q3VCGGRmBrxRLtZMDwzx6fxkKYo7f7f2N+PasEiQIgjX+9WHZWYDu1Uw
mzlnSeUCxcmM+wOp3pcrBZ2wrzlH3oIC22WfZOKX6fMl1vekIrv1I/6I2b+sOLPTzwDGJcvbH41J
ljtoKrpWG00qV/VsDv2u3XmMPJXnc1f/EDV8YVPe/PTev/fJej6Fsaqal+i3xSHvWczRTYA6E3aE
W22kvLVPswaTVOxFApdL5RWIgF4VwD/evcLHcNg/8TVU0NJv2g+Bfu6siTIqn21IKdX9p3yLhxoG
AG2VDWXTE3dyZKKuimRmBthkFFh9TEcr/2skok1sSuKyPEYvQMqMrPbNiipDlD1QMMwPUJyK8u/O
RFp6/L0Ke64bQ4M1pGoEZ4fObZ8bp8dwV/oXBP+QsVr+P/afT6ds7qGtwnUM2z4uMDdc8aBqAwvq
Oq2Ge7yoee19007ZrjcqQPDGUsQcRE5YZEJdm2fPK3Ju/wtX1GXy59ybU/Vz3xM6feV5b5B/I2du
gJDaQd3V/J4Y+dfoc4V1AO3X9t22L8h4j3Vgxr4ddzUsQ8hzFu4iqLMCHEwnZLyhR8nxuYY/zRAC
wlPcLMG6qN4Wy33aLWBjzs1pk9H24IRtihhznnwLxoe39Yb8anVDchvEkaiIa8DqdjEJKvnQwFnA
tp4mnEMa+mp833lSSoL3eGP8fXR0OQGerP+hk0bvDARM58jhq3Y9JyomVhORqLhKGa7YOvJJ6hIt
4F70Y/h10eCD8pHaAQ/jlIzpaCziO+xjjzCP7yINDJ7dF3z1DmQnyjVrsELCWrrW5jODanEamaom
iAgKrsBFAid+B3sFC9OQ/FFf1YRiKb4lRzm317B2I/rIrro22C2QPq8R462KL+svoGt7ghaAM4PS
FYjqzAsLJiDibDAQ0xbac4+e35FLYvt69OVq+S+zmBFF/oXi880AcKflPcK2AWWuWCJ+C+Li3Wsn
dCLkZm+j2XU25HKbsRws4WtE5InCvuTNYY5Dw4csf/tw/B/ODcVGdcOtLlOyWqesIHRKCIDUnqxU
B/OTW7RlQFHbYfuPdvUzaGsQgSywPmMh++zrm/pWCA0NuglOy6AGC+0E/L3PhMW4Hx2sbA5nEP4r
S5p2tzPdC6U/3slRsV8qSZGU9Yc7CKpBT496znBDsj3eWYKnTXWcC+mUmzB5V9fXt6FlrZTxHiWg
wmMk7AT4AhK8sKq1qItBkfTvtYEfmsRPsI/2Tbz6QJKyDOrJsbOO+7MQADUNjAmLZ6K4JpETeob7
U1Wnbr4aJf1hda9oadraAfr9Bl2xQkkWmwHgHZHXnYq+xISG6oyYccSexEJbWXPqXlpfDDd4df1N
YOpuu1kCSW6EOHcGy4iZO4VS2LEIDb8lQESOwUgRkohoVCBrZPZmvxxWncjidV5QWPa11gWkoqDB
MmSbDMF/rZj1hWCZKVXfm97FN8Lmxxkfx/+bSWOgZ+AVWR8x06wOYooRjl86HOXN3ZVhW4rF44PG
PcAEyaWkk1HEcFAnoHAaDQL/1RuRYRhv9k2PMmFiC+qIZr7vz20lpdwwjmr9OiqaI+UHsHGSNh/u
W5+iHT0gospCEiSgP6eVmBVqSCmj4B5fN+U866BoG36wJakcKH6zdWVJzPdFWElr2OiGPgTNpWti
biOwCIHBvBcuD897X9zX4ypJcJi1jZTWbIhTEqen3qg9+oxtg5aYESZ0TvbtDf6tYAAs7GIgzDDv
zfdBSsM8DC6WmOGpFzzjOokgYNIY+hk87A5SnXskaFF7y24vv9gcHiZVwAi20DHI67sHlhAfIbdT
tRkoFEPPnqGfEorA5SeDxHdPSuzua47x3ygmkqudGsISjNYGbUYyQ/isWTxAIvYy6jrZ5LLbdRTq
jU2OHIWPki4BCG5PB9j/jDoWU4Xdj26sBJDWeslc67LYQU8mWTNjZDLfBvttyOW+ybPw0OkvyuZw
cAl9Qk5CyARC2M2ksuTJLvCxnv7BmyzGEHyhXcOp9jM+oJJPLUSrA2/E/tbje+LDq7F0bZ7sENwQ
r20gGw2K+bRYVblYUTv/3T8/T67Qg+3lDjW/Ld9FcGeThB4AY1pT/3YXxE7EK6wBZIj2BhisO2If
/y/sVTKD1+3TJRuCVFW75Z66o2pB0cz78SyNX2HKLYWfs1MGgwbj3k0PYysHexk5qljjd8/Ya1sB
EmzZ3Wle3MrrcjX5Aqnsq6OWo3/5SteiHsgxHxJXpgE102r/mD+R9SpAwVQmskJRdtcCdxCHi5mt
wZG5e9UXv/4r/pay+k+3svhe0NI1dXy6GKSa1rFLPiw/wYkDuJyuSdg9iO3MIzD0EXvbLHWExRKp
09kc+RdzcdNDuG5wlV/TNKNkSgNq5hNlkPtjkGENfJh9vSEAPouzYl7lD2TY71M+lN+PMvoop2mU
BaDFzKGy1en0tDM7Rxf9W36pkTpDZEGNA3RZmXfD1MmcBFwREsPstjXzCJJ51pObHeipbChrDnrT
f2D7DWU7QousiM0OHTiK1iihAbxSb2q7EnJBm0EqxUJbYNhjDePbuESKcxJnzH4dBcgjjwYAqfN0
8Ql2sZqajy4RCcXccK+EcR6aKv67o2dtDBGodQMiQ1IVIX+L4DWFTsf6ftQbaoMIhKb/X6GnluN+
8WJLI/IyLJyxbwIe9zQcLJLB+IH/YcEzQGwB+H+t8Ib4/Ia0LAK9sLL/XWTzt9gozoKxrqiTZVsq
iQiuHmMEiFnFVHA4W4KEO1l43ZGbX9GkWDMlFcuKtz5Mc1Ik0X5rSy7OX8fdfD0ZmfpoQ3P0oRhl
89pdBcGvZSzree+WHopgrZdZ7j39BhhDO1v2iYmaRfNsxsMka4j3n8sVgrH4YO7c6nIhafx1aMQU
f+Bzqcxuc/rZk7mEAjkCzu5xSbTKCvAp3tv5tDOZGqE12XYwojaTi0Oqb6tmCEW3EV2YrzDnGOOS
TnN3iysDJCfxcg6hcUANfQIq9X3wF0AdgOdmSPm4cXleI8VUekuYdY7H1cyFWaDRxwpAH+7/TyWb
k6LcBDKRIqK0S1yOA3pw6+Aj68huCvmeINib2TgQOAnFThCG07uk93nxrL0sLxf/kt9X9IuRO/Bs
cy3fat4khjUVWXULMH8pyelJvC3a58d0pRx9aR1vHOOBMMWNolWRyBrxicCLFV3yo1LPoo+WtUWI
62s4Jq/iHnWOZ73GhR+UzWpWQI7DxpbACCXtIolDLoyRczej4qVsBrukYWssjhrmIVI2QsZ+pIvU
e9o6MjjQokwuwcELlZnjaMv7Lfj11XjXt/EFECojoLaMFB6/sulEuf9zUkn8e7TmPo3V8eW4YnpB
XP5BDZ/ComWLSslpV4XfbBhgpxZ82KqSvc3fm18Io9D8TWWiQ689X507fQuptCzgtvmzMCvfA1Fh
JZcfHX3ZVG8FCjEKxGOsZVh8dvg9pw6mVATHgxaM2OGjkMAhJMnJxfon3Ej4xgEbOyxLjgSBxumk
8FiNBcE4SZshwtOU8sJMk+Pkzqyp9hUgBe5qiR31h56M3l6Ue1DCIWXu0clU43ehHZBYopiNcYxA
ND9dcTbZrKxSC7LyHf2cSa7mE+QlqnlpnlToay8L/cc0a2r96+KWZct2mzBGA9s9s0TnobNBfpzN
uH1tYssP2LttwzQZQ3j4FIaNhA1sO+k/8IU0oMXgv054+EiOKkS3TnT7sxAOolzcz1kk0Y2Q49m4
y2lTSiDAdtes2sMpYk60vJzKBCGXLJmPmp/0amFAHzOhh0bbjq1/cGzbMTFNWznLyHT9q15qKz9r
5+nXPRu3MtLfU02eywj4ruKrze2pGpf/MK6IMzxxIq1LEg2yOXOmiUvb3gp5oQSi39jjaDa8Q6KE
dycgOl3S4GSsfzSccfupGtkD8g3OUx2Jr1eprdTkMhkXmuW6z/mb+HM8eNHR6JXJucg1qXLXoNbb
ERBrU63qwF0Ah3uAhdza2U0X8rSb4rBUww22jOu7hTiV8ecgF4l1hZJQlDDPz9dJlsCFWJqGuvpH
DuIzZYs2GfBcUx9s02LnffPRM4pwXfXtOpIB/7kEJ/qx8zn3fcSu7EIJvAu7TmU7he/R0hQqX7Rg
O1R9u7AkZoYYQsAGnNd/zJ2FGwohO4AEnlJG+adC8cJKlWUgKfJde1xJeCSZGvnIX6IW4VOmq/4z
hbE/BsrwnloteOjMzYZEFwZHRIkhRcA4r/sgAnbu6fjYCxw+MBt/Z/jCztHlPBSVrDx7W8iO7ZK2
AEo7ad6ImwnFsY9ndP6or0p/aFPv7/Umi5uCBTvU7m+dPuHLLETE5iMCxo7v69F2YDNwcqcG2LTm
2QOxjv6qfsJwYiAKn7CwRyJkOETWLdcCCNU7wIoDU8VHwiHVbnIMZ9aKaRLGn/GXCTtV/jA8IXDO
4JsJMLqWK8+j3+LCE+cLRi1Sr+S7oSoCbWgHipwOFA7j7Yndt09KqPfIpNKIjahRAy9a73oLMalf
uvlxN7ZGjwo0/KfUCVCffE7F2sRaVYIixhhpfVKkhoc13oiFI6omGRAVDjAXu0T60ycAbcC+7B0G
SC+OtnsMvv0eH1kZWVQvCsZcg2UeEMe+v5FnOhJj5O0Br04ukg5Ex66uHcW38vf0EmxAhNuJOlzA
v0Qn4k5+vhF4Z87zipsJBXloo6xVg/KJtz8JbNXNdTSqw6HSAjM4luxX/qMQdnWBOR7d+/w+OKqr
MQgZjCSyTUn0AldG0TADvzNMemaGaPCSkmyLL/2S+M3NwLvvXcI3G6W8xNjzCC57KZno6BfvHdWP
sX9Y8q10skjEv2sYYS7cNH6rlGjn/c8Yfo51v6kCpIA8Ao9AOhgAFT2LPqerr1YaRSYouPKxw76V
Tndw/hGByDTmEb9M4gOBXc4SsVlxCAPKH6/MtS5BkN8gp/E84bRRd0nQ2QCpVqX1Ev+ZraRrTWWX
Lzx76NhlK9GsArfymoStBKxkHNg14i+fWkaBAYXiO0N5oIfCAtY9Ul2a2XFQuQJZ85dAmVluM7D5
pl4gFUUZJXDokAbWC2/zX5sQm3mWWSXtc9tbSrY=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
