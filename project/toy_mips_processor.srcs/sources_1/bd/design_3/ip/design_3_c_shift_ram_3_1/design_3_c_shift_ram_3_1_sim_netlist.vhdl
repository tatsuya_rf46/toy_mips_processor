-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_3_1 -prefix
--               design_3_c_shift_ram_3_1_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
G2sTpNZZWkcbwT+6ixRCUhv6zZubaz4z9fTEC1DCv4B1x/DSxvOciyCE2UJXO0o+EOItqjVlwga9
L1uHpcIyio1ao2qroNaLP3dNUebOhrsedLks7+T2H7EDSAEvX95Fw8j8XuVCCmDzLzjvrplwUz1m
MNSogPyaPwcXJ+e9IxYGz3uEP1ajjzGuQTEEsxhOwesuWINBikGVmOQbxcbNyErmbMI7ZzxleTCp
kPIf6da2niYEXRs5NiCFAjj6sNIk2wYCSGZPeDI2511QA5rREsCbrNYU7vLEnlgq5s4xG5ClXJ2Z
YTwrVe+/kTTESJ60jiSoWlFkSslHh8Nc7r/pF4OAuCwAgj9ReY2+YByPw8xtXcuNO+gB9v60Rdkl
2ZbnAUKx5X0hE7kJKTrlQw9lFfzeqg3BuyBBFY1BVfgQCw4BCdnnDSmOnKXYrNYOkv2yIslQ6TFA
3sP6toEj6T7gYAnTGnTv5sHPekaifiJTAVdtlEV6QtWg/ehDrlFSqIvzz5e+5OE6N+p4ilatoZkR
O35zggVkWueMKwHDf8szsSuPPIVjsrJHcxtL7rLNO47HMG+RK8Byu46EfI2HBYgEkdFOOi+P68Mo
DuTF50SqVgMHrmkzP1SDvNeURG7qFp2kFAj2VuASdWk7j1T8epaRs+SRT7mRES9XzBoC9mvfRnAe
5GNm9ZUxq9ssafBYaZrSSoS/nc+h/340T9aFFY7uRGJJldTyNirULwY5T8XZ9y5on5q/LVoRrm1B
MpVpjGDPr907/N4RnkQg32NxeiVsR9b8vV76EYvm8SEslHuBUDFeU6xx+X7ns1aRtWjLWarBwNp7
y1aBZo+d62jVrBSv1IUDh2mncwBxFTjraFHm8Ve3TzGK2lW7k9uqVrds+g2MVnI4Fn6Ujthh63RM
9+qajg918D+WOp5IMoJVYNaCPke05XO16Ec8aspT2v6mTrjl8mgmY3oa1ff7e+LmB0ZmNjqZBn46
jNLAa//FoKhOc4r9fX/g1n9d/OJaTPrm2Nn1gm8AgxQVHhkXnzk7zVqyKzX8WC5AHwiXLRW0RCvR
Yuq4Y7pxPUyg36DGxm1QOgDBnVd6NU2ukpLyxDtHn1o9CvB2O/Iuox+xnSnrAIH2vQPjp1R5di+e
VDAW+OOV3sMX9wnhHgtADpvQUjPYDA+V9w+Vk1aswXvMdSqoiIG+dCSmiTPzwPzh7qZ5BJy+S9hs
/v7eHZtoHyRLLQEAjc57NLTakTB40unu5hLd2JCTWQ+mvutFFpv4yR216vDbDKV14WvfDvKeXqUO
mKQjuD1VJTTtSx4doJxEL07WCq++LLjdGh1Ll+jI4VsU/Yvsap0ZCZ7Rlgyumrh+5ZBBUd1mzFAm
qygGxE5rhikFByrafHgATAbtw6nMhEKAY/mgc0bttp/svLZFYvrEXV6MUJi5qGl80rZjH4Aw3MSf
NVsIuWbLdydzpjnOY5nLoHpFDmfNgjSQVytIheQSRTEqSuM31kpb6+Q0X1ZJHx0IobEUl+KfBfin
RXuOHR5Yo5e97xusALF/iV1KgCavgRW5kazs3++nX2J7sIIFgAbxwSi3hh4D5MUlKNA89/LULBST
AvzdigHbHcEQLZVEYLwlDf50LnuF3O/KRCcDdXOgg9QPaYWpCjIvKRBWsTp+pqm0ALKAz9AWw0hz
tlXTHTzHexm1B64gpH3m5o+PBUdJtNNIn2DhXsnjrdtQChtguHAbpkrlN200ozOMYaVcZ2rt8/LI
m0wR2amHHdIZbyQGdZTrPTS7l+qVlHrYeenSVKnX3CB0WUxvj612uJmtXX1bMLE0d7ayO5CVEF/a
sPsBbhkWx4qlWjdy9J4p8G7dd8mQt40SD91tAikP05AKjTPtHgyOP6u6v1CC9U5qhxiXul8hgFdy
Eml+Jy8xvYgvlKjAt46sYtSvLYxufK0SW+ASknDugFekvkEmnWFR3mvn2paiVmisIW/pBRZjfT3u
CGqpVHIGBX6wpQ1WXoQQk0x3vVJPa3x8MK+lQFPaMut/qVEBiesDVo1XK7nanljOlHy6Mkqr4l8q
cpuJOcrhdIKu7X5Qpxci/ssngZsgYntzkeZ0O7kSm6UKSxwl1Lci5qbNhOcAju+FWQWZTljfhlaU
BmUn3c76ilpf7ZybMj+gs7XElr6vqflfg4JSS9/URJchnDxVy/V+pR9gELmRUR/qVGf0az2QCDzH
CIVs6/fTLIGLX/QfcDNFsf9noa6R98RYijO5RyMHax7exKBN9M76LmwllnPZugYM3mHpnVOPgvA2
ZchjeS8htHeLI1gZfizYz2lK4+4f2Z1wuPnPvb28otcvxdC+wS49e/rYF+XQ2CleS/AL916WfdC/
XzG6Iuhb/OmNold37bsBN+wkFFxxbiDTyQ+V41sXzrf6w5jZRgSGtCIsO486AhQYoHSe50+KIs1n
bj1juTcB7lhb6lZOsHGaJ2StbXgFce2hB4D0PR1MYZcnmf400EVopDLiYjOTjts6Ee7o0j9tdyC7
agdNCFqdJ/rZ4HncIqWhcLVE1yR7HIcbvmO1ZOpLz7tE4c3AqsGA6r8VdRi2FNBL3B42l568RgQK
rBowX+dOwEfKuFVj6kS5UDvmOt0M2yLDp5irMEYWmOC34shcLwFHxAuWVp2LOeymF73ii9B4LNCS
qY8xahs0jceFyRNkTEtKgV8y55Peiwduo3s54gg1bnXqsHbu0USnGp+WdNmEBMxFYqbcr7qkejOh
R54wsjab8shP3f2nLZFRpCdOKhwCjfvvZiv9O4v7ZSGoc5pq/PLoRjdhXd1vpHJhAJmcsxuiEj9g
NBKLYZ+rGb5f+Q1AEQSJRl7kkX01vO2MeYeSS5MrME77gAPW0joOOTtnxSXvwXlzk0mBq0H09xlv
U5cBHZiqzzvu0uW9Bnq3fIgcwFBilesIeFvJAmNmKo1r/DYHDaYuzxLwQmDoSJm4s4KZWgtgk5Zy
9e4eFGEnPIOhlf8MxX698wy0rP0DUCkkWn63LvsC9HVa9lhHYHBcpcFcOKmkSaspUAKnbCT9lIQ4
XPewiZWz4iBdAsEo4CUxOKwvSqhFj1vDA78v1N8loDQPAX0Vf4xSypbSDhEAh8ssbeKzQHRAdFLv
nit1iiEWyr6irKKyEAdQtn5VWy6hnirF1K9C1q1WlmQYd5T1/tPhrIK4oBUeMkaMdde/vleZ1BAE
AQh8ALnPWlWFivL2yWJOUZ4F/0BzK5NGezL0HZEn5yLFjJBoeeDvW0rVPZ4z7tLYaqzxpAFA3xQL
SiuD3M1+DpgalTKZSrYnl37tiMElhrnIDQO33aUwzTLja1iNSt58iF+0ryKRfV/xGL+VGEXq5bGT
n8sKmR7DJC65s7EIU8BgzBhaTJCr+vsHWVwqD1x+osX29v56ydobQLKr/s0UcdHdp5dn1tKRIZeh
eRHiAmxtV3Ys1kSYFLgb92gXFNCRmctvjvHytcDnLDN3Rjo/7q8WJcGYMSPjW7/PYYD6jrUgpFaE
dQOAgUn0ZjV2uCiE6CIhV6yXzc5AL205uMt7rD0g+1DUjTiii73D/fxNSCcaFu3sH3MboE5RaifQ
R+G2i88xb7jF2j9/4z6F19jBQanEtwZX3Nw/j3LL2DJ3jojRyPndRePPgtTvtuWA2m8HPurDLWWY
02W+7aILdG1IOUyWUbGhUtv43N1vMWs2pgIVdX3mT2eF5vuef9pI5soubG9H03R3G0YLWTDrQoOt
sO0PqgvYVEqc/u96mdlEdjaswSztAqXS4adAizdDmNw0G+Y5glvNlPd+9maiAvfj30lSnQv0VtJS
y9kglR7BHqtCJZsPehaVuiIOeOJntapDw2CYBgocCgvUk8kGfAspAJGq8ByKVRf9FOphouGoMh8M
Pc1g/oz9AT7gazRysqeyckoBhdVkBcVzpyuXgvwtAiMjA8H/DgacWJOZH9ldwZ2UjuMsN0/Vlf7Y
GoQstdQoH2Ectg0c7uJ4VEScOWs8ZL9b0opAYHvberOa3KwiccqT6BIlnsQa0Lh6VU9/YC8YlY+/
mFM/b9Sxr/8yXzT6uJosSUXkJzca1EDS1lbTNUmlDpUsqXNfauNhcH6mhnFxQSEmzbs9p7/DQeCR
CySYI5uSSJM95s9fW+XQTMgbGWsY1TA19bv2vJzMucIx88RWlbmIL3V5T7yJrj/oYAylksvQZNPP
DvdNKfE9r3fHhmnsT6XCuHze6y35gR1KxtjNJP4hXEm+KwbpolmARRQh51ljZO/op2cSa2Af5TSu
n7BJU4mH7hzNfh81Ex+G7X8OnNeLNytoU1cfUyfa48WUj19XvHUFuAkdyt+lZAqsKy9WI3plQVyp
1qDIYwWIJDZnCkfn1D5/sPOg3TgIj0I82Bwk55OTG0jAAL9kLF6WiuopL5B2nUbFx4I5b68PEu9X
9P9FV//KWiqmzJhvC5VLrpxbv70Uk1mAomSdHEvnFBdhTF91RDcF+T4Xquu/TnVkaoE8/PJIHWN3
dlWR5a0JYY79xP+xL9yEQUC4FOeXdLFy0GvDEr976olYMeGpx/7Ht7SqaussN/72uHP7Cccvv2pD
BdFlKDFlZVDEMuf2P1xVdpXf2RJtXLKq1y9bfavmjqt9UzSJR3arlyMnSGOwWCQzOZ/VyVZzX0Ra
7DocTY8WoO3Km1zpJUOqHgz2ukKBk8po7qtcneFj4mAM97RJkUG/LoV2JuvHjNBi5Q7tMWB8ecI5
0wz9cBg9tD8+c4Jl++i2Hod2QFfRFfNbYZWCH8Re48vEvRJotpYBGJXBBXta/Wn6q6ioh7VutXE5
xCMe2AJfz6O9gweoYqmec7ryV7GSK8c6vNhqz2NEFHxObfaVeMbkGxOcIh5Nr2MmA+C0xY3c082B
BBSQQwi41y3wt/l3DWZQbn9mhiJMTyiqs4oXZ1FmY9+Qk7uOAie2sdDIhulMTo6qLAR4EXBL3T2b
LOTnEQbriEFTL/wl+EpE1lFXR6AiTPO6XCQCQhsmIORKm0DtY9I7Tho4BT0dNBbsQ4HIr0iEvB6y
Vugb/mwix75xCTSzDWOibqc8k6imXuqCu6+FCW+NoYp78n8ylrJ6hIuwsAb6F1r2KK3+OoriPmPR
YZ5T5/8H7vG/Xa9en1SFBn+UwNlmYq6h8u07vp/EtboeQfMjFaU9yjzaQ378b3bKHZLh+k6+NhiS
3Rp96feT4t/5yqqNn/kT9TC1YTxCoGE7V9U0HF+qYW7v1JiayMPZ6CfgDvIvFFCABTo1iQfZqhIL
pQ+LfDRl4qZxJUpcf19/S8O6vF3IxJibbZ9cdhDL6+EZb+uoWUgYm69dV5DELOAT9y9NrMe3d1rW
aBKt3INBoJgPbIMWzCvfLd6rXgwbmmGmuvQvX9QznUkljzWa8d6LCzpW2KeDc0cikZTtFqnQ3aE/
kgzZiA1qmH7U48N5AdVrc1+Qm8MelmgG9y14OucrGTvHrF6VA8tvrUa9Xm8ceESjeHTqa84haZHb
cR3oMcyR3Yx7xY2AHl2+HdP4cnRUFd1fJPyJdbqJqiCCAOjz3B2cVRyaj6ZXFerVcfx7UWeyRuFn
KWZkfqpZm4ilK3FDLs8S0NaNKVVc1TS+WruxMAPgw00VniNKwm4RI8AUN/lxjcTGVelyf2BD1d2n
UB79R8XGv4SX7OzY6pc6PZCpdK35CoBTs5J/5Zt+DR5yGUqjQEFv//ddtsSkWRXQN9zDgTlEZ2ZM
8aKyIRjQv77jcBGbvf2lGoGxnRGFi0aKDCQCz8peHu72wQNNKBpzSlSMtza7rljUxGt4jBGtzgjP
+VE0flyXbFrGgYeEhHVF07rYbbYqQXUiqPjxJv36FDFQVX04rvDf0Bfw01sJjhjH7UflYY2nTFrt
GjZM4cKNBrbZm6zgAjf7I1VGRWji4bFOLx7wgIMwdSULV9jSTfRkWkUl1B1mB+7VJJstEGo20L2N
wOU3kTu4Mtw/82+7JG5mVeDpX6ZycuFnGlg9ldX8lzFtg2UZ96OSl83DyUe2sZlndkPVqHVwmqiw
VEpQQkbbBBxIj1aERHWUgleFsJoWnbYpCJTxBjFqrosy8PNDTB2lgXlHhPmlbz4orpvKFkQeQURB
v0/a7/ksfX8kdu/S3ca8fP/8DibJDZZxh768Pnt3sO5fLMoc9Srooy7MXa0CQf4KPHmH7sThIrbS
2r606YvY33PsHV2NnBv7QgBiQSYy3Bugt+0/Uvhu3HbbbC1PnFAAf9gP3Yhi+YbAgF0QBZm91a5a
+ZkzJ2CsJ9Lq1XaNPlbSbRditNXzPRrvbCacZxXGxWQjLGYrABHFz740k5Yo/+UfvmNHqrM4dkp2
x9p828F1eOS3sBqfziKM8/WC4+Ck/6YDpTKZc0VPqQAE7ztkzpHhfSn7c6/9K5xwvJsEsvlOLQle
HI1F6DMhT/60iLdLGrKlB8vwTN1Bb10w9Xd7t8mlNB4cmbojqmd/2myAFrlZwZc+Vn03jpGPPAKj
VVdR19Gia0TFo28zIJDU3dr12CwuZYZIEinUiePTG+u/cRQ7RUPyTezkI99zdfD1iq2J5yLGyXi+
MV7i6jiaVAQIo7B011mqTxSBATfVo45EHRKpH0PiVcTfnZFsUiv+sLN6jnklurcVJQcomFABWCra
F4rFc88gqwKWbalzaEoDsm3Fo/XIk+3d1CVKTIfItuATwgHkRqHm0V8r1om6JnSnJmu839vo37UT
629WGE/4V+phoKqlvBAabBKPIdK2Uh5XDj5b8H5qpT2jBhDgYynE+MDi/5ZxoEKJCKJapKoQ7/xm
6/UBzcejV17+P0nu1JHi/IyNZF1/0zq5gsyN1DGN3J0HzEYfNzhdCOBHF40kv64MfVuafaQ7NMH9
HFGpsa7hgPUR2T4vi8gP9LxLIHGGXtphysScLu4Ulgh8zPM0D1fUtcrsXmh+dTjAcnb2vUgbyfCx
GXQbEc71Ex5oT9xkpxoUmZrVAFw7h1ul9rz/5KxjF544wv3KLzC5x+eBi2FFBzCm+u/4OqDGT8gG
vEjGwYn3Q2qPUH2/6UUeh7Ho19R2joYPI3BSjh+q4ry8+7JcIFe7iAJtkHlUV1AGBwfjBUC+0n+k
/lRtG60tNDPZkSGuPm9C0Z9ro7fwcKMhPrvgzpNV3or1e9MSl1hnGDq1dWausjuO9HLJisjO2e/T
eEp+z3ZPFUPbJoM99yIvqDvrpSNVYszurspwkJRHP/O95nimJpk6LrK1DGTMK37q026wRjfRXaxC
QRJP/6wE1hIxHAkohXLC4/ibKVTjqNXqXqdF0c0i/NkfMjprMGHI2jR70GspBz1ZP6J6WVUfHicW
sjEkAG6RG/J6xEGZd2u1jNehOBOEb5ALPbMXKFWcUxe1LmMVtFqiglSwH14VvcyMtE3lm3m8J4Ma
g2naJ7yc4Q1g+a+nGsmVKYe6neQ1+R+R7Q30PuLMzlhSf5wqCfwPcKxRpQNoiyjwbbkN3G3r/cmj
oWqq8Xs2TXMPF/8S7m5gk30OfGjmHe5VZsiYRt0nOG5LRAGcWAiCJXF2SlGqVwbg/56y7OpIWhLk
whJKCryfcLEENqSMNAnGEzPVTQszIPSQPii4R2EQ/JTIFLb7qFVyMf2AqJMV8BEUz3PUh3/jTWSs
e1PcUH6w9iAwOSxOuX9MLXjc4v8i6fbFDG+knotLmd2Z5dbx9JsfioetIk53nYsEo8TNhgE907KB
27r6m9y+8U8z8z6iJcPb4QBcSRmMl2M4oEYNwc+A+tH9L5gUMW2G7Aas4+Ckds3uQlwdrqGLPMQK
Dmjuw4oYOZsm3EdoEFUxXoGq8qkOl1i6D+iST4hK79HjCyuVZOT5W6uhRRE7XEItD7Qq6EHELS0r
yaI57VhaTgY1JqO7KCamyBJWHUtdUzKtrM0sDsCwXEKxUl2kMkbPu75iL33t9JcoMhW9Po5Lpx1d
9f+kIDexGXPqvh8CQ3xMj3uVZBNc2/esdUtEIkMxOSH1gznwZQtrU6MoTubG6GNyh1DhEE9jF6jq
buuWj7Q+o3mBkOzJHElBmUnZKBW0STO3oeA7AFdEbra8rf3GsB7lzcGQ/iu5DXb0SfR9itAUjTgS
JPlm6slymtodHsDMBFHlt5EEtj4fEB48NIflhGFMZHHIz2ZaMivh0Aje9alp8JQ9ikSizIBzGM1z
RcHrY1wcbdFKhJ/5RvAFgBpwn9N6f4LV6GRSTl+8JR9NxAxjmJ8Ou71r96KL9AbaYW+xKhTXmA0Z
pae+Y9/eJmaHzJetGUgOIsz17QA3UZkvvxC3eNwHwpbjoL5WjLM6TZYEzBSS2iM2ppPNGpQFzM/c
XEKPHWzFQgCEnOvsIVtx8RXMVwhlwScGFdVszmSbUROCtAf4VqsqK3YI794O65lDuOkBvAvOrDKl
MgC7dZOpP6lBWFYWtgpVPvU1vJjAt9CSNsddekYBFgZr7zLHsDMtQGUL46H7IElVKShP4hq/q9mP
CpcGutvDvZcZqMtf+5XmRlXD175Nlw+awaWlkaUYvVijtw7mfTmnVyCvCxUVdpgg+ekGhPbHueK+
GZmoqdCZuFuDwMrSIxpa53eftnNJ0AKmN1jzgYse1CscPZN5YGR+w7McZlAaGEXba0Y0Y//2YocF
szMO5chikFcQpqvFNr+D5QJT6Juu95LUWVk0g9+tWergENzgAmXL+xhe3xJ7rkMw6/gYgA9pV8IX
8fSp+2esDW7PIsw8T9Hz0zEyLi9LxrtI2RSTkD1VjdPJUKXquR/6MNvdKw3QtZ6JzIXEGNEemCVD
TmQZpHYCMoCMbgAKczPAmpebgqplvaQum8hMKzbBVmGH4gFHIStvFvP3hn4jXHgWIfu39KskWkyb
tkrUdsRQxw6fjLkustxW5UQ6d+9F0Cj5a2i1HQcpisGcbTGqsX06OR0Sgs1BqOUsyYZZBRbkWnOz
XTsB3XIDj49owbEqw36BR8/LqnttwN5+yAp4tEJxmWzDBZIRPPNEkRBpIkRBtMm1GURX+hbr9igK
hYY4AuzCZktm9SOsde934BRauImEVG41i3/W8s6sk1lSszsB5MT6bLoDyk4Okaf2zNMzRpJKiOp/
0rYZjLR+7HG1ODt8loDFeJWyJv+D+JtMs/viyjdWmLsvttsOPiwS8r5qu+GPiCBQ2S0VxCyazjdK
Vzko6odU7mDuU5DBzVr269kE/CWhU8+7EFnaQVfabeM2SGDgBUQjaAJPXt+P6FNUgOm/BjR6mHR8
MLrEo0NzoJfWy9gN53W4GboxsV/FxnBXcNxF+1sddteACqacPXeBIbBwPJ+/PG1s80Kpx+m9q5IC
3oZ0E7lNs6r1lVnPeR/t/vC4JT1aiZFFr8znl1xdGsQYbai+GXLggE833/rEx3MeOaKFzy0or+dn
Y3zT4Ampz3vepa6EAPQ91J+WnOusQduMbtgUmX4A16d7bFdHpwotcU796o18l9YcCfMiqmyzuAkR
/WMxsgPiPOMFG+omkIYEuduXt0ktfpN9OKOku3YAvtpqviDYL9fQNESF4PwinNSSGaBuc+KVU3VK
45+4g/LD52yKp0gduL97r8AzxTnKyYzbdJTiw++O6jsYkOVmZntJA4B7x2oXJfxtuuBHBxJvl4yH
ODbk8NLsdEVkUBB7HN8Z7m7bfvQ/Jtw83Ueb2yGWkt52KSP750avfb5U3zFVtseze+VNTz+sp4na
KVWet6eiuWEHRn17IBhiM1YnBoMXTNiNhPTrjToCO3hLh0+vjdG7s0A3f6juqdVavbZuRUNNceLU
oyaBsXnpoZZEwOugOO6VY97J6p24Mrnl41YuEYlsdiawwP7eiPnjHyib5+23IYiJ292sx4FCUmQm
AEywspklWB7CbxtfLbN5O8jwwwpdVIiqKZdefLeGJj/OZoJdV1u/WFZmfsZLv4lIzcIgyDlj7T+r
XqLYoUfr2vzvJ9VdThQC885unmMF73BCItdonCkY4nGQxmgvyyqgB3fFlKvU6KIhur0JjyLOzMVq
ATLv3IGVZWFjIJ1C7hed6bCYdQ2sxd09zS63AZvMeIsixANWn/CVD0S4PmmWF/GFkt8FVaTFRepq
oGvalQKcRyQp1H3mDEB5s3VJL8VtbMUAwnDjmYXEDZQNARqjh4YbR4InH2Ue/YTjhSJYG/hh/5Q0
gohcEpGNywQLBWFsE9Z8ULnnm/Yuph7BqNhuG72vqf7vUVaOFTNmA43eVutUxcjTi4D2Q3s6BzgP
pxMiDrlkUq3eMJoyx6SBceMI+4NpRFj01lhio1+aWbsffjE1Jg3omeXmRbYFANEMT6wtzABW00tJ
+ffd7MbTxmwxh85FdU+ppb60iPdP3ZpaHYalD8bf6swbeYEl3E25mIfzSKp1DxJ71aVh8gCzySkG
PUsmDkXWZ7AJ4a1axzg4cDDf9D0wJfTFPd1OMSeUza7VukIh+j4waB6taAAaQO4YnIT4Tm16GqzG
iz0bjN6OJUIuKzY0UlzxRTTf/WQvfThsB27xpcC4lqkty10Lpuue9N2sE5pndhAj+CDSAanJf6Qk
ZL3GPX1ToG2Pu13ZxvCuW5P0HlTGGMVkerWZ6tVThOuJu+3ais2fMhuXHXtmoZ1b1kKr0UsfEbWr
zgB9JrePSG7zdQbvBUw6/hfpBldv3VSmeaUoskTVnnuyw87yAs0IAAFg9jtAm+2b+7HbSvC2otf4
MuPfTqDRKbB6zK7XivKYglyz3h8MWLOhxb62GSvd0trbCVS8cWwv39p+LRVR1EZCYTrSe3R9jcUp
hoz0QTERkZYYIuSexU/plSYaVPTcTBq0QxeP+0ZPBsmfs/9mmHpN2DeMZFAAQIpwbPkEnfsyaTMY
XbQ39eE1H50ZdJp0Ths5/3k6NiBRpvcyc56kHWah4b/pHGBnpGXdLkA+2mx9+nWSSdUTv9nqbwQn
Czc3fHr6wN3a8nkhCBx4UMVPRS6tNTuafgBwALJDGN9hf6Vp6kwLO8wHMUtccExmAMJ5henDpErm
gwHna6NNsJSi5lh6Lp8klrDiIIk1cLad8rwzR1sc/Hu1Z+rq40rY2y6dhnZsJgEQA8R+hCQoG8yG
mXPVd7QF8QXIH86BPul1L6f3pyKlQIeWObuKk9UEJvCi5m4WE4QXeoAj8ulZoKPRL8aZ1e8TuFXL
78TmNG1DOnwM/6Em7VXxr7WwzxNTgn7PqicWyXeUHoPi21CKzF98U5kl0XJ3ZseuqooZc937k8d1
fvd6jTDm2tvRcL0Uim3rG4s+b2IWFndo2P6SRTrGt1At0eul7sMAQ76TY+cvQqqCK2SKRqsxiLI5
AX+oeovzBq22e/TYid94iSZ8VUrOrnd7ndafptRD3p+4PcK5ge1vA3SMLvL4VSu7VR+RydGLAxeX
0owz4NSPK0dTC1cs6DBxhh717pLItrmf5XbMWCipeSGedCSoQXWnawlRaAhWE58ucOCOpLoU4Vo2
Jzh8t/OG4dTuHbGADBJxuUmnu/wou4y9Kt5Bvb3ZuJaFPc/WTnxQmgLYDzwv7WSna9DAQ/ZlK0be
X9TofAY8m0ZOGA4+IZ1eq0RK1srDT3tyRQA1gGMcKzR6tCa8IK0tSsAovKcA+9s02lpfd9NeoeuR
QpDbsOFG8063IAkkEIq2QXukGMDxu8/C43s2xKq75p5TWqwYOVOaN6tVSkQ0if4QO5mF18tCcNIG
40BqEsklIF+1m9G1KSaC+rXJVeX9YrNt+8vRb3f8wxfzKhtoDF/JRnoIVe+mutKdjghNEibgW408
Xo45+t3BxmeQMrZlcS4laWZ1hOniBYdWcaJyFu33lOLTcEwTEdBaP6bHrSX3KSGGZByf0P/IBR6a
qD5jhXRCp+OrpgsAvTVXqi3UvgnDh7nb07FqktxoncPKJ1MctrVBf/D8updlf6AZnyxkPeQEphQS
6oNu81bsR5Uyb4v4kAJOie2I+MivBsmGASMpRt9G3xd1GAwCTXspajnQgP2WF4Sa03akG6y7Hw7v
0JojJiee0On6zIr3R2sEA1RVVaycjqJO2TMK9TCIcDugyvFwwCbnYNmUAZ7RnFnulpz7uLAE+B0O
KoXIdmXFNDabPBoIYYq/5x/FOnNWZu2GjPXN5CDyMcrmhznvan/7rfPa3gl8MNdhw+6C/m+IlyRz
yg5NUsyoaxTKY+EqRt2QTm4xatMNNY/3I1vqKEQoZLT7Yj92pLM/8L63VRzwqG5sSHWx9EkrwNrI
L1tm9bSkZv2LGAy92pdLoTeg52SUGUso2ll7z07+mnooskt9fGGau3/zbAkKrSKAeqR1g9Xhc7LZ
LFTz1i3oqjEEyda3pQurmn7U9Q1JWTXLTyg4iYpNPSCJWCLjnEYTLpawdrRNCp5XLOXu+TgNF+Em
iJsQcBtemCNRbBh/jY3rpH+9RwWjJX0nxrgujQzp2XKGf4bl0GIzrhdF3o18cUSwUCO4ZfwvDtGe
upYnttlO0pE6jR+NlMZTwPa36SIBICpkyC2+rJv6svD80dIUcvHO2qCLiPN1c10LF10EBMar+AE8
8171IUsb2EWG7fS03BmPaKFYn1Ao9MQ6drlFJQYtn0/WjFD9uXzrYfOArWXz9UpwWiCGEZJ8eQ1L
nlmsIi/nWgjl1bPfk1WDPYfv8j/lA0PetPPXtTf4u1Mi5RqAbsm3Buct0ZrqV58O8P6Vp+LXBtV0
4xAs6VmYAAAiSkRPGVqhP5soe4iP42bYvZQQgZTcZc9PzWnKBaHih5qymJJaYtDrKaX5U817WSR7
RiAdLE5QGvaZarm2d7WKhkguMBfxFZWWz1ns0AEd+h5BNFsB5IZn4/vRvFKd7o5wyQqUL+zEGleQ
DkJLQP7CWkmBjGDlqhD+cVyMtum/L8a+3gwySZBdWu0q0AiV31R1nMTWrjX7djAPjHrnF8FCemDw
/gz+4uYMzB4/AZJOS0ngj6Wldyct2aUqhIcMoVbtNfMlIuoU7t7C6lfsYRJpBbpkQLiDceatXU6p
FBIIE/Vr7gyVAy3roysScJUXqu/mjDHI+9waGJCwLjVf6nsdaVmWMCXRgQG7FELWX3hkSCIh/7Zx
eOhzoIogtjpw1RdDmf86gs6yJCLcUrGNSx2cK+s+0mdqsREH362m5TSAZNl0xYvFmAyPFkRPpU4+
RVmlrjnmA6+v/+pbsSlu/oeCIrN+aaPE2oBiJdGrpDAzAeWXqwWxMGBMGl6TjquFUWTj19NaFiMs
bBkj6L0K1NqEh4vgXtzVrLZ9FZCrTpFQflNt0tTdo1RKBuNRVqktOa/b692Ry1syj2NJc6KDz3Zn
VesYQtmmjon1gz4KC7+t58WHLn5p2oPhgiSNC6HaJ4cKz5iMDOq+rVh7Rz3nye5eLqUPxsKY0WOI
kXAg3G4R3mvV96hiFvwsOinwmiwTQ3KJAAS8XU5SdRkKA2nZHtkkQr19zNdpIwXzbejrM0Ltv38J
uevRXyEe7+MwjgJc+c1uH6yyzWUF5fwZuunqBTYRZuX6mJbC7vNuGtlkB00Ek6NYfFPIio37bUd7
LWADvcwXIR/DT6bkzTuepm/6f9Uc6wJjnxSiyPNqobGVXPVmV33uxj5HDNFxsc86kQeZfovkTU40
Ot6jhZVgTbZl4GXxuomKGc9Pa4RzxIqJPxfs3CngkSaCPdQukyV1e/QVGpVLgMwWD4KY3vWIxeG2
7akPM43vfN806H4G/UgczmQph+wgfhiByuZ8nwrswQ7L27W9CYNp0bUQ/9wg5REqTMsmHIRTl5mu
grvu97BtPL7jd5CoA1bDfLRxsjCrgtX6sSYl19HwMh7sVJUCx8HYMpmJRx0d5gCjPACZpEc44GXc
6b8ZhQoXsQS7Co29V4NHqW6hj4GvbAry1dhLQaV6eocbMdFW/w3zg+evzeCZDqpYsVoW0592uOn2
IzSS4ZHndTmrxHrXbOEHXC5croCH/TtBjsQnBO7e9onLAYy3uKqjhgJmJPIaVbIM9TJ8hwH5hRfL
WJf2fP/BJmcq2gmWLC4EUoHHn++J9lnekJynvgY/xetMapFsdcfaVQJgzne8BJFeAQ7fHX96w+fO
AALqbFZwL4xa1ehR0my232uOEaYQEY1KVLakI4/In58XUk2au8uGetG6KsbM+qQSk1zfTuez7Gv8
pgrhnYjuz75v0kUF3tDTeqi1iAsluXe68ifRWn8DaNM7TN+H/I44FK4JYOuFRizcesJLAjxUbXj/
OdoOXTkLuHFo1k5DLGmxmUsNo0syWnCYaYfpvOXtttkG8Hd5mcY7TSD0BY3kp2x5h1+BmK4LD4mk
K4kLhTRy2qEjurMUOKZGakdLOoiXemkEU6lbfCLICxM3Z3HtWZYHqA8YSSMi+gkkRI3YzWVvoPXd
i2REm4usBs9SXUrueLG6yiqwPf8Nr6KAOmdu6ZrAS0o0kKFdgrLBlDJq2aMfqfSsizh2OR4/Sf3M
kD615YGOqBwIlL0aELGq43RuGn23ONwmLaaDQ12pPJ8ykR9VhsWuczS5I7lvMjhm4CvTW6eU5/nP
mVo+W6YOW2wJ2a8MF1MYzjc5t2ic+W7D8jKog7jMBbQDy8VipTxJO/0LXmCFh+RmmNXIMO7h2WNs
CJWuxYPiR0uD0RTR31B2Scr4pJwIRy1WSZwuUSmopzjBeABMJwTsBQwMS6gEv0Z+FmQtNxPWQQlV
zpfVnCIitkrknjEPbPV+1CCrjC09PEcUMN1qRy+aTsxAwal6cB54fNuzTLZ9KfxNFhApFwB6fJUM
I1brGVyppLdCZgrebe9OD+0apv5I8A8rWFZdrJo89WQaTRcx+focIwBInA2lkpECH40vFqA5Tvos
0GZxuA1MxtGoTTRdZsFaeLDLxp6aCbqbhNXQ6vdPrSmtXv2HzA9P+XfVskBtNjbfiO2GxakwOt+L
2g3lH5a0AbkBwado4c7DUt4oINeKB9V+bCHLYqOAo4tk85FwJEVcjvgyL4+QS+m4xRJuW8gy60j7
nhGeznHbvb4140mYhODOBv9BL9ig4n+FiBIrWpz35BtTjEW0pb3mwX9rS+xq9X1croqMxf8qh0J0
56KsW2qX1UIZWZHW0rCTJ2fq4upR3tkAoWiwek0kh6EiWkKfRjsHc8mHc1ig8Ar9xj8w+77E4PRN
S4t9Sqg0zypondemlTUJLtwK8QmimmqstzsRHrKGQXI4/YKT5JCoJBd+ywvAYFn4fUjcw66hBPmh
LXoKrB3iultb/xMnS85USfuKBBwCc2HQQM53EswQJN1UhcWp3pDLabG3sGRkPOcf14ayylUV7Lq9
WFer1mIQo9Gb23TB2i3XFY7+Pbr5PqRaMZukURYY/34sHs1L1uCUJKngAILDKUVmyUKfH5Qc3MjQ
YOTt4cPq5dcCe6qdDQ6sulRg+/zM7SekUf3fRybIAUuIhDTSrgPMCAJg8ZaFCrsuCzgSkNipe0hp
Zr0MDvq1ZQDpeU8mCbzvB3y0MiYn3wKVfmnYPGHf/S78EwlvPbKx1NBP6NDCBRv+WrOpnY5jj51Q
icVQTE0sUJV+hSGXSTCLLT8qSgo6HEZtuhu4K24xmAI1DbCaouGXvO4u9ijvxV+xTvar31G10jpO
BpGLNvzoCQNeWoeoMWpZMwjx3QH3TZae9mIjwgHFfDysAqYrOt7h5EAllWXEQXd9I7bImtKEDOMJ
UEv5kDJDvmEk6mu80/4SyYpzq4KlEj3Ehn7QuvTCG77dD1PQgjwKba54/zO9ni7pWtUqaZZgyuZa
K7K3sivungyaebYwwLCPjaImU/Wykqj44MRXG7VPldPYNSGbAJvqTYf+ZHN0UlcQro7HHnafIVPH
3GtRoaXXMy4N0EWBESEtytxDGTn9+UskfwafGZVSh8M89mNAXbk10xHi/pyA3XEocDdElNWaYZ3y
fifhY2du+3sRkqUYZcG43Cpx26dxYC5IEOlnWDAqQR0PMf3pZCn9qFsA3vmkrBAa11DTEzLcrZGm
i0rUm/HfcO9J5Ve7d9cYmZ2rCIxXxbCWxlZJo5ZxGRD1VKvlUjdghzG8dcWYEBj2pg4zXiOZu/KB
blrYmZwsbS8ZA6GcGE9AuUV0/LyPbikfUu7yZgKwVbkLfHbEdkxzaQ2eitvjkq5Z63kdYZmkvaF0
7kekvwMKg1n41PpJZN2LaiDQlsC7Hty8X1wz47XraDt0eSjHwPgbQ3mcKIOdrM/XnilNOQa7pWJH
kt++VGGvkTijsDODek6cKfxj1VcIBhVIppBbD6rxCku80ISTuBhK+7LQo/ja+I2YDHBQg/h0ZTIM
L8hQYC88t/WH/IbMl3UXXD+FIAaDPpUWVD99tGrgXYGCB86zQZGuJOIBHHbf2WZjzeXoFLCvQdxB
4tw6rVMPdBw2SFimCWVYw/b9H1evsrdmn28MCkLDRXolRj3pwCodTYimlcdDVLaaco1uU+YLaKpd
LwZzAVYOpB+XSc77u3DuN6qAGvNBiIT4Xefxzpjy+mBUawoD8NuMd6fQTEDWgr0lGq+7m/2eIyYt
fzPPcL5qW1EZvDtaB0BuIyThs0f+dt5r+QRBLjsTLARdHV+xAr8tXJ68SVBCtGMUtj3N1FfIbjTO
/6ruhfOMseRQosNomPypoLRfLtMPzrFq3d0gKb94Hh4KAbbr4Mt6uk3qW3GCHqKKB3aYJlxB3BPC
y5bS9BI9/rXcZIZVvPalXXGJGlWenxAWh7+5B8UTCacwxoCmGJtgEWRS8jWzB6I3SV6xCcKNT5B6
co7mZDeEUBL+RbitTImG5XTVGSWKJBGCB8UCq+iHd3VVi/pc8DjeiXJw/oe6lEWARfC5Nmvi5Qqd
+V22OLdOerDtYO3nlUtNKFjpEtnEzEnDjV71ZP6UHqk7pyfnNuQlAiJML41sQ1FM5+AWJWGQGAvf
tloPSWaLwThAf3U3aKybBXnxvxk/5ThJoyzkLG1WUGvNXX94EwSzlPcY3PdtQRcQSAmHHs9P4reP
KvmpUqrmt3NWNZ0JLb8s3BzIzVKVtgPSiE58HlmffloMEriQHsxxhAstRorc4ShZpnT409TzU02M
/x9o8IswbD0PAmv1C+WZ4XKFziB7o0s9YE2peGGO46txo0/P5Or9rauZoYCUlt9wLOlZxQgySkxy
P1hibixldGhfpQnUq6RUtlVZ/RdLuj4fCi5gnzVNPZkjk+mXgDgZkxvi7N0KCp+wa0uQ4vqP4BW6
/9IXfd6UnY4z97S7mJHVsJp2bOOUlmxnyPE4LSYnCnkRd/supu5FD4WJ8UoomllLD4kAO4OgyC7g
hNci6Nx9yr6LeaKavzItv8Lee37XU2/mJ4F9/HediPuujfPPqEM0mtyfmp4awy5HUP9eDE6MBoxg
Bv9iLfEL07abOz5FcuFTeHwUiUlOoc0CsBe9GhHElWEYq+I67cVCel9mMIEBQV/Fea9/whR20O4s
TiEdD6oKKM7CRQut1mZDGl03qDjhDEuOGk4GmOoC9+QBwsG1i5XYJwOB6ffn5D2o9ugy/dQxaXE+
J4YONV/1u13C0y3HEOIGrya+i14IQCqLYDLVT+SmtCKD9mQT0KzzUS0IpJg9FWstAaZpsG46d8Zs
qm0fkYrFyGDB8ZgcLqJywSAkurZdsfBKWl3bCNaAgerxLMQ0aNBeSD4nvrysRV4wOXH3zfStZbG5
Sw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_3_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_3_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_3_1 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_3_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_3_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_3_1;

architecture STRUCTURE of design_3_c_shift_ram_3_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_3_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
