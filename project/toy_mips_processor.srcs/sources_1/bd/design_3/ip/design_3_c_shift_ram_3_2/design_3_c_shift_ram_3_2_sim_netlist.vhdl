-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_3_c_shift_ram_3_2 -prefix
--               design_3_c_shift_ram_3_2_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12816)
`protect data_block
j08B8T12oE5bZW30HNsH3gbOOQb5NpI+DDIkA5jv9FkHKtg7xn44jCbKe0aAELLvEqD1v8QGqgaK
ctvrgQGLPgz8ep6cX37G360xxWtll4M0l+PXqcI7vjgL80rr4Wn98SDD0gbLikPtExTI1NOCsYSn
t38ny7zlUP0N+kobBl0t1lisqZ9166pqdktUiXctekslLvS64YqhhDWtBb9p/lbDxjNai1VLLgLN
Rvmx77oG0ujze0MwXI9IRxDVJ9GuDjelkE9nP2vTwSEEpia6aEHFuuN7T1i7dHOsWV4R6SvVdkvu
OHtR7UDG+xewdMTpk10Hd6FgUwTSaA26Sas+nj0v2pyuZro/exJJvJCpZ+mCmraHL4XfT8tjm8rF
2lnaqmEesfeWOOaZillB6vvYO4Z+p98Q9kMjsqopTbll/ZAeH936o6Ovqheyy+8j6KQDGzRSQ7Gb
5myGB7SMvvqyZhI9zM/X5eHBoxXvpMSzTD9UnVFesdsX4O0uEvqSekUWfjdDNXpNxcipdaPygATD
tNZlmD1cC1ruZiAJ6n/KovB+qD3YZewGtvc9hhKgYFMabCOBSw5mO932i6QQg2RbklwopCuzX6Gj
SUIXpW9n+5C5JT0gFqAxnuVp3pFp36KcB3m1T4IC3h3u8sblae6m+TtLqHiZhyyFOAqMXYHh9P4O
iTIxAAouGp4U3m9dtfqKCblMktSTu5Mni5WgWiB/xiEC+Yeq4cOqkv8m86ylozF04FzQU36Ci8iL
Hgb9ISyqkRblkaT00VFXFGXorxkhn7ksoReDJ+3RICzCVDMaZd6sDWadEh929D60SNarAHGZ8Mrd
BeXAHbQ+vPFAAAz7FfVZzXdaUWvdi6sDhxe8EiIfHwOE2Ow+Al23eiMnrNcdmXXFhF/aDXsFBefy
OyyWsqb46krKEEi7yPHZJPMWW7e3/is2iSWjvjFFT03f5ZwRvmhp3c4QobrqKPAEahSXwRe7OFDA
mTtRxsbf6zLrkVvIV3ckeUBuX5bQzxY5WuvsdKSTPFu3B7beFzSiETa0fCWy0uFuN3t01SEaEN3t
cNH2b6/1ey+6K0YpQgzB1lHDq58yLsKSi/L3/e9QYNPCCgHyYr3BCwJ5bM3lmL62/rKWdAqDdjym
kG2QTXG+0NVWF+pLfewGLnmpAwRMUxDNkGqKnPFXnfl35i0bhhVtkdawxuJSFgthpa7kQG2nwKjH
olzPPvI6Ph741fmckSOndyZPdX3hb3SG/qzaawosZqTpaALu9mYaku2C3MKt+Qvv7RINvpsgY69s
Xaj6UIS6ZDYFyMPc1CvLOjHbGVoQCidFEC1OLyOel79S6JFrYC4emK/W18D2fbieU2+2yW6EPp1v
46rfhMa7OK8xB06kooWMvQRCE6BoRYYWgWXmXuAcFVrXMr7S3Iv3jU3zN4oDc8izGXkcmziinLvF
YJpw7aI/t6zYISfBD1v60hGuCF3uq6UBX88f2uTTQ+z7h7VtRtMWLRwJvT/VpC76OB1f2vMCNG0G
hQOLR983Zjnophni/M3mWrGRgtZOFmtkIAH/BcbgwqN5iM/Km4Q8SXFvso4R9RFr0DqtoX29Jbzl
Mp9/akLavE1mgTu8Nm5Rz2eXDm2LmZaN3JKsMdlP0+qZ13BbeHV7+Cw/4XSz07Opw7gOF6jona+0
caTsZRsGZF169uCLievHJ5kZKi3hn+8RoKKfBAG+NooigZfBh8LDt3lTUZqgDWPvtcWNwII10NOc
yeUuw8+MrEoWwBVh3L3rKK+gg70JPxr2bqbav0lI+lF3k3vV1oa3TktiPzwQMWhT2xSVYCONBGwn
jnyreNmJQcbEhEQ1ZRtiaoOrUr2sOFQLemHmrQ+DKOTZazs1qsHkpWmipXJpAoIBKrX4G8mIIozX
mTfgThA1GsvCiGDoUBqiG9mrwVDe8mc/Mmdbf3rtoAIwPg3ZEla9nS1nzKZBG2jKuw0G0TYSQ5hF
eiSveiPTVZ2f4ZlexMDKuLZwe1tfnn4Zqcah8glOvhcmS9EHERJX3gAvurOk4Lt39ecMtvV7zYs0
gNEz09qOFtntToKmqP8+g/uCWQvA7TNt3yIc+geabuclWPxoLYzEa8HaOASWQGc0+1Hfcu4GSUsu
d0y05TpBWzy9ZtNWXjgCTqLQFh+MPOt9fOSQCWxXjbSGtaHLztVUptZkNHBq8LZ6C9IFUhXkyBhN
lFe2FN0t1THdnM24Em9U1Jxrq84rSnEZttUjJA6ZNEC56VTChhHopyNsF8o54PPcvfZAxnEGU+Sq
czc67aJBjxlgel7fiv5WhzRD0cVJ8Tbn6/4lh3rXPjAfkrEobuagAmDzgy4eDDtNaASxb5yIzsxm
/bhekms5asKXBLekXZkYu+DcukgzwplJlThVJSPiBe/oyv5mXYLXcRW1OiUinP/bQhp2cfx/Yav3
bXppa0R+GHKZ8b+B7JJWFbtmmay6Z3VUmAayZOaUe+FUIE271EHpdDKDOqf1hXtZdUuv8e1QeCma
d4kxD34hEz/xpys8pAnA8TqzN0sdoqV5n3SpzYCfhlYHXOBlr72r+yTe67E+ztvOfSlaaeBLkudE
pGcmm0mjdpQzB9qEi+gdehq2qTd9Ui112QJA2JztL2c10vk21rm9k6EmrWrOllSNuGknvAJQkTnC
Se0hDyVnmeFIt6k1JtbbXPP6VrcxCjLf27JPqjhNkvjAaGa6L8JUprTIK4b9ZB2oMzpKPC03MQvW
tjG2iKgybL1fdpEBN94pGOSkS49FHVev2CKKUdO1wlvcXRqlfwJBEe9Eyqj0AVm5nVA6npShi2xe
BbPeOFIhD93Uu29pGU7UoYRnIkXCr4RvJm0nKepT1Ao4H1ylVYuNykiS130BLumR8n5xNEcAp9DB
Xag1AHlnG8kb22w0KPMwEkULg0Zxa408IK0PdaseMl/tl7cSaQb7DInN1ty2LodWdFUyIgPA+Jg8
gVqDSHNAbgOqXgMUyLrOp9tDi9JnLFF4SIwH8+HpwOambuvcxj11fnTGloeJDs1m4bGTCMq7iqba
ImumdcDF7u2fyE8p2iNwobQVjCSReQf21q86FDg10SSo4B8Wenif58lW9KY82TA91Whe5+3dqmEB
gR3SD+Eoy0UFWjXhoXuBBmHqL0vRhl59fFe50MRTnvymsxTQEMeVFoGATwFpMANiw76Nuu+vC7C5
aScbAc5bDLic6FCP44wz0j9qJoaD7JyZIAr+gee5HWTSZG6M+Zfo2bU6axbi1ytwoJ4UJb9d0cXp
I5cf/JRfbAI7ulEDKB8Z0l+n2+S4WHuLrAea4qc+EzVxg9XbhKMaJJGcjgXsGEeP2wKAE+OENSq/
MGggHtkybaabiOCRBZOGJLoDU5tOC74l8Qog85FqhTfVVhqHZiUcGUbCj5fkAjK/Q7/R+uiPNJXm
O+0sx39kT8oW0xmyzTVb4I/D13CuRHLJw0c8VyatwjAnMkc2E1Vz1B8aeDp60Af5vDgmdJyFGs/R
kRjduCa/RZ/jYsSZyvVWmFKOuwWRPdNTyuV2S4LaH2ogES1Uyr/ZQcsHQ2WDXNdK//ZDanlJ8nWe
kwae0EhpIp7atDWhN3bUBuiMsXOxskzdAi6DNHiry1jAgysUVCypU4g5YaYEipcsxeJOG712ch62
lzwevsrXzFi2djDZu+JkukwI6IDPYeyvKIGsezeWSF4dJ4hGk+FaeSp/gG0acAJ1OzUb4k61EleZ
HOUnZJWpZoBdW8tZ4G/zwSiHQqthUEMrnph1PhvcZaT5bTDhzDdww8cwWG3UYqtGAL7cZ8LAtdjk
FB73Qc09BzshMXBOQcNQVQaiMUSU7u0mMRuiBrG/k4YDOPlr75CeHLjyCYGA8V9jG1kWD9jUwsQ/
xyFOHBai08AsWAIM9S/AAX4BIn1KlpALAgytIo40b1w99CqYfDLWYxdFD0mVfrHPO+uUUiRTkX+r
zJMMcbIcPeLJx4xy9YuVRt0KB1K/OCSE1w6JbSrIu2tsBq4YKIbEnQk/NqSaKriF1jdDBJ0USZYF
NSWnbZblxP4TK/QdQy0dZnq38BVbUuFUkn+Gl+76GGm9YLI1GVOXjoc8zLOjJYrSSH4nM3MCFSoF
Zn2B7/HglfsS24jnUL0qGEI9qp1CMbqvOsFJAgDnhcpAM8hyu98gI9wwia6V4Z48IXxOoXqkM9wS
ByhTOaWiMaAq9wmWMd2gyA/9DURe3hANXv9mRoW0b36Q/i3YRVo1LYaPDl1n9y+7KluijQaVds5f
wusxRGL0CZk/775pbAgJ0udR6yG7m4q+KH9CH9P2+nToivs2tZdQYqbp11nDDvlI+cE/Lv1LKYLX
q0w0pyHtOZfmDpO28rFagsTamu9AlAcKeQTFryuG8jO1+V1PE7Lh6Oj2MxG/HZMSdY/CGDbytjuh
H1gyHkc62IO1d54SKsdJhOD9G+oa3t0VaLaCXNq4NHrZ+wvdPgHYggwjuMmgUF8YhN4LR79s2NLm
of1AHVVhM/w9jifbBa0l0hKGJXB7nyNvQ3ahryCZUeBPHQiYlOLefgUUYUb3lrYHP3lef37cEU1n
AGpQiMo2LIQTBYnUN2e2RU8XO6vmwu9i4JHOvA7bctVjallHkTlp6ikQotr7ROdXtiSLB3LjXrg7
roht8VHr78UqkEDavB9ISyoxnRiI4cv7fG/IY8+fZtlFK49PEeDZQwYlUm1k2tMlibsAFk0gmQka
x9tz4CM00Jxc1mTFs/txkJ1fVkPxPTPTyf8BQsMwMZUPqFqAajMV5L7vGf2QyJ9oBQNc3peJUT8A
upkrvslIJajOG2lD5oQ3PSHZIEdCqyE8W6EQE+xN7Mohtw9SV/NvvDY4BlO36Pkccq/vitJWv/xV
U6vUzP1Zt9sR+ma3CZsmS6Ibo75i8GTrthrN5K24Yg+JssP4IjT427BLRbUN22VPfwgcLwb6/GvY
e7H5mUaBSH1lpHgbdKXI0vh5SHq7Ogqp89OYeE3EgTVmP1//LITC+4Fb0rh8Q4ofJIwwASmT7JPN
WKP+pHwXp0A5kAAo+skLf45j+jy39inw+SU21D0IzrW1NrHOpVjgbFLyMVk7jBM6S5imNT9JGCII
A2+tUoQcOcZxJcEFO7wWcrXJTZQkKf78hl2PSmZ5AGP6C8u4Hb/3QX0/PIwLlQFNMFa6+F9+WMaA
1ZszNKw4Xdy2caGbDTyw2V8TXWmWC8uUwLwE/8DIR6/J8518hbqUC0kqoiSIl6N76heFRzgHZgnw
+QdYx9zElKjZJUTx4lZ69jsaeozR79MbLZtNKkmYyUzW8QpS1Y9LcEz28RHHh2g/t69A4FFMKhRN
Wc6VVlh2ghUR0yloC3tYhAaWSBMcdWuxsbCluaX5aYAAK/GWKPI9Obj7dVwvjLlObwS3IbN2uXsv
nbHo+8IOgVXv54yvx7mxpjZj2lRcPERmV6IrH051VEEfJAus1upCXO3hPky44yTpkxn63LGFKKUc
R1Ei36yu+60OasLYvwfN0JRUDmVcQ8HaFYXYCaOttNJ0/RqgKbaNuosY6syttXoGUjRgCOd3/I8s
UvaMaxAWVPq6/pOzflV/cSlhImglNaf5Yeu4indi9KTKqaKdzGB0Hc0jETK85o5kJ5AWss0WDf1t
+lMwDQdBOOmgPGu9vxfLkVaxj6CQZ6r876KZd23P0kH4Omgan0x0q6EPKeUKIQu/NO4U2nA1ls43
IA6U24q06M7SktLggIjVG4Bsau2135cM93MDKv56wqbEcFlSjSY+xwKrr+Gc9w+ETOtQUUqHfo3w
QTn+zdiPhGyHjzTnt/tEQzdS4BixNblohKpwO42qKz3HMKUiWm+9IHvkC4Sao97MBUh6MaoJ1tx/
owhc1fLyMMoCkq/WO1JBOpryqdFReyTD7t52kPbXocxwIHLke5AAlVp4JQNrM7nOqtOKEZeFvdWQ
GfTr/fycoEiqnHeVCI4KtADZr4XnPZn3IPKl70tMDm02Z47XUYOV0R4ptAWvOv6tsL8kwViZLXsF
AYiIbXAq0BNsMyegV4gJFg/z0U42mjIq7Fwbpa+tQhaPmKlEKj9RdvQTymeyYXDnXMvLIUGK7Rd8
gMAz76ACaGo7rhbKNERaG8xsZyybUmSL2PDdWtGeW2ePfaT1NZeo680pZvh8uczqVYcRvCOuHABQ
93h1CrPj2TEY1xJ9HzII3YB3sHQa4z/ZQrrihCZQkDFczR9P6j5v7S36yMUJ8HtUB8z3eaaawuYD
ivsbNsB/DDJQoIirpzYfaoDNxbikHHaGVu58aUryzWUczQneMo/vJJUwc+0zUWCilkzbJihPcBnS
8pFA9VuKGbGVT+YYOgGzT+RA0Za5mzHAHprlxOWZsqRIffg3WRMqMk/UhIObjOtIQMSH6x5kE/Go
9D5jmyhHbkRvrIsdLzFfSmuK+WfddOJM5I8JD6H2347b6GE+fosqZHr+YSVj1goqM96OMX2gkWtt
VYpNPCPsOAmsWNaX1t4XzCXcOqQqcqnnEmtG82Fdklr7A7ILUI+DBsPHzpUcSO7nqM1AHcG+uyNu
3jFSgQwKR18SD4topld4fZ8IV8aSa8r394KU0g6IZngSysBe5QuvW1fOeAX7DsCbUwBGpdmyd6nS
FdQfspO9Zs4mA4VXnwAkvu4YgBE/sDrOEx/jQmNbbBQVvC48Q5XWSlGcYyaJeqoZ+8f35foYMA3O
W6MbmWLv9rBa64mm8Up60pL++zHldcOpKhNkPkHW2Pxr05m/+sHYjxm89IFfXhIChqun70vGn1uW
fGCOQv8v6A9o8aWFf4U6X0ECA/9TWFH3zY09SsvpAQylYdTV1/L1K/DER7fXr1sjZ68uEiRXmRCf
CNSQCXH7U2drQ/K6l7EM0+zXg7B3bEcDtp1v5Mz28DQvVgaz6hzMrjvztbpbbSQjjzFYx1s2DHPK
5Ji+HPUJ3E7xt0D/Ca88h2lY8G2KWHFEI1Gf+Vap+c64AL0Cs41m92z1O2WrjQb4h43Pk9kLcsgt
wASVZKI0L29HnPr4/z5JDkUfQGHewIeLhngJbyFPnf2XR2Sff9GwDVc/3iZo+RSSkvhiuFgUXwP0
T1A/mw+CWD6bqm7DyqCELTM32cW74hY55Ul4Eb5gV5bNlMQhDBkbwxkGurThFVncxiVGr0zvgMG1
OWeA6HsHqZ+StsXYOuaXEUiSfEkp7fmaz/ulfzx498pGbGQWL4VpbH7y7v4xsdRp9uw5NJfHYPMm
UNN4PfEyFB/Yb1TZayDAFxtCydcgER3sytuYyzlP1qyZGGpd10K2sYlLxPK7scYi8xlZ6MJUNn3v
0ffBwED1dl+3aI6wtqwpXWo74qtqIyVJafwuNxdvtPhVEv8qJJi/2VcBEXPQbIll2rfsBG6grNBe
eMYk+EgWb9reyDgfbQ2DrGhmaAfvaBRX/JUa56pFAzzYJtaBVGYrCTLUM/OYBdJvZkDpd8orSIsa
bOgPsVdFyn/qEktUB2onuTmykPIcdDbhSH+0/46Dsnl7PXi1CBOXidwjbNG+aUZJMqId/Qk3y0R6
tofW4KRVa7pvNrI3v6KmtQeoZENHaOfgv/Doa9fpZubpS1/WiFkZvmGixMR2bbkk4Bv512lzzxpF
8ntNwye1FUKMvLulb+N5Ew+BSV3YIAyWmZx1pWz0ESyI4c9ZRtopcapEUABTJAnWg6ePpY8ol0c+
G0iJ3SqbsFb8gNDaZiGePggyK/DuFLo9EiPoa4AZr44KxrcHqHf+kcHi4wBqZ6NGPLFqI83F/Umy
f2OCi0yKHDGpz1/uNo3n+6jU9LM+uHq8ZY19G6uKB0jQf/cvRJgNL7aUeXHIjcg9u9mzSTmbppV2
lxRvFFYrUz8wZCq0N1BnK/Gp+zVEwHY1lD55gv3xqfsHm881ihD20+syOAA9A7ZcxYEytepo07Z6
5vNgIjPVS7p3YluAAy61uX8PGb8yVrhiL+LHGGVqSNBqOgYk427OefU4xmEAOGSfEDbRNRUfMbl2
MlOFWgXxgpsgqSxY+gAjBafkiUY9ttBAaabS3CkHWIGJF6wwnXHnXt7BWsXfDzKgsUKJQIvqsX3f
dY8upwkog94VkLLmICQAd3c0GidkaJbI2bOZINYqB2Gcwjp2QtMZZrP6fbJ+IOrT5gYy9AsMAbpK
mKSGUuwJGmtNyB6D3Km2+QrKgmZVmgkxArhljShOzMkCmfXM2vyUhUSbysN1cNTraUmnpQ+bHbqa
8UVEkxJHM+SHCh8q94nUi+dQ6So+4lW8S/boDwxzX33+VyeQeJqkNXgg7YXfYchrAl3spYYRX5Ld
6w2IODPWrzqTZhC+qVW8GUosqw2etDf0v9ReRvmomXokpfZJ7MLXphyqivHFNgK6V227D+zVm/fJ
8zTc+AD6i6CXRxYdVRdakgFi+Z9gtGRD65V/pa5UMWp6qsOsIu+oUp6bawgwVwrrQRuO8wRdL0d9
US7Y3+5GJCBstnHeK6ThiUcN8D7VRd8tChC9v7IL749GqvGUQmrc6lb/1rUpWYIbJ1FIMGGsn+lB
qPC71Rrmy7VVr+c/0kTZj21mLcqOfifTo5hqZbG5T5dWpAldy/pW6SISxDgQZnsPxnKbFk1YtG/5
uDia8+H6Kf7w/6IkKkULb3bEKYSM7wr+4SpxbOGivTusD7zJO3k1peHcCezym3XFFt7WMWIJesLr
dYwbL14iiOE7ybfPtgmljNffN+HiubrDu0dbGc2WjxE8GdmrDk5jNqrnkTXsxHKil6EYrQdDn4G6
gQp3JETkmstbbI64Yyu6qfYmQswxVOXPWpgRuqIP71+eIJbcql1yPIc47ZXGzSoc1xwkNzaFcKmn
e9zUmllT+5xZzbxi5h5eskTJLOhux95r+piYZEuAvKaxmAxGx6UKC8w4X8aJmzOO8SnQn1JX/uFx
toy7TXETDOpWYopB5VcGt+G5qURptw2x1Slyy32A2tjclCki7O7PPLxuSfPmcQTdgMt3lm5PdsSp
54cMuMI1BMCe0+9WYyZVeVV337A2I2GGzvizNMMtD4tvVp1+0GNZw7jo4itPmpFY5jfzFs4dqTpm
2qFog3ayGvcWefGrkP8PFDwihrmYuQbGrgUX0C7sRXnshLbhLTHNNeJTYxK0eSKIRY/Fz42qVll0
+ALmh/+xoY6i16yqw2Ba+Qv0Dq2e4i0OhSWp+BbdAYLwgSeQf+muOhwaweAk493zv12zxHPzSbhk
P/RlQAVr2Y+KYhkcOQ4TJ96SWJZ10V7hNy0vSmWougLyOfC1+GBmo0Og0rw+yhAHTQtRwKI2uQ2w
5uAZbD9zxQGgTMfGH/gFlqAvpoTs2yPUZM4e040KegYYDtyo305HvMf+nXhHxKWpQRbMH0oZ2Nx9
BOsovB2UUITJpzMmFGQ4aJ4qgTF7lZocjTk5J7MbwseP8IYpM/MnTXAlTNs95y1UaACxepZAooqV
/bqYiD6eSklxJL+ZDS4uaEfLSUZ/dPa0GKIGR++o5ptg7sJBxQSVpqsoWeeq3aScHhme+5nsKTda
OscdRgB7eaC3QNvNN1/WHStVqDQ4Suu9IdhZYyLx2Y471mF11lq5MSTCrXzupPPqEKEbs0cVGZhS
5D7yaeL/AfT0yVf/bcpOsPxcAiBobqGvJ2oB2Z2vRVR6t6t2Gb4rxQPum8HbSAej0xAQh4Au9jG3
/n2eMHSI8F2mR/dx6bX5+OHIFdoMPpWBKcVmQHh8KFaPbSgXzL7sg9h/XFGUoZ3ZmjskNNT0RsMd
eKi6ymQmhZgRKFW43kKgtWUEgDCvq5TRWu9F5bTu+ejl7eZwiAG4wEOwOC96iUiTemYpVTXTAeGk
fS6uEOmiRqfruIjXWbBUKconE4xvKR+jdVeQeJlhniiPvlz0X7H5Q/RP6YVYvatDIa4XljgHPvfu
Bc2rGob8gH6S03RRcwdBycQUWDGRy2u0bxUKBMz7m43LBNLM8XFiny2OmgctJFNZ7A5M087JyTIH
3XRfdfomez10Y6LHUVgufvaqsa5o7T6yJwH3bOrgJpFu1zcKy0YEK2Q3VOb4PgzyA7sreUX1IubE
oeHWbWZcT0U1GXuEhMNxZxEc0wvyOZ8W1PIRtLKuLWQf23xX1M2vBIahXyGFQ3fge1H38lXThcVN
v2Jpxn9XIL5Rh883ex8hGcPzdBN6cizpc3OkJzZhWVvjFovtHQtxDkqhI0T2oV9q66e/Zx2lFJNt
UJ/wZXL8omjHkTiswHVH7vBVkxYctYzL0axrQ8uXLz/FEuc9m5kjNHDTFvRM0liBgn18ioYHmloy
92xwPyMaz5EiqezvhmU7UgSInXtkhco7wmV+dc61Oub2yv+ePBIKwmJtDGV5y9G8B3MOzHlkofmU
xPhqbWyIXTuONXdfpOQxdjznQxl6uvZMylLishqytn8MFpawwECu1DbEXp2jhViY8NidoHK9p7FR
QiOL5vtXBlxwLqmGu92i5podWvFRZXHHy/QUaAnbhYlQ4LeIjf4h3SFhQUdlYGrkQfc39OFEoOh0
DGByPMwznMZ/IO1e6STTKz97kSwsITnoQziCCx6gjefpKGLPiMbQbf1li9lGvV6bdCzbyHG3A1Tz
Fuf8awdh1ACxbHQmEXj2FO0TQ7g7Bg9xoTwPT/xLHBfJmgZzxJ2qt0IC3oym6lhIQTJCVVdmvlpq
h1gRKJv/a9TYa4buPc9EvRdoSqS5IE/pYEs3LLPwlHT5v3R1PtwInh9sVNUm9XZfTHZfdG9oGSFH
pGXHIYZ77IkwpZLxjfTqcnJt2/5XUz5djzX3dggFjk0S5/1eltltbJu2MHKO9hL0Wf5EwLRZR34m
TRBF/SpCFYe6SFH9t8dNV3KlzhppTRQTxXphiVAfTzJy6+RUTtHH10ZCVdrq7iWWZ+1lGXoyPJGv
YCXhyUxDjD0jFa/DbNGqjJe6v+98vS4Tkl1zb2UJX/bVpkChVdzNh5pKZpAC6/bkBIS3ctC/Jr91
jDEDCKotHm/+nFbNJagxDgv6yn4XtefvikK/AFlfduVl92WS6PIsgzRlc1wbZo/+t5hLjRBpgxr9
f6kDLHaebj/0/bycRwsXgPuEDcFPv+92kWzzPLE4tmq7dSDJH3SiBeZvk82DW0CKuFedqeb9QzoW
9kmLSzYaPOXxbT4kOlzi2dr9ABK+4UngDbV9bbH6/3na1rFSY+lcAvUZAfxpfk/Z3CXePXSgUs97
AbMZ7ebl0r/KuCZ/6P+/jMr3iEwJ96/fmERPRX3x/cVF0cfoZ8hZzqgLsRMZIsSw3VY43kEuLM5a
h16CWeK0lr2UXf1CW0UZIr2QNSrPjZyMZ6xi93sjyjwaARCHcAFumg4gTiiToEX4sniGdrPVAjiu
mBrx6WClAIHgV9EZ/DAEmxy5hPMuZrCLTIzd/ViDN9hWbGOiLMUsS3v51PvmP96ZPlMgFAVbebki
kFvPIfIM1JwIWZlTqkTphvet2FMI9hjA2nJ9VYLDANSXHV6nNVLdPxsxnv5kjdqsWHnmo6bPbwak
6Q/ktLQjzb59wxe31zTxn0D2xd8YsREpmESdQJMTnijmi2QuVFNTauS5/5Y3JNmQMWapkZchPJCr
7CwJJqTF4a5jCNA55w55/JUoV8SDv+p4v9P2dijr3rttk/Oj1GhbA/OAZZMTunYwPIiU99fBwnCC
TH0pHFQWiYrMFo5Ec/40FgP9ezO0BFZaq9C/Jb6gs87dhHuUErZ3ELKMBoO2XZvzyOIQbwj2jLLl
OPEwmfdS5/llA/8or5W2lcMsM7/xx4Y3z/pKSMOX7dO8PFovIkeGbVQM9R9S/KcQbU8pupcquwae
oKvMyJ9m1SCDV/mAemCtG26cWFB9M8FZ0Q5idROKb2LARWuZhSns4teWwFc9MZw10obfUj7xZiAt
1eP5adN6VUiTVjzy6xTsE0EdJOcdO9JXWfAbuN4Tt7KIJ7ZbU61bT2GQib/uMO/HCFG2iPjB0D8T
Cdop49TBBlmqvNkeOSd0ccF3pN7/94ZDds/xtM/5d3iD7l1+7GGYK+sMX7z3oOhiTx10D3Gi8LTT
mYK/pkZUqTfNJ/kZ+mCxm+Oi7MHZUlb/SELPE9mq/YVV298QRoXSXrKHbl+IgEmqUElmCwJ8+q3k
oUrNT0PqkwVMGAPRtVc9Gy+PZSoo7rUxs6GtVpkgvNbh9HF7XSE/XXAew/2rhBaxMdwe9yG2E2HQ
/o1G+ZEV6Jiws95xOqM0roeA72rsu6vHEjo2IHwmFQVv3exmHSxChxpD8mlCTiU+P51td/rj/mX2
Pl8sXR39kxTjVgL1qj3fWCxBS8Q4cWYSlsRXdWU4spkTvsjaRJ5t/g8v+d3HSzFkDIs6/btLrHSt
Zl/ao2Ocuiz4vcLWLYTSLhWDjTeKnVIn/sEcBDcyd/MGWHZPDti53wlZ5lcD/C5XkB6/Zh/b+YQl
UifXlSm3139EPydzH5/yF9nRAaDHaGzCREtFtXv+tK5m+AR95xclJHdOp0THJ7pvctY4934gs886
wj6VtqVMciGErWkBMx3Qb1B2rAf/ZW5zRMn1kkEbNSr37QT33FjcJAfRiazWGxnzG3WObwqxPa0A
nrnRwnQOOj2lmgfpUO9dP14Ze9FYCb4ocL1BUFpFd++76iIVB2aP2uf7HKW/xAV9y0YEhYTU4EU5
hSpMhUJ6B0vMY/pUN3VpKiR6pWQKZDV6z+h7iHIbyC0y9VJefnxDHrtHtgHZm0TS28fan8+OySGT
c079o4bEztTeqI0YDyHIhO6EwsQqs7xiJRvN3fH7NzYXYBeg+85TuAWA/P/b7LHChZSMwjReYO9C
pMDmPASeWmV2U4Z/gqGm5QTBVjf+iM06MX63yn+luckipJazlfSyBkS6XUFBBRqAMynr9Jhg93DZ
wwKCKw0XA+GD62c0udBa1V4gjcF7rYarAVJkjwGUd1VD4FYqHM7P5MXgmy2fNXPZY63Z1MPMwd93
3JOzXJsHPMOkwEBmU5/i4DggFaj47LlhBWJCjE3IYSOs/K15o+lEt6pwbjC9eyNL49L1hrVjCmtJ
ME9YYUcnHOS7Fl0U2Cbra/+yUAAY03a3yUMLpjpBN7moqPq1IG1rxutW6sOoGjEeIDVpN6zW5d66
bO4lW5qzGrLexc69gyp5lvoS6fYnfYtXMxw9MDao+hubenM9jhjSL+qAX/x0NbGZ1BKQ0rGnXNDi
EbGuQKB4VEa2amNRJLApOaNnhKwyeOc16NvWSzrk0stCaeKoTUfjumPuX63IVwocAqrg5oZiTpvb
PJnWEX1+6YYvIDIHxx/PbHAnhgGaRTd6Z5CEiCOrQXGFpDBnJcKr53FePxnMxTrBU6c8A6fqBkaB
+KYdkMzdWF+4bC9zIQ30fT5PYRiUwmFQgcAsRX3ZP5xPvkndTjG6N1AjSDtkX80jUCUX36XLpTPy
29XhgfThHCCbaPNXmT12UWhM+OqocwTF78Xw9ZAOk86KwSukT4dT8o4SGukSnpcPP2MAtoOj15gC
X9Ixvhs1SW/KC1ZPLDz6G2IMz53PEX+M39akLU3l5CXtL5/1T1lnjScuepeeL3kveCwN9Zm31dNp
+IQpb67nnHlUoEGd5lbqceSvVXiKyCy5QnuGfg8uxCqcwXHYpWDof41PD/rnslMao+TbclpBgIiE
rwv5L8gEManB1UFgnlzZrzXDxaXHBBeXaFv6vLeylsGp3mr1w9n5MRPXjpZ/H/EY8qQXAzLSsxLX
UF0cnuIFYdbs7I0Kn9SWqVnzhV8nu/MQU2Mp67/f+bIbcvRlrggiDJU4Wk/BVCeSK45CKNQAqAiK
n2OlHMekDry4wOqDqv/l/BsOnrh6eE/+MLdp0Td010qQjeOvHpRADfiKpsADTUTzkCSaY22v5A4Q
l0e4F7TMDBjHGUdlFKkyhhuikE7ALvLo4x8Bq8mf9spUwFJNELa5Keg2fRmp8mYu9D1WtFD+yfrN
cknMRKk3cAV/gCwH4x5e6w99uMSBazPLccwFsDnSaQdEGnnJqOe4llOn8JrhblSC/WaJoyQaRohK
yTV20dCdzKcn7a8WeqTmqM2C45II+xlBpH+kloG6q/QkvmxXNwvKf6q+yotSVPLkIqRMpFHsGYBL
O0IrjbH78a44Na7nEI1/U4F9u7g1fmExEIMHIZXqvPiOnDdlNJP8PrbEoNOAk6akCN3I5+uwXFNT
f20v6sm12OXbGnlLPsxKKreaiGOSZ2Q62/vcLNfh/Fn1x6uRgUvzQx04xrGzrT4PdEhgifScSCIE
jjfOW4jkFJkEJ25AnmouWS0ZpWztiUWg8HQm0R+d3vkFBgh4rzCahYLPc59Ml3eYVP680cv3b00j
HFFrDhhOy5mdylHhY5BC7MYbFU8ryjD8mmLlH4KGxhTbHkIs0J1Ikml3WbwdbaQasxTadK3Ak9EB
m3FGUlPGHo1X6B9p6ytYXCtRb+dpKUUHH8kPcAfU/sCXDqvDi0Xi8j5Bl98b10FXnY1rZDqhugNE
WlzMQyKfzeQEZv8wI1OR1LiTeLiuLgb9tyOx2V6b/6GP1zs8g1lpUYOevW+jzdFlsQ4QSqrQaXiX
PLpdF89HKIyaW5wGpHBnQs84PkfL/IIIMFYo2TiW3xrTCAkhs71C+99TlQNKz7PR6nG8YoB9kgpw
xuC3ohG/w7bOTxbwcT3Mh35P7u07SIeSzwRq23siNU4BJine7KNvpBWVJoBBy9urKdCfkEajF4h8
yQpHMnklRJ+CL58GXBm/xN3TmsG5OZMzXyvHu3225Hru78WfyEKuWxLRM0qwEOn4q8s1QK/Eh0hc
W0WpNYBJuS615mI8Ia5sLe6D3qQqzvkz1Ri7pRVr6klJxMaIFgKZ7WYkby2DkDiATglImlZ5YrXO
TjkWQ06GbMJlBtNgTgIZ33ZimsXxRSuUFuLQIJRw62bbNRPmpV74I/y9Sg02JyR8RXMtsTPsVM8b
2D8IBx7wV5yCLsiqrhvzcYK7CCE6fRNq1LtJS6vY3ou8czCK+tebSXQUyqyOgfFxnx4mEdOjB9Lu
SGe/Nu/Cg0hIEKSTy1f8Ib2k1jU8yvqrEmxqafdbspt7ubhOxry6G8xEt8IiL/QIBP716RYzsGOW
8oe+APkq+XaVa1BgEd5ut8kLpPZRUMIDrM583nEE4OnCho9d5HsIzcN1vMHiMlpPh4JaqStYaJ7C
6dHIrO6tS3Cao2joFgBXBMwOri1g0riLARXEdmkJpXvy+mmfXO9mfoN4H5usUpauQcv+GY0rBAK+
h1pRv645rDQnZz5H2Z6dkG1iAZGvJtScuwVqAnPguvXi4NPlSNyKGwLLuJR2QYWRRu2MZb/bzNiy
oY2qZFkhvdnVoPQhJ/ATwzVlqjC1AYrvh6b658uIzrpWfIXk0psJPAykJaelbU0p61ZV6UkUQKgO
gCxxYnhcAZYERanaRjnvDnVF7dLPLuZi1OK9svNxKPMRIfwTG/viFIe4ofMjw/LeXe/CX/6QjEYT
94+ovrt4X0/izxfiBRmB7/B+ozU0LCuJOlYJfPJD2ITAJbQFNyatt4jw9wHqoNzbVZefGov2bvTu
xDfMlWy4s+LCk7GJnZx8J9dk3ianTiG3q41UKCSBYNNIMqj+0dtZMBbMz4Dt7TTU9vGs5BFc9hAc
BNUHYLXYtU/2Kd/veV3Dp19twqircAcsqrlQp0ToM++AR794B1nIAGWH/5u0nMFouimCjOXUoBRC
j7BAD6VgJU+cWGNyT00D520zQpyrKhmOcQyEXVY+lPFMQVW6jbSYWlc5XYBtwquGlDgbyWGgMugx
ZXCLwsyA3qGcTrBSjbrKTKs5/51bRImZYXISmnAbHyD6Rd9yWQm4nRdKaGUKs0RDmO0mJB+VLxob
ybYRX/CXRVZ9Ys2zAe/KJj+53l73/8P/yNl4nLHrbMZIsqzqTtqTHLlEckt5l6vZwhbGbnvbOURB
qWUoy1HQp1doNJKNmfhi12HNXGBqRriPRO5zbZRDaVhmXgeOx9Mh9oB3jxcH5HTWSnMV8CINdWCa
Kd7JdKpVJViCy9IxivG9eDdTHVlKBDN497/SY6FFvJUSo/aYnzwHIKSG3Ee+DLuh/6PGrYoLL1Xo
09db4tt62LYBXwk5LCsaGSUshg8OxrxJd0pTz3JyogooBkfz4y2YwtpKLZheXRiBhlsRbcypngL3
tIvWqSV497TqVojwOqJbJkZJzQ2rlm2yCqm1EEMN5DCswyBklxhEZ8iHUpi29HUq2u0cILdgUxE6
45TEccidSCh0A4A74QJ1dPYkxI4Uu8w6vjKYzl7UxoiJFSOrRNNhx9sg5dFl8dXUKFaluT6Hp3OL
dcLTq6u7yOW5CFzYqygTJHHOllfXHrQqQftMNnxjte8vWxyw0Phq8pXFc1d0eO6HB81ownY1P+QG
ylmNuG29IFivaYCJof+m0akUZ2z3DZ/KfVqiDthbuy2OTD+fzaAYvu7kLTlsWUQoCR+vk/tmJlx/
l8hrddvmXBt7NQZ+fHj/so474MnRSrenK8WqSbjklwnVcGsL+9bCnf6nVJzwtt8UbUFQKrSDGjgo
CanYZfESXwjgxTS//mdbKsXffsDpYeY9NQpDAinRegcP3tWkCTu1bDddhc0UwwQ/htTOtOohUM7I
dqC67kDS9PHlL2jdiETBfnkZh4d6NnMAbgCxOCiJ1sQHZPLOSmWRsJvRpRe9C+mGVir1AVJTZRWl
1Y9hA9ysZyGQ9yP7RkSjbxt5DZ6MHaj5UOv7l9zHS5kTNfXJOYl4O1WZzvJTTeZWT2bk/S6kRPXJ
RlmMcsEU5wLO0AtYa7DGIy0XwM7FkDAe1lempmq0yGs6b67kbAr7TM7en+yJ+UlxnpqrnmUxNyj9
gAjTEeoUZ7fWpeWRAMaD/6sPK95lF2xTaDCiTH5ZaSgx5dysvHwAwyi/PE2nL5rtS4TcSz29bghG
nVYUZnJOK82VL6444OaCsxEgs+U7e3PTBej0FxqvFDFOcUUWmW4Por01gQaL26NnxlikM1HoVZl9
UUNVyTY6x1s8YuSBkXmcWVkSWvN65Wu+S08nM/etqjhBLk2xqTURykz6WQAAqfCOZKutG0rlynEL
1Dze7Wb58j2JzSnCTKONYQdRnDo4yqssG16wDTyPzglU/Mz44NWR3GgI0J4+Bh54
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_3_2 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_3_2 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_3_2 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_3_2 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_3_2 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_3_2;

architecture STRUCTURE of design_3_c_shift_ram_3_2 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_3_c_shift_ram_3_2_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
