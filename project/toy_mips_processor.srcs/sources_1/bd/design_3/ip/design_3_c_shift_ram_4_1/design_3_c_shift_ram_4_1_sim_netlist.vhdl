-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:03:52 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_c_shift_ram_4_1/design_3_c_shift_ram_4_1_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_4_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
LT2F13ZgqoaYaxxW/La7wXXNQm87otcLvNJ+tc6ShlN9ktpRN/Ua4sgkHols712jR/alzuGNUlKc
UkCHjw2m/7tRlvpVcG4+Lg4LRh1I/3LI9H1Ri8ig7PyxH4pzryxCXVEyxhpQryzJPzjH7fgsciSG
06lkXg72+m836+RxIZfLJQH7tbHFrw6IR4MdDVXMqdE1Chm6c13ENbPL5IeiWh3r1qymjlezh6VJ
+FaBaBieVHqvMCRYdFhAWhNJ+43VawNCwU/C1O8Dfhl9W6PO7DBVtr8ncVeOubuSie3J6AgIHElm
UEB3Xp5mpN0QmAhVFw96oIwjt+r5ydfotjnCwA==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
0rgRkISv0RJ6NZPSkOVZVsyJhq3g2VURdBIy6Iy64Vv8az7cDsIKuW97bC5S11arqmNiEY+G3SVW
rNqoGQoUBadeWDBH+IHTttVcSkVACbLz/Gz20cEHy7Y/hZss5ecEP2MEsasLSLlWnVOaaTnatLnB
5A/WroDnqepSpG6abwSubBubww5J0i7GGMnS3Ct2J0sx7HU8SEp9nEVfjiFRgHiF60NyRScX1OZn
6ZHqU6vvFnICgH4RSh+G6cp1Ce8R7upu9Be2XI8T5ABbLorrhuIcnKg0TCSM8enQ9hqRoeub69dK
Z4+t7sib9J5mN4RT5v1EpQ0g5UYSt6v7SrPwrw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 15008)
`protect data_block
IY+L8cOcl2a9zDjBWzX2Vru9EIYnPAfNOTvO31JWE6tZWp5oKG7cE11OLFq8ogav/6TteuOgjpsy
wYRydMU1jpund/V8b17xBMlt7/8vgQ3BcVtCzU+Cl+WZiKzeOxDH1uq2S2Q4mieD9zPcpumC2hXh
kgz7kQ5T2mBV2TDcgtm5ZKbprJaM66sN1yrB82adRSYsffsX0nAnwI6bvGA1vy/Ixy0lIvHASP1t
JNpYtah411J03BsZyVd4XAJ4HmjQ9zSHbIk8Am1UCzRiceA3czg8rVWGDJIxnEhlyJIzzWq3nazl
JAmYFuvxmOZGBXKS5khhRg+3c60Qae9FrXPunuiH8kKXK1PgYhYLkcRySgmuTgAEYSKLII/413O/
BD8tcL3FqnHG9k1SYC6k6EMfwok6AHh9kRe8Obome+bepjBeXHmB/qKKcs7WSvahtc6VdOl6ZIa/
cT5l44SF5PkGy5xTL23PZrv2jkrdC4GndDqE3QBH6Cb/m0N3dsR/PHXCEXs3CHyHihz4jUu74m2n
o6iO0MKuvMfSi29e+nPHswjc/by/faEVBe3P3LgSPJOPj/hgvKURluG7syXBrVIGcIQpqnqYgLiq
NC73Lh5r0JbA90xLmPhKSvRGciLthyywhmOJ718LqwJorFNkPOM5Yaei70xGHJ6G5kplqmoFDoql
PuiL0qsY1l4JFC+11URX9xMlze390lN8y1gNF1+le7CDUV6GnkbhITFPvrLeoHqyYn/LHxYG6UdX
2tsKpIbPt7JGJqgb7LglcLprCD+MswhFyX8ZvhvVQyJvW/UOmCrnITHhiz/e3LM3UKP6WdSfpbH6
o71xU1DKeej2Lcgz3pAbe6rRrKdyjbN9Y434UyVw/2DoqntVIEOfXOsCTl8uPbaYLa6kDf5yYLPA
dtuKYWZMD0VEXXd8WS69oZdjs8raKsZQUzp0MzKuRA7WG1XHyN6uWWgP22KgR6LykPLeEvqB2LiI
bI/5dAYofkGdjYiChqIQaAj27I8ItYvu+e+ivq+2EGc7JAckX9HhqzmHfBgr9ae8cs/APsINlMeg
G2tdeCzQOkAAubZxNbznaCHDwB5M5g5Ts+qOMAl6SkPL4jPgU4urOc/6UNXle+YABjxnQMsmsOO6
RhyMwrkw9fErIekUgDLDBlP4MYFZsgyDbTDW7C4a2vWW8weK45Qgmarnk8Y7QQG0cUylloy524d5
WVT+jQ9zA77iSHWaKg6mdIRXza21xgSyh/Lcg6o+9hqJA56lQ7LmxOIQKSGXMOZDfoA3ReSjfS7a
YARTz2mftLLZxgQK+1eytLqVQCtBwUabJ9KPOxc3hSWEprlvV9ws3os0kpkBOaABB+008tlb0OgK
EJ6Est4M/X8N/hrPJ7hRCiaMtdm6NNOEKNC9tcXU8MVtNzMeG6LyWN1ANngKAScBMikaYx/eceVB
+p7Zy4ehdI4tRXNBM++gjBhKqAeDUG18QVCZO1WQ5s7MUzfIpBbLR0ZmlADTVF21ywt0l1QVuj4C
HsRu2X0vWMuOFUNigOgPGmYeRxiosTYOpKhQxLNlPyoSV9n3c8bis1W2q6ouwOIMmGWiom4M7FKM
JiZAacxcWHvTStV1TSZvQR0u8o1VDLr8KoFn3sx7anOOuW2P/0yo88SC5G5oEWZSLlm9aNYhul59
xAyMeDf5cHZbBC4CCok2Mdv/80eRpWe9lglPAqDCr4kfHt0oUBG3K/D2y9CQ7SBgt0bMPmuQx7SZ
NCmJ1ssv//TaVeE6AQSFArJLkP/c6JxaXz4erQUeOLvNVqGjn27lg9YPbkNfLQPbpomGcL7aAJmQ
m97jwwg+uSWANGp7FAbl5ykFvq7DuVI6/sBohKX80n7BquXZkeSeNDeSh7RT5FR6KUaBuJ/t9J3/
lVuLPaCw0ggiK0tZT8AIZ9pYxtLEMFFocXJyZe8WVpgjiL41qFS2C5/xxhtC6xsU+LtHtw3bsjhB
OTlA2/92jwWiDrJPZxDakN+Z2ff/Zz5Ps0QDAaGGq1Vpkc+va6wIDS/QLjVGlqaFCH9jZmaolcUA
F+J+8h+ahgXxTQb/C5IYqVRcbSNxrWTshTfWRMhL01aT/clAWbP/LRuBy8q6L7Exfm9LIWOsrJ/L
QUMkMN2ZwB/aVVWuIGHAGQS6o8EfGixzb2tqviq3SpxjJSC8iFgnuL1SedpnDJ2Ga7Lu6wgpTaAM
5ShEmxK40NAiOPTZ3pNYd4leAYhjyXfUOnq7XmUVs6nLKlRxibDm0QSZG9ql9QSyib3mmyhyRqKC
qn9R3De9Cu6QkOGdH6XArPV1xS2gtFpx6DUrBUPCBtxlSzPL7hQoPpA0dxEH34nQiSd8OgOivzm9
S93RWtoST7oykI+o27KFIleZTDgu+SkyCY1y1Nhi6cGQ97l2xdJBLsTmVXvON5z3qdQ752bfTGZp
DD2iKO0g+uIL+98Gr6/eNq80P7N5boD7Z8+TCdeYZ1GZmP2uI3c0VBHuhvybb+uZrPpZ2kVtplBI
NQKAZ/0fJ291SXcd8ebQVk1HqEwOvmsmimbTAtJu6r0/gU62Lgtgug2cVmr8FHcQfeCFE2PBvEEy
jEP+pZ8/7BtMfS4EcymJ2HmemdhgHgZchiyMwxTJgh4jFTMxK0ZT5N9EQZMtzxiV2lEMnrggKj7a
88Yalls9nxgbhay5B9F4sk8oKkNIo/G73aPptW7pLGjKI8eEnJbMMR2if3C1s1qJShVOFeL8FAgw
Cooqso/Ox09Nynos7FYxTdjWowgaxoXu6bS7GyHsTHE2a3X1udBrm2tIJPBYxornuu1nhQcf2as/
YYZr1UCuieP2rGRF2VQkWM4VXb7xGq4e7Wrl3K+fsEPk77SDkDsl0w8fLH2OeJKuXED5gvS/eOvi
MGSiNB8cjDvyDQrZXqqFpeuLm0jtciWrzdM9PQ/AvGZT0A5l/E2SBajdWVwN1/8oIppoaonOdX/i
quMJYXh89AwxhDu1wUFSECgyexoaEqmnC0p4BDIls/GIguqvhzmQQptZMp1b42FVVz6rdbkbjKFj
m7UBFcR5NxbjwsSMk70MO+4zJs3isB1cQYGRvWSlVnbgkoIwngpdgOo/IfN4jRfqwN9jhpEPBiJv
BKz+xsvzER3cL8g9IF8xv5VUeZ7/ZQUv1Lrw/i+TuFi64qprrbVBJkPjO1uX4GfQ26I5ET7yPYCd
Bx/rDxXYzYW58LzA5YciAL1WUEC9AYAfQSYYzuFV0muo41CFCoQ3zeB9q01/AyTOPhgYxE16PPyc
GYriyZ0CryEynGlSS+M8TP5ar0B9STDKoSt1poNzPNXnZ/JDCO931oKSEe6b6EfqToIb5ZcIqMD0
8onOEg2lBkbfrs/vRHfPN3Su4ySCYDx/Lx4Dn20avbsA42q9c5Q9Wmf+eObzXa2iWeZwm8igFSXo
NDzKWBnVu3jQCKNZV0JfWsgmG67p6oqPbLs6oJ2UrunLNKqfL41pGNka8cIDbptYRgq2QphbhFmt
jfNp/ZuhvrFCvhPUuBLemN57Smf9E0MCunDHL3jkvWHGltwwnhWvbGMhWaTFx9Qp83kxr3uAIncC
rwChjAkDzHP4WlMLVmYhfAv683zr90PQ6Ewbx3elUH3i7/C7CqLQf9uxcZ8PwNl3qjN24te5Twjn
1frAoILiQVT8z+rTP+jHBsc1jjstdI/dko0dl6unDdnKaJ7ddQryQcIHOBCD7mSaFfXelfEa4yIe
F+OImi9verAf0R8LDXmv1MEvPMpw+dRC3HQ6QO/pSb7hfP8uKsjovF9bwfq2dxhRzD7MWaWWltE3
OYQuTuETL2ScnJfCN5otXFhsm8NfHw4XYY7Bm23QZPtRB6CS2xYgnVjbCmJMrGmeotfKEkYZWgS2
fWCmj7fRM2cyp/Kmv9LD5CXXm7OEgyAGQwkdx29m0rCWfC7dvonzbJxeIjFptcrFipn4Y1uLKVY/
6Nyh4nNpJsCc9foqaPjzRBbzSs0IAZ2bb5l5JU1kWOiL5uzYaW83P9CugKx84qqw5TYZW78+EHCe
dl4L1AWvf3MMZM+NieONGZeOagrfKuGdYl8K12eN7TIFvCa4H59BtCkXBdEhqhmX1ESHQpeIlvGf
PhK3JZ6Y+0HsIUHd8ktOUAWWO6bshFByjtjUfdbztHeKB9eJE1jotXZSZiPLPvT+3aJqxAcgaa1H
pX1Oun/6kuiLaul+Pb96aZvtchIcj5vTFM2rUEVM93nkNQS2szyuCw53P9ep0m9+utqCvDJvirZv
eOx+0ZKbvNukRgJeW+TECBlLZEJyy0lWeU4gd9X+luLkpPI0vOxNBsI7SPUNFyjz5WOpqoo+FQoN
1sBwzMsO1eEBGffJCiajJDXUD9NhyWojQc/pPdks2Pg7E6Qn3c3Z8xY0q3M7bEdilJhQ8ZDhKNAv
qjRhvqbEfHi88ZyYDdsD/Z/iRy5O7SlQ6swWm+aFdDx8GL3nu8ONFiBUVUBKVSxoK1pnJ++s9Mf7
dD36mN6nLwWFQsWlobF2DnAep3KFEtPnvkemDr4jqHHSBzfYkBg253UA8TPM8YyCNuquYMZHksyF
F2Y8KyN1vnACCoOdlw63M7sO/nut1ADGizwnuaX4Kmm2Pvz2rb5F/RaJldFwVZ2VKHo779MjEjO3
PHeiUPP8gRNOsG2W7DpZGp5XFBCS6qYqTN+XkmdgXPP6vdIAP8DmICpwAwJ4REZtwrCt7N310rRt
3pEtukLCOmipwbeQV+JbJ/A9O8EM5D28gTbeUCxyD5G01ryeUhGbd9kdl0trFmO6kQQ48euDZFGz
fP8bcmuHAeqDfk/SmfmsgVS6OnCoWtmFmoAmsiOqfc1TSNWQH5w/LXJAtyL41DKNBe4zEIHFh2QW
0t9s2ao5v8t7Frh/ntjhs0LWCTqx+OgOIuKtgAzLevyBiuxmXW6zSw5ziHBcypzTMpHREAbtZqPr
By/piMF794V1p+Iq0vYM0XrLZrBau8S4hMZe3a5ucq1Q42SZTD/XXoJsmdHdqYRUdHfEBfay5vVf
wSa8+zXSA0H/EZXjOA4so6QSK4Y5eda0xgJceTqjAMytvznnRAqFmLpARSB/IAMoBo3cnv8M/FxN
XHZ8YeDOmMv/TV+HuDEZnyaEirkoaaQq6W0ePxjDPEflVmC105rYN9AGUiKFWR+u9MFPnzn057nz
LJx5G1WIkr/eFIEoygCMj/JIWs1xBphkFysPs9xpT2DGxUFIavoukzUSlN/5MN3fH3+eg2V1oyvs
JR3Tq/fe4f+ME9oYRMU63KGVnaWnqDukLjnW+U//DahPYOzVRgsXLyCmmqFIOTgi9sykEKF7/HLS
Yv7CojUg2S2bnlIJ/gxI38bBpcJ1Jl9UcaNhvaIntaFHXdoUy0X/djgWvF85AOSjz2xsrerx88rk
/MrFjmsWeMt9PTGzIouCqOB+H+R0eCkPm4bCTl7Lsoco8Dm7VK/6HhcQYMqq+Jxa/XJZyMdXoguK
PKOVLpB+55IArU4dsEkG2Ke3vnue+AAPFN2fUrI+RQ6mqmcV05WDcfsfTu/dDeJ8MHzTODeByoP+
hCwQzOdydu10A78wA/9eYHlATLbQXbsgv5EfCMP2U9LLL9rWivNEsy972AXIFmZdS1y8bjd0TiBq
/CyRGLN+pdEtNEp+gqU1DFEBLUe4BPiL2J9eylY8RpUkpkMaMnzoKnzFX4nvhrxfzF0stvtT/Yrs
+tRsOtFo6RGOelVsRJF5bI+ZIIfePl/AcCdJJV60WvsPhjQ/ypqfUVytE/FvDmn6DVv+9Q2+o2yM
ezlrCS8p3RvSqDMDUZgvxPphFqgwxQ/Lzk9NnRAHoZKNGKmpXS0bLlMLc3wf1zBkfLZysiZDfwBp
6cS7JbQKkzqsXZ0ycJYvrXaOJs2UJs7dHPPRPvMPtce7k39K3HplbPgzCiNUZv2W+eVh8oO6Aohw
mRq7H9WMdQCe1OB8z5N4e4qYEcElV4uEUhJ38zth7uZp1GR8nwSZ4JeYVwJDWzA76Tun30QGx96V
in10D2FPgmdFbdsLbUqjuRBmB6UBVqlKxbUfirQiJaRkwOYfxoppeLbpcIyeY4yc/Lpu0GxVQwZS
TUUjVKiHkMhwT0jfeV/dVw+pFPQpFFRiWAphrkgYozOa6W3qvNLSp1lmjCOaMGnkfck1yUJzeOs3
KhLmZlPU+L7k0JxrkY9Uar1wOYEgcahdFvDPm+0guz9QN1p4wD6OdF9F4SgYLHvo7igmXoxWKBW0
zXjJGszA28Ex0sLNy4krGHsbUtUncc2u4KaJHeM02o3Re6C4igswQTcgJuwiuMaJjkXyLACoN6F/
Lt4lILoUyU96USKSxkX0n8TxDa1onk1MDdNglwi9m8LsJSPZgJ9MW8Eyk3KTAm6/8twJT+S0oJi6
gm3FgBCRhbdpjzvddx0vvpUXqjnnELVIWLqsRgSptD4JMefNxmQcGN76+AM+nMKGcIBGp1QhwHI9
ZLzYVuvVZF8f5w4EpDf4VLAfkm7BA4lOZQrE9aqhU6re9++Q1BmcBkcWL57RuicpICyC6764r4CG
+YxMqpPUc/VRStwc22NIJfiVYdbTzd0OB6LuzDg4eDRv8O85yW70nSW2qyc7rF7yrhZ+smoXLtma
vJgJB1U0sTzG/HDgukuJh5veo/DNXU0LJSdBo1oZ6Pr/CA4XZEYIqJKf5/KBS2JasOTN3wr9cxJ3
5Ab5+qm/gEGmiUhDxGnfbuwz9EhtH+r0T4gAJNwSAEHAqBnA54uk9h1aC5ZqmQD06xIowXtcA7F/
vmOk7kOfvvDs3Hk4RmaV5vRsMuBc3fwh9sAxGd01/MAyEcRKYYLE+fF5ksMyYaJWbc7mNvXrqmId
Dlmgs6jr+Xf8l2nusfFA9YQHfMl47arCRxdjdUWrwZ0M0meDQgntAiwwjpifnTKfarVAOzHOCORS
EcHjj2dF7tcVeVfcuI0o9VwP8xUwgmy8HwpT4B1Rfl4c3C/Cunqiz1sqVVqZlLtH6YD+6fTE5I3d
1cAscTKbTrKX9Pcaau54sTXa+0qdTbqSCh8XVF1lYzzLpcOf0pWD5zXD4iGihccYn7JT/5bwX5qe
EWM67Ff5GTW36Camkwrws7xEThfEEnV7R74MA8R+r4qx/yL3Wxw8euDOg9Mc7JIRseLW093wBJi7
WAtNU5ptNvIzeY+QQdpYzsnpjgEiIjk8DItNtKLLdFLNFGSYJEeGTTAdDwILwhjHeklotL9+Oj4N
MEGofGgrO+GYpV/IR53ZMJ5MrCRWmUY386lWEZMmdyPkCrq13rI4sBJWNEDALs8G/sUI9MvThhhD
m1b0HYDhd7JrZ5BPODdpD2ts8ys7Wwqvd9eyDq5mgFiKZuT2BRWMurN+OISwEXr3h06isdXKV47d
FVdX5O7IT0VLE+BrBLfs3KJCMEIwaTGzOaAWb+PGTpTBADcEGy+A0ukxR2uHYWqBx8VYdAU4NyO9
nvBm0Apfkk7E0iH70Szu/0wFxjSBBg+AHzOUdDsnFwgeisUXdLx3hVDJJqrxwdcIbyq+puj2sToY
qdkWrWD1Sh/ZDbZxghEpro+bCN258hCENkzuOZPgSWwW6qG4mR4Z5RWEK8rDIvOKiDfcR0AKDWke
TriRDZRb6Vg0okYdvyJB2juUbUaj6dxPWfNPeHP6MChagts+/l5wTOgUXkFpRzMZq8EHxKtStAwu
KZOyVCHX4fL6CTZFTVXsm22sowW6HAB4P7KjrnqlJXx1xP+pzCOPMuLgB4qbSy8pwj+3rjj8DcLf
C2y7tuvFQlpKFCxUIasylvnJNW0j57eOy0I0S8DfBgEKzwsl65GjXIRKYGoNHEVHJN1HJa7OeYe6
ryJ4yRabqtHabHIIm7zWJgRC1/4CRauEATsC4iji8HksdGEDnTnIQbcG5GXSjQvzrKDkNooJcHtc
HasQMag03/I5mzHD6mWuaTpIV9boG09YCHIv3rri31HSgCQdhFglZrp6P/e27UnyHwmbyBjo/KWV
LxREyVJsGHmPv46hUP97wheUDTNvnvK8f6ZIBCfMw72OXD2HxEDNnv8uJO8HLL/fdtchBDdYf0uI
jtP31cge28Vm+NWgvwmltkWBkXysz5zVGkOsU6Y556VhPScQ5FZpezIfnwc9mhsay6gcP6w1KQwH
DoyYijwfSpPVd4qaeFTfC9NGMSora9OqPotBaiG6lHvG4I2tdm5Nu69V2AzRcjxDYjprPN5lO3V/
bzYVFBhPRgZO/nnxndLdXYLjKrmmpsEJ9MdBN1O2fT1wLz0lV3Z7yYGjgllmcXfJapxB7NzYXdnt
cedRZQPEWO2Qz2bqdtXPPSySXokndN4ORtAXorg6sKUxQw6krHtOxYbv7zIsniEuf/2uUfJvO0Fc
E9owtvnPfGraafUmRUxvpxfJpk9PGvifgJwKk12SSqNjtye7wWOjwhCUECIKfZoMiRAdHw3fBPSf
jLtKxLGN1ysvMqeM/dgK39+LaiYQTRRCnf4jBU9ZTGHlOlctHknoYGKxhk5OWtUNWNG9idoh12CE
+Kg+Oi7ePGRvL43mGIKC2G46j/eV0yvvagBLAHuagcGhS3+aR8KVLRJ1mDjf8hpHbvn5FXGcKJcK
sS5abILZ9nqzWSypS7hQUrgKtk/2DiA43da2DTvl1XuHB95Nbtp5n/gGD6HUg5dkaw+T1n56vvql
YuaLlSXmmrWSYE2x/YZHArC0QfkwqMwpniDZGdYmmTUa7pA77drbFDVxSha2BkpZNQCNX5ptLe/d
qYmjEesShBjWMWBHzdXBVXcwn1JFvRFXAg90cpYhaXgMujMxL+cfxFitBO/xLoXvbmREfY96q4MZ
mCIejkOtxyfL+GLV9K0nn+lr1s7R+E5Yxq1WhVAEeeDyhFsSYjyNLz+gvel6cQ6JHbmTbV3BB+Xv
C5JvfLesCY1T67j0FNG9zTHj2PCmyzhWcXS4hMiJ1hKPtd9L0+M4FFsvda4U9wHd16oasytNcM9t
PDFXn0+MdnUV+3yABxYU2ggU3mtZnkN1pzp8qZouI4iWpS+gSPbzKmtolvGZZ3jBuHL6VN4SeRh0
5dpIlCcZN/jX7xjlELr+SAJAPSFSH0Ubov8dQNEIO+7TdN/v5QBlAIf0DTfxjpZK7/D26k9Udy7K
ZEr59Z99rC1yoHDAXsx+9CJmV3JY84bVeFUpE/p5i/bE9FwcavJaLptHcjgxC6Q6os4eE2ZEOOwe
Mjtzl9uXEzdIlk+yKXs8xn1YDf8XCLAMNsVKPbWiP4Zo5BakwupAxh0dfy3JmWQ+bSqfaDtvVVBf
YBujlKY3whbQ1QCVa1OX+1n5P9IN6nEEuaPTLQ26jhqmBtoMYRwm5wm7edgZOP9T4JO1n52DVgJk
Zzie6OPxkDfcgKaPVoTdF/qYOg54JzLjrymT9Rhn+KI/p1IHVvJbaHQQFtx6YxUboohj1C4dXvor
Q1p6ow804yfNED5Im6bT7k6H2I4/9Eq3R6eJBEMoKXOxQUfkip7H9KCUnPtw9J7CyPYlcTKfBWlK
hNSjgcP0oq+VPPTXzx5dy7Hupak67gNVXHYdQqp+TIee1bhGgno3ySxGqT5gl0Z/KCJszG/dtiYK
6P6537rNgOt77XZX9RPeNeHAUyMCw58yhCfGNBhsydzyD2ft3r3s4f3HJSEq8P7R2HEdomMCIsPp
CYQxGU2/4UFzbjq0zZ0HHKKRngcfUez8wJwMfkDSh6mmyu3NpNciYEKmhD5dV7Gqd8ZMsC8HYJDv
G05TejyiSh5QJZACd8gW2yDZhExxosH+4YrcsRVfENOZPAxxE5DheAv1lBsFoZzeIopoTDR9LfyQ
QHAvWJklF9bv6V9gFhTzSXUL6pvBFCkfHvLJ4kGb7N8d7/aKlmIztGNkBcD3rVwvuFBjf035umtj
Di2jN3lgJO31+kzu+tFZvwel288jPnX/7tvjLF5lVxqqFxg5LWeEYNX1k8lJ4SdlEMHLTa9yxzwm
nMJP+rbeRcMcx6+wxztJTvWiizuc5kyWn6ICzkNL7bUKTgouFJf84/p4Qmd1BHDAsE1mEdrnGY99
n0jU+DOkjFQp4y3V+osF05+mAZOSahhmjbEX4SSjWNNE9OvbkawYIUtFkmd99tDg+b4/YNK30s6/
PvpCT2CcQUrO/pcewoGg+c0nrIx1EzumQCsrBTE3wOrkDZS0lMdHpmmJ9e9tVnrx8st6MSFBk8xG
CcotsVgRFW1m5IKa3BRjNYlTTLNT023NZg9pOyQMizusaATIoATOP41RL2TDw6N6e6aHbEXKP+CF
Ewf9be3se2gS7wHyIvmuOZxeVbrCgyUkRoNJoDy3O7eKobp+d5T/kdP+lMlfbhEImY4iY+89xmfL
N5xlANeEiJVZD5C2t5s5t17ugl+sK7+vWU04sNGphXFKNFyiMFAKJYkSWguu3wp4/bEwv3VHlHUq
uiCM33+INUwAHeyNw9dmgSh+Iz637wips8bjtJgk+e7aQNE3MegEsJMalsmZsrvDmPvGLwELsMYl
fh5chf1MHDL96AXnkSw4n7L13tnjJ/ph8KmNKvfNkkvnFuLRweOpGqtSKderQPglevtgRaVqWEUI
xcOrKXpxoXNmhYMaVuXslctNvJhZP/FsClu+rSaq1Pm0eJ+goxzViLDutzoOh+9yIBz3WJ51hxCI
pGmOlZTHRBksUHvdz8gvtpB9+MGmOOeqQP4fkPx0lC34jZrAkHgmYFRI+xwCzLD2mVDnHcOa1mdw
SJaMeV8p2mxOaBOExxxACqvY6xleWQE5ZQ/k6+GxYCYESwSRJsuWCSvvYFTN56EzPCwk97UW/O6t
WBqEIUvtrugBSw9mRe51/AD7IthV/I5imMxJRqLkkSZihaSj2WGzXDYJHT5PDreVvck3NfM1xKyP
r/n6w3Hl7epdK0XAKJGUueYowiCqkIohO7dP12ovzlUF/UOiJ23fbDTaGBpb/l3mNv0FAboDls2c
RfprpOlS5vTuE/Q7UrIr0ZITlBFnt8Ym98wDMtO0gS54zwlBiH+fOSoQt2nmzY223vSKLrQVYM41
SE1FoUuzN4eZWFvYZGEh3Is4o3fyzoCPoZJs7cZ8p413puX9RmwsZKuHkd4pEAsGjYVHQy7uFyCn
mYlywbD/w34BmClm8hz0lKyeQvXGhi0kBcgTydpse+BspOpigs+Ambyo6WjBKJgSphWe6rU8rcc0
bdUg9XkndHvW6axKul9B3HnKASbduG7yM/p2udarLDKjRfc8TVS9c6KiqcJjkPpjBUhjI+ln96Tb
L2KHR2txb3udJARebfVjkzg890dUpgCwo7SQEqQ7uPAhsj1ogxdGG/DuoN9LGidOmsaI9xdA9qpJ
aHnxgY6uWOu9J2xc5Zj3DqYS7M6AVDFAzNw76B1Ou+lwv+v5ieQ7Ndi+9Ed4E502wq9jUTuvIbNI
74XLgnDjmTxRy7ZLsIHcs+kTQTEzUHqtR9aj3n1PxVLGODq1TUp0YRyXtb3zonxXApjBwFhdfyqH
iIhtDJPbV0eMo9zHeTqwqbNgTzPrdJeu37hPgq1dzkH7bxtdBz9pshwjmBU3hEBQhvnN8cOUrTKz
iicMKcire7+j1p5l3llvea6PNJjjZHwRp/lGHShWEP45L4fI585OEM20jkD7zVVo3I/woDZshsZH
FR+TCyEBGKV5E1OCppxwErJEl240vusweIirLsT1/eXIVtXVOcuXSd7TehqtLUGk52iVKyOykwAC
ZsgprYcOA2wGS8fotKJ5At70480ko2rAnhIwC4podCrWGmdHedzho1w3MpXVDl45t72DqX2Wo/F5
lAbzCwd4k/mwgs2QmhxdoNOQ5vtRzOaBDTA54pZ0MHhKG+5ksJ1UcL2sGOCXbVKKYrOLsfZPtTx7
fJjVtbacmHRJbCfkhx+qPqNpm+qVCK1ppxl56xUyAPzGdcv2wpXhq1c11UyYokbNr4jwa8ZqDb79
wom+/ptqgG/8gkTAxUea6wF6stDp4CWBavuugDcAMtqARBYp03O3s+Sxlglq0ytjnY+oqWjmFxFD
gW6XzQe2taCTPZl3yT4YRQWo7zLDed8hiVl7EOfvUciysTT1R6SrYL+oxtWawU/QaZse5LSK0+ic
o/PesrljWlkl9XPoUdVtNE8B70afqTBTGtcZIvQkMkCoN/T/S3Uf6mJ1L+wz11UXIxSCZ7c6/Eg8
kYkBzVWyMapm+Sgb1D/N6N4KGyiAL6J28ejzQtXbET6+dTr7dU8YbaELJoaRoYkd42ADtaJZSjO0
aQB7XkgIZqgpLLHNVUiSBrYK6xyJcba57nPhBYnfxcDnA0yDw4ajNcGf1HX3T6u0OJQczgEZM7vI
mjhGlIWdn0HB6eBLI/vtgVg6HqweGYIkCh/HxVr0xTYK/kDYVmKhfeuvOco09jed+Vm9Kxss9F0V
a3BI6Tnfxsnl38Oy6DG2wnQziF0ywsrQ8woTIXEUUULVV/p8Y6ATeSgEcyvRfeXMhcL5bFnD0FU9
7HVhwB2HbO/z20WEuhCC5yILnmkVG1SMVaaSEU4k9hmNJpRJI4L2WU7IcCkyMD3ysqdrVBPzvfIB
18I0zVneNe8pVDdgn4dOw+uHj3TCDK1Gazbacwk9/zofQ1LS/xgnl3GClJaITGWxIyTt+hXXE8PZ
bdXngUzrhJCuY9wf+it2hi8qLb9yEbByr/DuEm6xwTZ+qeWnEhO6r19Pe91h9yIkng+7sIktwae3
bQDoAjhXeEcPW2nfIe/NoNeNnrWHDOw/j4Qw85+J7Wuw85pLSvLkGahg511mOKfUYI2le7m77KXU
M54yPctj1nqJk247S6HiaOS+G7N4mR3eBHMUJfIH4OOWJe91PdagIzUxZZUeLreNPxSlFTw3o3GZ
14AiFNZs+6VxyPS+nGVtb/+SkGYK71zYiPdn/UaLKNwUKUR8Pgb8Cfh+unysj/KQvnmRFwNNZsM1
fqrtU2y1tl6nYumMHtjdMEPI0AICIycIJk3C98zW5Xh7rEXuziTmvH4+qLC1/tl6ROz+IpKSxA4Z
71gfvrcwY3D+98OsuBVgA2EFY3rKjwdRFHopYXKVehCVK1ZzV6w46hdMntBegTkgRR1YJgBcd2Ca
KZFP6TwGnsXFGXJNW9z/XthWebPEMu/SjtJXCdQouSpmzk/GyzH7og8ZSWBUWnpH63D64xPfx3ts
rtAbCYZdRDBOC/RAwsYtqmxsZWy5unFBWpOGzK0+DGuaF4MZ1tFWLcJHSvw99GwuwErLyzjfB1i3
Xsg6UI08zoyEVp9bdRneWHY7D7YUd+BugneJ4fe/+x1ITeTguIOl+1G02HFW2hHeaXo+PPF4fatL
0U0ZuQ+8+bAx7pKd0YSQt0VppHIRlg5LRHYfM5C2WMrtG9kfM6agUb7npQGYgVc/VLFThceYuP5q
s15kBzMN+A1OwYE46FEke5+2M15Px6s27UI21pwqd3wAtyssb5f2vBaywxuDsxsu8YbeWvHE7BfG
nWhG7XxeaYAavZyDJkGuJZBgII0PSe4jfGxW62TAxRADxLyOfC5zajE/p3iMqf3Mg3U4kJlbYFwe
pzWyeAfleQZR2slKpBpxqFY2NnMBAAwYiCHGKnS9KAn6jLFWdvRXSybsFROX2pP9wOQPUaf9vaYx
uIddnq9OsPx9Rv42nq5aiJqxvo+eag+masx+SORmGTOyr4r9tYAnvCVRFVAifKitmrVf+Xlj/6jn
eT5f717CeAKVVWuya2zBEunmkPfnfZ4Yk9YoemfRLjNeiGLi1OxaD1VHu2XGWzo3t51ip+T39bFx
N2ihMUunZdmrAmXCB9HFEvtGGNgOgk0Eg1XG16hSAmuP3TtuS3YTUhkwAcGFFa1viu9smN36dz2G
8YU1UzyKQIl0txPnN2GwsuWauk2/M4UJOqDDMBq8R28/N+9pa9M6pxbyV7wbO0HnoQjlpKcgjwac
Ygz2P6AMQLtIaXYNAR4LeQi/LLCeWjEShaectjz5Dp88cFo0WIQUDaI1DVu/6XBTkjyhCzA26cQx
I4ReFn+zCnEDG0pKBqBy0VlpF0jmrwB/XWP7cHXqncNfgbIYevYdvdvYQJGOqfSMTDUx9WHe1tJV
owTRNWVbwIjYbpFBg/nAUbwQaxmBhZL3I56tNNeeEV9Aj0U9ScxlQV9ggVZ//IIL4F2YEpRm3nFM
of3Ifmeg2tIqgXYn16C2hn4MBOaHFckJK7DSdv53EhXtJYfvW/7Z0yfdXgrDihq+BH628Gw26Qh4
SqOAf9eZlHPCLu8ane5ZQvBTZGSEUyXX7+ChXg1gyfAKev61NjfaUpQsg8tiDeMdrewtP9UppdH/
LZiCv990hA9WCztMuD83RcHsclOwmd91/sA5cJxZH5bSsBY+r0Z0v2uziNZMrFiRUiq1iGoj7QNj
01hK2woO3JE2UIPA1EomgBE571/0xgqS+DKaLd/NENEaCQ1Na3Dy6HYUm831jh8G6O0/XxF2KOn3
7Tdw66bPLTxd+/F+m8CYAUCaQP029f5ARgdAtcHXlTJ1vdRSzkojxkBlxZvYb79vsvjNylDhNMY8
wfBn5USuKKAJG/eObC5pend1H/crYQtz5zJL0MUaoUA5VGdqfnFFRojscVM0DMvmEEOOV5gbaa2/
Sw4/vesmvaX4lIQhtIOCzcDp3hnjoQ+G/EQZe+pQmUL9jSUxgWLSSoWzSikpzp17H0IJZsVSJM29
ihiaBHhVvRykZgyNk7B9w1MVPKHd6q7jFH32llZ9X/wgZiAFS4wiAyN2dHYRVTbutX5u5RdlzUx0
K8gi06BZW8v775lHv7m4VIRuQpZBBsFTrYcSFmJ+XUyYoglpLJqHPwvublJw0/qSlUfElvcr6zPg
/9U+ldp2gZ1m7ranovFG2iB5sdEBS0isATSOTCoksuOBExaWNh6GLHrMx6MDx+HA5lS9G0Eilyc1
IkP0i+4pjNN1VPnsdU3OCe9vG6qkv1llg2Kuynv9xUYUwqsXzk5ybhN6zmM5qLAx8JD5ojgTGfDB
tXE/tg8X2FURUKpzlDAucJYRZY8DWaOnANTWpK2i3IDEoGvMaaK/wP6LAqpUxGFMD7xJrmeQTyjm
ZN/7joO6PqrUMWVU/3sbD7D/VNnPsEViea98KJStmJgeNRVXICL5c512Yyg6uoumJfdZMX9ojPyK
d0dNYkXQ1H+waDwKMk49V28kpP8RsHp46aLaylPyUJCSBhi10YqYzM3ZSYFoYeQC0JsxNgjZgoQH
eQM57FYTDo5huBMjOWS8a67LjcdNQ5n3TGRwPi+HmsJsjVtmg4/gwhXZkLPSJ36gboHUXFcQqv3h
aix4sRUnsmFxtBeqF+Ju6bJ3yoGOCbXXPaWUj6vSKbe8TO5GIo08JhG4Fpxmz/tke8hrKsM59DqO
tA1hIjAfLCl53OAACo+Rs/3CEsa4vu2bBM36e+g7cbOUOg7HGDRf2YR430xEF0qP7Yk517FeyQyV
Fw+UU9JHPY9WNp/YawdVtTSqPqVGAJxNpSkRHMZv3RTJVA3uSx7+tqiD70HDwNwaN5FHfYJOiQAN
sIPYL3rLZr9dIuHrv3GEMCpQc/0aVtnq5FFiLy8SiWlRN0iqxxohN/ufeLXCAEsnXYJqC+nNz2EU
ZEJtqSZtyhjm4lQgaGI0887hneOYE2GfvLFoeZnycnSnpgtBpooK5LVtQgd0He4lhi4h7+t8rnOd
TomyeaYNlm3iTjxQB2ixCetkt95JwEb55KsIedfNp4ODW6v/f+dYrlMjvbcczrBRvmYAmUooo6Es
Jef6NYU0iDGq04hRx7LlUgLDwb2OnQVMwgDHO6Zro2+E5xZaAIqMp8D8IiqVRTsZgg0UD+DbE4yV
ee4owiL9AWYRpNxVRE1oDde5RcitK4GZA4OYdRH4vvgpCUqC3jIlwOM5lI1YuWLhMqYXIBjOKRWS
DxVl1tpexq05FVL4RkQqcuMim97QBtXMSS2SEA8nbbc586VC2VceZ00N4/XWM1QaRFobLfLmDMKK
pvmRZlIx61q0lY+MDHnoEJl3c0t4C99drLGKPX+goCGlU2cEQvuaoz1+w7qQiEmZ7Q8hoBhVd1zO
aDBH/gEvdpO+nMzxiaZ7AT4VOoLrELixsWick4OtW8Qfpk8Lbj8hdvE4SvjAU1g24gv8BYlNqtDo
oxcSBCxcoBjPouNxmpEun+0rROjNJnBVwy74Hm0tm6YSihaWidTrfW/eQgcLypT8WSs8dXBd0qaW
9rK27n1l8gbyuCFZgyF4pDuboySqr5+fgRQGwlbW3rZw3ezkeBC9CPS8Gf7a6RgSb4XhKOM/9NpG
/YDB+zHGulocrNfI3sahIApRj4f9rDQ+i2MHD9Mka5jM02seaJfLKlWNPH2X6ei3oJvrtPNX4sb0
qGQejwwwxakWia/kSMyJTXaBd0PVoJToNZJRYOD3Ow/CpwNM+XxgcXE26SKGe3RmEqCLDvjQaggL
YSbz7xqpfXAKbPUG2ej1bzjVEtHMhwFUmn4GHjsOpogpi+hGGHSe5LVmvR/GpdPpNUBidctSTCuy
+CZbA9LF9sU+S1EXMh7f42taPMvtmZXw2LPJ3Zlstwckve4O6+AqDNE+PqLvDJ8gIY2zmPJd7spX
25MtK5GRrX18mGXeBgJQ2sEUe08jsz9zwoP0xuaUVrSHyrep+ngHQybEJj7jbOBc80s65eIwB3Iz
FZSJjopDEwagHfFxT8DePyZgrPoS6XPB5eitbCixQ8MslEyveKNh1+7lQll/Iuk+/WE2uVG4hDe1
kZ8UaQG0vXF8UW9BbELgW+KBjWCn0qc53uFVmT6tra10UVBYJILjTvpz7FRUGcvZlE6usYhu/rwa
Kn4HmmYoygdNXX3k8tuYzGcm0bMcBPDSePMyLwysaTojLbdy+NhGPKS3Rl98AOe/Vy3jSzJ4e6h7
YICJD8Iff+nT5PX2CDBhyoxknzKxnhAmW/jovw4UH1dH9rTsG9vYgedj452GbVcMt16Rg3h3y/X/
Yiz4c5E0NS14LrmiVh+dsBKmdRJOoET1HCueT3VCXucal5n+QewDWoQuELOkmzYqqNkvDOAAvvPx
VEpTq64iTMZWzkOiwar5JUTvz4FbLh4/6NH9D76ZjO8U7FxnODYZXU5NSFdEOSAmZLtyg7lZnUmt
pRZAVmqeqv2+l05kW6rKMyS2qBPek85Q72lzVVUNx4BEN0aYni9vq4SyOz2tlm4Zx1rf58FQY/b4
Wmz6PqsAJnRxcFYKdhXrpWdE/YpZBJzx5UIxwlPwjyO2J21JgOeCBou5tpZMzhUN55SkViDayfME
1aI5OR3OToZN42jD3gvI8ccEHeggOQ3gxEYOiZMto/Qg+oPw/5SjRj8Syz7r4+tHKcLz+jaEOAcT
pjmJOf0x7DwI59+pPw+HDJ29UZcf/KCikTAAq/n4SQH1XNHRdz//kqMZ96yUDDabe9A067Kuzf6N
wVqrpdqKpuHdkIQzMx6rJD6NVz+LMJYDQQbSkygF5dG2H/2EuT5Pv/n94Jen1WigQ9Zn7N9Am4PC
CSsjkAQmNemvXUZOUwxDOeOtRCdcCvA1NTqZFymyAahFgYLf8lc8gHqP4Dq6tD8Wl+3n0PTMyxnl
OwB3GxAzFqpg/rz66YOTVX1vmROMG2jaVH8x8RYtT9YJXhUuR+DCknXVv9ENzqMs0JOzkI39ChKb
h7qP1xyLlxyGHzofuxyqO8v6xNzwLj2u5JFIDCxInbgN9LBLBWps2SieOvXNGOojyVtuFq5DGrb/
OPtD810YnpGm7Aff+e66G+34qkq8tEnYjrImR2HpMxCgu1chwfpaWzZpSp7LGJKaqf26TbPokhno
IoZRLLYd93CDok0OmnhxE90S329KsksD+v9Aqyk8l1Nj+brtkNBcFHgI70jYBsXuu/QWhS+UC4mr
7ZnKbQ93nZ9tPEP0W5S1oTaiXLZdOEI02Srr5sM37d0aQs3rnxWFanTKJWHcHXdYma2kpBAD1/VR
WBi+1wsQc5XWR59HRzD1MSuvm43apcv8s53a8l1bo34M0wzSuBIzn2sNfmMD6wFb38axpZlgVH7Q
VyRZpEAB0eVdcyqW0HtBQ8iB4MmeY9hZfCAkhkztlr0lT50oJXu6xpR+3j3QTR8NL7TdMkxgCEq7
8rFIHqA/redwIkReZuAdtadHlzVz3o9+0BHhxiDcvddS5oOo3cL12vHqPZiE9L1KTD4m8e5cow5K
PwIfAJmbw8m1brH8eQKB0OpfsnP5pLdNYrezs7YzQQMiYhoBG3xRDIVXKcuNIgMGvr/8RPFFHJLT
/+22hGZHdffqAKQJByapqWUbAKbG7MY9brW+35RHPKGIolvCebbkf50Jx0Ec4+Y9R4ztlfQ2rahm
mSompLVd80S/L7gpeds1YoaM1U3aX0hPl9p8j4SiczaAZnPvifTjDw33uj5M+PG0z2B/Bb432Wy1
XRwmD6uU22oMpDWUZGl40jXSSsH4z/T6rgaIuaGQkViiDEYgiydelFG8PASrkSmGHchfzcaUphRI
Zh4GBC6K5hqFmaZW9OTxYXcsLr+wODYsCFU+uwifruxNhaDM8q8cWuEUJWBweoYRWBKSrH+J9KQJ
vcM2Sl+8JGDzFsx5/3omm4i3YplFdl3TXGsE3PWXwl6AWHgVQ3FaiazR9QUT2RyOAtbRH1BspsS/
wSbBlbHOZ8BJZ7u7oNqVGvVPSNmGx918onHJjxHua6+HYZYvjbri7T0leOp5h5AIGdPdLOhgj78k
OS+zCHWWb/4AAKtgfEAS1l61T5VnliWjIxco4Vdx/qB3KHX6dtCjcXOjWDwriqaTytU7HmdXu/nB
0KgA8J3EFvWrlcBjEch7ZuEaGOTDOF0V1kD4kdztEXHaoSiQZviyRO+sQ8MX4Dee7xK1UD3UZv/q
1tVRgnadcCWY8T1/M3RSgyEMX05WaXeUpWMVTjp2+kgI1cDVNuCZuFUkJKcUbPYSsE7tb3Jz3nrT
gRPrqSwkyfahkASnR2jTn6WEMBJBz8ILVE6Sc7dGFJlRK3XuZ0QIaiA72PA0v6TRyUXDqq0Fm6aL
GEaJ2pA5P4eQNdcROxE/41YWhn/+ShsGsimaPNI0XaiVBUi+/IEJ2d+PKOnh3RV5h+P9JWZj7Qep
uY0ir84jk8eEpoGJtWpyWlzILftt4jA/L7XVAq/6pa6wZDTM4ku783JZcmstW7eT9dKPNOckCcsg
CvhKem0dc9SDsYbKRCqtm+DqmmDqjZ5zoNTxsRfE6P2myglQ2oqqS9cAnFoZGGih7KreFnGQIiCV
FO4aSn7NTZMOlrGONyHa0tz8g2y5TcJnA+ZaHkFxxgkNQ12SK+kidnZ4hY8kxIrnaQvdDnvvEakv
S16p4Fu3oGH3ZD6bL69yDZWRNeZF+rz9KrpyIoFyDkMiCBgZSbBVC2D8rAmK0fS7IfnrcdosoJHN
t3DzAp2GylDCGy6OaLPtCC4deCpjzUiyX2GIcAX4K2gv/GDdL2n4ZSy1HOuVuar1UW93tRx+48Wq
o7Ttnv0+OVchcNj/H9iGefLVuD4EcIQFzxRPqz2Kel1qT0vhYTxoA0b1PlZtWcLELrNRaAQJxI7r
nhH7iqM2pIPvdsL/Na2O7nFEHbI/T686emtEvoD2ZFlH6m829vbWqk5zqhzIurFluUMcD/kKcDmv
B1r+DSK5/mjBEXS61Zu2i6AWJ9uLlSSqD2KH2a7Pk8OjAAtxrSI7+D2grZH0nfivkhDjVCk/ZJM1
CV9kOMPhmB1/nB3rXPRDbpuSX1SO5B7p5typfu2Gr8T6L81zlzMQxOcrT/sDFvMDszaUjhXsOLf1
uaPJHGWVUfUVSMvVWwlTaRVwTdZB1D5vcBkYW/BKm0+QjMB3AXHz0ONgTqRykMBn5bbs31sdq1cf
ATHTNpUgKJyI1G46Ij7BENtRY6sWP4hzxOp0cAysM0h1rhKUUgTBCoVMPj1gDiGiFCKk8xVAih+V
jd9fTdChLILFROWpI7w5rFrIudkh89S8jA4YhI1Wp2jdgho372Vgl3K5yB1d71nwmFLewsfLU1B/
1UQgbUxAzGABLd+Qa8djyRg=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 3;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 3;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(2 downto 0) => D(2 downto 0),
      Q(2 downto 0) => Q(2 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_3_c_shift_ram_4_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_3_c_shift_ram_4_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_3_c_shift_ram_4_1 : entity is "design_3_c_shift_ram_4_1,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_3_c_shift_ram_4_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_3_c_shift_ram_4_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_3_c_shift_ram_4_1;

architecture STRUCTURE of design_3_c_shift_ram_4_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 3;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 3} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 3}";
begin
U0: entity work.design_3_c_shift_ram_4_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(2 downto 0) => D(2 downto 0),
      Q(2 downto 0) => Q(2 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
