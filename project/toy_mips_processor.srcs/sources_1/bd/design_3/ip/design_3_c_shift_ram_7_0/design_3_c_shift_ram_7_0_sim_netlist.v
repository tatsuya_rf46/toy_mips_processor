// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_7_0 -prefix
//               design_3_c_shift_ram_7_0_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_7_0
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_7_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
j+c5dzUsZjLPztwVxaImTBTD1gpM7EI+elQMvnGJJadVblrqf6KReOm5f2BMlzrFRdPQJ4CdFsCH
sJH8HlXh8EkY4tNOmSZClFj0rvzwye2LbvH8RIHIqLin6AJSVRekUl6mp9PSCfOxfHwUvXqguBEY
VmUXsBUEhnvcYLfCwGFCQG+76tPpm0P/I7P4i1YfQZV6oEZQWIcz34ODczLXSGqgkBzTMs9OkMcY
Ph2iYofu6zttkKiB73ikDrvlELNnZrMnv3Sj2wXFTghGvN6AW06DIFYWXhUwoSMsB6Vjlc2PCnrq
rgrd/mhIntVqy4VCb4bRjx/ciH/gyQHJf/lPUYg7ynEqGOkNwuNEzq1j2khrnDxrdsPNrVbS4lL6
MkDSjWd2LVbZrJ0zLy/j/5FrXsBwWtxQycCm76CSUyKPQOskGKtMu3JrQhjadeK2+Mj4MLeI0Ldp
i4zizRlycdnmyu4NU7jxWFhjJXkIWpz0hSXb8oid9qWXKX98MrfGwMIpK8u/ACJgZ+gVDNc7sK1f
lA91ka45wv4vlM3GzI6zHQMChC0E0ZvrntDy1s/O8+Fm9M64Cn8WUseZsCmIU69ppf8gE92o80+8
YDwZqrz8YPp9DFAaVICzJ4PY2U3zROCShv7dVCu+F0+MLdtdakZIWVmRnDJUNeHfFOs5rKQlPweq
ZjEqIhOGhnla5+Ks2RxH+dEKbCudltkD/ycMytWpuqIDzGEI7NvmRbDY1kX+8kb2mCiHh/AMFxFT
LAOEeSNHS1zn7KESGQwi8783S/gP99oz7hTDboDI7eqJjas09cmPZDFj8rgrOzt2PlTVmHDFnTLo
bP/TYdLAJ1gk2LOY3Ov90p+GGk8RRdQLZDC1lXUlBX1Fp67ttXgUSO2IR66/pLEkewmBKe3BoYS9
uO1yEq7C1p9ua3/zsbqWB/ECsj9bZlA0STRogtN1mGXbgQEQO8sKgyJ2Ar5WKW2QFmsGhA9mXlSn
LamZZRujMCx34AfOP9c/8aaMUV0QK7TdtSY4UO4/Bw/lDL5eTzxRnfvaookScbuBaBe6uOXXfiKA
0MGSDMc3wK+TSvxg57zPEvqDEdrrwrXFaa4t294i8yQLDARB1lTK9hG7mxGTMmbl3bSEhVqLaA31
0jBocrZUgCXLbcwdU1gCQhHWfXHJw1q4AUI7jpy9F3yu81/RsyzxMB0fJ9NaYv2St7X6DOW+t++Q
1IymYPyayOE4gWRD336Uo6QpBX0Y8rCM5Jv5uM+a2RItHPDet3W4BM6TjPfzDxBB63hiaRXptNl2
6iG6FxxBSkAPetxhKr5BsHkArQENdlnv6I7VYo6Uv0GBJbF4f2mV8WOyMv2NPoPk/Am7YqN8DQ/5
+dtJfKlA/Yf+Vmc0aJ2oTRPKw2DiZLoh3kh/4zYJTrPFh2gGCcyoK1w1aZ7kqAGgF1XlS2bvu4v3
cOt0F7A5DLRB2WcfVFnizISA2FsS1XFqIt+x8E8BMQqNDboz6K0ljaYylihZrgpwExlfxEYiowaU
uEB/N2S1UyoWuYec3FbQfBvGvb98DcB4yXv+Jw/PaGhJ+XsS9hT6Lrb7rh7FFJrmAidacIYnjrV9
LCc0NDx0mA8doNGZij6I9F8Gm+lXyyU8/zSDgTZ4d4X+7yH2oOe2R3+D7WCxNssZHkXtjnoK8rvg
W4dfc9mzvDuic6R1r+ZYT2CCnB053GcPjjwtP8H2dEh6/0LcxUXlr7NztZptbjw3NAHIHRus2WMk
5ynIpJp+QPqO4bHBXPJNSmu9IAdgr8G/26yxnkrF1UyBDU3zj010gPIdo4ty7e2eCIZQEJ5LT6bG
YE0r8BFCjJqJft/aZxV2Tmwu+F0G+8rHCa38ex8G/++Ia9qDvF+j59JYudX2M5Mgo0pTlR92rlrC
GW8DOv0RYf+QlhWxwcCfGEWcyUUsP9EfP5329BMQWovLMFfCJNd/2r6A777wak0XIpBh5UiFMR5V
FxJzU3N8YLgDsoE7e0MlzKoX4SmjWg18cBGHEtBUHwLqRdvVe7VX2TqP/dzq3yULJhOXjh1sgI+U
NaXr2Wi9gVYETLSg700ZlO8dcA95TJuqaK2mTIwPvLZSFaJarHLnHM82KL4hqAjEc4dgOXl8Y4ZD
g8971NC8bUjJIxeVraEJ5JYOfubWPbwhN/tABYdqCnTOzLKi7sAi+VMecxf8GVcbDY5V9vT5xBUk
UJBiUypRXyxfS3qTRDHN0vnjqVICoGymTjvdKTS/tQSg07ZjbtB4DEHWoCuucEb119cNB7mUTb6C
9MmX2E3bHIlX6FVmmu7xUoqNstAmUSOzIjGKu14E63aV2siHeGrGlbqWoMWEMPCE3iB7Zw/yOiOj
mVWsNl0beRhaBYWkuztcHlN3MWjka6dz2jVTM1zqwB7yzemw/Z1jLAGTiOaAkz9WnVvwfp0KtURO
/ZheXT4m8PO4fj+3Efx/PUmzG+IYObL3da7j64h++EOm7MDa7YUT/fhFMe2WORAJXWm50wBzlDaa
rUJ9hW3BJR174SWBLrIhwjOE5wQSv7LzgMbLJD3/QNAuZIqtIOw5mWG/jx7g4L7N3VFDhCu8jeWK
u4QNaHRXkg7HkAr6PMUoTd9H96rN9EASL6I3NFBRfyxCbRXE4hDne09DnfQnFP3JnxjdVivaz2zw
hF6kyvYjfX4LS/+XFkFywJlbpoAnfCBoO9pQG6HxzzgPVr8xtECvo4KaG3ayU31M8rQlJ7E9vbTi
uSUjpWFtUbzxob8aG5uEIWpormEHyC9aTEP3pGsiN0kyo4RME8BLMdU+MFkNH4tG2NBoj83dwFKr
PLv0ckRRTqP+hmbash0UbJpxeLIhacLyoqXGvUHKEcIzcEhFKd7m6+Nav9LmLCjv2Qum9W72lCf/
JrAxm6xBXsjtuDKlFUp99y6fLL8nG1DHj+YKwwdPfl7ZrEEeTO+smaZ8Tq81rQOMIDaTtLGkk3lm
AwY4tPmDe4Kv5bdJUzRdjWIIdXytgl69Wi7crB2wPEe93FrV0WJ4Z4nBI53btkplSQH3Zy7+P6VI
uocBnn7CIcQGVUN3Bls3VBn1xO3EAVpkbkqgLLBTUnBa2OwCcRJZ8fNqzf5zDtz96WezFa7YbJDd
JLg+PUMa97xDoWFeziPqJDY5ry3nXsRO4bU/vaa0pEca87/6IszoodstYrdpQndB5tv3nR3gDhKT
5M+VJeXxKIs2tUQHjjn6pm6R4gXYGmTLx/Ja/PyVb714n4g97ISPHSTrQjLiQ3/DNc0HadM44cvc
3YRce8jtjJY6bks6SCvp/2xf+HDJAfJrbUiiaDTFuyZkCyp1X9pR+M0uMxW3084fMll4wBH3lpwf
DdURuoRriuJ+GMcqlwlA3jNc/zhCd7+YeefxBZA0Ojjyt8owjHi5i1Yd9kGH3YG77TzPxzeblmtC
sx0SzgcDVKhKRBfsUMmORLo/oID1SCGLaqJV9W7yoj30rSdCN6W+8Ft/Y7S+R9ftbqHw+lewcNPm
IUQ5X+7tAWlD3WTXal3B5fW8HbeuiXoybo26MsjuQmW/FtkpodmoGQdU9d/POJMX+cxmQHS/+YYO
S99tjJhsKRtxBgDF5vBLBVxbZsijHoIuQVSQtbn+KaTyXaM77rH0P2tLZDjYReWfpWhTo9fgXin4
W/3L1e8VGvcZXdP/YEEhbIRfjk6XA/eCf8Fd9MHJxm4NxRHFJqez8tdhuHYwbzYcgFwnh+mkmX8L
3IywrFv/fuOkfoWVCqqGGNC0uItVRVQ54kyiD6Gy5tNhXcs6B2EATutQH1P/Smcqc9mUeXH4WI4t
T9ANTNiPzoSzmyFZMYwV1NCXgFPPue2x5dXshQSJ11tyvnCJFJ9/3fJZdfGLefFpqKkCDHgpos0j
R6Z4v4Uw++JrhaiCH5TIBV5R6/do5UkJFjYoT94yF3KvSjIdSnWBG1G6uufW8ZkrXpgSeDBB/mc6
Alv4ygF9+A8c5AAhqc8n41npaHX1RZV4p6smGXdIV4xeDRZyX4iiEP+7tHPQikp2Ekp6LGjGvdID
kNFUG11PjLmX3bkZYOmsTYf5T1mkTlUq3sWx0Yv6ZneQA2A1sRCNuBx9jH4HTntLuJTRfKGyE2B2
OgYqxfZsUTRoc7bKSRdqggLnGUzqm2YS32w6nrg0C8E7E1JsrMCZHZRNHzwkanNL3OLBVltsxc5q
t0i3ZAGBxXhofZr+Aic87hx7yJNQ7i23bQptOmzWNyVId+K5D0CsZo8+FHUUkKslWgDFlfJi58ij
XSIrNxziliCVBoW4X/LpPU2nnVEYFN2euxrQtgdrNZ+mbD7pFPsQHtqPvqt7fHXQlij8Rl0Ms2eE
aZcD3y9aaSSXeXrUUvPOLQpIAWM7Rxz4GoIAAFZF6QH/oGSNKPu9KozY5c+ZmH0LonH2i0W1b0Yr
/rQK1il9UXFbzmbi5ecAa5HO9sg9H/TeDpDI+jm3z/Y/hqqK0DgsQpLjNCc23/m9tcUg+ClwImgf
jo2Jc3OX2abOzi7+iJ6nT7Xam9VMmDq990fCzSRPOW9oivPvryCEFvgizr5CgXoAJG0OpB+3laPM
hv37S2OhcIOz8JI+z1bViNc51A6RFWm4wEsaiz9k6zBE0uh331UdNKXr08dQ/h69o7jvY1kwb+kg
wud9bXEEATBB1F0L4YdRZhPUgz1chfX1FIGGqzFqM7sz2aisSunD+Hb14tiO6dwWWkL8n4gQDBvQ
G6cl1zyLlZl3PWVxETl8BUjYirXMjk2sLT+K/UHayR1d1Zkg3ZBM3fFWgP1Ui90LeQYYnYjKFXHz
LyhLvQCXDBzlOWNto9gyuhebObFSZmWzPHcEmibk0tO2OQmePsg6LK3Cr8WOyfPVPYGLVOtKCdGd
ndfdSiIOcQnbCm7zdBvoJool3bZalRaFxqw+lW3hoxw3yMx/nTY+1KsbzNpW9NNGroTCtzf1yyCC
rwE4wMFZ8+3ulIz1eYwLcXEFdxsU1ns6inXJUS1YlhPX4QsHZZ6vhTggYBjXxuI6WlSef3+uUTW5
a/U8KvFSGmFDDVgLREPB2+rmv13CDdV670oJRwd/3Syye9npVThENF0LA/GVym2aeW/Bkrpa+115
kUH2sAyuOKaq54SnB6ptMs6onPgSV3FapKYYiwUIPzxRViHkgbznK9tD0YNO8nVDGbrT5kXKPO89
euBOaBQ274xPNI+XsE2PnI6mnCJw4wu+4CZU0h+/C3DQGhe6fssyXvxDEZpfJjd+acaryuYI+Ife
w7Mi4Eeo//9ApfgZZYukNyBQgpP86dienvMmMOTyexBeLgAtknIxEokSpDP2WcKJL5PoES/bc7ur
lrnQDF7mPlgtyNGmoTV456NlX5O8SEaFjdlGVleyhe/8cKG63ZSOcUSVwZbI2Rl4xguRqf/SJAYL
Cau9CG27vP24gKEP4pLMgL+wqL1N49TQZtLDuhOHepdZBdduTIqmkyC67KNG4KmOKcyJLrYXT8HN
10NB/t5vPtqZB4CO4zwirWPnvKhrvxSQ0aX0TrILE4o6pISsakwSDHPUFrXfXRqhzNSrXEHsdNfx
Axu8NoNRsdmSMbBI8NH0WW6C9xxZacHwwpalRZof0J23j+0tCmoTit26M9RJknuYbvNKph1mboF5
VTcy9HHd+BiAKo18zUyt2x7mKxAjqhkLqz4rrP+bu5rOE0AUkcYOK4GMA9YtSFJDkp0n+raRHb8x
25e6k51vX3mA7gquhbJBj6kU4CNWtDOmRqFVDyfWG32RzFcd9EPQauzjnAdbRBTCy4GlbLRAIhrr
zfDxh6Mrt+ngMnLFPAppMTrz57UA88rvDwSEqgHJ7HaW/JS7P4oT7TTsfh1EJ9QSeXmZCIomXgfY
VlL/OGGpSqmNIEA/k1gMxqwWRmYG7rP1XBlPFaZp3GyKUmQCs/jy8kxpX4f+WZElbcc8VEiuvhF7
i9D5EVWFCW0BsORrIGMjr+wSpyCO7dr7W8GbfUbP6w7lpQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
