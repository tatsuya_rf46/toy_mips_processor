// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_7_1 -prefix
//               design_3_c_shift_ram_7_1_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_7_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_7_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_7_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_7_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
Nf8nTWtFVpAit0dhL6WzMvh2KQnJ6ALZ5xOv6JR11aboH8tO7z9vXBDjjucvQ89zwJngkooxOMqJ
wXNWZxqod5JN8d8QMd+NKtV2Zhr55QcZACUGA/WngI8CLoNNYP2WvkEb3lV8J7LA2fclb/NXFPrT
/64hKnwQkdyATR86K+VH02k+B2E/Qh5uW/ZcyI48gGuD+kahmFBRhrpt8UrQtcN0jUogsXKdlQcm
nkopjmf8geD0/u8TfoEk/NpJMSLIkU5H1he0wQpjeZsQqIsF9UwBo+2o8sLU655Md7kmdTWny2qr
ubf2UfdYfmclVNv140bUx05/fO+LUlaLnmEBpDRUUFhaf33njjnU0yFx2yw6G7LDX4L8ZGJOtAtp
8NyZZ8oX4hJMKSnf2IrhWRqXLIx+/7BqX9PRq4vm+GNfOJt/56f9GjwWGTNAV46cUP462P/muC1B
yR12FSHJJotBw1em+ZoPV+PRNqSKcUxkwaoOy2UtwgsGqG8llSDoMgMGnD6sLwnBgKH/GKy4MBiD
tdy8Yajgncu8SGlWW3f8h3ezIxO+NwHRTwtWkwLo3dWwEM7nZ7kR86fxq4lo9JdnHjOl1vRGydBo
pU6lOUTcd9+XLTUNchYlZTUZWQrolZmGwDCnM7RvmOao/V7p/Hq5kGdsZTCx+ZWePLS8BI95ALNj
tZtLMODEZcnWPX1uCNdDe5ckj1UUGXzubFyvmVdRzJxYKXiSFoK2GzkdGKgl5vm8hlMpkSam9/r8
6r/YHO9nu9rdifMtEXLpYs3YLgG5u/BH9Aw9G1fWUKIQsu7azpCSEy4MJnjcwoO+cBUB5PSiiKhB
NMgYMbU/IYYyAvEhKAPbq1ebaH/WjqMg0Ufbb43c8uOI8eJysPYeC2QWzVcyZRABzxwSjQN+zv2n
kR/D666kWjJj1ZwmoqmcLEyYkrFCxS9e7F1muF2ipwzB4BU+SyaXWJZJQ9zojx4GJ1Uhs2SVnoxb
DHMKtVJ/Tb/lwvHGVwX2vf8besNDJiXvcxYkts5qGb3rhU53K5titO1ppqxlv6U0LVQcXULGOjp2
W0cFdj9T0CwYqrnfdMUi0sqfF6LXtn6qDZFxBmx2J2fnAw9yFrVpqPnfLMF73OtXu9ZXJFCbMLiZ
CZMTmXlGpTPBBA/u50Jq3Yg/Z5HVOHbIfjAjTi7hPNucXSvrYvJ1KW9qc1CZHPGL5uAO2W/QXYgb
MXYgrClPSgMnyXUIQBVnKgHLtMFNxuBSuaWY9Ys7KrEaqC5b0LTtIh5AufRkm/aoz0lyxktzry9b
N043ZttqeqUgweAfsx3nQ7KYEOj+lJQpCPlZe20QQI/lC4s+k8oHIubmRh3qTQabBQc2XWXKXiIk
5mrscX9YXl6Ru2SC1UiXvXPGRQxuzyIE7fYqehFs+9nKJWVuDo8+ecYbeIY0YBkdOEH7gx9mDaOI
eNwrS8CB8X/7Br7HDxTFBVzKvqTxykC9+N3mWdyMvpcBPieoTEitZ14FBzLwOe/P13jj9FX1ge+h
AJfh/7yzhwabTaFv+m0cT1+9mc3QhC+X6+Nnxh/9AjmzBb76MnEUxzkm7kapMC+mqMH5560bvE19
QjQJagIlTs8+cm6hX0k4tJc2dtUitcronpEeoTKWXl9iSmkTeqvrNLF15gX+euPzcOghkz3raDKA
Rq1NgwsRGfhxPhQxa0WmMdQrHbQKSxOY/8EyFeVrOOZEknkvO0Uj5CX4mft0q7d/lR2vZsMlgbgX
RzRHHZ8Ssy88G92yxJGhV6u9Zf4onhq/VyZ8n1sIRkiKrCp11Y9DGWaKbsoi0tn1kK6RDEIZDYHJ
zWmGuVoxJxRSFyChKTZ60fPKbyo9vK//uzzawnNe2fBU4HK3NneFZ/4Dyfv/UMcsalfRS7nhZN4o
tL7B9oSV5hVIduKCjXcvqc0/355HRGgL8nOftmH5iRUkPDKOuliTaTWqJU/ObdsONTuYfGgMHbos
Lvw25w/VZfAV2cz+LZuIffEex8Xt+7utp6D2breb+IYfNe7ZDk6ChikFSARDyJflZH+9G8cUw2tw
wXDJvg3QyVCcpy1E+SUHkXLhWh8/D1YM4b045rH5/A5/4BQGIKdvTOPUTN14KeoddB3V91MwGWbY
EEG1CSUHF1Rnbc0Aqo1mXw84HxrdcP6icY+F19fPuuBDRW15mPni7RDDXEMRHxM+ZCuuvtiKEH+N
oIt/iYkHyOalE1f6hV7yHoPrtSf0WbW/vgs/JRul3+Wns3AY8hfzGUgGo2MQPja9TD8aRnKKEzB2
xtshlxbVDRYCNes/wbWkmzI46G8CtVnOuK8/2V40rVKMUmOJ/MX10yTvAkPr9KNfD2J5sbV1dIRp
528zsv3tIjMNPswLX0uclFEspXKuMAeb4QACPufgrvWmzvxcvIBx+oDBmx98IYIThpM+GMZ4LrLF
svtoDr595j0Oa8j4qWNNvK/O6m8f0pBO8lRIXq3pOCnk5Z3dWigqgBg5W3d7YchHZ4CohMhiY0YQ
GwP3R+mqknJqF1kKQpyL/ERTnADz1Y74DCBu7ScsG9Ym8TDUL+xpmAFXYaOAs2gb39FLqv+nTIXi
kBmDxEmcSAP7I7HJD9yQp5+HKwAlANaYnosZ/3m+ligBw+hFpaC1+qZXY36dQ7HTnZAiChQ0UcXm
7WxpNRWJ+d6r2JoqBoEiOjgNo6fpk46khEYQu1AcOh265KJbRgIjOaty00uiXJ/WBaT8nxJ3y4Ev
fcHi3tGxpshc6pRF/4vJBYVoyEzuHG+hSvby6glwbxcY+4iTwlJq1zg82nDtleqR9/g0DCWHIk9j
30slafSiZJgrWMeUeUrAyPusviN3sy94qVMbl/7LSo2/CWSLSUwUNU6wLCwOTT7W5yv85Z9QAvwZ
c82Hpr6GBD+KhPUdGCXTJfmzR4Vz66dMFwj8y3PPw1MewDBt2BSrB8WK7pmOjvVAokNm/D4Oz7YO
DpMnykyoaARxNEcqGltQ6c2jqKhIu1FDPR6Ham9esH9y4KZu5/lOka2pDuFzsLNwvO3mEaVka9t+
OXWS8Vzzih95AQTWVi/GIpyrosmYVHWLUQKoxLGFelRe2OFXWtbzjmY3rZgQXqEmgdUIDP3gG6AW
5czs6dvtI9+pYipBRA/afcr3yCfVG4obwg6nsQxoI72I4viBQs5RBureZlc+NQWDiyLCEXPLxUbs
y7Bss42sgDa4RmudVUqAsfSBUJOrPZBcQQAkT7L023O7WKp4BJTeWVpl9yFcH5PwNEDeImOtt9CY
/2sGu2wQ1pcJe6J31sE1pYsaTWXkSy7HDrkOb03uthruyl58CIQBAK78slUPoUid2mTL+9mDrfsM
5/yTwai2xSYSHym9VLC+cg+OVD+/XAI0FldkW3BUfSNLBzDZGW+2oAs5TIEPG4DbPmveDqOuubUZ
f7xmkPo8eyTctfvtRmoF1yjjE9ssB+qEzCdf6GytobWFlIfWmZzQbMAjxJIpZJGtwGduEGQ8OcVC
X0sozKLQQMyNgLclsXwU4IZNCuXr8h32Rn7bWg8D1mSUKUj5smTs4+DumP1F9kdB95Mb0f+SbvVO
0j4+jtM7O+oPDBp5nwXx22JloPcNa/D2IuCR33SwvL1NOhTzMei//ihvOMYl0/Y9ZvmBSxifmSW5
Gt52tIXSFbNHS6fTyMs0aWydTHI09lblcRM+ROPTSzscvu7nKzkM0J5GcAEXD1sRJxzBZHsGWoo9
LnCMvAZPpgelQD1Uzwn/kGpJqn3dlLu1rELd31PgA+/sITXU9ETop6WDIt1rhEf2jDbNDXKUBE/I
gU74EjTby6UiLKsgkhekd/jRcGUsk39Mtx8xqIRdwnASkR892GRgkgjmy9IBeFLSiqnRmaEQGHFQ
eV7stBJuweNSKrtQCQ/eZnUJQwB6Gf3/UTvG6gMlkArgVwsf7KYGgTL7LYcyQrtTX4p8OdppR+iL
oz8+H/ehmwweyqP65fSvug1lgQxaniQ0RlVlUX563KSo4WCxSti5+XcjAUagVtwFJHBr8wYdGab6
fjo4F2ngD6CVQMwt1HOvXfJkRB+vuLpsC/v/HFjNDbdM9gI5IZxMpyel0/1bHfTI0CbZVlCmw3A4
erTGU/bl73NXmUuMuWw224TFVKDSFz1D/0HAsOLacd3mDGuroLH35YqDIjQRj/9fmcmv/JqULndA
Sff0P/5wKPfU/CuxjL5vea+Dcq2HYqysxMktDzQRmwd/rpliqz+Hs3qtxixAHxlRqqOswoQeVeNU
ilbycJseYBX9+ZQKctsLicC9JeGmguISkrmDe96UfaWGlsEr2TNyuvsorFxcTgkGHnbogyjNl42e
WRhvuucrMhTi1q6twLYwaGIanE4UO9WYCni6MwCN/6NUZfRlsQ/5gd80DBROmvvfWcLsiLrrsnQd
iZSLKUa6zAQidVFwEx/NG9AQZYQ8RRfHm2/h4/+zZY3AzM0H9zIAIxbO1hJHUejp1T78KKIkt8+/
GlID1/D5x1liaNSghtxoBHTDMBXwrMVopZDDgSngVxsCNQwmByRx/abFhnaeTtyiua24YXm/o04L
h8bgEGE7JHslOC3IWiQLERWU79oeE/eeBJRx6aSYDJAU/9r024yCaZW3APqUJXTbup8HQQzqZXQk
XYiNcbeVTYmWG/AUeSJ3sQCFWo0H2l66MwX7q5KSWixaKEEpP8ervlqLxKTo42ea5D9rlc7mYO1/
IR5muOLD/RQQWhTXnkKkoMtLUKSZVdbZKq87BjIW+oO8SmyQmV8/HGaqIpEsHUirOHgvdLxT3uE3
dbHnjhem1Gm55JlY4H8ZAArzt1RvazgyGnlCn7Wv51uQUgy3luAKz7ydBKEMDezAZ99uov5gHqMG
odMTqo1s4ewqLOE7PqqYUfR9SeQWbYZeLy/3VmW+5uJgEI03JfC4tnJQ4mIY0orP4xkCZ276p4UY
lgnGjyeRdsNFA+2OVVmsrpbUa+kqftTMtvweQh+DLK642gBiXaI+3ZGVYbA12bp738V4V1j755kA
HjJvSdFFhvsLijhQfdmDhHodlM8v+lb7POOKRlt5ilFKkT9CVF2Rhn9ncK0g6djrUi9PRrgg87uJ
rho4l9/doDqZn9wUjfuhgvkbusqdIKs5/SLClWrwl5lvo7OreL5TghDYBqlj7yvNgHS5vxoKOHLm
XfRrmQPeDp/x2Aq9aObvFtYUJavtrt/CufuZ/arJwfVlIbmVkax13SQd91UCb2q1MuOEB51/D1M5
jYQvLyCXa5a8KpuYH+4jY7TtUTai3HA66wglPWSSmEz3gQcpWqPkr1doJDpiB0PYsPoGLmAonXhR
UCgDnjBA6ABClgNTWZCLJH6yW8WyIF1beIHP6mBshOyuzjx6cGulygOzXcmlFM7FUVeQ351rdHW+
dJKn6RJ5VCvOG2SGr0IwSAnPw2QFeCbn/pjwfZZzq597f5qshpQS0CuKbgCkLcMI9tT1FVJ8sPxG
3CgqVYEl8UpIIWJQ+Wo4tdDgd3NsW9BjgMwalL3Nz437wBTfVep1g7no+Ne5xh7oNENLKe4gGnpJ
MQxXa3qkj2JD+hxKpuVHH2ZodV3p6pQQ6Sq5KknmQ/BR4DGA+CC/pHwV3YPVL/GLslDnMDQRSNA3
V77h2znZh/eBSxST7GQnuaKs5f71IjbtycYbXS1VtjHX0bcZhTtNREumt1LTrGZoGzDqi5DQznwV
eG/9mBDw6Va3AG+8OSF1uUTyjyWPNrWSLjVEMQw6bU7V2ukrAPtHQszIJj0W15TMPzr70XkCiI14
A2Yn0pu/T79SG6eb8tuN+P0K2gTaiPEuqUGm0o8gBjeXlA3mtegulvkJjFoR2rVeCuLc3KhNve/p
xt2hgrsNhyZpjoSJhqYZ/X82DVuFqhuuS8z9M9kCSRqlIe02i0TA7jXh5rbz6IH++UHlVeSKg615
BzLH+d4x+7bBPgIjCbVY/Cird56x9SIXBbIfUnMaUzdtSV15A0uQx0DiRwMLEcwx1yIOr8DsNNnl
WjKnJpQ/TaWFhbNvu4idH3Bc9FfmMfeUhEZa7PuW107BxOE6cbBfvTTC39HWBj2uO1lMqZw0PycB
JnJ/h5FhPP5akyhZ7SFGQjEhMXLQuNFR17BeR0aizD/E1Mnco2P7Y++NtxH3FQyroQDDjvi8wC7A
/H4yBPQmHzRrPEyyo/9DooNQiZ5DR+c4XftII0s73l6e8xDTsdZu2pY0Y4fP1t3X1NhTZqkEw5dO
og2dZCsVp331XNqXOF7yFov71E/G9qGcBNleUnSu61pViaWF5u9FWev+isxOgA2s21XsYXAnAYqK
SAT5IhC/RfNd9ZZbN2l8zY16cNBoiec1b+7nxxhUeZYDim9A57UVCK+fj4ZK+8GF0c8ALs7CA1f1
Gv6+fSADi8YIWIYCVi23YUNIHzN+91xDYYKbkohoRbV24ftL4lGhE0MgzcMKA0zspWVtEZCHOjOi
Jr+u4E+/AL/TSO7XsTOBz35E3/8UJ/H8U1LndGNrAM5R3Rn+XkCXWheZwk8q15F3P8fI9yewstaO
/jPfgWgkfFgY7PraToLAr8S/CFLU2BaEfoeuaTmG8Ne/fX62ctI1le7xgQ/RUx5L205eb1yUw9Mh
9EvtnghXJXWiFmIyiSoSovMEgpBqB4fWFYHFqx3CDaJ7/atvLAlOSRBoaMoQrJP0HbNCMLbE7857
ccB/8zKpDm3VPecbHuhBo0bzEflx36rC4HCH1u2hvzIAfEYwbVhI5t/EzWEF7C6kFWsDdPl7/PL4
L23wPqwlzojv0cg0R2hGNI1bht9usDeK3igCvU5LFb+ySCMCCRG3UVLmj1I6VHkxiT/LusAhdlEN
vwfJ0my171mmhZEJN2uDCTB/jXt4ROz9fQw6+ijwBgwzP4ccpGtebZcdGbWnKp5EA6V2liV+JHji
e2J4j/thRkyDu+aEy3qcNwtZtsFUGjqGn57U96oNmOUCcZWbapWCrCsOuHDMorUFyAQdPUbXcQU9
ANVglN4sryLnTUGEROfKoXnXBfUBfWWcb8r2iuCB2zSMW6B7718Lgk1XGtVV1lz7AjMf9/DbzzHY
TlYJcmtr0g8kfRP4x9VSMhPo1O8okqQPamIeRK6eXjr3XeEhmkoStrduf3QrneKeI37EeYPjfYhI
OjzUwSI74eRqbosEQ8BVhG3raIXsRQb3KC0NqcYEHLNkMFRQX7AlP6Dj8mTMYFQripz1srnSBUOV
Mli6ZyFrS9eCaLVvnIQwzvEzmxKXlzKYbRgCptmhJeTow8m0v03hpXxXWUyGpBnZz8HyekiiGVlW
nYcPNoCMnWMrVzAaSTQIWL4s/nsbCcozBTkfSEV4hQEcyyMnMnjB3vIAcOQX+fFTp1HKNWA/eueJ
r9ttxUYg2xHadZ0vG0Hsdycy/u4M0TTxB4vIEgEAyG9PeIlGaZlIZkqBVaovY3zbbRCFEIHkc5hB
97jgD50ovbBLtV095DJVqFKJtbMR6mIIZIdYI5ScAHE0PFIhYaO8glSSkKyj455DPI662s/vsfZT
MlNBkhNCpBVUEaongMMkFWF1e/KwU7YDEmv6EU8+xj0PoKsfYLwfWtwgofrkn7AYQohRrgw321Wo
hdQZC/UW/oreXBhAVBsx4OpvygVVtCJ4qKIDxqgmERhP7oLl+s+2BWq9qwIjd7KOlDoxb1FHyZ+g
9a7CajWGMQR8KeJLKgpvPCVceWCyCJVk4crhS6amABl3+5zS8vSRDpT/UmOUKGdLwrGQbloVKrKO
AhEaLwUc9o0Zf21RuZdGSnxyUp34t58nJVMc+NhPHOxRP895YOo9IcihPKKdj1gilhGTUU+tpmbQ
cf64nXRK7uKLr1nmGA14MnBzWeAwklr2G3xqcctbhuXJFQlCQAP2mXG6/GfgoaaZSuq+i0ax10LY
BDTIgSNgoVYBFeWVFmHzAjqqRF8xMOT0Zx+T91dz7EaXtcdhixGKCex2KXbmOFmhbDWPcfs5Tq74
GpusFwtZORLLko3FU0uYrn0KVoOjPhJWB8HDXQ1p/h8Bnn0qXaGEglqSh2V1pP6UBNjKK0AUrhEP
4Y5fIa0thcdQSp08sE+d5YToPozVp8S855DYmrTANGYfsBiTvsP44HyYT3Er+wb0OqD0/sVdRtp9
u6rkpnNIB4/sJ6mwVFZNO0Og8CMW9ZLG23q2bE17urYbrBlSHo7K67UPcvh7IJadxLbenD8H00N1
wGzHlWfCumJ1criJqlP+lQyMZu2mGkxMIzcYLG7m9KA6CNiXoLQIxSGVPWa5l2w4y4gZUFGjepT7
kDfO20+qWyWWhkb4aE7PkOrvJZ7txjVxz3rwTkri32phuNYhLsFH3db1LzJgLHrcjI1J1wsZmoh9
k51MLEnPvUdQVelX4tJZfVEoBP2Jnp/HaIU1v8xH4wfZBEKRXLVAS9/SjzaQW9kuNEV3HUT7qmLO
N/5+5Jhjkj43dQ53cSh7WRgO551m6wQkHhJnoTrkpqH+Mma9YrBedMZOj7lvwuwbbwZQQeTHbanf
iOlMxMd+EoVq/liJKCSe+b47Wn02lu+dMoaHTyJrDiiRKNUCLWJ0NZ2y4FPfcehQMMNbG25N9638
R4t6dNgw06q0DsofYypujSrxVCoDfRVlV1I/ykQ80MZ3HiNWs6bxv98HpKOp+B0i6Hre1ettLVfQ
BX7uaNYtqlIw2jrUXfzTor72pGVYNnNoNoJXiPCidc3N/BmgU79RFH1LL31L5eBAL9zqV1jn9Qex
s4fk3fNxQU/j4RlmCXluuv9fn9gEfcrD7WGm/WoDsyfyrKUMRJk9vdAJqc8NfORyh2AuCzrZf8gC
eoiOhh3VzxEl6OnZ7E/Br8BUGu+rTYT6tbWxHTm2y8HZUUZRyCgCCOBg4801i+MzLDmEH2X3cKxm
OsBU5E2E2hxBydAAtymFrvrFP0/ZybOmNpSBTTq8tAANFhb9XCfEc/X/sipQYYAbSsvvxKBF9Uyq
YWSe7eAKmM/2jge3swVxdZw4r6qGnuqHg+T+eZkFrCruJVXwavr8rten+RvPVXqBBRCj6yEG5vuG
J9+K/1vKHJjUiiw6kcVhcAGP0tt3VTzd4+/fmdWeQ1sVz+Npx13XpAjkFVARr6Eb6fyRAlNjEPOk
RfhOit5EKnpFS/eZ/K3kyhPuZIF2DyBWIJUzltJy7hJzqg0hj6ynIoBCg1z95AogZqifYOCH9rxe
Xz2Z61+kdz7ALP+6mUN/ZqDIU2GprDSbPPmcg1XxCTV1wNgBdFzQn7A3vBbDzXSb+7bLSztCHHa6
yT6Cg3Nc2fcq23VLa0wPPlRS/y+lkwaFTzYq9a+ZtnuSS8nYESyc2ZFRAXQxO7CGH359EiCYZRHV
3iJJqPqEJgOKuZ2mQxJa5UtL9Pp8x8+g9CZTte3exmBwoDGDBhcHa3RwC41GUw0fdn5JWdNptrRn
dsTRbgCZK6wgdps2vtJRRuWKe/ueEWvQPzdZIV3UKcDwttBODaejRNgejxtzperJJnKfMD5Nnlxp
i1+gkCYdkALpZQ0kQK2sqMhSDeT8Ldj5XU6WTZLTkaAPZWv59wLGYAjiE7Yxf/xCfzFhhiO/K9HW
MfuYRnZfEaUFexYtYGtJlM5i3YaDP5Am9hBMOphONjkKR7+6qCGCgs+cOLmWhE5QmU6wC9EKvU69
liTm8rI7KcvkowC1yMg+KKo5r9cXJ5ORTVGjWw79Qcyfo0Ihj0tS1onz2E4pB4HXYRtTqcS+Rzl4
Zq+jNzLyxUega0mmD2lc+ei45Ckx9KAphG+CyN/EOlR9jIye0H6EvpFOzKF4K/O6SGJQrClinbMe
whQqQ5mBatVICkYKvk29vFDuAxI4tsQICSCEnLM+OG9khiNZ8/dYNWu0QWESSn0vlPe/i2khYtzV
cWdlx0qDCcrYidqeZmx1aAxmU1q1mGlXcgvemQtL3fnUwSdK7fJ0JB4JDyHM7GDSjaNkX8tLF+b1
01MpM8Xo9yoZbFRT4N84rfVXWrMMzO+UjInccYBeDVo2IC2DbHdgQtjByPJEe8ypwQS/X/fG4lWU
xzu0reGOBy6TN9KLHO7NkvRkyA7i7CP+e8ZlSVAUpUAvoe5VsR2bh/LWj0b/5wYFjvxkvhQGP1i4
I3cFSux3+movtuy77BnmpbnDf3aeNstz7NYKHE6F/VYrOPmS7URRC4y3l8j7kXC8vzxJQg5p7nSO
OzNwFXNd8ZCYwPoZXxOb0A/3Ds5FiBOtHDinTDUzaAyAXDKAVxKSpCRzH8h4q1evnYrZ1L2OPvxG
IzwQOiG+w/Bpn9oiq/brVX1MIRiLNDWxAuFiQH5rfVwnPLABkkH4+Ifgy1nXZ5kaILJV+fjXhi8Z
1/MhC8V8DC756qD7vHkf61SJYc/2sh/cVTNWbgrNDpiEGIQe55tyQXt0Ke5A7xumZwpgTOy3JGFw
P4SSc21oE2urceOcuvAs9YKgR+2cmVdL0FupJOctlBhgO7Jm8ZslKTPEVIdxAg/EWyoa+qfSO9bw
OtuwsUrQSxUwvaRRXNcSNpSljt+cgVQ058yWrcsWyNu3qhZ5ZBSilXhXEab+TxlICUgYY6lZk6FW
ENDydwZAsgjQpkxBL6pBGqstcI5Y0Sy/YnkgioqB17nRxq0QJ1XwMt/gn+i/FIzTIs2DcYobbmwY
7EnaDMno6t4EN9eRFZoWT9ffAHJSiSg73O/CUi1g2qZpDI1NGDl6mQe8SOqpraUS3uc6Tju0V1tf
jdpjb+FUjn/qXmJDMNFeUpgrGHEHjCVpzCdfrQRfnfSbT/p+gJVnjfcxqEFM171PVw2ErqHUWthV
uoJiM27qTiy9g/4OR98lYZ475nmgrPkm1XtkX6klZ0f6xYcdDXHMNteoV4KmlnWefH9CKRkLwmYC
59GbWLqAhcaOuyBR8Zg2yLk5/Pj6GfTr+ILLoLztQYtrELLfcrEr4jZmn4JVw0qZ8N0A/GuYy2HG
7ucn06vyiqcuMORKjse3cnapNltyiE5FDadc3J/bEP4CeYPdePBoNiujvZtPqGzSVHUqyJ39RZr2
8qsFm+T9ZOKFcnzW8D2fC62FyAC5ClFzNcpVW2J3vKBCS/3keXRcs3YJwBGNVIV5qApv9aOnBp6n
nBkKME9t1x4J8rIuhOJ7zHalGyoGCliDzW7ejlBuco+HlcPBWPv23p2jcK7ZGF1yzUWqg+uHfboK
np2TnpqyMeX3ymCMI2RA04QQ2XbX0nrAmC0JhlI7upAC+cQ8b2ykVUX1sQWpVdiL4v6/mWpo4j6a
ak6DGCdoQ46Zr+hd+KNMZWQ/PM/Kxyj4q3uaB0M70ivfBvm7D78SKst/PhWlOWyHCpLKl76i3rad
NJonaJ4ocvkV9qnFv+tH9WGoO57udHUVGUZBgaaUJLaqIhmIMrMNjFT9DmE2/kV3+0UD//VH2ms2
Ptt7yw9BMUsqq5XVWutyB0gOo27I9Qgw93mtaFdiqzmbwf3Q2l3vj09V+TfGa7LMRYA3vVAjxQ1e
YnzCL4o6ASPwp+MCrtGppucukW9hLRGzEwcfMG69ZGE9dVjqI2cZx1OWH2OkSUNqWuMDoQ0Letkp
Y+EJRsqFFV+OVct3rSkDL+b4x9TuhcLLIH2VgcPNZiS5uwHg3MWMNecJ2TE865hMYp8slID8Cc/c
w9mwKS/9KwvjWrkbWP6Jwab9hcp0/Y4CsEyXp5Y8p3mpUWhzUg52vncn6Sl+qnflALWY4/OlIEy0
9lrUS8OrgIAH+B/s6vaNazF/ym2EnrEG2vngTdT7fdNarCckq3SLCp23Zs1QtdsdkbaMGiwfnSCo
25wWFLWiok9TJSK9dIdEX+sW7AUq4jjlXTZ4RmAQeq96bmkx/OAbhBRhbfPHVr0dfRmNaBf9THCC
pTuZ92VWPNfhQEEgyv4pRWXCR+YT+oO1O1dYYX5COBgEuC0u0pef6TO+FCFpSKq6DZ9fjfbeQ3wl
q/JJG/snBIVQwSyATnSAPZePRm5lajLISun5NRnlsims9b5n6CwgT8FtoUWQ+/1kUbMhEPh1Lj10
LE+OATzhB6e23TAgLO/rCHCEYXJwTJXVoFVxzFV7N6tWL4VlynOElMW7pOPmzLHZirokpXRiDaJA
vN5uyZD69yoRC6WQXecgnDEM4X4mq4tCqhV2IXgEsDmmXU9RL0Wx1QWIpiLLBzZJWtjrp6+MqDBT
li65FicaAIPzKWP6ReMX35vr4gVxrno6IRwtTXfr7MjhbCRRm7NxLc8XYt7hWiq798vQ0NyqaHmI
rscSaK97Rh5gh+RkzdHfFGF0/iCz72nHEQS+5slYoD6G15bqabNg88FA+GSklg9vQIUll+zOx0hp
Js3mqJD0gtQcqCIUBi/cb2Y24pMXOUNUUiPlBrlGZ0850n92J2sZ0pcbeLzlB2fooIX5W85mlZ0n
CAqYCqsvkNCFWmrKxe1JqXCjl5kelarUjyzhehqWom832ZHBQuckyfiADlY4dzzNleKliuCu/7V2
8ZViAxDpAIVMgAhTx2TBwuMK0vulGZrnwJFjJEBSUlSeBhkMz9IWCJB5c7mln+sKXqdnSq8yZ5NS
Xu41B7Zy4bK9v6EHtXtWL1mJCMrMbRv5BSAn3hptRqkEW/4i9MU6/xgj9rbRYtHrEc45p2H2qoME
uDDTlyjl43mcfD3J7CKrFAxyhSy4bLr038awoqOHXnksCV3Bt1CT8x7QCJs6zqPr4lnyBrBAm4KO
AAgTkoPzps+O1bpnWhqDup4=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
