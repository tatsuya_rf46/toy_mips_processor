// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_9_0 -prefix
//               design_3_c_shift_ram_9_0_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_9_0
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_9_0_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_9_0_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_9_0_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
4wn5BlAJKGvRgXEKNMkH1db+jyqBPUJHEHz/yBzQAV5rowpHO5OT/z6/OotPAiCNCrOTbcDnOu7/
OtZ+6wWS90Vq1ea5a9hLS3VrwLEkehsEzgdwje5MaSN707IyUhx+lcltleORnAF6ocJw+J9dHAr5
hq54GMd9uLdJPDH8zI0/tgqOZqb2Qe+VOb2B4r8lCe0KHJCy1TeaUdpC0Ly134jImp1PD+rmHYxQ
GJr73m1PB3X25/btHl+fN8oCpZtMsIQq0B+4TF73AL2YQIpKEZOu1PR38Ulfm6oiC8b8X6QE301S
dtt440pMG+iwsYXNlEHtCwh+9flgsvNkkf4joqICHuKx7XYnGGBrB5SnP66U1dfQSqz8PiqB0hWo
9rgskVyF3DUwT3//0dNIFk48kHK2qYxuUz7fQ6u4SKIRVIQtHu0FQdvlntyFDb4OqxBZptbUkl00
OKZ+1ch5fIkAV2AvFvaPHbX/E0pwuPLqnODo4GBEcYjrfvsOFBG+69r/jBVCQvXrFV9zioBQkz/C
DiYsxKMdTfZOJ9nIKaZqcJ0zfc1mdWVaSJXBUg+XN3T3XI8U04GBgkR5j8pq7Eg5oqrXKtkYbjVp
Xz3jEMB6ZuAwxaxkykoJXcgCCafdcOf2ysNSdNhXfXzouHKoXChghc1IjrWm7rr0+Myg/zc1yKkf
J/5XTNhPnMFSm2474SLa1KB29Mj/lit88lysGBBnlRsYrm5QZXn7T/x2zDTp5ipnV1cdUf3DPlnZ
iMrMgU0QusbrT1JjFFYrvtk8+VxxkXH/Q9FP09sL3AusbnYTzrnRkrMJfKwpTIVacCrVJNN+e4o6
29MvkBwlW6kZEjTh0+jVhU0EVbOJOD4wgpbMoHH3p2opP2KOQ9uaASPUlz7ulVaFtjxMbhl+3VV4
X0gylT4ylTuiFrA1wT91dEYBpKxK4adfGr5I0tcFa38ppgYZzPGgG3UN3QMGG5q8aHtAiLNmi+86
eTFsWRtONnxF2CVF4KMy0mHoOr1PFMhhCzMUtc2WFvYrfpEMNMmPlMF6rmgXHlDDLakMF6mtT3Uy
Nt8SVzh4u/4Gt5+p+/KdhB0eV23YDhv2ermtmS55p/kegNM7LrOBWQUicQccsT5GqXq9UUF3WXCF
tlnGtrnNusFMWvgLX80640tKGRzEeRzFtrccv2Jy5q9u+oP9un1AW3spGzPde64YptQb5Q7yQp1y
Dp1p0tC+uYBKQfrpBfNdc3aL/oGQhGX+Reny3uHv16DeRUoHd4hPHL1emILvhViiXVTjlpGeklju
YRQZcNY+XVPjLaHN4E+9xW8xFHwu6dKJFCL6AMvaKte4VRMFnP8SvL//EdqVE2jQRePdLpsHLeft
UWIwzsYXfLvvjikJF9pAW0jrCpC/q8qJq8eel/2Y+T2C/4VATeTp7HD7JZrGHw/6mGIHEMLD+xLt
YOjYrCTvnBjlC400KFNwt+FcYYCr+VQ4iIH5q5VLuvyziXILRu/sWbzw99Y9Qh3CH+cvzsX5f8ib
Xe5ttwO3tAEK2GqBELTL9oWgMr3GQRt70yiJq4U2qpCEGEEQzHRfqDPe8/zMmlLxdYZ4/hSgZ+u+
PbxexIHFMfQGfrK+szC7jA8ta4qLfop2NpKwZa0jegaWTsnYgJgpFPatY7Ue1AjcXKDvQgi7Jkfj
U2MOHnWwVArdP7vzydGm2mHpbOVh/ToTXDDUZlpHIoJXmmMa4xdUPv3p9pHWR0kOSurlGiGTOK8+
7GBZ8SFEbAHjhWX/kMJn6FRlJZPG+yjHqYxf5eBI8hL4M675QM7L3Rfm3/tWyYM+2hweMxVnQvWZ
xZHXlwD9p7j6yb+Y1P99rvEa4rEJBHCQHSoolOa4jxuG36xNTn6thkOptqWTyPb5k+C8GgIjCkuR
MtPnIyPmDk0gvFY0XDmlyq9Pp8CqPMUuxo48+SJXfYqyLeXuc+7radkqY9K+LMzaiHO2yPsH72k9
wQeb/byHdC8m7yei7DzVS27YFJm65vflUxy/Q5mj/LWMrBUUXOT0WDt6qmhaepOlLqOnwwVMO8rE
2QbP3BGUqbBpmgoolCCR+8Cn+aEDCRvRt/JCmY2hH+lvesMzE6KBQNKV7sboqfdjDEZWklT2642H
sZk0teZmcS4FXGkGprw5KPIIgSw/oJA1KY0cEhZvh61Kfv4OdNQ++B/2Phj9urIuizOO2lj0dQU1
CH7u5kedhHaxYlnMn6lxl7lRNbPRrNglhCh2FzG+gEyXStGkFpVLmWfqWekXxU7B02W/FLKiEGTM
IY19NF/gMrIPNdGK9xpm7+q4oJ8A9zBcdYnCu9sIeaxzXw7K6y/4O2Do8WIyYm0zZ/xB8W/zKnWy
RajajZtqRK7N/T7XRe+FHkH5HY8CFuIrjU+C0RXq5BPmdiZWto24DM/q/rGhKllhAcOGsTQ53guo
o+b/i4fk6c2dLN9M5NyJ/grtIXBEcZ2EAh/Z0BJBizJdpabgr2xfyYFGn+kuDCWxRE1QC1ITP2LI
6vRW7kKVaZQwJNfJgYg4ybxaU1VjwaaahLL1pANIfA+UTbhwRAqIRLfpZ+KiVA87ml21vzcSsOiw
bdHHEUBErbbNzD4TfnCvzHmEKwXqIuOPH3Vd2MYS3qZOyRguH5K3sKsH6+8dkiOTDmVL+/JTqibQ
DrRC6TvtZhkNSOSi7981m5rTVns7cZx6Eo9ufQF4G2Hqux3NZ7230YmBPOeZ42wNc68J4eA0tD3c
xcI1WhVhS+7VJ8CtffUGoz1Cjq8tnwYVVGsuF+Q58gChrf5oYVoHBtUXbdMgbxYkIBcIQoTTG/FU
8o5/xPG55VIGJT/U3wYdwvSEAmsePfQxiIgYYNZ9W9vVAHtr6e8vGwD1PBCmiprCi79XKR/p6IE3
Pp39yIGTRTqID28nT9MKFDK2w9PL0RcXhgEZ0lqLc7h354ombPysXHvHs9Fgs7CFTl6JspUdGxjh
0bwhxch/wphrmll/MhG5zaFhIJujLedLdeIeHsrqxsNgVkPC8yAT8fB0FnNWnqkLSRqDbambBhqD
CyyL53YiRMypdBjIpiXB/2PvY7fa/50lJq0vlAcaB6VKiOcJqPKmkFY4cdmwp1Qh3M1HgQw6l/qw
0H4xx0Lroe4y5dUQFhe3dl9nsf23y6rKiMAFBFM9bSACO+gxhFhtw+L+SQW5SPF4FwsXxXdpBenS
wYwqmkBUaO3i77wxtG3OC7RvDKhs7s9W+Nx/lBEGR1FrELzVcnn8Zvd6uK9r2gC/q+p2Ev1xIgqA
uSQahmoO4L0V+M3fLGP3wf9XWCdA0eMBeqOra3uV4/akSz65TLNUKEpofq4V1XzBVa+csTXoOjGT
d0lApX2c0gxZQNY67oD5wNgQGshMro4KwxNxRggPB7bz54DtLbdAGcMbLPMYeqVj6uORufXw8Skr
j9jupnLMlj/8GYRYcFtOtBwbgXmypQpk1zdkgjtTgqipjxTZ1UMt8pLn/ZGKAGYJ8apsJBW2+nek
VIYa7z1VNeLkzavf4dFl3RuqE2gE4jKE86F4/t7b9Gv48mgf51imEuH9VkjJ7Jf/phO0Ao5ImsVD
LH5htZsyAcyebcT91YzVrsiQ0ST8t29CSC1w81ztZw+iuS7nC9GzZhs+taI3miXNXfBQ2jRU07uB
WnU/6b/QEI0UCjLz+nk8fyJHIuEzy6zEsGjnuzvGljXwYyUqY7FeT8PfUzVNTYduuHyynhsgDDQd
3Z5Y/MN204e0O7tRRpfi3gZfcwYORmyMTSDoh6lCxX2p1DZJ2axtrKzsYfdogRSpi13+rat8EkwZ
2mdM77IOMnfUdDk+17fcD4HB9fHf7I0nCOdfMQCjdgSZDftWNFEFKL4tnRJVE02Kv7gemHIyhhtM
lfnj3koBwn3/61EOQxCQDFTeviqnPBy2A1492iVypMmSXocc9euxZ2fZj7lchIJxAk2kEth6EYmv
LgMfnrd8cTnQcVvaHijK3zrXc7zNeblSUTSN4yZcpl3d+/BNPbSIud9/pJF0kerY1g3s5dFVfmIN
D614Wg2fjknA/Jsl2prY32BbgJCyTjtLkEH5EInr531lDJNTo1tGiH0mh6VtYnp77ckTn6gYbs40
j/aqbVKKN7Z/FY4eToqgAv/naAvtwVNEijaCtLud4thPjqMe4Skk7pxIzfbf6sMbdXpjO0oDTkeP
WlyhlfgTWdQLxd60ma5miOOhItAHirFbENEBnNM9f6c8qoIgYlRRSu1aP9fdbhQFsCcp9hu3mCNs
kKC/w3r1sNrB0zCIbfWxnaeXQLyMHsVMSeiwqemKDiG7Hg1A6GlQuFjCAM+pbEH5SJXgUafBW01a
Lqjs0/avVXvQlI7Sxf1pFU3FKal0VarOIL1HkSrEkLIThAAOKqSjphJ7dDQPL4Fwth/CXzRdgQdn
s3UKBydJhhvTX0hzw1SF+KR0Z9ohSHp+JDEri3gD+F0CrsE24KnPBCNR1d1KZfiWINrDcR5dbOHj
Cwx9uic3+dtjwPRnwNuR4rRUYk+q6U3AaKY4a9GwdJriUrdnvlr8GxsM7IuhAUSM6pdVh7SD1nUB
tz5JWVKgbjGcXqQaqOlYSkmJTrqrdg1GGTjpoEwRTvCmJVCaIwkGNRB3p0m9KuAmVwWMXiSHQLjK
yBSH4NjD1qYowXHvglGGYgO0fVNEwDc94VWrsCM6n2kwdELatVDYJzyag1hRKTP0sZXfgyW3K50e
aauiNZdSHPuL9ypkLbL9eOSTmgJp4HA0U1rwqPMO5hayYUpjDngIDcR3eye3pFbHT5mA3LO63FYM
F1lVrcol1JLmmXJ9aqOSIZCVPuvNFNLa5pufucSZ+S6rxPv387kD5tCk+zFfccAKhxfCZZ/S8hyy
0ndADxbDBShAe+ozDU5Yjk2C4x66DGvpp9EiSvgR+39nn9cn+JckP1oc7J9jIxbfiYh+1ioL67H4
17jBoDjQsZdj/m/k7voY9LexAuMH2hSKASAR2V13dK8V+fu5/uwMoQTRlOUX3JZ5S3gt8keHfKvB
e67A38xSZBT2n7w9XjgDHs0Hjrq0is6scYvmXUwI2PFuaNXkMIaf+cpG53Ny8sA9TRToFs8aQxVJ
mL2cPD8ygGsH6JK5SF+adIc9pVyQOYX5Szgphgy1H+kxnVPj/egSfENOVf+unIUW4l8EC2bSoz2I
Qjnbx44oGBF08nevk9H3zRq4qjLp+5Qg+7VwZ1YnWEoJTeCHUUhWXF/ua2QMxW34JReKVhE/Z/fR
35YY8LHYPtx0QhAmMEJ4waa/X6ycQve4nr+xO2KI1zAX6PemM19BxvpPrZUQ61lFvcWn0bs6bhxd
Inlv/VfLZo1ONmXydqfwTi8p4DF4AM7iSlh/lc8ERdLdyFnDB7+RQMdTf9YkRcJa7ZvOPDnEDR/h
kD9KecGYOelagTVmaAhaqFyfPoj5JTILK25OjhT4RWfduJTyN8SoMKzesdt3OMSh2tpay3Q5NO8g
rlhyGX9g3Mqn9COBwxdRDQSQScqfTxfo9mNCDnN9EbVMs6MRThOGhQyty9WIsqY7B5/DBgWSCBWH
MQQWyijeEhFegAaBZjexDcraEHFDkqp1zjee5plRekBt0TNL9EvVcAIWtg3OHUX9VTTNeHyQzK1M
q1/tJ6K78I4cHRf7QwwBVGn6OxbVve5UfZSXa+ufp95OGA5zn9cbTs0tissRv0+2Cwg1qknSWdcE
yKXaDuuMLzJVqxi27Y27cp91zfVpf62YRkYdN4lCztezt+ckCM0GuPSrnLQwusBg/fAvlYgu9zij
WV/PtMMChncOtfMeUjQBpHVrl64dM00C0XCVrka24scJo7Kqji/s0NO5QHhshuKtOX+8cDJrKnMi
UDD82L70Qve4HqZHzfvTcBoB740037Sed3dwPQ6TIaGneLdVXYm+DFIEdVyvFcupKr+EF3a6S6Qh
1LQGQRMU74DVnVyJNI9imQGIDAC7N4ern3VJ1qyP39ZE9rDLrVV6SFCmw9v2dnBjYzR7mQT+mTOC
/T3LCEfzanCk45mYAcjPRnwJHz0z7m0XN6KYPcGhCAk2IoWhxZsrRxulBKznHtbjwusrWC9qCmc5
Fb+HXiCes3MiGSRex0Fip4ypN5cqquUqnZaT5ASDyBfAQXwP6UZUgDysVBMXLNzgEF1qPOagBbwU
ha9XD1D1pqEd7LipN4tOI0hzUTww6F8bItyIbEAMypSk3Z+XyybeDC4scs7DsHwi+MXKHFpaZ0f7
8KVOYa7u7Ig9wGk/7mIyKgiRtItQt9WkaPcDIcssnGQbwTWlGvz6O1/RxddZfPXvdKEGnVyZcUMN
PN+19aExZZjR1I1+R38o6NHpPMSSVUc7YzeTDjw8L9+CIzfCXVkuxRkYl0jJGZ0cMsYoGafG1sNq
yV3a47Xb46z0ILMvoiOIL+NJH1D4ycS8z/6NHm77u+eaFCicfkwcHYqcuXqg5odNm6CoHpSJrRWb
GjQTJJDkEUxV0fGcVTih4VfTxQg3j0BLjBVOh7dB2vP5d9ZcIXu+WgJnvlnvDytgagq8XlWWS0qb
H/whDT9Mwn8zZ8ADxhlqdVdrmWHNdVAovys0ISQnJ4pGBls7ZKt1PZqx6vxC+9ufVBewfnZCWjxj
Q7iwUHMrXXhesG9FeGaMsHAsrpwIc3ZSOXwc4BbAe8+L/m/wF/dogqDGrp6lpsP1xE4sHia6Snxu
2nA1DcCjNKfnLB1bNz7nyieKjkHy3QfXd6UHxem7cyTpws5G/iTIRGJiZgI8q5eJc5KCP1LmxAam
dO3gxnJ2KbzlFMWriebMqq3llyvgWQ5CFrbypB1tu6ZxRd7iyYBkmWUnz1ob1cp4Io6Mqahle85w
0eahr6iSvgedhjftt6hTubQUAbSV6XS8I7awY7cPY8ePt7Zp+Uxi+/HC3uMutTHU0JixtqozHWiQ
dGibrjyHAJHNbwD1B2YbKT1WMrIqwE5n7AmHVR2EHJKxonPPkQDYwPIA5qlEjHU9HACcyqGE2pno
SgRj8J7lEzhPAvn3fof3zY9t0CsdWF4WY0m26eKZf3DmBvQDDtkyN+d686EqIHEZBMwDTXH2v/QT
Gy2jrPNTbCT/WmvOmyJdz7mywN5q44kS+egMlmDOuiUx7vWPnlR2NLhGu92dlW/ttS/7dc2Js2tG
Sg8kVhO/OjQkxbJiaeOWSc4TmgkxOse7s6IQC0iTVRNPkKtcgrn55VttkADS3wjdnx/QChckUVz3
NIaFxVGKE1jsCkEINYClLziOKdEKfQFrVdeSd0hmlVAF8ftlYXDb6IaCXk9HDBcRNxdK95gAfwg4
3S94A2JbRGnGyrLKPoFDthoLn8MKSM+SnXN6uweqsyhXp6cNIK/aRGvyZx4oo5iYfq+IUambLMtN
liQJlsbu+LQlnIU//piL8R7wMAgGNgeN+FKDU+Xu17vKaxj1W0sMbT96uKrEdRen/v/5onG/Sgd7
M6npXAqj4O6uuGCtR/ywyZ1PpFICNvQI52Q9gbojbP1eFAY5Z2NIxO/V4dLNyEL5MgDz3kr8lnwM
jqkju8ZkZTt3a5XZ2Y5sq8NmKr86o+SJ5hsALDeuXT5+Xc+oUhsVEbl79xqrLTNsGNlokN4vQJq5
8t4MwU6sajgDLOqlbhkIfOavXO5Rqf+F+HycBTNW6ccml65xD4A9GpJxFXwlE5YUzlzoSlUFSYLy
03fGseToVg1yOrq+/WWUeNyEYuxM3PeO8IEM6ND+yW3/ueutSgtlihh/rHc1pDvdJ+acr/rhHRtC
nZWCvGQ0op971iXUjw2D77R5JEkpqYGOlRT7dIKtZy439EhHGchflx29uvstUs6Skc4waLVN+gGi
dwd4U73I11EfwhvB55q8nbzilI90sqUdif5wS5H68Z7uwo0FPqU9Ff9p67r0gn7VeEwxd6pBWYal
wIaRFkYKUUvkVIJMfxHkxGWYThY+9RTsjJ/DJl7p2cA7vQsQMSzXjBWf1Y3qSi7FecuLumabipCd
RLj79eDZm4Qk35OqbmRGR3u1sg8bJ2gn7v9/R0kllGkpppJW9Fn4PFQQ4WvQ415+ycz7+Gq+iyh8
Bx895J9pL9+Y4r/myyrwoh7i17qRuV25Kq9vLTnSaAf6NCDNvMcOXsTN+hW/FOd4ZV46xmokzIZy
TYwcVXEtZ8ZVMAAi9KlOEj56iQfhe3TeD7j6hE2gYHfzNqVSuaA3WrHt8/6LqmhVGw2bmuX16kcy
sD+jIvlz8bejJis72AqTvinEwWzx+++C/AdNvPwjmv0HntTmJ9s68OU/2yuY/QgaBWC4X28MF9BM
ZgbQVBZ1CpSGXy2BAk58rMk6YTlhI2e7pAb+Vc6wWL8nbvMyj4IKux302ZSRFSva85crtBzMOMvV
Y++CKPwocVTmZ2RiKmMuCKpEJWDA32Ai7kzfu4reT4YjV3TimCTGgOultQmamZmgaKPyOBmABFKr
lZNCtVzO9SMCBLVLLFrhJcAQVxu6mH/vYW7j0Okit2tmpoBacixmOJT52OAzd67vxC/Eh2qgZS5W
zM34TFjpeddFg5NYDNr6r7XWzqqzrNaYOmt3lB/mpQDSLjbhmD+2nQRPPsYNHek0f2BV8J+5r++Z
S7+rA+88S0a4cilyCxWqKIBLv7oDi5yzPopAffKA09oQEg2agpjyJ0ICd8+mym7671YRfLGFZ7GB
CF8avopd5X9bA+6mW3bWvdHWtlEyP6Hsz4a6OwzfxgohsTiWiPH+p88KgWQd0STLsf9HC7CbWOjG
iUGsEWF0JsC21nhkIhx6k/IEDQRvAQ1q7YygTal21xNzwVktX85F2xzDpDGal2IjCHfS5OecYT7M
2SIPxyk1itzvYHjFYxZjF0DDdjaJW10RWGKLdba6mHZ18Es1N0eUn58JFZIRiNQtFzrh+QKFCnRo
HmBsOjt7HVlWyDnaiOdNOw+z2K47H4Ds8ihR7Ut2i+2gdkT8rOjS7CLnwzVvK9TByLEvckhK9FBs
ebUp+Or1CaF029RMZ+PcOITAcyc+b/Lm23xtcWay3gckhFMn78twJqH8plXt3I1ldtd1lMjgB1cf
jNpaC20BBX0NFNYPg0FfILfZUuG31aDe6DP65iFdCSKdpAqnB5GJo4R8MdQamLVclaKeXK1LPWpT
hhKYrHzwVUdQhjZGIUQQU3/+QV4pUyvzGU5lTX2PqeBzemk4aFH1R+Y4mCeRZHrk8n70un7bfpib
n8OB4dd8lhJ3/ACZwvTzt5YXYBvOmCPpAMChH9s/rO+oPWlzcfKvB1NQyapaYNxOeDVNr7/AalFL
v0toXOc8U2xeKA0iLs8cRjchQS2RSDv7jeLf6CD6+Wj+nq6KXx6MF+XbyVSss7OkdVd5D5mBNu8N
4zrJHeERT+6uTvWo/qqXeqTI+kZA3iyUMIQg6YCpivXQemmO+TVY9od3bo6LvqkvqYAIaY61ZWD+
pBgN5kyLUGQtw9YuGGtG/LOYG2WeU93earbgXPCUFrs5i5jdCEKsAeuYS2Fka4lrvOdfZjp1xjVh
1Nu4cW/kdfMO7TTto7xgvjO7yJV5+fIRWpO3oWKk7G5H3Z+6bJbDpzQ8QoYQjwj1x0x4q1G9e40h
ZlhK3ApFe2jh3tePx0Xdqc0NQqOJ6yqlVR0eqjP/07US/f6SzHLnPPgPkD6mPFNP3f5KSWK0bzkT
3MLT15cqs9A4+z2Jw/REHPoUM4OnMAJlZOxRY6BGJeKSFK9v94BAYHAMAoNilzWIMxU+TmVKDvpB
I7m+RBxLzYsjPHNpueGQRF1jHc/zVgAD632A4bKRgzARv3jT2O8GpoVuSuvf4lWAcjutjQ57/jZh
Cu306aD0zovKQAGsNm5pW+uMT/mev9i3tQIA6Dvpcn2X3EFxwN2wnmza4FK2CH4plguZHQAT4t95
VClsW45H5nEPWvEmU2+vr/U3KWsV67t6tfaXTCRVOhm8VZstv2csKnn0BMDYv/VmiepCiiWhzZx1
2VVj3z1CKSXWuQWirqla56wp2XDbibw0CGSNMggDnoZCvqkeB/tpy+P/Zy76sUDmPsetF0c+podT
rzf/XPZfABF6gXyxhm5pG89NUSHPmVLsXQfz+bG71d6D4G3Vd+s5QD1qcKMH4grjUWTwWnuGKoWA
jIP+Lco+pTbVtaL92RhVwEniMeeqaXohC3VLAHUvXhbuq7ap5Vo3cp9sXAGkYelkARyAZYQU2ViG
8FdPJiW7XJNo6ckx3qKojepJyBMz1pSKMZhZLHl9Ht7BybnsW7NhBQbBG+MFKecrUz+kcVqKcce+
y9zYZgjOBYi5qKT9jxnPk8eDIV+M8nMMTmXvQg4aADjrsMxucqtKeRU1zZxzeUHjes6nJ9f3ghCs
TsEtsjgbDx6m4G0j/ToN/ANf/9dVxbuhQ/51TBoguKX/Vx99K8hkWre+KB8QSNrq9mWMuQXCk1Dq
/+APM0bShLyOoFXYmdvKFTlWBbwxFrGnmEZoTjsVGi+TTJbaeVjn5pTHABOSiDrcoBGR9o1tXl5B
iEG7L8CiTlVupgrwg5BSvB+XwrxGdBdmHF5DxLEhoHptjNkNHUaJI0mzRrvoW730SUhnbA5/qlhK
DsvJJxS3walFf+l/rU4mdNJ4Cqt6Tajf5rpWyhMFbGWraFeY7wdSQ9IUBL4irvQYO74EwaKmjlq+
cAsx0oFKK/NMffAhkMZ4rWgvNHUG+2Nrr+FiNNCuqJs8tTSIJaXsA55KU9Opx31f1vJtt+BrePZk
QeM0lIdUqMhiBplEPwTqXVcoddKAEu6cBmrIi4GUJ6lha3lE1la94iToKAr3ABDA4KH48iNgwv5w
oQuik4ZIFjP3qDxLwFLES2WUuJ/4c2DZW92k7kyuCUzehPhki2ppNCQvkGKsT7tQDO59xpW4TrWe
gXnUd2eniQVAtKydDLCJpFOz6h1PVi1jIByqOQZfGmfQpy8fjB9oVl7PzOT//IcNj5vkOZGRKK6f
Odbrsl00doyjkYHtOZwRFfrIdpYuLYnwPnDtcu3XXYu/J/6z3RDE2sTh6zlSSr/j6pwBziw5tOqT
ZzKkr3E2jXqQmOr8dRNmoPo7DlkLe27KVYrosLdIIfDZ0o+35NsS6LhIo2+UYjsiuZ4upqW1cj30
61XsyIWa7zDWQnD8cOwITpHdVF+H9tN+FQiGwaMrUIOeyEnMrAK29nVN8o77gg8XB2658XIeHdcj
9/PJibo9smLU77iI2i0My6sZezZs2z+Scs1PmajJH6HTiKXKahqaWiSY4i2H743kizrCd2yXQWgW
xsXViWmfB9DpWSO91K/DxxUxTZZYueEccBDH2FhNvnOnW7cLO3EltBf4TGwsfDPVW99T2thfQJGF
lKNRDb5hD4jvlvUcw2DrY+ucqVThEC+yW+qY4q90NuFisH0SEmcVqFz9tbEurP+SNM33nZGssXQH
TtvcuuG+r+LTW9d01P/0Kpgb6qGlsAF5ftfCaotKJS1Tmyc6zZNNPNn7PB3uKPf0QAuDyW/WsZTo
+bhnfvu+EuMBSuY4D+tECB4Eo9GffQF2Xyb2p1HjnQMt382zVqxmOS5LUEC3J02ISsYlViEMKQvQ
w471lnademsOCmWM+ZeAQ/LzftmmQSrjORZjagy8X1lPSJ76n5r5fs874FoBXAo0xmxKWkhbofnx
Z3QB7t3yfQPGYpQtn0+avBDmmdCVvXu793CE80RidqwpVZa7FpUhJCAlrs3ZXzmsUn53B1JAw5HA
qYzyVBs9h46+GAx7RiWMfBQydMSrgXzHhXMZXsnCu1pUpqRuKa+LEMaIN9+H8+Kwb0ZTrljehtLw
rc65dSjVK8F3pr28mqqHP+VKe2UIfdEgtIaOjBSpxYwVoZdsrMfHP2Pahr/nv2oMwaAgSxJpCAuo
7P+EqyDu3NkfHf4R9Sug/pe5r5n4Ijd4p5cwS8FRlEBrWNFY2iz4+GGMmotuQCFHKF826TXSr+fO
HM2icQHcvdFco40IDDrf7Cg2XYVyoxrcARZQvlfVXjUx6p1VyU9jKIqTp3KUsv+anUrF2bc4UeNd
rdzgC0Y75V3L3kOLEzIYQeBnpIWz7uTkI5da6+KdNml9kgpLQCfECY0tnmVk8jmnT39PHCj4F2CT
1hzn8T3bwByVEIVvLwJTq11mQMia3ls5YLwafZ711/s+QOrKpUDdvYgSYCtAhVI3G8ieQRkA67fI
0RmkAkxgz7CTqFJxQGchUohPJ4rZ7SM5Q71CJ5MM/DrOdKwVjgJy1rLHs0MfuXZLFvHiEfl/tS0O
arZYwVCHt4CwlHeYQv6GZL6lFtq/C7UHI/P+OQQZxUi31sKKyNmhQOWhxa+9AFgThorQuit9RhH0
3gZgRTy/lVWTueJ2uyqgxmJOFVruVjAz+fQa/pTK99OnZRzRI/BO4UOnVpnB3r/Npg0Kg9R/Zf8V
noju4/2Ffcd4rrlbrSeRXikQFwzHc3q6TeDJRCEm0Sr4tPtlCx+1MtepgFUvXFg2vF2WklSv9CFL
9PL+y1Ud+yAKsHx5yLROfYYte7tXzJ4lFzRpsDpuwK7DnX0pM1B2kcqcvkHkProj+7mf44AUhHrZ
DW38nfoVixGbJ6NvDFZaghBjECiJlIe1F9LE/GiYrg5BPzdHlr7YDmDnTMRHwuPwNi+ahC6a6F6/
knFOVkafBvvBUYDUIpfGa5UQhsvclbNfzOEWTfrH3ouK4KeSpCYySOH/MjTZ5ULc5bxGkiBUacsH
zjygaaehXuyEjptKfocuDb8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
