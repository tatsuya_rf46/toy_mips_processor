// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_3_c_shift_ram_9_2 -prefix
//               design_3_c_shift_ram_9_2_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_3_c_shift_ram_9_2
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_9_2_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_3_c_shift_ram_9_2_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_3_c_shift_ram_9_2_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
qPFra3zbUbCSk8HDLmITTydeSaMkfOVyZn9WwMeFqPJwxtWXA822jPFFuOe1jSmXTF6qvWE7ljhN
RgcapsWig8lkzra2q5eWZM3QlWyhyLX0ZOGRe9YYp0r6eNaL0P20T+6bGwR7Rg08Z8+SInFVEH1i
ucH+2RmEf4Ixz2ScL3Pz8Vj1+AZqi/W8raBLbKmZIGX3a81yll1WwEKIFJ8ZNA0zyqTVfbzujOip
DUXqrYzqTx/0wOj1Ou1ExNOs0N0HHrOAvHJbAis/DAgfjLxqph/x2Rzg0dorgXYgTD2MzTFDN05y
4RMC784uKACr90ESDDGCRsCokRTVjAZ5V4ZXZiza/AeTnOmhc4gtgLfC3MhMV4tcGPsfGvUeIIL7
CRJ/P+dQ8BDSMgeAFgsdGE4v29maAa84KfoZ/jV3DW2i7a4MnBA56DkQcg+4+L83rhsbL/Bh5kkx
jlwRJMHT3VlWoWWqPNuji9/i6T2MqRQ1+hEGUi3JsK0yxe+y/m8geN7GU/IL9Za5YD4jBt4ND8vr
o5sdOnKbGLZ9IaZKVEedKZR4ACcvVZkt/+c3frIwrOd5xirNh0w7+ckFMM8NIdI1y2xD2Xd5TWYx
bGzq1X1CaJvZBgB7OPKIYIdRbYmJjBchgtBnh4Xrf77EsPHDU66sFnUOsZV47lEhbGIlAMYpFLOx
jXyK4QQ3GL6V0qlrFymc51FhsT3rLP4U3q28dWpSFKqINSxpsmKNRgIh5GxUGsEwx9FZ3kijtlR0
fz4dcqwxBITcbKToSotZ6H0pyhgJBunTODxhkF5CuQh7P3Qw8X4oa0JQIzEQH5BBfVl87Z52i6D8
kbLBMO4cdUe4G/nzSbbskvkT4gjkvGwaFbbBtNg8g5u73mCjKzogNBqERduomb9BfFqUIywuM5dC
F4rGWRmxaBA3MtorPTWRi+QRd9ZSCeAHDHCoEMhidSVcYWEjCT8hIBSy6rUOLXYLSgftbqzH7F0T
kIzqQwFIeLiTE2/E0VL/2ul8579dTNLVBtIAxOeITTY+Z40RHc/NiCwDfwBkmg3R2cwBfLcAvtCN
dJ8m1FZ1JPR1jraA145z4go9UaLrsVec59of/7blGHTukRR6UkOcDwk6I/iPdeuKy37LMa8eE1dF
07ojNm7p5Q8SxoP6ovY9TE4jXZvKoT5B5clqRPfgTp9EcMysQhkeyEjlj5Fsd7iS0aku3bGZWvTF
GCj5XEym5StV6zEiSQ10ICXlgtksv8YDPFK1d45uNTYkVhT7KEA8gUTxl/S2CkIa+LzsqXyO6wd+
PdovMx2AKiNEaoqEaH3iXlarhs35/1Gp6fLn2Jd4FDac5HUU72TzHnwjDNbbNZMQKZ+0in47ZPlD
i9wA6htMGJZo6N/zVrHMQDqvIYizTWeNyic88LYi3UFI3yKjHKiZgGd4jXy/hBYkmZBTUL6ga6v8
eFX1wrp6eU/Xv2uRi0OIq1WyLsvMcZXXCs+nR8Q1BabFlhTvZ+UsYJzG1v80YgEUcWxzDRFXgVvZ
SXE5s8tt1dFIxRgUXPKUegq/fCQ92C1qb/Vas4YktTvK9VYSiBlNYR3M9L+2B7+8kxgCG28TdmXP
cydhZICr9/o2t0M7zzgY2WKNIIC0iOo/7KOrpRMLtRUie0lD3mjMHSifX5EnJSlHSiPsMXBYJw1p
X7MKg4qjoko24+Qm8X/o4sZCMYEC2FR8J/Xe9a0ovcXRyTXi2BHbH6ThFgWs2Hv6fIUFmy+rTvU6
6w2yuWVO0WvGKyyMnXz50X1qnNjZZw0ZRayQk1qt5Ka9e629KWPbSYraxJcwYrMGMSHXk2Q1mWEw
Tz92pmi8w6QbaahlLljmquLIN9I2TPqMi5gsWr9cHVwUhNW3g6oPFYZ5+eUVdUkIZVE5r5MXjMNH
QvEoLEUjioPdvQfA++Nk8BlbAcmEyQtTEysbx9B9diUJCbQmqhLlqopKHck3g8NuMoOKgiGx3h3C
HGocZb28D4dBz3jrQzvxWmmBNXF5wmTWyLTzYx+i7z2qFxtoz3receiUlngDuwYJ173BT0Mo0ONB
m7zIFBKQWwIhNE+JnZ0pmcb/L8lX7bdY1169iO6YE6hycbgTQMQQvZzA3451o8v0s2cAsV7Met/I
iC+Ag7YW0jTw89MVihoVqR62LpAyI1rItrW2QC1D8e7cnClle/aStZeqWzK+AWsVjzHEl8LHktsh
oZfnNIlqjouZ7mWrL+gVb/g8tBoHq/TKHWMotzpO40uCyhFp/I5UOWv1YcKbUrj9obP3KUTKoMkG
AlPoQYgKukYVO8qRFmMWzvUiziqRNfaYteXdBOaFsWa/kBaZFWCZOX6vZmvU33Z967I2GlSMjWzH
vOIZAAzYpXbuZXDN1O9MUstJf3s6K/dVcOd+OQyPS6JWvxP5HpIbjOEQL0RqIiJro12of3rnHoOG
90ZxKVDqpRCxWnauMElTOpGu5G7auZIPVV7NdfUXOjhf4dhx5e0rJeevHJIbFOz4NFCVb5w1jPyN
szB97sXpop4SOrG70BRifYtvWJidxEJe5dyqYOyxJYsa2wZDWBtNbsF6j+q/N0PKv2NwDi0TE+z0
w1xpVG6EO/QUfdNf+gMVOBEkdRZEb+PCb+x8uuQng0snoPJX0R1kLBahn1q8IJUFM5U+2wP+E8dq
l1WzJysn9w4Qn1k2cwVGndIuaDLmiINEICLPWcN2Tk0Wi1d/No9a0iWRjwR0py2clJ/u26onYmi9
Y5wt518V+O/O7Ms6eJXrvurSudFBjIG+houxUt2SLBM4Rz3MyKWMlkN4SLmnWtOm8loEkMU0UMnt
R7e3JPy7NHihruP+lWvwc7SaOYEbpas2NHpnXI69H3IQvQQGOtojO3qstlMoGNZTVTAqc+pQW/8j
wtwh4Bx8b1uqPrcz0qKYXRSwnNLGTns8AcTOoXwfiohnHmHD1PP1wzsGuSsUBysrPePITE2TVVVx
2Lbncjhm3TvRqkaYqO3IzGehtVtFHAZHNqE3McjZQe/OV1Wo++S5I1C+ZZ6ncne9wasJYYG+UtTo
HTiOFTyH7aNOPwkeehrj3ddx6edtcGx6zXfyHdjuOB007hml5g0DwR2/+ouivXgtnSvSWs692G01
Imvf1IY8CCug0utUP/94UWmbtWw94rRIOkeDSfaqWpeixXiFen3SCl3G2g0aO6cxwH+oOD2Exl1G
11rxUTlZNAdjsBjfBBR1Miv1vKTk9YpFWX17tK33sCVPYsouYxac0fJo/t7IaBp9cJa87eF4A2iS
jzKm/yBiMJvF3xRgR4J04D6DiDd9daosgW0TbzmNfbUvMONnB5chXs/i87YyZXA4FPB1lymoBfGE
n2XG9rL/sDWh4xXWl+U9EqvV8tGn153F2sQN4G4YWhYal0ngOePef8QiVgDcgndd14B3UwS2Algw
mCF4Ba4eEu8pmdvaJk9vml34f/Hx/HFfbadPBfnd1hCVHothpMjmLw+nv/BYLqF59Nq+vEYUl7ED
akhqiyeBuBnRfmk25pj/EwmOFq8qI59cR17XRt1zcSJO/rk4FA3CCTWnX71FEcNJr7wCfk7DBrPe
vshkxNRYFgmRuUSnJIr9wT+gzfgAApylMP1BdzhiiVG0U5LroH6u2G9TizA2fbbFNIXFrXFrrL6l
3KJTy2KX/OgoTbsYwS02+jCeNPU2DXzd7Wc3fRQPzPcUCg9zLV8IJgmQjKmeOUWmaYckbSMgQuL8
cPmSPFZGAh0nFbg2Mo+QPE0nKU4Oa41vDYfL8WD/nyyb1axKNaMqqUA1YVbncVG9wkaoGTxgeZWv
z41yrlT1K20cp8OISiwPwTXRTplmjsS6b6sS++fYBZZE1TjgxA92LKowNQaFaD0IfpU41iwQeT97
FOCtcqTQ+xUQGELWo5RAMSeQLVslBBqDgsOKqjF66aNjgmEMYNmOUtYbRowBr/TabbzAeCbc835Y
mKPBg1cKKA0B3MqVXWs3HwHl+u5dq4aI7BgR9205ZJSBjm391Sut4hLvUZI7OlrHOozwECIBO/rK
Y6YoatDeNEdnAQ8HoCeFLloT4uFyy8fzYDphg6nWj/Cb6XfnVgsy7xFf4JF2A+g0K7JmFYTZ58RT
vIESdXEsCwHGKpBPpCiB4m5EbKLzw+BFNrjxyOwhEWn/2T8bKwKksg2lBkhGUwZhhJh7ijLT++YP
XX66cFMRGUfq2buc+SXKjQoOZqi/NbgWlBf9rsi1WkeWs7x/fefWh0T9QQNNVujP8u/sTsAc481Z
hVwpnqogx6NQiE6dZwC1C82ZW+BY8HP6hRG5j36m1pd103YxR6X403uuHRik/MTqKLLYvXaKWjIX
QSrDUYi9KVuemJNErfBpRzNi2P7HtRCqiSaaHme386d11N7DF4xMP4xEGA9McpTyJl3XnaXs7sNz
zyCsg8kZByTJVrpWpsOJ1x1o1lfn8Z63DRdWewzqCUqFIvu+krarUmyb4X1+Zc96gepZuY0ZX5eH
LJ4gck6zQrt0fzmzGALjEQJ5vnsO3WNujHHib2xyhhPdSefN38CFh6xqIAOWVBWGEUOOCcbfMDQx
DcF8KrSWF0TBiBsSSndyigpGQ1MUAyNrxgvpLHhETX34dT/mu7x3ILNwUTq4UsxU5vaOie4FdcYT
AoQPhmP7nKGXIu8e+iu8FzQINdxz0qOeQjJIFGXgc27VjTGuMYBAgyByt7a1Z/ZSrcpRXaFiEtBR
mTaKbUjcwuG3ByMuXeDbkhEwvyU+FlePFlSXki/TIWjF0biIs/Fd2EhkU9S+tD6vcSuiHkpNqijm
IJjuxfbY01remCLnMoC5sDoY00wNski1cOSBvqTsneyZgLOVy3+MDBPIHz46lRXBoAMGv7v0RXTk
ifdg9qT2+n7ZSVHErAkMNaAcLjKmblPGMA88xKwLjzpfJrBQ7eWSjXq0tPDE3qHsVP6aZYo+uaxO
c2zpkb00gRU0aPR78HoR5S3kJd3wXJqPd2LIRcAvp2/5YuqMg1Db8AVPBH+XEZuNanlS6262XL89
VqJsaWvJ9aDBY/4PAyp3d+jYD5vqTk9+KYWIvZPAExEnD5f2QY0OAv/Xuf8HiCpQ74Atnyiex1fd
IHjYb8hUnnfeEzzPQNz+5u5u8gBM2OQdSnIRopaPHW2ek4DL8FjHbIRG35a31qkyFmiMQGXw/f1Z
o6yoNlZfJTa0HFApeNcg5s7+2QPTEOI6iMlLveNoeLfp02VqtRUkgDDuCb2bF2ZFaVIOVttX+KA4
6ka+1dvv/rgPK/Gb8ahXOk6PdqgCzZmfvlZNjfzAzHyJLCJDbnMWInOBucAWhmKVLw3AKb07qoDR
YAnV4OG6aHQpI2Ge5KllMFJ0vkqBrDv6zHqj6w5mpYTHUBbEhAEhsT8YzLgcwkziEJys58Yl1VKH
2ukP/Z8weRGXWVH0/hT5B9+cfnJmirpYs6IRJ3oXx9YRSAby2gDq6EqHE+JYi5XiKYhEih62U8od
eiAgaUANPou14ekuyENAVwjWy0nccrEJ8YyFmowPgJ1pgFB7eI4nIpXYBTUEfp2oqLN84F4L1fwd
b6f47gvSiw89e89XYj6PRTP2xfkDQ4OLM/Ga+r1F17xQJOZFfXKaOsRQUGR8Z8xzbHYmSVpAPsUr
ZQvFN7WuRnsIA9atBW8qOeN7B0RlN5CENkYdHMgyPRz402ZqK7ipggXciOGhsYaqsqk3FsXEqWOd
s2VfFL1HeMsg83siChoLbgInWkxLZxCGZqCvgr5vJ6t3ytWxkLeSqrSSJ2Xl9rNg+TYcnLoujaiB
CY43enb3Igj2g8ypyVha34eT/ZhkvTUnNRWeVUut/Y5MFFvBAfNdaQuofXSAqoeeWT9ez4gvSfu1
d4xPfzivstakeO5Ot6GwIS5lAeLlwquhjix+ox1skXj6uHBaqE95aORogMvNa98ZtEAFFQc+qbr+
9uIJ/btE6VEhvPqJ8Qjwlx2V2r9C94JCPMNYracgs8fRmGu2uef+9LObCDhDbCjPb3QEXQ3MovSE
zRsr6nE7x6rTzIM5tLnngR5NZnhw3A500l7hsWwjbeQ+P6VoHmElkQYIlGHqsasw6ixfcu5OnNWl
Y2vA+VCo6xwuljEEe1R3hP6lTPpCcaPPcHaOnBP9ncqjWit5s0rAM5ueIgxzsIkxCr17UNV8SL4N
M9H7Z+lm4WjyncR3TzdRNNyAfhaBsqGcX2wyaHNA/cJAqxIHvjFwdxTt7UBUGNxtX+9iITkmVuLI
kORe2mKNGK7H4kHdqvhImnz9l6C/1MY9V0AyMjL1or4xBMfRl5NxAcukYrqkDwvjXPXbnCWVOiW4
yVXWwTV4PoMcgMZuettneeMjrAxHj5X2c0a3NQ37mkz+u0EJg2X7FAXaB4KsvYnddUngA271Ms6s
rIRYXImfil0/LoO7EjbAvLQtOtNC8No1R/zzpbQKf8TYC+LWNpgk0QZejVIVkwW/ZDcyU99kUeEU
AKvYteC7bTFwjnW2tIfK2RgQdIryQ1r7yBF31Ix8wjY6OW825NmSaDqNu3wJtWUXvb/BLp13MJWu
BiyG4MzWV9oQ9yVHH4OtRSHX/BxFyYQI3ZKcwxpNDh4LsIbwgvCvhT1Tg9TznidWpCG7lVe+MJfR
3fhlrz7Qesh8r9tNewJGoCKw9is7qcGoBQw273SvkwiAI3Xfpog6QTgwN3yzeWdpeYOqVCa3csvI
Y/6yYw0Kt7bB+C/km2+evTRjv4FsexN91VC5me8IfcQ9j8HkT+Ralzyi/GpWuo/XBkzwl3Eadprm
bjjH+ypw+RhDOMh+FVOgwhiypgRMUqX2fhAtbYzs+lapmH7D+G9QsduR6Ebtq7cgVVrLcCisvXYN
oH0VTGf0xvdTP1FzFKItifOyDq2wy/0nc9OFBlQRIR0oFNiEt6bG9zj9M1umArn4dQCgqAdUNkTq
fefzh/y9/iB7R7l2inJ4jpqZ8DEXIMpN8ufjtN9LveaMzn7GPOBnNB8fbkIHcmzkpyAPsxPupEAo
h9rlIJJIAg7eJidOqkosbP5vraNu+OStvrfzGzynJ9hMe+Is0kCtIUYJuR5HHrwxHdckXfZun7I8
uuiNGeDO3Te6rCI3q0HmNwHvp+9MPVLPGJsXM5+DmV/gah+Cj6xzb4OaeOd0FCMeGUXdSO0VoFr1
PAm77aRyEID5/lQ01GlXzi0ZVbGMZADDG68joKol2s3s2B+D4mEbjTqVbf6mP5p+3CLODL36Cxi2
VOHq94F8SiT2q4VjgC6UBFIgy/JUkaTjQ98r9cH9+f7V+ikvTHaM6m1bS8edIhLEx867E4J13C3T
evznxXel+cU4D+XACtlpxKekFkFJ5OBcfZ26n8mWJSuifGVJlD6qFvIKbL1zYgbQFbBh5Pv4rmoZ
N1rkRxHD3SrSTthCtC/bHxp6OOgQSOEcIegG/lFg7SBFT5n3/01TJOBEUjf3Dvzi7iMs6FQxmPYG
Qb/lX5tylHFXehHqfBfJFd7k8+Ol7FFR/yOvAW5G4Ft5Vd2tM970s++Z5YeduOpJR8WJAfk2OdyD
7X/xQmBVi+peoCCBEnxPohjSJc+HQ51rTXnr9CQdUg99xiikWKuXpB/gnWHbyc3PrOuxoPX3W0qo
Gt1+2GvWBia+//7+rFWzoArnPERKsSXxBepDsFVC6zCNLyIuZq5YQhtEG11o1/DTioNPNti3ccqk
MZX7+0nDQosrqY7WyJI3hL/rPfc87uNiSV3kvUiwYyyZVa2op+sKlOGXWn/DWu4NSSFJcDT0meaf
cyXAAJE4FjjZcbVLkwJirmR99pyQxy2SOGHeMnu4Wx1aNwcoZwY/7jX1tt83VtZmrek+yPh3gE8S
AniAw5J3SWBSIbvl/X7xPgJUjsL3Bjh3FpRF1xiJW+xOCT5QGfX11ESjsgTbbPnomKt5eIUJgtNV
hE39biyK79nt+ooN+sozoN380vzRXkwYgQdThRzoAARLbTnYB/QcAxoJcen8MF456Nq1CWsW9Kum
jQcXVS2jhu7O+LQAEEsSOHnBh/Za1uCxHXHaHs3BuPsKKISuQqHGUCqVpSTeMxLPleLKsJ36Jgzf
1WSqJJgV+hzhwWYPJh9aOGiOhxF2e7OK/ykEVWZBNxb3jOQZO3/VWz2uxHQw5HdB8bjU8KwN7FVk
CHr6DRGzJWHRsJ9FAD6UAVZy4Zlku1uo1kjbhjUHycOWB5K5P7u33LtWoLQxaSZnr6buowkybOmC
NKQ6QZ2V3oryEdPjRKYp7JDKlVOWkAQRNhL61zwwVwyO8x9jLCikBAxL1bzdofPhe8zgoaSs92cY
90QWsyZjcJ+OzZYRurfNNyy5jSTSzQCu52eufvKp97F/LPAQLlZrwllheflkya5dW5uJytDdb46j
7xST9IiLArzYWZDac8z00xIACDSAVxF5ONXkPUvKBKSP69XwcPOJ6Thg+aOhZufnn33LGLgPsLTM
cQ61F91Qfu0TRbV3tckfJpaWeOjSeWaqPPo1l4GcVvTjT0tbb5S8oe8j8TospH9eKvRehDRYh2zC
DI6+SGpsE8JiLiG2LFWkeppPkn7tjb9afQ40Zrg5o4wyWqhgFndg8+IKBL9A6LOhhf8EB7sfWnIo
ZYH+4tZHzZ7zINK7pEhAUsbbfLlJNTbPFOGHg4Qjp8ntKN0Bat6nYh3j1z6pWxpgdjSQp4jir3QC
opjEfCyhcCNdLgMApaK+Z2axvZzT643GOivESRhaeT19qUNRWUsa59fcxLA8ULn4agu25Ct1/1X5
Q5jQZzaMswHW0CdJoSvkXNERXYc7Do/v+bpnW6ohPOjuXx2g36xwiOPbbhJryNnQEbFQQdvF1/J6
daO4csyEbr3tgNIKI3wGX1Vhjct16zMq59pWf+Iv5Nl9nU35IKJai+DA6KD107ANiHofuzvs/6nm
SZIX25EcvnWQvxmXBb/v9EdEQmqSVroo5dQKkKJV06qX3sR5Ps95NEP7XSI3i8CbMzyK0qNxzd93
9UKDPSdMT+w23tnTaz4QtVLeuxsdvKqKQY6+YQNADYYD265m5mHDrLJ0LsboGb15kUTHPphE32Uf
r0x6zt//zTtDLKGWVVkIpYHW4/bVCJCpE70wDs5lkqZIsZFyW3iEzh2ZUj14D6EyiGeWbIO7Fqtu
DcQ+JzbtP4GLaoEn6iPQYQ83fop2M1rtAE6BaSFxro/wcMNsowAQJS83vl7jFHGi0NJFdNDFSvYe
mAyAbadjXcLz/2y057LoZFgMxeN9LKJcDv+8KcytzWwPOxw4/Z+5zi6ZA6IVLYKDIhK49RzoHgkC
UfWtfc87x5l13hapUxF/jtRCFlGnLBr7kJ6ltYg50UzD0uI8Sit/YTePG44Wr+Abm7X4Uxoq7+I/
mrvhbVYqyidPfPIKpA8ONK/Dh5/hEAd9dUCR75kiHXC0M2KBO+EmPQ+z86XUWetEitrgxs8esb11
11SOM254gv/Vlm6wCLhZOvuPTGHUJdpriY2RCNI1ioaMlhq73ZqBNNRPyJNzcP/LKj7W6iSPgpJo
RT4xjvwRa1CHaFTK6QXsZG1cQn7ZwnIuneN3IPlGkY42wthGNEm8jnDmGuWGokd/XJJryER6Ukxc
LB2nIHOexpz3P6NWmD1N9fqB5mOXnlsJFZDU4vYODuCZHPSRXBMvItWGmXYq4Z3bRHfOg/+QhVa1
3rdaBW71Kf3Je7/QK9SbY80qahSQbrtn7zgsHNtcxCtTn7aGNwAfHOS6iCg++reo88bF3w845KGE
TFaS3ieRXE1voc6a09oVodp4iydHbFyioMSbEHKV+0TUwIs0mnOjVc6TAzUkaNum5vWT5f4rR5t/
L3r0cLxnk9DDmAAsIc9vkLPSRjxfnoPOlF4tiShPpBAaLk89mx50WKXDLfZuM5oRc/KuLuey2B/t
L47yyTNT+cu+cN8tpnJ/u8ArcoJD4ORJOrXtzADrLpL0PiZ+q0Gyo0IS3ZhSN8VZ/O5LMZjiy4/Y
PNsoV7fihLcQHpRgj71WqpNg2qFrnpk4il2tOVYm0uzCvGFbSw1qD3JwQyJXZw7ZLZUwYb8UBs5S
R6AQCPGISljwunuE5ovFBKiM4+OPhpVoHndxW69bBQ7a8xzgf4uqwMlj17we4I+wRAutGAgvXe1N
K8FqyMXUHfpNhur0YjDZNSk+iI3BEkZ4Atesbvqt68K3+y7P+QbaZ5uqRj9vDvhLRxXEAwJgLj+V
Y96u9b3Vmqdwj7ZMzGDJ1At2RsooM35KH+NEmpxovOMwih0HzE6tEK6svDy2Jx2OsO3bpkbo7FqP
7oymS7PEOy77wMSn6dPxI2Tw1rcfvd1N7y/8RnOTdC6t03mqyDsIT2N415fIwLad3dPUp2RZC15U
E/2/QJWbBpz7eGTGCxzhkZbIm/m7tkv6lYKgRObarq2aZdgjXaf10bdSUI370SGS6kc/zGkfIT3F
yE7pEyR2owXRegBryh+29KBZt8CMuOQubff/pexWrmBJA1wqLNgOHEw0S47a/A9qRPSGeGy9b0Zj
gxgvCGwurVtolSORIGLMM9RFxPiwulqeAYAGgTiboJJ0vMlWwXDYehcrsATjTZibYJzj5uGDPnhr
0L5Jz04K9wn4/p5r+pboYWwHEYR0QBGhv87Fp3yI72s7oiNWhHEEeKqFSMhl32N4NxRwbbCUrDyk
tr9koCNkLlBDnW1jGfnOxV2+DAjJmo4leanh94nfWqVUHtclJcYqZgjYNOebTvnJ2+Eei9dephDb
qOYPAxT+UxO2cSleoDNzkPSZFffEDtuHr2YlBeLjg2Stzeqk3lZQBRydD9vDd6c7SNvgrszLN2it
fxl3KFqd1f+Pgh5zmEX4xqrSplDwkNpz3+6u+jE6s1Rqm84+rkTlYsey4EgYW2Z6UeFk6jY6XYl9
2yBx9T2iBqpZjG4Ur7kkSZPCbPUU3aqPUgr3YLCmFc8ceawB/c2wSK3B1m+//ZS+bLNMqYg1WXXu
Qh0Z7Wj3cIL8/bt1awPazNtd1qAY3MMwtCZtd8t3L3bkBoZqhUtMzl68Yiq56alkbcB0/E7ggSsf
He8EE+s+++YsLDnzTAlFMEQ7Rwlf5oYK0Veu9b5uSiorMnZjVtBbkyO3UWV+9zORjs8bmyOq1v5Z
BQaXS+Cam2Z2C7u80oFFje/eD3qU8NvzL2dHVvh2R2/+Zcgx+StIdKEIbMnYVzT2TvYemGmU8KUt
OpMZQjV+GRfs6mBWTfHAmtVvtkhUAFgjGWMHabjJqKT5iQg7pWy6WBk9FbKofECc7zUOrlq3hDNx
amZT2OklBbPEJE8PbVf4g18QKcYbDoa7+0/00Cn5yTWrO1YHrnGtEU/P8s0rW8BekRCSyKGVUP/5
dksbBTzx4deWhSlsGKAxrcpqnbb6ExImWQEJdZZZUFVS2Xt/PBt+D/wuFExpa+cHwySgHAJmQNZC
jK+fuNyrlozii94fAZwmp0pxZTotQiAYu9EzRq+yC4NrT7u8rceJ3TPgmwT8kR+GAqC/nyZXmfz5
hqpVgKgG7NtrDi8yS1rgpFwDj5n2MxcNw0xRNdY/QqPg712pUMENOiBn4jPXeI1ngI7jqWFiHpiL
Rqptv09jojgfGaA8ybiao9acfoLyiEmLdidy4rpfNxdbfyOVg7tYTNWCh3ypiGzr/1qSDZ0N9VNV
96cQ3zD0tb7oTCWGQEXMyitvyVbTiwZs9yG9TtitsDx0jYj1aYGBcerR6f1u+GVGlaxhPcRBmDhT
SkDp8gqXhVQ3VLJiAjQLQSKg/j+VuCFEuAEQD/oTsGfRpMO/gtI7LHQO1oZSstfrvvZS8ElbKl7/
+2XsPDCMqoTDet14sR4CtE/REibW/8CNlKbOHe1lX/jBHwQNd59BColoNpek2DYN2Bif4O2FiBLA
lHWE0Kk1GBU8H/brQbDcq3qBlX+bbVzrmP1S5he+FvJoRmUZBhh9MhOIjWW/AZPXQC4pEJUSD7tj
HfcchyMRFhw5I5D7KMw8BQ5/SYIJM6NCBa6cyT+wRljd0/CHGDkIi5WA7wNmaCYsJTsW8VtENpAl
jZpUG0jfKXUWO29HBSyjVrGPDfH6rplt/KaNTUai2PNqaHkbNT5UQTd6clViEf8qoVyj6KyONCGL
QX8g9eqml8WuVg2qlYXNszftTfH0MVKOMVGTe0eKLz9jqJBzwFdh8uBRHHM5j6fK+EqbXYCgszVB
5g2OOQJDtqO2jPS7xZd97xO+RYK8NcnotLGbeWBoFqMXLfU1fsrg2XZk16Pk9CSekkbyWB/y8vYW
4a9ku8Jfam3Pm1MjQQfFh8jswsNPWJJa/ACAc7eKACFDdlB6OYQ4+1bI780PP/q5BEGw1RmW9cNY
I25x771KU3lej4pyhKXGB7BuPpqTC9mlEUQ2jIYH1XQhXj6HJfNynR6LmSTc2p4hoXFhMuaLcyka
S2rdKtPSjROhdB9/9EIG9/JcL7FISgkSR+0xVNbLJulu75scKWHW8fKaLWsts6akymX2M4zG59i1
TgcgfaBt22X7t1/XG8ol97MeIBDw7Wae0gx2VoU5T/K/SC+GJ1vz2k04HOia5wkz6SWuk6K/A7sm
xRUWhUu2IwJimAt8nbkeT0swk2dhvnMxNGbZrhX+x423Hf8DCOVqedQkp0O5wR65aJ44
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
