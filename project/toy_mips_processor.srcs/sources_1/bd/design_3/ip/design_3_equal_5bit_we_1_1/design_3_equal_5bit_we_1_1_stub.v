// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:05:06 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_equal_5bit_we_1_1/design_3_equal_5bit_we_1_1_stub.v
// Design      : design_3_equal_5bit_we_1_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "equal_5bit_we,Vivado 2020.1" *)
module design_3_equal_5bit_we_1_1(inp1, inp2, we, outp)
/* synthesis syn_black_box black_box_pad_pin="inp1[4:0],inp2[4:0],we,outp" */;
  input [4:0]inp1;
  input [4:0]inp2;
  input we;
  output outp;
endmodule
