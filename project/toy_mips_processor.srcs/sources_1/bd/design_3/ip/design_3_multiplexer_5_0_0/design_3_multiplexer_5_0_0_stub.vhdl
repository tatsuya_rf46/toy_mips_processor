-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:04:51 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_3/ip/design_3_multiplexer_5_0_0/design_3_multiplexer_5_0_0_stub.vhdl
-- Design      : design_3_multiplexer_5_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_3_multiplexer_5_0_0 is
  Port ( 
    inp1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    inp2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    selector : in STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );

end design_3_multiplexer_5_0_0;

architecture stub of design_3_multiplexer_5_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "inp1[4:0],inp2[4:0],selector,y[4:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "multiplexer_5,Vivado 2020.1";
begin
end;
