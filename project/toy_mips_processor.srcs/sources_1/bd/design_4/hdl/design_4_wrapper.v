//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
//Date        : Thu Sep 24 22:49:20 2020
//Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
//Command     : generate_target design_4_wrapper.bd
//Design      : design_4_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_4_wrapper
   (reset,
    sys_clock,
    td7s_out,
    td7s_sel);
  input reset;
  input sys_clock;
  output [6:0]td7s_out;
  output td7s_sel;

  wire reset;
  wire sys_clock;
  wire [6:0]td7s_out;
  wire td7s_sel;

  design_4 design_4_i
       (.reset(reset),
        .sys_clock(sys_clock),
        .td7s_out(td7s_out),
        .td7s_sel(td7s_sel));
endmodule
