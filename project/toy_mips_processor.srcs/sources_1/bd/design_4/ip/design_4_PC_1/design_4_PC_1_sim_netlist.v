// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:04:03 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_PC_1 -prefix
//               design_4_PC_1_ design_3_c_shift_ram_0_0_sim_netlist.v
// Design      : design_3_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_PC_1
   (D,
    CLK,
    CE,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_PC_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_PC_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_PC_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6C9fFBVho6CD7Vsn3+N9ZO/hKyIFu45gGO/28RMJutVNmDyLrnwdRW0EBWdSUdXptOHy2x/Bd4G
uObaCT9v7zaBik4aNcPo5dOUXDyM2Y7tP+hJJcRH9LDQbp1GXlHDF+8JYpWX+gpdvi2I7gSl6x+V
kOmnISSxzVogVUf9aXQ7SpXxF1XMjcDDSydpCV756NeO+tC40Bn3c5POZzaHgZ/l8e4vIVi4NzZ6
lVLU+gYTx0Os+c1jJBtDi954Pk5yBo1uVXaa8uqcaj8yCb6vpx62zUooUbH8R6jf8PW+Fm7jSBNk
R6zjhWZdat3sC6wtG0RZ9G+bkX6I8wKIRdGOHg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rgN8zUGKiRqCzLjukWvt1HUv2EVBTnOqG4pl1MaV4arcX3OlCy9WyS9yYFnl8WmjOwKt92RsyWpq
2Gml9i5BtwBoJBjiAJrwpiHfXdTrzuD/JWYtOOfYx90y4gPs48bYWQkce68o1rVl5M1Lyr6NDCnc
9YgYNHK4jO5NtVpPkoBew/7n8DJ/c2nyLPOuitAyEJSRecu2XrK2BqcaenWDvCG6zz3jyEwNY9I6
VY295NRED5tuDxse48C/oZoBdDLKsPyQjfH0daIS5bA2bagYsRmc/sSIC3Q4Q40xIEj+I1T/YuBI
xcIFEbXlbXQU6h1kgZd15e0qoar/csxC6AkZyA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9408)
`pragma protect data_block
H2CaqOgbmBC1f+ex4sPsK7axOcXVNOcLqTCvoZhdTHeuQp0oRJqF4lYIP7xFnW2ylsQAEwbA4lS/
m8zukw67nFL5UUXD+RGxI82a1FojkIINrPeXo4ovwmcqxsTGSgB5YBTKL7oJwr1rldgzLFOAdEoM
ePCYQQqZAulZRkHYUPWM/5Je2EXakcV0drB824AAH0CeKu/S5SpWm8ljCdC/8z5ohrni8kUfNEsa
AJ1l1QX9Tttm1kEBm1JKf2RL1BTef2Zfw5bcwtNwvjyFz9VdQ76irH6528xJ0CbWfjlUWLyqm/Nr
7wwm5M5+dDvSWMvR3ntLxgO66CddcsDIjn2MUkYrao1eVJenrHW5ivqvMlt7ikZ6jVO6lMBiPTak
9WlaHi0es60L8wkwc5yEXcUiPeZdUuf6eSXmzIVlH3c73ubeK/QLhyz7xyl0EyE6x73GA3YrxSVg
v949WQShnvpx6rbhcOzjWTPIlqY3eOHTr0CclnW8UKYPcLLdX166pkrmGp6i5hJlyDBrMoDR89ym
C7aHp7kgabbzOAPyBd0yb1xSoIXnSHcTnrFlzX1OW/+p/CVEc5jcl/xaeU2p46rlU1OcDxeZo3Le
acJ3u1cHOCNM4kmtzMts6LvBeN2Ac6zc0RJCh1DOJDncRZ1zw9j69wAFDw5oBUydsFffPgS4Rwdj
YH+gAbVDV38+kaOgItKPpu7GGk64JeHE36hbQc+uLfX2nLlPDl//TZv82Tj/+clNphbk1dYhUcSq
smoRc88L1NZMi24UxB4tox7qU/8jl1WiWiz20aTJ9tTWjQqW2aUNdsgcHoRFp9kUbiyBvwEZfUzv
NInwfuRxdnqZDZMtJrN7Vur6JtoQuKueZeG2NzMLEwC0Sopb00qUqbqhfV80T+6nnTdP/A0xHTG0
BtPfYa2cpVuElcCFk+2nTBkpylDaVS2UHSJ5olRsZlrsw135E/U6NN7s1sSZfGacb5vYr7pDJBTu
VhiG9eTWx7lsUI65vD3GF1tP+xBj6KbPmQ/DSmsb4XNwN7BQsjEYKq+4lsRwhy2ra3LpM5EJlx8f
Dmb0W1qh9OF5hpVsYEsrqERtTYQaCmt+O7roG7EEUPn5B9i3IKWbJxCtuFlgVmLlNyABbXdd5dh1
DeaY6KY5QXvPI01ZcBPHR9iGzKl5+OqLwY170yGgLNDSk27KYQ+G6u9FP1JOr7rVAl9tIN/OoiUk
IeIYiq2hfx/tx0B7fbhxZ2cYCC/IKio9Ei2gLIMKspzPizm2yiXSwF0zKfEwE0nsgENb1s8CcX1W
LjGDMqTWIJu38ZHigJTUddrUrjBei25C0dZjRGRXMcyj+eyQTxe3EhdySbU0xQ9AON8oSRsBWdFI
o8EMr/jcWZpU5MMq0kKVxAonODikeXisMh68QXxgPf/filjH9FClWdEcBkQe2UdDnnuo/hmzYpPL
kKqD0CxExzE7JUwNX3eEjduYTbGs8mpeNxVd9Fu3b2Ev2zpGaaIFpw1c1rJ807qCF7L5PZ4QQAJJ
C3a5GXxhYuNSa2KIH3QegikpqZjD5iLhhM0QaVUWM1M71n+TLj+n7weW6I/GOlgc1v6tMyhDgTIU
/VFg+adaTk2FRE9jDIFI8jUh0hrIeoqCMk/8abqhZ83Rayp1PE6luB/2J/L4So02FQjyFUqElwuV
ahEt0g0kMx1pjMhYDB8x6Eh0nouy35kgeZF+nrzLZhDPKovwZw2CO2ief9UCH/lF5uaGdB0iRkej
aRnQCG4Htc/TRghj63KgWtLOxH7rhWOo4s0oqGmSapF9kEnrHoaVffYlwVCeT49zATGWqUSIVjMK
prQq9hgTeYOuHvRR4NW5IDN+Z65gPlbZf7SqS9okfgXDsLKWE282W79QVu7/8Q/F7oGEJAqyVxmR
6n1YMhF2JPtfPDeMsDASW/bDGM7csjxiAxPdfRFsTVpGFVusRE/+jRzt+9B+hkELVQ95tnZvQ8wo
S4t9z+oHE4WUmkIRcLQgrvUF88hcaaaHycYOpEC8lSWIYltXz8S/6UzatU94whZ8OGER5ycFMKhv
9NAf72/2d5XYB4Kx2ZJptdF+q5DJ8RUU4AVZ/36fCVvzzHfDSpLdI3ZEBZxyFkYxRgzwuTZs7deZ
jE5hYDlJjW9LTgtWQxBUj5MmZ/ppoFMlnz9LZYkt/yDQ5xMDQ+HOanDohggxDaydtHZ9OpsHGRgN
wpCR9p9XVAcJCLo2r/gWjRA/A3zGf7yWxlN5Hv/obx3dscf3csAza38ELbhlVsEqG95PjIh+ohcj
vwaJqtOc24kpSR5z0+cfup/+QnMT/xqOCosSEviqEuHRhlba+QxhuzmjFJvfCY69N+DvJTnVccDJ
kg9k57ZzPLVVSynKxXOsLtGhm4iZjmnPg7/Cdg65WOIt6tl3XXPh4kZmN63xTvy1cTjuNRNGx2LA
NCi5OYZY2zclJSLyUxRVwvs1kFhd0VC+pCOz8lySUyOO+8JfyjIzsKe6gCOuV1Qt7aQk4YPZw10Y
AFEGyyMxFziqQG/UdTe+yQABh/l+8gg8EuLaifQSDD22SQif92IzcTPIII6PLsSYTl+Sj4IYis5W
S45et14CM3++Iy8dQI49uCrd/xkghqcL//au/bIqX9haILlq8Ce4HKo2CRV/uKsjwqGa3lEl7G9j
L3PRtthOsTknc7XPXHqdLhdp+E01u+OvGcqCDLNrxxH3/H9R3ChtAG3kaLaSbzj0cnOYdz8c+TBY
Fh6f0FCgPGair4EOunxSz7N+DLDCF1P1A9XFI0bqfdgJRdb8rnh1UhkROx3qjbvS43am9t9key8n
F1hZW3uvQOFZ5u9OLPUAODMFpP3FdJg4vzJHuznWo61q3NngsGwz6fM4UNAkFj6+zilHAnO4Kz1C
BmN3RdDkbgOufO3cYdEsU4ap8dHD/q0QVsXe3LkMiDPcTtQZZAShY1TStljoaq172pcLHm1ukDgB
wbq3QH//0FwTEFMrt3lyTL3cXVg8uyXOcGcYhg9bLnrfgEOnxENFdQWQqEGSsb5903/UKExbFzVG
DIXM9QhP6vXtRoLHVzwk8gYoGqbok8AqeaJLxW0Oj7czCtA3LCWC4zVClB63SG/bNLDu+YupBT9z
QlnhG527pU9ef9tl5VyP9BeGVsyB6VTYAUrwrmr8l5F2tmb4En01mVR/F0SUf9FcxvuSlKcs/PeX
EQr/QtCM36yxfeRYN2LnLHdQr8Z1ZckOo1w2V2G455+otRBj78M6PSapFa1SA46UnAtNlQKca0kE
ZpaeL8y0U7rjjgDOFQvJZ7EDUXLQgaulT4dIKoMbtApaJX4TZRkK6IpMoL11AUw54q0aZWOkn72H
dwA/fi0TRf/V1Sc3IKKxJwsv73UsiDGG8ZG+nrh8gJtteWuRveZG/kUBP7KLNFvi0vjFV4L77Q/s
fxM+Xo60/H7tMvb0mJV12Iw2qeNKRsEqni3q1+gf25/3+BMtVEV3YRFbiZ94zya2VRWVJ8BWKZa+
D3kMBKnYIr1VCTHqRbRZPXtv3faNu38M4ZmrmQ21wCARwuzNdkB1tEAT7kqGcx1xekDPz5C2wW7X
L096Rmr5/aqotNLDkhoT2sbKIfELEIf0vL3jfVZ/LCIwfPrek/tDCufFQnsiZfXDY9ep0b5jcMoc
iCfAkXY9jWi9f60QnvHoc8KZuv+D+B8mi1yztkuL8smix0/L9ZFOaYks3ISoU+9RD6JqUiHwKiLF
RPXWw4FbmofuOxOzNp+l9Ah0Gm0WGzYae7eX9IXJLz6/3VWwzxKMNhmLhgibFd15E1fj5Ms7FJ0o
HSyi3vcOGMOuuO0oi7iJeYVns300oj+q60cg4sKnaMiO0UlqBKHFtwHS2T8ygGsMPBYu6K9dClvZ
bFqd8s9C4+Rzj8FQXQrve7mkM0/2EAJaSjFMHr7K1JNCWCSUF0BlK2q46iW3L4HGTVi7HIlTbR3J
J6/nlks5P8BdJ2WE2yV8yLyyum1KP7McMawBbdcgDAQbCd2wXLyExQMwkivdq58xRmsnHz8wEvk6
TF4/0b6iTdZfmUgbDNCkDYFua5r96qPXJz8oCkZM8bgxu2CKuycwVaiaMn8O+saFEBJQNHVyMgIR
j2YvO549UWf2qV9qumNh8vHkIkyg0lsgMA9E/1DYrPSEpmomTZKkiluhI7jP0nze0xk+zJyxwwXG
9YqAaDe82/ZYzJWOocm8cuTh5vITWkSuVsCGHBF3PssMtoOn1dOj5PxMaHotEMzlAOMptwjr8VcQ
W5XuF1H7YrzRispw9XdzYBOhungSVL0/LeHfHN0kbrb2PwBOWalNc+a4BO8g1Krh4KiLFJuX1s7m
mwMaYQv5VF4IqNYIl8IpguHv7PaCc2dSjl3iw46Gpgsc9VXaTdkqBzz9jtVTtk85s5Wxj7Zolp5b
pJ53ddg/hV1ZWc1VXKeUVsNvOnGv/J5CSkAQw0d5LnRRsuTNIRY14vJG2IHhEDbuUWXSKSGo5yP3
eE9Cio68BYlfkf+Y2Xde3mGYzIZZXfHIf+TZIy+WjMkbtphqhRfmEhCXUnvIeIxrTqkWaT8/khJy
zok7YIgtG/uJUU11ZnDwHPmkhAYfIeNq9FnGWRkEIML/6et69IrCSd4MGJEfeJQgCwMEN7CaW1Fr
9i442WAfWcQHB2ysRymgBD6SZ4VPMirv4mrnXfSrJ/zTQC/tq9j+2rGvimCY2McMml5F1sBt+RBt
FcLbcv9KkkthEnD++/JpNVz/MW5+W4paKYHEpj03I4WXNW01nzm94/FlfbWaxvpZw2jjkKqhGdLu
c5UudtVPWAVtDQd9x8hUOfNmMA+7j6oQdpsY5WseXGH6cxzfeWAmhbbnGaptoaz4gNc7TpRsxOQ0
fhQorafhjJ0DV5DkBydUQ8A65JfD18nTfB/iryFZnmo2uErdeNreNTzsyxEKOtgFe52NdF45Cpso
/RWSH/eb69vtH32MK6fyU56AobQiA1u1reEwCENb7+167JodWual221iT0PJMja/KUPMDiIWQGPG
2nOtqa8DqCDKOYYtwQ6Z84ru1niVVrUnt7KPjbTjdNimnqYFf+CD0MKfg8jjXqXsBGwWHRSRhxOv
uaWxJl+S4N3miQu75yqv8bqFAeUHARNYtlITjb+vfrwWFcmNbXICNYuFWR8fMu5q/Nb5rD9Zi2ev
6Z1dPyMJilpN5NmvHWyUaTR5x4fvuNmew4CwTJ8803/vqccn8Id0X+rAxNPdtYiyRMnkYpTksU57
CUpV/1H/1RqG6cYTS7v4yd39JvnYjq3J15u7A1QRSgkQFZVq4EQOV2EbkUBScaCe3oLxptqQ4wwB
WedCy3ZaPFYq1JJ58EPj7L0+Rbtlka1qQMAp+Oi6ovFhdu0JedJSfmoBxGIEl/oEzk2H7wavOGBJ
2EHsFxw0owY10Mw8SsthkkGXPIFCsVWx5xF9yLEsI2keULdMyEL0x09c1735mEHr3lBRZN7VbToy
biuCSc2NOnIZ5bVW6c+2DAV+YlBLkAHu/5XRv9ya/0ozYSRTmyrZp4bLENTl3kq7Z25GDL1zfD7q
bd1foYZ7tbhJtpPqzV0PBUW3CDUI8DN9u5EAbyeAA12AppmeKTqj5I39Cd4aB0Fvfl2cTpPgo+Zg
DqDfbkmWS8mGNzMTQHXPvthOllRWBovRARCqq7exXq2Z1Jw5NmEL1lvwxGgZjbN/IjJXskEnItWj
cNPV8NAxZdPR9U7Bx4ge2pJphMUrIEDY95m82MCaP43RzF0BR4jXHJsJBvduhOfgAjU4mhspqBYX
A4Ol77fMMKtpAT/ZzXSpyoJm9qjwxGd6ESg9fx9uhAbGAO/AuEyvmPfkd97HesFGVymiWh1Guwex
R7a78KIip2USygRLlvRzGbHxiAowuMSUs+HsbOXNWacEWNv3LeU3XhrIakA5ZooIJrbMShfCCgUN
iP2BZuqAHGNsk4CguzuUf4BLQkqEO7hQsdfykS4PXl6uLx7DTFZAOgKW8x9IMr5qk1C1E75jQlKU
lKeG8ikdJ9x0LGJ/WizEaG06JzioG76Nlx+PSGdkF3xZbCF2a3VDYtJxBLjO/o9yR2f+t28vhpvG
NyM8Kcysof+7arJvvw6CTcSyhAAjCBQPV4XnJx+ziOL9zvzB83tpCdfym63yoKYxPnEw6/dkPrBy
qzK5f/MDsP+gTf85LLngcrYF60rkNrYi4L7VzmwSCM2kq9AwCOojyspGtHXzQn89+KYfORwqzKZB
aAotqk0bZ75VJp1H76K2sFHcnhQFA+3Q+xyMH2yKGOH+BHxnmz7rS7g9pZgAectRAnD1KjERKcLR
0cm5PSkGqS2AweLKTf5rcx7mW0jxJzqevK+xP7kuJylj7MY17Fiq+YvTPPJ9uFiaOV5u+zeFdssO
wLZ+ZGQLSEvAh2Ltcy7PThb7DgHrz73J8qAxNiHslpKnwwVvshfHh9Ry6XOxPXgwm967xR3kIUOg
ai9G45r3Sn38F6Q/GUGx9ZEyFMHgE5f7WYXu45nwYMXVAcw/RHmAjBDxuCpQrWirgJG1hwXVdb4g
7Fsei8ta5Bk06gGSZd+KY8wZW6KV8iQEy6htUHCiKfpwRyH1ygiwQgyALJYjKJ7fK3Re2f94J3BJ
P+xYy62z2tAN7xtBWnAl8K+ii9rjjPXn2R2WcPwnn2YZZ8yvmYojxymtV+/n6/FZh5UzQvu8mXe8
2qNSEJdPjXPMoflXzOjPo8GcVK+evJfHCAiwOrWnWvhDUdgGjZ5oUUePdsy/jcvR6VM2JnRs7zxD
8Qx7WqCOBCbHHl2v6W3b93xEGbm4tknK5sHzHGBxUgGqnUSRNE5ZtFzJBSork4zv/vpWpa+UYecF
OS3Bb1qJbvPhFQVtr5Nw3A7E26A7gd7kriDyYYnrCow+FSQ8YOG47/BBCm7QWGOdR4mQZOG1NkCV
fAG9gv3A5GNWpc6XNYP3+eliwic5atVtRoPaff4jv6v2dNMMDWvsHSGFggUb3AmN3LDnwwhEJo7a
VQt9g5vgztgXVjYjyqyyCMCs4+XgWlFz7KCC9cMAjrLyO7HdqXmcAcEj7lAk/ld4buyBMHfayKJS
0RIRaEH3tr0tNBz28ZewYGW+MYWlLlssz2Lrpx6o+r114J/micj4bfg8jKn9+1bi+hZkPNvw9aTQ
L1pr0bsmL0aXTv5iJTGQJ2iZTTPpeDVW4OHulYHoUQXmVIkLzgwlm9PtG14U8qAok4goIG+JwHrQ
i16wAqWy1pBECW81qnWE8cdc+nfJ6x8olldf3vcnuNMgYls+E8vCndmsu0Z3yqhwrQIymo729U2m
2plA+72mGS1/2b+MB5Ck5BFvqQ2B1Xtg3Rh3s6Aesc3ZtQ8h3mr2k4jI6strcVBN4HuVRy3FLKGb
J2CeMDy45Kd2/4mAPEGYc8jIar81mcIubL1y76vkc6fnVuNO5o7wvqF40H7Brh/ggKj1XDi1IWhd
8q0E2JRouGlLHnpbH/DET9N+JV8XwbYMr7v4XeI5A9dziJ3Nfi/+tEUTiopn2wx2wHcpNSYwTB7D
KEfVupStoj7mTEeWMpsjwlXESK5W5KABDKHxkEds1/uhf3/0LkwT1DebLveALwTbb6GpaffkjVJM
eCgbqsiAIXK9BzHORYP3p1pWeFxZd5KpMK7Lo6D9zRNqSVcNwf7ZcnOrRySBkUpcYDA2cn4HEKyX
nQrky9RxIZBWnAktGQj9svI2XoUe9PTQlz4rWHTRTwN8I+IiUT8O/cUi/SOuiIj9uLwBSTOL07Nj
u6gqSPfpv5mS6CTDpB/8qszMkCuDtyd5CD1eRDerl0OXvPAsfiDBH9WOq43L9xmaETq2Rggvhlkc
rKiZei9qd2fUttsi82nbyYGq4C/FS19peu3y1H3iDkYDCYthpqrq0pJ5zvGLCCby6hewLRZ0T3nM
JfbE9caw19rQk3BRYQuClVqBzMgfodL2ilf+0Muhx4dH3qBH2s1C5YdS5lVFiQk9aLpx6MVFvm3A
HB5J4Z9X4F6Q6fQsQgJlKMLDSVK7beASOr658n6p1BhWDHHToICxNYXmlXPHu6JdvvLfkYVWvJcU
/58zlUmR+aFoUvAM7vRB0LPeOK734m5Xj4nNDyOWFn0QHHhJ/08W+TYNlyZPJe9HKpPK/ysuuygf
2XMu2Y/dJzxTmKjGMdsw2RzU4rlV0Vrx2bHefleQwJvlctbwifuZILyg8JoBLVKergl8CaVM9s7y
D3klXg7splhgrp7LmLT+p6MJoilCTDeJUqJvTVTZPOwkhXSjYOe7roDAsDxKq3CU0tbWuJBZHQll
w/f/6mTGcZuZne7wsg7yhyYVMU+OGNsTbn95e8AflSHS1Wy1gTy3BLc8GA+LrgXaz083NCx07voV
WxTmtVbwBx3pGFDGEEtGuqtAJa9SyyOt6VLffPPKnyAc7ieYcLei1BcaqlFliSBypRnUZ9ZCSL6c
qQf1A0fSY1abz5hbBexumDm6RU3in/Jnl0wju28rnoJRgannfkdykVs145rFefMhymnAWFAYMXtI
MdlHJW7TJ0vrtD8HT3AjRvvD/Qcc+twRvsAWzLy60+tIr2jJBemOnaUc5/reOBYtcEchPBPkd9Ve
GbzTqvcpE9fVzLCTElaL5Lzkup3Q/XWq1oHI2RUSKgr9UXJljB3tJpaR5wwCltIjQurDYQ/vw9lb
ZNzYpgIkDIOipv0Zoc5jos5qbvdIV0siFg6g4epQ5gIeo+GzHp+Vo0fOYuPLIHSmV4Y/r/7pxESq
BPbabriF/YDzh5Omi2qJJlD0RM8erdcHtWGFzvPjPgOPDdObK4Cwx12BBTiVZbayZhE2b6vXSxN5
6X9MWte//50fuyHQ2HyEyMwH76KNQQzotwWhC5GmAiyVk+6KQOND/yTH9WmV+q0+Ndx1sidq9IXG
pW5Rh1rvOUbgQZxEyfU5zE+8bz2WhZW7GUPsffFV3lRwsZQhnYHg/7TClSzFjdrP2t2zpAnVWrK/
q3w5KE/i899PtAy2QiSRY4ngh3zB6kQr6IKHj1hDQCYKTkT7694ijzibFzcd3cYSgYCVKsuy3MX1
fqpCiUon7H/tLgdy9jSH+pSL3Z99yExJ8xed5g6mEdRbgAJ7MVVPrVW2lrPMTBqU2rmUchcDdhtF
yaROPLJsDgWM3eMe6W86XxnE8RPHTJO3sQKCgvu1RlofHtx5xnevowwbAkqx4kBubP/4R1ESglbN
fgSt11qkzGzjEblwWGIFEVXRInG1AQ6/esyePTjJaUZj718NqYtRXUlPV1rp3deIz930PpxpxpbR
3gMdkylpno03j8j3hoAUpXMNcgC11SCuMDUF1xugwd6zSw+yGpCAYoxCB92FXO/H92FLykNWviGG
gs+meB+pqjkevS5kDpa1o2VCDqBnIeDXzouflwNJxWFdpbN+hKCZ1rkV+sYDXEWD6g4PGWGBlSKP
pLZWFJrmsZLDfShOeOhFhfvPIuxVNW4AC/Z8/1r9DxSsZ3SvbPaVUWIwgswLWYDd1madbiwSmL8m
yZXlfOqFZN3BeWbSz6IxR4LnW1Q3CA5TNPSSpNWa/2bIoNJ3rBi4ksOeo80zvzTCmPbbmk349OC5
rVfn0CHLes7T+4FYcFl+MCHVFJirckfMCb3A9FbKJ1hHjae9iYHRfHQ5msBDjvaLlNKUiHeiois7
pDNiU7bidzmON+hSpStZpKK6SzF56d+NGU+FT1haumrWRAwLTxsWuw76n+T68R5QYXPNzxcVIUCm
UbrCjgE6E4ACZJs52e1C5BMafd/lIGrUF4MQYl4bBPt4FLHx3GQGUCkx9VHBLW+itGYUTbeeCJ0u
HCmPXgwWzzdOUK2rK/Ty69Cv/Y61B1uFw1RqKAyZKkIibNMJ4InDKOtL7JBUHsXuMRXS5bY81q8F
DnSrZ/BBqEtdt7BtQ82AzDql1Ci3Ya5Kzm4vX0MKgwcBsahEJioXMGmyM8LQGMsjksV8zimIX2QX
1yhw9pI41gj2S1BRfMsTc8H01dAZt4zp5+xY5ClFrrkvf2/KzbjI4/XgVjGutVNg9yYLqWte4+iS
VZhqjLdxnmymXoDvTViUfFhd3GyaZ5RnRIrGZy51ElRt9cPSVn5Gr2i4Kxbyh5Ng7ELQjpeLaWdE
OSmfeQigRsSIksVMA6dTJ/YbsyhfcC8dLjU+ms+XXOfY9OtVJBXDJ3HvDWJFKoPgsy8PG1s0Lu3H
XEVPIcjzk9q3kCHSo7WGThIidu292O9J9bZYiHWaRLN37x2wFg9l6SsONCND6XeO3QPgU4zQ1Eka
5EWveGoSDN268797W2N58aT7uvNUxfYuX8oYmL3UtIUeRbXdNgNbAn5/qeeftcFgPesSbyov9/P2
k76WJ/mJPa0zUS6qH2PzoAJZZk4tyFrqrr3blgYKFtsgxaSSasQNU24vYl1nKiEjE7JaX9EtPTx6
LMPLmbFiheIscW9wfTFKr0Apz5dDqwEz5VT9Fgm/GSlLRLJ5vI8HgMcpU94ljiS3fqF9SX5DG6Pr
6GJEIxgkqLlncBU6ZvZKMnVqoQbVCHDPG8cpeAgVWJBG0IhXW6648m6yIRRwsUD+Wh5tbw2UBCvd
CcAibB26QI6FIMn9m1mIIawYefaz4SUJizOZjlqnupwQ1i06pkf3bruxZLkCFPCi9olQtxACJANJ
T2JX136zRfc5Ukdy5un/yz/XtkmJbHSxlJqC7TngQBzJp8JbRdT2xI/5KqMgMSy5LFiOP1Z6yUZ8
P9uiQG1+yq6y9pQdGEfMqMGsVxKhVjpvS7VnVOfG4uxNgt1RRZ1VR3OmHOxbRu+UQl1lJiU6mlkg
3VxN9/gyi+r/Q74a/rvdsTg9gG3GiKT/m1IVxOG6T+hlV6bMiA/9oWIwNcHbO5r9nyZ7gtm8z12I
reA0Xpw7Ra1pOSwTwnZvY8SaNBKcBwUOSDw6TFuKSVOPoYOa40nH8McQT6cyzkmi6vYTaR4sf+a/
zB6claRix9nKX+LUDk132wOGkx7JPHkzh3vNPi0Y5dnvkTHoSYXktQFwbE5bsxwiEHfnxtxtSwVL
Z6aiorGIkgNTZrJ9UZtoR37ogSPaM4+y+gj1+o1jyaMrVwzw9FA3hFG9K+HSyLW0hi+WZCDRGyFi
+aLn+nIyNSYvcC3qyHICKhpSY+Z04f0rxcFwdZeL5OHp9Ome1u5wtu9sMHRERriYMMgLI3h/Scme
H9IvdX3PaM5pvdvjF77o45vTEIelFcJ+L9jvPGRuRg9mALRh4cplg2GHRd4YQORY3UtOqiAAsSLV
L9Wqm9lpDbZsDAsEMGLa+51N6I+rzvXmai62N0yhDTWbG3lfYRKVwa6A4SGFypbONUogiUNhh0UG
UEkj6gZzGxC+06QT5TFRVAQki4ZfEkN5ruMeYOcgeYPZJDspbpmRk9wrANfs994C/OJVIA59E+0Y
frqgF1fdYIjen6vP2Ek5cV5JFGgU1zZivYVTHbfWEtNxvlix1qBBiIR95iuAdzIc24nziJLGUxXJ
Y3FLaQPjJ3MKt4jzOQs/c8l52uCO2e5E8bNvqrk1sxYdPBPUPi64JiNZmFZmbCrK7c8gRgRj6KLb
PVpel0MX+Q79pry2faehB+3yD0+x5/n3YFW/UNtxzVmy+7GYT5wTJIhUiQXAuonucwB/jLlGzwBN
W4gR/ZrC5bdjS16uDu0kDq0H21XEcJ0Ga09u8lePCMBt3xd+Vks3RnGI6Y3EkbSIDTmFd8gc1bc7
7NXtZC+ufYxM3VffurswN2PhNmy1U8WfQzrWc/dKN3T/W1yp028G9NcQUNuGpHbDB/rv0pEpUMvh
boQ1I04+haXsHD+xmnGS1VoBY/GrxG1yrRDDfVCuNmwSpb2ReFIjAUEt1Rj8ZghjRcQEE2p8GNTi
yHISM5ZjzLooOsYcn/NnqOGliP3Jm6IuxfZR269wO/aH7kiM7GYbjdgZnA7Ft6Yz2eHk4kgvDMoN
54psl3G+dqV3SvmvcMxk6lrksfjssWWD8sKmhcC7TUMsCZAUYDymbbJ0F7T7CpSXCvHvm5fiV3Ur
g8PCh2SwISWef9uhnNHGWJvud+kMqb4UicuWkwXInHWNsPcrXZc6zGfFZmmnu4Vp2Mh/rOQOUOJl
jAwIW6S02r/Br+3lZu8+CvOqHmR1k4HHZ/kO8LrLgi914OY6RWDUQ8it3x01EgTfX0eSdKO6js+i
OpdcApwD/t0w7WAxf+ReWlysqNb9Mfyy+f+75bAjfEHj7xCCCTQblbW/N93Q7vlnw5wg+oVRoUZ5
jzXav/YgFxBx+VKnrZ/sWSwrnUd+CSSR4QMUbRS/CAO8ZVFlJ0eI2OZJiY6YZOIzPOAk5NarqVuq
OGG0XgtY5R9hbulr4FtKC2Db0zF+5lOIGP9YGIIPrZnebeei22u6ehnf5AWm64LysQSsAF+fQkcv
lDpFfV3cimX2Gp+PcFWHPeOfYp2Fr+svAn+ZcSg7HhTAy6vu2KGijuuFdvzVrWZUMepCa7l22iRj
0K3yqzqtchBTqA8cynOKc2E2HsGln1ONhUrvJuIVac7bqIF9FR8CD4CFZkluZMV/Cwu/fqsdakUf
gUzr
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
