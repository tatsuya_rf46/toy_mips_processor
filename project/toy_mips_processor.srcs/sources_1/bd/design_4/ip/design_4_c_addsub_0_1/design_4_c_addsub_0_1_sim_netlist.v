// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:20 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_addsub_0_1 -prefix
//               design_4_c_addsub_0_1_ design_3_c_addsub_0_0_sim_netlist.v
// Design      : design_3_c_addsub_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_addsub_0_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_addsub_0_1
   (A,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "1" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000100" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_addsub_0_1_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "1" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000100" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "0" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_addsub_0_1_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "1" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000100" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_addsub_0_1_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Q3x0rY5yGWC8HuhEpWIlg3gNoUlEi4xNkebt6wApgwOBKgj37i8XgF0YbJQkKXgwJ3fD+XVy+9aO
F7sMKIUxZX2IUP48bDOTryk6dRvF6kdAxz5tujZasKCr8pugubV8T7AcC8LE7FLLm80djrTiYfTC
2ytNLxLUt/YH40J2SsAyFAgLKaZ4r3N4vV8E1SPi6LAH8p9JXSV2DBrvQ7x4WvQGojIUfgZapdHc
tQWd5tDQ0PA40OtyZestcuUEr7iGY3jYeL2ETQlYPwjEK3ZM4Up7FaruGzzMb4xb0ztSdeGn6jeA
7cnObyQUIECeRkSS6gCiXqcRVHfEwhBLXjnBRA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
msoKKHRA6P64g9sXciGzCBtO+Ir+U8FxEO6qMA6sPitXEdwVcFNg8R/RcLjT0nkP5444PhKBviCr
VO8Mam+to2zMAppAZkUQJyCE030IgUnmLhvG+FqvkaLvdU6JtbpMSt+EXe/UXF+RQqh7eIcH1Xxr
Hp+cgB+VXjnTTY6DycmbfQsYd8uUPy+r/obChs66lNOFyo4IvXf2IyZ5hlIv7V3iY4dvyPh0lo/X
o1jSykqFztvpeVdPyFeAYtaOnH41pGK2sSzsfZY7t1BFSSNjKWIvv3GDikSSpZJTEQ/ghHxF0cW6
En93vQnZngjqkGQsFa4e+jsj7p3l3bB+DMLGYA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 6512)
`pragma protect data_block
MvUpqVeqIJCEwrFnOYs/EeUjU8B8omHAlNx5phm2Dj2tQCwYvdib7aAcu6m1VrvJy8ATGSWGfZSl
zAKBi1zjslXoR9VQuqI7jPCGT6qvgXHqRsjVSvkBQ1w7mY6Wug/IHgLdu/kmQ3vwCPJ5iFPiyEJa
pSYhSZWFG8AMtua5KRWaSinRZdGy9iMSBqN1eCJ87asdgTcv9HgybowelgQnXVb6ntT+217yy9si
1iYWb8Rs7cCcHy0J9dNyv3w0YnADZfqSmNpIywWUDCY8ThYy6YTfoGKNFvvCqtHVYHM1OtdEJhmq
UZsgRr2kU2Y3l5NGNXmJvcLvKrY3aZHUk9UIyBtwChpBwETSDZED+sYRYgRyUNoFwijH4refcLEZ
TyS5WzQ9MxrCDe30MtV9BPQ9NLYi3aGzwVJVdvVeOJvfXX0uA9bigWeIhE0AYW0/WM0YMBX51px7
Ey/wkwd8Xm5ZhC3vTItoT/yXLtRzYpAQkkrVtwOm3C3mFzooZIXWJfdfOkgHZfzjhhudsPbTVepy
oUFUn0kMGhP9uHWNZpn4OgD6ypSNgj88bqus8+nORVpPECzU+C/Wx6EBrZXdtETOvlrsAFYZnR1Z
WK/4tfnfKKtAPGj3JWlqw3CynhL6M1Km8iv8VYJrSD5Y4RzGe/+79qQqzbkYhbeWAlcznejJp+8J
mb5yctQwPmLciyiHS1aVAnQolAlqXkvvQs9Pkh4UyeZs0laLsws5btymfXRILpDV5BnFW0Ja686m
pe+Yh1NDB51DrsU7l//X8s5xtJMp4Ex7xckO5DNHq0nDJrdJQs95lbVBMxDEwLkujU7YQC2K4l33
H3tdArK4mKNyKhtb02J93LsGoRhHbpjf2L00SAvWcvn8yY2mIEYYx5aJWFxg9wYlib++O1d5V1r4
lz6rRXh0tgAPfMBjHhp1Cn6WBn6tu5PMP3yBbSe1uUt6LLKQGqH9b5Pybw98gBjtXUYzusH2j2pG
qc0+qb47PnB76gwVUfpoNgWqS7DR2vfdk/Jr2ZzsuJFjDqIPmwhYeYu0/gWcJIos0qY96O6uI2Vm
/7DdXPFeq/wMup8ljPWJzlnToS4o0zoo+pmSZl/YaK7DVZ/BUrTB973KztIlo+y/0pVb33fg3mAi
OXPy5p/oX1h0fcur+ORJUKQAqe9QozeOJCOVVpw7hIJbsgc2Tx2JvW4baMZKdKH2gNFTEvBBvXUP
2k1fa5DdZuhu9++CsWY+EFiHUQXJZPg+kT9gwjArnRdMBddzOU8REIhLZHtmr8ttlTLjXI+s7BA7
5GjOIPdwKc2S7s15tBOayllGsgyY97Vwj8MH5/YYOfo7CCaq7+0LZLJUyi1A27NTAYtXRluItTzd
cZh4x+PEVSSqTyeXLaw3IMiDmNbl3/vvdinq8i6C+4YJ2R7uBbRYoi3CpCGv92BefFwqb4bPFQvx
MIGOQQEQk8awlVe9je1kbjzZtV6kH0G8xkRohKu2Grk8hpOOdd6zfnWWyoaScJZoSLApEE3ao9uY
HYmTobL/6i05C2k7tj8KlCUAnVgyi501Sr2NgvGt2oEtzbaC3hU3ZGCXJeOyRT0fh0Scj6br4KRL
MlCdl2qwcXfO6MxmtW9WJ8vGAyoxw/2fW12YnYW+QfCPWzDgv4/JvbXNjgJEjDybVfUy5mhgpLxH
kVVCvmjwcYeRQuD5UITMIK0aQ8l0IinYrW2JdSDlSYBBujPD8dlQDALc/ZaiyElHSFmBgu7CGqHG
LdzgaqAl4BIqYqO9iMag2tf0/IuxamXL0K4Ul8akHftIJKA2RQbJL6HNna8+shojmS07Dc2APw49
03O9sov1SncTAsnIMQQgYovw4vbhLWM0Moyq93misNq7VGtAkpXE3GW8Ye2AKrbP/zIsIOkJxzKs
BVaAel5hWFojlRaOVv0LqMXleUwthaLSTfsqgWmneFL810bTCwR9Mdy5BHEQdS2n43m7SvQLf9iu
Scn0Kizssc8xSt80je2mdlSuGksCeyYuE7337T8rizXBigLOXN3uBngyja5hsZD633bGsWp2EaIw
M6c+m/HtNnYLjeDOAqd5l83INQjq9INT0uTtOWd2ICb3BAzoi3/aL5iT8+zVLHNQJ3fEQSMOkBCY
5Oh4ql9ED6Yb45x5kXF2fgpYdM8YrvK4wvUFgbUIlCy1ltImNKJ2e8FC4Bw09nTqUaQ7orIUSJWI
M/DO+CUyUCoY8iNwfwUFUfAU8qk8F67+9U5lgAtL2HUX4GzI/dImxErTs/tJdXhwPWAKtlzRJP95
AT8ro44kTzfAvl3PRro+H0ENqqarWsfYXsF0x6HdZVFPoF9gnzxYzJeKdYw6QgmNuoQoxPApAamY
iw9eY0GoYgMJq2sSSvvX2eIBCUwfRBjdsA22d81eHT6ErE1L2DzYuDPmWtVcLCju4mVGbB6Pj6jR
5SoW2JqXd5FPAvfKF8dcrT9JmCtLoeWj6DPgytNwS9x8gmDcZf+pyDw9RzUsFL8DKOAtSB/5p8bH
J6MsJro6D9/pshBQogN6MUlThe+xADS29DhKI4rcdeY+XZUrvdKKUwrlbT52Qyu+MuUelIN6ji2e
Pa3ebvoiLMZ3TR0n4d44piIqnOLBEXQJxLsMMdqTqEYqczWqo0SBAtfotkyzrYEV92UvuQgjIVK/
5IaWU+m12gavPTbiurxWZ7mf8Sz9GNsTnYjtm7N2JJFxfEs39O366i3iQiSm1Jx+QTjDqa+Ea0/1
rtIsjNr49ozNz6JeUz08DFjR9dJDRmhYBWu7B6cCyqHV/ESO6wpQOiUHqtbsPLwdINYWjEFBGtbV
SiOhVHK+st0loix5SymGwsBjF1BvVpE0IxvNKgITsAr9OgcRWeJhHq5O+p4oZ75lQU0DWPsZukAw
cwmCAlqkbsVQeYkP2GiW9NlSqxE8pg0t0jv1mtCo9/H/OV4LSutQKfr+0OXjWaQlXctuFl6mmlF9
CPq00/fYwNxIHzbgRTubmqrFQbseO/jtAu6vk+cjH4W4TqZpmqUQoW6D3qVonPjKHw97AJsM08HG
YY7XiBYZT/7On8Jo7SNMY0fysfcHGMqAmNmuGDQXYh6MaTBfGdjsIIjJFkMNF8CHonqZQ3wMohS4
B6YfMFUbu+ktNI8jjSqR5FwF2cF+HTW61h81lAdorn6R4A6TQDb/cS4SoIUdZ+JaN47G4zKpobaU
oQI3AIY3OjOHCIGLJXeTAiWZxoNaCFCV3Sz8SSBds/Wbu/GUqPlCmUFhiY7QP617ZziB2sadH2Ua
ksi6/BWkcE0YRX2yzeV+yG9EoAm+JkQED16xoIDn/opEPkPIr/8kKslZ2cYFcD+n/2AEQp+KcjD5
HzXOVJDDVi5OkZxUOJG38ccA1lPnM4sDnMwHB6hx5bL0uLQ5evfb/+p1MTzED1qdI0ne7ZSFt7Rm
2R70SboujiP1rnvPYwQBK2CJttKRK0G/G2M0wcJ/JzyqrtFSPhz5cnVjhkyDjr8VSXoXxDv25XjO
LEL7FR6F8yqRKHzTPQ4nu/0briZOvbhgVhUvFqMGFSGEKEz/g0u9tQjOWFlFS2jlr/RhGqfDwCO2
bwTKGUN1DLi3zWLtqRwjwTZQxqwXN9VpHsLnzUSJB2vbSHiMtGiTzYbUaw5pAVt2QutsgfEy6N8H
P1iRNtIC77BMm199BEOexU9ttrSa21Xrrd+zvaOJG4MUBYy77pylcnXm3T0AXqFlUn9fDpsQ/7yZ
MexdVQnBmAIRXf9di457QdAoccn650ejYZqsemnkO3mPZMGgEBC4o3k9NZvO9zWAvaMRInRnro6P
Q5WjmF71Ino/qfu6cFkGJoZN5mv+tAuPwY8SSW+fLrDNR4vK7P+c20XBBHgYgTowo9x3PqfVBarq
bKu/PoCricOK0iOyHG1a416SfWnhTTef30xTrx6yoMiTGWiV9ThwAW+1ClmYjd7tEN90FJQklXLO
Y/Z0wpvhzfuNwle36qaKNcnYJqxi6I3DITa1JmfsOlAyjN/1jZ4S714qzwtSbMKMv9AJbGZuGS/X
c7WjOtBt4RxKeb6O6wu826081I5zJz9Yns7CZ89EjtuZG4oMeYzet7I2uUwvl+BR1+e5AKo1yfFE
9VeOatk2cAmtklABfus+PrBXKFgUP3lqwa7TQtk4WxbimrppGZVIgcaUqEzbjyhFoZQNjTIk1f2d
6j5Dv8z95Wd1uj+6cB8146hPOCtRI3JvNKvrOXaJKv58q248AaO/vvLwVd5lJweujsY2AxM4LbKz
eJliQL7r9OcExPsRv8wJ16KC2vWbOWG8npxr3DwNmbOt+cbKQAU1GnxB5rK+HJidB4s0oUiSPdRm
f4miD+Dy+PXtxyu1HQxhoqAcSiH/lenCZgd8J45+Wph+PyhHLW6ckRvGl00gKGIFmuPBzePRHBoM
zuBeEAIfAHUW7/S4PTa3r4gfpVX7kRjg4PvoO4xAmsAJw2w+KmZ5Kkr/ft72aIJkGOX9Xh1aDl6F
E5lsvt81oPPQIfupKU57oeq1m11/ReLvg2dZaPdldx6WbKmeex4p+bNAkIHvF2yR/rO0t3LlTTzO
Ov2vXrPZsS7BIvymHZLGCy8H6rr64VFlvSk6+vz3xTijWUnVSbD8he8ORc4sg45ZoDju8pzZIAk1
7s9WIhmIE85h6b4AkIxjJRUfpYgyraRWgEWDOEfZk2muHVVs9ffPpYt04b23k+CzbJ1PZHX8a6Xh
HMSph/jGmgpmkqdz64fjLc2fc8b6SDl3jYoXOnULQcjXxkQ43Gg6rM11y/evoTuCpqEr1BAS50gc
5kLhWRRpU9cFAv7N7pzzg+kcAO3wb1hXVLA/19p823xdkbJEvYEnijCAIRRzWY0j/kTo9t/MsOXu
vGdO11kqRxRpqY4houA8q1TJm5titkqrtteTqpSqPPOFfb9eOqV9pw6917eVFZ5rRFmoZnOSv1IP
HAot6tfpO7gBJp8npQTINPUc/KcSwUztPs2VF30iD1ykgKxOGVrEFeJ8P0KuDTLHxsq2Nzx3C+qP
pCwy03YeL4TK0OXt2HxiAjeGCnagmkax0zFnMsUPsZFRNk8LZUMC2rORw+kunZzFx5hfFzNh1Lf+
tiS3O4GpMcD70TNjSE+xDwn6Csg5DL96BWJYt3kHDQqJ95b/uufqGCwrxs58MuHcKLtC6c3vEYYx
OsIKPMmQefea14TBhy2j4145fDYcutf7fyZd/w7aMU9500nSETLIOTRTW3zewiHWLtBdCBjMTJj8
Ykj+REAl6Q1dotKHBX1Ub7ELGJEyeMYml6vT7vUnKrVnQMfbJM2jy/nCKDQuPUrHS+0/oDtM95wk
zA46MKvlZNDUOO5pnZV2GheHLu78m3NWBoCJi1XIrGBh3bIzj30bQvCnUVYE2D/PRYMgoPUS7KNg
22tu51PZMQxOX//DYqV6Z4DTcRYI9Tflko1EVUztYxO2MsZ6dyUK18NXl0eg2C1JnDCZDGlXnNUt
gm2+emxY1ueQ0YX3YqPW+12xLnHan2sU7eVsR7YN4nLNsEQg1Hg8QidblpI6UXpjX+3HI/LqUm+N
o2RI6kmMG3I6Oo5q8w++Jex0kX3YmWoJpREjuzgKGhk+F7WmqRILeeT3cWlGzdVElxgGoE8SrG10
juFC3dHfna5DQDe8FZ16SeO2D+CHnZDPha+juEvrGdsEMTkhYWMZrbuRJYDKNa/EV1lcE+FmTTby
MOFaUu/i3vXlTsElUbXgEiAcor4B/mhldwXvofg9Mrzh1AD70ab9CnhX0iq3C1jzUOJ0WokjCHho
mZQlPji/tKtvGvrdjmJESOfqGRDjM5GxZdwM2WHfc3Zso8eeRFSARdnveEc0Ncn4gSl30mk2g4QA
3eYYq1LjI5jqX2ddTjoK46IIooL9q8vl8nMzO2weo9Hko9Si1w4cJTeYFX5AVzT9p57GdKyo8Q3u
yHnjPpbWj4Qy/8pxkDPbQIGJ0rUE4fiy8xvR51JSJHe7IdXeZ+hU2xW35xTVN0MgeGknm4NoW3s0
dVdRCX20lpu7b+dfYn/hvNd/76Ac0q6XpiKzsnwOR8k58v6hltqeVr6x/U/k6W3GCMVyvBAWEinj
uXqjUOy7i55cbzTFldkjrUmBKcDrlqcU8O8D3hmzqjMyTQ37sTSncB7hDiwoGD6thJzXqmPmR+4e
ap0PfVGVQ56KC2pVZD7rY1YBN4E0OIIfu/TkJJXqhNAH0OsvYKOIl+u6+hJ3JNy714Q4d3qwpmuC
gQ662mWwKrpjPdwmEDDzU+4Sizbz1XieK+c3tUYG8VRvZ8D+US8ZdeHiKEgD59KrNpwexAHKThLI
dNFptWJasuCzrGIAMl1dPdvR6OQSFGydrJQc2UKcpDu2vnlvHHfDwlVDFMNZN4oAxxLjn6I2gdpD
Nb8RH1SaAlhfVx2fkAIk35npKY0sINY/cv2/mz+Bqhywy00Fww5tteAvFJptl904E5Xzt5AAF5qd
TH7ZmRD9jDDTFm+/qzrGmnx8RAq7wRLvjlcXRvN5o2S0q1MFgpY9f5/oAbqG5LtmdAJ6ruKEZO5b
tTViD2U/NXBCS35pcWnMmNJ4nptm+X4SZS/0QrR+PLqJ7ShrFXwQHUnxL3JpP+EIFBN6iVt9mChH
Q/HxqsTC5Sc6pqKYMaCcGtcVPEHTazgCq97yuV0s7t/2MZ3uJBbo9o0ZyIYHD0UDF1Mk3I3rHBTL
ZfO6vtY9uc+5/mw7TuoDz3ax20QUi/weE2q2U+l/3rvw7acIJrgMVosi0o+VCJNGGcVZWOzo0G43
W+I6ph6p465+JQ4fEZntZ4et+g3fcUS0pp53q0fZ/LC/gsGZcp0Y7Xp9Tn5Z35BZchbGBWGpqKeg
/HbJqDgHnuW/4NQyPxXO5VD5R/f/fti82zv60lhV79HOmF46LMP0LkPmJyl/hNEJnTEHuh9HFu3f
VrdhcRAgqyCn0decXQQZH1fKXM8bJTgrfdeao2lF8sOu0iUs+vC+14dT/k+2x8b+FC7vV6J5TceB
QvItrb2IjFxZ5pn1Gv5UyxaZWqeKA/QIoh+aZr/sRO4Bfgc74yg5InDHx+Zkqvid+Aw26PjpeJz1
6o0K6XNIUDDZ4PlwXOWBxe7oeBO2CZTowB2Mzo8GSc7mUTmtSkHgp+V9Rs8DtCGTwSwgwTujCuZD
2KAj2u9dC+ofJh8NErEc1ILmKWFnm8HZfhvUa9SZh0pvq0cItO4ab+kN6Y4Sbp2Zt0PxK/7flJar
kZiTznCpRbf1NHVshSRjJJJzr2mk7uEJuSdUbpupISsXK/0/k+BnoxxDoqfN1pxaHn/E9wOu1Afo
Wp4ml45VJ1H4mNSu+0jTE/NU4wv1R7co0Gp5bSaG3A+bwraP0E2vnkv8H06cnVnNcpqf6YKt63Gm
tou+In24H8g45utS4lWTsGNpJFrivFKX7D4hWKdBAskZYqMXxJIbXUdqJIsgkWHhLLJ8ljCIhrgS
cI56EJwB4DHH1R4kaEUvELrzaF8Aj31nF1yhJQEYaMqxD9iJdXgG9HQv6rUIVLPN14vQG0muZZaN
zepugLsWVeHQqxTFsnKcV0iMHr1PBQSEkSbIxnwjHCs38wBYFzdW3PDhNn4F7RnPkeJV4arhGhfb
eNSb6OlAiYZ2giZqrBtyb/CALlKgo9g+2LaF/rs5iLFMZ+9FRo6YX6MQv0uwy3IcPUlGqVKTQLAP
4RrIX5KxIWflZC3+zbEAKMj8e+TE4BgBBoSU9NBuOclQ2udo8KEBe45fw1X6aWJj4bccc6FE0qrC
qGEGLNvlntZPCd7TiD/HDaqpFvVWcvdmbg+c6D0hanJxjCd+QKqNalwUpf927mZVDEliEq5/OqDU
6kpmvfWhM/IaIuzREXEHToRvvSAwVPo5IgpWwflj02+sMl6G5juGm5XWdYngXkd8DA7DfjYQiudk
AgnsxeTPl0vb4vfP5WpQOisxRBu/wGPrr2WiEJ2CisVu9DbhPvfhHbyVwO1gov0QE8x1k9ygZlfW
t6RUSLTL9r0upLmMaTdKFoDL2F9OXBEGkcsnznkmaNDvaKJmVL4dcoYvQKsRzt9nP2V6+/uHuBm9
Kq5ebiGivdivHxHBCMomzPCElJpzHFrXuMAgLE+iZOgC5XDN7clAtXl8ant2F+hR43r2R323Kj2/
e8ltr+43FmusqBc5EtQzkGRPyXV9cZIajHbtOz2sROPSx+Tzos14fhe5nlIcrvJbBou1SQaCrLck
7DRDME8CB4oy249PuLeCoBNFrtae5LIRpVT3EU3Gu4vNE5wE6bu0lgBFSKwXXbPoaovmIM+BDdiH
gwhOnnjL8YUpaeNUjM3JOhknXp6wNypwa5vYIQv2c4yq6NP9rWGqIbQCHNLwLIP03WW93Tc59znU
b+OWW6Fh5q1LHv2zSAYeOngjDCGilgO37uU1YMAi+TQfYDpJ9VXjGQrxh7Cad12uGKMkoK9pqS0o
U/w1467EhEZTXDCjU6sM1BT8x9+v0Wc8rfyRD20e0KwnDys3NphanWHfhO41Wp7NMt3py8X4XTky
vI36uNAuXT00pJMRAb/7OpuFbZyNbSJfBdeS/i3Rojaiwu3PbYlhnMYEPQ/PsJVi2u7ARlFeXQZM
efp7DcXKmTYNmM/M+uhSevLu4ELpYXvfzK/kDFC/KhESheJQYDcEZY2U+x6NUPTx29HAqTSV8JLU
qmfbasPyUiKEipxwag8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
