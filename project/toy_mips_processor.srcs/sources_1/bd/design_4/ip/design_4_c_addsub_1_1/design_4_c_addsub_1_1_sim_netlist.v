// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:10 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_addsub_1_1 -prefix
//               design_4_c_addsub_1_1_ design_3_c_addsub_1_0_sim_netlist.v
// Design      : design_3_c_addsub_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_addsub_1_0,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_addsub_1_1
   (A,
    B,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) input [31:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire [31:0]B;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000000" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_addsub_1_1_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000000" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "0" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_addsub_1_1_c_addsub_v12_0_14
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]B;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_ADD_MODE = "0" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "32" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "00000000000000000000000000000000" *) 
  (* C_B_WIDTH = "32" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "32" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_addsub_1_1_c_addsub_v12_0_14_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EJFZwtxl4g9/OL6+bopUV8BP4e67HNukCIy7Ih3E75y7soa6GhqEucPXMiOy+mJrcrNwD+HjZ0/I
BwEKIiA4mA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
rZCGWdmPJXoOuANoS8fyUXk7SyF+uTNJL18BfeKc+fxcyRrCB++WrM02adxoUdICz4/92yY8TQgj
xyPC0eaHZcjSLepbnHHgSReIQ1PL0hmufLbye7QTD0ygUXC4MvFVY8s3KeW9cPCqOxkyCSziJQzs
J5OT9XLQno1e9rIBr9M=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
I7Zo4frj3tO6FFzeDhpSENS0yd34dQZBtiyIrI/GMASFBUeny6muOD2l0HK69ImRJIOyobvK1+9O
DhxptAc4NzRpY4xUZvr4ix1AhM1Kars1OkrQCWz4a7ciGU/XDblidF3IL0Fa7c41gHIZR9c/Usa6
XL7UEu3aSPQYbZLSDOzeao4VtSSn+dCcjsH4X8zVjSqXg8dcN3fd5C15JaMYg00F2yOFtxwWwZWq
Yvwe1q1PG/wcA1cKAOscANbj4o3O4LjfylNIB6L+Mssxosh+e0+oobWNk/ouBa4k1c3/IzXGSCAs
hEvbI+iqkWJJKZrSb9PZk7S7XSJcScrJO/DGkQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DDRecdVJcCPEpbUqhuwKtKWXteF7XhGc5d+lQn2uiREzbHyuZvQ1wDwAGGrPwE75gjqc7CdHPMOY
8+3nqcEwR4Q5USgQcou3Cyc6C0TnzzDD/dLKPHDWA1s52x8Rx+LBH9WCvBpD5BKkE4o1s3rN1tL2
wTdCqzzKD8YlryKQ4U0lr2bX6Mlf4/nIt2K1eyPKbIrHIvKDThmaIF/qLnLnkE04pksWJ9Af1OVB
46iqBssrR5p6wZc241D4CqSRCRamfP/s1JrTi8bBNCcXhC0f0Aa35UAoG8vnFngHlFd3G2J88cas
Fo7UH4k1BTTfgbQ35ec0XfSbS/qQWS+EgAF+wA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
L11p2bsABDhO9HvT3IM+HulCClFvs/UPexuAVExicKtzrLN7tNvUjSouZSn9KwAjR2hg5ZIJ23uy
1elB+eyEl65vQnoH4+s6Q5K4EIcMo5WVKfIKwgu5Q3Sg/jYW+aWT/kGuc7CazRsTxJ7XPFndpMIM
cxYWx2DLps320t+Be0c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Uublhc2r9VmPPq1tMATsd3XJltn9QRg1/PdCtSlxgFBDDAk13md52Fz+h+DOWptR3Q4i+Sx5IhIP
QIONVNTf1DnoK/wa1lkbd1dROJam8/cZQFiIxnsnSPGXzOGoc0c04xDSCJCCDxiDMF1YTtAqt6nw
yZh1RwOhPpgwUKjeJ4o4TY6/i0xuYAYVc83O6KwI9Ywk9UsfyIQQS8UXFo8zA9eniU2n2NcyAVNj
Y8xZ9PYJfzfDo6dHWsj4Ik588uhfO/bmsf2/ZuY5HCAMQpnda9XzPkVomNjRfsUghko7KipIl2ur
aHh+4i2kI/+cHaihhw3z14aGidBkuYKaopasbA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VYqlyQSuRywWcSrUprXX2UzoaWsJXTTbptzDY9ycgFR91H2uYfY43f80gn0E87Gvj90Qmn0Dl6ck
2VjO2Zn9yATmqtuzi/Etuf29dkl3uyKtk02OitZJEhD1CDyUJHDXKHkPMXOZCBU5CfkrIWw2SsSq
YuQKmvxp4BrhcwXypr+vRSsYd1liMxxuXOdBN5AIyzibGfcR4YUeOokIoP05xZoQOfPQkotMC1B6
SHVKEaBxe37YkyKAkQ0f9eKfnPPLG/G5qeLrFPAiIar0HHpOvdCOO69vi3RG1XqoxtTm/wGwRb5J
ZqzZyTn1Fm55PXyKhlElzXXAv1xPOTbkJXRZNQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EktM4icAEVQRmfzXBBFeRr7d3ZTOU9f+J40sQAiff114nDU+fxlewcv+twlytUk9LMSR67RJlLt4
+ZBTwcuSPZ2Cvrommkp++7rNze0VCD8pSAdj4uo1ZnYWVWmPMQaRIqI88lnAzc5+T/LxEiXKn4ji
AYGs9fja4ME8C0CHbBsg+jfUryleVk1D8jEMCetM7qDx64s/7AGfwzDqMiW2DPCPLKNUsdlOlBYT
JAOnfy6deN7/o7BYxBsE1P4Pib1x1hvR8RwEm38pBOLKGade6KL/1SHmz5N1KGLPSXQXlK53RLTI
Exc4wN04Kg72tf503oGq6Vp90c5pksQ9cc0M+w==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qzYsaSn6YzxyfrxIwv3eyowRK7ZyzZmQHzUmV2AITf6g43c7IV/fwNBDik+XFhLScW2SxsyaGGI7
5n6kAt9uM3GerkCXA+LJQrqshcEyjuvm17vWVovBURqxhTARgZaTs5OtXdhc/wLi5e6lsdyyLtQo
bt66ubjErMgf5+tD8rpn0HkjUYmGv/MBZ0i4bGui735H12aK+wTfhGVOOiuWHCk2zCJJSx3vH4sl
dKtlpg4W0hPEM3TBPHaLnOpIDkrIUaGGN5fm6NJL6US59+Lr8/3mplbD8ld21OKzgLH+5YPRMoo4
1Pbjxkawu5Kk60AsuaR/OxngawaRMd9N4niRfQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UO0fSmAGZhetrUJrxP7/ipTT//TI5UceiYrPeBtEAM2vtD5Ua0nkWu/nvW4hs8sh0k87l1TlZ0/A
gNvYyBTJKpZneKXo1I7IO+cAuyDrKEYY+B9sRcFxEa6YHNbF3meI4lASEYL9P6qxJL+5sa7tOIWq
8kAq/DAhGzQowPmSaKD2dSVYXo6L3g+ZLLhn71kO+3W0iMRy0pAnpomLQnESH+W+A6cTqDru9W/s
8Ir72x70ZmriyDxYMVl+MtLwa96pJUCsvaOwNlajHUhEihiTRDZXgTXUDwm1U82QEJ5cuj7lDR+I
qKNLDFL0w5iKQ5kvBxdVel1I0n6k8uhksM77fg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NZ3JwPQIUy5RIIO866mNWRc2iewl6oeL55ecS4QaV7n+OAVZ4uIncNNx04dJCpKSIF7J0LJ6X7zT
dt7VHpP0nRz7Cfvv9Injk8FBAes537+uDCJ+wl0MDXRYPYXfOYOaH5+27W9nvcTUuQqr5mG5dGmF
lejS5mTcxhYmA49Cur8ixOrvCUA2YjpE56jD44tks2ELl0EoQXQOnnyqArIUMRYJ3z0InpxRRQYR
NaRZdCw6Ec4eSNC32te1+cgsyyzjEBu4r5+T4pNESIpzWwzYYwEiqVatrJo+aVlw8F4e6IqZ1V+D
QQ93id0e27R4FSNaA2TqrL9lDWMZw4lQbhretg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 16224)
`pragma protect data_block
F+rQ2ODGj7SVY4c8aFex/WS6isVOtBKim4JfddYplj5rrPg90ioOgM7NXm5d4kA/9BBkQVh/3in3
rJA2wgCrptDd6pXShsw7GlM03NNVWZ9VENRJ4L/FvSBRkMN0tO5rcQIuQ56ZOhxqRPeesAQgrq3J
LufKB2vfV8nCDpn65/kD5L5ttmfGVlgld5F/rcYMF0j638A2vmVeaLhjtEdDecaNc3j4BfAIYNxq
0eQQ/pKaaW2vlQreb/M8a0uUB7N/UMGeEkQx8lamaJE27CMngFF2q86D4gW7TFP77hwCMkMbjJOp
65DGfWLmz1IHMVkb4oLQ6vAfswlZb/EXKNPyHgOg/9Gg1y68KhIi+PV0jryu8F+8MvwbwhqHbzd1
1nlGt6KEQraQmCyBtDEnPYunH/NV2MCuLNzXQK80rAe/Cyb3CnKebht8SIs0wh+3oxrwZ5TWAICt
43Qjh1+bxfC7nmAUiSiby6L9RHS05lOXwNW2T30l5hZ2jLlsi/y415k7Ca6HmsbD6UQ9nRJ0j2Pb
dq594NoKiGH8VCZRJZzEZkxfYFQ1C4YWudkbZEQpEFldrPUr6DqZyodQiwSrI9j8Mm8yEWkiYk55
0w9kyKTVILNjlME2Bw6Dz56rzWULY+WBUItTk8wX67itxUFO8ODvWrlniIUl3UznKPY2RBg5vmIo
vYn9ljHkpL6K0BZdEKRRIX3iI4GA4vMftS86z0OVOGz/mDKxuAnGCdJT73xiqlMmZ/puYTjS/iSs
SXThvGKF4exqgRKZ+UzSghGR99+wBCvyM6NRUdts9HgST8kJwsx5l4Evo0+rydEGGYpti/j675Ha
+tEOrE/ZJP5ZVKrDSrHvpvmW9XlNMkuBwPIoY/6yfOoHSDlOagRW3XejeiDJPcMi0VE3Jpb+1IZs
Pd5Homc/SLvboavrfgP1V3NK8H8QzlGdghBVNtmREDtRxRNNELcmOPNiQJsXBhgleTl+2tZuYzwG
kEDzmnJRR9kNT9TqEmaXm3z9ldihXr4aS7G3Hz9HGbvEJpkpY6u2Yx3AGQk01/kfj0ACcPiikkVe
J9giWXyqg8Go6L7pwCzgU51XB1iocgy8d/Vch8lbVhHhC939d5qyGHyqYooF77yZdwD9QMHNEjyW
tJ4tvN/nb4IJA25DVvZCF5OiC8DvJAotRHQuaDlN6/nzj12QWLXwwF+MZDUqMn1AM8ljHionCVi0
MYH0QM2J0uqW/OkqLwmuts5e0TmHYD35bgyOX1pzfyUow92vvgC9SNnP/8iSEYAgZ5RLo+FXbKaP
89mBowIz85DwX4dBPvbxmTl8HOdxDWSdYpyzMSLfpL4SothBFTCkRzhflMYXvpi9hG4C0xuf3bxm
Q7hXkoG/uwFP5XEeSX53NHnD7ZYeqik7pNUmmU7NNTjfJZ30v6KLDJMIe0XytLb6ThAzS8DcQDk1
ZOumHTBzW2Fy0iOa4oDqzf7/gfZ5Vdrrd7z6k0lpNYqdXmwritV2e9Bqd7HmObys5xAsriBMjrB5
9t6SCo+jL6/QCdXzQCCGxtPSpuQfsm0UJ4DSDIojv//S2q9DV60Ds6iTLTLsGZ1TCIwrPh4Eq6jw
n8enVS+MreLEYdRihNMbEPIVqkgADIVmZr5kVwuEQ5tybOurhzFLF2Ap/CqgjbHwAW/wWCtrisjB
O9kXTIwD9NXOqPf/c7MER/9D3m9IBLLtXD2chRQSvWibks0WYUiWTKAVzVWUmTI+6ZwJamP17w6x
ce/JbR2X3UpLrn72tUv2By06u18Nda78eZPg7hC+OpOmz/q0TxIPKktYB4cEbf2n6o0pBduH0uYg
YFAP8vpydrObcYQtrbzd5HPiaRTDkYUfldSaMV8y71QN5PVZYGiI0xbCZc7OzuZfkP+92uqZO6h9
Oq2WhFmTpGUg+cy12Ni4XX7u0zOsM+DN6WdffrEIpjvAz4Y58If/bEX2uywVdgOFdBbnrMkjOimk
mU/JHEcauIqccd79qbqtQbS9gnYr/rukdf7Anoy95volYS+toAM2XYipLS/vBlxc6J+1lcGw6nzq
o6WmeuZCJSl2UV/uauJtWwpU39flI1K+0sNsp+vWr8wxydMRI4tndPxXFPTtTHcamqxC5+128mXT
hdJVbtKN1IQyd2g/ADH4wdxv2aJZ5QPb8IoRxWy7DqUL12xbSWhxtBoTB30X6Odn/PgQA4irPUxI
epeO2HRQ/LFpD1h0NANMq9wRTeqaH4UaurrV9hpnCFxCyhZi9sSn6MuFv92epLBEUWV9mFPB+82c
DEPXh5J24v7eWcoyk/ii1fI2BvWw6fWDshuwGuXG16YdQ7T9zM0iv5IQe86XZ8t1L0M0EaZn6lSJ
rrg3zaE359AdCVaEfvdHt9CCDrRDMyNoXY7cejyik95k7Mc6ecVpXRqXlRwZz4IuhdNmkgYW8N6n
eb37l+rT4IW5jN3iCO7S8K0g0SvykN95aZN+wjL+yamBmYpWmyeXPYyo1LfUaQBeS2qGe8DidPic
6pNwNg8B7AH1bqvKFU5jzMYDROtyvzHUNMvCSI53ndyfhuIjTE+nwRKUTn22+SctO+SP3CJckZ02
DTN8axI8dIlPxwBfrwdEXT5TP/fjQSWmVF+/EhmU5J3wrxV9SnIixD8+w6cftMAnCaUdpJ/2+Fn+
EdbRY5KIdthXk9j/dJGcdNmzZSvC4pRIKQ5IHGtFyDy3aVFQ7zs6XCpSlJJ/ZKQWRjIOVAoNra+P
Hm4SikaKHkXyn/KB/+u6U08h8RM6qAu/zJUjvBsqIKdRVYDtrQDUeWAdB5Vp1CqqDFMzmhG8FwEZ
mk5yDKxuiC8KKhtR+MRNcrzBzQ4xTrnpUrkFnTO4AM8BGJY+9Y2G8BGfW0aqb3QDLiqI73erx5uQ
TkpZOu8HlX9ZvtArXkvYfRX39EK5x+0LRZJTezovyaTE5IpwspgFL9lxjtW6In9Y/+o7kl+LdEru
IDPl+6dgM4r/CDTKx8k/W0jOZ3EX0miySCMp4gZ8TNOXmx7vKKk/KbfVL4bQ27jv6G+oY5gETxD4
nuQHeM/lyh293qMa1y79VGXD9MUiGEmnMREDUAp/tfkxy9V6y5WG6EJVUztSr9LxS5C6OagBVmEM
Dtj61STATz9hmpn0eIOVymF/JVXdX4QL2sOFHHJdvOVBBfLQzzbH3CiERuiTpKVOpxuK392Spoqc
fhk8Qy04j5sPyvd46+J+4IdVauHqn12XB/y5ReCiyhYYHDSinL3PvWY0MEMrWQepER+rqq315sph
gG1AWkPGQNp5DP1DweXIEInmB2XD+sKgw9WR8XYov0t0KhDQVg7eJRmJJWt2qsy7pIACU4ERrb/K
8Oeh5mKJhp23FRfbifEYts2wxeACbjn52IGsYkUl45vgt2E4fqLPtuYeClj2gYklkZ6T0MkZY8uO
5ND5f2ORjhx4CyGG5DrG0cNPmIH0gZZj37T/K/OcLDibgksi+J1mFtrTkzKrD9idXSMTnkuSBNzl
SB8ETN73JoMHm1niQGWBfW//jyI70TRHHjHmoOkgUQX8TgE8W5jmohXvm7z8LK6C/R+TXp+tzJnE
CSdW6Cn94cirob56SI/SIHmRrF4Z16D5Oh9qH+6+7cikCeGuhkMfHquA6BlVBHVJXV/EIknPHmic
fmh/n/GfeFtp00ANybR/11/mcoUA8z0c++Hav4acqIJe/R1EG//+E4WP177rNPuRJCF737ms0E8E
aavLji9HFv1Dh62vz0CuEKr6ssxss7AVoKTQHvxcX+jkRSbqO4T/pvquMunF0TqWUE2ZHCkh4cVP
yTSY5py/PCXSaemBFxTzVJziVoYASxSrIx4x8FWmz+nbTfZfogbzIZvWY5SYKHBcM+7PTqRQwWzV
CApHhqUQ4gelza7YJ0OPopS/pNdR+CvX2rxaYjaC/mUUSYe8Gxv0WlHQkggGlbcrePUWSy/6yWZ8
aGok3tREEeuPzVwJCKn/DQ2yJn1AWWis1EWR3Dt6MJCcYMgb8K0nFjpfjcU0qsc+2feW6ZQaxreL
cBP9pIxP+i+WldtIPg8xRrEKY/FLBuvQMo1e+oFhfgobwuiq53ZNa9cp6lczmA8oiEIrwa0JWI0U
hxKp4QnsEa6kQfyuEboBDTgYSlDAezW1qVFQa7b2GLsGHtHbWR/sP2CZ/T8tlogTF2605z2XdCvc
wZlMkBWTdaqmp78vJ4CIPAAVvUe7dX9OxWTxSd+du5sCfbV4aiWOpstG6Bng5q2nhLEwHEYB+09M
WiC4NlDawz/Q0BZpjCIvPlG+mWWv0aMVKQpjjMXxbv3eFxU/hcZH2hR/jHEd4ATigz1T/Sw27yXq
7eejzJUGumHlyR8NUJsGAtrByqFIBLJjvZQlQlCZw33SaSQWtooZaspIpdLkVvoYtcoC3WGpD443
oklpvWjTSmmlCQGP737ExB8hg+zEg0Jd/B2POuZ7dNXHBW94O9TB49XhcM89LdGAQ1TnRzsx7lKk
px5QPAT/UV90HMmFHC1ScGUxYRVFqj0PH89RND65OmHKXX5FE5PLDxg/GI8qADNhVm61iIRXw5N8
uVkvl//lG44XqtMTHTklXdrQAVspcl+SGtffZgNV9qDqAGQUClPYlD8ZhdrszCSRaOCrAw0me7Df
Oi2B9IaaEc1Y47A5tVygg8RpFF/ZpOUVRc5EqpILEHxlPnlpY1kLiMzcVlvgJJ8vKJYgsJz8vnlL
TgJ04uPFC4J6H0b5JHJQVPJeZnwE5VcJK+xqg6r2nwy4CC7ekz8jx7ZkugvUkpDvruVlpoP1UakE
neb8NJeas5qiOne56uG1LWuva2KFoQmYM98wGpcCCl8r5l1OUZgOx1xMZ7o1jaFTGCuV9poCHsfW
hqifPdwwe/V3p9RQh8Yj+22O9FAZj/UyGPmTWNBiP3GfErVV9gnZPvsFvH9/M/b6cxYXzVfrSOlo
R5dhRAJ2nor5P69q7MnubsdfkoyM8LnNQ5oe9Xazp+zI+BSC8Z0SAR9pBaO6IvQl4S2bSVibxnUo
RhSCdJNzsFxSYTKdEGk5SMsK/zEW6N1vh9YXtrhB20374bO3RuKP0D37IyedLS22FnZt4/9kun8+
XIlFSSMovfVwFWOF1PJnvTrBNnukWwbxMYBikU/4isn1IJiAbPEa/ntKCUE8ll5jntOsRa7WOvlj
tjdpxQ63DNh6b/8thXb7lsVlTNycJV7X6GbH5+Z4gMxh/z5K5h9YHI+X9dszeVLIFRdTA+PKGMn1
F2zt4z/FC7GAilCghGo+MJLlto0Gc8DBchS9F/bFOQLjUEuGGWRTUbdDCafifphO/z9cxqqcMPIf
DmaSipkN4EHdU+p3XySQLwhCqVHNmTol/2yht+0xCzWlqaY5iNRgCq2FhvBLHNfhDsmYJk8g+WAi
qdAfJbBpJqZ5vgRQQY0lKO9rlcC5LkkXjSvACJp7TpzzrrgucqBLcgnUe69chkfmZXUmm/IjkF57
mEUjf53oqnRN+TmvWUIKlp6X2ULrPknVRdkTA9pk0TncCngdc7OzmiZDhhzM+PppecvUCDS1XWvK
jIgrdmUdiDgbbhn+l3Z/DxssFAySdftkkbetFNqYER1Vg7xi8AezX0lBkY3CLubGK7u6O/StUxBs
2Tf5qFUzRis7UXroReYMvK5DsG4aPkrTl6Dt/f0X6AR8G5YqNMU1xDNNvgf5N6S4J/Vm+ummBPh1
92ceajYJM4TwwKKQZZQ5S+pvUejmj2gp5j6YCtl2pVVVPI5A/BnnWeEwry/fwsLqDzI0yOTNHd42
hjsVlUL7Jxmd0CZsW7BCod7rh9JdGmrUPLUvT1n/9McJUhLkKwy2afiiLz2L6/KzlsGRpJD32Xl+
KrzRcP/meQEOgjobz74NcTBwz6V6UY9M1btAlGX608VjomkV9GKWb4odJVjbw4telvCvaMGtB3Y0
5HfT4p8cPlZ+pN/biYiD9Dmqg2FsLHzV1CNmRREZ6Fo4NJ5Ly4+a2T3kjxnoKUO6gwj5nIdgDxhJ
P5Mbqx1wEIfdQb2v8TK6MQ1Q4qOW4b7IEEFyvfrBsHMVutlN+aXX3cRlEz3CplelzK4xjEbcX/vy
ePcbRhYBVS5C8St0XGxoJYRRW5CzgbuYQRBKnuMzZy3PhMN+UWPs6KQ0gMzXQszpqOmMNCcqMFBe
aA4BXn4j+PzwpHo1vJZjvVPQqsbiLUuIzMV0O80cf+KCuafppkQqlli+CozDTG2C/OPay52i2Uh+
29d5Um/ELklTBgpB+RTDU+FQJgIPvmh9FT9PDvHwSAjHmwLMzmxuAtGuPIGNsI1T1ho00eD3gTyw
hUb/ME9Ab8bS1K5Td39CdrKLryWfTc9x/05Xr6oLlb/X5gz4PIHSAs68v0PF4ufMGkcRMd6u+S62
iNp8IrRUcSgLNFcZmeAvBHxdDVibdFnhcRd0KVYcp/2e+rXVp4dGhMWCPmRCrpwM3n7jTdpBlZRC
f24JwIz6+my7z4DeilRtkrcnmxzk2aOEChDzaH0D9K9AbOVnNBAAhvcaTxLPl/tqg8Pq7EAKdvCx
Bj77YOD/l/Wtn2WvpRe+mmT7JYdDArLpVlkZnTicsZgBnbKzj2VvwMRtiZYVGSRAjufO/r6dnP9V
jOcjPQttWlcEcuPkYVWDfsqMAoCSimNhtt28ivN1AdYoBwmlcxaIptRb1UkxhKluaGgOuNeJA5AV
726ZeiAQqMqW2BeQTIpbVwpUeqxESeQlU3xBoq/IPC1oBI/+QIQjVJ7ee7UsAGWsrZ9nfrVexbpP
NBcHLT+8KYq61ipGPMczHdXDrstF6dTlpxWx0O89wd2WllHj/BrARy1bbEfIQf8y+Eer/bxkU7cE
cpsePRF6m8SKmcrRYus3zXaUmOLfYLdk0qPu+vpLZTSrMk3j5T3UWiin1hu325E9ZzVA5vXYe4Iw
4SB4KHz10zleo8SXve3w9UuB8hXtQC8mgOpRjwik3bwi+/riOriacM9pEOrlJPvbcALLhNeNUMBv
hfAFWyZXFtXvIkidb5+fGJNeXi3z3CmBgwHMeyQ8Gfku8udUqr0QcA00gKe2RwE78A2lEsG1D7Lj
F1JwXC/4Gf1kia4pJXTwNH/kiaqN3gWlYVX9pnzOQNurYHeQTTvJ8w2u1Zis3BgZtDoJo6ipCdRY
WSNS+saXZHxnw+PWmz5PuFOkzYYiG1dIyMTZeO5TkzFssAetkp5/nlMF42Xy7izPlLvw1ZQQRBZp
nGfuGQEoMEQzgbuCcsVrdc3Ry0Zj85K6kNjmHA4x3W9cuNkUeEgfUplIqcOh1qkLN+tzNiemcJn8
aHGDtvnwr+vyu4uyx0F5AMKTNrfE06iZsRY9XfbVdo6k+TNFTmnqfP6m81bVO8+SlY7iE+1nst7f
nLIK+3Ythlrs9Qikn+YfGXgxP+vJIUNDoBlwvBnKzE1tVRzdLTkWCDnV7aeE9KqORrMk8CQKhoAd
78Tfvc2iar0suwOIe2hxF+99nmeAJKa7w3adANcK1AFnPnnB2aVjTEC+r/9WGgStb6EqLrFZO4Fq
fel0sW1jkcJoGY1xf8/VM+grl8TmVv5UKMa0sDeSuHRbx+iJ1sm4ntqRqbYx6e6wIoSgUL2tuP/g
dfWr+/DtV8Wbbk9fxmw1Exd6bof73GwgJsbnwzie6sf/201YDxY17wdflgKHqq4n8/UP7cmj/izk
9jcrHC6zOzT9gmhGieamxfXe9bsxBuefNCFlhF0LVyA6mGGs6aTfHNL3pLteUhXtAsUgimOf2rVz
l8YmkMC0v2BjgZ6gt9eObluPGvfr95inyQSw2kyjAKNh8pJXgxWaP147KDR9T3sAx0wIN6k+yjun
PWXqc+v5NeJAXsiZO9VnQKB8kcEbL26tpR3dzOKKX/1F6GKgL09BiSXDK7UWLihCYIgKUlN1nvf0
vFOFX7mNmZnp42KfTV7aFaSOne9+dhYK2aXg6TxgcpLEqzhQ9nW5BTpdda5hRjU57qx6lmn2NUJU
E/wRt1SZ3hOEAAIVfXlx6Q/szfj3mVhvYlcgi1ykPs5xTRfCNOQXsgrhAF83ji0YVgSfuGyFm08N
4I9W+yLB9IRvSHoN4QOLXCUzSGb/DGuI6CM/z60ZtSh+UtyTZLwO8IgM8B11EZ25P5xOtVxUEMoK
PhGF/Bb9bPFQC/kjFWWEOHDMpV5mvLJEaDPrz5HSVEDHIMWwCln+/XoGU8ePhR0pBz2I6edgj1Ud
/wG2g6KbHp8s7KOFougExBNiYCO/016qngTWbFwUXI64xzfIJwAAiCnc+wLU+AXtZw9Am0haO0Fu
93V1qP9Dv9Qlgyb+jDcoNZ7AcW4SxEJcHncfpe80VZczHXPtFaa29I41DxMsVcg48BO2rmPhW/bK
h6hjANq/PbvENpbHDIslEpfvhyPcYknZt7vKyNdZ7WSHBpeS+tfhHL66LrHxTBWK/Z4Y70BTeOxP
rR//Vph7ZRDNXo/1t37P2YkqPmQmHOSAnMye611D6fSMP19Ljg/FAtgEGER97GaXLD/pLvR3QE2S
h0qN9JdEOnHLiisc1aKqpV6ZyKy0MfAoN4h49hl75FrnhudOlIZuYe6EteawZpoRqDEFn5Ff8h+k
Lm2Kk1p7WHhrXRifKAkpunO59Yjc6JuGskwMnWDY3wgoN3d4M1ehUa5sFUS9ii3v1k6G55On9HVn
HzHEofcy1JoXaT6hCsPkZYyMebjHK48hwOEoKAKGYAfyvGk0ralEHlsKBOYq9BlH+acmW+MdGYx7
dp7LF1Yx/ddg+Z3ysGurZiPfgwAkrHQ0Pumh9fXs5+uNKBd2/LKdoIA1MdUecgcncM9pSCyZxwJW
icSkDfzuPWDiD9R05rLsqfQpWcrhT+gyRQdFhyMW4piNuuEriBlSOPeSGFjQkpjgzfoHdRTjuZTa
T7zpiWIcsISFLRvWRnYNJjdQpiDMwNtpo86AUA4LA9l/htcUBlFiZ57ZGXFIp77qsdV2HDUcuPDA
P/UGKOHtzC60YwOG7UhayeIqPChin1SvENuUpkamy0qe/805CRpmJDhVAGEgw9JLsbUCEAZCDl+/
mosdGRi/urjMqvyivaU+HOHrWROYu0eOLyg4ySb7z52rN9x9hOHnEiJnNegPI6+4qSF8wgmJ22E0
l4M166gfJxVYTjnR40enEhZnju/iWVKv1y2NQ3YbcLVD4Rp+xT4l3CIxXvZuQJkkXd8zFaW45Iv3
slYASuPk0J/yyp0mM1pK70LVB9vDqDb+ekVv6jiIgEzlYRLweqU7eS1nh1bp0g5wpXgb5BGecfp5
g3gIPYSekI3Hu0z36iVXp+q2sIqRu/bdSuaAUJiFyJuxFbZICVTRPBXp6WWmVh19hXrTqAbv7XT3
WVyUdQTnWctEUKjxLzTEtSYFC1Ry67HFxxKV30PpEbp6B+NXFx4ivbPVBm6tfm4W7IpwKbfKYDOr
idxZpQ5XlPcYdt6O6wJRT6cVxwKFpB4kOsTL/yMYIGkTdGYIJWVmSNw0gkSpzO3xpeiWdOQygtxu
bGmtG2Y+GjMeIvgBfr/NZXBw2lISXZGY9HZnGrrDE6z075uZBtlOSGOLrMGSxC9AzAK03MsDoI30
6ouhqfu50qsQN/XTuesgJ9d+c5Zaot2OPuRM6O6iihhs847yHUE5HcAyZEKw+9aSD+mpIgY0v1SM
rX6v9AT9Fd2vzbVmbDTdGOtTX0/nfOJIksC9YVGZdonGvy93//7UyM0KL8gGPSWmz4lr321qd38X
zdcKA1Xiklo97EVcBjXERIQPXOu4rxaxfqKIafXxAGjcexmvaZZQ8yUvgPPzoJKyowsNiO0xHFs3
VzukmZ0aN6DSdndDEcqFF+AYiFvTnRu88GpmxvtV0qpO9YCneRmHKU8D56QDFpgIclBQr/tXedFY
bA+WbhbQ9lyLBJkVzDbJN8a6yG1oaBVkyFrY0SOtchon2ZwBd8M01bPRaJqlQ4SJcc6NDJLW20xL
lFLS1LM6aMziIiM6rqiXgrngq8auluDHjeCZJs80gpskOUF/f6je5nmq8QiRketye4mw6MN34BPD
jtAl3jh+b51KE649Es/t0LRc5M4vuRiRjZQv0C+8/jJSjSE9d4Rp0dkadONgElruRC0m77CjjPz5
HUJ7g29P4JtkOU9vzgaHtabViNBIwYfOlgm9FsVN668uJUSB36LoE8JO0PhzNAoC/Os/XvRfI7c5
AddrsTlLmMenosXSlPkfVkILind2Wlti7pjsG0TXgkBU6Z/geYmPSYbaMUNABSbrDW1HGxgmIzO+
luy+6AJ0QqXxc0253lQjjkvoSBKMxeaKX7CdErDlFviGwJMizMYsLsRj3t9801zD128qZJ/pLfQZ
zxah8QGdoFwBvZcVXtxhYZRPxTivVyWHKm/ShEOpadM1GGQn1cR621VYvyyr5p6QkH492omjQTMH
NyJ5pyd6iev9VRaqvYMbBbJxy66Y7q/FvEHcJrMXjMf++HtGERtPgypb3NsEZtdFWlSlco2p4e8C
h4l6yf4gnNqAOks5idRGlCFUfiFbIYdGXySz3eKFT8BCKhQ5ytGFzJi55a6MsZ93Xv4W2HXFaeYw
x/kGTZ8yiFRTuXI7z26YCGlmaaiFHouCgJQDWO7H6O9tTwKXR1+TVZtt/KIUSO9YkT4qp0MkpB7O
FCtjPQNxPzReu8OIHTiiWzwqpzL7T0ywxGdXuMV2dOuW6ywJvrUI34OOKhprgY3xJrIBnTGxBqcX
rE/tyQzukPB58AEbAjy+SYvkme3df8ls1O66yAKgnE+fHLeYRhNTM4Rcj0v/oxuL5egAiCmvm/CH
FXzBl0DnO2l3MW37uXphD7gozAw6NPxmufvw7uAKi62gJ9LJ0ej6VWn1m0DoTeHe6fnTpmFUgwQv
ywhnmHcmeIi1ooXAieUHS+jI5qfjSVc2Ap35crrCEPtaze6YkrrJPTmDqEouPH1W21vNKPWgW/OT
+GhyOGE2xTq08mYNh6DcTcqKMK2tQZCpx68DmnyV9g1jf5hmvomSjK44+BBcGX81FsslULqjecYS
iBAhDaXAlc1+81XU0LpsqopRHmmjUuGdzqqEkqqCbMFlq8WTpLA5fPJsh+plEPkdcjz35UALnKXE
lvJE/mWZesXmgI5iK+4moto48Vkv2dBeUdsPFTv+IMojm+xZJBn0EAbJWqxNsr0y+0WjX/t7G9qD
iqGSxJ1jUuddfIpIt9NMhhuq237j2fYDSmjHh43zgdgqJQnY26k5/VaXpUip0NdaHXnhneIwIZCf
ZOwBy9v7ipDQfNFPFQN8BmY4BpAn634+dNlL8wJJcQQ7pxAjid2ya6zVF6/KZQKTf7j/h3H1sBu0
1V8LU8anrMgQV8i62hs1nzAVrS2iGZnXkaUsUcvaF6efyN4abqwWpzE9K6fUHjoTI/pkKf0D664b
pbMWyNuMG2HGJh/k9hoRpzDs7lVVvTQhBYP57ctdvVs7YsTtRWSAw54bi2zFwRqxCttEehtdAO8d
vA23ynRBQ1+CDhDemZfKWJuKXAR3JtULrFJOsCeFhll9vH1i+VULYooM3oZSukt9DIZqmfRywx/R
AAHY2HrmRrcLb7G0Q1rRrw84cclfHmK3y+/TJDyHX1PC1iT/nw5rh71771ocFJvbmXf+u2hfcRh5
/xrm3Cw4Hn+Xi1UgquzeTGUltwKpoS4VCeslEf65PuvAzn5h4JMzkKp9uUQTL662iGGhmUzukyCJ
LZfSwPK7nCY46eLEJo6xsJ2nHDi1+CjVP+66wSlX0MoO8O/NWqcployphNp+/hs2KJAyzUZP/5rr
NRzu6i/244LWudNOGsK5XwVQ0qENQdwAbxioRIo3QV2GwkXevaosvOTbUGaJ4a4s6PxC3Xx527jV
+ZnPD0xK/VjZATOBLAVHytEWCj+apLUP/HihbAUpFfzbiyaFGP5k+qKY4UkJKfGIvk21w3P+6lfN
BECEfOjsfN9I6p9TfkCFWQYUyPb8dISB1WX/g0YhX3/RLPh/2FWFYcoT+ePgMIdo3f3/RbjE/JNX
WuekKGRuB2eFHYEi2csNvqmJszEtL4ftPAbyvJMXruGhdnQawGGQM5kDjaJSs4f+bzgJKHbV3P3u
Xs/wmhrdXUmN8hXFZ53+PJIpuG7UtEH9tvXOImXkCKIYIDtdz80PDBZbWrp5BzNoboOrEPVT+puy
g8YI80t204nJS/DdL+q4WSp2WPE4Z6tVbUeP7CXzYj8ohJG8PBjXEWFgMFbFhmo9dS1ypT6oaaDW
sMzzX316qfudnOEu488oBoZLeJRzpteVs0Yl2Qo3ldF9H7wqrzNpkXvIbVV6huZaJY7FVSyC9pnt
A+Nw+TB2fWBqqMxhEwgsIhL4wEUDhf8G+oPt68lrPOwiDaWY2vSBdQyEa2h7XwWwUhKguC3iQvjs
vCqsWVreublxnhviGrHicIYCbIGSc/6MklMESLttJu673I8PLCQaJUhhKHt1RjbZj8QbIzIKERBb
RyKK4ZgPprojT984lIQlPhgZ4s/FgcQB0wq9Ywq71mX7z5kDN431lRF5Svpwbnx3y7zy4oqDn1ua
tSbYbcIBBZgJ287krtS0ZPVHxwf1BuxwFxOUr3IoYPX0iiM8BdXTzd+gzviWj99Xq6yeZOJhVEiu
uHIOYAwbCfkZ8JpeqomHkGKlGC/BQVkr1jPBmw8y/q1MNr7tFFxM+XoAxlUEN0eDriGvPhBOJlIi
WzUSeAlh1zLgSYp1OdZ5fRVIPiC0/Zhi1z4HrECk7gnZFYuSlPLY6bNbLbuFoBXcl56RG84V1ihA
6Ee9mPQUcHt7hFlSY5rWoSIKyOtc9pMw8h+kK3T4geXG2TnLH/vzCj+3AXwQc0YNB4k2EGW/zPg0
kdB7HaISaDVHLpo3AC9iH1/bTjLtZuDkub4853Eo+ACCfobm0SzS5JxdkK+WB5/MIIO2eoG/HtkJ
qGqH6DS2A7GCEjaqrQhXyE9gHlPuVxATeMSKDP/AxZLDFkbqp1a/bk548kxEthPCOID5s6kebg6c
UtHVosWn7IudTnquvLp+vlCk6YIjyb07Y4NWPMyfmybSUnN7tE8SAv3v5TjUsAxfqukVzhgUeVLl
Xw1yOAervXwAd7ArRwOxfQ7PLcbqXxIPmfMcmi27VukvojnTLM+F8nQwXreWerdYgPbXdGVxIvJn
bEpMc1oQ+MnyIij17CX5ykQZOsE2Z9+lu9vk7oFwhwWURnyBSsFn2ujaArazipppOOH6tQfKJ/hT
PNij2G5X6UIRZrkt/JLjDyeofjTtaQ3CXc7WRHtu3s/XkYm1ifK21J5KWSqxC8WDIzQGXBLAY3GI
dFbAMCQtKA0qFd0d+OAd+mXrjbQYec3HIARhfKVVDfoGZ44ieGG2OemuNl3VlBhzKAtt6pDw373e
QSNQQktxgx9UFh0LMM8NkbJn7B7e+Hq+19nf7OGZYcM1qxHR3Pzv50bfL5wW2hzWIU33wcMSo1ZZ
MtyFwAfBMQYNe7oK750Q04R9xRy8N3VLRKeo2NtEtpGWGQ8pA2b/4MwVON7M6ajKqemaTwgrrK6x
rBZIziOwiw1xaMdguXXMzrlMAPv4c0+RZZvkR2ECryHQooUwVZ+A9+hDLdwkyYEQ5d2rOXbph8RU
AEO02XFW2o6UlnWl5nQdUMZvnrMxdd2zUoZrkG80whsgx2DdsoooeuieEye1lKOgg34IP7nnLQfJ
QaKJRAF9swL0bPoDosjq3nQRlsAIv6AQud5XoeLeO3socm/NAHKswEFk3WkN2cm80/i1bRMHi6oF
k4Ieu4DtuiiCD1lGcpnyz0VKSqhzjae6Jp4PHb86qzltrBYCnQhj/Ez60SLv7QdcvuWj1XCOM8m4
bpgOJCmsdBE1UUVVgDxKN7d+3hY0bsW+pUCtgBcR4kFIXmwEAmyEEdXpeXbftnaBwxUg24Lxqvuh
cZpm6fjQdRXhgVtSyuEJwpRYWKXrCN9IeQD5dHTKbAEkmgu4YBVtmKWYylV2xPHodioQWC3syHVg
xvdp0omcNzO2hMMzQ0kIUfl58MkUkkPDFJjbphsb07ktbuec+yvASf/bqK2UgaOq/Dfq6orNzQiD
U1Wut/w9vuUMvAlMmKDvFAttFtplbqhbzTId9qzlSx38upANkcbQNPJwRWFvx/9M9+vtSBBrVyht
1MIdHu7mSr1hFzOECGq55Y8XNjAdLF8RNpskobthtUFH8PpkFAGLSaXZv52GNj4Dl9U90ILMtLvb
ZDz5NKsCBZ3UFVN82mPWavcKhdKA/q2n5aH4lMictjQNHJjCjbRJZgLFyIqZ4lzrmv8Fy/OjiDwX
5fUhZJ5jg0DVcREtiTied6Mkr9W84wT9u2/s04BrZKCBnT0BtyZrMSVc7IfDsiRdJ9UFiHi/MiPF
ryak9KPy42YR790XDFVm4j2Kfwyr8G0moQnF+zjfLvpUpU4o9RvdU3bOM1K8euUz9rkl6UJmnhSM
tre8P0VvlY6s29S78RTVu+Ma8xeq+n3qIBm4xoHnSjotTRkM9yaGPN3mlvFX9qoyRF8T3dAUd9Cr
JAIo2QdGVrOdRjWp648VpMNNP7WBeCqw3hoH1Kk73zFCrxz1IsasMYl+FuxmvaSi2NLxKBxbLF2T
N3g+hyS1XD+GO+BJnh5ldNj4aSZkeEQlLKFUw0cx4WxlncnNZI5wg6dIZsxQDkimxjN1fht1Z0B8
Rmqg4X3gjxb4btLvvO+Aegf58UnPjCUUv2sxVJDJMaMsuexTCNoWuAi1jue2AJeqkZRxMg5mx8T/
YZHME9zJ7MLoEZgyW19xuq9Cf4QFt9PZYvvKHNiikbCKgG8mlKuhEHQuY6JeC4Yn42xVcxCpNfBh
0B3/0qGLa9Eokz2jzaqjprSWrfnulVj/MzSdCh1A/dgU4YgNnP9h5OWUTseCEZb3RJq/UuqHKKjp
+NTPdPt8f3M6NDashxknO3i99pTjlPHIHTnK1fAoReq7msc46WO8hgaM9U2+AcemEtQ0kTFeOr15
NqPC2iW/qYyi2xbGbJXreJbZsMtQOstNOf6CbXEBCsOjSBkgaQZoOL9w7l+kn+QB2+ANgiZBvcGi
HJIEMkSgG5dfZh2ITDxSZ6AQKBZvOms8bzVdoHABFBiGPSoxtQCKmsJmXUI/ZsUAMBmBO37skfe9
zynd04n67Y2v48YYhal9B1RXk0nUTI5Ucs6F0ZNnEW2H7NUactFKn13oX5FQz3CgI8Du3ijP2Js5
bCxA3c9cYm+TMwc+mye4BPFdUrofxlGE+y28vVyxoaECa456eKO/aVT/Uj5XfW0LJ+SwUWNvh4m8
jeCgcxl8IJUvo0sASiwo329zvDqfSXQAI9AbfUzOpmm9paen/YsiIBnxf7yuUMW8aLP9kVdHl4Jx
BfCkfRfzFCNq9IrCuCKisURs/oIbVVeyr4vXTbvAQR2uhbM4wkkXM1fHF/+t4soaZM3uAlXwa3Wt
eeulqiDDebPlwW9eTmVIWinujOREJzf2B6OZBHn4+uJI05GasD598/ZFMIEe8JBcI6C5OnamUUVT
8G43cKmXmS8UjVYt2FykKHrUsWWtqCebMCZNLp9LDRheoQoo/QGZkcJNq5T6LrLx4YnmtkzBNmhU
qKjoIyzVOTcGoBZdOnGHL6dT3+MvndnA5raN3bGs8w98xlsKJ6yGgjsuuHMswGGQJ/bWF85qjjlh
b50hF+7pYxSa2dTJGT3OBWn23yoKWE/L47+qyjOLm8W8hxQGR/ZT+UHbaFXfFVMtZnUQ9YX8iGhm
nzjfEqh6S/JD/nsyUH+Y9p7GUMIPsxdpxBmJgG+J2frnsOc5KaigIvwBLWZDbCyGMu2tg6zMj+zc
IZGYrP6Wx4FoWk6XVyq96VkCuRhm7Ce4EQO3T4F+9gaOQkZEemeWWHIDByh87GWtKzzovfkrGhi1
IrLsMnxhrwCIaWBVJr+QkEeStWWUjWyOnay3lQEe50sU6KL3dk7i/OVijbYWjU7ozoDsI+Et1i4G
Pe3eifU1o6Djsh+804NkkBd7Zs0Bgzq+73u4HKj12qyGoFg01a+cE23Tuj6tsdwGk5mDLgGkHJ+R
2sr9S4rPA/G9gWwP5J+DWQNje22j9y2LVc+OQXPAgYxPj76qNACEHzt6ampuESirl0NRALQ2Yj5K
VESuPpoIreBiDtIdLfu6iYAJDqAtuxIL9yZhj/FkifjcpHukluN7m1icUqt4i/JHn13HEb7203TA
rkiiLG6iIS9c7Ef0mhDe8YAUliL0wd3g5kFY2Numw1TXNWdEpoGcUpyX21bX1q4BLrKPcEOQFRSY
NKwEp+5MgIAhNaRUwRM0gDfwK7fY70vkIC6ELOBwuLlwGLBEBI7VxnOfVSzm8TLYc5g/CBvFTiq3
m6G2HTw3k9x68XHEzMfW2C3RYJ3y+DJsbhXWnAmNDBevtUJ64d9a6T/Ak1D3MKAvZCnDqoUrZlPX
ODAoyRk92IJwohPvtQqOycc35AhghEqpM1ytToYJnRdYKMOk7xgbdSWMCDr2tGoTtjKPmjIZoo2q
qPa4lLVOMpPo9dZIQCq+KJxp4sCP2prvrjvavndxYVXZSczv5aN6BrbbS/y+W6mt0AENEVsdWstU
Off3r+BQb7YU/2axzvyFhnehzDH5RbOdhYo1wGRHNAO3EYg0QTovxbxrYfUJUjVmLtV31SkgWPod
w8iOSfnj0pKa2cr+eVX/YsdROztNjMWkGBmqVCFlFaHSBHaMG2Oy3n4rm2FIiswLdRD1UxKH6QaW
Z2UNBp1uURTeVpg7WXG9zRi5Ur4guGiH8OhNsZD5tpcHmJbXV0ymxORHzz86gWHfygpE4Q8x1ohL
2SHGJNhZq4zaMFu1h0NRUJTrG0uEn1TyO8AnHSDXatMcRrTGZlD0pL/0HRakB0En2koixq+rZrF2
U8GWkoYjf5aWhWGZlcbueO87RJfDC98719ygM8YCdFNdazPwnzvQDnhV16MLNSe1776Tt7ll3eAX
PUJmHUNG4j5iEQ/meynaey47+7nsVjfCrciRUrC29tCw9VUp3AxgfguMI2g2soCFk4GLDcM2Uk1c
ZqwqLfS25K8eXcKyUusPPvWiU+RZavUTYWDu6WD8GeFAf4oiXIGFDqJJwKNaTIsjO9/dvH1+FZqk
wJZ0PYfO4gEzdCSHif1J8poeQ2b7iEb0MVo/R36Vl5j3vGQtfuY/9Q5DbuOXDxQlo14eUEu10Cxq
0Vgbs2nyBkndfrud9H9VLMY5uGPi8CR56vXEvVeWTSfMxeqj+sAxN+dCI8SuvhOb5BTrOZGNI1dw
G4dy4mfpTVu3E+CZHV1GedDhW20Hxm4qF+q5TTY6xVV8lzHHI1fAHiSojpAmfx45XDa+peip49jx
PJfL4/p8KjNpsZVbzGzLAojoQYwaGpFNFMmDbbU4pEKNH4yqD4a7sG4+QHalmbUglUvGIEy/ulSr
bGVNr/ygZWjGdTUtNrVJkS3uAo3uffNJeVvqlokCZ6nXDu6zdmvIMUr8BmssyUxc8ZjtX+IABBJa
6Lbqtb4YDzT9YPBbbxiiqQJcIebETP017wn9L1wXUlsPAZRcSfRhB35//VQy0cDgMjf20TvzsYku
p83CRtn9QAeGImp0oy7KICvtMIHn0a1/foTtfJVzbmWd4D41AYwvZfs/R9eHNfomum6yRnSFPwZl
NuWjmGdshb2CdbHmJxlyljihlCrxsSs8JejgqVYL0pOXJALQleKOomh4BtUToQBINYhvnZbGRn3s
CH9MwSWy3F9CcZnCutlf8/bRuXLH8L15/jeXDayl9L5NfioPf9L+WR3gV0Oy8gS2IRT+XX2SbHSk
7L8vdUVmPlZZqcZD/x0gviR+klR+tREXbm38VE8+Ky8QSzlJ+6XcpWAS+o1AxUk0cx1f/xoWmWb+
coyUD0F1nx9l3fowpJUZuUO0mA3kJnx3zUdWCJgRkRZEne02ecvfIiPW4KgIXwJr+5QoNRcoQFhB
W3JOqx3HxmvD+jtFh48bHBFh1kfcIStku5wl+LohI4ObmPinJGJOI4aT8oCKV9PGHzBBZOOJPAe4
GoBkuMdqC3fFT5RfKG9VIhv1Z5lWrD2WbZtxL/ve3wQvdAhsw46wx219ziV7DU0PIvdsHHKQQbG0
8F+hJ7DRdUEPEkOOPyuQ1YT++qj71aBiLosf2mwlikhFHeA35qdviVJb533+LDFiE/zcNLaP1lpw
f99F+y+K1DN7iUH3IRzp1oi4IrI2NqxbWPh4Ox79dakc067FARnkA8kWKzDrIN6BTUL0jEJd2T8d
CxWS5ujHFT8SMg2/db9p7cBlYrWDpZ6KNfehIg5qlNoDfqLAMj95mNzWhjTxFIeRdQaUFqavfOhp
IDwTABPsqBBKWxh7ZSwbci74l2sdvk1oD10g/noCAVL2uEttmJwxzzyAp05WclZ/tHhvv4oKJLi2
lNm3Jsy8/ldF9TpGHJPurNNKoaqL6fYzKCt2XM23dY4lojcD8Kcz0od0lCdOkgK+Fk2WteVfH0Mi
7e+ZDM/sLyIudxC6LBpt8691bZ5XYXfWcEHD69k9G4Pcwtu0KoyzgXSi+GSxWQvlsxFQpM3V/lET
Aww5W+iTLzIXRx546GIk6GJtlXD+AfclCvrTXZ1nCMdIU7UO9UpNuwPfsOgf39yUigxdVgaddPtX
2wXPRIXUpoZLE39Dl6QwaUWrXdfz3R7UX8/1n/SCZosvKWASrj5TgQxSdOJyKbmw5CJhmhLqcjCd
gl0ffhPFH1lSSkzDA6nxpK24atoIgbV8InlfShUmBVa+isall7Y2jDztUpAwxjMi+TziYg41Dmzx
YLfS9e/euJlsi49C0PhLkHj4GcyQgM5+qZa2a6u2NAmIX1/Xl8n0V52+lxb0erWUIs9v9wr5GJK1
h1N9CBgaAPH9jza8MGNOq3bWJKe92i7gS40siXUfblKsOukjhxD+XNalSV+8G2IzJ6oL1Ctp/oxQ
B8DXWTlxHP3ki6z8vo4aZqyNyKb97c6KIpv+QW72l0rAb0xFhRWgsFyzJg/HA7JruaRKMMPavPcA
QOrKYbTYVw2MJan4oVSaCSEojk0zKH115LBXQaNFx8bAbevDYGT2fTDPIlT2XmPYIge2Bwlx9v8P
/i6mHczO2ziwoi4PxQ8dPSPVk3KbmrvnWAqiyVVomFyGcAobj5fyzDl+iVWU6XGf0+fs9NbrvE3v
z8pLIqg2iMeAWKynDnn4vCBH9KuqzrsyzezjDPlprqPLgzCqb2f5I3JKoAKtJQkwiEEmCyW6jwjU
evCiMgbD7HZ4/KpuDsiPg0MDxZdv17zH4PuttNBula6L88XQOtlSscULECWeQVzFTwvZ2qxLA4bG
uH9opkERSnV85mElJZ/vLz1m+TUDYpYlbHhuH2AO6fhndHMMqRvhYaGx4o6hGXwcntn/VJkpjlGs
RF0rO52gHITSzDNSIIVrcn0j6QTdDrvoE8CZMNGSWJHANFtEN/juVTcFN9fd7QUP0lJKnDbWbZnt
KMjYaKdoQ9FmnmGlOCb4AuDXYOIhb/7FPlIyU1X/UoST2rr5hV9OOahCVUtnquianWKY4L2Io/Fi
0aGd4J9gxlxDGn6WocSWFX08OCbcC82l/W0XM+0AGaJS/LjIljK/tOLFHm63niujZFi2KDorpN/q
1grZovjLowVeM63A40H6IJhYHwofh5o9trZPqWQszZjOjLck7+RRNz6Jh7MinM+HuEhaDh0D1mYO
UVaiEdZcGb4GcB2e+jRwA03awpWJYFsRc8128hWydgY78gM29QoJ9jQZGIa0dIZcnNE92HA1hbAc
77sCn9dt+/eaCnh49gloM6frl0j9G4y2COZXiGJPDI7oIsN2r+g4f1++9dwTLnUL3+WSPzrN68xn
nwyyOtg/hehRPr9Zb/BIzaBuLpp1J+NYiOx2bFjQNi0qgX2SEuxYHda16FYcroX6HCGFFu6jdPnG
iRv/zMl6/xR6lNEQBnFL3ybD3K1dQ4c/jZU++r4FIY3O/tD+6GbaNHREQCFmdnp5nIIrRlAlqECq
js45kZj5+T8XLeHvm97gSyp588tJG57YTxltH3QGgslXl/boCnt2+MzAhHkq7CMoQoykYtMWAqQR
7wZgVH1KzSaUr3OdwOvgm9sSIKEZwJDn0vVPHPxtPaPI5ubru8cPdb5gVMKb/PeQGOD5uGuK8ra7
fomgcNuwuK+u8FPh50+0Tw0IbY75V205cXRfvuRlBdIS8iYbiLVhDHAPAFyzLD29GmdqhdkhuBHW
XHizH9Y89n3qGYASChxDPABCclA1zavYC5clylROdeIjfm5nQ/GhVVyC466HUe/eHNErolPer7mG
qqp0pPxO+cqz73p0gRq0bFtUHMt02rVeVl/Hv9jpWNjEqlEK6cq/X2pOnM6lrO6+QlmSCYsyX7eR
T0Ac0Hlh5uyGAo8a1huQNKKDkptx06DetL63pdxAa7YigTUs4iaqix3UhvDM9O4QpyfiYr1juUeP
StJWQ3d2fPrKnf4kPArVzNFnDK5yq6pYuy5gNyUvWRguYvzszaALdbzQas5rt/YVwbj7+SdZFlOk
NmV23EUhUyhyzHCXdtl/YthmKqo68U6awQnvkHpFTidiTbeJQQ+ZjUMGhWmVuTWMmwx76qrPGfzu
P8yNmok23tJu+jGW9MQJFCmOXQYs2t1937FKK2E9R26jgkHtDABSQHTat6QYTApI1IPdnlVvNK0d
IrDrQxJi4QngsIMG6Z3TUBe/i9DPajFRvjawOpRWkrDvjZFWPnGOHin3gY1Ofdqt4TXfJ7CIHkcS
3BLmghFejW8dS4/STQqQUCX2ll/pHZR6VLPRo82+BSUmjllW1fzk7TMcW7IWFO0ZwSus2GuJDHOO
XDxm7A9bH5epwjEFTTPQcMPolWr09qfHGCz8ZV0NMpgZGrznvDwoWrClyBQ84VYzDFcCWhnSW7Mg
o93E4N9SzfVoYO9Mn1mGgrfHky4ivCrCq16WCIW7KeCzzH0Jnc50S/nk0rxDbe01PGOU6+o7Ks6Y
XVKknRZNWzTJFS/KDkErdpC7WQ3Ew2zzOQfyYmxPNnHtBRoE8K2upxYGqS/YyBZs0OyFL98oRMEZ
ZI0qNA/SCgcbyzYVmRQ97FLLT2VP1z1gGqHnE80TECOc4GMXRiF+xem157Pnt9p32k4yDiTmkdpw
K/rMtLEXg/UaTpFGu3CS1cqNzg9yr2LuqpVnbgyinBLfGN/PDBQsBxrBel6YOAbOGJ1Rh6y2KEdJ
J56t0zlDu5HtG/Gu+XUg5/pd210/Gw7xis8NQNYqmSMXFHe8SISBAgEGbPNT+B5MgisZiLZFJAvl
gcJTrC8oh4HAkdpXovTQCeMZ244ZoJj++Ysm9xlCY5Dc6OjFlrZXYf3kJNqboLANQkFpnu893ZUY
qU4MGMoKh0feYP1U+gS6zOX2oAwr1TWoYVxHPbQDXy26cxxsn3+Mab4dSUgb7NFiau3Jm+XKtV3c
AsYttNuw9yyI9+byDyR9KmwYdc+iSZDmDOV7J/RkcvZtEdSgWrd6g8wrUAWYFetoBwuMKnQz1byC
jcV5Jxh73idCJYDIv7QmrOQiyhvx2rvmQhgKp5B8OJiIiqckFtntZbPc60DGAoEpCWfAu2n5S4Z4
Td3iWE0rGCPTFZTHsPn00LJmB1tTCV38XoY4HKprUxqwB4N3
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
