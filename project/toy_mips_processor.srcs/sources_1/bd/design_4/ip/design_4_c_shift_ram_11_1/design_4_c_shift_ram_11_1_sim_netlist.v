// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:43 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_11_1 -prefix
//               design_4_c_shift_ram_11_1_ design_3_c_shift_ram_10_0_sim_netlist.v
// Design      : design_3_c_shift_ram_10_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_11_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nzu2b6+2pwFmlGio+ZB7ip57VZx++Axe5c/M4tXUaHoMXfu0Ohu/62s2rwnnPIytcpVXOBslp8m+
qmnxZqj2/kr4JFOLdjsU3kmGw4y09Iw3NWvVZQvNURXhEBeu8b18tO3yhg4GxK2+7OE89vkbg8Qn
hcIWzjnRy1AJtD3WyNO+KjXhXaXQgrGE55QmPnHRK0t8PgoIYfRm1YpSRTjIKWn9EIkUZQD7/F1p
13uwGN+/APTI48AiIskWPu9DnTL7EbxmlOJMHUXb6CI9vDTGrieWHvu6j/orETsjkqvS3AHf9Ou3
FOsXBbDUikL5aKt+bIOg7KwwIscAfP7rRV0IJA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FPsMJuR7NYf6t/1KkSRnSMiyzx4Vy1ffb3vIh1+BfkPUR2EZc4r6K/q4wEkI9VeaDw4BGVDIFn5s
uzN/aGbo8LjOE2DYSfH9uN97dsuMA0CCt0SHZl3y4Pu/GCjWMF1u35wNySBfaOzOa5n6LuI2Bp2r
8IozQes9+g8hWhoD/xSKbcX1xUKbrVgIRDewAnWNC/RH6HUABKxv9nxO4oOA1ydNGRyrQElgn6Ci
krySMYkpqM9ettb1bQcIY3669AGl58tSy69xLpH+TVwRftsAY9cug/8/HmdMWuzpn2hTWoWGeoTz
q7PJOUrSf9bBprkQtRBsaNJLbx3pPmw/FAk3ng==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4928)
`pragma protect data_block
wco2UEF4c1LWbkkIpT45sPkj/W4DNe6CQvrkhh7arI+QQ+Y1wrzyBB9radw+P0Lh52UPgMuXdwKe
xJV8Wwv3hZOnOtzA0DVM1lVd2TUgYdQ7LSGB0TMwA/6bfEZug+/L5GBZ/VOUAcZzp9UC9W4oTVwG
9jVi2Mjzel97U3MAas9kSkkRXpfUCzSaP1YtJhcZolLEHwrcg2+rvrWryW6uUmM6OIeEzvbCCFBJ
nk4f1Vm3Wf3bwUGJn8lbb1WnxCP3KN2pSlAKvfwuyBvJ6Q4mYmeGtmKEQqSQBY2rfqZYmh+rMdAc
pkXsEWkV5xx0ls8vwpz/peffm5brN/W3FJBCSTzWoSyUXpfNGpUSeWMS8ibsu0yOjyOLLJPwMpan
0pOkVGM/FJlb+vmUZ2M3HIik+0E1PHNUJrQ9v9yTIk9f1QXx0jby8IshNXi260HDfbookV2XTgWm
qJCjHZas770Dlf4dKVryuIxEwMlpy1yEIXlyKFaaMwweLvJXZNw5KZmx/ofCjsA2WlZnZIAh0SAu
JhH7Q5YMwN9nKYGXdrxoQF24fCzhRuVl9NzYZxpY+iDPtfDTe4p43JmJfIDZvikn5oN6542DWOjl
aKsnnfuCOqgZ7cAkR1O1wnSDjt7vvTFC3tct0jgyARV01AJ3LxItcuOC78TNj5vUvHFMb/JAXPDV
ZG/LcKBTzyi78SHNZV5R7Ei2uwzR7RaN8r/aq9KhAqHn9zMUTf9++9GFAkLf04Qx/BbB1HkvpebJ
CmFeSy3wle9iYQTKBunY8WspMDkPs5ODK70eVfX3bzo88wOLN8WESOcBj0cwZzsMcitv5U03y5S9
sZ7RPvOIOQtD8IfdUQVPXCiHqD+AzN3KeGWv+UySjKf45mR5Jn4/MME0RuUaoZag2FGVZiL20cX7
2YWfzOlFxsPD4I33X+VlrKQHWTRoOiBWiq9ZiZCb/bk1u8EPdpE/0oSzDl3eQYVFr1xZZPpg6DiK
/87NZMgWlbiFn2JHHhUVHGAWK/65q6nlE5RB6h8ZzJdFRex+LEfK52uC3LXaMm30+uolAalUAamm
tmKEFg7F3iluOpE2TIROua5UiapVY6RoBYm/OisSXnra8awVQRMYF3taypUTqN78TYqxf1UZp2S5
FWC+v9iO0qMe16Y93gJVcduZVKNxnVmxhBodxZTq6f66MlK2ST40ORug0jP/w7w9Wm9dprJ/Ggdt
hg4btH6rhUYKiUuLBcDzmeG73u8Zudk6a9J+a7zq2diWbYBMxcZWv7hp3m+Wpov/aBM621eMvzXv
2uacGZCJ2glJ26KZUcxiHKmcZUIpNopJm7TvESSHXfew/xfJY1CPo+A/9+mHQwbXyU9WQpyLLM7e
c6Nq5PJBeOhemSHETF3POuAc911amrgO0pfqzc5gdvclDZ7WFCtjxkss+0VTO5QiPAFdfmO9XiYH
I44JJS4Ueg4eapr4Wa3rETzwX2XY9x0KZmFCSfOBLnYWh5Se8+c1IjEJAWOHKX1o0WI+0d/76XEv
39s0PFPlJLUuC0x85/I7aGj2wHGqXwhAAUT83NKDbO3WRtQvqZ9D10ZyIb8Oj0WJ5R5GZMOl0vKI
el8ZEh7TpxYl+uCLG1g1djED3RmcFNFXyKVrfO0AASM2zZrTPpwIId2izbOqiwmfvsaCBDo1Ch2s
1NmW996KR7P/jkWSb3DeGK1h6xH4nqXq3Qi2xmG9bzt/aRUKrRX5fKsH6AhSttYaRvZ4f5Tov77W
FeGJ/3z7Rn2FQeZP0uYsCLcdYx7Iyc/RgruMXmX52dkv2s4Ag5YUHg66jg9n+EfEYhh3PuSCu0Fz
mJaF7VkPvrjEXqxzzjUx6uitHVb42VZqX5UrnDo68aVA9XQQ3ER9Vdk0VDsgUiJjDHMMI66sfSXP
BtAspuq2GoJqJgArp1HpxQn1CVVoVYFNwjsga1KNu8N9IKDSP4pA17ee81KI5ldyq2wJNveD6mvy
gXiinLikh1NG/+HddqV3Em4GP+Nde0ti6z1PUr0gsgj9mcpRp27+qv8yfEMBz3etGWVEnnAnNs7s
bhw3L7cyIIIVJCBBv+8HRB4KfOZJ8aGC7mD8qCn6sxR33ln6yc9jhm8OMTLnW9cS+js+1Wd9rT9I
uUTMAi9cHWx06lH45QQFZQrpgESxA7s+kRV0Xvlq0hoO3erKPs14zcyBunedovfA3ZZUtwLmg3QN
f/rAqxh9vsEK0hw4w9cmskD8G65dxTw9cQVNJeikXsx6sdIDo6UtYpRgDcxlayczdOOW6gQGl51S
x5ahDBUv6zAi3xwO+6ELNOm68a5cngj4dybDD2tGHkxiksdBhAQHEEcDB5GqYFPJv2gTYHfDTuxu
M5nSelblLOmFFM8DaZeAkBOwVYRRecgh3UGFvPUtT+XH5YcQCq6GWhi+IZfWDMwkb5/Swa9EprpW
egLP4mWOnEN6X7szhioHjjXri4Ji68c8EuuJkcv2zE676E7VeTo1vpdGWTOPmd1QO2uq8FOIeACl
9/btnAPLJaWk+OISoZCp4GybxvLfuvB8+QWYall8Q815pJlq7r8YMW5SWoeD5/x/B1Ga2SXYgSVA
CPwUAZvKyOSYX0Ru5onYrBb31n+h61Mink3SYGzJDmIBg/gzwjdkSKi1uAfzSJoACj3g/Rt5XY1X
0jfnzy7pSE3GhMj+LofJvWWGxmf4e/FU3hGd4Ape/zkl91T6XtGy9d7TuFqwGeLsQD5S0mVtaZr1
/O/ELOqEZfuPze7Bc1PjVuhog03kPeR1eB7aVb+60Oyf+gxejkgY9Cy8gvZDevEXXtKowmHlSoBB
GUzxCkoL6K4MLbdw+2h6o3fFaZ3dUOTUA5j59qsul/YlCt6DPxbkNOg9uE4Vz/Iwhd6ih3dkSnp/
qTQL7maoVS6LRAwqZUJ5+WnXqgUVSKDujHOW4vKPcuYjGSr02k1+RzgtnEmWdejfNGYPQ9HyXHCa
5BHKHtjckewzgMawcKkMOegIG1UpzRH+a3znzYppFZzHEg7efTZL0f4sAlb/pzWbtAHiAXThBhDl
OwunWBe6+N1VU0XXB6BxVj/i0aE8HfQpXhK0i+8EhTlMtsyW5/XBAWjz+QJO+uiBYtbSEPSeST8o
3lRJnRzEOqiQmVbt7ib8qj75qIzdV9a4PPZf8HxwQ1lBWVcDLzNwADN1JBoqEVkPVNdNwYy6WeBd
wj/Jxkmpa9pYz+BCFZmeF4ozmnGJ2zpHepkva3sYfVdrCutsUAr78S/V7QAWiYWrJEJ3BFlH4VFM
ao7IK6TGZmMxDcJyi16OisGHbW+v4Ir60WDYQW62h9wSJzRPTRhXo5o8ohhHQKKCCefYYkQy69K7
nFfu4SdzrSS7N8lLktmgqaLxJdYOQAmcwNjDnkJCt448eVquQ5azPeDEQob+Ecv9B6olvnMn6uCs
4gdKydSc2ThYGi4T9rnatW/4W3GgOQTovJmTeuKIzYzVsIMSBkpbqgJYpumDKmnJvzVGg0pcsPig
qsqpIQeNPROaV3plViv07FSQaiDQUTb99jdCbif0C+9n0ZLe2dx77AOeWeM1bCSl0DbkXuyViWa1
E/YMUeYBjVg9UTVF9aGJittz12WHCl99tMquNGFCywAOfWL6i8Ou/XYo+B/mG6r2g/LlIqOzGnHM
EcGQpMjYxG/PAHr8ANOAUh8Kfl1qU/erkd8iltYnnrornyo8FpgfjqeVbElrhYm7CmL2mC/sfeEt
hGbGzSDbEL/4llgLRzP0jP4PItSpDhO8ToS56hHBtQW3oWOnIpBj5D/+smgmeXoxHzYEqODMcc4Y
zeoOLyyBLb2Rrp14fXd5yQIs7SQ7KI+2iKyNl+Sgw1nSoljWEBGybF0DMhgXgieZGZMUCkDlKN/Y
Zv59lRDDNFtp4KJaABaIoj36AsEZ1ZHtiPx3btIgTXmhmsHgERqSOtSYmhnyzT7cYxEj/ddwajzv
8bBh1k0edVUvZ3q39sEyEeZqTFJcng896sAcNlgTlBEibzW3mu8CKBcZvisQhVRjE/KGbBM/VwBc
kx0WQIB1mT/szWDI++Ep0bOWBlj/dlaGKB6W7SVOZab4SRsui/M2IySn0L98rfQx1Me054yFwDZt
LgvhJAUgdxe6XcXJ9Z3ayWToVE4QDpZmMl9xIYE/PdUI1Ta3X2+hAVLMFORmCszdRm1KXxhoEOmJ
gb6W30u3tR76O8jbd1YEWVxqnxvoP+4bRE3agd2625DQiH3hmZNXoNGSDytL+UcHEbgp1kP0tqnJ
xCMJADI86E6Vtisg+tbcndJeXQj8bU37nD0wUUsoknYLPoBbvfxAU1KlzyT7W9k95gLPo2hgVo5l
xLfKJ9U2I/w/B8+rznrlNc0YAQg+YrulfAxYKCPum2yOUqjO8fy/3MKEW4/KzU4sBjwDXRJQ2hW1
gmkeAWSN12g1l7RBQiwXuCqfpmqFtVaseCcGfowp6wmwXO4yiwWMsXbPFVVf0LnzCD49mx8bWZ1e
klLv5ZHEF00ejO9in321PqZXhOG/dTL0CydyD9+mydUXM2ZzRnRlsJu3jlmSvbLgC+I1XPDfuHib
6NOmiCZyz7k3Rtba7nM7bws/TMhfxzZh9Uy6nIhJ3Oj1bwvc2DwSNt3qhVEQg4GTpp1k4n4HxPhZ
EX39q0bU1kz/mbrO3crw6Dtfya7uc3FTdy/VLqiFnf4+sr4RHYRQz2Qwpcd+pJbvh49szR97l/Ek
SNaXSmgVv6cwekeS16TCV+djrVeDNOM6v/sDZduICSTwX3SSnuBGObSc+IQKEisfyA/VIQPb+sHL
eNBSG8YDsZtq6g54mAwhbv/1e4ZHw9yGyX9OGUH6Nzq9CuqiRT4cUQoRE+3pAxbgaMQ8CewB2Mvp
DS5ytzfiYcAe3jLKxXoPE1oeiUaZJ2p7frrLz20dTC5U0YCMhRZtlmhaXuaBjZ7e0fdkMqrT4yEf
Z7C4yzfRiUlgh8NgFAvLOMvU7wTkH6EqspBc1HpBz+UwMQzZ6duzbIYPJTpXkeOqBJ6inGYjZzPC
qcp9abRR6sT9adV9UkC+iu6Vzg5aDGvBQra5Fb+ELk82E/vmZoSq54OyNQDhPom2/sjMyTlBMgZB
QltECrtl6uxV/zA0TSdqCWpLIjmYDf+KaZOgSTgdN8V1z3uy7lhGM6sK7nLdfBQES3pNhbWh4ndo
f/Dgf2yoanEVsUcTTN2gSStnRhWGSG0OZYti+sYI6d6m/AFC/Wpcel/EpPUVe+o4RQZ/J01nnaR5
w28MSa41GAiV+BAWc3zUD+tyY9zgnxtFVmfUZ1HDxw8N71iu1aTsIXtbcDf8cp/FihA7uVJHeoDa
ua5T3iTtRlqkzYbjxWrX31HXyqdrNqYXalZH+oK2uO9SLgomSapxAKECStnGUaBAxS/FDLj3HpQ+
Sn0qlJVeVcU/FNOoJj7+mL1O0Ls3r+PT0lz9blAf8Eseo3/a1Db7VhbC1kK2bvKLTfOOEd1Q6IMb
5yucPuIBmUspJlvUYpzUph4Ju08kQ4Rp/tUc9lEt9vB/T1fxW+qDx9cYwucfo6vlZZomsNkZh1/r
Toj+b0owC9R3BVT4HiG8n+ECHM9bDSDSsVxjCEwuZUzyUMwB1kBQYenenuborRP27Kjznp10RWDe
n7PVi4/Ulcr+hq8VAsYY3YnX62zkHmzuivUW0LIr9QqQRhVSGT1Ut/BTY2LJX6XU70iHsUuxqeCp
f+dcQpl8P5sqL96zWZkZqNZBBtYn6DfVSieKw1zVcb1HQsd3flhfkZv5OZQmD3wz17B3971YsXZ0
Wn9BQqnFeIOFL987YjU5v3Ap2sh3GGuZ5P+bA0KUbEAd0IUaglpM6QdmzBe0Dar2I8J+8xk6vT+x
eAnGz1XB5ddxA5N2ZULduUkKMDLNf6zpxTrxERLCt3L5SO0oTeOKxrZHCgYMcRFu2kuNJadrnHor
aqglHx9H+yrlIWqv4hwfv4343w5HZ3bz7krkg6Mq3LnAPmllhac0h/7NJgn412dUxzAw1gE57sTg
O1z9VyUUZpgbolFNOulc8ADNRLpg43K/21Xy6QXQL9cihJYaEUMI02cKw+MUn+vD5oBVFSafAVVe
flkAeUfYpMfYJMZ6C1t4WClqeaA3bu9ZSMWN7ZZF+TMw36mYlYwYTpEZJz2QVqGiFzJJ3XXAbSLA
Ji0koNVU9498GIqX3HOkHb46AqEhUgmpcS8Ogk/xT7GrRAySCE0dG0AOQJ5IKI2RPu80uUdxNRif
8GySVVxCiyFb8Fer8lDKkqE1V3/koR5BlIaFv4Ll4Pxsxm+snkGGKh6GB1yuxl7Xupd5JN46DFJh
8lOhYyTbpzi/ICpL+rCQBfcKQbRhKYY6y0pKDv8brcSdqKBS6UavPC5Qcg7XR8nOZMXJ1T696KPn
RJTA29zEbVlXMR9XjxklLTqsGTGQ2B9w/ZI+YwVsibcXecdwuUniyNVBPSeg33io4F34FC24+yLQ
nY1VoL9WImokHbvx4hkJYk1RVH6199O2FFqR34xkmI14Mpxfj6eP5Bul1pgnuOR3nAXc5JZgcO24
rbLUA01Frx3GAJMoc+d0hilbK23uwYdTOIA=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
