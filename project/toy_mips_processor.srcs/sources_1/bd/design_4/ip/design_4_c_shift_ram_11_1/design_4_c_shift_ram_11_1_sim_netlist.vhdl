-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:44 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_11_1 -prefix
--               design_4_c_shift_ram_11_1_ design_3_c_shift_ram_10_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_10_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QcE9JgSZJXlEdJa9l5/KM5UAOqKlcDvjOrzeoonFUtyvaM2j7QpunRFdD+nqKIU1Inva1SMB8nrF
gKiJvHkHyPRrcgMpXzTdYW3AoEFHbPojwVnbf/Su2642m4HcBlU2w6q7CBex29vK233dPM49gTvL
cCjsqfF5ORhuiFJh/8Y4Qm00d9ykN70WGas/TKecNJzkSyknK2KrDJaXz143djKBIW8o0Mw2Uk7X
u9QdFSxR2lHbs2ZcYl101/+tUkRm20+ZB2IFoubpRsfyA7Zr/1ruVO+cCC8bYU+935iCDHBrBH8z
gcfGMlWwQLEwQs0j0AIiXXCzbBIbg9jzFgAdRw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jEDgf3L4ROYKqe9m3y7//JBHbzABVZ2xw8rr+XLkcU6/1i/Q+CK1RrUSapYT3hns63S3dguuynOy
DYl6e2OTE/klNzp+twCnQthUEs4Pi41pIRPX9I6M+LVPP+pxJUfPeYAZhW+hSzxOFiasxpKmNxCl
+RmQ7gT/546CTxEn1w68kZXGG1W18g1AfLfg/nwd0jSr4BYTMeRE4jhnhSpINxIL+1iEWeH2e37C
laxtR9iW6eG2IkceRmemrmDcHn/Wgh0+jSmwlDtgbODRuuDkUu9xQ11TMt8SFzrXR3jPDxHZnvcX
n/Oy/YIB2p807TmxP3C0h1wHG/RyvKj+rBdBsw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13904)
`protect data_block
9a27BXIq1/KREH2zQK1y/Trz0Vv1EI1bZhKhaLLf2w9oyUhte0+qrzM5PDq0jqWA8T2mtklLYjMU
W+RCgWSrHspMBEn7XMvHUcauKkVrQrQNdu5LPJ68sxsBoyoqm/tBuhHnH5FUR+vz1iq2oXWzaIES
Odwfc6CgBKBg5VpmFTuMuzEpi+Hcq7ntThY6IHVivikqMQdwt2N2kEoTuwZU3wtJGLjfTVMa/eb9
7pg3xcbKY5mc/EgX1t4jyQFA2uwHzFuNC2Ghu5TfYxnJVd0+K3SAPlO6CXDQfCw4VpWij70bzSmI
WjJx/CzZH/Zow3Gg60MlPixLyEhCBGIV8ds8G0KKmCP7BuAddRjtNi9nEPFNToPsc4D2CgEx7CQz
WpERCrwWAHYWUtsTE3c9S+B6qcubM/zSJjoBWTkYC1JFadYFcBHXYZ0IZdOpUTINPedleSUYNRux
6Fa5Xv0wzvv6rBID5JWkzVC+6HkgrkNjL0nCpLXPXNj2mKikL2kVHPd698QLMQBl+u0wkLD2A251
Rm4xtbwSinFih75BriPmgwLq840TEpLsmgMyhyK+l0vWigPRLJwH7ogRo5bu7Tw0QGkPleICtb2d
mhSzxzbDd1kUYrBZkukdAD2xPtvt5gph3STqmdxDhSS5YrVt2qCYxGFBBz9DwRBlt/HbN6UFP8hU
oszCXc1X9qKR73nKT+u4nCitIWkn3sjF71EhZxHlEA85H29R7XTnB34JjhHDKsl/s4Li2Ij9McXf
WOkPRr6Ae5X35IUt/tFYOwi0CBok44O6N3DyP0DATHEclHWV19xr8j7RiiTpYFCCRo+x0r6SH8PU
1pMqjSBsqoLpXgzzkjnz4ZNth1hanDPj71OOMf1tQWmydVrNFzbLzipVJRJ4am0AXMacMH4AQp8S
BRIpl5kh9EViPRK69HInp8Ig8LwGhR4EiSOc8K0/WmumKAEXXgVP99aQbyclmxohpRTC9twLvWHu
8/56J4x9h1v7yWnasmaCFF7cbYIashSkMUSlclcXHuu6lNyXASEBV4uFCUNvTe/3ddQxofBmK7+x
2ImZajSlXdV1x3HNi9Sw008BMhvap1WQ5ICJPpgAUPHQxFVS1qaFO+dmubFIXzIB5banxoH/VcqV
h11lVci6sJPLn4et/Q90FbBve8hV45Fxkfv4F+xzpkcsh1fWOSAeNMupK24GKqPRS4mmlLtNWWUs
JQqqwFtpL3X+ORGs0QjOboEKy7Ub9TOBoyptcMO7zMAHU45E07biZt+XL9vD7dlGdctPO3bxRzu+
JOrDqraOx7L0Q+5ESnZIyBrUL5rpwPIEceumorR4AVvdeBShDW2AtZfQ+WkP3l/mChA8/HQhDQwo
yrKbFc9igNtq0wcVsjx7B34aXiCN2MEbxwKTVvnksamWkkfAXKldb5j9IsGjflPURp30zU/k/X0f
T0zeuCiL6sZT3bo8LHyFk8jfZTJeaSu6x/TwVM+Kl9KELNabU7xGQByBvHVs+QVF0x+syew7RRQ7
7mkL8ynNGiZhh3zZPnnrLIy7MZ2VgJ0iQzByLXliaTNwngHj+bg2ZndcuNDZeWOZqCmeMMw5KQTo
SApeQ4I74+89KkSIRlAfG2yj+b2u20BOX6qTYq0QY5hYdelYZuSCsi0hAl+AOVXHk7L6bKDmEX43
xF4j1JqMVQOddFQ7YFq/hMy1O5eRi2BROnqEeScCv58LKr8BTkk+Fjb8CbZGEsuTKaCHLLaJLkSI
0rtFLn2kVxVRw94krxXfjZ9NFy2muw/c3FdMXTxSgKqsKx++SKaZ+rlfrlQ5njjg7N1SDGMRglAc
1wjIxhxy+ic7b1JTM/IKvph3z/gNVRM6sgFUw89+DvZGDSC2fJshiDTUlWfoTdb1gApHKi+2N6u4
CbDhGDUGVcf1OfqSfmoNXWusRdxH+6SMxYDPSQpAzqm+QRYA6okJIAWKAvzxSI8a1ldQw4T0F2LK
K9KHuejfSOYbJNNfkQbNTI0yWWFIcNzxfQhnhDIXpqihyVWzCaUuIJfKs3IXbuibcH8qovL4JuO4
2CsKMzbvAqd1N5FI2YPWBGFAH3t+uWt992CJslaOVMmTNRWx8lyk9wYV9N1DW7G8lIAgLoDu1QgE
3H6WlMsO0/JEmECUah6GL4ABvxgMG0W7KSkcdX+DwTP0E6ORARY6E1O9RD539OtrHISmIj91QB0X
ruIQH7JAZ4D/I1W5cten3Tjj8QEQZ+WMrQ+X2jTeO5WoFrP53TyKFWLS9vqNxcUMGo+TEL81OfD6
wzoiqlw8xj9NCTBCq+ff2ckRNasIn9J9n9d6ZtTTuhIxlM64yBW4UYq8kfJLXKutYAckMn3nzbui
bcWsEYYhnusL1n9WCmTih+3ZB8ng9L7mGKgtX1RdJYtMrDCdWODWnHqSC2+uvZ6k/l1iFIRlcYnz
B/75o2JcSgp8UV1k2PedVNAOTt3965crpttecSIk0iVuXFv9PFbq8sWGNRakLi1vV2SVOcEy/igz
FRHkPO7kqukGQjKUCrFWdWWQTbNVS7Lg/ra9ELFRIcrgQhVFR+HwsTr2vGc7rhrNMMNHA6bsBw/7
5OX60+TBjtoYPbvpx8VQRzLjGD4z9JthplgtBHEOnPP/2pPR1f3FSA9eFxMfbbcIicW2ShdEhXld
Gw/CnZbdYsmFPzmcJricApR6zdRWsYwT6xo1PkQvyFv/35X6PmN/IpJJY5XcykP0SUYIc3Ijzxa4
P/bDE3GFRU29Eabn9XShlvMO7zdFNUyrOWjLaI3KB+HR/ZzsdLk4bL8X2gCbRLmSuUFh2A8t1dRJ
W4l0i/uMyePvb6eFWorMJ77XgZ5HTbkyfNW2L4WKYF1nY8Q+E2uiZC8aegQpCtR8UCSxIuQleGDk
oET1CSe0NhLzqCc07Fig0k3TqECGYfJmbq/66qQ23vuXJNczwK5tFDe1UMm3qRx/lQrL0M+iVa7F
sNo9ne4itOcjLL0LfogL87SMEJzfZ3D15DG3U29j75G/jzv/H/fddO46jkXi6NwvgLN+NPZnyAOJ
PDxzOtaFozJgIkdQ7+ehCjhPFbZy8QOi/MTtQXR/y3oMYcr7VwG7cygRxV3RD1sqWILHMPraI6B0
9laiVQ04X0AqNMjzGl6NUL6H2JqGHo6hi03nhcXK7W9UeYxJnUXSYnKOePSncD42tzA1cCB2Hd91
b6ozG6TCCfLD6p2C/jqbrWwMxnONgtSp5xIZDZfkl5Pe0pMS2j9vTWjzhLiONARmqam2MABdyqW/
lq7Y7W0aiROYhGe+goZo9YJxzK2xuBc7maLR4PlFI/x19zzjyKyMHXcK8a435iZpT1GDtFYzAl2D
daU4ZkB0HBdtyxO1TjdMwjdPBkMq0mmNjpj+KgxaP+iVTfn2mNzXdeKEQHXB6ms03eBH3GY/GMzi
jKdr7ypQipZZ/fdDF6b7p4Foe5tGKM73Jm/JaiS1HPHka0kc/mKdtBXL5PgxD2BN4YGT73AZH5x2
ug/nt9FMMocSGmenxJPCPC2Qg7uBiPOVNw45tbCF1vbSdm4Audu2fFLtUVoKtGqF3tA4eWClfsMI
as1kS8vmhySwzl+mFM4c8o2AbG6Z3HxrGANsfKJ+zYmmNjBzZM1ByyYHyLcLbzHy3X2yV2pqFsVS
pc7fVs0lYr47EfFeOJKudM2nr7I7dT3CU3RbjuWom3lB9OHLOJVXXpi1K8kySNF/pHLjQd170wrW
QAl+mx4rwAnMVlOBjQqLsrgntLzUY0zjtChooPOBhIQYlFcbbeJ6XBBAMTJhqwCcMFZHMg3dqxFU
AuP72lHsp2oFcyiGNYqYP9Ml+M+5JP2CYeJPftWdID7Lg7ScaXxfrrUWKbg5PRe4pNacxTE0mN3X
p9DauvKrTLh8HwB1+nd21tOs4GARXAHYCjyr+DeA0bZh+UBc85jfib6YjR/3o0FhTd2Gwq4NmjYm
pCeSDR21lY8MRbrcbfYsLnTVQnb8dpSijeuYzAlFC/y9XiYbC1aAd6mtn9BJaz2nRIBntj7IA+Pp
7UEmRdLzQvC8s3q6hOo/36qd9q6UzEcLWR0lf+S28WwMhkdVEmVa9wT7Uwo93O2OLPiu8q7FPqV9
fsXriHn+euGZDDNr1jBt23FtnAWiyY4Ek5wyUwB8C7MrPwKTRZdy98p6ddFbVxA2fiZL84MlYlQV
Ohno7axlovXydL+xS3AXI2MDjHxgYf8+L5HvIyNaH2URvUZBNYr/qeglVaNvqgs8hX749oEL1kOJ
3U5SIsX9lRgI8ic7I/04FjHYAC23TPWonW8Q/Dn2p39qFw4zXfJPlc2LQn/magY1Xlzvn7mlKX46
lc5hmSYPZ212YUoq5kKec6DoVbMGkdEBhsQk5rsuXXPcBpLLNplylLGWTn5ONofke6eH6GFmPfsz
1y81CtyCBewZ46z0787CNK9APMIpXWZfM9RlDHzwMNE255NqhQEub97Vj2sYFe0RRYZLobO65qxZ
jynq6e5N8mPrhBCj/QO8bwQb4kTgCLppgrzP5rLq2I1E2RPhyaLeUw39EbKqKe7leLfPkEHbVKrP
PWRvoWUzCEOZ0YgIH+xNG+qHzXuph+s89O5zQo706BtVEpLyLd09ORJcO0JKStPXC6rpE2KqR26O
tTuR9gZaYWsxt185/xU3oWiiQRTXmFN39VB3fidkFYsCH02sxRIwB95v1SBD6oX0fgeGT/9mOr6Z
kgs7X8/rYzhRPdeI9/FyS4JdGFxvZaZEVYM+Bshb7BgSYa6EZngJeINMx2R/NpRCyZUwgd1oBmhY
lily44VsYv4Xi7V74l7aIOJ273T8dzpwpsbJI1VAaar5kE7Il5aR0BDssEn1vPHDnLRWzWS5TBIN
y2X8GhnTKPLVoZpEZWg/qBCNQntoGyn0ZpLqNFitW/8HdwR6kdJajkkhuZoMf4fA0UnNouphwkoe
LPBaVQFboOMczoztoj78SLvqQ83V5fUsDWZq02eKg3YFgspRXK+gaXfuhsEe7ubLtXWk0u4XzVud
TLX0ysE05+s47OpO2jYjrgy7pAibO+Kg0SwgA05jD+lPAZdlY1sObZdBm0w/0hQKXn0rk2kc42Os
qs8zuMqkXZOqgPC9VAi9sRuQjVvoYiYjgSdJXi5Rb3SIcptBvUf4kC2WXcmy6ikMJIes4zoSWUBU
HM0ZQm79EeTqG9NOOzR95snqKcfj1p3SqG0nmwUqndNR7iUo8rHndo8iIsyUf5uDLyNIlsMvm50a
R3XwKLzoZRLPZ6jqcdmpI6hGd6OENjNOcLG897sEXytgbFyNUeoJ/SWSPiVHm+PHj5bmDPmAiC8o
m8PaaoMcPaS3cCvlEV7QcNsU2Owltld0A7a4VfO67vWOfpc7+CQnNRt4xyMrQ6K39/81yY83YQIH
OO2IHZvPfp6Ylp289MuumSlhIIoGaVpTY7VLGNCuoJsWQinDKSwtSR9o4Swl+nE8JtQGQqXzIjbU
5gZfL22SDoCm85r7yxXSUdAL1IlkqAKYXtCCgytcpio8JQhkfrFi9KzoVxFUgLZqjjq6TghVcP7I
a7k+Qmc5G/hzWG9A76yma9Uf5Wr5Jn0/9Ag0pYR/3nlzPnkVEUGU5YhcGP4IqBvcxmodHf68jfwP
6+a6/SOAs2nGuBSCkAIwsqzJTfn63YwD7WcCp/b6zindlp3YfiUm9DDshi1761CPnb2wIzX2k/nB
gcJ+uzO44WWfQNifu3TJ+ctKZ5fQUMIwHCA8rGjbOKyK+9J4psCKzGx1llYMAXH/7+ny/HPtPamf
DLSkKVZZjC73Re2SMvSjdz5JouPYSAgTKiBexv5LfqffyhDnhtBy2vuBQx7IPg7MITFjzlQ0s3tn
q6g9dRSO6/kCSgWva/9BGmlJHLtLbYgfSZqoDvHy4dV2ipjYB0mXnQBtSlwQH5YKbU4wC/kakS2R
d7Oq0TMoWhELmO+hbk1lt0pupbala2g402CoCF6TJmBQneMHVbJd46QgC5V3tMT+jw80mP3g++5m
ZOkk4JfA7fWuoeEoPpcFvcDyYFnAph3rDxshtyXcUhktUnfz2sBj79DqddecNyhUuohqLN0h6Jya
6+/raNu4gQ4l/CrNGkGNA8wUC2X0u7xFftdihb3inq/5iC1+Ui0en3FrXL2k2avWZNtpcwghAe+q
0J+z6TB77LPZeV5juDnd96MG6uMPjww5xiWwkAAzQpxxqHUt0uzhrvdVJFjjD2YwPPycAEZ7ZfXE
cYWdnrrtflav0vnCxiO7JbD5BBE7bDXddeG+uGhPJc6e9zn+DeVMO5yTqfaxmdXuSlo2lx7459ek
kOp5Q8bFJQCe2w1rczbqJwQ0jZwBa2VHXkRgpqQeFWgYHBvp162M/LCJVhcnvCYkCfBosupxDy1I
HlYEIXU53Ygqq+SdC2xiOmou4dvc+ceegf3CnKinIBIehWRUxmEZP213bF1BE5is9UDdpD/abkp8
9nCaqL+E2wdFhkjYKkDZeFZTJv5AQEERDVecb8sXggCU9QZ9zX54jcw4UqVpYhPekAIUlINGB0pZ
sbR0W8SQyIb2gMJjoa9CCK8BsKLyiXCnR51fvz0peN9hgeu48umW/UQBOqQL84tCwaPyd6psN58c
O4G3Xy/NLWbewqwEQOnapPmKbVKjWR6iikc/NnnONrRC4g3ZcKOG71Vx8tsTVvShdMw/4KaxX3YF
lVsvY254JYq1pD9wcnegIASvXQzfK1KogFx3JB5cePrkDD7R81DB8/YyjWMHkay152T9N2iH/5ka
20N9IwNf0jtm3/bLGJ26GJGgvA+0YDXrBb622n4THexlhlefjS71JTHOJFNfKnofpckGbTy/VsNp
MTh9X4BTqDgX4dVgmjnJe+ki2PVFhxcn0PEYtzO4EiZ2zlM/WM3K75MWRn8OBdzAzzigbj1+SMhc
ANI1y/9SPe4Wn4aGqryKjZE0+0tSy2rJETHTolGmgNVP2btRd6xxPk6P5tjKAanHDuB4VO0UMPWx
zSPOVWz3f6ZjwiViHNkB/82Uoz/7hJW6qPFDrPt93opQeaOb0TytmASedeGtKVmJZMnVlqY8OGEd
MRjiGVQwsQh2+tR1jsM/CkqpJ3nLoSvTGCOk839723FA/2GsQPf2tJccgwJZp0JF4haCdYaSBpjS
lTmhWwMILZN7QciQ98h6WS6pA1DpCYZer2+SIIjNmnX4iHrBTqvNkEsIiXgEyjv10/+Hj8+V2Wzd
5FF22vr9qnOu7blo90T0TCSqDNehATOHxmrE5XJ0qpTsFrwmzVcmox1qTiW5GUCq1aqUpxxyGszF
cMV8VOGvKYKj6yQmIrTAfe8zAmHyzzrepIwSv3gL4vKshlAcQyCwZZhAOEPPHQWb+IVDbyw0chfg
t8L3q7eMAB0Z4nkLcpzBXkuOgTgE6ERiU6EVelSZ3+ANI0AAkfsfSQCUuW526tu7XTdeICqXEmPE
hsCmt5zfb6PgzyQgD0KOJrW6NGipiKhIjeMQYrng3WSlaFeQ8FchrvbcX2MoRyEZPVg/rm98lG4d
+DQg7TXBpzrAV9wpCsCduGHFXqU+QoTAAU/f144YQjel5tnCsK4nwGCpi9CVayr9UgNVsegRu7Q6
SEpSyxmmXJC9WpfxJ+5ZAM87QQYc4tMYMfKDjLQ6srz327ctGq9xXitHw9MhaOBiHxIvu8svrY3i
2vqPOdDkN9tz9JCAWIt3avT4N9A7sVdFGcwH7cbOplxY7/7rBYUJwOOobEh+sZs2kfwbnLnwOUXa
/aVgyqXck3vWNWtQMnTompo/b7MpUAVaPk8gi53rLByS/qhEMGtHrjuYwFanyUM3cMWa6uMLR6/q
h4nlJTVujwxNDGqpkSXlA1sqAWCeEEDe7wc6YUIY4LPtCP9LuWDW2ffkRlJiWeDcMwsx+Le++0ZJ
itbI2NKIyN8ZrZHFyFPjRGXNAb8yPCCtUICqYkL1QPhVZ2E0H49PINiZDfHlGB6qFcb/dCer19bF
3bZbsyiKj91c27U1eAYX/tzcsI4WrXl/FnhW9GpuTiga7dgealwkdTkQl+TMvy++hk7gpZ2x1zuD
dXbXqzoA+4E9KapgoWF6qt6A54VnvCdoEBqNwthFNAkUbSwb5T9lM1HivR1P7qngZxVj3qH2mYvh
XO3b/qv3TDcKHg9uE62dpCfC/Wsv5Zw9I5Qf7xTDkwFxepfHvBp17DHrAVtovPJDT/VwlFFyEs3p
CTSoXZipCoKvRWpPBzL3XxxlHzQmAbJ0MloINDmKTcPd5Qt+k0NJFxJrScDzyxRUbVxGdn0k+wAO
vgJlgrF9HucQGhfvNurh4jgxjse8ahzjLa8K3b9/4cKug77n5cvigWVQ13hhJtMKSgildVHxGDqj
TEBIVCZRR5eTit9YFOsn5fzPYitkt6pzcJpoIJLxq7SIQpfQhrcivtZzPRGK2BXDna/0gz5/y1Xf
DGrP+VO77HwYx+cVUtputTPa2SFiCNv4njwEfDwoAmVCfpgSMlXGxI3dOpLBhiRemD8TFyXlaCKo
vim6H226WIjwUnvAAR6FnJ0CSQMuTqhqp2SUuB8301GUGsqWBSesHDJEVZ/0tFo1yDnh94ouLZ2D
pHcAK11FD2PI6+rJhYrMBM2E5Uhm85nho4cK+NK8CukL6aRxYTiKpOukPvSbQF4yZqYeWEV+ZRHq
uEtNH19c9aPkDyf4LakFLIw0A3i1gKdKCVh5LcVtDHk1x2Mdidhg1l+Vxolqagvk2RZGKxvi/xE+
Dn4Sn3RD/N5TG+e2YlNGzoTiXS0kyoyInBYbqcofN+U2GS461E8X0M7KQddERUOsMJSJSP8Ek8Lj
UVrMNAFiOS7NknwwM/BbJB02I9m6Gi4qaWbLP67y+LJ7zZ4n6jrWVzt8gml7gcT5C3qTwDlmcN3n
b7NLezkkS6XnQPNM75RlPHErPmXHoqBUbLG8Vfm/na2WBrkQpevR1dKxD4tL5JEPufz9+rb5HLHx
N0UceKNk1HRbyLwM1c5P8KNKjFYGRGwVEwugXogYXd04V1s4HE36DE5PHmAgckY4ALmyj6KcVtgQ
H4AmeOG2vVpVZFKrhB1A1jU17oCl7ah4CKSDPJtXIVHTW++0vUDgpwisarXP/h+PpBBhEaLe3f8r
r4qgZ/NgVK7Q4FxcEla/a7j+QkB8rc+m6Hj7WKsR7J8qqHUbwxKDav8q1I/tw58YUYvpfD3oMI8G
Eo/B7SvDaB2K8x5ga3ygRcPurtvEaNyHUuX9VipMUg6rJ09mYzxDMixainknL+1DeZ785BMy1rYk
HiaHZWL8HQvn7gSUiHqHryOMNVQfvdlDna4rtCwTtP3EelQZzYCCKrTvc6oi8m0JymL6Xe4THIzu
UuUHTsH1S5eBTi6U50i6OXJOp0xzN1ITx4YyqnDEY2gMh/f25sF9l1TA+BR3a8gIMfq5hAuOpF3e
A6usuEc4FswhgnauwepC3U9A33jx+6smLstDw9PDeEbck2m56TEeXggNJK9vB8Y4b6Kaq66oBLcp
U/LtSCB4hubI2CAcOwf3MudNFYFIgyvT8NVoAi8t9Xc9tadR/19KW8fggGDhApQRT5Zf1AS4WD+3
FSC5mUiTi9tn153o98/LZmwsBCRq4YcN9pCvM7t2y3xvAdBT50daG5+g/9GLYflTNhzz7EyGXxnE
AXsgYwDSAmlZo0eNsvfum2PhoSJLEE9w+JQcYMb1ZXhSyfW8QUN1oDjoZ3Sz2eO9vlsMU94ZqzCZ
kADCOik8epmtvC7Hg+5Y/jRuJbtzXZUPaUfVxlSv8z7S9/d+6omJO6On3Q4xR8V2bKxrOO8AKwec
qm/dySCVsDMhklqcPMJuU0Cv6OkG/yVw4Rl8lZ1L3RZIhAQa4zlgB5+g0v2Cv/lCZB3TAa53Np47
qrTb4IMWoEWDlONk+0MutH+a3JS2R/Y22FbjSPAnd595f2KZ0zssee6sVpzZA56fCyXfi1fC76Fw
0PFy7wyfg5+WzuJ8h8rjAzuh4Bm0Qk/uBTUNdA4elOBj0uGhabCkQRjw24zfSaSW44Ijxfb9E3qW
kpuM+nXR0WwWUDbDeP7rIjORDLbuYzSMEkaZ8f59dUTXxTjZgjQPVKsbEHb7JpK7SHbGjsD901/R
MqXt352irPLs9JWISw8fBw6XJzg2zLw7FOBbiCbS9mj5C/K4FkMmOKrZ3v0rQCF5mE20juj2eIaU
Eg29H4L28vBiQI3LhVAkCY1Z1wkXUxkHHnE1CNOjgVEN8d3mTCe5AHu2FOIg9z7ezzDlaDkmnpEi
Vx5jNPx0tUq3sUZgQNN5AhLgqqFzGgw2H136kb8omQgsLaJ1tcbZj7gXN1OJmIlqsxV9ZFJoeu6l
rvi+8GK5azcV/8ggJN5OOoSU3YhypV6XElxxPOsMN8PP26yABVqXkoGcVWawMlIbPkv8cB3e2ZVI
nCDXdOOvT1p7UmqHGeFThBjRiB8AhySm/8b2FCJl9sRxbRVbOyKAUjyXkcf1P+UpE0nPqcSVdixh
vOlbu0MtfnZiczI6GT4BaaQ1mTCSBaIHVXBuIiQxr4KpFzHkauweCOs1zd9wB3eA5e6sBBgniuOo
FghWTgxWIXrE8yix8SDdZplibrnkZnY7g4EPrrbLwOS/qbzdfaaJNsWw50oZv0o/CEnrECi4q9Al
/dqeZmQlReIymk1UWJc3g5d5frtlR5NjbolpB1zgY3inGF4tX1IO/eYoSosrXkMd/3RFRTQFBTwA
59o6gVl4QW+pPRXqRfA7qAuIkv6agzzn7CUPlcxMoawUSslKMK2+XwaMI+yWVKWrE21rXl9rjhWz
POV1tnuWWT6KSOA4WHQ8T5fi7PnLQN5C2nzMV/KflulYtayWlhgM7inE4sQNyZj0jbjdd1EtGxBY
9CTcLpBnmfHIJ+Qc2kXeTMkS21mPzmKIe/5XXKfm03mDz/F+lZ6NIaZS3R53D4SdblCBe2u9xFgz
pGMwHt46jsiBzFK09vDJthXXflZ2D4px09oo/4zG6tXsOG0Rw2GrSG+7i55rJNhDhLm8VIa4ZbsX
VqDFr0fxlj4jrprwETHAXItZsL4XYaO8Tky3Qbbp4wSVE9AtPdGIH503a4sY8fTO85cH8l5nx7Yr
+5WW3xFLAZ57Q7UGmAIPIADrdjECYZ2QXPgIbez2F2lvs2Mdpydlfc8S0lVVvB4U6srCLsTFTmIf
Es3tSEpkwALpdzUe9nokaLibOUVpekKHGZy21HUGNhXlKw9J96V0XibiOQuqTsqHUxxBQJBBlrua
v8lxw+8U5tFK1SBvV1pY1gE+mHnrfd5NOuhArfucSzdDg5iKMAi8h865CrUPD1TYoA2TD+bPHFqj
dOR0HqNGbXoJpKRh+ncvLqgGqcsCX4OZdXmVUG6XsMNgwBNrNo6DBOTMzAUt5NK9rzjiFKPEhHHx
xFm0rg/ZiUfn7z7xWkGPN7AYMQ2PZQNIatHvkf2VSOW40gaMifoUJGrcvl/61w35aWY7E8J8JZDH
3yyiEFfMczvDBx9G3UH0nq4RYqPF3IrQtb6fjzYoiIwzJqlHRiEW9wS7qQ57LzQyyeqK5cZa2eYQ
dxweskZME+ho/Jul1IDlOuKHRK4LlFThik4OjCl7zSNcq3CG6/oiBubBf9X6+/qJb9CsmghlsKxi
JgSHXbfZtX1mrovXmqEMm91GQFnuL6G7po9SXtikravYKoiEvX+2wrqnbTGHqx6h6wCXVVHzTWxZ
KKsIgHxI5BXEvPuMb4+pu9YbJjSQRddD4G/T/1ttgBzoA3oJxbAsmnIDZzgHvxWZIa2MFOJeAMBY
aY3ZsQDjB8d4k8xVi3CM3gAYb21s7HET8iW+3MgA4/Qe3P8Ez10e2nQtOA4WDFiumEyJCWF0JwUZ
klf15GpBn+I5UHAXCThKsWXz2vpwrIzXwzDkdrk7gwvkhr62/0WCWPwV4b8Ztd8v/9lbbDMhWPrv
YAhih4eKVgLwxQSgEnfQCL5wBsJ6A9jDxpOi1WUggTmyrNjIkx9CRX4j3YOWChBGs78VYHP19pR4
ivAGV8LItmsPJswJRRj6TYYkwX/+Wbo51lYSoNUxGBXPKUld6SI5yueoT/7/ze9UXTwZSROvEeaM
nDDiRrY+HQD5PixZRvyZF/eJPmAJmQ48/Cth25W8NKG7GUoyrHtqH7QAv16vvXu92sDeVngqXg1a
4W81si5FOvHfJt5OGk9P/c9lMl2ITeVIksCIv4Wj98qtJ+6j8Uee5HilolyfdpU3h5x9x3Jt/0RQ
2KV/d60chWXn/7bhWDDlAq1vf3G8IGCPWZv+j/3GnUvidrpXP6PaSSXcLqKwoVe42Ckjr2E1M8Iv
btmrZ21uJ3MDS3P+Bcn13nzsHZVjwhuWLk7hBLfnqY73HUkqiwG7eofj63c9kCqPoAj5eEG8QMQT
sx1llCR4fxjolB1b3XDDpozA8rpx4wTfp+xy+9IsEirc2ss9x1OsPO4QDbWf1Y/8i4fiGUBTEu1m
WNQBiXQohFb5BLCQszwvpKdls0jdMn5yZgib2TFY4R0U9SOQ/0OkMS/9YxaZsCNfO977NvF4V06x
z4bsB4WOcL74tHxqw65SeKbw8/m/T4XKmb40LoNbrWt5BWdaIK49UWgsEfiRER/nfULgcYCqpWb3
SwkGjwZ2QfzNcws/9toCAuvvI3GrhWkVoQKJQuvxDxVg376vINtPZCoVDeJquSSk+uZOBjH0/71W
D21btHQpgnZynpkOhSswG5KyeMEEWjXa0rCubmII7wQRLV7EJwOFV+jV/tI98rbW5vdal8OoevAF
LLQT1nl1Xxjri1Dti8+n847p+jsEgm6dzUup463SZa4CKjVjatoUs5+L+aAy9J4u28bIsV0f51FM
efa1rgS8P81WS3WVpZrHjIwgsuWoqpgmfTiFN4vEPTnKjtnj3UalvYO4rNa4KpY8nFKD1MY0oAGV
//imXvIJ4BmDu5xf5RMHPxeRbLSaD6y+tUJBghRng4T/d4kmg4yOiKLJyjYyPf9dse00qkTLDgPE
sOfrin5if3ubp5sN4UVpW2Nztyr5M95MQCwIgEWZFyU+aLn/OmHGoj2QV3K6xhg+FSAFh3h4qkjE
5znOru5B15LO+ZT0Z73eSa0Pi7jK7M1nlAsWUDjGSyJ+H5DsoSMjwvswo7lZdQtcgO3VxBlMEeJs
aPok3euhtk/dJ198+LVqx1PbIm8VuMWMyJHRY+0bt4FDJV9aQ31IMP4GpDzJGWIwwlvRUTIzvisK
dzgIySB1Mh+cdXcWnXk4mqsal8jchF1wTUPfaXfpSKxfOB7ZNTteLSDy1BiyReiFbNQ44VNuEzvg
CLX/fFVXo8BQIDGcBprhBjfUq6wqJXQKexO3CxupJ8LdF8prAHCrNddQP8eyPchB4hlereuSNQyt
B0ykbOX9Z9HoCvjcGw8tPwFB7ARjPvzVFFARRq35hiMCY/FJSft+JYL0xO8PSoKuX9y4MfNgbSKN
QfM5V6f5ukw9Rj/wrVpsFantyxY8KSqqREiCLB5OIpM+dFqcFH7u3fzmkbKRdzDsdb4ttGO5If2e
5juZEXMhU3F7xACdcLetoJ9cNj8BoDPrBRsofQ7VlpIapBFAjoz3G4FTdgSPDc6odspTTqBN7cUf
3Edqaey6+EDQCBGDkUuFzuJcWyJpA7glK12YliyL1nHPCgOyOoqdPlFO7dVEVVVC9JjgsmWp/Plp
3SMvKYP2fCHTiJE9X9/CXgiNlWjDSJJlpNBxJEd+/itu3UO8bitvr8EwU4rBqmbnh6qLqZMu9iQu
18EwWxaGD7M7kmdb72N4NXNdZs8ZM6k9doNT/KpYrkKMctrDSRg5CGVUi5uWhjaJKet/si2Uw5zm
abhVjdZ5IydtNmvxhZCx5KSXz+3RjAFL1eRw5+xDk/qpOksJyYFyQAt5P1t1IvsuaoWlfrmR0d5+
DirLxb163JhCGc7JCZCtXa2TZPdhrOek/tI8r4wknf4E4TUn9lyX5uzJ9mVleJRrVircxUOjcSc+
HGB/dkg22dubMKvvMFkMSakskkauKnUOmTR5meXDXROuMtCyc9yMkspRZlgiKLN0liLA+BBDDCLU
XtxqOpe/ZqiGOEuZhO5B/IFxTk+KPj+J8BtWZcQIV4Gdoysr43cg9/YXz28dTK0thHA+xk8DKYSa
Nezr/eqkIxNPw2P14YA6eXy1g2n+9/Qf+A59L5rOSTS6vN5LVO7Y33k04Dm1vLuZV7wVTeeRg8uM
BNuHthSNE5soDOgRjmKXVjVH1bbGKEMTS0I/3VAZWPj3ygyMHcqNoaGbK1KYBK6/zD86MYuSe+RZ
Jf705q+1vVABRhHK0UIZWPFdX9le3gjL6arwXLPiCiKswzefyReQo1EgoN/fles47mBZtcdTwDTL
X8D5IxNtyPeQANSKioL7UK/7aRJ3g6lzM8UJn0XNpLsPlZKDPKJs08I1mlYtu0w2lG6mRHALmSA/
CuE4T1J5kjKe+WqI5jnhQ/rtMhw1OwRhMo6KUFM+xYYQ2mZYjWpVir2XpP6/uN0KiidHJKKM0QWg
lZiAkWJZSG9/VSqr4L88Hs8Ed5YrvS0ETGt+1L11syaEG13CLyiGCTfmSjqk9UzTElnpxnQtbG5u
mmQMaZh5Zgh1vYdXXMCIgH8kpopPzODfrOaLhzklwTrSZlTRVD+vWnb+hEueCf1wtUVsSrF9VHi6
FJhEN4n05VRsLF9L3mv5CD9Efc8S//qWWKBgO6YZwpdY6HNjbU9BW0jma+5ZRhqYEy//Mtnck2zG
kD/NRxoaeVbbAMyjk3YNIrvKbZvpQ6fYlCx0E2QocifOTSBL/6lgRZmy3VVdVHLyT/4OwF2Y6HFD
MqnAtmlCkeyD7bdaGjtKyGPDghDbq9kmmLxfz9k2Ur2jqvH3D7oBhq2fZZMNahqcuh3WAu40X/Wa
9P9tq2BH8p9B7IFToVIk+VNNV5dy7HQTNwtgotGCTteliq7vlptvtR+pyJituCzJVUbogNiJgQgv
EMAs+zjIST4dYC1VnvFkcMTxZ58q0HImyBGzAj0KRGtL/uq1WvhAbjuP9aHcrK5BwYzfxMSqkKbZ
OFLBRQliPp7Ew9TZi0EaxySMZuhnzdfDHNutWVfN3eyjIzBpR5b4QqF8BWt5X9J4ZvValKrJ4Gu3
Oc81qti4PWyi7EMENfx6yInt8vEFB1oMPmAGokMfd7bC/tKnl/Mx4ubf4606kQZAlQT8rmf71h4i
upiSqDYPsHeomBCPb0NE6wth0BvuM+a96M5UwhrnEPI4lGuGuqaJE10rA1Yo4vgJ5dBB7yHy+OAy
3Oi580DdQQ5TNsqdgPumwrqDvS5cVcGvF3+8hqvaxmxqQWQqWef/VJG+6oFz2TXYzy2LSZtOZfPB
6cs/tx0qOEFXE4PWdFXvm1llxY4k/dWVEEKlifAxyOJESMZNZBcgehC4WmpvcUyMhEIOxIUF8/Os
VmNQV1uz+26pOFARGtuzKW6IgqI5LawtxyQUofJBkw67B5EWr/Alo07e5V10pWbx9jNpfMOb4pW0
dP9WOv3ld5/pF7YkOqZgtqL4anSURGmXDuCrwAKyV4AtaYQBjKcopXIpxEFc7yu32znFSWzmmKlZ
L7PajdyNHxjfWbBEieVhRyWHlf/gbPob/AiI/DLyENKAo1W+fcLxj8rI+LKszjed8KqEF9iR+lXJ
PmCJFJuHSBPcbgaN/L6pnNByjrdZnf5MvhZcxX0rM2V+fw0FOtUB9qmZljbreF8j1ffH9gNg4Afs
LcOHEG+N9PSEmxVUBLYMBU46Z+gvdJp9ogVzp0Ap8LhVQhBJ9piLshsSLsQ7yFgfJ7vyYov8LAcy
7HCQ8jmMi5+C3ueOqA6Gebj77/nGvlLyeCVzi8K2v6OFuPSoFLpg4UG9UXGcuxPPo153R3naxpVd
G8WASJJ5qT9ub2i01ydOtVjf/byCugNCdqqQgZWdp2L96TTSsVBdgR6rMEmnvrgXCvOqCdCfVy9r
tZJCRGesLxCeWgEG/P4v2xD7yuaX386HliCT5zdgfJLAlOCk/DL1MWdTm24/qfzEbqjAEsMTAQa3
Hzeh52WJFx3tDKwC9I1V39qiLKONj8Xwyzt4fHyu8KHgksflABo4aRl8wEjr3MK+MOFaluBJVHlL
UgsaDBG84d45BDCqnWjLwaTzjpTAzOrxR3MmPEnofXTh4TqvQxN3aZbiGdQJVBBzJAfhybEClGOs
2ilFi6VIKU34obDdg4QsdKsouIMXVYRNRucJI+oGsiLv5NcPwVn3tEe1PBZF4XkC9t4m3wFZNw0I
QLL/WRWh113iYT5tg4cug+eeOy1E+hjbt+xTlAuRxUf8/BIwj8R8r5dr+sybZH01JjExTzH/Nav6
mTx5k1uHzHHysKKvdCZUl45P7oiZ3uAGrCkBX2sqkOb0lramizomDw/dGMuHcrBB5xwyBEC4jV68
LlDbkY1UJF1NGWtsKS+3G0I18Ila/CcRjb7BaWE3wl6DfQH0szz2j0INpPpAcGKhWIDS6uu9g/kC
OZ5ZVJGXjl/k9/kcTRwjk0SIEDfu5JgIUCNK72wt+fNYNjo2C2V5qzl8FJymuC63CCSMuXFBb1/5
V0uEpbKelrux4U/lDKofSoDNLWTC/8JVZq95HFkFH4gatZBtfy7EttyUjmAjl0H+17M1px2lhN94
w/neVCJp8tTrnk20kPPt0ytfGEuQ+dIRqZiUF0h8KUPLXp9TWwd0eX/8H2deJVDP6Cvz6wcoXSjC
5hTz4EdabIGQ9axZruBZX0FVL4AEnrAKh/4QYkTBAmewpp/rC0uKREt/S1nubXj9kvdMEedk6zYu
L6wxIezlZFL5ibIKg/KKmQM2xqodLVlwmQNoymzihBHJHGWNeawyCzSVYk+S47LSSy4HgGde4FiY
pKs9cpKgh2MFLuYFZffz1/LTi8HETz4S6S9SwvBZOvqiEK3+rIPWLgsqNol66phi+SMQD4XFD2x6
MvkVWO8mJBA5oLIIr+9FTWwQxWzvroyf8xwAZuOCBrcfnuAxb0AS08LctfFrB6dcOMN77ocFatUt
TQw87k6i109xAN7NgIJ6414GuiLx2doVm9+vTsW/T+CYHhlGpHqG5p3E0Lg35LgPWSd8EbF5CXfU
itR6G3yyBf1shmmVbeojoVy4jF/KbzRx1ksYT2qDHe57RaSNdozl6RQ0ym2/bvfPOYZF3sdVVaSD
h8jSjIhTrn2AaBGsC5cZjQcdVgsXoDfnpF4VxZyxkXufgGYM+fZsH+gU9YH2C6k7zKS5g+MgBbdo
mcGQ2stdNTEJGs2DggMqTn+RTycOIGY283RJL5ZYR3DR7Pm3z91ni2g09FnIRNA3lDqjbq+e6x1U
ldiP37TxJqGXySFGhNS7NlokY1oeGOaCVpY+R12ouaSU3A+GnnIXQvFvC+5IAVYAvENdwpOkE7L3
12sTD7X9RlWR0Qlg7jh3UdESDFuFDXHvqvGEq6ZEUv+aJ93EmCMK4ZczWtdWk2+9grywSiKHBdnL
1++d/UPL2mQe2f1BDccIJYMACXdBGQql8vIdx8W4sgwasr+uLLnBldQT+Wx06Bdq4OwgnW6X0hJJ
Xb6dTFZXTDpmwFcMfnKiPGQC1IJ4xSAluaag58BzoNL3EqJ9fvnq2nfg1jOghIkuA2/oFtGCvoAc
2B8neGQhboYxqdMKqYF6fxA+pZm8J7R/gZXjluye0QMqPQNqMSkwJ+/SR05Yuohqmbhlz99/yr+o
lm/5ltF3ORQKAcm0v+TKjD8ab44FjMY9Y+vriVuvJHkTvMMCqdntk5493JpocsuIQA5OphTYljK2
wDpwkU6FXryo83/li3sZB/GB8PKAgiGZqnTSDFwTPS1BjXZSTmqfmXv4OXoJDnJX2KMXNdTHV9gr
UIidv3cUVObqKy6r2iwM9lul5HeGxjHKF/PPKA+dgHQHdI0PywWMS5hWS2SZtCivGNbZpzuzIsSs
5tXwwpItOMOX80Ku7tj97uwrrZU/X1mu39IfH8YumRzqRzKuHb4xk2IduIG74KGnBPsFURflzrtp
VkrOoeySvJDrYMHEFCwCw7J7EMdLQdbeIE2Rri9uHxDRbnvvjmy7MAVXLp/PHe+lcI3hQG7O3OYJ
xvXjEZfGw9N73KUGv2rqxP5nwqW+WZpu9BfN4xYSk9m6dAgrCdC8PJ0FjO1KnvnafTea0iYFWjzC
D+vYalZ5UUaTfw4FaNDJC8dRGJdNbuclbs+QCVl8CthrUxWqDlauN4moqKexE4Z2tPkUP2VpVvn1
wIdST7dW1CDFTIfbqOOiV7pd3X+pBDnGIa0O+f2WTMWU2OMKDjD5sQWdGvMxIDoT0hqv+Bz7JK8H
ugfUOfZw23CTe52Zc+1kCZWfeo59IkETHWmXW44agc/jBra9paLk5Y5qurBs6+HBjVFH9t4n4w3q
f8yKdZCNK2QHLYTLXEAzi0WNpj2jCR7+UyyJf/Gc6uXCvA6zDEANIW5pYYzSXPoAax3HtLHKFsCM
95eDvWgDMT+K8/Ng1MkkAGC+LG1HOnbA/2GIwUwzneTcnPGvNV8ey6x2YaYN2YQoKHXK5d0=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_11_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_11_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_11_1 : entity is "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_11_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_11_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_11_1;

architecture STRUCTURE of design_4_c_shift_ram_11_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_4_c_shift_ram_11_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
