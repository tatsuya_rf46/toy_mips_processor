// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:43 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_12_1 -prefix
//               design_4_c_shift_ram_12_1_ design_3_c_shift_ram_10_0_sim_netlist.v
// Design      : design_3_c_shift_ram_10_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_12_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_12_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nzu2b6+2pwFmlGio+ZB7ip57VZx++Axe5c/M4tXUaHoMXfu0Ohu/62s2rwnnPIytcpVXOBslp8m+
qmnxZqj2/kr4JFOLdjsU3kmGw4y09Iw3NWvVZQvNURXhEBeu8b18tO3yhg4GxK2+7OE89vkbg8Qn
hcIWzjnRy1AJtD3WyNO+KjXhXaXQgrGE55QmPnHRK0t8PgoIYfRm1YpSRTjIKWn9EIkUZQD7/F1p
13uwGN+/APTI48AiIskWPu9DnTL7EbxmlOJMHUXb6CI9vDTGrieWHvu6j/orETsjkqvS3AHf9Ou3
FOsXBbDUikL5aKt+bIOg7KwwIscAfP7rRV0IJA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
FPsMJuR7NYf6t/1KkSRnSMiyzx4Vy1ffb3vIh1+BfkPUR2EZc4r6K/q4wEkI9VeaDw4BGVDIFn5s
uzN/aGbo8LjOE2DYSfH9uN97dsuMA0CCt0SHZl3y4Pu/GCjWMF1u35wNySBfaOzOa5n6LuI2Bp2r
8IozQes9+g8hWhoD/xSKbcX1xUKbrVgIRDewAnWNC/RH6HUABKxv9nxO4oOA1ydNGRyrQElgn6Ci
krySMYkpqM9ettb1bQcIY3669AGl58tSy69xLpH+TVwRftsAY9cug/8/HmdMWuzpn2hTWoWGeoTz
q7PJOUrSf9bBprkQtRBsaNJLbx3pPmw/FAk3ng==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4928)
`pragma protect data_block
0RPruRcDaWQRRv9kgBFnd9lT5qxVeBejH7NFFlNwkY+JC3+9uPwFpHR+2edNvbtHobESDX2DP7vb
4HJNI8vHVrEKqOtVa/yq0yQaUO47mf2yf0Uh7E+GeWKtGvHPysqQKq61ohJD274ZL9roW2pNdwtb
KiVNvRd83cj93A7gAnCl3n1onkTjQWne4L50AOSAD8BeHt3hVvcSIcTpMYvdBZX4ZwWJN6LYsK4r
LRgoB2m0zHf9PN8429V+HHEFNyHkts5rINySXG2b9D9N+dbEvjWiXZAWuT5r6hkSfedjzd5wUZ6p
H+MFCG9Q3uSY00INJoQF6pLXKx39YrU1yvJQqNchVM72UpP4qzys495+PIwEOHLNLNIt80rsOAFR
7dK8j/1yMD0dHO/tEqJEnKCfrnYoX4nTom5GYGzWf+eILjYLFKDXrIYU5dbGMkkiGXmpHjznznqm
drcMk/arygEEWfveA+f7CE8lXCUPuGXN6U1Zvufwm0tgaTlpwmBm8XMjhJTCDqoedGBDg4i0Ms6Q
BRrKh3EqSpHCH6itgxnm9MIlCcrDv5KyyMJNUzQ+yvsJ/LV/3b/EwRqdlyPZzUWUTBYfDxai2QSd
YFUeUbVgLV7LFuzcrzW7bMJMfIzSZ4Os/1yCIpvp5ZehwWpTlpmRepElMb8T/0TSRl6uBwQQ2YDX
VlTvpBeVjrk27p57wu/CAy6eD4o8Ixh7UEvwtUWRjJlCWdDQ8mYwi5UAT29AgIQc7if3+fOuDG0k
nnElnYD1vU4s55UcqeLNjtCBzEBtNaaMBcvTA7KrrF3joGEyaGrsjDYPn5XF4n4sfTULcLGZs2cO
ty5egBAUMP09rxtD1SUnwUMmYAxtkt8TOVyedjqZpe/s9HFwyCos0Pao2+hUogqDl4CI1PZ3Iczf
97MCmCNMRojlRgf6OF7eehW0Kmvd8SD1DlN+bi0fqlVsdp5QV9bRUiALrjH9HgES0e3Mltz9DMs+
fi9Weh/hsal4BZaLSIGi07GGcUX589yUSqwPlqJ7HEZJ2SdGP7zyZsEYMM9LpLdMwNpUubgLUqwE
V6Br2X2uBtSM33QGZ4LzXE2v5D+hp7rLxuPYXWZ9ZCP0hnzQuRxbnpQPMkBddDQZdPuYQiaI825T
oL0lQ4c+Cg/LcwD0tfJ7DLE08zdEzevf1adOMoZ1mORzUJtcQBKpMAmJznZVtc1sngIVeYLmrhfC
qIE0itEyNX5L5sJBmnacjPY4pyDJ1NaGNe6bZGIBMep0auGAv+SSIqRds4m6LzXkKYkW1k/pUEFY
e+WmITgFrScBzLm/dMuMjEIPpBkNo2P/5T1r8yoZ9TiyYmB20xcEj3lvSCT2O1sPB6FGTkPdzD9J
sodx+7QT91e5wAvpBIc/0sRjVRQYbjLUigduoz67dTge+/xl6K0h+2Oi+W3qUSia5+Aip45I72bH
2gmv4d4qZQQ+7uJpWobeRGrHpSUIDygX+sNNZfYVCu6b28VUCUGwF9LTF/Cz/dSIS6XQLhBgB0tb
giE8vqEUNNXYnifO13ttUR0CRwliBCCM3xtgYM1fJ76YptUz+/N/6jVizHvBhKkEgJPTDjkPY8fb
AATfeVwBQe5+8/B5z0PMXwVX69tCbtnXgdLP/+M8jVlkR3AkprZayV6RVRlEc+8n4xeYOtsg7Qy9
vntJYYq48+avIx1xo0hIllJwbOqqLWK9sSss6TOipOF+tpIsTbkaflK1bti3mn0efbmH6KgznjPB
GBfmeqGaClN3x6QgkWAEzKy/3IsFbQ9EdP1sht1f9hTH086eT3JgSO1qclS9klFUMD7j+zwZYkOX
M1MsuAIQmrgpH9ClEW49gLEeDJ/O7jrSNObEXiCLO/8shK/wQHvl25ernCvsy3Ems7FA0EmOtnIy
7m4RChKLTiwe7NgkWdpmfoH8TsaO6waynjj3glNENIzfCb6bQiZ8blNMy+6SqN6AWqhozGL4I9ZH
NNz+9Fz5gCFrguQ02SK/0De+ne5asKYHaVp9M7fNSCIGLIzAMiMjQRkYDpkdRywCxKdbC2kD3MMq
XeqtJv9LYnvui816rMnrJWWjiwZ1HA1gXX1CdNYOdbBdvbhEZY1OHKmQ/YwudwdJb0mVBuCgtqeK
ovYk0JUHaCvg69imEGtX7GDsfPw241HfZLXgpT2ZDKSSn8j4q1DT+s1a1f2TRJCC0PLIVQMXCOok
V3XNCnnUd0viZzD+rtEsy22J3wSgXePWEimlbocKEo35iB8FtWWLsdqUje3wdU5zwF69A/tU6Fqi
Q2l8oq4rKfHp1JNqgjsATC+3QMUYhmq9WwQ8CVeRQyKqqFavVyUnuz/DabmLgvcrrJG73nUo5nts
JNOldoc/VEdXfx33brqlBcNTV1ZzFkwg1WmH7OVTR0c8zv5T1WZGG5Ervi3kshL3IRHeXGBV9SK6
7nyKXLsap4UWYo8YdcnjHHbKVQncxLAP24/LOjZqUmQ6aNrwM6HoY/umnqVwkspUTWQqg4aDWdLi
gqtKdN2oosJIYmniakKxAIrBPF2NlMj+mtqD1rW0tqanHNrg7mxevy+4kT8DBnqUp0M9fs/gOrc0
TBMO/29HxbbLDKs59S5PvLOZCbem/3k1iv2UsKWE0SDbYo7lPWKHFlQrgLa7dl8qQO8f6o8fvEAK
fxkI71lgm0TUUN4/Ys3YL25WLPoS/XKRTmSexbZeWiN5upS7KnsXIQsLcsSXyIw4F7daI/DgJiXW
/GT0gfxaPrqN5hK4CNC2BcVHp88z8ikukfDVMoVJGYrmyBddBbZqifjy8JugshKiUMMgXzgJoEHy
21DDkia+A5wO7GWYVSgevVK+Aqn1pQVFR0nUiHGTbFpxAB7Ah8Zqnba03hxACJ8j5wZ8YxzOKrBe
udvL/HuiWnLemuAJk1LHa7Ds0Kfvxl7duZuKDr8Adc3EzufLZ19UchL5lqVN+mqHDwOKDJm36/Vi
k7ncHUXfh9a+X66XE7EALrL0rFHJnC6P6mB9SJZgBv6CdeaZPjVBO99MoXisJjjvgOgbvOATg/z8
HYv3dF0UIv9egYw4tuUvoY1Jr9aB7pKRJjNFvRPlELFWTa8h9ewy8SUxLSm+DyU27ZObXctr+eWl
eo/TzJiUdQjKCoZRtW7Ac8/hJlzBfhf0Ff3a0qkuCnZqQPvMXzRvQO0wk+JCponRFtkiTtb5+/lB
jnaSpAgCfLw5tkogErss78vvtKtAtYM6WkCUVcemHMYxfwvOy1IuzLD+3S81hpXV3d1A2l1wdQxF
12ZZr6+SRKSzlUeHJZKkGNAUvDel3DakRhHb92SWHCu+0edKYUDQVmJhsDE4eOWEACOjF4Z4NL0j
f5rqtvVHEl5b1EZ8RCR1Eg3CzhI6xs5BBnTxZfYlN4JgVLPI+hjz/4FkWr3cZUbZ8vCaxl7gBjQF
BD2ccl6LxEhti4KtpFdiferZ0o4fGd5AknoJ4nVMnYqbWNeuQfLg3Gw+Bqks2IcHEhN2K559h3Ra
8hT1gudNORSeGZWCQkItr7chWs2fc+32gAENu9/Bvo0//Ms8pP8dnVB72oAoJsYRQDb03WyLRfzD
rGNUEbDD1VnkBPwVxu/Tm7BjD3mUoSkcFdVZGHMlRL2VHt/kU0FX3v8ptTfBt2AwnW6iNP6oStlx
dQfu/Zwmmx4cvHBvsvbtsU48D8+ByzooFWkMkpM1TOprRibWETo6Ks5j87m5s+BU7KIs7IRoz3x+
d4K3rkk1QShFxtqt/MlPOjzSDPombfwgFVw+8SIKwmrQdMBN4AFAlZ9Y9XSYe5fCgTC8OeJXU9zT
hQoIV+ewlz7HktcQs01vl8WQWR4s2oDyRBSLmGnPYw8Smoec2pfS9KBrCbIYlONeHGG96cJjus9n
IVsGDdhc/L/9Nva8/paJ71wxK5kmkZmElWCcicUTYxf9qdNNHxwQb0vQYr4POdx0ytyTCXLcXqYc
1FvMqmeezMCO4kV31lLJLAqYafiti9yntcPstXAuZpHF0YktFYWHz39o5DMugKTL82aiDvclDQs8
fB6VJ56Ewml9o+HkiXIDRJadxNe7WppV4K3X/tkNOQuUDzv1uYAgfJ7LCLFEITwFNr2Xtt7xlJd0
qbJ6Os+ABQ5FO7RVgRiFsK45V+yMx8V6S6gKM5eVBzMW/fcxLZpe7G+knQIzZggbtsqQWwfMTtFA
e5FI2xxeU4nUSaIQnuq0a8MDyxRLW503uavxDpJnAJdHH70+9gvrv7u6RaOhA/EvANqZqT7OpNma
rm06u35n80Cm3hbyHz+MzH9aZq6Gn6AcT5RXHJEqoN/VDy0SAQGQPPtLPxPuhl/QZLQ9dk8fD4mL
FnnjSNwQn1JtHk35tc1QItxTcAq66KFkhRZqNqmZYtYCyX6RtoKx7j0r1C7TXOjWqslVm2b5xqhn
ER/UL6TqAEyRuPSiGXRX6XetjoMdVARiqOsm8lT7wgS26+9b9WHieAI0aS5/v0s6W6a1upNe8pri
Oeu3Xa9SK/NA85cCr2jMR3CFVKcgDIgVDL3LHk3JCUKZaa3ubYY4qOozUHV2FFZRXz9vgzgUm6Ht
9Resf5LosYbjXzLbxvpc5HUl5A+/SBJEmEyKKWOz+yOl9TPbdkQZV9/ram8HF1sKjE7C+f0bu2oQ
mC/wudQO93g+3WaMVHYCPE9jbsTLHSDFEB/MSZszmrAYEaMp3/QUfannuKKwck6RtG8Xy82yJ60j
QWBaVG+rrnoPinw6eX73/qleiqH9Rya8ueVuakErl8ZwFxObmt7/vsPpCYiotqS3B/j+EkTnmXPK
qVLByf43UyQO8km7dpTkvsBpBHh6zTTuG/MoNx1GKDOdc5pfCv3cihsGc0awQW+HNh5wu2hiruvo
e+T5W3heraSDDA2Vy4yuK6lKx5LUAgdSmzLyMZDuwbYm1Ko6NNOKOH4KpbfAI7MGMMrqc/5DHg8f
iPMYDhJ5xF5/4W54JH7Py4nad5ti/C17vggXht/UE/DKjYFAj72ouVl9F5a6Ia3+WBmuJ3lOzoMv
srRUgpSxiPMhbOyJWYZ+SQdRLb2sYBSy51lIBSYkpnI/EMAKtLzDvsyfCd6vhfujISHYg/6qt6PG
AET1T2mqhEHjDCO8ee3J8tCL2y6DsFaE+GbM4P4vw8CuktkSFjo7mf9fPX21UR70rcSG5cw2mf4t
5BzFZQAaptzJc1yLsZ8DCLYymP/d/K0kHzUDlzYoDrLWlsS68nbmrxrTI62sVbN0IOUF8deDm3Vd
8DoPY3sxXaF+w0HWx1dc6UHhirAGOc/XViC2L1qLxReXOkd2SdotoHgcGHkTYn6AF+RHFIUllSpo
Dp8W0RZHiqsv566m+Du02RyWZyQ1F4cgGmM+vdHDO1pAiP6ql6am0h3/yVmFqTE6IRLSv36UE/qu
PfOdbqL6CeM34mqUo8NjJkX8LgJLlmUjHV3wX2YRijNEC6oDjc6dpkE5rGUnKVXvp3XA3qRUYnki
hYKidmo6+t+y+pLshnqje87bMt1AuCYLmlMRBgzpoLBuDk5kdIUb71ngl0RdJ8oOeH3PnBIMmJ98
yr1k6YF/Q+iTaZi2j1EPnGnGMPnPcYAUaZZI6U9kJL1z2OKuhboFZDaXN0wTuXFF8JN+egmgwaw0
c32zMZ1IfODRpIDNWGyuJrd3xHN1/WYhEA67HTYhLrqymfiw5B9hWB87VSIrTGytKFkJzXmLzLbS
nsCV0G9WMNYUaniuliYtEYhjFLQjCtEbj/e4cjqy85Zbc7etjZkZMRerd1rYK5bfpNicmJPQ3J4Q
fTKBtmqdZwtCS/KyU61mqZGUFC/3qIJLOBmkvdLca27I42ARRHDQ3tRQgQyuzNSHwCYCu9DqFDN9
FYyL+cvr7ipTAZ0FkdSIdkcbk4xkOM0pPFcqu7VQlMGyRl1axMm/uAilkc0gcNv7pC8cfDghBflj
w3SzBGWflZkQVFJXTL74Ku6Vh6OcZqxn5QwQV4mDGCRb6Qx/WFOOXuCEwltZSdxTLxK2KLtmEhNI
oiWT0bmQbQ9K2dpU74wxwetu786XpHR6Msjo7PSzR5QowWNaawSI7OOJsz2IPEd/xxDyhRwyKOMT
/9/DiTr22xapGdUFB/EbnVoVVh0nR/oB29jTFgAQ5Ry4Y1ViyH1JfJAS/W5uHeriOZNEaahYT71a
1Hfrte46JK/4CV1IdRD5k4spdhmw8nUu5M3QyEqs/tSTgpGXlYK/Y89ayiQGB7GBilKAxaqPzsGb
iv9homGcB6P8Oj1OrZd9u32M3OZY7wL3eKZsI9WhLjWzCsx+IBIDqRZLoPsyo1ZDUuHYSHO2dgQf
dAqXRkkP5YH5Q32TwLR788VprndGCxHzmC+KT9zo2RATBcKGwCl+OxC3U6BTGscTd3gAyFO8feYE
53J8DQvJlH8v1MXN46pV8GALEJU51LR3XPj9IIPYflfOzJaEMzOl7Gp/Ayg8axD/lhVEFBMujaUz
PEOWtkZe4INfItUhw4o5mV5Aiuoira8GseHUGzk3PUOyxMUkwTY9Y+SyMzXNuU3IrzglxmKM7ODN
xshVvHcUNe/3MnLX9M3WTIHUhJ5FQmHqkcs=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
