-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:44 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_13_1 -prefix
--               design_4_c_shift_ram_13_1_ design_3_c_shift_ram_10_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_10_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QcE9JgSZJXlEdJa9l5/KM5UAOqKlcDvjOrzeoonFUtyvaM2j7QpunRFdD+nqKIU1Inva1SMB8nrF
gKiJvHkHyPRrcgMpXzTdYW3AoEFHbPojwVnbf/Su2642m4HcBlU2w6q7CBex29vK233dPM49gTvL
cCjsqfF5ORhuiFJh/8Y4Qm00d9ykN70WGas/TKecNJzkSyknK2KrDJaXz143djKBIW8o0Mw2Uk7X
u9QdFSxR2lHbs2ZcYl101/+tUkRm20+ZB2IFoubpRsfyA7Zr/1ruVO+cCC8bYU+935iCDHBrBH8z
gcfGMlWwQLEwQs0j0AIiXXCzbBIbg9jzFgAdRw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
jEDgf3L4ROYKqe9m3y7//JBHbzABVZ2xw8rr+XLkcU6/1i/Q+CK1RrUSapYT3hns63S3dguuynOy
DYl6e2OTE/klNzp+twCnQthUEs4Pi41pIRPX9I6M+LVPP+pxJUfPeYAZhW+hSzxOFiasxpKmNxCl
+RmQ7gT/546CTxEn1w68kZXGG1W18g1AfLfg/nwd0jSr4BYTMeRE4jhnhSpINxIL+1iEWeH2e37C
laxtR9iW6eG2IkceRmemrmDcHn/Wgh0+jSmwlDtgbODRuuDkUu9xQ11TMt8SFzrXR3jPDxHZnvcX
n/Oy/YIB2p807TmxP3C0h1wHG/RyvKj+rBdBsw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13904)
`protect data_block
eeN1+n21z9K3nhfJGWg0XbWtDiV9+77zj8WVdbrosEFnpvClvFzDP0X5WiMAU4ChdkrM4POO1+jA
ev2lwlrV1CwUv6jd88MhllodabxJ2z5vE9f+5Qrg5iSi2f8WKft+F5PRZ9D3zidbOgUwyWtVcnVz
GjY4iPz31pboARM9JMAqQwNMkdYaNC5I7+BO9756bo1rmFtoJQVfuVZDVWH3cRzLhVFlOObWGVqE
AnnwPkujeqXtoAA+3jVkkzG8rw0wR4MmedKnKGkgtzkPb6vMEtwidMCohacF7UHlUVaHBEOxOwMY
BhRj2myFfCMKhbMqpwM1tSHPxJIhvNCDZHzF62szLn6bd7ySj2l5LUVoQp1qRaijsj9RQ6qc2w6T
/LRoYrbojGyrbQFicMUaTpdAnnneK6FZDjLj8fa/S3pqok7K68YqwCXxAup1OAHtDp3nmKvcY7GI
05eoSUayeKE+PxKrnSi0lNNuH9nhBiFRlBJ43YQW5cFbBPJe6Xbt6Psc9x/qU4cvhoBP8jLAcy4u
BDBDthzuyRIZpEjrmroJ65xO4f1kOzvALsSelNC8/ptdBhbMGiynExu7F+vra1ZWF4rqrYczj4D0
aEqqFNfTGA9rR+zlRbYmwXRa4V9jH/dFnabQCU0TzNpo21fyXLBOLb++pEJIZgBEFIgRRbNhV2pT
uWLCyFVAPgUAZBF+8vwRTxSgxMliwWxqfthdd0dSug8/AXC7e1F/VPzp1NJIw9o52z/NjI3RHvdk
JF6YQFAIsZ9C7nHyEDU9bM4MJKfpqN6DbTB8PARzsOlfMahxo/4UdNVA/HAFf+51HOur3qBZh60u
tvNnu1Eaim7UhwZ38shAf7TDryCMjJa7QAArnWFHBW+28yuEeIOee6y6BZsXJ/NqYbJmYLUT4U6F
HHgUIcNvF0+WWWzpPVegPt2dJ0rs1cKzaNPH77oLHJVvymDsEaXbYWta/mLN7EC/nJ8bK21dQ6XS
0U//PL5qI6X7yGi+XIrwJbEE8pXCBL7Uh5Y5wmLS/Ktf8n34X97uj5d71hkgti+fbk5vNNhXAUzP
R8tZ5s7lfmyYweDUk/5bC5zF59/+2I0GUCik9bjHVG+ryx0abdyQ+v3Kepm5sziTgVDM6khiIEMN
k8xMgfQW/1KQ4P8IzodrBq7BncvnQVqHFVcnlmV4+CxpiaxBx4wtNye/d609pMwyw3WpNgRfUFeM
4ip8t5aXapJe/vaFV75C2Y2orbMFMSebtBA8Mh/7k8MHveIypOvBAfFCw45qQMAR6sexvYY3eKhP
i29MkqjH29y0m5zZrmtu+2yHtJ7LE1xv5MolZcuyHR37pn0Mo/jQjwcrkdhv3H3djCRTtiDAS16e
jj4hfnOl4+VckQQFNRQRA1o8Rbpumu4ZdjaikZZ9BOiZxyIMqeKB4nHPgAAhXPSeRtkeW8iyrGES
/T24eq2pRaO4OwI06UxY9M/Oak5nKlRFRsFBGqoVjWfbN69LX7wtuxSIl7HDE5ogwYMtY/w0qugN
cFvX3WGnhug8MhfC3vD+idwZ130cN2SE5Om72pd3heI3vNOkTBaX5+Sr7LQ7SpjFxS3GVYKth4YM
edjXsl7hsdrGl9kVI+4i1w4alX+FTgmuIT2rd1WGuy+LXGn5jYCOKJF7mz0Vxl3nwSfIcJ2Yoxxv
QGFSaXunI1cFA5RMN9zer5UzcEuKqoaLdw7YiR7s3Uo3Ikoa7J8/QiGdHkOmeBy1VD1Pc2h3sGZc
xjZddGadunL1mMfnX2o4ZWZk4QQxrZKXewEFVqt12h/SNZCBLtnmSnjB5ttfBWQiW5aIGF3E0Lx7
BykIGtU4bhAwGQslPkFGWnbkVIB3s7veAhX5labgyX/n/qX2Y8mFogM67XiS2rLwNMdh4clP1wGD
gupZ6LeFVZsl21YSG1movsMwj+dcBo0fkRdPacESkLmaU69o05k+yn0GVJ+7zH+7S3LA3VWp7BId
wscxsjCdBL+lrN/YqNI354XK8+Q3ULHi0bCyxYPtMy6001KioTMr+AiWPEUXuYgOTzYvQm5mqpL1
WGZPTFIhIrSE2y42V2w8/aHhBiqjeVkmBdagcbc3aV4sMCxhuWQTOPKDto6C2BjLH5msLvfpY5pc
vDB7xJ6qeLXNF3ptVa3kFdmz5rPbLZVUCdhPsa/Li/HQsjFcAsko5J7jHoDoCoF4v4tvZoPacgao
KeUj8r4GaszAXO4relTMTllEGjyGRsKj9YoOqKd5J7WVutYCSjpPC99J8nKFXvIDtnV/HH3GlwKR
47UxIHz+T9H1UnbTSjleK+MMWIz//UPB0p8xn9Db8CH3ua3TwfuGon+NhfE6nsVq7CpDvdsoBzjb
pQ6S8pwL73k2PoTX1kc2hfET6gPrTgt1sb1woDUo0O3OOfrn9ZUqcMloGY2LTlNOYok+EAipmg+x
gDYM1duyic77s7LA7S3y2dUOmIxpvM/4jwHRWicIPZAdtLgfKiQXdKhQSlSklmc0BANAfSplFb2V
bHyOkjpfpI6h8An2R0TuCiodBA3F8JlsPo6AhyK56/nvgE7sR3vmueLnIZR5cUkQ4CSLuPg8dw3P
DPFGoAB2FUTtHIwlNKluTmObxK/PItdWWEJHOMj09f6OfvTLgpKWgAG2tjeuz+YbsAAESCxUIsDU
ulu6qJ/5VB8gjWP9yyLiGA8nl2OCj6DZyJXRwmedaT6KB4J+D6xlBhyOxfQje19h7MWsCr/iOIxH
/1Ujo435p0A6QEAqx/yeaUjBhTXsebJ/lZyUuLHLJxieOAEsQsGawdaLfIXgvVV3GaP9dwwuMjli
5HhNIvtangk9J8zryuArG0PIAlfDMRZGWnRK6yf0HmXRp7RHDt15SFq/k93DrPbWoZslJY4jG0Zu
mZ+qkFNT+0S7dOBcnqQWOuuL4ciOdOytlcjWMprKN/SUjHEclzlNhWlcSpa8MW4PIcTDHEdA0AlK
pC3t3m2H2IIuth9rrcC9O49tiIqgfFiUB8zjTv3rMCK3xdockHO6WoFKE2amvGL+S27EDljFx40v
MLHasUUvx+V0EeRBSQbB26TIMi+P/kd5tDbIU7S2rekxnZ8f2AxaJhhjIMAjk0KtcYrcNy3BbcXR
+om+YlSP4iRyVm0O8Zx++K6DgrvF5lVAo9KJ9nZhej18LjxDgEyHjf/5/AEmGCFZqNoTsKsWGYZ+
AtIA0lOhH6lFW3/1edENAoXwjkSPRYGrGb1lvhK+limyr/ShDOwPU3wVLOd6cHD8Us6e2aYQmsVT
cHlkjzvWd4zAVYOpsiVCkq7eCOAbbAsFmdLNIRYy2oxvhx4UgsdViqWXseZliSnMkd5c7CLFUwcW
Xq9uOQJ8OufBvWxsDBhOXj4vR59Qg/+C8/grPgOQstzCKhBpx+cnDI0ZoYqNPAcs2TYpWNf4kDe9
rgBRukOTK+vBfjujIEElWPVYf7+kgWfiWcviX9nfm+bLO+C4NRS18UropZSSonZoNJIhoKmbQR1n
skxfVQ3oZT8OxZFU9GSy7Mew5o4nFDVATAlbtCwYDWNd4+VziJdKc4mODQhUjB7VKBbmxRkTlNXs
IbwFUFHElMaZ5Ds7w5IIw8oUc2B2kiuaRdcuR8/I3WUegPpXQsgRZJNOa5pYoz+LVYBQdPIUE2OU
ILo2hyKHMzRgf8fm6tBeOgvDJkr2+nzQ/fHNRLwtNkAxxn4l4Wu9e+uqadpgEmUnK5Yc/cHuyYUU
G8KAHmCkq0A8Qrn35f1vb0sAE97lOpZMDVsxpCTPyfozqi/4BdtMU6sAz4j3GRuh5gSsC6jnPLuP
8uZXTM9hWSLaDld1Ln57aAaJgfbhRJGOAMuRvg2myPMCtZ0Rxq/grpWCKehOPXTcpwmNLbXubUMK
rFMtwp/Ujel6uTMx+oXt8by8j1oEvtcuC/3Z8NopDL/3XHgS3z6DQdR4a69hjSTivkn+siI4vRLD
j9FP6TuIyXSG0oUJZ3QBZvn8Gbmm3oQJr3dnpxdYHR0XstwzDUxwB/6kgNi5DZxM3qApBy2F2ki/
WtEcwSOZ4pPMV0v+GkVhcTWaJTFrX58Ql2CmummgsHWzYCymxZ+GJ9fHcQoQBC8sCP2G/O/432kp
MeAbef9CzgrMidIwToXLSWNmYGLJQ9WeN4YoDZLyvNzYvwe07hQ9akMN2mpc2dvcljBckezzYehP
+z6NFQTb5oEhhg9s3HqxqYuWZQWHIh/gr+aV4LwwLjI7Q1PhcdocdbDHrWcfDtrhRrDTOeP56ouy
uSsDnhRuSiudfKSXScc6tSP4wwTk8sIWiOAyv948ZTziQHOhtHrgJjkihdvkJbAzpyuPcbqBW+dr
sclSztrzsPb3KBASHAtLFiUlxu0+CSnXbjflkgjT5yNftOXeywnc+ja/nxOMFRMSdK46CbGdAYvT
RvcN0GznuW1HnknV+82DHPD5VJeVlImzc17Cx/BuYrBRhGcQ3r0l2jKyIGy0mkOz7Hb2q+BO+SEL
hpyETAS6E1dvQpO/JkxI6UWj+q9kVZgMSnpAxj+TTK1QY7ecp6UiPlP9vVqPsHRtkgbMOrM6URGf
4TF/CPFjTSPvVkOHGLNtqll+JPsgZmLppO9xJQOL/NcITjrdHDMrZt1fas3j8LxkWzV25MOMcoSe
twEZ11N+zTOVfVco3O1mvdXwdMDnA/qfo7OdZmNzAZkwvd+7P3VQkf87zm8+NGTTiqz+fywM9lxE
dxswUIPMBlOFqPAaef3VQDPe8kRPnNj1p7uyC8dVgK4irM2zlZA/mFxuvewd0aeYlbzGROCDufZE
aOvwqm+BzX9CZ+FMsCXBDMGULx6zOsBLTwn6jqUMbaaFc1Nwi71mpQRxTvLv59g31FSKOOhZOIFP
+5jrVYD/mr86eoZHWxGnbeUMAuzVw/+7iXzytJC6USdVoxalYyLcN6ycsnl3/YNQ0nobL9Jj/21U
CHfbvBHlgJkRJwp9E1cFDAmvPk1zh1g2c/2JvG2/0MsZRtRzM0CQloUCGTOA3LBgSoZgqH1xcfcJ
nVfs9gb4EgIHE5Hov3gkBbK9U3SV6QI+shweo3qgwf0FBNIJP94ucJ3LUXDIwV4UiFnVlT4Eoyl+
GjW/GA6x5ydy5nL5JegqNfV/E49MstGIXpQ9O+ICiQWYL7g4oSfXwlaAcLdYnGWjfSAMXTb/maoA
qhHQ5BtnvRolD3UlBKEEfgX0BQDN1r3Ee1/UsKhb4ybHrosi6QGkVFoybPLmQVLvaHRf7oqCh1UV
BtR4PsN+Ok2YKOjlj5R9MlxzdaG5csam3thZxtla3zN4gXXDfN4W56kXA2FWAek2qrU2SRrod+fP
Giq1ymWy7YqoGHpVfXqePwvoE4bsawkfOZlf2T8elthwmcArszHxDTJQo80szgMFvfw3z1uP2ehg
huVQ1skKj7J1khKGVPobuF9AGZRdbTEQ1oXree78olmVALJf/3zm/2dkFZKSy4TZIRP2sBSsUsRR
IlYwTY+nCM59PDDm4pDGfeNU2GflR0EHwEmIoUHOmAlrrrU2KqM0mpRXIw5yysqO1F5bx86+6ENk
GpQ3tgmRs62TCst+QZwGS8ZdvZnJWVA3q9fbMbUeAb28uWj6cKPyhqxS6w2GtH6FmpGy7KrcxMAt
j59wpsiqg6CjVra6dFmK0Rd+UhdfmNO4klSfWZL5Vt6ZM6nV+K4PQ3HVwgutr2UW51qo1YZKNjHa
l3C9VxysEf2djOnOBn1H9EVyLBgZBpEpawNyvaH7yXp7UqwD88Fj+OoJXQ0tZWoLJ+i0Mptvfzty
0sUktk75I7i0lyoc1Qn5eQtl1PLRAGp0d+qk0YoxttIs8o56f1TlQhh5/1KDdysIbB+qa//GVaxO
+qPFzKHOlB4x0ajckjl6AV4dOWb7nv3ZUBHL7xPG7S8/FmwBf7GOVADjyyyKTw0E8ZDPihhLc2xS
Vu7YJo6JsMP1nxBNMPwo4PbeCWBKF2oQY87BUqQFGPKiEEDKIbMu6WiXmpc25U71sk3uZeC3ABdf
mphXByJgvnCfkUBWi2ZyRrXo6u5rVyghYug6SccGq3d2vPWnCYL9wq488aBeeop9wq0QqGzAWyBd
NZ+dsThMaa93Ok/1TJHT/W1nCB4B1IOVk1TpTD2fJqU5OJ/ap/pIgkwn2RM5osZe1SsdTlrtX4xR
5+3+Al3qKNXN1HFL2xr3nhJvay6tvID6ZstGpJUkyh/XcSPIdH3FIY0F7ZiBTUoNk7PBiTQdhcvR
bfjhbyBaMz6FAnsC/wKglkKPvDPjNqa+7/biruYJd4ur7RLj57SvHM75vqCEcu2hPgKSuvwPQV7j
/vAmHUxn0YQjaZ994mXoOg7kx6yJdcfeVcU6/KiLajh+fYO0f02U6B/5L3DrHoIIenpPz5dtyIOa
HoiO1G2xc2nA1Dhbi8kuJE4EfAimEGQ3+A3PAX3xcHYpw/QLsicnI0CQ/HVwtw2wxZkyF9DV3iw0
1AqcEkK1EjoapLKJYARz17+VzLw48/4zIQHNB8DUYTn/1ymeeCQy1/5Ept8bQxGPi4OTvd5Cg+Mf
S9BFF1PtcGqpXJnS5ZpWhJB/jUz+s0Z6FReSu2qY+nM2RTcyt+wVH810C/VxxGPiZhVbR7ojzkXP
GvgDhyqt0gzVd8jIMu8yPYTAHTxoFXp5ZIHPIL8v2uecUvKcFylpGL8PsQw79bYQYsaC5NRXBV41
aqoBMRPTDFXPJsEK7Rammc0tKgos/bJOUv8+eczqzi/5t9f11dfgopm6I63q4RGSMv9z0sF8gK55
QW/slaVu7uqugA4k+Aw9KZsxoTDADHVY4oSsfhGQI/2QetCoCqI12pm8aTGzYcJQJH5ucU+JZu5U
yfmoLCSJwaHxmQxYN54i6wTFYbm/B9Y4usdClM/dIFHTs6HtaaIeIqhKm6pPSpjE4yMVRC1/ixUA
TLBeWYhOXRLvkxzaYI83XesZT/4e7hbHXMLjVMuszkk6sCDDrCV2Jg9ERrF51VPFTBg7aZanoLQ8
f0SpzA2ZEMZ3YL2wd7AXq+oJutKBvqqub+P8JaNVuiep0gjwTIzOopzuoBdMOpN0y+o0RtBb60Kx
67rYNIKx1e0yUZfKwW0Cto3a1a+aKJGfvgIhi0VhUp5Q3WzkXL/atiMojVWfrhz+OQJ7x10Cyk2C
blRaAkoyM2g2EV6lejTJH/ZNe2vp6Xt+z5fPhL1pMuClq1fmv8KjLuF5r9DIMfTUR3M+9tDyKODu
+JnhJusD6vPbo6i5LbrzVi9hzQW352vetiAS6OfmfIHIvJwY795wmQWb3mg5Y7tu+XIXTRty19Vd
kW1InTCS5cNZvOgc/2HqNd65sQnleJGzXcOvC2NS/61EArcHaxGMGlPoDhiVc6bnw4fUpAftWgfC
N5XFRKUOkZ9VJhRC7SoRtDdDrKU+SlVNhgtAtPD2XRXHKI/IUIR9YrKWQ/J96Q4aNR/70lQnjNmm
L9Y3zg37SJvHSmNTsD9WJ6EJkoW0c5b/KJud8MeBzQqfU0bljkqcx3VmAsSRU8mAQHp1f8vYbGw1
tFjjVHYwKy8ZPOU2xeCTV0Ax6l0HLMZyBHPmwPU411Ca5Pna6asIYDy5qtD8CMYh7KWesDBrbXfl
DyJeG6XNPvNP697hB/nRmkpqA5MZR5GXxR2uG+AMehoD5Eqz2tJYRC05Q7NZuuWbyMxiqu5sQpBP
ZjMXcHa522wiCVXjnwDnKkUZDlgjUn+VuNptFdg/TRt1iZ78CXyun+Vi94g1dxE1CTOKWzvzg2ct
zijFDoG00jI+syrSzZ8o/guZt/DdGurnj4qUCMkh0UcRRkFtrEXqsXdBmEbTAjxCVb7b/dtQX3K2
J66TPw/5MWxNxLGwnMC4BNb1w1qSFH7tcW9vlKU99J7wdrqKttvx6CIf5qsHEqeAAlZJRTjnygQN
pjTFhfJBIgIOMXrNtfKauApDsha44al4xxddJbxqImSUe9Ua9usQ3ete7mijc4tQINQzIHJ7THGL
mgeZlQxj9SsxAG8gLt3EDrWY0m/4cgqFII+k8e0kECz7AVhkS0sNX9hNIgOap0cvUJifLuhCf3gD
p0+f7DWLXlSJzWUBlaUkJxZVDKF74RW20UdFqeBacsbNpVsNTJzXZ49BfLGzhfzCPwJO1LJkqUs5
hONS4ytBYMcrJBDnV6kqvHMpMwS/gguOLbmsVqI/d+sN2md3vHMLj+9ngwfUjBaIQxJ8sUN2mftl
c/1LdhUK/t70JJfa5wubHZuYX29Nl5hX7+ZXAece/DTT6QZXGNTACSCGGt7oxAE+NJ0S0WuPyzVk
ykfOteY26LDYyl0txtL3mJ+Y0u5GRdShEK6x1frDHFbuBT4JNwL4MYC9e11NgUSw5xuhX6rMKUw6
SJwzwWVcR80VGD+gueXu0SX4wCUgCpSc0PXNxUEyCFeKlBRWyFQDz3wncc0vpl3RbFFhAYn1kaz1
+4cIxHecTjTlGd+WP1jWVzPpwhcGjdwyG+gl0RhQZj+wbGp/pKa38JOoQRLkxdzRQgTbKO2o6Vdp
atoWOQl6q128UKzwJBaynMd+Ty7tnR3P2O2BJq3PPFIaF5FCtDs27AVNfsUYGIrro19m4Wxzydej
jXgcJ4u/oKuxvrcdPOAMLw4Ps2VZsTwJ8oDhsb8rroeAesYFLTMpnQ1WDyFLHPkywq8HwWKJ8G2L
Zg2apyeBw2vox1D9lU87TSDeormMIcP3DdY6g7m2S+Bpx6Jx5sPG2I29K0gr2YdXvGVGMBmbFzGr
7gsvNWgXqEi+yoiwpAvj78nDucwyyl1iMZNJaUwCiwXPiPV8HY0cEKHFrPlwB+zzOyrmTHCVIoqS
tPoCJya+f5P4twJ8k/7iXSoqTfJJSD6Mw2ok6ocmwUrBD7CWKVBM23CuOC5/Q/Dp/Cnu59vpZHRF
W38eFU8F0Rv7k5qOaKPt4Cti5hobyfeiuyWTllW9PAAxYECM5W7w5dw+Q+3595Rb4Bc5VChNHK8D
caErrpfER41++uRt+gLAd0Z2VsbnbTrE8iUOQkXuLA8O53i3XIwwJyRBpdy3atf1YAVF7HWqV3Jo
77MRF0GqBdHC7o3UkX1hM+ewiuBS0pIiX1l+0AI8n3+jBEVwDF2rlfmycbOP+H/aERNu2MGAMawB
JtOdola1ZexiF5MBjLG892zaYcFw3RNAVPjjH0GvB9I6PEcnZG9SLFJN1zQM8jkPdqGbnmSbnhVp
6lPlHIPZUWmqbmV7nuKn0eyBwFplkhcm+IUxgtVW4i+nViauPlBWkVXGQ/Kj908n5YIoH1agzx6X
XDPOV7YSJCEw5Dk5VORsYj/CgDsFcbJMkzdT+/CvWkEIMwxP4N2Fpyqg9N1Gl9ZqG9UoLplC2WK9
qUN+J+ZGazhkTNGq0yjwmSg4ABWkuHKZ0BUxXth9YK6+ZJhHYdhaZFvkaPSDzcxNOhfn0sTDe2Q9
7hvtU+qoLgqtxl34gKPAgOL0ukOGglG61qlcWQpUjTAXarWquwyX9wJIeyB2VuJSZmC2DnrN4myW
wfHEh9r2rOIw/aBhVKOaVchD+X1GaNNXWWfyWmJlVxiE/gHRAIlnS4kQw+IcPp8jlrqfRu7ApPxD
aBd/EN1uPu+zy6RnUgMWttDzLQx7QhvnLnerYuIgP535QmQ0ssYnObGydLO8eIv0z0AmvuVrh8EU
O09r4PRWKxiM5L6iRue6exNLA7CDM9dVQ1rdAvs10Ix96C/YhCAJBtp2ElzBvoBYle2/gE3oaDnF
nSli95BDmMd4JInjDKo9cwvXYqtm/pXACdjZwbZg08SkQHgElhax6ASO3QcFps6G5Sl7o2SCy6/d
LHYxg6VHmJ15pa8V/brvznKakXbdHaBnf3zq5snI5TQu/XyXu2CcKKzVNBXvjwj4Ddci3w87IdK5
EzxqyPUetQ2/VVjWICCoh1wilJHQNDUOZQvoHaEozqqiw22dUXjcQHZRYiyp7WaKsq4YfPs3bttt
V3u6IoLwGUi0LZGWLte2DxRm5PbEs+NdZcbptYDjLnIe3/5pgc+f5swSQoIIArvfasL0AQku0GRN
GQk5GksEbEXtxwCX1TbF/Qp4E+3OuVHiPkKJiX5338XmugMJG2pT9Ep55cBApoIQHNqqClktPTBh
5gFz4uqzGB/12XToVo95x7XYpNDr+PTq4QqBf0+qJj52QE39hYn5jYLh43gvKRgxoJt2nzjs2I05
ZtKavGaUw79lJisBG46Iyu20r40mgbGuiQpf03Gvik7ivewxoWzjtcBSOoRf+/NNd66Ns0juQRqC
k6WqizFdlP0XYzazRc36luLYyngWktlEd6GT9zCXyeRoXfyq1sc+CjHdfVgxgxljRLRnNSqLhlAc
QxEJYhzQ4FB+l3J4wiAXxtC+iSy13lBkj6lDKFqoXSwuSB606xD6MQlLIgb+XzpjucrS5R7mZ2dp
Ys5NCdPWQ1ByHkiZx+x+iVVEWwo7Ccmfsr4Vou2ndgY7EAJTKHztOGwHUFi2/8ErWM9nyjJOVH2R
Iy4hb0GJAKImr1Ek6DxTaIT3MLIGREEfCKCKayZUIhI5d9WlXMhBQW6S7Y7sZHFFgWTpQnLXvRXr
hKwScJmkZ2Q3hmY5FG5+NNBje0msl399GalQ8tRbmAh+7wxK0jFMvcbvVZ8mTKCdEksz08Si+v9h
zBvzOBQn71USSBs1y//EChLLPCZ/Rr+xdeB4ruZPR3KnDTYC8jUsmyFStoz1KVJ3O7k4OUBYhGgl
e/fGR8/AEz+T+i+L5PSVeqHwTmPGkEoO31KIG0vrhfdcE07KDKtmROLSzoJb/E6aivatIhZoJKV2
KygeWexWvV5bTMvUc62+01J7I2J9Gyab2q6UWGXzpwyv/TygFX3xOMYZPRXiqu1tPuNhkgpz1vYV
lSpR7i+2YblRmM3npsF2DyBfen4pKLGURzbXSw+q3Oi0Ox8OZZHYeWrfgQZ0Nado3uuKg0GMM8MR
AZP+RNl/PQ9q8aXfj1wynYS/bW4H7yP39deCc/iNXrDnN6gD+FgCR321tUjWY4eH6NwreQuuuA8W
mNykqq15e+Je3dK5HaJ3nH8Q/OrxQ2kMt2WZkQUC7L3o2GfcfXH+/XaFI+/n4CVl5ZwE0VAYB5XX
u6n6oUpWkCA8/wL05ABVr9TP1BIQdsN7fucnKMtnoL8trun+4xbVtm/mwQ9Mxy8M9TlS4aclyZMq
UN4NyzofjFXEBOzggpkSjAP0upqBw4YRNM9N4jBoag9mlNqIlRI1NrpmQOht9sL26PlO6Ydy8Wzb
fQVF+V+r6zdjoqfPetvt+FR4gFDYAZwLtQnpglJvFtAB2TdvkSig37dDzXnnrC+2a++6RScgAX61
nh5Ott9Tn3A27pLOvglh7CazecXnpEfNidbCp13boFU4Qz5frKxQrg23GwlVnCgFED1gNTs6C+xN
A0xSQq53TKvP7uhT62zZ3WQIWXuJvbYV/p3IJihcDutPZ6g32KAFAl8OV7dPmQMAqdIE9IzZWMIf
4ojgxHoKCu/bu/ny9UjZvlOnwSiMqk+n/NOS8TJ0ejMbnRXvxh2fZIJOjP+5LDbZ07zkpZ6nLuGW
U+SFWk9MHaWLC4YVfbsL8t162+qXf850ogIuhc5Hs38U8PuK20h5jozhvVZOUeIRFpGqCYz3oGD7
0dfnxEl+BJGI8bb5vkziEaHH/Qrv2EhT57Lu4L6e4TMggZx4IQPUVl02C7SUxzyKVEZ2gJKsPoOI
l8y0jhEZpCVfLsXapLsAKgWQKsCunSRIffiyfCtEjdv8W8eY2eX58vACVqUVGL0OAIQkqeguaj7h
0D7oW/xv/d+691cus82NgRyK3jocQ6h0JMmmCtvlcjVvvgBSTkHX1fMkjUgVG5qr0dxuv8KPJtM6
HjccnrC2NloNk/BLx0m1nW/sFzrhcP1fYUZqo2ZryQrit6qzmKl37vmXANUu9cCgaFBZ4UOZEfz8
XOUpK44tnqhe0xqfe+lWGjGrYjgUQFTY5ulndNWWHhHszGrKQ5oisLwSyuXLUDpsqNhDR4FF75Ht
MkkceUsVwNFmYQJsGcpspJSw6MuRfJzB6TghVYWM2x+8OYx9IGfexOby5TS4ET41amvN9rCJktuo
TiZzY14Zd2e09gnybliaU+VzsMYcQmyyUGxyeNueERxNlnm6RiubM05X3TUSBRmiOH59EniO0IQP
VuCpAwHngFMzFrnDGrXptJm+qWqMOuH4BqT1AZscn5C3dzlc6O+EleHaAh+09b2d7U6Xpzx1ori1
PV5YY18UTnqklG/8/mYr8rICRhYea0e7dKC3OWlETH+F8ZP6znUEHCdxnEMQhzUHC1LtfpggQDol
elMTdRr33p297XgZLX5H9YNVvAKVrOcxT1EYYraog3WobK7EVjw6pX7a94L9VJFIJj6M//BSsURo
mThrtDaY3S3v2cxkpiVHoM1AVj7A8HnyU59f2CO3MVtRoMQEtzysbln+oUUYmTpcNdkKTp1gpPJ8
3QUjm2D737GAphfz8gKAOoe1c5CeITDnr//0Ky11B88zfvKwDlOuXH6Ai0GlQ0emgW4y9iLRdNB9
7JogoF2g9mq3ypn56cyzzHP8LhcM2wot8URub/GSNaopWD+Iw1BNDYJwwWUeRLYGquyjxXf+Okb4
TD4sOZ5S4a4h5T4fyvBfHG6V+OBGjNZ9AJadwvVf2OTx69Ck/yd5MQzQrj4IdCxaSxGRZ64p00In
wFIy1tcY1T/0mcOtqYkEcnu//d47WzTOn4KrtzGs0DgDHyBCaSdQGqevV5qECLi2gp/8K1YK2LJm
MJ+uPHK6okIndCAPLxawZ7FoMRwUQlc5+mmyu/DTlP9sV/TQ0383N5+m5mgKmEMTa5fYuLBEmS6H
gNummbsAaRzlpGPUy6cWJc42Qr6trp9D4n27zLRzmG4EfTnmQUwOQuhxjjUAfDSv7UhTwJsdfcoX
Wo5OD0RgUSUEdCvs4U70EPm4Ewo9lJA25UWlQTOe6mpoRUOBR4t/98g09C1JxZN9GfygYvUBgUiM
e8bGwktEekA0vN1YO5LAkNdQeZuTJevoDxxKgzq6jjJXXeSpQellte9SoGUyXxmbjRhf8nt/YBzv
cZf34msnHJi762ew8dHb98oM7YScEx3qqznshIMJgHaA42S9b6h5dK8lBbkcP5nY7dwgREAtJ9wN
jS9aaWAFcqdj7rh7meGLmv8BGYy0dclQHni6LqSlWMliMT4Mc0+RDO9XtfOwvOYq1n7OICRO0UZE
rgvbRCt/mR2y+nD3hsbwrJ/YprIkkYLN89bp9jjVUr1IoVVK7QZobav0RWa710XjR1QE4+LUNc6U
4k1GsSijz066Io9mEz4wT3oYNKd4GBfAEBB8OySSw4YAKFsapv4OZLuDgdLbuPFoUn6+T2Z/AtuZ
LbjBy8vC4vC+o+d9XXTSyxAlNDWy8lgNVIVr+TGKP/T/n3QIbmJUo4rpLD6/a32fpzhfDNk7SNz9
wIJdSYKJnzCNP+o+wHR42rH8JF5l6degoqJJbaT1HM90jmZsWxDPdMA7SMzR7a883O9k9AKYR0VK
C89jfUpvvTrAySZPfAfDgOJcl5VeaA9XF3SGlswhx5HhWKiTsBwMJkJlkYQRVCa+OrgQyHUjkFeA
GqGCpAVAWL2n2OamvH3q3pwBJb31Q/dNCoz6zrOQXYIVrebGE6TKvDizprdYN/zY5EAN+/1PrAZ8
C1qIudPbAlTFsBlrppRggPVyZFO0bQtY1VLboe9WVi0Df0E9w5STnuMdFOgm0nkwOn61EZmNu5o/
3vZu62JmEn2gwF0GCToICzXhvt4czMUiDNhzkCPZsH//lECNQS4Bf1lH2EoFSHVk+AV/4XDos2QG
czKkdMiuTQOrBAQmtHpO2q1bbkylKL1AkG+dyuGlGw6VvjSuw1NaEc16055RnTWd/G2nZsq++PZ/
bpPcPkRbqTZzZbUdyd2V11PadghiFgaP8Fd2I+XqdAjuDXyVKr6CYVGt5k9Z3oA5erbhOZLxK/V7
xccU82XADkXC3jR25e8q6MzZiFzv4E2XCB0Tp13w3X09XmIRfKc4nIVKoDp7TZ9j15R8al/luRu4
gAnLKrg0LTPx0i1z9aFI1gTe3M/+tp3J/Pn4IoSivLaj3KgIj0J2hE0c5TwWnR8JMoHKuAbNFZMR
A4mQewyojLaYGRncWb7M5pqHmMRw0AWMfQrHE6nT29MlBam4/+Okv0b+gqDDLHTDzYBNTKLT1oPL
WDCVVuuWsjX/JNNMdX45s73zoB4o+A0iD1YAUY5o4OILyj0FEvgOf6X4I8a3vPZ7dQugPlvCV+Ux
OfFoYelmSbiIQnyc+xYMqHrDGv5fInAJJt2ydBWd9mCLNLvS7SL6MEqGSdAtWPJjXwqYxgsBq/iZ
HrgFfW3Sv8kE9Y1UJ1JlMn5/vf9nyIZN5MnfVoRf5PO0qva3bBpot9+dbwAkUD5k6cbYwbZ+li1k
luts6ig3gDs4A9N2d5G9YBMs7TxeHsxkTaStYhsiMtJmzKIxz7DoW7AJcDsqCbGyGWWd3ZaQpRgf
onPfKvKrG3YntagZWhvKNgYmvuo8N131QffTMPz+Cmqz+bXxtNeDzEm8MQVwhb2QHWsZdzEvSl8g
G8Ib/XZPqui/gJhyoZXrQb0sbidI0poQgyC2nEWVmUr/x2T67gXVmOjsoziDZJqI1lZ/g5k3QQ4x
UXqUPsCcCUsPsri4ZgVQLkgqfvQTHUUrRAXh3/To1TA2y5Zdk+S4jZsOjsddPaoyt//6W/JaO7lo
pDIFXhPQM4jwt7YLn/bb9KVyIbOnUhFfYh4u34IU9u6nfDfl0U1p9VjoG8sDfKqicIMqZDapbWDN
bdXXaUzxRi89IDvQc58llLvWQIsd4kSMeItmamYNw+NwGxzqbR5gfUPVpymNj3Op0GykcpUYqXHh
ovtenqKzi+yYxbMMKs/6dmmM+ntoMyo4xrlLDzTp/R0OFcReUeUbOHJgnEXT6/bRpSQwEF0OfPB2
LGcwSzNmUIwkdSsGYtUsBfysoEVXowBr6oW89aNJ0Zwyu+OTgi7L14waMODOS06qIHDk0aEdNLG0
xOlTtVdCb96K3VQHx3EalKhvG4Jj5o5oFn5u84cPeRusDr86TIIPot2Qn1XFIsKf6TCFb0x47Z9a
n7lH7VfHuseDCR+2Edgh3XU9nJTK4ZNA5EoSKG1QhkevsipfgPPEfkocrCHiFh762jnL994nZcNg
Sts/AgG6vfdbradGT6Rk7HbQATn6GvZi2utrhQwMnTkZe+iE5NKV6X859MqsCv2+59vTuubDOeEa
yi9Z97PAGvIo5Q2PiUQsszE5Qm1Wx08PySoBmQtEvU2+D34JfbXegL+9F/ZyTQl9XLQdz729qynD
I4Wn1IWw86C02Apz4I/YhroTldfCNE7FixXYjxtN8vvfvAPattecUeGtFKr0r27gE4/e4EBHHfeI
3liBhhEA3xZHJ1KrtEtdxl/Ms5TZ3NwC4A8/okTlehTbiIo3N/+VxsfJFULbu8n+sWWxc4IEuyO+
AFN38TbQR0cdDSzBmv2SozHhu7i/Vns6sg7hM7u4y2aHokzOTeT42O0ZZne8AF1ADfgIQW++5ghQ
MfWj995nGhVdfhyNk/SsOJ5ji918seseGWoWo3mGf77bgsX+Haf2UUBXFEYngepOU3+4oJWmYSO4
STRcvFZLXkdubf62GDhVIrHFXQbt56Bo8LS0pZPpsQhwGqvYL/h9Q44faIpzoy51fpJWCoA16DNE
egwh/iejgz4gP5k5uxIw8dDLcde1WVTOS3LbSx7yEZ5qEW/6NUYTi7noyvMGdekSQC8dg7eIFy+Z
feVQZRkzzt3KrI2/SvovNUzokuTZX/yklDtPVszNIirDjvk1eCSzN/2aY4jzK67gdEv5HYJB596T
FIZJLYFjs0UQuzfU51zQy8BdSBhEaIJUpA+GLihMzrBCAWyK0Ezoi/gqqmi8ksJ9i7/hU+hHZWcD
/hOJOL8xJ7/nwbZ3vnFYZVM3YyG28LpP3CazBeBpJyAi9cc0dUZ4Auk7+OKTFqb8cz9ip5uaP7iU
ZUim0ciGfc1n7m2DEZ9Ep+sW/lLsGEMU6jKPuXnb/zaBFoFmswWaW3HI3kFosoZ5xRTqNLB6Disc
pjknP8ftOsr0gPy34Nqz/DNTaCWbNa3U6S/PaM2NQyKS8wuTogpVYwJ5wv8Yfm0p21cIvIBBgq6u
khBP5zmov3oPrmXzhJTPAzP0XQMPcj4/iicY0YVjVfRjrEKw2iWMThplKgWpKz2B0ZXlWRt7SFXx
gUba2+GUL5SHtJOcWrfrHVV2hY2K+DpRASRRL5P24GvCLxQcL6UuUuvHiPfcuRJWsxR+GmveKZPT
zXAHqh8YwXT3XAnBppmxaoRszL+L1nVCKogYOsf8gSbW0BIEEF2f3HuLmmBuXcaDK8LHlxZhEwwf
MWSRF2stdREVjzm/VoF+HZzRGXO9tKy6f+rvGN+r7e3RnY6weG4wvFSTdjXW7u9lQ9PJyQ7jGUrz
LyaJw4bsQULn5ZK1Q7MvJb3bdY6SPRPs+0Pe2bXNEJ0RvyOApkXT3HNe4apyr6jqHeB/KdRQRPuk
mbT7ujNQ6NLFRSPxTtL8Fdta6TQBd+lvozjmK5zPRG0khRNDvH/7dPuIffjbNxwgq2deUmOe2hv6
GE2BF7WDwjRHhor8v1LbCnheZyDoEi//GsYz3tjFagloBngMxIdTG4Vp7Vvv5hrKrG8PmU8gbr//
WPi/t9m42R2ssd3YYf67z2ahb+G4VoXoBE8k/NYksADZVQVYLezWzMHa64FRmzZ3fnJVI1TduCWs
/wx/WC0eLJXaJj1h7OClmKSPeXt3Om9Wcoj3p+dASUAWsUub/ZA0NO4PKEiuB1MOGuA/cn69Bc0B
whZ5h5GdGFoBS0OatKXii3vhn3vYepEpznvaULwfKf0r0lEQeJ3ndJG8VDbhUJdRIvUN/4w2KOU7
b6Xs4tJP7B4BGn5TpG4yAoB6z7fjRiNXGFyRl6gL1RZFoPauOU+VZI65kDiVjMyeVybkeA5OgXNl
9uq/LomIqO8+7hpa0XekN1+6x0uKMh70Gb2heFgcfn9YaI0fec0cr88V1KkR03wMqXnpJkXnukTq
SVHkDya25+kVjHJdwVWDAHnb0zY1cIndZ8J5u4KmMxnIUJjYv2Rmc7ZuNHIBG8rffm/Fg2V/jZUb
8zKp5WK/ssIPgXyrXVndNE78hxFPCCSwVyGY7xfh5GyNyZc9/qiy03p+mslSKYNcZ9VXPhSyd/oH
kNRrYHuCJGQD5gMVP8fyLRbSpjP+O08jtuvkoGFEW4yRuvRzZxvmrbN7WhfgACPUuYKYPz3qsBmv
XgkknpllkOZAO1GvBsOVynuuE4Tc3eQosDJGhr0No/DjZnE2hOsEZBPlARja6kOoMuylcL9ZO5wX
kbce1Fgwr1F6woSff3cPoR6nb8NcS8I2K5TQATg5kTFS6DFMbhg6F9glPs6a0QFH5lsll8u7yVcM
3eOPMcqD8C/A34fU006cGOKFX5d/DxBM5qNR5gbPCLDAKDdkviOmr25a8r/FXF7AR3YP8+nTuVkg
K8I5tcKWEypCvbCbjgipQquiBTUxyvbO6zitkjjAVUEgWjO1ZonsF0UqI4nlP7fujkr34qDfyw/B
X1g33ULOxKMcVS/HjzpDy4FQfF8B3xSwOljWG23K7wK33XgJw0Zqpu3S0zAWHGOvnAA5F3lDRh9E
M7Ox5B4yG0pc3L9dKsBif6Xz7kcpSxxjRvf6Fg1pOxWAl0Sl98DNjOduT/JERbimRqrFsQRaKpn5
HDEOdhwrns8+DbCWQOwIoMPejItVaTeo2/1Keie8q4tgCqr6OvU6eRto510JyoYG5DHCb39gjLmS
R2C4L5FpAqUK5Mt2zMja9rgFafsPXsXcmM6lX7dGASXBbnfQc3oJ0MetyEC1rFL974PhmGUXnirT
fys/xWfYdV0hQaZKU1khfV4CXYRKCyIKISwCfjaDDROi/LbUeiX8ralOr9ksQ5LI6NBN0dIjh14+
Bz8cHDBHvN2nY//afG6LGSq52+R9zY74g1Q19Onn99+l7l/apHf9RWPa30qiGkg8D1olTnseEJnJ
LRtG3xoNzfVTCvlwrsohqogHn3q0svjCQ2mQwKMqzUJUlYJWhT5BLU/BwTDTCf2rJFCxwba1kQ/q
W4JRB4L52QibFI+Qdvg14JmYm4jvMh6pZvLR8tvmiBSPw/oblFw1FH9cLQbDCGX6lK5MKVnrP3Ca
JXk9EwWRtNCzx9qFKZ3SLKh5fNkI5lwRB3onXFshO0L3HjhpjvDzIaHOz5Kfv2mbozwGX9R2GYBg
S9IE8JyICPOKsOqtEPCs+pVTjJv2/c30vAduSfV/1wkf7z8NeBB07L9KclZd/De3bBjbJHOmE4Ij
yuti2bYm1/iPdOrI1vdQRRdyPVQ6qPZ2O9IBc7WnbaKfom/k907eBB24pnOgJyU0rWrWw3tq0Jc+
r+L7Jq194rJ+lFBmzfX/cIVsZkRagFJ71mVoH7iwj/MO0gMi8VtwmDf+ESVdoWaTCgVh8Aw=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_13_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_13_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_13_1 : entity is "design_3_c_shift_ram_10_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_13_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_13_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_13_1;

architecture STRUCTURE of design_4_c_shift_ram_13_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_4_c_shift_ram_13_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
