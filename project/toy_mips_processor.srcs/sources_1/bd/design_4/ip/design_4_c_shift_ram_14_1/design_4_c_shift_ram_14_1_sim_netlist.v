// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:58:25 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_14_1 -prefix
//               design_4_c_shift_ram_14_1_ design_3_c_shift_ram_10_1_sim_netlist.v
// Design      : design_3_c_shift_ram_10_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_10_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_14_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_14_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_14_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_14_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dIRfjKAd0WOgtBjW6NJ2+9xRbBjkVJDUb4lWpH0xGwx/4kUhlB7NdVwvK5y7WV8kO76u76I8RYaI
8osDqqJQ/vp8kdGeIDh4arsFRMBb9QHcCKTVB24hlgkGzHmUMxqhmSP/K4DvZ43w1kEQnS2k/Nrk
0xkXaOZkuPfnHT+zbMOH2Nct7D9ghxHe5L5BNQeRlwHmAnqnCcuAupMEbQXUBYJ6/kVMlB+1iH+2
QuQ8/JPsanF+BOsR6NIwneR7bCHNWUL4GxZ9+3RMlUmFjf+G3nxvA5CdIhWWA3rELgK5B0nETkz0
iPg/KeZIaMXV4qr8NVnRCyujLJ6E0zlsSglDcw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LxxaaD++L2wp8VeGD2Ei29vSsFdo1FUUvrUXhWQFwjZGerCrrvd07fw0JHLB7qXBUiXc4uxq9mkS
ecJvCbf6U6YLOVBIBZjtNNY7vMbv+UHM65Z26ILRVuQ3FwR+vwgJuZdrBnuaTD9nw0kMrwevBZs5
/rDwsKdOTg31JpyDUrMX7wTCsaDm5e1WvKwmSCgZDEPzDy8MuBiZOUADzp/fRRCCFOUMh0gJiXBk
OGxZdjz5s/3DthUd7urXKp9RfgbYRePBg4K89QyFWpmRJs4DreWVxhASfr/a5A9aO1gjaectY9q+
zcJzxBmC3OKREcVCi6BMUHAnLrubJARKhqKqlQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9536)
`pragma protect data_block
L1+tCsgxkusJ8CUZAtXzRGFp5qURZapR8CT1JmSmT6G9bHMCk6CSL44m2ODn72qix/6S1nVPRCx9
rS8AlF+k1NfZJRSaJpEh1kFwlMNlZNJMFdPfKqWOEcdGoHvfSaQhJ7XuSctJvS68lkHPE8wQBsF2
OFFRO14jXTptD5cAJHtYpK/0mFwUflT4wzyFaF8Fr8ZDXDy23SpBEjqg9z6kaFBPHmnpZwUh1rZo
6rmqGl4H/+gaq42ROhi7DTUaiiA3gbePn9HC6bqUEnJfAhFIAjjcDsiI3idvINiYIT90jWBmG3FN
k2EtmOCTjauxBNlYltof76vDXHucVUyiC9qKol5FpLQo5gXg5B3TM9gmlZpygT6zgrfgyqoeoYTd
eMkia1YE613wQr5Gpc/+XhQUWwDn1ium7wa6oV95SMVKMdZjYB/w1RgUhRmNREKdZgt3MmBqCg9f
iqRDs7e7QSZs2T+5qkVhJ5HQUiVKXPvZOwkF1ZV/I2wm+C01kfUCm5YmWlXxwstoEPJ219kolQeW
Gjz/cjjsVCuJ8ILjVMdcWHd5NdLizjMOXBtdC9Totjaa5VLwCQdwSdWlnwmCOG0dbScwdHHv9eUR
b3QLie9FDXCDNP8AsMi8yLGJyPKEaeE4ODUFVAop/bJ259irLbNYtMMBjafztTGHlkJ3hdjho3tx
dytvsGH6W9GLGhlApo7tjb8ETG2dPlXf+gJSVn8pdkY5lcBOhfvPvBf9xj8PgbePwggME4fEtn1l
BYgnMcv14QexIViLYCgomyCuaxEBZptuAfTaKbCzeNXYML0BX+S/JuADPWV1G9pfk1EcH8ypWjjg
PTggher+rg0xL5Ox+cAgtMaJlYjNSsJFDTBPnhp90eLAWI6yS45mOlBRUquyXDSjbqiKgg83tNQm
h4mFeM9KkHfPIKL/mJUpVkWGMPHdS6vb951FcFw1m/d8J5ocO2ZXEIDZDC3wDdBfxwALB9c1Vp2N
eqohjw6lha8FpKHNctp79hQRmtr+oebZa+DUFGaPtxxu9XPvw0AHxqbgTIaZPql1sCg03YGvzr2+
dFlaC0q5LvEYxsKRyIRQjU64KtdQJCabKFZzBiwhRWbbldP7SSxOUyhLGpuGmxvM7/r6p17cfItu
P3EosDsfB+eUfTeL8k0ghLw/9pChhu4RNBB9HzhbkXnZukGEcLSJChhZiaHRZnwuw86L2gpa4dR8
GHWyUykY/K0wzvtbU2Dese8mRI1n82rMNbMP5gjCX2Moyblw4cZXA4Y4eBrBfr2zuMkBpg4JopCK
hEQgYIJd5xhZArbzWkluYyakGlCaMZTBfSoGTrE463GF6vEDmPqleNFGv1hiyrTDck/qg3SN3TKD
2y75/8v5/oQEflKPPkeBCcYEUQ/p8pULVCEUACkS3Ec2CncFZCanzGjl7U12Vnro9owjJvzzsoeH
FPZ+SCexnTF9iXK4GxgRGFXvZWB4kQS6azI4m2xN01HqxP8P4PM3Q4+g4zuAXiqgrJtx1KO+DCmK
zKEq+EoparjclFPTPeDklhqMxDR/YxFviR0U6ZTDTDFptV1UX824FvAAxFddz7WfVxVwctGIbGr3
wtGGW4o6Bad/0OQEbx2JjZQSNBUbuNKhtoSrkJQwu74AXyjbsB/cBFpQiFWuf+WSFMr7RQNh781c
S+wYMmkloSid97a7R7TOZZz0JHw0X6y6BuBsIZh67kMtw8Unl2GEvTrfA+bL9q2/IlBxpsJzmrD9
J4f3krtSlwehUI3btjXpSk4tj4QBTA/9T9cWsFApFxljb1MeZVeWSrhivd5ythik2A09ev5M1ARq
3/kTn9WM5wtvhvUWnrnMBQJugr0YwqUgz0BQUhuh5+l7L8Pic5i5E/d3KRuxUiDGDH2nMS+snQgA
XXS/ysQJM/mLw0WuVVOBhC6oN0fcJoA73PN8ETIPgVOTgeCccx2Y8gjKSJZAh9Z/jLyAb/szAqGt
ob83kJUrTV7JVpmOTJbwR15Wj6p50iXFm1Uwnh5vZi5Qsi+Ab6bmX02/kmvhRwkYHVJOntIaWUUt
dgJ5f3FZBdgKbUxFjtWy2qlafGfz2Hipe0ONu81+qfSOQDKDMQn9aC5WLY4MZnGvyiYamv1dxkun
oY0jTMWn3VIVSkhssOGRmnM+ZIrmKpH1cbZllkrVgyIX49O1QjruVi1S1jwYc6PBtSLiBFi7w0e1
T/gVdRlvepaQEfxZjjHDTe3xGzJehPqk7rwvzJIm/SUFArPZ9QCeRlqZolqOpx/0epm4wukcfnO4
QnRKr57vSmdVCSckt6PleO56lyfY74eL2IJayRGIclWIjphWzAgSej2UvjBehi6E4WRzXGP3Sy6w
LFvGG8ZTjnZVaoit9QzRoGwzNMHiP0pSjicJmYzGnXQhbZ5cuu9e/OkIwZ0cjIAtZWn0NUflU6F9
wxPGxNsW37DbqqGl1oN666lU/0WesoECPblc8ZyAeOBFWFvCHOv3lILGY08o6zzcGjWXZffMUAiT
fb3DPgM0X2Q+bgdWu+UebqryiSMU906TJYKu57rCUf21/pXM3PMbCInYo4u+PA+RF+oGRUvW2c+V
xZIgStxXCvqxR/Lxm1u1+x8Gmxo7ksg8AmLpewkCiKr6Wgxv8U32AbLvfsDXQcRoy5U2MU0aVyTQ
Ar+kf4b74DxVUHXyQ6yzyEsa3xqetxz17LCfcyLDjtHVbX8mJcjGSaJUpm8KlvVF7H+PbzE5cvVw
0wHkHC2tzbFdv+gu6k8Ew+hbtgjH7GvBvRyozfvC7/enJFmOpB8LrStWEGS9Nz+Nt9crEnJcZnoa
nOtXEGLHrkHuhk7bIaRA24O4zfEzBGVizWObYrrZdG4hYAmT2i3DuFGoxAgNzHltrNDkrsYrG1fm
ubCl1wzNTsrOo3eK5A/bbBzD1AeZQHoudKleSKAGwags1064fviCMgfZwNLYTcptdpdP3ysoxyXe
12mhf2dgIO/R2FxWx7jeS0F8tBbseBl8YBSOK3wo6wuy4UPAJ3wyK/mBZLdS3AcazoHc7URvvMma
nqL9BmecvS2xkpJO+GmPh2iGRvVrPKoOAjgB0jdQx5HZRo6AHdV5lse74ZLt/eLEz9oZl/RjSNpQ
E25+hw+Cp80isTztNsX8hoX4bV8fl2YmyEtCOSWEdUdNKtQ+2l6BbFWvnS9eggjV3llTy3ovlwgD
349IXKHE2I6zF6PMfMxTMkKsvuNUXf2OWLPuA5iw0mVpriG68fS4na0PA7KKuC58qBSY+ApuCITs
InOS1Y/0ChYM0PPalsWEXJPUA8iS01xGbJMBMmOVUAaIg+h4NktvyMSTLgx10biLa0Q1kK3/s2/8
909fE5CWZz92Q4XS2cob/zOS8s8p8kYUtkCX/G2zGwQI56OZU5W0kJh5yZpIvTVj4m75aIeUHwPM
bKOfjPbcYAscAgYbRlBGTodOYDDJp1Z5L//pCWbsiJbR23Pogkofq34kl3UoKiGFoHly/a744XbY
4YcqaHqFwuLXtdVP7v7nxR6nvXARrqWDDsaAVUhh6qOjCSI4c4rx+A0w6HdTPrQSeRbHtFR5XVtU
Up2t7+HShD9iYrKuqlLnGJAmpydm5A0lu4KCQFiEyPHEVLuYYfUYkebFohbKfrVnFLPxTdiuBZMT
ik5w8iH5sC4GQjSrVzWwDZuZwEvuhKzlU3XNwdrL1myEIu15x2qDnjQZSxlezU/AY3UDdwLwk0Mt
wPjpaa7jCw1MzUSG75cyRHsIcpSoG+9P9qVxH5V7RA26v3JlwruIzEVsJgMgCb61zJVQ1cPl972z
zJbYtuud7Orc0lCUgHjARIqLjyoasQjKIkFBd9IM5PSEt5AIH+qTcGvqDJxlPOqKtH2KJqqeN9Na
7Q0Xe0sRp8IpGLiwGjftoTdACIX7gTNgvD184USZlDaITnST0TQbEfmPjUqiJoXFaZTQVX4nwJOu
guQkDUX6DJQFyrC3RkDXeXI5RPT9+dbv3F6uh40xRltRMvhm5nWpZDdZoqg1KlEWQAjkrQfUlEhn
QX36wJPbukxHWs07BdWlZej2FA/InIEYFE+H4fMyFGLnEHGfdL8nhi8ECtZnV3SZXP9ybXSndNgK
+EHYenV4Il4SB3cw2tsgg9tThmuuo6lxs8X31HSvLjrgY3aw072Z2wTiLGRHbVmrUCVE3oJes8Tj
VR9pbV6ce0ZmXiswpBAm0pH1uUK3F6/SrUQDPGeskgVFd9Ky15Xw+NCB+PRzU+iv5Zq6vdti7WSL
ZZNduHVKKUPeC1Ga5WmhHXk5IEuXeBEqDsclsDzsALc9BnfghJ9bcMxZjU0XEvCYBZ/R2f8xL1J+
ENFQuy0d2KxsDxZgAft9VJ+knuWcDCJRV8vJY4yd65YmHUAXY6cTgm0izjY917A3zurz4aMQTEVs
q5HuvVssKSX+xDJIgrIppXE99gIuBrAl/inRx7Mxl+VGcEQ72QXPIqrwhizNHLOgyLj3xFky13K7
UUuw05KO7Fm5bpdgcuSxPbsamUrPkChGiNxnkPHS0l4F6aaA+uYRbjsYlhiFNZ6DBTN6Rrr//cYS
h+f4ecX//fXbpykru0thW/3NqlIFIPDQnUhYDN9aRM9PP9vcLkidsYIXN/7GM1Jx9NNhn4Md0Q53
GVYlepCma5nQ0QsyE1iftDhDdFjbV2LUFh3ZJ7g/C20NGOuHtJNLxlOK/ViK4GxgMsM+UceOJgPB
zVHMPwNjBFJCaU1pOBSmqAvhnhzjflyaVzWsIkrdTwj+E5AdMXRUgzym36GMZM/lmyd+4EWHDYit
QxOvaIjshJovzHxLRwXWoW+q87TNqFY00mY4b6ubvjTGWAQcpkIBYhz9+3u2AUm6ZpRa21fRZsck
Rx1wdd/NI28XrTCo9omUMYBzBoo+fOkfrD1syGCTMg5arWg1nU5VMWF/Dme6JRZDF8U1+uKk7H5r
pNfJOV5cXo0UyywREmxJfbbjj9dVIMGwc/ugFHgLLGBvdeM1QCeMj2Jqr3iYG2CTnL6vUDQDfWbT
VKK4DTX/6OD2azrbhOqUHpD9BwnPG+OcFQ9hLaUeQd42d6B+QR7iiVrSyav4zJPWiiiXa1wMdK4r
y68LF1Xhgdl4gKj0+f5vEfxKXFjlmGHnquFFvisY03ieFTN2zg7MP3G3jv7NWuMOVSu82Aya3SI+
Zl5xzZcEFYb05Gknkh6lzcMft6RCsZnsGMmyDw2H+Li7xlsfM3HIu1/AnxwD3mrty3jwEbAc074t
potXO5jfS4KBo9djC0kAGnEcN1Aamt1FKKpblNg6TjqXwYb/j/tqAHW5CRkA0r04dIRMhidmDYcm
cKXM5N/8AKvPhaT7rxD6tZa/WKXv+bti4OjFmOAazDGFaV5yZLa7XvvmoXNHBojZC4wW5IhQSRzf
b3YBbw9AZF4F7g4ctmQjH+w7phhHplkIiPVhUUz88G5U09Azyi8yPmwDuNWgyJcwYMnqtMjTDho1
sRBE4uEvA8vOXqTA8c8A4WC/MAhf9TLA3Pp9U98Nl7k1ieQsXJ/StKc8G9zTShZScf5KZ9qcPycM
baCeGISP7KfPbkRiEU0fxaPg1tPz7yQRpj6vO+pYDvggu7ymDJXxUTxtooKwbkvVinaSpCj4KEk/
ch3Ia6j8m4uhu1hjZX/uayIUsdokr+8ZPjMqtwU9XB+UQtA40+p3+hBn5PScpPHYweJ7GTH3YxkV
JELbtNSOMIgfjXria6xGiDGEb/6tH4n1x9bL78+02fhm6jx1WFFn7rFo0UacpR0CDasvqf2zfqwx
6ugaqzpGKiV6pu7QSPsUaDzqJyAE/AI1wcEsIwimMFnb90P8/QcZTAuDkGbiuNF/+qdgKpe4GhT4
J3HCfJLJseW8ohs/3F5nrUYwwCs5igiF/u2IS3421J5WvQblkDl19wfZCtZDDVK7e3A26LwTPsuu
b2SekbOrfxdjGn/XAajztUV7XEYEuODBnYNvNKPu+teyZ7I8anb9uQADCfL1dDmchcRRQr3F2AVf
4tlWnqIg1q2KrEcya/Bel+6j023sn85pALQwEihyJ5Btr06EOIIGDDuG3bvt6qNyIPp0boIjhpa3
pp2y2fUwtXBBgWGol7XSDDmd89qq1wQy96ZvmUTyUEeWRNIbf+EREzWlEbr0nQ3K/Uz40XaEnmTA
y53nBccQHcRFf+qFt9rbghIfV3XIcXFdMWjcN8VKhOBFTuzFNmYZi4Vc/gS/LY60ugGfr8SvMdse
wtGBR/7WC9wbr/bONjNXr744at042LnqR2/LbDtsf+h9SibfK1n4iwmdUdUBPrILnq/uQhBZJ61R
W53QFJ4Z5ePkw+oGvby/1CfJEtOhqumDhPhTBeuan3tldpWOSCKQD/K/mgi/sRSHCiJB7q7yr7Tx
AqVy1kYCZNerVZ1DE/DAGrmFfz0OBoiDxXQllWX5UbL7zy9TOcfrej3doGYgQ2cQo6S+N6zyxxOa
eEQ9WoAbRlufwUD9Im7zfymqjhco31GRoFrvlt40vvVr64ZQut+2EH4IyLNdHeyS3A900X3wGU4O
/VQyLiP6aDk+1AlP4C0WpgCiWAhcOiCZvv4UN1NPPx2w0nz6iWu1lqQupGBUn4dgj11Bg5ls0it7
F6S2wA7dXSygWKpfRk6e5GWwzR2DwkbdGftLHdzyDKhptw+KIL4fm4z8G+IiTVOHySLNl7WD/xaE
nLPlP9nO/CK6PtW5LJOtUDc8wVmi8yfkCUrdq+uhmsfhM+qvC/OWuIOqlRa2fei64mNopokZP4M3
kB/XFWaPhvCxjDDmmb2dCWLl1hdTvGjI5jZVWg1WsMOuxbWiPJ1RzD5Y94jShi+sjrO9e8ueMJ4O
AXi6xhf1ywbBqly9OqYZTU2LYIRbr+hKjVzY433lPEWMSnnksBaxOfHPn0ENbEjYzmSquzN1Pusq
jrT3yAJqFkoU4vCprqMCJP7PcgSSNNaDvWfcAhCTytaoq5KIz/SZJ5kLlVSuCmFKgvjDHyKRfGmx
bq7ZD60prK96zIrDvLU9cIL7FaaHk8/DfREX/BivdN8d1PcEZ7x014EFI6UhDHSCu7RcFYos3bxa
kEMlWMET1LPWKSEQpz8epOirUVdA0EI6OcHu3zk/4jSCNGeP8QQgrjSAJznlFXPSggw1zTiV+7vB
LvhcWgGAr9KU7VtvFt/PNqJbFpizbIn9MwcMAjUBhp56wFtyiO+6BWrjmyV2pCpWmlM/VJ38Be7P
YVJmRgT3X0CLQtXm7SjsLkw1cktKll5TDE+FwC30x/YWiHpqes9bBJMWqHauh58obk15ALCkRWWG
qxoPnVrbH+dOcyKN6m8BeJex48mAnke5W0VXRi3MjC5IExB3fu53Z6moKzdqH6Glazl2HnI0KPwS
geWplwVopkn47/tnZB67gny0ty79CMT2HioxPpvCe60v+nPbLvha2w4fZXr4ZTpAFS5boinkGonq
+IWu2D8kvlolBOvmdK8n6qtJRkDjwtAb9tuknOIR3+jT9BjZdM73Um3qwgxDwLJCqQA4Mt/+p9mR
83W1FfXW6vxXwEyRg2OvsMec8B6cXf+ISkvUe5keNRfdmHONgKXtrs/l3yDJYN5AWrH+gUdiIh3m
bO8XSoC/hGA7r6Kkhu+bs5AV5dAZgo6pScSZ4vKAU32iUhz189djaPI05dug2LdVvHTHLpi/iqrA
4HyPqEbWyXs1PYG13XsF49DDAq3E0b2QkWXNWb9NOQA3bY4VIO5CVNQsjfi/rQG5n8cHhiHo5b7s
A1ZKGRbbi3ZpzCaRd7gO4QaQpOGgiuLddbS/MIYNeiH4Z10mU+hqyFR8wN1faLiNf5Xuy/8aWvPu
iY7wzblOHs5VhZtF3v+s32ewvqdEmD/Hq4GaidWQQvP+Mg5DQFh0Zuz4y60Fwjo7H606lRdJUPpe
7jNSlTnO2mn4nUg4MXNIeLfH3X3jMBtlNb09Q1BfVVPJk8XDZYbxBLU+mcC5FkTKs9k+dsc/M4rr
f9o/17RHmjYUqgUxOFMsOnAviC/waOK7iIxrH47b2ZmaxENYBN56igNA9yRt/T9avyd/g/P7WP9c
zVx0xcT5X6VJT9fAUMIjRP7TA2UnG9UgG//ktFWKhBo4SeFRPwixLCE3RXFfOY2kWSQEHytka0e2
C+p/Nf9al3akRtPao5cdwQB6vI6MSgIILUXSyaedhGp55KKSn5vTfT1HV8Mkk8UwkzssaZQ2CWbw
yRNTsSe7erUS+Vqy1x5FBR9dJDb33nrnBvICM6tS8YtSt7vswNVd+grhLb+enp4LucSlFxnwNye/
mQ2EWLueYyL+Y3BMV68mW/EAcRnjvIZq8HwvpchbUXwCz2EWLxjDtlKw0UTLasiJiJL3O2xo2nJd
283Z8I1hlzd9YJMfDnKJCpvOvQGUyYD97klkICXbzeFKh6SCxrk85eonxSof/iJ7xZfu4VFfCQD1
eY5fwXZlsFcCIh9863nAmj3kvmmD66I0kBM/RxZTWRDzSLqUBO0dWt1YBEXWFR2okOjLrPp3PN52
TyncpMd8Gk2NFsZkGs8bz3IphDaW5DPjwY3kvVgUCdrxO1Ec7pmT7bUF8uSTIRX/eF8d/kQd3E57
wDTGVQhCYRDzPyTXum3In+b92JmbiBEyCi/ojlq+JhVj1J8EkDK94YEY+IoQAueRdd/39mdazEbn
nN7lcVQDhWb3HIVr8NSWohO2oqH+tSlu2DWQNgUHu1SD8Km0gMZVHiszkYQpPjDAKeWSOY2ioutB
Lk4l9HCAqQ2OaN4El3hXNOoB2tbcCRTFGBUiZEUQXI+5zujqZEmHQIGpfVwmIt8y/6LQit0+sFOe
SHSAaEqdLOtIkXuiIutSFcL3A/ByLd5Z19pb9TRgjF+g9Jb82q+I66NDzhvGgIy1YJSNZgMo5q7l
9psJ6+eDb+yD6VWxbm5k2xUxREqphpdMjg9dyhsVOIbSixd55ptRCIj5jYhpuraWYwYZRNVVeW6k
LqETq253xSllvTy9Kzkhla1RcoBS74oUZATNbzs6sC22M7q1UYWY4RJ3/OgFFPHvecEAVpkKboRe
Bm/vlr7CwZTcUt77TMDFHQwj/PMq81fdLYCyVp07EojzyZr8U/4qFhelP4zDbKOFuf7Yqde/8nPE
o3qEmLuniovMeFtAIbFz7wUlVAFUPQVl6icBZeWw+yoouMV1uNZI/lohD2YT0pty7imwcMcHP45n
zcvcPfjrzTqfX5tgrh91T9ymSR+qjNWRsDsFYAwaGi86YNmiHA+LuV2KChN0EBcZMqSI3dg23/1X
RE9HOpJvoHDYSjybNWLJ/vUe6iJgqqWlmdUuJ3fWOZy4C6WCPNROnmyd8AsKWyH1DJTpRJwSrPGc
pbfQzEGcBRTCltlYvqpNr7T4/mpzTifeRb93Hbr7DIX9qn00gzrSvUr6l32fbr2+41dUpfhLOulQ
6SKhD3G+G5To6LzDkDohkqZGowru3i7eTHFRNWXgbJaVpC1djkGDvnkJwuPKdTA+U9SuYwyVHGu7
urc3wYSjeLwj9JN/Is5x6TndqIK2cRypXiKdQVaWXhQMJzYg9d4n2bgcTslaX36Czm06WoxUXNzl
6NABifou/WhSdrsHdILNafE9yv6bRG97mjr54KipkfVWI/+7b9Vno10TeFwBdKdSh/G9cCyE1NCa
343RER6Xn4lnMPik+rGRo5Ma1/3rEQEa4h7rtKAd/oHpOgkMNZ1rTuEDjEMwrsIKFbcXxByN3lIv
/xACfq/mmT5TlmFKnNogrkBrDKdu7OrrgzYcakig5qOGsP1FsnwIuk6HCgNwEIJgYhb/Gp8j4Wo9
qgWfpY9dJhFR9kjAjs1egKYc23CVNpZdydwyYeWtnnrMiRtN+URXAcywG2Im5IJyQz9CVdghVWcO
4pQR58UgBFoTZMc74PIZVJSilmo4VVslZJm6wVFGW1MAlb0Z9H/5z8wnFf/yDtk43ZNAVcOX3BSD
t5rTknhI0cM7+qToxee0W1C/TTV9oK82k+TReQxC25+g8v7pfRXPW8IYJtfggyQY8NkNI3MKpfXi
WXqpNBeubXZ2AZEUh84Odxx7+fV7Q4A+qMfe4irfSPWaX6pU6DhLg8NvS84BV9GUu8kYbo3YNwvi
DN+PGM+ufnlt9W536heknJ9ZflCfvY1EEiHYSWfiINWrE3Xukh4KhN58mn1Oc5M4cy/77E7mFobj
62AIL8IYGnq9iEYA39Z5hCa2knulK7CzBlZsJBuCtmrzMPMvEthusUNa4hHbjmfisLZYRFY+C1kp
Ao0FLz/En85w3CE8d0hIQE16xh3fzgFn5W8tC/FLbGF0UmjWpJU6gTj7lLPMeUNQODBYpY/FWb0N
e3/uGahFRBGW9fiEvs+DIToZxAKAIEKAWQyAbpJv2SSFj96LvZwT+AKDfWRmW7VDousPIqrqIdnn
uBIaaS903b1odJQ+N9EGUZ2nXP6DgCKwhUhKWu+tG2kUvTOZmAdW1rM1Nj6UXkqpjr6UXqqqUeME
n+LUmbZoVzaioHEt/wrioBX8d7lepYs0ut+bvK2frh4BRMrQv80XT2LD0BSxwjNgfXhXBI0C0fy/
Ug3E1Yx2XgmF+xW8c/MHhSMD0HhubGxBM6d1x/MGa64qYQbEHzqI9DUIB42ksU76IqGxqqJb6XzN
k57JeqJeNsdllf4mPou7RkcpdWoCQsQccz0O5RrBjf3j/CbokedQl2bdFjHgDU4Y16YjRMxwQa5x
m5w1sEZ23pYL+ctutOwglu1ojEYZbGCkS6rpbcJGANPbR9OtyTiBg+C/EftE8eu3QzIWZ6+dinPB
Pn5lLxr0wESA4FCLC/zjiC5poiK2pZ/Fa1IDmSzKAVUXK/2liRSRGeiYrD89sPAeCuxONiPybj33
/9toC8Le3G9KmTUwuhmVMd+ytcjGXoQhX3wxWgK/kQsTHOOxGoA80f+pJJ/KSOnN+T7V5WQhC/Ov
x3t3Od4297syF3PXEU7iwcJzTJSHuHOjUt2nt7KZgL9iyqbsuKUP1AqYiDFvsSztHvFSl4TcGSRV
FHIS9o1XEzCTx+IQkNMWPuOcM+NpOlOCfxKLm02gxSfHWMOJzJjiN0uvMaxHAv4WSsXKz4PkjStz
bP1Ur0I2xie36h1qNWqqc0exvEbxJdEg0RPmPOGrtJBxs2sd60sJO690JnRzFw+bJ9yTeEGB3DpP
mVPtmg/BBBPkG5NCrronUz9gqk92EPjQzBW76eTNylawbxNoHQr3bXj9g1jed1EBDTOcH4ZpeXGv
ZTtirLcGrKt4XY6emu4IECDnTi33+aS1eGS4DFXib7Dnp4/z9AZsrZ3NoZ4yRzZKt5axIxD3BTDz
3JZSCqnOdPKFqYgfb6AsEmBNfUbYGFEBGlhYxMvIosKKEpJovXNwWGKOl38NAyemGMI7mKPYGnPv
Ebe4j6EkpQk/mcxEZrMp9rxhAzL8Wkp9Hw3Hs5oJ5RvN1msJ8/8aIemm4ni/Ea43utnLwCGnFLNX
4Szt0p7aSt8JTDvSPdRUKzTHp1cm9JUEkLdsKd+zaeUZYIps9CF3S/9eVPSlBGkQBTIGX2Ax9Nx/
mquaD9ssjCogShTWyaftVtIwQwlRHS61k7fRoZ/+sWG90KAgobOWDSHHuRRt0GvqdqczI/NDGbKP
zwYB1zqVrhPNgkoFnNClg+8n3q8zUvHu3XKhLlUngS/guNO+Roi0O81/OU1UuplvAbKKlxcDXOm+
n3bjIhEVTRdDIqilBgKCbI+eocU2iCh/8oB653LR2xIMUQ3Jis/qYCKq8qJTv+zUMemVywTU5Rrj
doRSdlpWjAaEOxgJKd9v7TNIIyEGAXPxp1i2vJINQehvwU5du86WjHT/nC2cNH9kanH1wbUbCLal
vJeOc5lXBhmlGWDcuhxPhuA6U3A8kdYYNyTnQ7LGf44pR+XOxqozDuBco1+KE4ww6yMSv5GLyf4E
0kvivW1XaVh27Ubjnt87FvWpUc9eCCQrwEF3RKGdELWU9DGxMxXVGfVKrdLjkcx59RP3DuHHCC58
7BNqTkB/3jQpSD4Hlg1tmBbgFOW0uId3sA53e3AcMum5KbsNkRsP5scY+ZyHiYbi0GL1M5PQbBl3
hxMFfg+7mJ6h+6pKHj3/BZYHuEPVo99hBI950xlYdaVUUYvq3guOo841pCOyy/mq2ZCk7Li3Ko3V
M6oITNQngz1vPVPFlod/QmurXYI2YOb3IvbgaKBGxx0a7rdRYmsESDsjfxaxS+gFDiC8f7ImZQOU
sX7d5I4uIcv5ucFDd1xSX8g1nJAUJJN/9DIOS/S66iSX61wk6eHSIm/m/d8U8iOEec2jFRwg1uec
O5kyOGtpyUnbvfqhjxvJx71nMvh3Fh44w9Kszmvfq9XLsd292jleIlm8wm5Mqr9Q5loMfLwUGWYR
mudgZSxHbu9pdGF+EzjScyihkAbR1RuJ2RDuxei4lXmr1ZvkSMwoJsePRqKYZ59p7077cd5eGkd8
VJUgHYHdmzQ6ybQLyAJcEo+3ApDjHScvmB4Itb7PZ1lNrkk9zeNQfvyzO6vfVQMUzOdyxKkf89Vb
VuJku6c6BW0d3P0QyWiUPROzBEEo17nkgQjAvmM9sdqEvQYNoxB51IVmrJbDWQyHIqTFNt16WNMy
N9JYNTp7r/O3jEMRwoOUxKgkCeHe9vq1be9XYhxQEhqH2rmC88A1hvBxtp4RTd8STwPoIDKrXWbs
vHJpfJdWof7Nag2Wxjz/vag=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
