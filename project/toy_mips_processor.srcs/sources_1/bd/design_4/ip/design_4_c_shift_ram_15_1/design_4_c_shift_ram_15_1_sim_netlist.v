// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_15_1 -prefix
//               design_4_c_shift_ram_15_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_15_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
e013DL7eXADLm/5qIiN2ci+eAmLx5Twds7hOGjLWNlCslX4w5AQq+FkZsObHaqNe6IpawuTG+AFd
NQZoXy85sunwg2RVDueUjfZrr2dzN9kWXLJKaK3fk578tknb+5e6EgvLRrGnhBu7mGZ7YQFHjUS3
N/C4sZYlIKC9bBA3LdC94a3NH9uFy5kJBjtQ9HgOZq/Ngs3+o4rI4NslVaYgj2xdVowR8xUqFarn
vT6y8j5BnJ5EWLb3kYKCpnwUrGbVKlsAGWkYPREULfwcNScEFjG9oHQE6Cv9lBmlsrRoKMrfikrO
fKXWmOY3fVOPPWWB+WgxrU6RCz8wLa8ZPe4Br9CzkY1zT/eODlAcigyqxj2qJTUVTMlYhCXS/EaT
yGaeylXuQUGfaqAiNKv6B217rjVPpQgeHFCJZ5JhpMdiSyTAAR3WfzcxPQUvW88N8yuY9LFh2q5I
zE31m0wuXA1stp4Le0Txkl2LSb1/KfSYt0+zliyHlrJ496Os7UDutNWcBmVccXScPl+YHicvHNko
vLPg9gV0lIyoqvVcwzuddLeAbr/eyaddCrBnJZWhQ2mTshUvXaOUCVpHRB/Od9oiYenO47MWky7F
rYRAXbMIWaSfd4wUV88aK/bXdDzvx/6mhLXJzMjg5oTJMSjrmGGRSSYmedOphIl4uMxHrQyYhu+A
nDLsONwmYzw4snYPMlICqTxv/YQOqXu5MUlfnYpgP1ZanFDhkMB9i9nnlGhiLBPEh/Is+cyJ2TkV
ec+lkZT/IMSMiYqtFdsyG6d/7LktOegig/767aONZ+Dn1uy6toKzH7JIs52a6mKjY8aRTgLz9Cff
7tNAKsuOi3mI1dd0jS5mmPL4bzgZRxEh70ciDnfukgraqxLg0xnqEvRGyfKmg9h1TjNPkrX2LKCR
1TiVqQdn0ZwV8dJY2VYe3fLK++g5k0Oc0wFFvhlWu7LGAFmpogTk7PMg7pHgPiqYa8P1CxXnWNrX
xW5gpyJWBKZMT1jYg1h4NEseqMwEedsQRvJJxzypR3VqHv/85ABFGZFfEG+cKpmQ5lu+PdatyUFL
hlC9nkg8jglcubUsy3xvmW2PwhTKD2rMCvifyumg69wP/llywxCkrGs9gE2qc405YeLS29L9hlz1
Ndpt1t609TuadTKazTE+CwE3Uq1furFit553JK9w9ebmL0sYJ/3bh8oY9eY5S1VMA3WhFDJ3tz7F
R0IDE5aGPmCP7BurRDTcyL5gzQyPozQcNrL64FgherI0wJUVhjyRB8wD5RcrK4/btjQxlnH/7dLZ
jwT3B18T1MFRXegxHe3NxdwHb6yIZI4XsqN1YpJDb01HGfcu6gSLhH8PXxE9WE9kdwlonlKfcqj/
n5k5hz2mWD99EorUUjSBTuAvaVA+j9aVY4ge3fpuApqO9t3ZqtbHS+BekxmoFAKjV+u3lDNW2HLf
yaB57d//WM04KIETyRYdBvKZuO/tAYEGPgdNYxE3WVssxC8nDIiXFZE/xmreJln4aSvaiulOJKfS
ni9i2hcOLo/pe3MIQenqSX9r62oqfpzQJAFwpQRmnWXQU4XmEa436O9A8IYQtnl/eZFSxq9I5PvB
ffiGVODlIoSU4szUQPRjTo+i9927Tmd02CB86wiCEE93kZ5M5PTcFYrUPZeM9MzyyTMB8EB0NH+Z
YVpIn/hdjkPNsAlqP6tdSCzNZmPqUxo22A+wSfEg3GVNF4GbXGnHw3A5smb3jjNm+UBVC/Nwyhmr
mFSpTVcYizdcpsLq0ETYYPMwAVn/bMWytvlW3AsDQisi1hTR2QiBq0HYYe2IEdjtJMxj9Oj47oz+
foil271mgdGZnOHdL1p0enOzT7x0lOAvs5No78zj/gkiHstALbWpSow3H6NAORDOD3DHm7aZnwhL
eGHeTwQNoKz56n0wcVpLNHdKgwy4Yv2gg/p0PVFaov4ldNvAJ6Yg8UmI/PohjiV9iQOaBLpX76qq
NCb+gSKY/6dKaYa8KWOblhBIEgFM/q1ZWzN+dpB75AL3yzikaQfn1Apxic07ORBqyT8EhTGte0YI
5vDtAlM45CXosSIC/IkO80z9vOvt3iuxJdVBasAWIrEt8ugxAPaHypj5h7DV8ssiGwyXbniKZGPc
MKvDzDzNxHRTL02ea9aEkXd39sI0VBR0yaXm9zgrwodIv2SP8PyBAIelANvlAf2ISADEwsx2O/Ci
zOzsxpLsbVGQIksjPKKZoDJZuPDl8MhJEpHuGzwplHuUoxTnkppPIGGzozRSXnx72tQ8NPT8NRfg
grlPwgjsrdLSd1DJguCULv2SP8pKTSFns5k6Uu3Fx7D+h0e7LcSDcxNp2zPZMP6YwxkBdDBUxaUy
OXZGE/IjW9pb982HgzoRfDLfA6lP4oXL+idZWNU+SEifDYUhzGei4DZi4YsloNAlXanM3cIZ8Tro
pzaYlQRWcHquYxNGoTeBy8YptFLu0feWf+YMU06/eUwlV9pmpAVS1IRJDI3XKcNpbP4DxaL2e9vj
Y+l0fEjGdYGLY2hiR/ebACIuhIJIquHXWTR6EkvEYhuNj2En+ZB217coJFXyJLx8nVy0Kx7QxEOZ
hQM3+ZFFb8/yg/Mwr1U0yShDlYYBUF/AMAmfCovb1Skduvf10tUcennlJTwQIYRF3EU9YTIQq0tk
/KgIhojJP6dzrdTZRIFlcXNA4ddmUb0V3+AUo97qRWFWxqJ2BV7t0xDh2bd+7F+bcUSRTpvwHNtr
Qa+zWjYqkLbUcfDuly42jxNf8b6XHfvJRIVoL96KA4wU8rJ8pqV8lIRq+OaeJgDSnD5aq55raXgl
dMIFyPYx4sZRhbyMnGyHhEwbZwFXc5odrTd5ZuK8f+mp1JS6Gr3T5CtEV4vooZCURyOGutM9gpCL
BYKFY+5eCNnMwBenYXetwlZACaBm8dPNy/l7Cl23Q0pwXZDspjMI7ea25ve24hSx8ANINwmQgbYW
bOS8GLxXZMU2rj2DJuUdPzQxQRh2vjDxuBwOeuNBNL6lONUhRxbIfSthppxvMGMiEt7inoGbwmdJ
EsAcuEvan/HNzt10W6zvxr3s/Kvy5BnV2K8YdqlGXvS0U4x0XcO/p/ipM+K8z4zlAb0CVYCHh4+c
FLRGXrcSQQBSimbUZd4c9e7gHpSzcX8BO4gzYMOoVjBiMa/NJxQyGMBKil55/lYorRi3Z3XeQK9n
n9xsbRdLhqwNuF3es264hwSGFfThcPZwgKhunedB5E6Byq9aaUpREnOZjIkSNomTJ9GmcG8Sbqic
tkskREDfJIr0Rc1UgOutE8l2rNqrNV/zds1pRExcv0Q87PNSVPWm9+4zHjZHX+WSX8Aefvaoz5Aq
AE9nOMVF8Z/YVgRKWsBgnZvZL+tqoV+QPoFPcJtsMyPG6m5EeFoWQpz0kpvKZn0vCZeI/r9omfNk
BJQOvXEavuXIpopMOAK2sbpy6d+XZ9rdHpPUuw6BdZp6MLyH4dazvbN1v42bn/n40dLFTd5m4PSc
TT9teRpcw5JaxYvy2eWgo6UZxco5oATP28GKxhigi+1MncW47CFxsgFEPLOYcRriHLlaCjIsaeWY
+I/xowoTt8X8wROTKy1+06KTSSyF/3WsprxDyhAV46ENErT0fPW1FpON0BeApF7BTJ8K+dHehTnu
tFBdXBOPAtMV6Zw1muSM5YCMkDeYrDpCysNoh6hxtNqWc/OY3kMsNla/G9xDCl/BZ7rqucvlwZ6o
Vu++mnBVFRt2z4VE65u9fr6yK24V1y/B/Eo9UAGA/63aGz7e5RBXL2BiQnMPgtFrMA4KLY1HKeLL
ib/5pdrMXDYfWEYzmXk3c3qBepUcsu6rWMzhtlp/V3GH0xkzvBlYM/WYlHDC79K7a8uu+fhFMuWi
4rkmaehegxwoiLOoTorTDjE2ifZmNI46pwjZGUf21LfT7pSvn0skRTxCYrVbLfFaDBxbINi+tiba
Ic0c1HJFzLxC18b7va4KrBy5YMzg+wvespmgYxZvoL4IS3g4o7zmORX6s5xfAzSukJve3ZqewA8k
doOnFfeh0kWIbWYHjP+TXn7IZ6KPPjar83207AaMVl44b9l5T+BrKfGMzI0pyvFc5WmO1A6O/uMg
gKdfxdBjF8OpkrkmGAFU8ndcsH0dWWxHxYVOSYZKukkiYBT4fFINZPtFSTL/wVN6QG67dyTlaIGy
Ge5+5HOyRcqRmBzpQsNZYSsCquW93eitZbCuq7nZlCkJP1BxlXZn+59J+1Es4qnSk0+dDe4SLW6R
NW9tWI5xtEchvsAQVX0aq4JuM7lBp0HO3nIVVxdT2Qrr3hirzXhkzRxlv6KAuHsHdLG/pRh8S+/3
13DoK/7J8PJRRmPq17sY/hMunY6+qbYco9oE06i+KyqZxFDpwayEgaLMJbFMwkmyq7PVdvqPwIv7
QG73ywc3DJlRSiBgN9DOliIcRm3gVDjX83BbJ27IqMJhDRnRMpVZCB1XKEJFy1V06r/R6fJPhSek
hb1BVcwX2KOQbudn+nr5k4kA9e3auOTWuWgQC1UakIgcHXuqQvVlNfAjJ/S8AAuq5SbdkK5bSMB3
17uIKSQ/bUGl00q0gw+RT78gaS8r/7RdlD4LPD9rAG4HKxsIZoMbEuKFtQ9jo7KgLlyMIO7sT1X3
vk6ndt4rlqA9xQBDz7prTuBP8U2JY1696j88uu8ujfvS8M9+47xLYuu8ZD8PeYXXHUyqryLnzOHU
EA1JdnJTiFydLkeoNaedZDNa6zdjftLSgAmcJIw5v2duHqvUz7/CL0jcM8abwxbokT33AWgVJahr
xCRTES2HjI6O8d5X0EuMTW88Tg7rvJOUnvlAvWbvRSxce1TJgYLj3wn5TuV1KzFZoD8Wpi8b5t2q
9VbeoLaf+S34sFYRLK+C04Q3fyFi0j8KAl8Sx0s1lWPvUnWC6/d/wNPK+xN/mHOyKtxA1pR9O+cX
s01xz9OqlunBCkGkDs1Jg+6h80Nh45uNEn3DVWPE/xKf85kcvZzVMhl5kOwfYwCPm7IKlQZi3/lZ
/IvSV7ksasibbsOVh2TAWebHT/SVVy0KjMqDor0b9isbH9CzLyqQxFis1slDo59lAiDRTTukMA6J
srVGTwd0OEoC+uMh7r+WlcMEer9hw5Gg4BwZP9TjcKBuE9cXsyeZYxMmxlUhpCIAT1dWSfPLdmEy
lo2hKqvmlz3rTYXNK1RIV6vRE3g9+DJkUrXIJZvvvQ7idS4863y1uOOb3hAeySVBn69OrrOorgof
dt/GCOh6LeGhV3nN7yotD/vb/bbkIqYPN3XkKvhyqIBNNirUZvUvlR2hb+albjEzy+l/BZ68+v/d
rOHF2D+GiAgrOGafHFS3vD8UWCKLWuwCIvIBqiMKMQcL7LPuuiowIkqaJFbIcUZk3g1slIJkWmx0
P2zCyzKYQBcCPaG3TwsD31wJdF4hl/gg2TtRs84Rbs6/LUCuAooj/emIbNhii7I55ByeRxFXjGEi
imxSn4h/VuDoweiqp+5L7FU3aqRlLV9k8XqU/42CdVdhS3RHeRtgErW16htj2UPQavSydIyNuXo=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
