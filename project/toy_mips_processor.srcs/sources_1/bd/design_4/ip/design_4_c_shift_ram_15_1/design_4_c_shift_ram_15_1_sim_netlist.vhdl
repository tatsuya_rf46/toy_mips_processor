-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_15_1 -prefix
--               design_4_c_shift_ram_15_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
rUjOjXowFrNjK13ZYWV0R+sddhnSYZ1JjNdxGe7+vhVBQGM0s3q5iiEyK6Ma4KlEOpKknXXh41gx
HOt8f7Top/ByGKnNorjSebqmc1ObORS0CbtUt+GTmPO5BbtVsL0v1nnH0l9aMiMaMmUasVHeYjkb
5RL/UdUT3mbkJL249MOlWZV8R4kb5gIAS8GLBIwzKgTVrptAjo0ITAOYSd+f/N4PQ8o2ewe34Ieu
s8QUsf2w6DYXdxIuxUe8Rjl25fV++TGQ9XstrLYOEsLTiDrqIYTNnSxllnwC1yG5efcRW7fQyHVr
XHHHcH97VF+Tv6hEA8n/9nEgoMEEc05YoCC3vX+I14+LyrYyewmoQldfmAkB1kEovOA+2cH77xgv
G5mJMwyk/U9uY/oBgksKgF2XhNopsfsRoel7Jw3cZSWttsW7jHc1d5h1p5KmezEB6JcPFDodz7BR
D0c7zUxIV9orm7u0IFVkoU8QaDrYs/WRrilfHzxxVMno2eL2qqAVM6a0DyZ1Ry2uWLrQNkmqgime
P0WYo6UteJ1/wHKDhuB3EZMkL+45FUpwvwuqypDZER7e9YvKqyp5alU8uER8qQWhLrw8RZmKptbQ
e4pep2Hz0ZxPMBi/Mvo+QAMCr0YCs88N9BcNz+6AzrWcvXRsayEXsSRHTw4k9gmR1m6Xdz1TMRY7
5qPqflnDyVY3z23Wl3Dbq2FCtee0eHn3mwKw/LQFMm1nbBa9aImjheqN2LJPMyXuK3q316bmkRv6
nij8WLfPGg3jzgEQ41kZ3tho+KYEjQIBuKoEYYiFHlEfqwcuUNkIl9n//5YrQacSeOm8i7dLPqT3
SCrAnNzvp2rfcWZGU+7s/I7c5mRXmudA2GnHhUAGRWTqmRLbonCEB8c5p6g8SC/W8jNGrjVpxvCz
+DkOWb4s+sINdjxIhSc0q4ILGt9x5wKF79tvVZhgX8pyhzFQgYUyHbEJDrsgKidf6GbmioR1HwMa
3v62lT/gzQ+hQ924oYuZTlCsk42hG3PaeTGeFsN4Quw6HqCfWtSmteKrTxmcn12NYfF/zBl8POmp
djQ+dMNe9HoVw6IUGFV8ty9ir0tOx0H+xLkyxZ2E+/YuxN2oH+gvJFu9eIrvhaXfMffSH+uKbn7a
aFnNCOkOyhOerGCRvsy5LGoivgWT9W+jB+zgW0+bVvNDF/uzBX0bm7TnV24NZ5PWzVZTrUYyLcP6
cbErzP1KScUy0ciZsOnL6TvTvb5BIUApVbKH+reOXsMDhJ2i609RohRY1kDbVBGIjqw0QM3IAAxM
HzHbWH0iCGtuugirrXVcBmjIdoo6GDDeOTre/pympsT06+zud0nITadwMVuEm/IUcQkj568Rbeny
bKwSbmY7Dyf70UsTlYAwx493fEJy5WUAO5nuhao8e1EwBqsCNIjMdtHXfHSshpre+8wi33kA+TBp
B6oRCm91grp8DQF/vLmeqGI4UmJPJMaZ7nKsvC+v0oertELf+hXCw6GtuD5hTuxKJ9WNx3k8qHly
R2EreF39/mbtzNvtMX9/VvjiIizB5SFwCKEboNhRlew9aw9B6RYUbpsHWELnmr2xE0hCBlqWGwFQ
rDgnvtMa6ueryLg1rb+NbaobyA/MO6c0CkqlJDnAU/1Er9T+iqnzXoa0kp8ZgrXTpxFynY6EvWSb
8Naw4flNxoqd6dLPoaW/HgTjD8U3nxEh1jszf4cwqHcUR5MDAEWLxgxwM/rkQhQnLdl0DnWjX4GX
SLlc3RHdWdCGIi+vChMUY0PqnkW7kpvi+dzBHnkMjW6nXQgXtNxAxD5DijJkzCI5eSip/jNbD75G
88mmfWm4mQZyB7Vp+Ry4+AwrejX+ARhOMGRNTQQMAFlpUckbhCy3tZtnmT0KGspQomUG8HlhA6Uz
8Cxq478ly7NWrEqgSkrz9mmcDhWAYuB0YumPi6rj7MGWb1TZVjR3n84SYvKXBy7SjeEYZYEI0pQl
EvMKTxv3h1E3Yz7zndctftrlgog4vCtb8Mjj8oRlq5kGJYNWB7LRwUty9OVPyPaW6QwX8euh+1ry
M3Cs24udmz0DllgjdbYtl6nHb4EdCHE2cBwZ/ojQYGr8fqDQzw1cyGXLNRydiCEeNP4xHltXXzNN
f4NCvJE1lxhhRZlayfeUieYv4x75e5eW2mP3bNknkeCP2G9sHrylzsPjq3AfUfElfHaQP1lJ+DT9
N3eUQrJ7ONGm04nxljB4gLEXfw88pe6z3n8gMLPLEQbZ2qApEsIeZfl1js1/iiYTBOE+h4kGOmVw
OgPQx15h27X7ga5q06PIyQ6C9K/BsN+NVc9JVG1zjLia4xdNradEpHo2/o4TdhuIbJZpd7toa7Zm
mkM7zdpi/OXHjUC9xhawa3+Nh7WXn0Hg8eNkinQiRsUah6pacHC6XE5VOaj9mYgcZDB+tNRE2Y01
5K9JL2ro/EiyvAjpfSVNKxCSD7sY/WFL6NhpaPsMf+3hr0L8EHr+9OeaLkCyr3vj4VC1/tECEOSg
+am4+sJH1wUj8UdhZxpQQUw47EbYlDeg8La0oLlFQcAOTQlAPqs65U+B5T3ICJP3azbZUCVT7fEi
vSo6grR2C3WAi0W215twIK1VfxSlM/wMeYy1GdeiSKlFp5pdrLqlxunZMcmgXsXje9sklqXRIFgO
4EN0rJ7E0NUYSWAT74nAtAeDpLykgIViFthqp+x3zREWA8qqSu15CQILKWAbj8g6Ad379KB1FQqX
L08EL9iRxjRI1tmn2PYMQgxJcCJJwCxC3yiVcYrSPavDMpPYPnVns2IxO/xgGiEtChBOxc2iR9yt
6YjxfHZj2fdI/RLbHWOdKR7sturJdGbi8aqlF4Pl6smX9hh3put7mofaopj4IInJYa8yQ+w7E7r/
jTYtq6131bOqYUC0FXUrlB9C9kELoMm3uOc8lppspv0YXaDswIRBTcKs+hjoZYyIOTtW7LdnQVlv
2yWAZFsGKYbhhMMFPAQ1SsVfxvO0t+TOIgfWKpbzmUXsgZ71JgbPaAkgWshpHO+WZS4lb6ift9Jm
bh3hDNn7MmP6f24gqYzIfFfnCGK2tqcsBI4jceiCL3Crm6UKrMPGCTXGFjfQcFp2zRg11VinKH3/
ab9rz1tR06zsya5rbdB6XmymCw4ptD2k50mA00Z4FanKY347EKrwthaJ9pJbXAv3mAPtIkEKxUeT
Y0hgqHNYMnxKmL4OL4G6mhJ2pHmNc8BxtnnCWeonOppZi0Su7xZrhfVpPX3MSQYBxMKa3zwMUpUO
+n6zOr/ouCX6mWP8JjVa4HpA9CFhMhGSKpNJYx0R60lbRyKao8IymmsFDCdhPRZP8Lsp66qOnv/M
IjcGAXJxMRJoGCeCNrT/FzFL7NIV1ih6E4SLFUaWt/5W9LnFlYtXHSdviT9SN2i7r6OWjT/KXEcy
CvK2BhktjCINxDx/3mzqd3NboKKmOJ/tbUdabrqVvrfYaFaE4sIuC9uWqQ1IUP90/2qKS9k4vg/m
mgUtCPJ4hI0uw0Vy+MIAcdpXqinlFgPBPOhAh1b+2CtU8HOAY/3q2Fwk43p1WSXhmlWHPlenfIIi
uIdA3axZFKJRbZc5nS2H0fauKsZnVteoyDdYtxRgrDmp9NEtzTzoGXuZjmEBwogdwqE4oMmFRRoF
1sAGJqvfkfNEjg2Ztke3/qHvQr6nnkEWedrWbSxy3+QLqMeRheDbDtCOJ1ByGFj4+wZJsEeC1EA8
HbAcr7CbbutyEC67Ixe1HKpuHJLbq1OqHeTGa9uBmKOLwKjOHkTSB/ji9fNFwynKBAd0zbMwjUaZ
lhTapb90KBClg43anxNmqrq0gANF05acN+STTMX5UJEzkH+fmdbDzK7LiPJZJ0Fqn6H+wPZDhJjN
AikyXJVEUTHryAuborvJGJDWGdCYdzPJqkRqN8ijrWx42h0gANuxJwDb/bc/vN1N/BGeVPZ/f1AS
0pPmmp4W7yveKm/1ktxZrtUMA+iqhRFKXkgpgbPrISgduEyqMSrsfux0rQrjbbkNEWKntjp5frc7
aS1g2+aoc7CcwEtMXx8Cn8TL9O78GqJrQ0z1ngCIuu1JYFrNVsUmcf++/r5B8J0jhKTOjA54VTh2
E+Br87/p+24Hxxr+pf+DlFRw9XyFR28tVzdNqQzp2c1e6EagF2OYEkoUSdoJ55EAuTTAcbkxdajj
sFFjfL1pcLple4zHeddwJOjxN3OO3iID09x4PR/esfGg+VPqqRNeosMT/A0ODy1T0hyPPU2zjXcC
b6c9BPUJ86OJe4q67OHkOTslx3Bg/oRRzXVoTg9diNXMzT1SDDRB5PnZGI84GFvJfA/Yor0E99cB
oqt2992BE1kTPwSvZh8MTzGW5EbjKh6mXEAOuPD0L24BFNkwYimUyVJ40luCCceS57RRlKS8Ww49
BmGSXxCMiviIvDpL/BNZBh1Y8+h4I43pVXe+3tmCRw3dQF4IxfFuqDnHvJTLzrLvPjHaCYkQ1OPU
EGfm9vlKwyOjkCaG8iVMGB6SF5gHY8Gk0Lz0EGu2Dzekf0soPlNUfCkFDM4EOauZbxCWki4W30l9
R+FZgRtywIsJO57/tRnuuD+LzL7oeb6uCLh3boFHzzp8qiThdChBuzoMGV4GFyEqhmlauXa5fpyD
fvRp9ZBAfVdckWrha1WAuObjNETZ5PLQL1VH3sGRhRDO8FKtm32qvFI4c56LqTLCY3kb6R0sYHCR
5p1Fr8ZqoG0jVuTWTqQNxbhT9hyBP/BPACUXBICri5V6HO66+9puoBpOabugUzSJHRu/BTPZh8Qi
/Nto1OJ+K6ubSam+s+eyYFhNfaZtIYoo0fIlqUT72HAtxKldMlCNLAl4y147T5YDX8Y4meqGNuRL
+LsFYqYzJ8UHrg1O/rOz/APMF4PW8QZqw15y7Kt2kqFDRSjoLX2XcnSTV5+rSVhWAw83q5Awa8Ck
wER/cDl6rDzRTspJgXJ+aD0AExEASOWAVDAbzSr2c5xIfXKMticd7s19JVfPXi4A1k/X1+FX24Dk
NqkTPptF1u1G6uIk9bPkCtG3AN9vCuTq5vtDeNVTRUZcuifV8YRDuJVDwKecXQ8d19Xma6luHwz9
fE7nmrmUAIIfuAkxNXLZmiP4HaYKwmyMc97FBF67mr8AF4pA3Kddt9IxgJcrapQIbZVkTlLzM1Dp
WZLgqhsyI4/6mJ4E1RY9ZTm6erIyg/S/GqdzhP9lqjC8MebwAyB2EJ6AW5V9iAiQh3Aez7wSYZ00
PErsxSkXNb98DjkyR5wpfA3okyKyOz4nKDSvrxnJK7i7t7qxyEuBCnHAcRnQSIO4pW/LjlV2+o/4
BgO2AzEOuY6R4MK9DV/zpkwDYE1c8UQHWWY7kg475W+phQmRUT3KHml08eJAYniNcFJ2mML/5Hu1
QsAaFt4276zMXQcx4CqEC3iXc7mUFO34pb2w3cbfb0pXJaNO1UCCgGMHj8bPhtcVky2OWuho50/A
Z7xrn2O79VCKTPgyuxBI+Jk6FG7D4K8OfS2Cw5Ujre2XPskj+4Zuk2W8hbsOHXeW5UQZO4KSjAA5
IYjN++ogxvHtWm8IUxu3q693lY/dwltOI+NWm9YMgScspof3v+SSm1hGSNcWaYuyK4Vb3v2h0nR2
XtvcC4UoFV69Kp7FDlKrFgd+SvU8Jb4OVPlO69/ENP8KtzfimDOOYGBLAUWraThUkeO/SOAQBA8x
wrihqjpMHQUDK2G8hHzIyT/rB0xASIC59+4e4f7sl5F+xMd2UXypi9BvvQVF95qk00jKqRI1Mm7M
apjoT6Qai08FAdlUGGz0Zm4MEqnMSSZTy+ecuuach7RNZzpgmCDcslEkeft4d8P60fNu8s6PVAwp
FyRQ5c5w2Ym1YAr6mIe/aQ+RajfxdWSKdM/IYCIAJK4AfxS+0PIugpCRYXxOYX7MLmWEaSb9viUW
mgtl/v7T8HjJs5iLPIoV2ozBIbeU6gCSiWGE3hcKyR3j3nZBNb7veLuGDcNfOivRMvM/qPuyrooM
lTlLl8SkH13L9ZqywQI2TQqE9etQ6hDLaTNvud6yyxvJiGFSMuaDabz2wmvJRWw708+XUg47g12I
+oZJRbEBlXROMVIUjPlOq+3Fi3MbngwdRTcoZi00vrz6CDJMVqhysB2vqg0gePJaZsSs/BGHYeOi
CN0ILPQcZaoMxuLO2Y1r0ow4QonBJ6KgjYZtxAN2fgYPf9zhVw4llw5Jp+4L3+o3Azuzem63hAwW
hjj/gcU7mwmjKCVedlNyDkOZAGKvrtGxP++9PkqBABs0yBRseWrh3P/+UY7ytk2dECc3SkQSkiJb
vHBo8CX5Qho/NbL6gaOxNa4jI7H1t7EvkWgsHlxqnma+eR/yKhRfWzfbeGO5t6RJ8SKNSv1owkY0
lp31PoeuBGPXcY9eufxscK8wwYNN+Vs/+PM9XqNYZat9LgR+JtizLpz//2uuU2hyRPhddVIC2d+6
4bKMpfoXW5VaZHOBZMVl8dz3P8PKibI9C6YrgI21LRuOQTbfscFgspmADsk0QRD6DYBazr5iAuky
Gnk+RZ6EJQ3DBJ/wciEJK2RmFgfmcniV8Vf2yfwsmSPw+9pBsx5dLGQg8BJajyp0gyFi0WVx6MmY
X+YW/xoaosYE0v2ZAd0RyAMn8gimvbWADPCbaQfEzzgkjCuOUNc9EZmFYgJQrcyS9ptHBCuZDCNp
C9ERCPm/2VIjIc1qY8QA5JxGbSJqUcoVDFYZlrnTDKtnBtXlHOxc4ZnTGn8lH+kJY1I2H8hthkHO
Qmbp3dH+0rHY2ttIieYYFp2IfDgg1q9OmBG9uzW4/uPkYXbyqLVLKi36IrPqCA2gBzGIhPA4jIae
zgPtkwuere5rRU9rOnrElDzM5wjxcL/nkdycLuTiFXQ9lo5lsNtIh5Yw7Ii6xeSmogaMLHjqcQK6
jGQKNBkBr9WY2eSpUVfxa2jaKO/2sl0C1iqtId7QcItfshpiPut8Uf1x5ijPd7m43p7HrX+oi8NE
Y21ObAx0vuxoZV81t/fWaaJcWWCXbw89fDJc+kqeeIfU1V0p4TeL6zOHC7Wy5VdMmwsDKot87UGc
Xk91v3CgAf8klQ2mgYoV5JMQAqZ5byza3wb73NZthfcbQMaNIHQbhJIdKs3v3kajLuS0hA9fDak6
kMH4eL5ZjWiCaDaltIWMl8ME0Y4S5h/QudHcLc7yDLstyxbAGB/6a1+sTq9+beMX0olT3YQtbYGC
N3emqJ4ckFzQO0Y/6W7oSeAJqkSUx3yUWmnf7XnGT4rnyUVmBzloEqTxRz6uzdIEYO6ph/SnzoTz
3wpTD8ebnaDkHpSPALUXhDVsQYnEIQlUuidFFqrJeOV8LO51aZ8f61PSq+RqFPRih+WW31QXF74F
mco7QcOp+XMLbPApojaLyKu0bxKk7Hezr+P1Mnfl9FPNxBifedGkUISYNHAB3hQ2+h71E+GOujN2
25N/iRb0qSAxM1mtsFYCKEoYoLRUXNZlRnRA/HZMdy7J1LfzPJSXlqwB9ADHgdUA5oLDTfoThxLG
X7c8HkfqhNpiIEArb/sTO6xeoLB8ttZq//1kAgyhu/zqpfeJvOssyxBanW8SuHHnrL3X+6i2fQdH
KTghIT4oVEUgabccOW5V3Wc54nPqY1U9PI6lGZCh8t97H/jkHzwwfF4ZDyCGTjz0BRcO8xeuQVyH
TYRzO0V4I4BA+3/P8eNHq5KoX39f8sZa/1lzSl5YtGgKtCWgGJTmdBooQpGvax+ncY663GZa1NJS
mcbtEfxMPohBE/nIJC9DAWSqObYC6eVDv0Dja2jA18pzjhutQXFZbsfqXFNLgLPU2O0GN9sSgJvJ
5ucgChBhuONGfIQ+pHy+9+YH8VCyuiWnnZ54xiHsaipUirMEhP0tnijs2IFoN5BshJZfmsryMrsc
342L/WBXkmXjUBJ2BMYtxJxliAc2Rz9BbeWE0f8rB5y61NDoGVoDeuFkkFjvb6tMZwcSc/UALyim
HuLK7bVmkraiAJh0ThZEeqTtFu8mOouMJtB1VywKvfYEm7dUkw2A+WO2XJ4tLwOxWxINmP44/boR
7LttrPvKqh3HNPwIhl5beCheHYYOECKnw9EwEqHMoo2puJA782XWPN7Q5p0QG28V+E1TOizs1Um9
KuvLz8E8bUA6T9gJ4fZMbRANANC8NM3QaxI5cdZbnve/LE37fODULx2f3VZe8+sMCxo1AZ7oiHaG
rIXGRfzaZDm4ccnWd7v+N8v0pWHlaux9vCdbJsWZDXLY6cuYoKqR1h0Tiv9dbOtNPFiaD4TrdeCv
MdfVctXjtrIquEE+rIzH+2u11ECm5NEeGya0tv46hvDsZSqwGpt/8qeNDeWT6qF//JO4BI/xBsJ/
+HpmOMA8TqITMVIeGAqkkVaM5N5wcmmWriXcx2k5FHTSQfVM7trGgohV1UqRVghZNzdRP95Z+/fw
djKnstyarKJ4Oal221KnVS7Sg3RTC90xvLQ9+TJgqXGsjbba6IctgOydvMQLu8hwNWOKOC5A3cWJ
6y6U+dv8O2sF7AzmuS56pCdUlXAnA6/WumkeDaGASzi9ctziKFx3jBjJPJoDyBn6pMSVn7T2QWKB
OuX2l/NzTDI1vzSjtq146EQjjxbUWRYDqsfQY3I6Myro4gyVaJPAlWh6bLXZsz8qMGzcbVb5BRtw
fdugATitrza8bKmq3/Qvvp2Z+eFpdsc1aPCp6W+lP9Cwk9ljBtwY+Pv4iZcwnmMOWtDhWOdYq1ec
a62x9RHwIFXbEwYxnkkqcH88ib+Wq045lmOpIfNQMjNnPSyYJ5JkamBePSWxi2WdLK9YT4bRw7LS
nydhoX7lFUSWkbELQoMBew8oDc4KeXotc19PNzIBfghbzllYiUQsTUeBL5jF3Sc0KZZ1+Oz9A44i
UrdzJtrpGdA40SbdEgpgO6eNrTU9nJdfqrxd9XkZuVWu/9NFOcdg6IvTALZaJ9lZhIIV6ASzG1Ez
EXKPuOmb0qepEWBQXsf+joUph7/xKCW+lkhadljKLJFiV+9N4M0/PSauaVYiwe4EApFSMjOt2hcW
gidmhbV0IBBqByDwEOAn59fcpvHMz1l5+5Xt8Ld4ZiEuLwvmPj1m9aVUfAO41qWx5eqNKZjXZOvv
Fv1pTty7bcGUzWUAsakm9cNS7JYDncwbBj9nyEknx6nBL/OpnM5eG8HmqqpCHx64CaLIM46YPsQJ
9q8bTIOe63sVCBq7NcObZB8HVoVdfZF77FbSdtLczwekKhecfPeahlSSSx16BGIMisqFbPAmRHP8
JYlYoE+4nM8MuPtCKuzuwdZSfgaY110r2Uu0poLesVQU9Pzi8sygX0i15lH3Y+oCW6YDvjbIyCH4
w0OVtoWyuSngqQAZAH6Y3OSc5UXY/wEn/Q3G2Np7JwJ45juYVHIEXA8s0IIiTOGTdatoL3Ibi5vi
KNPxOw86f2YT/8aU8reaYGPrQfV4RZNb30o3YVE7r18XhahOw7znx3qZePUV6sxL3/j++U8NJwIV
NsRv1h0E86TO98j7w7JhqIDX+LFNtuI3LiHLrXvPWIhQPzqss+GBCEMUGwg6aMABWHPNciGse6FF
Rt8y9FDRsWSH/rPxhq1joUZ64Iw7NKg/AyREnCFi3TBmxFdTyTwaoYumZrTor3QbYSccnJtnoQV2
92opJdGsu/4TKuwxZj6//Qu3YPIJI2SBm6hvKTdPf0Vu80r2+WkERz/s3rwRYvDcTep7pA+zeUzK
t2LhqhmKrS/p0OrnRRGXPW4eHoMWvlPC+r2xMyKDXKvb/6DxMT9if/9p2Lbv0ZfQyR5a3+LZf2Wp
7oZucaBhmsCxXoa5VWpjVYnVr+o0I0mYO+GefeqTJGP+FaVafvDnG0Q3ddh3AWn5MtYienNwlbsA
unmlve+D2rfApTwvZVjl4BNmiUiNVIS5yaLlbyiXE2UgoTfPRpFS2uwunkL/LUXDqyUSQA+ggFX7
BisoNzXGa28G1wFhonoMAebtGfYr3uKyHgc+GDyK0nZN0aYZ6bNAE6Fztisy1S+uix6yqff+Tpo7
/zG3m8XyjU0RVlD+g93dd2LyT9D0uzaX6CXRuLTZK2j5Q5GS7fv/76WeIckBJpeYHxQtZuQ5nYSu
wtusrOXTOKC38C5zxfw2tX8yxM/qh12EGRJUYmoj9vmrxSigh+zql22FnccoT+Dkw7tl4vouunvQ
JbuHffi3YHRWhak8rHkLP+IZI/nWRyYWWpFgphtSUz96EH12vBpu1xHCTQNnvPszqeGtIi/apFSo
J9T7pBqZNDqSpVBDzvvRwhs9F40IMbqWqYdKHT/3tjmCm3dWWleTxL14ylv1rHSbzcYpgUwRnk5f
IAFWa9ZMdGU7vjIMS2as546qs1gIdpmR/QHpZu24aF2rKuFXGs8UJLULnYyxRFLtDWSLZPPNtMGm
qfJqBo8nb2UAXizRMLgZIZjGutzYYFzxNAbeTb3LK9mNpuSrHIXBBEyyjzByPiZrdz8Rm57Nrxuf
GacVRFxQ2xCsHS1bRnpY5+nAeVFY2UQ8FNfB6/hiID5jVqAQYo5kch57mWKe4nU70+3+ZS1JyV0A
Od/+a/CRSo+De/LT9u2F2vCspMUsQmfCRVwaNCEbGesIjkr6qOYtLYMdo0sKVECyqo5K9kMI6sPe
AM8TGMxrFJLAUqdcyAHvwMWro7H4D35P85jLfr9ItrELbM4IznCtIJlOWNcx+y/cdh/ZAyjQtZrB
oOx/3zIZF/3HcOxArjsu3ED3ZAB2zlf0C3ja4x4bzNPSA2QL8GzSTySD7DV3jRMPUE8JdK0KC0r6
h8sJo4qyqZXifzWgNaAJe0ypAxqPj6yC1bA7lq7DNsctNNjwzIolyXy4soEBanOITAlGw4E9qnB/
OCWggPxyKcMWmyQ0iB8LW7UgqPKzrnDKTZP0oI0T02sDYr11/epX4QptSIJMq22nh6mLYd+Uox4l
ZqIMMm88OsQsuF1ssfAMslcdZnWTdI3oR0Z/ivGg2XBz5mW5bkcCmEDn6B7CqayI/wmeR3QRcnQC
f6V/KBQLLiOsEjWO3Q3kTT8WiWtGFAvg9g61D9gjbwOywqCLinakXdc8FsQ8QwyQg4pZvVWlG7lN
b0u0JJtzOclWeSkRexqi7jjp2gNSGyt5deVa+qCapTVEg14WyQ3fYIUeSxPp/nuGi5ifMM/kgPDE
WFYn8S1u/h+mbJlT8tewGz5FA8vVacmubGaE37xBAYTQvQoCDmZCfvLc/DOuvDeg2NtBjyCi+U43
2IqwJl54yrnpo+sx2Rw2QoRwHZuvDVnuHYOMwkC0Phd+pYr8YvJlwlPU8QxPVaH8cQULOEzCoPJm
vzyH4wRbV1YWn2FLaxOsHeaZyaduF+YyD5/ANHYniPqniF+4Sm+JgdyFUUbwq0qZgj8XvfHcRp9+
qy+B1XfJdStMDQlyhqwL56KiFlYPGUV7T/ziEJ78ofyCG1ZqAwT+PY196p+uFh/mKCgaRto2kL5O
wOqtAq78qWF82cn8bEL0kbFGRUgJGfY31ecPOSe5zfoYx8CROZl1YPWdn5Qall/ynclJ3mR+OJnQ
jAVYEyHIfP/9JEKdzpKsip/Iv4PK8ClMVYfJuOmIHRyZvaQiMAanBgMXAJVXsy0Ak66cFSxMfChQ
ZKkmE+EkdflaRjmhZd0kImG93ErIXTX8K1sVwYFCMkhfLCqIoVyc5Ms/LOC6ZLZFj5oF8QQqh0HR
Ca74hC5tnrJxYrDmcOjrrFasKog7zuc2+1O1oJOCcbLYt2+dkefFpix1ASDfSWEQ5oOubuSUWxgJ
vLgRWEOQA3NaW+WZDliDbhGFPXrLsvaDPBELUiIu7VRQxNrZf9eP/XoUUslFWh3rV1qZeDa03b5U
hXkBkd4BS7yNYKGPJC3+2/1t2i4gV1coJx14/NTgtu47q9A6kzF3RN/dAlQJN/BWRiAZtQ/NtQCY
Wid4yyHdTjauAhInEXJGPZYbo80LASvkijfVtk2JXmHPu5KiiF6i3D+abbD3bhpGsn/2A07a4tcM
XJtd/mXowASk/MbIIi+BohMVPLR5mwoEivVtthMaiejgIt6RMZDUzeiFK2fxFGB3/+IHvImInYtf
S10EgZLKUW5AmUptfHSfzZiXoMQ1vsY3cMvaVCJSSJsjg7qToRB9YKMRGpl5f+4KwEXz1qwf368m
gipQUBlYiqmmKxqpQYU06Z1i8RESqCj/jxrJivonRlT2wMAn6W7CP9MIwlOE+NbSm0X+047yNvN3
5m90sX3d7s6M1XCjC3rVRiYsEPnAn625fI8dc8ZANsdX8I+y9K4r9jzCHCkigquMZhmp5hUhmEKo
sG0FEF5zf1UyzawXfQTYkxUKrrHqAQtmKkEUOJkbcK49R76/1SaS5vI5G8UFNALsaXKvMdS8nucX
Kn4QnZk6UjCF1k2K/ugv1KT3gFKR43wwl8NgpN50KUMjw+vKiX1mngrmG7XnAXKrYesPy5NFoNib
pb0MWSbMERZ1hVoeVrPnjvEmlaqTcwkhCay6FmeyurEdqNE2dHx4Vy3Au+EQkU3oshR5AlKRlcaY
2jnhj30ejDLLakzkwElLz44x7CRJIoz6DHshY7O4EPKCJFSjSsJA33DwbmocW732CzEN0q4L+Euv
67E21P91zgqSv6KgEeW7I1MMhIdE4NriwbWWYlZnH97ftv9jqt+4GxS6CbN2ukiRGZT0VI2PDsyV
Dt892EribfDfNuIUuedhOFHPpzw0Zb5H7QNLen4lATT6pKRgP0rx7eEhBtF4zqvItR5X4RhVtc7r
28tSeWZcrl1dbX9iR8jChOkeQJEcCIyKSGsYWrVcQO0gDVPOiooUUaSMX55WO+NPydUF/fm4NcQ/
8YB4L9ycbwxaLdsAFZ67dyq32Sf5bOqI0e6VzdVDxxixYo3YTIqwLN16J2XChE3npIIn6+RK/2O1
HV/oLwMEeNl61QmWCEdRw/QokjT6pbW+Jc7Ksvwr6AXf30/5cCCSSQ2yPoY4eq5qL6veS7B9/7Yw
UytHg4qWrnR5xPVPY4dOjiASJ9wudNRyNGNQ2nu/Ks8GcAO90OF01nKoenihuSlMiMQCSPtbaNnS
cgw2EbbDMj3o2To7SWlx6Zkyxkf5fwvWasT7JTKhGZnZSsAocRUY7wjnGvGthx4qjPqfvuJ7WZvR
wQgyG/Qv3wh4d6fn33rBQkG5v6qvuA0J+/UGBa3DMAsH9cc/ddYCgrVueeM8+FkhhvNfXUza124u
aLGmMSvbdgfB1Cht6YTMFKs/tXRczPMLC+nggEH4sFHeRIyaw/TlhY+o6OemuRC26TdPqvp3oVSF
VkS+NkwKPR58mNoK9xPeRPa52wUCRfpfHWkt7VJhB1JhgKPf8AU7niO3FO2CmRVlFOK1czaCdkkU
d3yCanNCO6sHPbfitg7piZWfHM9+lWP8IBUuCyUnRV1pqBWTABkAEie2fDtSJ17waeE4W9D1KDSl
hF2M414J7FePTbL/kGaUD4/t+vvnqVdj0LCVHwVMqudC5MhxCFyCKXKdK1mNKu9qT5gV37u3xxUA
RUP31CAtTiceuvp42c3a86H+K/xXVvdlLGlrJlKOQ5J5u43BF+WYrgGk5sLz8sV8L0qAKLKoZvGR
s+OpYT8btjJ23irXL6ru2HvLYZbBhyJdMn4yULMwIC+tEEEV3LZY0J6KRntnXRa47BlZKoJYXwOM
FUhoq/zzeCsW6UIMvfKN/8UMEjcm0knFBswMvqo8qBT40wInI9XgVAAkhhpBaOBOaYcUZe4s9bXH
hzOJ5GG3Em+MlIDl1uY5KeR0DhxM38PTGg8V7wBZKZAfMEeP4cA08Iohdffvc1OBXLpyGJ6SdVl4
jJ5CRnPluOCgyK4jx03YikQHbDlPEQRNfv6mH7xkg2bAgZmik5sUTYITDwuqZNUOWMOSeobSbhV9
QJFj8Fr8uP0cgRiDZmNvWT/ZF3DLj2LapMmiosolCgMYi7hLFOriCzFQAl94VaaOBOQZMqcLsiYR
6oKhdYwKl9d5jqocr2FR5THOpl/a16ZJHu2B7E9vh0C+N20OiMzJY6pbPPQ4cWnCoV6cDSUCX/om
QzyLcl6iE4yJL1wJCrUu9YZfUHSoJgs+q7sfuRlM7zmxpPIeDGrqPK+7O3lLidfsDaxWZW8it6vB
HtbAYohYX2IMN/eWbm+VtjOy1IqFk6Hw3p7Ll0XGQaaqOBykTy6iSHvXj//6SYqO0QAVZ2VHl04d
SFebWsOyYh71JuqOKQiZ+CbsnzD3EtEj54mVHc4aqJm51qr7gc6oopcWWlusVbKHJlDqPlrOWYRg
5uvX/6xjBazWhrJLbXSUzxE3HPzYcA2SCwQo53GRtGOU+kBO96XT/8HYig6TyAHheIxldaQVy+Kq
3vGkoMql31iaaRHp6sHlSTCLFFZhc0yL12OaFh5WVbmloSLqZZEzoqbvymf/+L8SUYE2Nkic5B3k
53WmiRLAA3X38vA6HKhAc0ggjBooaEeag5NBbDKqSD01q6NFZ3LZnmQ+T+1gcfePUwryql9ZSIYz
5hWHGRwC6z/f9UIWjU5ujtiu59jXIBMuxCmgRnrTCHUqfSnoHg8kRuX/bpzB1ttRToSHWdkB77ME
fd5O+i3esJ6LEIcwzlVpjFLIG0Ei7ycKekcA6+ZG5kLdTH4lzvmHqsdBLKc9j2lJVWJCfivD1eUm
ExRT99bLRed/wUdCNrGqf+++CIpYgoZW+EChrHYtTQxzoAiMzGIdeHKrzB+Ew/BnFrb7dEZUWUY/
2t5rHGuYzpngaqgYuOCulCApxAkgItbOuQXDhQI+exYFu2nIsZWdk4zjoeeLNj8+YVedIOiGDm1F
Aw72hzmt5Tr+erH8q8qAphuiQTTYvtAYS7mUXxIH92Y8JtaDHA60OzphAEZHPqcRKnnO8mQczLA8
0X7Yh2BC4QDojnnnqeilESa7YV8+BUn8aPqCVGvITgEM1QMJ/4IW4ytjmcvHyi5zky8r0DZ3yckn
MT4whwiTcWvNHH5KTt1S2Jt2AUEeI1eGPL0AmZBLXnBg9TAmeoW0JkTd+kuY9MrvDqDkx4rnBJGW
qtBWLE6kKnI8KErtIFoWqmgk1ZfOnXuT/a2HjUa/YVkHFpJCJhGVBKkeSbzXogzAapdWSXR3t1xz
Njrt/os+1/NDs0Tph3Bsgh63mzwR1m9YpcCQBLL21dIMYdEo0g9FtE9Xzb6ZXJKieD+aDakHwYhv
xUNpUfmNRYgSx+l41SrN87UpevNAeXkpiWdnq9AxU0n8knM07uQdpSJ1VxwGK7jjP/AFBFbkPp5l
Re10Rm2nMnogqN2aVDohB/+7cP78l1Nv/rhe8JvAYdDS+MvVHm8TNBUcIURMLSd3CNd+OtBo/uDd
8BQUaZradelOCGI0toGCW6RN1JbsQqApMo9tXbRwyTHxxqW2D9xNrsMMj9VPorCend/cYZ4yKDp0
LxeTB1dBhQ7Sw/xUUB1EGqqwC3PITLmRawbftNLcyE72EDkj3ExViw4S+r07TlXBCkHGfffWkOR1
jxruF8yj7xmWx8lHfucAFhaXx4/1hpO6fTzq4Hdb2MXwUNMpUOYcqStgwBF+KBu6tC4VXNaNRhhM
vkIJ0ygIEoBgG4/FIuOzntF9DjbfstpiIahFpnr2DRCl1QAlOcR1eBFnhDtJCiILv/lV0C9kcnUz
4zrUx8BnfXEsDcbt7WkwjCoj/2BfaIWL3ks27jfVY98yKtRw5LSxAjXsX37w4s7GEvU+wvUtz8cZ
+VhWOBkL1N+xC4+F6g9StODEb1mxLBbrkZmlyp3jzcHv0RljWb3d0+Uc0Eipygax8GQyc/DNWxF9
TyqoGVuPFZc/6Fknnd/68mG8PajFkv7VbADxIZKoBxYCVmuZRg/GYpkdd87ZZV9hQD+k9hJZA/Qj
pSnzC+XKWVy9OKFRKUzpoYSUf7/+AymO2BC8VqZUDOC2Mb2LEPtdi9Dil+KwMx0OF2xoIdSBfQiI
+I6dWusP5Ol6U/8oFeNokl0OLM3+VEatXEAdh3YJk1Ogm5KkaATS9ef1Wu6TWDvaFEiGhP9DQ2Vl
9BdaJZEMnzRJ8zFTeqknRic36q3Hj9gqTliHrA2BTc59s/1MgCUZrGRN8SCt+k85Ne3U+/HifrSa
vZKtAMppyt2Tt3TJURH2OiI4xddfNMLBgAYCVelyfeZWuYo4Ar8t9BZuDOwbF2hpwNBrpen9BR2I
W0y2+VI2vevJWkv3M1NMD+eC5PPvB2wdJ/DBzc0LP0myzSBeKmX4dbJnSX/m0EQ+7V4IB7qCgGcV
dI1lzIpcP0NFiUs5pJshwPaLai5ZYi2EfRjt+xffPIDvxvMOrSfOEzheVsqPXaBlbnaiFXMs2N9x
/QZeN5QtEVqy3eRdRnduOhaUaISjlNjgtyrR2qbMKnwUfrKr8FM45w/pfUVL9TLts1hdN92kxNXQ
0UUPOZb7JWigZapTDop3MH3fGqjQFc0W4K64PjvJX5XpOTZO6hXPmpmtB9C67PST1aSomZ3AZ/Za
ruJro2lLeVHl8BOpdj/RJzFtvGA3UyIhWqdtPo82F1tIRApl5M1rQZ75EsPXjedY2jy4y1QKuR4O
kqnvxmXu2T7zcGn6TbFNQw3pJ0x0FiTJuf9R2+nqT291KegchOxAlicEdabmN6bxKzIKslOxjP+l
aHKGHBUvF5LF9AaVJgc3C4pTwUSyK936t5ECQHBFpvSkrsVDQiImD7vPgUuLfbJcV6JY45GZLZCP
WycgqLaEOd/OQdE/PGajVy+IjmfY/5uIpRUaGnIkDGr5xWwcUJT0iZg9PoFzbLPMYkSvlIKNh7HY
LJ/juzM4HKZSauEhM63SZ05CXJRTJJvbTy68iz4ooZsTFMEEL+YOdZGPfTQzCqJneRSEXtF/0URU
L7DPj8OopOXT+0QzTRwzAL2JxLpKjpP4rQIbnZ8Kqa143pjStcJgP5bof9aXMAd7IsBpXrrsMcEL
XJO6L+88168wxrHjwoo8yEIAr9joe9i/ZYrWQwpATEW2e8aWKg73I/XgGMH04ePWk/R3EIQRPV3b
buUdW0/5yCtfjb7wjZADpCBzf35B9RYVLyO1WymRRVANfWyLrHulTiWUcbU0zIZIBwAgD/diE2Xg
YI6jI7xOXf6JB2F7fl68WfaRoyy/Sl94Fb1DUNwFvpieZIUo7ze/YaAf1Q71T3Vd70/0Vtn29A==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_15_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_15_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_15_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_15_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_15_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_15_1;

architecture STRUCTURE of design_4_c_shift_ram_15_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_15_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
