// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_16_1 -prefix
//               design_4_c_shift_ram_16_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_16_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
+mumPnkDJKzJwNRgS6tCmhg9dP016qSCqpHbl5tMj0M9YGP6ipWHuYjFVvjW3+YfTn9GO5qPnQ5t
l4L51iQFrnFhWyUvawcib6hZ789NkcKIi6y7rrKXLFhiY2JQluETLxSwifjx1zoQm2Sr0fvZDlf+
OR+bAKR6f+IDiw4U2ObOPpl4csm4z9EUSIspbyq5+INE4GHFArOYfhJlzW42WyGTbDx6aQNbGUw6
OhCXIlzV98AiayyM6chu+vM4ikHQyFEwG7w88OiGVXrBpKlXEzUdNQ6Ez71so93q7FoBqAtCyCHf
J2nmXoDp9pAvXLLhMCRVrFP7N6vrP8b059/Ec8XXyA8K58yUXynfd0FVca+tyLBoPEbn8E0ZH7qJ
77APTIygRl9TdquzlLp2niXZLFOcOiCMBqasDJd9mdCYBpsHHIMy+Y2h07arzJK6ch9jlmMyJUKP
NoDnPNiGkCoYDTUJ0AzsOdozc67ZhjY/7iPaZPN3lM74qpac22KokMm2IVMPduTAAcgH9o6gEn+A
aSPuzGl4Yoro0s1q/6LRZaEnxK8Ez3AmdAiSu9sVrYegXO/3n4ekqm9T/Q+2+2t9N/BCAaFlGxVV
vvNXKzyPPuW7LJeA65cTu91mh2Vr3M+dyA2NPv0fJRa25YvnXSnT5NWH4OTv45BXR8e2G9d+VDu7
HM8+NN7ts5ZzwueLege7tyTaMEwyfGGFkKDjjNJUEvHbu2cypyoU6F+FCRZAP6eWvl+ishvUOIcJ
dq3t9E0zQdXg9bmDPbGI+d/h1GI+HWziJdor34y+Bo4Rl4aXF0s07D+n519O9fXXMTqJnV/La3JM
SRQ0s8Wi42ZeVC2eWGiZYWImaJwU7reW8hgMsBxhr1fR8WnXEiy5FiDvB5Iog+GrlI1UJhm6DNeB
HemkFaNfIplaR3UbvFOYBr5IBFBCbrsXKJNiLDC46TwBUVFNxU0cOp6JzJS4QHBtghqtoK14Vt8S
DdnQ3+yf7adJIwXFb/M+cve/uoEUUFsVjnW9hEBk+mBl70+wsOK4B5EysK/fIDSt1/XOkghCtN4K
MgsqbguCBbxl4q4uoI2myqZduXdE+aGO3EfokNUePLM7RooUa3aX3XuJ9Dpu/rust0GEQ2ewxiGH
m4wMlan0FLzpw3EL49L30JrWyhK6x2pB8CWz5aksPUXCdtf9RzAxNbpzDPTC7lj/V+FmOy9VRXG1
xsFYxYY+p+Ac8WDE1pmVeyd802r2zJiqDpJ1bC3Zx//A59jYJwyz3isNUaRqC+RY0kTUP51BIIOA
cnAJnkp2KD3hKhnge3eO5YUOwPfr0avAIb7B2W+ah/LS4Hvi6gutt4pEIjnqjI+E0HDNn+BPviIH
MKTZnppCWNtEkFuZErTk0cWgeUP1/sD8Ri5//qGubz4AJ+PjzQtvtuEe6m1bW04LWQMT3V7HEXj6
d6lp0cvugnuZI4SdHL+frodlAEDrz+2IwvUPiCjr2qmfbnZHdxvpBc0FMVnRuBSaaZkcuNuVt64a
/T7rD5ZY+o4LWbmZhMeiFy8Ru4ZN8/mSB17LeG4iDhAu0O8rYCMNhogGsfSWvkV/ZTpLdCVZjIZK
/RuA6YxTNNeiXjJ6Ze1cTIuLou5XAOwz0wJ0/E/tTnkFRpIN6PbYC4RXGnRYYuTtuxHVMlvYpWYn
7KiZvZ2T9q6AhlW3MKYFfb4zsKEAZoG3kUEx+Z2Sn82E8ed+qq/fTu4uYL0JQRB3TiBloLwibGvz
QK3CEukLYtY0n49Wb/YFD6oFjrwSY5W5Q7nXaTXv4VBB09bo2ugl1ZkNZ/wSjsT130ocbxC1ko1A
Dqv4mGsKFOzvqoISLZvFeNrMyNGqDUPlgCDH5fhWBzfIQNxM9fnCrtiNU1qHJEfNSFjdvznHReRh
U9QOu9vGMCJPNZOBA+PWEjwyB8oC4xgOKW/wzQQ0rxAeZ8tj7xd+U0EAU9DgPRSxNRC2OpeGuzZJ
lRoLQ9FOsksPWEaEymx5I41l1pAg6hC3cvt7LvI6RkPtrvGfOEqX1NkyVVzZlp/CpYCyaS9duOu5
TQMTUdYf7zfi+TbpdrWb4+lGdh3ys/99m99HCJSGR/315R9BgsJJJJVzimsHfvse8yYlgVIU5eep
ubFjBE8YeIPTlTomgH2VAynHTIGgqiiJhoQmCGUK4MpyNRshfhc/bSiHzJq5KH5vg+ih8/m1y/di
WgBcWMvpDNXuAWUf034LmFhTRRd6XSCloT0RIotc4Pwvolynv4Eb62fSYxYpgueC8bzUsE4Sj08S
dDN23rTqlfh3z6xMwFcY6pZ0l3B/utQiQ32hUsAcdA84y/az9wmk6lwtfFr5+nha0jgZmn6DosBE
7dcARTRvSvRLQ865n2tSL9Vmaxz9dE5oOBZRfaSDmAaoZoUCFfTn8pBUmS9KJPWOULShm3f69VLg
Qx+B0SI7zbZ8aJSa7AXcpg/Bx5z7oYjc8cKcjvg7dKr3Evk4ftZ7opu0Yfl2VlXtnfKMeJ89YjPC
oERKf+B2jCkj8z2jSKZtQtHW83BG2ifUNo1IDzmaprZkPrJ3gxKOpM1HcfMTGw+mRRYyw5RjBm26
VnW1w3jmWnC9uM6j/0S+EsO98E0EaflT7azKcqAKbOiKTPAtxFalalDiGp2f3HJb+O2dFFvuuhER
mFT1BtbiDzTitYDXnhjmC/0OI+7d71YuuQHXNt9AFGdb7VcN3Bq1WS78oSdDC6h2A8qbmlATf2gz
2lTvR8UFm1AWUZvK2LGybxsFAXTLkXVaVm1js1jHvRgXHMSv1mtSoYd116p+rStTupG+CsT1qlUq
QQDQeoAhtrpziyJ3SsBzPmdIzbqmYVzDf7tWxQabCqudDRSlH4/fsBNCRw/gwEF2c9pYmtRKAQ/z
MAggxOPJatk/IQDfuWbBM1aO6sjMXBbdPdR4GqIsatiYPEkg99Dy/47V4UwBTp7RPzkVi3402fax
cUN7CdM3HDoaAtcgEf05YuSd6M7NbqfH4uzr1bFLa6JDCKtp68VOmTPtrkTXiMwpaaGqLJPn6XIs
UwJGh8Nwx+bo11kibXvYbH3h0KjtfHvMs9Hh6DWxV0xXN3sf6mCQQP5eeMe3nW4ZO5GLB3c48cqY
qV1zIx7lbRgCVwXO+HSKIzs4sa9Ndg88HjzN2vv5RlZlw8qyts1JuXBtIJ+Wtsm24Iro5hPwM2Yk
LSsIRQFiWZA8Lr8GO7xMAlZttObCzWpjDykswacFK6MCvrbaHZq8TB+XjYZ7s7epB4iyGUJI1dBI
DUPAu1+8O0p3sooq8ggkeCjP0XJok6NnOvzxmCLQxlLQG5WA+79LR7KxgmXBcGXHby70tIWlYKxD
RpSbV3XGwMbpQnkjUp+hj3eEfHjAPSNhaPC/j0V7ynyPFYkYJIeA4+8hXylfJICcVikTahJyiiTI
A86n6SMRMtOHg2IWa6c8hqcn4JwqmxLyENpHTxbAHQddALPlid5hNICmX8Tgkj4NAW5FI3qx9RD+
YnSRT7Tpez9XFIOiU2nb27iLcp/rIo9afVx3z3cC0QZJ/Qr1Ac2xdYhvtVQGWc4eIma9rUubn7Hz
lM/dAthAJjtoBv9knIjX6rNNQyI+pVgVqC+gG5afWzsrfGYeVYCTHH+7K9tyJqFZsyiPPAzWkpbg
Sf93hxJt3olgKzLBToa43ysxpPY8Bdb0WRCHbu2mJOqWUBXwKahVH2gQuIRHqRVSlxy9SSRYjAiB
/Gy/ToQW05owcQD2MxUzQ6ShQ0GRaRY9fHmOJ1JJczwE7GoXEJVb6o3MqJZoWOwm2MRcpvRzWc/U
KXnpzJi7EEC/x7x7EOtNklbeDKAmgsbhG1Y0HCvqgA5VLghf/0NIQY43FH9yenKRwrjxnF+9fcTQ
YYJE2nxbHB8Fvyiza+47QoDIFqqZvDt5VXxe78w/BTdXsz4Hc6Wy6QJjXgRfChe4RduKLWT8yOXF
151snYLBWXA//BEUEoDxfbjWFyQNJ0xt9HaxcXy4zpmaF3ViJ2r5jonbBESu8jKW7JsDNJKh8ET0
1d+pMWMjOhNF5LGQY79FEJEBwbp2ylDYW5N+zfbucEIUKEPPUVN9DhzXkwQxsMH6hBA/8cRzq/mZ
0jA8OO4/0GBGQ5XnfBINN7Oevdfsx3CpQtguw9yTqd1WYhpSUa8WdpZ528A7QQabMxcyHR9VsBFm
lDqVg7/sdClZf1Ynqv9CML+9BnhB+ypUcF71m2QXhzoPNJotLstArAoYjZuB7KjJrcVitm4AOkX9
oi8Q2x/sUL7wqLs2ymGxeqT1TAgHmG6q+dKyjI5/NNHpBEn3Z071UAbS/grEbcffLvF84SQx5Whu
+0k/9YSYNgxr5sYDwcZCfe3iRTOi3T1+Pfk57NWf3eKXojA+PHIsETbwyqk4ffgNELdPRY/o/OZF
GlaWta7QWEmClu6hycTbmS0BrE9S2+CkOS0x4+akTgpVAkFF0fDnLeXy7nmop8FwfvFPM8IF7j1e
ACQFGk081DsJcobU63UGQz2BFFWftTZI5he9ezWjBO4PYulYBuh6oDPLIwiVo5IIYOD2qvKRaUVp
AtZblXEt7Rt8w/1uNs+/Vg1o8Kpckte//66h1sYvpjTLpIVDE91ub5HsPPfWvhQg3whEYglZqbKq
Ze5JyOI5MSYmwmhUMbSd0/hCitfuztE+5VkzDF/D/HBOQM3hE4MXpuc4tNFwlJPH+7sTe58bpkBX
6hfgtNojQTNQ92my/Xi3i2ISy/NwybkiJQncU0IPe7fh1u+89AXgbevBxaOojtqSA3QIjB8Nb5NO
Y9YEIkF2khOWlczd6MBsGvoc/tKs8r2DxsL8SR4J1ArE8aWlkpTCSSQ256je+/ASV3LKEVLyogDD
7n1s/kz+x7HOX+fTbqP/5VUnp/+OJ9ObA2BC8fHelO0Ge9UiuJmx975QeblbXU3klUC/aBMDNwQ6
Z6ZFeliV3bDF/nMNGwDfSYvzXAW98uCqhnZdRHBGe+exOtWg6KNgesrEc2bsXUHKianhFLBv5oX9
dmA+tFA4IGcDRdxztqbgYetGIUwIlujZhQYN3k5h+r10s3hVBuwMVKbh34aLbR8JEBraVJgnOkje
lSbo2EW8MTJl6BKs7oNKZb+Tfjc9RD7jgJ6h5OGpBmoOcfnv8VV9TUoV7Zga9NZKkntYgRg1z6xA
Yxv/paR37sHXVjSECte9rYZaKrWRiChkzUFoAw9LD7MBF8GookiLe+0ZcY3qw1nOHlnWlHW2IItY
axlH3lYEt5BbLAz6INMvaszrDhpmTTLvFcuneKQrnaSlhm1YFEK5dB3dRc26fjr02X5tbzf/bCDr
BVqthkRplhLkkFhwYJ6yxTBCez6BjtEYi99J4uCD9ThxLhgSTgzcP/0DDH86UZkOPfqUeYvysDiz
6n3VQxiEhT00K/J/Tbg1crqhOf5X/2BEfqBpgZL4UTlfTKmhiE3QxQYRVwLVAG/RZPj7wYdRYMIs
437Co+5+IJGmVHnRNYMKYHMPcIyhvzDGt3L525noHaDJSxiTD+HvGqhJtcdJaDTKVo9l8YRhgZs=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
