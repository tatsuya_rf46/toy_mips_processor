-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_16_1 -prefix
--               design_4_c_shift_ram_16_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
8l6xe9NB/r02q6vN1ocJSncvjwChQ85+xkSH94yFM0gyx987kjL09AbhSbbSFWAmlMlcM+qneUxg
8w1rCDQ9519+W4IjAJ7sHhzqvV1tdi9ylyzP/ffJMuUordeM/tdB0BeV37QdZk4hkY4NY2aamMWR
DsqxtGrR3Hfg2rsIfwT3sEV4Jfa+eG+cXhpUaiA4VAF7LDK1IjqO7NnVjrfbrAciF/5/rIzFTZ9Q
hFcRZ0MwZitsAis+l0r1vjNiibKadw73A6l/E2yXb0/b6YkTIkUyIO7OyfCJ14o7Feb5BUdV/bFK
bvGS2IgbR66nUPGFEkxJaL1yF0M9GytpN4czXHJ66z9ZGWX0wMOftLlfo52j7NX3vqM/7PZZ+uW3
ivcdIsTCGIUWMdRdDIYyyYdngpmndzB/Wv0Ps/Uqk5ae2pFhyqY8GoO3UpbXxME3ya7X9MBdUsOE
aIvMVdeeyxIXEtuYlazTNzDKKwlC5YGQeg7WO7hftnZIpSl+XukklHEKVdOQbkAxJb3ciOH1zN7I
/Ql6Tpl3pgsMj/KDAW8LS1yUIJMm3amgClCFZTK+FxLzYC4L+weE6A2HN99KMSYWCEABiB8qCMf4
fc4iJSZ/F4kX889DjQqWXJ+R+VkQ8Fognf+KTpQ1d8fOgsjuZQCPC7tRTGG0L38n+HlH8EVfF1//
tx3szy6p2mwWphKhNjFa1pse073svVePYPysd3eiBBJi39UGjsW/pB4PjTtBRfoIZmjOD0W5j7lk
GcB03h+p2jEoasQM7AkiTw74Wqwiq2A5bFMfcXtIM+iyoDsoQktWxunogw2/EHhCPxP7Koo3na22
MH6G/RnwI66XFvHIdyqxiZvdoOtgAbSI7lZjlQPjz5w938XDS/JhdCgYUhmeVi8MzVhfV/ln1n7G
NzgYMN7cLtx2dbhXRGdWlIlzdR8KHZcClTgSVFLCBH+++dRpUIGioVC540EaTDTzPmf858kfgY3y
9/C2Tcu8MbZD1pFFSiSaM4qU8eKrSxOo+Pgy0khvQj5dn5RBqZKagvzcO22A15mqUGTBsY9XOusA
izI9kIOSZoxz1pcC6bdFwbzByBOeAHKgKTYP8j369aIkaaAp/lQzwWcq9KJj1seSyhjw93r9xKvx
7xP0xfMjQJDUS4F1jnT4bNRxa42c+Nes7wcgR+yT+M/BmUYTH3y0xac3U7I+W2JdmPHwWksO4Trp
uHLsYmIhvDZmwsMZG9chACs4WmNKkP+gwBxJUKOYjBYxwIk0xt8EcfPMkOSMFSmuCl5+VlDslXJX
c7WnvliJ+Ta59IfkhR2YoKsPqoTXVrCbXfQiEWdkwWL5AG18z/ZOjyMIGo5/WbqUfoUhkKhA8hfD
zvqGwrbWkNaJfWFyxpg1PKUmGCoSy7H12yKYtMEz1HxgueIMrrfvj2slic/ml+ZrcpghKSar5Q1y
4ghdeKD3g8lTVHaAE/zm/tdcLmqZjhIeefSDSa0Ws0gBiPWDIvarnuoItwyHov+2kwOzgN+98Bpu
c2Et0v69abF36kabxkpfmL+ETsdXTsrC/+NlputGJyKHKuzjJyWOfjT8N5/DMeiww3tYWmo0kR7J
uDIkJlWbt6CNbGj7EgXuL9MGlzBYulnGY0HM4a6xaBZanBlfe5E9pKx+aa39wyMHaQPpn0lsLZUE
KLh+N//RNuGY/6YLEXR0L3FGXtULd9AWhqQhnegx/mIa745O+Uj2JgP6E7/Dsb+rc/TlF4bM6hXq
iV3yw82fFWb5OPuH2whEXkkS98eSgGynuyLliyLPJY2p/dyVoRxQAaTiIOtVsLEYuAN3PUzTRGvE
yvJc1LgCeeSKwAZMWLwKZV/1IBcqk5f1Kklvse7CgJ8JG0zvRB/0RoMygHe8XkwulUpRqkNTdAvR
PG/5QmAMkCRvDJDocWoL27y2fh3DVC/lzV7nTxv3OYcXHZXejelitMnm78bP9oXqyh/rBghc33pw
eZHLz8k9SZgYbGAI4/bONdZYU2Jh1FDfnf5IQJXxABpQoQ3Gbe+83BRC0c2TERtFwhKsKlLoWXHj
uuC8WDCByRFgStueIg+9o0p8mM27lNUiInNoJ5/aGXnpFmIfycUM5YXGw1wVpC9lHs4G2qiQC1xx
mVSwYWeJFAIzbwjwXUcT8rZy7gB5TV0/fT8blkMZzQJHad1xiEgaziYJsyqumZI2PNPhsMnykX4n
8jDU57BtbaZTSPsJcyhzYVilo0Vb1TCHmBxPwBr+D8uc9DNF7kIQ95CRXXoWXhXA27NOYwQwxF9G
WG791ZBbhRLMqJrgdb1m6YadSQVckyJeVmAzCuWPoLYGRVkyPgJXZ3S6qapaoHG+ennkRN94SjkN
I+If1cT3CCCUfGIuEDKfmRYzrHYBSkLMXqc3Z6mX5ds6he6k2hQ1hUYNllUndaO89dPbnkK7Ju41
p9zWXrIZSm+atO3sBpi5i7gi6qz9L5at4HwP5r2QjsBmsKtO6u7hhTMd307oU3f8XpNJJppBxyFm
Iq6CT0x6c0nLw52WRslJz4+fTMzCMKLHuxsFEYY4Y+1uLigsqegsrTHlOhEcaWCunuxorjssbjer
melcRLtxZFB5lFrxJgA/nK6ZXfKA6C3CmDxyapEmmxbBLjvNSVn/KivfStAIXD04D2zgjczgTVLi
SZkL2VW9gAj7/qUnBuRfD1xx1S8Z1v7H5pPea5w3/VmnKTVQY0MO6rzNw/y+LGTxi26RMW7ou9U5
PJN+u38gHG0y4JtUDSUYjdjQfCljUs/gVRdDXLzN+FrEz4kOGKNp6ag86Cy0p16/FUmZ8SSbgrke
4olM/KLreqxrsmNwEzkFukeqyF9Oc0nuta9XH3frID9jC3plTIXW4g3RWTEh5fa4BkjOWxnLNF9O
jBItm71b2WLrJDRpTSIjUxyZh9A/bl5Bb5j3XCQuLEawCtcbTEwa/uNYaNcKLg4C8J8VJfSLgY+K
83IrjcK74iQuOuBjpN4EHZHhpaDIejBQwRxMOyv7SBFh/fwbjwl1X2LIzS+s44SxvxEF3ygcvXJV
643ekAYfQIxaYchKzAvTPP++vR2RllCXms0yiTM7b7ryAjIV0Xv16mAV2DZB2+f+SavZ7oCvPzvJ
BIBT6/tsXaxZsJKNK5KZ922DkHGVkj7J/UoB5/Ch1S+VdroR5ZSCRCpjURWa+wH6T3AOx65qB63e
7Pp1/cRnyjPReRaycATR1x/9tzMgez5LuKXG/wtVNMi01lNxqfZ3bHDzMUnJXgN0C3XXiy75/Z5f
XFpzOuz8vp1af2szZSSMelOGWpD98A/tWIGyjY3eMn6ga9BuD5mL53h9gq3Sa0Icfb/qw77UwwPI
xd2IXIAl3cgHdG72wlORcIgvUhjRM7yW1/hrmaL1LfyDi2UGO24Snqb28UUqAlc6EBXx5bI9oI/E
GK8QhdzTqVQU++G11xwZ+CxFMVMN6AqwiuMNnl3I2tlYp6jMDQYGhJKBWjG6wy5WTg7rd+Y11YU9
OFIJpIFLujhcqoLWCp2JLKIscQoClUnluip9RGtZi7YNDx6HV75qsoSwmSsrgV7sQG5+L41Ohk1E
3zBNyVVYRPcHh+tc1u6UbMQkSgPqNjWdZx5hnKvWmpSyX4CIdCYPtc6yIO7HzF1kp745jFcE0RDF
M63qkCLh9DIvcmyrR8PQXJgxbVND8xHcfgBJ4WlxkO8kYk0QiPhqPfXiPDl4+F2WS8PrBcjRC9t8
GuAX/C+ol90fIMn0jeq5mVKiTQE0obOscWFFjOZXHnYmN/ekruWoLWqFsmsXvWeGdAqDTNf/jBB5
5chdbq4YdavqSprjOk1Ro0TWQOhJT0krA2p0/JNiiWkujwsMAG4NR7SZX17zh47t37nUR3Wsg6JA
PPLAr0ZABAKkfexBW46aOO2xHJVjvFw1sZfoPuU/0vx8dxDrewFonKMGxakubcs2WxfgoAOb7CJO
qYqrjXMZ6Wxe1MQz1UQYIjYiu/s7o6It/3SdcUrtU5+jGrHygGDLG6x2bGflbUvcojS/h1LMVggS
NmIqKd9BfCoBE3IIiH+VBCoLcdiAM/AzvJkBGs6c7j+FxHRuXlI1DIJcLk3Hx8oIkE6oUERYJCKj
ifUX49xNnb1tCUYrEp0Z6o94j7NVxXW1QBbxAPvAdHUQ4S1X0Zw24wDGcsTxExXRQz9N+GTVf9Xh
F8VEwXWNMiPvZ5jm7EQoh0AiWHRYDN3F1R6+elyJlZKNUCM9r98g8jFyRj0qkN3qaD/0VBNxRGqR
95eNjqA5nOORYWY0TzlAQMusomekzNncwIuuWK3GB4sGj4taEAb3nloUZ3tGunJLH4pbzWenjuTL
HwDUBmlbDVc3+RDnLMsPHpefjjQ3yvwzWE3cX2vwu2B2P1zL0nA7yDmGhZQTg9qcFHF5CvRrK+8m
iRmuYvLgMCPN3HYw5+LySYTMpBtQSJDnhSlyRY43C16tnS95KpLZb564DR4GtwR60jNVUW+KELcA
Cxz/0z0TOfc8dMoX7cS2xm5lLwOmk6czAQ3IlpLwjXV48wS+P3PQIDa8ZMt4Eozkk7KddUdW0RDh
FFXmMaea0QH3Z1z/kT4D21ReMEwYXjCXN5AOl0dCNiOUt0J/+BmxCHzXIAlOj4qGOEVwCsG5FYkK
DwRhr1SnSAtikf3JxSc04VtAMx3fg8YO0RdHgMz3Obu6/9ksXCigWXp8RoBZquzIBv3Zo+3FNCmm
FwCUrtISTJGgFzXNE6577FtL+Ue3wR/n0Ls1ylv0VGv1uZIjWC2PWkKFQKhrM/1r4Lt0NwlJK781
hWTfx/+2TF5QFm+oeMdZx2LqTBRliqyLr3GG37a+FcZ60174lzZZnGWsBN4ffiYFK0eRHB3/wDrt
nLUpivJlA+zRe8I6QmTpImav9F34AMvAqU0dAgy0qOSCZhvQ9s4BbaeHnhcPilb/+mvtHFKzNT0K
X+AQvjufHIHZuqT33Ho4lPYf89CAdTcGtiig2wQ/3DG0z2DMkuuvMnosICRUCoPBs98/OzgOEW4t
mLOGi7/G0dK1SZorTPKe7S1//bWb8fP9Xf2fdRMN6Pd442I/wHcMgft95vEGiR0bGJp2cE5neGsh
8qIa7OBFtlWqqGwvFhdjkdG+S2eOkkTgh4aWk6eAfiQTEAKbdOLKKHQjKcOxIuVizztUMDUEA8PG
zh7ub5MUvenwQQR1ynVAw9GpgUSg9X8HGBji+QyFcY5DqaENylsDg7Be0+kuy3iDeqzvWifn16BZ
9mVIazi0BOl3xmJbmg0masUCVoTEiYn9mOvXIEubXIJpwoL1O/fYvcQLeEBFVEPBzkFzjzQLccoW
pv2xjiYE8YvSFIPetbNY7V3HkOJo06wKhmWDr0UVgEdgydbBj/LaBNOzdEU1BK+/kMUE0QhRF6sc
j/fPUGCkVUTZ35wAmUy0yPGXSuLrLpFNf/mHeX7mAuFxNgkBUybvGz6eMB7rKs2cCf+XD1OhuMcS
4ZiBJJxIVwFto/tIkCVm3uQMfKYBo3c09rDXycNDsif6H8M+OdRmUQin0A2+FTvKMXZqhzTNYCz5
VNUxeXBdiKy54aq4m0VudMGQcY7cWKeB1tQUYeSAmmmHN2dfAsWAibbOcOmWGr/PKvKuzN5M0zK6
kiYqc2iMm+/9XASNq34DrnfWxqzV4JUTZXW/q837giaGr6TQ7z6Ol/hHwY4BbdvMKr3kQb9K/IJC
IAhe9d9FObbLddrIue1j0wCjP5YD/lZd4DQWtC23vpA1lZuV1ucLEQqmlXEYx7hd/qjF/2gpY44s
S3Jm8irWboLBxXG93u5rPMsH4cKx2vyg7PFz46p5aarmCJ71Tp3B8AXCrf58HbML0cnNNqpY3e88
bqQzNWJc2VrAO3q5XvnuKxveHUuvEa5jvcW99cUgRmZO7w5GnqGOkhmotovZnfhtZsom0nyvrDY4
fCy7SAhGsx8mLGLrSsRnx0fTIEyHwTR6aR31uFh8RBPAMOOyyUH5e8vOOwbeG91gVju/GHPab8Oo
xGnH1fSkqnggaTF4rRPZiJ2dh905Jib3og2Mw6O4/ffZxktn8IVKhYAvuxt5qBwlsUiYAB+qaG6O
7omY4bTvFfa13V5UFEMhfFm3OU61rQlYhmaLSY0S1w9KqDSnCwGecJaY9uztPS8/M5dXSN6h/GPg
oHv9J4STEBUSyMbycRy71au8nmCy/0fuObxbLv1CQsmTRBDR86+MtR577/q6YkKYxTKA+uac2zLz
0LGfJgv3ooEfr2a3HJJMkzz+lbKuyRML0jmu1NTs2Lph0NswTBkF1F2sbBIO7ZG/evqE0G/uML64
4hZ/764flK7dPccZIryJCpScleU+RIFV54fYaAEcrs4sTNQUS8oSMmjhrSWa7eUtKrDD33GpVVQ7
XRa/zMs8r7cwXTP6Ev4fa5y78Eg1zNaJNlkgD5uz/TaM0HTErtA4AZO26JD3rdNilOSXn2BzfxJb
jOhHJbUCrm28Aq1py3PoE9tDpAriZMZI7AxyE7wguc95ONxpo2Zg6ceoKrgZIgdmdcyD4k8ZuSnY
Rw9Ts0DAmH9EPqr4aQ15szADwz4faBL3Q+yFhAOzoW97OaDfqH/oNKGIU+EZlqDj8eq/K29GnuDZ
QZ7beU4YWeAxWXgkv4aQBO5ESnQfvThGNLbEGvGdvXd9ji+nCTSVIQfg2U0ka/TDsQZj/h3FF/UE
smFZXj+00U93/t5MpvUeNKsFwwoJHbbUIuMnhr879pNzWPghK8Q4NVejY+lT24V2ulXxQgt/D3+o
tQ4Q5ZSwmK7yjwVD0xRCJD8xj4dhoCkrIvSNcwNGLhj/2OQpX2qEICh2OTFlEPwanGPbDS0C6N3y
z4xFWWFJ8HgOGuBoZv1hhj5+oRECbflGQragAAm8B1jLj/biwuhPFWhnfzAMkYjq/ciloerzWoVq
/L2F7gIGsZ9tAyecXFwG1Yt9jmqcFNPm0xU5pfqeO8J6cq4946OwRvvzouDlUNc31OmWn/oQCwLr
b0tSkxuYrvgAMDUj305kZUiheAKiwWVLfSFWdtHbISGj6RAI6xV0IcGurNxlRICDd3Iy3d5Vog9l
reRDjdzXFF2Cj3aZhQO14lD8F9XcMPtmaNy7Isi29PVGPr3oB+6B52x3WWFSxkqUvUlMhvPTQ2my
fCOMQ8UDy35cm6hnGpQUUslzA8YIAEBilxygZhyBD4ngl3UbucLXrQ4icYOfbmSCSnqvbaMgNe3m
/lFieJU4lWK5KkUVXwqGtHN+KLsYc/dYmc+qUs24jcJkxljxNGfUeHyIAO0y+rXUcGj58Px+uC/e
SHL6lMvzR1vcC82/EJq9Oe4WcitKoTlodtELNt2hhl8HyzqIf1LkZGBlWRVrA6VXz+IedUsTNJ6S
MG7hb3dBvNm6digcAM6L28trCp8rm4QTAJcXWn1cKYJmDOGrtliSt4l7Atd6k+86Erk9KewOa0aE
l5lMqLwwH+xxN3D42DJNBVf9oLI1kyrBLDVYY8Adf5GrKRLtM3dTNPqJgxwiwIqbF64nomASr1XB
R5DoeEX8Ge5Dj7eAu5KpRimWA1mugmj42orM3g962lW071iEaGQZP7PYSdeDSUePvrMqqAxAXBU2
zA/VXa4akQfxuGjqG+MaJ9xJSOO+qqtBWtqSiRVAgtcwxhkYbqawOqsVdUT6hsebpHgVnT1+D1Xt
ZIiRHr+8ToseTJ06TC5rgPEOoL5mrIqQe0oavNyhZdxBzMYhTBcZyf45cozsnRfjbhT3QDGjNKNr
WFzgE3dp7OxOFFbw+bjko6vtatSX6YdyRYo0c3bGegAXBZhdqbtHVUb16irq4ItvJ6AxYwstW5d9
e+Dn4Oi4KQO30CzCe1BYP4TeGZrN/Y9UfCDmeofGC9J2UZqR7saZ4EOeabO5AHmzZaJ80Tjo4oVX
eHb7gnctqNTQjNrU0znKsB9ZaSX0O62fJm+jAoZIuOpMEzhvy6xIv8dnXbcioRBTZxz5gcC6tgmB
ZOIJGAYYdb+8MaTsOPxh8XSRTsKmUFdNV164bkssGXqIxow584NL47mHSE+XS5kijmuT96N63bq4
OdEBMXZ/1WxgVUPn9VaF+f4ekgNWjr1Lj25ANpAzsuCdqdog4umu69TzU/NXfOONIwEkDSuAE9Wo
fnYk/cOzU2mIj4Mu4Eks/gIkoz1bMCSi/M4Ck9AwjXm2lrg5IHCHwcVdddBWtIB5SMMnd+TBKqpz
HNxiNaKPbVduQLtEVDHMqh5SGHoAhw83GdN5/GYDHvTrJQ0OvKoOAABTJ5pdwh1RcwtkqWGxOjcX
NVGnlYCZbMVzCoKX/GaMUYbUTDQii98ly8rlWNKuNnvg+fADO7yuZpVbauykqtD5Z1zTyYlBs4WQ
FxK0ZOrNH1iI5pJY6f+ifgyVXhMb3poYHAQWWimU43PbH5+1aIzBugvoy2vWU2Jycq2cfRgT8GVZ
/oZFhKFVgY0UST+ShUCc/C8HrouIv3co7T4QIJjkLzS35Y7AvJVbcPGpC8aINehS3nPgahA6xKf8
qgABgUkfwyl1DlvqfLd5igecBK7A3f3GhUOkBjUz7cMjB+KuCn1/dy4nSkzpyGn3EvHvD0Zvaa47
Ej+u+bBlAN/FFbYtlbjZ9fSSLco4AbSHuVMYgPjSRgxhpJ5IY90ccUSBF0oa5Oe13nDjMAmmQoFP
J+nH0Nv5q1dDMOwptBW22JVBKi4mIcikyGwPfJ3UH7x7VMwhl57GncByDHsvi0i2C39YlbnalhfE
eoKccIShfnonaq8TOFc1jWWP3RsOWTLkqHSy68uD8C24lcdPQNikRtQjPkKXVlMLoe+wl0Vnp0Pp
+WiuERG3aT3PVQ7gGKg11TQILR1QiQm6Ukd0gH0D7ZH5DXlCVn8+lZ8+n1y8LLnbM6J+PfOQfDVl
AjfuPrZBekk1pt7IDKP+ewhYUEm2QY1KZZCoPffWymME40OLndsgcBK/5RS2WuTy4j9FH/6S7G8L
gBP0sy8yrpozlItAefTNYwKJle2WZt+vHQXY0n4ObMf/cSAi1oL7f8gTcM5T1tY4IvbpN0gvQtUA
eIP7BgExF1aT/K/E7IPU9lpyN2HepX5QJ9ch0vfIJiZ7AUfQ/pOo/o+tRzuODObaJpMowtYJFclv
S1GgL1CKPHhUczcK8nNhMh9VRJC/DjWTEteYsP0o4xjiQEimyKSHvYv75UlGPmk+71CACg3tRouI
7uXdaxe2ls4o6yp62UFfYezoL3VCcILaUvz6UBxkFOfefdKLOiPYJlgN9HIRvW5vGOlcpoEevVCD
ukQ9kNy5KRuXvBipnslzRO/XwrbmJgSiJr7kdTEHovHcXDlEjPmSM2VP9keZ9eoqMaLZjmXFzMPF
azJ0np1k+adpo/ISBKk/n2/0+yOBLSO4a/zmHDlmIBp1KpIftmS/nWvP7KL8TgmxYHnZl2Uu0AZp
z5kSPb+wG2M2YcpeVPlSqwI0OfSEhgF7uh0NBNCLstv87kHw1IzLBeNqe2iHCEpbop/Qs0153WET
XRVrXVQkjmPX2Q8fwlQog+4u4HTsaTxMmgj8+mJ+kIM4JTX8q0SoGHp1xB54ypI3vZ8LQum4hINR
bCyF7FwP0cf/1WFXfhMaGrT3eB4ztrHk6+rTEeaNq0KTs3mQE9VanEHfhkrjq3ApyIsFWtAma6Y7
89n5tYwZTpvypzVXyRJ3j2quaMexOEOltg0pyYWxE1XL111yy1PiwVqUGlPFjy1EJTm+YsLI2Sag
yWDNe18k4F/4iC4mBxgV8mYlNrQrFPmwUpzvbNcZ0NeINOvPavk0M9xGPmJaaj/NAZ9PDuz9qcGC
WbKO8wJVTvlBZkZxoA7aT3Ul41K6FrEabROOX4CWUqi06mUmxETk6Kg7Kls+tDlHeuwgV8UwDDYD
mJb7JKEeV5dJzeHzRNuY1MfyJ6+3dnvixd3BXj2RLFY5vPw1oP8Sy9qpXeCQ+qWIsEiULEeVQ09Q
BBgMjhTfj5miZMaD4+HYhOpTPANHLW8AsUXeh5sUT8Qdk3Zy+Zrds/SMonaeGKzGE0dEvMEh7Gga
qiDXHwY0bpMFspsnYr8NZDcvqssZVQyJAL4YeEkK18VlPMAGBbLlx9EUlmPDc/ht7LVSpNtjYm4O
RhHs5RpLXiBGPjciew0iy3nctYAu9jffHGmGcVdZdNIPH6IviqyIWHiyyYyEFDvTL5YaBzYjZ9FA
HB3+KrjEGskJBPfj7J9lkPiCnADGTQRt9bGmcKcrDTEjY5LKIDfPt69M6OMVH4fGBZ6aCgJ4wrkO
vwym+isaadMYduNjuaBTYd1L0duql2UJGGIrDMO50nZiPWqu7VqLuSPsbwBXBTvQql5cHY+imJE+
QUnhFY/4eTKpwhU6O1f9ZBhOyFpL5p8Od3lwtoI4Wk99qy0+D9ZYnekHxAoxXmdHCqNzV2/kLVJH
MF2tX1qv7kY3+HLwG39zVlRVIWK9iAOi6G8tvrZKcgPB/U7zANPwOIg6Msokwu6W99u7LE1eU9iE
f+3NAdtkuAT5Ox6vbaB69B1vbwA0TDWBgG8Rp553EU6GXYEb91g2PgrZDvwb2Y2RUR7KNnABBQZo
IsLY4Qd4dMAjcBl/Z6Vbhw47ihEC9o9KXT4Jf7vjOLvy3QOSSeK5W7G+wlmiJa5BOFzR9LiFKKb0
gIGsVQVt0mXo3lXLG+XAD4r7xvLElS6dIewZF+hEPKofcA2Cqswc83TTG+puuTgXXq394XhnvJqO
KEw0mOXbdFA0C9FCVDdwYEiEBp/dqW5hqA5wC6lgVyulzJYBDCkjtquZm3//x1w408zP7hu+w1eT
YhW8ot0OouiyCV8qUmPsUHiY/LhvL0pKmU7Cz5Mid+AIBNNHSfkfHX1rCmMpIoz80wnwOQWRgBx/
9CJ0dDLrwMVztVbwiYDJbd1ysfylzJmFL8V1CExNi6WZU7w5QoK1/4me8nvkmBhptUXr9tHxV8/6
sMiWemnY1D9PaM9oNx8DFWY/rurxzxAHxDcZmWGjgYJdDpXOwYvXZfj6Mbcsb3A+lkEj4ZcnSoFO
yGdB8ZXXHupncfuoVb7CRZ4TCbFGXcKPQjVXqFX6YECxsKj/zrdfcNhh5b9CwxCx2c+LkS06w/x2
7qrdQKm1+VpvuPb7Y8HutBaD3lJsctxjbDtYv8T5CYmSKz52F4xVdJSeVp06DvMgzVqzVhhyJH3F
A5dYz8ovUhNJsjzrefpY2piSC46kZuZU/G+ruKRWo8+hCmh2esVhUOPmF/4imhIPlNwNE4xNwP9N
nME1a9/MUFLNlC8kUnDFSsCwQXxPMVlcEMu87Uo7/s+sG+wbKEOt6CGfZQhWPjcvNHimNc1AuzM+
7S2jAwJoyiWMqIKC0P24c21d2PpT2VvPNNfLU1INwY8oTjqxeTEPUGgsWNZtkD8aXgqInDLhoPF9
kBVU591LEXIgUEYxNkU+kgOzzbJFaE9Xv+wlIY8qgetm36+6C8kjN9ITqgd+ENGtNXQsV4dPkLoS
M2HaWuMu8urTOBgk0S6k14g+ykyw3XufvQwwFXIuu2i9b9sz8YZg0ShYik/ztzcdtb2Mku0eUJVb
g48qUpKb+IUXiA//r3/hshBSSM7xeZ6avYma3s/CJwlY9WeSWQPY4+Zl+nzzbygHMGfR8KlDW41x
FUy4uwtJDgNQxnfXo7rj9bxwmLOaBb0CcEYpUaicfShKhuymXeRsjfNqaschyXj3WmlFemXr8kOL
5zC01Ln62dRxktXAdzgFhtB7wUbxgOb5fBrbvCM40RvXZ9AFWfm38EWJkOhW9aEpDokMscKhcUX8
yysN9gCYYpGUz153Pf+CJt423qZxh3R0TJXVCOImiO8KUpgjdOEj+2RE3zeCQG/GPj3iE2yP9FMK
YPHke25TWmd0IfeKK93w+jvdIIcgDl+FpljB67sxwzOssK3XskGP4lcZP6KROeQOcuY3FD2C/zv9
EfY8KVAHYyKVydZSXr+827TXh7BEYS0dT4zfPk99+jCsvap77MOhwZ/jXZk9RDxWCBjFHDk3z8Sl
X9hHWKRkjazgmTFQS/3Xi9OKH321XAZ7u65cVsg62T1AWn79yNfC2XaBQ2xsGqvS3cKM7bGrORQk
n8XPNp89QRLmeAem7O0zj1yUFCRIg9LJyCSjxJombxcpCJxjzXhWYRd4Bso6AmOP06YgplufXB9V
VQCXCF3wXQgmx5MTCn1u8nCsFNwUCiO4Lf8vkDv/0Kqu+Ut+dWcaAmaJZq6V5Q2/vnfnkj4EPjXL
pRsX9FL172jqHv7hXRSKAoNk0b1lUiFfiVVCPRUrzFEUUmR1+Er03RLE9HocESW0F3pm9RBzgSpJ
V07/kpS2TFkgSPv0w4ZcbOIq9qVgTnWVN1IL39y11CHbgZoJRxdF+MoG8nmGhoUIwJ9fnhHm0bnH
HuLT5JDnRzgDhf8Ihh6olPh/aaaG4eRcDHUSmone1qQHltyn6OhRL9b7Q2jLGMnUtEngAI4LjenF
rK5i0s1CWW7/QWeJAWs2ClNtnVxJ0xzMgDMmV5SVxLNvs6xQZ4l0vzpsHxzoGZgITZ8uybkdPRln
jzbPdZGEhW3XjWzCf3QUitHSPeJ95y7gSrKIxw9PR1H5n55Vc7deGsQu9+h0NceXJxLxa+TyVEwH
xb3/jcV/afZOnQ72GV5+vSdgdVRA3rnGQQb8pGowhKUnRLFTPvUBv1TbK8YpQ8K7kyPaZaHL2l7/
6JMAkerVtg6winlDwgcljgv/c3M/Ck/XV6hK4f38Z1foLxPyhVm0Q1Rq/NxyA4nMHbc/QnwCR0Pr
zqL+PvSSQlXKrldX8IJvhSIocu9uAUzcJaWEg6UNjadhFFc+vuarKfPtDhr6XWtIQOYeJqiEzayE
ScVQbYvXg8nP/Y1t6sPMcJ8PbBaoX4R1p3pnTxtrHdyqllQdW2GH3Mk+d/u2muqUjai2Fyt/rErh
+sF8CJ4ilX1MCimZk+EerbajDwsskprBS/Kr5ziMLJeRLaHu/QXB97v9kzjAkfJpykyZ8oFOMRso
7N/6PZEzphZkjKotwQ73/At3rDU5wNXQKurkbgSMGpLXNJK/Mz8+qOWsRbzZQSXoPJzbzm4rct+W
6M/Kz/u+OWnKy2DCC3BU6C1HuD0oOuG5Viy5eujoBSPXJVaOrRZYZqfr72bXE9ZTEpa8WmCWFSBF
T/ZRgfYXP3SCdD3YQC3AQOqrBHJP95gzWfG3MxTvV6iXTDyUe9pRAu/DbUF0M2E0rXwbKPqGGlcb
iIAvZR43S5I3qAx+OWkrOA1lwL4Hqu4KgOeIRuOQx2ChNHJ1shZZkHBaELXLYKXz14zW3luckpst
XdAhT2obDxRj7nrOgb31sPfca+CGU/5Jm+EK87BiCnj2dL8lfspqE2D6DCzoEhppUKgZTCcu6HqA
I8wi05AuSaG5mV4Bd/bEl7snVxMiA0AZm/fl+lUDotmRtktNjysDV/JAmPjKeY8QCqUPHUjX68J9
BWicwst+no4a9FOU5kDLJHG1yylqAImW0696erx2Go/ZGmCMB6OaJOgZ1xDYHj/z9+cffoP2I6Ll
LymI+AOt9jcrjULKkUkWBMwfF/xD0Q7D0jlM7eqGqmXi3r44OaBe3C7gzUbL4U0RzLa/MEjHnA0q
K2rZBWSBF4l6nlAakMmYYdeW91eY6PiwwbfbFYwZwB7+PpTyDdeAvkgcG90F+7qOjJXRMyn8qPs5
QXTXdfTLuFg3bzSSNRRjNTaptysfB0UToXxmPpXSlkA42h8GjYZpOqFEUp+3afg8PG8cR82jrpED
YLiNrm7tP46BKnyOGbCIrVhL/PvkNK/uzCpqxhvUfr5uVxxPTgeXQ76Xt3stm3Z8mnGLH4vJGk1Y
hKaH8UCtxbFqmDDuS44nUJ0Sb8iHi3Eai9wDxXohG/Mm5X/1inG+gxYXbDHX4MadNN5yo8mmxFWr
jLvjik/y8ZSUGNnXVps0avCD/++NRzVYMc3pYVT3O7yOaU95toJwoyEbfMo98Cyv7dIGBSfplbZx
/dsw1SmZG6wL0mBMlEu8vxFWggAzT9R4mdsMSlEDSPwxC3CeT071HGGM4Vxj3JaOuIdGlrr7B9yz
Y8VFSFWA+epTUgoPPSL1hAukX8bXcwMly/jzpPzwHr5K4Mwck2lag2aCQBA+HJ9TVnvhdsD48fWj
USwUIYvGzwWHg9OWVtlkcWWhTpHSt5tI62Z+WaV0nSWbzAtSO7/LZaY42vXiNB7C+SuvfkUcokNR
ZeY1mQC3DmogV+6K4ZuCopInP0a8SNeTYc3JzuIdw2rgPdIFNS5F6P3GQHDRubudrg3waGWOxvfh
6mEivTLb73hKAdAXSEgL8+P4EbH2zEv40IXca6hpnXS3oBh+Agr9YpYu4isWjRVXKm+w9mWhOKzi
62yCCpppgZhK/+7rEIQ9qtOJ3PBFalv4wh8OC1FaW3D7dYvYE6z4DGqJJWIn2N1+JNUnzZy3LqVc
LaLriKOgoa2dtJfApxfzMkHXUuOfYmPsgYik74lVJMpP0mlAk8JMpfnYGOr4f4PkyDpL/OQaNeO3
9T99ZaHg6zCrpOn9HpqDgIqLZuzvl/Ep2uw2RhrvtawXjv2wsLqFtB7OIgbV49JkpLPFEHWLPY9h
viVdvOyrUlP4R5fuuMFQTTGxfGS9++6CBWdAB+EJhSvnMDtvvL+vYFWPLiP1DMuAqXBF28u5JhAg
acoQXFhGe2dePYieG/M0w2/q7LW2H0cS08p/UGs/QgJfPPVSkJOx2munx+MptET27IMisZuqd3K3
TiPBmqFZKqL7E4AFPBcs0/mXnY2PURhSRMMKv5lL8oZ/Apkqk3xZmeRponAN+4AwmbyaJczcSRd9
RKR57eEkPz4jebtp3wZ16+Qhvn9YNKvUBroyqyo2Mkt/qZ52y3qb4DMN8Gs3iFtpswdGZF9MXCMA
kU2rrZv8kPzForN1s2yFXzmUACpCJer9KH0UhYBVzzqgb+5zinafUVjFlauX6p4qfclzuiB6JXkO
smyc7HA7NLXQLC5aTpZjdPt44Z3EKKYQRdBdc/hur10KZGh1GXTzzKuu+M1glwYLDLWsOxSabgH2
poB9ebyiXf3Xj+mUw25VCOeqRCDfI7x8j779X88jlwrB5WBXb4m8eQrN6qFS2rEmvnxXQtg7mqF3
49fEayzhY1hpC+dRzAmBv/D/GVB4ujJ5OSQCJ8Jb/FYAWj6RcPfemOuWznKzSyom5PFbIcIgsNY9
e/klAPE/TJdjOrGUioq/vOL24V2O322JQIyWEkFi7u758OnChzQiCIaIfii/O+ikok9uxdSdnGxp
MiY1fAbw1t37/5LtDeVoVqMwgAd9vaPpNLU09oBprRML2uE9gkM/m2L410TactHGtk+YGfKm5fSF
L54B0y+MHydjAkSy1HzlyHbkfEYrAf7820Xio3pHuj1ksrjM+wbpJADqdrhMOiyjXRrNCZ/cXyHg
9TmQ80BEx7JEcFNSCP+cc1QKPa2avIRdz4MfPmwG1JY/G2AjIGSh0Ka+YojEcJm1bnKrTrTGvw9T
3nL2fm9vgJZBeRS7l3nL92XZFCarUymJaazkstMTqRB6Wt78hpjaHG023sDaxxWRZq/vrq85mePb
A9nhNHXOrNrvId9Va2EBapv6fzNwqGYhG4UNjWq0Lmtq3vBAVpSGVxpnnsvo+MZLr9dzE8jEhWTF
EA4GM3PH969L1DbyYCjMEZisTuM/eBpPAid8WExZi2aui4etRxqetZ/YF8/y5o3Di1qEl0YMH6Le
fdK/YY/k3dV1QjJLiR4BnkAVNUS3oL4lpNh1DthiSReh2Uu02R7qaDZsurtLQ/hOUafZ+DC1ioWh
tIBlgPC/dg0c1gKrGNveS78lUFc5wy64DrzlpMjOc/uIcCvtMk5ENnG91AbgIedVX6y/KzsUgvPR
LDb73eziXZVE0sA+cnR9/bN7UZY/Rkuexu4VARbs5dKbLtAsyGc+WnRZE2jcSg6/WyXPQVxoM97S
a0CNemic2cNqHA/oOUc5C3ZZfoDnrLdw6RHa8HQex+IiEO5+vlPQCv+JI3nGe8t3Pe9RFAZi6yYS
EO2kVWT9NXPbrxKywbdkUZTuoIunDVPCutofVnGgxB7kegwCIe8nJOTlGNiKw1Zh84a1JmqQ5YQd
5vdi8dho8oC6bKVMNokdccReNqdNfQNRVUGKOIWQV8VuqEaVF9KQGLiDc5NcniN8oVxcLWNIJL5Y
g9Ge8HLVcBE+6glWBa7X//VVaPGME10XB5yKopTcGwyzvnLENUOrnR9FevVcFWwbihNbEfUel8oa
wDZpMG7gEfYfJO+awSfpB8VywJZCXhq79cL+9mbFRueaxNbGyrzvtysaUz+FHx/zfKQsZ+XEYzmB
C+NWjdBjv1Qi7q8/BgcPUiqCrhQzI6UOLHJEy94W/QLEL8Gsej65cDcz2QMJ+RXvxIRNTaQeGPt3
giGbBA7hpgW/7vFVkWIzwEjlah2YKtME58QgVQFnp7U4mVrjY8xf7N7pWb7DD7X+Ep+mbsTcs7PG
uX6yzc8o2qiDKIuFpO7a9lJMJwA5e03PJcfOzt6uJgPo76QBBwcKZuYfMTY4Fmj8A13+1KKoK43+
7a8Jlrch4tKVRCkbTCFZD20R0hrh7wg79FY0u1fM/w4981IV2wQNdN/1IGaXRdy8qMn+Ak7YRw+e
L+ddODwbZ1ap8aePVGGZX9/HlTbe313Y7wDFo+kJkHastnErq0XUyEx5O2y9uCV3ZFrAnoYdzMvP
Ztg0RP6o/LFXA7eZq8JY3NY9OccTYoloYR7hodQ25eRwER8w/xT7dhygrWVnwVtLn+daOxzN5AOo
Q0I3s7jegrvI3d5pUAOWKyE6sc0YBaLqsGex2ZUfYPqMCPhhGM/VxQKAXlWIbYCFippqAzGR2PYO
6SFIOR0k5STCGKOikiUR3AjvoDnHReuSdNKdTEFRbrb9uDUd//+hJ3IHokh3iP6mBqu7V0uQ3o0k
1FtySrBJlDWlIS9g/EV4KJeEIxC0A1BnNtlpu7toXxk74MqgVlsjRVaHdI5Jf8+Kob/LYSS9ZIKO
NtkV0lG1O1J5m0LTKzfCXKuSglfugh4FnHZke+Q9/Q1J48Ss+C+/ZlGYdLgyZbdRSQRERLOQyW3J
k4uyubgbaYwcYW/9Tu5Pa4GeCAnaeu1kpS5AzQ96K3Zql0FinvH4UDNUHJ2FrnK8g3eAo689Qg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_16_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_16_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_16_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_16_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_16_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_16_1;

architecture STRUCTURE of design_4_c_shift_ram_16_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_16_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
