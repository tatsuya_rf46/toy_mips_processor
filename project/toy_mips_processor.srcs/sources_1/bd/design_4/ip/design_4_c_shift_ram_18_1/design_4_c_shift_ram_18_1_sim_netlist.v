// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_18_1 -prefix
//               design_4_c_shift_ram_18_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_18_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_18_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_18_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_18_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
NWxG2fwSl01b/6VnYyDPSKVVX1QD+Jc+pKVJ+gIsf0AcnGpj563ywHsXgjyJV43i8QNiLZPGhc0H
wz79bJToPsS3+nf2G7Fudi2MJuA/v5iBWJRey8z/0qGoY/keCW/9ixDTlBrKOBQn0AKX+TqSHKsG
BFaHpU2178FEXz8vgJRcQe0F6DNQdGELIr//EH12Dsx00DKsDlqRf3x4jRSOP7DTKDt7hfMN35OX
+EvuQcYrC4TDubEabD7g8DvHEQmqqAvsUCLbIIw4atY77LB/0h7XYs0cv/UBxTWi/g2b2Q/iC2bE
6MEb1822M8wA7YwBy35aLFPJZ4OxNSNos8bcPE6sZovof8CxnZR7Vm0TXm56GiIYT1oFvucTx4rt
deB9inzt6K98NfQJ/gOyfExLfrxi+PzLcnDXHIWTwjbHBPZym9ykzbMJivqwncTf8DZNsFQWhu4P
p8MmZpg/aBz5ueTts+Kqy5iR++1gEvOIj6kY1joycEXtGaLufiNeqcb08OQNdm41uGgadVm+aSR8
AYff8uwaqyjAaWpe04C1x9IwURnPK0+paXQ5N5YeGfOAJVwVi2hukLkRfNFf/GGcdDUNlyeO9zum
d7+2Ih1usUkwmTroxGulyolm/Ix6jzyA24XXJ+KXgRItT5EV97uItjUHcfZUWnys6Zs4RqXh5ezV
mDuZoRjdFZNxseFTxveHgWcmDjZnnOuDygEpxmisvRW7wi6LgvLPmVxS8dNI5gET+JgYx6fUnVbH
UivDgvj+2/DQz8XNdtsZZ2SL/hPn8me5WFtNPpNzHBWPnHPyt6FUvWXxTqKxodkwPUta6P/xQMdh
wUsMPio44feeJq4snFVkLLnVrqg8viJab893GpP/aKmtyfEnFDj9DcMll4Ipe0p/u5ukiVWbhCYS
O8jXSC6Zz6JVeAzI6BgJFwPXi137mhmmlKKYLLNxihqXxyOJ+gn0MF4NBXlZmIECHw2hByX4a36g
6MOjKPOo1bQxU4bZO+8GLI33xJKtVl2f002nFnWGug8onDYpEdUo9zWqN2bE5LpzUsqhEu8Kevkd
O1GKKXTWXuz15pCBBEh3zdj4JXjDUqE5TXMxhmjUSOSQZZXt5CHxhe8MQnN3l90qNXVq4SCd2q0r
l1oYqMySNSeOuwS1pGCSh5YDMsqPiH9bR96SYH7ErREOmnOnWQyY9K6zYmCEWxIEatBsesjH9FBZ
r+XO4in933NOW97gOD8kLLoCzu4X6KHpaqW9UVxG6hes2ZbciM8qJu/TZQB128MQZEOP5qv7RdqO
nMOQ5XWrM+2Phw/rDrup1EX4dKexhLTjJsfmAydHUBCLOxLrsb6e9eIErB7IAZxOxYNY4TqXT+Go
7TogKX4LyeE1KPXElNnlEPVqIee/RRUF2OD3m52IeknY7e+eSnCweT3iQ9rLe9DdqDuPgkJRJb0y
5eYbucfNUu3d53K84kOY9WROpFuwUnlyXPVY9cj2pb7AdjbgqWuDiFEdXW26DcOStZwEfnu/Kqtm
kZ1U+NcCL/E9gnxoQfPWBfDYfYGThN+my4yjxSlXFNjp5SlreQgdWsgCEtA+DjJ9WvEFCpJJBPFa
3WPU0zPaR59BNd7N76/dh5L/MFEWttTWfrYFRiJ1sfEMybbu9Jqb/168RlAoa1D4Yy3AbPwgTo1+
iP7xiVFB2ub0I/mPeHOH07lL+DYeAGezVFUjir/wWiP9gl5AQr6UCxFMNdhW3G7QSuZnHkOpIB1k
lfrIXd6kjQdqgx3lxiXjANiqkAnjdlEJCDqW1hQuZdTPnVV/w4hYAPV0JBG6GQAPIkgWZYykBxwI
Rw+UDP8FqervmPL+BKRs2QlRsPM/494cU0/gPS1R1/mGbKaNEwYymRD7Sbnj4hVahbfP8OkBnHDP
A3ex8E962e44eiHDMDqNDi9rI9QELQBFEFEGJyg5+ndb8haeAyQzq9Tts7POp7m28tI+xTLGreWe
YqybtEqIKswBO9MvgcFeXBATIIFP9MfehcPyk9Gmfn+kHje/uYQ7jV7WujkK1wk0qm8UcMXhKroQ
/WxOOT/LjOPiMXxJNMfWRFQtk0iAnwmMk0GbAA6kjKoMfa4oRhsivSX2CRKrVVF6k36ts6S1P6AU
+wlkTyPlR7KzdL/nTUQpF2rQ/MsGQ5S6hRcv1YSdV7tty/ifXDPDDD0J7Bh9w3rJ4H+VYmxdokUb
qLwf0Rz5dEHx5TCors7MFbri05l8/SPz4rgrpXwZY32icGv+TkT+s+ym2djbyn2RmSzq4KVJ7800
k5gV8PGeKFtk8B/jWoMrJxQEL5TbQyfBooNzGDJIUoOkFKmEaBgRtsNpcM+mjV9xcXCbkHM3/p5c
+vuN7SdKI4j7/eNtU4U9YjTI+eIwZf2WLSZp95FV0Yk81W71JljZ+7tafylBjjq4v2M4cjkzJ6EE
0UaNXhRY/Xen1BaC73WGtWKlCpQ9E3LBqLxkQ2witNTGbGKxla3XCfmUKgrPJb/Y2go7GzolWhFY
1VYW/tHK2XGGOixgOA8aN39IMKY1O6+Y2qbzVKw35HxY2fZ40L/HDbQ8qcmIOF6uERWqwzpiKyp+
tCaMa0xWchzL2szzF1HjEWVsTEyh3lC8iSsktqqGZI+yLGS1Cs8U3DX3MwCplRkqcSPRA3WV6HDU
b1MAN6/3gH0ZD5qP8aONSP4sV1UEi9I7NeuU+AJwxpYlVGASyeowxxc0jxU1Pd5F7WsTxp14UBSC
deI819N/dHGfajJ8KQcrQFGhOvXfe036j1PAiHUamYymSekvX2sYrpUN6p/om7upeTmbSvV0bs6N
vWlX11bw71zQX1F91V9q0hFLQb9tNDtfxJWBL+nAnqZqikTL2zigvtP2cXi2ZvuFRE88j0VuLnhn
8ZYvDRGQ0GqIG0pApSL8jGmzYhDOWSwehHktxMGp8uT0NZsi7/H/qKiBzmJrAXKEFf4e9XJthuot
XErtoeEj0zM+uGY+c3hO+ImzWy+zM8XTnGNwt5SvHXlzdCxYVmbsV6Rp9hXgPOFBfj0HkCVp6FFn
KhJHxHV5zPh+E6P8MZsfBLyqgQ6sDs4LNPF/6Uy3jd+XtFz2CFGsXXfWf/hvlgZIgF+l/Soj0Nt8
JmNW8DYoidirzbi2b4FslnsXTPGwXw3Z4JPPJs7DvvR0/N/tR/mhbD3SqLaLldNRN/Rzxw9m2gt0
TCLDLk4ec1J2ea3wuCbZDqzugqMJSJq6hClsKzFJyJi3p5zz2gYe+oF73iaV6Da1kr0Zd11tK3cd
noDtvDTh8awjq54RCHZ3/GBBbowwiyQijC0JNgdpLt/prUuiQUnyisn0w8o+XUgzc0sWHrzx/tv9
RtHByykRZdKyZEERXwzxng9aMvZM8keyD96UQzMXcc0rR5sWzY8sUxiYGItBmIBSKDWFjiQXuAmW
o6Qlj230hpB3A3KT5U7sk53IgcgmngQuoUQNh4vUsdCpAvZlJsJUgidZ580j5agpzoORsKJr5Fro
rYZKmBOTy1tnPBE/bRQjNT+EckG+yvdVgddKxVFnt4ChXBZbLxnkKez7OErWtVnzG1tL0BD809QE
l800jNOOMSjhoN6hqBgm+hnqb8NzlW32kFjA3Ole26LMnZP3mTgdBeSkZe6udXBEjS4qzwUyj+pJ
DGnbZ83Eto0ovCOLgrqu8/mm7LkEOYOSoRYLM1FFqcgqWNxRZfxW2+dd2HNF7s/68pgcMx+7+QwI
EltFzDZW61iqsYtMUZ7exnLsHB8UWw+Iim4u+NZ4PshTs2TM4uQjnY2qogA18viP6XM8/450MKFZ
4/QmQ+LmJHyV0cf2zwmeFwPVtfGR9tKQrrvOS3FT6wTPpFW1Vf4SjCcZEm4ur538zDiGqRy5zuUz
QqGy+2VQRe2ePbDWZSoFvCbt6D99VCKV6USfAwYkbaGlK5SsJf3mBecbvt3ibsrwlksfGXgYWeKA
2wgXad8tpQ88l8wRZEX/IF4tR8aIdiYRJeAJLSwj/fZKzjGFby4eu08XJtSUxucB3FuV1k7kzJYf
oP11SCxvoqPYT5ELnx32J8UMxT5HzSZWTniN9s0FnsX+XwuuQ2ul5rgWlIVZZcS8Mk8RdJMD1L+0
1ecw//pBOYyjSkZDNF1aHEYHBh5NkS3p5FlolNcfXWMBuO6D9S8QIFN1qK0SSQqQNWWvcxIesC+1
IYfmkqNMiVwSpUJYTKYm8MOUZz8XQ8pZ0BvM0A4O5eew22DoS6xywCFRQHz2eRHfcvm80MFTeucq
bTmULJ6W9msA432dQNmm8Kfz2PhuTEA6tweOP89yhOZ3dwbTBgr/3VCzuWfAEsHslGwMcWfgaENl
SKDQZj23gOepCTGA/ib1zb2SypbGpeN9hnmpmDj8CER5Q+ScrRq+lJzhIt4LtDoR1blFGVSUPvlP
efCWYZK4ES6KbFhAJUZMiVDZXK6ClDB6EvBOrumHfLpEnhfIhVNYqyebfqLqwJqeLSnmBbJuMmfz
rVVQSeK7TVV+f9feouXU1uMvcAWCqkP8aYFI3L+4YQq/MudrKLFnvvCWKHooqnMIpq6ffQT4ND57
U+8BFbuIp24hyBMCkQ+/TIQVuC2JeXx+cWxzEAYIpLlBv7wWB6CCEJ3pS9cRIVFjJpn3jyYmkFpH
MEpd8KoGEwgBK9MBED0t5NAhOiZdNU8DRphnBIIDLYIwtYrgYCZJzg5+4I6k6lryxHoSMPMrekPT
frf1pb5jjya1tHUk8uxtru1+UMDX2D6Rac1GQW/O19vm+zYf4GJMOmQGMUsG4rO7MtvI62pjkO7v
rjhXIZlIg5tN4xp5Qar9utkZFEzADCUnuTwaWJQDWPHtX5vyxbXVDiUGvwOC6sIDGgdZuLiSgb1V
UwsZvDFxNrL9iDe1+usGoabmuDEdxyLjyT/7xonUIHyQUbGPiHa53uaXvojdUSFb+HEYJzsm6UEF
pGyUK1fxnmhvo94cBMwD4jUX/B6R1Lse3tsDnlzycV9j0ujC+M/Qh32J1hxJzAb4K+NmJzlmFQZ5
R0wz71dQrkChynEEwMZCXFjNVCOW8zeGBaiuYtWFKdtgOk2N5jY6eJFcqgsCDDv2Q0yt0o8qQ5pm
JHkrTIAHN5wTJLtnOPFaEOK3pYQ4kYHIyPY4a/uH2IEE8gr1+MZwqJcY+l1yOPvLDU5DCn5mWVuk
SFc2ae2+pMm5xBLq6kECG6zO0fPzn5q5hYhHvBsa3wavldYwBD3fHFQKGRXqXHr0MuevB0RXteZC
5DikcRnZ8T+IT3Xu0kiNMC0bihfU4mHGK5S9zQFK+DI/YdVdHi9Omi4rZ7opbkLGVR1Qoi9MU3AS
NQwxSemx64VtV35VwiQjm85FPMn7kH4xvw55WpuKO895fjWad1r7iD6WEbwDHZtsdS/vrbbqPpu3
G1B3V7H3R37gKia83lCDhhGjkaQMlsYYwezbul8pI7+oJZ7C+Rdz0leKcSiK89G7za7eqkCRlD9D
VK9k9XWsSF1gqC5Au8ViK8mNmATDc4iMBH/8wvvvjPn1OnagjcOn8z5OqHTzzKTNxyV+5PU6mLnO
GOVzkmWINoKoKRea3SYyoaTdnqOwVyyOIh2uDklFptR7E4v5fr9rG9RQNPv/Rcnp//aLXGAcC9oi
+WrsfeSw/vZxYAojicfxrdGKoBvOKua3n23ybpZk4oQ9l01aJaBffjmJZPBXCzH7t/x59XWUkdfj
Y7M4Pn9fW+n7BD5k5TRRR20MmVnymHufJkRHQ8Rnc1ekXKhv8gKWbs5/rL7HLhxP1SrTtFyMjnEe
Br7lw29qJ3NIfSs4dGInsAKDHLAiuwvvk1U9Elh8Y6Ywh6GqKCBrnWy83kM9mTsgpbYXvNqkmvbx
/y7Tpfg/jPjus1cm9jfEFdrxyeabSqNf8LCY4FHVBUVQSrobuXBr8t1PmBbNzISNKAFdytNChY0W
3L2MQADeXYA4Lz7IQ6Z8ES/Xd0NlVrxw+RYoN0uSk+W3/QrZUynt1mp+RhpZZ2VIReBGACG41dkX
cmVfjZAGQZ2pcWkzJQOw5xSxRedN34wYCPCLz7RUpcsCwZbHZIwvtkABgg4u6ynqWcr9+0hhbYhy
AmPAQvMZoI6ebdbTXi3GE42UtsfkE/hTKgEnnIThscXN/mv1S8oHbEiyTA0Cwv5OwzpcqMRotjeG
KmYTw2cepAr10QFQRpaSFRh55fqpXZZM/exvs7Axrv4v+5WgS1MAL+vw1H9l2jQdg/eGRKcwViqY
nNErg+1i0wFGab505AuVNBU/XFqMTtxVgTXnX/+q0l493L2PAudHBaXWW8svJvfFoCHVfvVHxemY
wjt69SsCV5E3xBLE2IWUQnEUnF6inCaBbF5S2F4E69cB5pVugWhcLJDy+BUWutECPUDFaCi8iTTl
WF3Wyb4QV7PPTfwBtsaqLyW4ra6TDOrdP72pRsS4gbFd/hiXaAolUBFgY6yOKEfPR4Fu0dNXmjBL
+tWpf9+UfreO98hrMyY5db4wCBgxBjxBe0lQCwHvVlHb6dU2YHOBtz8XfwJT1EB2g38wy83fLWQF
NnVOgiPC4F3QjrxpWCHX43RkYLf9CT/oNpXGjUNrXOJCFhhI+dy0VZ1hLuPN0Ug8wuG/gil7rLOV
+XHeiMGZipqWkMQlH/uplE6/fR2sb2/+jTCYb8naPGzWwnsvzIBHFekoNdCBJJ6y/4yetgkZZgtv
1f/rt1rIXhRgppjUf0otY0eG2kStWMwrcT9nzizg9q9VUdPlBhvTWFCeZ14YWZVGSwGvqZfhmfb8
8a/uOX41RLtRqZ3V4YCsC52EgdNIKX+CPn5U+75MYslYmh9g1SEaj78Z1XHII+2xSsnn8noYubx1
KkUtWmOiTcm9b6lk3NEDYPsnZcb8OC1t20fpHqqCNPmMZgV+lBA2ZPliYPDVP08S94jyVQXoWFF7
bAV6sowtYpa6z+3EEa/qhMBNfeywrW9sNRdkQUAJRyr1UDs9OvyVSZ9PE8taUypE54qErfgqPluP
VaO8UsqOe6D4Y24CpF3fKi6YWkni2exVXDD/15ieNreH9/0S3BO/m/IY/AX6h4Xhx9O/0Ok2N3Dy
yjnd9dFQt8m1DInjvo77lj4BhJ7e4EaQceHl7pWfKiIBBKPRza+P3u4aKk2Uef8G/3/JxLFAMC5W
d4AYW9uf1MDb3vvRLnPGHUxD5V8Et6CRxCEMd59TiCLalL5rJNm7qoc/L7ProrWET52xJV7feKRo
yYTENO8s1gL/Dl7Psx5Yf7oFURu1yBLNAt+TfVjUusyXrq8ZmmpzgW7iyoR+0o+Iyub84hnqXo7p
X8WC9JJVYvIRpepO9ZzdNIfyGvlJQF3LHZ1Qja8llEfV5uMH6ZZA0/J7ikhCHnR587DzBj99GsAd
7pbwnAK+YP2pQ7X2VPOyUTLz7s1kI7BfaYZ7UkRKpOpw2oR3weIlY9SI3UV+pciNd+gsHVhnunZJ
Ygq5KlYU9UaYhhFGqZNSrbgK2e0Z77uloQr/ndP14FDnSMXxaUTfesllCNGAieoik9Xe6S+oMYey
u5X6CRbGDh96YRo2sjH8pGXRJzdnXZw000sDDPxc41P1IWHTxWUwBWsT6jhfeq4kH+dQGi8ZIPMR
YGfg2jz6F9Kt7o83S5gBsCJzirJzvKRPksW5iHq3rNnnyhGsYRqKVP4bFycBHhS3LFqSFVEhRajf
j9Jzj485N/InHiorQ6BOsMxoMKuXQSMtZXeUvNISbnSRLYFe9rd4eNn2U/08BUhj7R2UNtx0TkYG
1eMmFMXqZz0AC0IqbWnNevAT/dWJ691CsMRzBLGo3u/Q4PuRS7/zPQixAcdVfIikhXTe0CqZrp1e
bUqXOn9nAl6JRXM5s2mRt1QVhZNy/MTep4rttKkJRPFC+ZR2oiCzsJImSX/6SILlII2P/mwSvpPr
qv/Z2ml5sa1BGRM31YNIrOS/LeP6vMgEIbxxaJUxHbdOpsZddtHCPDdctckP6WR5KXsKplhnTpMx
Xvv98gVfjefSmWpuCQyxlEEiT8pPSIHY1GT5G/Du2izNjUsbZ7phsAjPfXy1T93+FK2USfiy7KKx
ERRvx0QXJnMe9pO1WF7vOcet9fCTOTz3ZuGoC19IAhIYFBIJJIzz/CGQTXGR6Z67/Cu0e6c/H4Ht
WKAG+Qpt0rRt+X8Kos++F6/2BhMDhg64ruuxGp/yO2FvDtR2UdRU2LdPt60YKPkZ1kWzJ+aDGH6F
I+0CWgvX0Vkn58+kYRmqu8Kiq7jVzDtR8fpT6dITj/9GhtX6wHVcow18ELksLTDwzMtSOvGHmgbu
M8qXZ4JeA9wS8eGm6BqJUQ6utuF0F+Nql0MrjY9CcLYoQVb1ViJm1xes1/sW/94WSrzL42irtzp9
taeeatQiiDUQIsTLaljJhTyHdkEkq0uoVSGHlDJybkTsbfkRBYaGm/ehYcum6pyzU88r670gzPXP
uotdknDyGHsgaDQ2LiAkjJGGbp2TKXDdD41wgmLW52jzDAmYagDsC38u/3VfSKfB5Z3OfDJbUN7R
kiHTQKbTkc44VGCH/wE5yDOBLxnu1anYQ31bgRVcFO5lmsQn1vdjTgRYf/a6LI+U8xpJFF3s7QBm
WqmyLYt52UZ/1ZiYc2NTgtuUB6emZzApY+arGHaMF/jWFq6YFUDRvv5yGxGsrz7CqdLnLsTUmPbw
b1NJVBtcmynEkKNn8pM25Arj2HGnnsL7ZkKQn4k4QtWyDeThyqQ5YqAYqcihmm60nNTrdHmuvtrI
25vR9VPoDdYeXVnqDLU5QuXjZn7uObgGgaL+MVzwhO5ABHzWzunn9YCxNwP7Xpjltrjw8Eb5LqLp
TpqY0z4QsdB8aKiyEK9y7oQxlMty3KPGkpL/ox0+exsfNthodU41wV3usU0Ly+RdYQ0HoJnLAekW
IyGJZ1x+3xmHgBcsF+woXVY6wiObxrHnycN54SY/FBhrqTR+sS2AmyblfHtrLbio7cXWShOXchVW
2+m/PgOhvD9eXYAM+zW4sgjG88pT6FAA3qKKAK8b7b124nJhgYjKDwNEnIpFA+jn3IGjMiTbWY/L
P8mucRWI5gm+gx6i2Y0MAlMyzeKpuXIVV0PaIUO9KmcznTwNcKEC7gHGrO1HhxVe0dB23Li0roHy
CwH/1DFoYkHy9b6Z4t1L0Qjq28lg6YlEzUyXKgQXglWzy1dCakuZ0xRh2rCxt+9CyJIoDhy8o2ht
3I6gJ1g6KnPovim8sf/cwBOBxiAMwZknQ19TanC52A1Uq84lsGVjnDm+pVgdJtw0jIYy/TNKKMg6
lHBQ2dadWUTLqGGJGq7d8OZ8UYKAO27KnuDmiP13FYtG8zL2/dLqAl1Bzodv8yUq1lvA+0L/aUXL
d7fgXIgC301S11jMnF1RIVion5AFwgJG4ckGL38UiuY2ZqBJf4ZY28AQOBuhE7v2STRmLPKc0tg+
UF8mVmXnDzVpfsZdVIYB3uwDwOHH9+bsQ/jFc0/LaewWClwmRGxPZKTzz2RQ2KVCGoz9dC6TzeLY
z0WbWgojKVkM/j7k5XDEgwfNRJb1PS7PcAxE/HLrwuRh2RTVRUg4e70MT1G8FR/mWaxzAAQ5ascU
Ab2b087JbKgmu4gLIyVnjurf3b9WtoCn9H6AvrbI3c7T3VK+aPFv3HbWtHVA+WqcVT6DfQAwouBR
lJ/Bx19P2ub3jWtAMzUwzTSaRQ39t2f5ng4ymK5QAmHS7xAIqECEiKpVucQek9xcDxMBPv8FS6Df
RKUultlsgilGyTzS3Fqyzug1VRPAC9luPI3PLi46VpM0ULphQ3fmV2XPk5W4DyJcWQyGrgm413R3
oQ5rPDxhMOmjvJbuKc6owSjmBaA9KVEJzvBejlCYECzw5gffB+bhjub+r0fUBNE+EfiFTdLd1rcD
g5aGuYDBoxm0a9GDXhs3HExjTAIM3dsdx/DAESBgHFMtd5MZN+IYgbs7G655xjaWc2WYA/iZZbHs
ysyhxTjTYNf9j7nEsMLrbS8o4iAGSG8k8IUevQfP4TsxCPLU9eKaOykgHlboySnvCiLhtHdo8SMb
yAx+H+P7XT9RBtEr8a8MKi8ydkh/GPIlju9JDEHqXDJ8/VXR33S5NllmFGH2ffeGQ194yqB7/L4/
4EDPagW4BwGF+4MnPtuHGts30X71DcEh1qYPeSzhr+tLGmIHThBfvl+B2enmilnSdYCWN4fS7464
QgMlK9x/h7fD/QQ0/pdOT6IRf06z3Z5hPyhR8oMOLVh4g6Q8WEbQapIIiCoMZ6XhgTyrL6qkX+K6
ikhkq+qfdc6CmnpMSSzyw1CkaQt7QULoySCG3Ldr1GcgrNmjy6a0rl3RrSoK68yOkOzO3QrRaFbL
CN/6HGTMDgdLW55E6j56FDPDPvhs/7W6WlhGJ4ddN6UhzG2Y/kT2N5TU6XTet5Fg1DCOaRA+4734
w6e7yhqdrQgC9rKaFWzXx4Yma9R72JePjTSMjiseI+K0BaqPmpxnVC/xcmUBvTTb8Ht1+B9Tdi5s
NHyNpxT0lzmEwxzvXuepj1/T0A/9uSGgdox8YllNMmPlypjsf/7wz4/QAmxpBIs+LLd7IiKnwkil
IY2uNyHr8J3R3FmTBckxKc3xBI/epp5Pn6AdX4gyKHcQEwa7F69Bx1kA0RMk14UmEkWy1zOkONvz
y4or88J0Hy6QpSiBocVhfrxHTPD/G6TId9naf43oum/Ns3GV1rjo4K0Rdbz3W7+AolXI8zl2qrCN
dIz994kSWuqwfRtbTv4HP+XuIHUJ1WdhpN9oTVdHFbUdcF06tDVZ+bZnifUBp8Mp5eqktu6Q7XFs
w1bbjed0GmCLEW1ALocyu+7MLBADS/UgVOeF4uZQ73+J+nUAv9turnD0vRkcN20VfmrWgtQcN2Pu
UoQbidQsvC0+qkeQz7zNqcueeO7Aphw0H9q67WjXzU+yHD9xWf0c3LMxOUY0R2seOyV6nqt5COnL
+/Ub5fUyRLh7LFdiTOnTN+BIYWcTSx3y2Gnkgua5SrltGed2rXaJ4xCLCnYE7R1sG4lT4zJt2PbE
KIysaQdUDi+Du69YRNEvwN5n6MwvJtJyS9Q6vmjR2hwNwZMMDW1NOQlW9cMqjxgyJSk+n5Afq+HC
Kkrt3pAAdEI+U2dA5NzahVHri6P5cOdbOfW2w/iptk7AILpPZ8+3k7aKb/nLD8qET5VZLLJhjxki
N+z+aoPUJeJH17IWr7vCnwp34oGp7aVZQN6R1E9DoFQL4uFfNCLRPb3UvcEmQqeBizDtwCD2/uy2
K4a2wRd31pg7pW7q0lDYpnYBJX9UNNtlu3RQpEpnThgYPARTBfDQm+vYeeVt+qKGbH6ScOAAiXZM
JmG/BBGqs4NIUwXN6Nx7p3coS59ucvxSJ6fsOkDScXdVy6VnnIXKU5Ls64c+O7uF5WU12w3csj8j
vkf3E9zAzv9LOdcqNJY6K4V2h/nE78+jqSAhBX3urEb4n1ux8SszkjdF14YLRmEdQEIzOf+Lt+0s
6Udc1ZxdDHO+TQPuSCnHXXI5DMNqakitnDaIqB9edhh5sXqoW4pdZf5CMfUxkmG8o9JrAaWsdes2
hdNtN6CsGcrO4SjmhbSWCplPnMl1FiN6uKFFXC/HqliIlAQHke1hEydVQsRVbrSEiJwwrBOXEm4a
yoBw+U24dcSmZ0Uvc2A8Xq+21Ekr6LDLl6yOe1JGRHqw+pcQ9QaFYLG7ZtrxjWtdBSrMXGBNh5Q4
dDs9qWQyPYyckZEvsFlygP9fLXUujEWzxDkMFFZwnnV5Ye3StAMHX4zP9GyCSkFLwWjVwnUTa1ss
k34YzKEYsGZFo3r8b/7STfwz8cFdfP/zm9b7s6kFdikktuLPl+ciISsspEsSfBQseCGB4qgEBcwY
fA+veRoyhfZlXRVG0A8mC62M9BZCkx1wS1cSszMq3me5kg6bvzv61jMdwrVGmYLXBb1sGi+dIOdu
Zx0FGOOJuIegluWlDoAY9VOFjdOCLMJ1d2AGd+2nTkdABFLdI4B/IFP5s2cD3cve7Gf2sNboODsG
mUQLr4zmTCaV5YioyMZlXW9DXRPNROtdHg6a2KhY/Y5fUKp2g2PAedUW7xjLkzHRGR6/rpZZZvq4
cztuy+fMp3Ulgy5WsQJJGZnT6WL6jQ+rqOJVmMxPxPbGiLugOk+mVQApJsG7jCWrxVTQpt3yA2C6
pw3fkt9CnjmZifB+b2HrmQxnMRuzsWL5NhfgSk3XLBNlMBraw+WXDAvdvB0t/g46IGjQQFZTsM/G
uJSwjKAY33uNfWJEfecBC5du4RsF1EwXa2iH7cLvB+ZeeNPeYug1YZ4MKtRl+mI8pYna8OIQcpBM
mNrqa490bP/YD2bXBB2ifpSOxDFQFpSL4gahxrsaqy2g/tdBObW9ffbmdjI9ci6MhBNQg8maC8YC
rVUG7fYCSeKtCBFEjaXEfb3LHoAicFTjlZsIA8KzLVmBA6WXXBeacjiahGEiA3bPKfMeOdcvqo4i
LFxlBbSaqjomZMpfUIS2Wui5BiEQsjT/aNwySqpuBD/5h2NKdNm70VRe69F/aJHYW0Ty3BwUTlzB
OXIQOk2qHUqcE1w3d1iaNYH69Z3y65okyhXOCKorhXowjpRl89qGq4Rd/8KzDbQl0Nen
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
