// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_1_1 -prefix
//               design_4_c_shift_ram_1_1_ design_3_c_shift_ram_0_1_sim_netlist.v
// Design      : design_3_c_shift_ram_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_1_1
   (D,
    CLK,
    CE,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}, PortType data, PortType.PROP_SRC false" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_1_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "1" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_1_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "1" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_1_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZEvbP3BmpbsEzNzKwXv9FfIdjnyEYErgGV5uZ6/x45t8rDORSuraYS7IefdTdRyF6pufcW9y7Oyv
5XQSBclEYkwRVvIi492OO+8OaLrEq/x2zHIRfWeH3fS81IUwKQxvshTdd7sDOCTKL+L6PIekfIM6
NpTWW4Ne+bmEjS8+gslJvV2MZekDCwimPb8Rn/PXP5arWs3Tea0x60gLUTH5GaLojGiUGzck5IR0
AGvhQgc3wO5lJB3oOtQ6NQvMrAMSkAM34REA2QN0uBXZN5n0V2SLqf58Q4XE9r+aX2Yy7wEx56aC
s6cD/9H8A8HmTMWgUkN+GzbRkjG1CUvKMvu3vA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RkokOkTBcySD/H9LYkaaYC9NrMv0V7F9uD4DLqQXWc/hgKA9ZRWXyhoNH8YjahdxW1lYiWKbPWgw
1tV4fXMlwNi5BtmqUfztHHNKYA236ShmrkKo7cbeEoPiTYVISyslj+RZO1R2ngC7g3PTC5XMiWCh
wDzOTIx7UmycavTRmS1iHCTeHzwuRYXJLOSHmuYDNBRdWP9JmX9KnPIv9Q9WQ0Z1u8ZeLHcHspuu
k+893AiQMEGuX2lJm6Ziz3s3FgxfUf4kxi12+fYt3yv7rHE9n4y7lZxuAFk2jN2Cv4J+GYMch8qa
9gQnbvcgt4aSYXZpcRFn066+wsV16Es7S77rpA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9856)
`pragma protect data_block
feKWQ4FwcjRJ5RYfWwhnKuLRQNbLZ7a83vxvE34q1/4U1i2AhK8oHSXTBK8K2HvUEfy/D+Nxlx7h
KGzUCjCupowhxlmeYehJeeiLKu13G9nKQwPSI8x6h6YvwhUmNFqArlZOS3xl8EpAeJpDVeK++6Xr
e6lCeG690xyg/6+hVj+sK/PXJjOQM8pa93/e2EMHlm6t8a83V8BlHZfpyXz37qqo9/yEptpOuDOw
Ol3aAbKjTxz5rjnKR6aRbVjVgApahyAc5+Bmzzjk6C0sUZjVutJi+0Lvj+ROhI+LOgYfHqhmph+5
5/XTeuvYMO8mShIEK3qDUasQudZphB+i2jd9TIzaWInqxg1TMV5zD0xZH0BP2/LyfckM20zkRfIp
Jp940p6VJ43H7W8HCTTOVaFwdHmCAVn4MGBGnBmK1H6uCExWmR3Rh4i6Ve8ICyY7lvP8XzluQFvr
BcxeGd/gzfxj4lYAZa2Pb9GGzxPVNn7J1dfFpAHDznF0RZTHPtsSSJz0S0nQ4e3tlA7/TkWEL17E
Rc3Bs2yQoEjgaSkw1D92WyEFSCl2sLkRvijvtH6VAZo4JzMHr24ad9Veb12/zVveKyClGbTTJPRt
u9L/qzeSced/35yEVUgJOpinnZzkuJSSjjMGzM3AiIHLrF9sYCmve0h5ZisvzQUHjEIN20wq/Mor
+bBVsbmgUrLJL1oLl1V+LnzkA6MYu5TcQ0XwR21zs6ULE4cGGawIwiViwPpidISA7nL+qynCXBHO
0rUHmW+aYvUiu2bkHHOqdy9whU6XP0TD2qce784SmdMO8BfajynNC6YVdlKX1b4xF4oHl1wxtw5g
Hv+HuAnV+AUrtO6j/tdRFnUydxLr8uPmSBEBNFCtQeRIeFzXv91/lfvJaIAGV6O4aIDGQD+4RRw3
KeAXkZA54cnnox8iKDxgimzffFs8SjKxn1KuO5tuqXD+8JjOKia4RIa87HXv2YqUGeT2NBaa5qO3
YxoMOIz5IvQIoGgqoGRvRwY00gtqv9P8sSBUoRb81HwvfIj/4/2x+mtVUyUt3BCEALKZlGOxMZfx
uzdEFwATEPyEBr53gOb55hsutxmbdYS38X5+QintA/CB3gJ0Mf8EndPo1519ovnNR/VKr7cOv4tx
IIkxpYBHHWiTgpCxkNlBEXvm8yQgGXWz18GIjpJrku6qNXoLw9IzqF+MH93ohPg8acD8CYcxgolo
nnAhFZAhuBUy5SRNilkJtjABCXcPOUMdtDw0HMhfm0Z0R8/KBkHfbF6Mb4o6YpPGP1ifHLs3VkWO
iH3rOxtBsSuIxc07VtzRjPJQhKNpi0a6J72UThXF0/KA4vHRAZKwL0IIPA/thZqfMG2hI8hTYv3h
noOJQjEG1l/+ex0Skw4ZOOJR97B0BSjIfM241M6zhMxYLmloyHxzF4998j0Un/T1qOszbDYqPKfW
vj7Gdp1APF7/9p0maBKdDTxhrxegc9JOTQSeST2xQsn4lgNcjoQODXCEuYQ5GUnfbgOzgLhuaRqS
+FexKRqjiYdlm+yMMqN04cjU1oa+jAzAhWsm4hjQnaJ/DVOWvs7OF/gSZbVDbqM5LFanB3qEF5hB
mDPU+WFPLrsuD2Fnit3oTUq1n+SI9YxACYsCYIEEWibfCeDYSbUd89FCoYEm/w8tafU8mYab917x
9+fTRDrSOFuAz0wB3kj9ixQf5nj4pU/b68oW2RMJTrT0ABWZ4S4ggomnNBe/PBphffIUXHBnZioJ
QEN2L0IUdPxFlDgvQTprHd6yI38dUF+gUsPd60joj7OyZv6XKfpM3Cgk23GQfpaDdESt2XkbN4MY
bl84vtjP3MgaNJa6vW4+0Gdciunci/qI2HIg3bYTtc1wsgFLO0lLi5InoPhmsorJpQLcLERJ0sku
GHKeqXtAMsP/1g8vQZH1Sh1uB9agqWfNWM+E8LeIMYZ/w4VP+w3AomYer602QJwl5GjZ71Bm2zd4
VuvsuvoEcg1/3vGvG8Y0iKGLhT2IlVg94lQhaMXxfxHotJw8Lt9jfJLNt/0/qWm/WeHUz9rwWpS1
ued/VxAgF9RkQ8MtPn4VZr4hg/XXCFagxj1WwV1zGyAdPo3jPmfwi3vUHFGQlvPNMQUyPaPd++Vd
FV7scVSJZzjXqYykiFWKlGUhFY06LB4eF+0PP6XR9qEA5czyp2K9a3X+wipCJq5yHzxcNQDeXQ/R
vZTWQmTzttRhskwnx+h4LHzKkqaeroNs1qnnbVCZ5Sf0e8neVUOe9WIxWrxOEPKM4lMM9NE5Zpmz
yqdaGhCKqo5F2avjmL+5FyeHhUsiGkbLquCUYC1HExZkcP03JMi8xnyx2L/4st8Ar0RAOunqzvEi
+wtfxQpC2d0Pp/+nF1k86BlTBvp01S/6sdQffK1h8WxNHmXXDSFx8Ru415knOqqXAEV14SL5dPtK
+oFi1oX4qxQRlDfXIFuHfLll7D9sJZFnhnP6yqanEP0Gl5edYk5Fb1eRALjfw3GhE9DlRiIiRafq
ojgcfASXzSsv2ARJ8UkO8E/vxmIHLMfF/Nk9EIfhVF7f5TJEuzZURqFHpjcUt6lUHjINu42JYK30
evJTSu6cwYUZ8IFzYjnyhkUKNHcqsQ9usEJL/hKN+f+rSENB1KBc70nVMC0s1mqvHmjtsOR2bDrO
AEtvKNWEt2YmDcS9syZWgkChxQuUX+ovUWxuLUJY/n87rwUuyhs07qquSe+B73XhHYYqd+MgPKN9
ExQd9McTQTaR3gl/KW7G1s9k7ycREk4Vl49T3HGCRW4PRvdkvIEZ55vyfXxNnhqgs7xtli2dCHHL
5gAnKUqgSh6xvdgCHLfbC5/zCckTnlsTeotE5Jd4TBXL/TUcfwvD/pIDLF+Akrg2/ixU5hIcpW+9
VPhyACoLuT3OturaDWHEXi0xv0NeMZ709XhDouP1NxSx0OxRtimixWjxpYE2SBCLkoTsH3iSst/u
46V6wzOY+sp3P9+XS3Z59BaTjic81Xe8TaQpbB32A39qUgGjBdfUJVLdezvgvxJrbVWqGXeGNvcw
p0h5yUb5afzVDJJthSbdbggIQi7anBE3raoivsxwVF71fDQPIVpJ9IUcKK5wNWG1RmuFYl45ywUh
mj2+LQ1VB+LPRpS4dWPvABkTINdHPbquYmLblWN2yDj1HVjIeWXrTtsniE3AGx2Rhrwe2amZh/4C
eHW7/dnD6xaY/RiKd6aVCp+y436l/OmpRnDN+3TWS7bJfVtx5oA0dkHT4K8IJ9Km8V81X6SLVFp9
37NX+28dm3BxPVeo4YDSNycXkpnHSGs7w5LDkEk8+HskFLOltI+N0IgttYOpIJxO7qaSJjPI7Hwm
yMh4uSVUDxG4mtzM9VpGSktUFxygx2poKRddl3YTk6q4nkWlMImcvfx6H04FST7br13ptXImSumJ
lUQioRXhfcvaiuu03CID49kQ6SQP1bFcCZg68VvQqbTjRmDn1U7FUBO/3nbWtOs8IKZ3npIhNfC8
x5Irud5K5Mpb3IjS56tVHmo7aDSAt1HOs5Q//JQqadi5b4sIh1Z/iNa6UnzIPR/TEKH5nEx8cklv
bevLyF+23f6SQl/WHKhnleCNAaaNYZhdX6JKR6+OSLxHOfgSrFOHGiWi/+pFcfauMXHmY6Hoi9q8
CGL7nDapaHuDEQWCMJO3JffS3mSA3DwHAyBjiWc0zOdS5FDe0Vz3DXBbpaBmck7tvq+/Ow3if4hp
msL7ttpyJ7e6NytkaQU3qMb0lIQ6vdnTypQ0LDozaTKmGkwH8uF9CKj7kyHdsGQMNMDYiTe8y9UT
idRE0IcBRPzafvsS3J5n/m7FaBDSikr4woKhPPC+1cObFV75QaA9m8ReOarSMXGpVu771cpRYiHr
smQmf0eav4rPtmUEt86+PnIHtgM1F1mntXRV7DFFyF+/F6Uut03o/C2GioGNMizCqFs2lfDbHMGo
c377xSqzHoKGnPP7y4/xgc84nIMN+ZiUVvMcllguqA2UKZ9gHbSHz4jfEomLbgtJIvlRBrz/MxI8
yNZohPFvUfJpMnyt6jUUUhWCT/VxnphsNWduupm4PqjmMdWUZK1bGscRkS3fje31VYqw/k8577cp
4qDTyt+8n14x9oI+qFU24vHhpkgxpzC6e+6AlU0NH6cnbDigsgw7ctMiioWNwLgAPfcSoeYcWFbi
f/so+fYl0RIjte5CDtHU8kQJGrtwGlcaDTikw3CP5fnDXMKDM1Zuq3mdaBxlZil4KooXKdUj6Twd
H3x6e3y6wVbWkHaUmLIbsn7h4PxLS5Ll8+fjr4R7WWBLfv0FIErINOtBU0YHMQKquuZHD6F3unZR
pk8jVSnf+BwRc2jdU3120gj/tSaB/iYB9FEdYRuXfiaVgBBefAdKkqqxNdkDGXsY/Hy29Ftmt0km
QWdky7U1mRgmKlypslK1sKBq2f9F3eXs6l068/DabiNHhFkDR67S5AQ10h8HyrlGruce7Ye3ec4f
fEka2OiZcpKhD2SKXCfifvMsdyfssgh0NuxDCoCruDg8Vrmbd7yDEB8yPrvteAhl0PXwinjsLZ8h
2LMxD9E5CeT9dxyOM69Xaktuh9M7GV3GqW5A57S9eNROI+CoPahDkdIAjxX8MzS+t1dW44ADxM2J
ypySqJglto12IlUfgUJmmKj8CYmO3XPZuT+GBH8tczgc4fxz+nNjWbckt4VZR+i0NTD57+hSGUog
s0pkq+nNZesCxV9Z4wtUdpWBkQodEH6DmCEl5eMyjnuqe8wlC35FEfqS4RTCt/79usW4ULNefx50
y2X5Vye/EPoWlIf9GjeUD6ebOoykZyzmwlDEG74/8mpdyrB3JMQ8RQDGRJWykbbp3PRrk6kIJ19m
1cxqjDZSF+DulVvnqCrM7KabUaiz2ocDXfpC4H/dlXTK9KmK/dn9BC7H4RgYubpKfVW1k1EZtqUS
PbjxQxg6FbNwgaUW80A5SQGNMKkCH4QHAgjQr0F6bUUblu91V0/nWr1nJFD12s8D6U2xrkwHWrEP
LyJRa/4ACf2LVGEejWLrl/p5eJ75CsjHt1vg/9t7kHj5g65Pk1U/pF+zINfvVHwqPiwx2rtcjDEL
6AtqW3KyYH/5p4fdwNjEAEq3orwM3Nb0uTFUituXWkRuSg8tGpYFv6TXwnvvgd/L3NosRBw3e11T
CdaBPTTBVeKTair9jDD2QMnBGyKXhfOjn6vCqSo6/jQ+nSj3c2jGgAske5jDlld1g+tg0LFPwsQb
nx8gMnpqTtnS9nWuTonPdgviBRIY8WusZCbH28U5XXRUoV0Dx+7VYIVmskVEd0veiI+7fk0aDntz
eIk2oGzphnKs5JZkRFo4SgJ2d5CzDaQ9+cnQb4Rh8Go9ymSgOeEUHl3VOkvVsoCz5PltjGr+Y2WY
bwKkRR3NMN1nm5nAHLLzlWBLXigWGNWiWSjnJ3VqrUGXFKgJ2+PZ9gGkkQXiKDH44zDyDC9w1Gjq
G+YmRNeJ53hbITqKt45htTxkb/vUP2xlzgS15qPpRRz051DWtLWHaW4izP+crMPYoTy4ADq94zrd
Mr49hVYnvO4VACNhPU/qc88gfNhUMHHGGbCOh+/vUxtBPpJLKjciJ8Sq9Bv/8Qb0A3TX20V4r6Cm
UUsF5fv9iBeY4NX18ul3JX5qvlxs/eoeiiwf/7S8gWaobgTm0TvlTiWsl66fC07h/j3+a6hBsRdh
PSKwjI0LRX/3lLWmBfPlRYZObgCxEl153unXO35mW7bzNYeP/At9AwEj8Kh4l7P8HqxmJsslp/4o
AuXSXLHwjNzLj+L6CXlmJphxPq52YDwbW4twQdI2vPQrI6a60LhXeuUGkM3a11PpX+CBk/mVxV1Z
i2D9LdllppOoG6wi3KBctTPnfdzh3h4QLQ4SNuA9WAFBiUNZyQgH/GFXP0jpBmcq+g29Unf63ejy
RiKcvvl75D05/Ea5lyQ72IKdgC689pFs7k8Bf5MlOVgTXX3u3g0O8DkfEey+mgWta2waieBNEAuP
MqICgbuRP7wSVrHr0GvrukDJsKW/3k9NWBCy8jpfcyBbDXoRf0BC0uPjZeOU0jMjV8Dnt9Jk/BVQ
CHMhtI8ydYt5cxZ3okWFoE0ecbwPb5bNVwCFFMwvSh4C/fPnWvyXJByER8zU3QMcNJuww21wBQOB
qO9ody0Ne4xMq0ca/nbh6PoCLAgxyD9MgvJak7669zom5J2Xxo3KjbmxnuvZQO2lv02o7WXcxsoQ
TL1Bg8iXQPY3JeiHfhlB0HrGoVj5pZgGnnsMywTrL/dyq1jhJS8YEkwTIP2hTPg2foxGYiX/gFX8
p4g+Pd+oLPrgV2w3Fc4IGq5qdTeYUf6iF9vfXpQykqI/tkJ0RO2OFDcU8pg9GKBQ6MTU+Qm0Wu4F
zp4JcZzFgYBd0fAhG8PIXaCIGTbpzEdkCKm/npFOGRCJSGhnc923mzaRwvCrqPxTW3HkL13OrI3d
qd5D8PXHZMZhN/8Xg/7uCFxIe/k1jZ9/iHWrDJpqZc3T3E5LhEQwYDbn6HNxFli0cAhIYpSCatC+
2OHjxfuNLye3RFtE3pjrKGxpFu1N5zfOIIDh5QmenzeR5Q3k2xmfG3BS6gIqJObO6XgC3cfB17Fk
Q95hvOa2XQuwlZA0PwZRYH+/C5oo2bs3G3M5VXTPHU2tQfHyuyJbAXRSNQ3pE8SYF3BLup00vJj3
HRrP/6ePy/MlmHOchaGqq4hdPPO1EHhCQn3+m4sOd03VoxDjlneBEFiguHIipLOaEsbXZ9cZuzTV
28oDIX1Y3RxPkh0b3tlYfEOZJjVofGnWq/xOyTVS3ueLl6EuM/qKmXP/55Xe7zZiI1sxL8z6hZ0l
+MjvUcU+2En7+2AYsJheciv1BoVnQmvzszp19t4LJMnTTKePyQhdWnqy57Gpms9qiueDXxpZK3Xb
zm0TSP++ShfZSddcOY5J7tgaI/gxEjH3/YKn1GFodSRZ2FINHPtfWMMr2UIiSvqbS3KH8Bsv4e2w
ojc7BVkk8J/UAOTq16bMEzY6RtcOjTJKIJqs0ZwejflNb2Nu8rayvAbdDd2lnsKOFaphJXgVrllB
kUyeCO0AIEKDkgGzSWmCSgv299D7Sd/P/JbJZt//P4ryLQjn1op1+5oNbXupqOdCKRtiexZrcN0A
mFgU4wqWyLzYYxLXRkNYWXX3CNTRLhbo+bGzXpDAmJbTHqpi+vFQh1BzycJ0iEaRysXkR7i8u936
o+foFfdIx2G5xeQkagmWhElGTkV9nigKgc2HzCrOlGfghTEp0CXCqHluRatEPmpk4KRQHwE/XAel
X0/IdqM/Y6xN3tsDHt3cm3YIwx25UjVN73aHmZ/jsvP8UHumf5hqKTyNHjUQCgOwTWmSDGejQwMb
5US2fvzeow8QjJ7CJKtQLX+BG9WXtFNtWb4msc8x1Ss6XK8cYhBbKNJaUDyqJiYzvYYJMDmBjImE
zxQjfYkwrd4TfIlT4lw8yMmnRCadgJUOjYZ35Qn7va97giBWXEWrqjKEOM89MsYG+sXnBbzrt9E1
9fXZzV8hdmcBpocOxbka+x42BVeiRkW17xPXEIh12dA8ci5PD3jsJVPN+FBq+his5U1Mg3MNSILo
YykWgDyk25DstpaiF28edhD/Yc7Qy3itpHx6w2LIneOHZLITYrHPK0qN83VSm129leo984zjwGuc
jOVZ4LGrbFxLNwNuyFX+HfTt6goSEjCOOYMVudiPuwN8hF45BBlQdlDZ2FQtiBoXffTBvpx4fMkS
L+3ynrzQ2cKHlBpwiTaW1mmte07FM5b4u6CeziKZr38CO4D9RQaLjNDgrHiwXeMJczsMz9kdSO6d
DllgAyzu4g6FycqscoWK/0N6p2viWNAegtKloujAwXjoSXv8mQv7p/6d0xu+R8ww49y2hu1MEjpV
RfQl+FV4khHbyRZGgT8NYyeeKku4Qv0dW8pJfWNN4fpLngYt2h7nGPRI3QlTopcrq8XAzGjsvnfI
hOYIbF5Tlvt3vgOoxbXiqnUfKee4MqEKr0jI4hmsTojhK3kqmvcvR7Ll18HiJjcwcrpsAKOYlFuA
n89vxtfrIxrvB/BUVeFzQFe4HENqpa8TBNVHaRYreVHqRGpjc+2i2g07PdK6NHrrAqbG89r6wOX8
dQ0aOS69OQDMygpdwf3Dkka7IG8ZPEJFdZDkH28//r81aQSg8pM7nxz96SrzsNX34QkF2Flc0YSw
zJPhCZSutDzy3JwDryiWlRHnhj97AIr9drw6GjE4+WXU+Z44SnAKsryklnnzDfJNKuI13aGWd3Dr
aCJEnpUoJWvmyfb754nFStWe+L/byHdIAMaMoGERhMRvawVYQnaSvKJWKDxhzKEFCqafw++fjSLX
Xw5CbLLuqcQtVwMBNih4jvyELhSyr5kzdg9W8e59UDuhvPnkZsFrIUxHgan8aDRzM0omPRFuCZle
vky51zp+mje7bflUcU7cwOb0f4+EGdh+OWyXnlmQtpGAr+nShqECaxc3gWg7cOTra2xTuqc4+EbH
xP9NjAJFFRZRjxuRNeNLvPSxU+TntcNeep+RF4hN48wTP6+AV5DeX9ujR9fRr5FmMCAiiKuDybgo
XXXi8iLxRngJv/+1bto3qRkfm9/u77oxfl/gzgXb6CXPh9JgsBWHA4/RclPPO5JBL93Pg6O3qg3c
5TpquNgYAMHy4ip4mtYAjHRmmuLvN8hoBynuISRsO4yp+hbKjiBSEBNl115XpoTkXshsbnQJFTGo
bLF1ci0aZVp9JH2DIv3r2KCYsuxHahVDHKdCYwjneCpEMnwZ35BhAuKWofch+hehlKDaZB4aO4WA
kCTdFjB2ntTN5YV8PraElOxqTFg7dFxu6jao5QyU90qIZ8WQp+wuwaTrCpqX9PofVb2lxxNu3u2E
Oh51JAPZUuUllZKkJCtL5o9lH6sFVO/gwCDGsO4ztY7YF0EF1HrxdR8plRSokL4h/mOKcQidhVM4
L43GZhHmoTX1u9vfdyW1ozZ3t2NT73qJP3kB9Fi6+EzR00PJ0LBTQZhmhPgM/WSJKH/9dgdSgKes
dwYNO0crTohKp0p4NrKbqE94QJiEo1/ogwCN91YU4NYGUploKb6Ti8Kb7zmsJTp0nGomyAgCG3tx
KAKB6SjrUNvcuYUaEuEBwns1ctgOnkim1hnO+SfOVcD6z69BXqp2YczCsWBJeOa+lehF3/+akwcu
AG9PgvFL/4KLb9GU8WI4hHiTDTWLNuxDd1Q0Pb6vs9kC3VtqlglP87rAwelasS7MiYpDgpQU7Vvl
b/tamhdvMqwbEDNGiQWo9XcqQ4jl1u5bQXJ0OQ24a9YpZSOuRwVqt02oZF7rISJ5BzjFb4CRfnr0
IueHtYbWz7BJvxSk6VoHP5g7FFA3oK7DrttonCmaDRK0SJCExn0fkIWCG4D6v5KBCmV9ECm0JLDN
FS4hPf6gi0EP1IfxSsJWsn++KJn5wmJ+EJs1LqUJx/0phY3ZgmYW8u038+UfCUuG9Z33vxajT//W
noFbq4xpcZHQ5UZSzVcTjGR1YIVIGcg14xehTu8UhDMKDMN1+vRmlRI/upY9d7/jIUZOo8u/vOxe
z3nCO0j+FzRC1uBS63TXj2iSYK6PzV7VbBO5EQo/hAyBU8i1WbmCkEoI06nUMQ7Rm9LDbT9LPPOf
6hGaXal4n7tHLAruuZBfFTYeppANXlVjGdH1NPp+FPUJ69w2adWrBqm1xJ9A483yqUiByOX5Z0l3
ZVdUNgzmPf9Ij6u3nHiBaY4VKxJy7LcdC7tTxdl5qAjIsItyAJjjhr5yQuabKC1Ab0MFdpOdZW22
oHbsGk6ecsxFuKzA+zjvzblr4JSneOgYmMHfwEszFQqkjYiZ1K3XAvRqTVifv1aOfHWzc8sAdwN0
H7SbG5RM6chDP6Zv9BG+zHgfMluIe8j5RTrXhueJdcW7+Q3Ehwv7ZE70bENYD1rAuW1rw/mMwshG
I18QBZdNwXtPpD2zLFgQDT/kfPnOtCmSQT6TBKtqu49NYc0qkAQhuc8YfK9h824pfkUaDOir7r+F
F+L+QWVJanUWz+ySOfEVq3hmHw1oL9GJcWPrhBhv3Sy8sgbxlvsEi4yVfxJjm3w4rmcDr+lJCtkN
ViVlwVfxGbNVKu8504DR80qTd8yC6ItClHN8byLwtXmw7XZasF1Ck/iJLdWpuFlgB8vMxZ4FQ5Am
UmhLqn3c2nUCg4CpdY5dhKArhE7cSWc939DFSQPxz7rTB+znv4BaYFA8uM/gQn4DdiitTxUlXpjc
lH4419mE57gw2cGko2KpSQJEWOY4TDap8vKFOVubkSj3KrK3k65re5AsfbeSCPnWJTMaNaPmw0ki
MNDIIsTXugwL0ulM4GmhfvO8FEqJsEBOmDUEBoYqzt3+45J61egqNIBuDTyNpHjIw2s1Q8WyHi0Y
k0DWsGA/Lu5Cjl91UcKjDAFaZZeY2FOdMsa22H2D11g7aOq5K/mGbcdtJSy6WiFYwIXh9LdkJNV6
UVZUJnc2wYNd9LV0QoBgrFIKe1VhLL8ut/c5OSNIV0gTF78jgwdFI24DZhmrHf3IjoE9t0BP+gpl
qY/PuHlWCEVw+Lq8bM5VQHXweDUuK2469KkPubFFZOYKVfSq34NRlNgD8+syq23Sn8PonJ68i0Um
AhC56eaGvAyJwAmxpS0tWa4SaDydwZ8PP6F3sXMyFwlFmQhdsNYu+3QB3d0WHpvdVHPtWWTitHIU
mDQt5lvb15rPmTkZuCa970Hw6lcf1AsqQ/tMrPXQ6wj1AcODd/KjlCtGxM1OGO8cGnI5Cu4HwnnU
wsoj5lhKVNYbpUAwylyowkWsI3ahv6EaPbzIK5voDiYt61n/8NBcDxSOP+HYjVdIn82qbUw/Zgqo
xFYUzNp0HhF+q6MjsGAQB913U67cKm/zR3ScIKmhqDbF1lNIIl5LTAtdGYWoAdM2fg0j3OZbueph
4ZGNxB0+hd54IWgT5Ok82RbPcY2gJKAb7yeISdpQxfpKlRzH1PTixT9LaUZ5Aeyf1XD/BNJ75ee6
MCjoYSl6ntjp6F75m6ojn5PFWRPD7JZwNXUJO+jX+9H0HmYhKYJv+KsyeGiN0xpbBu73XIxYEvTK
3GT4kK8av4fWSaX8f/pXaaX521GX9r9lUrC8jIxKEqfloh32RnfG5QA66c7HxX+IQofXYA/aJVkb
hTYgwgiMxz+ekyAmLAZlkziHHr0Xkplcl8eJ6F0FPLmGjjXfa5OxyI41GVvWF6juOR1GBl7jUH3Q
z4AfLt4kmWyvhcq5nX6g0Cpir0NfFVotcaibjsz2TyI2mDRkU36WbsWs7gSYGyWX5D2T3nAiFxq9
ZGboFihqjMnfHJBwcqnC8UX7WR4kl/XH8Zzvtbga/wKmm49RbCe3Kl9tDbEDGDFyNzqumf2YhZte
zUQj5lQ3YqPNtPpOIS2nwoeudXkSA4UqsIjBZ+CslMlkdYW2L3b+FcuFm28XnjnYPTzI9qEYdpka
Ww3qCjUpUpquO7K+juF9bA7SesBVsZ9dHrvsLxf72d2lf+Jv3sj8vnKfdaAhn7VEIvU45lihix0H
Xift+sn7QeJFn3YL9TXQYc9pdg0ZLLnkioruKigSGve2dsR62i7z7CMHz8QH24yEcKsJfgxhZ4pH
THwJT/9yL2wLfve9v/ayQeaO148rHiXt0M7QwHDFmWKXNdW6AGwtRF6nIN0ScBJXFTxPm+0/+/2u
9KR1Yj1M+APaFio7WdIJm98xmj9RJM66eWc1d+oxaODVXYMO00CxenILCXd8n14SeUW1Yp7QG49z
YPgUda24dtlqEPSFD5zJFsrdrup7GUQz+6mklBAwKRD4FTK3zMy5vEwi4FePImQ5YonLKtAFRb7T
6/WjXga0yh8vAJMZ+y8O3XdfA5ZeDRIrY3Nkn0sL/Ms6EVqZuoJd7X6n2LnlBF9wkp6k0LNe6Lik
ruEUl5xcAS2liehdlGJ+CQ//Y5RvNrZWhIP92nwDbDxW2wH4r/oFrsV2mWhbDPn/Zz1BCkC8Bvbd
xiwRUQigpaQkQiRGTofohJ5gJcAfqx+2Bzp+q929trGVFF9Nr0UrvdO0bmvNtCeLRGoeQ1Z17dh5
Ic3JNbhslAnXi0HXCQfX8eJ9nTpIKJo6c7Yp3vEak1wnYt5FhXCJ8G6K6FVDk5Cbl7DDJVQb5hpC
q5rREGFZEuOmh5AtAUmUx8fQOjYLGGKHpOmyUw7YvsmZ1EE48DN0NaFd5L+duZeNsxfdKkIW4h/0
46V50N1agLwTgSYpBJMSr14AjgG5xtY0P9Nl1IH0SPsynkc27omMePx/jqNhgsS+siI/v1LmXsnL
en0H3bvm0wtja/TesOXgon9Up3gtXpmCO/5lyaODQQG5v4eeW/ncdFWAC8kQUQ/PjxXyu3jA6dNx
LMv9x0hsbxsu6G0pxqVLz7UiMYuv1C6I4TrI6jYSe9zLbw5tymSulNER5tpdYEk9jb+uF/vAmblf
i6oUjkNvLW3kfV/EgdTZG8n5+ULbHZxH6VnSE5Ljq20mTDHiHPz706QCaXRl0QtYAZx6paXa/LlY
9nQIMtY/ux5A5DyR9/+IoSJov8tUv8frElw4jr5wH7MUFXoCyn8QKqN8LAkdirW8cehLDgv1WGLe
BYnUq6tvWEkuJaOaJDK8BynBUB493D0PQS+ypZpY7G+h1taehIa/RqndWLJxes3WDrYcuMUkZn/r
WaMBOoIB+70lOe8YxMvOxzMjf6a6SSpU2YUmAyh1j9gGYtt4bAScw6crTmQN4gB5gFWR6ZgS8hMW
QoZmjlf8ajWbbk76Q6bqf6QZXg5AZ2MNEmhOW1yM+XY1vIhcv7ARpnolGOH7FTKZPhmzqdr6mbjR
v8+i/2CdHMet0Lz+nEKoRIkQA6LMKc/VHb/h7o4x8ZqwGcgcIbFG3dcjF49n0d0YIhbUP8s+PZv8
hhh9hx5UKLUvAwCcjAoasGoGI5L3e2EE7SZ1krt3YRIbT3j48e2tdeesHWBD9btOdY4JweDtyRtM
7x+D0PXkfyuWRbkS0xoMGWibxT8V0BIG8Rz7NHvg72j9kXMZkoQnOhz8v20/FZ0CTbP0GEBgAv24
NpQOaaIVFL+1HrV/04KkRqa1jgJujm7FQ++lhfp+82PLYQtMnVqUC6Lgric8CuSrkRQWCA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
