// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:59:51 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_20_1 -prefix
//               design_4_c_shift_ram_20_1_ design_3_c_shift_ram_12_0_sim_netlist.v
// Design      : design_3_c_shift_ram_12_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_20_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [4:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}" *) output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000" *) (* C_DEFAULT_DATA = "00000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "5" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [4:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [4:0]Q;

  wire CLK;
  wire [4:0]D;
  wire [4:0]Q;

  (* C_AINIT_VAL = "00000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "5" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mEL3Zs2sBWwq70h+LUnZNQceciepEbrWHUq4x8NxuQ+R/Xo0qtAnNBdTgK0YVw4cfjk3HOeBPbTc
Ucr1RFYKuLGiegSPB4xT8O/tg00dD1T4MXFADWllCy5Kck+jfn5iyiGYNVhwwTzYVWefQlsgrrvu
d3da33N6Pa/p1O9VzL0QTdZsE2uhr+qG6Ztz4QO2+/9yrcx2BkKkMHwfR43FfFvDk8m7EMPYnSwV
FTIdjGaP96U197zOch3ex68ttlCjoYusJUqDKpGfAqxv63ogq5k02yXYGEXVVTSOfUW3+XSVkLsn
RIO6md+xL8AYcmsGNsayUUkb0mHYHteqHioJNw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NUxscx4eYr+gVYGdkcesDcqbOiD+CSwPwnxLDU5WRjj1FNLP+bzBs+17nuDkEHWISuMz4nvIeWSo
bL2oqzak4iG2OpiAZNtsA3pa/aDbVHguoNqozSWdM5jZ4qrzTkl8qvlfpvI0bEcbpjdGi1RjGN8k
gQkrNfdm7V2qUC2Zex0FaHSM2iNAzOoe9M2YejPTqngZn6iQ+t/LUytGpn9up1z4W7xX8E/CWahP
YoUigcVROQdHjscmxZH5WnjSoUczc4tT7/Q+fPLYcmccgiIHxZZzKrFPmOnI15nkyMVFzpwIcgzO
zhF7AxpCslLjG0GbE7kbcoOGOtOUMUr0F/38TQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4848)
`pragma protect data_block
zpDENrr3SPCehg7p+X1ya7hZCzbvlE/HoVaIGYry2LSzE895sstoXPDSfNeD5VDJjGDwT8LGOuQQ
Ql97lyhKzUsZ5fAoGanagbBGbbNDjTX6g5ySSLQ78yt4TMD17Z/4xP8+P7xFZKv3AXR7p1W+gbzR
ZPINemcZRUs4JNT6ut/CpPQmU0qH0Cet8dkIkOC7rLDhasG8gnyGGN6hwEe6Hjh+ClfnximvJzY7
N0lKEQQqOoHv0SDuShjM6piI6wkLJDRW8bFpqol25YRE24vxbsChUwrMevw0P0RclXFp4GNAHn8R
+uqxNqTVv9/Vk3Y95pBEpfHfPH4h+CQE9PcwQ3j4MPF9K5OiTn6LnjFaRfJMe3l5PlUPYAk8IXAv
6fmYCPKR4MqcfBetTaegu/qcoOWJo58+fKXHcelJz2GVdN02i/H1vPbcys9jUBZbW6ZvehhTry4/
Ld4oMJQCswnXOII8AEq4JcDk279cThNXqLBbLuKbRWjCMEziTb61lv50X1k4RNlmn9uVaVZWHcFi
KDUpwpK0CTmtSB1G6wARKA64d9uLq80uj2zorE1WjnQ8NvvMQTolMZB552qV5noC+ZtjsrMGwoRY
hUsrzXuGnhRyk04NpLz2r3xFJgNa2a5jIfLkJbryWGDGkXmFCmar/XyCbajxMVYGMcg0fZ5vpmBz
BfIf286N2Z0cA+UjO5+6V6DxPYnj8scVqgcO4XWDziSRXixFrPaoKIdBben55FEvZ8zYaghyD+Cy
HamiixKDgTio97AitMdDPyA3hE+0+JPhtgCuZTDc45NN2pqiSnwVFShuzBuUNXOaaYm/u9FREsrG
zuQsOFhKmj4dmvO154t6D1j03Lkh/vIhoejyyj6gHitFI32NtujAGoJ0yy09wqxI2Eee+Pa/rD6D
el6IfgWaGI9XV9vK7ddbeHHVcZ8Wk/9SYAdHblzbkqz7KzMq5Wqnpx/rfj1Hzgeo5wpfo1/VusIU
tOkHv54CG58IgUwa4xhkfPiYVh7m8boyGkFnfsqUs89xJ7gcII01gi5StoGqABDefXg3TztkWF4L
zpbq6/K4/+vfcfU4X8xx7tTVaplKy9kIRy+o7Zi0VlXKznu1rsjX/mJOJhKxtFDdbqlNj6X4+nDD
jD6tTHaO5YGtTOcGXcopxZJTveMRSBqHe2heZxYJ6+8wyBad8EEu2uMxhdZ+pgohlLdN1VcFj428
ndh1jkgUpaWqI+q5Q/3dlbzgCI7uNW0t+eD61NH6BQsXMwoiDPlmGMvq8Ni1C8eIgXB4fMZenHLq
Ha0U0vL+Fd5jpGg3qJScSYuLttPO+rtywL7Um2Y+PBg2qIoWMqx/xxOHmVSEwqRcYodLSm3hA5VF
xr/HHwL1OOpf5fPcnjvLRu+vgDIfwRCtN2UrBkhElljfjlfqsXwTxGPVanB69nfIhyRNMWFblLzb
Z6o5D/G8Ms7uCIfjq3Cr9fAwEftSht7UsIPpzu6EDaW9YIlRrRG/uHgZ16YvWV/Bd/Ln9b2L9wfc
/9hN1xe8KduiuZfX7iHOtUIu7FABy+wTQ6VXtTu/BxidB4YSWK/gM20W3s3MUw0KuDdlrsLVVWbG
CuByrNIbqOxtIs6CEKjxnCKxet2iErVvfHw2wgGQ12+qf27jR1Ip0pqU5LZHm5YV5zBtiFSFx/xa
89kWuV2PdgOaMmoVbLfU7uMS6RKbYnZuFKvp+I54Kjh83RT5CSHt9Stflx20IsYHSG/14GWajDO3
Cpq5bkhhC9Ms6rJP+EjKZD60YHERtjRpIYY/aSI4MJ4GmIPC74OMzDiU16AyOlqTwVWKK9UqFvn7
Wfg9fDm83Yqpp7jNAbvOG20iEyYx0q9f4SFs3eySI64fLVPY8BAAivKnpREJesy/zkQFoaTSD8ix
9suo3eMArdsMcazL18HqQCGJGEG9Luv/0hOOjZJN7/I6/vgJgRb+m4eQdz2826agKvmwAST7sINW
cr4Gg/m4V3qtoYY+r48bhXRG3thrrxTFCZoXTyfWFyTqLw0Yv6/b8k7HAr3gbjcKe3qtSQjg7OZc
NJe7GquKttMDCI4NcStPIjTUVkhpjFsmMU7+3o5ggRmP/I2SnP1GHEKWRjYtcmjC8m1ibuJLwJVp
budVTB4uGaPqow8neizrDY0DOX5xK01wONOf1soIjUpyAuHxiNP4RHKJzsN53PkfpXgRYgY7Dnl9
TT1KYMH2jRJLSe68K2f3NdYf8qadn/wI7OMUf/Pa7awJiKR8ZH5T/D6VUWyV+nVGziWR+FlTBUVL
wkjpsjd060bHyNpAuDnz5KVL7Kv/qi1aiXYiIed959CF3Ap7lKEfpCKPBzUS4UDziYUFS43TRnhG
JfeiGnHl5XEGrjMMjfZ53mISuWn7/urt2GXVZUu/fZNGY15UzHm1cRknP+QsECQYjrhI4G5PdUX6
XUyghR1u9tlzZdk5WpJDorLT4vX0bj2JBn9bzkoFy7s2W3aB1DGHmxHIE2F6AfEWLkEBou/kwifM
mqsrA8yYNW2XakrugwIejpqkyBsZhDJSPBM4FS/GJ3PnprBqePbvwUO9GaOdjbmblCGFxf0WHriH
u0Td9hbYXaruEd/Gn/dz1kOo7GS5tJa8KVRNI87bDHP8QJSfKFglv7wD1iLcFb7dWDB0oLkwGgs1
fEfZVviIIi977TXl47SPTlEwH2D7uPJfg7wNdwidZ4YJb0NT7lcns++QC7NtqcoRsVboyqCClbCu
27LVhZ7nJ8Y+wE96/z1zRiTRAfxodnu8uIHTIhzgEpLuuNR86y0t2vv+32vv9QS6Dc8sogY/xwFp
XeU6Ap9V1pfQE7Lzedz1j1VzLoH23WpgvKtICF4juBBXgJY9KDDkcZg4Gp7HT8NJxg8hcBoQuZ+q
lrB+XcuovwaHkBZFjtKg3VwbRhcE3fr7KVN3nhXCqNbfW3zsUruKAX1GJJNKdDp5L3W7b/p0U536
/mK5gMDcWp7oCFr43I3ggvu0z2/qO1NoWuzANd72fhVGNxBRDKNd1P0QCKgHgripFWufH3LiTTts
gIB7jO+rtBtNWhZom97d8RP7v8p4Zo7xUPf8Nivknbe28paIPe0r3hJU+No78Czm+xDEg0cLKd7E
9oa7cjmQMvkyFcwxBlg45SISgOKQ3AHjK2gOPAs3VgDNlmp8SzYWOoRFCgUzeg8JojdKKbld/S7a
WoOzrqt9L8WJcBX/if/vFmk/1MbRQRRVmZXXB7DcqjXyD8BIfsQ4sNHdFAJntlzEkcD+Or1MtQ9U
JTdz2m61WuXU2asX8jizKslrU3Crhx4npeM6pjzBXvpb0d8/bGP5NLfoH0ERyak5bjrvHyskt+7C
ZV4okRp3SDKSNSFfCKqbejtfRDyQa0/Z9RLoFYyQ/VUUME+lYUmPct4LIG9QL5j4rkpEsQpfsf/H
dndRWiZdC8KizKmer7nXBKSpMhZhlbteR7mBLSIysieNb/YQic2KfYZmED2Sggneg8hkluEm9qNt
to/fPpRjVvNAgt8PoRb89La0AryM0lTaVhVk0SwmQR/qQd9IFLOw5xB8Kt7d2MhKQhP5fBiNeIlN
6X+cZvg4fb+ywzoBLgCTVpj0HenECOzR5j0y+JvkEf8+7vgJbCv6ZUbTtpUkqMNBpLjiHqx5wvF2
V8idQEnoJ9tvrSd6CTYPLmdaJ+0TRF3hsIrSs3cnd7Tsy65EhM5y8u+sE5sX0pDIUjeJomuHxi0D
yFj7xShnk3DVifBu4D3ND5WCvdEKk0h1Q7EoJEzjnAaoH0xIwM+xVaasA9jL1+LlYJKhQoUNVU/y
ce6MCHcx3BLj1nyQEwI86G5ZXi1RYGqg3NpCDpp+OhZE+f2oBia5H3mIVpL58H/+W6zEmxsb05Wh
UGD9/HYcBnTcM8Swl7/7862HHWZ4rVqvmYcVZ8UsgztK3+Hev4IgAOLKR0L9YN6QDOGcOKkfRAlR
OH/QLgNW6Z1NIcHIfzb6TmqDi4HWk3Pn3LuLQYSCjWd3kku72MXrzr9qoRi16VayKYxMyqmdRMFF
KMs9m5qB7x5+DIf+oU14tOcvywZsChbCzEZpA3pI2eYvDOC2QwdpPJ5GAdz7eXAo/BMD+UMm1nrA
k33XE5um0+a/mfO4XS0bq/ELGZwrHBwdzU2P4GYI5KIbNWgfPdWPjq39hkaB15AYyoSdxDlCKLlj
bOLg3c80rCZFRPsUR79R/WKApUbt9PzH3PYu5Wo/7Gv0r/WIdfVjPHn+UR0APLK7UskXpFR5TII9
itHtgvdtl8IUqYorjR1mnWkaammIrZ53fk2UydD54ayAykZZK0uOcw8vx7oTiDqQK0ENoA6sovZF
mp7q5Jm/J0YDTKznYArgZnHs/0scUbpblTSp1b6t/w1K7nstZbnHfUqhpQnoEwq3eJab2l0OCJGm
ODjQynqov8LkrPytozzrxZLJ3NeSZOZszOawcF6/l5cdDLukhj9iyvrqNQ+bv15aoKEtEa73rL4W
ZGERl+4NjFHlqIFCR1PjpNwPeuhc5V9BHBbgKzHhNSeK7LPcwmLyNll5spM3bm8J7L7udYMZVTCS
Ex/+lAhRsBtRsiPpejh4hadPWEAj20vOIK4OAlJLmnvp+UHRCftFgSVr2hdgI4QFWInKlX6eEYRp
hzilZoPQb4Xn3Y2wGf+l5Kiva2pO8Qnr18fs2N3WIfgCYev3Z7cOesUz7xY9dL2Y5e6EQx0awE8K
/Ak4gUAroeaXV1JXHMQnqv/Nu2ol8k9gKl+P9VvoB/rPhl8j1RQeTtMMLwW5zNOdi6B8xE76CVm2
ghgLvyEe7N8vUBqlKa/BKuHqHHVibgf0jpnf8Nh+nRLMNP7FJfrjEAA/KyCMUXYSPlFY0Wt0P4PL
lpjrw4wW7X8Af4BpeED2nOlvS+HgPfwtJ/NX+Vk7nF+Pwj86oY1wqgJWP/7Sl7VoNy9DpIw3HvPM
Ab9xx5AgrqX6fQsjoGEx8IH2rvQ2nGS2f2w2WAqnbSyBcu9Y2eu7lazYIP6L1LGc8lcbPOspbL4Y
SYVd0Go+W8l3sXMm7LJGyaVT39Xp01B4A5advE7bJrqa/qklHlWrEvPEoaxPTi5iJdMMaKlNzbbh
jKkI44j25yyOW/PfGSZmBgTSEW/fLkybiB+/nnRx/qs5rFT0w475YXseqouXmHgT0W6IhwY6sKF8
Pq7lgr221Tr0WdaWn79Nn0nT3u6/nk8nVsmeq1GfTB0JcdER1Tiuc+iVWLQ3Kg0efveC0WqD+mnb
BXlivwfihrtASBduF9CV2v4Waa162q1/LbFyuhnzD2MQJkBF1BpSlIs5bmrU0uAZYvFHCHVzncOM
fd/xEpgeXndDUBV+kJBZLZn0er0RlsrnWwzNoDn+5famC7qhGKbfxXo+LlA+8Q4bhjAIzgmLJ1QF
v6DcYW/ibaDZnJd8JiqiMdF6wMmIei1BP0RvpH6NGHqrxreyaNcwAz+4ptaq7UNW3aDrMosh8aLn
W5LNF0jnUcqelLH2jSIKdlYYxdQqRL3v0WtVYeN8olQuUNR5gngmoSDABZtFTXeq7nlTSIC6igN5
6rIkuZJ1S4PTZe6azr7Dhu5r5eQaUJsAWS9L51pIYVo+zq9rmkDGOBnWlJi9WZmtLe5KZaMiExR8
4vEkf/8imj2PmfX38BKwzHkAoFUT7mKKmFzdS8+g0tSCQrN90iuMWdMqZ/zGsXKjSTDKcZN9XmfX
cUxxzwnPkFAK+Zu+PO36XR6wcRg1TkZmNfQZmqyOa3iJUec/yFWHChwzj+pTft3RF5lqE9UA2QaX
ve3uUC8ebV+HRk5gwEPpiC+OmpytUMhuBJIwrPE21lI4MEKRkwAjSunR5bm2Pj15pmYYcgP245/z
QRKm87/hTRaKlQzUcA9muXTbF18/YbflZMwHH3P/G43xCmLEzysSEk5EzosM47eL+8BfEs4gM7Ef
1+pE4frUMgiChAu3t1aPCj0bwRSFOO1lw9W+LNkIf+gLUnsuPSHJd8LjFu5dn2nDTIYKa2N4flYg
wL4hgeTeWE1HCtn0GrujX9ZQxQd1tUgdfY7O3LhQ3rkwgQOztvzO4lJXnj6WhCR3GeYYu9SiRFEt
fQDjyfwnDqowZXQDT240+8sUg14cxsPuZtex0GwraZKmx2Q9m6eFmean7RF1+9WAr6C+6xnB8+IJ
C9BSqMbGoG4kucPWFdzAdYcleator5pc385UVJW3X91uCDjWUsfRfhRRt7ySsafyGcJm/z4OTrWa
RlgLzJxs0c5jId22VGy96ZLeUQgUAcES5maOHFrhkHWyvp6Cw7YZKX0qMtK3WOPfuv+Yi/uOiJg+
LxGsdFrhb0tCoiFrtDbIgzavpJPctYrz9O4epHmD+ANVLJB/jTWIjZL5svRE1veLPqdEa5k9B4rM
ccqABryvnQis5WtzhfRZfTRh+3bDf6q9y2Mh/3GVWgJGkllrIZiExla6A3VyXXVyznSFjsZVHcka
f4bL
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
