-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:59:51 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_20_1 -prefix
--               design_4_c_shift_ram_20_1_ design_3_c_shift_ram_12_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_12_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
kUUqDeYk9Y2B9N1jVaHGhWR6OsL2SRZ8juKeIBX8zdZZ/KkYEwKz4m/SILJ3ZSybpuj7b7vYoRQy
wJVpgXiFvkz2J+9mWCdjy7duqjnJdZp9hPW71erfNae3uGorx4fZW26mCum1FvjuBLMhwUkve+7l
CPqnytB88l2VXCQbUoNrUALI/dStvUEep+sX8BqTO2NhsSiAMuJ+U1f9PIW3+WUgdJJ8k/UUUdqN
NOAGM6gSt81E38Fu+7DzPNszo4bEBf7n+Rdww6Si1eeFBhw21R/26EFgah4wVkNf/A2zmjiGnJZ+
vIqnu/nkpDDyK7jcvaJOgoQ6Yy+wQp3nFOjQJQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nSBRS4IuSoqugUyTfcBnhp03EBh9iQhkWX9JL6RZrYWbkSO6fPP7iW9qDrTIoFRno9sYx/KMYhoY
IPDC35Va8+vtcex2FGP0e+TUxUwXEAK7AeCs18B+uZkVcSmg/zpYLlajxQywjH9yTFCputLlH39h
DZUI3QtQarYRwf2W71jsvG0kduFSNefyqi8n+/L/tG2jaBskfVZhaWJB6E5BiKOlHU3d52fzfoXy
DTHhJlvwN+vmm9P5DxE5idQ9NSCUlCtOQYHnLvOM9HRhksw6qmVANRjb8CGYIty5gzHGvJNqhbU6
4UpzshdNu6wOyZou1YzalBCfomeHa5H/Il8HbA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13856)
`protect data_block
5Ljl1NbMdbAi9Oo5mBwblUAYC5HdQmG1/wKiQBSmxlFGc1r5jOMeqDBlwohJvQodLGWb9MCtMPve
EJ/4885fZRMmmPDqnlcZnQbV3PPLT2HjGnPtROtXMMZC7tdwsjWPJ7hK4OGZuMSSHb1TdkzHbLuY
OLS5DBwcCM1PcTWQWuKXmrNQ6rnXSH1LbPcd3KS/ZZ40LXTr2QU7Lln9jz3DwlLD9f/d5rHYmoAq
bZwhCGmwsxdkv45Du9fi7Dxfj1kSNXovz7vEVIcUkrSTh/tBtEZMp3ojXCIvBvvoV1Jhp15S9i0h
tWaxi52/xge8xC3ga0dEGPD8c79dECR4k2BBAxuuulylyKZcWTRVO67qWhY3eQ6EtZWi6qQ/PaNR
vwCxJPIuUgP5i3/lcJgfBRLJL0EjIEhJ5+Z+dqTYDZgvobC8Z1fwlWuBzWSr0H6fTV1zZr52Mw1K
aslgowv4WJdDl+LI9pr2TaaG9/RObAJJlYeK2Xrcn5RIwcM8QSvhsYVq26ofiBBNO6W/L2MdNi8X
Es8Yf/drF3lsuCdO5LwZtqXd3BO4wIPCqmfcumC8Z9eRv6pvrgXbS/DAOcts3ya387mJYzSICQil
gNXzNj8wpJx5p/mutOYXB/LaVibG+oMNGLxtLUr25e/F5dcFtPAaI6Xmk4zk2OZcwJAycQxCerre
3N+Zu7A2tuGewtPXBSeEmqyfFmltmpAICcR1QjtDaoSYNAhFhQdlwmadQ8sNU4L/lQdOs5dRRU5M
7X0B7KCUHpCyXj2yJT5wKvNKVzjQU8aoLIgNZOYJff8aQWYo2sqPdLrlEumYAz0wlUuDZRmHqGgL
MvLEAtt4rCJnrkSojcwpoCcu4FHByopS+otiKNZ5umDtDanQcxebiHikNf5w4yRDdI5YFk6t+8CQ
RW5EbiBGCcWTem22u4wm7TUMF1oHn0M8pOg9j3lyZnnNQA72I2S2xCB+6gK0jvfkZonyBTs7vmGe
mH8JEHWbrb90BJL64e16xEq8VHfzwApDaaAVjSilfCUT4gOe5qfp6TqdWtBM8c0ENNZHmBGVLIdV
XOhVRhuqlKCQklSQ64/6x8AKHfAJqis4vK03V5Q7r5qSIssUwTpFpnj0IccBlx/x0IMsCnKB/SB1
iaJK/l4urz4UXyVRVbABaEkIrSyC9fVkKiGh6AdVERtbujeqTAvYt+Lz4/I3+hsERZYC9eFSkntl
VET5yTnSQKsNVREdkMAcJzUBIKEDLYoeM06vcbYNJLLfxFOKtvS3Hr6YaxV5++zEMtEcLoyb4BqO
2Qvg3at2O4kFPeCwE2vpjQ6R6iY8nFlM3e8z1D/K1RMiCzT+aUh6AcySDNncGvsWvjEBiau9O5QS
9O7a29yxP3aR3KZb6dqoTPgcenmD7zZA9viOz1QrffCkToSqg2Ipi+Qzzedki9DdqMq9C3mZv4f4
2fjTNAqbkoHEGsVP+pZdf2lnjpz/OeWdMAesIK4xL2cylRBOS66m+gZAJDsdMJuirE59ptJryoBr
SM0MA2D2frbfUiBjZWaph34iXr4jcpGWsp6McMTONhS06boQValrGfQpZQ4TzJhEoZc03nC1RhEL
1CFcfsqJTf4xhjpzZZVzkBLOqHqqfr9RQp93QXALD0LmQCttwyM4m/I7U/J6C3Da2xM2tYSiIXOi
mD4S3Vr/Htucpt8LcJVEbBGnfyKRBRE3hmnh5klWpv4luxHPlo6+Uro0WP9OTzi8Yu/YyHyOzenO
uMP0auNEijkTDCiVJetw7vIvfoGKXbmjH1KzN91HyXKmurN+MswhX1lobcgVfH9SXSdWnZFkpmIM
7/8f2EgjpAL5zZoDXDbOTmIwcvMIkx0cmY9vNst95AJV6a2/WK6ErosecL6uhKsdfGq4gGe/ugIY
b99kidePA0KTakaEmheFSMrVAOedLQjKSCc6bFpchtgw4el4HaDjdbQ06UhFKbtgtbLiJxAs2/Rj
ufkQvRPJynrG4oVbvfJ45DS7b85gMQ6Kqp5C8ZZQcKTxoARuozGr9BITZzFybFYle6GlYYFpjIRe
HSfAsTtlyPwIOdkmMOjzm1LbcfMJaXRSiTCxTU80PSPVlDJV+OZDEZ+eCAQ9wrK65nKP8OGMlIf1
1LEp1p0CQ8/MowC7RtlzKXVfIV7t+ET/Ou9j+ozThtWS6bdzEy9kKVSODqyFzndbO5X45xJFi7eD
tSsYVsnCUHOrt2SH9PbD9sgS4HidQZ/LYmDHvWLAbQMtiNIjnjDJm9t9WnuBshffswxxDAm++20Z
Ea3M8OhGIsnfjX/yRQMHxu1xDHICgspwrRdiGHFKNSZ45OAkKGa2THpHM26cgH1KmQoAvae9T8bO
/iwM7/rsy7DfF1FKLTtUyC7eGuv1YdVnT7Q/SbKL5eZliRyNVQ9hvLXtIkPcoF77rqnqvGMVMiuu
x/g+e8vRvNaXWBcoZgi3bl/EWZwidNObbcn9Axh+aaeR1UOaGMU7eu/597NF446KuKF+sX5/knGQ
Hj2n1QAHtpI8HBWHMmJ5mqjsuZPsrFQjSjFPv5Gnw1azsvNvOb07q2V8gm02Ut30GXd/F3rwYrEq
g+4pLaZOyxNMVuBWETdqEJTXa6UMBRtL0fnfeyRnWJy9DLkNrey/dnYwJDx/RfRsdPZSVHk7j5ln
/cIxQRIXXJmFtd+D4TFWhUTRnQGciTw99JEtwGC/l26u2ydMEB122ZeLM82UfSr6nH03awS1v5Y9
61YCIokEld5BaEA5x7RGX9qmWJ4SZliaQCf7F/wsl20ejJ9CZwJtSQ0e+wSwHA/H89oqkjBKYtzG
kZP3XCmBxiD1wJdJwBfLhczxI9wAMGdyjgZFCd804HR6ESxRwCiXOLYtw4fG0zByYJIXdOxBEr9k
39PzVw7yvLgT/8t7JNqq1f0+6BA3Xj7nrr+6ECjAnSki+lb2TEC76suwusYqEIt/MgilT5394xZn
Exgpy5POTlPC6+Dt4SrN4sVPksl+9hSLF9sDFL2H4nWFIqhu8jTTzXrFqEEHyocjsjy6o6FG1YUp
O3D7G0sXP6+gkxqJeZQHc47BBjZHaSyLOP5/wCHjfHWnZsbLjFEvqDLi6igdI0yjEN+wjYk5qujX
w7GWOD1tu3v52rj6fPKDOoaDDenR8M1Kg9OlwvU/cwbDHobBin4mhAMl9+lbfd3z3hs1vgVCq/BT
Or4SKLNxKP5L9RBXe8CmIvLggjrxezhT4prbVdxfq8IL/M7wBPRbO27aPgRJKOC6R4vrkvTJLNzr
Cln4OnHCE/svHo191lUlyJMoL3nUzfVm/Kt+RcioQ8St43zZ30MrjvqAfXw6dkOZ9p7BHyKUzWS8
UhsU4nrrvWlFNNYHsTW5SJq0tcr8DLehRrnNxnl4EJ7HwSmQiCe0E+2/cx1/17yr9qhvHG6Wi67A
Y1U8jzAWXXR2WbG34NykXbq26yUKiz0Wz1XaHlsiNBcb5wB2A/Z+lOU2QS6yyHX92QGAQxEizTNY
1K0kfSEJidHLuJGJbuQ1ZUsriZSOr7u3g5Pspa/IBKgIc+fb1KQ5MqijLQD/dRLFzARrCtOW4MTz
LXELumdqYFgErEdo9xmngrvVPFIdlaExmKX//c+p5xZP+WNWFydQbyuknOnU5eq7XOwKtPsKB/1Z
bNGCpkgRlQ6NMFZfKzpU29A+V5rxog+nm7M3r18iex9w3XIxzlwGLMmRtj708HS2WzSzKPqPaAHh
KH/92ndEWkn+2XBZgDHsOXRTW0JGBMDuF+MDI/856XwQ7mWdJd2HIM89xpLzs1DAi6qml5aBJiXI
dw4Ly5iPsi7rHURawwFYejSnTtJMWp2N+eit1uBlsDJaRptAp8vjUs/eeRjOLrEx9MOPj+gK+ITI
Q02sODnrz8WhghhtoU6UkCJtX6I8AwtYwwSk0S5DgyGdsMyFSe4l8yTHQ1s2iJCXpvfOaKxPpV1d
UpFfsDU1sl+FaeJQ6+3M9lswoFEbjpJNwaTdDT+4i09OMrP8kX3dwfjrIEldF78nBgKvHgpVucKV
Sa1FuJKo85Zqr93bPjOoad3UeFPLOkBhzWXzkvwXiGQaaLzQjqeqYzwpUjHTYZ/i+ZBnoVUsipZj
NFuQc5jwtPLvnEiuGbsoePObbobSOEJHTXZYgZmr52mhfi9+bHzJRJWo7qkQPP9vJ0B3uQJ6G/v6
FLmtFtIHbb4mJR6x0PwBcFaD4qHB95t7A4TYazG0xjwzKpA+1r8EkIjVX7Hu3a1Jp/F++NeQC8kZ
g5r1FbWyPUGZmiKDXIsFSiheMhOPtQMXmJb6L8IP5re3UdvPAKZQHNSNdLhd8o+UXPBqm46p3XkD
tFPSeUKOeT1ZdhsHboi3FGepa8QGELoPZ6ORhxX8FS8BaACgiHVAxd/H83cUg1I0JN0R7T0G4Tn+
nuPCVXjXd8r/lEXI3lACHRIowCNwCNBjGQtKEG8l9oDA8Y6pI3y6GfSV7sFzCAbKAFU0t60QoQ7Y
EcSVNOokXz5XfrYdKvgy1y8jOxlPsE52bj9SE3r5VpX+h1zcH95BCVoCvSxqB3uM6x9WBvwgqxE7
61483mdB+dkqlj8VSEr+fdFF1enEbTwGsqtwvUAOVs+JXf0Tl5e8OysJi5j4qIDrT2rCqqqJAvG7
xaKWD5aInM1OYWrfVkbyyKZ9gXFGsY5/ppOPtSjBk3rXlABzIMna8awc6Uv0uQ+H8BtH0mfGg1bR
XfMLN/RKrVqlwrriBmJ8WWLfZ8YPJJft0E8ypBKnodm6FaRP7IQNSGdCq9RCkQWY8jM8Qsd2cfrO
JKOup9muwLKfOpv53DdeaSvCxmkebBm1vKvEr9GO+Q4UrU5dD1GyrRrm0GTN2RbvTvBWycAYkmVN
k4iA+TwEnNsk/JNxjGL2fKsVM/dokl21Kj7jQUIZ9ykrS5I6jaYhmj+2Jsi3PS1yQoF1TUHV/eO0
zSbwLC5GWWfpyWohtHDiftuNZIayJNIkHu2Qqq/E9srOaUR5DMK4nBPvVQQl5izsXopaZV30eVSd
0E33JCdKxxgUR6QZosKrUphIwxLqGq9i1PX9L35nJ+mmpXkrBcYUUm6KL52csmdkEADGoSfXe/jn
faXB1amBL9iKKsP7E3rAvAxgYzAcG1sfwRPfnK1Llpr/bgJ9gRLOn1vhVo+oraclK5rJDKgJe56/
Dsh8M76ZnIEdH+O5RgorudHu3o/t2VguIynfc6WassiZO41duWQhWKRyWernnoMrUMVd+w/WwKea
SkyTdGHGNkjanYSKqhXAzFsgkTTuSWTN1M8CNwfeLMvVYuW06H/LOeEXk452rw1qEf2RczqSUmfd
/F4/LG3Qab4yYWaegrmpb8xWHdhMNYRCHOgUcJj/OIFPHbZvagzZh8RpTwJXlbGH+wcKvP9SIVnu
btnMtO3E7oJJarVeIHnKQOPJGUAq3sPkEPeblzS3sjr6BLiJ2bnlJzvy5Vk6JmThUXfKEgjgpH3f
EEKm5VGCFqCCWLVzeIsfPePTdEz+FHRHqKt9HV+3Ev/7pvE3mpSR8tcgHRbFSbDjI0IfaMdHvQmi
fsZ9nfjbYkCOPwH5I55JIVd5PGL1eJftTPNosf36beT40f+3sIQ7V3xoSpUcEEqV54f/L3gFBxyH
CWUmVGVwi4TityApWl0feFdYnYL7YaKyo0CDYEaubJ1vNuZNqoQa1yFT+Tww1zE+dUJb/iUJ6uf6
puqS4pWYxOi9VOriCuIR4Un7R+3gZXKGL3xOqL5+kCbzzg7UFOVq6Jsumwnma5iAYBJyhTcNOtB0
HVei+lkIqLcYjE4EVB7w+X9xWDgK+KbTMxf3cUBrgBP9f97lKe1ZqRH0UOO10PrFEOQwIlqJNZ+r
56U1RMl5ZjnUpqgl5z+TV94GIZppHkgoR8xE5DXLR7f6Oe2EbvGHBxUYvbN5tYpZruMINg5ErW7d
5vhTSXOVw5tHaK5KTRq/HJxImdVvhZB3/azg0wTBBF6mdKqDrqXMv4K3oDczrnfDWESv46+l7KIF
RrRFHrND+R45BubH/hHuvyCJwVwxpvhPOwAYhhpGNM7TkhXCo9nAsp2E6FNyD52a7xtjZD9oMTE4
Ifc1QBngUsYBcf28YmMD9FSn0ZoL/lg9Kku8m52m9BmWuglt/77axm4AF/D5b0UTbvekF9Z34Aex
8R1A6zwasRq+Xy0cpB72GtWr/KTqC8fxZcnAfDLq+Wq111tUm0uQJ79M+GpmNBJIxUB3ipTbBf7r
gCXTAHu2epBkyELqr2YgJtVmqvOZ+vHXVgdZvCf5G//qJQQUwT787YPwBfFWM3SCAB/lWXtqCr9c
X5lOJt9nMTcBZbgech5QaUVueLqw63JNn3/YIYfwUuJTuOXfeaC5JFXnEoZ7mYB09c1y+edeHUnW
13I0m1FzUvk5h5g/rJ583bRJxaR0gH0FMSLHXP2s/VYDvtjs1WnI3vtKnz2MoUwPi0D3nvAQAuKv
upojQUT0HNTkP9YqsmgddVcMTWwj7Lp8p/OWtJTtfFViMnZwpPy4KpeynqGFimP+9tr1ij3Wwkwf
dDzdIZYbAn6fZCUsqq4+kLWMKybju9E1fHFMiO0BG27N/Dm5CFkx2aaztbXY6Mwx20nLLD4O+C7t
XiliG6D5MUVy+4yCACu43BPf8wF4F1HMwfnK/BGSM7txZVDE9gI55rm5biQi4ht9gTmH+zHQz/um
nt805RzmnPHVlVGwPsjNKoEN+Z9sLn5Yah7tPdm72sUMTl+XUXJwmRtMqwH/8SPah8fB/NR7NtCC
LXNqQTFIqpCqg40uumrOCtFXhaU6VlGxep+mL3scqc324lYc6hSmQF2UAh8sw4bayKXI+LuK5BFY
Zm7pp8P9Y6IJBfuHMV5cbydvI0SBVdef8XAB1nazqTVc+XojPj6RjfWNUp4/MqXmlvGsdvZjG3yQ
Ou8TBw3rmhDtUB7uajxROc4xXA4QiaWOgCecQ/UIWk5qmRtqKfuDAZRRzgM+09rl1Bal6OHe/R+W
gloUFf4MAQpl9Ly41JwmCNhmvjgZ+m/uU1cNGQoReTy23Rovwo1MVQw8UYNI2IZ2NQwaQXuw/xC9
jOSYsmuJhy+wjfD5JzMfAl9cBVAaHy4JWJOQGEZoieg9JupJLrXp1sQjgRBVQxgRcD8tsjPt734S
L9Du/DCIlvMGrxfsFPXSfas0BreXT1Ly6pubZy9sZDbOlGPB9iGFNLIKh+i6q+ks0Tou0QwWDUJE
aG7sAZVVrMa1q8ygNHlDEjTchoH8CcfPgPkQ4SZ+wBukFGcP6vgK4Ex1Gmp0quBfAsyy+sInNJwl
HOE4ZUXfDGOVXAipu2mIJJLIAGvvYXl7YVqmrBYA9f+nWCMV8Jb2Ba7e4kXAijE8ed6e1qPZ23TK
HxceQiwni2kuu77yy1/NkOOMk7blVjY6/4ti853M5c9FvDkJzrTOMTlwDeUk1QVnpaJuItbouO3b
yAP2LHCujoH4j/uOiPbYG89zXuTrwqIkXtHRjJ7R8ye6d1q6UCmPhROK7/roxMTBL3Jb3rni4b5f
8MkgNBaPQDltxVlaZEdUzV4ZhMXvlJITW0ZuMPIZDXOd0Zb1c+AUp9XifQuCQepLRkpib9J8G4Tq
5V/M0UTp6anC/QfKggglTLLsHcjF3a5E6ZPMPzfAxaKUbOr+Y6I9kk1mIj6aXqA9bR8FB5SE4yvr
/2Dq2i5vhiuJpYpzKbaZSojCVHwoUHKquvMwfIxfDfQ7Q26WVuVS7QtvbbxWoBhObVhYAaaMEapn
eRf4LXqg5UqNajp1cTk3HFakEMt5FMm/8fScujbq9UlTUEtTb+CAX/T+MUGNm30dh3sd4f55M4GH
KMzzgow4hh4aajQpCabofh39JCdGFL4rbGsyoxCEs5GaJSXa9pT6DKlJlkTqY4dzQlBfWV3LBSo+
yDaXPZujGMVxL5Fv0Kdsh+9cYQfvfDCAYhVvQIKC+j/qJJuaTQkt9hexZvXgf2bM5bqeEVg5ejk0
HVZHXq+IuUg17mjSsbhFGAN2W0nQo8NohJHQ0+LJeoFaKuUiu7vO77PVtAxcgh7ZGWa7RNpBvGYe
KHKFZbSbPMhGqX5F5EKaiEvlvSgHkk/inc15LQNNopzafs6pHAMl5xnpPFYtPXrjSONHX+Zae9wV
SWNL/LhvoobX8p2oE0+jRS6cXVewr28JVQ13jPFJfsWJQaELKsXtQ0YeTsEbMzycgQrzFI0gPFw2
U0C7StwAet3QWB8weSUxCaM9HuU1oP/LUi9QMm5C2CNi8J0Z1u0EYgcQBsAAvidVP5HbUrut5dAB
ogVA/cwa/NMgEQJVj1A3qGWes6pP5pb6Zkeh3AcscsjyeU2R2HB5K5zJcbEdKc5Cr3IBJi9T8hLf
bDuH0er4wvTn0LuTVFzvpcS3Db3U3n9vUTAFe3tkgW9htPvtbFsbMKZ6JWwNcyWf1xQh/9jKqqp2
/xhpvaHSsHLEbyiK5ZXGefyrm2uSiYj0cXhvcLRf7tva1BgWuS+qvcBd8HzqwtvWvkt6EzLTMok7
BC8kl1ZLt8qOemGyaQlUlhWulOZlR1EQfkxuU2X672sWj4zbgBeSThLot3m4v1pdMbwHzpv1O3Tj
H++/0Yf36XbpT491L1WBZJGyg1Ue8//af+8vWzoNzPaB2BHJRrd6qZtxvyfR2FMcCOFBEiaJ6g5A
ytwFgstdP9XM1yddyWsIDvkHcg78ylKFrZbK6pVQvxEg1PpQkbrsF1eOYj6v1TnKEdCdXbVwhgGG
4KyVM+KQzILOX84FR6SsBFraqE3pSckidDPaEfF96hWwoHRfJbyXNUFTgHQTF7g3D5yy+Qy3WWK/
l3FM4blmEbjx/JZf+yWQwcGXwnwu0/n3NOkgsceYIVSa0EYfyF/e+5zPGM97Phg/QiNs182+2L78
8bcmYlLuvaDlTKSfelobjxnUxT8v4MZlMAa0bKkHwUj6qXY+Ml2vH/1VBWCqR7iicpvhb95FFqhv
qH/dOFdiqgp0NiyTwUFy46jMZ+NSqv0y/8zofn0+dLYS+Olx/5AJ0QxQseFtnlVPH25S/blm0yPQ
JjYfALKdWjLUX2cJLUg8R5HHMQ/RoSZu67irRdoIiNsetV9FqC8PSAKUlGKa2ETBEEo1sZXBZ3RD
nOSKVJPeKDELwHvFP5TKUk8WIofHnhIo6Ew8ZxpQlOMu9nu9tNCu7TYJAk3n8L3ClMG+XKuY1tLQ
JIJUWJxD1nCaOmHYjW4JYj3XSypApnbNElxp+cjy5p9hP/vEghzI4x+k3bTUZHxi4HONtdwSDBJG
wOo8Vc0i05Fenevg6zvzxN1Ja16bqtKThUkTuFnlvmREpttuiEaHC9/wFAmWIRKnB1CKcFLGwpOz
Dl7Fm2se8dCDoHgdsQuTfvozPBHtR8xbus3mYo+5ZDs5chgvGAt3aYhWjNdUbtS5dj6SnILRIldi
NFA7Hw8cOY79Pzngk87Fg4dfSLzitILEdBwxZT3Lc6SbIu99iNphV53QLhYkaKpc2qCIhUz3ro2o
/M1ZVEc8ZGgJyuz5jzYd4nLkFBbFZ4MYkPrlJGhgUbnlLsIuA+q5P1Y6sVcjXC2tGnQTjZ+B9H3h
qv8Mb8DEPptyAbJKZyv08VvT0tsL4fzZLuLi7uUMvjirce4RfigKRnnGLV99t44MjdryOshRPMno
mzHKQ5hWn6o4o5LYz0T8dGY6jPjrUeiySMvSkZHGV3L9/2k0jQ7EwNe+27b1JAd/RfU15YVd+k2P
76MtNwTWyeMVkBwEyGYNmlxOYXy2C0dki0Q9sz0W1UWi/AUt2QEXfnU2eC7K2TC15lV1waGz3xvy
JbmSqBFWP6ApX+0J4BzTA6pxX59ULhPy0LkQ7HDe8apfwUwB2YlML+oWLN+OaZ9MoQTY45/EI5lO
xxt6Kh3oSazajOsOIFszl4L1h9H4S5XdQke51G7pE1emfz/sElaNZU/4tV5Nm74728VIko7LTzfp
c0n/AXAMpEouC9VAuZK6IGOF4g4GJ/+0S+Byl9th42DNncxcuQnYgEPViimV9gIYyficCgCaiBkC
V+HI+LRfWrsTS6Q5gDzUiy3DrVIu+yMibZdThL2AJTnz1kC0h9nfji4Ky2f7lFpZ7CozGNwiPQRC
MJXOsKayzMDFS1dwpFAY9EUMgoL2q8vYFummkhBEaxJlnd/UWBDlI6j6ioPTqqTRZXmYZkG+Rv8E
BbxDrpsFfLHcVtokmfetTsZRzwGjXdYln75xAO3lrKmdE22lmRoj0uiCKogkGbJz+MM89PSCUYwo
I3jGjuGJTjOWv7Ko1cHTPqX7np5Yf3IlRisSltXSlzW9JtNF0iOkuEqp11viIG1on5Pg60TuXXke
TZzwSoGxkzxfX3e+Rwd/FQgp4FddrhnH4Q4Ci7QcQ2lRgv+ZS6HORBibMiwG4di03Sq2J1V7J7XC
xHuTsfSNVJ0wha6qaoJtCGa9Nklsrs85U8Bma9tnKYKpv1igzTi1YwVZzKT22v7QQ3iwnqHCB6Ja
UshpHsFRiirDBD9oHJZBNhmDoVpCZf+RBMXNyjQ9Q8KjbCKEbg9f3l3YEp/hTbJOG7fE+HGaN6L6
GrXa+cZ2XB3LSdYaJJRyMy6wu+FFPYtVAyXlqJSflIgGI4p1wwma/+G3VFj/Rn2WUymU819Is23R
YXUC+hMtgmwO0MGJxg5yKATC34jzkDetMT3HMpvizmiNuxehqrmhqUda2zJIPRFZTCBQKmR5bRlz
aSYbJQoJj4zmucL0t9lwI/6Jshy9qHjZnd52qHWXMQcQUIIwKlP3wuBkcfNxMZZmgomsEmSWP7jk
cba7VFoqW2ZS3G9ELb8YNmiQmRcI5trAIuFmouExm0Dm1jpkm6mrqSEp/dL9SSGdRfIRbOlW+xzn
MJpJ79Oav6xk0M+i7MkGejNOMWekhmG7BPxDwKPpPHpVuNqV1p18vAEGsP0rq7gEb5hPlbSpbSbz
WaeHuTmxXyFn6vdLmixv2axD+qZwpuTscw/U9rlJJvqZOGtRF/6/aL8f32wF8dgVdN1K3dmEFVmZ
VrWpxRTHxyAjsyCZnStopCzenudKhP0lW7wtQKgEfn5YjYX2j7enQxy1Dto2kxl+tLgIXgj5V1kV
nIUPUvbTEP18xCxDjLg44isOi6+k8OoV81Is7T2XMxDSxIxmD3bRf/+q71kNBePUuk4BFVjJnOQ8
hgdDYrdpypHivHx/zoiWu8gIywmyugHfls28dySjmdPkbYPrxLDGmYeIbeIYOlZOaI13Pc8BlnO2
z3yhPFxs1DQgHuejYePPWbVD2qbQkDpb2/0g4SJdt70vauOJP0LYywNiAY1C69dKBc2vRc4rBjNI
VjI573QhUnUs0v2PAu+jNXevqiHz+lwSNCl5yeujJFc1oOy0rZY7lHuqxRPEIcZ4cnlNkgz/emvW
Bfqd+KpSY/MTPCFW5QS4sGmU7pwu0LWneM5nUdg/QBuYsS1g/w1RBw6Ox5J9AZWpN5DypW7mpFP8
r8gUH/2tSLhynHQFq5Jkl7Njz9IuGQ1r20U8MYNSvm6jKOFNZUG0LsfYekh43rX16agIZseSsm0E
dovE2CNyix8X0W6CTe2Ima6So/TIviKjFESlwnwiTnhMHn0+J5JCPNkTF8w6nij/XiJnstzNdVI1
H56/yGearaL7f9rCFevN4gLkqOosdxEDGDnZ2/4Ttu2EclrwdaeLYCAD3L2uJX6HXeSqkbbKpjMc
KF7zexCNgq8J7I4xoUAqlnriZxyTnNn7SeiFxT/zVzvlaF8r73AxiV4mI6fKG3L+x8HIWyT12+ah
2Ep9sEP8tXTJ7F9YW38QoOMfkWHpB+dZwuTnP1E1ev08kqttoDKo5WR0rvMU3mp41uMyAu1saI7A
AG+G6/oQDen5MxYc6rVyloRSv6CHlT7Pme+nF4vqzi/eFctjVfIylc3nYJXPYbJqhiDTiGpMMQ+f
DCEvCeOA0WmNQLDjrzlgCccvCPWu9wOKUw8w2TgwivbnYiZCBjLCJVMnABRQyBsSJwMo7MuqmQ5p
MnD9oNA7xb1OLcw5A0zjPsQTKcBoJFnOXsGP5/xQ4VLliAnUNxzmNVRp+gmTD9YKlH6PCiA1L7tR
CJA2o8KqPubygCqKTx3plv+f5I5RQpN3eyZWl9imNfSlbiU/8to6tMMtIPRKIRrkMiQiV9Qu+rBH
928VeB5YYK5cqrj5IbpGlbjFuRE8HP0QHbFtAi5Owb7CnVXTQjkp50Ps/JaJSdqEqZy+ydzjfQ7M
M7WO+FCksZAdfIlt039RLI7CzLc760/+TlPKxryyP9T1vAlHh+41fTMtmD4ssZ3De1QZwyAuMzt3
OSO5nzda75haWPoSUmhSCX7VrzRivGt2SrgZcHgNS3Nl4qBTyC+IERS+WpCTfbPQBPUaUL8WXUk1
ELWIHss/AwRKN7yL3H1Np+jLbkdmccsbjRBc7vMG0rth6tKqaV2uKkzY8alCuMiUWhDp011tauR6
Fn/iFOXD3+GZKbposx+L3ewA/AO1aKXMz8uSzOXj1hrYPBmsRXW7BUk6CSCJcLVFIBYIkzWuWMtv
qXmWapeFCSEVoW/HYso3f37dourNTwuIVPJotePXd8mKZu7lbm+YcZZGxJ23MEk1ll/EMyPRRrH9
/okVWcSFKg2T608PJ2HkYAvI1zfkcHuljDmCOXOzK5IL2dCN1CLc7inwiswTuLxka1T9jWLPDVgx
5lpiGgsLFOi4+3XysnpOMovnY8FD3XljefsNPULU6K2Yb1r7MrdJUQ8+2C0jqenW9v/RUwGdrhIy
TPugaX7kifcDKwWnAXHrnC9HThIHC38v91qrTtHYgkju7Rm6QDfJIqy002rZmt5EZKMSJ8fXyixS
illACsJKfQ0tnLErYdAAXYDFyeqnOsrp8G7iBLYPdS89TQ2nNI3079TMZuFxC9D2XaCHfWu46zm8
36qrybTDvirtB1cIUAweH+TL/pbpu11syckmwbbcCqkHbIkEUMJaC43aW4TaOgY59fmyKUV9ZDRq
PsSZ7ydThTgSgWp1sxxf61EeNXr+aF0LCuBrXNSNgFKEsc9aM+dKiwA84cLXVgP3ZTn0N0rRs8K8
3GVWdWO1crCatEf6laMoGf/XKunA0u1NC4ACmZoDubPPprROXIt76EYwgwWEfaInnS4uYBr5D6OJ
9vpqPHVm0+1heB7PilYr8Yyb1B37vwV/1dGQj4g7tUEP2QpRdrdrbsSxJD4gJVgVSl8OJx9Pg6rP
1pWk3rJrmoakUpA24SwnSgKSLkIXW8yuZrx6ozOyDdB6aW06ZPo9FCyOev5AT2SDL+uN/ILHPxuV
xrhlpgpsX5zeu5/q9rR+LDULHBYmZbwG8DJfG+dNwDznqapXKaO3Gg6ddiyY6CvfK0rMbF8UpewW
sooyFl8THO4q4d0bTGwvv22UyQaQadHWbagtKaCn+vuJykr001kPUUYVrXn5x4vG1HJ6i90dQ3r4
3ndprxoX/OdupqASll9i+PnwGf5WgXvhoXUPhASSKbw/WARz8pjZ9jQnnXlU0TS2hwy0uc7rXE9L
gtVHL98CjlsKkLsyxKKIN0ylHRvRPFodmeuXt8zLc1HFWS6UIUYrjZCaeuacCAikuVUoSRETq2wy
12GsSSeUp9+5BkTsuOp8/uSKMiKimcllKNvp61cG5vDG0eDQslvis/cKeJXUy0J/WZDgOMxz+CZ3
4PozNC/mmcpG+VcMeWiQZrQzEoaKrAUhrr/pUewgQA2Ty2pNy8YJqHeNGTpZ83id50LGzegSu7XT
OqU2oSVysGQyQuZ8CMJ/jJ9FaFbK7jGdku5wdW9M0ljWRtKlmd8jCXVuE8iNOZsR7ZX4qFrVdOX5
Ls/SDDsaJ2IBIgGRmJNDtVxERNFYnkefURhSOvjTG/tarQ8qJlnA1z6VdPEfJzFentJNsI93a5eP
g9cKLhfHpmy0CN2M1THs5LqF79CbVdZXmqu11Eo/dARRTa7L/DhP9BlOSMlY+4YluCFq8UjKJdP0
wtUGm5KMInLn2nINRUPfI0AmSzUmryoxPSgJxLPHUooJLy30p7lzbBK7VL/QWbmDUSUKfJeEPrTh
SjGyQagwqRL8UvuVSdSCGsz6kYpvAa+92jZDNLRM9sfNOnMdaXVwILZmsdKnIReEKuzEfqNj92oK
KuBw5wtbCi3SVwaGHhowI0pESPaWXGoMKq65WJ8iTgkmFr2ZlGzUTUCKNdwVwCNHX66hmxc5tyf9
fpsVag3jGg0dXxskea2Zr45AnBwejeOs8nNN9woXbrjXNe7Lc30OmUFOdlTgNd/V/YkMCga3KqMf
hNsn4wC3j0B1nH3ZhsqDwiVy2LmezpG7wP84Fe1sKrWkLwSMCBTUCJmDNssmvhVKdnTONMpn6uRI
04Ot6HO8qszlGNfiRosi+pIt4H9ewtl+FjYCy942Px9PAoc7AOePg5kN6nxqGiCpA29hB6sYEFfK
B1fAHPQOh48qi30tNPGu/5bthD8WZxpy7r1sKbW6iyvIVnakTGD1thfae1lqTSHODeEdcmptMiD1
WKSNBwkzdi+eO5Do1kcnKza6lWuuyv9/rw3nBZCRZj3y0yVVNd0BZLMvxjzjKnMBgwoWFXKZLjJQ
iXbahxHzld+pyW2FyRgy8iYD5YEJqfQ0U/9b3Iajk1viu4nYH26n1NJSoeIntq/K1RvUCtBKmRYO
G8rK24nEi2DRG5Utw7MTn3D1XLvv+4HubJcVZwSWIwPq5Q0jzoUQWkzkxb3nnjLCC+/NaAK0JBtF
An1HaqDjP08n3JTtFCUCd5KNz1/6NQMBx6WxSZqdmndTcmIDjdOfvxIblRg39tdDtKRHBNEwJ4j2
Hs7lMoPZVFcOZa9D88AncyGMnKzLfJX4vqk25H0dFU+qHfEN9s0yxYosKAFPjKGnWkF3Li/gIEl9
Rfgg5NNRkLQZGMZf3wWJLSgLWu94NPhVA1J8EVNL4qdkQQ/YlRV64xVuVCYQlWDEE/QvLMaTY/Xq
swqPUEGeHPZFSYDeFEOD8ST2zdg6QWqWUxwpNZ/BgSY7eBdpX4G0+A/jnocdhVt+kMlASB/YQaLV
J6U6/IBn6n78VKHWPSixZ8yAFkazLMEjeHxO+C5Xg7sgIXZtw9haF5pPIfrQ6A8+xVt2zr0pCZrt
7H5UgL0+fz0GV6iYmEGypRo50wS0XI3XJuSdiUFSTKVEBHmB2b6Q28yNQl+2BjfwuAjhs2ScxRNH
R9dww03MeBzl7Qj9nE98Kipl9ZgHNMaRbTmSYQJEq2eaAWzgFL+rqWKoHf3DjRsIusTQWRZtJGi7
OVokKuXh+OUZFAVZ5wy9VKjFNJ0Q23KIgbz+wc1xo8Nht7EWazNyaoZPDb8Q2oOKcHBM3S1knSmc
xc3NGHft7BGRjjgK7hC13Hf/c120ASG/xiQfKy1oNoXWCT2tae9s8BcI4+GZLuCNVEwSdamD8RxJ
+DFL9N61CIRqmkgFxGz0Mr4sXl67C+lf6Z9+tJrWshEDa8f+7Em+bTMAoGYXteJjOJB8k5LZRV8h
zvjrtoYwlkCIdfLYjfOXapDMexdy7Ewo1fADJ8iSIr1rQCJwJnfP2eusQ/8lDEEBuyQHgK0pD2Ii
cWvQz1xETgohHnmkeeB8qL2M44KVD1P6Akxvn5e4Q0GHFNWauKjEs7ajWxs3HtLusI/93Khr5+74
KBV3zdz29LdAeSSmauBr6u4X1PfUg9U31Rx1tiIxFfybUaPIVazpkGNUzbJFPBoQHtqegvz45YZW
cGfeqlq6wCBr/oIZajQNElRr57X3zDDbyYbvx3wLiWlBhHw6XJGwOuXa1SZ0tKReVl3ZxRG55A4j
pQNX2ZXtWlA6QU4SIGeX2IshxQcVh0oBHuLMepofZe9GHmpgBfsvKzm/+A12fQF7rIEXuB1rhbrr
pA5rJl04opXxIMfLlYZ5qkzTeTK5b72SmUeYIglV915MMi1pzvFPVXh8WF+S5PRaHfRmB6PABc2+
WmW5P23eyVW3ZsD6dpPiv7rypkOuXuSHMCZNJtBK4egDQFxRa/Vd1TaZavqVih29xsaQv0X4Hwdv
QYgoZc9fB0Ifx1xFVWgZNL8zwjpROgYw4yp6oI2ZfjmwTO6Z/4hGRJmMhVJDza0o9yav1Of5e16K
ljluclH0mDml6uTh3C5RtQlM3Hmusz8d6HfRrXdKvhtazFs1SlezPWBYf9rWnFGFq7MRrSt0JMsu
as1wX/mgFjV2IN1qdcSS/XZFynWyEXAppU55nzNGkoqKNCybR2gqfMKjk5J9BmCoPpW2KVCIgkLb
/WH+gG26izCZF32Qu9ZOfosQO1EXrGGXv7qu4Xkw6PnuGi4av2EXj6IXC4SFgcnPBbPP/kISfKFF
iZMcrNF8D83ZuYaqO0/LUc6zbnCy4677o22VdyPlnwa0JaMNeFGmI+/2910mMRowvcVAu2oBUahO
/kQX2VEfmH7dBS0vAEXr3QWGvGm6KQl93DfTwwY5Jj9BloFwy4xl/192HUJpAHagP1Gn5zz0UQbZ
8u+Z5Kie53J3BtC3BYg8ZaiFZmIB/oZNjtBtrXu21dKAEWwVA2f20pxKBPe+ZRZlu6oqok2gdCRf
5pcu37rdboPFPkfFSJGUdI4CvHpzcxbQWEohr9IXdLhpb+9YyFb8xRYu845vONHvelUTzRUX2KAO
fPxVQTxmnFAUdlk5VKhdkJvAdNIcs+zXOB3yspVJnQCToFyr1uhG3HYU7U3kqqRFJyg3PFXWf/Wl
leLr6J8VwI0IBUs6yeMpWRDhApxJQDBkj4xmJpEZJGsmzNPIINrHYYABY2X7+KAci9xJj7kjdsbe
fUmrh/19XIqyd+C4SXL4cmnDxPx5jk+ek0cAGyBL0jiffpbCvBHhwIwm+jbhFdvsc4FVqZ3quOoh
IM/vlnMHKCEgSoYhbVmC0F+4vm1G2f4rXJ63fwbPADeLjp45xJnsG6SLISO5xBa5gMHMhEwjy027
q/gwnmP8vXtWUE6fi8p/gNFVW5rf1jliaNBjzNj7bpjj170TKOVgMolw8oPy8pWujWz67JfLXr1J
gWZpm8ZDd/HB5v3rB1EKVNHphZexN3vkKCvCzYCZ7cnXb68Wrma3gZPUfg+GZ9m6rJZkM38aIlsu
t0Tj/UU04U/cCKuaQUoTZI+M7PIhTYEdQEENSHosU/y1UPlBG3Xay/2+GaEo251TfFRt/hBxyyd+
6PClWizd5Mjm1w/n88KAlWa3IskQFfPZAPHV+NO7F/fIjqEg4ElkKKgeU3NerX21o0xAauizanuH
RAq1oN6u8jJnk+OgUFAMWWrCk28fUw6awKcD1kL1m8iZ9Fv6V4oPR0o3fG2mJKsqpzBpWzFwN30Z
a+YWroAcQGTr7vAhfdUYOdLWcFYRcbA1oAMRErEcbD9rcdkZzs2cw5GJESKknKVF4YpktLlBy/h5
JOV6xpHYslbxrE2iQ3NmBNDYsysnBuSBHBIXTCm23pJQ8B+iRIITA1k/8FMD81BeaEr0rJ1tWjgd
AkLiCHtPqCOP55DPUO6+DMeMc4m7ds/O/lJciTL7FbZMdJMn3O30ETmmSpehl+dRzgqONIPV/QXx
rttCHdCLPlrTbJFJPJdZ9NuTWwvCwZLLr+Trqg+KDem2VwvN4XxlgLDZ9wVnQSd1wPIVP/j5yBcX
fyoeQWqQYKAR8dm4ySBXZCCsu1OUsgjTNGyrvLiCuVQC2Fmr1co5Fp2LVAoreifO6UsMfOBH95YS
5uPAc5xau5lHK9MFzlNm0tYg04AUTs51ZqojZY6bBxW0HNq4KAEshGz79Kh6zqShFJl3uAMFPQUr
nWr8uPUpm39QEoCi3fZ0sT8PK2Lvv0UETpsk3wUo+ZT9vZ8jxjoTfLliLuiRJnlNKLwtf/iR76pw
xJWZ679qqT9zPBUIuYkz9G4v2/FqtJwUXXGXkhjzSGIepZe2dz0iwNP5EfdOMkshBXo64Jcq++Oa
FeQJjxN9TK3jHirpFg0bLN836Be+/xNJMT/NdLTXfFjr+TlhAZHgg/tXSl9QYFafYNHOXp3e/xy1
X28B3GhF9JkVp5A8Cf2nJ0ouHDipK20bCHIWq5bLF4WupnipfDyiHadiadXjHdkS7hndXaMJ9BxV
YhgC/Xk8G8uw/ZA7plKH18O6h4/1iUcxOXN3QGFSyLlUSApvpQ9SzAJA2ccP6TGS1T23hN9uxGTu
VldBLCd4k7B4jMF/ygDpAQpPDh3JTLNL1oOtse6CaRyB0lvtwtYvTM9pzgaqMigZ/9CejS74jk/F
GGv2hhLRla/JSl/soakDLLXVJAq8871UKLW5zlJqdrwwc+Fsm1X75qGjsFSDg2WMpz7ppj+eSy+x
n8rzYYLzhItimIB22dnsG2/Sn8R2lenILFD4GJOtaYu82a5jCEiJVI/vwP8Sg3n4HiuXJN59gzrT
omTVD7EBo9k/PKRvSHHQMcPGSmHdIfPqDvAKx20G2W40xVxhi+U+LyQjTkYv+fdhF3kC4JooEqKF
J+Sa7AM=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is 5;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00000";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "00000";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00000";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_20_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 4 downto 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_20_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_20_1 : entity is "design_3_c_shift_ram_12_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_20_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_20_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_20_1;

architecture STRUCTURE of design_4_c_shift_ram_20_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00000";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00000";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 5;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00000";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 5}";
begin
U0: entity work.design_4_c_shift_ram_20_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(4 downto 0) => D(4 downto 0),
      Q(4 downto 0) => Q(4 downto 0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
