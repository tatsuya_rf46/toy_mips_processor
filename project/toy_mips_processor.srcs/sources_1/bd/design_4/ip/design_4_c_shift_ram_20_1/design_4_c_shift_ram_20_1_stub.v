// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:59:50 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top design_4_c_shift_ram_20_1 -prefix
//               design_4_c_shift_ram_20_1_ design_3_c_shift_ram_12_0_stub.v
// Design      : design_3_c_shift_ram_12_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *)
module design_4_c_shift_ram_20_1(D, CLK, Q)
/* synthesis syn_black_box black_box_pad_pin="D[4:0],CLK,Q[4:0]" */;
  input [4:0]D;
  input CLK;
  output [4:0]Q;
endmodule
