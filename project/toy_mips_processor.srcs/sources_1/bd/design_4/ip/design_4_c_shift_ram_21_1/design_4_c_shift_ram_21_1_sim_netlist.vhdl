-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_21_1 -prefix
--               design_4_c_shift_ram_21_1_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13248)
`protect data_block
sTdDGIoF2RZITWvKVMEV05GLqoCgyFy/sTA5AQ2vI3LmzA8UhVyb6Ltq79tuB43AcxE84t3o4g5r
sRmngujZOxnceWA6RC8ubIrIG9+xFL8vfX3fWbL6tMNJqPzfL1foc2gf5hnaLnTTZXSoSGqRGqrR
KHJGfdShnJzVFSYr4DKgzcl0PHiFGDcePTyaUcFnSOwmVkyEHHLR5CYsEZ6RWdgASNYGXwN7hyZh
E7E6yPGHgrQCNGCPxFiJ2/qKmju36OZE/6XTzdW9EPdfiCN7N+0U9W6HchKbZ+J3we1w5DjdGckv
fwqtsOYooRuTdOdstJ4JS+NFHU2lf8Xz/QgHneH5cGOIHIqC0KJ3Gnt+9Wu/M4Urrv9ceaBMT08Z
Aid7LB/1RwZdnZB7r9hK+GeWzcovZLiHuYoPAkjEoRXAgNmkjcpDH9+RIVx5ZsICJ9U6s/xllPvc
WQLT4B1rwftPqScrDS5ZNhkPFBQ1EeDPK3TSeaHU48UdR7ocavhj8lcWKp0PJ/JcOX55UVsbLfI4
gjwYgQ7En31tWbPBo239IZJ41LVrqOUv2CwIicdZ2aVdxrX+SyYfFiSL/x1vDwmk2EvB2OplvmPy
P4HP8XLJ/MvHLmeSPYQYbB0jI8GVjCwmlQyr/nhMTPyPphQK8dP7sZ4LAfp68QA2W2wSHP8XOkND
+VD+pe92+EaUikUhamHMgSdmHlr7LVOle+rtIUMRvLlUMRJ87Eji58mg+fB4T+uv7fyI/vA3vVym
WBPlzKLr42Tg1kJ115it6F2EKLI8zbwl4VBmz0qXtgWoIWd/1PsQcktlHRvIhuLicnQM0lnG5IJm
yLfCqj+xup69auZf9ZCkOrYAevl63iOLvDxu9oHf14i+dalhCM58BMig5CnXUiwxOkLBPRo5qWaJ
oP7H8l4nM/1TxSb6o8EPQi2DEyMwhtoJxofhd51wRsNUbAUki2Jx+6Vq7Tc9idcaOPCEnPJvTJd4
fT5AZhZXwXmqouXrGl7PY4+U1iMQabBC/QTY17T4wAm9OW6KRqto0vS8Tz6Hdb0q9YlqyiBWlwyz
KAMfU8ePTqLFRQK/p6gWhtpQl2inmM8ki+5/v169fyVTwAnEhWLwEPwPHoMnMjgeGv5kIOhOjw86
buT49y+v7j5kNWcl9A6zzuF2oBvYuALlxIn0BHe641OOYsfbTtXdclb9Hiq1A+XAipJBG9gce574
bYSZhqolFTYU8kv1/mq/O3w2ZTlog+p7hmCxwki7w5Oa4fBeKoPI263UGGDb9KvgE+UgedZuhO6Z
KGXhdbyx5QmWGBwS/r6xXEiUcQouIaM5Kmr6buQbJX4sGcAstUG7g8hSCZbO/IW0GM6/I+VXpemD
6FM6oR64fFVx+rM48ZN6K5Meys8DBCi/4AYB5oqgzEhHYt+QVLzy6GSdTjrmrYfcluYA2aPZVRcg
d/E4LSIyCZ+nxvPYZtD/VGd+cZs+Riw7LJnIxMvNdy0TJKfDLuSoOm1Mo//c1kplGVkjk6HBbhaG
P8woP2r3iQaQNKU3lx5nIyirZxAV0Rf55JdO3z2A1y6gri+Q0iepTWXr94l2bY1fotvsCokGtefh
2btaE+KPJDb5/qRf+WCk+mpG+Dpbn2XGjr5OfUd5UQlY9u9RNhw9ZE850Jog6Ybt/qzmZgEJMhAQ
AFRJxt78OvishQcHjzcgIM8ZPP4OKM9kxLNZjaydInjKJ1PuknJHGqMkcPtofzCZsh9dbI9e+u+n
9r4igUFd4yVYAhZIZQIuhdTGZ8Vo5Bp0cQ64lcwPfCLYVn7N7kROvaF1YZTwjWvrZ8EApVXB1j2R
MRWoWVITTVJOKx7H8TL6tgss+ckF7C3Al1gKKuVgGgy8IK4iHZ32GDVeYMgdfgIQF4shKPf5+O6R
Jkm1dIQZHsw7olrEg42VJ5c6tf6DPks3rOAXaGbelPrN5pv906TZQ3nXgtuvUJ5deNq152Az8eFV
sKxmNKHeUBcTzz15uZNq0l/I3VS2942JdT3sQ+5mDdv//Q2aKGcgpYZlqfLMIafiDXvkP5NjO+Gp
rJVNBayKKXsGyhEW2n9g5LjVyB5umD7Sv+fnKZQgx37peickrg9QHFHz2z0p5X3f5/zmnxviladG
005Ns5HnfcHO/iIHJcnmKARdt+gOHQGvoQJt2y3FoQ5006R47bSVueFYHLLZp7rSL5sF8bt7nTuu
5u3ydOlDsogbdE3Mxs5iv8F7A7Rd4G8yppIrQvgZVP24CzBKzB8os55iOrp473huVZZ2GuW/Bg/H
omAPq08YKywLLiqaUu6J4Os00MjjQzgePGNq7tsCf87TZvFgD8hP/RYeH/ctcAE1UW1L6PNo1EX3
9SxWs2Tj0X/WqfU9w8KmIzFnPofevc5g28yjE5GwJR3DBM0U4sc/MPYrC3koEeEAdQlPBUUEBzJv
s/v7WSoRZjkvvoL1b7AMSytlOzaYjjRK6gKIeDaW1kmxR0kizPRr2UMav1TABW36ftD9GoAgxbDe
Qq4BQuyNo/rOghld/y7joU5iflqze/chZqyWPOklj9jtCdG8gIamKT+rSlrjkNyb62hy3SyQdgj9
I11Z0CtaET9cmVBuFkXEv6Src/RZ+ZnmBbJZf9FGe4tvtngBKYdmw6NwxweUMhkRPjEiE9tkHIQl
iq8BkX0k9/JxGxPqJZ2GdrYjCGwPal0y+EKqo57MUlac3WKF2qXmFl7bBzYLr/1i91LIc6qo+uir
n5RHpZTpEvXk5guoCkoAO7c2bwkBmRi/rQdeU0sSBToQ/yV815celV+v4luN9RoKFTDqp8CtenB8
f5Qr3XqnTUpXWL6/8ACnKkOGT2qjVO28fMbXBpDHXzJcFjT3ZJBMlP+QdTubZKxza1bK6qWeQ0hg
Ok1uwMAzjLM6rD8rMj4eIAhe3l+5VYQuc1g7uqgQMlpEFd5fZRTABQkEubZBIvXl55/HiIvQvJ80
KKV3nBg1bJip2j/FElDzFWIivTncI0QE3jdVldKjmOjOFPQTEuqgqUVCy8U5NPXIcPOvbx0tT+Rj
+KfrwWvkdhkMLRxpt9lWj/jLIfBO620cxyCZLj3sP5aF9IfIrDDtEeRe3J0Kjnvh61qS1UQLhd2h
MTPettUIxoptb6qtvcM8uNkfd/MouM+55fCvrO5zJZE2R2IaOWZQVYz+b6LHAOw3xOCZvQlD/hjv
gOjWahISMS/LmhV1KUvnkTm2xMwHal8sn943YDq+l27EHHH8q6e8SLFCoBsdRIV6bxn4FmDupDDQ
9LJHQDW9PADDGX7kc6P2ciO0S/7RXXPEhx7e515C32FDnFy2aFDj9y7Y0XQ2+DL734GkGxBchEf0
Jx3S0U3i+oeh8d0vjt4anxc8E0awR9+9Kz9NTAEkVtUgfmdB4iZvvBewflwUY+15oei5VVoMV5J6
LuOAB1ZlQIsKtQ1oNV7IjbYQr7aDTQybn+S0U0vT6dAo66Xwm4x9hR6Mmoo9HxnReoqBgxVLJxNN
+dGd2VMLtymJnhaQ4+WTA8KDvPdEOs1WxUxfqoecMkgwdNtvZFTblVsPWc6pUd/V768jdkv5XENO
jxHsesS6T+DxkhDSMOV9mH+DLqUg+ANaB2CMUAgD76PdKOpqPQhzCTsb/yPp76LD/1seoDpRlQNv
P7tjKWzBz2BP/S1nvdjYUrtO13LBST2GqM+npt30C3vxkfgnAG5aj1TkFgL7rrtmLmRSReF0pjtf
BiGKFNwsdb9u3fAyqjBleQ7fdzXJ8028HYhvZSfd00cNBswmFEJXD973jmcg+4cb3hWfVi53Unf9
k5ZJoBka9jmV5TwRFvHP77hQ5PaUHKykK5Z9ORoG7/20ed3KCKso4qjiR9F+zZ3qKWBXkyB5PvPZ
goICoBl5/0AMrqfo2mgqEDEXawrP2tw5NI4fGHyHGxEq7JHMA0GL7LCRBqHZXNXPp/LcDrrAzKYu
V8TQHnCG0iK4cdCIr9SPQrdOVfZOkri8QVlcCqbM79GGJCbNPybYpxztN+aNdVQDXPemg4RLzd40
KcraJsATbpxHVs7q8QiEYUcq7lMdv79S3iRV/SqnGMw736vMU/YKtbWbKvzXz1vOrTI8v7IMs/YD
BjdHfQGurqd5KlyRw1rZ0azowaalauHnGweA3g5J+9UpyQpguzfu8+QsbbGW/TaatvzAXnNiQgRg
wP9L7LSA9caUuAcQhkVyT5zwf0SfXFPOr3E03DmUXDO2GlxNkjXFKg+mTS0MiI8kGHJbbE+4Bgkv
CoTD+hlDZT5wR+DWUiAXvKVHwUfgIwfKakZrLLxDB7XqKD2Pi+5P0/5HCyOoSSlQOrp4KEGSShpV
ctEFw1ITHrGdbwBSEGI5CdQ2B/72HlrNZ5+9FDHMSYigQsjE0ZeRzgPWdR3PzmQF3H4SeW1pEeUc
8WaaDn40V2W+X9Zx0k4Gpl63w24iriUui3Q0FkK1MRvfTZV7WFrUx8a8/Mtbr+w2Kp5PU8EYmFqZ
dq8Yk3+lU6QOw4ijdqUiNwNbhEQRGQ5o2xW9SBOxaGjij5bYzPTyzNU31Uf0DVIRukmgkX6sckhG
Ap7zy1pZXQ7ExQdBmtBeHrvvKEvC6Y3OHZOHYHoX9xE6QIwlpUSs8C6d6U2uZb2UngbuU3wZiClj
KhNRKcVc93IXHRbAfOdWRGwkonSIAWRC7BPW08zrUhkCEOSXvWeqAhxOXGirkPo8juAZh2cQCTiX
DFqkoD+YXbIdzB8mpttAu/R2XhvoVZvM7oYVp6jNx785tf/3UvCJOYgbUdeLRGEopfTUfTlUWVc6
J+up2WPQHxayUPS86GbR+0ywScjh/YChrZDrFqc5MUvx/tpBM/2xru11p2c7Zhl8slxUgX90D27G
FOJSsvfIkMEH1dnmfa2DTO8VzriTgqja/LZVrGD2zXOgXs1tnIwqWJGL+fdvmpQLF8O/6/aHjkK/
PvL0ekYLLQl1q+sfQznJfi5EW1GSVqGcj2slVE40/U7K0402JDU62EXsrhc17g1E5KeZVfoBNChl
d/qRBnYt5LAeBOKfZyl2AAt43H3YNyQuFicb+7mI83wwg8rFD3Pcn9moVatiLaRH9VrYokoKST0Z
dzb+Of/KcAhDq6YsUeRxsD+5G5qNhCDE0xJRUOdHHUFo2ysUaUPGDYZn6DqAgR8irwX3UvZAC5To
Kjiqd/0JriQFCBkqUA3Q4DONbMaY4Sl+RRQ7SglrJm97SUHR50LIoXVj/J6IS4c7SO7D9NnidqD2
PsAwSPFBkOVszrrmATNEeE3AgxXwWRA1+HyQVkbyu2Gmer7tGUs9UWHQIkeD9TWV2nqhJshGxQKk
fLp5U3XVGg6qG/yd0ZHaiACSnbki9kCQZxCcH2almApEzkCSAMwZimcXUf4pCea5cRFAyPmKL/PT
ufsWKckU2Z7rQXtJyKhR+SAXf7fNKk4nV0DXuMuctywQmByyq541NaZ7rC+1uwsFxg8QaanPaimg
wp5OABouhaGN6JFgXAwrxsW4bKiDkZuuizVe2GzOrRwzId3iwVeD7u7j06sgjReIh65MUngqPntZ
RVv3dVU7u/k2Z/n2w4D+fGYjre63trq+N4Z/dIRR+nhVHXkYQZJjnN7kowW1gG8pHpc04gZY12AC
8AcmrwiiKK9j3jXo432Y6lk8VnIsppvhq8p8quEUDB/0iguMcILn2BlWxL3R873I2T5k2qpqCSNb
ahTUJIWiaWEm6ELGl8qK+PZUDthqwY5YtzkHuzWj/0cIkt7k16UhAgck5mUHb3DM7CN+fFng9ENc
GFJHjcO69hRtv1PdtNs3VLfBHXMUrcmwvV7UtwU/XiMkr/RyUFvmimrnNCRWAiBla6rO1Q0xfLSW
wHtJvDRK3RgX4KrG/9ztGu3OiECZzCOibU/8K9ukLkgl3l19SDZGIgofCu5+yLC17B9m4Uj4Z1g9
rL7Nne9pKzSpVXRsZKPs1Ukp0MwubgQN6GrFlnHvFsoR0YZ855SE10XZ3AP7qwHBnMSmNvs8SkBp
TsmnC3rl/rhEq2Cq6BWT8gMGXCZRLOb0liKocxTx7rhoZLe3tLmZ2dNFuqZiVdpFPZ1ja2q73FTW
YykI6mjSwb+HC8hWA652l1iVK8uVeCC0uAr4J3iF+J5wI5k8YWLFbZNM9asweA6NgrL89am715QE
R9sPtRV/W80fBK/OniYZCWs+7WzmP/aAyPcVeUK3INX0jagLtPwgHOVS3YHGdhQqDveh4bJceSMP
QiD66JMiCuoDz5j1BjDxP14+orBsO9IuKHDg39eUCgW26AbZ2olhwEbiCcsycoIni+WVZ5tCAlqK
Rfon7uAkfULc1zLg5LAlF3CcK5h4Kuw+PhkuUdusFPyCscV3ifqVwhN6voPCN+a6tOMNXgqqI2R9
FVMcHVpppu2zrSLEc5zE2uyQi0U1YwLWkQMeyb0BOh4/pmS5oZKctkEkA4T84qYJfuFxXusmaois
ZZZmuOO0E1s0DpwhtpixxTfRRtyoRwpXHPr+ssXIOMPrXBVH4FS1rxjOBrNuDAIHPIHLOSz1/1kb
w4HyFs4iU8p1Os4zPRm/nUJfmhXPdq43nc2V3SM82dsb53o92fx8hCtIpAj9BmpP6WPRBIwomslZ
GbcCZN61E+BObYzIPQmAwZjKJFVpN7ZDuvWW0rp3gutetjskGLeVuGmHdNHsZ5IjYPEb8QdHkRrp
9diBreXYcVYFBi9Dc3Hd26JmIE6/Zjs4iJyRlMBZcSZntaKMc2fYu8fMudEDwf6fzqrRnH5RpAAg
VQJQRCuYZJl68v5FkVwS3/MDruf1R2EdHg8T+kcL6V3pnl3X+6+jFESMzSJ4XGpVKaG5b/S2MVWG
0BFu53juKI9DyJ8NXGVIYvhAoKHjOKi3fAfK6vfIxzLRwofuTH7/czuJD+nv9zjjfcsTbYanHkqK
DCR1vPt+Vuxt6nhYpVpwlHvNVLlg1IRP5GMFoItUWL9pDBKAbbIpwrCtEP3IwToTsq8vAFOAyoae
bqjDnZIlGv/qERVb7o4vYvr0irsU/v82sV06tnSiJtnWq6o2r1wrsVhXKSk/kdvVYfFXEkN2Ya6d
k5PFXmsd7eOcLMTlI6SFfhew/rLHVv2ydFqsc/yOQOJ2DZERw64KDQTujTsxgcj+rkrHSDhvMsX7
jvZYJ3HA1Bckvg8A+okoJLJT4EGi9T7WXBuwOmm22gmaCbw+HzlTMRnhcjBnwaOxAVWv+JIVTRVn
RE7h7PDxYnRDS6DN6QRHVTZPReK3ejhcmX70kw8GxNgyEVdXK2mPMvAJZBkjXLKIkp9IXquKVVh1
86OQfxdnCTJZuZ6aAdDV+oskoZPxxpCPjB+i3aXmzv4tXiZiv6Yf3wUiig94P8OTPp2cWYbo/ThW
6U7XKlmx5ewMgT0znoNaZTDBUUYgFttoWpx83wupFxa68nfjEXez1xiBumsKx8jsPi1gomZXZC0m
uZfMMKxKrgFJBmIALNude4dEFGv+x+VQemsdT/P83s5oK2oohyMVssv7jUnfYOnitHUXiB0lL1it
LV4ZUl+75b8kRpIXTr3JaMUiiLA/EDxRtef0O21T315RYktiJGLtjD1IGwINgkl1/sFZAv2P6AnB
SlxMAshF/4Tog3B9m4g0O1xHJ0ATHnepVP16/mX6EqpYyq56Mmx4ZFE4y6/UeQI0lG7/0uzqCcKQ
baELSlZVeJYANyjAX5dK7Y0NlkWxVQxPN3gXpa5kzPX0g7Lq8sgNjskVOtjVz+kXwSDLVcwwHhac
b0Te1hpsaZvz48UNaboh6H9rUuwT1UnjmQ42IYVFl9dCUCtn7x0ys68OX/77FjPXKoZBz+P0DIi1
X9L7032N0Qvle/MAG+IKvMrM7OE0BFu4gwoXYvLSRQHCXvc4uc6Cj8xxlBgDKq9ToRQ24tW6JfKG
4zmwVspeW9r8NGo1iToIOGboGWaY7imZoQ/TpZHmiVaJ5bb9rMHsqxgKxUrb98AANdWm0zBoIR85
+72+naV5/rSvsLf93agzzpPTB4wvYQvtoHQMysBL4cdDtkjQLp1HHjHxTFfv8z1o7K2BzcQLmLJt
Yg3Fp+uliJg5IKgNDuNxBD4qpMCd74sbleDqifLP3rQXlixqkeY4RzJztqmo9NZyj0Zm0YHvOT66
c0St2zNaa5hVR/PfnJ1F28eL8AstmLs1K9RwU46gOUY7LiIZNyEuxTsExnCClR7RfxkhZ5V2O8bp
LoCEFkXq9nM14MbrfkRKnc+sjXn4XQSFrWdDgfQFM47jPNOXccCZbA+aPEg5L6/XOaTm9AouxtZh
fna3PHUtD+i1FA5LaSjdPj7xMJ/c2l4wlv59TPkQVeDF3or18G9ejddN1VYC6Dh7Wga8ZYWEAIcf
o/L2qD03H6oPHVUQtl4ZWiC5YYJ61Z4z0XgmIDU51rs5qGyd55WSSfWYFzE1bspOL/gf899nROMX
HJPVH79QxOM6D8zG1yzdrePV1dr5fXpGKwE/j1E8J1A9w4ID0Y422HT9pjPM5uvpry4fX3WrIFX/
iM/7itOu06iXMBG28l2LXh+vtiOpjdLP0dUXCmNaIj+mE+cX6bgo3HHQvhpnY3RrKghm9VfUzpqB
XD8Ghwfd7r1ArxP5VVcQmaVTxGxPQJOxLyIGl8U0hDlElVIr7v30P8epzPOwNRswmZn8lp5mSfqo
NQFPr1MSIluWevDsDl1hEOAm0s8yZzcGuOAxQpmk9LjBg9zfiqyP3Lk3c/pDeP84fQuhufw2gxOy
kr6Krfh0DimTyMYkMMKFKN/sLuc9nz3RPgBYcxtMlUZ2/kMdgSF/gDZPFFf311UghT6iURhHlstM
Lgw+JIibFs9/yv8GcN21gp4wbVWdyLBVE57aTUKmsZjt94ci/i5tEs9eWedD4sffG0V5QgtGcqoF
1wRaO8oBufC59NQS4D29BIsaZIFITEp6Q4UA4dCjC89S+BiG4z1SziA9cHYRQwf20yIjRYU1GYK6
S1wE5e5NaFh4HiOwnAASZJxxPWDJwC8NM7Aj9pMxBJvEBRr99SWSyLsj3RFRPLL1GkZrLLId9LKs
g49Oqy2qiYO3yQLeoYRywCMjvg/1WLGaYUFewnT0LxWmB8Qql2iSg2aTW9svoVjO3Kiex3+OuSDN
dGJpz7ukW3kyoRpKifSXzi0qtNN+Ctbk/+bUKb9podA4kZyGWdr0ric34O1K64JvNcIu9RYTqmX8
NYoQFbdQ6R/LYMeRhAqzpnNsp++H7S5/W5c6qjW68h/52SAgwR7CP6vVMm3wVXf0tsEFsl12+VIm
TYnU4yvSSHz7kj8kvct44LbQgo24hh1OZh5udmJDcQBBKtlprvHR5n/5zllY7WLKYbXIxptHLfvM
xBQQROpkcQToEH0kiCLn+kw/c+Quf9ARtuDsHZHYq/Qh40uneUvssi9rbcoaQi89riUXeSs6eelI
UbTjr74JfYGvmV2COiuIiePEBJ8deUBCo2sq0HIwJR0iAz++O6MgT6npcCfkB9cYY6ZIds3IcSD1
JFSlnvbDMhMkVQA5DEme9mxw616X6/xHKI8lKv6EGoro0iI7QoQ4AKfyT8U8mMEoBLQLBRbyE1VV
QF3jxQbVOgxc/ohD1UJjsJVD+bLKOb1LLNKbmOClZaNACUoGfXzu3CvPAyEzFhUA2KjMmGRjwxVc
jsqj82iyBwGo3Z7SMZ8w89U3MiQOLBAyjnaTi9vLL3U3BLC+YsBiKNUwIichQMSzNTdD3BOdhEzM
LZniegQ/cM8WSBYckgG4oCTRmoWWYUKeL22gev1TWCTDSobPuhY0JHxnT1I0UioX2kotC56ZX7Qu
Xn1SXrkQI/RZlVgGL36/Fl+rv8d0DZSa2fjqOriZt/b2dhJ8JBRTBBzjaUAlN3D0uBW8hIzk9HOo
T1mNb5PXKPkU1wUwDcJE8GR4RQMwNBFGn9A6Jn1plJ4tnVnp0JSX3qZY+SxpmHXK0wilws6r8s7s
874tpd0NU5s2FGitS1+z82Pqe9HDmV7POyIIhnIkZgoTrWtn9GVbiA8UjdqS+/ljoxSOcyPX4mgA
sbPMTsOSp5yrtpuiSdFDWECY8JorEwPfNUOpIkoHI1cOv/eGNm76oLy7wSBs/22kB2f7M5U9egn8
ekG/p1JYbUNDn+5WfDDpHWS16FaY77vOLwFQkqZ4W5+jf1qIsHoAieOGVc2OAJroP45sBXlK/IxV
wMr2lgw8xwaesQeAAXspkbKTIQVJFxBA7TVQmWOJM5PgGVxuiyCtdDlIIf20Sk4wfkx2uRAqj8e1
SIyvM05io6ZWpaIb6yaeXZZpGiwKjUOhznE+Ih1u1VzOuATCvzK1b8p4UyXO3XyvQ0h2Icg+pxdm
66kQyFC+zwjiRLWqCTEJ4SbDnymMaG4N6mzmXKBx+XQFQdXdItwI/RRNzyPNQ7wcS3wpm91Uvxje
4pCezh3HZ4QFwns8r5vkMvTAkq85ChRa3jIg1fYl/1MplVG/FEnoUBbdrZozq0y4CZAnxJKWFlTB
6Es6EnbeSfiuAggtAc+fCje39SHNKH6JN+XFC+rS0cwbRJcnfDn7xRyWCnZnHAbCJlk5yg1zps+Z
SDarfEXdLn3GKRrdIaHaElC9Ol4D+yJp6TtpnsDkMUXE9RSZscsZhGvri6o3w4L3FPQrPLDkHgaB
XcVL5TTVCeyo3tMVjRstYFKkw5BmomVsBpcKmcUIJ13GyTF8wGhoyvFP4gO96Dnr5JV7fmpzLTGR
iCRyFJGsuF7jC3dspZhP+uVfL1TvB4Vo3THPRfSAi1S5Z2k51FSg1O5ZbymLA0mAmGt1lhoLnpNY
KW2UT5gmSbqEjnDA50RJSonSnK22C1znsdk/Kjx7eji76unM+kMWp6y4gzWR0YAtDyDyAtXxT0El
v35vDQNDHGlGdS7BzblhNpVdiODBukP1OYRQZMYkWIzQFTvNUOIiSw/DyUrtskj7jN2EbFrgqq2Q
BVY3dYN3LiU60HWUaltXIW6kE87FcyA7fPIx4zbwt17Fv7n4JHPdzA8m5d91dxG+8olSKJo1C+Ah
3r7PNrmvt6DvgEM5X4mXLOzU1AIHWLWTycjZdBYSi2hRkY9opvAqBxuhpApCOhWI8XiCbgYXjLR2
CADLdpljuSteY9KpJf6D65JFJhKB1ItAPH3Xp6fCl0fIl895GmEqvVm1y9ikzLtCPbgLCdrCwasL
mcQ14JnU3m5IIsk6HTHHYPDm4NfRNYIOt/ExWCgZ2VITLdE3LEUf4DzdGXxV+o7+3nyzGiySw3ya
5j2QN358844kZBVuBkYo61S3gGyE7gBhesF18Y/ci/J5/qXsMGWD2meZQnOSXFVDaeEJerzHQWYl
a93n46izVAnmGsFJ1v2Fa+cH22w1g8LfklrOUsk2jT2YPDJwo8TZskR9x3ugS2hW4TAKrraumY65
HG9eiMcDK1S9JkBApcXr3aaqZEptQMaQQVqhHvwx/eGS4uG+Ez1IJ+R6lHV8iiGCn2U7C2ammniW
TiXHVrfbp5ogq3FPmZAsqdQlcIlF3QbyTR7APzO60hi1VpE+mUxZSrAw4DLoBx2xFccmASIyIZnC
EgfYUqYF5ESHp4vJHV3o6Aer3vlZvkdRXp2Biltaw7wwoe1JpckAbkyztsj30+G2EAECuETPi9Zt
j+8Wnr3E0RFUyGFAUPziMLpTvholYITJf1/c+yz4uXnqzgjau9mI2rpp+lPH5d4AZXzSXcNkwGu5
4o6nAYkX1zqAf+2yRjFOHqjBJc3UbUTf79ZBYPZBbCmrJ/O5ZWfGNwOrUvjYebJ9Sc3AlBlSiIDl
sicJl8pG5W22dp4dPVEitD5fL/v6SQblmmio1sTqCpgsamAeRA6mbvum62dfd7JVdVB66dU484mm
3dSHeMrXKfJfslccGto6517Nurs3yXfbP9GadAS3yplTCxPqaxXydljvRTbT5XOSn8U8Ikwf9kG2
fJ2wExlPNRD4bhsT3OV/Ngc8M+8kFsVqwfG1aeMca7g+lLCyqyqcUpPmil82xE9deiYnQZJ6WwZk
RgfRwqaIJcdwf0zfNjvUvvd5eR3hEsphFPPc7vLSa/TD1r4Er3T3chtJR9u4r8hC6LvjOrHIoyh+
578IZ+Tx/PXgEw+L6DGbSwzwXTUQjiWPd4zed6m8XhksSWg5fLGkjNZ+VMeU6n8j38oUeykgUqdy
hfjtU7igopkh8Myl2+fYkVOPbpxk+rBQZAdpj8sMj0i6xKOzCtVAYEVtfVKy2iOr00LgbxxY37Kj
loCAtxYjAQcdtSLc6xPGSq4vYucMCgR5k2nMUQo3KCxK2h2bJ/BjgGmxzoMsDwphiQKFoPVH0pF2
FF+SJMXgGHLdCT33pkLZ8YzIpGr2mUKKqXf0D8Al0Igje9mbiVYorFYCgclcNP8Upzsyd75pvfcY
X1DdRt3fhN6+FwLq4KDMslu78WL/8GPdsh2+svPDDaZLNQL3ps7VPtNLn2L4ywc6Dd1seJAxNB+u
MbaES6NbE6eKGjYATnjGuAYQXACbhvj5Ry4vJ5BqPRLM61Dsj8gbxinyKtCPr8JbMnkLu8H90ga6
TUj/M5dZz7QCF5t2HaT1XkTx2LR4dnNXPgocPfoFEKJAnk44/PNe52mYLeSTqX4/dc8W+R8OuZRy
DK0rtrRXoklq5nj9RDhBEJkgryW78Im7YfrNDqVfvGVPIRxYCw0hJwqKUJ/oBVL1hVkPYCPYKhn4
2MOeonRY1P6dkVpRC7E3jDHNDktxDBEwMeM7lHhZWHyFm9Kw03iE83NRHnGv80uahkJWoqTuSGX5
1C1FNNGygiJHTW9yV/5INa0J3+7AxyQXwErAW2VL2g/vcFiDcVTULu6LsRkdXQ953CQOu+q6d4wi
cBlbFBMvuFWQtckxDTXF3WAw7uqca9DRoVvzWmNrTvFYP71xJOhFrCkkByYprmqdvsiv9fAwZ9FW
hT4qUCs/2KJIizCsv8OFiIBfU549KBa8uRlKrStMyMpv92FMr8q2qSPK94OTIIg4TV47q9t2aIdJ
2iLZWS5toAnAkNlAGGdynHmWUHcdjoTvazccGcnU4vCzfkCcgQPml32B9prUiKRjs3JxYHwHLE1g
vlEaJDAD20giUjjSqE3xqlGkiIr4vWpvJLxKHZIUNKygyCiTYATmlSOLzMYTr22mnHcMYra4IZpS
QXuz6O7fRFiGYedzhWIj7ZCjbQqPcDuWDw8yHJ+STEjTqBb8Qkdmwy/HVZ6ALWXSLPSjGvhrnGiF
vXBQeAwBoCe/einIKnHgw4DcKjOUDxG8vtxPwYBNXd6tYcaim3uXV26yJBjiuVYl7LtG17dzvI8Q
G27e6YsFuTDTgEi/0a4dYZ3M3bQYEJtFxgyqi+K4M7FBgJHNX2pjGw7MyZHtOAjw8SxXTvOrsYbL
G/z1lC2aPxVPbL0c1AyowjZ88hnq8RfUVHpy+EETfz7qAdpPdjr418q6f8cp+nizGywQ8qMMCODp
qI52c8Hlg1U+TXDpK8smrZ5/yr3qgfvbwijSkzlKtnu/KmtfXaKaDpxbDU1MV7H0jmS7X/4hpaNd
belKUzink6HjZ5SRancuuTLehAYIT6xlaovF3YNRaB92DP5hnSwdEa3DL8v63z0s/TEZdB1JKSMR
kEsSPJULXm+K49PRxoQPySl9kUqDHXohTTQxl19HUIi/tR7O6pCzTk+WHG6qbgNJR6/Fn0CztKcL
NB3AGmYzb8EGmiKalAtcKnYuTJOVhRYfD5r3vaWl3mHsDyChPQrPkMbms3gAUaJDZ5S0ZW/hypPe
U6bHop4x9kv5L/809RRvzN7lKxyuLsjfJQvKMLrBHoHHSkdyKAzcgVfhHwEkjcwfLCdQdzcQxT2+
9Pw5bctChP3D2p8cfXtpG2ZqzzwEBmydIVsnVUlWyWvFctDONppuBybTj4IVBXLyftwJFZJHs9pS
hV21IIlC3GtVw7hU9ils5AYGbc4amHLVUjlw73lhwg9oMvO2YBxzrdyb7KMc18SKMd9rIGwlXOTm
owQuaY3A9beYUH+mfIUYWp874DheP3kY1TRr1dbmshNcJiVpWQ/MgmcH1bMNmUP2Rk96KhbJzsmz
+yTnqFxUi5emsXiIO9nff0ff5L5CP1P7zORSpKge+0Fnr6/fSpSOMg5z+gQzmkcbOVn0xKsWxm2c
stchSIDodXDPTW4StucUdP4Y+D8LfHv9VpQbeSf64e/qpV+iq2G97N51yiB4USn8fnkdEB7woJdx
Zu/M0PV/azjo9rY0pkfNA3H9lnXm0wxPUcXHy/ihBHHQsj4ets5JimBopLiHdmNQElWFeX0KoVNe
07iTc7ucba+EKqLYfBYkJzqpuocumzTm+tUNHEv1l6DIfmH/YNGA3ftDVxsCP9JnCs1vik5vG1sl
rsQIZwgOt9jKhsjvX02QJEHeoJ+lCyJ0CzSMUiuFHeddGmjtjlonS7Gp1hdfvwPP4zmVr5pElNP1
CeKxn357D0/ieJokRLao4i3S4/EfeKy3OVBZhfSAcIGl1bVlF+dzHRIFRStO7p6Boi68hRLT52jA
ycLOajx/ygToe2lXHO6qvxvUEgqEWPKT7WCjUIm+9NTSld4v4YBeWh4ppzruM7R0drbvKHz8q7nz
bxtGuTVcMY2S+pzflTF0AL4JjuE2fI+nNU65jtYP+40k96dgTniSOlIVwZYKuX+eXCTnci1pb4DP
yXTIjUw7fQVY3LkjE2djSuFQuGKixL1yB766fsrJS0I+5aJpWR/mUwbkA8P3ULibStsw+XqgiFae
9H2gbHat4QrIDoD0kcfYN7dfppt8Q+RJJk++b8ZdnI955U9K9Ve8pz8xg0oHy0ZbRs/R4uXztDzn
3bxx2E0z2GQjU+iNVbfZzzEUdF5NxTRacy2Qu3i+9QjvFCdAl91PYF9aqDlalumf2qgJ1NFlP5iI
um6RoE4zjhEses/xkEdMj58lPCyxl5fFKDlHmvGr3+FfWM73CZNC68Eidi0ZozUSlCHO71AImrXN
XJC6ZG/T3N9iO4RcBcUxkTDer6tJYDesKDOtCxBc5y80PQxK03p6LgY6mDKZwJ1KtP6iDJqYo3sA
74oBb66tGLdbjdmUTK/a29lXcGUTqL7irhu6eUe8lKEvE65g4BehX5U0RPCWM4ZkYwe7KZpStbaD
HqhtXLmAABEHvSzHNNorfWlvjyhM2t9fcRJ5e05HGezGNWF9Aw6R6wxO3SMEbGoDjaqAYqtrZBgi
jdBG7BIOg3L5JR6iBxX/C85GomqsK9Nsv0xfxePrwlTQyNZe+qesx6NNIVhKdX94H2bteXENFAah
9wB0qqIlDGvhwhsARlg2eHx550wMqZvbm+e3w+mTnSqwdCM19ZOaSKLs3GUps2aBD+2pOtoH2Bx9
rc8wdMbY3j+se7Zw+lsz+Z3ZtX/3DHeYCScdfN2DDpZVgib+S/PNk/ZLY86kTU4i9497CveB8Zkp
el+rc5B64aFWupEafMror9qMudJ6sDkfqlqcf9ypKGWdhC/hGM8jewlSjzCWkT9u5X29iJhlBnpc
xu9RH1/RIxIS4VNu1iDxopjDujW1UcVe22DaF9PSK3ax6Hy5ELW3XBqoZzlRRHCt6wbBR6jUXxdD
fg6bgf1Nd73uFvfdw98j7QVb64Nd7nxCJ64S+Cobiz+17MSqyp886bbp2fyQzyXO2+UOdWw98eZb
ImBuPDzZzxdOygWxlzo20OT2PIDXVBdSRhUimb5NTng6emK2A97kcX5mm4sg+a7QFwi0cU2HJpdk
gJLjWQMe3FavrnBa7hkU/8aUuz9973bkOkUZ23T8R8Vg8TUSdsAevncjRit/oo3XcaCluiLZJy8m
cme1N/xCbts/fFhs1fGr1y97ZD5BvZqHHijG/btLvJl7GKpFjwKwdgaMRGhkQwoZztVdYZIDKYWj
yTP3DkS12cyAmTIfANdMQ49FfinunzsLOdAIvdGbeKjmnqcVkweM9mO9UZDqLIsKE8oFyJeRiUeQ
4AvfkKDotTxd82PgusBfFXCHF3hpdEInTqfpIBVqW8oHN2ihaxiKb/poPHw542amI1vJNX0/rNxn
cx6ZN4GOMzmeYIx2jmibhaEg6vbjpMAC0qURrqv2RMeBW9tXlsOYMHyCSRASGgwp52cgFM6573hb
ppDUp3SJyVJ8QaQlhFIo8xEsy4YrrJhIVEfDTDLyOqiox+TngcsYAv4cL1iMDoAo7Q/FgxS1KNU4
uCX05pfef68QjKAiPqpMsmFsQC0PPKKSTc5IMGPAig38xgAqPMwLj09/Iw3DAjVKUsN/sGiyTNDp
ntB8vGclRf7luqzzQu1rxWmNdxlampvXZgUVhH98KFCHYmCp1q3iVopNiw8id4T5Ky+9Mu+HpA9f
EZ+ubTrX4V4ld25eDuJNUGRBTb9LybRxPJX3zoCtE1wb9GZJFmxEXfOmHWyTBb43M9RkAI45fNa1
0+VuUmmd+CIYHzOFy4HXT1fDfQeyw24W2PA1/fc0X+npstw1JWYhgjPu6e1GFUuV3te2AYWVH06a
Dy/Y2d3Uby8++Sj209rcmrmjv42V2NDVH8IGMuFf0/iNwwOQWcmQBFlK4dYtdpJvuKMPPQOPuzWE
M8D4HzXyArF1y8L08zncOmXGijjr1yumExAGpCKe6xA3a3jA6Er4L4tZRP1BDMf9Q3dbukzqr6is
thZH1WK3vOKJGJwnWefy58U/44JpztaNbWjQNICymcMiwboTv+sXUx08uKjRSfeHCkSxM7E/fgPY
Z02vDQzT1syUvGCImMH8l1Mn8Ux82z7kIpg9ZFTnfi1rfx827Xle5SMtXadcknWn58F5yybXrphe
Yn7aPBFHRCYL7P8b8srt8u7Lzt0Mm7/wBEX97u12uCyG0p0kOZ+QMq8U8nn5yif9COVxABwcAEN/
6emtbNjQ2hIGyFH5hsooUEL2oFjOVK2cmKZB+Q37B29yvfYr6jYwHF8dbh5B8KuHQ3RCSWXeJnWR
Bs18N7hz+VL0XNNLSKtIMB2FkYm+UThDeieGMAay3p5BHnr9xUZEb7c8uQZaR+0njKCzXrS7vIc5
zygl6Nkb8hT0/eF2PYZSDtrOtrKzarjnlAeTfr1YGLxFyzNGGzT49GeO7JvrSjErqAnhHWyP9oQb
MY6lRQAA+zUEyT8yrVc+yX2w5nKRgvr8wJ7ukATP8tIPcMU7f4zecGXHTsp3P/jvmC4m9/LkMSC7
bEvEYPMToVXcohEC3qrLcv4FBlqjXEeEkL4X+PlGnVIxST9ucI8MG0dhp3zcmBca2s28sZAL1zrQ
mw+6aWvQjuhNrfMhuxbYGaqnqHCBctUqOC7nal8FZfamvQcCdH6jag4hFMIr7udwRW0hZsMx5ldV
/EzZ3zkr8xFH9n0TgdEAhTR46UXTpBLIC0IiuOHv9/XJ0vr/w4vri5IaRdNAGcP48a60W6XxJ7Cm
xjee99351rtFEoQvdHY92qxVlPJRQgLQHr7mKkfRXjbm3K6NiOOXJG4SHi35Sy1N5/NPxgCKL+M6
V3ltaEbHnWBX0Su4VF/GxUyJcDGKk4+ODfoxK+RcXr87eH0I9ldSeWrZvRd2JcvFTWBA0f2dWME1
Wwi7Hr0dizIkMsmIffxy4QewSlFJDO2b1HRu0wnbjCUQU28qLVVrYmpCGQV5Whr6xbpIzvUbwAek
YvXovmJ43hA3DUb61gv2cy4r0JXZBUD7
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_21_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_21_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_21_1 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_21_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_21_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_21_1;

architecture STRUCTURE of design_4_c_shift_ram_21_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_21_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
