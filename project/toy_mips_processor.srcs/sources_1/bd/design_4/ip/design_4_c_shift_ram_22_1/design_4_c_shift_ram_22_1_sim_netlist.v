// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_22_1 -prefix
//               design_4_c_shift_ram_22_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_22_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_22_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
QvzlQby4XkhWUX9ckW+sTRshNkFEjHvPnCyoXNbDFPAjmc4RRTSiS951Bxv8PiRhGNu5+EprFalj
wn9pqZRHVtg+dDCoBjE1MhY7KavzHQ2fSMHK56cMYRiTXXm6MyhYwrc33tybt/Po7hcxYyr62oLQ
iELvyI8ocvCV1t+PVJxc6VncVuDJJEaWTHne5Jemqna4S9tiRNA3b705tG6nlyHTSh8WY4gxKzTg
JF3Qr2aH9lPPhBxgP90sQTw3bJ9VSUh+1GzL2N+oz0jQj+IX0PNtwQzje8tIo4BhbXzGg9eRGhfn
oMCIFZceVe2sOzh1WyKtRCYlauFJg1URw1fEFG5YXIZoof1TyLVRsv6Xwigt3T+KFNfcDRd2GwwD
bnDuS9jR3r2xLiIE0fG2eyOEJqbD8M2K6v2BJKZrv/1+SMcnclM4Pkm8rZOHurblzAUC+RdWdw79
pDgqKJLXM7iunNSz5ICqZcc5nxrB0dav0f/M1iRYNIM+xoSlKjVghkME1zTGXLGUMjrpmb15i4eS
zyQRiqG1BhQNeMRBRnDs7zMGzOvY5h6WGCgPVb3fFCWBjp2HUKkzPjKkBaNcL1KZVOuiHgzj6Vrc
52/GamLl6URa0GfJrdhrKLKz5klTTpBHc2DClegAL/O032UW9kRB1KNHvA175+9PdOY6iavvu0W8
xjo50KcMgNetFA0NOth757s6oODIGCaYSwdJABswcnYXHAL3O8GVQA+9nbwjRI+fPQB8n7QXw5hl
9ZFsPeMTUJPb+1HW/CfFmoOBf/XmWXSoUDY8RLS2Ugizp4HW8LyEF+6G14/jPlqUkNrIQUBB5hHL
wsz4MqZpuGSZrkmXb4245rSl6mrw7zgwzZp/YV2p5DEACp7gtrNmhNdYWxR2kb2H+m3Mg3rCbkyi
2zprZyk/wYLGpL0CLPZvV6Q3q7/GB5IafmeLB53HdZcQR9/zqI4t3XCi5BYDcotJopHwASeDGC24
IGKdduJTj0+X5MaTmmlaTriBQt+iDDH9m1GKeI7wAwhu7IbHtAg1uww5Q9nItxVhJuilVeGUd36L
VogKgy/EpQ5B9w8Dq1Ap/D4ZWbvjnEt+WQS/wObAkmTYBECa8wYm/ljplT1YDFXnId/6CheyaDzu
vovidKEbWYBr7dx71TIS7oS6BTCX0F0HxxLfx777UXmYcgLRF3WUtcC8457sP7NopOeJnoe1I04M
TBi1KzGgwWhklQlSAW4Ycw77aPpSQLN1KYIyMUaSmUwRvFksknwz7cWPTuIpAGaIEPiyFbtasIkD
PNSJkpZw73k1CguD7ZOMQMBuzawzoTk9n4TaquAt0awuKIFoW8JRYAUblUFQIq+H0WAR8N5ar/Jd
6witXp5GHBS1coTupdr3ZlukQrOtatOs4YvTo88xIdkY3MkXEtD9G6yhajfQc7VblfEKF8Tscg12
7uLPFNzz+F9t/d+YAcXQcWiLgFYO9EDtzwU+vA+L2yKEnSUdkDzo0QKfzXWtqrmMRdixENdKt8ev
wXpBE2Dhu6xz5lqVZgj61ZBoxui+rtbRjuNfT2p1KQI/JWLPUoyi683vekFxmAGS2HzJmrSOtpzM
ucFsxvkysttoMoexQglXnkdDT1XhmvVZQ7HBNBpIYeWtlycJfUAadMbeROrAUDYuVSc6TWXkOBcY
TiCVN17BhZVWrEqkGStdUQX2UJmkhK+EkrkfsJN8swSQOjPeVjjmWW3bN+jYNzWi1xXKuKHD++Xm
ob4WvrULmHlCi1Do/yDD348SbALBg6LXlelwrg4LyCR9ZdppAuQ9Idfp6XuK4NLiNFcTSpQuU5Yg
vvsQzYGqtaDCqw4fYuV9OJloLL2R/fHgFWuVptDBbRYawE0ZKKHyvtFyezt0o1liPr90PTRb6S9y
hHJ3mj+3IOPF9E6TPDMAps/dezV3Z8BJRi4rtsFL9oekz6V5+ql4cCtyPUQm0ww2v5CA8L82tsIR
RXx0KjifxL3yw7fCbn9VxenC85nQv7kTDJMnvYx6yyt+4C0VvRkyHO98jpuKE/anGokG91nYJ7yV
p+RmcWkKLPTiJkN65DmQ+RcUzi56Ti7fJg7CNBU9aY80Pod+NvJ6pVTI+G72Xl3T2hriYVk1Xbvp
IKIlq2tRyRLUDa7S3IqfqRLVSnNWnnXkt1SznCtYuqW9TgRT0mzvdtcxeWX56quK6SIPLoQgerp8
YhohNlBRmGqEr8+8ZTRwGQpv72J1KLEZidtFgQKWR6/1xtMx1Oo3hcWWdWfVhjbOtvQCYSZv92sm
ZajSlH/H/7A2gCwSb/N4N9Ju5vesrpnoHP9+j9MucaQYIOzzsIWYliC5CSl2Ghx9ZXh3u2UsGlvq
p0Ce0PEi+HcRtLOh35WXcXc1sfoT4N4sR8ZffsouYHPmdADFiRfEh8S8jyLbsZGcGB22UXuBUX2T
nEGBGIvVxSBwx9a+a/PcQr9rfbG4kz7NpCb4ashPdqA+4ma4Qj7MyxkRfwyZbzY8so/zLV3czbpx
CzU66oVxIVL11KtNid+ShVIu8k4JTzkSjV8dqCtQ1khxnxkKxozxgXmPpWVyea8YgBVv7r7METLh
an8sX4WD11kd2cRTqPzj4LIGi7GySyFW8Fw1poZWthwlOgdMIS5Qzlz1jLUil5kyoqcUXacf36sp
6pOrXEXNrmVEOwg3ihz3zG8Gok0JOeVsZqmSGrogHOsAyc9pYSdGptRPj8FAiARXDrEK0qJgk8Ij
uHovlBpAo8/myPypbb2kDRhP4dh5EjzedtACo5nUjrbHYQ/kXcSDka2y3m15TTOYSpLabIblADUZ
uFl7qKbWtWWeNvcDFr+XjapM+ZQGc3zzyCDB6x3tIfWj5atZe46HvwhMWTexytCM4p881XS+nXAj
ey2mF6Ta3d8C60jCFeM7BweybKEr8ni3fAkcm6WmzCZxnyGPWPikHYq6gE2eyPWnso5HKNdZSZUj
Na/RAKQ4jWqfUeq6MS7N9UefShnCN3TKulDbzYcok/wtHFLxzBjVOik52tMxQK7OM/e4LaDBopMY
EEyrhMVVRxpdw8HTh2nPdsbX0o/lBDgZ52VrUjAsJ9o10zCI4w6hHiAOEqROX43TjUwvcAnjGMl4
NdsmP+pq8I68lbrAR8/g1di0B7BqKUKvr5vWTkncPj5VI93kGgvMeRgJb75njwQYY2H4jii4BBnF
TeEwRI/cuYk6Pv5c2/Pmx9ZGIrPq0f94CD4cALzpL173zlWKlZM0OmSkyo/HbARTDdP3Q4Xix1dN
TGQHSXcwZGJ+iyw1mIa/1HbFaJRbmngqSaR4EHfiyDcZSstyMVln/xVislfzyUL4uljSfJiujIxF
JMPR2wgHQKiqtm/6CGBjtr9rkgoxjNLhf6aAnOMJH3mcOhf6bhtpPFH9UK3VDptcXb0VenTCZj7M
hQADUmkBgBr/XxdcQBNo98xfI9LXDXrwwcSz/BzvSQ2O+9Rz55/+c/bLYGqEbyo/nSiuLYCmyy4y
UxQHBnpUos0gwWCXvSXgvhaKin5rPMlLo+fZEiHQBpzhFxXvApUgsyi4796KtnhTUP9gF1C3765L
aaapQHxsUYLkPJQLgx40dQIgX1k1UL4TA5Hh48z2+35w4zUZNCv2sRwzI+JXFrL/81R8wepe9KxI
8lBBBXCN5HePylNsqDrwrBNHsBT915mwONogEH9yVo2F09rpzv8+iiM/sITyuiYpnfqDmEvSstjD
xWMKrmEEDy/6sUXMI4EDLeIzCJ+YbFbs+6Y5wsFQQKOG7LgWPqE/UGg18lpgRpQDsHw6J6eb1s8v
JNZkFt8wV+DzIrfcDCWyIuLS1bWVyNkBzW67o4lB5Hz/aLfxJEAeaTzSPCG7uE5friivAyKVYO9L
uZ5J5BfT8xANYTVPVEWTJn4XydMiWQKHrN27GSt1t5JYeVuMW309qqnXZpU2QZuSP6qVll8TaCYq
2L9PGBwHG1QsILroBI19LQy2LVTyH62uWfGXZ02G+ZGtX7heLXC2Np93VL/epTyy+eKU4sJGndqt
BCyrPnyWJSniA0GWO1s6eWa8cFkreypU+1XfHzQY0OK6cSl55ZqxhOXA7ATA64mt9lnwUds7y8TF
wuDMnkSMxiUJmrZitqy0r6+v+qylwQdqjrwS/w1c5yfwy2x8MZpApCrCN8BCvdPFuhTn1aAq66G/
D0SgswIy5lUXdu+POR5fufjmcoMiS+/Bo2Q+VWwmW6VE3OxHo8fyE9+vjozsifB9zVoHSUiadAFx
JUaTQpFHQ8uRJ8JaBiqaxUR9SHGEK7Pt/WFCsV3NKqgGIwtfrjBi9//50CAUukZzTsFYyalrksRq
HaZQNrxbytPrzcNzRe/xDIw6NpcSTAOuMo4oZ/PjmTBZ4UmikCTopCywWH7+dp35U005tldT7Aqn
UuBhEQ/is8MQ/VgvsIbnDUXv++P0sOLqXBVqIl8h7zHVO8xJGVcNf5kY3Lse13//UUeml4ofifHX
BJd1rZuIwKIWbanIp1hwOFTSnostC1A1B0T+VnOS6XKwJUGVGw4BR4HEfbMiHEMisNXbykRg1wCr
hrtydlhGuRJnEakNUWG9QnnPK/X5daBXTQBkHwExQDUt3YvBZTXfmbVWgJsMFgoUU61eVxlsp1Ym
EbvCZa3bD7+zGNAI+P/AHkqkjwIYmcaeVCu0SWklPiH8RceYqLRGDhg/9QRhKDJbQN19dE+pbPJy
huzUrsTfdHaXabguSF2IU04XiAkenFGhHZ7W/xFw7S+gFS3bSVAuCnsiL7b+yK+MsUm81bSOIHXH
YNk92/pNz8L7vBD6JI6l85w82QPv2i8sMaLDzO5nB3GJ7wpYOHIh5iNI02aaOP9AZxsgrEwOgJ/Z
o3aBDZZswZu42QSj9d33ySwa8+WmVKnp8AonQ4ugii5uFiOnGDmjjYb9ClgO9v1cwself+gmILz6
c2XdkkYwef6wfqF27WgrQ6gJpz1OLhVvMfLZoTM0hD7RW1pylqXghTXmpX7lbe3/uPhy/wmWKsH1
kt2jU/pF5suuVh+22Pi+zaE7lTgLIrYJkFdqhuvU8T6ZS/nHws9Z39OGrRC+VtxwaT+ao2QH67U5
VPR0B3qpdMv/+6UPhmikpPQOh5b7TdAQNQQmuANx/dXrPtPdpC6uwhEx3VelUDjpux7SyjOTUXgv
Y/jEMIT65x8zAoL+xBDjoDBuSZ9S1yZP/RNgE2aDoQHuSmwBd2JHnU0HwmSaH3/XPRw8v500Cd3+
wrX7vVXMmdZn29Fcmp4gpx6Q/KK+EUxHbowWuIHj+/TZkB8rBVFBwcNBVr6Zq909+8wkRD+gVHKe
H9InUZTkhoCeugHVjFKFYtO8unVp3Eqmds4XHNz9Wro4OYXudfowgX2gZ87tgfTu0GAayAnVQSga
oxwhPUw5X4jyQ/2zW0DQPtA4iV+DBvkHn0ZmUhySiFlaaxGtzhLHBxm4EK3LqquEI0iXmbj9VJdO
b7hizauzKC2hOv3rzx7tRVxSL2av2fIP8JpXnAQ0uOKGmFi9Y4wPtr8j5agtQwuhfnJQ1jtollc=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
