// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:00:47 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_23_1 -prefix
//               design_4_c_shift_ram_23_1_ design_3_c_shift_ram_3_0_sim_netlist.v
// Design      : design_3_c_shift_ram_3_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_23_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_23_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
brjD0uXtn4g8zw7KIEVrI2S7qPjI6ltBBjIRhsXBJQ6T2KuFOKY9R/BtWmHJP+XspDMKreMCaPWq
k7HE/Y7y4B7mzceCgwzx7ctl0waE3oNs5WWcllINSzXXfvHl5yV6mdeDSXLs8bBAJFFet1gNYOFt
vhim8Q2NyiwDe3OsE0VyWtJct5zlZnfNBj/Ud7r2wPSTzOrPFd57T8e0rr4Ll0STJJtahYFgdntE
XAkfepbnpTevxlE8Q7dwJdT1eU6pyQBR2widxE4YTAz2gCrs0iAcpZEcbmlZP51IUOFzxLJyBRA6
h/KyWI0hLaV+bBz1mm57VvE6smV8j1L6iqx7XA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Nir5Gm0jvF9xSjdpYjI51axFKMNW7P6BfPmV9shZ/aknxPPkntdKssLG3ARib2jKyG8F9VrwQ0Et
fBQkEtVT0oCjGdheztbyrwQ+CpX8AF0Wk80ugJ28JooCKwB2Duu1fL2u/w/jWAVeggrk1IN/TZ2s
k0zXhFpnijXYbMnTlMRoT8A1jsfBEzFAogx7qSiamGVuSFSZPOv+4AKiOUk3N1yTz/YmYcPiGhnt
3NGvtLj3+o4c6kK8rtG8c6CboeguhDIN/FdDk1IF9+3b9sIdPrEbusP1Ob3xSQyg15dOkr+8D2hT
XbBpWM8q8yL8SdZPVjJjVJ0BBEQxK9PFT5K76A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4160)
`pragma protect data_block
mzt5Tq/CCqywNPwybDjRF+sTuTnXhXjgdD0GoUpzwKeym9cuXkoO7IRQRt+QIzTJcwg2/UePGc+5
bkvz5SZWxfmHn5a7POe01QZJlHv3QWUfjt4I4l3MBX25CpFveccV2+0PD56Yjo5QyqeSiG+RrbJx
MIKn/atrq+iKSqtpwE+TKK5b+qdYCWvyLcued3B6XPHPq4Yv3EDz998zVd8BRd7N6BB13fuJUCWn
EbUUuMJ8mkV69dsN1UKSghWVq7BQ8rl+6RpabYPGNLndqtT848nFePopTUI0oy09PWtFuG/BPTpE
wmvU2TkmrWf4kr1500b237SiExaqq2oO0yw8NoQLXdQhnyZO7wmn4HGze8zWlf+O/63o8pstk9NU
EJPUPv3GF142hiRcneccV6K0DGTrgIXFNKfyFAeoQ1Wwe07FgkVjlr+aax8m78z1F8CAxIjLMrZv
xcL7q9mCs7FfkRXI0hvz7zkYt+NYdnedn6DK02cKIQV1E05QBPEQVaMv0ITYK5DTtOEi1+O86eb+
cBSwchByi6a596CWHE7uBMXm3BE5L5dlwZpXZ3zWT/sVscW7cPKejdfohJRvp1DqTeWfPUFRj8tE
xv6md6K4KxZGc23NEGIDx0zONrTmac/ybuh7IurcU8Zoxb/UJmQ+cRSTpunWyGT1HrtLnom2u/to
VGlVQ0QoaVtUjvR5RHXOmUtYKvunzIp2OfmERhQ9xN5U1TFrjcUq9+erPTN5sfNMM4TFRo7HtoSU
vQrdo111mk72TiGj9rttC3TT/UyEtbVUZjeefmKAkOJDnmCQBw94Rv8SD3z1anw31F2JsQD9rxwj
HDTAgFWxthzUNTCg1pf38wVCpB/FqEnRI/h9xH1LaX4vvu9FWtp/F3fukW7hiMWirsfeAuslrrg1
/car+wq0uig3oY6MxdwCRDjUJ8+dehE5ZoFb3bfTZMTf/8nno1vFtxVTEmrL2RGnoCQlecq4uKNe
4V/NtCKRCQfqfJMmvdHETxuzyvvsuyNfGiLDg5PkSie96+17UUj8lbV1O9FmPQsIl2NunkHnrnTS
nniNPqnZmitIC4NSnZNt9zQuo/L1pw744hW88kmkTcm21xi+4rysiE4h8cm24BMLsp9BHa09wdfT
MowGuspzwMgT1x0D7PTgq0PDCjjRDyL/9B31iLpU8u6LxFwcegCRUrgKyYUsurT3H5KlcEO4czr2
Cy36kbIgiPqdGFakxGu1xcV4kb5IuQ8J/8FYIezy5KkCP/sNeJMdNHswkoKnglCgfmcCg3WhSLE2
W5iD2wh0nXAj77F5HzC9uKDImRyoIU4sWpdMBV/t/gy1BMnh00PK03n7u4wqT/Zizk6VLtUb3VHX
iPxO/1fpv9KkRw7N0B5jfItZcRRdzO0f/1LBOb6Wbhv5pil+ADzRvOC8WuuhKe0jLdfl0ZDhKbZa
KT4Pw2kcxfYhiu079Q6cDMlyg+wnbCBHS2eHdtl5J2SatgwA1C4yGv0MZZcdXYfvrjpObZ78TGpz
x1Kaeu277fdfMUAwp9r8clhx0EWYO78yyYnbBaySGdHtlwTt13dOfTXG/WMsA5sQztawpG68WoCo
y7jY2ZM2O9chxPZidb/akRWZATrav/eTFe0QctCcS6c8vCtZyO/DaOdBYnE4ZmYJitfa5Zrkg4vh
pTjnfpK9dEazT9PZyq7t7icfBygYAhm/lY9/DPuPwZhjm2XDGk5a+OmH0otL9kZplOEWDEP1DYzB
KVmDM83hZdLhta5q305VkFPzxA0FweXNbosRVgv9flRM50MAsjCBdL/Xt//X8A88krwkJY0F3eFu
R0Iayz0FXII6O27N4M+rDOTvUU0YORQG/ZzL/vT5CbV/zEiDpJlQ/KR3ZJN6cRZQekC1q/tigr6Z
AyE+ekH2L5x7H2ut8DrfQLcbsb7lr4xMUwBosFPsfNMF8NDNytoTZqdlBxotxJIsxHTxZDFkRcqq
OtX4uS5R+D496ksJO2MUbgmL0JtpV9GGDlCoCYNzk+S6oIJAKU8PO8lxEVUgLsh4J8+eb+RONxM6
7V0+a+SFsOILTjmtJo4ZJBif4c6t19if2HvhnotyTeR/7VJGCUoa57+DGz9OuvQVyDX2imJMzLV+
siTyVz5tzrOQyfcdfmeo5LGiQN22uXYtFa5jEpNFkamhs7hUuZ1FYe8RN1VM8Y5cvLgFDVopCVBE
B/zkj4Ahxr/QBNRKqG3GdMyLbcyhDR/wPGCR0FkOWx0nK5pnUJr6rMR6l0UMdNH7vz6a1QeMIOGR
+uTpNlRi5FR9wo8Ulzh7dzufpCPxHW1hatkfv84qP/CTMNso8JOzoCxrEMD7HOiEhCYMykcmvLq8
GSPzCc82OI3ydwRXPodgnWjSR3WD+g1MMpS4JnehkLS3F0kstkRTcQNQ1KNCNti7XyhuK979xOZK
LeVeehoCPiY1J71x2wvrP2QKEe7zMNtP/w8pupAuLazByEA1+WG44Rl5R8j/EXyZzaZJyTw1t1Pd
Cd4Ec7KbIqFre1j1dBhc8NNW/Fq9dpgr8DIHFiWpXXA2w6BnO/Vz/flsUPMb8+z/0b+PBi9eCvAn
ZQm0+p2AgKFFUcMB7V238E+gCa+4+FeFWbJB5WcWiKn2D6KOK/cLX6p4vphjikFN11zipQ0rfkUu
jb8BE5Ahc9g6IuqLgCAgp0AibpnXmVEhhgI0vEx4/clWPIYcrPU8eHf2ZJhV9+0JILk+C0aNSUob
2/0H1VZCpJp3hix0k2U2VPCUMWJG8gvfDZ4gk3tvGGKDpibcIOKo5d4s3gcwnI5OzAvbHuFLjT73
dVB8g4elBPhFwm0oCNZssI1VPKAOHJJVRQHUPypfxLLjRKoMwcMXU7xOuValbuAi7FVziLyYeUbi
Wa1B68ofw5NPA3q/YI8H1xG/480CDSRsh2WUWLoPwpGBcWSrbGDH1GXBQtkK9KKJ/u3J2Q5UYUJM
a+f5ew99Ft3O28UJvIDdA0QD/In5zJVO8F3q/G38pE3cj4Aj5Z7psT2JjZ9fZ5dT+gU0snfcyV+P
VHvp1Ro1hjgqzVIoMDCxCjoxIk6CUs+yZ36ZH8vlfvUesg3XbegvUF7laacmZfvYHqFhsRQkSTjb
xVvi7yHw76yQ+rhlP6BDi20R3/QsPYqxr7WcxInWn08rJ5SmakQTQELTLiZKJW/5Vf2YnWQOExt9
BhUsfcjl7OVJZT24qowg5WvLUbzj2/ETyYSNPo9ragppIEzDgGjR4yFSrgYm2ls+iy5ez/MHJm6Z
DyV0+86HsUdww4k7yr4wuYNOdKN7f9GXLMNNx/e0fpaWJyKuwy5QZ9GFvu9tERN3sd7T62tUfV8C
a5m34LfwnvdSQCnbPwHOMV8WUCovAWatgDqGC/E8M6287ephFMtiPGlsF095QOM/OlGkLEYZFPFx
o7SEzKs/9G4MIogfltu5p2KJvIahqdIo27MRyuDfKayuRREWdlsPWwKEbNdvWm2ApbxFqAA4GX86
4qtWVZGFDGfvZHAkkLozzJ9YoZ65ISiG8uprp7soqNtXZYdc6JY0sxqSi8HIsg3oWarpFJkaORXA
GOILqisdvd4k76KGf68RcHFrGHHyabOH7ICLoUISWkzwGQbiwd/46m2ycIyYRCBGngiuFa08B9ZL
s6VBGdCQ0TLPhnOXoP39l4XN5bSAeSHV1ha2mqbdwHi+6E4WLPgU6xgXUqiZUJnq3Ch8+JbBVzdg
PmrSJuvRwcRxD9RKfZtmydmRjuO9M6/wH770UD4DzvwjCX1a4k4CIQGBUHetjIAYg7M9B3ey+H7X
zr5B1PRLo0kPhZevcFaXMzNWdeIV43ThfgTYcfLRis1RZeNI2L5Qw6DX6h0hwJaJ1jhS3/1cAZ7l
qqZWzNJnVAw/JWo8REVpBpdhDtW0UGpZoCeoX1UQPqYgWuwfwob5CLpGjdazG1jrXI28zR+/kX3S
GSW1CD5FnxMvjQnqsrw4OZH6xQsp+hdgLxety6biqrTYWisoxyDNYkhdvNSyefCjHOROwapbIL7m
nF/Th1523/4RnAR+q6BrGYwYhAF9ECufL/ShRiFwOrrtcpvKleKHlzXtthknEm/myxXhSGBgWaxK
3YcRaDRN0ZiZDZKkIohkj86UGkXM7XYFMejPzrnWSnrdfNm7DyN9qmTfNuLop9rt1efYxv4SG1ST
bYGNDGfmxGnhuZwPgrKopJWIIgK/Bh9fGntPnMAS+T/92ajREKdRndMu/4XOL/FoCrn2QSQTd/P3
pcMNwfXFeaUQrzQCUMJFKMRo8PGTV32e1Unl2wDLiwoyq1LU2KWXRbajyRvng1esUSJQVzlrRBZd
6cFhpkT+vtVSp9TguDPRPzvEyySprqbeWiUJKCL3IrtA1BySF9UPHPmNc+lHABLtgPD2Qrxlw2Lk
NHBiAd6OwPko+KnsD+Y5QtHunCnC9ASK6eDtjxNKS/yksXbE9Y/V+SMxbkNOjWNYG6RG6XVL26HU
6WGsxrbGlnz9ItPCoiT0u2q/cwI07ShD8i9LoO05Dkh7THo8tnDG3R+4Mg2j5HaafKRXMRqhp20y
A076z/is/eRFMkh9qVIWKZF9R3FKjF8Dth255gVlPOmG8Cfs4L2eNHDzDh4LZBH32CHZoZbN3lh1
leZ/k+pFTQgt3noMMl1qma53vHLclg6Tm/hDML05/mY95Rn+8DU0Gg3WsLiMGox7p5fuXJaIy2uo
HBQnpuFkMlVzPfv7EwSe6Y5pg4eo+5mfT4DypgjL+pytyhlNt/GrQye1ZFpg7XKKid8Atz1/OpuU
8OJLcOmWeGTgF33tJ9VCKqYuu2ScIiv8aTQq1n+ajY3g3+z9xzZl+InHDJ2rL7OYBe68swZbm3sA
HFWSioZ1gSg92xzLn4uf25VBSCOw4/Uxt9DX3jKPG4oGtKIJXFpb7cpzTrYmBlWp/U+U8VU2fuN2
orauBbzM1gIiauePCB+RBH7rVo8oAVip+DZQpFs6dK5TwZDmR4n0jjdr7wn35q495L3zwbMp5R2m
pBZVwfGJ2XiVYTvQsjERoJLSMuzQITwT8D61jY9RTc6NnJH/Tz3oJ4QNVFV3vYBOeheAEyXKxBP2
kQ8DPD2bKEVaKKqJqnKy8qtfNHdNJHjItNcwxthKWzOeAbbf+dEx+0rfTliFB0uDR38O8gu5uw34
KcZdAjfooNrx5NgAmf2jPWC3AxRS9aif5q1qeebtU4jm5PLie4foQcPAFKxSP6R3jWknOvmTXTTr
OTHe/NHB2+t7btWJeb9+kuhrBKlMMExM7unCz8OrTiW5pF/WnC2mcXdrMm57yKVbMkq2jXEUH2pK
fmPKdAxoPFeHE+/7h9/klhdMd0rHPhsfEgFsmM2m5zoKhSLA9QvK4bRXytAb07u9tz3sJOhJmR0s
hz0/svUMeOATOxvHk8Ww/E1oUhaJaXopfha7tiskvZqgioZ7nTSnh0QfeMpxX+imh/PNErYg3RdW
jMGr49xtuyWejtv02HSvbbkS5nMJbIkSWsNiAdWragMYj74VSG5Vi1sy0OxYonfZyKDEajtOGJc=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
