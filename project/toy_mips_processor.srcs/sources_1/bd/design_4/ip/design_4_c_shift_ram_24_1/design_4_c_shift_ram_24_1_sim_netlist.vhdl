-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_24_1 -prefix
--               design_4_c_shift_ram_24_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
QwfVTiMY9KJ7ip7gtgPU5yKAi9p7PN3siuGEaQWuBNAl7yV66OkEfdV74OXzxjJdkBU276K3Nuk/
Arxq/4/FsprkJu6UUS7KWN8DeyAelUbWiMaff0o7LnT+euKIBH8SCqNBpPPo+95dtz+FhHL6vFXh
JTA/o+Er03hOA5F8JlaOqXo0tNEDC9ikciWzW2I5WwAvkFaZf4eTOeIeQ8BaA+jcZhUwY9uJzwNo
K0bSjN3Pga7qhIGfLm8nHMkEBLTE9WgtTrKM6anOA9ADJHz0E/apgDj/hgxo7vfc1nMZoBOenfk5
wmbBetRyHv+qs0wjleCaMHyQoJx65NXz9jmjs0+e5EkOCFenz69ERyISBZs4E0HB9shfUdm+1x0T
A5s8bQlCyhWZGKhrWeYbH1biuBg4qLywdoF0SzBvQ4CWqqj33XrCQd89hv3vOhrC+Llmxq75/p6M
hMRn5Q3b+2KmyeYwQmdMXi8799if2cp4sKKhwD4/LDtRuImB/qw5APDpjSnO/SgCuXzpu+deSlHb
VaWma3SDJWqRDlDEPQb3sazqSO2PuvfZ2OOpXkN0lE0EgAdxJjWQMtxf3VUivuec97MWkOD/trYo
eZA7e0IqZJwmdKCqR/MEDycLV82tiSY6oCZFQSxNiGwaJ+NRS3U4k2lvSpHgTeUpYP1TlCV08p5/
2eFu6sscvWzH+cc6nODkFnj3xMo2YX1NiYsU5GOfeTAdDLITVBKVWkCJmZiNYNGQBw6a6VukIIX4
b2ddAMMd6V6NvtXZZlelgKE0BgOk140S5ysPqXnYL19k/UU8UTqWURd5vaVzketugCxs79PCcGfK
fsrwwhnuOCBpnT4zCpe0xNvsyBEk4PgS6B0U9ZgMERHH4/stEiGs7wkzvM+PO9fG0HQre8extwsY
5NNSkdUZg9m7dpTg140wVsdeT/x84K7Nwu1lHe8UQR1rvJ7e+QyrhwMRRxKvUbp8NmLl9Uf1XEEU
wyWmXAO+LrtZX3zhIib3Rcalj/pDym2HjwfQLcESrOKcce68CVpa2nbrh0AN8vRPhBJRQLPbuhb9
hnr80/7iHbkL7w02gRvBFQyAfW1g7YL5LBqZwx0L5ScXtwimDKMmZ/FBcuZ2FfSZSUhem1cvhsiC
nBsnKFmyaL8RQKZIy59LtikZUQW/0ZS+LZvglqUgHRav5CBBO8FpNM1mA+3fs9/CLxTP8TuaLbeo
y/GDlLbLDaeC9gGKcfaVRMug57wxBn0sCmxDKr8tfJVUHgDn4+jILRrM44B74rmsaAH7vtisUbDO
/rG565SamQnmLNIneTQhz9zg/XfB/eja3Epe6hEFlhaaOvMAJfVkirkWBUxUfkl/DgCG+on912oH
hL068Jctj1oVC/KICIzVqS3lvnd+pm2y0y1cyGSpM73evx5mwHIDDpMyGbqX0NzmXAgzgnd6hYbd
aUiN6LR43mi5M2zfxAplAMkyJgzryUBW1kO4ZAkpqL6UawA78+p1DFHRbG6riaNufNYn7MreK+fE
/h0+9re/IQiWXRYTw+Y9xxBH6KemcAYrlN8MsZxIgmlXRWwKII91cHV6XqGnChiUw8Et2Wg3/CCp
/GI3wotOz2TcbzVJtHHAXmUhYU4Z2tM2peiH1bJoq9m0BGTcWzNBDaPoxJcntm7cwCJhgooULo/n
Ysa5u0iacBnvguLEcOUkh0vihDP2TmbsgZMEztXk9AwgIrUIGmu5Md6Iwqb2wxqVoxJr9IowO6de
I5pXk4E3KnBpkZ6BlWKQy5z0uBFBYWGkMoLWgIYApceweILQHPT2CiIKw+jLxzrw66OR4TSNbhh0
Ga3e2Hakr5JdlJSrETwdGI7PTvL7NGbwU/Ic5lf1i0QJhcd1ks60xuKZj+yHOXos3fTu68jOMQEE
62e7LM2Ja4ziQG/htdP+HcGTbtLROjtyOTgVIqv0lmAtXX9uhEztiR9Ysat1NuUarei5RP6G+PgC
0XqVFNHvBJBVfE0JGzR4QzBDATCjgln0dDwmQ1VwkeVxc1o1Cn/irgGZ6Ay5PVGny0O05iC0ahYD
m3+LXVV4SIw/kB9tAb4I5GH1ztk3hkJYdTdfUi3jyzZ6H79y24sh2f3eS0dwsM6cfS8TNPqzSN9v
er5fuGTiaSsISLJD4I++s7TtNR3Zv6l3uOe04OD+YGcgZeHgUT8QARqCFTzNVL7vq/v533+e1LYX
J/kHh2hW5SYvo0A/BIq6wMfL20OA+7vz+ExcNgBw95SroursRYmy3IoXxdLQMGA+M9C7at5fqBcu
7fk6kZjKFAJgakG8rtfxN1yx87PHEnDNFQoinIDF9MPG9b+wBK4UxeRWz36vXSPMytPdgDcrvtMl
bib/pIIJEOrApqpQAMPCs33BWC5gLD4vJT98dqKbqSV2KB9U8EIhUr5GGj62noZYd1xcXdzfyYZU
Y8wiLRXHKpkdcdVvcw4z+sEBcKgcd/Q0pppF2lIr3oPq+jufEo32sQGOovqS5yLd6W1dJUw3y4SS
cVE3h/GAtC4L0+HredzdBR2NZm3YBSqYykTUToBxJuayjLEuhT+BlSoUCDEiA7UocBdLHGkEtCSF
iZosDCqzLvJ23I3W501QSAbC8wm87Oqcl7n1fdHs59JO/hShXVlpD+D3Gds0GyEghNDhLbMUt3LL
Exl51Dzhr2RMcdhkpW+NPVruuHM37oiFI8mY1BWT67bUL3134fhA9MuwWiU221pcgqox1/tOEs2Z
ZZ8c8+Kq75pl0xgcKsJA98RK3ujHBjOzFqMZQ6oCbcSUfCpjW2X4u4EtWx2RWeK/+6kKqmhTiNw9
Gy6EK3gmj5YOQhv1IuZQZX1vtUCiuk9DFSIVm2fCtkuySWQ3CG4xMhBFF7R2uKfMuUIRK69JJEg2
zbBMp1ypPPYxkwHn3ksToAF3jpyK/PMlCfB4Z6qrSpCQ9JoSpd8NAOtBddW4XQyYCFYYH3pOXrkh
FHlQ1Vxv9Hr7xNWi+gvwUCt4N15cjBYn+l95XWQk637RYh6dcvcgZOsf6T3HU9ct+fXZAC7xycPZ
/uJrNl+4Fp9fj/j1mPd2U7Xjmvdv9Xnk1GM6tMADw2G1wPLJt2Z83f+gtUUJ/jfrYJU1XMlfbYy1
sTqvdt79fKmb6iSKyew8K+2//TIpRI8KTs6hnApc/XvDLvtQyRnL2hRQzdvzYZ6AmMpF0Nz6y6rb
DUZIBqFd7+8SJF6Pqu/WFHNRbfjEdM0El7cQ+cbBqZAC4nYpaK6vO8oayZEpD0AJ33D7A0DEB8rt
uF+jTtndffauUgVfVJprqYUYgaPcfghlHiJmsAuG0g1Qb1+ygk6DXPsWtjDAVedDssecGC18FE8T
nwgQ7uESdGzAUOD2Sqv/QvDb+4y0oik0pf5zyMFZzmmSdsrteBfnnQ8a0Z/nmn3n9KJB3AU1R/Qz
gSXPWw7lCEaFVp3ZahpuH258sFotgPiZG8QyqYGc3xiSpXwSIQPUsgC7LXz9LTpyEFq0yU994m3V
Vm+QFBHlifPFfnhEDGhkgrPk9gTZLjg/3rtmQ0EG76C/Hmy+Qvo2egGBhlY7T17FEt951xP1e8xd
Qxncnk5R4SX1Z8QYIyU52FW6Cfp3cfO0wk/CsZY3CmlhOcnyT4y76PvbcO6pHfzCHonp8CrnxZC1
ZTjM3bPaqP9/mOeL+A668U3yBqXye3qu2+WQxI3cYAarC5dMMnMi37CvZ+stg+vSRlWl5jlwPU6o
qNVS+XOcw+lkOXOVHfiNR8x1+0Vvp9R7p9Ymn4Ey/iVC+xCVeWvTYOzYsYMr2N41sByEzUqUQGSd
YOR1ABHrln/sCJdP0K7yBhqlFh/VAF49Skpwh88a5+TyW99y+C3n5Lw6BvEFoPdIboWjx6qLGGWG
MBkOxIytUYreDbyLnyf2v/xsV5olMepoeB0+RsFjWqv5+Yl+5RwmtehjPusSchPy8pfsqbTA2DzO
v/Q9+subDUAnXJnnLMPJfjC3SzssWzv7e7H70lkPUcC8JQpEglYJK+tAOBG0XSbWC6pCgw7cSZFK
66aVVq+xrc1Ldat4GfP1b/gUs2VP49oQ0eDh6OA4qhIQzZUKR/SMvDOOYj3h3Z9PNAsbEKA0E146
EZn78cF0t9ZfjkglCytYJ2U4zzNPoUFyur6xTuFkzF3xbbb/cb45zyhWX6og6PB8mgGFGfW2+8BH
p0mWf0FrcI/sQjFPb+E5x/q7C5V/DQgpFSpntkzx+TggFhOOm/pJHg16IqkDgEAaRMyUezPcaG/w
231rCKUTYQ6r3dJsZbWi08hJEoVufWDhzk4dEG9hpOAAs0joSjJ8YoGh3EPmo+f7NAOiCFRrB4sY
vQ1pDKSWJBiyKAh7Tou5vCVtSMRgHstaD61bhl1a6igCublbcP2L+3ZlNm4MJErUxWJteSl7TIcA
XBIDmf79dBEuGljrqu4PEOXZ4+llGTR4ei3EGBlHAkYF0dXY71N7pQvFHN3dvzAB3ZcRdiVOMm81
z3bpQtl+A4YU1fPGTRBrlYYFXwAs5rAgbS11Syp3P6oublawXOOGke80DMZbDYbdywMnEyU7gig6
YIjRNwSma9wA+QiQXBPeFySD3ke3hk6Jr1NqzeTEhGyhK50aZwtbK9vjr7Cx40mj5W5T1PhyUi/R
+5k+1Eeba7ykG/JUzcwmuKiWHx5YyLhOgo2ES71gk69uSnXfXa8k1/zAXSfaqJZdTDYHg/8INvvj
uDqghW+fCVkhEqHD8Q7ZsVRE+sxue4V6CHU8WQBmB4zHypub33Lfo7qmHKAyiWsqtW40ugKOwddj
JBL6WxNW2m60YRM8Ae1xUvZEjH7CZNb8mvAjOTI+Hqv31mUimIADwVTqJIRlTS/NNbQwoDYL6GYB
aHwCK2Ta8Ajvy7a7t/XcGVyFdUZgxlLtA8XAMkIIYihthrQ3weuu8uV1Vb1afqplhM1mueHDZswu
GvFNU6moWkRpXdS7ys5zSvWo4+iEeq8SstQ+Y5+CL8yOhPkKjACfIZ4nqklzOm2EB10YvvgqsA9/
/OTNnXfCLpHqii8tP82lusBL1lC52kOBiiIkhCMk3RLo9CEg+sQ61PHgOFPQfaUnen7FmFk0Gp4p
TeIDcGEuQevRtAGiC7FMP000MnZfDK/57UrdagB5SWuVfeoxTGzu+l80uUFqraEMp469VyexwVlZ
gjw/v3gsRkwzUyplb3T/S30jtLES1+dzJe6iibWcQIpWr8nUHBr8MKvh9Ggx4N8NwtxSm/mN4zlh
f6ww7CGeDo2q+qHIsn2ukciz+2vgOxtzUZoGLohIjyhvlUyvXvbEqlonBBu/vwWFatldKoJvm1ws
rCB+uPVENVrEHY/e9r5AagvIym3rQL7IRCLyJYSdSaHnJMEHVJrSx5OThUQ6sPR1QBuG2O20KPRh
oyEJjvkb6ktH+ats3thYogoi6B/WvoM3FY/HAwzL+31epEC+o2bPYcQWsZ4KyvWSrry8k0ISo3Gd
Wts3dKFj1K+VBomG0eEHtWAVYxrtmjwHTc8e8vl2L/wcT4otZk9erWWkAswX9E3J1CD6vxBsFVHL
NB7lh0Ce7KLeN/goR4xK6qJnq2CALZj3cywbtaW0Swj8CrfIiaiwEPMukD81R58qXdVqHdq7lOC9
1144/9ePQJiiPXURL8XHcCVckCasTg0S43aXD7onnw47SUZ9mUiZDM6JbdeZ4sEkzGKQxjnNs5AO
lNHyXbgtJttrmLdB46QNxb2/VTRkOZavzB1tWaJa1cS8vCrol0MSG/JexZQ1M4aGLVmF9TQXYi/L
BamZsd2kbCLyXNRp05oZJvbIr2OGtpldnnG0cntRTI901E/cT/es3WIGY63OIAw8Bmu2PvyJxHMg
QghMcLwVih/UQYM+Sw9OTUUa65kXGO1j4NooZ5lIO++vzrjPXZzPv73lVHgVy5nGiT5ScjgHFoUq
cWNMZU1QaD3lDcSUtR3PCdklYh6bPS+eNTOgoSyCA44Pbjo/PwVYRryE4oSqn/zCHzC2v8ikayWH
WbOTa5U7LzsOR7vBYLbGQGn73suQ7WsyOKa8VdWX9Ac4O6V8FCXTaNweaDX6w/5KkJGUQK35qn0u
rEF2DGmk9Fau8WQv2I34+h56xted38HelzfAxSnxl36M401Ov78GPwKFhalLnM3VovNXqyyVxtVM
XugSmFkA0yeX6aPWlqVMUxwctJZFMA3XBFUNBYJqAQwoNuIgd4FLdOSWY+mDrmDXwwuOiAbYZOv8
kplwOVAWTDBa9UsZf6JSKLCzIh8DXghpkEZAls28B44gQwis3E8dv+JQECH5TwUOZeFfM7/L3Z0/
s8QT6WU+uQrTLPrFaYza2FPQpZdlHjjP7rrpqO6fhqzlkTZhC/Rd8Ib8NU/mwaW3gUQ8H7XjpD1K
RYO7fZMkxokor/QODrnEH4qLvANaYFaMvfxT8+iDGckvP1AY52Rks20vzSpJPAG8cAHjrIzmmLjr
ExlR4V0z/r7Rxgt3r6aCk1QBnSU0ovfYbbvZY3BhFOg9E7YF+rkeZgznqHAf77PcO53+A8P9Q4P/
8aB20V6MON65a/twHENGA/U/iDtb0lifneXtrzFTt7DMe0J49289wgKsWL1kfvOca8DUj2OBTPxS
SnPUa4t5znUS3WLN1b6S3cHvGrOG78MMRTGRbcIirxZyNCtYFlsDOpp49IZ7Zj0e1oFR7FiSjCAo
gO0zLgBL9SpzYRdji5vtIVnxDYA/iWMNhw+/2Q8Av0gbGoO7IiPNSDSvtJb/3iXDA3QQKHW/Qw8v
HI2/ZO61Mf5P1b0345hU7lOKGXeVebepr0ycLjR+9pKKvhIfSPBy99FEow3Z/i2dtmYlVBl8P3fK
zc9u/hzrCApemgIdujlI9yva+0DNMND1dUwfKH8iExHm2Rmn2RHXlKG4SDj4Zd5kCEYNgqwlghv1
DuHXl0oCXixYHNLdE1gECgLoUhq1fOzRgo87pLcjtFxM5YYjbg5rvD+yOjOuXiRK/0PPaiTc9J/s
1UrwfAR6QnS2M67aL4XY3UsxYj8MUoMSVocAS4V3yyCpfqbr897Wp1LLwu/brGsGJPmVqQAkHvcf
ayb+rzGDof770krOzRZYb1BFTIndMGRXDd/K+ei721V0GX8wRU8/N/Fgt98SoVsli2ZgwvduhPbq
+4zb4MTNKms8fIoK1cUwxLOXZx0vdrkiWJln//DYSU0UCWZWWH581S7viuOsXtn7t6Ck2iSeARz8
x7u4Kh/ilegEDnSONB+MEv4p9LpwpsTIwqbgGmCE2xGx97MZGHVwuNySwhL88fPG5sMXK4Yy3a4w
PMqT/9UR9D2pO55XUnSRWryhuve7jldiw9u9SveAxUQQmGIMdXpcI2XpUB3eouZoGUDBK7t0potf
JbtZLTeE6yoVnGa9epsSOFF0eD+CUWcKPlBytvxNojyLH+sOayy+UIKv6VNeXXNr7uYRjWCWET73
Fg2k3GQSJbCB4tQ2G9CO5U3SuZ4K6aOa8+ASctWo85+1lyaC8PuEm0VCXPO6m1JQMASZLXIxVumY
8bUBxMrRyakVUDhcrKsUDUuoJTW6QVNtxFQBtNwqOUGcGmRG0rsJ5N+9mwoEzPPNWK1Djq87dy07
Erv1I+lqTvEj0zmzNXClcd5WNwMJo2iOtbU6wzij3HcxDSrqh4ZFLZ42sEfLGtLXQstaFnBieXsM
kojwW5WKAlklwztLIMvGhdf+bZncoMeqIqZTVxJRS7O5SQDBYPpn9jcCsMOE/FRJTvYidHPsUvvH
SZxzcVrcmbkXKQ54dqlfHmYKgdcut+VfRJ8kzp6VAEI6rRhwok+MtMvvtPSZBeTgKr1Nq1CRo6t2
85IAkYBSv4K6yRs6oiWRudq5c9dF5ZzBiDIJJ2WTHskqJAgboODYBsUqsdlrVI8NA2pR70q1E3hH
nGTlCILdZQffc0YMiYI2PWK92QRzXFjMGrGVOMPk8VUJBv5XnMgltK3LpxLOr1w3Rq4C/bvT8B8R
KzLcHQkjaaqIG+wVbX2jDUIxsVgTp2xrtsh0YlX05OgAufVdutCXqhAhnlRP1y+BK4s+E7M8tee8
q2MNz1pz5+6Fd1u9fKVhqs71gBGHg2F6FI5Udij6hPIMDrZXVmoyUiFeDb60r8awPM/p/gTimEHj
R1F0yjh9opGQs0ilbY5A4ukfbODfemB4LbiX+LlZiNIo9nCm2sU1uIBPfOnBwX0bFvYUCUZuJIsg
fKgj6vJfpVp0JOgW0aQYPmcE/zhjCf6TmDO7PnlfPze4sfosLPI/1zpYhSBlE/9OzJ6MDx6FmK+n
4OUH+ou/ExGObVJRpyH/9vxoIxmAVL7dprqEfHym8rWCUBAnHEns9ItwJI/1IMfSR9Q6yyqnUHGy
L6e45EgtxJL9faz7fhMW21SLi6nNzZgtCiBPvzxCsJ94sbyxdQ8KWuTstEzv2TGpWnZ/6SK8kT5Y
tP7l6q1ezn31WTlO0tfuwAmN75ub3bGEHTsXKoxowEV9+VktolhsFcW0TnZSODzy3UQFA0cvih1Y
BXhbctQ1vSru5YwBJSoJG57YJBASf5SiY5MzXtKmkm0Hluojbrjk+t+0NWBxV0bBpeQGD8cXnzTK
9IHjfIIDxi9o2ABn/eT3fVOdxG2Tnx9c3xCO/9rDim4//8WT97mkbTfJAS3LWFu/DXZE/1Mx54jU
WfELWKSCZav6+YC4SYAgMrhe9CP/k9h/prrRxJ1IgOuwjyUZ9/xXfjqe74it4ShLjppb5e9gDeu7
9+eTPSNGyHV1kJIJg7Wr9b5pxSjxxGyxUaOdH9V/4YtHhsRqOiS9HRstWczs39AgfX17blteddDS
VzZguxwbdy2mt0K+PqyO+DaB9hlNfX0JPBg3tNn9PwaWBMHOJotNk5Xk44uoJ2INI85vtMxFhBhD
f0sd3uE8xbICE+/Y4QX27oWcwgbvAJKuwowW3tnDSGnyRpMLv0CHl4dh5dPjUJhO1gvgu88L7i7k
ApyeCdlaRQEYQhCmNwi0pNiNqRZasoOt7Ohn0mX6eHIwVEVwh9bBAoXTwNR940GK0gmYeJW0iCo8
ifEZRyV+1xWBUXl/zZJeyqo2TUEmr/tA9N07q/ta8WsI3+eWrV+QpRPnWNLnbsNR2a+h2cQ+AUbr
mJ3lpM/xDI+EROptxcRk7qOsP/WUEH63pvojdvddvm7dMKpzmSpc9BfgEdPxXqpN+OTQacUGj7Ef
e+fFDdEhPkPtXi2+wVdRav/zCHLMJglHjRHsPt8Y1ZoBlloFhpscjq1JTKHwygakNxc4BRmq4Tor
VTLDk7w6PixNxzfxRyxJA6W13mpGrZSFLnvN8SsBjvutWMQ8LcrUUdKkjb7jrSARjXZuKN88Xc4p
R6MoD8BRH49E3OCw6PQsgigbmUfBL2TtZPh3qh81AdesVeJMN8ylkoubgLbeqGsJz1cT5bn5UH4/
IAOFHwhg81fe2HN5HUr3p/1FrXWaIFWKzZcfrP083saDmiDT0eryLHwMn+LeKVgpktUHkyqj9Fzu
TfXbiZIAgLOZyTibmEe0HrAP4BFnBc+yTZQUKGu1l5Afxze1M7V3ARyuzR4HVY8zZHQW0mgYKnGE
ARf3D1l5mtQ7NongkEU/2hn5spXuGB5Olnhn3PwU94rz+CCBL1nlHjgSXkA7SmeyhDM+eB+G06TU
jwNbJqrooZWnMQMgeDLE8Q9eO56yhDk2NZcXVJU+Cxd1naVP2nbqjflTmnwliyYCE0tKp6wTbP4A
jqOFdCq7cJKYlWtScI4dD07GhuN5ovnQT3adY/Mz/iomT3UzamWOisjDCZMKnMun7zFTKGhF361r
7ZynUA6UvfjDXoTjyqGXp2uUW3zmIVST8ffGiyYF14mMCiGo/iXtHSiiEpCyT1d4sugs7daH5AM9
j+Jiy17Ps1vfNO086pxEKLuQasUwG81u6FDLQmUuAohfP/lQLcNfgO9jfcwrLL9vt48NnPuI8Dhf
jwI8kHXwLUAD7cR46Pr0GGhNGSVcEFxbdSGuEpPES6qLKlQ5UgN9jmX61XTo7b+JyX166TN6yZSB
OhAZvq+zjPV7Fy5rzjVwP654/MzUvQgpt8GqEAsy7BxOMF0AjzuWlYqp3+JT6OuoiYHS3h+NeZ7O
vAC8eai9ycGK01EatIc+xl1qCSj+uYHQaZdzdAwD+gWsQocaBUXiD8zJi/s1OfYDMVrQAZaGCytK
5SY1UxhtYtgGmehZ94hN9idswOPGBWspbODzii7dTf4UdguVv2Fk5JiQ9V/lNEUzI4XUxfgt+Mie
h/0Ed7hByFNuIXX7cEBN5Or8cW9wykKfzUiz2HyzpkkP4mh/Q7sF9HZHNnR6TzJ5rlpVBKQlqRBr
6Bi8M2X2JoFZLncm5GHNQKMhzu1eQhwV/2/oTmMbGFlKDm6MMrwfcDXPk1FsNba6Gbmi4kpq7u3S
2lLlL+YhKlrVC04+4yqPscRqhjhqkU0ei9De9IgE3ZT0fIaZOscAJxXNXugfRFKcWov8CCIb/NuW
C7/GZGMa0uh2x91udSI+m5xTqJ609zIV79rtAQcnf3jLkLDec+i2SKYEM1awe1yOzAIBfsWSTrJN
5VtSeMs+aMxiAnBu6Wtv9QoViulGhmcMh2jDDH55cDITnQiW248LNwx/U5wUUs+O2zNC2j3RoJ1X
JODH944ZksqSvkK4vgizD6FcTohQDi8ZrsflqQtIG+Ge7fbP53yCk7CgYEjRG95foUoHEtBTIrWF
wihOM5SN+jPpEu1NdrsyUsbhXFao/E//qvZIRGaoKZuIepxxRIFysMzTpJhYpPVViUsmeuz2LMXl
t2NnE1PQjqxmBF2FSMALMehXVEcUjaynsezhVaAvMkyS0zagduggLvDV7NRytxbSRGZ2ICFXFXKM
Ljtq+AE1jnAnnLweKJKgQU/+v0Yk4HpdtZ+z9YYqi7cjzchwId2961MqgrARq3C6fFfivb6D8e4g
j1S2f4yHrlOexX5xBBqHzC4+SDkUy4zXyCub3hr5+ZURl9Yhw6NHkuiy8HxSfrzG7Rnl9FR78fBh
eyC6g3ZtuxZvdcIxYLTj5SApsKT+Zv0U3dyOoZcu4E7AfoGhljs/r5HkzhhAS1fmQ5q8UFw8lSUo
hgCdl5C4Rt62YJBkgXvtdkIaJRUEDBiBiQfFN66OpSyOqel/U8JvMg9d6noHG/wlpVvH3QZuzxFX
aMhVJWAye89EYtWAbWT0S4N0sy8pXDDA2kkBYRN8YpfwvgBnWv8Efq827KzOuYJXpp48SH7Q17u4
cm9BkKvEDoVdI3CJ+1oME8lpr3bX+xzM4wIjfKXxPL3krr2aMPyUI/GBeWe5/BGYqTHYgIR5snOl
YNBedkutjU8V+wo5CHnZJiKXqPmudCOhHdP4MCGjYxcr1KXQ+L+lhKKq0hKw5nNYiqusYACj97LH
UWxzRLa1Y7vGKkyrAPIrIX7Q4pRQtEsp7yRMvao8Fs727Ho/1sHRaBo9ff4fjsCICjfruYMHRtA3
2qMc2sQmrEL8oZZSLWXYOH/dXyj5U41s9pv/8luiExfACFMIXJ5L/Hdj+KNqJ7a6xwaMwH8wCa51
SgD5/NF0TxEg27SwEaq+9p6/uJ58sLGGARQpJnACkpmdyzYQSQLkQedCDBNnzFH1+QgCr8mqvBY7
YXWrHBuo15zeMf17H0Sf90L8ae7Jtej2+ppt+s2w5gWp34rIBwxeFVhwP1DCOczw7VOp008SZ6Ti
CDwITD8cnCy/7T3zD8egiJEfI4cOxizk/P1vQsmLipf84p5qUq0AxHgI9C1b042eRJwuy0GDpyAf
tEnHZF/xtMnqPJsRycV8ttAfXFgUrbXJw5xi007hENFqjSec/hF4PRqkdjd652GKzQVQaqsgDe/1
Umg+UI566pGJWCkNoaKeubANrU9erW5AnSN+WmB0YDvBxsaRan7+eA9w0cM/yp3gJHFn5nNK0R1I
4gxwcFfbmxDTBJjeDoxi9ld6xOSlVu75yUZnSE4A0kxJY2grRgrTE9ozy608QVRQ4zbUawic7+6e
uIAY06SvqUGPmWMLNBjYsjLVUjPCljfovJpaULYz+PJCh/wOamriaZXY39nvEWegAX0wSDaWOPX7
bNXUN4Lrle/6nJD/Y3XBlrNS2vykDU84J8fbqhbDorfE7loDqQfe8jHA9iIijQ+GoK5m6es404wT
1iv2orFVOw/7p03c7XxXb64iK1mXlbup/CLCYeCpFEbHkEo+7ETu0El8A4CBq9V3s6TsBPkwh9TL
dhE4WYVcvwfPvY90+LpHiVMzDO3rLaF1gxFBjMsllGicLiadvftwfv74MksjeHItr4HEg5Y3DBiN
ChwtuIFWVOTpXBrteFZ07uRvNmPtfOcdHXsxRwH2xdCYIMmwRAjY8swb4La9tPqtO3sfKnhgCKuj
PlWksDGE9UZjRhu+beP+QbWSEKtTfGNysFHRAb/egSPgv2DRJBxkIMLrO0nOQaPECmY6p3+ddKt6
zCHDsXyJPOvs5qfUxIMPZHJrTO3fB8Dj1LufSfgv5yA06NJbIaorKYiq2xvzVnt0WF3dI6LMMvPM
a9BWQ3G0uL9EC8MRKFNdgnWqW82cHLf2x9p9EDqXDMipUoYKWHcM8gJm+qwz012OszR4MpxEA6Yx
ET/WJBxBNcRlrElTFekUSP+Mbw/+dDv0Q/cFokOys5XyPAi1a9jMlk3DBUIn3ML+AcKVA2UuSBYA
FLOQQAPx0Mv0em9dvznFtCTFuUCQDW06IW2AcXoVqzQv6r9bG05/Np+yG36sidqjpki6ssHiSuIG
uqhkOqXB96QiSEq0Zz1WDH1o+d48s5D24JFH+lZJNdMepHUsqmNUQYhaumFEXRyDU+6KEwm+6tmT
+ql6H7UP5y4vdvDv0CgzE3Cooi+CdreJZR0oBji/DG4pD7Pzs6wR/kmfdBT5XHZeOb5cLz32oLzJ
PGq6NRgETf97LFas2t3ukkT21maaDK+VvRyAU9etPYOQiXs+cuD6bFckFelGqecbg6b1TQ3+50SU
zNx1wDbKzBLh5+K+kS3X94+kPapVjNzt8CPgRF/GohxkOk/pQ9BfV9fB8lvkQTGT2ZnACWiMeTjL
nJ4PZRTu73+gfsa3IH5zRgNigoXOoPcGEwzsztlhcQ3/T+yz2E5HrcKPgr1Ya5SujtQDIsYMuczs
dvryzbXXtgPxKkzFh/BJcK5yLt/gClh4XvVNNyg26Xcy2ECYvD8nQ2K1RAnxAacUBPelzqGd/wsh
2gQGl6tE8f0CbNVa077de/Ypr85xpK8/vSOD/aHygjIyaNdIk/cUaRwzcrNDQgaxHUI3JlywNSJ6
+yVuzPeT1CrWS3aKWEAxcmpe9IRyJNUL48I37hiSQcsLQx3bRHDEHLQ+E9g0pydggwzyQ7oXrz7H
MCF76Uzg5S42aPDr+PbFZdGCTSKFNzodMGLpjHsUcJIRu8xHBRuQ0Y9/Sxq7hSviewL9CSbSTM2S
7CqLLBMnQF7ERaRIzk14o9xftIyzmOXPLBtlf72ktW2wR81F7kyLBxjD2jvPIEpeKeaT1C+QvkzU
R2kT/qvp2IuwyRCN94tVVGwoKNfCFK2P7mtCJ+yFhK9RGRYXIbiE00QWxAM1gmf5XCkNMRLt0nHz
doWsjUwd7mkjFew5YxwlEHVZ4c985Esscc8Y/yCcuTQncBKvVEYaug9EfaDBsXXKFN1gxOybHSiZ
EPm+lZzzr6xMtuQJ5cKcsdqDBGdttIpyjZuPIGmJko0KBxuG+4ZXKNnJNy2te4Sr0shYHPc8h6G4
GLEuSg540xG/MuRaR1DfHxPex1SFSu08k81O10zM1B9GqiFWQLVjAdSAnT0KPth/W/LOohQ81+As
J9jyDozFUCvFVrnE03ZbENne2EwOEZ7im+tY+HkFPAJ2c8hVy8m+ATTjVOwRbGRTEAyZzNHVuW2q
h71qnjsht+d8b8vhRgit1lVwyS3P6fVqGk5qnnNth+xVPB29mr70LPsN5OSlc0qmMhT4N9SuUedH
FIFuJcWL5Tm9mMrhnNQ3UsMBh1TFswsL5P6M5lv7K4OShSQlyl5dBadQ+63jbyivfqkL9OxxnpCZ
PdZsL95+xM2m6Tn8FCNC6SLQFNdm+Jtw9doS5Rt7EG1tnAJQq3Rsg3tQTLlfF2c4IC7R6IQ/71++
HY53h4r5ujvedpwt3KlW4/yq3r2RL8aHVTmqBPtAGsf4CycW1b1zZ2xSdnDfj2Fi0eFesJpcrVUS
gXV68ivyPYnKkhPHDoRRv9dcDZqBbH3bZFDLuyz5ggcrWXEu445jN/8i8QI97fr+42M5UNmvwSdL
GA0asMr/4UPEIF72zLmhxAo3eMRGAcu2fnGoVXpKK/5Uk7XTw3XQBrxIUCcR9sBg8KqvCHGjGUWR
9m7ZZT5Z+gsjXQAFK/KnyF55BLULamVBspe5PLTNfndQ+t+UmATO8xx60UUJTJ64IcKT5fppSCik
zW/sbIVk2ZP6pvO1hi2ZKtupYCklDSQRBN3s0Ual4efPFzhINXefjCbahpP39DDXfA9fyq87DsAR
LM3TtqKbsAokPbcJEgU0Zk0f7V7PDmfZuhID10Q1LqUCCD0TUO1s9bAhGZ0PU+lvXfZlHgDGBDgz
WY+OS6Pvn67GHv9aLPajO/OmOPwUvfq7an9nYWVLKRNVPY3wkWYYRm3Th0llyKU8xK1EZTH3nB/g
qvej0tXMQ5HODEn52FJ60DWEAggZnjBpjKBaoWX1YNLHnL9pQGGKUuSWhbJfXohnQndQm+04uTh5
uO/GSvu0oAr2QpySAgF7Dvq3KqwoLtrYFUCEHBr9EuPWTMvnRvKXgV5a7LEbj/SOr5o8VL+Vl+kp
0QHkMc/KGGcS0TvgD09xm5T2lrFK7QbKnBlZbKIsYA1oLJdxBrQF24zdSIT2iQZcWbOzXtF1shSM
8aXh33P8UPR532HfIXqKcnXHNg5mVC5Bu8ggxDLW/Bm51SY3u6/wkWyXhebfZ8OdHC6JP1GUHbGp
QjaJPbnz85hrGlJsJvrD3HdSL/YaMxUNyj26bm6hu8w9Z3hkZILnV8cH6IM2Dm8t/0XvOWaHDN06
WOuV/tpSgx+M0Slcmf10JjLnTPnUvPO9ukHJQfOBMIBv5tB0gaEufndajzl1XozEsOUlAdgq3f7e
U3DJS/YCVCBEIWpmVjhIMyjOKv3gbCPG/+CRyMfBKy4rDgyRZGWjOHAJc/F7DnUVXdKQ/TjygAti
gErRN/c/sFKm2KHZ3eKUec/ZBpJuLPLbg+wkmZ+u16oN3Bz/MyVQgG3vMDCuxCGXkva4xULF8lGL
Ixj6xsmWmBcEINFAX1CZAiSlRP/nYWwejr5llxXQ83tC+vEhP9JUuiWGuPqsLqYQZpepgeB7702s
gyTI+7DhWQ8tHwrwa6Ozj/08j1BkzK1Yja9k+yF8vDg9S7AYS7So+swuVIMjdd3XBcBKmcv6X4FF
69lKhn7qas3aWchLn6slYSmPnnBy7lP26Pi+Un0LAJCFCJCw0wUwBumcMMF8BwOvJuLXcVRn/ite
Ml8m9SleXvWpG/WIAziUrfZSl5doBNseQh+4IfZGeghMEMCltbQHDZPqPnJ2Rb7KAOj4Q9SHofW2
Gl3mabpUbfpfkvbKCIbBCHPuk/wVzrykn/z3+EgqjjH9qPDQutQQ/NT+ZFMjAwscJfiVx8ZPe4f+
JCyA5rPYD81fR6hT826GWPiKIC6Il73gpM8K3R01GFB4w5Fc9H+EMrbtaP7DVvzYx3Tiad0KvzKp
yXqgP5aSrkY+Rv+heegjscuL1cWJbYEnbHzMbxDLfuhtrIiw6GujvJIhct6V9LkHNE4x38G6SnbT
cMAfxAMowwESrsavWMimcBwBYak3LXjHQRqO4/YQa1qWNqv0HpxgFXpxip3Q4h0TgD48Kd9XmOfi
+K3aJo2porG11E9MaPfoqVLNPUZnFOShbmz0w7INOLoDyM8ClRLKNze29/oB6ysXu5Vd6FUZPbQg
73UHplJJokv6dlJgFUs7udcgb4fnWX26d+sd1fMKb7DF9j+6F5ABBRF7s4ajAeXO/5ATjAEbPHEx
LGvKQkgIfdkO4sEDJFNWKUXAqv2kNq1EHQaqd/Fegjot+pgn7K2VM/LBGqqjgvAhojwZaRIbAhzO
gHCQQMs+xq9PmYqNmd8tG1MqFjUgYZf+f8Rzw9c9isFhsZBncyQlc6OO+loS/GS4NH/gs0XtAfWL
yfbiIkJO073aieK6f8saWZa0VL+blPg8pgeLv9rgyxjeO18dvk5YfbysXC+mGnPeulSOS9aNq/Y1
MkQCA4+6eUrB1ktvkCMCu8oo+TFzXZCr9YRK1azM9s5arxr5K4hk9NTQUN+UCrxsKF18IHlSHeP+
wBxrWGAnkwlZsIkfLGvPIyYlcymdgqvhUcKv1UMeinhnJ31dKTQ3oHxaFKc6PZVkovQas2AN1lCm
92Mr8YBSxuTYAeUT1QFQIzUjpZpbM5HA7omcn+WDDJ4AUUsgT31CdcJKRND+ZasC/JHDDlBguwhb
IMp4ZZkacNJ9a17KBG26eCuJ5ctl+9HD9fKbjo0QeYJC9Cd4t6OJXF4ys1gEiYYfmeayJ+U+lpkL
FlH0h6qUKC7sW5qxFuyJqwRN6DITkL+FT5flWr73H0QNCoQgznbfsZYyCGU5DwIPrfz3g8xVLmSl
V0uy8kTaYdXBJU+dqysvEhh5IDMqal5nH8gY3rzPfddmjCiV9TR3wU/5lnEPT8jVDUqkDUWWAmon
ahL+tcu3ITDtzBXydvZFHb4wiwAZLWFJEY/xWU2wKMQKBAAJEX7TJtsdHLIxTjWcdUzdcP7/bYCl
pijiu4Yrm+puTndvUXgUouHBJBoxyBWzMCUb//geqPAi78Jvg00i0WtNoFjahPOP2ISLU4hA0kKx
sc6u3Cty3/DFFWgMMOht3nfc1mQWvAF4hfw6vkf6ClJ6MrW5hFur/Mt/hEYcQjsY4WpR1eIYqQ4c
yaMKfbWU6wyQlZj8w5IqUb9HUI1eZv65ScqZ1N5vU2HvTJPvIdC9/Xw9PDABWFuMDCKxImdLXNQH
OSdG2J4IgBCqGBMiET4wRAmFwtEbSuZPRne3UzqS1CRBHgnJWedPylotpuTo3dpYtpQiNSEimQuC
FTONWrRUI66VtKAANzDoQnDFZ97TmC9tWekLHBgd3Hfx6dOHxes0MfjsrbeitRcotS6uQh498Q==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_24_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_24_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_24_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_24_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_24_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_24_1;

architecture STRUCTURE of design_4_c_shift_ram_24_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_24_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
