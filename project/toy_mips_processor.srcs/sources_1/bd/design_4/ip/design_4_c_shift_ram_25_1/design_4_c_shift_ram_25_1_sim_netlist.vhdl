-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:00:47 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_25_1 -prefix
--               design_4_c_shift_ram_25_1_ design_3_c_shift_ram_3_0_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_3_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KSA+VgCYsnFJk+wleoMQFaRfVQsO9X2VUQPrT7RPrRbz12StDE38/7GFzgiB67+LsDSbcJvu6bXS
IIG2CQTLcZLUL3z0LywRNZEVASagTrCmoWXrEAzIxSbBJ/xO7baEXuqQgXOkVOj19jAAs6ioFjm4
AlIEJuUKcWa5C2BcWp+Hl6PTkBjCR5Nv03gXgbxmJScgiAj9IkdGuMG7KQtQcHMfJ5O2DxBiwW6l
Bv2Q2mYPVI/X7KBdcOmu8imky5Edqnm37jQSqeabkxX05Uz4Ajn3m1RifNkv8zskYH2tQHNy6gAG
nlkCeMiDOwSLR1Mn+WzbGcF9PwzC7T0C6qjIQg==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qJblexpMhnKDF+ic7rE3oVOiTknjduDk5LLLzZ7RQU+YqsmY/dh0R/l0j3GttQesYD9nYGbJ/lEd
zH0rOASvh8/ad5GJ7rnZmSgE91F4tKTSyY0JJ6Hcqw5VDTeQ0WiEpJp7O3okjRS4hlhdh5y32ec5
5CyrRt4klxcXc2ueb/fyN51OA40sWbKoKXz8m9XM/JFdeP1oV8IHxVAErLFeCtEDFBIvBz537hvo
U4afTwKjVEPHyG4pEBGNEC038KNp4esE08H05nP6bJGZcbgN9vgO8/rvoqpVZs89KnNjczKipduB
zuF/MN/vADT+U5ck91QcFAozj8pw8DhsCEQiqA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12880)
`protect data_block
ipyQbtSKspGOLuia87gjRvY5wNor9fOXJGPKngmbF/NskbwKpOqPqfhGJH2690Je0kDg1WyXcaC2
qoCMRytgBAPFQpr/w1amDV+KPmYZu3uoLK0QGmpDVCaZJqjZUp0yBV0UGqhhudKgUXrov9HWJhhH
um09U20Hk/cuG7sx4pfJPofGNfKXbkPcDak5Ny7jK5M288aaZaqMgj1m/CCH+QUeXXJY6YJyavdA
zHF+TDNEJ2r/DJ4ka+7mFFVaLJqVHSI+ogGHSrRZT2IHPgSbEmQ4xYke0gA6NSn//hilJWpAnqkX
IuemaABMeig394LSHOH2FRPoOICJHbwfvJbf8Hn9veD65yEpcwEZX2qmNhuG/Q5pJIjuK8Zle8/6
Rb8mra+y1ph6CbtHwaj1twCtY+/y8Jpgysr2Ucqqc/eZ8uuTXRl0075/CpwJB36iSvUdY197ARDW
kObH5nhojDbao9kYJgguysAY7vcM3TGrgX82v06yXaCHAwuJCkByJzYQHvKkrambirjrdwPihyiK
neRULz6la9nfeZn13kTqZYs7ZC9SBGxWiI7+9M83aBzlQLdwaAODFoRw/0N31lisxDLCd2YleVnk
TVQ8krSHTk4+dLbtzKGEioT0Uur0BvRLONalMaqZGPpO4i2JFfFAtRIjkiEHw3MFG7NY2Na4uvoX
ypw13aifl006Yj6OICPF4LD4cBUSOdEOUSWefa02mjPIZ6Cih0uQ7DvTOM5guELpexTVpZ+XKfeX
GoeKxV60WM/cZ69IZioX61vzOe15WrovE2dLnJh946HiIuOIwrbTrmtj9Nm8MEMC0+5D20gtN7nk
9W3VJt5/BuNyq2VBWSWQ53gpu1Fvb9M+08ZKJLHXMluqxUH8BYImdRT/bcGa9Cc5y6f4MQSIPbzw
RQm2pWhp7gKLW50+K6zamHdITr1IoFYtAdq1pAABc5zbQlC36ExRyc8QnfTSZGFNGBNUpL/1a5ad
oOzS1kpi4ArBPe6bgmohhxg844BlCIp6puWnWPOe5SCuCC0aeVTMuy1LiHZDlHccbGLBx8CExdT+
BYYt7F9d2bgAoPPFckYa+/Xyi/NV0qGO3geXl1zszpvzE00h2D9UHokscqUHMJZuA9Hdaj16qjGq
KIN/Fiq2k/7WtvqwoLOycGDzcDATam7OJu/3HmJa+D02mHPFM9lfypSz8Hd8ywrCmbrH0nU8Zotf
GQ+m9bo4xd27FKuFfQAqjg786v+gsfzplgwfih+/vBScQXOsZEA3OxEVY6ixZJz/Z9DmnKcJxbo4
Xx5cx7JswXFLYPpEq2YCTxnrQ0Y0KSXJwmqn4TPIEh1x49ypH/DigCnJZmdQYrqjHBFYNFsgCMVv
dpqBCTXdnWK7Sy4I1VryjYXL4cqDhUygRzj25t13c0R+JUXBRHE+rSgm3kTg5deX57sueZN8/DuZ
g8Yp9GBJpLEsasmwE5rvM/COnAyXxh9zGiFU8oLN5KxQule7al6yR2IkmBr1sNRdkkW7m7OZ0S9o
vz1dCUBrnuLpN7uzsXiUIH7rnLh25quzZmTjKdpAopOJbB08fxxWNBPzNEatt8cVLa+UXHGa2sKx
sWWM7TCbiMIDfjGYCI3TH3gl4P6I1dY4IRbgRYyCCRLyWzlu+y2cZByVbaDFtjEI8ZnLb1YZUUZ/
iukvgfkjLPeq7cqzS0vdMV7W3xn3HH1YQXTUd3j6WVHKBIQeQ798nsq47zUZYVRiDplZ/IIgWMz1
V6iVH+uOSWxD3owJzb5QcLguGvSJkn6wFCV6PIOFQ+lMyQqG76iGN2/C/aODg0Eq6ke39kfcnSJl
fTymHr87mO6SovIDXo06G2CSHs+Aylupn9V26X7XD4cIugpDutBJrULnE8vCNENALC1/WDnWP/Vq
gN9wJ9+BXlpaIWu4i44if9Vx5gqyIEWG+pRiHCfsPiFut9mN3RhFKoRofFy/+/47zbcRTp/E8zOZ
KSptZfDwMU/BeEYFL1UEmPw63/0FMnRkpEtdxhn9o/YNtLvEBWna5xoPStt+VjZcF8x/sPE5tBs4
Vgo2glY6PmHrZYwmIOuzqQ6C8Sjx7LFPcyhHTi7TrNCoz4yGQMbA1vlJEjHTQjbIlJw6gNLTuoWb
9JDeql0PqzQUFIFk4t3+R7VNb9CDlpRR9DouMOhYzICxUPlLLGjmKT9yDgayfzCHzk7Ky/pqRYJk
MWS9NjeBbqABw8Tskb/WRWLTs0M5o3XP5B2/WPIwedwaEVt0pWUt2VcSXLFixG03SRkMGa6AJjYX
8GJq52uja6OIYcj+JyW9ricvQatjO370sGNGTzoPVQ1LwIroOdnAxAFLS49tbonjsOHi3bs8oq14
M5D/USr+hOWhUGDKQcK+d6JmA/+wpHJN1d7c5TLj92lnJIT8GASuuVIg65hA0cm6rzgt5A67bSpe
TvvxLsFFL/v3hFw6Ora3ST/E8cZhj4nL1TgvK/C4gRtuI/N39B9dNsJL0sgRUQfVMRgmhCw0x9Wy
+tHAL6w6M/koFdy9uhQQHVqADb+fNyIzv0q9AN/KbMgPCBvYxSDVfeQwOrlV5bSFRYmnngrDRByg
yBwax0uiebk5lZaGJrBJ6O1WH9Fk23GJaSPZHNIZEL2kTYUhV4WZyu5xPydm4G8sQNM/+FnBG+Dw
OAzt44nzpC6uGvD4ilx8ys8ZvG8CIkhU3bN4tnU7P1pXeHmcBj58Bzjt1q3hNklDbfa3z7RKKa0F
0pbuil0AafXhajc4ewaWjXl37kpytesqazEDSNdHe2U+dipq1am8ETPClV7lL5i7ildVjJU05N3B
zGnuTu6+PlhOtieD08SlrAOxhIQ/LBBN6R3YK7sFoELFoNQvVD8foF90OlCRrn8jd58W1TlnmCOz
P/urwjuwcH7KLA4Ury/pRRNx/UnHoJk4zGTaDBYOYTFsGvdK/clGiUUn+SAKctIFvNChpqVLxW8w
AnKvrfqjMUx8q1Tu7uYroMiB9CM0xjVDII3EoQhVjieirJJDATlJuYBZQsGe7ySTdrcTc9IB3Nsi
Q3i4UQwe5j7w8f04xrbeVxjzhjPjD4xcwUcz1fX9bpmA/PURG2LZ65aroPUmqSRq3ZiIYVDt795n
z30e8m2OKtV3NoTUMkB4GsijZE89n2fAV8PYBEjkDfdV4vWUQgE3BrWptvWFCVIdd9Orm5GDKwg7
ee4lqUxz6ZMCwJUe/zxwWcbMyTW2dvOVaWbkJFAsXa/JfqgMFAIeNk3Cjcp8CgsZcuOt8yIeSS9O
33QtKcB/SyFOXTr1gBHal0oat2vZhJ39X4YB+cQ+MufdkVZudUAoDh07OPI8lnOjVbd3tICY5Ovw
O1uKIZMwk5lEICe/+3Go/SGXdWxgs/jV0NuAqKnI5tRBvMSDNupCiF1PIUNoErmxMlC8hgtEZjgB
cQn9fYjrf4nwqYf3yQfntvsAE4D9dMMdzYy4rgKsJZUTaRhljPUfhUy7DBe/Q+zqAZq5vb0dx/YT
plb/VEa4gGE48LfKFX8noipqoUVIjilAjZxxZU8hCSfceAB+azt0tEYEO9ePQTv3H59wT5j9QJQR
uTN5id0dn+32xFQFhtd68Se+hgNYhvj9QUAT+zVaf4jNkP6Foqoc4fmLRoT1OX0PIQI6UjGQvieb
3tdlHmwsl84x2Nb2Pnkfv5sbpSxaghbK0zvWe8/WrdLk3ycxTmKMbU+rBHynpYMlBARq4pEqVJdL
+uaRVMYq0oazpKo2YZlWuFuhDMdCR7JmhV7wQTokNFeDOxxxpJLpGCBJK3f6qCKVSOPMK3W8sRIZ
dO3udqQQT7ihVPkcLcnGqvK2Ac4fAo5Aoqk0w2O0Gskf1O8Hj7ocOZaNPu38u04Lg2oQfJHv15z5
IjR+IN/cGGEOGHuLb609uidJ4nMls73Ab1VX2ZgC657AqCV2prIH+Xt5SQNgYnp68YPX3TBE4Iy1
sUgiOf6sdU88qo4vQfGUFMQOS7ll+4nXcMwLTA4R+kh4gSF+5c1md/YnNjVRFpIhq3+bf7d6DMXh
tU9Ym/auGmj2Xh5tPLKpHRqnF5az71mVWaUuHMTP7L7GKTwxWAOEn8lY5IcsOFd8pCXv05s4vAje
FPPDYEZX8EHtd+NbBF4ck+ve0uKL0sjWsJntKoL1poFCcxc63+7RG7Q/SdhIcgqA1EteHM7C1QDF
AgWrDHu1DXn2970AgbacXIFLI4K772GgO3Y3jgXw7bf+owpOY04IaIOxjH1K05j83/XhxbfgOAah
heSaHzB7R1D7OyUteOCu5F1jKNnKo2nZ4tgxvMs3KPRQwak6Cl4fjxy/XTZqYFI1u9R+T5o+pHFh
3v45cNeX3C387SnkO8ohsVWeJLTe6r8D8FcAoi8Dk/fFSn6z3vtrxBAcFjGjAiizREgVu7BjXjDn
nTgUTd3EJ6eUYj3AcZfG60p3IRl0i98RwARWh+y4SRJLKKmz04wPMM5BlTe+z5nHzFKWRPcYwMzo
RI+qO5uHr4uu1u0UqaWpAVBIW60fsD8t2qIIK2cDFAbDE1uFi0ok3jKvYtA+T4wkesxmGhLTAFTD
QuXF8+p87hPvuafw6wtsnQxDPrGb8zbVg4OcfegE8CSROWa01COcHHnPsCE3xC2iTQoryoWCCxTj
qNjCM3Gu6ILyTjgcOaBSfahnAunyXNYuoCjIoNEe0vYL/ScIg5Co02BH2oyEtGrKi/uxKMT3Ra59
crJSSXcmwzm7u0dZCvYZFr/jRA03sSxmRdWAPW11M7OEZVTZqWlHHbEOaa/Ptipaux0G++RSsntL
tFnsv2zoOK6NF78P/6/0sJq8bscyYsV2q7Yz3B4b+oQjmjz6Ly9Irq+q87LX//z8abUbm6seCcPM
dJmW25JCAX2drgwM05nyFi9S0zLJNPQq0D7W4u6BgPClR3nTqhaHNt5eRyFqg+UeR1XsiqtNFsKw
T7RJWIqrm1cki2NFP7dvcHSEQm0mrR1XH4iIJbEEdUdcELS9+NDzJJss9kEQq9G0sNqOhVly/cgW
t/0b9Yzd2Bt1V+x8xRiI+ZdMw0Sq5vpHOLB2jxemQEl8yQfLV/6gMk0IGUluToEN0sMfcobJqvew
LDiHFbZdkdjneieTOkbZ8Kvh4cdWyldJRkhylPVlzHN7ozEYP6ZfGspXPKJtr2IMYtOmSNW5vBQo
5x6KmPAkvbMT0ccFyOyPJDYTjleL/QTSuhsK+zhlqyu1qwkuX38fB7Y+mGDLaIvgXhxko0g7GJu/
JKbiJt4l1Jh8x0RCIFMnaRbAJ1ocyl7Xs8IeLY4Vxgu3iN1IxS7Ii++PKLzZk/Ht/BEhBrZonGlq
4lzwrpUuSn573q2rubR0P/erLG4AMuDUeudxt9AZCvnB3j8CuU8kZ7EM7V5y5Mn+T/JKIp0Y/KIk
8hSLg4GuiXsyYiS6Xcg6qrbRTD7i6JARhXcsfvsTsLqyO0fYlL/+ZtF90leR24RuBhbD23sgmYLQ
R24UkqBixZMFiIxmwgeKs5fHkRTYmOvCpsgrG/lVGAJkQoH3ee0sdZCdCUPCnaqJrKgQdcHDvWI5
AF771TU68zpnsxY+YUd9SWZEhmXMyyyI8wrm65K5aOoilvFGDPxmaS4RZ4Z3OvKUPg4q9A0quPhR
mRWNNr3lRuNspVGjIoMPPPN0FmWiuePlUtizPZscAdOlZlVIk+iby3q209zmhF/8QvFui5Wk1KQv
/7nWirHBbexfMfZedIzH1KseO81wYAticS0yKTrbfe0GVbK+QupTIe7eCrz3tOSGALhMfp8Sl5s7
GdfXY/TtEcp/fHR3cOas4xNA5RSR7MpJa683dRspIbQIPi+XHXDcTJ4N/mZOXYu3Mth5uPxVAovA
6tsEfLO8mdUvwR/ARbipwPGDWX21GtM9PPQWLEYa2RvbSsV7VYQtgH+zlhWqUW/gd0fb0wzSzSqq
04EBSoF950AgHHE4PpZn5jNMejGrOBY8QTsuOfi8GYY5hxfi1aOUGao8HhoV6XjLi6onBKCg3mD/
7DgdyESLbH0R4ohML1fSOpxiHCf1UAZPVEOObrk5uYi7shS9W0tL1ffqYB4827uVF/Ap2m/2kU0O
pOAEm9O0quCWU6szvvyp584LbbsG/r4QHSOamEpUgW+Zpt/lj/AHKdcOVhqVZ1FDB8WCtglyhlDg
Zw/ASrYO/RxfWKJtai4yDekOv1I3q5hEF6QeAg2wcUg/fESK5QHOiwvYy8pe8DCwTDOxH6dlncWK
F1KLnVeQzQP+SWyATComr0Gxsyu0NYXkEF6qrGKq8zHNI8C6yPsmOMUYb/wGDaEDpGlHHXyd7keU
6HM8zZbjJb1rGtMkU75W6IVc0CDjX/060bHFpoek5hyfSOwqylWxqoUKNuluaVZDeB3vM4GI5Q9q
7ozbSle/9rOcXP/4CDlqGokIM88y4Vtaf/pU01TaqZlDuif17Kc90L69HS4pC5Z8kw9M9NeUE3Bg
6Dx86UP5Ol4hFWM2xGGLOlclbmW2pnecE3BYxseAdsgw37rEjaVa5QEEiViIb7M8MN9BQpX4b/RM
pDsWVmCqxZIMs+qkR2oWxCft0Ci2UDA9mLA5cIF4gkenOO8kPIOt6KgZgd9p+NGVJwg+6reNxA+U
DDTkz1RjsJBWfWxYeM7ePujVBoVlzLHwkAaoKjhnl02l+hb1YYeDFa95loQsVxkFwJxX8znuhb7f
gg0sm+9idg6gP+yZ5GzPT4wvXdrRsCgHR/TK62/c6zn3CCG5pjfk48mdYyUOShDDB2aOnTq5AZwn
YdMsRXuK9wPqidEXzFU+IbcXGZtTTduyo09pVowOcao9tDsLtnJ7NXbMI2qqc/uPrDRnY6uM1x0f
vGJzEx92xCYs5eoFED0uvxMqSIy795j+M2qghNeIwVzkhssZNOKbSYAZyfE/89psjcEyywPk2K7M
KaZGgOUGB7QtIsvEJnAexVY2OjulMTr5r44avC7lNBbSUaq7tGiiAMhmxbdC7nDBJdZsUNmD/9EB
MiMF0zIJQ4wJ1oa14Jb07cPqZaV1FHbUknLtLUy20WOZDgNFzCm6YgidBfaMpMmyYao7gZEZZ4vE
3rgY+3JaQ5/DC41q6Wo6H/bnEF0xNnklL0sL6s2EXQ8VyCXgqjivzgYGoPR0vprw0UQvrUW0kIzS
B+UoBiEb9SfW3KDUxmm+1qDgOibl2Df4RkZX836J0mKG1/Y3oz3WawuCXl2jyBzBSg1gQwAkqGKe
CLlx9AHaTemYCLcYfTW/ENbbLEoYUw6HoF5sE29Glj1p0Ah327Nkkok04j0qUeU21L0jcJe71ZCn
iJozAo6oP/x+Ts7znocDArkjmRwB6/KbFP3abJBLu7/jpbYrAOE2T+3Nl0a/Pi1DYcduV7GH9fkG
ebD/5WfrnGuvr0Hb3nrMnWsJiM5NdAvZxJ0uHwyjonRr76fb8XAe+9agHL38smxuB28njUEDHQO3
vON4R0BimlPrECtlaFl/KxwJnPTR210/ZLcC4iiiqduL67OJ7YnNFtNolJgFQwSIabCcudd4CndQ
AhqrpYcsgRdYJsV9JJYdRN84LJezS3bbKxa8vcGj4JxsFvK7UymQrHKIM/pz2UnpjhyAxoYQHO+X
22nK4q4ivvqu0gOfRPvALnprDuL4drS+0Tuzj0x+aT8/fq5dpbaYeViyCIBuGik3DMJ0C7mfDtmJ
Ulla3mzmUzMUdlkGTed/oN1bldDdgsAE2auqG5vt51UdcmCx6Ymn/efklfHUkZ6ecDx/0pzV23Do
E95mcxU6C2AlAe1uwb7tqT0ncCJ+YV91zuIecxOyJXODtNx6a5buEh5DtTMDuP+V8KnCOfjDGLu1
o8Zon7UMf0O9WieYKYXf/eNFnJbSeODYHfQtinXVbjYJCKUkg6t0SMPutC4P7l2pcwU6vcCbE7YN
D9K4EC1rH3hjSITuRTjW93bd8cbEAZIsoJ1pi+Lk1a0rkN84+kc+S0pCEpsAFB1t6fPu6P8T90kK
/P3ZYRAAEwhQ2zvjbX6w5WNa52pk0I49L1vks0evsnJkbE9eF28YzcHbYFoKdvNXTH/OA9zOb78D
NR8olfXYWtwnhjUfnwCHMGyEw62Vu8FKY2jvxqogyP7qKPtFSx/mW7ux/Bk8EBRUXbWQXaChIjKA
BYFEcesrkPC0vsaHCX6FwGd4fMvhvos7ix9Ql3vp4/ldzX5wDaEIH5bb4ZvFkmjmJPUHxvJlFfGD
j+MY3vEunPepEam9xHTG4hr9gAs259B4IhMXZYh1E653V10u+WIOCAsafeQJgfodSZPsz8tFJhR6
D58ifgQItC3VEtJF7UDpYMxy5HG5EVSJjGQCR8Ng6vKlf7w1UGWbvrxJGNURTzznvQOQq2pGAEXp
8zhdfzXEm1FoNoF82ei4WWKT5kVNLajQAdsUedSDqs+QrqwX44Yf3+vDnADHgEoN7ZcpXjJvgXgj
8ejBNYlh+SWlGMal9WvzRZw0w9jovJCo5ZViUYUeZkscWITZA9yU6ytvuGidhQuuFTybotZK5Q82
6oLKyHtjORfv7MgLDbRIg4XAINTQXJ83CNiqsVcSTpOe8yqU+JxyEtMSJIz76PrB3UzTvPl5hsZD
0kY3Ws2auCd3fGZfS/6pwr1QZ6XwQpOMA7cliSZFGj8y0u5iQUKBdGraqI4/xpklRMaZm4y9TTTi
90jtMlLTOaBSMJpbBiwJ6sxRzmF9GOAA8646ZuDP3UkYZJYszGcZQ2xKfre14Ijxej48qE4wx5ww
uUuZwGBsmEkCkPgiemoohAbaPaUE1POXFoRGh0PahyA/qa7qVr7pnO65abho0SDuac2IxdbTNPC7
BUOrHj3SSlb67Hsd4cY6kSPwVZV7TTyOumZ1cOozsxTSz98MNEOxzRaYNDAj94Bz5bcOAmARfcdo
K9yBqxPpchLC6i+y/jZShpGoKDsuUWZ2kXnfcvcUp5ciWMMhq5r5EvmNVlQAkbaQWaOZ3wjNGIcL
YloMVodhXEQyqO8b+EAM+nWd8D2GO29jWlPsGvbm5tRmyn1KUZgm3CpuBLWX3h4aZhfJ3uigfvvc
RNjjeMQgZZZMxPIcgS8P4IYIjoCd/o2toimSa6zQyGhFRqKaUBm8biFwGmYLqUn7GpeDjRDupZH2
O9lk2WoQ0UFRTYifpM8rJolnotNic8T3c1Rf9YdwwU8Mmzl2/GBPKdBiaayvwgl0/DTODw1BTwt3
qVdpe8CEL7UEt8VtFO/VkpplI0bXsEuBSaY9zyLqkWpKcmIFNyPgVHaBIdlNLTLjArWn8kbF6RtO
wijHo2TFDxEmhhQ5/prW5hf/RNTKTU+pkGd6ACRSYeYl2Rz+8uNqJhEfRJQ9fnWpzBEpaHkx8hdo
SmFkUQpuJ4Ne0DiHp/Lo0IV5l07pSLTgQR1BKhLkPu0hTHhSKxESBi2JZBM4/Y0X3tQckkCjJCHJ
FyXG+mY7dfKE4GX0oE0aVlbSGQBr4bia+WTqE4IMnYn59tiI/NshnYn529YOXX5ZDYUNow4+2P1t
YTFlI0jH1usaohQHq3Yk2Eyh0gziUqSnNIZEcSGoJW/75PP3XzdK7ra0r9+bszOto6rXLxTquhYk
ytq+MyBEocQrgQeJVLGYsExBMSiyzOwsNsZS1G+HjigW7y5uk6gzEnn/VuSjIcc90RalazMtGte1
VIY2/8UiKbzwYwi65etGg6AnNKVYfal+dl/2L+AvnQnXcXmgMd4fONHT6gRD7Ue5VqfZnBZJCMKU
nRQPCRPGsttC7mPJYalukqQi2lUN7pfNR9HnJzyNyaVWoKMp6Yk++ltjOUIHPOHdJ5n5Rg5t6gih
7BIdYqFYpQArR+G78J6iaL+MY7SNQt1nNyFR8rhUx+sWQF0ka5N5tJi0XxiOTriLhubb24y2Gbb3
pfMdQyfBdTsv61cHuna2SfiEE58/zfcaNe206UQsuLKH5SgbXMH+VWlAKTUO53gN6ohiXFNFzkcD
QmXCqlTLM27L66gPugifrLuh6izOVscBbHkmBQoGkj2fnQLS3zcKHZck/tiCVf6lhi2MQHGoGKmf
RWEfeyln6HH2jGREf5C7hc3bWzTcE2ozk6lOvZEsJx29Sa/Z2IjY6nsLJpX7RP+lNchti1BQ81vj
Pel0YiqyzQvvRlVCXPNYF0aTiYCrKpWa+P1YCD+CDAM7982mYc+s+hQTzOgI1Uy5LmWMVmdcRxQG
gBCPL/4C+yJkZS+ObrEyUMB+/kUWgEdAYgkiwdCFGQ+n76JfOxXj0JBONUTgSNQ4PMQZ4DK5p/am
y2e1L6nGQllmRTS2CD+0vuxDJurWGGZJcKCMh5Z4DF7H2wfAwKFPX5N23WGM8aH4fEZIiyegMs+a
/l7U/Ng/ySFDvyeEAzhnKxJfG4vfeaeekeA7dRSZLHSevfIUWUdYD/TksAM5mihgJyWASwzMoVov
DMWEGLM+/FiGKMlO/axmmnYdV4260G4zeFtL+52D6Me8rfQVO17wZb7OBW9/DWMHXochubvkGmL7
Pk5J4dlQoNkn4dh9aU7z1wNI8kZz03QVWFtP9S0GPg9lJD/AZri0kHU4nTxTUC2rp8rk9YABpHxV
UTdBKBl7o7uCHE3Sf+ZZMu6qoG7iiPfbNzlkuCMr3GvXuJngR3MPLkkKZhjCo86TWINMO56oGh1t
Z1N8uzg3H4bw7nZEJiztfiC9M2sqRrsxYkHm9/6cY2/fh6pu7okImWBKHnk3apOVe1m5Pf7q09El
7mvGmTgMO0/O/7cIcqaKxE2USkUvHE5t237GBSKISlIpAgM9wSdngMDB4gErXVMWcsPjMVqxpcOn
awHMGZG+LRWdii/OgKYNqC5M3bFWOjkZFoYq5aGgN3RXyKGURKW5a6oCXLdMaT2iG7voVXnxe00F
Z+w3FMgOht+/1Vtly+aIkJJTGATCr4OnO9rNswEvZ17heFdLhyLzck2JqsNck6+30t0IO5WuiK89
158LUq8Xsm4kpWy93XPgrUOw0ONyuR6YH67yuZJzXWX8YbHbTiAiPxHUuiA8R89OaoE+076OTYBU
1fXXnm1O1ZODrRK4kCYjXumeOoYRXJgl4t9qYEqjmrmVlK5i/LFzIQE5zNUcw3eg/P7zrixo9hGQ
KOXuEiVuHUyOWozkiTbcUMfoGTMTFYkapJFhr7lDFyDBNTVrSIfiQ9whAe4VhcVP8n+ecKhPwe0t
pOJ3+UfHw1DTzCqmq8WAJLo3tQa5C6y8vkDzpxOqKWIPHlUIajvlOf2/7kSBo0OdJsOiPR46/Eyv
5vmzQyH8gwvrXy8GOMiOe/HEGTo2NVEufWoeGt1ywFhMXeKeT+upOWQCQsZjF8yFFbzSD8NZY+oo
zr8pLgYG9vOE1IKivRsmgALDLZD043zaS5ovgKE99LYQJaz/lx5ISex6p83kbfXwtUJNxKIP8XN2
MyY6BixGMzgWjuRn8radc7RDGbZAV0H3zN4NqxJ+bpVxLSYvldLuedOtMVABQ4efsalB18AX4sxC
y6xUL/TB7+daQf5sMUipBJHQH96p7mnybQu44nBjuvQNHZo/+QSKcFCfj3p4Pycgl/AvbQ3Dv+aN
RHkq9Id/rqnm0NtXjx0ot2+ibTeOXyosLVKJs7W3wmMmpVjoqnJUQ2Ki+oQxrjXzhslpPB56xmgI
g1UkJRCLNk064S+GeJYTsO+lZuiTiYH7IxVLXcyTUYLJmzXgJsD4+aIvNtPfZByU/U60impauRID
B6Ut6FlhNOcN2mdUf+qmH0hALUdB2dvZ3bTHrR7UHbIWijVCFhcT/I+ilWGeN/9tBWaDF5BSn4Bq
QC8VtANhK43vgmtH4ZfI83BW9IzqmQ7S8NtYLC+P1RO40Wff36pQgu+ChT05/AA1SGIXA27GDFp5
gUIZro6urWLxMqGqhrsQe1lUQKfyt4fzarfMiqVyh8ZA9wc28EAcBKhi3TQcm24c+a+fljpqKV1h
mk8+Dx7Fkfo2bfEnNoHPC3UPtxe1yykkJagmxq0IWxjxGYY8wz9l4o485FDoHFTOz8+hqxXSVeOk
xh9g6/8mqo9F3VCy/peLqJ3M9cC1gTx6LhhOIohK6No9jpduibCrcpajCu9lkj/X8aZDZnMtDVfe
55wWQTj0Txo67tctFtzoXBBOMRykDvWeFtZiyQWnKRdtdtvnGcXlhmqzV3WVG+oLCz4PkVfGLNxI
Vk9fVSC68D0oGso0ZMjVhVVnM2+HI+u/gLgB+1cmSziPpnTgSPtlxASSHGMJP7GinWBiR7AawgsT
U74uqcG5cCUIc/gRNE9dJ4ubPDT97EIhvWJQ0Irj+JORKuzwnMteFciwp88RrdIO2Bvfb5FGG4cW
evMHNUaX2RvsQdHN6KoSPkHlIoD7FAZpMAMapPp48ecaZk+dYyZP/d2Mndhjdg/+0GgCPaT8BE3X
Gwc+eQ5VIVh574jKpqwKZK46Gamyom9/Vu+6uRWvWbNh5OXGb9D3LU8YPksIazca8WVOvBF6gqeP
s8H+utTLZF26gZ8abQUDeo626Wh9bQtPSR9vofWBUZhJrZ9IQNwmASbe+0Nz6FwwLU86pN/9upCK
r9qhXRFwjfKdht0RI4FyeD2euzIS/TJ/TmHFHO4xnabxAZOajzFlezgmGQ12L6WCsIiw8HcmZ/po
OYs0aQC2pVmIjR0iHbAVkfpwZxBPij35Gj010iXvDb7PylttFIX02egVRhUYi+PrxAjdwdza6KZF
0IyPPIxh1vdXOhY17s0z5O7tTX/4fPO3MHNSKv8J349g13nFTGN60YjDJBfybQZHfZeVNOC4Yh2+
15p1vBz9iXYhDu+55sVRFoeShjc/PneOEerKsegfwZ+Asr09c48SC0STuotyQfbhvEl0BEg5Lf8M
xRK6KrqbbdJmxLFFaieZDfWb9b/lN0s0NCPC6Bvs1B3lpJ0uDg+DdVGu0Xtc4B4HgjL6znfA/qOe
l+s27j70uIgt3uw0EbVj9r8CGm09wHYg5invopud1dErYJtFtU0d9scbN2Zx9gA0QPpuMZjHy09O
lGFAXVZsewSb58++HgAY6l2x5avAYBUeJy2lAw7RiyxWUKI29R36yLg7FidUSvAPxqRjsPThHZlW
gNahBG7HtpOMDmHT/KcL4bZACe8galVs6QKCBqo65qoc7oVxanAXmyEAwzri6He9DezE/PuaBCav
IQSHwKcxlGFOJQrXA6o7WpvogEd2n9psJEnvwA+D1phNCKql+Nr1AYcuKarwvVNYCLf3atrkkiXI
OjStHkvWnrMQ8kPxlBkYdRaiNPBQhefw4Htf1CPKbC9Avh+5c1MprF5dvN9v1Gcemxhy4fwe/S16
gO9ghj99zAODQSoinlZTTVSwNaFa6DvhHhMuugdThrDb8Tes25KebO/v4bYSaO1erdAVd9GUXcpG
vM8N8PVG7AkkHClT3v2kh0gPKBz2xtHBY6eNetCDeQcQYol7zorRgTlRjm7jQMT/imZhovbD9qGp
AJS4narfboYow3Hb/HgMC88zPOXpkQVemA/Pg5hSVdqIosyovYiL5T/uPc4fT8sjvOIvdUx1J7/8
nXtdAByRtahTeKgbcajAIMnbEVcg/ia0S0art84FZb7ilfv2/gA+LMpAk2UhiVLnWYTvVGN7zdlz
FYqvXCJnmPFztBxTy6Y81BFjxe+O1dUCVRGVCUgCVJrj1IIWv9gKQ39vinDdcYy6O8M0J7j1PL96
M7nGTYAlnUeFC6Vbx7MBmFlHR0fNusgO2bQYJZcJFw/sHKeosbc1JXARuWgptOH9yLZaMHuO8+iB
Urq+YgtEM9bFzOKlg1kdLf3V1GyWffyKXme9qboYiRgoMliZvQ5khmrCvi/1QODyl9g6rWpveSOA
zDoQQic+6ea6iemzhonMapwgxKu3gTqxR9a1iWydmiS27pIWDqi186U7dqkSqv843A5AYW3+I0SR
z1whRkDwlIQZcE84hmrZ13AErcefq3w3mQn3kThjEGI19tLQRD5zsZD4/i1hR3iiGav/tARMzt8E
eIa4bhM69VjBdJEegme2Tu4wyOXqS7s/ciTWDGmTQVVtyAUJNOew25V+P+0nlBDy3aFETy3xMhFS
PDxzClnswAsyqkwOp7NFGpEPfQ7TTdo8PL1TCaQFurynn9bq3+G4Sm1eluZmcBW0zSucu7zaWa7S
Yo81voRgWTO99wdhb9J/nwagD7LXSjXcR2u5bRG5/S/RpqKOid0UJOgxTpN928f5DgEw1ZvhnSGj
AtN9cnReW3VlJ9C9VD0PVZSPy5GgBZnRzIiOUY3pbAH3F9LXXPJTwcKrksUIoJsjMc3JpWKjpHhK
+QrmNMD9njBRfSPsewqHs3uL5fKxeSDLTzHe1O6ROTBl7tH2fLfjcHdLhqBIkhg1uFGV3ecvl4Q4
WepSqfr97yoaryV1tvv4SOPY6juE6yB4tRVr3ewKoDQ10IxLg3YjEumK1k3dynanCBplWmGOMDCn
ksJfp9Ysl6MryrkC6q1S1d0vLmKK2wHH+0yDV3E/RAeYLtHpb44JxT+v9f6dgFCM8ghJc3YIbCvj
J+jk7ydkW8rVfjaisJ9f0F8+/jGrABBFNuMqYOyZFGBquI4wwY3zu2fNuq7RGfO4owq8w44+DK+w
qZxEqW2/b2pSXvHN9+3Xh+/DDh9fz38zXX43AVR7E7wbfk7ceLRJEPYune5aJO+ceM3QhHBn2q7U
mcupjeZp0Ed7njPVpkiASvxH+7oHhEQ9+LvkOGGuDtBidSvIkZQDj1Dn5XD9h/efEKfGsJtMdnf2
kRj3R/llwZzoQhDWn681S/jdAKzuS4/CYDVQXQX/TIu5HcGzs55j9nkLD2JbmMbQ0sY5K4mnaF6+
6oXPyQ+zYkldSLOIqCmkEwk7awvc0ZSv4Du5Wz8dL2iTnwTDjSDXXWFllH1NhCAC9UeY1y+AKxmV
GxQjlpUwLQeWHxwrGpV69KsyWUruAtr8gZ9vowlv1miZg0NPrLQLJbB4wXk+2t+p87dcS3/pLGWW
Hpe4vPkey2Nalt8DO++iHhzOHay6AxYwOut4wESFBZyksSUfGUNPlV7e3yxWqkt3m53bMKdP3gr6
OHqiIcc+lBIKNa3QwDTt2B/F3m/AtqYp5JI2ETfnNwfdRnid5q1Q1qffOsCdQ7MH2P89M+BMSMul
WBQx6Gw6y7buJo1NxCKS4w4Tdsb9KcIp2SDZiP0b0KbBhxBVTrr+UbYYyW7Za7ZJNOjklqU8/2Mv
5pYEuInaq5pTj7c1P5UJZDan5oPCBUsow4jf1C3jU2VETip7JzIdFRpQHWAN97ApQA0p7TkcouLs
NQVxWGrwjmyiDQqI6IHJsgMQjjWfn9jXBOF6NN+8TqsFeRvedHe+6bgpl14RibeeX5TM+OJPq6oh
HYG1zxjDkrFPx1ihMXxmpBgL0nOtqJzX4aT7ZOUfdOIMCzISTMlgf53TnkdFE3I1u5X+BWyYJ3tV
k0fhw94yztbqumpo+5VVVK0VCeMRFZbnfGdWdadLvPopErivKJco0j2kgmkT+Wq1TcRLdK65EQyF
NaCNYUh/DP8O28eomQ6k4N6lKRNKLKEkU9UAXme3e2UsRnHe3s+01cifx91i8ZkuiXdQ0UqB1azL
4iYlC8M91pr6c+K8Rhh3/NhJeSJPpNirT/UcOwe7nVajj4brty1zLYwtybNAFwfOLTeBaQIuw+yl
pdkY0FkxDC1jBAaRoLIAQlxEcUff8pF9DvVPf72rGSxfZwKC4952PRX/+9vTzENhAze7FTdp7obC
V7z3A9lvTGRumU0bSMDFtQd3TMTrDpTo73QE6oM8K/ETcG8jl9APswJsV+DicxfMT3OAEEPJKhC7
pV7jRX+aMezpNLIiYjZqdcfT6JdZSEiLBN/QV9GnGNt574ydUM/Et27NeG9J5StNCFObiq+PiopG
QwgPE7af/TL/rdpbJ36MdVURP1IRNwUUKTStOLd6N+uT76nX66eBm79eCp9BLyrP21ij9+U8OL+Z
c5P1oOFmtBG2V8DBpY5hzQN+eUMG81craFJ3w7vSIaF3OQbSd+UfHPEirtmZ7WhPq4UwWTfXWbgX
qNS59di13aooPf8XqJDy8bUeA+zS1hBh5fsfXCMqG8G/1TUxkmxhZGOa5YC/EwAcUxMVdwEYnNaT
Lh4u63Ow5GHJIzyKaULdhhef/sNK1zMCk4GlRlFRMOTuXTSTIkHNUCt3b8VFWM74vaRRV38ZeymW
2j95CS1x+hRafi1muWeuVuU3ZymJOgnDavJdEqVevQK2DhZyrz0RU7d2Qj7GXVPqiQRrodZWAF+r
sD42EE3A2H+MQm254pG7iktNbYDyVEokMoYrdnY3LCpzej0t15Z/ukqumurjaGfwTYWAX5Q+wvht
Ocw9Me/nhVz2e9deCAsYGWVqBEuCTtDkV6HRcN9FlWlb2m81d2v8XX31+yVNtKk3TQWIOacD1QSL
Zqyftm+mbmAO8nMbCLvtxkiYSXYhuv3lJH5kGQxMQnb/wmAzywZNz4frkZ6H3ZnbpSHrkVHsb8WE
HvGa57TEC2JYo7ZzM0l4I8Vb113g3Vq9a2ZaIQ3/1qjQ7uTCoag7xtYunGEo/muK1A1KnTQw7uiu
5fAfPzr+GbEk4hCHAoyRqe3+cOKwYq68Hz81+PmO/7m1Bi/Kw6oCOMDwIrxxJ3KLOP/q8hVGTuJM
S6hP4AWaEE7XqMtFILCeOwt+dMGOHSP2OiRKz6d8B/CbTqx7z+MH7IFawQXIIxPmsUzQzVpuESiZ
hiPvOx3VljF/KCoOvkpDH1SJpak2q36Kvlg7NsrDvcjLR4QabaAOtU2yi/8w6pgrJjGlnQtgMbVY
0Kji16s56jQ42OgpYc2MZd9rJJDoPeVW4C5tdvxwUEon4ChPmTg81UZszPmz6Ei+yBdVsmy1sIhK
ZlRRTjsIobaiAH19IpEEz0GECbug7PL7SCVs6Yu+7j98pcs3pnhopFfs73VxRFGNkh+DnkS4MefU
rAEmj8Q0LA9IgaaBXJC4+tlCbFfurMn1jo8bUreLzEWTrEudiHTRAR1VP6O4n+fsjMZftclQprIp
yyoh8KWDz3/hCHAoVslE1sbm/zJalbz72XX9rsnCLnMhc9Ez1xuWf7yMsMVTHKlytLb3M+wgnc2G
AX38usbC8FO53vZkb3SEqAOGVfuFI3i/QE67N1lLhHh2eCdOU9c6mJtx/BHgox77hM1BRTmtiw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 0;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_25_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_25_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_25_1 : entity is "design_3_c_shift_ram_3_0,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_25_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_25_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_25_1;

architecture STRUCTURE of design_4_c_shift_ram_25_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_25_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => '0',
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
