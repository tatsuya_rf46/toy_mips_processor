// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Tue Aug 25 12:00:27 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_26_1 -prefix
//               design_4_c_shift_ram_26_1_ design_1_c_shift_ram_0_0_sim_netlist.v
// Design      : design_1_c_shift_ram_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_shift_ram_0_0,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_26_1
   (D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [31:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_26_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00000000000000000000000000000000" *) (* C_DEFAULT_DATA = "00000000000000000000000000000000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00000000000000000000000000000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_26_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [31:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [31:0]Q;

  wire CLK;
  wire [31:0]D;
  wire [31:0]Q;

  (* C_AINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "00000000000000000000000000000000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "32" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00000000000000000000000000000000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_26_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RWL4V1SXbIf6i8pFk2utvLrqT+eOhqITFdUnFMBzLcnkwfhTyjNDu6NaMPiA0SzUaJxCQBJxY51b
uwRg3R+Jk8tQKQ2gohPhWyufcIqHKOIo+zyUggTLQPP573gQ/NiYLuGI902Tgxv4/suUQanXzVq/
0VXPQTKGRrztZG/nhxloGD95/W/CXAjcXNBmVh9bovWO8euSEv6iYlWIhee+FIBava0dGbgdWYGM
Xa+/ZPumUzcejqBu7OkipzEdFbNaPoCtH92cYm0jolJdVm8NpV5wOr/pqEkTRDxN+6Zvl+FHyBw+
gZjnnQMpMEY1purdUdtZAwpj75wrpF2ydWkQlg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
e158wLxVdq+d6mOLqgt0lGcObktLvzo/djtnIBIQkddAk+dO9Oz8XfQLpB3OaQw2zeZd0eIvh2fM
lBp0FK9QDKYWy/d0gnrIEiIk5UAmCq//soJJUm5xlpe1ED9NWzZEeTFDSIvPCMWDs1HC/ypwLOPu
bcYZnvjHTyhMSzO+HEeEPiINFHK+3i9uQRSlJvJV8d/4oz0uA+KA33/IRLgEvIVBaBvDun+/vYSD
VIoCeCLNW8TdqeCjh38X+ZbgbJenbaYe7uUptf/P2tZENhztLnDLEV/4i8cw9JzcEjf4RKsHvHI9
8PMIpczSKJSaTz1yjwNzTG3LOYhXQUdVps6GgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9456)
`pragma protect data_block
MRGFjX8e1kQ5DLVVMyhot3FosUuZezNlMPQV3dHrll3TkX3M9StBgig6GOfWty6nXLhTs0DJ5kwd
N1Rt18QufGjDcBiSzmREQYdaaS9krXE7pQCjxgZwolzJwMljzVe3s8AUVGYIl7VdeeYJk8YXJKP8
4CwjhIHYnPH6yXAu4VpLJBip5t9YyWk55+/0odXABv8faE2nZjGt+04+HXcsUlzs3VX9pHCgZOKz
3jzciLoJ40+n2eURZ/QjGL3bOs1seeKk0TnNVUr4NdJU+Cxuee9sIa9eLMyM3bSsfX39qx3K1qvB
p8CDnf3NfZilZomhTrilVKGZcpx7C0lwvWO6zPuSuyJL7UwNGSvP6X4pB3zTTNgNOkLF3YVQlWlu
u4SxvBELaeLg5e26llm2JxbzVjBX7eJBgtvW1inSJF6cm+P97WjNINIng5ap1F6ueyFFY5cQTFEV
PiDkWiCN2fg7JH8uHbbzcre/a6sr02ffSh2Rw/fgoTw6mhyic0o1WXnfEqGhenzNrMO0OC2vzmcC
VsaZ2N35OyJQ244cZ9HlKelbk1BzSC3J1AYKXynNgo76HCgH0/k3T+Gkm+HzRpUlyHWzAYaMkn/2
og6Tb6+UxxWjyXE9aS/CW6pphQvLi5y22oCEXWotOatP71crEmM3LMOp8V7HMqs3ldCHhu+NtXBw
5W5pgh9W4wzH8VZyHR3NoyULFtBCAUnQ447AU+LcOmAQ9GIQ9/J5KTfQXqGwRTR9Ku/VG5q6raRI
p7nbKTluYll3qLwvs6ENclFu5qf0lDYJeNhFknq0DuX8YVPOSxLUBz/1pancFLTM3TOyLnP4CYWU
IRLnWuPNzmLC4seBY9JZ46iW8lJW4hw+AHiN9pFsOOZTfBNBsmfugkXN6YZfNNLVkJ7g+HVR5ZYP
Nx2HweCh2K0fIZXEW8OjpqzLT53pIWIiB4+6Wp622O7XVKaNs7BrBz0leE7eHsAz09SA3Uy5NCQz
RC/bbZBCu/GF/1ojnyPe8XyXplzw3yhUFk1c1BFHL9QTeEx80EAX4y1tfDGsq8bMzm8LpcHG8yHq
8IXSvtbVmZzgNmQ036BRrsz63M2ibFvMEjBXLHmchWYLPrelBZ4qQe+GVGrPIZSARnqOPp9LZAke
+XeLwIsFzJRBM20SYW4HC+Ofw83ZIhrtb65RmRF9HX8+luwwXXCLp7uqZyiZcspEx6u9YY50vHFs
cxOkz3IRzg+ifV3Gy0TOYBU4z7qNLt1hrEQH9+0pVM94aUvGWLl9lVLgQLYXiKAUhR5bT+fSKWI/
yKlIyXPx2zyqj9esQ7CUAPC6O2CSLJdNo+1fhsPwZnGEMuWQKw8ngNTGUhvdSwQq8pHrQurvzKBG
N60We88iXW6SqwRj++ewtcEo8//rNJfJBcU5R06tFEzPu1mf+dCn0l8rgLxWLU0IxU0aJ2zBI/yb
iQtkLjpqgH9yNRwQJDPxR41ltnj1VPScCcY0q6q5XbrkGpCr6MGRXrU6bX35ZOIpA5V0J029zHY8
+Oi6glm3WpVlgm3K4Tq2erm5fLKeRt2vHCOYb9pkgMVON5ERTVDFbZqYPWKBQn+V1YoFjLY6pPKW
6lda3rGEbuvh2sZ0QQHE8NKq7OBEAY3r3zsQak8P5bgKDi9PK+orcokm8YmV4Wc/Bmm+ACFxZWda
WH5JXk90jRQvrmSivh0FCmVaizi56XrEuQMbyo2xkJy7CqQi46zSBSXBdO9DMSLui9RXZxXLApOn
teELZGpDAVXed7znsdipFi6OBbRe1n608ER3FBxTQRUDBHRXqGkf5UVhNWrjxAhoDeigwlN6N4Iv
Z0Y7BcHfKOdHA3Lt3qG0Xj0JjYQASYUdN49p05p3CVr2V1gHnJmu7FFI8wBfXwCOaCLjsn/oExUb
4e2NXkyb67E8VCxuZ8NBzUwZGZoKl3QYkKiI3GLyRf+ML4C7PclqB238Sfpteqfc0c86rblqfmIq
xLRjHyg0CUNKz5p9xSA53suBd9rEofTXyEh1Ur87+2bOIPEK1V1UVI57eOoE27+qMAVB+fwVnnOQ
/lh/L9s1tV8RIXzMUcsvPcx14V6QLHsHsrRAgGDlcy1qlgNzXB0bu3uN/0gE4FieYjn7YxxXBCHg
eNEVcq+V5fJAqvW7Z/wdH+I5ThhZ0Ht1eDrRKxbgjJ/7fGAURWiesyfJgHiHrfwJjiraoKZ82PW8
+ij2WZX7pVWtrNrfQSjfUDkjgoo3ljUNjiVcdJCNBJUJOv4HexJFaki1ZxLmcSkbNphMrqDd309i
RpiLYwQBOo3KkXPxOdDGMMuGhCrwoYZqlMxObtDzKkIaZ+O+WAXRPuB0H0l5+uDhglzC06JXXpVF
/m/GljC8t5ijZUz/xKep2Qs1P2bG+PeAVxC5qP9FiUEyVdbelxJxEdsXcWCswvfzzub+rEzK2nqf
LZe47xdBMW44kVJTJjS5S3wu/kpm2WGyWbZUBs1mi5ONlagUdmn0RRN/zMIUQJRwgVYjFGzXe4Uj
csXBn9d9/IinVK83NRZZQTA8MpV0Vv1OlHkr/uVkLid2RJo8Sqvj0t2ImiipgFHilNUzD602DIP+
YNXB48Q+roXbquLnDFUGAqDyPptuaC7AtAtA0CYIJXPbZCoPsiB1F3GRDfkgjgT9D3/U2wcJgPPp
J5p2R1NOXIscJY100Y9v6au6B7/9Gx4CCTRWGNu721BxH6c6mRLV9NwlGkviGZUOFTwvhW/09NKl
zwVswtyvNQNCwVILnvgudTt40yKfxoc8Am7pyZC1zSPCfZrK67HDMSLkMy2NwXhpcMoLRZEfJPuT
CIm28QO1lpAA+pwPoaZs1WrXdZUFa00qrMBo7ka10A55lt7XX5+hYo/w+GLpTPak3OjN22ecBRhA
J8lotfgEuZLvyClpTAYooMoGGRLW0s6Z/ya1ykL9pr+DgBSarROYLOeLLpxsPIFjvyLBYf0ssDN0
gxMps72NxD5a8+I+DCXhz5BCu/UXXvrPLd9rVfWqLQNt/ScG7RnfPQS8jprlY5923IwJV7/FRxZT
L415yiFpQ6wQO/OFfbZ50iBrB0DrQICU46vg8K5hHBwfzxYeLm+gmaB+fyz0ctlvbqfvAVbiKVGG
7oB31iYfbX5DQW94LAM6oXolDpVcd1xBRyDsEaOFadUgKPQSRNfi78b0xeKSFQKvtoD0fy5nsMPZ
Ycot7uDH0/wjfJGg1QKcf5CnewtUDArl8KWXUgHjfdkoPLIAvNXqYc81zqP+jsnqlEjdluSA432M
SCE599B+PtMI26bC1Kj7E9UxDQeV96ka78Pz/ujLKRUBe6tMpm05p+SD90TlNdGg4Fc/qzXcND0e
+bj60xuscQ807008CQwlk+Nzvg44Vv5wdDA6qLTeYpsIIL6aowDL1ZBwWg9i6UpyS9TLNi36XMpd
oZRBh1JrbrkvHYddIXP0nxQENiNH1UUrlWcFwO0zjlXl2bSCNpEJccnKQ5Fmqjt0xMmbIc6zBp26
O6mVlvqkKsaT4ikUAfdRhUuCH54zZfLW3aj+jMAgD7SPzdR5MRnqNvUz0qCO7sOq6+ZULbh8UsCT
JjcvDjgY51n40IpujY8dn3tGCnzF0d+c2BUYjsmSDBSgcSMTPGY6LM9nHKaGYeKRqsNd1swLQ5Zk
FA3Sm1W3eJZ+UZaptGpH9YRU3SXjFyfes88Iak0sE3a5YGTPFfUJJMtuZsxcgSKdn6qbamDRlrxB
Bqd/yWw7Wh93a6g7OIhTeftmqaH2UC5EP2MN1xWrlWyNyLwrY3lLAMSnVFCsl+UNduR1o6ZTBDlO
KlTsVBqoMZ7B0j9aOEygACtf6SrEVX+PiiVdvs9Tnq2MLgbRGmqUEnL2ZyQBAxO2iNZa0e5hzXJe
s/aMIqAaQarEEEzPfyktCy5+YiRI9OIsTIlKhIgCurrhZ6xIfUkNg1p7pz25iG+Qg0CwZSzVuYC8
IBYWecZxJBTTY2znzxFmdZa/qg1nGGrbHfMd9rqgABhcaK9SMGuS/q106/0Jr7eGeSMDaEIoUkAc
2nzKH7Rc1yf+4yUhrYQx3rZG7u4QGg7pMtx+VEcxLtW905vi/J5Cp6oZqcyrCcL0OvBXxzgyBrAu
upmE3QmdGbL+QO6UqrJmPLoin0jYKYD7FN3fTPgxPDJWbSPO+/VnqaffgG5Y8nxne2d6YUedn5D8
aSYSF8wqzMFjkVeZz9eOGGPJM8jIAzuz1KqrTBks+agkTsNnFXEcL7cCqApBqpqf9yOA2y7uPfmH
z32gLK29dSsH9vG5hCFxLRaX+42d+p7wVGFKC3H+zlrlKCGFjBsaXjgvzkcrgX1tj4At9sx7+n0B
lD6S7esxzdIfNA4yk1M/QUJ2QtagNYneQAXP9phECNw5hb4xxV2aeoNls61u0ZU6Q3XFMDwRH6Ju
2eEUwC0+8sBkiqfg+WG+Ko295TBEwCLwuDLYQQEtuzKtm/8gr4aYmNdK0dS/QZeSIqvsLehagV4c
4RCmPHej0g80I8WUEKbN6EKdkXBbG21iTkCH82wNsXZH5HB4G5un460FKGwv42PtmebyP8nQh1A0
h8SFBXZWBm9BkfGU5Q1VCAo9r2o3rkBQ4ABpA/z2Rb6MkZL5764eCXuqvAwXgW+FLog8c6Jm2xWu
CPY5JBf7LcVY3KrhHhwMXpLFsBKHM/gb7BYp8UXb4FSTa+4XLAUNBEsSIjG9PVyIyKqLtxzfzk+B
vC/NtrdKI8HHfyI2AYfzGPVAAUEHgwA9Wt9rVOAsyspNojiqUVjBjYD0lVnZE862tgIqKfaV4H/s
1QEkShsCdLE7PtsAhjITeAqZMvmHXtlU/9OZgLZLarJD58DSVqQ6QvrP04CFKMKD7S3A2jRDEIH8
wTNC8ct+Ie8JjKt4/fTXKDHeDGjX8VweyL3uEummIg5bSGWfptF4G4mT6I0I+1Oc6LpjNX4jma5Q
DqxGmh777wOtVem1wn93CojhlhIxAg6rHunGewNZjPPQFQChv2BhwpJnD6WAbDtXhWpSaPAWZoDa
7ikFgXpzaODVJoruiOjmmfAfD7SZnw9verjsYYNiw1GRD7ua6n38PGwm2XviOEqxj+WsSmFq2a1+
68UBZjnrzIWrjWRMItTo48K/kRSeTWJgcp+a5+RDhdPsE+PsSFuOnkGcY8oSPVoK7gtG8tigv2yQ
1oKzAGy96JD+yTQwccy589KowUmZ6y/9xsW3gQgcugeMuVAafdlznOTItC6n0Dh5B9pww72FeRRM
qn+q/nlHLyMvs5zAZAOkn8h3MPyKYIUDFKQvDxaAG9U2Jg6zzNvvUjVWiB/PIDnO8QhH37A4xuPl
Tl0c0SJsuvvYrxrVtjF7k4bVjvTMWEjf0vn7ewRDpSl2XW+nUrL6kkZ/fBDesbwIDdVgtbBq7i3x
puaL1hDIFD8BA4eIxZ9Cf5gYXWeq0ekLULxTSNOnIG0ipZJGDmz1rsZURQ22T4eRWIUf9gFqVhkU
w1cR8dLUl6wyVIUez184ojbC6W5gJ0yK+L9H6Xj0uQ0+4eqjOcOt7IU+zBCQhuo2bU7wUYRiPjPW
Ms6RkcV/9eWomHcHYmCWufdoJAmNWREHj2Yx5wXMNXFrwCJy4b1s5PRbo6AmUXV4EtRra1nghjz9
y3XBl3i5iBL46QniVk9v8bBonhAGM5Cmw5xnrnVcVfsuUA7YVQorD6YRW5MXdA+wLo65MZ23ZMSh
K39D4FrNAlUv3B6urt6jTfIuVmk3swCV681bR6Lo1pMa5Yv8t4n6FrJxTsmS1gnQVASR2W3dIYbk
7QmzYbOSqKWYJ93NzkphVlKdSv124msdORsagBR4/gXitnzsoe4dtD9M9aeVDWrTc1nEoYE9JP8P
2HAaFWUKuCLfTovWe/dIgEzN8gyNXTSVnEQjKTMg3rPBuL/aUJhY68id29cOtUBqDxywBeuyMS3U
v5SqGgeQW7HfYlFFLZku6T7LGXwPnUlY+gN5cxnCQtVBcD3aAvB+nUkJZM311vLc/ZHvEghaHWWi
E96MQh+QB08RXt05gFgLjAFwdvvYytqNfzSce4MsgQsdRS+lEJvVVny/Fl8cSkFCsySXIlojlruN
7C2ZFXpdKoX+qzLSv95ZTzkUv6v6GDef6i+TkCQYXBd1PUwc1F/dEqcs3hIrJXZo+Vd9SpBBdgeV
naUkHfvzgWXsljhUL7b+LseUHlzBHEvAWhmm/me7oHlt/KPbl3iYoLm9AO4m77z6xFASLSlt7Z6Q
bqkaXnL5WEWBPLO+APBjnc78v2YiTuejI0rJPXB1E1ZgT6wilyIOIGpFCPRxG9wzBqQPdJICxEM6
IcY3RAqljNCAG4OMWr1Qe9OFD6B8ZVQwpnIxF/iHdlruV8iM6Au6P7x4+cK7as3E+jpmQfUS7YJX
DiVZb9Shu5yY8irqHPR7WO1lHp2FLnVcqJLSGW+L2ruwr5AhafSpkpYhmJLy0UTIwC0tjDXZL6dG
zfufawFPZy3US6Vy7wabkGczT+Vv3GhKuNohR2ojUApxwAhTyb6bX8mSNJ3W0LDlp+XnP94yVCKa
lrHRIkseVxE9Ho0GLRsy7OFqsy2Q/LDW8meU5W7r3xQJw8TL7BdU1k23gusUSbZoy66aI7ZKr6Q1
QbxysAwDqp9FLgKY2Vr/L9utA5RmCiwKJ2f+uveV13/yUJ0T+6LujZX/M9RLl+xj1ntU/o22o0YV
nCPTxsHT7LnyWl1QfHi2xO+CqR5+47C8e9vYg6vHJCk9UMjapJZbKnLC4x4ZzWTI99YamCx3faX5
RVntnn5sQZ9EE1CSC+HAUiSy6EWje1Jj6wnsbrrUjTjFwf+52nAwJRfUXPFQc0v18rRsE1MGAosL
gI2zVmk7Q2JhHb87BeaOjPdQ8+y9mB+baJWnDqjiDRz2o/+YbxzQoN6gXCquwDDcmAlhJkjeR9hG
8oHboRYg/ESZhw1rofq5vNwyIZt0NY1pH7lzWxF39jxUWHVvft92+ENmcUsnHf2Yzo0bZ9uTF5x8
bpGzu2RRYDzvA0PU62nRnl3nESvn7xFs7ITauOnBFYTwCQO7Hgc3V4d4yjsHoP+00LKd/P75/BMe
C9gDSfmi613aEzs2NqGRfMTxy3MpHN/K3hlHhnjduTQaTFcLwgIMVEyk3PWcDNCrFL6ZG/LYEdpb
3CsdX5JxXYKIbTfdtFh2AVQdAqNG/4fYZRhDedJbZg37Dav3HGO2XbzVptO/lltbwx9YZPsBiDkg
F4LX/Pw2dCs35nLuOWEPh+YUwLM0Z4Di+d9+2FQqeBj9IMCMyFFQUMGSRQxviPKdhW8GbP+N1k4A
/D1NShTGNkp+awpS0hNHyC3d6WY47PNKR2sfO9Gg5bUlZ/rQIFdaio+uYH0Ib6eVaS14DithYi+c
55I80xM68YOp+LHsdGf1tcaUQ1KNTmqY5x8h7ZMsf+IXgp59LgK0XL+ASNYqk6M/mlrcvUwPKXWs
kOSyYm31pl2d9X7pzbC0tSWE8c5Jm/CObGjaeYdQjtVqLUeDEVA//yJqcJLFjxY9gs98csspV8l4
gKAopNDo0+ItjwglMWTh8uYFAC23+ncf7q6I1p2nG/TAtzdGhsC3rXB8SAAPlG3QEBDftaKL/Ovx
Xu7GHixZ3jb8X8Ha2gBcXokrWneHmWBvF2CCC7m8YBAL5ybuO/0EkvymJJn/gFaU/DfkG4XLkhvT
J4SPgwGItj9XZBFALEI03ibG2w2E69QCWRC+8QzUB6d1sXbV9zyei9l5j6W/dlwfufvQkF7mcfdK
zBczEb9NRlZm3MwYZ4jAFWh09pG0M3R1xsMLesSqcQTr46VCYS+sjbOpApHciAtn1WAQVA0wkIg9
mowKw/BehU1RGGRwBaV+WZI7T8GArKCRNKJyikWTUum1ky1kYxT913OUL+F/Bi5+FQtDiCQ0g6dU
lybNNA0gaqHzzeW5tbVi9QFEuPxGQ5eU4CwziFn0+CorJmmpwMtRCDWFKRbE7a5r0EJR8RPpwREa
IhhDKsw++wOBRLC2OWyVDJLmiWTVSdKBB8JgUUj92W8ysUsebwWL3IJLAt/ggh0Zc9sidHYwCtd7
siPAXlHq1S0efqVU/HQZGZ9AfCSbkZX31wgnbkkQgmE6ylgfbK28FuV/ORuzeBCmJWWlXaJLcA24
/r7UI+amvmKw5AOMq9kW++VJb34U3qQkaPJDILpiCe47zRsSeHSjvrx0kivy7/5AR6UeGsy1ZumN
GOu/sZXEMaYKMeVg7tGTscW0Ys4CchvNwHncL+u7O1L57YKVuBbU0niqeHHGO4AbFCtrB1+kv6Wm
dYciiOpVG2AjQzE6ddW1ggPUDaqb7M2TfFIK0tlXlCiBDGr4bjkdUv844Z8OumUN0IbQMdbFWn7l
/vKxpaRpuo9z/Te2UfbbjGd8Hew4gyRfswlqW4JPfaHHdXDd/Wq+SJs6YbdooKOVFCbICRl17i7e
s9ZFlyHnNqbK2mCVuMFubVu/eVxDqDOQpnHfsrecGYS9Wgk7S6z9ofGWN94Uil2IxsyBKW2vQKoe
uodV3lhJbWG8yjMBqGZu4q5wTxuZ3AF33IRZzpUAlU2B1ypQw1uv1CovWo57w415M/NXA9y+l6Q9
aXMzTBa6hBw39eaJEnBh7nNa4aClmTI8XvcEps0PuYCsC9WGz/b9Ofyy49StKqK2+Y0tjNR9flKB
xFxh+ZeLu1HGg1N7f7rbaUxD4Mo/6D9v8UA//4bx8YSg/DzAJ5SlXmV8UHQjx/nMoMn8ATg2GkpJ
4H1JiEwUbogtEVCijTBO8G9J7f4lSWvtecjZzU447TCIULS8+WxbD7chgV2ILAI80FC6IiHsrgaE
2zZNAhaPlG2K5+KVq92yTmAdndaU5XinGt7lw5lA0TDauCcSfk5rmytB5T80yLuTXLG8oN7zEmQz
NGx1K/QOn9HsddjSrkM43Y8XsazObZMyfAOzSXEg//zIWFLdH2s3VjGmWkRiGCdObveubPsoFZXS
+zbxTtWBNttcowT7/5YyHp5ndIu6dPiWLWS0KX9NsooemLLqcwsmlgWhEk47PznA42zp06oBfDMg
l1JUPIwQT2sDuD4VR6c+nSdhmDcFL1vjhbSMsM2JVrxNDFVFO4TRtQ3hfR1+DGTpIeVyQfG4XFOd
XdFSPcIFtyW7TJlOxrJyMjeqQ/qDbhKaQHBWC1Va2CW5eRfF+GhcKq4HGdc62NqtzUL48l4ctDKS
/9oESDWb16DiHvZP9//JsnQnqiK7FsgabMOIhNjVmgugnsl6nzA3Gt6utn0t2BSaMFzih3izfPvh
IgjSXDXogTZLG9NMXkN8sFAhv6EtrTKxQ6Iulh2eeKXkxXIfvCrfEUtNOjcQ0kOLcAp3YGgMMb9f
69tEOTDlAK3KYlg+IBf6LczKAQWqORKmL79UHtVuBzz1k9xeR4Pfptzcl4O3fiWRtNLXTLQgRUvq
oLkFIA8phuSc8zV+/lnugbIlZ2bYIp841jpt7tJ1eTQPlCjS+xw1/jDHDGieNylg2tO8mUNpOD44
B0lBzCcxsibDYp84hL7fVm1bTb0McI4jrAseeDUQJbNiBRuXLZ/6IrioLf7e9mk61ZpMrObIipXh
Z8jHWfvfOQgnhQoWpABkblqXkAHBq7PBi2KB3n8El8WKaUFFCfdGlfRYNsTozKoNFNFuK7RHX6wL
1ae53+m0mRPIBdBcXwS4c7gZ/X8r712H2HvcqiXgJInjN8ipt6HMJKJ3RGUSAoi57Kvf9GhsL0U1
+vt8Xjt9+TS4W1MkD3xgn8sYcgVlOJ/x7svEYF7v9mgR5mTapNHjFGnnyOiyQfBQxrtY4sOdBOvk
yDn8QKPeT0pG4ARtZ9Hik6fi/IpBpr3b6dvJxaGEvIn++QSbomEEHwb/k724LWip0E/cMvPN1Wmz
mCsdG7MFqh4AcOuR/z5dZa3b5sVVSG22datg3e7sk5wgi0NKY2+85XCRvdlhTW3jSzHNeNFP5oP0
Q7gEZ9+Vsr7yASbSWGt7vhHGxPz2TXpNtYBBRLyPeIxbQWr83NQiQj0mrXHg49Wm4uPGqiFESCmX
/NB02lkTj6NFRT1mL5tNvfhgtqU6heogC770rpBUKdzmFSGtfCO/XnBVKfY3MLDJfaK1ha63Z/Ug
kM6dG9QMER3Zc3W7/bijH5TPaYZhzHrrC8v+xJZYbscsDSqc5DMGhF0i51LaCxMiv7WP4IB7GjrP
BNfxfNavLW1C1nyezlNvndI/nMXoh1+HxqGwYQCBEvJ2BDN51AMLfF3H9BQdg2sMn/z4/jaZ/rfM
6T/kwaZm7ixqTlwHT/GDLCMGbUK6pYNvZMxCKKreZ01vBuZR77XfiM3pKCiVmFZ52OmVT35I/rNl
TACl3Pp7nXgLQ5kAVItJGyNeXGY1r+I8v1JO4WC8aCEZALucnqxCp2PLO58m/MnqoqXagebY1fSO
dhJosdrnUWs2MOvRF/LncTsKGOTFBbjVCLH6TgJ6qd2QK+HIWWgQjPBHt1ECscid4gFW0CRBF1FP
mQ/KOndctbH4yIhQofJGH56rFf7msvCg0aTqvaX5vsDoAfOZxdTLbXDRQTh1cIW2DVAWgcZA/aQe
3eIsOtoA23c2Nbo2ZocC2jtMn1Tferiops/QdZQmAo1Wx2eV1FtYmb1ED3Wya72e/JVK0xWF2OEX
EmA1ieSLnercpyfcmJOcxqNtPyxcIhutUJFs7ngIhgkjAW/VwPTmM0DxyPQ+kRX+rKxo1GZx/eXR
fEBvTyobP2h3rh7/w0ftsnzwOX4/fvK9rppWhTdlPgo66ZtIQKAt4e1DxgkBRkdkfqe7XEtDF/Wj
H26KF5maWVKZ/2Nctx9eGBArLfT1xmGeh/kDLJzxSJsFnOA2uAuEA+Q1rqxR7Axc5NV0nlqkJjNE
djZQWZlC0bw30VJVglSzZ1YRnmZAZH7ddInNDxWTY11Mve5G2AC1vrmo0uJ3IjOQ82x5wSmzczHj
B7/ePT6M8sthFaycDymPPgDur36K18nHTpLVzgSYFyNxJ3c95EtCK/LVwliw/rA2xFZFBUe0afVH
9KVEBFwt0iW5odwLvhAaBvSHY1oFtrB9PeYy1CUpDA7dplnbqJ236282sLcMEEUtV6mrZwJzreMy
jgpyjCjHTyavFsXPAagiqH9tYfNV26FL5lvxQlOn1O0p+9z3GJ8Xi2xSsweUhQGYd1oGNCj02n11
FUuGEV2FBMFHZmrCanpjBhKGIW6vgzYza7mXqAkQ76YIGSqZvItv6gVT+ik170H0e66AhIoYcEfr
QVnf6fb/wqjmbNzjLftQ/sisHcM3vYTUgkLDe74lV1H+qOPaR+TI66AYlFtCfP0voiIk++cBwsSx
BhzWjiPJ4A64jyhzOoYUo+VG5t6VcuLF1nhosYe5F7c79JYVEPFXH61WFRU5DG7nYBh4uHUNHFxH
JGaoQKD2hHfE9G8RKh6oWrvPqAeiq9wueGiy7NTjJklm5jRIgK84YOdhvOErLTSxYQd8bJvn2YaQ
RSsnK6kV4n7524+GoxDVkLFKGrl1YRmA7CneBF+pV1ful5rBsGATO6ylxJFlrz8cc5gahEIqXUHp
hhdHfOkCMwDZdux6IhyEq4YQfSOeMyjNblvBsb8OcTohyULyyNyLc0MRfwiw71wGs+Bk94ca7MIz
bURKesek9JllOy8FOvk7iglME7sCOtKxhW2otUolRN7kUhVV1cvK5HHPq9XFaJhGRUqwu/uEqusT
21QIgSufd/PJ3NlyhQL908ys3XmqS5bbuKUmK3QkMcbjyzyzr0CWV1NQR1eu/6LkbZpJvMnOOgkR
oU0XeyJ4FwcqsWzUmnnAsa/OkY3eQaFvBfM7YSDpzBY6ydS08Lbryyt9ekt6jrGqBWjkSQbkEb5H
GWD9iHY5NIWSRBWh5zbOEQd1rFUlxUsxWXAe5q1jEODhN4qPpsAWeULMpMhfAGZguR8sVk45h4ku
x8ofJgFdRvugIQbuAH6ERV9aL5WgkB6rB54hW22R+3nnNLD/wf6GgEPX4a+yXpcvH40LQN6OKxb5
Ud+Pcx0S6HuEky5SlCjYiY/mp+/CwcD03kt5y5xNsZ53P458oS2ccP5k+Y7pibriSE2ZHfb9hA0t
uvwtVmM3JBEcRx6Q2T9nsNHeqRno/4yGf6dO9ab1vLIc8TxOVImfRe/hdfPTiCxHYgmiAMDC0+Z3
UE54g6h9bMnrNhEvhzwWONyaRLV7btU3cDjt5XMjmzI7Im9lnDBaYSqUgE8/9ksvpkRs/huH5ssq
+VsS7oYwT5nQ/8t3hd9xQDc3c2KdGkG0zPkirnp4Ezt3Fz42KI9IiGwVS7kGS11facBQuwQBZElW
14DSYC7w+sy/MxIAb9vgjEeaJ5n1uiS4K6Pav/1PGFh9PJCIMS3dYSXrb4rqeTS0aWm1A0kGH+Rw
1h6u8IL7xmqdiUjIZy/tHtmnzT4wgIb9Q99GN0NfiqYv6vDdeKpbxVTnUQf2k2tGwNSxefFyEwhv
Y4XE0+tsaGX/wyNXCIgU8HPSflAYpN21Pje/YDvcpI4vseWkpwdnEBwXygcyKin4JNaQ6RHff7DU
Vm382vHJU7bTvtteKiI5xI9IenPPnorkrBrSPmLP/Ub33XmRvBqgwJjT28aoqGedoyWQ
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
