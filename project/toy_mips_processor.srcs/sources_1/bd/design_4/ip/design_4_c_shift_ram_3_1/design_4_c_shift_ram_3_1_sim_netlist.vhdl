-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_3_1 -prefix
--               design_4_c_shift_ram_3_1_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
icKMGC6bQsomzR9I8VazyNM2ufxDlgW9f9s7EIUXrbJ1oMFub916AHlnEhojuKnRr9stPLKl5j6C
Eq59oCa4LpiuU4AY8mbLGnn1lGMPZXfB6ua8Zi+8dp/T7yTmmQWaqjBXwIRlASmh36xI50lJwurt
1O4boSbns99XY8N4wjVhhYxQJ4t9Zd/efjUoOKAlUA+vyC1Z9vl7suCmteNJy0PneECh3XjLaFEe
3KxkjImstrbO4n8fiXpZgFUoJgkTN9feegAZoCApvZGthm7s1wgK4FDCChjbSKDdacihiHEZbkp4
ZHE/nd7a07Qh7HC4MHSyfkjWRCFedJ0uda+Zy4QCLYED5V7X4FmV+N5Ad7GJZl2GZ3rlcwL+X1u7
ySN05AKTztfDndeVrCdz2kfIYErSJ+in7Xv0Kv15mUOgH7WJ7jssQR43QjGVaJbK4anva8pOiH5O
LUCykA0sMg/SvtRY1s7SWvQ5m1/dp2zBDeYx6G4mkcQgFkEg0XGJqUobk7mp4QQUEO80tW/K+pTp
mFfSwfi28EYmag73RYzojNfo+UfExLCn1u4gUZicbj8NLu5o+7B5Y/pZiWm3wX12a5jTEKJqsra4
afP2yOToO8lE0zHZRdCcDzmMnEwHki8LCXFfaUX9XNHRVhAKhQCFIzuV/OKSg0oXtZSE5r1HE/mU
6DTsKe6vdTSruezpJt4chZ6AE5ARs5MQ+Q1DlB0xruy561YX3t7rNcjgzglSAwGTDNNJtrrk+BrT
Scj8EHEUEXkkSflKxiRk2XuvbD8r1nwctrZI8SJ+hRZO7vGmkE5fuBePzYCJkm90l23zmEhQeTuQ
uMl5BWAmdSxQLfvhxWoEPqWWYjKPNFNUip8b63RUINv11HNQeM3xQkoghJh3NaSa+iQoBaCTgSI6
KRgWG+vtyqumLrEZ6rFriwSFMfe+brBHULPei8xTbhwSHItFV+0stIeNnpuH16bosHzufM5mpIBr
YrprC0bqxqMwore2e3dEL7NUeC6OWHHKJHbH18j69p73M1sK2qqPBgYkoyFxaI3Wvxz6f+xXPLyb
yKfzA2iD0zkYF8cINzAfgbxFVUM1c+l1VfBU8If4O/X8Q0bSekELgWq4JBDPgIYDuz/KgiNjEJC1
9kdlFtFJrgsOevJ2Vif1qgMHvpfBvd2n+RT0Z6KqTtilBAyiVYeKknhHFUqe5E39CYX12JY+bDHn
Kp+oE367PrqoRUrI9deF/hfdpn6PoCF5SQ2sp4LVKIlwsXYU3co6KEHXuLNCcr9SprF8d1BEzfIW
5GY/oZd7sgVht7RYoZ+LgIL/MsLIAGZcraW6tZXvxfnm+G3ioQ8LO7726jCVV1pbpZcCHXbd5J8A
HNHzsajWwxgG11KwbAOVYFC2DFanAdMqcmdXv8IcR7iTNqXNQnMluIUPQsbLQL7kuiVrFZL+tRnm
6TjS++CvN8Ec6UpPAyjPgU/Nvu228/egKQqZ0A0HwcC1d2ozNOohBjBQM4esD6zMLuG1Hwuat3j1
eoTeSoZwAgvOKpWjz6Bv3VXgnzbsbAuD7wBCwVjKwUcl3PwZdjLfqZI2tKr6nnIdvHluusF0FhI4
8OtW1eg9mmJCGwXFQ4lQOXLWGMAtD2Q5yCgzYf/hEa8yfVN9N9fvjIAIMx55j3F/TSbISQiIXuOp
cAwr1G/p2j0ciwdRQdFem6VZJoPkFB8Y1OzV83N3fSCmxeYGY6+bD/6q2fac6rljsSwjGTQ6Atax
nGDRf7lHi9epQqt/Zao1zuuE+8hKiadhDxeV0o/GsyvU+QPp5KWpGIe/Lhr2oO9dRWAcbOky4ENy
NLzdndHWRzkf2WIjYpiBPK1IsE9y2SKTeXodBPiCkHp0WLoV9p+9nZ19lWfcvDHLS1Q0ZpbRnigc
kyvn/HxLtHPJFt/7DUJTykykQMJ9pkCEJpSQETl/8+RlX8lCcKgWnsdUyntFxXC/tcabtlekevWs
5wr1+aOruiUlouCoTmURFEFsBiK0TPcFCOmuqV2QUo4MJrzaQfzQ3QZc+XEdX3UWJGSLuZUft/zF
eKTtMM++qszgxdAUpCFgGq4nCMfIBcwZMJH9jt3bzCe0sJX0VAw0nokMUfPkgqI7/FY6txHRU+wf
Xobjyh5TCO36IKQmXQgrDvPwkYjUe/ghr2aX7dAsLMjZX8vanZVDdcFWPRBkuyYh8WNBf/wxBwmk
XbX+gBm/ccUf61y6XkvWcSbNGP5zz852AC7vX5c5+aCzmAokRINZj6azyKoHGX1j8d7B/4YcRTTs
Fbgfu7ImaWS0MIlWYOzliP7q6DoOzlS/CekzKnS/EH0lDOgUk0NWxTcl52KFoly7LhBAYrFVbVWH
kMgA1d20PuCAiWtukdt4KIZfyuRAVqf16XpkcBIwv1g85wwdl2ZgkvOvyxzCQ4EOXP2JUO4+jZjQ
8L6yjuACL0JJ1VQ0osKKvi7M0ZwtakrvHnD1obOp0MrmOrmtXGW/PR3eeuSzRRoh6Hl1lg7mQ1Xz
NcARvHCBA7JRASK+Po/otGndRIlF9TGzgZazgj4evbvBEqBtde/bLq3gid7gftYFYo7j+QH8A99n
zeeVlp4LBNc8tChRatpUa9oF/yGNixTo8NgAodVAcV7qtqdU0DaNL13CHb8U/h4Vnr13+s51kBt0
2nPH9UNnALxxOSWx58//Ykg2ZbdSA4CXLtIRB54Skl+eZUKODavKZivmf7vtYMajOI/d6BxQ3ATL
+TaMY0b2mEG1pRg4Rr50t6ujdDTlqjD1hSUmRujk3qMaZHEa3xtJy7p3AQbpBtB7wM1fzCOEcVtq
PJ07vBMGtlaSH9/9Nvw5ejNcIwLP5nczMQxX5pLHHqX7mIIfZu4Pr4X3GcWDLDMPbaVfH4iP7sxE
OoVB+dKAN5MrAfHCKYW47EaHgg5ZfiTHz+6SfcyGpdyIABmplMBAudEJz6rALXYzGW8YeiAC4nD9
Y9qOTrVv8k1QdH7dGU81roBF5A0ZLM0npBWsXYXyjQu6dVBnOrUsO89gg68tl+QYxr2QyA2tKjQU
If4tEUNxaMAGF21BKeQt4QXeoz6LhP4GIl0uwlI82TRlIn2w8DyplOqa3Jezb15iVxT7ift3Lati
L9VMOByyWz79MxQhm1wwluJktgSjrLK3WeK0ZvAVWcTKPU6WFctGNLdU79Fjyz/AdmuUkDnTbvNw
wCjpagDEQKB1RQM7P6TRnykLuti+ScMXODU92UGdrtgdM+ukVLxUWdd4XyDHO9pBWFaQA+M2vFXk
G8kPFThbeaHMuM8dEJ8J2Q9LXA6K1n583IJXS4+68zdXl+zrD8vGtyr9LmzM5q2RVj6oSthC6HY5
5AlzhFr60GK7ay6K09DLQY+hzcjYwQ6Hfyeb7jb2khVn+KwhKkJhvyCSrhHsHaXxWTN3SMAKw7yW
9Powli0Q0HH3Cng+pgTs1NfRhhUo3IWjJhuqvRmotTRVt/INkfvS1vvWFeZbpMK6yTDOW6PlCGca
qNA+t80yPl/iC7qZrkD9EGMQHv0mGsW8d+juuPpp0614mUxDZoWOQti4S+pyP5wuXuUpo0USpVJh
C1eLHCCj+q1mumrLlWo9IQIcPQGvqkJ4yFGkvF6mjS3vdcqv5DnI4Uogrbqf0UIJWK7Yv5tCjdpq
K/F1UTiTTLElUKj3fBjDJUbmR7wiyKr3a/sMMdvhScgtD9JaUXNJMq3p1T0Bks7wtvdBFQnk9M8D
WO6LHLf2NEwVa402Jwdn1B+guizLezvTZQWHHDFsUOu/3x/HTrL/qxPvVVg3zKeHaOoQCNjk4iK0
9bTZJfoVIDCRTmlbKeI/88DbWlluiqKB8wmbrBFwNq7BnvOrGl3VuKx2pkpklGj3o+x4RAWto/Ub
euQy4pDmRnA0imBxCeEreFTbQ962kIiyooGLW+Ua4iahg3f1cWav3MQKI08GxDjuBEf9Y0dWyllF
TRmYM2w3hV77tV4+gjECkgzCKvv4kcLhkigU1hgzyFRe1GBQVJt5y9sdfiuHV+4MhZvKk4pjrJMo
JCAvbg5vxuuq7n04LjFyPmL7rAdDcE/u9pcD26ctKtwbVG+FW+vobaA8dz8yU6tWRrmLafU/SJBw
wHtnWdx0BZw5BYrm29LyFH6o5nDToUNXsHHBOO7EGPFxBn41Cq+4rIIAsgmNlXZn3oCYJhYjVJrS
S8q/HeTg3wqvYZNzZCMRF1/aWMiZzDq04SCXwIe+n9onUO98N8vFjgjdwI3YKXg/qRZPmCW64hU3
qOO7uZDGjUmmHxb6xPxxEGUthziQX7XALpyUakH7guPVBnQ882xyMz7tSZUJHhYZdTzCwVJrIO8Z
jexz+HdmRFRBr9egiFusjn8vau8SnD+/jOifeBIuXgk6Mt6q2K4jf9su2hAk7cjscpns7kJW5k4C
sIlrrzOH7mi2Ch7cMOom1M3OSxLAqo0FwPwA+blEJzEsAvjxPR8QnHvSU3RXOJh/Ktw1zCt79Syf
HdQtx6lD4qGJYybAfvFmmTnW0gIxoVdYszY4EVws8ngTD6LqiZr4IPdhy8vaJ/LUdCEUg5p0dfpH
enJ7e37zm71RtekH0E0nJQRg/ke9bBppNtAyGovh9toFv1FCGylTEN0CckioIJ02rCkEAImdxneW
Gh2nvR/9KdNc0ji+HAhb7/1rim5uoUM/ORJwNrjrz/SkRRMT/4STE1KRN+5p4RidQ9L4Nblqc91P
eAs3ltvdMVdnTPeOZqciyOfUHO3lyizksWZURAPgMOn5RzBAKa4657ibLXYwn8DJzxsVoPO/WrR/
NwpNLBhCf8ASxcT+NCDLROnG/TylBh2PFWl5csijuLBVD7g7V6CMs0IzylM9++BY5NGRJ/dmd+W4
PGDwr5JMKskfHBXbtuO0GzVmm6iC/dISthNurmGqa+2Rn3/UtvmyZFHCanAGXR05xOJHXlMNlMRj
n1wwwtrv6Pc1+ZCGlFEJBK/+nat911P2nxAsETJZwxPMdR6fv6QOuOS21XUY+Fk0AVZ70ho2u3yE
FHFx1NcDUCJgTM+yuS8D6imUJ8d3bjJQduHXd8Mva89BN8jaUM9dyLHUTI/4mppcSW149GaH1HV0
OP3hqG/ul/4oeigo8zXx91KQvju2ok4rzuTzw71KjEiyLu6Ksp+jABpTrky3Cc08fP3s6lmte523
y/D0VPtKMNlTiPl79XmUqDCoWQ616yUxQiAoGxEeE54ItOhzUfMyQFRGRQWFqhGLMhQUjGALzKUF
DV21v6ZoELKPxc2o+FDPVqVQAhQTq4WoFrT8gDRmOfvKe1b9yxl+b0P5Y8DqG5KppzbX3XdKaeJ+
Vg4XVqfc+mhVJtW5ro+iGzcCOw8jgLu7MKIx3WNHsZUPCHYMrNnXXRb40yOIOwRVsRKosprhkRVx
MGs4LY70eEMa2SWpjcKCd3rRFTk6pv0ve52XfPevlUKyqGfMw16OYtsyxdMFNUfdWdh07QbtzaOS
AUEh6jY9FJCd0TbhZE+vqOwzOkLwHrJaHE0QCEEpC/Z6y0xVytWKMMhP+JxfWXgbiok0TVyeukAd
M2pKL0l8HvsrN2hyarCh+8tNkFfFGxQ7LSTQr947Wy35eH2fvL1w091IYvk0iopqCDWkzKGE/WWC
4hfn5Nd60gyE8JHT1GQU5e1x4i5HxETMKyA85IRgniYHqa+T+ZE/AHE8mbtOpKGUWmqY2gF2Beg6
6uinQhMJHO6/3gp2bL77tdrSfQFnLqz4ncZQcZPq09EAPz35xs+LRZaXL3/zwKVm1THzD2fBf3dJ
1VPGMhoZwO+SCvszBaicB8/B3HFzpYnAd1xm5kmR5mOsiWlv6opUgMddl8qbOPnKnf3AlQSlVqxS
bjUckUXoQLYY+tA1i59iDpaTYzi3Mu5X7LOwmBQQWmipFgX5+OapW5vI7HW/LFEqCba7hpPoUEei
XKDXKCODNisieBrFR+hXrM09RaY/vDGsfnhoqWgEUzftBzH3zwKkt4YzjXer7ZbGImMS5ZWx009l
vR5QZ61OpqQC+s6KG0gHeKL7WQJwDymtVOBeFPpsIeFOXbgPaM3LiqT0vR32e9ru6GWHPowF+mAB
zA4CFmYEpNs95NNLFs0tCG2E5M/neLuqt9PuI6tO4vZJfpzOm2HNZcl/S8V5ACIH+bD+bV4hczph
Exsn7lgLJ6YoOX6FVz8YiPoJRhXV/vm+qEmudfGe4zZsg22JLUjTQK1uvfaTc49hSHvNLCb4tzY3
Ey2QeJ7NGAMXNH/F7vV1Qno3RyR5latxa6NqAXvpYrdsWqo2QMRjnosU2/J+TjfZcch2DgQduvBR
aW85Av5Z2Z1awWW5epjKjd5yMPB1GGabxEmhL0XUGhsJd6lgUlWdQ5Q0vRAZLaIpkaHi4e0vfelw
OQHDuzrugNo0oX2qyEAYDDn9VWR40CVyco0cAk67L9hmpuN40HpqJNU7ZZ0WlT8Nd9zseUG/+37f
XfxTFQ7uGbzC/Oshr47S098eTdnDtxiaLCnLlISk7V8eyH0Hceu7x7jdgBbCDVltDmMcID+Ai3Kd
X7pa4QD3wTqyUvu/ZgWIDrHL/Uj9h3RjdU517U7J0Ykz+3jpD7zhaD5CJW64hU9UWi9U07oE0o00
+UFUFKVS+TJ+vm4MarYtun+ISSoa6F2wrqUg92OGSCPxhyRkq/pxLeH4X5xTLbk1q4VREO3W5rdh
+K7YfhFjJSol+op7OFD8cyGhn4w8l3JCepI1gpwjNQG713N8Qj738nhhrhkg+jSDtNK2KjiMGM7f
vxuXQUxmvockO8UrmiuCivPXuWhBZ957mbWVIeobM00E2pHuXRM84gmfLw6gfW88oaPZp6K67y08
c3H1jYd7Ao1wTb835dtfXifxFP/Fg309xpwXN6yNBcuAfuaMWTzQOjMinUx3CqPvi1r62t3Zj5NV
FtFSGtviSqMQEv0T4DGADxLJoKv/pUcjUQMbNkIP+rnpEOW6w/x0RVX7YY59AiFl97JhExIVTdel
IMdv7cmRFhDqrw3mF3l/E91aVcRYi3pmSzIeKkvx3dpAk5HCO6+2OiNbdg4XxcOgZiCXaCgeeppz
A8EvUrCFApEOIhG1TXJlmbdE/KiSN8kMpNVg5KNWhKshR6LQcuLV0WzyalaT9qA6vvSsY25fQxSM
f30iGkT4om/FtnL6DKzTpvETcuWXl0WjTlprBB/HMFn5TEGayO3C7lhats3zORXsHV15msCzT03W
Baz6LiOvHxkWmYUiFnVyEgh7gm7WccLowqBZi2b/zJjdpxVaUeQnCj5wpIqsKus7v+FaOhhmdavE
numLiulcqb3ER+poh7AXihrtj8KzC4Yx5pyV+3JyrtPjdci+28eTJzWipGwEWSlhnzEpy+GcVrcd
vKU4k2PaIzLGL8aI+0CQ9HaLvwY9xF9WEEVsV4tS0JZM/nBo1l9QDUd9nYOnFuTufRrOARRAwTce
63tKbd0DAC+Ff5VPoeSFMoBLm43/+k0YtZIn99xCZN5jME4qaRUBMc9X/txuHQwtPhYcAyvx6+BV
qNW4vAtnaL4rilD33YNpW45Bm/dq/OGEQ50aOc47K7G2mmr2HOlTJh/ALkFNeTY3V6LQlXCU/M6x
imsUDLnmi2SbPIytzLtR/indUHy+Bh3KXwQQDJUH9g5yuuuaiVU4VC8ZZP/HmnuC71k8TgQ1IyuE
adNkrnu74BQrlC/YA1Usn6lxCRmTjKcpeSmFYXjFdwwjv9OlQ6hjVJX0hiNr7pgXbVLUc20/Gakp
2QIG6KIKYYcmAZmEE0zRs3PlTU1LhBbDu1I+mhUM6PHjW3lXSWj3s9Vfb48BfO7kW1DaREL6BfNf
rXxZmkRQPcE7QIRqR9YpzoT3X289hWgeg+W9W44AIlBST0FxuZP+VwF8ZP0s2CsjTflxLG7psbOG
qIE6eS0bTm3+NWaR6qAqVPirUPgeX2KpbMG7GypwHccCzNmt+KFtaqA/wG5SpQgg0Lw3w7jPoMxq
BrKUr7ixrQz4dURI9W7UdfdZAuo5FOQTJpyNg90THVxdpzksnPDHc8NAzxAf6CVn1PDdrU4pE8UZ
OHHSL74jK5qDYB1bw97V9dUqasqq59zgemvAomJEPMhNbwGJGr8q4KiUbcTdtlrkTz7PgSXcj9y3
wnNAuTOc2SqsJewenZYWD4WiYJVpElG7H0yHB8zh4eMLW/fSILp1pRRrhlik31UGNjrxGU3HM7kf
5dvks5qGq6F6XFlNhVTXCfuPOan276y9SwYFNI8LgP7iskr3108psNghpI4o3kSLglpU91Z/T/eT
+xSWDiboJ1o6HE4AKdjFykOp2Xcs2ISDbu0M8gZM4kYkqNQS6MkGX6nZzd7+5hVYI4ovsbD1NS6o
zG2IocV9uKIVe4EwtWQENsJh3fl/aRF78XhLnsTxoEN0fw15Sjbt6Ggjinnugaie4/TkvHZuOfgP
kWN49kMEr7g/yjI09BHuPab6ta6y5fUu/k0S+mnwo64X2WKA1OUESnitJWwBtsFajmR3rPnp2niu
iH47bt2DE5gBnmPRB4IITz7EU2aRJya1B7wpFZaXb9IiBsWL1TVySQ8EBhp4W9z5ZiV+ybCrxjyl
fgOc+pGQrbj2GnqlIWsiWTIH1sY1YsAdfvhca4uvhIHDrR2dc5fPbD6rMfMRpReMMOFhnDENqx0N
CyxPJW28uf7Z8lZFdF+yKUOkcyF9rwioagR2ShOkJEliYmrU5JD0xiLBfzVFK5nihrQWgssIsQ8u
shL6X5710UVL+FIGe2Vu6PoV/SsfGJqr3K8BPCAg6tuhB2m0h+TkaOjghZgKl9v928nipO+TyIZi
enBBV/ZowwxoJMCXsylq5vUCEfD8BvnKvPSs9xFCjl3L3zBlCN1RFXa2hkNQd3Bmz1iCN0LWqp40
fYEYEjvga4yhY1t20f5vIy6BeA1TxvjOkinNRFCMk0KSBh/1OuUb+aCODhfY/q/rWoG0FpSzmuEG
vf1Mo9PsSoj4AnkH9k+3a/4JmCtDPe4OC3GoMmRsJoUverJwm/xQ+3GoN+sy3PsX3AW/yvnjmjCX
QKdV3Woj3LsMBsF4vr2IuZVpAr4Rs8EdnTEPNtr85mBVEBlVfflsUXKvSev633U15Vk/8ZsB5JBA
3LQpSzr4e9Nc79N2UYl9/bjcf49o8paNfUgDClnhDia6oMV9y3BX8eWyQ5sHl0nxriBtV/o19DrN
rHuH8jaMffFWR4zbQDZdO4v0CEe9XED5sSnRoS32lEbb5NBuH7Y9juz30AEDXn53apyNNT8h1neO
P19g2Cxn2anxFxw80pdsyptdj7Tuioe/eVM9xv1ouJJXVnXt0rVuGaCEI0Ka6V0mdJmoPXGD4sFx
auJyBhmqR3tRxRHGwnuxSOUVVCaKahClaOi860EG+rkufLrGSFauNenbu01I7RsP5mVv9/ow34pm
PQaU8bhO4JKDiT8467yORUjP91GSGgiGei/1LFwe8zTxpX0f1qy7alswwqGJeuzHnsjNIRCEL+lB
fegSe+Ecye+tmuJBto6iu7J+sk4Phcvd0+3zrBuEnKjae2CwKup9wJ4IAf7SXDjm2i2ANd4jZB9H
3bWMjmG4vBt/0wNeSZ9PsydWk2LU8sWiJDUHPgPJL1uppOU6HZded1AIR8OqiHCEOmczvF4+Ipxg
GZWH8OBIR1noUrFUjaHvrKD3OBPVmuD+G7EzrkdUH5FX8ZpzGizc9snmA4QL7fS8MudI/BkkqPng
4/audZ2W4o576CKX1SBGDeuKDfZ7uNmm5OlSqai8Hi2QQkB3k2U43fNbWfYwyiLCQ+NkEDfx7bDZ
R0PJ49S+aaGQjlo2EboIE3k/wvp5bGVZtH0meb2Cou2WqFrQnCILsR10wp/qIrxkicyEVL07k9j5
GOgUEvq03ttyyJ5wxmx44sLQvG3kP2fvdHWLsBJf/TtqqQQJ2SqiD3qq7Dy42qf4r+RM4ZLnZjRA
Rx6JckSATmDujFXsVsEgt4rGqKEUykUS+HnDGNNuytC6eFt6Gq0AbYi0f7wgyab9J26q28basX1A
j96TmOVCHDVsrK8WKMh20zxplUYEqcoj6iaMhhxgKM+mU6/Ou3z3bgkJFE/iuLUNzri161fNASVE
yfq7GcaBdiGZeUwv9cgXKljjAvhNPrqHmuLxLuIgBLGyCTV6YOWaEn4RkETt9OfiYpss+iPmcpJ1
LSAUqDuHHnNuWZMKdrlpluytmnISKCScRzwF+NdMSwE0jZ53VlaXD6vFygIFraYcYFgew9QQILoq
2COmIUxIXyF5qeY0k8MmJkpPMWnOwlzoOrowtHcp7zdl7LBGcngtJdYQvGw4MzH+PigDWsARpefA
apSTxk9uzMi1/b4QcTaYg2XXs9/tI+97GHHAM+ZP2+dbbEsOPi1kbnfrGkFG/8M4roGybhtYwJPo
YwuuDsTXv6fudULy1uMZNvrRy8JGcJ9sp+JNiJQ1AzMBk/bXvnI37jFKkui/Lvc91gfNQglavQuP
2tGlMa1zXz2zw0ROltNp/ibUTLYrd30+p137gB1uTnWywENsjZKplBFB9nY8u6skKuMjjCwfoHr5
PCqUtifr08JYdjIJsivMminxPnlHsI7UL1mDonqkU2dotY5T6qL0t1Fbq0BRgevxKcA1ZAJAWDYP
CurfVZgiLhi+FE/g8/U9elkfze8OGFcqSw8K+R8Rkq5Nqmcm9g8WVVXBOr+hJionVZDO5e4v72fL
/S9PaTi+KdYp4SWx9CkdmlO8JpYz6LJc07UI6ZxOCs83VbvnSK7h0VTjasfjWYyRAHySCTt41VYt
VTaMXxOwmhl49SyUue1vnDN/GStRZ9XjZwdCZ57CifSIPkByi4uydMDQldrIxKgBgrVKOpw6mcBV
vPIxFlE0JxQar3D4DS5cRZ9p1QSHsalpspTKbX8AeVjTJUMl/WVIUu+FC+3KBA7XpZsZ955szc9g
PZBqu4CYwwdtlgFGJHJpExkMSm+3vqKCyKhEIn2gXZ0vnN1lBP5ddJkxVlIR3BoEgNmSDuIunDmv
mmEIkspevXPbZmf7hCCwfiVqRodawzHLFBJRKXCx8KggtY0QDw1H6i13KmXY4d1um/3vS87yJ4TW
HWcKaoFzkH74g2if69h/yWDaI1tzK7PSx8u05aYvjUgV5M+bC9y7KVindvvmn0G+w7HAvpMVY8tF
GMnTRKCdTjZHaWPvXS5gpxM9kR4R/hKNwQJ9LNvmA7JAhvJ3DMoBhRQx2sQg5gCj2H4fxl2251VF
G244LrnOXcxu555ZDrq05RwfBWyzkJtlJQHFoJxble3sD+MqPkZea7Vb0wUpcnDFdvF8XYLQHvQ7
eHL3axsjx5QYf2wRT4kN0EA0p7VyeWgOUVJT2HMKKLiwtgrCHeG0IPaCTlbt1w3jDutm5VHzmdvJ
c8GDufE8jd09oreV1+GjNaxLhrNERjvoaHOaiAxeGOnruyd3anD2DL97ZnEjhrJjCtVVD9hT8YXw
r4opisPhXJq6PSvqqNYVIEI3j9Fs48z08MIFJHZ7agZKx1RQjM/GAFx5TY62j/jpHX/LTK+e6O/e
tC26KY4dO0DlBSFrdBVogBM9zNr6Dxlma5Y11Ye+vVh3grvhDFijW5/lkGcVN5wWpDX9mdZBqFz5
LwK3V5bSsguGqQGI/F1rQLq5Fglv/v4gI25H04C7pxSQ7NezMoaO3RvzERlFcD/g5K59iiEL5UqA
/1yNtcM/jatD+wY9GSSWvR8s0C0u245J4nwh+MEjWss4dgkARNZPIl9V798gnm4xUpH1UghPwAKn
W1zUjPO/L50TA/ysTqL0jPBYeiHTdBZpXYgHcqBcK4Mo95WPoJvyQbdwovkDOSPhwLyEX2DzoFkd
T3m6QRAJGoKkq+8qp4k5TfJ40qby07IhN+YOHOTJAviaglomGYGIQzXBV574kAtZ5X9blbfU3oRM
DsIhBpGAOBFMdAHx/t1KHdivBTNE6DpiULI8hghxj0hi1EriLZ4lJyzUiuObNNFu/fuR/Jh8DOhs
aovVNwZX8292S+LAJM7zp1zQZUvJVBwgZVDztclNORpZsT+HC7TqafV89ol1kXtPMghqriD1OBYy
+qoXkKoT8dKKON0Xb07li4eO0Z67/mjmwjHDCjCoegSL8bLX00Isai7hPM5ljEy6Kmj7Y9tcMUfR
QoMm+CwOl4bkJOTTuP0w+HntKVFd9ZftFPTgOCpWtQZM9kAKcHydZUUannday3N7jobQd+yDPOCE
J3LSrpxdB2hEy0ZwODia6tjb/8wTYqyYA1/HFwPw5qLlUgs2UOim/IvIaHpUOXEJse9iRdzk5uwS
iimICKM7Zq5mHkccajqW2hexklHB/c0vev/TL+S0OhUCheym6U7hYQ/ISD1z0Zw9ZZgO4W2Jfah/
iRvdp2DVxOLu2sQ1FpEmEO3Yjj8vrnHph2NBGnBWbJ7cAbW2B73rsAaazKdNN07VWj0rCwebwes6
q76hXK4VDZE6KBxO2p/EEHYSYiQUqk8AiWZOGEwzl8hAJYEDjg99ee1okA8I7cItWvCtaHCcr5f6
6y1bcRHxCd9FFw2jiQs4RTNMOnHjcgbkLrhr0sDdNoo03jGB+JfoYYzhlGvL39Nlm/On+WGORZJZ
g63+Yc6JMjbrQ8jDnE5hFQuR8rX16D3gE06PTosei7zFWE7ZWY5teNsgLAMX5LiGGJcSxQ6Pk48Y
tC7P9JhWBG+VmhudsV8MYHnVX6cGS2UNdJjS81MawMz/HdcSYO2BVNPcJLxSeHzz6RbgqZFr5Qw7
X1XryE+cxsosY0oxQ9PBUNbdFFrNTb7gyobcNnj+xaDLM50HlKlMu9CuCiTdymE4RYz9SWu+x25f
70cM/TKn8saS0K7fjoBC1NJ//iDBupl4hxq0p4kVw5Ajc8SoZ+n4/EelxizqmP3Cd0LlLSn17prJ
FczupuTlJ1bGyisAvWX03tFn7JdsSbd6SRWFo4CYT+mXouDzVx/fYPLJCvxf7vTcFoFMi5Dddj1z
kcPfTy+ciNdCCD2ZVOwh+K7mlCJGvRVtt1cGcUixzvlzGd5O1LC9itbShBZv9wB2AB9Dv4FiFN8D
N1uShG5LfpV7Rv9KCAvm1JDmOReqyTSd5mcDygWVvGCOFS6UXgLuJAGEGwVog12s7nsRl5ZR2041
dVwDYkgqWxp4nUdU1K7WL6K5McZsYbL54eEB39iRLEztFGNysooLpdNoZ9jNf0FMkv1exc3SjKGu
jfX3SBscl8LQrOrLP5f2YHyQV3C8KWDGZ6Om+gxsTlFCS0DT6SdThsOlSyo5cejGLLzFSTw3Vgcr
NGYjCbAJ8znRVHiSH6hubC/XhRy3Nwf5Htz5n8k/m3cWgQJbyadv8hQNO9LQNIGtM9z+T4q2GDKD
Z7JB3Bw/Ljs7MJzTLQGlRSRFx338NPtocI9CjlMkIfRoh12S3ziQ0JKprdbc6ToSIo4W2DLFxiZ1
n06nCRLTifgCzWZMVR/mekMQaxqFqNBF8Bsdm/551JzyY0HlTJwrKtkGsG+ZAzi7XczvIC4yg5Ok
eX9KbhautDuupFmvMQosUKirmdpoETId6GS03egKc2RAiAdXBpozDsjlFT9es1v2Kv8iyp34QYYG
ne4FJA122shi8JbES6Cg8FKIq7uck7c0jqsYLhDT7XbXI1EZqNRDT1kY41GSLuaVfOe7ntaeoG71
vfQy0J8zonQxi1Nwco9y+AFdXncvCaUWOD+jPVw7+yftPJkyXeNuymBbtmB7OXeFFa08o1ifHVWI
l47t41SS+SpMlXLq8iVCag61+kmgwthiW71AMovbvqySgpoEKXYQO8j3K16R7lfFjkMYZI6B+iyH
fhJnxfcdNdknPJwvXRlau1O4KNYUSvsZyDuYoy2oqq3SVmewupA4nSu039tZWoSTUfx2UOfm8j2K
wXSVM7RhKyBZz4hQlJpitTo+dY1D2bvG6iNYFWPBQNiqjxxrO6gVJD3gahhteAkM7LSxxXDAdCM5
YowCW805HvYz5Y8o7TJmBAoS/QHxdw3Lw1bxoV034K9d3H6U26v5+yh+y2J8ZUYSqqQCWPcQ0vmf
XJwF8Q4V+qRq30zFEtMNjm7kOXL0aj/dFreF84D+ZswLbedewPkreBObPviZPqAoiM8FO1Sm5tR/
li5wQ4JN/spO12XMKLFswsK18hopg9KLFcYJuvt9q/iTwrJGYyUQiJAhePERpQ/bvtnxcEOOG0cM
rzqJef1UOwQgsQLCXEEpxF9fmSJ7mZuqKIaaZvAP29qM6h+o/Z92YGtcvCKGWwYT540Ihw9K7cs1
yYI2UVmOjnWNIBMV8LBQwqA5fD7W9cRKU+oVlmDxrPTIXBDqYeHqOMIxyUa7an2qdT/GoMPhnMdk
HO8yI4ly2ZWPChdguOBhet1SxieW/iSTJBySRFJT8f5faOpToRolSQ85DIubddLai2VoI6pxBQFz
PoAy8Ilmaf/IJK0OZvrNjkEf8izVVYnY3DelqxthUUewXFcbryIjzQ7z60FlBURpNi8i5LHfT1PB
3mcAvdnbvF57Wvota8lgAjzhyUKkkymqB58ND7hAeEMShVIv9rUGTV3GrBE+ttKjCeMNji04vEFk
OUpsE5+XonIwCh2th3YaHoRZ8mygBONDM90thPAS3oiSw3iJqdwTHKt4Y29+VspN64Pqi2gx9dn7
FTm4UmGcNF9uZ1fH7xuROrUavkUrQzw+DiwtZj7QV914bSzLu3cG9jEI7T1tHqVvUH7Uka6N7TNj
WZq1Drd5APiNztFFVjKDvOmIZswZffMZjVpCSnysp1S90gWRj2XoBJRihgA3bk1A8Nx0228LHRz5
bqdNFygX+EVPGkPvsBLvCQ8PNk8h34HY0714ubjvpfGNzZsxhhkfmJmnq3Um3BWtWlWt0UzvRQK4
BiXvugjRdZaUaqXD9CzOaDrrgy+cFk+FgHmj0qq6hSfgCpatOQQ50n1bVWSKppbLbxDd5sEg6Mob
edFvdaJMKBjnV4DLIYkraMBb5I1zjqdjHqKjcuHi+La6ld6b5HHSjHEjIc6oae+2QiKE2AkaQ+SO
3m8o1mdX7ONkB6QtNZZaBFq5LiHthMFu6MnBgXdkJuwD4kJDG6hCINbLeiodk83PNWzqiI0RShtW
wQxFWkLL1g7OpEhnVLpHLttOmQXrFBcRNR4ppIUlQFJLB4rgpYphXqCyr9ZNdkk4v7kTwUBH3eQs
N+YCbJYWuPvAqSZOnAUeDkygT/pz4bU21MgOrhPVfDR7+8o9OEXaaJbDYjxrOnDxT1442tQhMNzg
bz7WOWC+P4mWop+Z68EWZ2FfGx+a0CLWu7kL2AHxlavMh/VSQmkY6nddMTbMzWPWDyUcdc6ertfK
UpDCQzCaqXSGOm7NXNHb7KAC5dRI/0hyi0qHwiAIZaAGxs8QMszeSGgHz7FBDUQZ07RMNbVj7LDs
rNdlZdOeWchpUkOZRJv0NsICfamzTBYJZ6Yq3mV7uP6fHMzS4qprnH3Ok5iZ09RDh7IR6KUjVA+7
P1O7uV+xBv0XlCKFMdy02cyykDsTno/4rdYwh3rYju7vRlqxoZ2krsGcTfh77xYDBa2YikeUJau+
QB+cwx/pqj4dUXKcQh/OahcOukkWMlQSPwKzzvDWzVy5apVhi+lBiYn+7ILExmRxaTHh/wSh2qiz
/46ceN++3qdXjA6fULfC2BS+YRUxIyqJj6nbgHTOqBSxwP2YfSdOYxHm4JJSiVpkOF6EGObBSt5F
hSUc13tarLTcCQYvwNgKmgXqQdy9bJnxHrjn4i+2ZkLCCwD7uwHeNcL2wfCXc9NWGRl3O21IGgQ9
pfydTTKNM3U3oz/aPWdh6oXYkaQpM9bvCBjDd1AsHVX7gS11FY662N/fG+F+0qMDnvjs0voUYvMy
Ob33TR/AqdiE6reRLCc82piBuT3J5ZIhXyb5vl8IhOD97v9kJLYrB9yJlA8lNf4hrt+jbdIxFUBX
b4/f64Mz28riwUi4LEFEKvPL0D3Wa7ciU9eujw5Mog2VWc2KHR5hUjRz9pf5Wxu5OUH4hH5f6W35
0hwlnIpJ5gggEd2WVOuczEcStKijbqW+OS+SAA0NZ8T57TBgUEIY53I0uEhth044Aar7udV9UjT6
2Y0gObcKQgi+rT7qXky6+qmLwjwDPW8L3xGKs7EDsjJcKRZ1nbhABAkiVCvlk5nHhvHadQZCBHeJ
76nYSGELv16wp2eomkyhRu49OwAEg/BnaWPTbh4qceYH4GN1jTbGymaC/V5MeSxT9d8IStYq7anf
cXahbGQSviGaxpOv226zCLu1RIIAWZHv1Xf2yA3Qu3YCTx3AmW8aqpLcVxsZCH531W8OoYNyr3wA
YIMVPBd8jezhfjNGtQj2nL8E77gbJTtgfI7e2gLJ4OENWoClwFDas98Iw1SEUQc3jgIzUN0DLTnK
K+7UEo1nzC5mn+bNtmbn4eDsgch7P+5Q8ZcHfUkePpEfIFoXdk4UsIWgQI1cDKHI9GTKIsbVYlEq
vfPOTw1X57iv6ZdEiCrPyk3+a8cCVPcDmz7pUlXEv54REIttAfFDn8wexcCBVGKdU5wqWnZ+zu6n
sctr9Tcn+SCJwJ659UNzuU98+9482gH2Uh46MSITP1WNModjgCNRXILY1CX1oE8a7ZhM4Sy7mec6
bZlGMfgdwtuAZFnQlVIbrdyRhbcC8/61qpwTw8gbtSn9WDMxUi1QTPB4yqLpWC3Rjv+DftKUk7DX
VTtSWTR+6sJa8+pfNosxpzu39iYykZJVNompAkj+crcXbWDDYCDDcrh+xnoqXjGba4SWE5sQX6H7
f21ntQAkAXvuLZxwrjqBH3W4zKq0FFHtZ8aZaKr1f2e0rT5q61YOBAgAbhZQhJD7gvXeQdH5bpdp
GjgRKtF58m6mhwaaaBQgpFzz/0xgu3PQMImzbig7TPON0j9DBvi0hgZyHWcGyBelS4TZYYFjIcxI
aZP6BHaGVwVrn7y53AvGu5fBM80HQw/tkZ2pU05VlFLUo+b8/8mSfys3L2pqZqy9zVQUQw0mGFgc
Isuy54LXzamUEcwnkq9rvA6IVGT3K+4x6KR/QJh2IFF8pXG6OIsridrwqXsgoQGX1aY+2p1xHI09
U96K2XmuWkKf51fkWKkXDauohmZGObQgpRCUVDm5k6mR4Qp7EN5Lcwox4oXnWiHiGgKM5JhJERt3
2vh0NTOQQVZ+GOggG6D/8S+zZpHocCPwofk1c1I/gua0R701gfztuldlMEzcQF4FnvT8WVINBYi+
pjm371BGrKfb1GkrrcF9mbh6iymSfeUJ43jbO9TakI9MfoFfnnC9jvnCPclHjPXI6B0SxCQ4Om/1
ShiZ4Jlm1ZNZt1wjyjapze1l1RAXsTIY7CiohxwbRc/oR4eYqQrYAFhZFJfrfjBvylLyfd55R17I
KOQFK2huMqML6i0d5ee+/dV2Ta0YNOtgwJQIkGkMagsRqrZZsVL7aleQoOF1+38dJHkMP0v/fz92
WezSV4nOmFpO2inHMfSrtgDqaXmVE7HNl2MbJj2x0NTPuCHkEJjjPKAqC7pTDjqghgdIhRjVCFJp
LQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_3_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_3_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_3_1 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_3_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_3_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_3_1;

architecture STRUCTURE of design_4_c_shift_ram_3_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_3_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
