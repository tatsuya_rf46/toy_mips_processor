-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_4_1 -prefix
--               design_4_c_shift_ram_4_1_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
aw6Ww+f5j5jBBCvCqzkqNaaEYWvX919nhbDXYATB9uFohgWMmzuoG8IxfTkC3UXj7jiNDeapdtJn
O+eait5cHnNL3Cclm7asRuJG20TjFlNxSOY5yM+hvlVtoSrGdr35miNN3KK6mogqeP8atToSW9HY
6hCVXcP6yE0nhMaZa0nElt/4V5Ll2k4SGpBtqHZnJK0nYOe90+5XtKw5nW/1520FaIu1Z8GBluhr
6iJBciZw/TMjPnMkBH6+Wjy7RzPSq3wjN86ajTzYg9O3A0XR6YWs6PsSHIGgWvutZQs53bCdD3TQ
IkAIxI+HdCWgaKWjANFx+AH2Om/mTDH52jpMHZuRK+1bg5aNvPHsrdhtTklkT1h3gTU11dnQaGaz
1ni2ome38rpMAR12S/EFDFoh3twlPjo1rLIrW3KA/NqPtgEnqy+OwFKHwR+gAmC0VHVOjqz4wJ67
X683fBYLpeNIrCDycM7tRB0k4x2+wRCs5Vo6BOQ2o82M48tuhaRdIAue9BGquhF+WQge1sp+IfdZ
NXs55UpKFXtaVAC+kKXIxc5JYr9cvHiV/Qg7bjW/fUSM28rlC6GXBW0D2zDr6YVXeij8bQZdO+UE
2jOtxiKpFb46ZherYGicpFAdl7YSdUzErmG4qHA0tA+BZXFvuEGv4V7l1Kf0jcdQSliFw9um8U2f
smLlY3kym5ZWoUc3Sp3XU6mlr/L3R9xfNiT3y8wl0pTCz2cVrGu3+gKyvx8dbR5iUvqt8vVKeD5z
4Ifc1PVuIR3UxDG4YUcbSpVIGtYx3l4mh3jcde0910JQvStmu05qePK51qpVTwOqb8l3dlRR4ZxZ
BKazDswAi+4FXXxpHUus//IZKDkdiBAuwwY0cmhXKg7EX37tM9FHV8183+pPafo+/FY9WHLsmXfT
SsZ4e8eW+jo39XYzAhKhmHIWglDAojLpKIlyYoPGEc/AlkYj3zTinT218AfVOxD75qtpo26mGjrP
kc8z7juKuQ76vOw8sq3VN7XyVhn8yuKo/Q4GDvUkUQoTxuFelXLx4PxXJk4FNrI0YxTMlimTxUYr
J3Hk0W/SR/PlaMWetAPRH9WgNEjeuJK/fgMoQi13RR4KRtCpdfWu2IEBIoX3tm3dWbR5VLGJq5NN
y9PThnmBotvlL/bFlZC1nV/VHMqNuvl9ZmfxI7LizZ+qN5hQlwip8HnHJk2uowObM8BNsDAe5NUx
iQjWrVBJe6omU+05r0J+VMXuLMud+2JNyq18BFsKf4HfpYs+T63M+aUpgrBqC0T2TMH9o/wx16m5
fSuo4mmGjZTY2JeqzJB3nwDUB4yot4w9TwD1R+EHuRPjACC2MrlPujYB4lUqfQmFaHGqKLswnSDi
uJ1fDZSAlXtYcVTNbb6zZvGwBKwBl1IrUkWOEhTOvWYKGNpJnMLc/ojGFDxRpBd75LM+Q8eyRqpE
fITNDgG+NofnBadWZ8TW49d+I5xeET1vkbmqjcLfxKbNteSvDuCMdi887Q7CzR7XXUad1JrG+Zxy
gt3ISNb3nyGtpvICVmPTwRTlPMGf8gzeGm0vF1TsCnQ7HAVQnJnxmu3nnQgKg93z/XAGmjh46BmC
bARuEwzYVaI6aScwBRBey+Be7aPtKGz7JHRC55XpLlV+v+lJuqxrlZ1UE9Qp+gYLnkeE9Ruu1zsR
TepbxzUkJoYzXF0YPMdELzISMNo08gfP5YthztVOmtH4MB0jTFiIfg4nf9ZZACRefc9mX9vSF0u5
dOi0Y8eHKEl1tqiHh9G61sG8Ac27pm4Y+kc+q9mvRVKg5CGvdgnFyOIx1SDyZTLr4L5yC/5R85hN
Z2VnJTjkFdDCzU1FAzeFAMVM+rPNNPcro4IRPcyrDg2Dbozh2LUNkz3lh5McylUl8rtISftwO9c0
f5fcpW9DJbuFWacXSdL7HwjdckuSfazPWLcwdWGviD8uQkrRpcgUTBqD8zY4axCiXT1TiicIlKrX
bgE17TfNN3rAZEUKmC4mAe7P7xUq2Y4QrX5+vyMGY5Z2qxdzdb0mErRnBvOnYOC5WuJIY4VfawiA
VhDkbAX5MzWBM4MCxxsv/r38iYkrpLgSvqHJIEiONAlhvAm+QLp1Gls/UCp0b4qY0vsvnYW/vqKM
OU/7dUudxyHfDx5xrT5EA9Ip7B36pwuI34Pt5xzKe18ZgEiMc109DWLUpvQhJx0XWbzumx6AGLtY
TInsvX8iEd6j+dB3M7rxMuGxtxJmDskBbULmVYY0xAs6+M/uyWQhGvk906/NttZ2t1n/QN9+3uNm
3NptfkU8lBiLErgci3iP+Lz0UrlHFL2LOMRl+T+3TstJ+PC7f8NE+FNXXS2yzthHSz5MHeuQuJlw
0OEChQoy755tjJjLZtOtHo8a+NnlN5/zFj/BQYT3/05o8OlN8ntXaSBU+M9jcLeSP9IDtf2/fsQG
Ka7+eELnYA6r4cTAOeEvUbet4AfaLQ3XAxQUEyF9xolq/+akJTf6pVvyDFTNQcls3rvEuoOCtQqq
WSHhtFsJzYTK5IUYUiMhcWHwNbtTOtg0tWXn6P3n/+eH6pey5RDcb9yvec9+Hu0D4hxHQLWojA0r
7Z9o7C9KNvuSSex6XXhpJsvOHLTcmu/wFBDl30JSXFXKEO2nWsIkR0c6HBBqRaDzPc8ElKm7GhfR
E5Fds2RBZP9LZoV6D8G9zon4BJkWsRPXUjGD8hhcdncdjWP7PC+y6oUg0Sf2D6CaiTcKvYHU6Ba1
O8Irnejx3pnoc746EezBCz41SNmAzoWLF2hFa/UX5UWVLf7s4YyYt4q67jIdcNPVcBaW9y2wIQ6d
/Db3/5wuzDblJGxnHFP873053mGQlnu47SPV3e4plI6mdxo/lkhy+zK3qAmzeCVyqx851/2x8E0E
3BI1MJDpCJyx9ntR7yzPibgUGduOKCMy4qjQOVKbaBLKIxwcnsq6a0sAITKloD55S6bq26cBwtYe
DuvnD8rU91sXihU3/DtePtog6ugH8kJwvVsgPU1KJs+tJr4upcY5F9lgxobV4o25iU3op6QdR3OU
+TFpR5/f7DxY0AYZf7Ih9wN8IyNnlmO/BxtdLS04kE0ysB/irP0fpbZSTcTMuW2QERuJ4FmfpoNV
v3P1hI5ked7cX/tuopE+1QvzoaPROR3mbrcUrVs+wjWXx6PQ8ZT7cp9P/3DBNCdKEHAEcZcC4fKX
DmH2aV4ms5WcMYbbajSHvhXUNitANy3pG10ZBs64CbdXpO4wEnbt6zneRm3CaEm2O+9r7nMAQ3NQ
Axiobm/aFIVyBjfaNqHEpDJc3YoZRedZp+FlKnsQ1vH/zIEqn3MQA2ZOXvg8tiNDebzRyGaroaFl
RAXa9F2tItZbYIadlpYh/ix9RUi6TC3FJYZVFf12glplNFIgMTFS+FjK5dLkFMTDkODTDygxDCym
ABmoBtdjy075/02Ntiv+YpgfxlJiZnzge+OfpS9hgGCIsa8+nEbBlb6CJRMn3BunUMKm21L0/jHD
ZkSZnODuq3NUrUxB8I2Y3tF/LYLtiEnT9NeNzAHJwz39JB5uxjTOHKrz6KzD1ZQsoAtpCBX/ZBQw
CCTxBZMdz8psl27NVGiXRGBwjMGuxVl7YyFTwmPLNG7qxX7nzrtG6fj/eESdlKg3k3tFzNBY56DK
7BQB3dxReKS8Z+vsJ6eKg/of+52mvfK/OF2lcey1imJbUKOntcRRG83Ex/aNN0RCMtjrRsTE5RYu
gutr1kAGPX/sX3U29ChnQWKNaWf46cerLF7c0dtu3SRMx2rT2M/8kxhdeKbs6dRtRziiTLBW1vKK
OjimZxDLEhn4PhxQ+q1tQ99jfSZEYnEz3uxmgonW/Bq/+0T8L/9EccWClCw5f28Aqnj7p+xUwDkM
/poe1JioOw7dNj/VyPcMCRp575Nnb9jHF5rKMowa35d6tylLmm5cqCK6c6dONY1Wmegbqc+6GhdX
SJ9cj4OVsaBZGZqqFPxw6BQkbRgL3oMSSij3wFyeFV9PukirqhEruyLjTQKCIPBn7zuP4LYQ05kj
pWK510TCFX95rR0s3kK317gFCgjW+11RWrVoOwEd51Hx5OlNKyNPRXgECUHKUsF2j6KHE4EOslJ3
VnjBrFN59HQ29K7bY+TY5URVBSchFZWwrRthcw+OHvPHzGOEu4vhvSwSSK5LpnRAj0e3k6nTwf1M
Fu6jTPWqAkD9Pk3HTRAzwNx2xg5/xaoSUwTqznJuDmbldg4tI2lrKbzVB/w/rXjgMEOxixjgzIFj
owXhjT3GB/wwPWyn9PBVhP0GeUU8tFTZUd9UcBXC+RvtVSXytl++EkJEidi4Eq0cxCYi5pTjTgPP
6cnOAQXRX1cSD/M0bs3+OijqIg/CPb7OqU9XLFNZJlfpeAsM7SCtMUTH5ILp3WNGBnE/vhVZmqSt
XLYG9D+qXHfHoEvnrXT/+jmmxjpjEoS2vdNs32KTk/+MY4ISezHYfUuBTUhtYZYDRQ9Qj09t27pN
erhGA8nV/RGCszTZqr8TwF2I1W4Pp46zMgyAh4GkDKlafNN+0I8EFAQKltH3j4DXxFsPV71bbJoR
lX4oMfpgQ/SHk91Nrbbcoi7wzW8J+BKZ+xcS/KxB+doVtYN8yk0OnBCIrNsVVAQAjoHr5PwPa4XV
MX8pTLK/gADjB2bUzrJMVYJCRlq1Cpb5hRXPR3MzXpUWb6k3lB2zRnekPA4/YHzXtAwvyHsxvcoA
hCsWJ1/MP+6drk9no87Bn3GBeBIE7j+p8STJo/dxRvTPvHuVLaLOI7/UaGoABqz0cYPh23HvcGfp
+sEO8Wx1hDJcyueCRfRf9YO4bfh9yAYCnULxvkU4nsB8y7o3JL/pvCtR6jQKCyBDuwnhd43H8bSZ
7aFoGywewcFmrAjYY2QX32+z52igbyekST3F1Klb1cRuxEjEvqKrv6FP9KQQGknaFVpA3j3cwDyy
ylbjXlNXsJ7Fakk80oHeOhSuE1MPYkby8vwFJlFdhQyel+Y4C50XDdyNUIsa61vZl9lU3SiX7IHG
C2o7SJlDI3A+BQ+bvlie2zJXDrXU+iSQ5MdrIsBEomsNWKxYSaltqjGG08tYEThHG5wdFI32oL0k
M/Nr2rqnSB4x2J8V1rOIQp4rw6n0w9lZqPavh+o/TDDLY8FNYFbFLzuMLm9d5QjtQh1XvoJTWzGw
G4fkSpYMGFStLJqbTTaSOlOXI6VO7+5orXLogo6ETqKbAVRddNzc47OqMG7j87sScbSSdKM5XKw9
v+xa0QtVJY/RINrHyFJfvsQuxy7wlw3XkELueXuZWiCX5QiyRXrMXbQNdnVmTX2rvKscrnBC/f6+
JYlR0GqlDBAIGmy4DwYjrwM6NxteUPU9XPxUOKUsJntGV/iHzFYrQNDqHETKddegARIlxe05T2Du
hmxVGpYe1TUsMkFP+T2FjcMOZfoasQnnDPmO0mo9vPlIV+bf54ZPr6AZL1So2CBD11mZ1MgKE5L4
B6QkwALljX/UpE9GHTxSXcZq9VtMgDgl481G1UBrRSksLErLKbR2QVMWwTrGUNDm5aPr5jRpONZ+
KFkdF+yq493iVxe/Np9G5fFdZ5GEMHnKccZcot3IRDp+0bW60XXHJydpesNDRVmcPt70AVspz7Yj
ID4+SMjuUIzlrMDGbPu+cL3Vjc0tfKngBmpH7MSbzsxD8Ojr0GTRW/R2vZxSBSlTKCnySHNj8bPF
oJ68jh2N6kQtzbzrNci2SNCYZs+diKQI6MzX9N2YcuTXM7ar0kTJ2uikUbXQIgTaSl+DF7+c8sNq
qFPhLp0pjvR/7wxdfVTiGNeNAhZpTl1LhVBB6TTMbYc270YLNojT69VT2hQhp5FxrYA1jA3tDDMn
VCAxYOsROGfRDkcoA+7CdZPv2bHc1doFeyi3yOkAgjgoVnPY93nhs4H8VodBdcAQ1H8vTCztgDi7
jOD9Wg2uoL1TnufAR8DgzU10ajfEJC9qtS7TMXeOBicW3uBOsYxc4bE3sGJY42uA8YZKqp8eo1FB
bjjgwXuIc1yNyKKVrpaY40WQ6ehZpTJ+93ur37vmuPulHAKqfZ/xouRqoWjCuk1s5iaAAVjAHtpY
4/KTfCb9u6lEX29srgBpnheozNuqVO4CIC+C6sByrD3JdPKmWNYP5jt3XqrpAPPWogG3ikLHMwds
HIFpP2jLO8y3jEOXDpJcaSJiXsClOgfY4Uk0L8bY5rAbSaaZJ8ZnRGM3Q92axSjhUWXao8sygika
zgxXX1IPdskaGJ9pLLZlyDVGNCuNUQKb1jB1F/pPE06p4aMxkPDhA47HBJyiepFDpSZJPSZFk+Lv
sM4rnuIIJ70AUNqqpaqMU6LLE5MBZqM3F1VClI5g/UkQRP8Oq4lbrRGTizpNIyqV79ImAf1KLg2x
cAvepWZGrZKKp6VG3TFIlg7KUdvbYoGYw7J1YL8koZuBCHQIHsoDKPpDl5n9JSeSW5XpyndYUTUj
9ge0eyCBqssJFVoLeGZvMTJykRKOUYH6odBUdf/F9tFhQIXfN16NuFhxmaiDc11amam2/9zTnZ0z
4sevTXMrzWZv7SBSirIc5/xZaVt+ou0XyXr4KjLr7YpMKkgfFfllFKt+AoB3ambYSUWxdtGqx8ff
kf6UJ0V5Y9Ro/0lM31oeMNZUwZZW+b6MebZtWCeNu5Q1/Ygh1b11y8pFyOzRndGjbcYyR1+bqVbA
KXEpQOktNAm3C6PZF+lO0h7eyAT7IAtYXK1Ze7FzyAHlZVVEFOR6w9gGy/5h6542QQwq3scPioMr
iJHIGSH78+U9XcE6JWIt3iJXGwMwXb/bAz6hTf0+ZtMy2aFQbky5k8qyjIeWhxxo3pvJquUvH3bF
HSnfCmMwpwvL2nK/3dOHKqZ4HV8OM91iRnnqzGZGZwVW9ZlgNkpSn+hs2O33i1vNv928UyG8p72/
7w8EcP1PlvDagyOQA0sScbIEaFFuw7srynEugAEbOpCVsGUrVEH2D28pvQ5VrmS4DvYGJpdR5ohu
P357bw5PhHw/FxVr/ax8HJ/NoDJlEsbalmOy0IvQoHYSfL9DBuNAmkB8L9RVpGgb/tzkGzWWXP+y
Ibh8izF/jqu8dJbK0aQDJLWOPsVYipEyNiwt9LyaGdVu/fOIrYxYjI2gwrlH2J+gWDaX2hMEr2+/
h8qhG1GjCVX3DGmxJ0a29nXLKJtOvvY/qt5+7rUbe9Tv40F6w7T+WFf0VJbdAfU//1Mkp6CnmyVJ
71EJqOyqadjDXipxFkjCXJx2e/02xPPoIedwk5ZgAaFI7OXa8LeSei9JNSOONHvXRpbR/tcMYb+J
o+KnESK2Y+JR6W6fDelPUTKB++kKk3XNsCs1D2DiTE7d5ZGJNkgetZEeHM+CoDGUX34e/n5kj007
ohNYlTQSj37QZjgLzlv3iZUojV54WX3GttU27zKXUVpT7EoyKD3y8GlVhtym4Ooat0o04XERYDA0
yYYaFlOYrd036GgHdmIY08EDu5ggMN9zfY6XAeQc7HFHGs9JPx/C/FYVY8pfogyzRKL6fGGL9ujI
X9WizjQVOoK7k01aRdnvnw6x28lrZT5IbqYKKQjPccItbGJ13kTn7ysez98Xh+5WOxXxewqDNBQr
ahFPp2vVG8vRu9JdzNU3W7ZcqeW6Mg3eUPet/lfBubVTWSM2BE+5yLG4Q+bSheVOf4OKSToo2r0f
SLE4jZdqDo0EwCaxnGMje4iGpx1JnT4ZgqJ3uWUNRUNZDliszx7bV5ZbkkL0biMgb6ukLGq1jHn5
ae2Vj+F/xhm0qiqUTRmo8GsgIyJDIz57CqBtkUupUIy1qBVfxXRdn3dG/VHt1BShuOzdQc0I2ycL
vP5iV5Jg9USoDYs1GRTXd+dNcMCZlt0XWvlBq1KtpEyAbmYEAhyUAm+kkmNdCiZruEUY6My3l1h+
fI/apKffuOdrCzEsZYVYOQyZR4fBK2sWxthiO3uhiDroGYkHINxxd2q4GUEy4SyB6jZxWl0of5gB
bLwNRmeChNQUUatSWoM0OA3jCEvIW128dJOHVJsZh4qz9V68iDU715r3wMYsBTVrmWlJPc7fuU9G
pf7LeCZmukpBo3tvqHKdis7p3S2hOy2eOivzvck5YgKJH1NNvqrZp9SSbaRmhYofVIx9bBDx+hpT
a9E9b1oXzH6YStgvcIz0kUJQBln9mpk+Ui6W4bBPJKSQE8x9UpYfORxqPuZvJwEPPtZBM/TUq/jY
HtZuwHEjtHIWsYNazJ2u1LOjUeSqVii9tt7mvBnZ52qlcm0T7ftY9OZIlcuy3npVZ83YlClcql9l
HmE2Dw93WasiJZrXPk0Xct+1outPCx2VU/lUVr4P3XxxFwMFfC+9Dl7xBJYG4pin6GNFNN8kaEva
BCIxwobbuRAXbzi3j8cXX/sqbWUvC9NQI1LQLXJCU9QKmJbX9Km8xSS85wNcB+8ogmYIbKWK1JCp
ruFo+JqK76ln4eFHwbC3GfcRQVBe2Pd+8B15zrAFNRpO9uHHO738FJcDs7DSaceRAcHA9ykzFbr1
8g0b69NwANP4N5qs/1nO2V7psdwHRmX4//C0EByieywl5VxcNQT29/pDXzYHvCdu5BbOy5jZqVEu
MV8dwLkA0Oqs9ehe63p7nYOm1IUtJIgF3dAh4Wwlw73l7i4gKZFQPlo1op3a6ij4sQXru24wWk3N
enYrvlcqxE+U4FeGb8qNHIw6qkGwHsLpuzrQFsYoV9+zDZ3AfEQSVCdt+PcjIUPb7zb6vV665TGu
U6+l+JHwcRXqU9rjcs+h9tLZ3zqp//LTCYPsJojgI5T0InXBal0mywO1XYmZyz5Wn0jYlzUlEGi5
OG2S0nWUZEvalrPR0U3oIKK+s1HROSwSvv1I4IHHR3qUR0VNaSFBFgCkpSgvUzR1n4WOuOsG7Al6
OhLYekrck22cGLCWVE8nUVkbh1epMAiDUXVYO1IarsRs4ZHjskbFlGAfqbWz/KhtCQj8IOt+e6A6
mgCOZJ0ySBTguR81a+mLkxMABXxNWQBemI1uCI1grGzrGWbZHElkAoVRsR/0bfthE7TxBTYvWABU
sSHZMb24V3cygPkguNoEbwnSQ7aLo/DIbyZAFPpoZ9k5OrSUy9/r4F8eLXsViWn9okpFFrV9c7cC
HfxDvLYLBl4Ww9ZE1hN2FZZnkR0/+sKqwp/qYmYGi2A5GgPRvnrsLc0xqaZzR8YSrfWoiRno2VUZ
S0INHaaOReAVMPtx8bfQ7g85dO+/L50JC7cbP3X+hLSsxBYJKEFTq/4ksLKw0au3JIAkTJ/UWlU7
MtEZjvWPOT06SfVQ3p4InlWoaYtFi2Pmv4X+jVvQVPmq+JQJ+I4wxfHpCvpD15DJ/u3dy603Gz1p
RCYAe3x0oL5KH8OHg82k5SRoIaIvxKNNpRU1FWoXPP4/idzLrI2Qtt28pJaBJLep9K7FhYI2pK5I
JoYzv7ss775hYGJuqHDDIfILbsvSDh4tIFyS5h6lTAQGRvBA8c3heLoLZPjcex8fdgytKtejaLfE
j/Kb+kG1Lf/fEu4wVeUIwa8Q5CnBYo9WcSLmBZRbB8BK0QdYfiQFlPEv7esv2/hNWwCeKDbWDzh6
W4xCfqweivl5PVPvyM7o4l33cpvhNIVBI7i5rf5YIcM7vlhAUnyTBozTDSTcAS44XuA2xiWsKSXK
NAeNpl6G1mgsh/RO1uzoANKEfpanbdb0Hsb+xXrT5i7e7+GG/4Vqv3aKIOygnj+7DrZKtKbWk9LW
ace+DtYO/0ZfkeFIabmOEQddVdNaZN3dQhud0S9ARPm+mmjYHa12FRDF6qWzy5uI42DKLxfZqBSk
K7V264BSU0g9KaYc/wY9K3wPorVN1F+LeM4hTvEYk2NIdrwGDJM491YyXOS5KbcJQV/TGmfc5YAv
rIPsWjw8MSXjYbLOJpiKIhvsDiFMOvb80ad+AQxbKqQTF+QGC6CxqF9suO/EFbBjWUHkLzVRjMdw
Px2L05rJsJzD62eBtE07TiNhx/AeTCGcmrr8zJeuUR7F5rua3kgQ/vW4hVafMxxS01Yt+V29InWA
5AsRM5SROLgI9xWGZA6R60BntbQososdG/WKcQaz238m7duYsXcKRFdlgoiT6CMnEcdmAZ8UIDf3
xsx1CgG1lR03tmniGN9/LRDmeBCbbgCoe3hdzwgQVn0x/L8ePLQdXOurZpVsGf3G8XR55c76Np6I
pLdEOQap3eTwgWtSCRwOCYFjyR6fVx1nNXVIbMjYLwwO98gAA8Mud7z1eiLMOlVwPLbtmE6fT0XF
pTrMo0XUzhl8SXfrGM51CuaU3rtFfbl5ZpSl+nA9apUYp+fRaw/iuJRpmt8xs5nL0l6qPyBOlbMU
+lv1n2RY6EUiOJRBcZKAnPqiQa1JqfV1+0uHLE09+R5qc6XjnniAE/p2ER0xs+LjhVO5h0nIVbCn
mpxExsyYY0XHYmfTGsVXH6G/U9GT2R39G2HZ24+5Pk5684biYVVbMQJBff1J6sCL0B6SVGTFsEN9
bwIi5267480Zi+K/8FPmdNu3tisjA36Ow/k/m7kEJ2W/YRfqOo/QaiXFvMGrs0supiusmpOi5+fb
uO3pSG+SsmFELNpOaDIdLtsGNW5X5nEPu+KgMKt3oC4v+KgxUNr6yrTrSP2OrApHtUdkPElvXSdy
O9tKJ8h9IX6C1xtN1m0n8vB718UnVFJ9fXOptg9KQ9dTI2gcWLzR1MWOnDbxA5+eVhIxD8KMliyL
Yde8vCEre2m1luoxBhW2p7FumgyVd3Tj6Wx7yXI1mDaq4uE8ta/YSJ1a82+hgAIQ5kG7bvv5gwVj
1P4byi1dLIrb2P0Kmu1ZYvD1AqE8y2O+3EMse1AFjzfRP0T1PyCN9Tl5BIRTGAsOmPXHaQLXdDWp
qKEW+x6P///ToILBMHG5jx6rmUmq86/pRxYHX80cDlClyLuGgNKLEysxzxkvvkz+rhxZCDRjhSfk
pc8ugjDRbs/lPtGTAxN4d5/v9rDh3zcJt7i/WSHdczb/Db0Q4vGPtBKe/jDnLsyX+OZP48zAvoEF
FTdq8ySpUj70Bmn0gpnxeLDYsWkKABZJRrL0Eg2Jw3cZYWHRDH33NDt8u33zKv0tlpLF/Id7r9dW
n69+sInxCHBbe5lBCL97BByElJ3tKfvnslhUW1P7Fxa4NC7dnc7YunQPG9tdIELTjiITDei+DCU4
6DRRHcPptw/9/e3yo/6KS8632oOVSkdDI+yJXxoTGPLTU7UW4yN2amMTUnC9aSnhYfGsyn785ua7
bxI/OKzzu2Pxpuni2pr4sY5U61iQwOOLH257dwhmZfIDlBjpFjaiICM29E3y98pFnd3/jjrws5U2
/4Lx9kv0yawrIEKZ5nViKGMkd4xHn8paME15jKiu75rGjagsqlwSwrX3ri2LSiUJejEscYzGcEDa
TdUZtaxUlOdLUce2ksh2fLUIcBPhjVu7CpLp8VkoLQ+nckYg1Em+3A69SCNWRfM70TqaMXwXMVem
53nSxCJMeFNs6bGLfyngAxyWD7085AymBQ6TFkF3YykqPhwHFkwC6kPyevFjNNe7dTLAntgXrDGx
Hvf/i/9AcGM7qYPWbldT6/Z9/k+o9+nGgSqxb5Md7Ic599WfJqivmn61Y11rfhs3o+/yM2TzEGyK
hvywirpYMwyOQlzyXCxMWSL36//SgFDZpyUXrKY+L9YNI8bmRG3W41MH2QMN7E4NJ5r/L/AYydDA
dku9MqabCxJINLc+qDnhQieJmnexngUTiix+SK0QriLOiduxb3KREvcL78lqjWtj9q18qW8XRsHa
jcF4LJrmHAo7V5q1BrNYeCQt6qy25iWpTG6w0jaOm93jCcFqUsksV+UKCFi05giKWxtwKr6njJaA
ytRStWSZmadxZAgCQhiTimaJ52kVxgKuyfPLa5VqndnzQFPaTiDdLzj276qsjtjED9CCRLZyRLU7
Qdo6JWZ/7DDTgb4qUnfOOYlzuNcx3AvaQJPhK+COm90p3waz7TMBmJI7iuB1OIZ8ttlonvC5ThuN
FtHMkJ23jwuuQKiJG1U4MKtUVg+KcmVo1XRuvjh3PxJUbwVtOl+y1O5p3+quLhMW7epr3350I5HY
FLzy5/3Nhh8bGnXiS45tVsqrlDuDzckgTlDNemudPiPlwaTMHi6oifX7SOgioamw+bw+YOM5Yqmh
4ai6ex4tyTWCWJ+82uyOJS2lAd9DrJT8v0gwnIb1S6nWQsr7crNcx/ZokkzTIDs2y8I96YdyGGHj
lHnkeNclmYD9IJ9NyV6NzavODHld9QjaG9YdVh1nZ4ocbSaxX8Ni7tH/rEJdPmGId3XWO3mloOb1
I4bxqTuC8WXNcxPvmW2yiFGhI/BQy+nana8HJEnqFVinksOEltxseeiwSD/7nyFEuxp82M6K1oLW
mSKzaWPXJz/TusEDhio4bYCjssRMCDbi30tSqbyvEUymCfOkWJgHbupv7Waq09q/abxUCtY7DqUG
wphyM6hsPb180cS1LGweg2U7WA2FDERe0oOty0Uoin5g0L/zJkdxNMtubXJgo1aCj2IcHqRlFbAD
DixiXoy645Vs2Jpa9ZwpQzU4FJrnkti0SF7k6UQWuG0d40U7WI21BZWn0pSvL4/1aY4PegBdZmmx
3lVzeFM+cvrXtJD0ZljpNVk6hQaX8yRUblpgzpsj5LEuWXwbqzOgS33v7AVB1SmCSJAGG3NGj8UT
LBzpa0cpjxIYds2TEmxbHN+00FpLg8hZ0vlUFrQgaWWRic7Q8Y+gjTAPuU97URcbj3R78LMhPNiL
PM9fj/QtRTEaACKwnOck02cCG1rcPxXwAOHqn4Q8Mak+ZijLkXQRCqh6KmRspDPKXPU0Iha8I2Do
WRx7oukS0wCSUNsCS2F3OTvVlVQzMUz/xE7F/nhAyvVqM3adPq2+hxE2LESopGCWQEIuHvyCVrEJ
TYJfBxOI7xeJi7XXVQOWMAo4OHea3t7NyAZdzH6JAX3o3xzG0Hk0CAyqMM/OFftIWGtE+0dOR7ea
oK+wYPYY6cqCCJfYK6pmmqnwyII/PY2DP/E2cX8jtLSyqYfFhOcAYPx5IFfu7Dv6MF9bBkax6rNH
u0WdzCJ4HGTJigKTTgGrXdYoIaDzwoSG+iwXcl/KHVYL8Qhpv+sTzJtrwp3NQElnWWY53eNGf+V6
qacVofMiYkx3EWcoXuYfXAqG7GGYCcaE930xTfyKThjgDU1Cj3GpQYwDmiTLIRaDriqPPlgiIrAX
vfCrfl/rGJoWqKAmvmsaZHdFXnmgNFlKW+DTnM6RH/FUNOurxkpn53rotlI62ULikTkbUIpTK9Iq
ntz8uj5Dh1eEXLVea1ejhGoklufe70jK+WNiG0bZqoi7mjLvGaLD5lxpZ3zHmhs6bEOz2FfW2luN
ohTDT8qzv5aBXgzKcRV6WObh4cJhXlESlGmKoHm0fqXIIkGGPsZRpRfAAjt4Uy+FGBsIpfJ48ftY
acTpEGj5gz4UYmvT39qbS4KcCtSxreDKRVY4iY5LOtA839WYQlDCt92O8ADaFft7cLcl5+ITN0c8
U2SGbMM5gDTmU5hUaae1d1CbLjNQ3SqKK4q7tkqH866lxKCogIUnHgYoJJlAGsHU4acqmyE1P78T
Zj4FterGw66g1V6k8SYYBJbZmYK0LvVcm6wqbCIke70shwJQyzNXG0MbfJP3GxpsVtrBorOJ7xw8
w9tAo5Tk0V+F2xnkl41FSA8VJx/Py2XLQM0e5agQ3vIwBqU+HCSreg+wHCL8oZKcccp4ilr4CmPs
g2jUf9J7HcltuOfBSXCE/N+8v5L/X591DHn4aPKE3L9h5ow5dhofJaN36dhLgTMBZC0zt+zlsXbo
MCHFZqvT06fWw2hzKRQwof4vjGnirWWC4/boanpe7d87iHBdtF6jb2rx05kQlFmfeXr9QMcj17I0
XNTWsjG1KvNDL/mhlLctp4F6XTTo4d7o+IiBMr9HFWhF1khVS42CWZn155lXlXdulQNSh5FHfjPc
8SWFDdBWys05ypBeEmhP9lBT3pKBRWSPRMyA5Cn2gQ1TqBgXZ7Zd/cwlnV78gUJjeliKkxRGr7Y6
diQ6PReOsHZGYj5C1N+mQj6v46ugmKSHBlKS0ELA9iwKnqvPzMgmL8tMFgc1bUsVJwVz8eReJV9e
OjK8xJrJwFPgDmI+xQqkqssxMrEkG8tPB18TwIU/VqWPW2jfE66rbdIsAuLqqMKsUKAZ40OHihec
YFES6m2hPJ/QOz6YSr6q/viwTZ23vlyLOB3xFhLEZtx6ZWR3WQQq/Q8C71p1X99T9hZPlBOIIj4m
R5mcF6eP8Jnqj0e5QCtUAMYGfMvlvIxu8uI2Pu2z7jPVXdjZfJV1BXBQEUjGY1x5vekgvwdGSRmI
XKvaUdSVh66W2cP0KSZ8S7C1xX3CNRjN0txAULrnSCrN0LmPFxpfTVhahE48JUiLJyLAPXXCzEM/
ePQt9P2YkIFuE9tOUYrOn/Is7pFRvvJGhiE63qEHOe7YV91DiOzbCTv/6VAzzwk+IaWecprER9/2
ptr/MaVyLPP1ypbqVi56jlBwb/TsQIGHlVMEiY5pDaHAquNd2mZ9voYjp0UDMuUz7OmjYS/2pVO3
kqdTOk7nvRmL9pU3S2RCe0HRQSAH2Fs4gtAwz4tRbydQ0PNRZSBv/FraUghREXe/bHuL2S7ibuYb
PFG0l49k4tFmEAyCfzb0KV53uf6qeG3KzpvWjS44Hlixa0wJVWme6UynBFz+c4xxfnNbvKfPoMA5
1v1Xo8104VQcPvo6RorRv0bjQ1ncYJBOLgw2o4rSYHITcSPYtzwVeDQAZJnn+Ez1mTOJfjm6PVFs
u0TUeWBcnlty4ojbpiQL3L+noxQNar0Amjp7xWmxBEU35+Rz2xcg9Xt6SP3Gv1N32W4SZ3E4bwnV
gqSeTfOKw7CPvwnjd4dLf8M8lMYpeZgVUUYDc7Ewz4ntagaoCqBir7k0aU0a/WPMRDLxC+gi4ZEj
6aI6K60pP9c9dqo3YXWy5bqhlyQlQ+9zDaBAqY1aeEkU9bwQxFaZ/SGjfctBb6l5SQulpeNyGJBm
wL/aGDFYlZp9VbobC5c6XFChEy7sWfh2QsIkvDvHDEuHPbjY8C9I181pXRZPilrQDolplZaHnwAI
5bySgg6M/B+M0A1R22EXOzxHk8p6wWz2TeW7YEQA2beROsliWQeZRYREJaHZcP8wCGOxKUDz9Uhz
OBIoda295v7sGPEp/mvwivxLyiGQQFJSSnTYDAnPBbiND4U4Ga2CFrrp0An+lI/IpBUsd4J6akhC
d1SYok97m1fRZKwNxxzX3PIBHOMZHumFCmg7Z8PkCeJO4nLMBTljNDsIlp2pl4QmcexOG10X7e8C
DeBCObUTdOvciYKHjFTVNH5/et+ba/He2FYiW+TSOSf47OXuehsYy1KS8E3mkSwaNfXRQsRmDE3Z
LI8PTZABOV6+qF9K9cVQqvqMonVVTdwGQK4jnTjIXymNJbntmmK+OPwXQXIWk06c/0LYmKUjLLIr
19F0r6r0RDgUCmo8TXn1Fzc3PMaBABubAEMxvF2rUUevdRmtE48aPMq6HKkhD2Z8Ce3ujqtDGHUJ
+z/411ezLHf/RxGfl5cQXJ07s3OCnyWB1tpVbIKrbOwjn2v94oYdiAD13dZJ6N4j2q2545zCvn5C
Qb6dXsGuY5bB1msO8XAQS7IayOa61HaR4gETdeyzF9bKBOee4DoAYax4HO3hqXL/2u3n/J2uxxCZ
urDWmYxYnWa1oPniv7evTwcYZ5AT2ozORuA+3q6kgP6f6fGUJ39eDXLFA41bS8pC65eL1mFMyuer
cFcy3rPWnO4yxSRdF3gEF9KPkGWOgH1DU/CqqJl2e5hgrnx9klqXYO0JXFrEDfVECK6K46mI4H1V
hquSSW6M+pplCTDqXgdEcBvAdB09ffbgRye6eJ0HcnjeZa137XoyJRWSTatZsAnFi1zPBGYjsAHW
3XN69rhu/tswkysK7ahnD04YfimkXPuuXg/dUVHx0W1BqC0A9CjlQDTYhGCp3YGk2PIe7DIXZ94H
WZoFujhIwgwrh09DwBPWln6R+7U6FtP3W0N3Pa91MoOXr9SO88/1aRhQct04RSYmY6zbg3gGvFAN
n6pC3grQXjHYfPFLXhuX9NwmDlNn0YWbYG3rUfThCwTGPyFPbygkImuXFvgywAJPrFnkgS2ccA42
Iqhl6UrmrnilrUc9qPVh28vKsfJR30T8SpSDsPg1n++vYlwygSbsb0mFEgiJwvXd8yZWFaM6j1g6
Bst1raBfdm1uRbfT7It/J8YbPZDrcykwCCwrnjgRmHnmFnNeXOd7gSl3RwB33djYSyDnMKZJtCaQ
7bj6v7eB0OqE76oPKh7BapqDacBPPMQ7u/1GbGuJ+sfdVg+wFjCi8Ux8pl+1TQYoGtOxMKA7SQNv
J+rqNCnBgb1xbxjLQeMo6wPR7NxOWOgUyESpB+46ONJqoJaFg/tzx7D391uwgd3EZKH2Q+wiKWmV
fcRP3KuY2cwVRGeElx8rfij+qyU2d6pffrfMa96HC81jEh1hiofWxidKlrFQj3KnR5S9NTcvAGd0
4G19sMq/USzpiOr7Z+YA6azacv+zDtboIi10w+WZgDwEdYOF+nyqHiuLvZFzr9OYb0q99jTIyz3m
s7VQpfBQHDKFE9QFVCsPyWaKRAi4hESGfIp6WFzAbfOfEmyY2UxN9eDHc76H/MZZFHT0FoCjiTvj
Ig+E1r0tsgRbwwAmFgNrgJ9YvbpFpIE3wbYfaePKaXoleBfGHgX5iSBjuniNyBb+sdm7Jfrdsbg3
ripmdPmOxQlnO9jvibZqL2UVuS6g4ZZ7dqJKsnyJOTZQMRHRrZLoWwtF6HljSZpG/M7M2qKwerIn
QQaI34yuXIkU3G2DVbUAIPLhwA4fOKVnFsJsRG8zannGDrKH6EYoUsrVEtBa9kgU4WeTZg6V1iPj
44Vc95Dd37Tfa3CvaY9FXHcEtIgNfuKzBfra6Tu5SfQJsoJxeXVpTMndYweMQnvrS2tCWWiVzwWr
ziS6TT1fVWuY6h8tkHmjnIuHyi8IMK4A2kUITdqHEUe1NaUzn6GaSx6tWiteM5DIF2fFA5Y63fOG
GaRF9Cxhis0xTMficHJq8FPmHEvElTikKhpbt2hRfiCjliiC9danFjxDL7EtnFOExZvt5cNnGopW
kP27w4jvf4GsQn216DgYRG3ej7QUy1H8y1EZhGul+9MahaDhwP8CRH89RagIHZav1Ge90ZzfrV4j
kKTOqK2pBk4ppIkc1k6+2D2Je6fKcSFozQAYXpHiZFfc6GG4roIy6okLuVPfQhLKpF49HHVUSaVC
bnWQhyzNP2NFslLWfZkqTZ8aCZRMFs9mS+Vw6lKhfRncF+St5ent1z59fjYJxWxhafOh/kZZHYIC
P0vC2b3/9NLmC57fOi01Ztd6k/XFzuUUH9DJNHq4tT/qLLZKldSa9a3YvwLUXhECOGXPoCz4CZI5
TZ5wEWUL630jrWPRv89yTDTgMY+Sz6Q+5DFrvCuCyFwJozTDHTjJCLxuG9fAK9BfxT7Irpw8/SYh
Fg==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_4_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_4_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_4_1 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_4_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_4_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_4_1;

architecture STRUCTURE of design_4_c_shift_ram_4_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_4_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
