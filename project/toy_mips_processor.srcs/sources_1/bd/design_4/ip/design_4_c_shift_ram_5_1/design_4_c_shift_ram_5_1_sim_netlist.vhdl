-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_5_1 -prefix
--               design_4_c_shift_ram_5_1_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
y477AifPTz/hHNbiWQzIzjrdNeFSe9PEbfLHhKiKknuQKUzXH3mra83khnxgFpckY59HKZSsJqIK
Ysft1Lj4t0t1kh26922uwk/sQ3wKqLrNmMTzXuK29Dly20E7jevsuESP0kxHNIsoT3aGioatwo4q
U50wWWL3vDX6DYgE/xJrWZa/tp2BTo8o4dEugD5YcXKpDvi/7H4jlRhnqnHtXOFmgqBBx1FK6VcJ
go7OOPKYD9BUl2INGgDPu+JIs5Az3LBLp6ex4SF30lRBt9JISL7lDOsj+mRAlKS9jQcAdKrcsAQ7
azdvD/yHUV5MwF5j520SRpZh8e8jZnJa3zsJMvs/8sH4DpTIDwshSq+ZQbafEe6OCacxn4Bluar4
sUorH7TWjGyaPyjsNuMzXdNlGU2JXX1RCc4mSDWn1bD6LM64dY8CH4ke02muW8sBPcATetDylAPX
+lqj+pfujGOpN+fa3aFhnL858wcYx0l+fW1kW2okqMShbXn6UGIlslEYB0JXEBCzKFJDAA8wD7Oo
U/U0Lxi6EVxnHEN2pBZiNrikTyai4s9aYxZBnLuZy5WPjee0beWtNQrv4fn2mIiN99fB86J8SCcC
dpQSIGlqj+y6g1bdhE1vdhZvTHnfX3a0Mgu4C5CisKd7D4P4kFC+4UbQJPSKPjhJs4FmBoEC8aXs
C+AZ5eOEsOS+sGJE8oH8cf8E54mrYbzHs17q2Q8gwyyKqMPBXsp+J8h2aHUAfaYCg95p28vQh573
C+kDOHZX7kSAXaKOIrWy48k3TqLc2v3EOycX4KXswL2sylfeFrX/B8sGZ8dwvdcNeApDzkiC9670
yES7bRxKAYCyP7lfzpR75Bv8S7K+lQ1u1k5FdcuraJaxlKNohcWJ8G2TiGES+zp7lltyx1DMUw7d
5JLdKo33xdfHnpB9VzIOaHUrYItES/AXtlQrwEidwQKbwUGv+nO5mQ/w3tsCi3Uh29BvUiOgMfHn
0ld/BumIdIjkLA6WLqnk8Wb9McOV4km/0K3P5mK/tMrthUwxEXsEGhHtfwEYYZFIsVH7JJjXhO/h
6NUJDvwbofSudwSplMFOTIeeQ07xwei9LVQFvFEURMayYXBqiJXg+rIOr1kPhSPhkCrTesz7/xze
40ufKsQrznyTBKQZi8/Qbd9RQwzWI8IgHZ3VoTCzzx7+hMQmMJnoMZV+ntfdGN6+Vbvd0Q8Snq/g
7jzUv7GBhaBUQlSOyngEfMwpu2ajrLRC28xZn4mfxZP3bkfKPP4ghKSSk/R2IyxHyv+HbbPwU6Ll
HXtHaX4yFR9q2w4LEdcUVQ+xTlmAMxhAg+tXNf/GknoM2CL27zotHnBHOARiyrKsqRXqpEUMSlFe
psirtoDSdNm0D3iJyjFnsPbQlmhBs9OiNRWF6APmIsAo5bHwdOoSMvXiuPT/ijPvyrI2XMNNuSt1
rpPpiZxGtWMM4bqaBIdnW45KpyJysxEXqBwetl0CoxjIY8vHPOxwuSYTarSqoWwnNbSSjL6Vfq7e
m5SYUFl6a1cW+vMsrbwHVLTyKGsWSmxXzSa9j3GXLIUmmVOBaGHJxcNTob+hgd2pCkfIh8CvdPsA
X/h/kWqhzoN/mkz5j6/zY4bYc6oLl9KMnqKEFeRepbF1pduysUGjwdTWnCyamV416Or1ICYfvvhh
LIRh311yO3BvN4GI5Jc6Zwmaz8THn9KmoDPad6JbQyeayvJ4BUCoU457m7nZmjvVgvIt8qMEuNNl
zXt+UE3vhd99DQoO/8EIjg9032dgs7lTNIG4ejAU8i6R7AXgA/+y6hdVdot1XLu2rJLAS1ccaUMI
4OmT233IrF+5QDsZN4b1aDVbyMGiL5n1WuZnPEjPFH/NL4mv1MAKz+TRjcygNMIOpmA+qsJcEasc
hCXe2eDO7TWuvdNipOt+nOW6YXBH4HjkbVXtTGwHzyvEb8Cu0DoDwWKxfdZoqOfG39iay8ThVhr9
42VKy9HH1JhQNOFn8xUDbyiCuRexirZfGxRDdLHYrZ2XVwvCEwt+fmCMZ10a+9SSKDrtkmQScGtt
NOAtsvSQ3mqmZm8Az1uhp4F9LXkgZXht8StZ2PWsvXjHYmFo3wAGXw0u8DKFIWHkVRNTPx0xrpnY
HtHMooFMRulciJH6ezeHh53CPnjB11vaor/3s49/EsdoZl2dB/7HfZut0lY+0vk52SRjZO7WQzHy
DYeW9I2LVbMUD5w3y/4OWoYpQLnJ+7cZHnYNJyQjRuwIKC1mZW61/wDGL6sfBR0/8Ln6MKK+4mLV
H3cjznA9EGfYebseVqEB8I1Lk7Wt2/H+j1mSm3mO6mnYX8LwEzEmn43gn1V8/UfiTXhYmmpHh/4e
TunCFs43WWNMabaBEodseECNONsxxDvhD4Efu55x0f+FZd1i+4ilQX4nQICe0V+eBwXCTBnocQMk
ZwM8WbBJezm4yivyFMMH5RWqvLlV5nle1nE3yEhD8awuWB7GHNTtkagPXL6tEJYDHhDZhWV15XtQ
za5ElmTO1D09VmpG9oLT+XwbjNzv4jts32RRIAELVK9T1gsNIEuYA5UUT8z0SUxgvxEB6P54FaAs
Ak/V/THo720Nd49M5Ud8IFKe4U7xaK8GyoXalTn61GvEttLPMYW9s+225yPrhZ3WAx4l3qCjtPeX
eAzpX8ca6aTNzKMg3JfISNznS5dyu9kBVPViiPCF/OZOKlaLBulrkBS6tu/ycmbTwK13v3GHJA7F
G1LnHmoA5BzDUnB9c6CRYs+blShQCuU89mpcbb/BLj3VI+dg9cXF1i6aJPdWLxELG+Y4TYs3vaix
Pt0V8DmBTruq/7RitSj0NrXFF4cD7Ka3oTPXeBBmhg9nz8Al61wlG3gTMJo185seKd03UCc53kXP
y0mkijOT1NwTIQ+8YYo6j/EdhVIqnRxKXDHwBMfZK6ClGQ1uC6iyqtcJWu0NaCgTY0RhXCxsEoh5
64YG3DU+VZcYnVXAvhFBxqFXasjt3WGY9SiKe3CcJS+ZLNfQK7r0Z5XQ9mz+Eq5nLb4w13lUIU0N
tG3VwhqUX6p4pqXE1eM74EsUQggsYPR77NdN9dTD11wDHU4Zkm7+8sE3iRJB7M6/Ph/w3AdEeY4L
XaMda1IGf5aq6DaObkORLy2nXeQ1dI8hqz2lnekH4WyNMjD6xogNbM1NWGOsh2kMumWLVtdV583r
tPWRWI+r5M11tFtmUy75Hyn7d6uSFJsoyzYOhJDJGk5gnBGesfxT16hgj92Om13DGERKcAxGQOgF
V2crIDKJOT9oGU9cBXNu29/HRvQ2JUS5s1razptWI8hkcPXxs64zFzw5AGnnsZClb01bYSbNbmRw
06GXvNmbCKsSJfFVPBb6OuubR5U+ALqnhao9SefX4V8AlJbeWio/pJHFcY1hHT78m8wvW+/Znixn
8Jk/D/LEs8CUaKY7LIrkwHj7JMGOfYb1doi06lzmcvAgP22qzQnGtoAuRIwN3xBvYs1cNQohSwps
uyWrLK5pdU7fDC62RyGM74XS6ahp9JP2LS5oP6OYT6Tp+2Br3xUYuLIR/jiSRDaU1jNOKUk2Q95l
ycnlcFECveV188cyugRuEI6h8JJO1TJsq83Mf5/h7FDpnpH8nGfRwERhBr8lTTU0cwyoSq7xV+s4
2bMO+WFxIHku+pJ/Y81/6ifvHlnN+Fzsmy7lGwOXdl5G7Ft/mAzKbYscaOmbOMtKOQFSXSkdvcNL
LQMEwcLpiPhGz6c7UF6shzaYckMYfNUe8YYySVPRTdpOvJ3t9DLfufvF9cXa0Df9KoqzKuQWmyiy
dcJQRj+xYDkwGbt1O5V6CGQS/SO6q2m2iz3e3Zbf9/H6buFwPNl8V72LNUKVyQP/RscB32Od1in0
NwjkiqHgbjFdpuHRy2khCUkft/J7NbkIlujr80hndpKF6nWQ8ZjqXKpKRFHpUBzpI65/xitPKi6A
hyicKb1LJnkXE+ZDSLyKwE5r6o6EHSapE9GY8KiDKr9gxshNTQnUntVP3/C38z6qI5Yt3tA7d+Hu
MjRkGJs4yqLzo+s8QGSkLjnqEPRCT3Lf/A25KtfZCmvyKcqqT0Z/GvHwy4zLIMX8J2H4jPTrZaSO
v3KsUmkPgQ/N4KZ73bhyk+aQXZ7pTDeXjeKidNN+5tqYy42BVkawklTVyVADtvnZ+8YseleirUBw
ZgbZT0TT6kLZjwWsPNlqqq8iikjpH/u/5/LbdoDFrz1GkxYZ5u/WAiahaB6KFl1KJiN6h4fiai/G
32/pcMbVACIbXRDkU0iUQL6D2b6CGm2yj2HfXYs6kvbcBCTkqiF1L80DLgZma+KgRlPcB6WdQ5Cf
MtXFBtEO1B56pJ9TbOVINk1HFmP8ek9/Wt1DC462hQ1SScGoDfCMXgC/yx8tODFxH1AVhe7UcAqh
pQZAgdfLNeOyn+ns4ZmEBPB2HaY7HvBeEO3e9WVF2LNHjrtBLSpcShP5V+O78jsgly/QEb71D3cN
6ingzDONIwdi0BfY1x+Oij88qcW+fwjeIMvfhtswIxKfK+VWlaz1lbV6MiTrgOum9kJRIOiBYlAN
QEy+eFg+U0hCARayD5eEr4e/1mzw1DRk3IFE1GbJfOFcYMpZ6GXyG6Fha6AwKF6vP/bpSOxDr96a
fxM1jRRPsHXtLqGs+77kBT7IZukc/0G1KpvTm2D1/kfxjsLj24FQIFM/bYZ7pbkfNffXkEYfM2GW
8LfOkAH7zxAft/g+krR8ZU9//j0NZxF9I5sHyfuXVk9UZT/SzqohZxCv3DpQNhYau+K9hFoYR+64
e/nQjUuqI392FcQv3DKK2RE0BNav2ivHiNUnlH5LW8ivGGARO0jIwRoNAl0LOPkGZIlbhpnXpPeM
1OeEeLfhb0BiYUggSP5y59YNqQOQev4MJYw/zUVvrdS5ym/MR5cZz03GAgCgPNPY1a/O4GAmhlOF
wG7FhU38erKS7f9EDT6h7EEgJykIsH5hbUM4vpUvWk0U71DbxBsc6d0hSM0pszKNjhOD4d446qoQ
AhH5P7lMPPRpQYIxDWV9Y05NvTbU1RtX0CZS+6OF22x5ArmLrxqY9LWIyXgbCplA5atMK4VJKTlc
v5fYdd6aZGwKzpSiQJ3Fo9Ez2b8mfG2HyLZNTJDyoWgouvuXRa4iFFPXaCZyfTUzZOxqMeIIiMR/
+pdxv2Ck2ZrZ6978opPC/t2nZbO82VU+II9+ABEMTXiDgs8PCKVz3k0UzirH5/Uj9rDiFCrDFXSt
xr9H4K7/udhl1K6c48qtfCXjzrobrt9mdULS6tx+3RNrA0NPGJruES5WHCFzumNc4XsgWiM37Apn
E0aBnZrTDwR/1TRvVD0XDtPL9qSSS4Z9AOLzaXHtR6Gv0CEgOom7YQ1Lb5AAsLeZIkWzukXQyGsS
6yGd/XmbOeBoAreGsnjEyfflkIIQ6Kjou5dBfQ9CJyYvJbq48b840OhTcsIPbPbYnzx/1tCo9dPm
srlLeACQBgbNCTPjkufodkn5ewRtRcitbHpvmKlm5rvpRqrNhKNd/cBMpIu9V5ZxTuh8Jv7WzBH0
6Y9YOQqj9OVvMFkCzTY13n+AcDDKRUIHGefR8MVFl3Bmkshj2RnoStcG/Q//ycZFkFylagFiUVn+
xMc7ZftE8ki9UM0fJhIutXVGEEmoFh/t/jjsQXyBWoL4VNvbHTKqZFamMSyd0jjv0xEf2tDrMvzi
FAo/vAmUMWPj3AZ7dHXeIK889aJixpidbibDG5b2v6dLeNrwegCZTFU26Cmgv6OCDsjvRCLHlNEA
BTwkva/DMoh1kykaqqjrgQoHk35BFfAukLQyhUkruA7kBIHUnXFtl192kQjNjB3LcTu3/QkSmBi1
xPpfZUwFqzAGH7JmHwupTqFZDDTGiLkW0BmashIbTaRL0mmM8nZ3Zo8muWwNibw9k7m0T1k36IR2
gGtFPrVSiogE8qek8PYpmKRbPJ630keTK1gG2Gcyn0YBqQj1dqXYAEuk0gby1eavuq+sj+EpmK3i
bAP3hABgSCuDRK4vuZNIPryWBd9G2yLyp4FIGeveNo0DHvmZmXePXHAzptzZWc/Kadrrcx3qDZtB
MdPmLQOlCkokZFUEa6VJYIusGNYMhNSHzPkgDNK58g/uXju4AJuzqO2HIcf+u+oWUDz/jekfb6OO
8wOvl6V257JDuECgHcpzGtbUyQ7b0ntOTHdtyOAS2yFMv1PAWRtRs5aTKCtZ5TETu5RIWSClSA4j
5UQDidQ6mc0AnCRH4i56In7SiwGKs1fXi9ldm12++FPXOlCJVQyBN/4lVcU6mL8NbmOdQmFy2uaP
yHcyornSoPWx47fI+G61jgffU3T9ViKJnI9v+rJeUTX/eARMrFaUlabitRruKF6kmNam1/SZQWWf
OzgKEkU3OAUzM7aQTPQAv6oc2ghbM3T0nXZ6fC0NbevLa/Q40a3BqiRUjmkMhLKBfOx3+IhKrrhi
b1Ra8STxwgQC/SN/HWrkNuvSzcvwaZv5PVY2CKvHXFgsRAVt4in4u4kSNCeu9grtifc8TdUz1grF
Gtv9cV8jxpraDt4YJnStgmGTPUl947wHSAXs+K7zB8DcOd3Irq9mOno0DlB4qR5nnHss1cD5oR72
6gEfkEM4vVV8rWaC9CSXoFwuk/RQxvrEKE2ie24sLbj6x+KAIYnzg1yFN6nNniP/vZcWMPh+o8iP
JT3QsWagxm3ZSs0j9AH5hV9gBA5JELUwZ62h/diPzOVIko0jjJgZWeMjMGdrj76YsRC5lxUkoW53
j3YFN3oURqWUesotQjxAvFVWBmISmpSSWfREqxFdqxIR8TPTmRoVz8Tq6cltHDGuTBzHka0NVztm
lnDRHxObkbhbiYWIOZnuzJvTD1lzT9tam2Be7UNIPWq/DqIilb2/3vuOvGMu/BSkqPnY932AHPk1
SdT/0kkJ10cBsogfSYwKghtNDBsvBSyKcEANfBwVk7FUAASiKIHVrCRXVl8wHVz32GnjDgk7rQHb
SEENXl5KAsQK3mq8Uqzk7gUvd2QTXODexBeVKsklSO2fkiV6aQ1oPVuYnSthuGN+WN16r4la2GIA
zjAqmNZyW/HBZd64pZ0CqUz1vRUxIWVjyNG4Am+OV5uZN0TbprQPxuvTrQOV+9ndVaIWAgsBNiSI
AsoNiSgNU8+cQtXbbzzeRTZQCCD1vjoTpnYgLHH9X9RexBMC9gKzvnISlaa+1QK6PLwCROMpTIjZ
Wsnhli5ykyJXeWvaLEckKepLSDPdwOCLPeVASvM3rj+p/oTKia3LN8k5KbziRj6JhSVIumjybhN0
hTVVYYl6YN0WU7te2Vs0QDWInqVBqHLcWkWHc0U2W8QbBdANl9i0QH0RVWMQzlM92MAbXZlaASkA
gPtqN3j9ujdA8EXnSIFZ4O/6RF5vUjw3YjbqOX1cft54hlTVG3HAPmJy2Zto8xZNUJgusYEUNiRx
H1BMBcdkHjDqLYaDRIYGj9lkf4RSQwiDDCrQqvD+wKffF0kpik3tXVnE0oHw2ES/YjMYdTp44M1k
PjL4rtVXzU0OOenqnm580RKl9GQeACDRKD9v/6J6E9/dwwiwXQhAhMqUQqFSkZQGdRonzzNK5DNq
2paA/JIlCP2z4INcHbNU4HLkaqkQci/ACDxGsYH4GL//+Htsxu9iTe/m+NoO0QFyqlc++5BfRcYc
cptcbGlzn6sQQ9N7O5Rr9Egx7FHHRwGi1VwH10DY2hxrUs1kniKKzY7JAt4q/u2tpGmqTY3qhoVd
QVrTJBT2HOf8BgDN8wC0mK3QlZwv/N79b1BUXQ1s2PXCcWm56EAebu5ayKozJvfPlUW2HUKU5zJG
/4o+WlmZDF53WahIQ0KedQfjEubUyVcIzTXznxsLD3TXgdDAkz34G4qaj9mmrb83IIApBONfkzzF
xYN49KK8Y60IMSsPctGXswqapq2sVySzaMrJpYtdrLqsLsV5V2IxRMMw6v+TN5s0k61kd4GbA0Hp
MTdlp4CvZ8loIlLSBk0rOt7e9nxms7SMlKZOy6dNMEOY2REmvD6ugzA2hh+nZblqU/+gJnJ7TKMl
/+11vJbNDjswNvDfL93ZGZ/4WuNj7DKw2tNwp+SOvYhYctK+S6LrBpKRl61nLjvx+yxy8n1hyP73
NMgHDa7TH7EUcgoVy8soiSPjGiB0RSP5U7LSD75BsNbeT/NJqWalbwaxpz1Vp2CwGopVaKz+nB9N
y3yGUgzUzFyyy104r6JwdDhY0mZh9PEuneg/0WzzcdZKPru9dHV3Xmwjqn6rsVtspq2hbeu61oVk
AF6X/T57BTbiF7RytkigQXXj7qDvYz0Ua32Vmj4ej1ALManwXqzdGgBp14wXhkxEiGRZh324ZJ2I
BOa5fJtTux9KesKOfZ6Gh59A9eJvs3cX1zOOUFmI8a6o0aUzfRU1JD3AkUa4xfCs0/sLTRiaVWcT
svtdOipUJ+ErD4IAGw1r2hXKzrFvqN8VG7zxrqcCeJWGNRVoB59BaJML70KfmdbBmhlzU/9rUc+D
umwwagrhzhsmbG80iuTtD6dJrhT8lcfYWVdWD6cp6J+sOX6svENIwLlljWEHasmkMqIL9haoW6iJ
M8OQ5EYvovPrjUluWppZrtMStXdBxcXRCqQrQMYdtfYLu3guC0woqv1kabujdEruGgglQrBD97/2
nEojjUAy1bwDPS4eugRGdHokhhgMPqswDBs/ieC2n+Euhx7MaNiSUgnPp3DWx3swcNCNXqJ8CDdV
2J8ZFdW4099T3VcS7m/H3b1PDsO73AS77omeN/zylGcSHKgQLFbJkO633339xiIrKdWltJNH6c5m
JJduaN/qbFGT2Y0b73GCvJ/xDh0aOz/RT38MOgRAU6O7TzbQCtz1pNiRo40z/w2pYcRQj1pN48dv
y3NQ+tAlZJbr3kwuZMN5MtZPxCjSMoo0fW1SqlOmLeJ6A/cLNE6mihMVAUEXRn3wtt3IzOhlB353
PeybaR2Mdwl94nLOo7JTaSlt1SeGC1ih+rWaO4j3UoowcGj3dSfim86pzDIvHTht57AOAnRxm54+
ZEliWk0oVVgIFGTizv2LDd0yt21KPmufSxY1ujpCLA5S9ALFwwHkNZDlkQ3wApht8XvA9J73bk0F
pSbiyro64hhOqKGbCrVmrFk/mW8G/cD8L4bB4ViMyc4LkVbwslna6LpKBTikqZz/NuAgZK5swl57
wa9OkEWVSc0PyPbNXlcmQetgSFhCxjGTgKgKwV9ZLXBF8aAPTWoJgP04jsA+yrlWR69uuaCEr+Ys
HyTYjZy7h5KtsU80qdHXU6Oxpn3NBUD9VYqBtvmKq9Q8S0CWVPcJHAV3fbKNPZEbijyc+fFAo7vQ
kOEqz+TXbj7DL5oTGM/5QjR8ZtaVC8bszHIhFrMmv8z3bJ+NmwLqX6xJTW01+3ojotD2mSG5/Fbl
P+HGbh6zIufDjTG4TOq5Z7W8hN5sSsyb4gIeqG1PXxPpQ/DOP7vuya2CGraS/BXK/sWWkvPHHXXA
nKempx9nCcuT1fwwLK1mjAO2bWwmJCo0jbwZhUTGxTN48XPxHhYTqdOcXMeZhT/WeyYDdAJL1EN3
JoVoSSVPPhE7PCPvL789oNx7KI1aQyW8+NkB2ngWzwT0q7MbIKdu/PvhF6Q5vjVBu27suGeXWiwi
QvyTAOhSrrVRvvaenDP6LJKzNgrw904vY6s7dv1v1zxVX/LTpzjnKwyV+cpJ5RsmOl6S7c/ZpmC+
XtoOWiHXQ+cx8FWkKiObfAskDlAYusMmq2sJji12IPFC7F4rxup0pf1RaAdfKC/uQdrujHs8FlbS
Ngat1aEJXNaZyz7ZCRz3DxKhDEW5P1duDi3+9Nv2FVPdO2cuqhQbG+1BaCgO4Msz+RQIOayjl7uo
WpOH1fD7NUII0gsbr8MTQzXVybZW3x7CektXE5lz+VD++TwoKpHThqHw8TFcRdqRXhathe43gg+F
smEiTn4WrgaiGhGEAQp8pVoWmQhgVX1cta75vBMYr97TJIc+0HRKcoYj3YFXnOE0qXKUJAJF96M8
A/SVmJ1NaAmUkZW6JoYD5J/NxtMeDlViSTThNeoXx7Rz74tpRQ4fQ/nHOrRwHVPPAvAZHeTLrbf4
UMzYEjAux6WgTyAnFN+jP/Qeytppdq3Y2NOPagcxNjQS4nzEeOb/PR8NXhgY0yjnkU8KB+oqrWwX
Ix7T/RDpLTc/ZmNwDYCAFTJ3QJ6vUEFLk/dnhV+V9Nc//8sGuGkCnWfI+HCIbCLjV4VEA2Qm0BDO
q24d9ytI0MWClwjllWyf2urVXTLBadPDW41U0ixj8Ba2gpSHfP3squdplSRv3teVXAUuS2m1b4Hc
hBajHCMxTtd3GCJ6TPdv3L1oKTLHO9YyhQz0EwX/QF+sU+c/rFYCfukWXC95itpu3+xtPEO6AR7i
HHzYCFx85hmrPjxafaqTIJCx8qFybfcifRvk5cdWqbj3AkPATdiFu2XSyPyh/oDGiCwFUkw3Pc+S
KaFT9BqFiqhmKNYHW1IaD88Au0dqa2CNOpHKXcGS73ksz45yZjWynlMma3vzYMl/6sTsMqiyYpZa
gs00m4RqlcJZucCDLqvkvM3ca2FTa4NXrNPVaFdstENjz1xTOt8MigmmdDQLWIc9yGJjlsThx1MZ
KTYY1eJlFsr8DdfZ5RuyV9VDX0rRRSNtxoNzDtWMr7QmPrGyzqUe3X6qBk6Y+daw14pqzBXLRfkE
/No1T87BdHIVPAOTVTi+AGmLl27NXQ1h3ROazw6lXb/yaTMLIdHR8kgauiNuvAuOaRI3WbRqVcEM
/Bz8Kxr8OENv9fvbeEo9wyIq9ZCfryTUCBQLLU4c332T6kVOtXT20G2n4mX8YAaWqDs26ylSgnn6
4LCd72Ph9dIy0l4Smy3sIH4oLnK+iz3yHy9tJu32Y7r/7x/9IyOt0t6HIM5/1mFI2x+kOnbdlmZE
2TMPUH2edcnWr0hgK2/RKTxi6V1iS3hibMMb7GYGbniXgcU0HQ+PMmMB7bPfcLQncUXIRG9yBIV3
woZRaNfvtG+axHsnCAz/hfWRJwK1WtPqNff0zPn2NLMef+9sKqzkUX48jwNF+KrTzyg8TDBXePrL
ymjXVnF0sZ53bcDSRGpgSmEhOFqG/VuzjSwMIkzLiE2FqmuPYv1gHAj0ACC5z0H2LcqJpRD326QA
oMbpPw3U9l1KZN5FZYvtYAP15EFkeCS1oXvIN0MHTzaWF5IMckOYs49chCSoPApAggm4vT8yEiCk
8y+LkNFkAWLpjN5uO4HW2VNUp6B8RKoFhwq6QeJdmQSdEYWL7UNpuOxh+au6B4bfilMsTRZjtWYi
A454JRSIbfAbCw8lzsQrZDBir/DYOZX9MoWadHSM9ZdrjBF6+u8ujEgDKppOfJL7pH0XBvg2myCh
HNWR7cUDTCnjwlL17LNp9OYYZp78NAAA2lr8wSqQWFmrW5jAj6YpSQ+ed2G4Th6Q8LUak3INbYe+
r8L4m6AaU0y9r+rg/752PWaIOxjiYmcKVfL9GPMXop05vd/HQhiUgpxqQ+Jju+lUNdxGSejMK77d
kvBm1hY5WbAMmFtyUsP4AAA9S6aBDZgO1rjg1ZNjQ/KPH6mmnK3qJ+RgpCTeIJ/OZe+Jrn6qRK8T
qADoMRm1JBXks+K0CKsLXL4K5dZQHcnddwGQKTNYY2aj+TlXfXHJROo3s8BbMCvV40U6s1puvLCv
HKrbKkmEgHnJl/CDY2c82DhgQ+0Qq4cCRNGqa98O4FFiqFix3YbDs00UrLwIM9FfRwEg7NeZleC1
oE7GguxN4P5zeFcRmxZh+CMxxCahINHW6HCt95x4BJLJ1WbPr0Tl4jSw5JSMHs4NUg7vH4uxXneD
R4sjoARDO3xNDrrMaum+ZPJ6bQv7trkqF54vVlRca5LZhFZIix7t//WiufPMidoX5xdps00BIf3o
1WEjug9XJtsaMvuSnZp91tLkIsfAhcpb+bOzz+8rY4Tda8dZkypWaggFlANn3WUaGVU84kx64jmY
dkyxanzhndyL9G2FZPR2rNm2prELBgLKACsLh15HAljNpjcycOHgZsqx0MqTZYdXI6pjEBkjsdZ0
Om+zZLOFE5+gnf/9DOxjRbMGx5r1dMxnQSueXreTVszKAx4cGOzij8xxgzC2K0MDuccuEfQIVifK
p82uB65AUFdtqVNXnHBBm9u2qfemi5Kv9F8/2jrPitqVAebfxHutRxGH1NQhXcTrNo9S6wWg4TYI
EeYEuz5qIfPt6uUcwyTW0kX++SsArJC03R7ABKDDNU1c1QRj++51PEOTUXHJWpmaCfnYFwpyRUk3
iBemnS2xFo5JQwU0ptZhfbZxC0ASfOlJAM2++lmSXGfk3rxOHJKoNSHYgIW6kH0J674qTgxaSJba
TBUf9mteIWDtzgT9CRJgtgZphWd+imofKD2kFIV6tNXmVjcgAlVCJ/R5igVAQSiVU9IumHlzrSLX
GllYNmRgl4uRggpE+xWkxckuqkp3Ym/liGChflMYXrWQRHcloaQwMEPVbpcIabWkxNhPrYiVxxoT
JgBo2uhQyspCX66ULUPhzS7Wl6qk0vGdX8BaA6zl1scvks95moZnQBAemIKfz1Jrq1u1Tx4Ifmy6
hXComW0+xMV4t1ZqEBm+T159C1mXe/YNCHuC6K1403tltbYnv1cKP/FpXmB6UgKbxpN5caP7FGWV
t77SV2NEw8cYlLLWPJlxuGwFAHe7HWLDT8dHBmDQEoMsNvzJ0LBe9WcAMARkzMcyx1YzfRGR6JH0
fy9Ot2HHRhu9ZNs/L1s6bIUaKW63sC/r4nBxVLOnVh6nOyuQBswVA9k1UXkJ8Rqe31CIwizO4lrf
6XRwoqhURUJFW40TURvZdNR9MKIBwxDZxWVentx46lwjPcHN2Q/ZNMr8brlHyL7sf13+jb7oN30p
eMDHokuoVKzEuSPLgs+vxbYKi0ChDODEpua8aNo0r8lam6YeJeJY7A8PeAVqM4hvYudKrX1LEitI
MNH2OrcfqDNQxASe5xQQ7+lYawVwUadHDBXEUbWHngVz2MsDI7G+L8rmOA7ZNBZwQ5h+xliVJITm
05nEQukfDobEQp5TTX9vUbsQgOf+wPNBNoNAIIBjN9N5KeLGMgf21UIaCq0RCHUc7eAJAmCAlnOe
0rElnwbRxfdDqc4feSFG3jJa/SZrBPvnKwl5dPqZGuxAFYL0GDjQ9Fmtgq/FaltZTVZx1OZ0CsFE
9oZN4EJjDMINsPDDO3xxzW7H0w0Xhxt2aIkFvg0y/K13Jt575VjPMM6qTwQyZoC29ONGQ2M2ZZGD
X8BDjWR9bZ9phrJt3pWkQC3fwZ3wocKJgBx0ze3+4NjE0DXaFAB/H4T3DGvMQ0tIW4+65vwB44VJ
8cJxaDLbg9ydA0KwkLZeFe/ooT42ntUMXL8CusQ8B5SQc+osEXDHeBPXgf/crZqiJQygUyg7er2g
bFk3oSdplrdtZTZvD7l0kko1sz66US+7sHJjOA0kV4Ilnl3L9geaiA0ulGBdFwHSmMBmYfOaB1+P
3OhU4k8vQQlWBIiu5BPj2h/IQ7jLm664wXT2p8XQ2ij8DyQ97BVW9ljq1JAgTZ0wD3LpAKdvGlF0
QRZ2GXp+Avl0qGtgOGkAXRi91EOYKzYHk+apODk+pQwi8+OGOElBDyXzEjBPOZwnb6+CjyHFRlmn
Ra9xV7mPDOCIo/Wj0GiszLSIpDCVLlqmDAvpg3B/g7Jc8LwYKer+KrmGi8QaGtypCrZl/LaHgvOB
NkdYHQXy6c+NiqDe91O89yNGdm+QHJ9rM2cBFBA9JFfkI1DV/8R38Qp8ZJSbBc37SACh7a9OdVIu
Kl7V+xuKAgyisxO4hcKN8sAGFftAxRt95kli0Xss3oFAEdtQO2cEtN8MjAayONsc7p2cpr98Tpi9
qGaY5mPdOzpXkWu1w6KyZTAI4pPCV+28CYHQ6huVqCT3fWke3HQ7p9cB9aDKp/olrQL+OWAcphFP
mq9/rbHBXoqViTUhWxozft87omeIlqWdhO5mlVqXocHKzJerTu3DTouRJEL8ZBT0uPOA8gPNgJr1
WCVsBYgzIEsFJEzdGicSDO+qzV7YpSpU6o0dlUhQ55e/eNpME3+3CLiz50Qr+jsHCNBYDkZRdBg9
IP4+MNRbbQJFOt54xxibIRhi6RowTW4sOQd+q8PRj+06aJq0kKbTYO2ZdY02WtQhBbw4CHnnP4Ih
rm551HRXWwmvlguTQ+QxBmnHWh7+qq8+JsHP0AmunSrBL2jxOJHJEELbastqQu2ILxZvrVBaguGt
tu9F835IW2GoV/HTHmGZpS6H+bdpu9here4rJHP2pyPvLMT6FCwgcNVjuT/7+BNOjKKuv1lDyZUP
2GUiTsu1LOsRvIvmbugpUpkImZ/hAsJWJ/vq2ROUHCc+8NqZy4EY7gHCppAi1xf3TiNwuBLPVOuk
XkLcxhzzyhZrYNEpI4C1jQOJT60Vq+n5XuD1aLb9wLrHD0jLATz1huqDFQjuvEFiLo+f+pRZ03Q3
f9/vqkvJ14ipnA2lNLlvqbeKRX8k9Afnjv35Aj8xeYllxfwou2YiniePtKHbZOH0/Ljyv5t6sttI
bqzMTOupvyECDEEExMN3ZqtBaxgDPwm32L6XI+XBBGbgJJqV1AMgcCai9BbJOCa/3iRvisH5OD24
1jmTMs1Nro3RW0IjOMCxgMrksiYSS8QV3OkoAW2pe+Xl4rUayKmOiISAI+yeD7WmxJDkp/7FAAwZ
Cqks2ahMI3QNFEO93c0qfd0Sf6NHf+WrL5BLtHiLuxQbaupej1UIShvaPRqgdJwJxB++tA5iHbyX
eYvqFmVtV9HxGbsAhSsQXsxUPePWrt95ur9VQWtxIm5GkakvOxPy9HDtmmXluFZg/XPCq4NlUsQN
UPYw0Xl9wp8qjHipXsXkvYO8uBgHqcdtBvjwjolVg0hDpnydfQyT3rdlpq1ujmNxcgA/AhYT25WC
z/NMY54HN2hboqBl1IZW2t0vlLRGBo5ed1nmyXDLvdm/gEpzmLdRijn8H31qD+L+HYpAfI8q5zLM
HcXj1FK0JTM+r+mZ6A4VWkvcaWlNF2zBgMRUYiygOoEi0lthmOHDW3mpZRHYkPDJfVbn0WFCy5U0
A/w7dqor5G1ivFKXvUfOksVpvEY4jxEDetMBV5lrygYwIcdhu+H7BzUETy/Hjp29Cu2hI0veR6nO
b/L+Fj1YBnDKMI6KiOJgdil3pflgAzPVLFhjHG2ztSobt5nHG1Im5ZIJmbMPIh0i0T4XBr9px5f8
4pLrCWRjLCXSQnc87mg7YpVh3fida3gDvEoGCXBHjGfBjdiEs99bDvYd60bh1+r8KcInX7L6citx
DC+DH1m5kWxA9WhiZc6zT/kZQwN1+DZrD4SqDi0b3a41AgLNcBMzYaStuTV3PD31GuGVYQh+pIoF
0SiAkU5ogTZAXko9YIlABk2ogxr2Rb44wiCguKEpHHnbsiU/wIfO+hzKqICy6ifkF+uJ67ZEvei3
sYKsu9sCDVu3q/PspA4mE4Az3xrIIt0YMxQnRI8rFhJeppHV03zn023znqjGZ1qpJY122QSecL91
NPluMwG0PH9Q6rz0kiPcgTiOH774mXek8cP9fBqFwPgIUa17SfHizzMDollRO1CDafMiGArXj28M
K6jo2H6iW/sQ1x9FCmmXm5NA55FqbDMVjGRZs5OxuSIxLwx+agYud78THAgxjDd9otF5gDTuzjQi
XwO86xwdtsLQT4BH4Lm7YVVkBEO53eZAMsQ3sfn18ueoS4fESlXqpEVN9P9YhWcfxNio8EaX/e04
RQ8C03VV229B51uRVWcbs7ZPXGKsOx3YJ9Y8+k/p4k1sPpCwsnCtkB2JWHu2lMqscCIzgh4D3JcV
BwGarGAyxtKreQxWbXsJYPTNvEmWkfNxgT5D2EWtvstaToJ3BJXSM8AcE2c58GjkX7zCmyUTa8Re
rfsypNEMeG6RMRpF952C4Lf7GGfYndNN6zFeAd5cOcHqKQNJ/RzYNndmbZFz1qFkOuY2GV48pkvJ
NkaUhMcws+mtQA/BGivn20RfnxHsqK4WGrTHxn3RLzHToScW943rJFC8jzeVdbV2ujvzQTvx4mpV
bVpSf8BwEvHzT6krQjaUqGyG8AV01LZwpG4H4v9FB7mo8kAkSEotsoXTCNjSIhJYrhkpauSYfyxc
R4dHTlqYTaovUHWBvGKgnIjlnjCVxu368IC0h9pvMbfptMSWeVJagTjez0WdAAplbqscA/PnVwi7
tc8WguBQLqOzsz5ebhSwQ96TdMCCP1AeE58cEh87f1uOzmTKEqg1Fz5MXF4PdCpFdCq7zMKHNQno
T+dhupsHQZUktPc3sYVWFbi7PwdxlBCccQinl6e/WVLCIDdSdSy5PNojMrHlReG3TeBMoHUvSzsv
efbHFvZ0QhAATYx2e5oNFl3nS0nX6TsiR/UXuptcHz6cOMrhONQhfpYsGQ/lcnleSh0vgJeOj4XL
Q4zo0Q0OcrFvhacTkQj9UEvzBuerD9zcah+M/aNnyouiMtm/NoRzXwrS5jeANwpydT7eM09aPqwi
Wr7vyAwjnQ0togOVUBrefUI9NiUsrgQl2Pb8oNgD1hR/IHid7TlB1yoGfJ4x9jMz47ljXHiUlR2P
heo5bJg2s2QgysU+vwS43jrfINmAmo/KUNaXITjAjgLa2M70PH3XCsWPWrQl28i0fkAkLHcQQJ7Z
R708jyR7bSeKgGhaOoC6ILaxlNZKn9b5/N53Ryug50lCcCW6n2YDuiVNJVJ5siBwNsuzRt4F3H0+
CavdFLvf6CesZVtUE6RuIhmMmCRO0SBY+ZbQzRzIbEs4fRbx/JrH/SxA901QTSzrxYQDEDuxdnlc
4jYNeZj9u6pPreHXfODj43/LY1FlPOy7hNgDuHXK/dZh283IBEF1alV2AmeFr3OSWjOsnkj6tRV3
SdYyzpQbMKp6unLoLGidwLBn9w9/l1iBYm81EMtuI90jCqO57hfQ6Vxp5tjdX4kAoeCGHKmlSI8M
Y4KzdyPc8ILKNwKI+b6xJl1oYiyMFgIPtIP3DExi3MgpVzFo0SRnYVtpJiTl9wXzNSWzQLYmbeh8
AGw1HwHwBvjN63T4EVq7pX1NG3iksXp+ePiJJ7Vty12CRT9/gt7O3Oy+rsk3q5hbxXSRDaZiUVa5
yqbmM/tieBmziSe3tL36wICKs+egbabe4tDUEtFqZ06Q3pzoP57J/9moPPgG1KVszUEeI3X01p1u
zDkCFpItJWRYa/vNVXvPr2/o8yy3jYUTHdivODHwJ1I85np+VRYw4shRGb3/alVPZpGKPcBCQicy
UzJs8fbvZNXM5Fv8asUIvta6YpFI38pvj0SS8sIrIBc3ZPJvg6eG4hlSNikBdbvAOpBXPHhzFVuF
8TjpUl9p1F96YyXrCsyHIWFhSFwUVRjHIMxVFxpWV5D9QZ0Ix26X/arXkXPPVQMLcsj1R6lghyH5
ASGZayKsVHcyns7msx3Za2hxM5dc071xl399DnWGJFkBZlWmVahjRD2lh5Y4bn9VkSWPs1CqIZel
kw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_5_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_5_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_5_1 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_5_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_5_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_5_1;

architecture STRUCTURE of design_4_c_shift_ram_5_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_5_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
