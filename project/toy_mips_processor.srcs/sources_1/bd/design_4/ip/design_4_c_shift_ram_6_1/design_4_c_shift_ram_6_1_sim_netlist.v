// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:03:51 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_6_1 -prefix
//               design_4_c_shift_ram_6_1_ design_3_c_shift_ram_4_1_sim_netlist.v
// Design      : design_3_c_shift_ram_4_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_4_1,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_6_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [2:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 3} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 3}" *) output [2:0]Q;

  wire CLK;
  wire [2:0]D;
  wire [2:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "3" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "000" *) (* C_DEFAULT_DATA = "000" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "3" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [2:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [2:0]Q;

  wire CLK;
  wire [2:0]D;
  wire [2:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "000" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "000" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "3" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "000" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_6_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
OJG+snRQrl8bPVVJsBeO3lMkqs7RuZdrH06IOzPkd7XIW22m2avpetfGCxaTFF/bJJap5+R9OECB
TbjkjYdLe2z6eBetcFKoUkyj6EgAHYt4fszO+NeG8/j+PeRR+v5w0OXGps/gezW9a3SOYgb8h2ex
sScrrumd9MAkCw5LPokpLpUyLBPpB+r89I+cVHcNnU05zhYjZ7V/oC3aaZnWEoQ+38eVChF6Oqkx
fLANG1FFS1xRZfTmQcojUy9f1j1fehnKIOybpx+PoKiczZ+V/XAEME4kg9iZtFRCs10U+YV+o29N
GPNASVXjD1UOtfBoFowlyHk1O8btktuYxby0EA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
1t+ZlWiensqLQW+nnLr4TC4zZH3rSPihQFf8IZFzUlN+dRGpuTLcXVewT87hgHl7dlVi29ktYIlK
llnM1DS3dxFoDric0zu6ThUu5WvZzjE5Cuc41z+BgSo8NQr1I8nS06Q5NThmchXGmA941sP4U7rI
NMCqcci2IqMRdkuQ1TnfNIw1cre5Yi3e469q7PFy7gvvht8qtbwlQccCN16JqfLbNGaNlpGA2Y9Z
53v80cp91Wnq5ePOEXJeEz/Odj0PekFU3zoraTeQGCEwkzFxRMK59IS7iuJt+FDwzTps2/skUBvH
0wCw4OmaaR53ixM+mCdXsuJJiN+2eHfcK0gokw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 5392)
`pragma protect data_block
X012MP6UXOrw/AJ1pEj04wlrGoEO8KHGhusTgm80J5BlHARgo4eGftNUXWqoTq6jnJ8ptwdQ0mzJ
FJ2J/7fdjk5yBkG8ojSfVEsRXjZSC3g8nGfLPhD/mVkwWlbBRddfn7sPny8alj3onhERowpZT0TO
YOwfPJCSoz6OP6RFQWEVw1tiXswU8p53P0hfPSaQKgVpe9CblmIkChLrg1sHRgefQSV1zO6KroLl
z98V043gSJ683ItpEqhFQQjymKqQZ7L3xGpJIFNEkLmAIPPmI5DlzJjoELZeB3tw44qRO5EmBnGE
tSRbe0UKCyvzGax6r17WG/GBUpzRYEIjIrE8KSxUDhwQyq0/+rscQZOw6jRd8+r987mPEWtHOONp
YJyI4bITIuP+An864RY0skNCnERoHi2Oe2S2w7QYh+Wa7qdhlNZ+YpGSP+73H8xDy96/5BZztIIH
R7x2m388KbOMDCgx74dPN1Lr/UQIB6gtf2ekK/+FsUQ8IknRGDXpcmphjpaju2lthyBUF2zi5/e/
E617tDmILxByfDiur0JJyJyCCD7XzhhUfOOIuabearsWoCiT/bOPqOSWHp2kaUjooGGe+yNKYsUL
ruKKCV7Tz1SMR0NSI6+adR1TSlexzUlZ24WlGh89hcl9a2TPmfITxwAx6pFMDYwSPtMa+FI7XwwC
3E0h3eMIWU4H13cFxCFA7yVUbf8S4jg8kqzL2OAI0qUTlDdjPHjFJHBrjrnhXHW0iFhQuXsw93Vk
tCOcz6tSqUb9J5xjf89FfXvWNVzvUcUWYrWSUoedN7VWKltxU3iW+bZQ0IK6WItAGN3ChH4wHjP0
k+pmdLHUGNWBTardVBJDNLKIidEdbVfUMQrf9mSSwc+251kBZNujmwQinYVhgn2U0duGWzdvYbrT
v9sfbnqeVifkTl2T9R8iOOzO/AOvdCpEiz2nQUY3FmNTGo5aiQN3AoKQoNT5GmEng0AtsbwcixG4
DKNqfe9iAXkk9H2D4Ff6J4UQrhIHpoBb3uBuRRKv6HU0ClR0BRPNJGnW47kHAHAUcsuEwtBx/zKO
YIdhoPd2SvNfn9qQCylv+33tFXRxvbQiyrtebjemOUXrwCmLrOaZzx1blatKIS6AjgfbmE4+YopJ
cHkVJv0LUE6uZrnkbGxxQGhptZThXAQZIOQ6Ro3R5d5Vb5X9uvczmX3RBpA2LzTA9qAppJksMR7t
ZttMMiiyCUkM81fddVQdD9rUJI+mU3+KRcAh7UvaaqXPQIa0hO7tOr7hISUCHJ0G25tdTnk795cM
r7WywtCWNuVY2YamOHdI3KOen9bu9swV7WPCJKr5u2yFvdCvJxs29TMokkLbu5E5SLOObZSaAVGS
7Fv2f+mhkjCu82oKo9bnJe9ehMvZKK0mDucWHAhM5fBw1vrpcCuwu9Ml+za6xiqWvRG2sa/fVEQV
ZiPfCI1few6T+awYCBt/lYHnBF3Hhz54Dzlv8ko8RHn5l6ei0OeVbWa1+gOBwGimJw25Eu9Y8ewC
bZ/gDoKPP6im2wi48wPsuB+mp6DOjRjKrIOJMUnHE3yQ+/2muL+3um9/IYCpPwZsiEL1DN31cQii
BSDubChAm6yrStlnYqJi/JVv1UjMFpuyKZqmVPGkIJW4GEBHBDeSRZK5t0gjalwdHPb3SHWIbzZL
jvTj1Rz2jdiozkoC4582LVth+mxYMdBp3HK2KnPVFYop31utRfDLolrUsvuhFAHQbviEcpSWgu5h
Nce5797QKkRzF3DUO7LM2rfwg3/TDxWdRAwl3gb+XBhh7Z4a2K6+PJh1uKikfju4olS3p1E8vzXS
gbAH3dY0UoHxf2HH9Ei6T9GERde7ZeLISlfDoC+iZ0mMMfL20on62lFKQGGv8Wj2rLRJ6JpFY14d
sRGo8aTI/hBkFoLIaZkIYYg9WST7xUmznS1ojcnksVjd0L07kIOR1unxRmhqhpepzHSWorOfPyUU
RvPRw5du72kZ2qSwB1sGxN392i8QXttbr8IlmTTJEWs4l+YHjwQAL5TCQc1mBfLxcPz9w0AxYggj
mylULVKy4I2MQdWxGgxXiwzxohhr5D9MQsZq1DV2vRyDm0LIw/2EH60KKQyuJEYgjCA+WhozNOun
8HZTs2OYFYQXMbpuuHORcgRauw+r7xYoDD8Q5N9d8maud+oIgttM5y6EsN+kc1fEAsVafer4SlUv
W96eY+ooKEwDtdaLhM/YnmFAZ77t5Qlk3wDh1VSljizEsHc2ItzbaES4oqa9XszpJrg5ty4mnsoe
udSxT2u9xiLi/B1GISd7ItgQmaMi8nrHjyMpAObWP+I66zUYpE3CpQdxV+gDRLtk4qSN6BMyO7u+
z6b/kIW2nHRP+QnSmQon5YfD0z7xeAnPz5EWUs98Fx8QALksKk10fgQVPGxwUCiefP6HrKH2XoBK
Wcb3siR9qf7llUFxXT7YtP8ybNRfq8llfKSnkWwyyXj8AiEqRsSUH+frSju6WWIfR98tOB4p9kwn
2hNFx2t2IolvD4VKsj4x1T8ZAg68gdVuqzcNy8BHs5vuZHv5/9775A6TZiFkwnx0N7AnPN2eDZKg
dIloAqPMGxOoWTbPM0pZhfV6BkaVN9hh06xI3+n/aiojBkEB34PWLdA1sR13iixBa/OqC/LcRj8k
FV322sB+42Rv3DA7QkQP9p/dUreof+glsiSNatdzWf8Dmj70IZaIxvKpcXluZ98/W9GDbFrNoWlB
TbzyuHsJyYdBO/T4Hi4PRvMNbF9gY4eEBqWXrFfo91AD5epXvmNfPD9sDS5S3cbu+X3VaMNaHT50
NVVzsHA2POcyXsgPLSCcVmasm1ghningU7YwBFqywk9x1p8g8isRF0M8jLs5hmot2WN+xDrV0llT
7XGEKTyJjGSq2eAs6JDbkzENRgF18KWcbB6EDDciRcg8vGx2hM2uoTgDhJHp7A5BO3JFR7Uz2NbS
HTuV8BeOKWdZEHZ2sg0CxLznINP9ZPMUpPserbbenf8Blon93n/LZSPpYBOKK5Lk45katxRQoCLw
tb2yY4lE5Sxv78SntmVi97PSOWkZpJ2EV7P1YMysW/r7hcePDmA8OajBW7hPAdsFQoMaqYVJPYtQ
v8XllHY35JH6mdk5OgUOcz7AkbXeC6NIhl22FqBjpQTqgl0bBvEdJ9Z3DCY87L4uKJOhRrQjfaV4
D31Zrec7FfGsQBcr4jErX7WmG7DloUY9s58rkv9Jo5vIT1Y0yswTgApjP2xou7ooqQIFn5oct3qM
5nxP8J2qoPeqWFOOB8Znr8as9zWJ2sFhis1e/Wzs9MxzzJik4P5zWUp5b3lMKgAJYgl5hKgP7y1A
FJ8AdM+EzMNS2qETHaWeP36YYCIOLJ/4tfRo+nQ7U4shDJc4MqzKzVQb+nl+LD2nVLLmHZhwx42I
wGw+eS9DkV5zg1cPeORRvuhQwZYJKfJWQOavwyRIEP/AofhhM6/HPMZJneOmBz4sFLocdiXNmIAj
JecvIbvEmLrqf9Umv7a/09VXCFxXYZCKxPTuFrs/5qGNWCONtxjKWxJLNTovqlIgwDe9QXRA6CJY
UHjrNePJhTRM8a/ez/kf6d9LxBvqFYvY6UMTTix+V97UzJlVFQNhu3/lOqTqsEj57jBHK/sIEBZx
+CvkN/1/fGu8rheuFilk/ipOtEdxz3+MAcooQxFD9dBHcMRMM+pU1VHxoNDHsP2PGQ4zwbvXvrsY
/1ZcuK3r97XosV7ZncGJ1FZL52vBIQ5ON+VGvraKemjUd+nXCHSSOwvNTI+cyW13LptdcnWH3NXj
uHQ15I8Q1o/aEKkjxXE4YAHqzQZJGulogTIPNgdmqD4WhYtl+3BlqifrcC0W1roUqWGsEQNXVGWa
svnInVWzbg5F/hB/orMmPFUjsDqHHsBf6NqlZ9jefEOgrn9U+YvtOTNeGS8IboT/YiC9/B/n4pfP
46ZQGtjNy8a0HV+63Qp8X+FJFDYIBv1AQoXzjd+SbpOiH3oXNG0Vq+KMiYcYRUk1zayIzVRGRO01
Mm17ri9cjsCKeF0VBKc/yjSH2pgAphuXMi8U7GtOe6NyPDtv8ZeD6brklLrhpbghxrngbgFN7jsY
KLuf0WD2QdZlexrG81PHr8LZydcPzV9OsZWSvFbtwQlE2kcgIlTBY1O4zFSMCUKZfINDfKw1OXHm
qJn6Tsnin8r8rw1YJudFmBoAPhJdFrB21h9x/KTUgnf1+ApmgTNh9TcUGlWDPWqQw0m1N630rmt9
+L3qSmwvYCXzwOjsK5RVMuDgADMRPys+gGA8e8n94SsROOaTiTJbNXn1f+4pYzQiqkBHp7Cp+lxk
Y7KfuVfiUgOk8m1gLiDElDDYJR1H21oMwzxf0FZctMK9RsOQwMp7NcVm45//175hCf6xG3LEgBCT
TPiy/Q8hJkc/7aQY/HHNiEkOGg30e4qNIc4kmodU4TYQuqvWVmfZ/I5L1zV8f4Tnt6CfFPJjXJUR
K7BNfBXti8uGhuRXVo0C+E/7MgYwRO6CqvfMiAo9yxVG4MYvk3v68HTCiueMLqFAwuAH4nmVfJIV
KlLV4LA1Hxs0K8tGe9fWyJX396667IAjz9Sq+gAxi8nEMVpY5LmPAWWmjtILE2pEd+gomMTkG1ce
ZRJ15jWVlunBJ+CX78QkalaHJ0GglIbO2xg/UB9Wm/aJ+gAl0tRM1VBS/w3LNVtG8sal+2DefkBj
NQlL0PJTRHQmEKXUgA6c/2kdrCCqNTxNlFQdB+d4xtAoxOhggEfIyQ81yfYsw+nPe+YbLh6yFEkw
mjgNGbakH5B6WXgk5q048Jo5J3JxPi7EVNkSNtT7ExVY7gbcA2HJIwvsmc6689ewMXl/2Q9iy9W2
4uP7BhDcKmVXzs2JAGRFg1LtlskV/W/NXzJ45eoKu+mGVpKe5HINPVrGPPoYEQxogTYvQdtEGpw2
vNYT4B4pXxb0+ddgp6xf6ElNvMS+a1zLAZE7U8/TxM3WYJZWhayyVRU6Q7FaOsiH99c7+uRnu89+
jgBCbpHexCHzx3Abv70/Lx+R92t4LEuJXS4d+jzgc3DIQUC/tmeykNIsTDYuFqB8Nz07OBPH7vRz
biGi+QdhZyEg5SYgjdxSHYZ5BuRDfl4jreXNN+Jaeqjd8TRB+1GCvjOuVs6AF3i7m9CkBAvyBt9U
Z9Mg3zDIoIxPy79TwI4DHm0bj+WZtNndVfbfaBNI1l/HWrHu3neaC+kK4VuCWBX93nGCLhcnZJSW
k9mjfHndp5wqTWLeNjOh5jL4kAjjDc7GFNq95jcHk/+Y0nuZFaZDR0W0PcNHkusKhqTkH0YRSSEL
PROnLL+ZlGw5dVlora/QrNL4nvtsURR49HwdbXHTgeOqEFkXwIERy7iDRgckFG2TFgfX4qIiY/wh
UanWwNPu7WVpRHqcjMZmJQLPSBLkIDKgc+LxMDwWQHHyLtx9LJV5LPYyrvpFHR8+4FRUEl0/O46B
XcRMD3C2UFxwDifkWFl8NJM2dEf5KHBf6JPnVyhtS6bIXwxEH0MdQCSf1q4yK+WAp2xEwr7Ne1uG
Ot1m/NsMJDyhTdDZDx1pGG693UlV2+m4dN8SdpGaTLmajLemVqIsl//G0YbXsV0BBRRDUwijHTBc
4b8P9WgZRNNERGYK5Pimi7xFnjybv6OVaC2VZdL7bdaim0XjYSAJPOCTSkxQBQ46yb8Yut34OFt3
0z4iRzR+/AA1DeiWL61VRr32HOiPN771Sh7D8j/Ua5lWkVZGWBYsue1NzHIDRXEn32Zoj6HZj5j6
pGaFKyeh4uYOOR8uOPcACprLsaOgrEH+XjOCs1hnH61woqi+CQYHYaaDg2k4iDTpETkgrZMQATtf
TAZiXAD1WIiXgFX0EPC5svs5D/5tdgeQQ///NR+N/tAc0BF4MvRaE/Dlb3lOaFn+wYi0ChgiJuk8
4bZA4nex0/CKGnL/1jFNK1wezUdsBQtbBIo5JkTaJWglKLxkjuPU+UgWbTfQaXhqxKhKTci8YwFs
2UdPMNfF9gffwz272F9P1/06dSSFdJexuQy/R8NF8jmA8TAB1tGLOcuIVnBpwqrSrXi//1pXycOh
OsvYTCoB52nWIgdBs8T1k8pQ8e6LuP/oie/tx54cWill2brjV4aO2IxpfsfAlGX+xzHPZ87LLeUK
7AQ+8QinqnbHcUK9QHPfmVA/i+KITDDooQ4hdr5kD88QDuF9+J4Cj6aALB8k/IjxIeOyaX+uXeXO
pep5WfnM59uWcf8DbwRwLOMoQyO6rofqhQkSMUe//9W99gxRkKARuYwBhCFwD6sKi8OkGSgRmzpp
fBpyskbGfddWwvPOQE8UcCL1mKw82cMhjfSx52xHBHJy/1/d57MTRLISwInD9XdvloSP3Tc4BCtF
KNeEOVC+m9248YmvfzaKDB1RSh20ORoyYlJjjvE0Rk0cMusSdgjqK33azYHGNxaTIq7ux1uj7uzS
AB2vZmDuLlQ99OGZwkY9s+Ki7MvVd+YdWiy1zi/7q9AQy3t8FoHrZFook1++FNz4Rdq7nA5OBhVI
2BeBuc6Z14yCHR8ut7M05N7dzJDd8+/Dlt2KeTc+6NipVlwYTaq+R5LEfp+LbBL+bz5+TFjJx2/Y
+XbkgasJ7ejMGChWYFt/OMsVw1Rl1VifHCTE4h0J+xKPgtqEGcfmCgmQxSQuAwWAuVAvJD89ebHY
vLObatpo//6Msv4aY/86NRSVkM244sehW9hUN6TiLtJx9lB2VVtRO2HYVJx06YZPvylDexpDHkAF
FwXTJvdkpVzc1THkTKpBKvtU5u/BT4S6MmoHJ8iZfB6ViSphans8x9jK3Oatx5H+mHAJa4orvRTx
p+1RT0h003CXthrfsVWmfNWjkzmQCkEFMFH6f+Ys8+EtbPYqSl5B5f86sYxVpSky+AukcL8I5ckv
EQ/b4ljRHlRlDgwkUUEWlXYibpXxUoOcv0D7cAI519DKmVPxTP/0OZ+6DAOQ6ADcWXjOiTjfBVtx
QPBTg7URl9PtHRsTzAwWHfHdLYW3H3dNhe7igR8SZg4Skrh3BeK2IUQHjvwmMuUEALNXLbYnMEQZ
Zei2OirrvUOONHFSn2CKv6iAeyMLORhbXO9Ih+GLLNbWF+qzBvYtHbJvIk50JfOuk/AdT+Rpp93g
vaLRtSi81psvsledFq/2AerwlBIph4KBFn5ep817Ce1ayg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
