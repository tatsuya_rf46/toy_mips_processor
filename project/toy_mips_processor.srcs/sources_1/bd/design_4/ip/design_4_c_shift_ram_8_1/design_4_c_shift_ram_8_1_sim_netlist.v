// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 21:57:30 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_4_c_shift_ram_8_1 -prefix
//               design_4_c_shift_ram_8_1_ design_3_c_shift_ram_0_3_sim_netlist.v
// Design      : design_3_c_shift_ram_0_3
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_c_shift_ram_8_1
   (D,
    CLK,
    SCLR,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [0:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}" *) output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "0" *) (* C_DEFAULT_DATA = "0" *) 
(* C_DEPTH = "1" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "0" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "1" *) 
(* C_XDEVICEFAMILY = "artix7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [0:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [0:0]Q;

  wire CLK;
  wire [0:0]D;
  wire [0:0]Q;
  wire SCLR;

  (* C_AINIT_VAL = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "1" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "0" *) 
  (* c_depth = "1" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iuyzNa2aUkJB6srnTjhynmSdth6M/++yIe9my2hbYv2hdosCr3bsdgPbZqrmky0yiRHjglyoKw2q
/lRmM634RBq3rS69CHfAD4sGsz2O3SSUCjpm8APts0X0AhPkdTWUi6Hr/DlEg5iuNIbufrzuA21i
EiUqf5Wq6bi9YFmzFn/c6VvOoSCbdvub9cTdbpAakNwWePC89BcU9LE7mG8MlotsMoJ1Kv3pnge4
u6A2YEkQ3RRl4fuGIH2qfvBpCsF27RReRfm6Is17V1orEPw2cF3ov4Qa2A9vmP5KIpfkxz+NmQHY
NCwm2RPPGnM+UmsIsG8vCWyeOcKlmR3K3U6LIg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
GblzY812ibXStYipSANE5av3uJjVfD6SyAMOYsFwvEfBydfdzCtNtGilaJWgdWB3pv687xBayRtC
mMYYX6EtNWeF+MbFjfiQt98qIReCge/oVgCUnfeW5oDNv/ggpiGjEvNU8eRZ9A263/WCVf908Oho
rCKgMWhfVpE2pt/vRrTextnrLsXaTbG9b8VGFK9oOYDMclpSSfcuhk5zvZ9uabd6P2xs6y4x2YG0
nQU+9Bt1o4NDkb5o1qphXHaI7vcun/OHurfzEmbzE9q4bmb9cmGFfA+BtefSueww6L35+iVnbuUq
0wonJ2kYs6FyxYyHpiqXfh+sObj/iAhdMbL9lg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4480)
`pragma protect data_block
0VZgPWjsRnP3xAae3SeK3s97m55qWqLIT60W3KVLNq+REDNeF/1HrBmaqvIJGmWChctUxQJOIENm
NfjL7r+Wi+EVfgn6KbFB7txWm2QX+InN5Pl9AdgCpEKaiI/y/YPgadoqallNeNsKRP4fZfUo8aoj
Bu3UtgGLAjG+ho6Y7aYJWyng7xgA6DgyByCt27FTwsRg6jjNo9T5Ew7reYlHAq8FDnMIZmvMpLdv
zBeEeA7adKSRbjNOfI6ExRtJXPh1jQaMGbpbSzw8S0UrNTR4VapPHY6xBw6AbAfNb+LPf1sDoer2
vAIaM71wxzEtQHp3OCA5qp7KG6kO2f3qHZ3Qp/UtPJjx0vYAFbISao4rYCaEkZwBmh/zIjU5BLlx
50yIL5zdFCXTgSc//Ds9Za6nu698Yjr0vIigpHTRCLEwBttm3fCJUbucskuOf59S5K2RVkrgzU2J
nHccAN6WaDbOPCh3YCqMlOr1zd7w7wia5VZVaJM+QRtCyz39Db7iob/ZJl7gnLO6KTaFlP4RXo4a
wu/Byydq+4auQXvpSygHMF/FRlvrnHBHV8CITpaT/tBQNhxbBMqy+c6PW9sQ9fBJr2DavsY/aVib
LkeJ91tABwQDCjak6Aje2qDdvCQrejomiZjpIOR4KEgwBJq9/3pSZsQ/nBoL/9S8FKeTXv4gkYe4
nM1U9tQ+kdwqajorWSTXLz74/UqEnedTRpggIOWB2loRJttz2/m7Ywec9zSwVTUQoE89jK6gxftL
44/QI3LwmvzF+BHjUQjiqAiR9I0H/M/NM3T73GkWN8hPB7CGmnMviLpkrFsUj8linnFQuM0kDj/U
KF+j8/uhVXrHZOMywzrcw+0XPJykwI9XQqxt8srdXxMjudmFW2aUwg/lwWPaQQbG1VL+goM3BgYG
8Hj8CtQU/ZuOOuUURr/el54cg+EWOmrN5crwypradSLJ2LQUus74UV9kntZaiTrFnJiKn3J1dmsE
4MZgVWNnb/Ppz9qTqHWpqSfwxYNFB6mq7CnrQ2TpEy3GViaBwtdA20TRqvtnkyXFuh0RNWcK3byX
6lAzL2dttJhwadEwooycFJTSkq97CQ5wdO73pn3R/14TFTTb+PqNxnYEo0DpzLXbiQC4vvRDtZPK
kgM9ZERgiR6T/0/so7N2nfYBvAUWxOtcC3zxXTFWC1Xn8XfjH5S5NJNGRS5M8QWSIbx3gIt431nn
5McJyM8sTgZGhGtGf+pEEz722Mbi6PqBo/WMI/nSVKW1nBaTVNB2U8p1qlvGSkd06eTYi4Id12H5
WQrerWIKWHBpxyZggTcpFbHXFjySTMT7fc3FUU6M7BPj1WHIjugdi6ii8YM9a6rsJpQdHwqhLKzv
/5I2N85O82G6lGdifW4QoFLsxXzyVmcET0dUNNqlxeqB0Cc9KXTI3HohTrSJNppwC/WjTZVDxlYT
hZe0WXDJ0H46N62CGbtvOkQjJ1WDDjBZMXF1uCwGC31rIU7dSYhdqA2d+EgAp/nL4/pk3Bm76f6Q
bzEIgUoXXkt3HwePnk0wQ+DFlTGM9pV+OJtemXjAVcTOpT6xRaYkhitwiKp2suB/QUCT58zos27B
rI47g6SCnNMR5Sx6RtvsyWw54JO0JRMd0HbVXcMX298nVRUrwJtPkjNT8B3S6c4S4PWhKNdx+nrj
x2asBmdxNVT079WNLpXz8yul82wUm6roPacbFhT5atALGTTleHhPfHFMZGLA+WP2H8Yt9eITL2OJ
R9X6j/qHT4zt8PVKVjH2LUD6Jj1Jnb+wdOmE1xXEyI5EQv9dbou2FpFWgXrylQmycnM3pwSI713t
EqIMo7OshpqDynDpsOZK+gAlky+UhqURq/KGF6zkndf7hR13YP6vZ4F6UdDJMcNlNWccWFWcy5zZ
AGbVs+beSzLEkkDNt2Vw7lzzy2LZho/Vy7d3QCP8lQQwGCOYkMJRIrfOqixpbrVp/mZm51gh8U0q
rvC0ygV3iuh5cEwKO6/4UDJPUUaAToDInmUpgKePmta1c9jJOo93sSHmEBN1h+DBaRN+iTxLVfTE
4iaM/Vw7dejg9BHhEYXqzFawxRomiyF1+UcnfJZbjzf1jTKk8NW68uhLPZ0s6sflKhjBkNB01Mdz
01iUCX/PsVOskEQyoNT5VR+lZwUM3XmhpTQi+YpCq1p8a5b0X+vuqL+dSqTsbzmJMxxyIKEmICLM
h72LCXCO9rgy6R3pT8nGm7J4XXpWEtZyMIhxl8zMGicnpHOpd7dw1s2FmkTJrpEMGMFj4uwZJGRP
hyFijrY0yyxnvUWyEaWRbHkgmUIWtfheFSLTHkMD9oQNtGZgVLGCcGUVeRexHr5RiZgeHzbJCbSE
CpDv7Z6okD3VdNKMYEn4A3SDg2PW7K0Vz2ZpkyK/l7NyYVOf2Zo6J+UeBz0ALSPPZlv89dOeXRZy
vGVvqt9VeOsCOw5ALehm2FE4MnflAk9uAXMP2JjToWsDpPqVTVcS+C/0u4Lgn7LqqCgDSpHvTBV/
fyzDhmSf1Nk4B9S6R3D4jny7W2U2/60rCVZKUtwnBrm3duRiuouzrpSdMJt8w+XkkucC33JoEzdj
WpiiHBCUYqRpv/w7Vx4KBofft0t3UosIoM9j1fjWCP9BG6I8VfP9f0TX/Srt+H2FjpUUnTYVI7B0
5/JZw1jFABeWjOPKEJFidw7HWu1z5bH4pHqqz9Yr3ikYFM1pU7MeUq5Wr91iK8eUowXGAwRcnHEs
HjlLa7QGnopZgpJ9J9Rh+ASKxKe0Wn4DheLnsklfbNKQeubseEJVi5xmr03WtRNSTYcle54gjRwm
KabJiZ10IgV2PuE/Fx/pdcV5aV8wcFT/no7+azvQ5SetzHYa3h6caNZMkJY2vnnzWKfooKffV05g
hB6XLRfEbpN0N0jqdJJrklD833y6WxAFwKqSKotuxnUhCSTwhdah4G+kJeundZS/gFDJ25pr+hmQ
Ol/10YMWSgKwqSDWhhPhcVUAmVbJA1auVZeMFObKfGehh4/tjg2bMknlJDYeSZJW/UCqZaOKz5Ln
av5rn1ATnwqrB58kArsCDQnfiOVm/rSEdrxdclDSRXF3+ZoKHtLpS2jhtQaAGPPvF+xo9GyCHaT6
RoMKN0YzAuYJlyTEgmnYytU0Ed8OhuhB5kUw94XoXpS02FsmUHw2Hhf5V+fHOfcMLTdtgCmIApp/
UqhQXPm1m0UwJAnz9Rx7VLsQCCi4luU5Bf8fvlozUAu7FoIlrObILVkxKIBTGna+EHwnHHp2gkUe
BTwXVy2qiL646nYLKT0bRetGEgqKvF8Q9BEDlFd+EigLhBsN4iSI08y87HMmrsfnEdiwY/e5YU1z
t6pkFtm2lJuMOhH9oy0OdR6s/I13KtWIBUN2HLixhRvBT3Tptt9s/TJA74bkkx6GxocEnppVeZDK
Nlc/YGYA29DgWzFkUO9TUu8iO7jNY5YUUrU1jZaXJ871O+dIBZFSKTK9g+UiclRKUy6X1qMW7SgT
s7nmf1BseXs7GkREM1VI1BZXp/DQDlLXnQiNP/kvMYi/Rvt8jLxfCwrBVNshgr3k6spANs9lBDhP
xuvS/JLPrBM64MzywXxYBC7xXLpUY1XwmklKZGdvSEn6/RRey2tXP8CeHiwofPh+R6dgyd+1JYoP
clvRia1OwExcI/TZutf2YSN7ZNWB9rlSyAibo2GQBK7p5899jDnagry5znOqhzanrtaq00O24fV8
Z4lLV411o1a42EIbczqFrUXJSZylQXAKARKLx9AhAuNVQl826ka4SB+GwRcY1a5VUAf9kd52KJJd
aecucIweuGqkMz76meKVq3G3UJLiysoAC4Hawo6bdVoLBaRuXVkBCAHaV2RLQv5JvBZugRfS0MK+
ddJfVMrL+ozGEqrcVDXpnyu9Sqn6zIIJ8pdJGwPi+N/5lxLu/iiYtgENPJCLmy1JPPBWVHhhhdP1
WPw4Ba2FY+SmCJLsm2+xLmGPdMZ/ej8qUzagP9cxmCe5OCxIzzsEGnbfO7cmnhIwNMg7GEgSzkGw
446OZkqYQtjLmlzSIAVOleGGfSZffmDFSVfyjtMN5+TdB0sgKkFQueNkIq9L1uNCpNPQZNxg6XWr
N03qlL83Tua4hLtY0rpxRxq2whm9hmnPdNLujj36uT1E1lKifkB0Iq8AChRh+6E1NpelL9fN85Np
D0T1QmEH4hq6JNVwowsaDkHKMfYDQ2OtzS1D+2QNG7tb4ru99D6j0da23eHuBT3k4ah3sq5/f5W7
P8jFWpT+evGKPfhc7xrEIKEAEPC7IobpB5v7a33RlsgopkXoe5zoGLIZ0qfXIOydfmVBzIkkHpmx
UDGi0s0hIbVAE7d+qVo3wc5uL4ojeTaOLhR2Q7dAyDixW8CDNl5gD4g8La5EFCJ2bHfzlrjo+aMN
oSN3NKNii1052oQlKbAIhV05PPLixEUEkZlhc7/a10sB3bnHakDn7B1MJhTgEwnY9zGXwvaSOPpC
vd3hWtmQjrt7qzHQvapFjItB9NXhngf7GiSl4WSOD10Rfffs+2589mvm1ogU1C+P4YSmgr+t+TJE
jpKaaqXR1wxY8m34n84C+s2m/DPJJFNbMuFV8mcSW41fvltadIVH31JPF10O1APqnShNIA+6j511
kBXGOyVFWrjTUFSYQNqf/fCgcsIsB8TtVwSrehH7Mwl+2KXxmsksyZ+bStZ09lOiLOpw6gz68q4p
4445LwqQXqN/maAHp/Vbq29n5xZZq68fipiV9NAgnDOHr+MQgO9Cgv318S7y90nwzZLOniclAG8q
OBTE74W0G2ikOjvXNwQAqNDGr5+5rLyfxRfFgMnlD4KXH93wi2Y5+cJPvhRT/ElNWJp9EE6axof9
oFgIPn06+oY+PAS0TU33uCDCJBmXoXILilOz4QjPxBuWCcrIzIuZvN8hms99woIHpOYbWF5AqmD7
kTgMezkqTUuThgFUDJu2B1uLcsoofmQnvfsGFvgVN5nW2dkU6CwH2qhweLmVtEXnyTtEWlNAahfT
b8PvxfN2QXAGVQ5eRzyPkp2uuFP6JZBD/A8E3Igk80CyM5TQjW6L6idOMIZEBdbbQzbUkYY5kFhP
19eoZXcGAFkp1O+MWEUvJA11Ut7QYqWLLqLa19S+iqUQYufR8hW7csLETOVcO85PieP1mEWT0arF
G6kd4+TRWSGA/tQV4nr3VIQAMGMkq3JJ58MvWF/ROQVNvuwWw+dGIV2VNfoUn/7WQXTPrK9V1tEj
1zt6FBjFsrNV51PYFbuWB+Qjg+W2oZwVlvK95Mnb9VA5AVwzukavuLwrkYb0XQgIqK8j1QZ6A0wH
JAjs51fP/QwIXUp9t2NSKon+SlVKeF8nrVENIqG6zCaYeFAzHifDUOnJE/Veq1GUlmeZIqlGsYEF
pfGxRXKa53h6GkMr1P/B5Nu9T9Q6F9z+g20t4jU/FIrQP8TPDTiaGV6CRnidPDjCt4sqZPW1Y/Og
F4DMMfkKd5A0PR5DUP8sQsvGdzVhhrO/QM8WsQGBfyFyYkiLSQ5mSkARwrpkLHKeCQaZNS0nWUXM
hK2tkB2yo3FIZtkprn1srSbpXZdG2upVdI8elBJfmpkDLN2fKhIVRgUtuSuOXiAKGjvwThbOT657
KOqj3jgPgYrKpw+vuZcAGcT5KUfQj9Y2UY4gEnY6rNPRHiZ+CiGFkOCmOH3GLXgqayiUDn0x4oiB
rB7WrCQQXZOkwiy1iM4MEVNQjre6IOiRry2M4YxB6e3Ke5yZJe/SnvDCm8GIvDtqMrSlgpgqXd3B
2Q+eo41G8EHCTTjFfM76S3Fdmd8Tjsnyb3px7/c9WJmJwA0PDpehUb0VS8mUrFz3KMG45Eu4FxkL
ZmuuHxRVrE5r11DpiSEUugD63eGUdYQ/E5jO/ZThWqsYExDVXkuH/uZtXH5y3q5mFi0HptN+VA1W
+g7AkJKKTLY0dg8UzEg1/OhS70O1gHb5kUJ6z2g8IJkh3w==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
