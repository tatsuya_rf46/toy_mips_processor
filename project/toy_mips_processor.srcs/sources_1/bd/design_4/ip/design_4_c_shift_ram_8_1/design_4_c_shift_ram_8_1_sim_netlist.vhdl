-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 21:57:31 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_4_c_shift_ram_8_1 -prefix
--               design_4_c_shift_ram_8_1_ design_3_c_shift_ram_0_3_sim_netlist.vhdl
-- Design      : design_3_c_shift_ram_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
EbXZS4y9cLjOTv9aN2dDC1sJBVVR3T6cbmKAVT9lmEHVIdHGCTfu8iy7QkwIs1KmhdwMqwdjQdXK
KX59vPzAEw==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
svosYlCBRVGey6v4WrNTTJ/a5E95XJFz56V4Zc0YljtTgqhYJjaDcp0yGul9TGC5O3yPB4RfWGyi
btg6o3Dcl+FOWudpxsWABJlvSnbhUeNY+1OKCV5sW4s8s0XiKCJje0Ckn8Rp6OvgxUpP6PcdRMvZ
/iOZAbfkFtowP72szm0=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
bkZxbcKN0VCVZ8Sn45uafqVYQYk99p4mTYGqhmN6rGL2wN71zIp7oyvjrZ5+IkYIHjaRPVw6MFHU
01i0/bnlUJiW8yu2wC0IWq+Qr+7tToxb6o9RWnXK0n99HX1QMXGzkrlEpdmtBZrVGvgv4FixWWZQ
dodQluVohp21teUBqa8WcGsxqwaf1e28uNmi0DepWjqMe9id/BduXSphJGM1DlXD21S42kAcvg1F
rd0pAgZ6lhG9/NzFbvb2jrcNLh6ifBCr2yjVd33eQU68fnkIGCXAggzWpyR3yOvnmG/zCHLWi4gb
PMOlEmzrjfeM8zl2NP1wqpFDnlaPnYEIcaR53A==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
uYdetOP0NrAC/6FuAtYFxT5Pr7xP1xI60RhX9Ysmg000CklbBe3op1FJo9+N93iKzuAQn8/dUzat
ZR36c3yAxvWyYey+XkDfh+7aMlphnj5vggVXK9DqeVsHakNPxVCao7RCkkSR5x9XCYQXJlARvh9C
RhB/l2sQN5DF9bDt9yCKJlWeBEbbcjDJ34WronEFGxp/E9TbIEVWGB4V7jnlgc0oxMMYU40V0d4i
oAADER64AUPfYZ+0e97lsHeETWrkCE5+mE0OLxvjypqZXIFAINmnYsr5zMzToF2CiK/NT3DIL+hM
q6OlPRN1R85uBOCDP7qHtxj+CdoOVPKhdBfsMg==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
mo9oRLIx4kH0M86v4sywZvgPz5p30+mzb2H1aU6fkraIKHMy5ue8V7ysmq55k9NVOSXTmYoCdFml
rPPuT8ktqPXADjRPNUmPsenolR9+96Fta26fIQSUqMHuwI/y88nM10meyCjIBjD3+oIqsgrFqbaG
saQSaPJ/MMnei2igUfM=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MqMRozeQ+7B22v/pgqDAubmlkM+wpqpbsz6L+ntdBscEB6ki7vLly/oGOJTK4ju8/qS8LlggHRaO
xtd0voFIGd0icRz64Q8EBqol0lxXJPuQx4zOa4ucCqaUViJ8DL8xQgErcDHpb1p8W6mgaMCbp1Kn
SuN+ZfS1rS2R+r3eI2jOHh5EF/8a+cFR0oqrSsWzggfrGMzKWWsSLwd0s7UMDTtruNQTcAzYvm5V
RP9lHvvN8So5DeLrtLSl96n6SsbeObAAXX1i6fiyPV/C4IkPyx5F/L/IwAENNAvrINtYTWp3zjEx
G/xKzVTUEKeNs9XMESxa+4oJjG8+036ic0vnUw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
IMm39dcG+n5fcIDQcybfOguCUX3GDSDHnE0ukUt3z0GfgxGXQ4udN7KfIK0bhw+jASYUkEQOG82Z
jWNGyelrCJ7tpuvsm9YaIUYr2IJ2QT1Ynkbvb89to7fC2N8oJIj+CoBTtLC86KT5zZElgE6hbiEz
7BmQos82ixAQStfvYXzLNA28OuJ6lb2E0qmPHv4aIX8Fpurga4e+hsxFRIU3Z4ic/LvKJqpD4ezA
/K83dWOlScX9ZuWTi4mAGoqA+zlbNbFwBU8V+8K3oDzdsqo44Z/2l9hMNYUPYCk1/tnKaQd15Ehg
LrY/vRDu7I8Vy15n/vvtYw8+JsW+ZTjk06pwIA==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_11", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
SsO3/u3pdnkO+dB+OKyx1QDt1mi6uw+plCPLC3gD5vGcT/Rw1DFHrlAIQTmqwHN5GzbPEGkjYmZY
9kwB9EjM2gIdSIdoYRB1RyY5bhp3JCgYfTzMPK5LNFIi+g7M+TtGYVMGT8Di35eaWdm5aaUgxJyR
rB3b4SCUL81yP7DQyIwpQFQa4PC7Xf7b/l1KQrz+rVnuLA25Y6pCjkhIHqPImKXB1AIZfdbma0kD
own9h+IJWBIJ2BjOJkXUROMuM/7PUU6G0C+o/q/qITJAS9HIja+EqxZMlLGXOml4m0pXrwayXWl6
J//yfLFAhoQveWL1I3f0/XvBrtcSUqNyZJThzQ==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
rGUo/JqxXHI4LiroeJP/5v98epEBpyTzmJ7YInVFh76jqPQYqQwo7AVwoh9TgiUlhpU9Wb+qQU19
+qvTF/Gqn30nqqrVU/oVBHdlWt4Qs7hNLYOLL2vX0gnNrqLUKTwnZ21AvRsqNAIDdd1qtREs1EeS
42HSzbuUYLsGYNqM8uyFwr0jelHBt5LHDWvXN1qjep+TpbkIqq07XOteo6VssQFqpoz/YTd2B2WE
0lBQSolvgVtGwYzyvQpu1ZzLlU+b0f4KM2H2Ya3wcFnTGTJr+/5jFzS67ngtvo4QtGMsCXIVZ4g3
ExCDIk47At+SmE7ocd0zDTf64FowzSAMc5LF9w==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
nuO0KE3O15la2q+X0duoOOPh53i0ZIe0dTVQCKixWPKCyqi99qZQwoDVPxh+0K01/FaAT7ZnHjuA
OrGf4ER3FZYCimHRILoDS14nBVj4suzk9EHRWdQFPK0MvpODzEwk9YgR3Pf2pMxCR1sAFaenakw+
i0GNrJmp+QV38/aYBdazItktlSKtncELCPjN8PgK+RZhx9Qqai7GZ9rtKN/2XyiGh3+8/vdgxipR
suyU7XvqCXUo3jwTXxW9Q9d4LuwqRVtpMVCzJLFUZKJwNdWGWAE74VGChpvepalF1Vyfh2qLpk5F
bt95ETV6wV/fkkzMXvt4qrp5EtAXWZjsAd24nw==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
GffKD2DyMJn3yFyNvpLcq16WZT4CUy8eZCO8YYK7E+9xSMteOCri/PZUPOOKXSBifRo51rhhuT3E
kCFh4qfhxJmpfR9NeQ74e57XTZWJ3qQAmrZcetwhjhyed6CSzge2MuplnYYmmu+636bfBuP/t7sT
ap9th2bgoWl9lRoMW0hVFmp/oPXz7LTuU/DwnFpg0iMY5ag0Hc34FHMDNwlJwdt6iQ9k6Wmd6+Ct
NhdjYzh069k7GW8lZxxjGFB1SdU2Q5YkRYqsQco4I2ALekkcq1VVAVxFxspF5zwtGjEsCNdd8F/7
LspIbFD8Vd6NrRPx2F9qwoViPzxTT+InZPl/CA==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 13168)
`protect data_block
gT4VsZhDrikZ4oVy5X/vDUhqnVsZz0z4m4gJ/zbBR3U4RDzvqRw0K8iIZelZKiOPgX2ewzkjX4Mc
ixofni5yeeJi/xuB/Cs6QEKzDfh0lzFLp4ZiZ55NyjScGgvBzgtksUrYBCerLoQkTPjMEfoe2yv7
tKrzytzykia9lOQyHHNZDyYg068anAh0YQMwIA3l2zXBfvMBIhvuNiruw28uRdiUk+oH486upBaJ
pVm2s+8J/2YZZdf95rZCLFfnTuZUEFWgYtrkLDLHvPAU0Qh0pbkolOO7s07Od6gZN0oP24xkrv4r
G4XdgqTBh+UjcZBM0Uaczsx0z/0nIONhMz8ug3v9VUF45QLFYv4N/TY2w0C9rA/o46vCeF5aGuxb
T/A9sjdIFqkUUbPElcRMDk7MQiwvMuZGyxUq8tDKjKma3yBFfK5TsY/ULbSwUv1XRwaxr3YzLPxH
uyJkbx8DkyK8DowyqIP2NkvtG6Q6Gg19cnkER4o4J2KSboxxRyj59avwdKQuKZ2bEEN6pJy9uf0F
Rn2YZP+W+uUZBE23X0wrHeyj47JO382MOD5ALRKfZlvaam2WH2Dq6UPg0oj+CE3YR9cwlqn6UNvG
1amXNrbpjaipVu49/iZm/GwKd1jDc/kFC8/yrxo8jUXynoLohOIvNucmxj8qNVzaTsYxzC8H5qo1
m6FZVT+wlR73JvdevRi94R679lWELMqvE+Uks96IaMEWJNhLmfZZ1HhmL8dvRO1V5iTpuaT4WLyW
NPW304lL5yVIJIPF1WODU9ZAV4tcdEE8BDuWNBQnu708bEa6rsQMBM3/CPrlAJEGm+9SplmJDVP0
sYjWzs3N/dNqhDhyAC8fJlvQIMRCdzFLDzHSmF43YYoAbttRLW3ADOZ0ApYQv1NOdvzEkLLuC8ZF
ddrgCRS0bTLGGyzogBkjf4tJWwCu1q5xep5BcL8WLj3Hnryo/kEozz1xoi50Cp27StxgjKqF5LNh
wmvhJuTzD4oB/4c4DvN/BR2yrpXkxSm6h71UVnOjE6nRiMizHZZCA3BcUE42x+fqzmTfLnP0ddHg
H67YlQ7qpxjWVfpEBv1gsAu6Lo8gTHXe9rQDclZKS29rjVJIgvzGeSLqC/7PsQtJYIegSlVgcQj2
XfyTtD68SEPWxhqdWEs9hSMfgWH0ooHlJtNyQsaSOcIIHh4Tusu0MT0WxU2fmdZUnxJXwdL1XCgI
+/OVs2gL6dsoWISXffxaSYvqls9Qbg+E/BxT11M3ZiLOPMuNWkFMT0CmYK7PIt+Szy1M+Y24Qjwh
C3Ci7wi4L7QmcdZqfJC/E6OD73OnnGq5jpE96ztQg1rKYInTE+9ebnepP2gOtzZkEiGxKFWK/SlD
sMtOAX/zwXJDsXVewIJPbR1OoQnq2C3ySxTW+GKlWamlKYqpKBi4fiFl4ZAyIe/1uH1H/HZy92YD
ow2ybET+4PrvJ52JsndIpXxYD8JGC4SCLaH/EL2YLcrZ9v7JJXeMf1IPcpC1KGPVuvdx5VXIBh5Z
Ss0FiWLO2t8o+CUktHUzgy42WHe5w3LMamRfcRZjLh3gdPJJe1YwkuhdSWjZRnbHTiGgFZYd7cDz
ln/9gc6PrDgwzN4kFghWSDkg4HxLBalkzJ0jh9pISOOKhvoxLhxft1cFQiJHeWBi1IJnWt6uPqrZ
mAGp1nfj8p5o43CddvzlsYWm8Q135SH7nuxwbR8jRdzlmqxHPD1l604QG5riMMU/hUgAKjX4O4Ym
EwfHWtLYsUtpLMNshiBl3JiFSeC9i2fugwyNgXImc8g3M3FhH9CPzyFkH3vGE/HzNa9Fy2sz0qns
RLEt9fRHA7nR/Ss0/z3tkQWPyTNtfKiSGhjhEVj5SBFt91DE65pGEf5FNjyyWSmonADnVGJQglZl
5AJZlxKtiWoyqs81ysWkeOb94ztxEqdOYeg7xnfm/UKWniJW/DVulH1TPIqRRkfINBpNpKFdbqHi
+A9N/FdUCJ8Ia5i9l7sVn07ZrP2nU/n7gj9nBq8UpfYWlMhQQclaTR/GbsCnyC9TwOna0JGvw2tj
FD4fsnqQWwQNIIuh1pO5kpY+RIE2+KdE8ZoHsYGtj02hIYAA8EHa8SR2alrVQEMwwKQllP4XxvFV
9FmDavsNB1YFX20k1YLfLFzu0jw3Uax31uPN89r4CMP+z4TA8pacNNHTjttOT0nm87fbHWMCHplz
asYlHZ2273b1FPFrTQTV9gaE2EaDO5xophjGuP7rTzQRyJGbSkNEkS/ITFQ9evv6RDi5Df6Cy1pX
OiKtZencbjDBLEjg4fhX5qIqk1k32dWpihuphxbIz5LwlZ7Ws7sleljXN11Rt57rKLd7IF7wr5KB
MotOcrob3zSJaHgUbQTe4os8B+RG2aPqZjljhhEmBEVtu+v6u5nnFcGSK5rNOkpDEYheHMtJG9/v
kTyW2V7KU+L4fSE7WcHf5zIwuriAVH7kZFumd6RXizklb7y+tpZP4vWb1OTglJVk64Oli+igf6HJ
kGsLDOGO73ISd54zub95qkVVU+2Sd05WcbqF8ggs5DH9so++j5/3uPUlOLPoMiuT+Lg69qBsfYR4
mDH8vsbxaf/+iBjr5oiG8AvdUbIgsrXLmlCkvvNONBexAivmzgZ83OMHkBaI653IY58FHww34FxK
WhiuRHrJOAaa24onbhV1E8kJdfGFADmvA4N+3yxHkBGOYNlLBzqpHZ22NJ7KUaFleAw2OWCUzjpK
OWkq02Aq/NVYigw+VVk+qY/oaG2d+bKsM6z+lJF8X3VCm4vSQ6Hul2J4KzPzsKtv7VAax+oFqh6b
4kjLEPr3npWw0fXoT1rpDSxJN0iyRtNn3awA7fC5KeV9HkbZ3yrhDVtjpudC9jeYGxI220c1AICr
NC38jsZkely8BGd/qfV+w0xiDjkEh+EZhPrrVS53GhjSH/SMSzikNY+uxFUERFXyfXeNz+sBqbFM
noiT2eKLWMrjxbqcS5B9CeI1DrVZ5N/1t/iQKmgohRsx7e0DeDD8k5UMHtI9IAE3Ji13aEwFLvny
Bxyw/HYOZSRr59H5rputNu067XNxCWDkSD/PPManrCHB/X8tG17wqVkNXlSJZP/82Sm5sNF9cIyn
RFrnthYNNYhnNEs92b9IId4+Jb4Ra2/S5GyLJWVSqcUJ2Fd2D5+NInnbShfBBWztgTkvjFgeszQR
VRb8bIcv6gMTU3/5NHpeRPDl6Ls8N0lFlg7vwytfLxjBv5oXQ61qiGu2mXqAnQxMpVImrWt/cs0G
IcaxTTdnEEtRX0Je/Zc3es95L/nRqTFwqafw71+UA/7vrXJySYNXECUvCvOekod8QtetoIvAvdMM
s0Now2FpPmUrPPqwgwfnX0SkT6EnlKYDSAzKztqJllGT8gEUE2aBRm1KWArILiEHpvh4tOk+lDVS
iLex57SOkgAKXSmYDh/aTY8j7tfHyb/tjlyFuWY5atLKNS9lCw230uTMnKEV8YEGpYRX0ednAai7
nOP1cblQBx2fh9IOOwI7FNXPVs4s4UUElu7eTyn1s0cR5ZX/dPD30OQ/mKZrehaOk4757IvWcOw3
bn/1yxSkioigqYfh5tWfGSi7QOd49ssLJYHvKEGIo9/79Dgk0Pul5eW276F4IwLNkGTfHfiqW7r3
g6EaSQEZZWXbbjOUBhmCZFOynoRMg+w4gNtn0wXAPufRVLC2ZXVnh+/jsQgaK/TNoLFCi+KNg9/J
BtXH3GPYWWzoFw4acUMhZcVgWwhbJv+sP9PytN9lQMsY8EO62JciXXzLitxrlJedJHW6xlYGEGMM
hrrgC5VIZyYYf8wzMRXf7LGpfNxtbOl2+7JTfx3Vqm2kkBnuRHhv0AwA7J7DUzEWH0ZT2xojCe24
P4p+qnoR9Qgjy4jXustGbc7QpBBNkOlPXkIhb0sluOxvbb1XBMf6ADkokvyjYEv+W0o1++/2sIsP
bdUJkGoq4IPV2EqQLMKR3oWdpNMc7uwntDD9WBEcqKqrpmYu1XOBKuMrGW6m2msV/kJ+a9ggph0X
NOooMkvgV/RN2tpqiuUoJdonq60KbYLl2KakY8QuPJsOpJ3KIHeQHApDT41yDz4mmAywHh4jgJxT
tV2k8TJdiG++0zLwEx0IM1vYC1Idbk/DmNtbWEfBMBgoIatG09qCQ9aTDSOHPZkJdbIFxH+i8EHr
5+uAeD7rSPaJCKX+Kb7ZgUrIBqXS5017Tm30Lp2jbNZmFdll1NWOJoC/CvLiQqQAOW6vEnCc1g1t
bn/kSx+2IM4L2SGI6fIBwZ7sPHueRnW4Ur55vKGldoYe/5F3izhJ3VAXYEcTEsDaaeePl036AfsZ
qw1rgezwrZ980NrAqYWtkFtkBd5s0+v6WzC90si+ZpbRTzm+Th+nyW7QH914nojSOZGf/XGOonTn
3Pbr4v4uhjEQQ+AC0KnPCWYysYCRH+RFkWJHtffEB2c7ZUCVv3NaOhv2QIOxGsFIcbvuG/TpLwQI
XlzDE/TKxlZbgeVz4D2L3geCZJ4vi4KIrZ0cbyI0GRL6bF7k8EEgMv1yRB3qgMWJh0Cg4ql4jiom
Iq1/LtYJHbgj1IjtN5L0L4ABSHYJ5kRb8OmxIa3TCxslX5i8EJL0lSwNWVn6fdGlnGzP8cVPNSzi
vY8Fq72Jrp+I4g28Lk2X2axh1/oLvTNyCykdBPpHMQtw9qjJP00KCH+6Loam3alSTGTouB8CXyUG
nX1ClV/oefZcSPOyGdDkZ+pNtswAcKnzvTGzYm+Kw3XPkI2synQL0f0gx2SKPf5eGygNXWRr/BLC
kyES2X90iKdXZCWLljac/hErwboj0LcbZWpjS/mOKb8srqCfb+5HSp5FZMxQUe+sxVMUtp/0kRlz
X/EMCD0Bj4pTfcQoV4eJRdFehLpfqTFl5JH2WlyidybHCmC1qV6WAgu5oAmtcvncqEty16QK4/R6
UuvBW1FZqMhhRSQGhMFsm6FqMqg4ehURAXeCEe/ScMHFX8ELt3A6rgA8bCJkTaSwEgdqHIx9u5yb
JbEkIz7rMR5OFxlhEKrPvKSsq83TzxkiwtX2HWqlD4f/CC0Y5F7zlqo5ZeyU0u/6U6IEtPyDi5vo
/KTrmejgherPcVjY3jVWFVZlewLnO9/WcZhI/XHNfYqLkC9FIHJZ8lnzIfY/RvgCK9AOQqTXzvfx
UIu6t61kO6dI46OGfyBdiQG6BK9s4pqLRROg6TK0BtK1rLEyef7KkkOXxoy3tYZkA99/NrQKqsoE
6gWlNavUFQ6W7au1XJksVi4NQkf1c873txXB/7vQUPm2hGG/lgi3rxPeVMo5sTkpPm6acOgdIyzR
ikr4PoSa15iYdmwfdWTsRRc38Rhol8O5K1et547P3Mmjo6r9jlbL+LxExCnXyWqFrL8UL18KTQY9
d5pqE8jwRmlAje/y269gSiiSthYBRmmOk9LGowVxUNdpVk3NDoBcSuKSXt3GcszIX2YbizxTXVYh
+b2NNF+UMfDoWbb1NtBYJfVpkMRop3oCaR8hIVshY0LkEic1Q0MrK6zEhDdE7WUkGraMZMPCE1Jl
zmpzM3MnD+Biw0mO2kLlXUR7Dh/Fkai9BNnEGTnrMK6qPP4muImgJ2NKtVmFuhIIiju/Lg3whRWC
ajmPpIS652j1w1uSKtX9My75W7pDRLqU+dDjtH0ju4J4X0Z+C6+bTCbsiyyEg/KOI42p+gXSTyfV
ywmYMzbMdkd5ZVG5tOe7eUdmlss6eRgKdZO+jkaBraykWBqQfcpNSYInm0HX/3RudAvncDju0LWx
5eVgXEt/Ej/KZYflCRr1Gi5SPn14KXHajjNAHWEHdYHHeS8S81XiOTzVdBfvZQANI559FV9fUCaJ
z/Iara6m0k5AiTxsR/SrWuxXJDMzNNHIXvzQIDLup9uaw6H4ogag1L7Vilt3MGuJ1HSCuz0yfWs9
gak1qWUh/mO/xXzSumLuuVQsUEapodhwJtkAvwMmx3EZ39G98BQfUd0v7SfqvbIuhAN4XyWCj064
+o9bvtxiVOyAkmzM7dMp324bTWt7VNKszqa3hKwiEyF9YIf7SBUfTUcNrhDnY+jpghakQUIRQ9wj
UKwBl2WIPQ6phUz32U88aexmE0cINkmpGTo9Y5R9VQgf1LRIaVcCV/0lfwW68vEQlanUGu/y2dEH
0DaKh7SIWiVrkX0RcvNoO+YkvzRGuO7JR9XrMCkLeEN+NXoI4L9jFyGbznNWN79GDz/RvbIWv8fA
xTEte1e94FtWzl4s19IXu0soezFxeCr/el7866VWPr7NgfVpGVwGgmQPZKvIHpOQCbdq85e0mNef
eTepH5jqTezSkJ6uapuvVxtOLZVApWWVh3G6vExu1bEtxq7xl5pFss+VYz1q7yJcfTaewQZr+tHp
Cgm8NfUNMffMNgJcv7mjDLJ7Hq28vuipDVe+nsXDE6cX08p3avhI6Z90NLyuVb18cjPK+0V26xQq
6hUYiPcWdFETvkvwLVTSrdmW+N4vqYvHpU7c+Gp1Sg7JLD7+YHhyhTYlhJJNs5jn5oaKBcQMve0Y
TDavR32RqNAD5g9bK9HQh0loxvHVG0wavlZYaYYwXVFXA0YwwZQWDehzvEP/9mYm1c/jLzZo1LgH
fpstuSPNH4fsMed1UD26PmEHbk23jhJu16H3Jq5xoqTOUe5CDDTDxzUFYhX1vrXuLLmGZyZMXDQ3
BSiOeaYJdMZYobEMpV69q6bNeNQCtPa/HG++/kqgJQ8j9/GOtMwCa+HxnyKt0AvEzJpbEh2wvE6v
AE9uKuQ6oNlM0dEbRltOnh98bfkOkk+iPid/ZL0WnCaPQogrDgAFKf338KqMRxKyUXdVBvhILkTA
P5EYIsSa0r6WZNR3Dr3mIR8ebhwiJOt+JiFq0vimTHUdQQfq2Prr/j5jDQeCGdlOLj7929Vo2GXd
y46GuF3rX/adD8w9OUtmRL9nFZMT/r0QQrotzgDw+PHNDQspqgfWJ6QNzMMlZSHB+tIPUcLtNc4r
RnbmWPZEumGwV+0EC39Ol244apqtUFZw8KmuvmE7HT4H4uwOKRrQL9SurC9CV+dbu8jP3yXjN+c7
IjP+KuIsHFY5Eg1bpsGSyelxJL/44RO3fYgTcKmUP3SdCuLOQsDSNLZoFOwuhH030Bo885FFyZmu
HRy88l8CA2UTPDUvZObFUKWLq5SReTDuFcC+DMxjqhTZpC5L3p2AT4ESO1SSkIBEtMmwHvwj4bgu
9NVJSyhoO3o2cFhVLd6pPxr2pOVwmi2xDDU4M1fT3E/M2CZxTOVS9MV8SQzgrSd2OOqIAZn35bXg
yqhs/g1neDVkzM28av1CoeCLh3x5e33JSqe3omtW1MZBnlnS1W5HIAH8+MXNdl7VEGXoNGMSWRRG
msOOb5vefORWVfmqYhAWHKM8n3wR727C3L8wiyQwnsrmFQb1eWwMKsN0iwxcfOAU71LtTftWsiYj
IxfyW/znWfX/EY22CRteV3DE5kQY+jeWkqcIUKgmH+KxOxmrH7qP3ZftYNXT6B2H94zQcXaEnFE7
q9RwnKuvIJrTHmTh6gngbjDNU6EbjgEwL8BAkxRcuTui+RtZ+BC0MfEm9vXcS2zTtY50hhfFqrM8
PsnsmMmhdp9IAIsT7tb3FYEaG58+yFWIFRGhNNPfjxxrK/U37UOJRJyfnrK0VEAq9Lhel90pINeV
7gPZTaSQaIQ0/TF1/lc9/veLxTMvlk8VeZc0Xbq43LB7mPxVlQfZ2SCqlkzrwmX0aO34K0DGk4FB
WITsR4irfR1NEKmybW1gt/5JNAkE0LrwbUfSuSeCmg/v/ORu+vNnV0MkqneraB2HB1CuQalA+jTb
oBAaAtsok3Yz7YkfUwb9TzvIow+KF7ihqFbmH894vfbGSlExl9u5lmDXtMx1Gs4VAD08I77p/yww
KelWBCwaqJJ+3jJzIiRsfn7kc6HyRbaqIeQukFB/FGoh/cIw2iSAN42U7fqtdpgOBQXNrZvomD+h
WTzVvyugicVGVdKwuS6lpgaMITr2rJsR+Qu12TorL/pgSUVBIoiTp7L0uJlKb/2kMNvXk0kyUWCG
wNJ4xCAnChE3HIWS3N4UW7ZdEu8sk1JTbzGKCUsDEoDXyxRpvDPF7ioZSaOiclmko+kj6YDPBKqR
08yz6giEtsFupDdCzs6Va3Y9GJ5utPGWI+89QQNW5etuPyxRrJq5ZQ7jTbjzLfa+P+Jpr3Xm/4KL
dFTHzg0hdm1znp46i117kj0SiyP/SseStqcX9Fn2uB/KB6VQ+Pdgw8FkTsPIwxccrn5KiIRSt/hJ
90ORbrdBjrmTYA4ZIN3l0vVb3D3FT/JT8amfYa74Mc1LeWEJuA4+ixny/AUhZg5HvIjh+owjuRJW
B9/E2bKScJwrX874Gl1O7FcQIoM0KRtV67e0Zvg1J9k83w1g+Z2Zhvq6mUoC1812qEj6VleiTSlV
xx1T8ix75z0TBDqmTscMSAhypIPq977ITKTD6RrmerOWfiHSKDZlLs/Ggyg8wpza8bLQ5KBAzxaI
DkkOQcO9dGSl0WorTTxQeOa8mKFwk/GERbeNjRXtkN5MUJjQfHMPm85i9f98IBqMyeZgBhYqdacY
7++3iRdvUqiT4r0W2rtYUv2BdcAmNmM1LJLd/Xauw4yFvjHtgpV7PKbkUw6tLpLkX149FFMKLbOF
Sl6fJY3AcFz5+dz5mY63p2XXAT/GQUGKX4jh9kNnz/vB4DjX6C1D8DZYBGUfJX+LttnqFt5LLwCL
8ynLSI3f3i2OIwZYUhE5U91m2oPj8spi/7zjWnv0berVn49J5wyOoZAaEAVFzCZRPMSlwQjPbtxA
8vxnGzRM8QNbsKxohgYUHcms2h6Mpwt/JW0I7RaDnFuSZvSWDiLY1r6ZCFIljlW71Xghj6mVCwcm
OYTN3UtUxu82vie3cBzuu+2wStjRr2aHEJL8ysBuxx9ot2zuySTxOxUeriGGoPqpZOLnd42oT0bP
mfuIH89DE31kBaM6adHM9lWINfQFkR5HhOFcJqM74d9YJD7/jtWbPfQW2yX7IuWa/q0CdvgN8xGx
IPl3HIer/GgetuIC9FuTSIQ9juGJdpx5s1Tbs93BhCioAkPaBsraXkPtZd1lFrYE2KrNQNpESNnU
4nAF2/7EX+F+nBuTlTeQx6Emq/5hcqATXVBvwGTcoCXOIetm5Qq9A4iNuDPaGPbXyjq8wRxccez3
qslxSpniyhM2sYXMpnL2hmGPFy/LuE8xAgj6plILm+7o8hliUDXOxSIxNyc0kPZm4L9GvoCnt+wm
aQgsZgzrOWbA62+iiyb8eLApzreDLyEDAwDpobzsWw0e7E9xV9sWCZXQRK8eJpMp/rpAQvCfu3G3
F8HreyL0nPfAvaYtaZm6Ynva8lpW2U2kfFzXPWDpN/EBETQEUh57yUMwc/Q9RRe46hBXiBynaxgX
QtiHGO0395aPJrUbMEzKBgFuwretC/UP3ZfTHJCBb/u9BYZgOqym1Yj5kAiBc/v5o9PnQdNtmrHe
v7xMNvZBHZt66ofiV/KZ8bYpdc18pf4cmj2cs5pyRYTtu9nK+++R3K6KxZPWXzBvaTsV855UMnka
vQ1raNBexF9a2obgGmPRXgp0MVA/Wb+dcLbfqlQ1/KW6tGUEmedpmZOJDqAKL8mKQ1kS5PTSjWot
mQVK671JBIsnwOZVTawYlKMBqlVC1BvmP5RGd4Z5uLKn5re9DGDsIHBx+7rQley0oITputgXphkH
XBOSJzwF+DOwlg2Hz3OtyUJ9kyUZncL0zoEO8TdHIHSzMKLzgRBiExWGV0yfh/Y388tmju5Sd6rx
JsAUbl2Vdif3wUWIo3WFaqgNkm+pYMw0Aq1SokqInrBX5ua2K7h2ibyq4YH2nC6EHZioq/1rvUJd
VA/fh9Pt2KzjxOdEzRm20NmjUGenmk+xa6D0bP0cxooqnr77d/wpXGf2aY+YHP7nAtW/QdmiPZwl
bC1uk51ZcVYDRtMDg8VcVgMpCeGWwAWzUAUrjdkmwXSwtRWvWH0QB9W+0ze1WLscz8UUA/fVqqoB
Exem+95ppdf58Xd+c0rRI6BYt7rto2RBIe3ZjrVyZpDmVPiGcOpFxgJ80/iYk8ibDytAHdXMDIjA
BpybATqiqAhwwgwAXqmyIUaKkHHo/JWNKelIbCNb0/qe2B8NCBqIEwCkqvaEe1SHnUww6W/KpyEg
1zKm+Ld78bnqbcILynpHDgr4plL7AqO57nT+UIURmXwt14dhFYdhJWeNygEIlAEKMiTu3LeqTWO9
XYwhz9VBCjSsIqIyLidfhwDtiFHeeX91WtEHtEhOQjC5wHw32EI/jgkQrHI8qDNRlGOpITErkt/N
KnsyxLvFSa8Q1qcZ6E/pB3sUU5/4ZzyY3dCg4xyX8TQfjhvuQixYVopTQiRtuc8dkwJh4ERorqw+
r7vRivK/nv5FauL+poLwHBF4BYgVHbns7qKQYPQcoe7AElQY0gFI9fMLq8Mnz1DMpYSlCYJilDKE
+Sf1GlZbT6cqYGXlzby1tE4MwWQ1JXCMBcgHyY9VrgiTdfmQyMQ29bGOL7NtAf9uLNWwPdFPw+V6
Lzrq5ZFq0xwQAH+skH5OB8EZH5YH8E9KrjfP3c/tWxtx3ORgMJQLH09J3FZFeqhaoL7ZBpM7OghZ
m3Ec9uyeTQDRZfNIvB3ebwK3978ldTfPacf0giem+hSDg/9GUR6P/yq/qoRgUObWTv1SnM4roQuN
KI56+vLudy+0cGN2HTCLfvExD+HstZzwxBQ2Yf520eD+U5tamGv4cgQb2UPIr/IBkrXvswaNS1wT
mFSTlhAJb3VrTCdWZJY/fN2WmETYI863EFMXgDaR13Tl9zCBraQYlUFmBC5zVJhVZe6ef++vRi/G
uPPNogoJIqpgT3Vbk/TAMrCS4hBd5WkfjiM9aikIqLjdnq1CvV8OBNmr0bD2i9BVkEhFHKLQeFCu
rNJ8luKBKFu598eKovh4s2jdqNBnPg2lHipyb4OaNaP6WM1j4G19CX7hRrPfudErnid/XZOAOAgX
nZ9KiP3jpDl/rgAMGMcfGZ9aEP3s1l8cZabvGbTMIQitthk82ZYBfmsNGWbGZiZYMQQ3dspK1Rfp
E71LD7NLQ/ZYCv0P7lh/bER9BlPgzOZMNeVCJRFu94IUrl0R8+Ij5jMbEHmF50tnz4/1uZZ74mBe
3HoxiDsKn45YF1axZn5wUJchq/jPbLTm+PdyiQqgwhpY0GdQ55roGBS2Pk+znuWkL0a7pTiIhn6b
y/3Ea5kAmU9bXI+AGGLYGQbYnN6wvjwT5vGlDg4PKMrzMeKV3ULsUw5qljyGuCydSA9bCAdCSPV8
e+DGFfS/CbIQ83EeSV7z08lqjm6iPbBmq7vEuzc0/OJNNjabM950BbefmGKBJB+9rdWnMVXAt3HN
jmHMS/WqR6S0Zgo8US1Y2tTyApLlqLZ5IUCkk39mEA+QVImWYV0I5y8g5yyy2U0Jk0/7yGP9LnT2
M0lVliVDaT3hBUYYEwJY+Rl7QumpIax7+CaUSA/s4FlDIujfEYypFYnfO49jH54SRX/0nxZaZARK
whcHlrZbSiLW+EWNRB9p+FNwJ7rZKKI5obdvDeKNg2oKzQ8OaPVFI4BRupGmepKUQMwlgy4gXD+N
7pjrV6jHVtgnzOhLrRKw3jjyx3V6quJ3HKlyHaize58DMp8eN0z+B/HgUWxMnIY/Om8xsrVX7YYm
dDhn7gckmqew3niy55kHX86F9fRcHmll3JsDuqfC3MJzDXICHvZ9OwQR70yQTgx5xi8hkGUYp1b1
Wuwkxx7yeczRXxXYcz+Jl6Kj8JPGQiZ8i9uTZv+c2qh2FLCoUoKo9jJI1019nvhN2+k8TFi5heNj
xab89RVjYQdHpZ4IvJbQTmsljorrut5u00To5SczqeQQBf6IsPrRkZQw0F73Xpr0INt5CWTPFq4f
qC+pXOh8F0Qx1l/GwQmmhY6bIpbRE6A4HY8Afooc4zjWrN4YGyG5zAdeUXCiNAnViWRZKpUH5/Yx
IqNFRoBNq8N8IDv0cy2k8uL7FzI/6vrmD2UKmjpM632T3KANB9S54GWE/fbT/RJP1h/QrHj0ztkH
KO8MxnJ2tHZdLzgJ6mHBvlVXxadcPyCUtht5GgdydJfjaY7YXB+ZgoSCg1iy7YZWXi2FFjNQ5SYP
UbnsBDtECbrY3x7WO14R4PzMtwIwY0bJfI/ltTsZAP1h5pDvoXK1F57A81YCP61GmXpg91zx4sQ0
7Qyf8ZHeUZtHuCMqSf3GSGCdoNae/Gotbg57mMw2kCvVYKmg1wKIaMH62CQjVIE1EwVneLcaM422
Mv13ZoZZpBO8rYBLNXWsirStbefmZ09XkJ47LOpvbvvsakZ4twdmGfkL05KrbWdnZNrJ/poWiw7D
Ms02Sm9kdzVnaAm4WgNqKDdc/HZDKdn2j+9kJ4nSqcIAmNfHZpWWSqrT5ESDkLLmEg773Y9UkSwR
mkftccif4MM9BbZBKmZd33JMVS5xhoTLMOfqpB/nuTuMm4fxnhTIRVDbVsDJdpW1sp09DLeRiqYu
kmZr53SyrBOe6p9vqN2pkqcn6zeUonHPKDKtEmdNSTLDXR/NdPjcJ0uRgVRKVdCy9Og5CRGEm0v7
7tQrlxIt7cuwKP6l7qRGv3Go/hYzCkn6QrclZ164UnGs5gPwy6KbO25FpKMVqkirbrB5AogRDlJr
sv22wL8gjG5gVUY01XNmuyrAQ/dhoiRGrA+820hxycEsTEmc9r+El1ysjEheYrPNB8AQkqfJRfN/
1sgrvXLFuEM9SoBb3zZ9VwMOa+MDP469w67RHPPGzI88hZjvxAeO/OHXM8gZkHh3vGntawB5pcMW
+5c7yxLzTSoWxeJ/duZvBV2RTz3dx7HH3RriZhVeC1tB2MYxZDS+A7Jx+YbA0gspnCYgV436FmaS
JYISO/aowxtMk3UqgFZwu8WMTt/hW0/rq7aS3PO48inMIypd7IyWIiVDDg5M02Ns9A2X+XN73Fdh
88DO+c9SVBcJelop32cYciSQpLgdX+yqO7SM+hgD8HjRM9WWGpNj8uYXLtZW2Zx0UxiLQ35wwbLs
ZpOjpvC97mmtF8Nq6Vg3c2iiE55tERiAUvRiA9nQRRsmFO7aEcXGOvGREaBjLkCdf5/TNNOnbl4q
CPbeS5IM6zDKKkXtvodURwl0knM66ghQW6F/s00RH7BA4sF5DdhUTThjIRawhMH1SjfN4lPPwm1h
rijUlnjJk/p695m5i8NXKHdEvKS3w3iks7GijpWi1Nm8V195QgVvhTZII+x3pe/OtOd8vX76PQnd
3xFS4PKT0U06F+yw4R4lAGaKnyoN6f9tXCqpAy28ngWlzXlAPpX2Unh4BjOcifDw1Gdrt67zulZL
sNH2FP15Y1TFMk7KquY/2KmcpGJf+DsVb9BPQ/nOS26Cf7Yjovbcec7oNvvbwTwdS7ZbtqTTIQN8
SeazSHXoFV/hhf/07HSootvbJrlcNjD5w+5EmJasojDtmDsK9E6lDKqoJtEV9ZjVccCtSafcIPyP
aW3ZtKFaCwyMws13GrJyZgEsL6h6Iy3qoIvhQchK5r5nfj3jocd1r0tNXIOwv2R5ClBJ0ctoQAEk
YbAe3+Km7kZypyKQbNDA/Y6SVhW17HCPuKtBPP993hDWGgj/V/+klkK+vFXVVLmZxxC4H4+ng76S
N0z1+QVTl3zdVA0di6QSmrK32ncdVAiXuGzCQrrgVuzbZ0mUhW8l9wxyR3/hxvqxbHH9e57vq5Ye
6qTxHSsWyzNQgQEaiLkHOlmbUCXeBegpUvMN7ID5Px6K9owQ2GeJCTN4VNyAdkdBfM23M2TTYTGm
qZrjSF44dPEW+LUmfo7fo8jWNKgpQKDY+5ccs1kHd24jDkFVrGVDtPtFFl2yxDlhIgyqvPmYNZ5V
7rAEYV7SOWZkbybqhGueKAcu4Ht5KSqASg4He/EwbbaGS3+AwF38+BHHjeL5IdkhU7ZwPkinoQ/8
ikjUh/lKeayfM1pFhNyoA3biVX/ndsag4jKEMXGatVdaZSFuPatJEjfLHM+Dhw5MK7g1zL/VXqbf
nK2JiH1VItGXl680FhymYV95D3XVa70r0EprIienFThYQJVmHE536dJcirB8AQwejlZl4jzWGrzF
eqA0JyGShhyzgKxX5nGBPz+A3dNrvH5EWGFEzs/uSlkBzVBhKLq1uwSGkpWOV5XZ33V651rg/hgT
Za+8BoS1jKmCVx/n809jVbydW39PmRjQEmHKiOqungqFdnH8QwKM+EWOIQfjRe5ro33YYopk/qcM
7M2I9n/l0R+rAVKvF/Nrh5uFbHfEVgYd2f5Q6Jyf5emyv4Jk+UmYqdnJpzYAEyPzDNCvtCHORQuj
jt8+5F0xzigChzGIve01F4Y+61pgDVYyRg7WV4wIAf9IDgG+JXkUEglK2XkWf3YlzdRvOm7yembx
2Rqfd0M6cZPViJPx0Lklusc0XeIWGXwj7PoRcfjtpsAF7wrFqgsUy37I3cHb205p/JIzkxh5Qdwl
ZAIarXmCF8ngYtdXXb+cPGDLqSzJ5/hQsRobn3xTAyxOJuovByavVTWc9fV+lc2PwhKAro4f9zWy
uBRZGWwac6qLgVNxV2O996Fn2w9l2sztK/77PqvWb4qyokxFpb4miM6nFzvZZfeLgsZ/Q12Omn4r
YmyuXOaMzUdMDHnQohTsGrdYMWe2IpLc863w8D6LTpBnh9Py5VCEvbDVv9+irnJixI6xUx0caZjo
YhGAKf3pTpvRVL0t2wB4SgP4vwYuRxmkjpssOdUwYVH4ypSETX67o4auizNmf9R1UmrZuCGVKc6G
4F2BQKZSQsDpyLV7Ir0Kp/Mq+ynNfVGQB2/y8pWoLIbgDfBKXaqwYJtBHCkIC6ceB5FASAVe/p0l
KiDl1dWs8EB/F8QtwFSttRywCOrcxr//MNNnpKRBTUsEfDIl/ts/XMSi8BaRbUuxQ7ZB/tJDdvC0
KZ54E7MLvA/HvJBNW5WNBeYcIe9baO2fr4udg5zFelKFYFrUcjdl2A0aGEheFC+PKQc+5C/DdjEj
FTT08kqQ1OtCSDodKStPSTapPPUqwOcZ1UcewTUMIUM0iJf2a30R1I75EKoyLlJnE2HJIkuDw2wK
TGqg/pHbV5IOyT925ZySkqE0pYiF7zfqVxtNsZIDdwZP0GimzkwMxFUbM2YOBG15lOqhquJFWTWP
O9PWP6LG8jQaGBP8ga1WDMFd6XkXtwYu9lSqUHCDX6NT/PvpwekmUYdgAU7haPfQwPuWp74NdCot
q3Zhd8jtc4/+DyOWrJ4dqNIDb9b+ANMKNxp3v+RxHTCs6PHl4Q3qgG2Mcklrk0jwulrN2KtHDK42
/JvFJBL/0cMR91v/2Zttjuayni/PAIA4WyG/Aug9IsIdKtiWRrOmEMGNpCDsLydHKL3rCJ4OpYxo
Bhgd4JOpJ2rM3lHnqO2Kqg9HDvFd8EMMU9RuuRk3rwVBpkPRaFciuVa1T2M3gegeQ6KCiw0uB0wn
grdQEj25CTYlaWermyDRou3Q6ZoRZbzLbETpNIofuRJkWew6rjjlJIUDvnspy/rOoVI9wxatYEXU
dkkbdCxJ7soivfIHbHYYJL+McNU+6hfmOomUzBN59PsOs6iiZtVsjGXPQ/D2zyGnkX8D34Gachj4
FjZqSi3MZ+Y+BcdJLYPj20wiuNqrAZHPO9er1HUWE1L+1mDXUqwJ+mav97a+UAwMXdaj5Xv4ydqh
ump7CTx3QwFjdNPIC10SLEvdJP+7f6syqhQwIhaU8a1TvaWSckQQ5//+ndGSWhTjyKKVga4wvoKQ
80DWdqHmGvzik7Ntp84WhV4tI+ww243BhO0KwxsCvylptJqqiotGdKcla+Vkq3ehkO0IFLmiq83O
DrEC7InpHmvjtnRQmcaVUt7O+2DdSb6QmMwRq1kfQ15k56fM6JdlpWVyGxd4TP7BnuTTFz5KdUJj
8IZO4n9lg6oG4mCp94COBfyI+Rs0h7nDk+kWW9e31eAjcnoDT3Hu/tHW0v42YPcx2jevC/Eub14T
itpX5HYEEJShgUsxizmbFUVfUW8JMwAtdmHBwpoKxUzxqxEvcPiXNg2uTPwYY54pib3AXfb/hLZE
KvJlxB2aT69WRpb5aNsWhvBzxUNVAxIz281gH9crmShG6cAfwE1nqy/EOBoCp5l1dBWFV0UWhreu
fzW6xYRtBnF52DCyflQia+aCbB41tid9StzqjlRqkoFP2yqWZJXrKd+b14TrFLd0deiJSB8gnjU4
NL8ynqi8COF7OYeDazkORSp2IhtBbr9yQGeWeQyK1dswAI7Kwzm2KifPXEFosQHsFv9vMeaPikYg
P0TT9lgBQuyhuEbkO46mbE+q91aNt3avN6T+ih6GYpsYIs5acCvjTHnsDcg4AUtqBIxDqhrIA6ZK
kG8krXCHkDBlNjJTA4mkQnhmkPNSVgG9P0VfobUna2ov6Ztv4WrX26ENpsgygSjGUPviXzYqadw7
vGn/k1IR+XYQK+s0If5Lyxu5H0BDlG7rNELU8vitrJvTeL88X8sfRmOCvVM8cJTqOELkgdF2vwy5
0Qu6fQ6VbtPBaRSCUQgm/Qd3M41jJo+CLzNqUvKPbzc8SwnM2/6y2+qx32orIUe5/UfvD1cygv2f
6JPIWqX1b3GhiVLj+jTWDSk93vMeCDlwDXXWO/eV5ZgnCaINUrLEvn5sqqojDLYSQ1M5PODEgE1V
w+xoieIA4pYGuvCGn6HSddqGbJjcMbCo+i5d4KJuLkgiu0X1Ejq7qK844ugvxr7kXkac7MWyhSRE
lj4aOM9fmAEgnfwp0hNBaGcQOrbFCc/AwB+J2GdvSghnFdVqFzo8lzBzZrSMclUvdkwWVFeuZLes
OUIQf5hs11xcZISGfAMhPifFUkByS2+/DqqksBHmiJF6+vQe1yGLVmCaws+SWYUhe3P18qEX2uIh
XKnltS1MwHNtVuc31MzmpIxerF3E3LiA/L98olizUlKvJR5p3dN4Qn49fXB5KttdXqohxurGsIhl
F8I1zDqhJCvuIa+nXecgvL3f8N0IYOAelGuiA8o3+cDxgSkq1NvkKwoB2IMtDs9C+jUEstDagxyi
cIiKQxNvuyshYa4vM4Un0Wd7X2TL26/TgRS+jgmzieFtQqFtNVIilvqcRylB9E5+XnApGMSgwGzt
tfCuA7VktwNLBOQw6eitXx5HDIQDgr3DjFG3umNqOO/Ywp/PwOxULiF1vLKvKGUPJYKODnTgQXfj
ta54MxAE2JUkbUpFrpBAIveiyeT6r8g/nYvj4EMDY205ovjMSO928hrxza9PfNELhvY/VIdc6tiX
o7pSmG5UC+OYIqu56fw7d9y0o9/UQx2JAWiud969Zm1/cdX5LMP16yrO1zzJ5a/ehsIyDrSPFKZz
ASWJkk/tNERljBVD2MQIiAEnvBju3GB+fnV51lm3323B5oZeqgPbyKP5cJYTOVNgfM4N/YO3t0XK
OTn6CRJaBWHk7ob2zkXdtiRgvj01Dbdy09677Eu8kB3/sSguZtS4G7BklLwFl4rpj0spziHKkyL1
hQ==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is "artix7";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 : entity is "yes";
end design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14;

architecture STRUCTURE of design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "0";
  attribute C_HAS_CE of i_synth : label is 0;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 0;
  attribute C_SINIT_VAL of i_synth : label is "0";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of i_synth : label is "soft";
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "0";
  attribute c_depth of i_synth : label is 1;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => '0',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_4_c_shift_ram_8_1 is
  port (
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_4_c_shift_ram_8_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_4_c_shift_ram_8_1 : entity is "design_3_c_shift_ram_0_3,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_4_c_shift_ram_8_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of design_4_c_shift_ram_8_1 : entity is "c_shift_ram_v12_0_14,Vivado 2020.1";
end design_4_c_shift_ram_8_1;

architecture STRUCTURE of design_4_c_shift_ram_8_1 is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "0";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "0";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 1;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of U0 : label is "soft";
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "0";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 1;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_3_sim_clk_gen_0_0_clk, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency data_bitwidth format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0}}} DATA_WIDTH 1}";
begin
U0: entity work.design_4_c_shift_ram_8_1_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => '1',
      CLK => CLK,
      D(0) => D(0),
      Q(0) => Q(0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => '0'
    );
end STRUCTURE;
