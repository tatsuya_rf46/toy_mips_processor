// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:31:00 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_hazard_unit_0_1/design_4_hazard_unit_0_1_stub.v
// Design      : design_4_hazard_unit_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "hazard_unit,Vivado 2020.1" *)
module design_4_hazard_unit_0_1(BranchD, RsD, RtD, RsE, RtE, WriteRegE, WriteRegM, 
  WriteRegW, MemtoRegE, RegWriteE, RegWriteM, RegWriteW, RegtoPCD, LeavelinkW, LeavelinkM, StallF, 
  StallD, ForwardAD, ForwardBD, FlushE, ForwardAE, ForwardBE)
/* synthesis syn_black_box black_box_pad_pin="BranchD,RsD[4:0],RtD[4:0],RsE[4:0],RtE[4:0],WriteRegE[4:0],WriteRegM[4:0],WriteRegW[4:0],MemtoRegE,RegWriteE,RegWriteM,RegWriteW,RegtoPCD,LeavelinkW,LeavelinkM,StallF,StallD,ForwardAD[1:0],ForwardBD[1:0],FlushE,ForwardAE[1:0],ForwardBE[1:0]" */;
  input BranchD;
  input [4:0]RsD;
  input [4:0]RtD;
  input [4:0]RsE;
  input [4:0]RtE;
  input [4:0]WriteRegE;
  input [4:0]WriteRegM;
  input [4:0]WriteRegW;
  input MemtoRegE;
  input RegWriteE;
  input RegWriteM;
  input RegWriteW;
  input RegtoPCD;
  input LeavelinkW;
  input LeavelinkM;
  output StallF;
  output StallD;
  output [1:0]ForwardAD;
  output [1:0]ForwardBD;
  output FlushE;
  output [1:0]ForwardAE;
  output [1:0]ForwardBE;
endmodule
