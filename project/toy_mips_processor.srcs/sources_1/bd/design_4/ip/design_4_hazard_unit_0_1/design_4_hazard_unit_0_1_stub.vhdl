-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:31:00 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_hazard_unit_0_1/design_4_hazard_unit_0_1_stub.vhdl
-- Design      : design_4_hazard_unit_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_4_hazard_unit_0_1 is
  Port ( 
    BranchD : in STD_LOGIC;
    RsD : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RtD : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RsE : in STD_LOGIC_VECTOR ( 4 downto 0 );
    RtE : in STD_LOGIC_VECTOR ( 4 downto 0 );
    WriteRegE : in STD_LOGIC_VECTOR ( 4 downto 0 );
    WriteRegM : in STD_LOGIC_VECTOR ( 4 downto 0 );
    WriteRegW : in STD_LOGIC_VECTOR ( 4 downto 0 );
    MemtoRegE : in STD_LOGIC;
    RegWriteE : in STD_LOGIC;
    RegWriteM : in STD_LOGIC;
    RegWriteW : in STD_LOGIC;
    RegtoPCD : in STD_LOGIC;
    LeavelinkW : in STD_LOGIC;
    LeavelinkM : in STD_LOGIC;
    StallF : out STD_LOGIC;
    StallD : out STD_LOGIC;
    ForwardAD : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ForwardBD : out STD_LOGIC_VECTOR ( 1 downto 0 );
    FlushE : out STD_LOGIC;
    ForwardAE : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ForwardBE : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );

end design_4_hazard_unit_0_1;

architecture stub of design_4_hazard_unit_0_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "BranchD,RsD[4:0],RtD[4:0],RsE[4:0],RtE[4:0],WriteRegE[4:0],WriteRegM[4:0],WriteRegW[4:0],MemtoRegE,RegWriteE,RegWriteM,RegWriteW,RegtoPCD,LeavelinkW,LeavelinkM,StallF,StallD,ForwardAD[1:0],ForwardBD[1:0],FlushE,ForwardAE[1:0],ForwardBE[1:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "hazard_unit,Vivado 2020.1";
begin
end;
