// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:31:01 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_multiplexer_32_0_1/design_4_multiplexer_32_0_1_sim_netlist.v
// Design      : design_4_multiplexer_32_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_4_multiplexer_32_0_1,multiplexer_32,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "multiplexer_32,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module design_4_multiplexer_32_0_1
   (inp1,
    inp2,
    selector,
    y);
  input [31:0]inp1;
  input [31:0]inp2;
  input selector;
  output [31:0]y;

  wire [31:0]inp1;
  wire [31:0]inp2;
  wire selector;
  wire [31:0]y;

  design_4_multiplexer_32_0_1_multiplexer_32 inst
       (.inp1(inp1),
        .inp2(inp2),
        .selector(selector),
        .y(y));
endmodule

(* ORIG_REF_NAME = "multiplexer_32" *) 
module design_4_multiplexer_32_0_1_multiplexer_32
   (y,
    inp2,
    inp1,
    selector);
  output [31:0]y;
  input [31:0]inp2;
  input [31:0]inp1;
  input selector;

  wire [31:0]inp1;
  wire [31:0]inp2;
  wire selector;
  wire [31:0]y;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[0]_INST_0 
       (.I0(inp2[0]),
        .I1(inp1[0]),
        .I2(selector),
        .O(y[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[10]_INST_0 
       (.I0(inp2[10]),
        .I1(inp1[10]),
        .I2(selector),
        .O(y[10]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[11]_INST_0 
       (.I0(inp2[11]),
        .I1(inp1[11]),
        .I2(selector),
        .O(y[11]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[12]_INST_0 
       (.I0(inp2[12]),
        .I1(inp1[12]),
        .I2(selector),
        .O(y[12]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[13]_INST_0 
       (.I0(inp2[13]),
        .I1(inp1[13]),
        .I2(selector),
        .O(y[13]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[14]_INST_0 
       (.I0(inp2[14]),
        .I1(inp1[14]),
        .I2(selector),
        .O(y[14]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[15]_INST_0 
       (.I0(inp2[15]),
        .I1(inp1[15]),
        .I2(selector),
        .O(y[15]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[16]_INST_0 
       (.I0(inp2[16]),
        .I1(inp1[16]),
        .I2(selector),
        .O(y[16]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[17]_INST_0 
       (.I0(inp2[17]),
        .I1(inp1[17]),
        .I2(selector),
        .O(y[17]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[18]_INST_0 
       (.I0(inp2[18]),
        .I1(inp1[18]),
        .I2(selector),
        .O(y[18]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[19]_INST_0 
       (.I0(inp2[19]),
        .I1(inp1[19]),
        .I2(selector),
        .O(y[19]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[1]_INST_0 
       (.I0(inp2[1]),
        .I1(inp1[1]),
        .I2(selector),
        .O(y[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[20]_INST_0 
       (.I0(inp2[20]),
        .I1(inp1[20]),
        .I2(selector),
        .O(y[20]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[21]_INST_0 
       (.I0(inp2[21]),
        .I1(inp1[21]),
        .I2(selector),
        .O(y[21]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[22]_INST_0 
       (.I0(inp2[22]),
        .I1(inp1[22]),
        .I2(selector),
        .O(y[22]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[23]_INST_0 
       (.I0(inp2[23]),
        .I1(inp1[23]),
        .I2(selector),
        .O(y[23]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[24]_INST_0 
       (.I0(inp2[24]),
        .I1(inp1[24]),
        .I2(selector),
        .O(y[24]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[25]_INST_0 
       (.I0(inp2[25]),
        .I1(inp1[25]),
        .I2(selector),
        .O(y[25]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[26]_INST_0 
       (.I0(inp2[26]),
        .I1(inp1[26]),
        .I2(selector),
        .O(y[26]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[27]_INST_0 
       (.I0(inp2[27]),
        .I1(inp1[27]),
        .I2(selector),
        .O(y[27]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[28]_INST_0 
       (.I0(inp2[28]),
        .I1(inp1[28]),
        .I2(selector),
        .O(y[28]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[29]_INST_0 
       (.I0(inp2[29]),
        .I1(inp1[29]),
        .I2(selector),
        .O(y[29]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[2]_INST_0 
       (.I0(inp2[2]),
        .I1(inp1[2]),
        .I2(selector),
        .O(y[2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[30]_INST_0 
       (.I0(inp2[30]),
        .I1(inp1[30]),
        .I2(selector),
        .O(y[30]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[31]_INST_0 
       (.I0(inp2[31]),
        .I1(inp1[31]),
        .I2(selector),
        .O(y[31]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[3]_INST_0 
       (.I0(inp2[3]),
        .I1(inp1[3]),
        .I2(selector),
        .O(y[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[4]_INST_0 
       (.I0(inp2[4]),
        .I1(inp1[4]),
        .I2(selector),
        .O(y[4]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[5]_INST_0 
       (.I0(inp2[5]),
        .I1(inp1[5]),
        .I2(selector),
        .O(y[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[6]_INST_0 
       (.I0(inp2[6]),
        .I1(inp1[6]),
        .I2(selector),
        .O(y[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[7]_INST_0 
       (.I0(inp2[7]),
        .I1(inp1[7]),
        .I2(selector),
        .O(y[7]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[8]_INST_0 
       (.I0(inp2[8]),
        .I1(inp1[8]),
        .I2(selector),
        .O(y[8]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \y[9]_INST_0 
       (.I0(inp2[9]),
        .I1(inp1[9]),
        .I2(selector),
        .O(y[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
