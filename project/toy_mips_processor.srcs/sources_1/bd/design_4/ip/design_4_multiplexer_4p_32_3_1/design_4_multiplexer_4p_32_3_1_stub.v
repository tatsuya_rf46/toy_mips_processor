// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:32:57 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_multiplexer_4p_32_3_1/design_4_multiplexer_4p_32_3_1_stub.v
// Design      : design_4_multiplexer_4p_32_3_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "multiplexer_4p_32,Vivado 2020.1" *)
module design_4_multiplexer_4p_32_3_1(in1, in2, in3, in4, selector, y)
/* synthesis syn_black_box black_box_pad_pin="in1[31:0],in2[31:0],in3[31:0],in4[31:0],selector[1:0],y[31:0]" */;
  input [31:0]in1;
  input [31:0]in2;
  input [31:0]in3;
  input [31:0]in4;
  input [1:0]selector;
  output [31:0]y;
endmodule
