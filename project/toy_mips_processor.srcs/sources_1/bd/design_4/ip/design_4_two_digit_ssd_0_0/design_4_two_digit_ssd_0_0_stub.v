// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Thu Sep 24 22:30:00 2020
// Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_two_digit_ssd_0_0/design_4_two_digit_ssd_0_0_stub.v
// Design      : design_4_two_digit_ssd_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "two_digit_ssd,Vivado 2020.1" *)
module design_4_two_digit_ssd_0_0(clk, rstn, bnum0, bnum1, dout, sel)
/* synthesis syn_black_box black_box_pad_pin="clk,rstn,bnum0[3:0],bnum1[3:0],dout[6:0],sel" */;
  input clk;
  input rstn;
  input [3:0]bnum0;
  input [3:0]bnum1;
  output [6:0]dout;
  output sel;
endmodule
