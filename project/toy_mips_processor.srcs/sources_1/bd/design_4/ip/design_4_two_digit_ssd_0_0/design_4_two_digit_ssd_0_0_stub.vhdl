-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Thu Sep 24 22:30:00 2020
-- Host        : tatsu running 64-bit Ubuntu 20.04.1 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/tatsu/Documents/toy_mips_processor/project/toy_mips_processor.srcs/sources_1/bd/design_4/ip/design_4_two_digit_ssd_0_0/design_4_two_digit_ssd_0_0_stub.vhdl
-- Design      : design_4_two_digit_ssd_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_4_two_digit_ssd_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    rstn : in STD_LOGIC;
    bnum0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    bnum1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 6 downto 0 );
    sel : out STD_LOGIC
  );

end design_4_two_digit_ssd_0_0;

architecture stub of design_4_two_digit_ssd_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,rstn,bnum0[3:0],bnum1[3:0],dout[6:0],sel";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "two_digit_ssd,Vivado 2020.1";
begin
end;
