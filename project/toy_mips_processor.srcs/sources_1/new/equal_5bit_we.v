`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/23/2020 11:50:18 PM
// Design Name: 
// Module Name: equal_5bit_we
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module equal_5bit_we(
    input [4:0] inp1,
    input [4:0] inp2,
    input we,
    output outp
    );
    assign outp = (inp1 == inp2) ? (we ? 1 : 0) : 0;
endmodule
