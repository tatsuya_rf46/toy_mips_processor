`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/25/2020 10:09:51 PM
// Design Name: 
// Module Name: logical_shift_l2_26to28
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module logical_shift_l2_26to28(
    input [25:0] source,
    output [27:0] shifted
    );
    assign shifted = {source, 2'b00};
endmodule
