`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/25/2020 10:30:48 PM
// Design Name: 
// Module Name: logical_shift_l2_32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module logical_shift_l2_32(
    input [31:0] source,
    output [31:0] shifted
    );
    assign shifted = {source[29:0], 2'b00};
endmodule
