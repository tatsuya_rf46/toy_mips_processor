`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/09/05 15:06:36
// Design Name: 
// Module Name: multiplexer_4p_5
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module multiplexer_4p_5(
    input [4:0] in1,
    input [4:0] in2,
    input [4:0] in3,
    input [4:0] in4,
    input [1:0] selector,
    output [4:0] y
    );
    assign y = (selector == 2'b00) ? in1 :
               (selector == 2'b01) ? in2 :
               (selector == 2'b10) ? in3 :
               in4;
endmodule