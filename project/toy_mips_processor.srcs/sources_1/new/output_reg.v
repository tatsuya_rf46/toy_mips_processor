`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/24/2020 09:39:16 PM
// Design Name: 
// Module Name: output_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module output_reg(
    input clk,
    input [31:0] PC,
    input [3:0] D0,
    input [3:0] D1,
    output reg [3:0] O0,
    output reg [3:0] O1
    );
    always @(posedge clk) begin
        if (PC >= 32'd74) begin
            O0 <= D0;
            O1 <= D1;
        end
    end
endmodule
