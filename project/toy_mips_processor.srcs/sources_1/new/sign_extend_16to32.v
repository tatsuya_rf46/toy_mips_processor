`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/25/2020 10:22:41 PM
// Design Name: 
// Module Name: sign_extend_16to32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sign_extend_16to32(
    input [15:0] in16,
    output [31:0] out32
    );
    assign out32[15:0] = in16;
    assign out32[31:16] = {16{in16[15]}};
endmodule
