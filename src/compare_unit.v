`timescale 1ns / 100ps
`default_nettype none

module comparer
    (input wire [31:0] srcA,
     input wire [31:0] srcB,
     output wire equality);
    
    assign equality = (srcA == srcB) ? 1'b1 : 1'b0;

endmodule

`default_nettype wire

    
