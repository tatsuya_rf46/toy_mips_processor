`timescale 1ns / 100ps
`default_nettype none

module hazard_unit(
    input wire          BranchD,
    input wire [4:0]    RsD,
    input wire [4:0]    RtD,
    input wire [4:0]    RsE,
    input wire [4:0]    RtE,
    input wire [4:0]    WriteRegE,
    input wire [4:0]    WriteRegM,
    input wire [4:0]    WriteRegW,
    input wire          MemtoRegE,
    input wire          RegWriteE,
    input wire          RegWriteM,
    input wire          RegWriteW,
    input wire          RegtoPCD,
    input wire          LeavelinkW, 
    input wire          LeavelinkM, 
    output wire         StallF,
    output wire         StallD,
    output wire [1:0]   ForwardAD,
    output wire [1:0]   ForwardBD,
    output wire         FlushE,
    output wire [1:0]   ForwardAE,
    output wire [1:0]   ForwardBE
);

wire lwstall;
wire branchstall;
wire jrstall;
wire jrforward;

assign ForwardAE = ((RsE != 4'b0) && (RsE == WriteRegW) && (RegWriteW == 1'b1) && LeavelinkW) ? 2'b11 :
                        (((RsE != 0) && (RsE == WriteRegW) && (RegWriteW == 1'b1)) ? 2'b01 :
                         (((RsE != 0) && (RsE == WriteRegM) && (RegWriteM == 1'b1)) ? 2'b10 : 2'b00));

assign ForwardBE = ((RtE != 0) && (RtE == WriteRegW) && RegWriteW && LeavelinkW) ? 2'b11 :
                        (((RtE != 0) && (RtE == WriteRegW) && RegWriteW) ? 2'b01 :
                         (((RtE != 0) && (RtE == WriteRegM) && RegWriteM) ? 2'b10 :
                          2'b00 ));


assign ForwardAD = ((RsD != 0) && (RsD == WriteRegM) && RegWriteM && (LeavelinkM == 1'b0)) ? 2'b01 : 
                        ((LeavelinkM && (RsD == 5'd31) && RegWriteM) ? 2'b10 : 2'b00);
                    
assign ForwardBD = ((RtD != 0) && (RtD == WriteRegM) && RegWriteM && (LeavelinkM == 1'b0)) ? 2'b01 : 
                        ((LeavelinkM && (RtD == 5'd31) && RegWriteM) ? 2'b10 : 2'b00);


assign branchstall = ((BranchD == 1'b1)&& (RegWriteE == 1'b1) && ((WriteRegE == RsD) || (WriteRegE == RtD))) || ((BranchD == 1'b1) && (MemtoRegE == 1'b1) && ((WriteRegM == RsD) || (WriteRegM == RtD)));

assign lwstall = (((RsD == RtE) || (RtD == RtE)) && MemtoRegE);

assign jrstall = ((RsD == WriteRegE) && (RegWriteE == 1'b1) && (RegtoPCD == 1'b1)) ? 1'b1 : 1'b0;
assign jrforward = ((RsD == WriteRegM) && (RegWriteM  == 1'b1)&& (RegtoPCD == 1'b1)) ? 1'b1 : 1'b0;

assign StallF = lwstall || branchstall || jrstall;
assign StallD = lwstall || branchstall || jrstall;
assign FlushE = lwstall || branchstall || jrstall || jrforward;

endmodule

`default_nettype wire
